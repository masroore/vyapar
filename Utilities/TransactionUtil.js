var TxnTypeConstant = require('./../Constants/TxnTypeConstant');
var SettingCache = require('./../Cache/SettingCache');
var settingCache = new SettingCache();

var TransactionUtil = {
  isUPIEnabledForTxnType: function isUPIEnabledForTxnType(txnType) {
    return settingCache.isCurrentCountryIndia() && settingCache.getIsPrintQRCodeEnabled() && (txnType === TxnTypeConstant.TXN_TYPE_SALE || txnType === TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType === TxnTypeConstant.TXN_TYPE_ESTIMATE || txnType === TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txnType === TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN);
  }
};

module.exports = TransactionUtil;