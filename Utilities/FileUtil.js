var FileUtil = {
	getFileName: function getFileName(fileObject) {
		var pdfName = fileObject.type.replace(/[\/:*"<>|]/g, ' ');
		if (!fileObject.format) {
			fileObject.format = 'DD_MM_YY';
		}

		if (fileObject.fromDate && fileObject.toDate) {
			if (typeof fileObject.fromDate == 'string') {
				fileObject.fromDate = MyDate.convertStringToDateObject(fileObject.fromDate);
			}
			if (typeof fileObject.toDate == 'string') {
				fileObject.toDate = MyDate.convertStringToDateObject(fileObject.toDate);
			}
			fileObject.fromDate = MyDate.getPDFDate(fileObject.format, fileObject.fromDate);
			fileObject.toDate = MyDate.getPDFDate(fileObject.format, fileObject.toDate);
			pdfName = pdfName + '_' + fileObject.fromDate + '_to_' + fileObject.toDate;
		} else if (fileObject.fromDate) {
			if (typeof fileObject.fromDate == 'string') {
				fileObject.fromDate = MyDate.convertStringToDateObject(fileObject.fromDate);
			}
			fileObject.fromDate = MyDate.getPDFDate(fileObject.format, fileObject.fromDate);
			pdfName = pdfName + '_' + fileObject.fromDate;
		}

		return pdfName;
	},
	showFileInFolder: function showFileInFolder(fileName) {
		var fs = require('fs');
		if (!fs.existsSync(fileName)) {
			throw new Error("File doesn't exist");
		}

		var shell = require('electron').remote.shell;

		shell.showItemInFolder(fileName);
	}
};

module.exports = FileUtil;