Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.viewTransaction = viewTransaction;
exports.deleteTransaction = deleteTransaction;
exports.openPDF = openPDF;
exports.savePDF = savePDF;
exports.sharePDF = sharePDF;
exports.preview = preview;
exports.previewByHTML = previewByHTML;
exports.print = print;
exports.returnTransaction = returnTransaction;
exports.receiveOrMakePayment = receiveOrMakePayment;
exports.filterContextMenu = filterContextMenu;
exports.bindEventsForPreviewDialog = bindEventsForPreviewDialog;
exports.unBindEventsForPreviewDialog = unBindEventsForPreviewDialog;

var _SettingCache = require('../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

var _TxnTypeConstant = require('../Constants/TxnTypeConstant.js');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _TransactionPDFHandler = require('../UIControllers/TransactionPDFHandler.js');

var _TransactionPDFHandler2 = _interopRequireDefault(_TransactionPDFHandler);

var _PDFHandler = require('../Utilities/PDFHandler.js');

var _PDFHandler2 = _interopRequireDefault(_PDFHandler);

var _ConvertToSalePurchase = require('../UIControllers/ConvertToSalePurchase');

var _ConvertToSalePurchase2 = _interopRequireDefault(_ConvertToSalePurchase);

var _ConvertToReturn = require('../UIControllers/ConvertToReturn');

var _ConvertToReturn2 = _interopRequireDefault(_ConvertToReturn);

var _ReceivePayment = require('../UIControllers/ReceivePayment');

var _ReceivePayment2 = _interopRequireDefault(_ReceivePayment);

var _MakePayment = require('../UIControllers/MakePayment');

var _MakePayment2 = _interopRequireDefault(_MakePayment);

var _TxnPaymentStatusConstants = require('../Constants/TxnPaymentStatusConstants');

var _TxnPaymentStatusConstants2 = _interopRequireDefault(_TxnPaymentStatusConstants);

var _MyDouble = require('../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var transactionPDFHandler = new _TransactionPDFHandler2.default();
var pdfHandler = new _PDFHandler2.default();
function viewTransaction(id, type) {
	var transaction = id + ':' + type;
	var ViewTransaction = require('../BizLogic/ViewTransaction.js');
	var viewTransaction = new ViewTransaction();
	viewTransaction.viewTransactionFile(transaction);
}
function deleteTransaction(id, type) {
	var transaction = id + ':' + type;
	var DeleteTransaction = require('../BizLogic/DeleteTransaction.js');
	var deleteTransaction = new DeleteTransaction();
	deleteTransaction.RemoveTransaction(transaction);
}
function openPDF(id, type) {
	var isWindowCreated = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

	var transaction = id + ':' + type;
	var html = transactionPDFHandler.transactionToHTML(transaction);
	if (html) {
		pdfHandler.openPDF(html, isWindowCreated);
	}
}
function savePDF() {
	var type = ''; // $('#savePDF').attr('data-customheader');
	var fromDate = ''; // $('#savePDF').attr('data-filedate');
	pdfHandler.savePDF({ type: type, fromDate: fromDate });
}

function sharePDF() {
	$('#loading').show(function () {
		var receiverId = $('#sharePDF').attr('data-receiverid');
		pdfHandler.sharePDF(receiverId);
	});
}
function preview(id, type) {
	var asDeliveryChallan = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

	var transaction = id + ':' + type;
	var html = transactionPDFHandler.transactionToHTML(transaction, asDeliveryChallan);

	if (html) {
		pdfHandler.generateHiddenBrowserWindowForPDF(html);
		transactionPDFHandler.openPreviewDialog(html, id);
	}
}

function previewByHTML(html) {
	if (html) {
		pdfHandler.generateHiddenBrowserWindowForPDF(html);
		transactionPDFHandler.openPreviewDialog(html);
	}
}

function print(id, type) {
	var transaction = id + ':' + type;
	var settingCache = new _SettingCache2.default();
	var ThermalPrinterConstant = require('../Constants/ThermalPrinterConstant.js');
	if (settingCache.getDefaultPrinterType() == ThermalPrinterConstant.THERMAL_PRINTER) {
		var PrintUtil = require('../Utilities/PrintUtil.js');
		PrintUtil.printTransactionUsingThermalPrinter(id);
	} else {
		var html = transactionPDFHandler.transactionToHTML(transaction);
		if (html) {
			pdfHandler.print(html);
		}
	}
}
function returnTransaction(id, type) {
	var transaction = id + ':' + type;
	if (_TxnTypeConstant2.default[type] == _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE || _TxnTypeConstant2.default[type] == _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN || _TxnTypeConstant2.default[type] == _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER || _TxnTypeConstant2.default[type] == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER) {
		(0, _ConvertToSalePurchase2.default)(transaction);
	} else {
		(0, _ConvertToReturn2.default)(transaction);
	}
}

function showHideOpeningBalanceShortcuts(menuItems, curTxnType) {
	var openPdfMenu = void 0,
	    previewMenu = void 0,
	    printMenu = void 0;
	openPdfMenu = menuItems.filter(function (item) {
		return item.id == 'openPDF';
	})[0];
	previewMenu = menuItems.filter(function (item) {
		return item.id == 'preview';
	})[0];
	printMenu = menuItems.filter(function (item) {
		return item.id == 'print';
	})[0];
	if (_TxnTypeConstant2.default[curTxnType] == _TxnTypeConstant2.default.TXN_TYPE_ADJ_OPENING || _TxnTypeConstant2.default[curTxnType] == _TxnTypeConstant2.default.TXN_TYPE_ADJ_ADD || _TxnTypeConstant2.default[curTxnType] == _TxnTypeConstant2.default.TXN_TYPE_ADJ_REDUCE) {
		openPdfMenu && (openPdfMenu.visible = false);
		previewMenu && (previewMenu.visible = false);
		printMenu && (printMenu.visible = false);
	} else {
		openPdfMenu && (openPdfMenu.visible = true);
		previewMenu && (previewMenu.visible = true);
		printMenu && (printMenu.visible = true);
	}
}

function receiveOrMakePayment(id, type) {
	var transaction = id + ':' + type;
	if (_TxnTypeConstant2.default[type] == _TxnTypeConstant2.default.TXN_TYPE_SALE || _TxnTypeConstant2.default[type] == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN) {
		(0, _ReceivePayment2.default)(transaction);
	} else if (_TxnTypeConstant2.default[type] == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE || _TxnTypeConstant2.default[type] == _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN) {
		(0, _MakePayment2.default)(transaction);
	}
}

function filterContextMenu(menuItems, row) {
	var settingCache = new _SettingCache2.default();
	var curTxnType = row.data.txnTypeConstant;
	var paymentStatus = _MyDouble2.default.convertStringToDouble(row.data.paymentStatus);
	var rtnItem = menuItems.find(function (item) {
		return item.id == 'rtn';
	});
	if (rtnItem) {
		if (_TxnTypeConstant2.default[curTxnType] == _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE || _TxnTypeConstant2.default[curTxnType] == _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN || _TxnTypeConstant2.default[curTxnType] == _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER) {
			rtnItem.title = 'Convert To Sale';
			rtnItem.visible = true;
		} else if (_TxnTypeConstant2.default[curTxnType] == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER) {
			rtnItem.title = 'Convert To Purchase';
			rtnItem.visible = true;
		} else if (_TxnTypeConstant2.default[curTxnType] == _TxnTypeConstant2.default.TXN_TYPE_SALE || _TxnTypeConstant2.default[curTxnType] == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE) {
			rtnItem.title = 'Convert To Return';
			rtnItem.visible = true;
		} else {
			rtnItem.visible = false;
		}
	}

	var receivePaymentItem = menuItems.find(function (item) {
		return item.id == 'receivePayment';
	});
	if (receivePaymentItem) {
		if (settingCache.isBillToBillEnabled() && paymentStatus != _TxnPaymentStatusConstants2.default.PAID && (_TxnTypeConstant2.default[curTxnType] == _TxnTypeConstant2.default.TXN_TYPE_SALE || _TxnTypeConstant2.default[curTxnType] == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN)) {
			receivePaymentItem.title = 'Receive Payment';
			receivePaymentItem.visible = true;
		} else if (settingCache.isBillToBillEnabled() && paymentStatus != _TxnPaymentStatusConstants2.default.PAID && (_TxnTypeConstant2.default[curTxnType] == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE || _TxnTypeConstant2.default[curTxnType] == _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN)) {
			receivePaymentItem.title = 'Make Payment';
			receivePaymentItem.visible = true;
		} else {
			receivePaymentItem.visible = false;
		}
	}

	if ((paymentStatus == _TxnPaymentStatusConstants2.default.PARTIAL || paymentStatus == _TxnPaymentStatusConstants2.default.PAID) && (_TxnTypeConstant2.default[curTxnType] == _TxnTypeConstant2.default.TXN_TYPE_CASHIN || _TxnTypeConstant2.default[curTxnType] == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT || _TxnTypeConstant2.default[curTxnType] == _TxnTypeConstant2.default.TXN_TYPE_SALE || _TxnTypeConstant2.default[curTxnType] == _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN || _TxnTypeConstant2.default[curTxnType] == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE || _TxnTypeConstant2.default[curTxnType] == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN || _TxnTypeConstant2.default[curTxnType] == _TxnTypeConstant2.default.TXN_TYPE_POPENBALANCE || _TxnTypeConstant2.default[curTxnType] == _TxnTypeConstant2.default.TXN_TYPE_ROPENBALANCE)) {
		var paymentHistoryItem = menuItems.find(function (item) {
			return item.id == 'paymentHistory';
		});

		var _settingCache = new _SettingCache2.default();
		if (paymentHistoryItem) {
			if (_settingCache.isBillToBillEnabled()) {
				paymentHistoryItem.visible = true;
			} else {
				paymentHistoryItem.visible = false;
			}
		}
	} else {
		var _paymentHistoryItem = menuItems.find(function (item) {
			return item.id == 'paymentHistory';
		});
		_paymentHistoryItem && (_paymentHistoryItem.visible = false);
	}
	var printDeliveryChallanpreview = menuItems.find(function (item) {
		return item.id == 'printDeliveryChallanpreview';
	});
	if (printDeliveryChallanpreview) {
		if (_TxnTypeConstant2.default[curTxnType] == _TxnTypeConstant2.default.TXN_TYPE_SALE) {
			printDeliveryChallanpreview.visible = true;
		} else {
			printDeliveryChallanpreview.visible = false;
		}
	}

	showHideOpeningBalanceShortcuts(menuItems, curTxnType);
	return menuItems;
}

function bindEventsForPreviewDialog() {
	window.openPDF = function () {
		var html = $('#htmlView').html();
		$('#loading').show(function () {
			pdfHandler.openPDF(html, true);
		});
	};

	window.printPDF = function () {
		var html = $('#htmlView').html();
		pdfHandler.print(html);
	};

	window.savePDF = function () {
		var pdfString = $('#savePDF').attr('data-customheader');
		var txnDate = $('#savePDF').attr('data-filedate');
		pdfHandler.savePDF({ type: pdfString, fromDate: txnDate });
	};

	window.sharePDF = function () {
		$('#loading').show(function () {
			var receiverId = $('#sharePDF').attr('data-receiverid');
			pdfHandler.sharePDF(receiverId);
		});
	};

	window.openPreview = function (that) {
		$('#loading').show(function () {
			var html = transactionPDFHandler.transactionToHTML(that);
			if (html) {
				var txnId = that.split(':')[0];
				pdfHandler.generateHiddenBrowserWindowForPDF(html);
				transactionPDFHandler.openPreviewDialog(html, txnId);
			}
		});
	};

	window.closePreview = function () {
		try {
			$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
		} catch (err) {}
	};
}

function unBindEventsForPreviewDialog() {
	window.openPDF = null;
	window.printPDF = null;
	window.savePDF = null;
	window.sharePDF = null;
	window.openPreview = null;
	window.closePreview = null;
}