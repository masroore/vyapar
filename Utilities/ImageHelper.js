var ImageHelper = {
	openSaveImageDialog: function openSaveImageDialog() {
		var dialogOptions = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
		var callBack = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
		var imageProps = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

		var dialog = require('electron').remote.dialog;

		var _dialogOptions$title = dialogOptions.title,
		    title = _dialogOptions$title === undefined ? 'Select Image' : _dialogOptions$title,
		    _dialogOptions$button = dialogOptions.buttonLabel,
		    buttonLabel = _dialogOptions$button === undefined ? 'Select' : _dialogOptions$button,
		    _dialogOptions$filter = dialogOptions.filters,
		    filters = _dialogOptions$filter === undefined ? [{
			name: 'Images', extensions: ['jpg', 'png', 'jpeg']
		}] : _dialogOptions$filter,
		    _dialogOptions$proper = dialogOptions.properties,
		    properties = _dialogOptions$proper === undefined ? ['openFile'] : _dialogOptions$proper,
		    _dialogOptions$messag = dialogOptions.message,
		    message = _dialogOptions$messag === undefined ? '' : _dialogOptions$messag,
		    _dialogOptions$defaul = dialogOptions.defaultPath,
		    defaultPath = _dialogOptions$defaul === undefined ? '' : _dialogOptions$defaul;


		var selectedFile = dialog.showOpenDialog({
			title: title,
			buttonLabel: buttonLabel,
			properties: properties,
			filters: filters,
			message: message,
			defaultPath: defaultPath
		});
		var _imageProps$imageHeig = imageProps.imageHeight,
		    imageHeight = _imageProps$imageHeig === undefined ? 800 : _imageProps$imageHeig,
		    _imageProps$imageWidt = imageProps.imageWidth,
		    imageWidth = _imageProps$imageWidt === undefined ? 800 : _imageProps$imageWidt,
		    _imageProps$imageQual = imageProps.imageQuality,
		    imageQuality = _imageProps$imageQual === undefined ? 85 : _imageProps$imageQual;

		if (selectedFile) {
			var jimp = require('jimp');
			var imgPathArray = selectedFile[0].toString().split('\\');
			var imageName = imgPathArray[imgPathArray.length - 1];
			jimp.read(selectedFile[0].toString(), function (err, image) {
				if (err) {
					ToastHelper.error('Some error occurred. Please try again.');
					return;
				}
				if (image) {
					image.scaleToFit(imageWidth, imageHeight).quality(imageQuality).write(__dirname + '/../../TransactionImages/' + imageName, function () {
						var newImagePath = __dirname + '/../../TransactionImages/' + imageName;
						typeof callBack === 'function' && callBack(imageName, newImagePath, image);
					});
				} else {
					ToastHelper.error('Please select correct image and try again.');
				}
			});
		}
	}
};

module.exports = ImageHelper;