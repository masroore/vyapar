var FTUHelper = function () {
	var LocalStorageConstant = require('./../Constants/LocalStorageConstant.js');
	var LocalStorageHelper = require('./LocalStorageHelper');
	var DataLoader = require('../DBManager/DataLoader');
	return {
		isFirstSaleTxnCreated: function isFirstSaleTxnCreated() {
			var hasSaleTransactions = LocalStorageHelper.getValue(LocalStorageConstant.IS_FIRST_SALE_TXN_CREATED) == 'true';
			if (!hasSaleTransactions) {
				var TxnTypeConstant = require('./../Constants/TxnTypeConstant');
				var dataLoader = new DataLoader();
				hasSaleTransactions = dataLoader.hasTransactionsOfTxnType({ txnType: TxnTypeConstant.TXN_TYPE_SALE });
				if (hasSaleTransactions) {
					LocalStorageHelper.setValue(LocalStorageConstant.IS_FIRST_SALE_TXN_CREATED, 'true');
				}
			}
			return hasSaleTransactions;
		},
		isPremiumThemeClicked: function isPremiumThemeClicked() {
			return LocalStorageHelper.getValue(LocalStorageConstant.IS_PREMIUM_THEME_CLICKED) == 'true';
		},
		setPremiumThemeClicked: function setPremiumThemeClicked() {
			if (LocalStorageHelper.getValue(LocalStorageConstant.IS_PREMIUM_THEME_CLICKED) != 'true') {
				LocalStorageHelper.setValue(LocalStorageConstant.IS_PREMIUM_THEME_CLICKED, 'true');
			}
		},
		isPurchaseTxnCreated: function isPurchaseTxnCreated() {
			var hasPurchaseTransactions = localStorage.getItem(LocalStorageConstant.IS_THREE_PURCHASE_TXN_CREATED) == 'true';
			if (!hasPurchaseTransactions) {
				// let TxnTypeConstant = require('./../Constants/TxnTypeConstant');
				var dataLoader = new DataLoader();
				hasPurchaseTransactions = dataLoader.isPurchaseTxnCreated(3);
				if (hasPurchaseTransactions) {
					localStorage.setItem(LocalStorageConstant.IS_THREE_PURCHASE_TXN_CREATED, 'true');
				}
			}
			return hasPurchaseTransactions;
		},
		canShowPrintCentreInHeader: function canShowPrintCentreInHeader() {
			var canShow = LocalStorageHelper.getValue(LocalStorageConstant.FTU_SHOW_PRINT_CENTRE_ICON_IN_HEADER) == '1';
			if (!canShow && this.isFirstSaleTxnCreated()) {
				var dataLoader = new DataLoader();
				canShow = dataLoader.hasTransactionsOfTxnType({ count: 10 });
				canShow && LocalStorageHelper.setValue(LocalStorageConstant.FTU_SHOW_PRINT_CENTRE_ICON_IN_HEADER, 1);
			}
			return canShow;
		},
		canShowPaymentReminderInHeader: function canShowPaymentReminderInHeader() {
			var canShow = LocalStorageHelper.getValue(LocalStorageConstant.FTU_SHOW_PAYMENT_REMINDER_IN_HEADER) == '1';
			if (!canShow && this.isFirstSaleTxnCreated()) {
				var NameCache = require('../Cache/NameCache');
				var nameCache = new NameCache();
				var nameList = nameCache.getListOfNamesObject() || [];
				var requiredReceivablePartyCount = 5;
				for (var index = 0; index < nameList.length; index++) {
					var party = nameList[index];
					requiredReceivablePartyCount -= Number(party.amount > 0);
					if (!requiredReceivablePartyCount) {
						canShow = true;
						break;
					}
				}
				canShow && LocalStorageHelper.setValue(LocalStorageConstant.FTU_SHOW_PAYMENT_REMINDER_IN_HEADER, 1);
			}
			return canShow;
		}
	};
}();
module.exports = FTUHelper;