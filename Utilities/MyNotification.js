var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var os = require('os');
var version = os.release();
var getVersion = version.split('.');
var logger = require('./logger');
var SettingCache = require('./../Cache/SettingCache');
var settingCache = new SettingCache();
var Dataloader = require('./../DBManager/DataLoader');
var dataLoader = new Dataloader();
var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
var sqlitedbhelper = new SqliteDBHelper();
var MessageDraftLogic = require('./../BizLogic/MessageDraftLogic');
var messageDraftLogic = new MessageDraftLogic();
var NameCache = require('./../Cache/NameCache.js');
var nameCache = new NameCache();

var _require = require('electron'),
    ipcRenderer = _require.ipcRenderer;

if (Number(getVersion[0]) <= 6 && Number(getVersion[1]) < 2) {
	var WindowsToaster = require('node-notifier').WindowsBalloon;
} else {
	WindowsToaster = require('node-notifier').WindowsToaster;
}
var path = require('path');
var CompanyListUtility = require('./../Utilities/CompanyListUtility.js');

var imagePath = path.join(__dirname, './../Images/', 'vyapar.ico');
var imagePathInPNG = path.join(__dirname, './../Images/', 'vyapar_1.png');

function sendNotification(displayMessage) {
	try {
		var notifier = new WindowsToaster({
			withFallback: true // Fallback to Growl or Balloons?
		});

		try {
			notifier.notify({
				title: 'VyaparApp',
				message: displayMessage,
				icon: imagePath, // Absolute path (doesn't work on balloons)
				sound: true, // Only Notification Center or Windows Toasters
				wait: true, // Wait with callback, until user action is taken against notification
				appID: 'com.squirrel.Vyaparapp.Vyapar'
			}, function (err, response) {
				if (err) {}
			});
		} catch (e) {
			notifier.notify({
				title: 'VyaparApp',
				message: displayMessage,
				icon: imagePathInPNG, // Absolute path (doesn't work on balloons)
				sound: true, // Only Notification Center or Windows Toasters
				wait: true, // Wait with callback, until user action is taken against notification
				appID: 'com.squirrel.Vyaparapp.Vyapar'
			}, function (err, response) {
				if (err) {}
			});
		}
		notifier.on('click', function () {
			var MyAnalytics = require('./analyticsHelper');
			MyAnalytics.pushEvent('Payment reminder notification clicked');
			ipcRenderer.send('openWindowOnnotificationClicked');
		});
	} catch (err) {
		logger.error(err);
	}
}
function sendReminderSMS(smsList) {
	var isOnline = require('is-online');
	isOnline().then(function (online) {
		if (online) {
			if (smsList && smsList.length) {
				smsList.forEach(function (value, key) {
					var partyObject = nameCache.findNameObjectByNameId(value.nameId);
					messageDraftLogic.sendPaymentReminderMessage(partyObject, 'sms', false, false);
					sqlitedbhelper.updatePaymentReminderAlertDates(value.nameId, null, 'sendsms');
				});
			}
		}
	});
}

function sendPartySpecificNotification(nameMap) {
	if (nameMap.size > 0) {
		try {
			nameMap.forEach(function (value, key) {
				var displayMessage = 'You have to collect payment from ' + value;
				var notifier = new WindowsToaster({
					withFallback: true // Fallback to Growl or Balloons?
				});
				try {
					notifier.notify({
						title: 'VyaparApp',
						message: displayMessage,
						icon: imagePath, // Absolute path (doesn't work on balloons)
						sound: true, // Only Notification Center or Windows Toasters
						wait: true,
						appID: 'com.squirrel.Vyaparapp.Vyapar'
					}, function (err, response) {
						if (err) {
							logger.error(err);
						}
					});
				} catch (e) {
					notifier.notify({
						title: 'VyaparApp',
						message: displayMessage,
						icon: imagePathInPNG, // Absolute path (doesn't work on balloons)
						sound: true, // Only Notification Center or Windows Toasters
						wait: true,
						appID: 'com.squirrel.Vyaparapp.Vyapar'
					}, function (err, response) {
						if (err) {
							logger.error(err);
						}
					});
				}
				notifier.on('click', function (notifierObject, options) {
					var MyAnalytics = require('./analyticsHelper');
					MyAnalytics.pushEvent('Payment reminder notification clicked');
					ipcRenderer.send('openWindowOnnotificationClicked');
				});
				sqlitedbhelper.updatePaymentReminderAlertDates(key, null, 'remindon');
			});
		} catch (err) {
			logger.error(err);
		}
	}
}
function getLastNotificationSentAt() {
	try {
		var currentCompany = CompanyListUtility.getCurrentCompanyKey();
		var lastNotifications = localStorage.getItem('lastNotifications') || '{}';
		lastNotifications = JSON.parse(lastNotifications);
		return lastNotifications[currentCompany];
	} catch (error) {
		var _logger = require('./logger');
		_logger.error(error);
	}
}
function setlastNotificationSentAt(notificationSentAt) {
	var logger = require('./logger');
	try {
		var currentCompany = CompanyListUtility.getCurrentCompanyKey();
		var lastNotifications = localStorage.getItem('lastNotifications') || '{}';
		try {
			lastNotifications = JSON.parse(lastNotifications);
		} catch (error) {
			logger.error(error);
			lastNotifications = {};
		}
		lastNotifications[currentCompany] = notificationSentAt;
		localStorage.setItem('lastNotifications', (0, _stringify2.default)(lastNotifications));
	} catch (error) {
		logger.error(error);
	}
}
function checktoSendNotifications() {
	try {
		var lastNotilastNotificationSentAt = getLastNotificationSentAt();
		var showNotification = !lastNotilastNotificationSentAt;
		if (lastNotilastNotificationSentAt) {
			var lastSentAt = new Date(lastNotilastNotificationSentAt);
			var currentDate = new Date();

			var previousDayStart = new Date();
			previousDayStart.setDate(previousDayStart.getDate() - 1);
			previousDayStart.setHours(0, 0, 0, 0);

			var currentDayStart = new Date();
			currentDayStart.setDate(currentDayStart.getDate());
			currentDayStart.setHours(0, 0, 0, 0);

			var modifiedDate10 = new Date();
			modifiedDate10.setHours(10, 0, 0);

			if (lastSentAt < previousDayStart) {
				showNotification = true;
			} else if (lastSentAt < currentDayStart) {
				showNotification = currentDate > modifiedDate10;
			}
		}
		if (showNotification) {
			if (settingCache.getPaymentReminderEnabled()) {
				var numberOfDays = settingCache.getPaymentReminderDays();
				var notificationString = dataLoader.getPaymentReminderNotificationString(numberOfDays);
				if (notificationString) {
					var MyAnalytics = require('./analyticsHelper');
					MyAnalytics.pushEvent('Payment Reminder daily notification generated');
					sendNotification(notificationString);
				}
			}
			var nameMap = dataLoader.getRemindTodayList();
			if (nameMap.size > 0) {
				var _MyAnalytics = require('./analyticsHelper');
				_MyAnalytics.pushEvent('Party specific Payment Reminder notification generated');
				sendPartySpecificNotification(nameMap);
			}
			var smsList = dataLoader.sendSMSToday();
			sendReminderSMS(smsList);
			var notificationSentAt = new Date().getTime();
			setlastNotificationSentAt(notificationSentAt);
		}
	} catch (err) {
		logger.error(err);
	}
}
setTimeout(function () {
	checktoSendNotifications();
	setInterval(function () {
		checktoSendNotifications();
	}, 300000); // 5 min interval
}, 3000);