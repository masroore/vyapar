Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.destroy = undefined;

var _electron = require('electron');

var BrowserWindow = _electron.remote.BrowserWindow;

var whatsAppBrowserWindow = void 0;

var createWhatsAppWindow = function createWhatsAppWindow() {
	whatsAppBrowserWindow = new BrowserWindow({
		show: false,
		minimizable: true,
		alwaysOnTop: true,
		skipTaskbar: false,
		autoHideMenuBar: true,
		webPreferences: { nodeIntegration: false }
	});
	whatsAppBrowserWindow.on('close', function () {
		whatsAppBrowserWindow.destroy();
	});

	_electron.ipcRenderer.on('TerminateBrowserWindow', function () {
		whatsAppBrowserWindow.destroy();
	});
};

/** send msg to whatsapp
 *
 * @param {(string|object)} msg message to send
 * @param {function} callback
 * @return BrowserWindow
 */

var send = function send(msg, callback) {
	var whatsappWeb = void 0;
	destroy();
	createWhatsAppWindow();
	if (typeof msg === 'string') {
		whatsappWeb = 'https://web.whatsapp.com/send?text=' + encodeURIComponent(msg);
	} else {
		var _msg$header = msg.header,
		    header = _msg$header === undefined ? '' : _msg$header,
		    _msg$message = msg.message,
		    message = _msg$message === undefined ? '' : _msg$message,
		    _msg$footer = msg.footer,
		    footer = _msg$footer === undefined ? '' : _msg$footer;

		var body = encodeURIComponent(header + '\n' + message + '\n' + footer);
		if (msg.to) {
			whatsappWeb = 'https://web.whatsapp.com/send?phone=' + msg.to + '&text=' + body;
		} else {
			whatsappWeb = 'https://web.whatsapp.com/send?text=' + body;
		}
	}

	whatsAppBrowserWindow.loadURL(whatsappWeb, { userAgent: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36' });
	whatsAppBrowserWindow.show();
	if (callback && typeof callback === 'function') {
		callback();
	}
	return whatsAppBrowserWindow;
};

var destroy = exports.destroy = function destroy() {
	if (whatsAppBrowserWindow) {
		whatsAppBrowserWindow.destroy();
	}
};
exports.default = send;