var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _LocalStorageConstant = require('../Constants/LocalStorageConstant');

var _LocalStorageConstant2 = _interopRequireDefault(_LocalStorageConstant);

var _ImageHelper = require('../Utilities/ImageHelper');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getRadianAngle(degreeValue) {
    return degreeValue * Math.PI / 180;
}
var ItemImagesUtility = {
    getCroppedImg: function getCroppedImg(imageSrc, pixelCrop) {
        var rotation = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;

        var image = new Image();
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        image.src = imageSrc;

        var resizedCanvas = document.createElement('canvas');
        var resizedCtx = resizedCanvas.getContext('2d');

        return new _promise2.default(function (resolve) {
            image.addEventListener('load', function () {
                var safeArea = Math.max(image.width, image.height) * 2;
                canvas.width = safeArea;
                canvas.height = safeArea;
                ctx.translate(safeArea / 2, safeArea / 2);
                ctx.rotate(getRadianAngle(rotation));
                ctx.translate(-safeArea / 2, -safeArea / 2);
                ctx.drawImage(image, safeArea / 2 - image.width * 0.5, safeArea / 2 - image.height * 0.5);
                var data = ctx.getImageData(0, 0, safeArea, safeArea);
                canvas.width = pixelCrop.width;
                canvas.height = pixelCrop.height;
                var dt = data.data;
                for (var i = 0; i < dt.length; i += 4) {
                    if (dt[i + 3] < 255) {
                        dt[i] = 255;
                        dt[i + 1] = 255;
                        dt[i + 2] = 255;
                        dt[i + 3] = 255;
                    }
                }
                ctx.putImageData(data, 0 - safeArea / 2 + image.width * 0.5 - pixelCrop.x, 0 - safeArea / 2 + image.height * 0.5 - pixelCrop.y);

                resizedCanvas.width = canvas.width > 800 ? 800 : canvas.width;
                resizedCanvas.height = canvas.width > 800 ? 800 : canvas.height;
                resizedCtx.drawImage(canvas, 0, 0, resizedCanvas.width, resizedCanvas.height);
                var base64Image = resizedCanvas.toDataURL("image/jpeg");
                resolve(base64Image);
            });
        });
    },
    selectImage: function selectImage(callback) {
        var updateItemImage = function updateItemImage(imageName, selectedFile, image) {
            var jimpResult = function jimpResult(err, result) {
                result = result.toString('base64');
                callback && typeof callback === 'function' && callback(result);
            };
            image.getBuffer(image._originalMime, jimpResult);
        };
        (0, _ImageHelper.openSaveImageDialog)({ title: 'Select Item Image' }, updateItemImage, { imageQuality: 100 });
    },
    getItemImagesList: function getItemImagesList() {
        var itemImageList = localStorage.getItem(_LocalStorageConstant2.default.ITEM_IMAGE_LIST);
        if (itemImageList) {
            itemImageList = JSON.parse(itemImageList);
            var itemImages = [];
            if (itemImageList && itemImageList.length > 0) {
                var ItemImage = require('../BizLogic/ItemImages');
                for (var i = 0; i < itemImageList.length; i++) {
                    var itemImage = new ItemImage();
                    var itemImageObj = itemImageList[i];
                    if (itemImageObj.isNewImage) {
                        itemImage.setBase64Image(itemImageObj.image);
                        itemImages.push(itemImage);
                    }
                }
            }
            return itemImages;
        }
        return [];
    }
};

module.exports = ItemImagesUtility;