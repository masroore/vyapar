/* global jQuery */
module.exports = function ($) {
	var isNewB2BAdjustment = true;
	function openBankToBankTransfer(accountName) {
		try {
			MyAnalytics.pushEvent('Bank to Bank Transfer Open');
			var PaymentInfoCache = require('./../Cache/PaymentInfoCache');
			var paymentInfoCache = new PaymentInfoCache();
			$('#fromBank').html('');
			$('<option>').val(accountName).text(accountName).appendTo('#fromBank');
			var listOfBanks = paymentInfoCache.getBankListObj();
			$('#toBank').html('');
			$.each(listOfBanks, function (key, value) {
				var bank = value.getName();
				if (bank.toLocaleLowerCase() != accountName.toLowerCase()) {
					$('<option>').val(bank).text(bank).appendTo('#toBank');
				}
			});
			var DateFormat = require('./../Constants/DateFormat.js');
			$('#b2bDate').datepicker({
				dateFormat: DateFormat.format,
				buttonImage: '../../inlineSVG/item_adj.svg',
				onSelect: function onSelect() {
					this.focus();
				},
				onClose: function onClose() {
					this.focus();
				}
			});
			var date = MyDate.getDate('d/m/y');
			$('#b2bDate').val(date);
			$('.terminalButtonB2BParent #submit').text('Save');
			isNewB2BAdjustment = true;
			$('#bankToBankDialog').dialog({
				'width': 600,
				'height': 'auto',
				'title': 'Bank to Bank transfer',
				close: function close() {
					$('#bankToBankDialog').dialog('destroy').hide();
				}
			});
			bindEventListeners();
		} catch (err) {
			var logger = require('./logger');
			logger.error(err);
		}
	}
	function emptyB2BForm() {
		var inputs = $('#bankToBankDialog input');
		for (var i = 0; i < inputs.length; i++) {
			inputs[i].value = '';
		}
		inputs = $('#bankToBankDialog textarea');
		for (var _i = 0; _i < inputs.length; _i++) {
			inputs[_i].value = '';
		}
	}
	function editBankToBankTransfer(bankAdjId) {
		try {
			MyAnalytics.pushEvent('Bank to Bank Transfer Edit');
			var DataLoader = require('./../DBManager/DataLoader.js');
			var PaymentInfoCache = require('./../Cache/PaymentInfoCache');
			var paymentInfoCache = new PaymentInfoCache();
			var dataLoader = new DataLoader();
			// var accountName = $('#currentAccountName').html();
			$('#fromBank').html('');
			// $('<option>').val(accountName).text(accountName).appendTo('#fromBank');
			var bankAdj = dataLoader.LoadAccountAdjustmentForId(Number(bankAdjId));
			$('#bankAdjId').val(bankAdjId);
			var accountName = paymentInfoCache.getPaymentBankName(bankAdj['adjBankId']);
			$('<option>').val(accountName).text(accountName).appendTo('#fromBank');
			var toBank = paymentInfoCache.getPaymentBankName(bankAdj.getAdjToBankId());
			var b2bDate = MyDate.getDate('d/m/y', bankAdj.getAdjDate());
			var b2bAmount = bankAdj.getAdjAmount();
			var b2bDescription = bankAdj.getAdjDescription();
			var listOfBanks = paymentInfoCache.getBankListObj();
			$('#toBank').html('');
			$.each(listOfBanks, function (key, value) {
				var bank = value.getName();
				if (bank.toLocaleLowerCase() != accountName.toLowerCase()) {
					$('<option>').val(bank).text(bank).appendTo('#toBank');
				}
			});
			$('.terminalButtonB2BParent #submitNew').css('dsplay', 'none');
			MyAnalytics.pushEvent('Edit Bank To Bank Transfer Open');
			$('.terminalButtonB2BParent #submit').text('Done');
			isNewB2BAdjustment = false;

			$('#toBank').val(toBank);
			$('#b2bAmount').val(b2bAmount);
			$('#b2bDate').val(b2bDate);
			$('#b2bDescription').val(b2bDescription);
			$('#bankToBankDialog').removeClass('hide').dialog({
				'width': 600,
				'height': 'auto',
				'title': 'Bank to Bank transfer',
				appendTo: '#dynamicDiv',
				close: function close() {
					emptyB2BForm();
					$(this).dialog('destroy').hide();
				}
			});
			bindEventListeners();
		} catch (err) {
			var logger = require('./logger');
			logger.error(err);
		}
	}
	function bindEventListeners() {
		$('.terminalButtonB2B').off('click').on('click', function () {
			var ErrorCode = require('../Constants/ErrorCode');
			var fromBank = $('#fromBank').val().trim();
			var toBank = $('#toBank').val().trim();
			var amount = $('#b2bAmount').val();
			var date = $('#b2bDate').val();
			date = date + MyDate.getDate(' H:M:S');
			var description = $('#b2bDescription').val().trim();
			if (fromBank && fromBank != null && toBank && toBank != null && amount > 0 && fromBank != toBank) {
				var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
				var BankAdjustmentTxn = require('./../BizLogic/BankAdjustmentTxn.js');
				var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
				var paymentInfoCache = new PaymentInfoCache();
				var bankAdjustmentTxn = new BankAdjustmentTxn();
				bankAdjustmentTxn.setAdjAmount(amount);
				bankAdjustmentTxn.setAdjType(TxnTypeConstant.TXN_TYPE_BANK_TO_BANK);
				var paymentInfo = paymentInfoCache.getPaymentInfoObjOnName(fromBank);
				bankAdjustmentTxn.setAdjBankId(paymentInfo.getId());
				bankAdjustmentTxn.setAdjDescription(description.trim());
				bankAdjustmentTxn.setAdjDate(date ? MyDate.getDateObj(date, 'dd/mm/yyyy', '/', true) : new Date());
				paymentInfo = paymentInfoCache.getPaymentInfoObjOnName(toBank);
				bankAdjustmentTxn.setAdjToBankId(paymentInfo.getId());
				if (!isNewB2BAdjustment) {
					var adjId = $('#bankAdjId').val();
					bankAdjustmentTxn.setAdjId(adjId);
					var statusCode = bankAdjustmentTxn.updateAdjustment();
				} else {
					statusCode = bankAdjustmentTxn.createAdjustment();
				}
				if (statusCode == ErrorCode.ERROR_NEW_BANK_ADJUSTMENT_SUCCESS || ErrorCode.ERROR_UPDATE_BANK_INFO_SUCCESS) {
					if (statusCode == ErrorCode.ERROR_NEW_BANK_ADJUSTMENT_SUCCESS) {
						MyAnalytics.pushEvent('Bank to Bank Transfer Save');
					} else {
						MyAnalytics.pushEvent('Edit Bank to Bank Transfer Save');
					}
					ToastHelper.success(statusCode);
					emptyB2BForm();
					$('#bankToBankDialog').dialog('destroy').hide();
					onResume();
				} else {
					ToastHelper.error(statusCode);
				}
			} else {
				ToastHelper.error('Bank name and amount cannot be left empty');
			}
		});
	}
	var adjustCash = {
		openBankToBankTransfer: openBankToBankTransfer,
		editBankToBankTransfer: editBankToBankTransfer
	};
	return adjustCash;
}(jQuery);