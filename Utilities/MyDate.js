var DateConstant = require('./../Constants/DateConstant.js');
var moment = require('moment');
var MyDate = {
	// usage
	// getDate("y/m/d"));
	daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
	daysLong: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
	numberOfMilliSecondsInDay: 24 * 3600 * 1000,
	monthNames: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],

	getWeekOfMonth: function getWeekOfMonth(date) {
		var d = date.getDate();
		var day = date.getDay();
		return Math.ceil((d - 1 - day) / 7) + 1;
	},

	getMonthName: function getMonthName(date) {
		return this.monthNames[date.getMonth()];
	},

	getLastTwoDigitOfYear: function getLastTwoDigitOfYear(date) {
		return ('' + date.getFullYear()).slice(-2);
	},
	getFirstDateOfCurrentMonthString: function getFirstDateOfCurrentMonthString() {
		var currentDate = MyDate.getDate('d/m/y');
		return '01' + currentDate.substr(2);
	},
	getFirstDateOfCurrentYearString: function getFirstDateOfCurrentYearString() {
		var currentDate = MyDate.getDate('d/m/y');
		return '01/' + '01' + currentDate.substr(5);
	},

	getCurrentDateStringForUI: function getCurrentDateStringForUI() {
		return this.getDate('d/m/y');
	},

	getDateStringForUI: function getDateStringForUI(date) {
		return this.getDate('d/m/y', date);
	},
	getPDFDate: function getPDFDate(format, date) {
		return moment(date).format(format);
	},

	getDate: function getDate(format, date) {
		if (!date) {
			date = new Date();
		}
		var mDate = {
			S: date.getSeconds(),
			M: date.getMinutes(),
			H: date.getHours(),
			d: date.getDate(),
			m: date.getMonth() + 1,
			y: date.getFullYear(),
			q: this.daysShort[date.getDay()],
			Q: this.daysLong[date.getDay()]
		};

		// Apply format and add leading zeroes
		return format.replace(/([SMHdmyqQ])/g, function (key) {
			return (mDate[key] < 10 ? '0' : '') + mDate[key];
		});
	},

	convertStringToDateObject: function convertStringToDateObject(dateString) {
		return this.getDateObj(dateString, 'dd/MM/yyyy', '/');
	},

	getDateObj: function getDateObj(date, format, delimiter) {
		var considerTime = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
		var timeDelimiter = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : ':';

		if (!date) {
			return new Date();
		}
		var formatLowerCase = format.toLowerCase();
		var formatItems = formatLowerCase.split(delimiter);
		var dateAndItemPart = date.split(' ');
		var dateItems = dateAndItemPart[0].split(delimiter);
		var monthIndex = formatItems.indexOf('mm');
		var dayIndex = formatItems.indexOf('dd');
		var yearIndex = formatItems.indexOf('yyyy');
		var month = parseInt(dateItems[monthIndex]);
		var hours = 0;
		var min = 0;
		var sec = 0;
		month -= 1;
		if (considerTime && dateAndItemPart && dateAndItemPart.length && dateAndItemPart.length > 1) {
			var timeItems = dateAndItemPart[1].split(timeDelimiter);
			if (timeItems && timeItems.length > 2) {
				hours = timeItems[0];
				min = timeItems[1];
				sec = timeItems[2];
			}
		}
		var formatedDate = new Date(dateItems[yearIndex], month, dateItems[dayIndex] || '01', hours, min, sec, 0);
		return formatedDate;
	},

	// for storing date in db, converts object to string
	convertDateToStringForDB: function convertDateToStringForDB(date) {
		return date ? this.getDate('y-m-d H:M:S', date) : null;
	},

	getCurrentTimestampForDB: function getCurrentTimestampForDB() {
		return this.getDate('y-m-d H:M:S', new Date());
	},

	// for showing date in ui, converts object to string
	convertDateToStringForUI: function convertDateToStringForUI(date) {
		return date ? this.getDate(DateConstant.indianFormat, date) : '';
	},

	// for converting date string to object while fetching from database
	convertStringToDateUsingDBFormat: function convertStringToDateUsingDBFormat(date) {
		var consideTime = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

		return date ? this.getDateObj(date, 'yyyy-mm-dd', '-', consideTime) : null;
	},

	// for converting date string to object while taking input from UI
	convertStringToDateUsingUIFormat: function convertStringToDateUsingUIFormat(date) {
		return date ? this.getDateObj(date, 'dd/mm/yyyy', '/') : null;
	},

	isSameDate: function isSameDate(lhs, rhs) {
		if (lhs.getFullYear() == rhs.getFullYear() && lhs.getMonth() == rhs.getMonth() && lhs.getDate() == rhs.getDate()) {
			return true;
		}
		return false;
	},

	compareOnlyDatePart: function compareOnlyDatePart(lhs, rhs) {
		if (this.isSameDate(lhs, rhs)) {
			return 0;
		} else if (Date.parse(lhs) < Date.parse(rhs)) {
			return -1;
		} else {
			return 1;
		}
	},

	addDays: function addDays(date, noOfDays) {
		return new Date(date.getTime() + 86400000 * noOfDays);
	},

	setTimeZero: function setTimeZero(date) {
		date.setHours(0, 0, 0, 0);
		return date;
	},
	getDiffInDays: function getDiffInDays(date1, date2) {
		return (this.setTimeZero(date1) - this.setTimeZero(date2)) / (1000 * 60 * 60 * 24);
	},

	getDateByMonthYearString: function getDateByMonthYearString(inputDate) {
		if (inputDate) {
			if (inputDate.length < 10) {
				inputDate = '01/' + inputDate;
			}
			inputDate = MyDate.getDateObj(inputDate, 'dd/MM/yyyy', '/');
		} else {
			inputDate = null;
		}
		return inputDate;
	},
	getThisMonthFromToDate: function getThisMonthFromToDate() {
		var date = moment();
		return [date.startOf('month').toDate(), date.endOf('month').toDate()];
	},
	getLastMonthFromToDate: function getLastMonthFromToDate() {
		var date = moment();
		var lastMonth = date.subtract(1, 'months');
		return [lastMonth.startOf('month').toDate(), lastMonth.endOf('month').toDate()];
	},
	getThisYearFromToDate: function getThisYearFromToDate() {
		var date = moment();
		return [date.startOf('year').toDate(), date.endOf('year').toDate()];
	},
	getThisQuarterFromToDate: function getThisQuarterFromToDate() {
		var date = moment();
		return [date.startOf('quarter').toDate(), date.endOf('quarter').toDate()];
	}
};

module.exports = MyDate;