var SqliteDBHelper, sqlitedbhelperObject;
var MyAnalytics = require('../Utilities/analyticsHelper.js');

var AnalyticsExceptionTracker = {
	logErrorToAnalytics: function logErrorToAnalytics(msgLabel, isHandled, categoryName, logUserDetails) {
		try {
			categoryName = categoryName || (isHandled ? 'Handled Javascript Errors' : 'Unhandled Javascript Errors');
			categoryName = 'Error::' + categoryName;

			if (typeof isProduction !== 'undefined') {
				if (isProduction) {
					categoryName += '::Production';
				} else {
					categoryName += '::Development';
				}
			} else {
				categoryName += '::Unknown';
			}

			var userId = '';
			if (logUserDetails) {
				try {
					if (!sqlitedbhelperObject) {
						if (!SqliteDBHelper) {
							SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
						}
						sqlitedbhelperObject = new SqliteDBHelper();
					}
					userId = sqlitedbhelperObject.generateUserId();
				} catch (err) {}
			}
			MyAnalytics.pushException(categoryName, '[[' + userId + ']]  ' + msgLabel);
		} catch (err) {}
	}
};

module.exports = AnalyticsExceptionTracker;