var CovidHelper = function () {
	var LocalStorageConstant = require('./../Constants/LocalStorageConstant.js');
	return {
		// This trial was added during Covid-19. So for all those users who got this extension will continue using it.
		isCovidTrialExtended: function isCovidTrialExtended() {
			try {
				return localStorage.getItem(LocalStorageConstant.COVID_TRIAL_EXTEND) === 'true';
			} catch (e) {}
			return false;
		}
	};
}();
module.exports = CovidHelper;