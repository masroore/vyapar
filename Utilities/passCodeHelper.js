/* eslint-disable new-cap */
var _require = require('electron'),
    ipcRenderer = _require.ipcRenderer,
    remote = _require.remote;

var app = remote.app;

var LicenseInfoConstant = require('./../Constants/LicenseInfoConstant.js');
var LocalStorageHelper = require('../Utilities/LocalStorageHelper');
var appPath = app.getPath('userData'); // This is in use in whole application
function forgotPassCode() {
	var PassCodeHelperUtility = require('../Utilities/passCodeHelperUtitlity');
	PassCodeHelperUtility.resetPassCode({
		passcodeType: 'login'
	});
}

function passcodeValidProceed() {
	var path1 = require('path');
	var fs = require('fs');
	var redirectionScreenCallback = function redirectionScreenCallback() {
		var path = path1.normalize(appPath + '/BusinessNames/currentDB.txt');
		if (fs.existsSync(path)) {
			var curDbTxtFile = fs.readFileSync(path).toString();
			if (curDbTxtFile && fs.existsSync(curDbTxtFile)) {
				ipcRenderer.send('changeURL', 'Index1.html');
				return;
			}
		}

		var CompanyListUtility = require('../Utilities/CompanyListUtility');
		var isCompanyListAvailable = CompanyListUtility.isCompanyListAvailable();
		if (isCompanyListAvailable) {
			ipcRenderer.send('changeURL', 'SwitchCompany.html');
		} else {
			ipcRenderer.send('changeURL', 'FTLogin.html');
		}
	};

	try {
		var businessNameFolderPath = appPath + '/BusinessNames';
		if (!fs.existsSync(businessNameFolderPath)) {
			fs.mkdir(businessNameFolderPath, function (err) {
				if (err) {
					var _logger = require('./logger');
					_logger.error(err);
				}
				redirectionScreenCallback();
			});
		} else {
			redirectionScreenCallback();
		}
	} catch (err) {}
}
ipcRenderer.on('launchEventsOnScreenFocus', function (event, data) {
	try {
		if (data) {
			passCodeHelper.login();
		}
	} catch (err) {
		logger.error(err);
	}
});

var passCodeHelper = {
	login: function login() {
		var _require2 = require('electron'),
		    ipcRenderer = _require2.ipcRenderer;

		var passCode = LocalStorageHelper.getValue(LicenseInfoConstant.passCode, true);
		var isPasscodeEnabled = !!(passCode && passCode.trim().length > 0);
		if (isPasscodeEnabled) {
			var MenuType = require('./../Constants/MenuType.js');
			var titleBarUtility = require('./../Utilities/CustomTitleBarUtility.js');
			titleBarUtility.createTitleBar({ menuType: MenuType.HELP_MENU });
			var passcodeInputDiv = $('#passcodeInputDiv');
			passcodeInputDiv.on('focus', 'input', function (event) {
				event.stopImmediatePropagation();
			});
			$('#reminderPassCodeDialog').show();
			$('.centreDiv').addClass('is-blurred');
			$('.headingNav').addClass('is-blurred');
			$('.sideNav').addClass('is-blurred');

			$('#1').val('');
			$('#passcodeSecondInput').val('');
			$('#b').val('');
			$('#c').val('');
			$('#passcodeError').text('');
			$('#1').focus();
			passcodeInputDiv.on('keyup', 'input', function () {
				var p1 = $('#1').val();
				var p2 = $('#passcodeSecondInput').val();
				var p3 = $('#b').val();
				var p4 = $('#c').val();
				var passCodeString = p1 + '' + p2 + '' + p3 + '' + p4;
				if (passCodeString.length == 4) {
					if (passCodeString === passCode) {
						app.hasUserPassCodeEntered = true;
						$('#reminderPassCodeDialog').hide();
						$('.centreDiv').removeClass('is-blurred');
						$('.headingNav').removeClass('is-blurred');
						$('.sideNav').removeClass('is-blurred');
						ipcRenderer.send('changeURL', 'Index1.html');
					} else {
						$('#passcodeError').text('Invalid Passcode. Please try again');
						$('#passcodeInputDiv input').val('');
						$('#1').focus();
					}
				}
			});
			$('#forgotPasscode').off().on('click', forgotPassCode);
		} else {
			ipcRenderer.send('changeURL', 'Index1.html');
		}
	},
	passCodeCheck: function passCodeCheck() {
		var passCode = LocalStorageHelper.getValue(LicenseInfoConstant.passCode, true);
		var isPasscodeEnabled = !!(passCode && passCode.trim().length > 0);
		if (isPasscodeEnabled && !app.hasUserPassCodeEntered) {
			$('#dialog').show();
			$('#1').focus();
			$('#passcodeInputDiv').on('keyup', 'input', function () {
				var p1 = $('#1').val();
				var p2 = $('#passcodeSecondInput').val();
				var p3 = $('#b').val();
				var p4 = $('#c').val();
				var passCodeString = p1 + '' + p2 + '' + p3 + '' + p4;
				if (passCodeString.length == 4) {
					if (passCodeString === passCode) {
						app.hasUserPassCodeEntered = true;
						passcodeValidProceed();
					} else {
						$('#passcodeError').text('Invalid Passcode. Please try again');
						$('#passcodeInputDiv input').val('');
						$('#1').focus();
					}
				}
			});
			$('#forgotPasscode').off('click').on('click', forgotPassCode);
		} else {
			app.hasUserPassCodeEntered = true;
			passcodeValidProceed();
		}
	}
};

module.exports = passCodeHelper;