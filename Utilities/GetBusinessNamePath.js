var GetBusinessNamePath = function GetBusinessNamePath() {
	var app = require('electron').remote.app;

	var appPath = app.getPath('userData');
	appPath = appPath + '/BusinessNames';
	this.getDirectory = function () {
		return appPath;
	};
};
module.exports = new GetBusinessNamePath();