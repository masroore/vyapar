var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();
var DecimalInputFilter = function DecimalInputFilter() {
	var inputTextFilter = function inputTextFilter(object, regEx) {
		if (object.value) {
			objectStringValue = '' + object.value; // convert to string
			if (objectStringValue.search(regEx) < 0) {
				object.value = object.oldValue ? object.oldValue + '' : '';
			}
			object.oldValue = '' + object.value;
		} else {
			object.oldValue = '';
		}
		return object.value;
	};
	var inputNumberTextFilter = function inputNumberTextFilter(object, digitsBeforeDecimal, digitsAfterDecimal, allowNegative, allowCap) {
		var re = '^' + (allowNegative ? '[-+]?' : '') + '[0-9]{0,' + digitsBeforeDecimal + '}(\\.[0-9]{0,' + digitsAfterDecimal + '})?$';
		return inputTextFilter(object, re);
	};
	this.inputTextFilterForAmount = function (object, allowNegative) {
		var decimal = settingCache.getAmountDecimal();
		return inputNumberTextFilter(object, 10, decimal, allowNegative);
	};

	this.inputTextFilterForQuantity = function (object, allowNegative) {
		var decimal = settingCache.getQuantityDecimal();
		return inputNumberTextFilter(object, 10, decimal, allowNegative);
	};

	this.inputTextFilterForPercentage = function (object, allowNegative) {
		return inputNumberTextFilter(object, 3, 3, null);
	};

	this.inputTextFilterForInvoice = function (object) {
		var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

		var retValue = '';
		if (type === TxnTypeConstant.TXN_TYPE_PURCHASE || type === TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
			return;
		}
		if (object) {
			retValue = inputTextFilter(object, '^[0-9]{0,15}$');
		}
		return retValue;
	};

	this.inputTextFilterForLength = function (object, length, allowEnterKey) {
		if (allowEnterKey) {
			return inputTextFilter(object, '^(.|\n){0,' + (length || '15') + '}$');
		} else {
			return inputTextFilter(object, '^.{0,' + (length || '15') + '}$');
		}
	};

	this.inputTextFilterForAlphanumeric = function (object) {
		inputTextFilter(object, /^[a-z0-9]+$/i);
	};

	this.inputTextFilterForOnlyInteger = function (object) {
		return inputTextFilter(object, /^[0-9]+$/i);
	};

	this.inputTextFilterForDate = function (object) {
		inputTextFilter(object, '^[0-9\/]{0,10}$');
	};

	this.inputTextFilterForPhone = function (object) {
		var re = '^\\+?[0-9., ()-]{0,30}$';
		var objectStringValue = '' + object.value; // convert to string
		if (objectStringValue.search(re) < 0) {
			object.value = object.oldValue ? object.oldValue + '' : '';
		}
		object.oldValue = '' + object.value;
		return object.value;
	};

	this.inputTextFilterForNumber = function (object, length) {
		var re = '^[-() 0-9]{0,' + (length || '20') + '}$';
		var objectStringValue = '' + object.value; // convert to string
		if (objectStringValue.search(re) < 0) {
			object.value = object.oldValue ? object.oldValue + '' : '';
		}
		object.oldValue = '' + object.value;
	};
};

module.exports = new DecimalInputFilter();