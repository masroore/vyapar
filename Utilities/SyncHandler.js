var syncSwitch = null;
var isAdmin = false;
var SyncHandler = {
	isSyncEnabled: function isSyncEnabled() {
		if (syncSwitch == null) {
			var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
			var sqliteDBHelper = new SqliteDBHelper();
			syncSwitch = sqliteDBHelper.isSyncEnabled();
		}
		return syncSwitch;
	},

	refreshSyncEnableValue: function refreshSyncEnableValue() {
		syncSwitch = null;
	},

	isAdminUser: function isAdminUser() {
		if (!SyncHandler.isSyncEnabled() || isAdmin == true) {
			return true;
		}
		return false;
	},

	setAdminUserValue: function setAdminUserValue(isUserAdmin) {
		isAdmin = isUserAdmin;
	},
	userOnlineForSync: function userOnlineForSync() {
		if (this.isSyncEnabled()) {
			setTimeout(function () {
				var isOnline = require('is-online');

				isOnline().then(function (online) {
					if (!online) {
						$('#dialogForUserOffline').dialog('open');
					}
				});
			}, 500);
		}
	}
};

module.exports = SyncHandler;