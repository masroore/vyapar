var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var electron = require('electron');

var _ref = electron.remote ? electron.remote : electron,
    app = _ref.app;

var appPath = app.getPath('userData');

var CompanyListUtility = {
	isCompanyListAvailable: function isCompanyListAvailable() {
		try {
			var companyListJSON = this.getCompaniesFileNameObject();
			return !$.isEmptyObject(companyListJSON);
		} catch (ex) {
			return false;
		}
	},

	getCompaniesFileNameObject: function getCompaniesFileNameObject() {
		var fs = require('fs');
		var companyListFilePath = appPath + '/BusinessNames/CompanyList.json';
		var oldCompanyListFilePath = appPath + '/BusinessNames/FileNames.json';
		var companyListObj = {};

		if (fs.existsSync(companyListFilePath)) {
			var jsonData = fs.readFileSync(companyListFilePath, 'utf8');
			try {
				companyListObj = JSON.parse(jsonData);
			} catch (error) {
				var logger = require('../Utilities/logger');
				logger.error(new Error('Company file is empty'));
				companyListObj = {};
			}
		} else if (fs.existsSync(oldCompanyListFilePath)) {
			var oldJSONData = fs.readFileSync(oldCompanyListFilePath, 'utf8');
			var oldCompanyListFilePathForTemp = appPath + '/BusinessNames/FileNamesTemp.json';
			fs.writeFileSync(oldCompanyListFilePathForTemp, oldJSONData);
			var oldCompanyListObj = JSON.parse(oldJSONData);
			for (var i in oldCompanyListObj) {
				var companyDisplayName = i;
				var companyPath = oldCompanyListObj[i];
				var companyType = 0; // 0--> Normal company, 1--> Sync join company
				var lastNotificationSentAt = new Date().getTime();
				companyListObj[companyDisplayName] = { 'path': companyPath, 'company_creation_type': companyType, 'lastNotificationSentAt': lastNotificationSentAt };
			}
			var jsonToInsert = (0, _stringify2.default)(companyListObj);
			if (jsonToInsert) {
				fs.writeFileSync(companyListFilePath, jsonToInsert);
			} else {
				var _logger = require('../Utilities/logger');
				_logger.error(new Error('Most urgent. Company file is getting emptied'));
			}
			fs.unlinkSync(oldCompanyListFilePath);
		} else {
			var _jsonToInsert = (0, _stringify2.default)(companyListObj);
			if (_jsonToInsert) {
				fs.writeFileSync(companyListFilePath, _jsonToInsert);
			} else {
				var _logger2 = require('../Utilities/logger');
				_logger2.error(new Error('Most urgent. Company file is getting emptied'));
			}
		}
		return companyListObj;
	},

	updateCompanyListFile: function updateCompanyListFile(jsonToWrite) {
		var fs = require('fs');
		var companyListFilePath = appPath + '/BusinessNames/CompanyList.json';
		if (jsonToWrite && typeof jsonToWrite != 'string') {
			jsonToWrite = (0, _stringify2.default)(jsonToWrite);
		}
		if (jsonToWrite) {
			fs.writeFileSync(companyListFilePath, jsonToWrite);
		} else {
			try {
				var logger = require('../Utilities/logger');
				logger.error(new Error('Most urgent. Company file is getting emptied'));
			} catch (e) {}
		}
	},

	getCurrentCompanyKey: function getCurrentCompanyKey() {
		var app = require('electron').remote.app;

		var appPath = app.getPath('userData');
		var fs = require('fs');
		var companyDisplayName = null;
		var currentDBFile = appPath + '/BusinessNames/currentDB.txt';

		if (fs.existsSync(currentDBFile)) {
			var currentDB = fs.readFileSync(currentDBFile, 'utf8');
			var currentDBSplit = currentDB.split('/');
			var currentDBFileName = currentDBSplit[currentDBSplit.length - 1];
			currentDBFileName = '/' + currentDBFileName;

			var fileNameObj = this.getCompaniesFileNameObject();

			if ((0, _keys2.default)(fileNameObj).length) {
				for (var prop in fileNameObj) {
					if (fileNameObj.hasOwnProperty(prop)) {
						if (fileNameObj[prop] && fileNameObj[prop].path.endsWith(currentDBFileName)) {
							companyDisplayName = prop;
							break;
						}
					}
				}
			}
		}
		return companyDisplayName;
	},

	updateDraftJsonFile: function updateDraftJsonFile(oldCompName, newCompName) {
		var app = require('electron').remote.app;

		var appPath = app.getPath('userData');
		var fs = require('fs');
		var draftFile = appPath + '/BusinessNames/draft-messages.json';
		if (fs.existsSync(draftFile)) {
			var draftObject = JSON.parse(fs.readFileSync(draftFile, 'utf8'));
			var objToChange = draftObject[oldCompName];
			delete draftObject[oldCompName];
			draftObject[newCompName] = objToChange;
			var jsonToInsert = (0, _stringify2.default)(draftObject);
			fs.writeFileSync(draftFile, jsonToInsert);
		}
	},

	setValueForCurrentCompany: function setValueForCurrentCompany(key, value) {
		var companyKey = CompanyListUtility.getCurrentCompanyKey();
		if (companyKey) {
			var fileNameObj = CompanyListUtility.getCompaniesFileNameObject();
			if (fileNameObj) {
				var companyObj = fileNameObj[companyKey];
				if (companyObj) {
					companyObj[key] = value;
					CompanyListUtility.updateCompanyListFile(fileNameObj);
				}
			}
		}
	},

	getValueForCurrentCompany: function getValueForCurrentCompany(key) {
		var companyKey = CompanyListUtility.getCurrentCompanyKey();
		if (companyKey) {
			var fileNameObj = CompanyListUtility.getCompaniesFileNameObject();
			if (fileNameObj) {
				var companyObj = fileNameObj[companyKey];
				if (companyObj) {
					var value = companyObj[key];
					if (value) {
						return value;
					}
				}
			}
		}
		return '';
	},

	updateJSONFileForCompanies: function updateJSONFileForCompanies(oldCompName, newCompName) {
		var fileNameObj = this.getCompaniesFileNameObject();

		if ((0, _keys2.default)(fileNameObj).includes(newCompName)) {
			ToastHelper.error('Display name already exists');
			return false;
		}

		var dbToChange = fileNameObj[oldCompName];
		delete fileNameObj[oldCompName];
		fileNameObj[newCompName] = dbToChange;
		var jsonToInsert = (0, _stringify2.default)(fileNameObj);
		jsonToInsert = jsonToInsert.replace(/\\/g, '/');
		this.updateCompanyListFile(jsonToInsert);
		this.updateDraftJsonFile(oldCompName, newCompName);

		return true;
	}
};

module.exports = CompanyListUtility;