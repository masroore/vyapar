var LicenseUtility = require('../Utilities/LicenseUtility');
var LicenseInfoConstant = require('../Constants/LicenseInfoConstant.js');
var CommonUtility = {
	loadDefaultPage: function loadDefaultPage(url, callback) {
		if (!canCloseDialogue || canCloseDialogue()) {
			var defaultPage = $('#defaultPage');
			var frameDiv = $('#frameDiv');
			var modelContainer = $('#modelContainer');
			defaultPage.show();
			try {
				unmountReactComponent(document.querySelector('#salePurchaseContainer'));
				unmountReactComponent(document.querySelector('#frameDiv'));
			} catch (ex) {}
			frameDiv.empty();
			defaultPage.load(url, function () {
				callback && callback();
			});
			modelContainer.hide();
		}
	},
	MountDefaultPage: function MountDefaultPage(component, props) {
		if (!canCloseDialogue || canCloseDialogue()) {
			var defaultPage = $('#defaultPage');
			unmountReactComponent(document.querySelector('#salePurchaseContainer'));
			CommonUtility.hideFramediv();
			var mountComponent = require('../UIComponent/jsx/MountComponent').default;
			defaultPage.show();
			mountComponent(component, document.getElementById('defaultPage'), props);
		}
	},
	showLoader: function showLoader(callback) {
		var hideOnCompletion = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

		var loader = $('#loading');
		loader.show(function () {
			if (callback && typeof callback === 'function') {
				try {
					callback();
				} catch (ex) {
					var logger = require('../Utilities/logger');
					logger.error('Error occurred in loader...' + ex);
					loader.hide();
				} finally {
					if (hideOnCompletion) {
						loader.hide();
					}
				}
			}
		});
	},
	hideFramediv: function hideFrameDiv() {
		$('#defaultPage').show();
		try {
			unmountReactComponent(document.querySelector('#frameDiv'));
		} catch (ex) {}
		$('#frameDiv').empty();
		$('#modelContainer').css({
			display: 'none'
		});
	},
	loadFrameDiv: function loadFrameDiv(url, callback) {
		callback = typeof callback === 'function' ? callback : undefined;
		try {
			unmountReactComponent(document.querySelector('#frameDiv'));
		} catch (ex) {}
		$('#modelContainer').css('display', 'block');
		$('.viewItems').css('display', 'none');
		$('#frameDiv').load(url, callback);
		$('#defaultPage').hide();
	},
	generateNewGUID: function generateNewGUID() {
		var length = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 16;

		var guid = '';
		var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

		for (var i = 0; i < length; i++) {
			guid += possible.charAt(Math.floor(Math.random() * possible.length));
		}

		return guid;
	},
	highlightAndShowCurrentTab: function highlightAndShowCurrentTab(evt, divId) {
		$('.tablinks').removeClass('bottomLine');
		$('#' + divId).show();
		$(evt.currentTarget).addClass('bottomLine');
	},
	showLicenseExpireDialogIfNeeded: function showLicenseExpireDialogIfNeeded() {
		var licenseInfoDetails = LicenseUtility.getLicenseInfo();
		var isLicenseValid = licenseInfoDetails[LicenseInfoConstant.isLicenseValid];
		if (!isLicenseValid) {
			$('#dialogForLicenseExpire').dialog('open');
			return false;
		}
		return true;
	},
	getSanitizedPhoneNumber: function getSanitizedPhoneNumber(phoneNumber) {
		if (!phoneNumber) return;
		var settingCache = new SettingCache();
		phoneNumber = (phoneNumber || '').replace(/[^0-9]/g, '');
		if (settingCache.isCurrentCountryIndia()) {
			return '91' + phoneNumber.slice(-10);
		}
		return phoneNumber;
	}
};

module.exports = CommonUtility;