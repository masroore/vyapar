var titleBarUtility = function () {
	// This module should be required from BrowserWindow as it needs window object to add titlebar
	var customTitlebar = require('custom-electron-titlebar');

	var _require = require('./WindowMenuUtility'),
	    buildAppMenu = _require.buildAppMenu;

	function addCustomerCareDiv() {
		var customerCareDiv = document.createElement('div');
		customerCareDiv.setAttribute('class', 'window-customercare');
		var windowTitle = document.querySelector('.titlebar .window-title');
		windowTitle && windowTitle.insertAdjacentElement('afterend', customerCareDiv);
		var customerCareDivHTML = '\n\t\t\t<span>Customer Support :</span>\n\t\t\t<div class="phonesupportDiv">\n\t\t\t\t<span class=\'logo phonesupport\'>\n\t\t\t\t</span>\n\t\t\t\t<span class=\'contactnumber\'>080-46268888</span>\n\t\t\t</div>\n\t\t\t<span class="divider"></span>\n\t\t\t<div class="whatsappDiv">\n\t\t\t\t<span class=\'logo whatsappsupport\'>\n\t\t\t\t</span>\n\t\t\t\t<span class=\'contactnumber\'>+91 6366827420</span>\n\t\t\t</div>\n\t\t';
		$('.window-customercare').html(customerCareDivHTML);
		$('.window-customercare .whatsappDiv').off('click').on('click', openWhatsApp);
	}
	function styleReloadMenuBtn() {
		$('.menubar-menu-title:contains("Reload")').html('↻').css({
			'font-size': '16px',
			'font-weight': '600',
			'transform': 'rotate(80deg)'
		});
	}
	function openWhatsApp() {
		var send = require('./whatsapp').default;
		var phoneNumber = '916366827420'; // Customer Care Whatsapp
		var messageDetails = {
			to: phoneNumber
		};
		send(messageDetails);
	}

	function unregisterBrowserWindowEvents() {
		var BrowserWindow = require('electron').remote.BrowserWindow;

		var mainWindow = BrowserWindow.getAllWindows()[0];
		if (mainWindow) {
			mainWindow.removeAllListeners('focus');
			mainWindow.removeAllListeners('blur');
			mainWindow.removeAllListeners('maximize');
			mainWindow.removeAllListeners('unmaximize');
			mainWindow.removeAllListeners('enter-full-screen');
			mainWindow.removeAllListeners('leave-full-screen');
		}
	}
	var _titlebar = void 0;
	var titlebar = {
		createTitleBar: function createTitleBar(options) {
			try {
				titlebar.disposeTitleBar();
				var menu = buildAppMenu(options);
				try {
					_titlebar = new customTitlebar.Titlebar({
						backgroundColor: customTitlebar.Color.fromHex('#F7F8FB'),
						icon: '../inlineSVG/vyapar_logo_without_background.png',
						iconsTheme: customTitlebar.Themebar.win,
						minimizable: true,
						shadow: true,
						menu: menu
					});
				} catch (e) {
					_titlebar = new customTitlebar.Titlebar({
						backgroundColor: customTitlebar.Color.fromHex('#F7F8FB'),
						icon: '../inlineSVG/vyapar_logo_no_background.ico',
						iconsTheme: customTitlebar.Themebar.win,
						minimizable: true,
						shadow: true,
						menu: menu
					});
				}
				var themeBar = new customTitlebar.Themebar();
				themeBar.registerTheme(function (collector) {
					collector.addRule('\n\t\t\t\t@font-face {\n\t\t\t\t\tfont-family: roboto;\n\t\t\t\t\tsrc: url(\'./../styles/roboto/Roboto-Regular.woff\');\n\t\t\t\t}\n\t\t\t\t.titlebar {\n\t\t\t\t\tfont-family: \'roboto\'\n\t\t\t\t}\n\t\t\t\t.menubar-menu-container {\n\t\t\t\t\twidth:15rem;\n\t\t\t\t\tbox-shadow:none;\n\t\t\t\t}\n\t\t\t\t.titlebar .window-title {\n\t\t\t\t\tdisplay: none;\n\t\t\t\t}\n\t\t\t\t.titlebar .window-customercare {\n\t\t\t\t\tdisplay: flex;\n\t\t\t\t\tflex: 0 1 auto;\n\t\t\t\t\tfont-size: 12px;\n\t\t\t\t\toverflow: hidden;\n\t\t\t\t\twhite-space: nowrap;\n\t\t\t\t\ttext-overflow: ellipsis;\n\t\t\t\t\tcolor: #1A1A1A;\n\t\t\t\t\tzoom: 1;\n\t\t\t\t\t-webkit-app-region: no-drag;\n\t\t\t\t\tmargin: 0 auto;\n\t\t\t\t\tcursor: default;\n\t\t\t\t}\n\t\t\t\t.titlebar .window-customercare .whatsappDiv:hover {\n\t\t\t\t\tbackground-color: rgba(0, 0, 0, .1);\n\t\t\t\t}\n\t\t\t\t.titlebar .window-customercare .phonesupportDiv {\n\t\t\t\t\tmargin-left: 4px;\n\t\t\t\t}\n\t\t\t\t.titlebar .window-customercare .logo {\n\t\t\t\t\theight: 20px;\n\t\t\t\t\twidth: 20px;\n\t\t\t\t\tbackground-repeat: no-repeat;\n\t\t\t\t\tbackground-position: center center;\n\t\t\t\t\tbackground-size: 16px;\n\t\t\t\t\tdisplay: inline-block;\n\t\t\t\t\tvertical-align: middle;\n\t\t\t\t}\n\t\t\t\t.titlebar .window-customercare .divider {\n\t\t\t\t\tborder: 1px solid #1A1A1A;\n\t\t\t\t\tmargin: 4px;\n\t\t\t\t\topacity: 0.3;\n\t\t\t\t}\n\t\t\t\t.titlebar .window-customercare .whatsappsupport {\n\t\t\t\t\tbackground-image: url(../inlineSVG/sendReminderWhatsApp.svg);\n\t\t\t\t}\n\t\t\t\t.titlebar .window-customercare .contactnumber {\n\t\t\t\t\tcolor: #1789FC;\n\t\t\t\t}\n\t\t\t\t.titlebar .window-customercare .phonesupport {\n\t\t\t\t\tbackground-image: url(../inlineSVG/phone-call.svg);\n\t\t\t\t}\n\t\t\t\t.container-after-titlebar {\n\t\t\t\t\toverflow: hidden !important;\n\t\t\t\t}\n\t\t\t\t');
				});
				themeBar.registerTheme(customTitlebar.Themebar.win);
				addCustomerCareDiv();
				styleReloadMenuBtn();
			} catch (err) {
				var logger = require('./logger');
				logger.error(err);
			}
		},
		updateMenu: function updateMenu(menu) {
			try {
				if (menu && _titlebar) {
					_titlebar.updateMenu(menu);
				}
			} catch (err) {
				var logger = require('./logger');
				logger.error(err);
			}
		},
		disposeTitleBar: function disposeTitleBar() {
			try {
				// unregisterBrowserWindowEvents();
				_titlebar && _titlebar.dispose();
				return true;
			} catch (err) {
				var logger = require('./logger');
				logger.error(err);
			}
		}
	};
	if (window) {
		window.titleBarUtility = titlebar;
	}
	return titlebar;
}(window);
module.exports = titleBarUtility;