var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var authWindow = false;

var SyncHelper = {
	loginToGetAuthToken: function loginToGetAuthToken(successCallBack, failCallBack) {
		var workflow = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 4;

		var authSuccess = false;
		var os = require('os');
		var Domain = require('./../Constants/Domain');
		var domain = Domain.thisDomain;
		var emailforlogin = '';
		if (!isCompanyListView) {
			var FirmCache = require('./../Cache/FirmCache');
			var firmCache = new FirmCache();
			if (firmCache.getDefaultFirm() == null) {
				emailforlogin = '';
			} else {
				emailforlogin = firmCache.getDefaultFirm().firmEmail;
			}
		}

		var url = domain + '/home?client_type=2&workflow=' + workflow + '&email_id_=' + emailforlogin;
		var rem = require('electron');
		var _rem$remote = rem.remote,
		    BrowserWindow = _rem$remote.BrowserWindow,
		    dialog = _rem$remote.dialog,
		    shell = _rem$remote.shell;


		if (authWindow) {
			return;
		}
		var sync_auth_window = new BrowserWindow({
			show: false,
			minimizable: false,
			alwaysOnTop: true,
			skipTaskbar: true,
			autoHideMenuBar: true,
			webPreferences: { nodeIntegration: false }
		});

		sync_auth_window.on('closed', function (e) {
			sync_auth_window = null;
			authWindow = false;
			if (failCallBack && authSuccess == false) {
				failCallBack();
			}
		});
		sync_auth_window.webContents.session.clearStorageData({ origin: domain, storages: ['cookies'] });
		sync_auth_window.loadURL(url); // url
		sync_auth_window.show();
		authWindow = true;
		sync_auth_window.on('page-title-updated', function () {
			var title = sync_auth_window.getTitle();
			try {
				var jsonData = JSON.parse(title);
				authSuccess = true;
				sync_auth_window.close();

				try {
					if (jsonData.hasOwnProperty('auth_token')) {
						var TokenForSync = require('./../Utilities/TokenForSync');
						TokenForSync.writeToken((0, _stringify2.default)(jsonData));

						// token = jsonData.auth_token;

						if (successCallBack) {
							successCallBack(jsonData);
						}
					} else {
						if (failCallBack) {
							failCallBack();
						}
					}
				} catch (err) {
					if (failCallBack) {
						failCallBack();
					}
				}
			} catch (e) {
				return false;
			}
		});
	}
};

module.exports = SyncHelper;