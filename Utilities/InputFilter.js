var InputFilter = {
	getInputFilter: function getInputFilter(digitsBeforeDecimal, digitsAfterDecimal) {
		if (!digitsBeforeDecimal) {
			digitsBeforeDecimal = 10;
		}
		if (!digitsAfterDecimal) {
			digitsAfterDecimal = 2;
		}
		return '[-+]?[0-9]{0,' + (digitsBeforeDecimal - 1) + '}+((\\.[0-9]{0,' + (digitsBeforeDecimal - 1) + '})?)||(\\.)?';
	},

	getAmountInputFilter: function getAmountInputFilter() {
		var digitsAfterDecimal = settingCache.getAmountDecimal();
		return getInputFilter(10, digitsAfterDecimal);
	},

	getPercentageInputFilter: function getPercentageInputFilter() {
		return getInputFilter(3, 2);
	},

	getQuantityInputFilter: function getQuantityInputFilter() {
		var digitsAfterDecimal = settingCache.getQuantityDecimal();
		return getInputFilter(10, digitsAfterDecimal);
	}

};

module.exports = InputFilter;