var logger = require('../Utilities/logger');
var rem = require('electron');
var _rem$remote = rem.remote,
    BrowserWindow = _rem$remote.BrowserWindow,
    dialog = _rem$remote.dialog;

var printWin;
var fs = require('fs');
var path = require('path');

var PDFHandler = function PDFHandler() {
	this.closeWindow = function () {
		if (printWin && !printWin.isDestroyed()) {
			printWin.close();
		}
	};

	function printFrame(frameWindow, content) {
		// Print the selected window/iframe
		var def = $.Deferred();
		try {
			frameWindow = frameWindow.contentWindow || frameWindow.contentDocument || frameWindow;
			var wdoc = frameWindow.document || frameWindow.contentDocument || frameWindow;

			wdoc.write(content);
			wdoc.close();
			var callPrint = function callPrint() {
				// Fix for IE : Allow it to render the iframe
				frameWindow.focus();
				try {
					// Fix for IE11 - printng the whole page instead of the iframe content
					if (!frameWindow.document.execCommand('print', false, null)) {
						// document.execCommand returns false if it failed
						frameWindow.print();
					}
					// focus body as it is losing focus in iPad and content not getting printed
					$('body').focus();
				} catch (e) {
					frameWindow.print();
				}
				frameWindow.close();
				def.resolve();
			};
			// Print once the frame window loads - seems to work for the new-window option but unreliable for the iframe
			// $(frameWindow).on("load", callPrint);
			// Fallback to printing directly if the frame doesn't fire the load event for whatever reason
			setTimeout(callPrint, 750);
		} catch (err) {
			def.reject(err);
		}
		return def;
	}

	this.print = function (html) {
		var $iframe = $('<iframe height="0" width="0" border="0" wmode="Opaque"/>').prependTo('body').css({
			'position': 'absolute',
			'top': -999,
			'left': -999
		});

		var frameWindow = $iframe.get(0);
		printFrame(frameWindow, html).done(function () {
			// Success
			setTimeout(function () {
				$iframe.remove();
			}, 1000);
		}).fail(function (err) {
			// Use the pop-up method if iframe fails for some reason
			logger.error('Failed to print from iframe', err);
		});
	};

	this.generateHiddenBrowserWindowForPDF = function (html) {
		var fileName = 'print.html';
		fileName = appPath + '//' + fileName;
		fileName = path.normalize(fileName);
		fs.writeFileSync(fileName, html);
		if (!printWin || printWin.isDestroyed()) {
			printWin = new BrowserWindow({
				show: false
			});
		}
		printWin.loadURL(fileName);
	};

	function getPDFPrintSettings() {
		var SettingCache = require('../Cache/SettingCache');
		var settingCache = new SettingCache();
		var paperSizeArray = ['A4', 'A5'];
		var option = {
			landscape: false,
			marginsType: 0,
			printBackground: false,
			printSelectionOnly: false,
			pageSize: paperSizeArray[settingCache.getPrintPaperSize() - 1]
		};
		return option;
	}

	this.savePDF = function (fileObject) {
		var fileUtil = require('./FileUtil.js');
		var fileName = fileUtil.getFileName(fileObject);
		dialog.showSaveDialog(rem.remote.getCurrentWindow(), {
			title: 'Save pdf',
			defaultPath: fileName,
			filters: [{ name: 'Pdf File', extensions: ['pdf'] }]
		}, function (filePath) {
			$('#loading').show(function () {
				if (filePath) {
					printWin.webContents.printToPDF(getPDFPrintSettings(), function (err, data) {
						if (err) {
							ToastHelper.error('Something went wrong while saving the file. Please try again. If problem persists, please contact our customer care support team.');
							$('#loading').hide();
							return;
						}
						try {
							fs.writeFileSync(filePath, data);
						} catch (err) {
							ToastHelper.error('Pdf could not be saved due to following error: ' + err + ' .Please contact our tech support for help.');
						}

						$('#loading').hide();

						ToastHelper.success('Pdf saved successfully');
						fileUtil.showFileInFolder(filePath);
						try {
							window.closePreview();
						} catch (err) {}
					});
				} else {
					try {
						$('#loading').hide();
					} catch (err) {}
				}
			});
		});
	};

	this.openPDF = function (html, isWindowCreated) {
		var MyDate = require('../Utilities/MyDate');
		var currentObject = this;
		if (!isWindowCreated) {
			currentObject.generateHiddenBrowserWindowForPDF(html);
		}
		var fileName = 'vyapar_print' + MyDate.getDate('d_m_y_H_M_S');
		if (!fileName.endsWith('.pdf')) {
			fileName = fileName + '.pdf';
		}
		fileName = appPath + '//' + fileName;
		fileName = path.normalize(fileName);

		if (!isWindowCreated) {
			printWin.webContents.on('did-finish-load', function () {
				printWin.webContents.printToPDF(getPDFPrintSettings(), function (err, data) {
					if (err) {
						ToastHelper.error('Something went wrong while opening the file. Please try again. If problem persists, please contact our customer care support team.');
						$('#loading').hide();
						return;
					}
					fs.writeFileSync(fileName, data);
					try {
						currentObject.closeWindow();
					} catch (err) {}
					var pdfPath = path.normalize(fileName);

					var _require = require('electron'),
					    shell = _require.shell;

					shell.openItem(pdfPath);
					try {
						$('#loading').hide();
					} catch (err) {}
				});
			});
		} else {
			printWin.webContents.printToPDF(getPDFPrintSettings(), function (err, data) {
				if (err) {
					ToastHelper.error('Something went wrong while printing the file. Please try again. If problem persists, please contact our customer care support team.');
					$('#loading').hide();
					return;
				}
				fs.writeFileSync(fileName, data);
				var pdfPath = path.normalize(fileName);

				var _require2 = require('electron'),
				    shell = _require2.shell;

				shell.openItem(pdfPath);
				try {
					$('#loading').hide();
				} catch (err) {}
			});
		}
	};

	this.getPDFPath = function (html, txnName, callback) {
		var MyDate = require('../Utilities/MyDate');
		var currentObject = this;
		currentObject.generateHiddenBrowserWindowForPDF(html);
		var fileName = '';
		fileName = txnName + MyDate.getDate('d_m_y_H_M_S');

		if (!fileName.endsWith('.pdf')) {
			fileName = fileName + '.pdf';
		}
		fileName = appPath + '//' + fileName;
		fileName = path.normalize(fileName);

		printWin.webContents.on('did-finish-load', function () {
			printWin.webContents.printToPDF(getPDFPrintSettings(), function (err, data) {
				if (err) {
					fileName = '';
				} else {
					fs.writeFileSync(fileName, data);
				}
				try {
					currentObject.closeWindow();
				} catch (err) {}
				callback(fileName);
			});
		});
	};

	this.sharePDF = function (receiverId, fileName, subject, message) {
		var type = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 'report';

		var MyDate = require('../Utilities/MyDate');
		if (!fileName) {
			fileName = 'vyapar_print' + MyDate.getDate('d_m_y_H_M_S');
		}
		if (!fileName.endsWith('.pdf')) {
			fileName = fileName + '.pdf';
		}
		if (!subject) {
			subject = 'Vyapar PDF';
		}
		if (!message) {
			message = 'Hello Sir,\n\nPlease find the attached PDF.\n\nThanks and regards.';
		}

		var fileNameForSharing = fileName;
		fileName = appPath + '//' + fileName;
		fileName = path.normalize(fileName);

		printWin.webContents.printToPDF(getPDFPrintSettings(), function (err, data) {
			if (err) {
				logger.error(err);
				return;
			}
			fs.writeFileSync(fileName, data);
			var GoogleMail = require('./../BizLogic/GoogleMail.js');
			var googleMail = new GoogleMail();
			if (receiverId) {
				$('#receiverEmailForPDF').val(receiverId);
			} else {
				$('#receiverEmailForPDF').val('');
			}
			if (subject) {
				$('#emailSubjectForPDF').val(subject);
			} else {
				$('#emailSubjectForPDF').val('');
			}
			if (message) {
				$('#emailBodyForPDF').val(message);
			} else {
				$('#emailBodyForPDF').val('');
			}
			$('#userEmailForPDF').removeClass('hide');
			$('#loading').hide();
			$('#userEmailForPDF').dialog({
				closeOnEscape: true,
				draggable: true,
				modal: true
			});
			$('#receiverIdCancelForPDF').off('click').on('click', function () {
				$('#userEmailForPDF').addClass('hide');
				$('#userEmailForPDF').dialog('close').dialog('destroy');
			});

			$('#receiverIdSubmitForPDF').off('click').on('click', function () {
				var receiverId = $('#receiverEmailForPDF').val();
				var subjectToSend = $('#emailSubjectForPDF').val();
				var messageToSend = $('#emailBodyForPDF').val();
				if (/(.+)@(.+){2,}\.(.+){2,}/.test(receiverId)) {
					googleMail.sendMail({
						fileNames: fileName,
						receiverId: receiverId,
						alertUser: true,
						subject: subjectToSend,
						message: messageToSend,
						attachFileNames: fileNameForSharing,
						eventLog: type
					});
					$('#userEmailForPDF').addClass('hide');
					$('#userEmailForPDF').dialog('close').dialog('destroy');
					// ToastHelper.success('Pdf shared successffully');
				} else {
					$('#userEmailErrorForPDF').css('display', 'block');
				}
			});
			$('#viewPdfBeforeMailForReport').off().on('click', function () {
				var path = require('path');
				var pdfPath = path.normalize(fileName);

				var _require3 = require('electron'),
				    shell = _require3.shell;

				shell.openItem(pdfPath);
			});
		});
	};
};

module.exports = PDFHandler;