var LocalStorageHelper = require('../Utilities/LocalStorageHelper');
var LicenseInfoConstant = require('../Constants/LicenseInfoConstant');
var LocalStorageConstant = require('../Constants/LocalStorageConstant');
var ErrorCode = require('./../Constants/ErrorCode.js');
var PassCodeHelperUtility = {
	validateOldPasscode: function validateOldPasscode(oldPasscode) {
		var statusCode = ErrorCode.ERROR_OLD_PASSCODE_INCORRECT;
		var existingOldPasscode = LocalStorageHelper.getValue(LicenseInfoConstant.passCode, true);
		if (oldPasscode && oldPasscode.length == 4 && existingOldPasscode === oldPasscode) {
			statusCode = ErrorCode.ERROR_OLD_PASSCODE_CORRECT;
		}
		return statusCode;
	},

	removePasscode: function removePasscode() {
		var statusCode = ErrorCode.ERROR_PASSCODE_UPDATE_FAILED;
		LocalStorageHelper.setValue(LicenseInfoConstant.passCode, '');
		statusCode = ErrorCode.ERROR_PASSCODE_UPDATE_SUCCESS;
		return statusCode;
	},

	updatePassCode: function updatePassCode(newPassCode) {
		var statusCode = ErrorCode.ERROR_PASSCODE_UPDATE_FAILED;
		LocalStorageHelper.setValue(LicenseInfoConstant.passCode, newPassCode, true);
		statusCode = ErrorCode.ERROR_PASSCODE_UPDATE_SUCCESS;
		return statusCode;
	},

	resetPassCode: function resetPassCode(_ref) {
		var passcodeType = _ref.passcodeType,
		    successCallBack = _ref.successCallBack,
		    errorCallBack = _ref.errorCallBack;

		$('#loading').show();
		var handleFail = function handleFail(err, statusMsg) {
			if (err) {
				var logger = require('./logger');
				logger.error(err);
			}
			statusMsg = statusMsg || 'Something went wrong. Please try again later. Contact Vyapar team if problem persists.';
			typeof errorCallBack == 'function' && errorCallBack(statusMsg);
			statusMsg && alert(statusMsg);
			$('#loading').hide();
		};
		var handleSuccess = function handleSuccess(statusMsg) {
			statusMsg && alert(statusMsg);
			typeof successCallBack == 'function' && successCallBack(statusMsg);
			$('#loading').hide();
		};
		var isOnline = require('is-online');
		isOnline().then(function (online) {
			if (online) {
				var Domain = require('../Constants/Domain');
				var FirmCache = require('../Cache/FirmCache');

				var _require = require('../Constants/StringConstants'),
				    emailMatchRegex = _require.emailMatchRegex,
				    phoneRegex = _require.phoneRegex;

				var apiUrl = Domain.thisDomain + '/api/passcode/reset';
				var firmCache = new FirmCache();
				var defaultFirm = firmCache.getDefaultFirm();
				var token = '';
				if (passcodeType == 'login') {
					token = LocalStorageHelper.getValue(LicenseInfoConstant.passCode) || '';
				}
				if (passcodeType == 'deleteTxn') {
					var SettingCache = require('./../Cache/SettingCache');
					var EncryptDecrypt = require('./../Utilities/EncryptDecrypt');
					var settingCache = new SettingCache();
					token = EncryptDecrypt.encrypt(settingCache.getDeletePasscode()) || '';
				}
				// Returns a valid phoneNumber or ''
				var getValidPhoneNumber = function getValidPhoneNumber() {
					// Pick phonenumber only for india
					var SettingCache = require('./../Cache/SettingCache');
					var settingCache = new SettingCache();
					if (!settingCache.isCurrentCountryIndia()) {
						return '';
					}
					// First check the localstorage and default firm
					var phoneNumber = LocalStorageHelper.getValue(LocalStorageConstant.LOGIN_PHONE_NUMBER) || defaultFirm.getFirmPhone();
					if (!phoneNumber || phoneNumber.length < 10 || !phoneRegex.test(phoneNumber)) {
						phoneNumber = '';
					}
					if (!phoneNumber) {
						var firmList = firmCache.getFirmList();
						for (var key in firmList) {
							var firm = firmList[key];
							var _phone = firm.getFirmPhone();
							if (_phone && _phone.length >= 10 && phoneRegex.test(_phone)) {
								phoneNumber = _phone;
								break;
							}
						}
					}
					return phoneNumber || '';
				};
				// Returns a valid email or ''
				var getValidEmail = function getValidEmail() {
					// First check the default firm
					var email = defaultFirm.getFirmEmail() || '';
					if (email) {
						email = email.match(emailMatchRegex);
						email = email && email[0];
					}
					if (!email) {
						var firmList = firmCache.getFirmList();
						for (var key in firmList) {
							var firm = firmList[key];
							var emails = firm.getFirmEmail() || '';
							emails = emails.match(emailMatchRegex);
							if (emails) {
								email = emails[0];
								break;
							}
						}
					}
					return email || '';
				};
				var email = getValidEmail();
				var phone = getValidPhoneNumber();
				if (!(token && (email || phone))) {
					handleFail();
					return;
				}

				var getUrl = apiUrl + '?token=' + token + '&email=' + email + '&phone=' + phone;
				fetch(getUrl, {
					method: 'GET',
					headers: {
						'Content-Type': 'application/json'
					}
				}).then(function (response) {
					return response.json();
				}).then(function (result) {
					if (result.message == 'success') {
						var statusMsg = 'Your passcode has been sent to ' + (email || '') + (email && phone ? ' and ' : '') + (phone || '') + '. Kindly contact Vyapar team if you face further issues.';
						handleSuccess(statusMsg);
					} else {
						handleFail();
					}
				}).catch(handleFail);
			} else {
				handleFail(null, 'No internet connection available. Please check your connection and try again.');
			}
		}).catch(handleFail);
	}
};

module.exports = PassCodeHelperUtility;