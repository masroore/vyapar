var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var fs = require('fs');
function copyFile(source, target) {
	var rd = fs.createReadStream(source);
	var wr = fs.createWriteStream(target);
	return new _promise2.default(function (resolve, reject) {
		rd.on('error', reject);
		wr.on('error', reject);
		wr.on('finish', resolve);
		rd.pipe(wr);
	}).catch(function (error) {
		rd.destroy();
		wr.end();
		throw error;
	});
}

module.exports = {
	copyFile: copyFile
};