var SettingCache = require('./../Cache/SettingCache.js');
var MyDouble = require('./../Utilities/MyDouble.js');
var CurrencyModeInWords = require('./../Constants/CurrencyModeInWords.js');

var int_to_words_in_US = function int_to_words_in_US(int) {
	if (int === 0) return 'Zero';

	var ONES = ['', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];
	var TENS = ['', '', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];
	var SCALE = ['', 'Thousand', 'Million', 'Billion', 'Trillion', 'Quadrillion', 'Quintillion', 'Sextillion', 'Septillion', 'Octillion', 'Nonillion'];

	// Return string of first three digits, padded with zeros if needed
	function get_first(str) {
		return ('000' + str).substr(-3);
	}

	// Return string of digits with first three digits chopped off
	function get_rest(str) {
		return str.substr(0, str.length - 3);
	}

	// Return string of triplet convereted to words
	function triplet_to_words(_3rd, _2nd, _1st) {
		return (_3rd == '0' ? '' : ONES[_3rd] + ' Hundred ') + (_1st == '0' ? TENS[_2nd] : TENS[_2nd] && TENS[_2nd] + ' ' || '') + (ONES[_2nd + _1st] || ONES[_1st]);
	}

	// Add to words, triplet words with scale word
	function add_to_words(words, triplet_words, scale_word) {
		return triplet_words ? triplet_words + (scale_word && ' ' + scale_word || '') + ' ' + words : words;
	}

	function iter(words, i, first, rest) {
		if (first == '000' && rest.length === 0) return words;
		return iter(add_to_words(words, triplet_to_words(first[0], first[1], first[2]), SCALE[i]), ++i, get_first(rest), get_rest(rest));
	}

	return iter('', 0, get_first(String(int)), get_rest(String(int)));
};

var int_to_words_in_Indian = function int_to_words_in_Indian(num) {
	if (num === 0) return 'Zero';
	var a = ['', 'One ', 'Two ', 'Three ', 'Four ', 'Five ', 'Six ', 'Seven ', 'Eight ', 'Nine ', 'Ten ', 'Eleven ', 'Twelve ', 'Thirteen ', 'Fourteen ', 'Fifteen ', 'Sixteen ', 'Seventeen ', 'Eighteen ', 'Nineteen '];
	var b = ['', '', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];

	var n = ('0000000000000' + num).substr(-13).match(/^(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
	if (!n) return;
	var str = '';
	str += n[1] != 0 ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'Kharab ' : '';
	str += n[2] != 0 ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'Arab ' : '';
	str += n[3] != 0 ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'Crore ' : '';
	str += n[4] != 0 ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'Lakh ' : '';
	str += n[5] != 0 ? (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'Thousand ' : '';
	str += n[6] != 0 ? (a[Number(n[6])] || b[n[6][0]] + ' ' + a[n[6][1]]) + 'Hundred ' : '';
	str += n[7] != 0 ? (a[Number(n[7])] || b[n[7][0]] + ' ' + a[n[7][1]]) + '' : '';
	return str;
};

var getDecimalString = function getDecimalString(num) {
	var currentCurrency = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

	if (currentCurrency) {
		var amount = MyDouble.getAmountWithDecimal(num);
		var firstTwoDigitPartInDecimal = Math.round(amount * 100) % 100;
		return ' and ' + int_to_words_in_Indian(firstTwoDigitPartInDecimal) + ' ' + currentCurrency.decimalPartWord;
	} else {
		var amountString = MyDouble.getAmountWithDecimal(num) + '';
		var decimalString = '';
		var isDecimalGone = false;
		for (i = 0; i < amountString.length; i++) {
			var c = amountString[i];
			if (!isDecimalGone && c != '.') {
				continue;
			}
			switch (c) {
				case '.':
					decimalString += ' decimal';
					isDecimalGone = true;
					break;
				case '0':
					decimalString += ' Zero';
					break;
				case '1':
					decimalString += ' One';
					break;
				case '2':
					decimalString += ' Two';
					break;
				case '3':
					decimalString += ' Three';
					break;
				case '4':
					decimalString += ' Four';
					break;
				case '5':
					decimalString += ' Five';
					break;
				case '6':
					decimalString += ' Six';
					break;
				case '7':
					decimalString += ' Seven';
					break;
				case '8':
					decimalString += ' Eight';
					break;
				case '9':
					decimalString += ' Nine';
					break;
			}
		}
		return decimalString;
	}
};

var CurrencyHelper = {

	getAmountInWords: function getAmountInWords(amount) {
		try {
			var Currency = require('./../Constants/Currency.js');
			var settingCache = new SettingCache();
			amount = MyDouble.getAmountWithDecimal(amount);
			if (amount == 0) {
				return 'Zero';
			}
			var prefix = '';
			if (amount < 0) {
				amount = -amount;
				prefix = 'Negative ';
			}

			var integerPartString = '';

			var integerPart = parseInt(amount);

			var mode = settingCache.amountInWordsMode();

			if (mode == CurrencyModeInWords.US_MODE) {
				integerPartString = int_to_words_in_US(integerPart);
			} else {
				integerPartString = int_to_words_in_Indian(integerPart);
			}

			var currentCurrency = Currency.getCurrencyFromSymbol(settingCache.getCurrencySymbol());

			if (currentCurrency) {
				integerPartString += ' ' + currentCurrency.integerPartWord;
			}

			if (amount != integerPart) {
				return prefix + integerPartString + getDecimalString(amount, currentCurrency) + ' only';
			} else {
				return prefix + integerPartString + ' only';
			}
		} catch (err) {
			try {
				logger.error('Currency helper Exception: ' + err);
			} catch (e1) {}
		}
		return '';
	}

};

module.exports = CurrencyHelper;