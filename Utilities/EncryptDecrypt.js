/**
 * Created by Ashish on 7/23/2018.
 */
var crypto = require('crypto');
var cypherKey = 'BmI1#iVypGoRBDEO8eU';
var EncryptDecrypt = {
	installationCorrupted: function installationCorrupted(err) {
		var remote = require('electron').remote;
		var app = remote.app;

		var dialog = require('electron').remote.dialog;

		dialog.showMessageBox({
			title: 'Installation Corrupted',
			message: err + '. Please contact our technical support for fixing the issue.',
			type: 'error',
			buttons: ['Ok'],
			defaultId: 0
		});
		app.quit();
	},
	decrypt: function decrypt(data) {
		var showDialogueOnError = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

		try {
			var decipher = crypto.createDecipher('aes-256-cbc', cypherKey);
			var decrypted = decipher.update(data, 'hex', 'utf-8');
			decrypted += decipher.final('utf-8');
			return decrypted;
		} catch (err) {
			if (this.installationCorrupted && showDialogueOnError) {
				this.installationCorrupted(err);
			}
		}
	},

	encrypt: function encrypt(data) {
		var showDialogueOnError = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

		try {
			var cipher = crypto.createCipher('aes-256-cbc', cypherKey);
			var crypted = cipher.update(data, 'utf-8', 'hex');
			crypted += cipher.final('hex');
			return crypted;
		} catch (err) {
			if (this.installationCorrupted && showDialogueOnError) {
				this.installationCorrupted(err);
			}
		}
	}
};

module.exports = EncryptDecrypt;