var DataLoader = require('./../DBManager/DataLoader.js');
var TxnTypeConstant = require('./../Constants/TxnTypeConstant');

var transactionPrintHelper = {
	isLatestTransaction: function isLatestTransaction(transaction) {
		var dataLoader = new DataLoader();
		return dataLoader.isLatestTransaction(transaction.getNameRef().getNameId(), transaction.getTxnId());
	},
	getPreviousAmount: function getPreviousAmount(transaction, name) {
		var txnType = transaction.getTxnType();
		var amount = name.getAmount() ? Number(name.getAmount()) : 0;
		var cashAmount = transaction.getCashAmount() ? Number(transaction.getCashAmount()) : 0;
		var balanceAmount = transaction.getBalanceAmount() ? Number(transaction.getBalanceAmount()) : 0;
		var discountAmount = transaction.getDiscountAmount() ? Number(transaction.getDiscountAmount()) : 0;

		if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN) {
			amount += cashAmount;
			amount += discountAmount;
		} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
			amount -= cashAmount;
			amount -= discountAmount;
		} else if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE) {
			amount += balanceAmount;
		} else if (txnType == TxnTypeConstant.TXN_TYPE_SALE) {
			amount -= balanceAmount;
		} else if (txnType == TxnTypeConstant.TXN_TYPE_POPENBALANCE) {
			amount += balanceAmount;
		} else if (txnType == TxnTypeConstant.TXN_TYPE_ROPENBALANCE) {
			amount -= balanceAmount;
		} else if (txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
			amount += balanceAmount;
		} else if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
			amount -= balanceAmount;
		}
		return amount;
	}

};

module.exports = transactionPrintHelper;