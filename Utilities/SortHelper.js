var sortFlag = 0;
var SortHelper = function SortHelper() {
	this.getSortedListByInvoice = function (listOfTransactions, key) {
		sortFlag++;
		if (sortFlag % 2 == 0) {
			listOfTransactions.sort(function (a, b) {
				return b[key] - a[key];
			});
		} else {
			listOfTransactions.sort(function (a, b) {
				return a[key] - b[key];
			});
		}
		$('#sortIcon').toggleClass('rotate');
		return listOfTransactions;
	};
	this.sort = function () {
		var caseSensitive = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

		return function (a, b) {
			var str1 = a;
			var str2 = b;
			if (!caseSensitive) {
				if (typeof str1 === 'string' && typeof str2 === 'string') {
					str1 = str1.toLowerCase();
					str2 = str2.toLowerCase();
				}
			}

			if (typeof a === 'string' && typeof b === 'string') {
				return str1.localeCompare(str2);
			}

			return a - b;
		};
	};
};

module.exports = SortHelper;