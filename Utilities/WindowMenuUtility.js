Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.buildAppMenu = buildAppMenu;
/* eslint-disable no-path-concat */
if (!window) {
	throw new Error('This module should be required only in BrowserWindow');
}
function buildAppMenu(options) {
	var logger = require('./logger');
	var MenuType = require('../Constants/MenuType');
	var CommonUtility = require('./CommonUtility');

	var _require$remote = require('electron').remote,
	    Menu = _require$remote.Menu,
	    dialog = _require$remote.dialog,
	    app = _require$remote.app,
	    getCurrentWindow = _require$remote.getCurrentWindow;

	var _require = require('electron'),
	    ipcRenderer = _require.ipcRenderer;

	var path = require('path');
	var mainWindow = getCurrentWindow();
	var mainPage = 'Index.html';
	var destroyDBWindow = function destroyDBWindow() {
		try {
			var win = require('../Utilities/DBWindow');
			win.destroy();
		} catch (err) {
			logger.error(err);
		}
	};
	var openSupportApp = function openSupportApp() {
		try {
			if (!document.getElementById('supportAppDialog')) {
				var supportAppDialog = document.createElement('div');
				supportAppDialog.setAttribute('id', 'supportAppDialog');
				document.body.appendChild(supportAppDialog);
			}
			require('../UIControllers/SupportAppController')({
				mountPoint: '#supportAppDialog'
			});
		} catch (err) {
			logger.error(err);
		}
	};
	function showLoader() {
		$('#loading').show();
	}

	function checkingForUpdate() {
		var ToastHelper = require('../UIControllers/ToastHelper');
		ToastHelper.info('We are checking for update.');
	}

	function hideLoader() {
		$('#loading').hide();
	}

	function showCompanyNameUpdateDialog() {
		mainWindow.webContents.send('ShowUpdateComapnyName');
	}

	var companyMenuTemplate = {
		label: 'Company',
		submenu: [{
			label: 'Change Company',
			click: function click() {
				destroyDBWindow();

				var app = require('electron').remote.app;

				if (app.hasUserPassCodeEntered) {
					mainWindow.loadURL(path.resolve(__dirname, '../UIComponent/SwitchCompany.html'));
				} else {
					mainWindow.loadURL(path.resolve(__dirname, '../UIComponent/', mainPage));
				}
			}
		}, {
			label: 'Rename Company Name',
			click: function click() {
				showCompanyNameUpdateDialog();
			}
		}]
	};
	var manageCompanyTemplate = {
		label: 'Manage Companies',
		click: function click() {
			destroyDBWindow();

			var app = require('electron').remote.app;

			if (app.hasUserPassCodeEntered) {
				mainWindow.loadURL(path.resolve(__dirname, '../UIComponent/SwitchCompany.html'));
			} else {
				mainWindow.loadURL(path.resolve(__dirname, '../UIComponent/', mainPage));
			}
		}
	};
	var reloadMenuTemplate = {
		label: 'Reload',
		click: function click() {
			mainWindow && mainWindow.setMenuBarVisibility(true);
			destroyDBWindow();
			mainWindow.loadURL(path.resolve(__dirname, '../UIComponent/', mainPage));
		}
	};
	var vyaparHelpMenuTemplate = {
		label: 'Help',
		submenu: [{
			label: 'Contact Us',
			click: function click() {
				try {
					MyAnalytics.pushEvent('Contact Us');
				} catch (err) {}
				require('electron').shell.openExternal('https://vyaparapp.in/#contact');
			}
		}, {
			label: 'Remote Vyapar Support',
			click: function click() {
				try {
					MyAnalytics.pushEvent('Support App Menu Clicked');
				} catch (err) {}
				openSupportApp();
			}
		}, {
			label: 'Video Tutorials',
			click: function click() {
				try {
					MyAnalytics.pushEvent('View Tutorials');
				} catch (err) {}
				require('electron').shell.openExternal('https://vyaparapp.in/desktop/videos/');
			}
		}, {
			label: 'View Release Notes',
			click: function click() {
				try {
					MyAnalytics.pushEvent('View Release Notes');
				} catch (err) {}
				require('electron').shell.openExternal('https://vyaparapp.in/release');
			}
		}, {
			label: 'Check For Update',
			click: function click(menuItem, browserWindow, event) {
				showLoader();
				try {
					MyAnalytics.pushEvent('Check for update');
				} catch (err) {}
				var isOnline = require('is-online');
				isOnline().then(function (online) {
					if (online) {
						menuItem.enabled = false;
						hideLoader();
						checkingForUpdate();
						ipcRenderer.send('checkForUpdates');
					} else {
						hideLoader();
						dialog.showMessageBox({
							type: 'info',
							buttons: ['Ok'],
							title: 'Vyapar',
							message: 'No internet connection available. Please check your connection and try again.'
						});
					}
				});
			}
		}, {
			label: 'Version : ' + app.getVersion(),
			click: function click() {}
		}, {
			label: 'Privacy policy',
			click: function click() {
				mainWindow.webContents.executeJavaScript('try { MyAnalytics.pushEvent("View Privacy policy"); } catch(err) {}');
				require('electron').shell.openExternal('https://vyaparapp.in/permission');
			}
		}]
	};
	var shortCutMenuTemplate = {
		id: 'shortcuts',
		label: 'Shortcuts',
		click: function click() {
			try {
				if (canCloseDialogue()) {
					$('#defaultPage').hide();
					CommonUtility.loadFrameDiv('shortcuts.html');
					document.dispatchEvent(new CustomEvent('defaultpageother-shortcut-triggered'));
					unmountReactComponent(document.querySelector('#salePurchaseContainer'));
				}
			} catch (err) {
				logger.error(err);
				CommonUtility.hideFramediv();
			}
		}

	};
	var template = [];
	var _options$menuType = options.menuType,
	    menuType = _options$menuType === undefined ? MenuType.HELP_MENU : _options$menuType;

	if (menuType === MenuType.DEFAULT_MENU) {
		template = [companyMenuTemplate, vyaparHelpMenuTemplate, shortCutMenuTemplate, reloadMenuTemplate];
	} else if (menuType === MenuType.LOGIN_MENU) {
		template = [manageCompanyTemplate, vyaparHelpMenuTemplate];
	} else {
		template = [vyaparHelpMenuTemplate];
	}
	var menu = Menu.buildFromTemplate(template);
	Menu.setApplicationMenu(menu);
	return menu;
}