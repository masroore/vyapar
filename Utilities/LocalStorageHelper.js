var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var EncryptDecrypt = require('./EncryptDecrypt.js');
var LocalStorageHelper = {
	getValue: function getValue(key) {
		var isDecryptionNeeded = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

		var value = localStorage.getItem(key);
		if (value && isDecryptionNeeded) {
			try {
				value = EncryptDecrypt.decrypt(value);
			} catch (err) {
				value = '';
			}
		}
		return value;
	},

	setValue: function setValue(key, value) {
		var isEncryptionNeeded = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

		if (!value) {
			value = '';
		}
		if (value && isEncryptionNeeded) {
			try {
				value = EncryptDecrypt.encrypt(value);
			} catch (err) {
				value = '';
			}
		}
		localStorage.setItem(key, value);
	},

	removeValue: function removeValue(key) {
		localStorage.removeItem(key);
	}
};

(function initLocalStorage() {
	function updateGridSettingsInLocalStorage() {
		var settings = LocalStorageHelper.getValue('__GRID_SETTINGS__') || '{}';
		settings = JSON.parse(settings);

		var orderForm = LocalStorageHelper.getValue('orderForm');
		var orderSettings = settings['ordersTransactionsGridKey'];
		if (orderSettings && !orderForm) {
			delete settings['ordersTransactionsGridKey'];
			LocalStorageHelper.setValue('__GRID_SETTINGS__', (0, _stringify2.default)(settings));
			LocalStorageHelper.setValue('orderForm', 1);
		}

		var isContextMenuAdded = LocalStorageHelper.getValue('contextMenuAdded') === '1';
		if (!isContextMenuAdded) {
			delete settings['itemsListForServicesGridKey'];
			delete settings['itemsListForProductsGridKey'];
			delete settings['nameGroupsListGridKey'];
			delete settings['namesListGridKey'];
			delete settings['unitListGridKey'];
			LocalStorageHelper.setValue('__GRID_SETTINGS__', (0, _stringify2.default)(settings));
			LocalStorageHelper.setValue('contextMenuAdded', 1);
		}
	}

	try {
		updateGridSettingsInLocalStorage();
	} catch (err) {}
})();

module.exports = LocalStorageHelper;