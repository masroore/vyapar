var deviceInfo;
var DeviceHelper = {
	isDeviceIdValid: function isDeviceIdValid(deviceId) {
		if (deviceId && deviceId.length > 0) {
			deviceId = deviceId.toLowerCase();

			if (deviceId != 'fefefefe-fefe-fefe-fefe-fefefefefefe' && deviceId != 'system serial number' && deviceId != 'false' && deviceId != 'undefined' && deviceId != 'invalid' && !deviceId.startsWith('to be filled') && deviceId != 'ffffffff-ffff-ffff-ffff-ffffffffffff') {
				return true;
			}
		}
		return false;
	},

	getDeviceInfo: function getDeviceInfo() {
		if (deviceInfo) {
			return deviceInfo;
		}
		var LocalStorageHelper = require('../Utilities/LocalStorageHelper');
		var LocalStorageConstant = require('../Constants/LocalStorageConstant');
		var CommonUtility = require('../Utilities/CommonUtility');
		var os = require('os');
		var deviceId = LocalStorageHelper.getValue(LocalStorageConstant.DEVICE_ID, true);
		if (!deviceId || !this.isDeviceIdValid(deviceId)) {
			deviceId = CommonUtility.generateNewGUID(12);
			LocalStorageHelper.setValue(LocalStorageConstant.DEVICE_ID, deviceId, true);
		}
		deviceInfo = {
			deviceId: deviceId,
			osPlatform: os.platform(),
			osRelease: os.release(),
			modelNo: os.cpus()[0]['model'],
			deviceName: os.hostname(),
			deviceOs: os.type() + ', ' + os.platform() + ', ' + os.arch()
		};
		return deviceInfo;
	}
};

module.exports = DeviceHelper;