var fs = require('fs');

var app = require('electron').remote.app;

var appPath = app.getPath('userData');
var TOKEN_DIR = appPath + '/syncToken';
var TOKEN_PATH = TOKEN_DIR + '/vyaparAuthToken.json';

var TokenForSync = {

	writeToken: function writeToken(tokenData) {
		if (!fs.existsSync(TOKEN_DIR)) {
			fs.mkdirSync(TOKEN_DIR);
		}

		fs.writeFileSync(TOKEN_PATH, tokenData);
	},

	readToken: function readToken() {
		if (this.ifExistToken()) {
			var tokenRaw = fs.readFileSync(TOKEN_PATH);
			var syncToken = JSON.parse(tokenRaw);
			return syncToken;
		} else return false;
	},

	ifExistToken: function ifExistToken() {
		if (fs.existsSync(TOKEN_PATH)) {
			return true;
		} else {
			return false;
		}
	},

	deleteToken: function deleteToken() {
		fs.unlinkSync(TOKEN_PATH);
	}

};

module.exports = TokenForSync;