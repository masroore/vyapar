var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var RoundOffConstant = require('./../Constants/RoundOffConstant.js');

var settingCacheInDouble = null;

var withdrawIds = [TxnTypeConstant.TXN_TYPE_PURCHASE, TxnTypeConstant.TXN_TYPE_CASHOUT, TxnTypeConstant.TXN_TYPE_SALE_RETURN, TxnTypeConstant.TXN_TYPE_EXPENSE, TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE, TxnTypeConstant.TXN_TYPE_CASH_ADJ_REDUCE, TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE_WITHOUT_CASH, TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER, TxnTypeConstant.TXN_TYPE_LOAN_EMI_PAYMENT, TxnTypeConstant.TXN_TYPE_LOAN_PROCESSING_FEE];

var roundOffToDecimal = function roundOffToDecimal(value, decimal) {
	return Math.round(Number(value) * Math.pow(10, decimal)) / Math.pow(10, decimal);
};

var getSettingCacheObjectInMyDouble = function getSettingCacheObjectInMyDouble() {
	if (!settingCacheInDouble) {
		var SettingCache = require('./../Cache/SettingCache.js');
		settingCacheInDouble = new SettingCache();
	}

	return settingCacheInDouble;
};

var MyDouble = {
	getSignString: function getSignString(value) {
		var explicitSignForPlus = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
		var spaceAfterSign = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
		var spaceBeforeSign = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;

		if (spaceAfterSign) {
			return value < 0 ? spaceBeforeSign ? ' - ' : '- ' : explicitSignForPlus ? spaceBeforeSign ? ' + ' : '+ ' : '';
		} else {
			return value < 0 ? spaceBeforeSign ? ' -' : '-' : explicitSignForPlus ? spaceBeforeSign ? ' +' : '+' : '';
		}
	},

	getAmountStringInPrintingFormat: function getAmountStringInPrintingFormat(amount) {
		var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

		var settingCacheObj = getSettingCacheObjectInMyDouble();
		var digitsAfterDecimal = settingCacheObj.getAmountDecimal();
		if (typeof amount != 'number') {
			amount = Number(amount);
		}
		var absoluteAmount = Math.abs(amount);
		var amountString = '';
		if (settingCacheObj.isAmountGroupingEnabled()) {
			amountString = absoluteAmount.toLocaleString(settingCacheObj.getAmountFormatString(), {
				minimumFractionDigits: settingCacheObj.amountToBeFixedTillSpecifiedDecimalPlaces() ? digitsAfterDecimal : 0,
				maximumFractionDigits: digitsAfterDecimal
			});
		} else {
			if (settingCacheObj.amountToBeFixedTillSpecifiedDecimalPlaces()) {
				amountString = absoluteAmount.toFixed(digitsAfterDecimal);
			} else {
				absoluteAmount = roundOffToDecimal(absoluteAmount, digitsAfterDecimal);
				amountString = absoluteAmount + '';
			}
		}

		if (options.showCurrencySymbol) {
			var currencySymbol = settingCacheObj.getCurrencySymbol();
			amountString = currencySymbol + ' ' + amountString;
		}
		if (!options.ignoreSign && amount < 0) {
			amountString = '- ' + amountString;
		}
		return amountString;
	},

	doubleToStringForAmountWithPrecisionSetting: function doubleToStringForAmountWithPrecisionSetting(amount) {
		var roundOffBeforeConvertingToString = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

		var settingCacheObj = getSettingCacheObjectInMyDouble();
		var digitsAfterDecimal = settingCacheObj.getAmountDecimal();
		if (roundOffBeforeConvertingToString) {
			amount = roundOffToDecimal(amount, digitsAfterDecimal);
		}
		if (settingCacheObj.amountToBeFixedTillSpecifiedDecimalPlaces()) {
			return amount.toFixed(digitsAfterDecimal);
		} else {
			return amount + '';
		}
	},

	getBalanceAmountDecimalWithoutCurrency: function getBalanceAmountDecimalWithoutCurrency(balAmount, showSign) {
		var forReact = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

		var amountString = this.getAmountStringInPrintingFormat(balAmount, {
			showCurrencySymbol: false,
			ignoreSign: !showSign
		});
		var returnValue = '';
		if (balAmount < 0) {
			returnValue = "<span style='color:#e35959'>" + amountString + '</span>';
		} else {
			returnValue = "<span style='color:#1faf9d'>" + amountString + '</span>';
		}
		if (forReact) {
			return { __html: returnValue };
		} else {
			return returnValue;
		}
	},
	getBalanceAmountWithDecimalAndCurrency: function getBalanceAmountWithDecimalAndCurrency(balAmount, showSign) {
		var forReact = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

		var amountString = this.getAmountStringInPrintingFormat(balAmount, {
			showCurrencySymbol: true,
			ignoreSign: !showSign
		});
		var returnValue = '';
		if (balAmount < 0) {
			returnValue = "<span style='color:#e35959'>" + amountString + '</span>';
		} else {
			returnValue = "<span style='color:#1faf9d'>" + amountString + '</span>';
		}
		if (forReact) {
			return { __html: returnValue };
		} else {
			return returnValue;
		}
	},
	getBalanceAmountWithDecimalAndCurrencyForTxnType: function getBalanceAmountWithDecimalAndCurrencyForTxnType(balAmount, txnType, subTxnTypeId, revertColorForBankTxn) {
		var ignoreSign = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : true;

		var amountString = this.getAmountStringInPrintingFormat(balAmount, {
			showCurrencySymbol: true,
			ignoreSign: ignoreSign
		});
		try {
			if (revertColorForBankTxn) {
				if (txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD) {
					return "<span style='color:#e35959'>" + amountString + '</span>';
				} else if (txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE) {
					return "<span style='color:#1faf9d'>" + amountString + '</span>';
				}
			}
		} catch (err) {}
		if (withdrawIds.indexOf(txnType) != -1) {
			return "<span style='color:#e35959'>" + amountString + '</span>';
		} else {
			if (subTxnTypeId) {
				var depositIds = [TxnTypeConstant.TXN_TYPE_PURCHASE, TxnTypeConstant.TXN_TYPE_CASHOUT, TxnTypeConstant.TXN_TYPE_SALE_RETURN, TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER, TxnTypeConstant.TXN_TYPE_EXPENSE];
				if (depositIds.indexOf(Number(subTxnTypeId)) != -1) {
					return "<span style='color:#e35959'>" + amountString + '</span>';
				} else {
					return "<span style='color:#1faf9d'>" + amountString + '</span>';
				}
			} else {
				var color = balAmount < 0 ? 'style="color: #e35959"' : 'style="color: #1faf9d"';
				if (txnType == TxnTypeConstant.TXN_TYPE_LINKED_CASHIN_OPENINGBALANCE || txnType == TxnTypeConstant.TXN_TYPE_LINKED_CASHOUT_OPENINGBALANCE) {
					color = '';
				}
				return '<span ' + color + '>' + amountString + '</span>';
			}
		}
	},
	getBalanceAmountWithDecimalAndCurrencyWithoutColor: function getBalanceAmountWithDecimalAndCurrencyWithoutColor(balAmount) {
		return this.getAmountStringInPrintingFormat(balAmount, {
			showCurrencySymbol: true,
			ignoreSign: true
		});
	},
	getBalanceAmountWithDecimal: function getBalanceAmountWithDecimal(balAmount) {
		if (isNaN(balAmount)) {
			return 0;
		}
		balAmount = Number(balAmount);
		var digitsAfterDecimal = getSettingCacheObjectInMyDouble().getAmountDecimal();
		// balAmount = Math.abs(balAmount);
		return roundOffToDecimal(balAmount, digitsAfterDecimal);
	},

	quantityDoubleToStringWithSignExplicitly: function quantityDoubleToStringWithSignExplicitly(value, emptyForZero) {
		if (emptyForZero && value == 0) {
			return '';
		}
		return MyDouble.getSignString(value, true, true, true) + this.trimQuantityToString(Math.abs(value));
	},


	getQuantityWithDecimal: function getQuantityWithDecimal(quantity) {
		var minimumQty = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
		var forReact = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

		quantity = Number(quantity);
		var digitsAfterDecimal = getSettingCacheObjectInMyDouble().getQuantityDecimal();
		quantity = roundOffToDecimal(quantity, digitsAfterDecimal);
		var returnValue = '';
		minimumQty = Number(minimumQty);
		if (quantity <= minimumQty) {
			returnValue = "<span style='color:#e35959'>" + quantity + '</span>';
		} else {
			returnValue = "<span style='color:#1faf9d'>" + quantity + '</span>';
		}
		if (forReact) {
			return { __html: returnValue };
		} else {
			return returnValue;
		}
	},

	getQuantityWithDecimalWithoutColor: function getQuantityWithDecimalWithoutColor(quantity) {
		quantity = Number(quantity);
		var digitsAfterDecimal = getSettingCacheObjectInMyDouble().getQuantityDecimal();
		var newQuant = roundOffToDecimal(quantity, digitsAfterDecimal);
		return newQuant;
	},

	getAmountWithDecimal: function getAmountWithDecimal(balAmount) {
		var digitsAfterDecimal = getSettingCacheObjectInMyDouble().getAmountDecimal();
		return roundOffToDecimal(balAmount, digitsAfterDecimal);
	},

	getAmountWithDecimalAndCurrency: function getAmountWithDecimalAndCurrency(balAmount) {
		return this.getAmountStringInPrintingFormat(balAmount, {
			showCurrencySymbol: true
		});
	},

	getAmountForThermalPrinter: function getAmountForThermalPrinter(balAmount) {
		return MyDouble.doubleToStringForAmountWithPrecisionSetting(Number(balAmount));
	},

	getAmountWithDecimalAndCurrencyWithoutSign: function getAmountWithDecimalAndCurrencyWithoutSign(amount) {
		return this.getAmountStringInPrintingFormat(amount, {
			showCurrencySymbol: true,
			ignoreSign: true
		});
	},

	getAmountWithDecimalAndCurrencyWithSign: function getAmountWithDecimalAndCurrencyWithSign(amount) {
		return this.getAmountStringInPrintingFormat(amount, {
			showCurrencySymbol: true
		});
	},

	getPercentageWithDecimal: function getPercentageWithDecimal(value) {
		return roundOffToDecimal(Number(value), 3);
	},

	getAmountForInvoicePrint: function getAmountForInvoicePrint(value, emptyForZero) {
		if (emptyForZero && !value) {
			return '';
		}
		return this.getAmountStringInPrintingFormat(value, {
			showCurrencySymbol: true
		});
	},

	trimQuantityToString: function trimQuantityToString(value, emptyForZero) {
		value = Number(value);
		if (value == 0 && emptyForZero) {
			return '';
		}
		var digitsAfterDecimal = getSettingCacheObjectInMyDouble().getQuantityDecimal();
		var newQuant = roundOffToDecimal(value, digitsAfterDecimal) + '';
		return newQuant.replace(/\.0*$/, '');
	},

	getAmountDecimalForGSTRReport: function getAmountDecimalForGSTRReport(value) {
		return roundOffToDecimal(value, 2);
	},

	getQuantityDecimalForGSTRReport: function getQuantityDecimalForGSTRReport(value) {
		return roundOffToDecimal(value, 2);
	},

	convertStringToDouble: function convertStringToDouble(value) {
		var convertNaNToZero = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
		var throwExcption = arguments[2];

		// by default convertNaNto0 = true
		// throwException = false
		if (!value) {
			return 0;
		}
		if (value && Number(value)) {
			return Number(value);
		}
		if (value.trim().length > 1) {
			if (convertNaNToZero) {
				return 0;
			}
		} else {
			return 0;
		}
	},

	roundOffToNDecimalPlaces: function roundOffToNDecimalPlaces(value, n) {
		var v = this.convertStringToDouble(value);
		if (v) {
			return Math.round(v * Math.pow(10, n)) / Math.pow(10, n);
		} else {
			return 0;
		}
	},

	getRoundedOffTransactionAmount: function getRoundedOffTransactionAmount(value) {
		var roundOffUpTo = getSettingCacheObjectInMyDouble().getRoundOffToPlace();
		var roundOffType = getSettingCacheObjectInMyDouble().getRoundOffToValue();
		value = MyDouble.convertStringToDouble(value, true);
		value = Number(value) / roundOffUpTo;

		switch (Number(roundOffType)) {
			case RoundOffConstant.ROUND_NEAR:
				value = Math.round(value);
				break;
			case RoundOffConstant.ROUND_FLOOR:
				value = Math.floor(value);
				break;
			case RoundOffConstant.ROUND_CEIL:
				value = Math.ceil(value);
				break;
		}

		return value * roundOffUpTo;
	},
	getDueDays: function getDueDays(dueDate) {
		var currentDate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : new Date();

		return Math.floor((Date.UTC(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()) - Date.UTC(dueDate.getFullYear(), dueDate.getMonth(), dueDate.getDate())) / (1000 * 60 * 60 * 24));
	}
};

module.exports = MyDouble;