var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault2(_keys);

function _interopRequireDefault2(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CleverTap = require('clevertap');

var CT_ACCOUNT_ID = 'R55-765-4K5Z';
var CT_ACCOUNT_PASSCODE = 'ETO-BIZ-ALKL';

// init the library with your CleverTap Account Id and CleverTap Account Passcode
var clevertap = CleverTap.init(CT_ACCOUNT_ID, CT_ACCOUNT_PASSCODE);

var Queries = require('./../Constants/Queries.js');
var _electronGoogleAnalytics = require('electron-google-analytics');
function _interopRequireDefault(obj) {
	return obj && obj.__esModule ? obj : { default: obj };
}

var _electronGoogleAnalytics2 = _interopRequireDefault(_electronGoogleAnalytics);
var analytics = new _electronGoogleAnalytics2.default('UA-85524827-1');

var AnalyticsHelper = {
	getSettingname: function getSettingname(name) {
		var settingName = 'setting';

		if (name) {
			switch (name) {
				case Queries.SETTING_ITEM_ENABLED:
					settingName = 'Item Enabled';
					break;
				case Queries.SETTING_TAX_ENABLED:
					settingName = 'Transaction Tax Enabled';
					break;
				case Queries.SETTING_DISCOUNT_ENABLED:
					settingName = 'Transaction Discount Enabled';
					break;
				case Queries.SETTING_PASSCODE_ENABLED:
					settingName = 'Passcode';
					break;
				case Queries.SETTING_LAST_BACKUP_TIME:
					settingName = 'Last Backup Time';
					break;
				case Queries.SETTING_BACKUP_REMINDER_DAYS:
					settingName = 'Backup Reminder Days';
					break;
				case Queries.SETTING_LAST_BACKUP_REMINDER_TIME:
					settingName = 'Last Backup Reminder Time';
					break;
				case Queries.SETTING_BACKUP_REMINDER_SNOOZE:
					settingName = 'Backup Reminder Snooze';
					break;
				case Queries.SETTING_CURRENCY_SYMBOL:
					settingName = 'Currency Symbol';
					break;
				case Queries.SETTING_STOCK_ENABLED:
					settingName = 'Item Stock Enabled';
					break;
				case Queries.SETTING_TXNREFNO_ENABLED:
					settingName = 'Invoice Number Enabled';
					break;
				case Queries.SETTING_DELETE_PASSCODE_ENABLED:
					settingName = 'Enable Passcode for transaction delete';
					break;
				case Queries.SETTING_BILL_TO_BILL_ENABLED:
					settingName = 'Bill Wise Payment';
					break;
				case Queries.SETTING_FREE_QTY_ENABLED:
					settingName = 'Enable free quantity';
					break;
				case Queries.SETTING_OTHER_INCOME_ENABLED:
					settingName = 'Other Income';
					break;
				case Queries.SETTING_SHOW_LAST_FIVE_SALE_PRICE:
					settingName = 'Show last five sale price';
					break;
				case Queries.SETTING_TIN_NUMBER_ENABLED:
					settingName = 'Party GSTIN number';
					break;
				case Queries.SETTING_SYNC_ENABLED:
					settingName = 'Sync Enabled';
					break;
				case Queries.SETTING_PRINT_COMPANY_NUMBER_ON_TXN_PDF:
					settingName = 'Print Company Contact';
					break;
				case Queries.SETTING_PRINT_TINNUMBER:
					settingName = 'Print GSTIN on sale';
					break;
				case Queries.SETTING_PRINT_BILL_OF_SUPPLY_FOR_NON_TAX_TXN:
					settingName = 'Print Bill of supply for non tax transactionr';
					break;
				case Queries.SETTING_THERMAL_PRINTER_NATIVE_LANG:
					settingName = 'Enable native language in thermal printer';
					break;
				case Queries.SETTING_PARTY_ITEM_RATE:
					settingName = 'Party wise Item Rate';
					break;
				case Queries.SETTING_PAYMENTREMIDNER_ENABLED:
					settingName = 'Payment Reminder';
					break;
				case Queries.SETTING_PAYMENT_REMINDER_DAYS:
					settingName = 'Payment Reminder Period';
					break;
				case Queries.SETTING_TRANSACTION_MESSAGE_ENABLED:
					settingName = 'Transaction Customer Message';
					break;
				case Queries.SETTING_PRINT_TINNUMBER:
					settingName = 'Print TIN Number';
					break;
				case Queries.SETTING_QUANTITY_DECIMAL:
					settingName = 'Quantity Decimal Places';
					break;
				case Queries.SETTING_ITEMWISE_TAX_ENABLED:
					settingName = 'Item Wise Tax Enabled';
					break;
				case Queries.SETTING_ITEMWISE_DISCOUNT_ENABLED:
					settingName = 'Item Wise Discount Enabled';
					break;
				case Queries.SETTING_ORDER_FORM_ENABLED:
					settingName = 'Order Form Enabled';
					break;
				case Queries.SETTING_AMOUNT_DECIMAL:
					settingName = 'Amount Decimal Places';
					break;
				case Queries.SETTING_TXN_MESSAGE_ENABLED_SALE:
					settingName = 'Sale Transaction Message Enabled';
					break;
				case Queries.SETTING_TXN_MESSAGE_ENABLED_PURCHASE:
					settingName = 'Purchase Transaction Message Enabled';
					break;
				case Queries.SETTING_TXN_MESSAGE_ENABLED_SALERETURN:
					settingName = 'Sale Return Transaction Message Enabled';
					break;
				case Queries.SETTING_TXN_MESSAGE_ENABLED_PURCHASERETURN:
					settingName = 'Purchase Return Transaction Message Enabled';
					break;
				case Queries.SETTING_TXN_MESSAGE_ENABLED_CASHIN:
					settingName = 'Cash-in Transaction Message Enabled';
					break;
				case Queries.SETTING_TXN_MESSAGE_ENABLED_CASHOUT:
					settingName = 'Cash-out Transaction Message Enabled';
					break;
				case Queries.SETTING_TXN_MESSAGE_ENABLED_ORDER:
					settingName = 'Order Form Transaction Message Enabled';
					break;
				case Queries.SETTING_DISCOUNT_IN_MONEY_TXN:
					settingName = 'Payment Discount Enabled';
					break;
				case Queries.SETTING_AUTO_BACKUP_ENABLED:
					settingName = 'Auto Backup Enabled';
					break;
				case Queries.SETTING_AUTO_BACKUP_LAST_BACKUP:
					settingName = 'Last Auto Backup time';
					break;
				case Queries.SETTING_AUTO_BACKUP_DURATION_DAYS:
					settingName = 'Auto Backup Duration';
					break;
				case Queries.SETTING_REMIND_RATING:
					settingName = 'Reminder For Rating';
					break;
				case Queries.SETTING_PARTY_GROUP:
					settingName = 'Party Group';
					break;
				case Queries.SETTING_COMPANY_LOGO_ID:
					settingName = 'Company Logo ID';
					break;
				case Queries.SETTING_ITEM_CATEGORY:
					settingName = 'Item Category Enabled';
					break;
				case Queries.SETTING_AC_ENABLED:
					settingName = 'Additional Charge Enabled';
					break;
				case Queries.SETTING_AC1_ENABLED:
					settingName = 'Additional Charge 1 Enabled';
					break;
				case Queries.SETTING_AC2_ENABLED:
					settingName = 'Additional Charge 2 Enabled';
					break;
				case Queries.SETTING_AC3_ENABLED:
					settingName = 'Additional Charge 3 Enabled';
					break;
				case Queries.SETTING_EXTRA_SPACE_ON_TXN_PDF:
					settingName = 'Extra Space on Transaction PDF';
					break;
				case Queries.SETTING_MIN_ITEM_ROWS_ON_TXN_PDF:
					settingName = 'Minimum Item Rows in Transaction PDF';
					break;
				case Queries.SETTING_AMOUNT_IN_WORD_FORMAT:
					settingName = 'Amount in Word Format';
					break;
				case Queries.SETTING_PRINT_LOGO_ON_TXN_PDF:
					settingName = 'Print Logo in Transaction PDF';
					break;
				case Queries.SETTING_PRINT_COMPANY_NAME_ON_TXN_PDF:
					settingName = 'Print Company Name in Transaction PDF';
					break;
				case Queries.SETTING_PRINT_COMPANY_ADDRESS_ON_TXN_PDF:
					settingName = 'Print Company Address in Transaction PDF';
					break;
				case Queries.SETTING_PRINT_COMPANY_NUMBER_ON_TXN_PDF:
					settingName = 'Print Company Number in Transaction PDF';
					break;
				case Queries.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF:
					settingName = 'Print Term and Condition in Transaction PDF';
					break;
				case Queries.SETTING_TERMS_AND_CONDITIONS:
					settingName = 'Terms and Condition';
					break;
				case Queries.SETTING_PRINT_DESCRIPTION_ON_TXN_PDF:
					settingName = 'Print Description in Transaction PDF';
					break;
				case Queries.SETTING_TXN_PDF_THEME:
					settingName = 'Transaction PDF Theme';
					break;
				case Queries.SETTING_TXN_PDF_THEME_COLOR:
					settingName = 'Invoice Theme Color';
					break;
				case Queries.SETTING_TXN_MSG_TO_OWNER:
					settingName = 'Transaction Message to Owner';
					break;
				case Queries.SETTING_TIN_NUMBER_ENABLED:
					settingName = 'TIN Number Enabled';
					break;
				case Queries.SETTING_SIGNATURE_ENABLED:
					settingName = 'Signature Enabled';
					break;
				case Queries.SETTING_MULTI_LANGUAGE_ENABLED:
					settingName = 'Local Language Enabled';
					break;
				case Queries.SETTING_PRINT_ITEM_QUANTITY_TOTAL_ON_TXN_PDF:
					settingName = 'Print Item Quantity Total in Transaction';
					break;
				case Queries.SETTING_LEADS_INFO_SENT:
					settingName = 'Leads Info Sent';
					break;
				case Queries.SETTING_MULTIFIRM_ENABLED:
					settingName = 'Multifirm Enabled';
					break;
				case Queries.SETTING_TAXINVOICE_ENABLED:
					settingName = 'Tax Invoice Enabled';
					break;
				case Queries.SETTING_BARCODE_SCANNING_ENABLED:
					settingName = 'Barcode Scanning Enabled';
					break;
				case Queries.SETTING_IS_ITEM_UNIT_ENABLED:
					settingName = 'Item Unit Enabled';
					break;
				case Queries.SETTING_SHOW_CURRENT_BALANCE_OF_PARTY:
					settingName = 'Show Current Balance of Party';
					break;
				case Queries.SETTING_PRINT_TEXT_SIZE:
					settingName = 'Print Text Size';
					break;
				case Queries.SETTING_PRINT_COMPANY_NAME_TEXT_SIZE:
					settingName = 'Company name Print Text Size';
					break;
				case Queries.SETTING_PRINT_PAGE_SIZE:
					settingName = 'Print Page Size';
					break;
				case Queries.SETTING_THERMAL_PRINTER_PAGE_SIZE:
					settingName = 'Thermal Printer Page Size';
					break;
				case Queries.SETTING_DEFAULT_PRINTER:
					settingName = 'Default Printer';
					break;
				case Queries.SETTING_GST_ENABLED:
					settingName = 'GST Enabled';
					break;
				case Queries.SETTING_HSN_SAC_ENABLED:
					settingName = 'HSN/SAC Enabled';
					break;
				case Queries.SETTING_NO_OF_DECIMAL_IN_PERCENTAGE:
					settingName = 'No of Decimal in Percentage';
					break;
				case Queries.SETTING_PRINT_TAX_DETAILS:
					settingName = 'Print Tax details';
					break;
				case Queries.SETTING_BARCODE_SCANNER_TYPE:
					settingName = 'Barcode Scanner Type';
					break;
				case Queries.SETTING_USER_COUNTRY:
					settingName = 'User Country';
					break;
				case Queries.SETTING_PARTY_SHIPPING_ADDRESS_ENABLED:
					settingName = 'Party Shipping Address Enabled';
					break;
				case Queries.SETTING_PRINT_PARTY_SHIPPING_ADDRESS:
					settingName = 'Print Party Shipping Address';
					break;
				case Queries.SETTING_PRINT_BANK_DETAIL:
					settingName = 'Print Bank Detail';
					break;
				case Queries.SETTING_SIGNATURE_TEXT:
					settingName = 'Signature Text';
					break;
				case Queries.SETTING_SHOW_PURCHASE_PRICE_IN_ITEM_DROP_DOWN:
					settingName = 'Show Purchase Price in Item Dropdown';
					break;
				case Queries.SETTING_PRINT_COPY_NUMBER:
					settingName = 'Print original/duplicate';
					break;
				case Queries.SETTING_ENABLE_DISPLAY_NAME:
					settingName = 'Display Name Enabled';
					break;
				case Queries.SETTING_ENABLE_ITEM_MRP:
					settingName = 'Item MRP Enabled';
					break;
				case Queries.SETTING_ENABLE_ITEM_BATCH_NUMBER:
					settingName = 'Enable Item Batch Number';
					break;
				case Queries.SETTING_ENABLE_ITEM_EXPIRY_DATE:
					settingName = 'Enable Item Expiry Date Number';
					break;
				case Queries.SETTING_ENABLE_ITEM_MANUFACTURING_DATE:
					settingName = 'Enable Item Manufacturing Date Number';
					break;
				case Queries.SETTING_ENABLE_SERIAL_ITEM_NUMBER:
					settingName = 'Enable Item Serial Number';
					break;
				case Queries.SETTING_ENABLE_ITEM_COUNT:
					settingName = 'Enable Item Count';
					break;
				case Queries.SETTING_ENABLE_ITEM_DESCRIPTION:
					settingName = 'Enable Item Description';
					break;
				case Queries.SETTING_ITEM_MRP_VALUE:
					settingName = 'Item MRP Value';
					break;
				case Queries.SETTING_ITEM_BATCH_NUMBER_VALUE:
					settingName = 'Item Batch Number Value';
					break;
				case Queries.SETTING_ITEM_EXPIRY_DATE_VALUE:
					settingName = 'Item Expiry Date Value';
					break;
				case Queries.SETTING_ITEM_MANUFACTURING_DATE_VALUE:
					settingName = 'Item Manufacturing Date Value';
					break;
				case Queries.SETTING_ITEM_SERIAL_NUMBER_VALUE:
					settingName = 'Item Serial Number Value';
					break;
				case Queries.SETTING_ITEM_COUNT_VALUE:
					settingName = 'Item Count Value';
					break;
				case Queries.SETTING_ENABLE_ADDITIONAL_CESS_ON_ITEM:
					settingName = 'Additional Cess On Item Enabled';
					break;
				case Queries.SETTING_ENABLE_REVERSE_CHARGE:
					settingName = 'Reverse Charge Enabled';
					break;
				case Queries.SETTING_ENABLE_DEFAULT_CASH_SALE:
					settingName = 'Default Cash Sale';
					break;
				case Queries.SETTING_SHOW_RECEIVED_AMOUNT_OF_TRANSACTION:
					settingName = 'Show Received Amount Of Transaction';
					break;
				case Queries.SETTING_SHOW_BALANCE_AMOUNT_OF_TRANSACTION:
					settingName = 'Show Balance Amount Of Transaction';
					break;
				case Queries.SETTING_PRINT_COMPANY_EMAIL_ON_PDF:
					settingName = 'Print Company Email Of PDF';
					break;
				case Queries.SETTING_ITEM_TYPE:
					settingName = 'Item Type';
					break;
				case Queries.SETTING_ENABLE_PLACE_OF_SUPPLY:
					settingName = 'Place of Supply Enabled';
					break;
				case Queries.SETTING_ENABLE_EWAY_BILL_NUMBER:
					settingName = 'EWay Bill Number Enabled';
					break;
				case Queries.SETTING_ENABLE_INCLUSIVE_EXCLUSIVE_TAX_ON_TRANSACTION:
					settingName = 'Enable Inclusive Tax on Transaction';
					break;
				case Queries.SETTING_IS_ROUND_OFF_ENABLED:
					settingName = 'Round Off Enabled';
					break;
				case Queries.SETTING_ROUND_OFF_TYPE:
					settingName = 'Round Off Type';
					break;
				case Queries.SETTING_ROUND_OFF_UPTO:
					settingName = 'Round Off Upto';
					break;
				case Queries.SETTING_DELIVERY_CHALLAN_ENABLED:
					settingName = 'Delivery Challan Enabled';
					break;
				case Queries.SETTING_DELIVERY_CHALLAN_RETURN_ENABLED:
					settingName = 'Delivery Challan Return Enabled';
					break;
				case Queries.SETTING_ESTIMATE_ENABLED:
					settingName = 'Estimate Enabled';
					break;
				case Queries.SETTING_IS_COMPOSITE_SCHEME_ENABLED:
					settingName = 'Composite Scheme Enabled';
					break;
				case Queries.SETTING_COMPOSITE_USER_TYPE:
					settingName = 'Composite User Type';
					break;
				case Queries.SETTING_THERMAL_PRINTER_TEXT_SIZE:
					settingName = 'Thermal Printer Text Size';
					break;
				case Queries.SETTING_USE_ESC_POS_CODES_IN_THERMAL_PRINTER:
					settingName = 'Use ESC POS Code in Thermal Printer';
					break;
				case Queries.SETTING_PO_DETAILS_ENABLED:
					settingName = 'PO Detail Enabled';
					break;
				case Queries.SETTING_CURRENT_DATE_FORMAT:
					settingName = 'Current Date Format';
					break;
				case Queries.SETTING_CUSTOM_NAME_FOR_CASH_IN:
					settingName = 'Custom Name For Cash In';
					break;
				case Queries.SETTING_CUSTOM_NAME_FOR_CASH_OUT:
					settingName = 'Custom Name For Cash Out';
					break;
				case Queries.SETTING_CUSTOM_NAME_FOR_SALE:
					settingName = 'Custom Name For Sale';
					break;
				case Queries.SETTING_CUSTOM_NAME_FOR_SALE_RETURN:
					settingName = 'Custom Name For Sale Return';
					break;
				case Queries.SETTING_CUSTOM_NAME_FOR_PURCHASE:
					settingName = 'Custom Name For Purchase';
					break;
				case Queries.SETTING_CUSTOM_NAME_FOR_DELIVERY_CHALLAN:
					settingName = 'Custom Name For Delivery Challan';
					break;
				case Queries.SETTING_CUSTOM_NAME_FOR_PURCHASE_RETURN:
					settingName = 'Custom Name For Purchase Return';
					break;
				case Queries.SETTING_CUSTOM_NAME_FOR_SALE_ORDER:
					settingName = 'Custom Name For Order Form';
					break;
				case Queries.SETTING_CUSTOM_NAME_FOR_PURCHASE_ORDER:
					settingName = 'Custom Name For Purchase Order';
					break;
				case Queries.SETTING_CUSTOM_NAME_FOR_EXPENSE:
					settingName = 'Custom Name For Expense';
					break;
				case Queries.SETTING_CUSTOM_NAME_FOR_INCOME:
					settingName = 'Custom Name For Other Income';
					break;
				case Queries.SETTING_CUSTOM_NAME_FOR_DELIVERY_CHALLAN:
					settingName = 'Custom Name For Delivery Challan';
					break;
				case Queries.SETTING_CUSTOM_NAME_FOR_ESTIMATE:
					settingName = 'Custom Name For Estimate';
					break;
				case Queries.SETTING_CUSTOM_NAME_FOR_TAX_INVOICE:
					settingName = 'Custom Name For Tax Invoice';
					break;
				case Queries.SETTING_PRINT_AMOUNTS_IN_DELIVERY_CHALLAN:
					settingName = 'Print Amount in Delivery Challan';
					break;
				case Queries.SETTING_ENABLE_OPEN_DRAWER_COMMAND:
					settingName = 'Enable Open Drawer Command';
					break;
				case Queries.SETTING_THERMAL_PRINTER_EXTRA_FOOTER_LINES:
					settingName = 'Thermal Printer Extra Footer Lines';
					break;
				default:
					settingName = '';
			}
		}
		return settingName;
	},
	logSetting: function logSetting(allSetting) {
		try {
			var settingObj = {};
			for (var key in allSetting) {
				var settingName = this.getSettingname(key);
				if (settingName != '') {
					settingObj[settingName] = allSetting[key];
				}
			}
			if ((0, _keys2.default)(settingObj).length > 0) {
				this.pushProfile(settingObj);
			}
		} catch (ex) {}
	},

	sendEventToCleverTap: function sendEventToCleverTap(eventName) {
		var properties = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

		try {
			if (typeof localStorage == 'undefined') {
				return;
			}
			var clevertapID = localStorage.getItem('clevertapID');
			if (!clevertapID) {
				var CommonUtility = require('./../Utilities/CommonUtility');
				clevertapID = CommonUtility.generateNewGUID();
				localStorage.setItem('clevertapID', clevertapID);
			}
			var data = [{
				'identity': clevertapID,
				'ts': Math.floor(Date.now() / 1000),
				'type': 'event',
				'evtName': eventName,
				'evtData': properties
			}];

			clevertap.upload(data);
		} catch (ex) {}
	},

	pushException: function pushException(categoryName, label) {
		try {
			var GlobalApp = require('electron').remote.app;
			var GlobalAppVersion = GlobalApp.getVersion();
			var GlobalAppName = GlobalApp.getName();
			analytics.event(categoryName, GlobalAppVersion + '', { evLabel: label, evValue: 0 });
		} catch (error) {
			// eslint-disable-next-line no-console
			console.error(error);
		}
	},

	pushEvent: function pushEvent(eventName) {
		var properties = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

		try {
			if ((0, _keys2.default)(properties).length > 0) {
				for (var action in properties) {
					analytics.event(eventName, action, { evLabel: properties[action], evValue: 0 });
				}
			} else {
				analytics.event(eventName, eventName, { evLabel: eventName, evValue: 0 });
			}
			this.sendEventToCleverTap(eventName, properties);
		} catch (ex) {}
	},

	pushProfile: function pushProfile(userDetails) {
		try {
			if (typeof localStorage == 'undefined') {
				return;
			}
			var clevertapID = localStorage.getItem('clevertapID');
			if (!clevertapID) {
				var CommonUtility = require('./../Utilities/CommonUtility');
				clevertapID = CommonUtility.generateNewGUID();
				localStorage.setItem('clevertapID', clevertapID);
			}
			var campaignId = localStorage.getItem('campaignId');
			if (campaignId) {
				userDetails['campaignId'] = campaignId;
			}
			var profilesQuery = [{
				'type': 'profile',
				'identity': clevertapID,
				'profileData': userDetails
			}];

			clevertap.upload(profilesQuery);
		} catch (ex) {}
	},

	pushScreen: function pushScreen(screenName) {
		try {
			var GlobalApp = require('electron').remote.app;
			var GlobalAppVersion = GlobalApp.getVersion();
			var GlobalAppName = GlobalApp.getName();
			analytics.screen(GlobalAppName, GlobalAppVersion, '', '', screenName);
			this.sendEventToCleverTap('Screen ' + screenName);
		} catch (ex) {}
	}
};
module.exports = AnalyticsHelper;