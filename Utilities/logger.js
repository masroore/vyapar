var winston = require('winston');
var AnalyticsExceptionTracker = require('./../Utilities/AnalyticsExceptionTracker.js');
var logger = new winston.Logger({
	level: 'info',
	transports: [new winston.transports.Console({ colorize: true }), new winston.transports.File({
		filename: 'applicationLogs.log',
		timestamp: function timestamp() {
			return new Date().toString();
		},
		colorize: true
	})],
	emitErrs: true,
	exitOnError: false
});

logger.on('logging', function (transport, level, msg, error) {
	try {
		if (transport.name == 'file' && level == 'error') {
			var message = (msg || '') + ':: ' + (error.stack || error);
			AnalyticsExceptionTracker.logErrorToAnalytics(message, true);
		}
	} catch (e) {}
});

module.exports = logger;