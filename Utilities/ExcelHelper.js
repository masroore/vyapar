var _require$remote = require('electron').remote,
    dialog = _require$remote.dialog,
    shell = _require$remote.shell;

var fs = require('fs');
var path = require('path');
var rem = require('electron');
var ExcelHelper = function ExcelHelper() {
	this.saveExcel = function (name) {
		var showItemInFolder = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

		var file = appPath + '/report.xlsx';
		file = path.normalize(file);
		file = fs.readFileSync(file);
		var fileName = dialog.showSaveDialog(rem.remote.getCurrentWindow(), {
			title: 'Save Excel Sheet',
			defaultPath: name,
			filters: [{ name: 'Excel File', extensions: ['xlsx', 'xls'] }]
		});
		if (!fileName) {
			return;
		}
		fs.writeFile(fileName, file, function (err) {
			if (!err) {
				ToastHelper.success('Downloaded succesfully');
				showItemInFolder && shell.showItemInFolder(fileName);
			}
		});
	};
};

module.exports = ExcelHelper;