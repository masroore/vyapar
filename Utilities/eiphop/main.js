Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.setupBgRendererHandler = exports.setupMainHandler = undefined;

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var logger = require('../../Utilities/logger');
var getPrefix = function getPrefix(level) {
	return '[' + level + ']';
};

var getMsg = function getMsg(level, msg) {
	var dump = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
	return getPrefix(level) + ' ' + msg + ' ' + (dump && (0, _keys2.default)(dump).length > 0 ? ' - ' + (0, _stringify2.default)(dump) : '');
};

var log = {
	error: function error(msg) {
		var dump = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
		return logger.error(getMsg('ERROR', msg, dump));
	},
	warn: function warn(msg) {
		var dump = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
		return logger.warn(getMsg('WARN', msg, dump));
	},
	info: function info(msg) {
		var dump = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
		return logger.info(getMsg('INFO', msg, dump));
	}
};

var setupMainHandler = exports.setupMainHandler = function setupMainHandler(electronModule, availableActions) {
	var enableLogs = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

	enableLogs && log.info('Logs enabled !');
	electronModule.ipcMain.on('asyncRequest', function (event, requestId, action, payload) {
		enableLogs && log.info('Got new request with id = ' + requestId + ', action = ' + action, payload);
		var requestedAction = availableActions[action];
		if (!requestedAction) {
			var error = 'Action "' + action + '" is not available. Did you forget to define it ?';
			log.error(error);
			event.sender.send('errorResponse', { msg: error });
			return;
		}

		requestedAction({ payload: payload }, {
			send: function send(res) {
				return event.sender.send('asyncResponse', requestId, res);
			},
			error: function error(err) {
				return event.sender.send('errorResponse', requestId, err);
			}
		});
	});
};

var setupBgRendererHandler = exports.setupBgRendererHandler = function setupBgRendererHandler(electronModule, availableActions) {
	var enableLogs = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

	enableLogs && log.info('Logs enabled !');
	electronModule.ipcRenderer.on('asyncRequest', function (event, requestId, action, payload, win) {
		enableLogs && log.info('Got new request with id = ' + requestId + ', action = ' + action, payload);
		var requestedAction = availableActions[action];
		if (!requestedAction) {
			var error = 'Action "' + action + '" is not available. Did you forget to define it ?';
			log.error(error);
			event.sender.send('errorResponse', { msg: error });
			return;
		}

		requestedAction({ payload: payload }, {
			send: function send(res) {
				return event.sender.sendTo(win, 'asyncResponse', requestId, res);
			},
			error: function error(err) {
				return event.sender.sendTo(win, 'errorResponse', requestId, err);
			}
		});
	});
};