Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setupFrontendListener = exports.setupBgRendererHandler = exports.setupMainHandler = exports.emit = undefined;

var _main = require('./main');

var _renderer = require('./renderer');

exports.emit = _renderer.emit;
exports.setupMainHandler = _main.setupMainHandler;
exports.setupBgRendererHandler = _main.setupBgRendererHandler;
exports.setupFrontendListener = _renderer.setupFrontendListener;