Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.setupFrontendListener = exports.emit = undefined;

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _extends3 = require('babel-runtime/helpers/extends');

var _extends4 = _interopRequireDefault(_extends3);

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Some apps can require('electron'). React apps cannot.
 * hiphop doesn't assume how you'd be building your app,
 * and accepts electron as a dependancy.
 */

// singleton ipcRenderer
var ipcRenderer = null;
var win = null;
var bgWin = null;

// singleton requests map
var pendingRequests = {};

var randomId = function randomId() {
	return '' + Date.now().toString(36) + Math.random().toString(36).substr(2, 5);
};

// util method to resolve a promise from outside function scope
// https://stackoverflow.com/questions/26150232/resolve-javascript-promise-outside-function-scope

var Deferred = function Deferred() {
	var _this = this;

	(0, _classCallCheck3.default)(this, Deferred);

	this.promise = new _promise2.default(function (resolve, reject) {
		_this.reject = reject;
		_this.resolve = resolve;
	});
};

var emit = exports.emit = function emit(action, payload) {
	// create a request identifier
	var requestId = randomId();

	// send ipc call on asyncRequest channel
	bgWin.send('asyncRequest', requestId, action, payload, win);

	// create a new deferred object and save it to pendingRequests
	// this allows us to resolve the promise from outside (giving a cleaner api to domain objects)
	var dfd = new Deferred();
	pendingRequests = (0, _extends4.default)({}, pendingRequests, (0, _defineProperty3.default)({}, requestId, { dfd: dfd, action: action, payload: payload }));

	// return a promise which will resolve with res
	return dfd.promise;
};

var setupFrontendListener = exports.setupFrontendListener = function setupFrontendListener(electronModule, bgWindow) {
	// setup global ipcRenderer
	ipcRenderer = electronModule.ipcRenderer;
	win = electronModule.remote.getCurrentWindow().id;
	bgWin = bgWindow;

	// expect all responses on asyncResponse channel
	ipcRenderer.on('asyncResponse', function (event, requestId, res) {
		var dfd = pendingRequests[requestId].dfd;

		dfd.resolve(res);

		// remove the pendingRequest
		pendingRequests = (0, _keys2.default)(pendingRequests).filter(function (k) {
			return k !== requestId;
		}).map(function (k) {
			return (0, _defineProperty3.default)({}, k, pendingRequests[k]);
		}).reduce(function (accumulator, current) {
			return (0, _extends4.default)({}, accumulator, current);
		}, {});
	});

	ipcRenderer.on('errorResponse', function (event, requestId, err) {
		var dfd = pendingRequests[requestId].dfd;

		dfd.reject(err);
	});
};