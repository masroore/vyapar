var electron = require('electron');
var renderer = electron.ipcRenderer;
var BrowserWindow = electron.remote.BrowserWindow;
var path = require('path');

var _require = require('./eiphop'),
    setupFrontendListener = _require.setupFrontendListener,
    emit = _require.emit;

var hiddenWindow = new BrowserWindow({ show: false });
hiddenWindow.loadURL(path.join(__dirname, '/../UIComponent/db.html'));
setupFrontendListener(electron, hiddenWindow.webContents);

var DBWindow = {
	send: function send(chanel, data) {
		return emit(chanel, data);
	},
	subscribe: function subscribe(chanel, fn) {
		return renderer.on(chanel, fn);
	},
	unsubscribe: function unsubscribe(chanel, fn) {
		return renderer.removeListener(chanel, fn);
	},
	reload: function reload() {
		return hiddenWindow.reload();
	},
	showDevTools: function showDevTools() {
		return hiddenWindow.webContents.openDevTools();
	},
	show: function show() {
		return hiddenWindow.show();
	},
	hide: function hide() {
		return hiddenWindow.hide();
	},
	getWindow: function getWindow() {
		return hiddenWindow;
	},
	close: function close() {
		return hiddenWindow.close();
	},
	destroy: function destroy() {
		return hiddenWindow.destroy();
	}
};
// DBWindow.show();
// DBWindow.showDevTools();
module.exports = DBWindow;