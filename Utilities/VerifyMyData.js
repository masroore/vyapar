var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DataFix = require('./../DBManager/DataFix.js');
var dataFix = new DataFix();

var VerifyMyData = function () {
	function VerifyMyData() {
		(0, _classCallCheck3.default)(this, VerifyMyData);

		// define all the variables here;
		this.itemVerificationResult = [];
		this.nameBalanceVerificationResult = [];
	}

	(0, _createClass3.default)(VerifyMyData, [{
		key: 'verifyData',
		value: function verifyData() {
			this.itemVerificationResult = dataFix.verifyItemQty();
			this.nameBalanceVerificationResult = dataFix.verifyNameBalances();
			return [this.itemVerificationResult, this.nameBalanceVerificationResult];
		}
	}, {
		key: 'fixDataForCloseBook',
		value: function fixDataForCloseBook() {
			var resultVal = dataFix.fixFaultyDataInDB(this.itemVerificationResult, this.nameBalanceVerificationResult);
			if (resultVal) {
				if (this.nameBalanceVerificationResult.length > 0) {
					var NameCache = require('./../Cache/NameCache.js');
					var nameCache = new NameCache();
					nameCache.reloadNameCache();
				}
				if (this.itemVerificationResult.length > 0) {
					var ItemCache = require('./../Cache/ItemCache.js');
					var itemCache = new ItemCache();
					itemCache.reloadItemCache();
					resultVal = itemCache.fixStockValue(this.itemVerificationResult);
					itemCache.reloadItemCache();
				}
			}
			return resultVal;
		}
	}, {
		key: 'fixFaultyDataInDB',
		value: function fixFaultyDataInDB() {
			var resultVal = dataFix.fixFaultyDataInDB(this.itemVerificationResult, this.nameBalanceVerificationResult);
			if (resultVal) {
				if (this.itemVerificationResult && this.itemVerificationResult.length > 0) {
					var ItemCache = require('./../Cache/ItemCache.js');
					var itemCache = new ItemCache();
					itemCache.reloadItemCache();
					resultVal = itemCache.fixStockValue(this.itemVerificationResult);
					itemCache.reloadItemCache();
				}
			}
			$('#loading').hide();
			if (resultVal) {
				$('#customAlertDialog #customAlertDialogBody').text('The data have been fixed. Please restart your app');
				$('#customAlertDialog').show().dialog({
					'title': 'Issue Fixed',
					'closeX': false,
					'closeOnEscape': false,
					closeText: 'hide',
					modal: true,
					open: function open(event, ui) {
						$('.ui-dialog-titlebar-close').hide();
					}
				});
				$('#customAlertDialog #confirm').text('Ok').show();
				$('#customAlertDialog #cancel').hide();
				$('#customAlertDialog button').off('click').on('click', function () {
					// $('#customAlertDialog').dialog('close').dialog('destroy');
					if (this.id == 'confirm') {
						var _require = require('electron'),
						    ipcRenderer = _require.ipcRenderer;

						ipcRenderer.send('changeURL', 'Index1.html');
					} else {
						$('#customAlertDialog').dialog('close');
						$('#customAlertDialog').dialog('destroy');
					}
				});
			} else {
				$('#customAlertDialog #customAlertDialogBody').text('There was some issues while fixing your data. Please try again or contact our tech support.');
				$('#customAlertDialog').show().dialog({
					'title': 'Issue',
					'closeX': false,
					'closeOnEscape': false,
					closeText: 'hide',
					modal: true,
					open: function open(event, ui) {
						$('.ui-dialog-titlebar-close').hide();
					}
				});
				$('#customAlertDialog #confirm').text('Ok').show();
				$('#customAlertDialog #cancel').hide();
				$('#customAlertDialog button').off('click').on('click', function () {
					$('#customAlertDialog').dialog('close').dialog('destroy');
					if (this.id == 'confirm') {
						return false;
					}
				});
			}
		}
	}]);
	return VerifyMyData;
}();

module.exports = VerifyMyData;