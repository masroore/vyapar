var _StringConstants = require('../Constants/StringConstants');

var _StringConstants2 = _interopRequireDefault(_StringConstants);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MyString = {
	escapeStringForDBInsertion: function escapeStringForDBInsertion(input, convertUndefinedToEmptyString) {
		if (input) {
			if (typeof input !== 'string') {
				input = '' + input;
			}
			return input.replace(/'/g, "''");
		} else {
			if (input === 0) {
				return '0';
			}
			if (convertUndefinedToEmptyString) {
				return '';
			}
			return input;
		}
	},
	invoiceStringForGSTR1Report: function invoiceStringForGSTR1Report(input) {
		if (input) {
			return input.replace(/\s/g, '');
		} else {
			return '';
		}
	},
	isEmpty: function isEmpty(input) {
		if (input && input.trim().length > 0) {
			return false;
		}
		return true;
	},
	compareMyString: function compareMyString(string1, string2) {
		var ignoreCase = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
		var trim = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;
		var typeCompare = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;

		if (!string1 || !string2) {
			return false;
		}
		if (!ignoreCase) {
			if (!trim) {
				return string1 === string2;
			} else {
				return string1.trim() === string2.trim();
			}
		} else if (!trim) {
			return string1.toLowerCase() === string2.toLowerCase();
		} else {
			return string1.trim().toLowerCase() === string2.trim().toLocaleLowerCase();
		}
	},
	isCashSale: function isCashSale(str) {
		return str === _StringConstants2.default.cash || str === _StringConstants2.default.cashSale;
	}
};

module.exports = MyString;