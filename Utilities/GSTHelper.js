var SettingCache = require('../Cache/SettingCache.js');
var settingCache = new SettingCache();
var NameCustomerType = require('../Constants/NameCustomerType.js');
var StringConstants = require('./../Constants/StringConstants.js');

var regGSTIN = new RegExp(StringConstants.GSTINregex);

var GSTHelper = {
	isCompositeTxn: function isCompositeTxn(txnType, partyObj) {
		if (!settingCache.getGSTEnabled()) {
			return false;
		} else if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_ESTIMATE || txnType == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER) {
			var isCompositeEnabled = settingCache.isCompositeSchemeEnabled();
			if (isCompositeEnabled != null) {
				return isCompositeEnabled;
			}
		} else if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
			if (partyObj != null) {
				if (partyObj.getCustomerType() == NameCustomerType.REGISTERED_COMPOSITE) {
					return true;
				}
			}
		}
		return false;
	},
	isGSTINValid: function isGSTINValid(gstinNumber) {
		return regGSTIN.test(gstinNumber);
	},
	getGSTINState: function getGSTINState(gstinNumber) {
		if (!GSTHelper.isGSTINValid(gstinNumber)) {
			return '';
		}
		try {
			return StateCode.getStateName(Number(gstinNumber.substr(0, 2)));
		} catch (e) {
			return '';
		}
	}
};

module.exports = GSTHelper;