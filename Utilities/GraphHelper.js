var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var GraphHelper = {
	getData: function getData(data_list, type) {
		var data_object = {};
		if (type == 'daily') {
			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;

			try {
				for (var _iterator = (0, _getIterator3.default)(data_list), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					var _ref = _step.value;

					var _ref2 = (0, _slicedToArray3.default)(_ref, 2);

					var x = _ref2[0];
					var value = _ref2[1];

					data_object[x] = value;
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}
		} else if (type == 'weekly') {
			var _iteratorNormalCompletion2 = true;
			var _didIteratorError2 = false;
			var _iteratorError2 = undefined;

			try {
				for (var _iterator2 = (0, _getIterator3.default)(data_list), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
					var _ref3 = _step2.value;

					var _ref4 = (0, _slicedToArray3.default)(_ref3, 2);

					var x = _ref4[0];
					var value = _ref4[1];

					var date = new Date(x);

					var week = MyDate.getWeekOfMonth(date);
					var month = MyDate.getMonthName(date);
					var year = MyDate.getLastTwoDigitOfYear(date);
					var group = 'Week: ' + week + ' ' + month + '-' + year;
					if (!(group in data_object)) {
						data_object[group] = 0;
					}
					data_object[group] += value;
				}
			} catch (err) {
				_didIteratorError2 = true;
				_iteratorError2 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion2 && _iterator2.return) {
						_iterator2.return();
					}
				} finally {
					if (_didIteratorError2) {
						throw _iteratorError2;
					}
				}
			}
		} else if (type == 'monthly') {
			var _iteratorNormalCompletion3 = true;
			var _didIteratorError3 = false;
			var _iteratorError3 = undefined;

			try {
				for (var _iterator3 = (0, _getIterator3.default)(data_list), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var _ref5 = _step3.value;

					var _ref6 = (0, _slicedToArray3.default)(_ref5, 2);

					var x = _ref6[0];
					var value = _ref6[1];

					var date = new Date(x);
					var month = MyDate.getMonthName(date);
					var year = MyDate.getLastTwoDigitOfYear(date);
					var group = month + '-' + year;
					if (!(group in data_object)) {
						data_object[group] = 0;
					}
					data_object[group] += value;
				}
			} catch (err) {
				_didIteratorError3 = true;
				_iteratorError3 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError3) {
						throw _iteratorError3;
					}
				}
			}
		} else if (type == 'yearly') {
			var _iteratorNormalCompletion4 = true;
			var _didIteratorError4 = false;
			var _iteratorError4 = undefined;

			try {
				for (var _iterator4 = (0, _getIterator3.default)(data_list), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
					var _ref7 = _step4.value;

					var _ref8 = (0, _slicedToArray3.default)(_ref7, 2);

					var x = _ref8[0];
					var value = _ref8[1];

					var date = new Date(x);
					if (!(date.getFullYear() in data_object)) {
						data_object[date.getFullYear()] = 0;
					}
					data_object[date.getFullYear()] += value;
				}
			} catch (err) {
				_didIteratorError4 = true;
				_iteratorError4 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion4 && _iterator4.return) {
						_iterator4.return();
					}
				} finally {
					if (_didIteratorError4) {
						throw _iteratorError4;
					}
				}
			}
		}
		var date_data = [];
		var amount_data = [];

		for (var x in data_object) {
			date_data.push(x);
			amount_data.push(data_object[x]);
		}
		return { x_axis: date_data, y_axis: amount_data };
	},

	getLastELementsOfArray: function getLastELementsOfArray(data) {
		return data.slice(-6);
	},
	getGraphFormatOption: function getGraphFormatOption() {
		var forDashboard = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

		var options = {
			legend: {
				display: false
			},
			scales: {
				xAxes: [{
					ticks: {
						fontColor: 'black',
						fontSize: '10',
						fontFamily: 'Roboto',
						maxRotation: forDashboard ? 0 : 60
					}
				}],
				yAxes: [{
					ticks: {
						fontColor: 'black',
						fontFamily: 'Roboto'
					}
				}]

			}
		};
		return options;
	},

	getGraphData: function getGraphData(date, data) {
		var forDashboard = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

		var ColorConstants = require('../Constants/ColorConstants');
		var graphPointColor = forDashboard ? ColorConstants.graphPointColorForDashboard : ColorConstants.graphPointColor;
		var pointRadius = forDashboard ? 3 : 4;
		var graphData = {
			labels: date,
			datasets: [{
				label: '',
				fillOpacity: 0,
				strokeColor: ColorConstants.black,
				pointColor: graphPointColor,
				borderColor: graphPointColor,
				borderWidth: 2,
				pointBorderColor: graphPointColor,
				pointBorderWidth: 0,
				pointBackgroundColor: graphPointColor,
				pointHoverRadius: 0,
				pointHoverBackgroundColor: graphPointColor,
				pointHoverBorderColor: graphPointColor,
				pointHoverBorderWidth: 0,
				pointRadius: pointRadius,
				pointHitRadius: 10,
				data: data,
				spanGaps: false,
				backgroundColor: ' transparent'
			}]
		};
		return graphData;
	}
};
module.exports = GraphHelper;