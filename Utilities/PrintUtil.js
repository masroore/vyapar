var PrintUtil = {
	printTransactionUsingThermalPrinter: function printTransactionUsingThermalPrinter(transactionId, successCallback, errorCallBack, cancelCallback) {
		var printUsingThermalPrinter = function printUsingThermalPrinter() {
			try {
				var SettingCache = require('./../Cache/SettingCache.js');
				var ThermalPrinterConstant = require('./../Constants/ThermalPrinterConstant.js');
				var InvoiceTheme = require('./../Constants/InvoiceTheme.js');
				var settingCache = new SettingCache();
				var BaseTransaction = require('./../BizLogic/BaseTransaction.js');
				var baseTransaction = new BaseTransaction();
				var baseTxn = baseTransaction.getTransactionFromId(transactionId);
				var printer = require('printer');
				var printData = function printData(bufferData, printerName) {
					printer.printDirect({
						data: bufferData,
						type: 'RAW',
						printer: printerName,
						success: function success(jobID) {
							if (successCallback) {
								successCallback();
							}
						},
						error: function error(err) {
							if (errorCallBack) {
								errorCallBack();
							}
						}
					});
				};

				var showPrinterListDialog = function showPrinterListDialog(txnData) {
					$('<div id="thermalPrinterChooserForPrinting"></div>').dialog({
						modal: true,
						closeOnEscape: true,
						title: 'Choose Thermal Printer',
						width: 400,
						maxHeight: 600,
						open: function open() {
							var currentDialog = $(this);
							var printByPrinter = function printByPrinter(evt) {
								printData(txnData, this.textContent);
								currentDialog.dialog('destroy'); // Should be destroy only. Can not be close otherwise close function will be called
								$('#thermalPrinterChooserForPrinting').remove();
							};
							var printerListElement = document.createElement('ul');
							printerListElement.classList.add('printerList');
							var optionTemp = document.createElement('li');
							optionTemp.classList.add('printerDetail');
							for (var i in printerList) {
								var printerObject = printerList[i];
								var option = optionTemp.cloneNode();
								option.onclick = printByPrinter;
								option.innerHTML = "<span><img src='./../inlineSVG/print_center.svg' style='width: 30px;' class='printerListName'><span class='printerListName'>" + printerObject.name + '</span></span>';
								printerListElement.append(option);
							}
							$(this).append(printerListElement);
						},
						close: function close() {
							$('#thermalPrinterChooserForPrinting').remove();
							if (cancelCallback) {
								cancelCallback();
							}
						},
						buttons: {
							Cancel: function Cancel() {
								$(this).dialog('close');
							}
						}
					});
				};

				var defaultPrinter = settingCache.getDefaultThermalPrinter();
				var printerList = printer.getPrinters();
				var isDefaultPrinterPresent = false;
				if (defaultPrinter) {
					for (var i in printerList) {
						var printerObject = printerList[i];
						if (defaultPrinter == printerObject.name) {
							isDefaultPrinterPresent = true;
						}
					}
				}

				if (baseTxn) {
					if (settingCache.isThermalPrinterNativeLanguageEnabled()) {
						// image printing for native language
						var PNG = require('pngjs').PNG;
						var fs = require('fs');
						var domtoimage = require('dom-to-image');

						if (settingCache.getTxnThermalTheme() == InvoiceTheme.THERMAL_THEME_2) {
							var ThermalPrinterImageTheme2 = require('./../ThermalPrinter/ThermalPrinterImageTheme2.js');
							var thermalPrinterImage = new ThermalPrinterImageTheme2();
						} else {
							var ThermalPrinterImage = require('./../ThermalPrinter/ThermalPrinterImage.js');
							var thermalPrinterImage = new ThermalPrinterImage();
						}

						var nodeToPrint = document.createElement('table');
						nodeToPrint.id = 'customDivForPrint';
						document.body.append(nodeToPrint);

						var pageSize = settingCache.getThermalPrinterPageSize();

						var canvasWidth = 352;

						if (pageSize == ThermalPrinterConstant.INCH_2_PAGE_SIZE) {
							canvasWidth = 176 * 2;
						} else if (pageSize == ThermalPrinterConstant.INCH_3_PAGE_SIZE) {
							canvasWidth = 176 * 3;
						} else if (pageSize == ThermalPrinterConstant.INCH_4_PAGE_SIZE) {
							canvasWidth = 176 * 4;
						} else if (pageSize == ThermalPrinterConstant.CUSTOMIZED_PAGE_SIZE) {
							var charCount = settingCache.getThermalPrinterCustomizeCharacterCount();
							canvasWidth = charCount * (canvasWidth / 32);
						}

						$('#customDivForPrint').css('width', canvasWidth + 'px');
						$('#customDivForPrint').css('background', 'white');

						var txnHtml = thermalPrinterImage.getHtmlForPrint(baseTxn);
						$('#customDivForPrint').html(txnHtml);

						var txnPrintImageName = 'thermalPrinterTxnImage.png';

						domtoimage.toPng(document.getElementById('customDivForPrint')).then(function (dataUrl) {
							var data = dataUrl.replace(/^data:image\/\w+;base64,/, '');
							var buf = new Buffer(data, 'base64');
							fs.writeFileSync(__dirname + '/../../TransactionImages/' + txnPrintImageName, buf);

							var png = new PNG({
								filterType: 4
							});

							fs.createReadStream(__dirname + '/../../TransactionImages/' + txnPrintImageName).pipe(png).on('parsed', function () {
								thermalPrinterImage.printImageBuffer(this.width, this.height, this.data, function (buff) {
									if (isDefaultPrinterPresent) {
										printData(buff, defaultPrinter);
									} else {
										showPrinterListDialog(buff);
									}
								});
							}).on('error', function (err) {
								console.error(err);
							});
						});
					} else {
						if (settingCache.getTxnThermalTheme() == InvoiceTheme.THERMAL_THEME_2) {
							var ThermalPrinterTextTheme2 = require('./../ThermalPrinter/ThermalPrinterTextTheme2.js');
							var thermalPrinterText = new ThermalPrinterTextTheme2();
						} else {
							var ThermalPrinterText = require('./../ThermalPrinter/ThermalPrinterText.js');
							var thermalPrinterText = new ThermalPrinterText();
						}

						var txnText = thermalPrinterText.getTextForPrint(baseTxn);

						if (isDefaultPrinterPresent) {
							printData(txnText, defaultPrinter);
						} else {
							showPrinterListDialog(txnText);
						}
					}
				}
			} catch (err) {
				if (logger) {
					logger.error('Thermal printer print error: ' + err);
				}
				if (errorCallBack) {
					errorCallBack();
				} else {
					var ErrorCode = require('./../Constants/ErrorCode.js');
					ToastHelper.error(ErrorCode.ERROR_PRINT_THERMAL_PRINTER_FAILED);
				}
			}
		};
		setTimeout(printUsingThermalPrinter, 0);
	}
};

module.exports = PrintUtil;