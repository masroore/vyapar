var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LocalStorageHelper = require('../Utilities/LocalStorageHelper');
var PlansUtility = {
	refreshPlanDetails: function refreshPlanDetails(callback) {
		var refreshPlanDetails = 'planRefreshedDate';
		var offersCheckedDetails = 'offersCheckedLastTimeAt';
		var planRefreshedDate = LocalStorageHelper.getValue(refreshPlanDetails);
		var moment = require('moment');
		var currentDate = moment(new Date()).format('DD/MM/YYYY');
		if (!planRefreshedDate || planRefreshedDate !== currentDate) {
			var successCallback = function successCallback() {
				LocalStorageHelper.setValue(refreshPlanDetails, currentDate);
				LocalStorageHelper.setValue(offersCheckedDetails, Date.now());
				if (callback && typeof callback === 'function') {
					callback();
				}
			};
			this.getPlanDetails(successCallback);
		} else {
			if (callback && typeof callback === 'function') {
				callback();
			}
		}
	},
	getPlanDetails: function getPlanDetails(successCallback, errorCallback) {
		var planKeyForLocalStorage = 'lastPlansData';
		var settings = require('./../Cache/SettingCache');
		var user = new settings();
		var referrer_code = LocalStorageHelper.getValue(LicenseInfoConstant.referralCode);
		var LicenseUtility = require('../Utilities/LicenseUtility');
		var UsageTypeConstants = require('../Constants/UsageTypeConstants');
		var licenseInfoDetails = LicenseUtility.getLicenseInfo();
		var licenseTypeAndValidity = '';
		if ((licenseInfoDetails.licenseUsageType === UsageTypeConstants.TRIAL_PERIOD || licenseInfoDetails.licenseUsageType === UsageTypeConstants.VALID_LICENSE) && licenseInfoDetails.licenseValidityDays <= 3) {
			licenseTypeAndValidity = '&license_type=' + (licenseInfoDetails.licenseUsageType === UsageTypeConstants.TRIAL_PERIOD ? 'trial' : 'active') + '&days_left=' + licenseInfoDetails.licenseValidityDays;
		}

		$.ajax({
			async: true,
			type: 'GET',
			url: domain + '/api/desktop/v4/plans?referrer_code=' + (referrer_code ? encodeURIComponent(referrer_code) : '') + '&country_code=' + encodeURIComponent(user.getCountryCode()) + licenseTypeAndValidity,
			success: function success(data) {
				try {
					localStorage.setItem(planKeyForLocalStorage, (0, _stringify2.default)(data));
				} catch (e) {}
				if (successCallback && typeof successCallback === 'function') {
					successCallback(data);
				}
			},
			error: function error(xhr, ajaxOptions, thrownError) {
				if (errorCallback && typeof errorCallback === 'function') {
					errorCallback();
				}
			}
		});
	},
	checkForOffers: function checkForOffers(onSuccess, onFail) {
		try {
			var offersCheckedDetails = 'offersCheckedLastTimeAt';
			var offersCheckedLastTimeAt = LocalStorageHelper.getValue(offersCheckedDetails);
			var offersCheckedOn = new Date(Number(offersCheckedLastTimeAt));
			var doFetchFromServer = isNaN(offersCheckedOn) || Math.abs(Date.now() - offersCheckedOn.getTime()) > 10800000; // time difference of 3 hours
			doFetchFromServer = true; // Added for flash sale. Should be removed in 2 or 3 days
			if (doFetchFromServer) {
				var successCallback = function successCallback(data) {
					var moment = require('moment');
					var currentDate = moment(new Date()).format('DD/MM/YYYY');
					var refreshPlanDetails = 'planRefreshedDate';
					LocalStorageHelper.setValue(refreshPlanDetails, currentDate);
					LocalStorageHelper.setValue(offersCheckedDetails, Date.now());
					typeof onSuccess === 'function' && onSuccess(data);
				};
				var errorCallback = function errorCallback(err) {
					typeof onFail === 'function' && onFail(err);
				};
				this.getPlanDetails(successCallback, errorCallback);
			} else {
				var planKeyForLocalStorage = 'lastPlansData';
				var lastPlansData = LocalStorageHelper.getValue(planKeyForLocalStorage);
				typeof onSuccess === 'function' && onSuccess(JSON.parse(lastPlansData));
			}
		} catch (err) {
			typeof onFail === 'function' && onFail(err);
		}
	}
};
module.exports = PlansUtility;