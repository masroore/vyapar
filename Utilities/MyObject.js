var _stringify = require("babel-runtime/core-js/json/stringify");

var _stringify2 = _interopRequireDefault(_stringify);

var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require("babel-runtime/helpers/createClass");

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MyObject = function () {
	function MyObject() {
		(0, _classCallCheck3.default)(this, MyObject);
	}

	(0, _createClass3.default)(MyObject, [{
		key: "deepClone",
		value: function deepClone(obj) {
			return JSON.parse((0, _stringify2.default)(obj));
		}
	}, {
		key: "isEmpty",
		value: function isEmpty(obj) {
			for (var key in obj) {
				if (obj.hasOwnProperty(key)) {
					return false;
				}
			}
			return true;
		}
	}]);
	return MyObject;
}();

module.exports = new MyObject();