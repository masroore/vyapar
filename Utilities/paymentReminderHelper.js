var _require = require('electron'),
    ipcRenderer = _require.ipcRenderer;

var logger = require('./logger');

ipcRenderer.on('launchEventsOnScreenFocus', function (event, data) {
	try {
		if (data) {
			var passCodeHelper = require('./../Utilities/passCodeHelper');
			passCodeHelper.login();
		}
	} catch (err) {
		logger.error(err);
	}
});

function openPaymentReminderWindow() {
	if (!window.canCloseDialogue || window.canCloseDialogue()) {
		$('#frameDiv').empty();
		$('#frameDiv').load('PaymentReminder.html', function () {
			$('#modelContainer').css('display', 'block');
			$('#defaultPage').hide();
		});
	}
}

ipcRenderer.on('openPaymentReminderWindow', function (event, data) {
	try {
		if (data) {
			var passCodeHelper = require('./../Utilities/passCodeHelper');
			passCodeHelper.login();
		} else {
			openPaymentReminderWindow();
		}
	} catch (err) {
		logger.error(err);
	}
});