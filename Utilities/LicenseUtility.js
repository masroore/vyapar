var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LicenseInfoConstant = require('../Constants/LicenseInfoConstant.js');
var LocalStorageHelper = require('../Utilities/LocalStorageHelper');
var CovidHelper = require('./../Utilities/CovidHelper.js');
var numberOfDaysFortrial = 30;
var numberOfDaysForSubscriptionBasedTrial = 15;
var UsageTypeConstants = require('./../Constants/UsageTypeConstants.js');
var licenseInfoDetails = {};
var usageType = 'Trial Period';
var licenseValidityDays = 0;

var LicenseUtility = {
	initOrResetLicenseInfo: function initOrResetLicenseInfo() {
		var _licenseInfoDetails;

		var lastValidTime = LocalStorageHelper.getValue(LicenseInfoConstant.lastValidTime);
		licenseInfoDetails = (_licenseInfoDetails = {}, (0, _defineProperty3.default)(_licenseInfoDetails, LicenseInfoConstant.trialStartDate, LocalStorageHelper.getValue(LicenseInfoConstant.trialStartDate, true)), (0, _defineProperty3.default)(_licenseInfoDetails, LicenseInfoConstant.DEVICE_ID, LocalStorageHelper.getValue(LicenseInfoConstant.DEVICE_ID, true)), (0, _defineProperty3.default)(_licenseInfoDetails, LicenseInfoConstant.lastValidTime, lastValidTime), (0, _defineProperty3.default)(_licenseInfoDetails, LicenseInfoConstant.planName, LocalStorageHelper.getValue(LicenseInfoConstant.planName)), (0, _defineProperty3.default)(_licenseInfoDetails, LicenseInfoConstant.licenseCode, LocalStorageHelper.getValue(LicenseInfoConstant.licenseCode)), (0, _defineProperty3.default)(_licenseInfoDetails, LicenseInfoConstant.expiryDate, LocalStorageHelper.getValue(LicenseInfoConstant.expiryDate, true)), (0, _defineProperty3.default)(_licenseInfoDetails, 'isDeviceTimeValid', this.isDeviceTimeValid(lastValidTime)), (0, _defineProperty3.default)(_licenseInfoDetails, 'isLicenseValid', this.isLicenseValid()), (0, _defineProperty3.default)(_licenseInfoDetails, 'licenseUsageType', usageType), (0, _defineProperty3.default)(_licenseInfoDetails, 'licenseValidityDays', licenseValidityDays), _licenseInfoDetails);
	},
	getLicenseInfo: function getLicenseInfo() {
		if ($.isEmptyObject(licenseInfoDetails)) {
			this.initOrResetLicenseInfo();
		}
		return licenseInfoDetails;
	},
	updateLicenseInfo: function updateLicenseInfo() {
		this.initOrResetLicenseInfo();
		this.licenseTab();
	},
	deleteLicenseInfo: function deleteLicenseInfo(lastValidTime) {
		if (!lastValidTime) {
			lastValidTime = new Date().getTime();
		}
		LocalStorageHelper.setValue(LicenseInfoConstant.lastValidTime, lastValidTime);
		LocalStorageHelper.removeValue(LicenseInfoConstant.licenseCode);
		LocalStorageHelper.removeValue(LicenseInfoConstant.expiryDate);
		LocalStorageHelper.removeValue(LicenseInfoConstant.planName);
		LocalStorageHelper.removeValue(LicenseInfoConstant.licenseStatus);
		LocalStorageHelper.removeValue(LicenseInfoConstant.planRefreshedDate);
		this.initOrResetLicenseInfo();
		this.licenseTab();
	},

	isDeviceTimeValid: function isDeviceTimeValid(lastValidTime) {
		var currentTime = new Date().getTime();
		if (currentTime + 2 * 86400000 > lastValidTime) {
			return true;
		}
		return false;
	},

	isLicenseValid: function (_isLicenseValid) {
		function isLicenseValid() {
			return _isLicenseValid.apply(this, arguments);
		}

		isLicenseValid.toString = function () {
			return _isLicenseValid.toString();
		};

		return isLicenseValid;
	}(function () {
		var SettingCache = require('./../Cache/SettingCache.js');
		var settingCache = new SettingCache();
		var currentTime = new Date().getTime();
		var license_purchased = false;
		var lastValidTime = LocalStorageHelper.getValue(LicenseInfoConstant.lastValidTime);
		if (!lastValidTime) {
			lastValidTime = new Date().getTime();
		}
		var licenseCodeInLocalStorage = LocalStorageHelper.getValue(LicenseInfoConstant.licenseCode);
		if (licenseCodeInLocalStorage) {
			expiry_date = new Date(LocalStorageHelper.getValue(LicenseInfoConstant.expiryDate, true));
			expiry_date_time = expiry_date.getTime();
			license_purchased = true;
		} else {
			license_purchased = false;
		}
		if (!this.isDeviceTimeValid(lastValidTime)) {
			isLicenseValid = false;
			$('#textForLicenseExpire').html('Usage is blocked due to misconfigured device time. Please fix the time in your computer.');
			usageType = UsageTypeConstants.DEVICE_TIME_MIS_CONFIGURED;
		} else {
			var licenseStatusFromServer = LocalStorageHelper.getValue(LicenseInfoConstant.licenseStatus);
			if (license_purchased && licenseStatusFromServer && licenseStatusFromServer == 3) {
				usageType = UsageTypeConstants.EXPIRED_LICENSE;
				$('#textForLicenseExpire').html('Your license has expired. Please purchase license before you continue.');
				isLicenseValid = false;
			} else if (license_purchased && expiry_date_time > currentTime) {
				var timeDiff = Math.abs(expiry_date_time - currentTime);
				licenseValidityDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
				isLicenseValid = true;
				usageType = UsageTypeConstants.VALID_LICENSE;
			} else {
				var trial_start_date_file = new Date(LocalStorageHelper.getValue(LicenseInfoConstant.trialStartDate, true));
				var trial_start_date_db = new Date(settingCache.getFreeTrialStartDate());
				expiry_date = new Date();
				if (trial_start_date_file.getTime() < trial_start_date_db.getTime()) {
					trial_start_date_file.setDate(trial_start_date_file.getDate() + numberOfDaysFortrial);
					expiry_date = trial_start_date_file;
				} else {
					trial_start_date_db.setDate(trial_start_date_db.getDate() + numberOfDaysFortrial);
					expiry_date = trial_start_date_db;
				}
				if (CovidHelper.isCovidTrialExtended()) {
					// Adding 30 days extra trial for Covid-19 lockdown period
					expiry_date.setDate(expiry_date.getDate() + 30);
				}
				expiry_date_time = expiry_date.getTime();
				if (expiry_date_time > currentTime) {
					isLicenseValid = true;
					var timeDiff = Math.abs(expiry_date_time - currentTime);
					licenseValidityDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
					usageType = UsageTypeConstants.TRIAL_PERIOD;
				} else {
					// todo handle for empty date
					if (license_purchased) {
						$('#textForLicenseExpire').html('Your license has expired. Please purchase license before you continue.');
						usageType = UsageTypeConstants.EXPIRED_LICENSE;
					} else {
						$('#textForLicenseExpire').html('Your trial period has expired. Please purchase license before you continue.');
						usageType = UsageTypeConstants.BLOCKED;
					}
					isLicenseValid = false;
				}
			}
		}
		return isLicenseValid;
	}),
	licenseTab: function licenseTab() {
		var PlansUtility = require('./PlansUtility');
		$('[data-dynamic=1]').remove();
		var licenseInfo = $('#licenseInfo');
		var licenseTrailPeriod = $('#licenseTrialPeriod');
		licenseInfo.hide();
		licenseTrailPeriod.show();
		if (usageType == UsageTypeConstants.VALID_LICENSE) {
			if (licenseValidityDays > 30) {
				licenseInfo.find('.textInsideMenu').text('License Info');
				licenseInfo.show();
				licenseTrailPeriod.hide();
			} else {
				licenseTrailPeriod.find('.textInsideMenu').html('License expiring in ' + licenseValidityDays + ' days.');
				licenseTrailPeriod.find('.subText').html('Extend License');
			}
		} else if (usageType == UsageTypeConstants.EXPIRED_LICENSE) {
			licenseTrailPeriod.find('.textInsideMenu').html('Your License has expired.');
			licenseTrailPeriod.find('.subText').html('Renew License.');
		} else {
			$('#expandable1').hide();
			if (licenseValidityDays > 10) {
				licenseInfo.find('.textInsideMenu').text('Trial Info');
				licenseInfo.show();
				licenseTrailPeriod.hide();
			} else {
				licenseTrailPeriod.find('.subText').html('See Plan');
				if (licenseValidityDays > 0 && licenseValidityDays <= 10) {
					licenseTrailPeriod.find('.textInsideMenu').html('Your Trial expires in ' + licenseValidityDays + ' days.');
				} else {
					licenseTrailPeriod.find('.textInsideMenu').html('Your Vyapar Trial has ended.');
				}
			}
		}
		usageType != UsageTypeConstants.VALID_LICENSE && PlansUtility.checkForOffers(function hideLicenseTabIfBannerIsShown(bannerDetails) {
			var showOffer = bannerDetails['banner_status'] == 1;
			if (showOffer) {
				licenseTrailPeriod.hide();
				licenseInfo.hide();
			}
		});
	},

	checkLicenseStatus: function checkLicenseStatus() {
		var that = this;
		var Domain = require('./../Constants/Domain');
		var DeviceHelper = require('../Utilities/deviceHelper');
		var deviceInfo = DeviceHelper.getDeviceInfo();
		var domain = Domain.thisDomain;
		$.ajax({
			async: true,
			type: 'GET',
			url: domain + '/license/' + deviceInfo.deviceId,
			context: this,
			success: function success(data) {
				var lastValidTime = data.current_date ? new Date(data.current_date).getTime() : new Date().getTime();
				if (data.status == 1) {
					// No Attached license found with given device id.
					that.deleteLicenseInfo(lastValidTime);
				} else if (data.status == 2 || data.status == 3) {
					LocalStorageHelper.setValue(LicenseInfoConstant.licenseCode, data.license_code);
					LocalStorageHelper.setValue(LicenseInfoConstant.expiryDate, data.expiry_date, true);
					LocalStorageHelper.setValue(LicenseInfoConstant.planName, data.plan);
					LocalStorageHelper.setValue(LicenseInfoConstant.lastValidTime, lastValidTime);
					LocalStorageHelper.setValue(LicenseInfoConstant.licenseStatus, data.status.toString());
					this.updateLicenseInfo();
				}
			},
			error: function error(xhr, ajaxOptions, thrownError) {
				// console.log("Error while registering to server. " + xhr.resposeText);
				// console.dir(thrownError);
			}

		});
	}
};

module.exports = LicenseUtility;