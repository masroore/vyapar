var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var VyaparAPIHelper = {
    fetch: function () {
        var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
            var _ref2$url = _ref2.url,
                url = _ref2$url === undefined ? '' : _ref2$url,
                _ref2$data = _ref2.data,
                data = _ref2$data === undefined ? {} : _ref2$data,
                _ref2$headers = _ref2.headers,
                headers = _ref2$headers === undefined ? {} : _ref2$headers,
                _ref2$method = _ref2.method,
                method = _ref2$method === undefined ? 'GET' : _ref2$method;
            var Domain, domain, defaultHeaders, AuthHelper, tokenDetail, token, response;
            return _regenerator2.default.wrap(function _callee$(_context) {
                while (1) {
                    switch (_context.prev = _context.next) {
                        case 0:
                            Domain = require('./../Constants/Domain');
                            domain = Domain.thisDomain;
                            defaultHeaders = {
                                'Accept': 'application/json',
                                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                            };
                            AuthHelper = require('./TokenForSync');
                            tokenDetail = AuthHelper.readToken();
                            token = '';

                            if (tokenDetail) {
                                token = tokenDetail.auth_token;
                            }

                            if (token) {
                                headers['Authorization'] = 'Bearer ' + token;
                            }
                            headers = (0, _extends3.default)({}, defaultHeaders, headers);
                            response = void 0;

                            $.ajax({
                                async: false,
                                type: 'POST',
                                url: domain + url,
                                headers: headers,
                                data: data,
                                success: function success(data) {
                                    response = data;
                                },
                                error: function error(xhr, ajaxOptions, thrownError) {
                                    response = xhr.responseJSON;
                                }
                            });
                            return _context.abrupt('return', response);

                        case 12:
                        case 'end':
                            return _context.stop();
                    }
                }
            }, _callee, this);
        }));

        function fetch(_x) {
            return _ref.apply(this, arguments);
        }

        return fetch;
    }()
};

module.exports = VyaparAPIHelper;