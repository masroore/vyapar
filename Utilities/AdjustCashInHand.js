/* global jQuery */
module.exports = function ($) {
	function getTemplate() {
		return '\n        <div id="dialogCashAdj" title="Cash In Hand" class="hide">\n            <table class="tableFormat width100 nopad">\n                <tr class="width100">\n                    <td>\n                        <div class="inputLabel">\n                            <div class="labelText">Adjustment </div>\n                            <select id="adjType" class="bottomBorder margintop noOutline width100">\n                                <option value="TXN_TYPE_CASH_ADJ_ADD">Add Cash</option>\n                                <option value="TXN_TYPE_CASH_ADJ_REDUCE">Reduce Cash</option>\n                            </select>\n                        </div>\n                    </td>\n                </tr>\n                <tr>\n                    <td>\n                        <div class="inputLabel">\n                            <div for="adjAmount" class="labelText">\n                                Enter Amount</div>\n                            <input type="number" id="adjAmount" class="bottomBorder noOutline width100 margintop"/>\n                            <input type="hidden" id="adjId" >\n                            <span id="cannotBeEmpty" style="display: none;">This field cannot be left empty</span>\n                        </div>\n                    </td>\n                </tr>\n                <tr>\n                    <td>\n                        <div class="inputLabel">\n                            <div for="adjDate" class="labelText">\n                                Enter Adjustment Date\n                            </div>\n                            <input type="text" class="margintop width100 datepicker bottomBorder noOutline" id="adjDate" placeholder="" />\n                        </div>\n                    </td>\n                </tr>\n                <tr>\n                    <td>\n                        <div class="inputLabel">\n                            <div class="labelText">Description</div>\n                            <input type="text" id="adjDescription" placeholder="Enter Description" class="bottomBorder width100 noOutline margintop"/>\n\n                        </div>\n                    </td>\n                </tr>\n                <tr>\n                    <td>\n                        <div class="floatRight">\n                            <input type="button" id="saveCashAdj" class="terminalButton ripple" value="save"/>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n        </div>';
	}
	function bindListeners() {
		$('#saveCashAdj').off('click').on('click', addCashAdjFunction);
	}
	function addCashAdjFunction() {
		var adjType = $('#adjType').val();
		var adjAmount = MyDouble.convertStringToDouble($('#adjAmount').val());
		var adjDate = MyDate.getDateObj($('#adjDate').val(), 'dd/mm/yyyy', '/');
		var adjDescription = $('#adjDescription').val();
		if (!adjAmount) {
			$('#cannotBeEmpty').css('display', 'block');
		} else {
			var CashAdjustmentLogic = require('./../BizLogic/CasAdjustmentLogic.js');
			var cashAdjustmentLogic = new CashAdjustmentLogic();
			if (this.value != 'Update') {
				var statusCode = cashAdjustmentLogic.saveNewAdjustment(adjType, adjAmount, adjDate, adjDescription);
			} else {
				var adjId = $('#adjId').val();
				statusCode = cashAdjustmentLogic.updateCashAdjustment(adjId, adjType, adjAmount, adjDate, adjDescription);
			}

			MyAnalytics.pushEvent('Cash Adjustment Save');
			if (statusCode == ErrorCode.ERROR_NEW_CASH_ADJUSTMENT_FAILED || statusCode == ErrorCode.ERROR_UPDATE_CASH_ADJUSTMENT_FAILED) {
				ToastHelper.error(statusCode);
			} else {
				ToastHelper.success(statusCode);
			}
			window.onResume && window.onResume();
			$('#dialogCashAdj').dialog('close');
			$('#dialogCashAdj').css('display', 'none');
		}
	}
	var adjustCash = {
		loadAdjustCashInHand: function loadAdjustCashInHand(onCloseCallBack, showDialog) {
			var adjustCashWrapper = $('#adjustCashWrapper');
			// check if adjustCashWrapper exists and has no children
			if (adjustCashWrapper.html() === '') {
				adjustCashWrapper.html(getTemplate());
			}
			showDialog && MyAnalytics.pushEvent('Cash Adjustment Open', { Action: 'Click' });

			var DateFormat = require('./../Constants/DateFormat.js');
			$('#adjDate').datepicker({
				dateFormat: DateFormat.format,
				onSelect: function onSelect() {
					this.focus();
				},
				onClose: function onClose() {
					this.focus();
				}
			});
			var date = MyDate.getDate('d/m/y');
			bindListeners();
			$('#adjDate').val(date);
			$('#adjAmount').val('');
			$('#adjDescription').val('');
			$('#adjId').val('');
			$('#saveCashAdj').val('Save Adjustment');
			if (showDialog) {
				$('#dialogCashAdj').show();
				$('#dialogCashAdj').dialog({
					width: 400,
					modal: true,
					appendTo: '#defaultPage',
					close: function close() {
						$('#dialogCashAdj').dialog('destroy').hide();
						typeof onCloseCallBack == 'function' && onCloseCallBack();
					}
				});
			}
		}
	};
	return adjustCash;
}(jQuery);