// region leads
// during company creation and restoring backup
var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();
var SettingsModel = require('./../Models/SettingsModel.js');
var settingsModel = new SettingsModel();
var FirmCache = require('./../Cache/FirmCache.js');
var firmCache = new FirmCache();
var Queries = require('./../Constants/Queries.js');
var MyDate = require('./../Utilities/MyDate.js');
var VersionConstants = require('./../Constants/VersionConstant.js');
var Domain = require('./../Constants/Domain');
var firm = firmCache.getDefaultFirm();

var app = require('electron').remote.app;

var appVersionNumber = VersionConstants[app.getVersion()] ? VersionConstants[app.getVersion()] : app.getVersion();
var LicenseUtility = require('./../Utilities/LicenseUtility');
var LocalStorageHelper = require('./../Utilities/LocalStorageHelper');
var LicenseInfoConstant = require('./../Constants/LicenseInfoConstant');
var DeviceHelper = require('../Utilities/deviceHelper');
var deviceInfo = DeviceHelper.getDeviceInfo();
var SendLeadsInfo = {
	sendLeadsInformation: function sendLeadsInformation() {
		if (deviceInfo && deviceInfo.deviceId) {
			var referrer_code = LocalStorageHelper.getValue(LicenseInfoConstant.referralCode);
			$.ajax({
				type: 'POST',
				url: Domain.thisDomain + '/api/leads/desktop',
				data: {
					email: firm.firmEmail,
					phone: firm.firmPhone,
					business_name: firm.firmName,
					device_id: deviceInfo.deviceId,
					platform: '2',
					device_name: deviceInfo.deviceName,
					model_no: deviceInfo.modelNo,
					device_os: deviceInfo.deviceOs,
					country_code: '',
					app_version: appVersionNumber,
					referrer_code: referrer_code || ''
				},
				success: function success(data) {
					if (data.status) {
						settingsModel.setSettingKey(Queries.SETTING_LEADS_INFO_SENT);
						settingsModel.UpdateSetting('1', { nonSyncableSetting: true });
					}
				},
				error: function error(xhr, ajaxOptions, thrownError) {}
			});
		}
	},

	sendUsageLeadsInformation: function sendUsageLeadsInformation() {
		var DataLoader = require('./../DBManager/DataLoader.js');
		var dataLoader = new DataLoader();
		var transactionCount = dataLoader.getTransactionCount();
		var partyCount = dataLoader.getPartyCount();
		var imageCount = dataLoader.getImageCount();
		var firstLogin = dataLoader.getLoginTime();
		var firstTransactionDate = dataLoader.getFirstTransactionDate();
		var lastTransactionDate = dataLoader.getLastTransactionDate();
		var amountRelatedData = dataLoader.getAmountRelatedDataForUsageDetails();
		var licenseInfoDetails = LicenseUtility.getLicenseInfo();
		var licenseUsageType = licenseInfoDetails[LicenseInfoConstant.licenseUsageType];
		$.ajax({
			type: 'POST',
			url: Domain.thisDomain + '/api/desktop/usage',
			data: {
				macAddress: deviceInfo.deviceId,
				business_name: firm.firmName,
				email_id: firm.firmEmail,
				phone_number: firm.firmPhone,
				transaction_count: transactionCount,
				party_count: partyCount,
				image_count: imageCount,
				first_login_time: firstLogin,
				first_transaction_date: firstTransactionDate,
				last_transaction_date: lastTransactionDate,
				current_usage_type: licenseUsageType,
				current_user_device_time: MyDate.getDate('y-m-d H:M:S'),
				country_code: '',
				app_version: appVersionNumber,
				sale_count: amountRelatedData.sale_count,
				sale_amount: amountRelatedData.sale_amount,
				purchase_count: amountRelatedData.purchase_count,
				purchase_amount: amountRelatedData.purchase_amount,
				inventory_value: amountRelatedData.stock_value

			},
			success: function success(data) {},
			error: function error(xhr, ajaxOptions, thrownError) {}
		});
	},

	sendingLeadsInfo: function sendingLeadsInfo() {
		// check if new db has diff mac address and if new send leads info
		if (!settingCache.getMacAddress() || settingCache.getMacAddress() != deviceInfo.deviceId) {
			settingsModel.setSettingKey(Queries.SETTING_LEADS_INFO_SENT);
			settingsModel.UpdateSetting('0', { nonSyncableSetting: true });
			settingsModel.setSettingKey(Queries.SETTING_CURRENT_MAC_ADDRESS);
			settingsModel.UpdateSetting(deviceInfo.deviceId, { nonSyncableSetting: true });
			this.sendLeadsInformation();
		} else if (!settingCache.getLeadsSent()) {
			this.sendLeadsInformation();
		}
		this.sendUsageLeadsInformation();
	},

	pushLeadsToServer: function pushLeadsToServer() {
		var isOnline = require('is-online');
		var that = this;
		isOnline().then(function (online) {
			if (online) {
				LicenseUtility.checkLicenseStatus();
				that.sendingLeadsInfo();
			} else {
				var intervalId = setInterval(function () {
					isOnline().then(function (online) {
						if (online) {
							LicenseUtility.checkLicenseStatus();
							that.sendingLeadsInfo();
							clearInterval(intervalId);
						}
					});
				}, 10000); // 10 seconds
			}
		});
	}
};
module.exports = SendLeadsInfo;