var TaxSetup = {
	setupTax: function setupTax(country) {
		var ErrorCode = require('./../Constants/ErrorCode.js');
		var Country = require('./../Constants/Country.js');
		var taxSetupStatus = ErrorCode.SUCCESS;
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var transactionManager = new TransactionManager();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			taxSetupStatus = ErrorCode.FAILED;
			return taxSetupStatus;
		}
		if (country == Country.Countries.INDIA) {
			taxSetupStatus = TaxSetup.setupIndiaTax();
		} else if (Country.isGulfCountryByCountry(country)) {
			taxSetupStatus = TaxSetup.setupGulfTax();
		} else if (country == Country.Countries.NEPAL) {
			taxSetupStatus = TaxSetup.setupNepaliTax();
		}

		if (taxSetupStatus == ErrorCode.SUCCESS) {
			var SettingsModel = require('./../Models/SettingsModel.js');
			var settingsModel = new SettingsModel();
			var Queries = require('./../Constants/Queries.js');
			settingsModel.setSettingKey(Queries.SETTING_TAX_SETUP_COMPLETED);
			taxSetupStatus = settingsModel.UpdateSetting('1');
		}
		if (taxSetupStatus == ErrorCode.ERROR_SETTING_SAVE_SUCCESS) {
			if (!transactionManager.CommitTransaction()) {
				taxSetupStatus = ErrorCode.FAILED;
			} else {
				taxSetupStatus = ErrorCode.SUCCESS;
			}
		}
		transactionManager.EndTransaction();
		return taxSetupStatus;
	},

	setupIndiaTax: function setupIndiaTax() {
		var DataInserter = require('./../DBManager/DataInserter.js');
		var dataInserter = new DataInserter();
		return dataInserter.setupIndiaTax();
	},

	setupGulfTax: function setupGulfTax() {
		var DataInserter = require('./../DBManager/DataInserter.js');
		var dataInserter = new DataInserter();
		return dataInserter.setupGulfTax();
	},

	setupNepaliTax: function setupNepaliTax() {
		var DataInserter = require('./../DBManager/DataInserter.js');
		var dataInserter = new DataInserter();
		return dataInserter.setupNepaliTax();
	}
};

module.exports = TaxSetup;