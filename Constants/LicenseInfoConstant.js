var licenseKeyEncrytionRequired;
var LicenseInfoConstant = {
	trialStartDate: 'trial_start_date',
	referralCode: 'referral_code', // With whose reference user has joined and whose referral code user has used.
	licenseCode: 'license_code',
	licenseStatus: 'license_status',
	passCode: 'user_passcode',
	emailAddress: 'user_email',
	lastValidTime: 'lastValidTime',
	referralCodeSelf: 'user_referral_code_self', // Users referral code, to invite others.
	isIdMigrationDone: 'is_id_migration_done',
	DEVICE_ID: 'device_id',
	device_id_validation_done: 'device_id_validation_done',
	lastNotificationSentAt: 'lastNotificationSentAt',
	expiryDate: 'expiry_date',
	planName: 'plan_name',
	planRefreshedDate: 'planRefreshedDate',
	isWinTdFileMigrationDone: 'isWinTdFileMigrationDone',
	isLicenseValid: 'isLicenseValid',
	licenseUsageType: 'licenseUsageType',
	licenseValidityDays: 'licenseValidityDays',
	getLicenseKeyEncrytionRequired: function getLicenseKeyEncrytionRequired() {
		if (!licenseKeyEncrytionRequired) {
			licenseKeyEncrytionRequired = [this.trialStartDate, this.passCode, this.expiryDate, this.DEVICE_ID];
		}
		return licenseKeyEncrytionRequired;
	}

};

module.exports = LicenseInfoConstant;