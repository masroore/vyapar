var _set = require('babel-runtime/core-js/set');

var _set2 = _interopRequireDefault(_set);

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StateCode = function StateCode() {
	var stateWithCode = {
		1: 'Jammu & Kashmir',
		2: 'Himachal Pradesh',
		3: 'Punjab',
		4: 'Chandigarh',
		5: 'Uttarakhand',
		6: 'Haryana',
		7: 'Delhi',
		8: 'Rajasthan',
		9: 'Uttar Pradesh',
		10: 'Bihar',
		11: 'Sikkim',
		12: 'Arunachal Pradesh',
		13: 'Nagaland',
		14: 'Manipur',
		15: 'Mizoram',
		16: 'Tripura',
		17: 'Meghalaya',
		18: 'Assam',
		19: 'West Bengal',
		20: 'Jharkhand',
		21: 'Odisha',
		22: 'Chattisgarh',
		23: 'Madhya Pradesh',
		24: 'Gujarat',
		25: 'Daman & Diu',
		26: 'Dadra & Nagar Haveli',
		27: 'Maharashtra',
		29: 'Karnataka',
		30: 'Goa',
		31: 'Lakshadweep Islands',
		32: 'Kerala',
		33: 'Tamil Nadu',
		34: 'Pondicherry',
		35: 'Andaman & Nicobar Islands',
		36: 'Telangana',
		37: 'Andhra Pradesh',
		38: 'Ladakh'
	};

	var value;
	var name;

	this.getStateList = function () {
		return (0, _values2.default)(stateWithCode).sort();
	};

	this.getStateName = function (code) {
		return stateWithCode[code];
	};

	this.getStateCode = function (state) {
		var retCode = '';
		$.each(stateWithCode, function (k, v) {
			if (v == state) {
				retCode = k;
				if (k < 10) {
					retCode = '0' + k;
				}
				return false;
			}
		});
		return retCode;
	};

	this.isUnionTerritory = function (stateName) {
		// to get weather it is UTGST or SGST
		var UTGSTList = new _set2.default(['andaman & nicobar islands', 'lakshadweep islands', 'daman & diu', 'chandigarh', 'dadra & nagar haveli', 'ladakh']);
		if (stateName && UTGSTList.has(stateName.toLowerCase())) {
			return true;
		}
		return false;
	};
};

module.exports = new StateCode();