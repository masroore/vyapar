var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var businessTypes = {
	RETAIL: 1,
	WHOLESALE: 2,
	DISTRIBUTOR: 3,
	SERVICE: 4,
	MANUFACTURING: 5,
	OTHERS: 6
};

var BusinessTypes = (0, _extends3.default)({}, businessTypes, {
	getBusinessTypes: function getBusinessTypes() {
		return (0, _values2.default)(businessTypes);
	},
	getBusinessTypeForUI: function getBusinessTypeForUI(businessType) {
		var businessTypeName = '';
		switch (businessType) {
			case businessTypes.RETAIL:
				businessTypeName = 'Retail';
				break;
			case businessTypes.WHOLESALE:
				businessTypeName = 'Wholesale';
				break;
			case businessTypes.DISTRIBUTOR:
				businessTypeName = 'Distributor';
				break;
			case businessTypes.SERVICE:
				businessTypeName = 'Service';
				break;
			case businessTypes.MANUFACTURING:
				businessTypeName = 'Manufacturing';
				break;
			case businessTypes.OTHERS:
				businessTypeName = 'Others';
				break;
			default:
				businessTypeName = 'None';
		}
		return businessTypeName;
	}
});

module.exports = BusinessTypes;