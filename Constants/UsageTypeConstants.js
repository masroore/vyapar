var UsageTypeConstants = {
	TRIAL_PERIOD: 'Trial Period',
	VALID_LICENSE: 'Active License',
	EXPIRED_LICENSE: 'Expired License',
	BLOCKED: 'Blocked',
	DEVICE_TIME_MIS_CONFIGURED: 'Device Time Configured Wrong'

};

module.exports = UsageTypeConstants;