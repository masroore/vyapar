var InvoiceTheme = {
	THEME_0: 0,
	THEME_1: 1,
	THEME_2: 2,
	THEME_3: 3,
	THEME_4: 4,
	THEME_5: 5,
	THEME_6: 6,
	THEME_7: 7,
	THEME_8: 8,
	THEME_9: 9,
	THEME_10: 10,
	THEME_11: 11,
	THEME_12: 12,
	THEME_COLOR_1: '#097aa8',
	THEME_COLOR_24: '#948FE3',
	THERMAL_THEME_1: 0,
	THERMAL_THEME_2: 1,
	SINGLE_COLOR_THEME: 'single color theme',
	DOUBLE_COLOR_THEME: 'double color theme',
	DOUBLE_THEME_COLOR_1: '#232A33',
	DOUBLE_THEME_COLOR_2: '#E01B33',
	DOUBLE_THEME_COLOR_3: '#0B5EB1',
	DOUBLE_THEME_COLOR_4: '#FE941E',
	DOUBLE_THEME_COLOR_5: '#A51502',
	DOUBLE_THEME_COLOR_6: '#FE941E',
	DOUBLE_THEME_COLOR_7: '#2C3F51',
	DOUBLE_THEME_COLOR_8: '#2ECC71',
	DOUBLE_THEME_COLOR_9: '#192B56',
	DOUBLE_THEME_COLOR_10: '#9C88FF',
	DOUBLE_THEME_COLOR_COMBO_1: 1,
	DOUBLE_THEME_COLOR_COMBO_2: 2,
	DOUBLE_THEME_COLOR_COMBO_3: 3,
	DOUBLE_THEME_COLOR_COMBO_4: 4,
	DOUBLE_THEME_COLOR_COMBO_5: 5,

	getDoubleThemeColors: function getDoubleThemeColors(doubleThemeColorId) {
		var primaryColor = this.DOUBLE_THEME_COLOR_1;
		var secondaryColor = this.DOUBLE_THEME_COLOR_2;

		switch (Number(doubleThemeColorId)) {
			case this.DOUBLE_THEME_COLOR_COMBO_1:
				primaryColor = this.DOUBLE_THEME_COLOR_1;
				secondaryColor = this.DOUBLE_THEME_COLOR_2;
				break;
			case this.DOUBLE_THEME_COLOR_COMBO_2:
				primaryColor = this.DOUBLE_THEME_COLOR_3;
				secondaryColor = this.DOUBLE_THEME_COLOR_4;
				break;
			case this.DOUBLE_THEME_COLOR_COMBO_3:
				primaryColor = this.DOUBLE_THEME_COLOR_5;
				secondaryColor = this.DOUBLE_THEME_COLOR_6;
				break;
			case this.DOUBLE_THEME_COLOR_COMBO_4:
				primaryColor = this.DOUBLE_THEME_COLOR_7;
				secondaryColor = this.DOUBLE_THEME_COLOR_8;
				break;
			case this.DOUBLE_THEME_COLOR_COMBO_5:
				primaryColor = this.DOUBLE_THEME_COLOR_9;
				secondaryColor = this.DOUBLE_THEME_COLOR_10;
				break;
		}
		return {
			primary: primaryColor,
			secondary: secondaryColor
		};
	},

	getThemeType: function getThemeType(theme) {
		switch (theme) {
			case this.THEME_0:
			case this.THEME_1:
			case this.THEME_2:
			case this.THEME_3:
			case this.THEME_4:
			case this.THEME_5:
			case this.THEME_6:
			case this.THEME_7:
			case this.THEME_8:
			case this.THEME_9:
			case this.THEME_10:
			case this.THEME_12:
				return this.SINGLE_COLOR_THEME;
			case this.THEME_11:
				return this.DOUBLE_COLOR_THEME;
			default:
				return this.SINGLE_COLOR_THEME;
		}
	}

};
module.exports = InvoiceTheme;