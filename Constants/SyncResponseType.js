var SyncResponseType = {
	CHANGE_LOG: 1,
	GET_IN_SYNC: 2
};
module.exports = SyncResponseType;