var WorkFlowType = {
	TRANSACTION: 1,
	ITEM: 2,
	NAME: 3,
	BANK: 4
};
module.exports = WorkFlowType;