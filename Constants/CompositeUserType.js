/**
 * Created by Ashish on 10/11/2017.
 */
var CompositeUserType = {
	MANUFACTURER: 0,
	TRADER: 1,
	RESTAURANT: 2
};
module.exports = CompositeUserType;