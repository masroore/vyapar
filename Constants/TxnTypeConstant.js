var TxnTypeConstant = {
	TXN_TYPE_SALE: 1,
	TXN_TYPE_PURCHASE: 2,
	TXN_TYPE_CASHIN: 3,
	TXN_TYPE_CASHOUT: 4,
	TXN_TYPE_ROPENBALANCE: 5,
	TXN_TYPE_POPENBALANCE: 6,
	TXN_TYPE_EXPENSE: 7,
	TXN_TYPE_BEGINNING_BALANCE_PAYABLE: 8,
	TXN_TYPE_BEGINNING_BALANCE_RECEIVABLE: 9,
	TXN_TYPE_ADJ_OPENING: 10, // Item opening stock
	TXN_TYPE_ADJ_ADD: 11, // Item adjustment
	TXN_TYPE_ADJ_REDUCE: 12, // Item adjustment
	TXN_TYPE_BANK_OPENING: 13,
	TXN_TYPE_BANK_ADJ_ADD: 14, // Bank adjustement, does  affect cash in hand
	TXN_TYPE_BANK_ADJ_REDUCE: 15, // Bank adjustement, does  affect cash in hand
	TXN_TYPE_BANK_BALANCE_FORWARD: 16,
	TXN_TYPE_BANK_ADJ_ADD_WITHOUT_CASH: 17, // Bank adjustement, does not affect cash in hand
	TXN_TYPE_BANK_ADJ_REDUCE_WITHOUT_CASH: 18, // Bank adjustement, does not affect cash in hand
	TXN_TYPE_CASH_ADJ_ADD: 19, // Cash in hand add adjustment
	TXN_TYPE_CASH_ADJ_REDUCE: 20, // Cash in hand reduce adjustment
	TXN_TYPE_SALE_RETURN: 21, // sale return transaction
	TXN_TYPE_CHEQUE_TRANSFER: 22, // cheque transfer
	TXN_TYPE_PURCHASE_RETURN: 23, // purchase return
	TXN_TYPE_SALE_ORDER: 24, // sale order transaction
	TXN_TYPE_BANK_TO_BANK: 25,
	TXN_TYPE_CASH_OPENINGBALANCE: 26,
	TXN_TYPE_ESTIMATE: 27,
	TXN_TYPE_PURCHASE_ORDER: 28, // purchase order transaction
	TXN_TYPE_OTHER_INCOME: 29,
	TXN_TYPE_DELIVERY_CHALLAN: 30,
	TXN_TYPE_LINKED_CASHIN_OPENINGBALANCE: 31,
	TXN_TYPE_LINKED_CASHOUT_OPENINGBALANCE: 32,
	TXN_TYPE_LOAN_STARTING_BALANCE: 40,
	TXN_TYPE_LOAN_PROCESSING_FEE: 41,
	TXN_TYPE_LOAN_ADJUSTMENT: 42,
	TXN_TYPE_LOAN_EMI_PAYMENT: 43,
	TXN_TYPE_LOAN_CLOSE_BOOK_ADJ: 44,

	getTxnType: function getTxnType(txnType) {
		var TxnTypeList = this;
		for (var i in TxnTypeList) {
			if (TxnTypeList[i] == Number(txnType)) {
				return i;
			}
		}
	},
	// subtype is used only for cheque transfer transactions in bank list view
	// and for loan transactions in bank list view
	getTxnTypeForUI: function getTxnTypeForUI(txnId, subTxnType) {
		txnId = Number(txnId);
		var subTypeId = subTxnType ? Number(subTxnType) : 0;
		var cashDepositIds = [TxnTypeConstant.TXN_TYPE_PURCHASE, TxnTypeConstant.TXN_TYPE_EXPENSE, TxnTypeConstant.TXN_TYPE_CASHOUT, TxnTypeConstant.TXN_TYPE_SALE_RETURN, TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER];
		var txnType = '';
		switch (txnId) {
			case TxnTypeConstant.TXN_TYPE_SALE:
				txnType = 'Sale';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE:
				txnType = 'Purchase';
				break;
			case TxnTypeConstant.TXN_TYPE_CASHIN:
				txnType = 'Payment-In';
				break;
			case TxnTypeConstant.TXN_TYPE_CASHOUT:
				txnType = 'Payment-Out';
				break;
			case TxnTypeConstant.TXN_TYPE_ROPENBALANCE:
				txnType = 'Receivable Opening Balance';
				break;
			case TxnTypeConstant.TXN_TYPE_POPENBALANCE:
				txnType = 'Payable Opening Balance';
				break;
			case TxnTypeConstant.TXN_TYPE_EXPENSE:
				txnType = 'Expense';
				break;
			case TxnTypeConstant.TXN_TYPE_BEGINNING_BALANCE_PAYABLE:
				return 'Payable Beginning Balance';
			case TxnTypeConstant.TXN_TYPE_BEGINNING_BALANCE_RECEIVABLE:
				return 'Receivable Beginning Balance';
			case TxnTypeConstant.TXN_TYPE_ADJ_OPENING:
				txnType = 'Opening Stock';
				break;
			case TxnTypeConstant.TXN_TYPE_ADJ_ADD:
				txnType = 'Add Adjustment ';
				break;
			case TxnTypeConstant.TXN_TYPE_ADJ_REDUCE:
				txnType = 'Reduce Adjustment ';
				break;
			case TxnTypeConstant.TXN_TYPE_BANK_OPENING:
				txnType = 'Opening Balance';
				break;
			case TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD:
				txnType = 'Cash Deposit';
				break;
			case TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE:
				txnType = 'Cash Withdraw';
				break;
			case TxnTypeConstant.TXN_TYPE_BANK_BALANCE_FORWARD:
				txnType = 'Bank balance forward';
				break;
			case TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD_WITHOUT_CASH:
				txnType = 'Bank Adj Increase';
				break;
			case TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE_WITHOUT_CASH:
				txnType = 'Bank Adj Reduce';
				break;
			case TxnTypeConstant.TXN_TYPE_CASH_ADJ_ADD:
				txnType = 'Cash Increase';
				break;
			case TxnTypeConstant.TXN_TYPE_CASH_ADJ_REDUCE:
				txnType = 'Cash Reduce';
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
				txnType = 'Credit Note';
				break;
			case TxnTypeConstant.TXN_TYPE_CHEQUE_TRANSFER:
				if (cashDepositIds.indexOf(Number(subTypeId)) != -1) {
					txnType = 'Cheque Withdraw';
				} else {
					txnType = 'Cheque Deposit';
				}
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
				txnType = 'Debit Note';
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
				txnType = 'Sale Order';
				break;
			case TxnTypeConstant.TXN_TYPE_BANK_TO_BANK:
				txnType = 'Bank To Bank';
				break;
			case TxnTypeConstant.TXN_TYPE_CASH_OPENINGBALANCE:
				txnType = 'Opening Cash in Hand';
				break;
			case TxnTypeConstant.TXN_TYPE_ESTIMATE:
				txnType = 'Estimate';
				break;
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
				txnType = 'Delivery Challan';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
				txnType = 'Purchase Order';
				break;
			case TxnTypeConstant.TXN_TYPE_OTHER_INCOME:
				txnType = 'Other Income';
				break;
			case TxnTypeConstant.TXN_TYPE_LINKED_CASHIN_OPENINGBALANCE:
				txnType = 'Prev. linked amount(Payment-in)';
				break;
			case TxnTypeConstant.TXN_TYPE_LINKED_CASHOUT_OPENINGBALANCE:
				txnType = 'Prev. linked amount(Payment-out)';
				break;
			case TxnTypeConstant.TXN_TYPE_LOAN_STARTING_BALANCE:
				txnType = 'Opening Loan';
				break;
			case TxnTypeConstant.TXN_TYPE_LOAN_PROCESSING_FEE:
				txnType = 'Processing Fee';
				break;
			case TxnTypeConstant.TXN_TYPE_LOAN_ADJUSTMENT:
				txnType = 'Loan Adjustment';
				break;
			case TxnTypeConstant.TXN_TYPE_LOAN_EMI_PAYMENT:
				txnType = 'EMI Paid';
				break;
			case TxnTypeConstant.TXN_TYPE_LOAN_CLOSE_BOOK_ADJ:
				txnType = 'Starting Balance';
				break;
			default:
				txnType = '';
				break;
		}
		return txnType;
	},
	getFilterOptions: function getFilterOptions() {
		return [{
			label: this.getTxnTypeForUI(this.TXN_TYPE_SALE),
			value: this.TXN_TYPE_SALE,
			selected: false
		}, {
			value: this.TXN_TYPE_PURCHASE,
			label: this.getTxnTypeForUI(this.TXN_TYPE_PURCHASE),
			selected: false
		}, {
			label: this.getTxnTypeForUI(this.TXN_TYPE_ADJ_ADD),
			value: this.TXN_TYPE_ADJ_ADD,
			selected: false
		}, {
			label: this.getTxnTypeForUI(this.TXN_TYPE_ADJ_REDUCE),
			value: this.TXN_TYPE_ADJ_REDUCE,
			selected: false
		}, {
			label: this.getTxnTypeForUI(this.TXN_TYPE_ADJ_OPENING),
			value: this.TXN_TYPE_ADJ_OPENING,
			selected: false
		}, {
			label: this.getTxnTypeForUI(this.TXN_TYPE_DELIVERY_CHALLAN),
			value: this.TXN_TYPE_DELIVERY_CHALLAN,
			selected: false
		}, {
			label: this.getTxnTypeForUI(this.TXN_TYPE_ESTIMATE),
			value: this.TXN_TYPE_ESTIMATE,
			selected: false
		}, {
			label: this.getTxnTypeForUI(this.TXN_TYPE_SALE_RETURN),
			value: this.TXN_TYPE_SALE_RETURN,
			selected: false
		}, {
			label: this.getTxnTypeForUI(this.TXN_TYPE_SALE_ORDER),
			value: this.TXN_TYPE_SALE_ORDER,
			selected: false
		}, {
			label: this.getTxnTypeForUI(this.TXN_TYPE_PURCHASE_ORDER),
			value: this.TXN_TYPE_PURCHASE_ORDER,
			selected: false
		}, {
			label: this.getTxnTypeForUI(this.TXN_TYPE_PURCHASE_RETURN),
			value: this.TXN_TYPE_PURCHASE_RETURN,
			selected: false
		}];
	},
	getFilterOptionsForOrders: function getFilterOptionsForOrders(selectedTxn) {
		return [{
			label: this.getTxnTypeForUI(this.TXN_TYPE_SALE_ORDER),
			value: this.TXN_TYPE_SALE_ORDER,
			selected: selectedTxn === this.TXN_TYPE_SALE_ORDER
		}, {
			label: this.getTxnTypeForUI(this.TXN_TYPE_PURCHASE_ORDER),
			value: this.TXN_TYPE_PURCHASE_ORDER,
			selected: selectedTxn === this.TXN_TYPE_PURCHASE_ORDER
		}];
	},
	getFilterOptionsForParties: function getFilterOptionsForParties() {
		return [{
			label: this.getTxnTypeForUI(this.TXN_TYPE_SALE),
			value: this.TXN_TYPE_SALE,
			selected: false
		}, {
			value: this.TXN_TYPE_PURCHASE,
			label: this.getTxnTypeForUI(this.TXN_TYPE_PURCHASE),
			selected: false
		}, {
			label: this.getTxnTypeForUI(this.TXN_TYPE_SALE_RETURN),
			value: this.TXN_TYPE_SALE_RETURN,
			selected: false
		}, {
			label: this.getTxnTypeForUI(this.TXN_TYPE_PURCHASE_RETURN),
			value: this.TXN_TYPE_PURCHASE_RETURN,
			selected: false
		}, {
			label: this.getTxnTypeForUI(this.TXN_TYPE_SALE_ORDER),
			value: this.TXN_TYPE_SALE_ORDER,
			selected: false
		}, {
			label: this.getTxnTypeForUI(this.TXN_TYPE_PURCHASE_ORDER),
			value: this.TXN_TYPE_PURCHASE_ORDER,
			selected: false
		}, {
			label: this.getTxnTypeForUI(this.TXN_TYPE_CASHIN),
			value: this.TXN_TYPE_CASHIN,
			selected: false
		}, {
			label: this.getTxnTypeForUI(this.TXN_TYPE_CASHOUT),
			value: this.TXN_TYPE_CASHOUT,
			selected: false
		}, {
			label: this.getTxnTypeForUI(this.TXN_TYPE_ESTIMATE),
			value: this.TXN_TYPE_ESTIMATE,
			selected: false
		}, {
			label: this.getTxnTypeForUI(this.TXN_TYPE_DELIVERY_CHALLAN),
			value: this.TXN_TYPE_DELIVERY_CHALLAN,
			selected: false
		}, {
			label: this.getTxnTypeForUI(this.TXN_TYPE_ROPENBALANCE),
			value: this.TXN_TYPE_ROPENBALANCE,
			selected: false
		}, {
			label: this.getTxnTypeForUI(this.TXN_TYPE_POPENBALANCE),
			value: this.TXN_TYPE_POPENBALANCE,
			selected: false
		}];
	},
	getFilterOptionsForCheques: function getFilterOptionsForCheques() {
		var _this = this;

		var options = [this.TXN_TYPE_SALE, this.TXN_TYPE_PURCHASE, this.TXN_TYPE_CASHIN, this.TXN_TYPE_CASHOUT, this.TXN_TYPE_EXPENSE, this.TXN_TYPE_SALE_RETURN, this.TXN_TYPE_PURCHASE_RETURN, this.TXN_TYPE_SALE_ORDER, this.TXN_TYPE_PURCHASE_ORDER, this.TXN_TYPE_OTHER_INCOME];
		return options.map(function (txnType) {
			return {
				label: _this.getTxnTypeForUI(txnType),
				value: txnType,
				selected: false
			};
		});
	},
	getFilterOptionsForCashInHand: function getFilterOptionsForCashInHand() {
		var _this2 = this;

		var options = [this.TXN_TYPE_SALE, this.TXN_TYPE_PURCHASE, this.TXN_TYPE_CASHIN, this.TXN_TYPE_CASHOUT, this.TXN_TYPE_EXPENSE, this.TXN_TYPE_BANK_ADJ_ADD, this.TXN_TYPE_BANK_ADJ_REDUCE, this.TXN_TYPE_CASH_ADJ_ADD, this.TXN_TYPE_CASH_ADJ_REDUCE, this.TXN_TYPE_SALE_RETURN, this.TXN_TYPE_PURCHASE_RETURN, this.TXN_TYPE_SALE_ORDER, this.TXN_TYPE_CASH_OPENINGBALANCE, this.TXN_TYPE_PURCHASE_ORDER, this.TXN_TYPE_OTHER_INCOME, this.TXN_TYPE_LOAN_STARTING_BALANCE, this.TXN_TYPE_LOAN_PROCESSING_FEE, this.TXN_TYPE_LOAN_ADJUSTMENT, this.TXN_TYPE_LOAN_EMI_PAYMENT];
		var chequeFilterOption = {
			label: 'CHEQUE',
			value: this.TXN_TYPE_CHEQUE_TRANSFER,
			selected: false
		};
		var filterOptions = options.map(function (txnType) {
			return {
				label: _this2.getTxnTypeForUI(txnType),
				value: txnType,
				selected: false
			};
		});
		filterOptions.splice(10, 0, chequeFilterOption);
		return filterOptions;
	},
	getFilterOptionsForBankAccounts: function getFilterOptionsForBankAccounts() {
		var _this3 = this;

		var options = [this.TXN_TYPE_SALE, this.TXN_TYPE_PURCHASE, this.TXN_TYPE_EXPENSE, this.TXN_TYPE_BANK_OPENING, this.TXN_TYPE_BANK_ADJ_ADD, this.TXN_TYPE_BANK_ADJ_REDUCE, this.TXN_TYPE_BANK_ADJ_ADD_WITHOUT_CASH, this.TXN_TYPE_BANK_ADJ_REDUCE_WITHOUT_CASH, this.TXN_TYPE_BANK_TO_BANK, this.TXN_TYPE_SALE_RETURN, this.TXN_TYPE_PURCHASE_RETURN, this.TXN_TYPE_SALE_ORDER, this.TXN_TYPE_PURCHASE_ORDER, this.TXN_TYPE_OTHER_INCOME, this.TXN_TYPE_CASHIN, this.TXN_TYPE_CASHOUT, this.TXN_TYPE_LOAN_STARTING_BALANCE, this.TXN_TYPE_LOAN_PROCESSING_FEE, this.TXN_TYPE_LOAN_ADJUSTMENT, this.TXN_TYPE_LOAN_EMI_PAYMENT];
		return options.map(function (txnType) {
			return {
				label: _this3.getTxnTypeForUI(txnType),
				value: txnType,
				selected: false
			};
		});
	},
	getFilterOptionsForLoanAccounts: function getFilterOptionsForLoanAccounts() {
		var _this4 = this;

		var options = [this.TXN_TYPE_LOAN_STARTING_BALANCE, this.TXN_TYPE_LOAN_PROCESSING_FEE, this.TXN_TYPE_LOAN_ADJUSTMENT, this.TXN_TYPE_LOAN_EMI_PAYMENT, this.TXN_TYPE_LOAN_CLOSE_BOOK_ADJ];
		return options.map(function (txnType) {
			return {
				label: _this4.getTxnTypeForUI(txnType),
				value: txnType,
				selected: false
			};
		});
	},

	isLoanTxnType: function isLoanTxnType(txnType) {
		return txnType < 50 && txnType >= 40;
	}
};

module.exports = TxnTypeConstant;