/**
 * Created by Root Admin on 9/26/2017.
 */
var RoundOffConstant = {
	ROUND_NEAR: 1,
	ROUND_FLOOR: 2,
	ROUND_CEIL: 3,
	ONES: 1,
	TENS: 10,
	FIFTYS: 50,
	HUNDREDS: 100,
	THOUSANDS: 1000
};

module.exports = RoundOffConstant;