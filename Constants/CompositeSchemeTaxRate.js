/**
 * Created by Ashish on 10/30/2017.
 */
var CompositeSchemeTaxRate = {
	TAXRATE_TRADER: 1,
	TAXRATE_MANUFACTURER: 1,
	TAXRATE_RESTAURANT: 5
};
module.exports = CompositeSchemeTaxRate;