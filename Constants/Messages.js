var Messages = {
	discardChanges: 'Current changes will be discarded. Do you want to continue?'
};

module.exports = Messages;