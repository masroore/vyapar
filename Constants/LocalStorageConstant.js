/**
 * Created by Ashish on 7/23/2018.
 */

var LocalStorageConstant = {
	DEVICE_ID: 'device_id',
	IS_FIRST_SALE_TXN_CREATED: 'isFirstSaleTxnCreated',
	IS_THREE_PURCHASE_TXN_CREATED: 'isThreePurchaseTxnCreated',
	LOGIN_PHONE_NUMBER: 'loginPhoneNumber',
	LOGIN_COUNTRY: 'loginCountry',
	OPEN_INVOICE_PREVIEW_COUNT: 'openInvoicePreviewCount',
	FTU_SHOW_PAYMENT_REMINDER_IN_HEADER: 'ftuShowPaymentReminderInHeader',
	FTU_SHOW_PRINT_CENTRE_ICON_IN_HEADER: 'ftuShowPrintCentreIconInHeader',
	ITEM_IMAGE_LIST: 'itemImageList',
	COVID_TRIAL_EXTEND: 'extendCovidTrial',
	IS_PREMIUM_THEME_CLICKED: 'isPremiumThemeClicked'
};

module.exports = LocalStorageConstant;