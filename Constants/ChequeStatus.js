var ChequeStatus = {
	OPEN: 0,
	CLOSE: 1,
	getChequeStatusByValue: function getChequeStatusByValue(statusValue) {
		if (statusValue == 0) {
			return ChequeStatus.OPEN;
		} else {
			return ChequeStatus.CLOSE;
		}
	},
	getFilterOptionsForCheques: function getFilterOptionsForCheques(selectedTxn) {
		return [{
			label: 'Open',
			value: this.OPEN,
			selected: selectedTxn === this.OPEN
		}, {
			label: 'Close',
			value: this.CLOSE,
			selected: selectedTxn === this.CLOSE
		}];
	}
};

module.exports = ChequeStatus;