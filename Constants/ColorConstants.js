var ColorConstants = {
	'1': '#6ee38f',
	'2': '#d66c6c',
	'21': '#eba479',
	'23': '#c77eb8',
	'7': '#c17592',
	'4': '#dea04b',
	'3': '#59a86f',
	'6': '#f27474',
	'24': '#74d7d6',
	'5': '#5ac398',
	'10': '#123123',
	'11': '#421421',
	'12': '#516231',
	'19': '#919121',
	'20': '#626011',
	'27': '#008000',
	'28': '#eba479',
	'30': '#008000',
	'white': '#fff',
	'black': '#000',
	'graphStrokeColor': '#A6B1E1',
	'graphPointColor': '#097aa8',
	'graphbg': 'rgba(9,123,168,0.2)',
	'graphPointColorForDashboard': '#2CA01C',
	lowStock: '#ff7b33'

};
module.exports = ColorConstants;