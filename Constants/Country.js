var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _from = require('babel-runtime/core-js/array/from');

var _from2 = _interopRequireDefault(_from);

var _set = require('babel-runtime/core-js/set');

var _set2 = _interopRequireDefault(_set);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CountryObject = require('./../BizLogic/CountryObject.js');
var Country = {

	Countries: {
		NOT_SPECIFIED: new CountryObject('XX', 'Not Specified', [], '971', null),
		ABU_DHABI: new CountryObject('AE-AZ', 'Abu Dhabi', ['AED'], '971'),
		AFGHANISTAN: new CountryObject('AF', 'Afghanistan', ['؋'], '93'),
		AJMAN: new CountryObject('AE-AJ', 'Ajman', ['AED'], '971'),
		ALAND_ISLANDS: new CountryObject('AX', 'Aland Islands', ['€'], '358'),
		ALBANIA: new CountryObject('AL', 'Albania', ['L'], '355'),
		ALGERIA: new CountryObject('DZ', 'Algeria', ['د.ج'], '213'),
		AMERICAN_SAMOA: new CountryObject('AS', 'American Samoa', ['€'], '1684'),
		ANDORRA: new CountryObject('AD', 'Andorra', ['€'], '376'),
		ANGOLA: new CountryObject('AO', 'Angola', ['Kz'], '244'),
		ANGUILLA: new CountryObject('AI', 'Anguilla', ['$'], '1264'),
		ANTARCTICA: new CountryObject('AQ', 'Antarctica', ['A$'], '672'),
		ANTIGUA_AND_BARBUDA: new CountryObject('AG', 'Antigua and Barbuda', ['$'], '1268'),
		ARGENTINA: new CountryObject('AR', 'Argentina', ['$'], '54'),
		ARMENIA: new CountryObject('AM', 'Armenia', ['֏'], '374'),
		ARUBA: new CountryObject('AW', 'Aruba', ['ƒ'], '297'),
		AUSTRALIA: new CountryObject('AU', 'Australia', ['$'], '61'),
		AUSTRIA: new CountryObject('AT', 'Austria', ['€'], '43'),
		AZERBAIJAN: new CountryObject('AZ', 'Azerbaijan', ['ман'], '994'),
		//continue
		BAHAMAS: new CountryObject('BS', 'Bahamas', ['$'], '1242'),
		BAHRAIN: new CountryObject('BH', 'Bahrain', ['.د.ب', 'BD'], '973'),
		BANGLADESH: new CountryObject('BD', 'Bangladesh', ['৳'], '880'),
		BARBADOS: new CountryObject('BB', 'Barbados', ['$'], '1246'),
		BELARUS: new CountryObject('BY', 'Belarus', ['p.'], '375'),
		BELGIUM: new CountryObject('BE', 'Belgium', ['€'], '32'),
		BELIZE: new CountryObject('BZ', 'Belize', ['BZ$'], '501'),
		BENIN: new CountryObject('BJ', 'Benin', ['CFA'], '229'),
		BERMUDA: new CountryObject('BM', 'Bermuda', ['$'], '1441'),
		BHUTAN: new CountryObject('BT', 'Bhutan', ['₹'], '975'),
		BOLIVIA_PLURINATIONAL_STATE_OF: new CountryObject('BO', 'Bolivia, Plurinational State of', ['$b'], '591'),
		BONAIRE_SINT_EUSTATIUS_AND_SABA: new CountryObject('BQ', 'Bonaire, Sint Eustatius and Saba', ['$'], '599'),
		BOSNIA_AND_HERZEGOVINA: new CountryObject('BA', 'Bosnia and Herzegovina', ['KM'], '387'),
		BOTSWANA: new CountryObject('BW', 'Botswana', ['P'], '267'),
		BOUVET_ISLAND: new CountryObject('BV', 'Bouvet Island', ['kr'], '47'),
		BRAZIL: new CountryObject('BR', 'Brazil', ['R$'], '55'),
		BRUNEI_DARUSSALAM: new CountryObject('BN', 'Brunei Darussalam', ['$'], '673'),
		BULGARIA: new CountryObject('BG', 'Bulgaria', ['лв'], '359'),
		BURKINA_FASO: new CountryObject('BF', 'Burkina Faso', ['CFA'], '226'),
		BURUNDI: new CountryObject('BI', 'Burundi', ['FBu'], '257'),
		CAMBODIA: new CountryObject('KH', 'Cambodia', ['៛'], '855'),
		CAMEROON: new CountryObject('CM', 'Cameroon', ['CFA'], '237'),
		CANADA: new CountryObject('CA', 'Canada', ['$'], '1'),
		CAPE_VERDE: new CountryObject('CV', 'Cape Verde', ['Esc'], '238'),
		CAYMAN_ISLANDS: new CountryObject('KY', 'Cayman Islands', ['$'], '1345'),
		CENTRAL_AFRICAN_REPUBLIC: new CountryObject('CF', 'Central African Republic', ['CFA'], '236'),
		CHAD: new CountryObject('TD', 'Chad', ['CFA'], '235'),
		CHILE: new CountryObject('CL', 'Chile', ['$'], '56'),
		CHINA: new CountryObject('CN', 'China', ['¥'], '86'),
		CHRISTMAS_ISLAND: new CountryObject('CX', 'Christmas Island', ['$'], '61'),
		COCOS_KEELING_ISLANDS: new CountryObject('CC', 'Cocos (Keeling) Islands', ['$'], '61'),
		COLOMBIA: new CountryObject('CO', 'Colombia', ['$', 'COL$'], '57'),
		COMOROS: new CountryObject('KM', 'Comoros', ['CF'], '269'),
		CONGO: new CountryObject('CG', 'Congo', ['FC'], '243'),
		CONGO_THE_DEMOCRATIC_REPUBLIC_OF_THE: new CountryObject('CD', 'Congo, the Democratic Republic of the', ['Fr.'], '242'),
		COOK_ISLANDS: new CountryObject('CK', 'Cook Islands', ['$'], '682'),
		COSTA_RICA: new CountryObject('CR', 'Costa Rica', ['₡'], '506'),
		COTE_D_IVOIRE: new CountryObject('CI', "Côte d'Ivoire", ['$'], '225'),
		CROATIA: new CountryObject('HR', 'Croatia', ['kn'], '385'),
		CUBA: new CountryObject('CU', 'Cuba', ['₱'], '53'),
		CURACAO: new CountryObject('CW', 'Curaçao', ['$'], '599'),
		CYPRUS: new CountryObject('CY', 'Cyprus', ['£'], '357'),
		CZECH_REPUBLIC: new CountryObject('CZ', 'Czech Republic', ['Kč'], '420'),
		DENMARK: new CountryObject('DK', 'Denmark', ['kr'], '45'),
		DJIBOUTI: new CountryObject('DJ', 'Djibouti', ['Fdj'], '253'),
		DOMINICA: new CountryObject('DM', 'Dominica', ['$'], '1767'),
		DOMINICAN_REPUBLIC: new CountryObject('DO', 'Dominican Republic', ['RD$'], '1'),
		DUBAI: new CountryObject('AE-DU', 'Dubai', ['AED'], '971'),
		ECUADOR: new CountryObject('EC', 'Ecuador', ['$'], '593'),
		EGYPT: new CountryObject('EG', 'Egypt', ['£'], '20'),
		EL_SALVADOR: new CountryObject('SV', 'El Salvador', ['$'], '503'),
		EQUATORIAL_GUINEA: new CountryObject('GQ', 'Equatorial Guinea', ['CFA'], '240'),
		ERITREA: new CountryObject('ER', 'Eritrea', ['Br.'], '291'),
		ESTONIA: new CountryObject('EE', 'Estonia', ['kr'], '372'),
		ETHIOPIA: new CountryObject('ET', 'Ethiopia', ['Br.'], '251'),
		FALKLAND_ISLANDS_MALVINAS: new CountryObject('FK', 'Falkland Islands (Malvinas)', ['£'], '500'),
		FAROE_ISLANDS: new CountryObject('FO', 'Faroe Islands', ['kr'], '298'),
		FIJI: new CountryObject('FJ', 'Fiji', ['$'], '679'),
		FINLAND: new CountryObject('FI', 'Finland', ['€'], '358'),
		FRANCE: new CountryObject('FR', 'France', ['₣'], '33'),
		FRENCH_GUIANA: new CountryObject('GF', 'French Guiana', ['€'], '594'),
		FRENCH_POLYNESIA: new CountryObject('PF', 'French Polynesia', ['CFP'], '689'),
		FRENCH_SOUTHERN_TERRITORIES: new CountryObject('TF', 'French Southern Territories', ['€'], '262'),
		FUJAIRAH: new CountryObject('AE-FU', 'Fujairah', ['AED'], '971'),
		GABON: new CountryObject('GA', 'Gabon', ['CFA'], '241'),
		GAMBIA: new CountryObject('GM', 'Gambia', ['D'], '220'),
		GEORGIA: new CountryObject('GE', 'Georgia', ['ლ'], '995'),
		GERMANY: new CountryObject('DE', 'Germany', ['€'], '49'),
		GHANA: new CountryObject('GH', 'Ghana', ['₵', 'GH¢'], '233'),
		GIBRALTAR: new CountryObject('GI', 'Gibraltar', ['£'], '350'),
		GREECE: new CountryObject('GR', 'Greece', ['€'], '30'),
		GREENLAND: new CountryObject('GL', 'Greenland', ['kr'], '299'),
		GRENADA: new CountryObject('GD', 'Grenada', ['$'], '1473'),
		GUADELOUPE: new CountryObject('GP', 'Guadeloupe', ['€'], '590'),
		GUAM: new CountryObject('GU', 'Guam', ['$'], '1671'),
		GUATEMALA: new CountryObject('GT', 'Guatemala', ['Q'], '502'),
		GUERNSEY: new CountryObject('GG', 'Guernsey', ['£'], '44'),
		GUINEA: new CountryObject('GN', 'Guinea', ['GFr'], '224'),
		GUINEA_BISSAU: new CountryObject('GW', 'Guinea-Bissau', ['CFA'], '245'),
		GUYANA: new CountryObject('GY', 'Guyana', ['$'], '592'),
		HAITI: new CountryObject('HT', 'Haiti', ['G'], '509'),
		HEARD_ISLAND_AND_MCDONALD_ISLANDS: new CountryObject('HM', 'Heard Island and McDonald Islands', ['$'], '672'),
		HOLY_SEE_VATICAN_CITY_STATE: new CountryObject('VA', 'Holy See (Vatican City State)', ['€'], '379'),
		HONDURAS: new CountryObject('HN', 'Honduras', ['L'], '504'),
		HONG_KONG: new CountryObject('HK', 'Hong Kong', ['$'], '852'),
		HUNGARY: new CountryObject('HU', 'Hungary', ['Ft'], '36'),
		ICELAND: new CountryObject('IS', 'Iceland', ['kr'], '354'),
		INDIA: new CountryObject('IN', 'India', ['\u20B9', 'Rs'], '91'),
		INDONESIA: new CountryObject('ID', 'Indonesia', ['Rp'], '62'),
		IRAN_ISLAMIC_REPUBLIC_OF: new CountryObject('IR', 'Iran, Islamic Republic of', ['﷼'], '98'),
		IRAQ: new CountryObject('IQ', 'Iraq', ['ع.د'], '964'),
		IRELAND: new CountryObject('IE', 'Ireland', ['€'], '353'),
		ISLE_OF_MAN: new CountryObject('IM', 'Isle of Man', ['£'], '44'),
		ISRAEL: new CountryObject('IL', 'Israel', ['₪'], '972'),
		ITALY: new CountryObject('IT', 'Italy', ['₤'], '39'),
		JAMAICA: new CountryObject('JM', 'Jamaica', ['J$'], '1876'),
		JAPAN: new CountryObject('JP', 'Japan', ['¥'], '81'),
		JERSEY: new CountryObject('JE', 'Jersey', ['£'], '44'),
		JORDAN: new CountryObject('JO', 'Jordan', ['JD'], '962'),
		KAZAKHSTAN: new CountryObject('KZ', 'Kazakhstan', ['лв'], '7'),
		KENYA: new CountryObject('KE', 'Kenya', ['KSh'], '254'),
		KIRIBATI: new CountryObject('KI', 'Kiribati', ['$'], '686'),
		KOREA_DEMOCRATIC_PEOPLES_REPUBLIC_OF: new CountryObject('KP', "Korea, Democratic People's Republic of", ['₩'], '850'),
		KOREA_REPUBLIC_OF: new CountryObject('KR', 'Korea, Republic of', ['₩'], '82'),
		KUWAIT: new CountryObject('KW', 'Kuwait', ['د.ك', 'KWD'], '965'),
		KYRGYZSTAN: new CountryObject('KG', 'Kyrgyzstan', ['лв'], '996'),
		LAO_PEOPLES_DEMOCRATIC_REPUBLIC: new CountryObject('LA', "Lao People's Democratic Republic", ['₭'], '856'),
		LATVIA: new CountryObject('LV', 'Latvia', ['Ls'], '371'),
		LEBANON: new CountryObject('LB', 'Lebanon', ['ل.ل'], '961'),
		LESOTHO: new CountryObject('LS', 'Lesotho', ['L'], '266'),
		LIBERIA: new CountryObject('LR', 'Liberia', ['$'], '231'),
		LIBYA: new CountryObject('LY', 'Libya', ['ل.د'], '218'),
		LIECHTENSTEIN: new CountryObject('LI', 'Liechtenstein', ['CHF'], '423'),
		LITHUANIA: new CountryObject('LT', 'Lithuania', ['Lt'], '370'),
		LUXEMBOURG: new CountryObject('LU', 'Luxembourg', ['€'], '352'),
		MACAO: new CountryObject('MO', 'Macao', ['P'], '853'),
		MACEDONIA_THE_FORMER_YUGOSLAV_REPUBLIC_OF: new CountryObject('MK', 'Macedonia, the Former Yugoslav Republic of', ['ден'], '389'),
		MADAGASCAR: new CountryObject('MG', 'Madagascar', ['Ar'], '261'),
		MALAWI: new CountryObject('MW', 'Malawi', ['MK'], '265'),
		MALAYSIA: new CountryObject('MY', 'Malaysia', ['RM'], '60'),
		MALDIVES: new CountryObject('MV', 'Maldives', ['.ރ', '/-', 'Rf', 'MVR'], '960'),
		MALI: new CountryObject('ML', 'Mali', ['CFA'], '223'),
		MALTA: new CountryObject('MT', 'Malta', ['₤'], '356'),
		MARSHALL_ISLANDS: new CountryObject('MH', 'Marshall Islands', ['$'], '692'),
		MARTINIQUE: new CountryObject('MQ', 'Martinique', ['€'], '596'),
		MAURITANIA: new CountryObject('MR', 'Mauritania', ['UM'], '222'),
		MAURITIUS: new CountryObject('MU', 'Mauritius', ['₨'], '230'),
		MAYOTTE: new CountryObject('YT', 'Mayotte', ['€'], '262'),
		MEXICO: new CountryObject('MX', 'Mexico', ['$'], '52'),
		MICRONESIA_FEDERATED_STATES_OF: new CountryObject('FM', 'Micronesia, Federated States of', ['$'], '691'),
		MOLDOVA_REPUBLIC_OF: new CountryObject('MD', 'Moldova, Republic of', ['L'], '373'),
		MONACO: new CountryObject('MC', 'Monaco', ['€'], '377'),
		MONGOLIA: new CountryObject('MN', 'Mongolia', ['₮'], '976'),
		MONTENEGRO: new CountryObject('ME', 'Montenegro', ['€'], '382'),
		MONTSERRAT: new CountryObject('MS', 'Montserrat', ['$'], '1664'),
		MOROCCO: new CountryObject('MA', 'Morocco', ['د.م.'], '212'),
		MOZAMBIQUE: new CountryObject('MZ', 'Mozambique', ['MT'], '258'),
		MYANMAR: new CountryObject('MM', 'Myanmar', ['Ks'], '95'),
		NAMIBIA: new CountryObject('NA', 'Namibia', ['$'], '264'),
		NAURU: new CountryObject('NR', 'Nauru', ['$'], '674'),
		NEPAL: new CountryObject('NP', 'Nepal', ['\u20A8', 'रु'], '977'),
		NETHERLANDS: new CountryObject('NL', 'Netherlands', ['ƒ'], '31'),
		NEW_CALEDONIA: new CountryObject('NC', 'New Caledonia', ['CFP'], '687'),
		NEW_ZEALAND: new CountryObject('NZ', 'New Zealand', ['$'], '64'),
		NICARAGUA: new CountryObject('NI', 'Nicaragua', ['C$'], '505'),
		NIGER: new CountryObject('NE', 'Niger', ['CFA'], '227'),
		NIGERIA: new CountryObject('NG', 'Nigeria', ['₦'], '234'),
		NIUE: new CountryObject('NU', 'Niue', ['$'], '683'),
		NORFOLK_ISLAND: new CountryObject('NF', 'Norfolk Island', ['$'], '672'),
		NORTHERN_MARIANA_ISLANDS: new CountryObject('MP', 'Northern Mariana Islands', ['$'], '1670'),
		NORWAY: new CountryObject('NO', 'Norway', ['kr'], '47'),
		OMAN: new CountryObject('OM', 'Oman', ['ر.ع.'], '968'),
		PAKISTAN: new CountryObject('PK', 'Pakistan', ['\u20A8'], '92'),
		PALAU: new CountryObject('PW', 'Palau', ['$'], '680'),
		PALESTINE_STATE_OF: new CountryObject('PS', 'Palestine, State of', ['د.ا'], '970'),
		PANAMA: new CountryObject('PA', 'Panama', ['B/.'], '507'),
		PAPUA_NEW_GUINEA: new CountryObject('PG', 'Papua New Guinea', ['K'], '675'),
		PARAGUAY: new CountryObject('PY', 'Paraguay', ['₲'], '595'),
		PERU: new CountryObject('PE', 'Peru', ['S/.'], '51'),
		PHILIPPINES: new CountryObject('PH', 'Philippines', ['₱'], '63'),
		PITCAIRN: new CountryObject('PN', 'Pitcairn', ['$'], '64'),
		POLAND: new CountryObject('PL', 'Poland', ['zł'], '48'),
		PORTUGAL: new CountryObject('PT', 'Portugal', ['€'], '351'),
		PUERTO_RICO: new CountryObject('PR', 'Puerto Rico', ['$'], '1'),
		QATAR: new CountryObject('QA', 'Qatar', ['ر.ق'], '974'),
		RAS_AL_KHAIMAH: new CountryObject('AE-RK', 'Ras al-Khaimah', ['AED'], '971'),
		REUNION: new CountryObject('RE', 'Réunion', ['€'], '262'),
		ROMANIA: new CountryObject('RO', 'Romania', ['lei'], '40'),
		RUSSIAN_FEDERATION: new CountryObject('RU', 'Russian Federation', ['\u20BD', 'руб'], '7'),
		RWANDA: new CountryObject('RW', 'Rwanda', ['R₣'], '250'),
		SAINT_BARTHELEMY: new CountryObject('BL', 'Saint Barthélemy', ['€'], '590'),
		SAINT_HELENA_ASCENSION_AND_TRISTAN_DA_CUNHA: new CountryObject('SH', 'Saint Helena, Ascension and Tristan da Cunha', ['£'], '290'),
		SAINT_KITTS_AND_NEVIS: new CountryObject('KN', 'Saint Kitts and Nevis', ['$'], '1869'),
		SAINT_LUCIA: new CountryObject('LC', 'Saint Lucia', ['$'], '1758'),
		SAINT_MARTIN_FRENCH_PART: new CountryObject('MF', 'Saint Martin (French part)', ['ƒ'], '590'),
		SAINT_PIERRE_AND_MIQUELON: new CountryObject('PM', 'Saint Pierre and Miquelon', ['€'], '508'),
		SAINT_VINCENT_AND_THE_GRENADINES: new CountryObject('VC', 'Saint Vincent and the Grenadines', ['$'], '1784'),
		SAMOA: new CountryObject('WS', 'Samoa', ['€'], '685'),
		SAN_MARINO: new CountryObject('SM', 'San Marino', ['€'], '378'),
		SAO_TOME_AND_PRINCIPE: new CountryObject('ST', 'Sao Tome and Principe', ['Db'], '239'),
		SHARJAH: new CountryObject('AE-SH', 'Sharjah', ['AED'], '971'),
		SAUDI_ARABIA: new CountryObject('SA', 'Saudi Arabia', ['ر.س', 'SAR'], '966'),
		SENEGAL: new CountryObject('SN', 'Senegal', ['CFA'], '221'),
		SERBIA: new CountryObject('RS', 'Serbia', ['РСД'], '381'),
		SEYCHELLES: new CountryObject('SC', 'Seychelles', ['₨'], '248'),
		SIERRA_LEONE: new CountryObject('SL', 'Sierra Leone', ['Le'], '232'),
		SINGAPORE: new CountryObject('SG', 'Singapore', ['$'], '65'),
		SINT_MAARTEN_DUTCH_PART: new CountryObject('SX', 'Sint Maarten (Dutch part)', ['ƒ'], '1721'),
		SLOVAKIA: new CountryObject('SK', 'Slovakia', ['‎Sk'], '421'),
		SLOVENIA: new CountryObject('SI', 'Slovenia', ['€'], '386'),
		SOLOMON_ISLANDS: new CountryObject('SB', 'Solomon Islands', ['$'], '677'),
		SOMALIA: new CountryObject('SO', 'Somalia', ['Sh'], '252'),
		SOUTH_AFRICA: new CountryObject('ZA', 'South Africa', ['R'], '27'),
		SOUTH_GEORGIA_AND_THE_SOUTH_SANDWICH_ISLANDS: new CountryObject('GS', 'South Georgia and the South Sandwich Islands', ['£'], '500'),
		SOUTH_SUDAN: new CountryObject('SS', 'South Sudan', ['SSP'], '211'),
		SPAIN: new CountryObject('ES', 'Spain', ['€'], '34'),
		SRI_LANKA: new CountryObject('LK', 'Sri Lanka', ['රු'], '94'),
		SUDAN: new CountryObject('SD', 'Sudan', ['£'], '249'),
		SURINAME: new CountryObject('SR', 'Suriname', ['$'], '597'),
		SVALBARD_AND_JAN_MAYEN: new CountryObject('SJ', 'Svalbard and Jan Mayen', ['kr'], '47'),
		SWAZILAND: new CountryObject('SZ', 'Swaziland', ['L'], '268'),
		SWEDEN: new CountryObject('SE', 'Sweden', ['kr'], '46'),
		SWITZERLAND: new CountryObject('CH', 'Switzerland', ['CHF'], '41'),
		SYRIAN_ARAB_REPUBLIC: new CountryObject('SY', 'Syrian Arab Republic', ['ل.س'], '963'),
		TAIWAN_PROVINCE_OF_CHINA: new CountryObject('TW', 'Taiwan, Province of China', ['NT$'], '886'),
		TAJIKISTAN: new CountryObject('TJ', 'Tajikistan', ['SM'], '992'),
		TANZANIA_UNITED_REPUBLIC_OF: new CountryObject('TZ', 'Tanzania, United Republic of', ['Sh'], '255'),
		THAILAND: new CountryObject('TH', 'Thailand', ['฿'], '66'),
		TIMOR_LESTE: new CountryObject('TL', 'Timor-Leste', ['$'], '670'),
		TOGO: new CountryObject('TG', 'Togo', ['CFA'], '228'),
		TOKELAU: new CountryObject('TK', 'Tokelau', ['$'], '690'),
		TONGA: new CountryObject('TO', 'Tonga', ['T$'], '676'),
		TRINIDAD_AND_TOBAGO: new CountryObject('TT', 'Trinidad and Tobago', ['TT$'], '1868'),
		TUNISIA: new CountryObject('TN', 'Tunisia', ['د.ت'], '216'),
		TURKEY: new CountryObject('TR', 'Turkey', ['₺'], '90'),
		TURKMENISTAN: new CountryObject('TM', 'Turkmenistan', ['m'], '993'),
		TURKS_AND_CAICOS_ISLANDS: new CountryObject('TC', 'Turks and Caicos Islands', ['$'], '1649'),
		TUVALU: new CountryObject('TV', 'Tuvalu', ['$'], '688'),
		UGANDA: new CountryObject('UG', 'Uganda', ['Sh'], '256'),
		UKRAINE: new CountryObject('UA', 'Ukraine', ['\u20B4'], '380'),
		UMM_AL_QUWAIN: new CountryObject('AE-UQ', 'Umm al-Quwain', ['AED'], '971'),
		UNITED_ARAB_EMIRATES_UAE: new CountryObject('AE', 'United Arab Emirates (UAE)', ['AED', 'Dh'], '971', ['United Arab Emirates']),
		UNITED_KINGDOM: new CountryObject('GB', 'United Kingdom', ['£'], '44'),
		UNITED_STATE_OF_AMERICA_USA: new CountryObject('US', 'United State of America (USA)', ['$'], '1', ['United States']),
		UNITED_STATES_MINOR_OUTLYING_ISLANDS: new CountryObject('UM', 'United States Minor Outlying Islands', ['$'], '1'),
		URUGUAY: new CountryObject('UY', 'Uruguay', ['$U'], '598'),
		UZBEKISTAN: new CountryObject('UZ', 'Uzbekistan', ['лв'], '998'),
		VANUATU: new CountryObject('VU', 'Vanuatu', ['Vt'], '678'),
		VENEZUELA_BOLIVARIAN_REPUBLIC_OF: new CountryObject('VE', 'Venezuela, Bolivarian Republic of', ['Bs'], '58'),
		VIET_NAM: new CountryObject('VN', 'Viet Nam', ['₫'], '84'),
		VIRGIN_ISLANDS_BRITISH: new CountryObject('VG', 'Virgin Islands, British', ['$'], '1'),
		VIRGIN_ISLANDS_US: new CountryObject('VI', 'Virgin Islands, U.S.', ['$'], '1'),
		WALLIS_AND_FUTUNA: new CountryObject('WF', 'Wallis and Futuna', ['CFP'], '681'),
		WESTERN_SAHARA: new CountryObject('EH', 'Western Sahara', ['د.م.'], '212'),
		YEMEN: new CountryObject('YE', 'Yemen', ['﷼'], '967'),
		ZAMBIA: new CountryObject('ZM', 'Zambia', ['ZK'], '260'),
		ZIMBABWE: new CountryObject('ZW', 'Zimbabwe', ['Z$'], '263')
	},

	isGulfCountryByCountry: function isGulfCountryByCountry(country) {
		if (country) {
			if (country == Country.Countries.UNITED_ARAB_EMIRATES_UAE || country == Country.Countries.SAUDI_ARABIA || country == Country.Countries.BAHRAIN || country == Country.Countries.KUWAIT || country == Country.Countries.OMAN || country == Country.Countries.QATAR || country == Country.Countries.ABU_DHABI || country == Country.Countries.AJMAN || country == Country.Countries.DUBAI || country == Country.Countries.FUJAIRAH || country == Country.Countries.RAS_AL_KHAIMAH || country == Country.Countries.SHARJAH || country == Country.Countries.UMM_AL_QUWAIN) {
				return true;
			}
		}
		return false;
	},

	isGulfCountryByCountryCode: function isGulfCountryByCountryCode(countryCode) {
		if (countryCode && countryCode.toUpperCase) {
			switch (countryCode.toUpperCase()) {
				case Country.Countries.UNITED_ARAB_EMIRATES_UAE.countryCode:
				case Country.Countries.SAUDI_ARABIA.countryCode:
				case Country.Countries.BAHRAIN.countryCode:
				case Country.Countries.KUWAIT.countryCode:
				case Country.Countries.OMAN.countryCode:
				case Country.Countries.QATAR.countryCode:
				case Country.Countries.ABU_DHABI.countryCode:
				case Country.Countries.AJMAN.countryCode:
				case Country.Countries.DUBAI.countryCode:
				case Country.Countries.FUJAIRAH.countryCode:
				case Country.Countries.RAS_AL_KHAIMAH.countryCode:
				case Country.Countries.SHARJAH.countryCode:
				case Country.Countries.UMM_AL_QUWAIN.countryCode:
					return true;
				default:
					return false;
			}
		}
		return false;
	},

	isCountryIndia: function isCountryIndia(countryCode) {
		if (countryCode && countryCode.toUpperCase && countryCode.toUpperCase() == Country.Countries.INDIA.countryCode) {
			return true;
		}
		return false;
	},

	isCountryNepal: function isCountryNepal(countryCode) {
		if (countryCode && countryCode.toUpperCase && countryCode.toUpperCase() == Country.Countries.NEPAL.countryCode) {
			return true;
		}
		return false;
	},

	getCountryFromName: function getCountryFromName(countryName) {
		var notSpecifiedIfNotFound = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

		var countryForName = null;
		try {
			if (countryName && countryName.toUpperCase) {
				// First Find based on new names
				countryName = countryName.toUpperCase().trim();
				$.each(Country.Countries, function (key, country) {
					if (countryName == country.countryName.toUpperCase()) {
						countryForName = country;
					}
				});
				if (countryForName) {
					return countryForName;
				}
				$.each(Country.Countries, function (key, country) {
					if (country.oldNames && country.oldNames.length && country.oldNames.length > 0) {
						for (var i = 0; i < country.oldNames.length; i++) {
							if (countryName == country.oldNames[i].toUpperCase()) {
								countryForName = country;
								return;
							}
						}
					}
				});
				if (countryForName) {
					return countryForName;
				}
			}
		} catch (err) {}
		if (notSpecifiedIfNotFound) {
			return Country.Countries.NOT_SPECIFIED;
		}
		return null;
	},

	getDefaultCountry: function getDefaultCountry() {
		return Country.Countries.INDIA;
	},

	getCountryFromCountryCode: function getCountryFromCountryCode(countryCode) {
		var countryForCode = null;
		if (countryCode && countryCode.toUpperCase) {
			countryCode = countryCode.toUpperCase();
			$.each(Country.Countries, function (key, country) {
				if (countryCode == country.countryCode) {
					countryForCode = country;
				}
			});
		}
		return countryForCode;
	},

	getCurrencySymbol: function getCurrencySymbol(countryCode) {
		try {
			if (countryCode) {
				var countryFromCode = Country.getCountryFromCountryCode(countryCode);
				if (countryFromCode) {
					var currencySymbols = countryFromCode.currencySymbols;
					if (currencySymbols && currencySymbols.length && currencySymbols.length > 0) {
						return currencySymbols[0];
					}
				}
			}
		} catch (ex) {}
		var defaultCountry = Country.getDefaultCountry();
		return defaultCountry.currencySymbols[0];
	},

	getCurrencyList: function getCurrencyList() {
		var currencySymbolSet = new _set2.default();
		try {
			$.each(Country.Countries, function (key, country) {
				var currencySymbols = country.currencySymbols;
				if (currencySymbols && currencySymbols.length && currencySymbols.length > 0) {
					$.each(currencySymbols, function (key, symbol) {
						currencySymbolSet.add(symbol);
					});
				}
			});
		} catch (ex) {}
		var currencySymbolList = (0, _from2.default)(currencySymbolSet);
		return currencySymbolList;
	},

	getCountryList: function getCountryList() {
		var withNotSpecified = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

		var countryList = (0, _values2.default)(Country.Countries);
		var notSpecifiedCountyIndex = countryList.indexOf(Country.Countries.NOT_SPECIFIED);
		if (notSpecifiedCountyIndex > -1) {
			countryList.splice(notSpecifiedCountyIndex, 1);
		}

		countryList.sort(function (lhs, rhs) {
			return lhs.countryName.toUpperCase().localeCompare(rhs.countryName.toUpperCase());
		});

		if (withNotSpecified) {
			countryList.unshift(Country.Countries.NOT_SPECIFIED);
		}

		return countryList;
	}

};

module.exports = Country;