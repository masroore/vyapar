var TransactionStatus = {
	TXN_OPEN: 1,
	TXN_ORDER_OPEN: 2,
	TXN_ORDER_INPROGRESS: 3,
	TXN_ORDER_COMPLETE: 4
};

module.exports = TransactionStatus;