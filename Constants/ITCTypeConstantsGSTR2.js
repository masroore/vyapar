var ITCTypeConstatntsGSTR2 = {
	INPUTS: 0,
	CAPITAL_GOODS: 1,
	INPUT_SERVICES: 2,
	INELIGIBLE: 3
};

module.exports = ITCTypeConstatntsGSTR2;