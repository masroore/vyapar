/**
 * Created by Ashish on 10/11/2017.
 */
var NameCustomerType = {
	UNREGISTERED: 0,
	REGISTERED_NORMAL: 1,
	REGISTERED_COMPOSITE: 2
};
module.exports = NameCustomerType;