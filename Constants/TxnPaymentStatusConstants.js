var TxnTypeConstant = require('../Constants/TxnTypeConstant');
var TxnPaymentStatusConstants = {
	OVERDUE: -1,
	UNPAID: 1,
	PARTIAL: 2,
	PAID: 3,

	getPaymentStatusForUI: function getPaymentStatusForUI(txnType, paymentStatus) {
		var StringConstants = require('./../Constants/StringConstants.js');
		txnType = Number(txnType);
		var paymentStatusString = '';

		if (paymentStatus == TxnPaymentStatusConstants.UNPAID) {
			if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT || txnType == TxnTypeConstant.TXN_TYPE_LINKED_CASHOUT_OPENINGBALANCE || txnType == TxnTypeConstant.TXN_TYPE_LINKED_CASHIN_OPENINGBALANCE) {
				paymentStatusString = StringConstants.paymentStatusUnUsed;
			} else {
				paymentStatusString = StringConstants.paymentStatusUnPaid;
			}
		} else if (paymentStatus == TxnPaymentStatusConstants.PAID) {
			if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT || txnType == TxnTypeConstant.TXN_TYPE_LINKED_CASHOUT_OPENINGBALANCE || txnType == TxnTypeConstant.TXN_TYPE_LINKED_CASHIN_OPENINGBALANCE) {
				paymentStatusString = StringConstants.paymentStatusUsed;
			} else {
				paymentStatusString = StringConstants.paymentStatusPaid;
			}
		} else {
			paymentStatusString = StringConstants.paymentStatusPartial;
		}

		return paymentStatusString;
	},
	getFilterOptions: function getFilterOptions() {
		return [{
			value: 1,
			label: 'Unpaid',
			selected: false
		}, {
			value: 2,
			label: 'Partial',
			selected: false
		}, {
			value: 3,
			label: 'Paid',
			selected: false
		}];
	}
};

module.exports = TxnPaymentStatusConstants;