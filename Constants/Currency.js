var CurrencyObject = require('./../BizLogic/CurrencyObject.js');
var Currency = {

	Currencies: {
		INDIAN_RUPEE: new CurrencyObject('\u20B9', 'Rupees', 'Paisa'),
		PAKISTANI_RUPEE: new CurrencyObject('Rs', 'Rupees', 'Paisa'),
		NEPLAI_RUPEE: new CurrencyObject('रु', 'Rupees', 'Paisa'),
		US_DOLLAR: new CurrencyObject('$', 'Dollars', 'Cents'),
		SAUDI_RIYAL1: new CurrencyObject('ر.س', 'Riyals', 'Halalah'),
		SAUDI_RIYAL2: new CurrencyObject('SAR', 'Riyals', 'Halalah'),
		EMIRATI_DIRHAM: new CurrencyObject('AED', 'Dirhams', 'Fils')
	},

	getCurrencyFromSymbol: function getCurrencyFromSymbol(symbol) {
		var currencyFromSymbol = null;
		try {
			if (symbol) {
				$.each(Currency.Currencies, function (key, currency) {
					if (symbol.toUpperCase() == currency.symbol.toUpperCase()) {
						currencyFromSymbol = currency;
					}
				});
			}
		} catch (err) {}
		return currencyFromSymbol;
	}

};

module.exports = Currency;