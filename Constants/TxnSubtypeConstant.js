var TxnSubtypeConstant = {
	null: 0,
	'invoice': 1,
	'tax invoice': 2,
	getTypeName: function getTypeName(id) {
		return id == 2 ? 'tax invoice' : 'invoice';
	}

};

module.exports = TxnSubtypeConstant;