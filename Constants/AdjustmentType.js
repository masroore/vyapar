var AdjustmentType = {
	AdjustmentList: ['Add Stock', 'Reduce Stock'],
	getListOfAdjustmentType: function getListOfAdjustmentType() {
		return AdjustmentType.AdjustmentList;
	}
};

module.exports = AdjustmentType;