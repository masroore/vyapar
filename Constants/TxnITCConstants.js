var TxnITCConstants = {
	ITC_ELIGIBLE: 0,
	ITC_INELIGIBLE_17_5: 1,
	ITC_INELIGIBLE_OTHERS: 2,
	ITC_ELIGIBLE_GOODS: 3,
	getDropDownOptions: function getDropDownOptions() {
		return [{
			value: this.ITC_ELIGIBLE,
			label: 'Eligible for ITC - Input'
		}, {
			value: this.ITC_ELIGIBLE_GOODS,
			label: 'Eligible for ITC - Goods'
		}, {
			value: this.ITC_INELIGIBLE_17_5,
			label: 'Ineligible as Per Section 17(5)'
		}, {
			value: this.ITC_INELIGIBLE_OTHERS,
			label: 'Ineligible - Other'
		}];
	}
};

module.exports = TxnITCConstants;