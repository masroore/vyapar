var PDFReportConstant = {
	SALE_REPORT: 'SaleReport',
	TAX_REPORT: 'TaxReport',
	GSTR1_REPORT: 'GSTR1 Report',
	GSTR2_REPORT: 'GSTR2 Report',
	GSTR3_REPORT: 'GSTR3B Report',
	GSTR4_REPORT: 'GSTR4 Report',
	GSTR9_REPORT: 'GSTR9 Report',
	GSTR9A_REPORT: 'GSTR9A Report',
	PARTY_REPORT: 'PartyReport',
	INCOME_REPORT: 'Other IncomeReport',
	EXPENSE_REPORT: 'ExpenseReport',
	PURCHASE_REPORT: 'PurchaseReport',
	TAX_RATE_REPORT: 'TaxRateReport',
	CASHFLOW_REPORT: 'CashFlowReport',
	DISCOUNT_REPORT: 'DiscountReport',
	CUSTOM_REPORT: 'AllTransactionsReport',
	ORDER_TXN_REPORT: 'OrderTxnReport',
	ORDER_ITEM_REPORT: 'OrderItemReport',
	OTHER_INCOME_ITEM_REPORT: 'OtherIncomeItemReport',
	ITEM_REPORT_BY_PARTY: 'ItemReportByParty',
	EXPENSE_ITEM_REPORT: 'ExpenseItemReport',
	STOCK_DETAIL_REPORT: 'StockDetailReport',
	STOCK_SUMMARY_REPORT: 'StockSummaryReport',
	PROFIT_AND_LOSS_REPORT: 'ProfitAndLossReport',
	INCOME_CATEGORY_REPORT: 'OtherIncomeCategoryReport',
	EXPENSE_CATEGORY_REPORT: 'ExpenseCategoryReport',
	LOW_STOCK_SUMMARY_REPORT: 'LowStockSummaryReport',
	PARTY_GROUP_SALE_PURCHASE_REPORT: 'PartyGroupSalePurchaseReport',
	ITEM_CATEGORY_SALE_PURCHASE_REPORT: 'Sale Purchase Report By Item Category',
	PARTY_REPORT_BY_ITEM: 'PartyReportByItem',
	ITEM_DETAIL_REPORT: 'ItemDetailReport',
	BANK_STATEMENT: 'Bank Statement',
	DAYBOOK: 'Daybook',
	ITEM_STOCK_TRACKING_REPORT: 'Item Stock Tracking Report',
	ITEM_WISE_PROFIT_AND_LOSS_REPORT: 'Item Wise Profit And Loss Report',
	PRINT: 'Print',
	STOCK_SUMMARY_BY_ITEM_CATEGORY: 'StockSummaryByItemCategory',
	SALE_AGING_REPORT: 'Sale Aging Report',
	PARTY_SALE_PURCHASE_REPORT: 'SalePurchaseReportByParty',
	LOAN_STATEMENT_REPORT: 'Loan Statement Report',
	BALANCE_SHEET: 'BalanceSheet'
};

module.exports = PDFReportConstant;