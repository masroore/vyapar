var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var businessCategories = {
	ACCOUNTING_CA: 'Accounting & CA',
	INTERIOR_DESIGNER: 'Interior Designer',
	AUTOMOBILES_AUTO_PARTS: 'Automobiles/ Auto parts',
	SALON_SPA: 'Salon & Spa',
	LIQUOR_STORE: 'Liquor Store',
	BOOK_STATIONARY_STORE: 'Book / Stationary store',
	CONSTRUCTION_MATERIALS_EQUIPMENT: 'Construction Materials & Equipment',
	REPAIRING_PLUMBING_ELECTRICIAN: 'Repairing/ Plumbing/ Electrician',
	CHEMICALS_FERTILIZERS: 'Chemicals & Fertilizers',
	COMPUTER_EQUIPMENTS_SOFTWARES: 'Computer Equipments & Softwares',
	ELECTRICAL_ELECTRONICS_EQUIPMENTS: 'Electrical & Electronics Equipments',
	FASHION_ACCESSORY_COSMETICS: 'Fashion Accessory/ Cosmetics',
	TAILORING_BOUTIQUE: 'Tailoring/ Boutique',
	FRUIT_AND_VEGETABLE: 'Fruit And Vegetable',
	KIRANA_GENERAL_MERCHANT: 'Kirana/ General Merchant',
	FMCG_PRODUCTS: 'FMCG Products',
	DAIRY_FARM_PRODUCTS_POULTRY: 'Dairy Farm Products/ Poultry',
	FURNITURE: 'Furniture',
	GARMENT_FASHION_HOSIERY: 'Garment/Fashion & Hosiery',
	JEWELLERY_GEMS: 'Jewellery & Gems',
	PHARMACY_MEDICAL: 'Pharmacy/ Medical',
	HARDWARE_STORE: 'Hardware Store',
	INDUSTRIAL_MACHINERY_EQUIPMENT: 'Industrial Machinery & Equipment',
	MOBILE_ACCESSORIES: 'Mobile & Accessories',
	NURSERY_PLANTS: 'Nursery/ Plants',
	PETROLEUM_BULK_STATIONS_TERMINALS_PETROL: 'Petroleum Bulk Stations & Terminals/ Petrol',
	RESTAURANT_HOTEL: 'Restaurant/ Hotel',
	FOOTWEAR: 'Footwear',
	PAPER_PAPER_PRODUCTS: 'Paper & Paper Products',
	SWEET_SHOP_BAKERY: 'Sweet Shop/ Bakery',
	GIFTS_TOYS: 'Gifts & Toys',
	LAUNDRY_WASHING_DRY_CLEAN: 'Laundry/ Washing/ Dry clean',
	COACHING_TRAINING: 'Coaching & Training',
	RENTING_LEASING: 'Renting & Leasing',
	FITNESS_CENTER: 'Fitness Center',
	OIL_GAS: 'Oil & Gas',
	REAL_ESTATE: 'Real Estate',
	NGO_CHARITABLE_TRUST: 'NGO & Charitable trust',
	TOURS_TRAVELS: 'Tours & Travels',
	OTHERS: 'Others'
};
var BusinessCategories = (0, _extends3.default)({}, businessCategories, {
	getBusinessCategories: function getBusinessCategories() {
		return (0, _values2.default)(businessCategories);
	}
});

module.exports = BusinessCategories;