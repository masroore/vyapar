var TaxCodeConstants = {
	SGST: 1,
	CGST: 2,
	IGST: 3,
	OTHER: 4,
	CESS: 5,
	Exempted: 6,
	STATE_SPECIFIC_CESS: 7,
	GST: null,
	taxRate: 0,
	taxGroup: 1,
	GSTFromDB: 0
};
module.exports = TaxCodeConstants;