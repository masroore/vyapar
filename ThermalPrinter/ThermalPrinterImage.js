var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ThermalPrinterImage = function ThermalPrinterImage() {
	var BaseTransaction = require('../BizLogic/BaseTransaction.js');
	var SettingCache = require('../Cache/SettingCache.js');
	var FirmCache = require('../Cache/FirmCache.js');
	var firmCache = new FirmCache();
	var ItemCache = require('../Cache/ItemCache.js');
	var itemCache = new ItemCache();
	var ItemUnitMappingCache = require('../Cache/ItemUnitMappingCache.js');
	var itemUnitMappingCache = new ItemUnitMappingCache();
	var ItemUnitCache = require('../Cache/ItemUnitCache.js');
	var itemUnitCache = new ItemUnitCache();
	var TransactionFactory = require('../BizLogic/TransactionFactory.js');
	var TxnTypeConstant = require('../Constants/TxnTypeConstant.js');
	var TaxCodeConstants = require('../Constants/TaxCodeConstants.js');
	var TransactionPDFPaperSize = require('../Constants/TransactionPDFPaperSize.js');
	var DataLoader = require('../DBManager/DataLoader.js');
	var CurrencyHelper = require('../Utilities/CurrencyHelper.js');
	var dataLoader = new DataLoader();
	var TaxCodeCache = require('../Cache/TaxCodeCache.js');
	var taxCodeCache = new TaxCodeCache();
	var Queries = require('../Constants/Queries.js');
	var StateCode = require('../Constants/StateCode.js');
	var MyString = require('./../Utilities/MyString.js');
	var TransactionPrintSettings = require('../ReportHTMLGenerator/TransactionPrintSettings.js');
	var ThermalPrinterConstant = require('../Constants/ThermalPrinterConstant.js');
	var PrinterCommands = require('./../ThermalPrinter/PrinterCommands.js');
	var settingCache = new SettingCache();
	var UDFCache = require('./../Cache/UDFCache.js');
	var TransactionPrintHelper = require('./../Utilities/TransactionPrintHelper.js');
	var TransactionHTMLGenerator = require('./../ReportHTMLGenerator/TransactionHTMLGenerator');
	var stateTaxPlace = 'SGST';
	var exemptedTaxString = 'Exmp.';

	var NORMAL = 1;
	var BOLD = 2;
	var UNDERLINE = 3;

	var SMALL = 4;
	var MEDIUM = 5;
	var LARGE = 6;

	var LEFT = 1;
	var RIGHT = 2;
	var CENTER = 3;

	var NO_OF_CHARS = 48;
	var SR_WIDTH = 3;
	var ITEM_NAME_WIDTH = 12;
	var QUANTITY_WIDTH = 11;
	var UNIT_PRICE_WIDTH = 11;
	var AMOUNT_WIDTH = 11;

	var SPACE_CHAR = ' ';
	var DASH_CHAR = '-';
	var STAR_CHAR = '*';

	this.txn;
	this.pageSize = ThermalPrinterConstant.INCH_3_PAGE_SIZE;
	this.textSize = ThermalPrinterConstant.MEDIUM;
	this.isThereAnyLineItems = false;
	this.stateTaxText = 'SGST';
	this.textBuffer;
	this.htmlString = '';
	this.fontSize = 1.05;

	if (settingCache.getThermalPrinterTextSize() == ThermalPrinterConstant.SMALL && settingCache.getThermalPrinterPageSize() == ThermalPrinterConstant.INCH_2_PAGE_SIZE) {
		this.fontSize = 1;
	}

	this.fontCompanyHeaderSize = this.fontSize * 1.3;

	this.getStyle = function () {
		// fontSize = fontSize - (6 - settingCache.getPrintTextSize())/100;

		var style = '<style>';
		style += '.imagePrintBody td { vertical-align:top}';
		style += '.imagePrintBody, .imagePrintBody table {' + 'font-size:' + this.fontSize + 'em; margin:0px;padding:1px;word-break:break-all;}';
		style += ' tr.border_bottom td { border-bottom:1pt solid black;}';
		style += ' tr.border_top td {border-top:1pt solid black;}';
		style += '</style>';

		return style;
	};

	// ----------------------------------------------------- PRINT IMAGE-----------------------------------------------------

	this.printImageBuffer = function (width, height, data, callback) {
		this.print(PrinterCommands.INIT);
		this.lineFeed();

		// Get pixel rgba in 2D array
		var pixels = [];
		for (var i = 0; i < height; i++) {
			var line = [];
			for (var j = 0; j < width; j++) {
				var idx = width * i + j << 2;
				line.push({
					r: data[idx],
					g: data[idx + 1],
					b: data[idx + 2],
					a: data[idx + 3]
				});
			}
			pixels.push(line);
		}

		var imageBuffer_array = [];
		for (var i = 0; i < height; i++) {
			for (var j = 0; j < Math.ceil(width / 8); j++) {
				var byte = 0x0;
				for (var k = 0; k < 8; k++) {
					var pixel = pixels[i][j * 8 + k];

					// Image overflow
					if (pixel === undefined) {
						pixel = {
							a: 0,
							r: 0,
							g: 0,
							b: 0
						};
					}

					if (pixel.a > 126) {
						// checking transparency
						var grayscale = parseInt(0.2126 * pixel.r + 0.7152 * pixel.g + 0.0722 * pixel.b);

						if (grayscale < 128) {
							// checking color
							var mask = 1 << 7 - k; // setting bitwise mask
							byte |= mask; // setting the correct bit to 1
						}
					}
				}

				imageBuffer_array.push(byte);
				// imageBuffer = Buffer.concat([imageBuffer, new Buffer([byte])]);
			}
		}

		var imageBuffer = Buffer.from(imageBuffer_array);

		// Print raster bit image
		// GS v 0
		// 1D 76 30	m	xL xH	yL yH d1...dk
		// xL = (this.width >> 3) & 0xff;
		// xH = 0x00;
		// yL = this.height & 0xff;
		// yH = (this.height >> 8) & 0xff;
		// https://reference.epson-biz.com/modules/ref_escpos/index.php?content_id=94

		// Check if width/8 is decimal
		if (width % 8 != 0) {
			width += 8;
		}

		this.print(new Buffer([0x1d, 0x76, 0x30, 48]));
		this.print(new Buffer([width >> 3 & 0xff]));
		this.print(new Buffer([0x00]));
		this.print(new Buffer([height & 0xff]));
		this.print(new Buffer([height >> 8 & 0xff]));

		// append data
		this.print(imageBuffer);
		this.printLineFeed();
		this.printLineFeed();
		this.printLineFeed();
		this.lineFeed();
		this.lineFeed();

		var extraFooterLines = settingCache.getThermalPrinterExtraFooterLines();

		for (var _i = 0; _i < extraFooterLines; _i++) {
			this.printLineFeed();
			this.lineFeed();
		}

		if (settingCache.isAutoCutPaperEnabled()) {
			this.cutPaper();
		}

		var numberOfCopies = settingCache.getThermalPrintCopyCount();

		if (numberOfCopies > 1) {
			var currentTextBuffer = this.textBuffer;
			for (var _i2 = 1; _i2 < numberOfCopies; _i2++) {
				this.textBuffer = Buffer.concat([this.textBuffer, currentTextBuffer]);
			}
		}

		if (settingCache.isOpenDrawerCommandEnabled()) {
			this.print(PrinterCommands.OPEN_DRAWER_KICK_2);
			this.print(PrinterCommands.OPEN_DRAWER_KICK_5);
		}

		callback(this.textBuffer);
	};

	this.getHtmlForPrint = function (txn) {
		if (!txn) {
			return false;
		} else {
			this.txn = txn;
		}

		this.textBuffer = null;

		this.generateHtmlForPrint();

		return this.htmlString;
	};

	this.generateHtmlForPrint = function () {
		this.htmlString += "<html style='margin:0px;padding:0px;'>" + '<head>' + this.getStyle() + "</head><body><div class='imagePrintBody'>";

		this.printHeader();

		var txnType = this.txn.getTxnType();
		if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_ESTIMATE || txnType == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txnType == TxnTypeConstant.TXN_TYPE_EXPENSE || txnType == TxnTypeConstant.TXN_TYPE_OTHER_INCOME || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
			this.printBody();
		}
		var printAmounts = txnType != TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || settingCache.printAmountDetailsInDeliveryChallan();
		if (printAmounts) {
			this.printTotalBalanceSection();
		}

		if (settingCache.isPrintTaxDetailEnabled() && this.isThereAnyLineItems && printAmounts) {
			this.printTaxDetails();
		}

		if (settingCache.isPrintDescriptionEnabled()) {
			this.printDescription();
		}

		if (this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE && settingCache.isPrintTermsAndConditionSaleInvoiceEnabled() || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER && settingCache.isPrintTermsAndConditionSaleOrderEnabled() || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN && settingCache.isPrintTermsAndConditionDeliveryChallanEnabled() || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE && settingCache.isPrintTermsAndConditionEstimateQuotationEnabled())
			// if(settingCache.isPrintTermsAndConditionEnabled())
			{
				this.printTermAndCondition();
			}

		this.htmlString += '</div></body>' + '</html>';
	};

	this.printHeader = function () {
		var firm = firmCache.getTransactionFirmWithDefault(this.txn);

		if (firm) {
			this.printCompanyHeader(firm);

			if (StateCode.isUnionTerritory(firm.getFirmState())) {
				this.stateTaxText = 'UTGST';
			}
		}
		this.printTransactionHeader();
		this.printPartyInfoAndTxnHeaderData();
	};

	this.printCompanyHeader = function (firm) {
		if (firm.getFirmName() && settingCache.isPrintCompanyNameEnabled()) {
			var firmNameArray = firm.getFirmName().split('\n');
			if (firmNameArray && firmNameArray.length > 0) {
				for (var i = 0; i < firmNameArray.length; i++) {
					// this.printTextInCenter(firmNameArray[i], LARGE, true);
					this.htmlString += "<p style='text-align: center; font-size:" + this.fontCompanyHeaderSize + "em;'>" + firmNameArray[i] + '</p>';
					this.htmlString += '<br>';
				}
			}
		}

		if (settingCache.isPrintCompanyAddressEnabled() && firm.getFirmAddress().trim()) {
			var firmAddressArray = firm.getFirmAddress().split('\n');
			if (firmAddressArray && firmAddressArray.length > 0) {
				for (var _i3 = 0; _i3 < firmAddressArray.length; _i3++) {
					this.htmlString += "<p style='text-align: center'>" + firmAddressArray[_i3] + '</p>';
					this.htmlString += '<br>';
				}
			}
		}

		if (firm.getFirmState()) {
			this.htmlString += "<p style='text-align: center'>" + 'State: ' + StateCode.getStateCode(firm.getFirmState()) + ' (' + firm.getFirmState() + ')' + '</p>';
		}

		if (firm.getFirmPhone() && settingCache.isPrintCompanyNumberEnabled()) {
			this.htmlString += "<p style='text-align: center'>" + firm.getFirmPhone() + '</p>';
		}

		if (firm.getFirmEmail() && settingCache.isCompnayEmailEnabledOnPrint()) {
			this.htmlString += "<p style='text-align: center'>" + firm.getFirmEmail() + '</p>';
		}

		if (settingCache.getGSTEnabled()) {
			this.printGSTIN(firm);
		} else {
			this.printTIN(firm);
		}

		if (StateCode.isUnionTerritory(firm.getFirmState())) {
			this.stateTaxText = 'UTGST';
		}

		var udfCache = new UDFCache();
		var udfArray = firm.getUdfObjectArray();
		for (var _i4 = 0; _i4 < udfArray.length; _i4++) {
			var udfValue = udfArray[_i4];
			var udfModel = udfCache.getUdfFirmModelByFirmAndFieldId(firm.getFirmId(), udfValue.getUdfFieldId());
			if (udfModel.getUdfFieldPrintOnInvoice() == 1 && udfModel.getUdfFieldStatus() == 1) {
				var udfFieldValue = udfValue.getUdfFieldValue() || '';
				this.htmlString += "<p style='text-align: center'>" + udfModel.getUdfFieldName() + ': ' + udfFieldValue + '</p>';
			}
		}
	};

	this.printTIN = function (firm) {
		if ((this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN) && settingCache.isPrintTINEnabled() && firm.getFirmTin()) {
			this.htmlString += '<p ALIGN=CENTER>' + settingCache.getTINText() + ': ' + firm.getFirmTin() + '</p>';
		}
	};

	this.printGSTIN = function (firm) {
		if ((this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN) && settingCache.isPrintTINEnabled() && firm.getFirmHSNSAC()) {
			this.htmlString += '<p ALIGN=CENTER>' + 'GSTIN : ' + firm.getFirmHSNSAC() + '</p>';
		}
	};

	this.printTransactionHeader = function () {
		var transactionFactory = new TransactionFactory();
		var transactionHTMLGenerator = new TransactionHTMLGenerator();
		this.htmlString += '<p ALIGN=CENTER>' + transactionFactory.getTransTypeStringForTransactionPDF(this.txn.getTxnType(), this.txn.getTxnSubType(), false, transactionHTMLGenerator.isNonTaxBill(this.txn)) + '</p>';
	};

	this.printPartyInfoAndTxnHeaderData = function () {
		try {
			var party = this.txn.getNameRef();

			this.htmlString += "<table style='width:100%;'>";
			this.htmlString += '<tr>';
			this.htmlString += '<td width = 50% align=left valign=top>';

			var txnRefText = '';

			var partyHeaderData = '';
			var txnHeaderData = '';

			if (party) {
				var partyFullName = '';
				if (this.txn.getDisplayName() && this.txn.getDisplayName().trim()) {
					partyFullName = this.txn.getDisplayName().trim();
				} else {
					partyFullName = party.getFullName();
				}
				if (partyFullName) {
					partyFullName = partyFullName.replace(/(?:\r\n|\r|\n)/g, ' ');
				}

				var txnDateStr = 'Date: ' + MyDate.getDate('d-m-y', this.txn.getTxnDate());

				this.htmlString += '<p>' + '<span >' + partyFullName + '</span>' + '</p>';

				if (!MyString.isEmpty(party.getAddress())) {
					var partyAddress = party.getAddress();
					partyAddress = partyAddress.replace('\n', ' ');

					this.htmlString += '<p>' + partyAddress + '</p>';
				}

				if (settingCache.getGSTEnabled() && settingCache.isPlaceOfSupplyEnabled()) {
					if (this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
						var placeOfSupply = this.txn.getPlaceOfSupply();
						if (MyString.isEmpty(placeOfSupply)) {
							if (this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
								var firm = firmCache.getTransactionFirmWithDefault(this.txn);
								placeOfSupply = firm.getFirmState();
							} else {
								placeOfSupply = party.getContactState();
							}
						}

						if (!MyString.isEmpty(placeOfSupply)) {
							var partyState = placeOfSupply;

							this.htmlString += '<p>' + partyState + '</p>';
						}
					}
				}

				if (!MyString.isEmpty(party.getPhoneNumber())) {
					this.htmlString += '<p>' + party.getPhoneNumber() + '</p>';
				}

				if (settingCache.isTINNumberEnabled()) {
					if (settingCache.getGSTEnabled()) {
						if (!MyString.isEmpty(party.getGstinNumber())) {
							this.htmlString += '<p>' + 'GSTIN:' + party.getGstinNumber() + '</p>';
						}
					} else {
						if (!MyString.isEmpty(party.getTinNumber())) {
							this.htmlString += '<p>' + settingCache.getTINText() + ': ' + party.getTinNumber() + '</p>';
						}
					}
				}

				var partyUDF = party.getUdfObjectArray();
				var udfCache = new UDFCache();
				if (partyUDF && partyUDF.length > 0) {
					for (var i = 0; i < partyUDF.length; i++) {
						var value = partyUDF[i];
						var model = udfCache.getUdfPartyModelByFieldId(value.getUdfFieldId());
						if (value.getUdfFieldValue() && model.getUdfFieldPrintOnInvoice() == 1) {
							var displayValue = value.getDisplayValue(model);
							this.htmlString += '<p>' + model.getUdfFieldName() + ': ' + displayValue + '</p>';
						}
					}
				}

				this.htmlString += '</td>';
				this.htmlString += '<td width=50% align = right valign=top>';

				if (settingCache.isPrintPartyShippingAddressEnabled() && (!MyString.isEmpty(party.getShippingAddress()) || !MyString.isEmpty(this.txn.getShippingAddress()))) {
					var shipAddressHeader = '';
					switch (this.txn.getTxnType()) {
						case TxnTypeConstant.TXN_TYPE_SALE:
						case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
						case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
							shipAddressHeader = 'Ship To:';
							break;
						case TxnTypeConstant.TXN_TYPE_PURCHASE:
						case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
						case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
							shipAddressHeader = 'Ship From:';
							break;
					}

					if (!MyString.isEmpty(shipAddressHeader)) {
						this.htmlString += '<p>' + shipAddressHeader + '</p>';
						var shippingAddress = this.txn.getShippingAddress() || party.getShippingAddress();
						shippingAddress = shippingAddress.split('\n');
						if (shippingAddress != null && shippingAddress.length > 0) {
							this.htmlString += '<p>';
							for (var i = 0; i < shippingAddress.length; i++) {
								var add = shippingAddress[i];

								this.htmlString += shippingAddress;
							}
							this.htmlString += '</p>';
						}
					}
				}
			}

			var date = 'Date:' + MyDate.convertDateToStringForUI(this.txn.getTxnDate());

			this.htmlString += '<p>' + date + '</p>';

			if (!MyString.isEmpty(this.txn.getTxnRefNumber())) {
				txnRefText = this.getTxnRefText();
			}

			if (!MyString.isEmpty(txnRefText)) {
				this.htmlString += '<p>' + txnRefText + '</p>';
			}

			this.printDueDate();

			if (this.txn.getPODate()) {
				this.htmlString += '<p>' + 'PO Date: ' + MyDate.convertDateToStringForUI(this.txn.getPODate()) + '</p>';
			}

			if (this.txn.getPONumber()) {
				this.htmlString += '<p>' + 'PO No.: ' + this.txn.getPONumber() + '</p>';
			}

			if (this.txn.getUdfObjectArray()) {
				for (var i = 0; i < this.txn.getUdfObjectArray().length; i++) {
					var value = this.txn.getUdfObjectArray()[i];
					var fieldModel = udfCache.getUdfModelByFirmAndFieldId(this.txn.getFirmId(), value.getUdfFieldId());
					if (fieldModel.getUdfFieldPrintOnInvoice() == 1 && value.getUdfFieldValue() && fieldModel.getUdfTxnType() == this.txn.getTxnType() && value.getUdfRefId() == this.txn.getTxnId()) {
						var _displayValue = value.getDisplayValue(fieldModel);
						this.htmlString += '<p>' + fieldModel.getUdfFieldName() + ': ' + _displayValue + '</p>';
					}
				}
			}

			this.htmlString += '</td>';
			this.htmlString += '</tr>';
			this.htmlString += '</table>';
		} catch (err) {
			if (logger) {
				logger.error('Thermal printer print error: ' + err);
			}
			debugger;
		}
	};

	this.getTxnRefText = function () {
		switch (this.txn.getTxnType()) {
			case TxnTypeConstant.TXN_TYPE_SALE:
				return this.extraPad() + 'Invoice No: ' + this.txn.getFullTxnRefNumber();
			case TxnTypeConstant.TXN_TYPE_PURCHASE:
				return this.extraPad() + 'Bill No: ' + this.txn.getFullTxnRefNumber();
			case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
				return this.extraPad() + 'Return No: ' + this.txn.getFullTxnRefNumber();
			case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
				return this.extraPad() + 'Return No: ' + this.txn.getFullTxnRefNumber();
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
			case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
				return this.extraPad() + 'Order No: ' + this.txn.getFullTxnRefNumber();
			case TxnTypeConstant.TXN_TYPE_ESTIMATE:
				return this.extraPad() + 'Ref No.' + this.txn.getFullTxnRefNumber();
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
				return this.extraPad() + 'Challan No.' + this.txn.getFullTxnRefNumber();
			case TxnTypeConstant.TXN_TYPE_CASHIN:
			case TxnTypeConstant.TXN_TYPE_CASHOUT:
				return this.extraPad() + 'Receipt No.' + this.txn.getFullTxnRefNumber();
			default:
				return '';
		}
	};

	this.printDescription = function () {
		var description = this.txn.getDescription();
		if (!MyString.isEmpty(description)) {
			var descriptionArray = description.split('\n');
			if (descriptionArray != null && descriptionArray.length > 0) {
				// for (var i = 0; i < descriptionArray; i++) {
				//     var desc = descriptionArray[i];
				//     this.printTextOnLeft(desc, NORMAL);
				// }
				// this.printLineFeed();

				this.htmlString += '<p ALIGN = LEFT>';
				for (var i = 0; i < descriptionArray.length; i++) {
					this.htmlString += descriptionArray[i];
					this.htmlString += '<br>';
				}
				this.htmlString += '</p>';
			}
		}
	};

	this.printTermAndCondition = function () {
		switch (this.txn.getTxnType()) {
			case TxnTypeConstant.TXN_TYPE_SALE:
				var termsAndConditionText = settingCache.getTermsAndConditionSaleInvoivce();
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
				var termsAndConditionText = settingCache.getTermsAndConditionSaleOrder();
				break;
			case TxnTypeConstant.TXN_TYPE_ESTIMATE:
				var termsAndConditionText = settingCache.getTermsAndConditionEstimateQuotation();
				break;
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
				var termsAndConditionText = settingCache.getTermsAndConditionDeliveryChallan();
				break;
			default:
				var termsAndConditionText = settingCache.getTermsAndCondition();
		}
		if (termsAndConditionText) {
			var termsArray = termsAndConditionText.split('\n');
			if (termsArray && termsArray.length > 0) {
				this.htmlString += '<p ALIGN = LEFT>';
				for (var i = 0; i < termsArray.length; i++) {
					this.htmlString += termsArray[i];
					this.htmlString += '<br>';
				}
				this.htmlString += '</p>';
			}
		}
	};

	this.printDueDate = function () {
		var dueDate = this.txn.getTxnDueDate();
		if (dueDate != null) {
			if (this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER || (this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE) && settingCache.isPaymentTermEnabled() && !MyDate.isSameDate(this.txn.getTxnDate(), this.txn.getTxnDueDate())) {
				this.htmlString += '<p>' + 'Due Date :' + MyDate.convertDateToStringForUI(dueDate) + '</p>';
			}
		}
	};

	this.printIn2Cols = function (text1, text2, totalWidth, wrapFirstCol, wrappedTextAlign, textStyle, sectionAlign) {
		text1 = text1.toString();
		text2 = text2.toString();
		try {
			if (wrapFirstCol) {
				if (totalWidth < text2.length) {
					this.printIn2ColsWithBothWrap(text1, text2, totalWidth, LEFT, LEFT, textStyle, sectionAlign);
				} else {
					var width = totalWidth - text2.length;
					do {
						var holder = { continueLoop: false };
						var stringBuilder = { stringText: '' };

						text1 = this.wrapTextIfNeededWithPadding(text1, stringBuilder, width, wrappedTextAlign, holder);
						stringBuilder.stringText += text2;
						text2 = '';

						if (sectionAlign == RIGHT) {
							this.printTextOnRight(stringBuilder.stringText.toString(), textStyle);
						} else {
							this.printTextOnLeft(stringBuilder.stringText.toString(), textStyle);
						}
						this.printLineFeed();
					} while (holder.continueLoop);
				}
			} else {
				if (totalWidth < text1.length) {
					this.printIn2ColsWithBothWrap(text1, text2, totalWidth, LEFT, LEFT, textStyle, sectionAlign);
				} else {
					var width = totalWidth - text1.length;

					do {
						var holder = { continueLoop: false };
						var stringBuilder = { stringText: '' };

						stringBuilder.stringText += text1;
						text1 = this.fill(text1.length, '', SPACE_CHAR, LEFT);

						text2 = this.wrapTextIfNeeded(text2, stringBuilder, width, wrappedTextAlign, holder);

						if (sectionAlign == RIGHT) {
							this.printTextOnRight(stringBuilder.stringText.toString(), textStyle);
						} else {
							this.printTextOnLeft(stringBuilder.stringText.toString(), textStyle);
						}
						this.printLineFeed();
					} while (holder.continueLoop);
				}
			}
		} catch (err) {
			if (logger) {
				logger.error('Thermal printer print error: ' + err);
			}
			debugger;
		}
	};

	this.printTotalBalanceSection = function () {
		try {
			var stringBuilder = { stringText: '' };

			var balanceAmount = MyDouble.convertStringToDouble(this.txn.getBalanceAmount());
			var cashAmount = MyDouble.convertStringToDouble(this.txn.getCashAmount());
			var discountAmount = MyDouble.convertStringToDouble(this.txn.getDiscountAmount());
			var totalAmount = MyDouble.convertStringToDouble(balanceAmount + cashAmount);
			var taxAmount = MyDouble.convertStringToDouble(this.txn.getTaxAmount());

			var sectionWidth = NO_OF_CHARS / 1.3;

			var txnType = this.txn.getTxnType();

			if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
				totalAmount = totalAmount + discountAmount;
			} else {
				balanceAmount = this.txn.getTxnCurrentBalanceAmount() ? Number(this.txn.getTxnCurrentBalanceAmount()) : 0; // setting current balance amount
				cashAmount = totalAmount - balanceAmount; // setting total received amount
			}

			this.htmlString += "<table style='width:100%;'>";

			if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN) {
				this.htmlString += '<tr>' + '<td width = 30%></td>' + '<td width=35% ALIGN=RIGHT >' + 'Received :' + '</td>' + '<td width=35% ALIGN=RIGHT>' + MyDouble.getAmountForThermalPrinter(cashAmount) + '</td>' + '</tr>';
			} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
				this.htmlString += '<tr>' + '<td width = 30%></td>' + '<td width=35% ALIGN=RIGHT >' + 'Paid :' + '</td>' + '<td width=35% ALIGN=RIGHT >' + MyDouble.getAmountForThermalPrinter(cashAmount) + '</td>' + '</tr>';
			}

			if (discountAmount != 0) {
				if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
					stringBuilder.stringText = '';

					this.htmlString += '<tr>' + '<td width = 30%></td>' + '<td width=35% ALIGN=RIGHT >' + 'Discount :' + '</td>' + '<td width=35% ALIGN=RIGHT >' + MyDouble.getAmountForThermalPrinter(discountAmount) + '</td>' + '</tr>';
				} else {
					stringBuilder.stringText = '';

					var discountText = 'Discount(' + MyDouble.getPercentageWithDecimal(this.txn.getDiscountPercent()) + '%)';

					this.htmlString += '<tr>' + '<td width = 30%></td>' + '<td width=35% ALIGN=RIGHT >' + discountText + '</td>' + '<td width=35% ALIGN=RIGHT >' + MyDouble.getAmountForThermalPrinter(discountAmount) + '</td>' + '</tr>';
				}
			}

			if (taxAmount != 0) {
				var taxText = 'Tax (' + MyDouble.getPercentageWithDecimal(this.txn.getTaxPercent()) + '%)';

				this.htmlString += '<tr>' + '<td width = 30%></td>' + '<td width=35% ALIGN=RIGHT >' + taxText + '</td>' + '<td width=35% ALIGN=RIGHT >' + MyDouble.getAmountForThermalPrinter(taxAmount) + '</td>' + '</tr>';
			}

			if (this.txn.getAc1Amount() != 0) {
				this.htmlString += '<tr>' + '<td width = 30%></td>' + '<td width=35% ALIGN=RIGHT >' + dataLoader.getCurrentExtraChargesValue('1') + '</td>' + '<td width=35% ALIGN=RIGHT >' + MyDouble.getAmountForThermalPrinter(this.txn.getAc1Amount()) + '</td>' + '</tr>';
			}

			if (this.txn.getAc2Amount() != 0) {
				this.htmlString += '<tr>' + '<td width = 30%></td>' + '<td width=35% ALIGN=RIGHT >' + dataLoader.getCurrentExtraChargesValue('2') + '</td>' + '<td width=35% ALIGN=RIGHT >' + MyDouble.getAmountForThermalPrinter(this.txn.getAc2Amount()) + '</td>' + '</tr>';
			}

			if (this.txn.getAc3Amount() != 0) {
				this.htmlString += '<tr>' + '<td width = 30%></td>' + '<td width=35% ALIGN=RIGHT >' + dataLoader.getCurrentExtraChargesValue('3') + '</td>' + '<td width=35% ALIGN=RIGHT >' + MyDouble.getAmountForThermalPrinter(this.txn.getAc3Amount()) + '</td>' + '</tr>';
			}

			if (this.txn.getRoundOffValue() != 0 && txnType != TxnTypeConstant.TXN_TYPE_CASHIN && txnType != TxnTypeConstant.TXN_TYPE_CASHOUT) {
				this.htmlString += '<tr>' + '<td width = 30%></td>' + '<td width=35% ALIGN=RIGHT >' + 'Round Off :' + '</td>' + '<td width=35% ALIGN=RIGHT >' + MyDouble.getAmountForThermalPrinter(this.txn.getRoundOffValue()) + '</td>' + '</tr>';
			}

			if (txnType != TxnTypeConstant.TXN_TYPE_CASHIN && txnType != TxnTypeConstant.TXN_TYPE_CASHOUT || (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) && discountAmount != 0) {
				this.htmlString += '<tr >' + '<td width = 30%></td>' + '<td width=35% ALIGN=RIGHT >' + 'Total :' + '</td>' + '<td width=35% ALIGN=RIGHT >' + '<span>' + MyDouble.getAmountForThermalPrinter(totalAmount) + '</span>' + '</td>' + '</tr>';
			}

			if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
				this.htmlString += '<tr>' + '<td width = 30%></td>' + '<td width=35% ALIGN=RIGHT >' + 'Paid :' + '</td>' + '<td width=35% ALIGN=RIGHT >' + MyDouble.getAmountForThermalPrinter(cashAmount) + '</td>' + '</tr>';
			}
			if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
				if (txnType != TxnTypeConstant.TXN_TYPE_SALE || settingCache.isReceivedAmountEnabled()) {
					this.htmlString += '<tr>' + '<td width = 30%></td>' + '<td width=35% ALIGN=RIGHT >' + 'Received :' + '</td>' + '<td width=35% ALIGN=RIGHT >' + MyDouble.getAmountForThermalPrinter(cashAmount) + '</td>' + '</tr>';
				}
			}
			if (txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
				this.htmlString += '<tr>' + '<td width = 30%></td>' + '<td width=35% ALIGN=RIGHT >' + 'Advance :' + '</td>' + '<td width=35% ALIGN=RIGHT >' + MyDouble.getAmountForThermalPrinter(cashAmount) + '</td>' + '</tr>';
			}
			if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
				if (txnType != TxnTypeConstant.TXN_TYPE_SALE || settingCache.isBalanceAmountEnabled()) {
					this.htmlString += '<tr>' + '<td width = 30%></td>' + '<td width=35% ALIGN=RIGHT >' + 'Balance :' + '</td>' + '<td width=35% ALIGN=RIGHT >' + MyDouble.getAmountForThermalPrinter(balanceAmount) + '</td>' + '</tr>';
				}
			}

			if (settingCache.isCurrentBalanceOfPartyEnabled()) {
				try {
					var name = this.txn.getNameRef();
					switch (txnType) {
						case TxnTypeConstant.TXN_TYPE_CASHIN:
						case TxnTypeConstant.TXN_TYPE_SALE:
						case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
						case TxnTypeConstant.TXN_TYPE_CASHOUT:
						case TxnTypeConstant.TXN_TYPE_PURCHASE:
						case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
						case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
							if (name != null) {
								var currentAmount = MyDouble.getAmountForThermalPrinter(name.getAmount());
								var showPreviousBalance = true;
								if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
									var txnBalAmount = MyDouble.convertStringToDouble(this.txn.getBalanceAmount());
									var txnCurrentBalAmount = MyDouble.convertStringToDouble(this.txn.getTxnCurrentBalanceAmount());
									if (Math.abs(txnBalAmount - txnCurrentBalAmount) > 0.001) {
										showPreviousBalance = false;
									}
								}
								if (showPreviousBalance) {
									showPreviousBalance = TransactionPrintHelper.isLatestTransaction(this.txn);
								}
								if (showPreviousBalance) {
									var prevBal = TransactionPrintHelper.getPreviousAmount(this.txn, name);
									this.htmlString += '<tr>' + '<td width=30%>' + '</td>' + '<td width=35% ALIGN=RIGHT >' + 'Previous Bal :' + '</td>' + '<td width=35% ALIGN=RIGHT >' + MyDouble.getAmountForThermalPrinter(prevBal) + '</td>' + '</tr>';
								}

								if (currentAmount && txnType != TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN) {
									this.htmlString += '<tr>' + '<td width=30%>' + '</td>' + '<td width=35% ALIGN=RIGHT >' + 'Current Bal :' + '</td>' + '<td width=35% ALIGN=RIGHT >' + currentAmount + '</td>' + '</tr>';
								}
							}
							break;
					}
				} catch (err) {
					if (logger) {
						logger.error('Thermal printer print error: ' + err);
					}
					debugger;
				}
			}

			this.htmlString += '</table>';
			this.htmlString += '<br><br>';
		} catch (err) {
			if (logger) {
				logger.error('Thermal printer print error: ' + err);
			}
			debugger;
		}
	};

	this.printIn2ColsWithBothWrap = function (text1, text2, totalWidth, firstTextAlign, secondTextAlign, textStyle, sectionAlign) {
		text1 = text1.toString();
		text2 = text2.toString();
		try {
			var stringBuilder = { stringText: '' };

			var width = Math.round(totalWidth / 2);
			var _holder = { continueLoop: true };

			do {
				_holder.continueLoop = false;
				stringBuilder.stringText = '';

				text1 = this.wrapTextIfNeededWithPadding(text1, stringBuilder, width, firstTextAlign, _holder);

				text2 = this.wrapTextIfNeededWithPadding(text2, stringBuilder, totalWidth - width, secondTextAlign, _holder);

				if (sectionAlign == RIGHT) {
					this.printTextOnRight(stringBuilder.stringText.toString(), textStyle);
				} else {
					this.printTextOnLeft(stringBuilder.stringText.toString(), textStyle);
				}
				this.printLineFeed();
			} while (_holder.continueLoop);
		} catch (err) {
			if (logger) {
				logger.error('Thermal printer print error: ' + err);
			}
			debugger;
		}
	};

	this.cutPaper = function () {
		this.print(PrinterCommands.FEED_PAPER_AND_CUT);
	};

	this.printTaxDetails = function () {
		var taxDetailMap = new _map2.default();
		var totalAdditionalCESSAmount = 0;

		var sectionWidth = NO_OF_CHARS / 1.5;

		var baseLineItems = this.txn.getLineItems();

		if (baseLineItems != null && baseLineItems.length > 0) {
			for (var i = 0; i < baseLineItems.length; i++) {
				var baseLineItem = baseLineItems[i];

				if (baseLineItem.getLineItemTaxId()) {
					var taxCode = taxCodeCache.getTaxCodeObjectById(baseLineItem.getLineItemTaxId());

					if (taxCode.getTaxType() == TaxCodeConstants.taxGroup) {
						var taxCodeIds = (0, _values2.default)(taxCode.getTaxCodeMap());

						if (taxCodeIds.length > 0) {
							for (var j = 0; j < taxCodeIds.length; j++) {
								var taxCodeId = taxCodeIds[j];
								if (taxDetailMap.has(taxCodeId)) {
									taxDetailMap.get(taxCodeId).push(baseLineItem.getItemQuantity() * baseLineItem.getItemUnitPrice() - baseLineItem.getLineItemDiscountAmount());
								} else {
									var list = [];
									list.push(baseLineItem.getItemQuantity() * baseLineItem.getItemUnitPrice() - baseLineItem.getLineItemDiscountAmount());
									taxDetailMap.set(taxCodeId, list);
								}
							}
						}
					} else {
						if (taxDetailMap.has(taxCode.getTaxCodeId())) {
							taxDetailMap.get(taxCode.getTaxCodeId()).push(baseLineItem.getItemQuantity() * baseLineItem.getItemUnitPrice() - baseLineItem.getLineItemDiscountAmount());
						} else {
							var list = [];
							list.push(baseLineItem.getItemQuantity() * baseLineItem.getItemUnitPrice() - baseLineItem.getLineItemDiscountAmount());
							taxDetailMap.set(taxCode.getTaxCodeId(), list);
						}
					}
				}
				debugger;
				if (baseLineItem.getLineItemAdditionalCESS() != 0 && baseLineItem.getLineItemAdditionalCESS() != null) {
					totalAdditionalCESSAmount += MyDouble.convertStringToDouble(baseLineItem.getLineItemAdditionalCESS());
				}
			}
		}

		if (this.txn.getTransactionTaxId() != 0 && this.txn.getTransactionTaxId() != null) {
			var taxCode = taxCodeCache.getTaxCodeObjectById(this.txn.getTransactionTaxId());

			if (taxCode.getTaxType() == TaxCodeConstants.taxGroup) {
				var taxCodeIds = (0, _values2.default)(taxCode.getTaxCodeMap());
				if (taxCodeIds.length > 0) {
					for (i = 0; i < taxCodeIds.length; i++) {
						var taxCodeId = taxCodeIds[i];
						if (taxDetailMap.has(taxCodeId)) {
							taxDetailMap.get(taxCodeId).push(this.txn.getSubTotalAmount() - this.txn.getDiscountAmount());
						} else {
							var list = [];
							list.push(this.txn.getSubTotalAmount() - this.txn.getDiscountAmount());
							taxDetailMap.set(taxCodeId, list);
						}
					}
				}
			} else {
				if (taxDetailMap.has(this.txn.getTransactionTaxId())) {
					taxDetailMap.get(this.txn.getTransactionTaxId()).push(this.txn.getSubTotalAmount() - this.txn.getDiscountAmount());
				} else {
					var list = [];
					list.push(this.txn.getSubTotalAmount() - this.txn.getDiscountAmount());
					taxDetailMap.set(this.txn.getTransactionTaxId(), list);
				}
			}
		}

		if (taxDetailMap.size > 0 || totalAdditionalCESSAmount != 0) {
			this.htmlString += '<p ALIGN = CENTER>' + '' + 'Tax Details' + '' + '</p>';
		}

		if (taxDetailMap.size > 0) {
			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;

			try {
				for (var _iterator = (0, _getIterator3.default)(taxDetailMap.keys()), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					var taxId = _step.value;

					var taxCode = taxCodeCache.getTaxCodeObjectById(taxId);
					var taxAmount = 0;

					var taxDetailMapVal = taxDetailMap.get(taxId);

					for (var i = 0; i < taxDetailMapVal.length; i++) {
						var amount = taxDetailMapVal[i];
						taxAmount += MyDouble.getBalanceAmountWithDecimal(taxCode.getTaxRate() * amount / 100);
					}

					switch (taxCode.getTaxRateType()) {
						case TaxCodeConstants.IGST:
							this.htmlString += '<p ALIGN = RIGHT>' + '' + 'IGST' + ' @ ' + MyDouble.getPercentageWithDecimal(taxCode.getTaxRate()) + '% : ' + MyDouble.getAmountForThermalPrinter(taxAmount) + '' + '</p>';
							break;
						case TaxCodeConstants.CGST:
							this.htmlString += '<p ALIGN = RIGHT>' + '' + 'CGST' + ' @ ' + MyDouble.getPercentageWithDecimal(taxCode.getTaxRate()) + '% : ' + MyDouble.getAmountForThermalPrinter(taxAmount) + '' + '</p>';
							break;
						case TaxCodeConstants.SGST:
							this.htmlString += '<p ALIGN = RIGHT>' + '' + this.stateTaxText + ' @ ' + MyDouble.getPercentageWithDecimal(taxCode.getTaxRate()) + '% : ' + MyDouble.getAmountForThermalPrinter(taxAmount) + '' + '</p>';
							break;
						case TaxCodeConstants.CESS:
							this.htmlString += '<p ALIGN = RIGHT>' + '' + 'CESS' + ' @ ' + MyDouble.getPercentageWithDecimal(taxCode.getTaxRate()) + '% : ' + MyDouble.getAmountForThermalPrinter(taxAmount) + '' + '</p>';
							break;
						case TaxCodeConstants.STATE_SPECIFIC_CESS:
							this.htmlString += '<p ALIGN = RIGHT>' + '' + taxCode.getTaxCodeName() + ' @ ' + MyDouble.getPercentageWithDecimal(taxCode.getTaxRate()) + '% : ' + MyDouble.getAmountForThermalPrinter(taxAmount) + '' + '</p>';
							break;
						case TaxCodeConstants.OTHER:
							this.htmlString += '<p ALIGN = RIGHT>' + '' + taxCode.getTaxCodeName() + ' @ ' + MyDouble.getPercentageWithDecimal(taxCode.getTaxRate()) + '% : ' + MyDouble.getAmountForThermalPrinter(taxAmount) + '' + '</p>';
							break;
						case TaxCodeConstants.Exempted:
							this.htmlString += '<p ALIGN = RIGHT>' + '' + 'EXEMPTED' + ' @ ' + MyDouble.getPercentageWithDecimal(taxCode.getTaxRate()) + '% : ' + MyDouble.getAmountForThermalPrinter(taxAmount) + '' + '</p>';
							break;
					}
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}
		}

		if (totalAdditionalCESSAmount != 0) {
			this.htmlString += '<p ALIGN = RIGHT>' + '' + 'Add. CESS :' + MyDouble.getAmountForThermalPrinter(totalAdditionalCESSAmount) + '' + '</p>';
		}
	};

	this.printItemTableHeader = function () {
		try {
			var _holder2 = { continueLoop: true };
			var _rowText = { stringText: '' };

			var srHeader = 'SR';
			var itemNameHeader = 'Name';
			var quantityHeader = 'Qty';
			var unitPriceHeader = 'Price';
			var amountHeader = 'Amount';

			var srNoheaderWidth = '7%';
			var nameHeaderWidth = '33%';
			var quantityHeaderWidth = '14%';

			var printAmounts = this.txn.getTxnType() != TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || settingCache.printAmountDetailsInDeliveryChallan();

			if (!printAmounts) {
				srNoheaderWidth = '12%';
				nameHeaderWidth = '60%';
				quantityHeaderWidth = '28%';
			}

			this.htmlString += "<table style='width:100%;' cellspacing=0>";
			this.htmlString += "<tr class='border_bottom' >" + '<td width=' + srNoheaderWidth + '  align = left >' + srHeader + '</td>' + '<td width=' + nameHeaderWidth + ' align = left  >' + itemNameHeader + '</td>';
			this.htmlString += '<td width=' + quantityHeaderWidth + ' align = right>' + quantityHeader + '</td>';
			if (printAmounts) {
				this.htmlString += '<td width=23% align = right>' + unitPriceHeader + '</td>';
				this.htmlString += '<td width=23%  align=right >' + amountHeader + '</td>';
			}
			this.htmlString += '</tr>';
		} catch (err) {
			if (logger) {
				logger.error('Thermal printer print error: ' + err);
			}
		}
	};

	this.printBody = function () {
		var lineItems = this.txn.getLineItems();
		this.isThereAnyLineItems = lineItems.length > 0;

		if (this.isThereAnyLineItems) {
			this.printItemTableHeader();

			var quantityTotal = 0;
			var freeQuantityTotal = 0;
			var index = 0;
			for (var i = 0; i < lineItems.length; i++) {
				var baseLineItem = lineItems[i];
				index++;
				debugger;
				this.printItemRow(baseLineItem, index);

				if (baseLineItem.getLineItemUnitMappingId() > 0) {
					var mappingObj = itemUnitMappingCache.getItemUnitMapping(baseLineItem.getLineItemUnitMappingId());
					var secondaryQuantity = MyDouble.convertStringToDouble(baseLineItem.getItemQuantity()) * mappingObj.getConversionRate();
					var secondaryFreeQuantity = MyDouble.convertStringToDouble(baseLineItem.getItemFreeQuantity()) * mappingObj.getConversionRate();
					quantityTotal += secondaryQuantity;
					freeQuantityTotal += secondaryFreeQuantity;
				} else {
					quantityTotal += MyDouble.convertStringToDouble(baseLineItem.getItemQuantity());
					freeQuantityTotal += MyDouble.convertStringToDouble(baseLineItem.getItemFreeQuantity());
				}
			}

			this.printItemRowForTotalQuantity(quantityTotal, freeQuantityTotal);
			this.htmlString += '</table>';
		}
	};

	this.printDashLine = function () {
		this.printTextOnLeft(this.fill(NO_OF_CHARS, '', DASH_CHAR, LEFT), NORMAL);
	};

	this.printItemRow = function (baseLineItem, index) {
		try {
			holder = { continueLoop: true };
			rowText = { stringText: '' };

			var itemSr = index.toString();
			var itemName = baseLineItem.getItemName().replace('\n', ' ');
			var itemQty = MyDouble.getQuantityWithDecimalWithoutColor(baseLineItem.getItemQuantity()) + MyDouble.quantityDoubleToStringWithSignExplicitly(baseLineItem.getItemFreeQuantity(), true);
			var itemUnitPrice = MyDouble.getAmountForThermalPrinter(baseLineItem.getItemUnitPrice());
			var itemAmount = '0.0';

			var showTaxForItem = false;
			var showDiscountForItem = false;
			var showAdditionalCESSForItem = false;

			if (settingCache.isItemwiseTaxEnabled() && baseLineItem.getLineItemTaxId() && baseLineItem.getLineItemTaxId() != 0) {
				showTaxForItem = true;
			}
			if (settingCache.isItemwiseDiscountEnabled() && baseLineItem.getLineItemDiscountAmount() != 0) {
				showDiscountForItem = true;
			}
			if (settingCache.getAdditionalCessEnabled() && baseLineItem.getLineItemAdditionalCESS() != 0) {
				showAdditionalCESSForItem = true;
			}

			var printAmounts = this.txn.getTxnType() != TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || settingCache.printAmountDetailsInDeliveryChallan();

			if (showDiscountForItem || showTaxForItem) {
				itemAmount = MyDouble.getAmountForThermalPrinter(baseLineItem.getItemQuantity() * baseLineItem.getItemUnitPrice());
			} else {
				itemAmount = MyDouble.getAmountForThermalPrinter(baseLineItem.getLineItemTotal());
			}

			if (baseLineItem.getLineItemUnitMappingId() != null) {
				var mappingObj = itemUnitMappingCache.getItemUnitMapping(baseLineItem.getLineItemUnitMappingId());
				itemQty = MyDouble.getQuantityWithDecimalWithoutColor(baseLineItem.getItemQuantity() * mappingObj.getConversionRate()) + MyDouble.quantityDoubleToStringWithSignExplicitly(baseLineItem.getItemFreeQuantity() * mappingObj.getConversionRate(), true);
				itemUnitPrice = MyDouble.getAmountForThermalPrinter(baseLineItem.getItemUnitPrice() / mappingObj.getConversionRate());
			}

			if (settingCache.getItemUnitEnabled() && this.txn.getTxnType() != TxnTypeConstant.TXN_TYPE_EXPENSE && this.txn.getTxnType() != TxnTypeConstant.TXN_TYPE_OTHER_INCOME) {
				if (baseLineItem.getLineItemUnitId() > 0) {
					var itemUnitShortName = itemUnitCache.getItemUnitShortNameById(baseLineItem.getLineItemUnitId());
					if (itemUnitShortName) {
						itemQty += itemUnitShortName;
					}
				}
			}

			this.htmlString += '<tr>' + '<td align = left  >' + itemSr + '</td>' + '<td  align = left >' + itemName + '</td>';
			this.htmlString += '<td  align = right >' + itemQty + '</td>';

			if (printAmounts) {
				this.htmlString += '<td  align = right>' + itemUnitPrice + '</td>';
				this.htmlString += '<td  align=right  >' + itemAmount + '</td>';
			}

			this.htmlString += '</tr>';

			var printDescription = TransactionPrintSettings.printItemDescriptionInInvoicePrint();

			if (printDescription) {
				var description = baseLineItem.getLineItemDescription();
				if (!MyString.isEmpty(description)) {
					description = description.replace('\n', '<br>');
					this.htmlString += '<tr >' + '<td ALIGN=LEFT colspan=5>' + description + '</td>' + '</tr>';
				}
			}

			var item = itemCache.getItemById(baseLineItem.getItemId());
			if (item && item.getItemHSNCode() && settingCache.getHSNSACEnabled()) {
				var hsnSacText;
				if (item.isItemInventory()) {
					hsnSacText = 'HSN : ';
				} else {
					hsnSacText = 'SAC : ';
				}

				this.htmlString += '<tr >' + '<td ALIGN=LEFT colspan=5>' + hsnSacText + item.getItemHSNCode() + '</td>' + '</tr>';
			}

			if (showDiscountForItem && printAmounts) {
				var discountText = 'Discount(' + MyDouble.getPercentageWithDecimal(baseLineItem.getLineItemDiscountPercent()) + '%):';
				var amount = MyDouble.getAmountForThermalPrinter(baseLineItem.getLineItemDiscountAmount());

				if (baseLineItem.getLineItemDiscountAmount() > 0 && amount) {
					this.htmlString += '<tr>' + '<td ALIGN=RIGHT colspan=5>' + discountText + amount + '</td>' + '</tr>';
				}
			}

			if (showTaxForItem && printAmounts) {
				var taxText = taxCodeCache.getTaxCodeObjectById(baseLineItem.getLineItemTaxId()).getTaxCodeName() + ': ';
				var subTotal = baseLineItem.getItemQuantity() * baseLineItem.getItemUnitPrice() - baseLineItem.getLineItemDiscountAmount();
				var taxAmount = baseLineItem.getLineItemTaxPercent() * subTotal / 100;
				var amount = MyDouble.getAmountForThermalPrinter(taxAmount);

				if (amount) {
					this.htmlString += '<tr>' + '<td ALIGN=RIGHT colspan=5>' + taxText + amount + '</td>' + '</tr>';
				}
			}

			if (showAdditionalCESSForItem && printAmounts) {
				var additionalCESSText = 'Add. CESS: ';
				var amount = MyDouble.getAmountForThermalPrinter(baseLineItem.getLineItemAdditionalCESS());

				if (amount && baseLineItem.getLineItemAdditionalCESS() != 0) {
					this.htmlString += '<tr>' + '<td ALIGN=RIGHT colspan=5>' + additionalCESSText + amount + '</td>' + '</tr>';
				}
			}
		} catch (err) {
			if (logger) {
				logger.error('Thermal printer print error: ' + err);
			}
		}
	};

	this.printItemRowForTotalQuantity = function (totalQty, freeQuantityTotal) {
		try {
			var _holder3 = { continueLoop: true };
			var _rowText2 = { stringText: '' };

			var qtyText = '';
			var totalItemQty = '';

			var printAmounts = this.txn.getTxnType() != TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || settingCache.printAmountDetailsInDeliveryChallan();

			if (settingCache.isPrintItemQuantityTotalEnabled()) {
				qtyText = 'Total';
				totalItemQty = MyDouble.getQuantityWithDecimalWithoutColor(totalQty) + MyDouble.quantityDoubleToStringWithSignExplicitly(freeQuantityTotal, true);
			}

			var subTotal = MyDouble.getAmountForThermalPrinter(this.txn.getSubTotalAmount());

			this.htmlString += "<tr class='border_top'>" + '<td colspan=2 align = left>' + qtyText + '</td>' + '<td align = right >' + totalItemQty + '</td>';
			if (printAmounts) {
				this.htmlString += '<td ALIGN=right colspan=2>' + subTotal + '</td>';
			}
			this.htmlString += '</tr>';
		} catch (err) {
			if (logger) {
				logger.error('Thermal printer print error: ' + err);
			}
		}
	};

	this.wrapTextIfNeededWithPadding = function (text, stringBuilder, width, align, holder) {
		text = text.toString();

		if (text.length < width) {
			stringBuilder.stringText += this.fill(width, text + this.extraPad(), SPACE_CHAR, align);
			text = '';
		} else {
			holder.continueLoop = true;

			var s = this.splitString(text, width - 1);
			stringBuilder.stringText += s[0] + this.extraPad();
			text = s[1];
		}

		return text;
	};

	this.wrapTextIfNeeded = function (text, stringBuilder, width, align, holder) {
		text = text.toString();
		if (text.length <= width) {
			stringBuilder.stringText += this.fill(width, text, SPACE_CHAR, align);
			text = '';
		} else {
			holder.continueLoop = true;

			var s = this.splitString(text, width);
			stringBuilder.stringText += s[0];
			text = s[1];
		}

		return text;
	};

	this.extraPad = function () {
		return ' ';
	};

	this.printTextOnRight = function (text) {
		var textStyle = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : NORMAL;
		var withEscCodes = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

		text = text.toString();

		if (withEscCodes && settingCache.useESCPOSCommandsInThermalPrinter()) {
			this.print(this.getTextSizeAndStyleBytes(textStyle));
			this.print(PrinterCommands.ESC_ALIGN_RIGHT);
			this.print(text.toString());
			this.print(this.getTextSizeAndStyleBytes(NORMAL));
		} else {
			this.print(this.fill(NO_OF_CHARS, text, SPACE_CHAR, RIGHT));
		}
	};

	this.printTextOnLeft = function (text) {
		var textStyle = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : NORMAL;
		var withEscCodes = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

		text = text.toString();

		if (withEscCodes && settingCache.useESCPOSCommandsInThermalPrinter()) {
			this.print(this.getTextSizeAndStyleBytes(textStyle));
			this.print(PrinterCommands.ESC_ALIGN_LEFT);
			this.print(text.toString());
			this.print(this.getTextSizeAndStyleBytes(NORMAL));
		} else {
			this.print(this.fill(NO_OF_CHARS, text, SPACE_CHAR, LEFT));
		}
	};

	this.printTextInCenter = function (text) {
		var textStyle = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : NORMAL;
		var withEscCodes = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

		text = text.toString();

		if (withEscCodes && settingCache.useESCPOSCommandsInThermalPrinter()) {
			this.print(this.getTextSizeAndStyleBytes(textStyle));
			this.print(PrinterCommands.ESC_ALIGN_CENTER);
			this.print(text);
			this.print(this.getTextSizeAndStyleBytes(NORMAL));
		} else {
			this.print(this.fill(NO_OF_CHARS, text, SPACE_CHAR, CENTER));
		}
	};

	this.getTextSizeAndStyleBytes = function () {
		var textStyle = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : NORMAL;

		var bytes = new Buffer([0x1b, 0x21, 0x00]);

		if (textStyle == BOLD) {
			bytes[2] = 0x8;
			return bytes;
		} else if (textStyle == NORMAL) {
			if (this.pageSize == ThermalPrinterConstant.INCH_2_PAGE_SIZE) {
				switch (this.textSize) {
					case ThermalPrinterConstant.SMALL:
						bytes[2] = 0x7;
						break;
					case ThermalPrinterConstant.MEDIUM:
						bytes[2] = 0x00;
						break;
				}
			} else {
				bytes[2] = 0x00;
			}
			return bytes;
		} else if (textStyle == LARGE) {
			bytes[2] = 0x20;
			return bytes;
		}

		return bytes;
	};

	this.fill = function (totalChars, text, fillWith, align) {
		var newText = '';

		text = text.toString();

		if (text.length <= totalChars) {
			var totalPad = totalChars - text.length;

			switch (align) {
				case CENTER:
					var left = Math.round(totalPad / 2);
					for (var i = 0; i < left; i++) {
						text += fillWith;
					}
					for (var _i5 = 0; _i5 < totalPad - left; _i5++) {
						text = fillWith + text;
					}
					return text;

				case RIGHT:
					for (var _i6 = 0; _i6 < totalPad; _i6++) {
						text = fillWith + text;
					}
					return text;
				case LEFT:
				default:
					for (var _i7 = 0; _i7 < totalPad; _i7++) {
						text += fillWith;
					}
					return text;
			}
		} else {
			var stringChunks = this.splitString(text, totalChars);
			for (var _i8 = 0; _i8 < stringChunks.length; _i8++) {
				newText += this.fill(totalChars, stringChunks[_i8], fillWith, align);
			}
		}
		return newText;
	};

	this.splitString = function (text, noOfChars) {
		text = text.toString();
		return [text.substr(0, noOfChars), text.substr(noOfChars)];
	};

	this.printLineFeed = function () {
		if (settingCache.useESCPOSCommandsInThermalPrinter()) {
			this.print(PrinterCommands.FEED_LINE);
		}
	};

	this.lineFeed = function () {
		if (!settingCache.useESCPOSCommandsInThermalPrinter()) {
			this.print(PrinterCommands.FEED_LINE);
		}
	};

	this.print = function (buff) {
		if (typeof buff === 'string') {
			var endBuff = null;
			for (var i = 0; i < buff.length; i++) {
				var value = buff[i];
				var tempBuff = new Buffer(value);
				if (endBuff) {
					endBuff = Buffer.concat([endBuff, tempBuff]);
				} else {
					endBuff = tempBuff;
				}
			}
			buff = endBuff;
		}

		if (this.textBuffer) {
			this.textBuffer = Buffer.concat([this.textBuffer, buff]);
		} else {
			this.textBuffer = buff;
		}
	};
};

module.exports = ThermalPrinterImage;