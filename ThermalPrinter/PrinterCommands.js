var PrinterCommands = {

	INIT: new Buffer([0x1b, 0x40]), // Clear data in buffer and reset modes
	FEED_LINE: new Buffer([0x0a]), // Print and line feed
	SELECT_FONT_A: new Buffer([0x1B, 0x21, 0]),
	SET_BAR_CODE_HEIGHT: new Buffer([29, 104, 100]),
	PRINT_BAR_CODE_1: new Buffer([29, 107, 2]),
	SEND_NULL_BYTE: new Buffer([0x00]),
	SELECT_PRINT_SHEET: new Buffer([0x1B, 0x63, 0x30, 0x02]),
	// FEED_PAPER_AND_CUT: new Buffer([0x1D, 0x56, 66, 0x00]),
	SELECT_CYRILLIC_CHARACTER_CODE_TABLE: new Buffer([0x1B, 0x74, 0x11]),
	SELECT_BIT_IMAGE_MODE: new Buffer([0x1B, 0x2A, 33, -128, 0]),
	SET_LINE_SPACING_24: new Buffer([0x1B, 0x33, 24]),
	SET_LINE_SPACING_30: new Buffer([0x1B, 0x33, 30]),
	TRANSMIT_DLE_PRINTER_STATUS: new Buffer([0x10, 0x04, 0x01]),
	TRANSMIT_DLE_OFFLINE_PRINTER_STATUS: new Buffer([0x10, 0x04, 0x02]),
	TRANSMIT_DLE_ERROR_STATUS: new Buffer([0x10, 0x04, 0x03]),
	TRANSMIT_DLE_ROLL_PAPER_SENSOR_STATUS: new Buffer([0x10, 0x04, 0x04]),
	ESC_FONT_COLOR_DEFAULT: new Buffer([0x1B, 'r', 0x00]),
	FS_FONT_ALIGN: new Buffer([0x1C, 0x21, 1, 0x1B, 0x21, 1]),
	ESC_CANCEL_BOLD: new Buffer([0x1B, 0x45, 0]),

	ESC_HORIZONTAL_CENTERS: new Buffer([0x1B, 0x44, 20, 28, 0x00]),
	ESC_CANCLE_HORIZONTAL_CENTERS: new Buffer([0x1B, 0x44, 0x00]),

	ESC_ENTER: new Buffer([0x1B, 0x4A, 0x40]),
	PRINTE_TEST: new Buffer([0x1D, 0x28, 0x41]),

	// Paper
	FEED_PAPER_AND_CUT: new Buffer([0x1d, 0x56, 0x00]), // Full cut paper

	// open drawer
	OPEN_DRAWER_KICK_2: new Buffer([0x1b, 0x70, 0x00]), // Sends a pulse to pin 2 []
	OPEN_DRAWER_KICK_5: new Buffer([0x1b, 0x70, 0x01]), // Sends a pulse to pin 5 []

	// OPEN_DRAWER: new Buffer([0x10, 0x14, 0x00, 0x00, 0x00]),

	// Text format
	TXT_NORMAL: new Buffer([0x1b, 0x21, 0x00]), // Normal text
	TXT_2HEIGHT: new Buffer([0x1b, 0x21, 0x10]), // Double height text
	TXT_2WIDTH: new Buffer([0x1b, 0x21, 0x20]), // Double width text
	TXT_4SQUARE: new Buffer([0x1b, 0x21, 0x30]), // Quad area text
	TXT_BOLD_OFF: new Buffer([0x1b, 0x45, 0x00]), // Bold font OFF
	TXT_BOLD_ON: new Buffer([0x1b, 0x45, 0x01]), // Bold font ON

	ESC_ALIGN_LEFT: new Buffer([0x1b, 0x61, 0x00]), // Left justification
	ESC_ALIGN_CENTER: new Buffer([0x1b, 0x61, 0x01]), // Centering
	ESC_ALIGN_RIGHT: new Buffer([0x1b, 0x61, 0x02]) // Right justification

};

module.exports = PrinterCommands;