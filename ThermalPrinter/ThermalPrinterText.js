var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ThermalPrinterText = function ThermalPrinterText() {
	var SettingCache = require('../Cache/SettingCache.js');
	var FirmCache = require('../Cache/FirmCache.js');
	var firmCache = new FirmCache();
	var ItemCache = require('../Cache/ItemCache.js');
	var itemCache = new ItemCache();
	var ItemUnitMappingCache = require('../Cache/ItemUnitMappingCache.js');
	var itemUnitMappingCache = new ItemUnitMappingCache();
	var ItemUnitCache = require('../Cache/ItemUnitCache.js');
	var itemUnitCache = new ItemUnitCache();
	var TransactionFactory = require('../BizLogic/TransactionFactory.js');
	var TxnTypeConstant = require('../Constants/TxnTypeConstant.js');
	var TaxCodeConstants = require('../Constants/TaxCodeConstants.js');
	var DataLoader = require('../DBManager/DataLoader.js');
	var dataLoader = new DataLoader();
	var TaxCodeCache = require('../Cache/TaxCodeCache.js');
	var taxCodeCache = new TaxCodeCache();
	var StateCode = require('../Constants/StateCode.js');
	var MyString = require('./../Utilities/MyString.js');
	var TransactionPrintSettings = require('../ReportHTMLGenerator/TransactionPrintSettings.js');
	var ThermalPrinterConstant = require('../Constants/ThermalPrinterConstant.js');
	var PrinterCommands = require('./../ThermalPrinter/PrinterCommands.js');
	var settingCache = new SettingCache();
	var UDFCache = require('./../Cache/UDFCache.js');
	var TransactionPrintHelper = require('./../Utilities/TransactionPrintHelper.js');
	var TransactionHTMLGenerator = require('./../ReportHTMLGenerator/TransactionHTMLGenerator');
	var stateTaxPlace = 'SGST';
	var exemptedTaxString = 'Exmp.';

	var NORMAL = 1;
	var BOLD = 2;
	var UNDERLINE = 3;

	var SMALL = 4;
	var MEDIUM = 5;
	var LARGE = 6;

	var LEFT = 1;
	var RIGHT = 2;
	var CENTER = 3;

	var NO_OF_CHARS = 48;
	var SR_WIDTH = 3;
	var ITEM_NAME_WIDTH = 12;
	var QUANTITY_WIDTH = 11;
	var UNIT_PRICE_WIDTH = 11;
	var AMOUNT_WIDTH = 11;

	var SPACE_CHAR = ' ';
	var DASH_CHAR = '-';
	var STAR_CHAR = '*';

	this.txn;
	this.pageSize = ThermalPrinterConstant.INCH_3_PAGE_SIZE;
	this.textSize = ThermalPrinterConstant.MEDIUM;
	this.isThereAnyLineItems = false;
	this.stateTaxText = 'SGST';
	this.textBuffer;

	this.getTextForPrint = function (txn) {
		if (!txn) {
			return false;
		} else {
			this.txn = txn;
		}

		this.textBuffer = null;

		this.pageSize = settingCache.getThermalPrinterPageSize();

		this.textSize = settingCache.getThermalPrinterTextSize();

		if (!settingCache.useESCPOSCommandsInThermalPrinter()) {
			this.textSize = ThermalPrinterConstant.MEDIUM;
		}

		switch (this.pageSize) {
			case ThermalPrinterConstant.INCH_2_PAGE_SIZE:
				if (this.textSize == ThermalPrinterConstant.SMALL) {
					NO_OF_CHARS = 42;
				} else if (this.textSize == ThermalPrinterConstant.MEDIUM) {
					NO_OF_CHARS = 32;
				}
				break;

			case ThermalPrinterConstant.INCH_3_PAGE_SIZE:
				NO_OF_CHARS = 48;
				break;

			case ThermalPrinterConstant.INCH_4_PAGE_SIZE:
				NO_OF_CHARS = 64;
				break;

			case ThermalPrinterConstant.CUSTOMIZED_PAGE_SIZE:
				NO_OF_CHARS = settingCache.getThermalPrinterCustomizeCharacterCount();
				break;
		}

		if (settingCache.getItemUnitEnabled()) {
			QUANTITY_WIDTH = Math.round(0.1875 * NO_OF_CHARS) + 1;
		} else {
			QUANTITY_WIDTH = Math.round(0.125 * NO_OF_CHARS) + 1;
		}

		UNIT_PRICE_WIDTH = Math.round(0.167 * NO_OF_CHARS + 0.4) + 1;
		AMOUNT_WIDTH = Math.round(0.23 * NO_OF_CHARS + 0.4) + 1;

		var printAmounts = this.txn.getTxnType() != TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || settingCache.printAmountDetailsInDeliveryChallan();

		if (!printAmounts) {
			UNIT_PRICE_WIDTH = 0;
			AMOUNT_WIDTH = 0;
			QUANTITY_WIDTH = Math.round(QUANTITY_WIDTH * 1.5);
		}

		ITEM_NAME_WIDTH = NO_OF_CHARS - SR_WIDTH - QUANTITY_WIDTH - UNIT_PRICE_WIDTH - AMOUNT_WIDTH;

		this.generateTextForPrint();

		return this.textBuffer;
	};

	this.generateTextForPrint = function () {
		this.print(PrinterCommands.INIT);

		this.printLineFeed();

		this.printHeader();
		this.printLineFeed();

		var txnType = this.txn.getTxnType();
		if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_ESTIMATE || txnType == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txnType == TxnTypeConstant.TXN_TYPE_EXPENSE || txnType == TxnTypeConstant.TXN_TYPE_OTHER_INCOME || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
			this.printBody();
			this.printLineFeed();
		}

		var printAmounts = this.txn.getTxnType() != TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || settingCache.printAmountDetailsInDeliveryChallan();

		if (printAmounts) {
			this.printTotalBalanceSection();
		}

		if (settingCache.isPrintTaxDetailEnabled() && this.isThereAnyLineItems && printAmounts) {
			this.printTaxDetails();
		}

		if (settingCache.isPrintDescriptionEnabled()) {
			this.printDescription();
		}

		if (this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE && settingCache.isPrintTermsAndConditionSaleInvoiceEnabled() || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER && settingCache.isPrintTermsAndConditionSaleOrderEnabled() || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN && settingCache.isPrintTermsAndConditionDeliveryChallanEnabled() || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE && settingCache.isPrintTermsAndConditionEstimateQuotationEnabled()) {
			this.printTermAndCondition();
		}

		this.printLineFeed();
		this.printLineFeed();
		this.printLineFeed();
		this.lineFeed();
		this.lineFeed();
		this.lineFeed();

		var extraFooterLines = settingCache.getThermalPrinterExtraFooterLines();

		for (var i = 0; i < extraFooterLines; i++) {
			this.printLineFeed();
			this.lineFeed();
		}

		if (settingCache.isAutoCutPaperEnabled()) {
			this.cutPaper();
		}

		var numberOfCopies = settingCache.getThermalPrintCopyCount();

		if (numberOfCopies > 1) {
			var currentTextBuffer = this.textBuffer;
			for (var _i = 1; _i < numberOfCopies; _i++) {
				this.textBuffer = Buffer.concat([this.textBuffer, currentTextBuffer]);
			}
		}

		if (settingCache.isOpenDrawerCommandEnabled()) {
			this.print(PrinterCommands.OPEN_DRAWER_KICK_2);
			this.print(PrinterCommands.OPEN_DRAWER_KICK_5);
		}
	};

	this.printHeader = function () {
		var firm = firmCache.getTransactionFirmWithDefault(this.txn);

		if (firm) {
			this.printCompanyHeader(firm);

			if (StateCode.isUnionTerritory(firm.getFirmState())) {
				this.stateTaxText = 'UTGST';
			}
		}
		this.printTransactionHeader();
		this.printPartyInfoAndTxnHeaderData();
	};

	this.printCompanyHeader = function (firm) {
		if (firm.getFirmName() && settingCache.isPrintCompanyNameEnabled()) {
			var firmNameArray = firm.getFirmName().split('\n');
			if (firmNameArray && firmNameArray.length > 0) {
				for (var i = 0; i < firmNameArray.length; i++) {
					this.printTextInCenter(firmNameArray[i], LARGE, true);
				}
			}
			this.printLineFeed();
		}

		if (settingCache.isPrintCompanyAddressEnabled() && firm.getFirmAddress().trim()) {
			var firmAddressArray = firm.getFirmAddress().split('\n');
			if (firmAddressArray && firmAddressArray.length > 0) {
				for (var _i2 = 0; _i2 < firmAddressArray.length; _i2++) {
					this.printTextInCenter(firmAddressArray[_i2], NORMAL);
				}
			}
			this.printLineFeed();
		}

		if (firm.getFirmState()) {
			this.printTextInCenter('State: ' + StateCode.getStateCode(firm.getFirmState()) + '-' + firm.getFirmState(), NORMAL);
			this.printLineFeed();
		}

		if (firm.getFirmPhone() && settingCache.isPrintCompanyNumberEnabled()) {
			this.printTextInCenter(firm.getFirmPhone(), NORMAL);
			this.printLineFeed();
		}

		if (firm.getFirmEmail() && settingCache.isCompnayEmailEnabledOnPrint()) {
			this.printTextInCenter(firm.getFirmEmail(), NORMAL);
			this.printLineFeed();
		}

		if (settingCache.getGSTEnabled()) {
			this.printGSTIN(firm);
		} else {
			this.printTIN(firm);
		}

		if (StateCode.isUnionTerritory(firm.getFirmState())) {
			this.stateTaxText = 'UTGST';
		}
		var udfCache = new UDFCache();
		var udfArray = firm.getUdfObjectArray();
		for (var _i3 = 0; _i3 < udfArray.length; _i3++) {
			var udfValue = udfArray[_i3];
			var udfModel = udfCache.getUdfFirmModelByFirmAndFieldId(firm.getFirmId(), udfValue.getUdfFieldId());
			if (udfModel.getUdfFieldPrintOnInvoice() == 1 && udfModel.getUdfFieldStatus() == 1) {
				var udfFieldValue = udfValue.getUdfFieldValue() || '';
				this.printTextInCenter(udfModel.getUdfFieldName() + ': ' + udfFieldValue, NORMAL);
				this.printLineFeed();
			}
		}
	};

	this.printTIN = function (firm) {
		if ((this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN) && settingCache.isPrintTINEnabled() && firm.getFirmTin()) {
			this.printTextInCenter(settingCache.getTINText() + ': ' + firm.getFirmTin(), NORMAL);
			this.printLineFeed();
		}
	};

	this.printGSTIN = function (firm) {
		if ((this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN) && settingCache.isPrintTINEnabled() && firm.getFirmHSNSAC()) {
			this.printTextInCenter('GSTIN : ' + firm.getFirmHSNSAC(), NORMAL);
			this.printLineFeed();
		}
	};

	this.printTransactionHeader = function () {
		var transactionFactory = new TransactionFactory();
		var transactionHTMLGenerator = new TransactionHTMLGenerator();
		this.printTextInCenter(transactionFactory.getTransTypeStringForTransactionPDF(this.txn.getTxnType(), this.txn.getTxnSubType(), false, transactionHTMLGenerator.isNonTaxBill(this.txn)), BOLD, true);
		this.printLineFeed();
	};

	this.printPartyInfoAndTxnHeaderData = function () {
		try {
			var party = this.txn.getNameRef();

			var txnRefText = '';

			if (party) {
				var partyFullName = '';
				if (this.txn.getDisplayName() && this.txn.getDisplayName().trim()) {
					partyFullName = this.txn.getDisplayName().trim();
				} else {
					partyFullName = party.getFullName();
				}
				if (partyFullName) {
					partyFullName = partyFullName.replace(/(?:\r\n|\r|\n)/g, ' ');
				}

				var txnDateStr = 'Date: ' + MyDate.getDate('d-m-y', this.txn.getTxnDate());

				this.printIn2Cols(partyFullName, txnDateStr, NO_OF_CHARS, true, LEFT, NORMAL, LEFT);

				if (!MyString.isEmpty(party.getAddress())) {
					var partyAddress = party.getAddress();
					partyAddress = partyAddress.replace('\n', ' ');

					if (!MyString.isEmpty(this.txn.getFullTxnRefNumber())) {
						txnRefText = this.getTxnRefText();
					}

					this.printIn2Cols(partyAddress, txnRefText, NO_OF_CHARS, true, LEFT, NORMAL, LEFT);
				}

				if (settingCache.getGSTEnabled() && settingCache.isPlaceOfSupplyEnabled()) {
					if (this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
						var placeOfSupply = this.txn.getPlaceOfSupply();
						if (MyString.isEmpty(placeOfSupply)) {
							if (this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
								var firm = firmCache.getTransactionFirmWithDefault(this.txn);
								placeOfSupply = firm.getFirmState();
							} else {
								placeOfSupply = party.getContactState();
							}
						}

						if (!MyString.isEmpty(placeOfSupply)) {
							var partyState = placeOfSupply;

							if (MyString.isEmpty(txnRefText) && !MyString.isEmpty(this.txn.getFullTxnRefNumber())) {
								txnRefText = this.getTxnRefText();
								this.printIn2Cols(partyState, txnRefText, NO_OF_CHARS, true, LEFT, NORMAL, LEFT);
							} else {
								this.printTextOnLeft(partyState, NORMAL);
								this.printLineFeed();
							}
						}
					}
				}

				if (!MyString.isEmpty(party.getPhoneNumber())) {
					if (MyString.isEmpty(txnRefText) && !MyString.isEmpty(this.txn.getFullTxnRefNumber())) {
						txnRefText = this.getTxnRefText();
						this.printIn2Cols('Ph.: ' + party.getPhoneNumber(), txnRefText, NO_OF_CHARS, true, LEFT, NORMAL, LEFT);
					} else {
						this.printTextOnLeft('Ph.: ' + party.getPhoneNumber(), NORMAL);
						this.printLineFeed();
					}
				}

				if (settingCache.isTINNumberEnabled()) {
					if (settingCache.getGSTEnabled()) {
						if (!MyString.isEmpty(party.getGstinNumber())) {
							if (MyString.isEmpty(txnRefText) && !MyString.isEmpty(this.txn.getFullTxnRefNumber())) {
								txnRefText = this.getTxnRefText();
								this.printIn2Cols('GSTIN:' + party.getGstinNumber(), txnRefText, NO_OF_CHARS, true, LEFT, NORMAL, LEFT);
							} else {
								this.printTextOnLeft('GSTIN:' + party.getGstinNumber(), NORMAL);
								this.printLineFeed();
							}
						}
					} else {
						if (!MyString.isEmpty(party.getTinNumber())) {
							if (MyString.isEmpty(txnRefText) && !MyString.isEmpty(this.txn.getFullTxnRefNumber())) {
								txnRefText = this.getTxnRefText();
								this.printIn2Cols(settingCache.getTINText() + ': ' + party.getTinNumber(), txnRefText, NO_OF_CHARS, true, LEFT, NORMAL, LEFT);
							} else {
								this.printTextOnLeft(settingCache.getTINText() + ': ' + party.getTinNumber(), NORMAL);
								this.printLineFeed();
							}
						}
					}
				}

				var partyUDF = party.getUdfObjectArray();
				var udfCache = new UDFCache();
				if (partyUDF && partyUDF.length > 0) {
					for (var i = 0; i < partyUDF.length; i++) {
						var value = partyUDF[i];
						var model = udfCache.getUdfPartyModelByFieldId(value.getUdfFieldId());
						if (value.getUdfFieldValue() && model.getUdfFieldPrintOnInvoice() == 1) {
							var displayValue = value.getDisplayValue(model);
							this.printTextOnLeft(model.getUdfFieldName() + ': ' + displayValue, NORMAL);
							this.printLineFeed();
						}
					}
				}

				if (MyString.isEmpty(txnRefText) && !MyString.isEmpty(this.txn.getFullTxnRefNumber())) {
					txnRefText = this.getTxnRefText();
					this.printTextOnRight(txnRefText, NORMAL);
				}

				if (settingCache.isPrintPartyShippingAddressEnabled() && (!MyString.isEmpty(party.getShippingAddress()) || !MyString.isEmpty(this.txn.getShippingAddress()))) {
					var shipAddressHeader = '';
					switch (this.txn.getTxnType()) {
						case TxnTypeConstant.TXN_TYPE_SALE:
						case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
						case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
							shipAddressHeader = 'Ship To:';
							break;
						case TxnTypeConstant.TXN_TYPE_PURCHASE:
						case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
						case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
							shipAddressHeader = 'Ship From:';
							break;
					}

					if (!MyString.isEmpty(shipAddressHeader)) {
						this.printTextOnLeft(shipAddressHeader, NORMAL);
						var shippingAddress = this.txn.getShippingAddress() || party.getShippingAddress();
						shippingAddress = shippingAddress.split('\n');
						if (shippingAddress != null && shippingAddress.length > 0) {
							for (var i = 0; i < shippingAddress.length; i++) {
								var add = shippingAddress[i];
								this.printTextOnLeft(add, NORMAL);
							}
						}
						this.printLineFeed();
					}
				}
				if (this.txn.getPODate()) {
					this.printTextOnRight('PO Date : ' + MyDate.convertDateToStringForUI(this.txn.getPODate()), NORMAL);
					this.printLineFeed();
				}

				if (!MyString.isEmpty(this.txn.getPONumber())) {
					this.printTextOnRight('PO No.: ' + this.txn.getPONumber(), NORMAL);
					this.printLineFeed();
				}
				this.printDueDate();
				if (this.txn.getUdfObjectArray()) {
					for (var i = 0; i < this.txn.getUdfObjectArray().length; i++) {
						var value = this.txn.getUdfObjectArray()[i];
						var fieldModel = udfCache.getUdfModelByFirmAndFieldId(this.txn.getFirmId(), value.getUdfFieldId());
						if (fieldModel.getUdfFieldPrintOnInvoice() == 1 && value.getUdfFieldValue() && fieldModel.getUdfTxnType() == this.txn.getTxnType() && value.getUdfRefId() == this.txn.getTxnId()) {
							var _displayValue = value.getDisplayValue(fieldModel);
							this.printTextOnRight(fieldModel.getUdfFieldName() + ': ' + _displayValue);
							this.printLineFeed();
						}
					}
				}
			}
		} catch (err) {
			if (logger) {
				logger.error('Thermal printer print error: ' + err);
			}
			debugger;
		}
	};

	this.getTxnRefText = function () {
		switch (this.txn.getTxnType()) {
			case TxnTypeConstant.TXN_TYPE_SALE:
				return this.extraPad() + 'Invoice No: ' + this.txn.getFullTxnRefNumber();
			case TxnTypeConstant.TXN_TYPE_PURCHASE:
				return this.extraPad() + 'Bill No: ' + this.txn.getFullTxnRefNumber();
			case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
				return this.extraPad() + 'Return No: ' + this.txn.getFullTxnRefNumber();
			case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
				return this.extraPad() + 'Return No: ' + this.txn.getFullTxnRefNumber();
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
			case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
				return this.extraPad() + 'Order No: ' + this.txn.getFullTxnRefNumber();
			case TxnTypeConstant.TXN_TYPE_ESTIMATE:
				return this.extraPad() + 'Ref No.' + this.txn.getFullTxnRefNumber();
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
				return this.extraPad() + 'Challan No.' + this.txn.getFullTxnRefNumber();
			case TxnTypeConstant.TXN_TYPE_CASHIN:
			case TxnTypeConstant.TXN_TYPE_CASHOUT:
				return this.extraPad() + 'Receipt No.' + this.txn.getFullTxnRefNumber();
			default:
				return '';
		}
	};

	this.printDescription = function () {
		var description = this.txn.getDescription();
		if (!MyString.isEmpty(description)) {
			var descriptionArray = description.split('\n');
			if (descriptionArray != null && descriptionArray.length > 0) {
				for (var i = 0; i < descriptionArray.length; i++) {
					var desc = descriptionArray[i];
					this.printTextOnLeft(desc, NORMAL);
				}
				this.printLineFeed();
			}
		}
	};

	this.printTermAndCondition = function () {
		var termsAndConditionText = void 0;
		switch (this.txn.getTxnType()) {
			case TxnTypeConstant.TXN_TYPE_SALE:
				termsAndConditionText = settingCache.getTermsAndConditionSaleInvoivce();
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
				termsAndConditionText = settingCache.getTermsAndConditionSaleOrder();
				break;
			case TxnTypeConstant.TXN_TYPE_ESTIMATE:
				termsAndConditionText = settingCache.getTermsAndConditionEstimateQuotation();
				break;
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
				termsAndConditionText = settingCache.getTermsAndConditionDeliveryChallan();
				break;
		}
		if (termsAndConditionText) {
			var termsArray = termsAndConditionText.split('\n');
			if (termsArray && termsArray.length > 0) {
				for (var i in termsArray) {
					this.printTextOnLeft(termsArray[i], NORMAL);
				}
				this.printLineFeed();
			}
		}
	};

	this.printDueDate = function () {
		var dueDate = this.txn.getTxnDueDate();
		if (dueDate != null) {
			if (this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER || (this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || this.txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE) && settingCache.isPaymentTermEnabled() && !MyDate.isSameDate(this.txn.getTxnDate(), this.txn.getTxnDueDate())) {
				this.printTextOnRight('Due Date : ' + MyDate.getDate('d-m-y', dueDate), NORMAL);
				this.printLineFeed();
			}
		}
	};

	this.printIn2Cols = function (text1, text2, totalWidth, wrapFirstCol, wrappedTextAlign, textStyle, sectionAlign) {
		text1 = text1.toString();
		text2 = text2.toString();
		try {
			if (wrapFirstCol) {
				if (totalWidth < text2.length) {
					this.printIn2ColsWithBothWrap(text1, text2, totalWidth, LEFT, LEFT, textStyle, sectionAlign);
				} else {
					var width = totalWidth - text2.length;
					do {
						var holder = { continueLoop: false };
						var stringBuilder = { stringText: '' };

						text1 = this.wrapTextIfNeededWithPadding(text1, stringBuilder, width, wrappedTextAlign, holder);
						stringBuilder.stringText += text2;
						text2 = '';

						if (sectionAlign == RIGHT) {
							this.printTextOnRight(stringBuilder.stringText.toString(), textStyle);
						} else {
							this.printTextOnLeft(stringBuilder.stringText.toString(), textStyle);
						}
						this.printLineFeed();
					} while (holder.continueLoop);
				}
			} else {
				if (totalWidth < text1.length) {
					this.printIn2ColsWithBothWrap(text1, text2, totalWidth, LEFT, LEFT, textStyle, sectionAlign);
				} else {
					var width = totalWidth - text1.length;

					do {
						var holder = { continueLoop: false };
						var stringBuilder = { stringText: '' };

						stringBuilder.stringText += text1;
						text1 = this.fill(text1.length, '', SPACE_CHAR, LEFT);

						text2 = this.wrapTextIfNeeded(text2, stringBuilder, width, wrappedTextAlign, holder);

						if (sectionAlign == RIGHT) {
							this.printTextOnRight(stringBuilder.stringText.toString(), textStyle);
						} else {
							this.printTextOnLeft(stringBuilder.stringText.toString(), textStyle);
						}
						this.printLineFeed();
					} while (holder.continueLoop);
				}
			}
		} catch (err) {
			if (logger) {
				logger.error('Thermal printer print error: ' + err);
			}
			debugger;
		}
	};

	this.printTotalBalanceSection = function () {
		try {
			var stringBuilder = { stringText: '' };

			var balanceAmount = MyDouble.convertStringToDouble(this.txn.getBalanceAmount());
			var cashAmount = MyDouble.convertStringToDouble(this.txn.getCashAmount());
			var discountAmount = MyDouble.convertStringToDouble(this.txn.getDiscountAmount());
			var totalAmount = MyDouble.convertStringToDouble(balanceAmount + cashAmount);
			var taxAmount = MyDouble.convertStringToDouble(this.txn.getTaxAmount());

			var sectionWidth = NO_OF_CHARS / 1.3;

			var txnType = this.txn.getTxnType();

			if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
				totalAmount = totalAmount + discountAmount;
			} else {
				balanceAmount = this.txn.getTxnCurrentBalanceAmount() ? Number(this.txn.getTxnCurrentBalanceAmount()) : 0; // setting current balance amount
				cashAmount = totalAmount - balanceAmount; // setting total received amount
			}

			if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN) {
				this.printIn2Cols('Received :', MyDouble.getAmountForThermalPrinter(cashAmount), sectionWidth, true, LEFT, NORMAL, RIGHT);
			} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
				this.printIn2Cols('Paid :', MyDouble.getAmountForThermalPrinter(cashAmount), sectionWidth, true, LEFT, NORMAL, RIGHT);
			}

			if (discountAmount != 0) {
				if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
					stringBuilder.stringText = '';

					var discountText = 'Discount : ';
					this.printIn2Cols(discountText, MyDouble.getAmountForThermalPrinter(discountAmount), sectionWidth, true, LEFT, NORMAL, RIGHT);
				} else {
					stringBuilder.stringText = '';

					var discountText = 'Discount(' + MyDouble.getPercentageWithDecimal(this.txn.getDiscountPercent()) + '%)';
					this.printIn2Cols(discountText, MyDouble.getAmountForThermalPrinter(discountAmount), sectionWidth, true, LEFT, NORMAL, RIGHT);
				}
			}

			if (taxAmount != 0) {
				var taxText = 'Tax (' + MyDouble.getPercentageWithDecimal(this.txn.getTaxPercent()) + '%)';
				this.printIn2Cols(taxText, MyDouble.getAmountForThermalPrinter(taxAmount), sectionWidth, true, LEFT, NORMAL, RIGHT);
			}

			if (this.txn.getAc1Amount() != 0) {
				this.printIn2Cols(dataLoader.getCurrentExtraChargesValue('1'), MyDouble.getAmountForThermalPrinter(this.txn.getAc1Amount()), sectionWidth, true, LEFT, NORMAL, RIGHT);
			}

			if (this.txn.getAc2Amount() != 0) {
				this.printIn2Cols(dataLoader.getCurrentExtraChargesValue('2'), MyDouble.getAmountForThermalPrinter(this.txn.getAc2Amount()), sectionWidth, true, LEFT, NORMAL, RIGHT);
			}

			if (this.txn.getAc3Amount() != 0) {
				this.printIn2Cols(dataLoader.getCurrentExtraChargesValue('3'), MyDouble.getAmountForThermalPrinter(this.txn.getAc3Amount()), sectionWidth, true, LEFT, NORMAL, RIGHT);
			}

			if (this.txn.getRoundOffValue() != 0 && txnType != TxnTypeConstant.TXN_TYPE_CASHIN && txnType != TxnTypeConstant.TXN_TYPE_CASHOUT) {
				this.printIn2Cols('Round Off :', MyDouble.getAmountForThermalPrinter(this.txn.getRoundOffValue()), sectionWidth, true, LEFT, NORMAL, RIGHT);
			}

			if (txnType != TxnTypeConstant.TXN_TYPE_CASHIN && txnType != TxnTypeConstant.TXN_TYPE_CASHOUT || (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) && discountAmount != 0) {
				this.printIn2Cols('Total :', MyDouble.getAmountForThermalPrinter(totalAmount), sectionWidth, false, RIGHT, NORMAL, RIGHT);
			}

			if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
				this.printIn2Cols('Paid :', MyDouble.getAmountForThermalPrinter(cashAmount), sectionWidth, false, RIGHT, NORMAL, RIGHT);
			}
			if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
				if (txnType != TxnTypeConstant.TXN_TYPE_SALE || settingCache.isReceivedAmountEnabled()) {
					this.printIn2Cols('Received :', MyDouble.getAmountForThermalPrinter(cashAmount), sectionWidth, false, RIGHT, NORMAL, RIGHT);
				}
			}
			if (txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
				this.printIn2Cols('Advance :', MyDouble.getAmountForThermalPrinter(cashAmount), sectionWidth, false, RIGHT, NORMAL, RIGHT);
			}
			if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
				if (txnType != TxnTypeConstant.TXN_TYPE_SALE || settingCache.isBalanceAmountEnabled()) {
					this.printIn2Cols('Balance :', MyDouble.getAmountForThermalPrinter(balanceAmount), sectionWidth, false, RIGHT, NORMAL, RIGHT);
				}
			}

			if (settingCache.isCurrentBalanceOfPartyEnabled()) {
				try {
					var name = this.txn.getNameRef();
					switch (txnType) {
						case TxnTypeConstant.TXN_TYPE_CASHIN:
						case TxnTypeConstant.TXN_TYPE_SALE:
						case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
						case TxnTypeConstant.TXN_TYPE_CASHOUT:
						case TxnTypeConstant.TXN_TYPE_PURCHASE:
						case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
						case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
							if (name != null) {
								var currentAmount = MyDouble.getAmountForThermalPrinter(name.getAmount());
								var showPreviousBalance = true;
								if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
									var txnBalAmount = MyDouble.convertStringToDouble(this.txn.getBalanceAmount());
									var txnCurrentBalAmount = MyDouble.convertStringToDouble(this.txn.getTxnCurrentBalanceAmount());
									if (Math.abs(txnBalAmount - txnCurrentBalAmount) > 0.001) {
										showPreviousBalance = false;
									}
								}
								if (showPreviousBalance) {
									showPreviousBalance = TransactionPrintHelper.isLatestTransaction(this.txn);
								}
								if (showPreviousBalance) {
									var prevBal = TransactionPrintHelper.getPreviousAmount(this.txn, name);
									this.printIn2Cols('Previous Bal. :', MyDouble.getAmountForThermalPrinter(prevBal), sectionWidth, true, LEFT, NORMAL, RIGHT);
								}

								if (currentAmount && txnType != TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN) {
									this.printIn2Cols('Current Bal. :', currentAmount, sectionWidth, true, LEFT, NORMAL, RIGHT);
								}
							}
							break;
					}
				} catch (err) {
					if (logger) {
						logger.error('Thermal printer print error: ' + err);
					}
					debugger;
				}
			}
		} catch (err) {
			if (logger) {
				logger.error('Thermal printer print error: ' + err);
			}
			debugger;
		}
	};

	this.printIn2ColsWithBothWrap = function (text1, text2, totalWidth, firstTextAlign, secondTextAlign, textStyle, sectionAlign) {
		text1 = text1.toString();
		text2 = text2.toString();
		try {
			var stringBuilder = { stringText: '' };

			var width = Math.round(totalWidth / 2);
			var _holder = { continueLoop: true };

			do {
				_holder.continueLoop = false;
				stringBuilder.stringText = '';

				text1 = this.wrapTextIfNeededWithPadding(text1, stringBuilder, width, firstTextAlign, _holder);

				text2 = this.wrapTextIfNeededWithPadding(text2, stringBuilder, totalWidth - width, secondTextAlign, _holder);

				if (sectionAlign == RIGHT) {
					this.printTextOnRight(stringBuilder.stringText.toString(), textStyle);
				} else {
					this.printTextOnLeft(stringBuilder.stringText.toString(), textStyle);
				}
				this.printLineFeed();
			} while (_holder.continueLoop);
		} catch (err) {
			if (logger) {
				logger.error('Thermal printer print error: ' + err);
			}
			debugger;
		}
	};

	this.cutPaper = function () {
		this.print(PrinterCommands.FEED_PAPER_AND_CUT);
	};

	this.printTaxDetails = function () {
		var taxDetailMap = new _map2.default();
		var totalAdditionalCESSAmount = 0;

		var sectionWidth = NO_OF_CHARS / 1.5;

		var baseLineItems = this.txn.getLineItems();

		if (baseLineItems != null && baseLineItems.length > 0) {
			for (var i = 0; i < baseLineItems.length; i++) {
				var baseLineItem = baseLineItems[i];

				if (baseLineItem.getLineItemTaxId()) {
					var taxCode = taxCodeCache.getTaxCodeObjectById(baseLineItem.getLineItemTaxId());

					if (taxCode.getTaxType() == TaxCodeConstants.taxGroup) {
						var taxCodeIds = (0, _values2.default)(taxCode.getTaxCodeMap());

						if (taxCodeIds.length > 0) {
							for (var j = 0; j < taxCodeIds.length; j++) {
								var taxCodeId = taxCodeIds[j];
								if (taxDetailMap.has(taxCodeId)) {
									taxDetailMap.get(taxCodeId).push(baseLineItem.getItemQuantity() * baseLineItem.getItemUnitPrice() - baseLineItem.getLineItemDiscountAmount());
								} else {
									var list = [];
									list.push(baseLineItem.getItemQuantity() * baseLineItem.getItemUnitPrice() - baseLineItem.getLineItemDiscountAmount());
									taxDetailMap.set(taxCodeId, list);
								}
							}
						}
					} else {
						if (taxDetailMap.has(taxCode.getTaxCodeId())) {
							taxDetailMap.get(taxCode.getTaxCodeId()).push(baseLineItem.getItemQuantity() * baseLineItem.getItemUnitPrice() - baseLineItem.getLineItemDiscountAmount());
						} else {
							var list = [];
							list.push(baseLineItem.getItemQuantity() * baseLineItem.getItemUnitPrice() - baseLineItem.getLineItemDiscountAmount());
							taxDetailMap.set(taxCode.getTaxCodeId(), list);
						}
					}
				}

				if (baseLineItem.getLineItemAdditionalCESS() != 0 && baseLineItem.getLineItemAdditionalCESS() != null) {
					totalAdditionalCESSAmount += MyDouble.convertStringToDouble(baseLineItem.getLineItemAdditionalCESS());
				}
			}
		}

		if (this.txn.getTransactionTaxId() != 0 && this.txn.getTransactionTaxId() != null) {
			var taxCode = taxCodeCache.getTaxCodeObjectById(this.txn.getTransactionTaxId());

			if (taxCode.getTaxType() == TaxCodeConstants.taxGroup) {
				var taxCodeIds = (0, _values2.default)(taxCode.getTaxCodeMap());
				if (taxCodeIds.length > 0) {
					for (i = 0; i < taxCodeIds.length; i++) {
						var taxCodeId = taxCodeIds[i];
						if (taxDetailMap.has(taxCodeId)) {
							taxDetailMap.get(taxCodeId).push(this.txn.getSubTotalAmount() - this.txn.getDiscountAmount());
						} else {
							var list = [];
							list.push(this.txn.getSubTotalAmount() - this.txn.getDiscountAmount());
							taxDetailMap.set(taxCodeId, list);
						}
					}
				}
			} else {
				if (taxDetailMap.has(this.txn.getTransactionTaxId())) {
					taxDetailMap.get(this.txn.getTransactionTaxId()).push(this.txn.getSubTotalAmount() - this.txn.getDiscountAmount());
				} else {
					var list = [];
					list.push(this.txn.getSubTotalAmount() - this.txn.getDiscountAmount());
					taxDetailMap.set(this.txn.getTransactionTaxId(), list);
				}
			}
		}

		if (taxDetailMap.size > 0 || totalAdditionalCESSAmount != 0) {
			this.printTextInCenter('Tax Details', BOLD, true);
			this.printLineFeed();
		}

		if (taxDetailMap.size > 0) {
			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;

			try {
				for (var _iterator = (0, _getIterator3.default)(taxDetailMap.keys()), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					var taxId = _step.value;

					var taxCode = taxCodeCache.getTaxCodeObjectById(taxId);
					var taxAmount = 0;

					var taxDetailMapVal = taxDetailMap.get(taxId);

					for (var i = 0; i < taxDetailMapVal.length; i++) {
						var amount = taxDetailMapVal[i];
						taxAmount += MyDouble.getBalanceAmountWithDecimal(taxCode.getTaxRate() * amount / 100);
					}

					switch (taxCode.getTaxRateType()) {
						case TaxCodeConstants.IGST:
							this.printTextOnRight('IGST' + ' @ ' + MyDouble.getPercentageWithDecimal(taxCode.getTaxRate()) + '% : ' + MyDouble.getAmountForThermalPrinter(taxAmount), NORMAL);
							this.printLineFeed();
							break;
						case TaxCodeConstants.CGST:
							this.printTextOnRight('CGST' + ' @ ' + MyDouble.getPercentageWithDecimal(taxCode.getTaxRate()) + '% : ' + MyDouble.getAmountForThermalPrinter(taxAmount), NORMAL);
							this.printLineFeed();
							break;
						case TaxCodeConstants.SGST:
							this.printTextOnRight(this.stateTaxText + ' @ ' + MyDouble.getPercentageWithDecimal(taxCode.getTaxRate()) + '% : ' + MyDouble.getAmountForThermalPrinter(taxAmount), NORMAL);
							this.printLineFeed();
							break;
						case TaxCodeConstants.CESS:
							this.printTextOnRight('CESS' + ' @ ' + MyDouble.getPercentageWithDecimal(taxCode.getTaxRate()) + '% : ' + MyDouble.getAmountForThermalPrinter(taxAmount), NORMAL);
							this.printLineFeed();
							break;
						case TaxCodeConstants.STATE_SPECIFIC_CESS:
							this.printTextOnRight(taxCode.getTaxCodeName() + ' @ ' + MyDouble.getPercentageWithDecimal(taxCode.getTaxRate()) + '% : ' + MyDouble.getAmountForThermalPrinter(taxAmount), NORMAL);
							this.printLineFeed();
							break;
						case TaxCodeConstants.OTHER:
							this.printTextOnRight(taxCode.getTaxCodeName() + ' @ ' + MyDouble.getPercentageWithDecimal(taxCode.getTaxRate()) + '% : ' + MyDouble.getAmountForThermalPrinter(taxAmount), NORMAL);
							this.printLineFeed();
							break;
						case TaxCodeConstants.Exempted:
							this.printTextOnRight('EXEMPTED' + ' @ ' + MyDouble.getPercentageWithDecimal(taxCode.getTaxRate()) + '% : ' + MyDouble.getAmountForThermalPrinter(taxAmount), NORMAL);
							this.printLineFeed();
							break;
					}
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}
		}

		if (totalAdditionalCESSAmount != 0) {
			this.printTextOnRight('Add. CESS : ' + MyDouble.getAmountForThermalPrinter(totalAdditionalCESSAmount), NORMAL);
			this.printLineFeed();
		}
	};

	this.printItemTableHeader = function () {
		try {
			var _holder2 = { continueLoop: true };
			var _rowText = { stringText: '' };

			var srHeader = 'SR';
			var itemNameHeader = 'Name';
			var quantityHeader = 'Qty';
			var unitPriceHeader = 'Price';
			var amountHeader = 'Amount';

			var printAmounts = this.txn.getTxnType() != TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || settingCache.printAmountDetailsInDeliveryChallan();

			do {
				_holder2.continueLoop = false;
				_rowText.stringText = '';

				_rowText.stringText += this.fill(SR_WIDTH, srHeader, SPACE_CHAR, LEFT);
				srHeader = '';

				itemNameHeader = this.wrapTextIfNeededWithPadding(itemNameHeader, _rowText, ITEM_NAME_WIDTH, LEFT, _holder2);

				quantityHeader = this.wrapTextIfNeededWithPadding(quantityHeader, _rowText, QUANTITY_WIDTH, RIGHT, _holder2);

				if (printAmounts) {
					unitPriceHeader = this.wrapTextIfNeededWithPadding(unitPriceHeader, _rowText, UNIT_PRICE_WIDTH, RIGHT, _holder2);

					amountHeader = this.wrapTextIfNeeded(amountHeader, _rowText, AMOUNT_WIDTH, RIGHT, _holder2);
				}

				this.printTextOnLeft(_rowText.stringText.toString(), NORMAL);
			} while (_holder2.continueLoop);
		} catch (err) {
			if (logger) {
				logger.error('Thermal printer print error: ' + err);
			}
		}
	};

	this.printBody = function () {
		var lineItems = this.txn.getLineItems();
		this.isThereAnyLineItems = lineItems.length > 0;

		if (this.isThereAnyLineItems) {
			this.printItemTableHeader();
			this.printLineFeed();
			this.printDashLine();
			this.printLineFeed();
		}
		if (lineItems.length > 0) {
			var quantityTotal = 0;
			var freeQuantityTotal = 0;
			var index = 0;
			for (var i = 0; i < lineItems.length; i++) {
				var baseLineItem = lineItems[i];
				index++;
				this.printItemRow(baseLineItem, index);

				if (baseLineItem.getLineItemUnitMappingId() > 0) {
					var mappingObj = itemUnitMappingCache.getItemUnitMapping(baseLineItem.getLineItemUnitMappingId());
					var secondaryQuantity = MyDouble.convertStringToDouble(baseLineItem.getItemQuantity()) * mappingObj.getConversionRate();
					var secondaryFreeQuantity = MyDouble.convertStringToDouble(baseLineItem.getItemFreeQuantity()) * mappingObj.getConversionRate();
					quantityTotal += secondaryQuantity;
					freeQuantityTotal += secondaryFreeQuantity;
				} else {
					quantityTotal += MyDouble.convertStringToDouble(baseLineItem.getItemQuantity());
					freeQuantityTotal += MyDouble.convertStringToDouble(baseLineItem.getItemFreeQuantity());
				}
			}

			this.printDashLine();
			this.printItemRowForTotalQuantity(quantityTotal, freeQuantityTotal);
		}
	};

	this.printDashLine = function () {
		this.printTextOnLeft(this.fill(NO_OF_CHARS, '', DASH_CHAR, LEFT), NORMAL);
	};

	this.printItemRow = function (baseLineItem, index) {
		try {
			holder = { continueLoop: true };
			rowText = { stringText: '' };

			var itemSr = index.toString();
			var itemName = baseLineItem.getItemName().replace('\n', ' ');
			var itemQty = MyDouble.getQuantityWithDecimalWithoutColor(baseLineItem.getItemQuantity()) + MyDouble.quantityDoubleToStringWithSignExplicitly(baseLineItem.getItemFreeQuantity(), true);
			var itemUnitPrice = MyDouble.getAmountForThermalPrinter(baseLineItem.getItemUnitPrice());
			var itemAmount = '0.0';

			var showTaxForItem = false;
			var showDiscountForItem = false;
			var showAdditionalCESSForItem = false;

			var printAmounts = this.txn.getTxnType() != TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || settingCache.printAmountDetailsInDeliveryChallan();

			if (settingCache.isItemwiseTaxEnabled() && baseLineItem.getLineItemTaxId() && baseLineItem.getLineItemTaxId() != 0) {
				showTaxForItem = true;
			}
			if (settingCache.isItemwiseDiscountEnabled() && baseLineItem.getLineItemDiscountAmount() != 0) {
				showDiscountForItem = true;
			}
			if (settingCache.getAdditionalCessEnabled() && baseLineItem.getLineItemAdditionalCESS() != 0) {
				showAdditionalCESSForItem = true;
			}

			if (showDiscountForItem || showTaxForItem) {
				itemAmount = MyDouble.getAmountForThermalPrinter(baseLineItem.getItemQuantity() * baseLineItem.getItemUnitPrice());
			} else {
				itemAmount = MyDouble.getAmountForThermalPrinter(baseLineItem.getLineItemTotal());
			}

			if (baseLineItem.getLineItemUnitMappingId() != null) {
				var mappingObj = itemUnitMappingCache.getItemUnitMapping(baseLineItem.getLineItemUnitMappingId());
				itemQty = MyDouble.getQuantityWithDecimalWithoutColor(baseLineItem.getItemQuantity() * mappingObj.getConversionRate()) + MyDouble.quantityDoubleToStringWithSignExplicitly(baseLineItem.getItemFreeQuantity() * mappingObj.getConversionRate(), true);
				itemUnitPrice = MyDouble.getAmountForThermalPrinter(baseLineItem.getItemUnitPrice() / mappingObj.getConversionRate());
			}

			if (settingCache.getItemUnitEnabled() && this.txn.getTxnType() != TxnTypeConstant.TXN_TYPE_EXPENSE && this.txn.getTxnType() != TxnTypeConstant.TXN_TYPE_OTHER_INCOME) {
				if (baseLineItem.getLineItemUnitId() > 0) {
					var itemUnitShortName = itemUnitCache.getItemUnitShortNameById(baseLineItem.getLineItemUnitId());
					if (itemUnitShortName) {
						itemQty += itemUnitShortName;
					}
				}
			}

			do {
				holder.continueLoop = false;
				rowText.stringText = '';

				itemSr = this.wrapTextIfNeeded(itemSr, rowText, SR_WIDTH, LEFT, holder);

				itemName = this.wrapTextIfNeededWithPadding(itemName, rowText, ITEM_NAME_WIDTH, LEFT, holder);

				itemQty = this.wrapTextIfNeededWithPadding(itemQty, rowText, QUANTITY_WIDTH, RIGHT, holder);

				if (printAmounts) {
					itemUnitPrice = this.wrapTextIfNeededWithPadding(itemUnitPrice, rowText, UNIT_PRICE_WIDTH, RIGHT, holder);

					itemAmount = this.wrapTextIfNeeded(itemAmount, rowText, AMOUNT_WIDTH, RIGHT, holder);
				}

				this.printTextOnLeft(rowText.stringText.toString(), NORMAL);
			} while (holder.continueLoop);

			this.printLineFeed();

			var printDescription = TransactionPrintSettings.printItemDescriptionInInvoicePrint();

			if (printDescription) {
				var description = baseLineItem.getLineItemDescription();
				if (!MyString.isEmpty(description)) {
					var descriptionArray = description.split('\n');
					if (descriptionArray != null && descriptionArray.length > 0) {
						for (var i = 0; i < descriptionArray.length; i++) {
							var desc = descriptionArray[i];
							this.printTextOnLeft(desc, NORMAL);
						}
						this.printLineFeed();
					}
				}
			}

			var item = itemCache.getItemById(baseLineItem.getItemId());
			if (item && item.getItemHSNCode() && settingCache.getHSNSACEnabled()) {
				var hsnSacText;
				if (item.isItemInventory()) {
					hsnSacText = this.fill(SR_WIDTH, '', SPACE_CHAR, LEFT) + 'HSN : ';
				} else {
					hsnSacText = this.fill(SR_WIDTH, '', SPACE_CHAR, LEFT) + 'SAC : ';
				}
				this.printTextOnLeft(hsnSacText + item.getItemHSNCode(), NORMAL);
			}

			if (showDiscountForItem && printAmounts) {
				var discountText = 'Discount(' + MyDouble.getPercentageWithDecimal(baseLineItem.getLineItemDiscountPercent()) + '%):';
				var amount = '-' + MyDouble.getAmountForThermalPrinter(baseLineItem.getLineItemDiscountAmount());

				if (baseLineItem.getLineItemDiscountAmount() > 0 && amount) {
					this.printTextOnRight(discountText + this.fill(AMOUNT_WIDTH, amount, SPACE_CHAR, RIGHT), NORMAL);
					this.printLineFeed();
				}
			}

			if (showTaxForItem && printAmounts) {
				var taxText = taxCodeCache.getTaxCodeObjectById(baseLineItem.getLineItemTaxId()).getTaxCodeName() + ':';
				var subTotal = baseLineItem.getItemQuantity() * baseLineItem.getItemUnitPrice() - baseLineItem.getLineItemDiscountAmount();
				var taxAmount = baseLineItem.getLineItemTaxPercent() * subTotal / 100;
				var amount = MyDouble.getAmountForThermalPrinter(taxAmount);

				if (amount) {
					this.printTextOnRight(taxText + this.fill(AMOUNT_WIDTH, amount, SPACE_CHAR, RIGHT), NORMAL);
					this.printLineFeed();
				}
			}

			if (showAdditionalCESSForItem && printAmounts) {
				var additionalCESSText = 'Add. CESS:';
				var amount = MyDouble.getAmountForThermalPrinter(baseLineItem.getLineItemAdditionalCESS());

				if (amount && baseLineItem.getLineItemAdditionalCESS() != 0) {
					this.printTextOnRight(additionalCESSText + this.fill(AMOUNT_WIDTH, amount, SPACE_CHAR, RIGHT), NORMAL);
					this.printLineFeed();
				}
			}
		} catch (err) {
			if (logger) {
				logger.error('Thermal printer print error: ' + err);
			}
		}
	};

	this.printItemRowForTotalQuantity = function (totalQty, freeQuantityTotal) {
		try {
			var _holder3 = { continueLoop: true };
			var _rowText2 = { stringText: '' };

			var qtyText = '';
			var totalItemQty = '';

			if (settingCache.isPrintItemQuantityTotalEnabled()) {
				qtyText = 'Total';
				totalItemQty = MyDouble.getQuantityWithDecimalWithoutColor(totalQty) + MyDouble.quantityDoubleToStringWithSignExplicitly(freeQuantityTotal, true);
			}

			var subTotalText = '';
			var subTotal = MyDouble.getAmountForThermalPrinter(this.txn.getSubTotalAmount());

			var printAmounts = this.txn.getTxnType() != TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || settingCache.printAmountDetailsInDeliveryChallan();

			do {
				_holder3.continueLoop = false;
				_rowText2.stringText = '';

				qtyText = this.wrapTextIfNeededWithPadding(qtyText, _rowText2, ITEM_NAME_WIDTH + SR_WIDTH, LEFT, _holder3);

				totalItemQty = this.wrapTextIfNeededWithPadding(totalItemQty, _rowText2, QUANTITY_WIDTH, RIGHT, _holder3);

				if (printAmounts) {
					subTotalText = this.wrapTextIfNeededWithPadding(subTotalText, _rowText2, UNIT_PRICE_WIDTH, RIGHT, _holder3);

					subTotal = this.wrapTextIfNeeded(subTotal, _rowText2, AMOUNT_WIDTH, RIGHT, _holder3);
				}

				this.printTextOnLeft(_rowText2.stringText.toString(), NORMAL);
			} while (_holder3.continueLoop);
		} catch (err) {
			if (logger) {
				logger.error('Thermal printer print error: ' + err);
			}
		}
	};

	this.wrapTextIfNeededWithPadding = function (text, stringBuilder, width, align, holder) {
		text = text.toString();

		if (text.length < width) {
			stringBuilder.stringText += this.fill(width, text + this.extraPad(), SPACE_CHAR, align);
			text = '';
		} else {
			holder.continueLoop = true;

			var s = this.splitString(text, width - 1);
			stringBuilder.stringText += s[0] + this.extraPad();
			text = s[1];
		}

		return text;
	};

	this.wrapTextIfNeeded = function (text, stringBuilder, width, align, holder) {
		text = text.toString();
		if (text.length <= width) {
			stringBuilder.stringText += this.fill(width, text, SPACE_CHAR, align);
			text = '';
		} else {
			holder.continueLoop = true;

			var s = this.splitString(text, width);
			stringBuilder.stringText += s[0];
			text = s[1];
		}

		return text;
	};

	this.extraPad = function () {
		return ' ';
	};

	this.printTextOnRight = function (text) {
		var textStyle = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : NORMAL;
		var withEscCodes = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

		text = text.toString();

		if (withEscCodes && settingCache.useESCPOSCommandsInThermalPrinter()) {
			this.print(this.getTextSizeAndStyleBytes(textStyle));
			this.print(PrinterCommands.ESC_ALIGN_RIGHT);
			this.print(text.toString());
			this.print(this.getTextSizeAndStyleBytes(NORMAL));
		} else {
			this.print(this.fill(NO_OF_CHARS, text, SPACE_CHAR, RIGHT));
		}
	};

	this.printTextOnLeft = function (text) {
		var textStyle = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : NORMAL;
		var withEscCodes = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

		text = text.toString();

		if (withEscCodes && settingCache.useESCPOSCommandsInThermalPrinter()) {
			this.print(this.getTextSizeAndStyleBytes(textStyle));
			this.print(PrinterCommands.ESC_ALIGN_LEFT);
			this.print(text.toString());
			this.print(this.getTextSizeAndStyleBytes(NORMAL));
		} else {
			this.print(this.fill(NO_OF_CHARS, text, SPACE_CHAR, LEFT));
		}
	};

	this.printTextInCenter = function (text) {
		var textStyle = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : NORMAL;
		var withEscCodes = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

		text = text.toString();

		if (withEscCodes && settingCache.useESCPOSCommandsInThermalPrinter()) {
			this.print(this.getTextSizeAndStyleBytes(textStyle));
			this.print(PrinterCommands.ESC_ALIGN_CENTER);
			this.print(text);
			this.print(this.getTextSizeAndStyleBytes(NORMAL));
		} else {
			this.print(this.fill(NO_OF_CHARS, text, SPACE_CHAR, CENTER));
		}
	};

	this.getTextSizeAndStyleBytes = function () {
		var textStyle = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : NORMAL;

		var bytes = new Buffer([0x1b, 0x21, 0x00]);

		if (textStyle == BOLD) {
			bytes[2] = 0x8;
			return bytes;
		} else if (textStyle == NORMAL) {
			if (this.pageSize == ThermalPrinterConstant.INCH_2_PAGE_SIZE) {
				switch (this.textSize) {
					case ThermalPrinterConstant.SMALL:
						bytes[2] = 0x7;
						break;
					case ThermalPrinterConstant.MEDIUM:
						bytes[2] = 0x00;
						break;
				}
			} else {
				bytes[2] = 0x00;
			}
			return bytes;
		} else if (textStyle == LARGE) {
			bytes[2] = 0x20;
			return bytes;
		}

		return bytes;
	};

	this.fill = function (totalChars, text, fillWith, align) {
		var newText = '';

		text = text.toString();

		if (text.length <= totalChars) {
			var totalPad = totalChars - text.length;

			switch (align) {
				case CENTER:
					var left = Math.round(totalPad / 2);
					for (var i = 0; i < left; i++) {
						text += fillWith;
					}
					for (var _i4 = 0; _i4 < totalPad - left; _i4++) {
						text = fillWith + text;
					}
					return text;

				case RIGHT:
					for (var _i5 = 0; _i5 < totalPad; _i5++) {
						text = fillWith + text;
					}
					return text;
				case LEFT:
				default:
					for (var _i6 = 0; _i6 < totalPad; _i6++) {
						text += fillWith;
					}
					return text;
			}
		} else {
			var stringChunks = this.splitString(text, totalChars);
			for (var _i7 = 0; _i7 < stringChunks.length; _i7++) {
				newText += this.fill(totalChars, stringChunks[_i7], fillWith, align);
			}
		}
		return newText;
	};

	this.splitString = function (text, noOfChars) {
		text = text.toString();
		return [text.substr(0, noOfChars), text.substr(noOfChars)];
	};

	this.printLineFeed = function () {
		if (settingCache.useESCPOSCommandsInThermalPrinter()) {
			this.print(PrinterCommands.FEED_LINE);
		}
	};

	this.lineFeed = function () {
		if (!settingCache.useESCPOSCommandsInThermalPrinter()) {
			this.print(PrinterCommands.FEED_LINE);
		}
	};

	this.print = function (buff) {
		if (typeof buff === 'string') {
			var endBuff = null;
			for (var i = 0; i < buff.length; i++) {
				var value = buff[i];
				var tempBuff = new Buffer(value);
				if (endBuff) {
					endBuff = Buffer.concat([endBuff, tempBuff]);
				} else {
					endBuff = tempBuff;
				}
			}
			buff = endBuff;
		}

		if (this.textBuffer) {
			this.textBuffer = Buffer.concat([this.textBuffer, buff]);
		} else {
			this.textBuffer = buff;
		}
	};
};

module.exports = ThermalPrinterText;