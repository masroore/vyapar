/**
 * Created by Ishwar Rimal on 17-04-2017.
 */
var FirmObject = function FirmObject() {
	var ErrorCode = require('./../Constants/ErrorCode.js');
	var FirmModel = require('./../Models/FirmModel.js');
	var PrefixModel = require('./../Models/PrefixModel.js');
	var FirmCache = require('./../Cache/FirmCache.js');
	var TransactionManager = require('./../DBManager/TransactionManager.js');
	this.firmId;
	this.firmName;
	this.firmInvoicePrefix;
	this.firmTaxInvoicePrefix;
	this.firmInvoiceNumber;
	this.firmTaxInvoiceNumber;
	this.firmEstimateNumber;
	this.firmEstimatePrefix;
	this.firmDeliveryChallanPrefix = '';
	this.firmCashInPrefix;
	this.firmEmail;
	this.firmPhone;
	this.firmAddress;
	this.firmTin;
	this.firmLogoId;
	this.firmSignId;
	this.firmHSNSAC;
	this.firmState;
	this.bankName;
	this.bankIFSC;
	this.accountNumber;
	this.upiAccountNumber;
	this.upiIFSC;
	this.businessType;
	this.businessCategory;
	var extraFieldObjectArray = [];

	this.getFirmCashInPrefix = function () {
		return this.firmCashInPrefix;
	};
	this.setFirmCashInPrefix = function (firmCashInPrefix) {
		this.firmCashInPrefix = firmCashInPrefix;
	};

	this.getBusinessType = function () {
		return this.businessType;
	};
	this.setBusinessType = function (businessType) {
		this.businessType = businessType;
	};

	this.getBusinessCategory = function () {
		return this.businessCategory;
	};
	this.setBusinessCategory = function (businessCategory) {
		this.businessCategory = businessCategory;
	};

	this.getBankName = function () {
		return this.bankName;
	};
	this.setBankName = function (bankName) {
		this.bankName = bankName;
	};

	this.getBankIFSC = function () {
		return this.bankIFSC;
	};
	this.setBankIFSC = function (bankIFSC) {
		this.bankIFSC = bankIFSC;
	};

	this.getAccountNumber = function () {
		return this.accountNumber;
	};
	this.setAccountNumber = function (accountNumber) {
		this.accountNumber = accountNumber;
	};

	this.getUpiAccountNumber = function () {
		return this.upiAccountNumber;
	};
	this.setUpiAccountNumber = function (upiAccountNumber) {
		this.upiAccountNumber = upiAccountNumber;
	};

	this.getUpiIFSC = function () {
		return this.upiIFSC;
	};
	this.setUpiIFSC = function (upiIFSC) {
		this.upiIFSC = upiIFSC;
	};

	this.getFirmHSNSAC = function () {
		return this.firmHSNSAC;
	};
	this.setFirmHSNSAC = function (HSNSAC) {
		this.firmHSNSAC = HSNSAC;
	};

	this.getFirmState = function () {
		return this.firmState;
	};
	this.setFirmState = function (firmState) {
		this.firmState = firmState;
	};

	this.getFirmImagePath = function () {
		return this.firmImagePath;
	};
	this.setFirmImagePath = function (firmImagePath) {
		this.firmImagePath = firmImagePath;
	};

	this.getFirmSignImagePath = function () {
		return this.firmSignImagePath;
	};
	this.setFirmSignImagePath = function (firmSignImagePath) {
		this.firmSignImagePath = firmSignImagePath;
	};

	this.getFirmSignId = function () {
		return this.firmSignId;
	};

	this.setFirmSignId = function (firmSignId) {
		this.firmSignId = firmSignId;
	};

	this.getFirmEmail = function () {
		if (this.firmEmail == null) {
			return '';
		}
		return this.firmEmail;
	};

	this.setFirmEmail = function (firmEmail) {
		this.firmEmail = firmEmail;
	};

	this.getFirmPhone = function () {
		if (this.firmPhone == null) {
			return '';
		}
		return this.firmPhone;
	};

	this.setFirmPhone = function (firmPhone) {
		this.firmPhone = firmPhone;
	};

	this.getFirmAddress = function () {
		if (this.firmAddress == null) {
			return '';
		}
		return this.firmAddress;
	};

	this.setFirmAddress = function (firmAddress) {
		this.firmAddress = firmAddress;
	};

	this.getFirmTin = function () {
		if (this.firmTin == null) {
			return '';
		}
		return this.firmTin;
	};

	this.setFirmTin = function (firmTin) {
		this.firmTin = firmTin;
	};

	this.getFirmLogoId = function () {
		return this.firmLogoId;
	};

	this.setFirmLogoId = function (firmLogoId) {
		this.firmLogoId = firmLogoId;
	};

	this.getFirmId = function () {
		return this.firmId;
	};

	this.setFirmId = function (firmId) {
		this.firmId = firmId;
	};

	this.getFirmName = function () {
		if (this.firmName == null) {
			return '';
		}
		return this.firmName;
	};

	this.setFirmName = function (firmName) {
		this.firmName = firmName;
	};

	this.getFirmInvoicePrefix = function () {
		if (this.firmInvoicePrefix == null) {
			return '';
		}
		return this.firmInvoicePrefix;
	};

	this.setFirmInvoicePrefix = function (firmInvoicePrefix) {
		if (firmInvoicePrefix == null) {
			firmInvoicePrefix = '';
		}
		this.firmInvoicePrefix = firmInvoicePrefix;
	};

	this.getFirmTaxInvoicePrefix = function () {
		if (this.firmTaxInvoicePrefix == null) {
			this.firmTaxInvoicePrefix = '';
		}
		return this.firmTaxInvoicePrefix;
	};

	this.setFirmTaxInvoicePrefix = function (firmTaxInvoicePrefix) {
		this.firmTaxInvoicePrefix = firmTaxInvoicePrefix;
	};

	this.getFirmInvoiceNumber = function () {
		return this.firmInvoiceNumber;
	};

	this.setFirmEstimatePrefix = function (firmEstimatePrefix) {
		this.firmEstimatePrefix = firmEstimatePrefix;
	};
	this.setFirmDeliveryChallanPrefix = function (firmDeliveryChallanPrefix) {
		this.firmDeliveryChallanPrefix = firmDeliveryChallanPrefix;
	};

	this.setFirmEstimateNumber = function (firmEstimateNumber) {
		this.firmEstimateNumber = firmEstimateNumber;
	};

	this.getFirmEstimatePrefix = function () {
		return this.firmEstimatePrefix;
	};

	this.getFirmDeliveryChallanPrefix = function () {
		return this.firmDeliveryChallanPrefix;
	};

	this.getFirmEstimateNumber = function () {
		return this.firmEstimateNumber;
	};

	this.setFirmInvoiceNumber = function (firmInvoiceNumber) {
		this.firmInvoiceNumber = firmInvoiceNumber;
	};

	this.getFirmTaxInvoiceNumber = function () {
		return this.firmTaxInvoiceNumber;
	};

	this.setFirmTaxInvoiceNumber = function (firmTaxInvoiceNumber) {
		this.firmTaxInvoiceNumber = firmTaxInvoiceNumber;
	};

	this.setUdfObjectArray = function (ExtraFieldObjectArray) {
		if (!Array.isArray(extraFieldObjectArray)) {
			throw 'udf field not in array';
		}
		extraFieldObjectArray = ExtraFieldObjectArray;
	};

	this.getUdfObjectArray = function () {
		return extraFieldObjectArray;
	};

	this.SaveNewFirm = function (obj) {
		var statusCode = ErrorCode.ERROR_FIRM_SAVE_FAILED;
		var firmCache = new FirmCache();

		if (obj.firmName == null || obj.firmName == '') {
			statusCode = ErrorCode.ERROR_FIRM_NAME_EMPTY;
			return statusCode;
		}
		var isEmailProvided = true;
		var isPhoneNumProvided = true;
		if (!obj.firmEmail) {
			isEmailProvided = false;
			obj.firmEmail = '';
		}
		if (!obj.firmPhone) {
			isPhoneNumProvided = false;
			obj.firmPhone = '';
		}

		if (!isEmailProvided && !isPhoneNumProvided) {
			statusCode = ErrorCode.ERROR_FIRM_EMAIL_AND_PHONE_EMPTY;
			return statusCode;
		}

		if (firmCache.isFirmNameUnique(obj.firmName.trim()) == false) {
			statusCode = ErrorCode.ERROR_FIRM_ALREADYEXISTS;
			return statusCode;
		}

		this.firmName = obj.firmName.trim();
		var firmModel = new FirmModel();
		firmModel.setFirmName(this.firmName);
		firmModel.setFirmEmail(obj.firmEmail);
		firmModel.setFirmPhone(obj.firmPhone);
		firmModel.setFirmAddress(obj.firmAddress);
		firmModel.setFirmHSNSAC(obj.firmGstin);
		firmModel.setFirmTin(obj.firmTin);
		firmModel.setFirmState(obj.firmState);
		firmModel.setFirmImagePath(obj.firmLogoPath);
		firmModel.setFirmSignImagePath(obj.firmSignPath);
		firmModel.setBankName(obj.bankName);
		firmModel.setBankIFSC(obj.bankIFSC);
		firmModel.setAccountNumber(obj.accountNumber);
		firmModel.setBusinessType(obj.businessType);
		firmModel.setBusinessCategory(obj.businessCategory);
		var transactionManager = new TransactionManager();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_FIRM_SAVE_FAILED;
			return statusCode;
		}

		statusCode = firmModel.addFirm();
		if (statusCode == ErrorCode.ERROR_FIRM_SAVE_SUCCESS) {
			this.firmId = firmModel.getFirmId();
			var firmCache = new FirmCache();

			if (!transactionManager.CommitTransaction()) {
				statusCode = ErrorCode.ERROR_FIRM_SAVE_FAILED;
			} else {
				firmCache.refreshFirmCache();
			}
		}
		transactionManager.EndTransaction();

		return statusCode;
	};

	this.updateFirm = function (obj) {
		var statusCode = ErrorCode.ERROR_FIRM_UPDATE_FAILED;
		var firmCache = new FirmCache();
		var firmModel = new FirmModel();
		if (obj.firmName == null || !obj.firmName) {
			statusCode = ErrorCode.ERROR_FIRM_NAME_EMPTY;
			return statusCode;
		}

		var isEmailProvided = true;
		var isPhoneNumProvided = true;

		if (!obj.firmEmail) {
			isEmailProvided = false;
			obj.firmEmail = '';
		}
		if (!obj.firmPhone) {
			isPhoneNumProvided = false;
			obj.firmPhone = '';
		}

		if (!isEmailProvided && !isPhoneNumProvided) {
			statusCode = ErrorCode.ERROR_FIRM_EMAIL_AND_PHONE_EMPTY;
			return statusCode;
		}

		if (firmCache.isFirmNameUnique(obj.firmName.trim()) == false && obj.firmName.toLowerCase() != this.firmName.toLowerCase()) {
			statusCode = ErrorCode.ERROR_FIRM_ALREADYEXISTS;
			return statusCode;
		}

		this.firmName = obj.firmName.trim();
		var firmModel = new FirmModel();
		firmModel.setFirmId(this.firmId);
		firmModel.setFirmName(obj.firmName);
		firmModel.setFirmEmail(obj.firmEmail);
		firmModel.setFirmPhone(obj.firmPhone);
		firmModel.setFirmAddress(obj.firmAddress);
		firmModel.setFirmHSNSAC(obj.firmGstin);
		firmModel.setFirmTin(obj.firmTin);
		firmModel.setFirmState(obj.firmState);
		firmModel.setFirmLogoId(this.firmLogoId);
		firmModel.setFirmSignId(this.firmSignId);
		firmModel.setFirmImagePath(obj.firmLogoPath);
		firmModel.setFirmSignImagePath(obj.firmSignPath);
		firmModel.setBankName(obj.bankName);
		firmModel.setBankIFSC(obj.bankIFSC);
		firmModel.setAccountNumber(obj.accountNumber);
		firmModel.setUpiAccountNumber(obj.upiAccountNumber);
		firmModel.setUpiIFSC(obj.upiIFSC);
		firmModel.setBusinessType(obj.businessType);
		firmModel.setBusinessCategory(obj.businessCategory);
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var transactionManager = new TransactionManager();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_FIRM_UPDATE_FAILED;
			return statusCode;
		}

		statusCode = firmModel.updateFirm();

		if (statusCode == ErrorCode.ERROR_FIRM_UPDATE_SUCCESS) {
			var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
			var sqlitedbhelper = new SqliteDBHelper();

			statusCode = sqlitedbhelper.updatePrefixDefaultValues(this.firmId, {
				'invoicePrefix': obj.firmInvoicePrefix,
				'cashInPrefix': obj.firmCashInPrefix,
				'estimatePrefix': obj.firmEstimatePrefix,
				'deliveryChallanPrefix': obj.firmDeliveryChallanPrefix,
				'firmSaleOrderPrefix': obj.firmSaleOrderPrefix,
				'firmPurchaseOrderPrefix': obj.firmPurchaseOrderPrefix,
				'firmSaleReturnPrefix': obj.firmSaleReturnPrefix
			});

			if (statusCode == ErrorCode.ERROR_PREFIX_UPDATE_SUCCESS) {
				statusCode = ErrorCode.ERROR_FIRM_UPDATE_SUCCESS;
			}

			if (this.getUdfObjectArray() && this.getUdfObjectArray().length) {
				statusCode = sqlitedbhelper.deleteExtraFieldsValue(this.firmId, 1);
				if (statusCode == ErrorCode.ERROR_UDF_VALUE_DELETE_SUCCESSFUL) {
					statusCode = ErrorCode.ERROR_FIRM_UPDATE_SUCCESS;
				}
			}
			if (statusCode == ErrorCode.ERROR_FIRM_UPDATE_SUCCESS) {
				if (obj.extraFieldValueArray && obj.extraFieldValueArray.length) {
					for (var i = 0; i < obj.extraFieldValueArray.length; i++) {
						var extraFieldObject = obj.extraFieldValueArray[i];
						extraFieldObject.setUdfRefId(this.getFirmId());
						statusCode = extraFieldObject.updateUdfValues();
						if (statusCode == ErrorCode.ERROR_UDF_SAVE_SUCCESS) {
							statusCode = ErrorCode.ERROR_FIRM_UPDATE_SUCCESS;
						} else {
							break;
						}
					}
				}
			}
		}

		if (statusCode == ErrorCode.ERROR_FIRM_UPDATE_SUCCESS) {
			if (!transactionManager.CommitTransaction()) {
				statusCode = ErrorCode.ERROR_FIRM_UPDATE_FAILED;
			} else {
				firmCache.refreshFirmCache();
			}
		}
		transactionManager.EndTransaction();

		return statusCode;
	};

	this.isExistPrefix = function (prefixVal, firmId, txnType) {};

	this.updateFirmWithoutObject = function () {
		var withoutImageChanges = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

		var statusCode = ErrorCode.ERROR_FIRM_UPDATE_FAILED;
		var firmModel = new FirmModel();
		firmModel.setFirmId(this.getFirmId());
		firmModel.setFirmName(this.getFirmName());
		firmModel.setFirmEmail(this.getFirmEmail());
		firmModel.setFirmPhone(this.getFirmPhone());
		firmModel.setFirmAddress(this.getFirmAddress());
		firmModel.setFirmHSNSAC(this.getFirmHSNSAC());
		firmModel.setFirmTin(this.getFirmTin());
		firmModel.setFirmState(this.getFirmState());
		firmModel.setFirmLogoId(this.getFirmLogoId());
		firmModel.setFirmImagePath(this.getFirmImagePath() || '');
		firmModel.setFirmSignId(this.getFirmSignId());
		firmModel.setFirmSignImagePath(this.getFirmSignImagePath() || '');
		firmModel.setBankName(this.getBankName());
		firmModel.setBankIFSC(this.getBankIFSC());
		firmModel.setAccountNumber(this.getAccountNumber());
		firmModel.setUpiAccountNumber(this.getUpiAccountNumber());
		firmModel.setUpiIFSC(this.getUpiIFSC());
		firmModel.setBusinessType(this.getBusinessType());
		firmModel.setBusinessCategory(this.getBusinessCategory());
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var transactionManager = new TransactionManager();
		var isTransactionBeginSuccess = transactionManager.BeginTransaction();
		var firmCache = new FirmCache();
		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_FIRM_UPDATE_FAILED;
			firmCache.refreshFirmCache();
			return statusCode;
		}

		statusCode = firmModel.updateFirm(withoutImageChanges);
		if (statusCode == ErrorCode.ERROR_FIRM_UPDATE_SUCCESS) {
			if (!transactionManager.CommitTransaction()) {
				statusCode = ErrorCode.ERROR_FIRM_UPDATE_FAILED;
			}
		}
		firmCache.refreshFirmCache();
		transactionManager.EndTransaction();

		return statusCode;
	};
};
module.exports = FirmObject;