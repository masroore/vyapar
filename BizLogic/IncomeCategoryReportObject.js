var IncomeCategoryReportObject = function IncomeCategoryReportObject() {
	this.categoryId;
	this.categoryName = '';
	this.amount = 0;

	this.getCategoryId = function () {
		return this.categoryId;
	};

	this.setCategoryId = function (categoryId) {
		this.categoryId = categoryId;
	};

	this.getCategoryName = function () {
		return this.categoryName;
	};

	this.setCategoryName = function (categoryName) {
		this.categoryName = categoryName;
	};

	this.getAmount = function () {
		return this.amount;
	};

	this.setAmount = function (amount) {
		this.amount = amount;
	};
};

module.exports = IncomeCategoryReportObject;