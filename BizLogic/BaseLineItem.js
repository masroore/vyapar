var itemCache;
var ErrorCode = require('./../Constants/ErrorCode.js');
var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var StringConstants = require('./../Constants/StringConstants.js');
var SettingCache = require('./../Cache/SettingCache.js');
var IstType = require('./../Constants/IstType.js');
var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
var TaxCodeCache = require('./../Cache/TaxCodeCache.js');
var ItemLogic = require('./ItemLogic.js');
var ItemType = require('./../Constants/ItemType.js');
var ItemUnitMappingCache = require('./../Cache/ItemUnitMappingCache.js');
var PartyWiseItemRateModel = require('./../Models/PartyWiseModel.js');

var BaseLineItem = function BaseLineItem() {
	if (!itemCache) {
		var ItemCache = require('./../Cache/ItemCache.js');
		itemCache = new ItemCache();
	}

	this.itemId = 0;
	this.transactionId = 0;
	this.itemName = '';
	this.itemUnitPrice = 0;
	this.lineItemTotal = 0;
	this.lineItemTax = 0.0;
	this.lineItemDiscount = 0.0;
	this.lineItemId = 0;
	this.itemUnitId = null;
	this.lineItemUnitMappingId = null;
	this.lineItemTaxId = null;
	this.lineItemITCValue = 0;
	this.lineItemIstId = null;
	this.itemSerialNumber = '';
	this.itemMRP = 0;
	this.itemSize = '';
	this.itemBatchNumber = '';
	this.itemExpiryDate = null;
	this.itemManufacturingDate = null;
	this.itemFreeQuantity = 0;
	this.itemQuantity = 0;
	this.lineItemDiscountPercent = 0;
	this.txnTaxTypeInclusive = ItemType.ITEM_TXN_TAX_EXCLUSIVE;
	this.unitPriceFromUI = 0;

	this.getUnitPriceFromUI = function () {
		return this.unitPriceFromUI;
	};

	this.setUnitPriceFromUI = function (val) {
		this.unitPriceFromUI = val;
	};

	this.setTxnTaxTypeInclusive = function (value) {
		this.txnTaxTypeInclusive = value;
	};

	this.getTxnTaxTypeInclusive = function () {
		return this.txnTaxTypeInclusive;
	};

	this.getLineItemDiscountPercent = function () {
		return this.lineItemDiscountPercent;
	};

	this.setLineItemDiscountPercent = function (lineItemDiscountPercent) {
		this.lineItemDiscountPercent = lineItemDiscountPercent;
	};

	this.getTotalQuantityIncludingFreeQuantity = function () {
		return Number(this.itemFreeQuantity) + Number(this.itemQuantity);
	};

	this.getLineItemIstId = function () {
		return this.istId;
	};
	this.setLineItemIstId = function (istId) {
		this.istId = istId;
	};

	this.getLineItemITCValue = function () {
		return this.lineItemITCValue;
	};
	this.setLineItemITCValue = function (lineItemITCValue) {
		this.lineItemITCValue = lineItemITCValue;
	};

	this.getLineItemMRP = function () {
		return this.itemMRP;
	};
	this.setLineItemMRP = function (itemMRP) {
		this.itemMRP = itemMRP;
	};

	this.getLineItemSize = function () {
		return this.itemSize;
	};
	this.setLineItemSize = function (itemSize) {
		this.itemSize = itemSize;
	};

	this.getLineItemAdditionalCESS = function () {
		return this.itemAdditionalCess;
	};
	this.setLineItemAdditionalCESS = function (cess) {
		this.itemAdditionalCess = cess;
	};

	this.getLineItemBatchNumber = function () {
		return this.itemBatchNumber;
	};
	this.setLineItemBatchNumber = function (itemBatchNumber) {
		this.itemBatchNumber = itemBatchNumber;
	};

	this.getLineItemExpiryDate = function () {
		return this.itemExpiryDate;
	};
	this.setLineItemExpiryDate = function (itemExpiryDate) {
		this.itemExpiryDate = itemExpiryDate;
	};

	this.getLineItemManufacturingDate = function () {
		return this.itemManufacturingDate;
	};
	this.setLineItemManufacturingDate = function (itemMfDate) {
		this.itemManufacturingDate = itemMfDate;
	};

	this.getLineItemSerialNumber = function () {
		return this.itemSerialNumber;
	};
	this.setLineItemSerialNumber = function (itemSerialNumber) {
		this.itemSerialNumber = itemSerialNumber;
	};

	this.getLineItemCount = function () {
		return this.itemCount;
	};
	this.setLineItemCount = function (itemCount) {
		this.itemCount = itemCount;
	};

	this.getLineItemDescription = function () {
		return this.itemDescription;
	};
	this.setLineItemDescription = function (itemDescription) {
		this.itemDescription = itemDescription;
	};

	this.getLineItemTaxId = function () {
		return this.lineItemTaxId;
	};
	this.setLineItemTaxId = function (lineItemTaxId) {
		this.lineItemTaxId = lineItemTaxId;
	};

	this.getLineItemUnitId = function () {
		return this.itemUnitId;
	};
	this.setLineItemUnitId = function (itemUnitId) {
		this.itemUnitId = itemUnitId;
	};

	this.getLineItemUnitMappingId = function () {
		return this.lineItemUnitMappingId;
	};
	this.setLineItemUnitMappingId = function (lineItemUnitMappingId) {
		this.lineItemUnitMappingId = lineItemUnitMappingId;
	};

	this.setTransactionId = function (transactionId) {
		this.transactionId = transactionId;
	};
	this.getTransactionId = function () {
		return this.transactionId;
	};

	this.setLineItemId = function (lineItemId) {
		this.lineItemId = lineItemId;
	};
	this.getLineItemId = function () {
		return this.lineItemId;
	};

	this.setLineItemTaxAmount = function (lineItemTax) {
		this.lineItemTax = lineItemTax;
	};
	this.getLineItemTaxAmount = function () {
		return this.lineItemTax;
	};

	this.setLineItemDiscountAmount = function (lineItemDiscount) {
		this.lineItemDiscount = lineItemDiscount;
	};
	this.getLineItemDiscountAmount = function () {
		return this.lineItemDiscount;
	};

	this.setItemName = function (itemName) {
		this.itemName = itemName;
	};
	this.getItemName = function () {
		return this.itemName;
	};

	this.setItemId = function (itemId) {
		this.itemId = itemId;
	};
	this.getItemId = function () {
		return this.itemId;
	};

	this.setItemQuantity = function (itemQuantity) {
		this.itemQuantity = itemQuantity;
	};
	this.getItemQuantity = function () {
		return this.itemQuantity;
	};

	this.setItemFreeQuantity = function (itemFreeQuantity) {
		this.itemFreeQuantity = itemFreeQuantity;
	};
	this.getItemFreeQuantity = function () {
		return this.itemFreeQuantity;
	};

	this.setItemUnitPrice = function (itemUnitPrice) {
		this.itemUnitPrice = itemUnitPrice;
	};
	this.getItemUnitPrice = function () {
		return this.itemUnitPrice;
	};

	this.setLineItemTotal = function (lineItemTotal) {
		this.lineItemTotal = lineItemTotal;
	};
	this.getLineItemTotal = function () {
		return this.lineItemTotal;
	};

	this.getLineItemTaxPercent = function () {
		var taxPercent = 0;
		if (this.getLineItemTaxId() && this.getLineItemTaxId() > 0) {
			var taxCodeCache = new TaxCodeCache();
			taxPercent = taxCodeCache.getRateForTaxId(this.getLineItemTaxId());
		}
		return taxPercent;
	};

	this.addLineItems = function (txnType, nameId) {
		var isFromEditMode = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

		var itemObj = void 0;
		var statusCode = ErrorCode.SUCCESS;
		var settingCache = new SettingCache();
		// if new item
		if (this.itemId == 0 && !itemCache.getItemIdByNameAndTxn(txnType, this.itemName.trim())) {
			itemObj = new ItemLogic();
			itemObj.setItemName(this.itemName.trim());
			this.updateItemPriceAndTaxType(txnType, itemObj);
			if (txnType == TxnTypeConstant.TXN_TYPE_EXPENSE) {
				itemObj.setItemType(ItemType.ITEM_TYPE_EXPENSE);
			} else if (txnType == TxnTypeConstant.TXN_TYPE_OTHER_INCOME) {
				itemObj.setItemType(ItemType.ITEM_TYPE_INCOME);
			} else if (settingCache.getItemType() === StringConstants.settingServiceType) {
				itemObj.setItemType(ItemType.ITEM_TYPE_SERVICE);
			} else {
				itemObj.setItemType(ItemType.ITEM_TYPE_INVENTORY);
			}

			itemObj.setItemDescription(this.getLineItemDescription());

			if (settingCache.getAdditionalCessEnabled()) {
				var CESS = Number(this.getLineItemAdditionalCESS());
				if (CESS && !isNaN(CESS)) {
					itemObj.setItemAdditionalCESS(MyDouble.getAmountWithDecimal(CESS / Number(this.getItemQuantity())));
				} else {
					itemObj.setItemAdditionalCESS(null);
				}
			} else {
				itemObj.setItemAdditionalCESS(null);
			}

			itemObj.setItemCode(null);
			itemObj.setItemCategoryId('1');
			statusCode = itemObj.addItem();

			if (statusCode == ErrorCode.ERROR_ITEM_SAVE_SUCCESS) {
				MyAnalytics.pushEvent('Item save direct from sale - ' + txnType);
				this.itemId = itemObj.getItemId();
				itemCache.refreshItemCache(itemObj);
				if (settingCache.getPartyWiseItemRateEnabled()) {
					if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE) {
						var partyWiseItemRateModel = new PartyWiseItemRateModel();
						partyWiseItemRateModel.setItemId(this.itemId);
						partyWiseItemRateModel.setNameId(nameId);
						if (txnType == TxnTypeConstant.TXN_TYPE_SALE) {
							partyWiseItemRateModel.setSalePrice(this.itemUnitPrice);
						} else if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE) {
							partyWiseItemRateModel.setPurchasePrice(this.itemUnitPrice);
						}
						statusCode = partyWiseItemRateModel.addPartyWiseRate();
						if (statusCode == ErrorCode.ERROR_PARTYWISE_RATE_SAVE_SUCCESS) {
							statusCode = ErrorCode.SUCCESS;
						}
					} else {
						statusCode = ErrorCode.SUCCESS;
					}
				} else {
					statusCode = ErrorCode.SUCCESS;
				}
			}
		} else {
			// old item
			if (this.itemId == 0) {
				this.itemId = itemCache.getItemIdByNameAndTxn(txnType, this.itemName);
			}

			itemObj = itemCache.getItemById(this.itemId);
			if ((txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE) && !isFromEditMode) {
				if (settingCache.getPartyWiseItemRateEnabled()) {
					var _partyWiseItemRateModel = new PartyWiseItemRateModel();
					_partyWiseItemRateModel.setItemId(this.itemId);
					_partyWiseItemRateModel.setNameId(nameId);
					if (txnType == TxnTypeConstant.TXN_TYPE_SALE) {
						_partyWiseItemRateModel.setSalePrice(this.itemUnitPrice);
					} else if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE) {
						_partyWiseItemRateModel.setPurchasePrice(this.itemUnitPrice);
					}
					statusCode = _partyWiseItemRateModel.updatePartyWiseRate(txnType);
					if (statusCode == ErrorCode.ERROR_PARTYWISE_RATE_SAVE_SUCCESS) {
						statusCode = ErrorCode.SUCCESS;
					}
				} else {
					this.updateItemPriceAndTaxType(txnType, itemObj);
					statusCode = itemObj.updateItem();
					if (statusCode == ErrorCode.ERROR_ITEM_SAVE_SUCCESS) {
						statusCode = ErrorCode.SUCCESS;
					}
				}
			}
		}

		/// Item stock tracking logic
		var istId = null;
		if ((txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) && statusCode == ErrorCode.SUCCESS && settingCache.isAnyAdditionalItemDetailsEnabled()) {
			var ItemStockTrackingModel = require('./../Models/ItemStockTrackingModel.js');
			var itemStockTracking = new ItemStockTrackingModel();
			var itemStockTrackingData = itemStockTracking.getISTWithParameters(this.getLineItemBatchNumber(), this.getLineItemSerialNumber(), this.getLineItemMRP(), this.getLineItemExpiryDate(), this.getLineItemManufacturingDate(), this.getLineItemSize(), this.getItemId());

			if (itemStockTrackingData.getIstId()) {
				istId = itemStockTrackingData.getIstId();
				var currentQuantity = itemStockTrackingData.getIstCurrentQuantity();

				if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
					statusCode = itemStockTracking.changeISTQuantity(istId, Number(currentQuantity) - Number(this.getTotalQuantityIncludingFreeQuantity()), false, false);
				} else {
					statusCode = itemStockTracking.changeISTQuantity(istId, Number(currentQuantity) + Number(this.getTotalQuantityIncludingFreeQuantity()), false, false);
				}
			} else {
				if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
					istId = itemStockTracking.addItemStockTracking(this.getLineItemBatchNumber(), this.getLineItemSerialNumber(), this.getLineItemMRP(), this.getLineItemExpiryDate(), this.getLineItemManufacturingDate(), this.getLineItemSize(), this.getItemId(), Number(-this.getTotalQuantityIncludingFreeQuantity()), IstType.NORMAL);
				} else {
					istId = itemStockTracking.addItemStockTracking(this.getLineItemBatchNumber(), this.getLineItemSerialNumber(), this.getLineItemMRP(), this.getLineItemExpiryDate(), this.getLineItemManufacturingDate(), this.getLineItemSize(), this.getItemId(), Number(this.getTotalQuantityIncludingFreeQuantity()), IstType.NORMAL);
				}

				if (istId) {
					statusCode = ErrorCode.ERROR_ITEM_STOCK_TRACKING_INSERT_SUCCESS;
				} else {
					statusCode = ErrorCode.ERROR_ITEM_STOCK_TRACKING_INSERT_FAILED;
				}
			}
			if (statusCode == ErrorCode.ERROR_ITEM_STOCK_TRACKING_INSERT_SUCCESS || statusCode == ErrorCode.ERROR_ITEM_STOCK_QUANITY_UPDATE_SUCCESS) {
				statusCode = ErrorCode.SUCCESS;
			}
		}
		if (statusCode == ErrorCode.SUCCESS) {
			var LineItemModel = require('./../Models/LineItemModel.js');
			var lineitemmodel = new LineItemModel();
			lineitemmodel.setLineItemMRP(this.getLineItemMRP());
			lineitemmodel.setLineItemSize(this.getLineItemSize());
			lineitemmodel.setLineItemBatchNumber(this.getLineItemBatchNumber());
			lineitemmodel.setLineItemExpiryDate(this.getLineItemExpiryDate());
			lineitemmodel.setLineItemManufacturingDate(this.getLineItemManufacturingDate());
			lineitemmodel.setLineItemSerialNumber(this.getLineItemSerialNumber());
			lineitemmodel.setLineItemCount(this.getLineItemCount());
			lineitemmodel.setLineItemDescription(this.getLineItemDescription());
			lineitemmodel.setTransactionId(this.transactionId);
			lineitemmodel.setItemId(this.itemId);
			lineitemmodel.setItemUnitPrice(this.itemUnitPrice);
			lineitemmodel.setItemQuantity(this.itemQuantity);
			lineitemmodel.setItemFreeQuantity(this.itemFreeQuantity);
			lineitemmodel.setLineItemTaxAmount(this.getLineItemTaxAmount());
			lineitemmodel.setLineItemDiscountAmount(this.getLineItemDiscountAmount());
			lineitemmodel.setLineItemTotal(this.lineItemTotal);
			lineitemmodel.setLineItemUnitId(this.itemUnitId);
			lineitemmodel.setLineItemTaxId(this.getLineItemTaxId());
			lineitemmodel.setLineItemUnitMappingId(this.lineItemUnitMappingId);
			lineitemmodel.setLineItemAdditionalCESS(this.getLineItemAdditionalCESS());
			lineitemmodel.setLineItemITCValue(this.getLineItemITCValue());
			lineitemmodel.setLineItemIstId(istId);
			lineitemmodel.setLineItemDiscountPercent(this.getLineItemDiscountPercent());
			statusCode = lineitemmodel.addLineItem();
			if (statusCode == ErrorCode.ERROR_TXN_SAVE_SUCCESS) {
				statusCode = itemObj.updateItemStockQuantity(txnType, this.getTotalQuantityIncludingFreeQuantity());
				if (statusCode != ErrorCode.ERROR_ITEM_SAVE_SUCCESS) {
					statusCode = ErrorCode.ERROR_TXN_SAVE_FAILED;
				} else {
					statusCode = ErrorCode.ERROR_TXN_SAVE_SUCCESS;
				}
			}
		} else {
			statusCode = ErrorCode.ERROR_TXN_SAVE_FAILED;
		}
		return statusCode;
	};

	this.updateItemPriceAndTaxType = function (txnType, itemObj) {
		if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE) {
			var unitPrice = Number(this.getUnitPriceFromUI());

			if (this.lineItemUnitMappingId > 0) {
				var itemUnitMappingCache = new ItemUnitMappingCache();
				var mappingObj = itemUnitMappingCache.getItemUnitMapping(this.lineItemUnitMappingId);
				unitPrice = unitPrice * Number(mappingObj.getConversionRate());
			}

			if (!Number(itemObj.getItemTaxId()) && Number(this.lineItemTaxId)) {
				itemObj.setItemTaxId(this.lineItemTaxId);
			}

			if (this.getTxnTaxTypeInclusive() == ItemType.ITEM_TXN_TAX_EXCLUSIVE || this.getTxnTaxTypeInclusive() == ItemType.ITEM_TXN_TAX_INCLUSIVE && Number(itemObj.getItemTaxId()) == Number(this.lineItemTaxId)) {
				if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE) {
					itemObj.setItemTaxTypePurchase(this.getTxnTaxTypeInclusive());
					itemObj.setItemPurchaseUnitPrice(unitPrice);
				} else if (txnType == TxnTypeConstant.TXN_TYPE_SALE) {
					itemObj.setItemTaxTypeSale(this.getTxnTaxTypeInclusive());
					itemObj.setItemSaleUnitPrice(unitPrice);
				}
			}
		}
	};

	this.deleteLineItem = function () {
		var sqlitedbhelper = new SqliteDBHelper();
		var retVal = sqlitedbhelper.deleteLineItem(this.lineItemId);
		if (retVal == true) {
			return ErrorCode.SUCCESS;
		} else {
			return ErrorCode.FAILED;
		}
	};
};

module.exports = BaseLineItem;