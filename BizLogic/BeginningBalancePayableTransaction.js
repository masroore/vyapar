var BeginningBalancePayableTransaction = function BeginningBalancePayableTransaction() {
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var balanceAmount;

	this.getBalanceAmount = function () {
		return this.balanceAmount;
	};
	this.getTxnType = function () {
		this.txnType = TxnTypeConstant.TXN_TYPE_BEGINNING_BALANCE_PAYABLE;
		return this.txnType;
	};

	this.getTaxAmount = function () {
		return 0.0;
	};

	this.getDiscountAmount = function () {
		return 0.0;
	};

	this.getTaxPercent = function () {
		return 0.0;
	};

	this.getDiscountPercent = function () {
		return 0.0;
	};

	this.getTxnTypeString = function () {
		this.txnTypeString = 'TXN_TYPE_BEGINNING_BALANCE_PAYABLE';
		return this.txnTypeString;
	};
	this.setBalanceAmount = function (balanceAmount) {
		if (!balanceAmount) {
			this.balanceAmount = '0.0';
		} else if (!isNaN(Number(balanceAmount))) {
			this.balanceAmount = Number(balanceAmount);
		} else {
			return 'ERROR_TXN_INVALID_AMOUNT';
		}
		return 'SUCCESS';
	};

	this.setCashAmount = function () {};

	this.getCashAmount = function () {
		this.cashAmount = 0.0;
		return this.cashAmount;
	};

	this.setDescription = function (description) {
		this.description = '';
	};
	this.getDescription = function () {
		this.description = '';
		return this.description;
	};
	this.setAmounts = function (totalAmount, cashAmount) {
		return this.setBalanceAmount(totalAmount);
	};
	this.getTxnRefNumber = function () {
		this.txnRefNumber = '';
		return this.txnRefNumber;
	};
	this.setTxnRefNumber = function (txnRefNumber) {
		this.txnRefNumber = '';
	};

	this.getTransactionMessage = function () {
		this.txnMessage = '';
		return this.txnMessage;
	};

	this.getInvoicePrefix = function () {
		return '';
	};

	this.setInvoicePrefix = function () {};

	this.loadAllLineItems = function () {
		this.lineItems = [];
	};
};

module.exports = BeginningBalancePayableTransaction;