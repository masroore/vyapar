var BankAdjustmentForReport = function BankAdjustmentForReport(txnType) {
	var cashAmount;
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	this.txnType = txnType;
	var toBankId;
	this.getBalanceAmount = function () {
		return 0;
	};
	this.getTxnType = function () {
		return this.txnType;
	};

	this.getTaxAmount = function () {
		return 0.0;
	};

	this.getDiscountAmount = function () {
		return 0.0;
	};

	this.getBankId = function () {
		return this.bankId;
	};

	this.setToBankId = function (toBankId) {
		this.toBankId = toBankId;
	};

	this.getToBankId = function () {
		return this.toBankId;
	};

	this.getTxnTypeString = function () {
		if (this.txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE) {
			return 'TXN_TYPE_BANK_REDUCE_ADJUSTMENT';
		} else if (this.txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE_WITHOUT_CASH) {
			return 'TXN_TYPE_BANK_ADJ_REDUCE_WITHOUT_CASH';
		} else if (this.txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD) {
			return 'TXN_TYPE_BANK_ADD_ADJUSTMENT';
		} else if (this.txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE_WITHOUT_CASH) {
			return 'TXN_TYPE_BANK_ADJ_ADD_WITHOUT_CASH';
		} else if (this.txnType == TxnTypeConstant.TXN_TYPE_BANK_TO_BANK) {
			return 'TXN_TYPE_BANK_TO_BANK';
		}
	};
	this.setBalanceAmount = function (balanceAmount) {
		return 'SUCCESS';
	};

	this.setCashAmount = function (cashAmount) {
		if (!cashAmount) {
			this.cashAmount = 0.0;
		} else if (!isNaN(Number(cashAmount))) {
			this.cashAmount = Number(cashAmount);
		} else {
			return 'ERROR_TXN_INVALID_AMOUNT';
		}
		return 'SUCCESS';
	};

	this.getCashAmount = function () {
		return this.cashAmount;
	};

	this.setAmounts = function (totalAmount, cashAmount) {
		return this.setCashAmount(cashAmount);
	};

	this.getTxnRefNumber = function () {
		this.txnRefNumber = '';
		return this.txnRefNumber;
	};
	this.setTxnRefNumber = function (txnRefNumber) {};

	this.getTransactionMessage = function () {
		return '';
	};

	this.loadAllLineItems = function () {
		this.lineItems = [];
	};
};

module.exports = BankAdjustmentForReport;