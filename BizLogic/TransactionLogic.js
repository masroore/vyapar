var _typeof2 = require('babel-runtime/helpers/typeof');

var _typeof3 = _interopRequireDefault(_typeof2);

var _MyString = require('../Utilities/MyString');

var _MyString2 = _interopRequireDefault(_MyString);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var itemcache;
var TransactionLogic = function TransactionLogic() {
	this.isUpdateTransaction = 0;
	this.oldTxnObject;
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var ErrorCode = require('./../Constants/ErrorCode.js');
	var TransactionStatus = require('./../Constants/TransactionStatus.js');
	var StringConstant = require('./../Constants/StringConstants');
	var MyDouble = require('./../Utilities/MyDouble.js');
	var MyDate = require('./../Utilities/MyDate');
	var ItemType = require('./../Constants/ItemType.js');
	if (!itemcache) {
		var ItemCache = require('./../Cache/ItemCache.js');
		itemcache = new ItemCache();
	}

	this.addItems = function (transactionObj) {
		var lineItems = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

		var SettingCache = require('./../Cache/SettingCache.js');
		var settingCache = new SettingCache();
		var unitSwitch = settingCache.getItemUnitEnabled();
		var statusCode = ErrorCode.SUCCESS;
		transactionObj.setLineItems([]);

		var _loop = function _loop(i) {
			var lineItem = lineItems[i];

			var itemName = lineItem && lineItem.item && lineItem.item.label ? lineItem.item.label : '';
			var itemMRP = lineItem.mrp;
			var itemBatchNo = lineItem.batchNo;
			var itemExpiryDate = null;
			if (lineItem.expDate) {
				var expDate = MyDate.getDateObj(lineItem.expDate, settingCache.getExpiryDateFormat() == StringConstant.monthYear ? 'mm/yyyy' : 'dd/mm/yyyy', '/');
				if (expDate && expDate instanceof Date && expDate != 'Invalid Date') {
					itemExpiryDate = expDate;
				}
			}
			var itemManufacturingDate = null;
			if (lineItem.mfgDate) {
				var mfgDate = MyDate.getDateObj(lineItem.mfgDate, settingCache.getManufacturingDateFormat() == StringConstant.monthYear ? 'mm/yyyy' : 'dd/mm/yyyy', '/');
				if (mfgDate && mfgDate instanceof Date && mfgDate != 'Invalid Date') {
					itemManufacturingDate = mfgDate;
				}
			}
			var itemSerialNumber = lineItem.slNo;
			var itemCount = MyDouble.convertStringToDouble(lineItem.count) || 0;
			var itemDescription = lineItem.description;
			var itemSize = lineItem.size;
			var itemQuantity = MyDouble.convertStringToDouble(lineItem.qty) || 0;
			var itemFreeQuantity = MyDouble.convertStringToDouble(lineItem.freeQty) || 0;
			var itemUnitPrice = MyDouble.convertStringToDouble(lineItem.priceUnit) || 0;
			var itemUnitId = lineItem.unit;
			var itemMappingId = null;
			var itemDiscountAmount = MyDouble.convertStringToDouble(lineItem.discountAmount) || 0;
			var itemDiscountPercent = MyDouble.convertStringToDouble(lineItem.discountPercent) || 0;
			var lineItemTaxId = lineItem.taxPercent;
			lineItemTaxId = lineItemTaxId != '0' ? lineItemTaxId : null;

			var itemITCValue = lineItem.ITCValue;
			itemITCValue = MyDouble.convertStringToDouble(itemITCValue) || 0;

			var itemTaxAmount = MyDouble.convertStringToDouble(lineItem.taxAmount) || 0;
			var itemCessAmount = MyDouble.convertStringToDouble(lineItem.cess) || 0;
			var itemTotalPrice = MyDouble.convertStringToDouble(lineItem.amount) || 0;
			var itemQuantityStr = '';
			var itemFreeQuantityStr = '';
			var itemUnitPriceStr = '';
			if (itemName) {
				ItemUnitMappingCache = require('./../Cache/ItemUnitMappingCache.js');
				itemUnitMappingCache = new ItemUnitMappingCache();

				if (itemUnitPrice == undefined || itemUnitPrice == '') {
					itemUnitPriceStr = '0.0';
				} else {
					itemUnitPriceStr = itemUnitPrice;
				}

				itemQuantityStr = itemQuantity;
				if (!itemQuantity) {
					itemQuantityStr = '0';
				} else {
					itemQuantityStr = itemQuantity;
				}

				if (!itemFreeQuantity) {
					itemFreeQuantityStr = '0.0';
				} else {
					itemFreeQuantityStr = itemFreeQuantity;
				}

				var unitPriceFromUI = Number(itemUnitPriceStr);
				var inclusiveVal = transactionObj.getTaxTypeInclusive();
				if (inclusiveVal == ItemType.ITEM_TXN_TAX_INCLUSIVE && lineItemTaxId) {
					var TaxCodeCache = require('./../Cache/TaxCodeCache.js');
					var taxCodeCache = new TaxCodeCache();
					itemUnitPriceStr = Number(itemUnitPrice) * 100 / (100 + Number(taxCodeCache.getRateForTaxId(lineItemTaxId)));
				}

				if (itemUnitId && itemUnitId != 0) {
					var found = lineItem.unitObj.find(function (unit) {
						return unit.value == itemUnitId;
					});
					if (found) {
						if (found.type === 'base') {
							itemMappingId = null;
						} else {
							itemMappingId = found.mappingId;
							mappingObj = itemUnitMappingCache.getItemUnitMapping(itemMappingId);
							rate = mappingObj['conversionRate'];

							itemUnitPriceStr = Number(itemUnitPriceStr) * Number(rate);
							itemQuantityStr = Number(itemQuantityStr) / Number(rate);
							itemFreeQuantityStr = Number(itemFreeQuantityStr) / Number(rate);
						}
					}
				}

				if (statusCode == ErrorCode.SUCCESS) {
					var lineItemUIData = {
						name: itemName,
						quantity: itemQuantityStr,
						freequantity: itemFreeQuantityStr,
						unitprice: itemUnitPriceStr,
						discountamount: itemDiscountAmount,
						discountpercent: itemDiscountPercent,
						taxamount: itemTaxAmount,
						totalamount: itemTotalPrice,
						mappingid: Number(itemMappingId) || null,
						unitid: Number(itemUnitId) || null,
						taxid: Number(lineItemTaxId) || null,
						mrp: itemMRP || null,
						size: itemSize || null,
						batchno: itemBatchNo || null,
						expirydate: itemExpiryDate || null,
						manufacturingdate: itemManufacturingDate || null,
						serialnumber: itemSerialNumber || null,
						count: itemCount || null,
						description: itemDescription || '',
						cessamount: itemCessAmount || null,
						itcvalue: itemITCValue,
						unitPriceFromUI: unitPriceFromUI
					};
					statusCode = transactionObj.addLineItem(lineItemUIData);
					if (statusCode != ErrorCode.SUCCESS) {
						return 'break';
					}
				} else {
					return 'break';
				}
			} else {
				if (Number(itemTotalPrice) > 0) {
					statusCode = ErrorCode.ERROR_ITEM_NOT_EXIST;
					return {
						v: statusCode
					};
				}
			}
		};

		_loop2: for (var i = 0; i < lineItems.length; i++) {
			var ItemUnitMappingCache;
			var itemUnitMappingCache;
			var mappingObj;
			var rate;

			var _ret = _loop(i);

			switch (_ret) {
				case 'break':
					break _loop2;

				default:
					if ((typeof _ret === 'undefined' ? 'undefined' : (0, _typeof3.default)(_ret)) === "object") return _ret.v;
			}
		}

		return statusCode;
	};

	this.SaveTransaction = function (txnObject) {
		if (!this.isUpdateTransaction) {
			var statusCode = txnObject.addTransaction();
			if (statusCode == ErrorCode.ERROR_TXN_SAVE_SUCCESS) {
				setTimeout(function () {
					var SettingCache = require('./../Cache/SettingCache.js');
					var settingCache = new SettingCache();
					if (settingCache.isCurrentCountryIndia()) {
						var MessageDraftLogic = require('../BizLogic/MessageDraftLogic');
						var messageDraftLogic = new MessageDraftLogic();
						messageDraftLogic.sendTransactionSMSToPartyAndOwner(txnObject);
					}
				}, 100);
			}
		} else {
			var statusCode = txnObject.updateTransaction(this.oldTxnObject);
		}
		return statusCode;
	};

	this.SaveTransactionIntr = function (obj, oldTxnObj) {
		var lineItems = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];
		var discountForCash = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;

		var NameLogic = require('./../BizLogic/nameLogic.js');
		var DataLoader = require('./../DBManager/DataLoader.js');
		var TransactionFactory = require('./../BizLogic/TransactionFactory.js');
		var TxnSubtypeConstant = require('./../Constants/TxnSubtypeConstant.js');
		var transactionfactory = new TransactionFactory();
		var dataLoader = new DataLoader();
		var namelogic = new NameLogic();
		var txnType = obj['txnType'];
		var paymentType = obj['paymentType'];
		var nameObj = namelogic.GetNameObjByNameAndTxn(txnType, obj['name']);
		if (!nameObj) {
			var _statusCode = ErrorCode.ERROR_NAME_DOESNT_EXIST;
			return _statusCode;
		}
		var firmId = obj['firmId'];
		// let invoiceType = obj['invoiceType'];
		var txnSubType = obj['invoiceType'];
		var selectedDate = obj['date'];
		var selectedDueDate = obj['dueDate'];
		var billNo = obj['billNo'];
		var placeOfSupply = obj['placeOfSupply'];
		var roundOffValue = obj['roundOffValue'];
		var prefixId = Number(obj['prefix']);
		var inclusiveOfTax = obj['taxInclusive'];
		var txnObject = transactionfactory.getTransactionObject(obj['txnType']);
		if (!txnObject) {
			try {
				var stackTraceErrForTracking = new Error().stack;
				AnalyticsExceptionTracker.logErrorToAnalytics('Error with no transaction object.. transaction type : ' + txnType + ' --- StackTrace : ' + stackTraceErrForTracking, false, 'critical-js-rrror', true);
			} catch (err) {}
		}
		txnObject.setTxnId(obj['txnId']);
		if (billNo) {
			var refNoObject = {};
			refNoObject.txnType = txnType;
			refNoObject.billNo = billNo;

			switch (txnType) {
				case TxnTypeConstant.TXN_TYPE_SALE:
				case TxnTypeConstant.TXN_TYPE_ESTIMATE:
				case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
				case TxnTypeConstant.TXN_TYPE_CASHIN:
				case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
				case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
				case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
					refNoObject.firmId = firmId;
					refNoObject.invoicePrefixId = prefixId;
					break;
				case TxnTypeConstant.TXN_TYPE_PURCHASE:
				case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
				case TxnTypeConstant.TXN_TYPE_CASHOUT:
					refNoObject.partyId = nameObj.getNameId();
					break;
			}

			if (this.isUpdateTransaction) {
				refNoObject.txnId = Number(oldTxnObj.getTxnId());
			}
			if (refNoObject.txnType) {
				var isRefNoUnique = dataLoader.isRefNoUnique(refNoObject);

				if (!isRefNoUnique) {
					if (txnType == TxnTypeConstant.TXN_TYPE_SALE) {
						return ErrorCode.ERROR_TXN_REFNO_ALREADY_USED;
					} else if (txnType == TxnTypeConstant.TXN_TYPE_ESTIMATE) {
						return ErrorCode.ERROR_TXN_ESTIMATE_NO_ALREADY_USED;
					} else if (txnType == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN) {
						return ErrorCode.ERROR_TXN_CHALLAN_NO_ALREADY_USED;
					} else if (txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER) {
						return ErrorCode.ERROR_TXN_REFNO_ORDER_ALREADY_USED;
					} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN) {
						return ErrorCode.ERROR_TXN_REFNO_CASH_IN_ALREADY_USED;
					} else if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
						return ErrorCode.ERROR_TXN_REFNO_ORDER_ALREADY_USED;
					} else if (txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
						return ErrorCode.ERROR_TXN_REFNO_RETURN_NUMBER_ALREADY_USED;
					} else if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
						var conf = confirm('Specified Bill No. has already been used for same party. Do you still want to continue?');
						if (!conf) {
							return ErrorCode.ERROR_DUPLICATE_BILL;
						}
					}
				}
			}
		}

		txnObject.setTaxTypeInclusive(inclusiveOfTax);
		// for adding line items
		statusCode = this.addItems(txnObject, lineItems);

		if (statusCode != ErrorCode.SUCCESS) {
			return statusCode;
		}
		txnObject.setPlaceOfSupply(placeOfSupply);
		txnObject.setRoundOffValue(roundOffValue);
		txnObject.setPaymentTypeId(obj['refNo']);
		txnObject.setImagePath(obj['imgFile']);
		txnObject.setImageId(obj['imageId']);
		txnObject.setNameRef(nameObj);
		txnObject.setTransactionTaxId(obj['transactionTaxId']);
		txnObject.setCustomFields(obj['customFields']);
		txnObject.setDisplayName(obj['displayName']);
		txnObject.setReverseCharge(obj['reverseCharge']);
		txnObject.setTxnITCApplicable(obj['txnITCValue']);
		txnObject.setPONumber(obj['PONumber']);
		txnObject.setPODate(obj['PODate']);
		txnObject.setTxnReturnDate(obj['txnReturnRefDate']);
		txnObject.setTxnReturnRefNumber(obj['txnReturnRefNumber']);
		txnObject.setEwayBillNumber(obj['ewayBillNumber']);
		txnObject.setSelectedTxnMapForB2B(obj['selectedTxnMap']);
		txnObject.setTxnPaymentTermId(obj['paymentTermId']);
		txnObject.setBillingAddress(obj.billingAddress || '');
		txnObject.setShippingAddress(obj.shippingAddress || '');

		if (!_MyString2.default.isCashSale(nameObj.getFullName())) {
			if (!nameObj.getAddress() && obj.billingAddress) {
				nameObj.setAddress(obj.billingAddress);
			}
			if (!nameObj.getShippingAddress() && obj.shippingAddress) {
				nameObj.setShippingAddress(obj.shippingAddress);
			}
		}

		var txn_total_amount = MyDouble.convertStringToDouble(obj['totalAmount']);
		var txn_cash_amount = MyDouble.convertStringToDouble(obj['amount']);
		var txn_current_balance_amount = obj['txnCurrentBalance'];

		txnObject.setTxnCurrentBalanceAmount(txn_current_balance_amount);

		var totalAmountForPaymentStatus = txn_total_amount;

		if (obj['txnType'] == TxnTypeConstant.TXN_TYPE_CASHIN || obj['txnType'] == TxnTypeConstant.TXN_TYPE_CASHOUT) {
			totalAmountForPaymentStatus = txn_total_amount + MyDouble.convertStringToDouble(discountForCash);
		}

		txnObject.setPaymentStatusBasedOnAmount(MyDouble.convertStringToDouble(txn_current_balance_amount), MyDouble.convertStringToDouble(totalAmountForPaymentStatus));

		if (statusCode == ErrorCode.SUCCESS) {
			statusCode = txnObject.setAmounts(txn_total_amount, txn_cash_amount);
		}
		if (statusCode == ErrorCode.SUCCESS) {
			txnObject.setFirmId(firmId);
			txnObject.setInvoicePrefixId(prefixId);
			if (prefixId) {
				var invoicePrefixOptions = obj.invoicePrefixOptions;
				if (invoicePrefixOptions && invoicePrefixOptions.length) {
					var found = invoicePrefixOptions.find(function (prefix) {
						return prefix.getPrefixId() == prefixId;
					});
					if (found) {
						txnObject.setInvoicePrefix(found.getPrefixValue());
					}
				}
			}
			txnObject.setTxnSubType(txnSubType);
			txnObject.setTxnDate(selectedDate);
			txnObject.setTxnDueDate(selectedDueDate);
			txnObject.setDescription(obj['description']);
			var discountAmountString = '';
			taxAmount = '';
			if (txnType == TxnTypeConstant['TXN_TYPE_CASHIN'] || txnType == TxnTypeConstant['TXN_TYPE_CASHOUT']) {
				discountAmountString = discountForCash;
			} else {
				discountAmountString = obj['discountAmount'];
			}
			if (!discountAmountString) {
				discountAmountString = 0.0;
			}
			var taxAmount = obj['taxAmount'];
			if (!taxAmount) {
				taxAmount = 0.0;
			}
			txnObject.setDiscountAmount(discountAmountString);
			txnObject.setDiscountPercent(obj['discountPercent']);
			txnObject.setTaxAmount(taxAmount);

			this.txnRefNumber = obj['billNo'];
			if (!this.txnRefNumber) {
				this.txnRefNumber = '';
			}

			txnObject.setTxnRefNumber(this.txnRefNumber);
			txnObject.setAc1Amount(obj.ac1Amount);
			txnObject.setAc2Amount(obj.ac2Amount);
			txnObject.setAc3Amount(obj.ac3Amount);

			if (txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_ESTIMATE || txnType == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
				txnObject.setStatus(TransactionStatus.TXN_ORDER_OPEN);
			}
			isImageAttached = '';

			txnObject.setTxnRefNumber(this.txnRefNumber);
			var selectPaymentMode = 1;
			var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
			var paymentinfocache = new PaymentInfoCache();
			// selectPaymentMode = paymentinfocache.getPaymentBankId(paymentType);
			txnObject.setPaymentTypeId(paymentType);
			txnObject.setPaymentTypeReference(obj['refNo']);

			txnObject.setUdfObjectArray(obj['extraFieldValueArray']);

			var statusCode = this.SaveTransaction(txnObject);
		}

		if (statusCode == ErrorCode.ERROR_TXN_SAVE_SUCCESS) {
			obj['txnId'] = txnObject['txnId'];
		}

		return statusCode;
	};

	this.UpdateTransactionIntr = function (obj, oldTxnObject) {
		var lineItems = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];
		var discountForCash = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;

		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var transactionManager = new TransactionManager();
		var TxnStatus = require('./../Constants/TransactionStatus.js');
		var statusCode = false;
		if ((Number(oldTxnObject.getTxnType()) == TxnTypeConstant.TXN_TYPE_SALE_ORDER || Number(oldTxnObject.getTxnType()) == TxnTypeConstant.TXN_TYPE_ESTIMATE || Number(oldTxnObject.getTxnType()) == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN) && obj.txnType == TxnTypeConstant.TXN_TYPE_SALE || Number(oldTxnObject.getTxnType()) == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER && obj.txnType == TxnTypeConstant.TXN_TYPE_PURCHASE) {
			var isTransactionBeginSuccess = transactionManager.BeginTransaction();

			if (!isTransactionBeginSuccess) {
				statusCode = ErrorCode.ERROR_TXN_SAVE_FAILED;
				return statusCode;
			}
			try {
				statusCode = oldTxnObject.closeOrderForm();
				if (statusCode) {
					statusCode = this.SaveTransactionIntr(obj, oldTxnObject, lineItems, discountForCash);
					if (statusCode == ErrorCode.ERROR_TXN_SAVE_SUCCESS) {
						if (Number(oldTxnObject.getTxnType()) == TxnTypeConstant.TXN_TYPE_SALE_ORDER || Number(oldTxnObject.getTxnType()) == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN) {
							var oldLineItemList = oldTxnObject.getLineItems();
							var lineItemLen = oldLineItemList.length;
							if (lineItemLen > 0) {
								for (var i = 0; i < lineItemLen; i++) {
									var lineItem = oldLineItemList[i];
									var item = itemcache.getItemById(lineItem.getItemId());
									if (item) {
										item.deleteItemStockQuantity(oldTxnObject.getTxnType(), lineItem.getTotalQuantityIncludingFreeQuantity(), TxnStatus.TXN_ORDER_OPEN);
									}
								}
							}
						}
						if (obj.txnType == TxnTypeConstant.TXN_TYPE_SALE && oldTxnObject.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN) {
							if (!oldTxnObject.convertToSale(obj.txnId) || !transactionManager.CommitTransaction()) {
								statusCode = ErrorCode.ERROR_TXN_SAVE_FAILED;
							}
						} else {
							if (!transactionManager.CommitTransaction()) {
								statusCode = ErrorCode.ERROR_TXN_SAVE_FAILED;
							}
						}
					}
				} else {
					statusCode = ErrorCode.ERROR_TXN_SAVE_FAILED;
				}
			} catch (err) {
				statusCode = ErrorCode.ERROR_TXN_SAVE_FAILED;
			}
			transactionManager.EndTransaction();

			return statusCode;
		} else {
			this.oldTxnObject = oldTxnObject;
			this.isUpdateTransaction = 1;
			var statusCode = this.SaveTransactionIntr(obj, oldTxnObject, lineItems, discountForCash);
			return statusCode;
		}
	};
};

module.exports = TransactionLogic;