var SettingCache = require('./../Cache/SettingCache.js');
var CompositeSchemeTaxRate = require('./../Constants/CompositeSchemeTaxRate.js');
var settingCache = new SettingCache();
var GSTR9AOutwardSuppliesObject = function GSTR9AOutwardSuppliesObject() {
	var compositeUserType = settingCache.getCompositeUserType();
	var rateOfTax = void 0;
	if (compositeUserType == CompositeUserType.MANUFACTURER) {
		rateOfTax = CompositeSchemeTaxRate.TAXRATE_MANUFACTURER;
	} else if (compositeUserType == CompositeUserType.TRADER) {
		rateOfTax = CompositeSchemeTaxRate.TAXRATE_TRADER;
	} else if (compositeUserType == CompositeUserType.RESTAURANT) {
		rateOfTax = CompositeSchemeTaxRate.TAXRATE_RESTAURANT;
	}

	this.intra_state_supplies_goods_amount = 0;
	this.intra_state_supplies_goods_amount_rate_change = 0;
	this.intra_state_supplies_goods_compounding_tax_rate = rateOfTax;
	this.intra_state_supplies_goods_compund_tax_amount = 0;
	this.intra_state_supplies_goods_compund_tax_amount_rate_change = 0;
	this.intra_state_supplies_services_amount = 0;
	this.intra_state_supplies_services_amount_rate_change = 0;
	this.intra_state_supplies_services_compounding_tax_rate = rateOfTax;
	this.intra_state_supplies_services_compund_tax_amount = 0;
	this.intra_state_supplies_services_compund_tax_amount_rate_change = 0;
	this.intra_state_supplies_exempted_amount = 0;
	this.intra_state_supplies_exempted_compounding_tax_rate = 0;
	this.intra_state_supplies_exempted_compund_tax_amount = 0;
	this.intra_state_supplies_exempted_amount_before = 0;
	this.intra_state_supplies_exempted_compounding_tax_rate_before = rateOfTax;
	this.intra_state_supplies_exempted_compund_tax_amount_before = 0;
	this.intra_state_supplies_nil_rated_amount = 0;
	this.intra_state_supplies_nil_rated_compounding_tax_rate = 0;
	this.intra_state_supplies_nil_rated_compund_tax_amount = 0;
	this.intra_state_supplies_nil_rated_amount_before = 0;
	this.intra_state_supplies_nil_rated_compounding_tax_rate_before = rateOfTax;
	this.intra_state_supplies_nil_rated_compund_tax_amount_before = 0;
	this.intra_state_supplies_non_gst_amount = 0;
	this.intra_state_supplies_non_gst_compounding_tax_rate = rateOfTax;
	this.intra_state_supplies_non_gst_compund_tax_amount = 0;
	this.intra_state_supplies_export_amount = 0;
	this.intra_state_supplies_export_compounding_tax_rate = rateOfTax;
	this.intra_state_supplies_export_compund_tax_amount = 0;
};

module.exports = GSTR9AOutwardSuppliesObject;