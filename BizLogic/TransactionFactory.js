var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TransactionFactory = function TransactionFactory() {
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');

	this.getSampleTxn = function (txnType) {
		var sampleTxn = this.getTransactionObject(txnType);
		sampleTxn.setTxnDate(new Date());
		sampleTxn.setTxnDueDate(new Date());
		sampleTxn.setTxnRefNumber('INV234');
		sampleTxn.setDescription('Balance to be paid in 3 days');
		sampleTxn.setDiscountAmount(80.0);
		sampleTxn.setTaxAmount(72.0);
		sampleTxn.setCashAmount(300.0);
		sampleTxn.setBalanceAmount(492.0);
		sampleTxn.addLineItem({
			name: 'sample item 1',
			quantity: '3',
			freequantity: '0',
			unitprice: '100.0',
			discountamount: '0',
			taxamount: '0',
			totalamount: '300.0'
		});
		sampleTxn.addLineItem({
			name: 'sample item 2',
			quantity: '3',
			freequantity: '0',
			unitprice: '150.0',
			discountamount: '0',
			taxamount: '0',
			totalamount: '450.0'
		});
		sampleTxn.addLineItem({
			name: 'sample item 3',
			quantity: '6',
			freequantity: '0',
			unitprice: '100.0',
			discountamount: '0',
			taxamount: '0',
			totalamount: '600.0'
		});
		sampleTxn.setPaymentTypeId(1);
		sampleTxn.setCustomFields((0, _stringify2.default)({
			transportation_details: [{ id: 1, value: 'Vyapar Logistics' }]
		}));

		var Name = require('../BizLogic/nameLogic');
		var NameType = require('../Constants/NameType');
		var name = new Name();
		name.setNameId(NameType.SAMPLE_TRANSACTION_ID);
		sampleTxn.setNameRef(name);

		return sampleTxn;
	};

	this.getTransactionObject = function (txnType) {
		txnType = Number(txnType);
		var BaseTransaction = require('./BaseTransaction.js');
		var basetransaction = new BaseTransaction();

		var txnObject = {};
		switch (txnType) {
			case TxnTypeConstant['TXN_TYPE_ROPENBALANCE']:
				var ROpenBalanceTransaction = require('./ROpenBalanceTransaction.js');
				var BaseOpenBalanceTransaction = require('./BaseOpenBalanceTransaction.js');
				baseopenbalancetransaction = new BaseOpenBalanceTransaction();
				baseopenbalancetransaction.__proto__ = basetransaction;
				txnObject = new ROpenBalanceTransaction();
				txnObject.__proto__ = baseopenbalancetransaction;
				// console.log(txnObject);
				break;
			case TxnTypeConstant['TXN_TYPE_POPENBALANCE']:
				var POpenBalanceTransaction = require('./POpenBalanceTransaction.js');
				var BaseOpenBalanceTransaction = require('./BaseOpenBalanceTransaction.js');
				baseopenbalancetransaction = new BaseOpenBalanceTransaction();
				baseopenbalancetransaction.__proto__ = basetransaction;
				txnObject = new POpenBalanceTransaction();
				txnObject.__proto__ = baseopenbalancetransaction;
				break;
			case TxnTypeConstant['TXN_TYPE_PURCHASE']:
				var PurchaseTransaction = require('./PurchaseTransaction.js');
				txnObject = new PurchaseTransaction();
				txnObject.__proto__ = basetransaction;
				break;
			case TxnTypeConstant['TXN_TYPE_SALE']:
				var saleTransaction = require('./SaleTransaction.js');
				txnObject = new saleTransaction();
				txnObject.__proto__ = basetransaction;
				break;
			case TxnTypeConstant['TXN_TYPE_PURCHASE_RETURN']:
				var saleTransaction = require('./PurchaseReturnTransaction.js');
				txnObject = new saleTransaction();
				txnObject.__proto__ = basetransaction;
				break;
			case TxnTypeConstant['TXN_TYPE_SALE_RETURN']:
				var saleTransaction = require('./SaleReturnTransaction.js');
				txnObject = new saleTransaction();
				txnObject.__proto__ = basetransaction;
				break;
			case TxnTypeConstant['TXN_TYPE_EXPENSE']:
				var saleTransaction = require('./ExpenseTransaction.js');
				txnObject = new saleTransaction();
				txnObject.__proto__ = basetransaction;
				break;
			case TxnTypeConstant['TXN_TYPE_OTHER_INCOME']:
				var saleTransaction = require('./IncomeTransaction.js');
				txnObject = new saleTransaction();
				txnObject.__proto__ = basetransaction;
				break;

			case TxnTypeConstant.TXN_TYPE_LINKED_CASHIN_OPENINGBALANCE:
				var saleTransaction = require('./CashInOpeningBalance.js');
				txnObject = new saleTransaction();
				txnObject.__proto__ = basetransaction;
				break;
			case TxnTypeConstant['TXN_TYPE_CASHIN']:
				var saleTransaction = require('./MoneyInTransaction.js');
				txnObject = new saleTransaction();
				txnObject.__proto__ = basetransaction;
				break;
			case TxnTypeConstant.TXN_TYPE_LINKED_CASHOUT_OPENINGBALANCE:
				var saleTransaction = require('./CashOutOpeningBalance.js');
				txnObject = new saleTransaction();
				txnObject.__proto__ = basetransaction;
				break;
			case TxnTypeConstant['TXN_TYPE_CASHOUT']:
				var saleTransaction = require('./MoneyOutTransaction.js');
				txnObject = new saleTransaction();
				txnObject.__proto__ = basetransaction;
				break;
			case TxnTypeConstant['TXN_TYPE_SALE_ORDER']:
				var saleOrderTransaction = require('./SaleOrderTransaction.js');
				txnObject = new saleOrderTransaction();
				txnObject.__proto__ = basetransaction;
				break;
			case TxnTypeConstant['TXN_TYPE_PURCHASE_ORDER']:
				var PurchaseOrderTransaction = require('./PurchaseOrderTransaction.js');
				txnObject = new PurchaseOrderTransaction();
				txnObject.__proto__ = basetransaction;
				break;
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
				var DeliveryChallanTransaction = require('./DeliveryChallanTransaction');
				txnObject = new DeliveryChallanTransaction();
				txnObject.__proto__ = basetransaction;
				break;
			case TxnTypeConstant['TXN_TYPE_ESTIMATE']:
				var EstimateTransaction = require('./EstimateTransaction.js');
				txnObject = new EstimateTransaction();
				txnObject.__proto__ = basetransaction;
				break;
			case TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD:
			case TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD_WITHOUT_CASH:
			case TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE:
			case TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE_WITHOUT_CASH:
			case TxnTypeConstant.TXN_TYPE_BANK_TO_BANK:
				var bankAdjustmentForReport = require('./BankAdjustmentForReport.js');
				txnObject = new bankAdjustmentForReport(txnType);
				txnObject.__proto__ = basetransaction;
				break;
			case TxnTypeConstant.TXN_TYPE_CASH_ADJ_ADD:
			case TxnTypeConstant.TXN_TYPE_CASH_ADJ_REDUCE:
			case TxnTypeConstant.TXN_TYPE_CASH_OPENINGBALANCE:
				var cashAdjustmentForCashReport = require('./CashAdjustmentForCashReport.js');
				txnObject = new cashAdjustmentForCashReport(txnType);
				txnObject.__proto__ = basetransaction;
				break;
			case TxnTypeConstant.TXN_TYPE_CHEQUE_TRANSFER:
				var chequeTransferForCashReportTransaction = require('./ChequeTransferForCashReporttransaction.js');
				txnObject = new chequeTransferForCashReportTransaction();
				txnObject.__proto__ = basetransaction;
				break;
			case TxnTypeConstant.TXN_TYPE_BEGINNING_BALANCE_PAYABLE:
				var BeginningBalancePayableTransaction = require('./BeginningBalancePayableTransaction.js');
				txnObject = new BeginningBalancePayableTransaction();
				txnObject.__proto__ = basetransaction;
				break;
			case TxnTypeConstant.TXN_TYPE_BEGINNING_BALANCE_RECEIVABLE:
				var BeginningBalanceReceivableTransaction = require('./BeginningBalanceReceivableTransaction.js');
				txnObject = new BeginningBalanceReceivableTransaction();
				txnObject.__proto__ = basetransaction;
				break;
			case TxnTypeConstant.TXN_TYPE_LOAN_STARTING_BALANCE:
			case TxnTypeConstant.TXN_TYPE_LOAN_PROCESSING_FEE:
			case TxnTypeConstant.TXN_TYPE_LOAN_ADJUSTMENT:
			case TxnTypeConstant.TXN_TYPE_LOAN_EMI_PAYMENT:
			case TxnTypeConstant.TXN_TYPE_LOAN_CLOSE_BOOK_ADJ:
				var LoanTransactionForReport = require('./LoanTransactionForReport.js');
				txnObject = new LoanTransactionForReport();
				txnObject.__proto__ = basetransaction;
				break;
		}
		return txnObject;
	};

	this.getTransTypeString = function (transType) {
		var transactionTypeString = null;
		switch (transType) {
			case TxnTypeConstant.TXN_TYPE_PURCHASE:
				transactionTypeString = 'Purchase';
				break;
			case TxnTypeConstant.TXN_TYPE_SALE:
				transactionTypeString = 'Sale';
				break;
			case TxnTypeConstant.TXN_TYPE_CASHOUT:
				transactionTypeString = 'Payment-Out';
				break;
			case TxnTypeConstant.TXN_TYPE_CASHIN:
				transactionTypeString = 'Payment-In';
				break;
			case TxnTypeConstant.TXN_TYPE_ROPENBALANCE:
			case TxnTypeConstant.TXN_TYPE_POPENBALANCE:
				transactionTypeString = 'Opening balance';
				break;
			case TxnTypeConstant.TXN_TYPE_EXPENSE:
				transactionTypeString = 'Expense';
				break;
			case TxnTypeConstant.TXN_TYPE_OTHER_INCOME:
				transactionTypeString = 'Other Income';
				break;
			case TxnTypeConstant.TXN_TYPE_BEGINNING_BALANCE_PAYABLE:
			case TxnTypeConstant.TXN_TYPE_BEGINNING_BALANCE_RECEIVABLE:
				transactionTypeString = 'Beginning Balance';
				break;
			case TxnTypeConstant.TXN_TYPE_ADJ_OPENING:
				transactionTypeString = 'Opening stock';
				break;
			case TxnTypeConstant.TXN_TYPE_ADJ_ADD:
				transactionTypeString = 'Add Stock';
				break;
			case TxnTypeConstant.TXN_TYPE_ADJ_REDUCE:
				transactionTypeString = 'Reduce Stock';
				break;
			case TxnTypeConstant.TXN_TYPE_BANK_OPENING:
				transactionTypeString = 'Opening balance';
				break;
			case TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD:
				transactionTypeString = 'Deposited';
				break;
			case TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE:
				transactionTypeString = 'Withdrawn';
				break;
			case TxnTypeConstant.TXN_TYPE_BANK_BALANCE_FORWARD:
				transactionTypeString = 'Balance B/F';
				break;
			case TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD_WITHOUT_CASH:
				transactionTypeString = 'Increase balance';
				break;
			case TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE_WITHOUT_CASH:
				transactionTypeString = 'Reduce balance';
				break;
			case TxnTypeConstant.TXN_TYPE_CASH_ADJ_ADD:
				transactionTypeString = 'Add cash';
				break;
			case TxnTypeConstant.TXN_TYPE_CASH_ADJ_REDUCE:
				transactionTypeString = 'Reduce cash';
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
				transactionTypeString = 'Credit Note';
				break;
			case TxnTypeConstant.TXN_TYPE_CHEQUE_TRANSFER:
				transactionTypeString = 'Cheque Transfer';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
				transactionTypeString = 'Debit Note';
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
				transactionTypeString = 'Sale Order';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
				transactionTypeString = 'Purchase Order';
				break;
			case TxnTypeConstant.TXN_TYPE_ESTIMATE:
				transactionTypeString = 'Estimate';
				break;
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
				transactionTypeString = 'Delivery Challan';
				break;
			case TxnTypeConstant.TXN_TYPE_BANK_TO_BANK:
				transactionTypeString = 'To Bank';
				break;
		}
		return transactionTypeString;
	};

	this.getTransTypeStringForTransactionPDF = function (transType, txnSubType) {
		var printDeliveryChallan = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
		var isTxnTaxable = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;

		var TxnSubtypeConstant = require('./../Constants/TxnSubtypeConstant.js');
		var SettingCache = require('./../Cache/SettingCache.js');
		var settingsCache = new SettingCache();
		var transactionTypeString = null;

		if (printDeliveryChallan) {
			transactionTypeString = settingsCache.getHeaderForDeliveryChallan();
			if (!transactionTypeString) {
				transactionTypeString = 'Delivery Challan';
			}
			return transactionTypeString;
		}
		switch (transType) {
			case TxnTypeConstant.TXN_TYPE_PURCHASE:
				transactionTypeString = settingsCache.getHeaderForPurchase();
				if (!transactionTypeString) {
					transactionTypeString = 'Bill';
				}
				break;
			case TxnTypeConstant.TXN_TYPE_SALE:
				txnSubType = txnSubType ? Number(txnSubType) : 1;
				switch (txnSubType) {
					case TxnSubtypeConstant['tax invoice']:
						transactionTypeString = settingsCache.getHeaderForTaxSale();
						if (!transactionTypeString) {
							transactionTypeString = 'Tax Invoice';
						}
						break;
					default:
						transactionTypeString = settingsCache.getHeaderForSale();
						if (!transactionTypeString) {
							transactionTypeString = 'Invoice';
						}
				}
				if (settingCache.printBillOfSupplyForNonTaxInvoice() && isTxnTaxable) {
					transactionTypeString = 'Bill of Supply';
				}
				break;
			case TxnTypeConstant.TXN_TYPE_CASHOUT:
				transactionTypeString = settingsCache.getHeaderForCashOut();
				if (!transactionTypeString) {
					transactionTypeString = 'Payment Out';
				}
				break;
			case TxnTypeConstant.TXN_TYPE_CASHIN:
				transactionTypeString = settingsCache.getHeaderForCashIn();
				if (!transactionTypeString) {
					transactionTypeString = 'Payment Receipt';
				}
				break;
			case TxnTypeConstant.TXN_TYPE_EXPENSE:
				transactionTypeString = settingsCache.getHeaderForExpense();
				if (!transactionTypeString) {
					transactionTypeString = 'Expense';
				}
				break;
			case TxnTypeConstant.TXN_TYPE_OTHER_INCOME:
				transactionTypeString = settingsCache.getHeaderForIncome();
				if (!transactionTypeString) {
					transactionTypeString = 'Other Income';
				}
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
				transactionTypeString = settingsCache.getHeaderForSaleReturn();
				if (!transactionTypeString) {
					transactionTypeString = 'Credit Note';
				}
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
				transactionTypeString = settingsCache.getHeaderForPurchaseReturn();
				if (!transactionTypeString) {
					transactionTypeString = 'Debit Note';
				}
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
				transactionTypeString = settingsCache.getHeaderForSaleOrder();
				if (!transactionTypeString) {
					transactionTypeString = 'Sale Order';
				}
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
				transactionTypeString = settingsCache.getHeaderForPurchaseOrder();
				if (!transactionTypeString) {
					transactionTypeString = 'Purchase Order';
				}
				break;
			case TxnTypeConstant.TXN_TYPE_ESTIMATE:
				transactionTypeString = settingsCache.getHeaderForEstimate();
				if (!transactionTypeString) {
					transactionTypeString = 'Estimate';
				}
				break;
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
				transactionTypeString = settingsCache.getHeaderForDeliveryChallan();
				if (!transactionTypeString) {
					transactionTypeString = 'Delivery Challan';
				}
				break;
			default:
				transactionTypeString = this.getTransTypeString(transType);
		}
		return transactionTypeString;
	};
};

module.exports = TransactionFactory;