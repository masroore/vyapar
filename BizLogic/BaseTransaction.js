var UDFValueModel = require('./../Models/udfValueModel.js');
var UDFFieldsConstant = require('./../Constants/UDFFieldsConstant.js');
var TaxCodeCache = require('./../Cache/TaxCodeCache.js');
var ItemCache = require('./../Cache/ItemCache.js');
var SettingCache = require('./../Cache/SettingCache.js');
var NameCache = require('./../Cache/NameCache.js');
var ErrorCode = require('./../Constants/ErrorCode.js');
var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var TxnITCConstants = require('./../Constants/TxnITCConstants.js');
var TransactionStatus = require('./../Constants/TransactionStatus.js');
var TxnTaxType = require('./../Constants/ItemType.js');
var TxnPaymentStatusConstants = require('./../Constants/TxnPaymentStatusConstants.js');
var MyDouble = require('./../Utilities/MyDouble.js');
var DataLoader = require('./../DBManager/DataLoader.js');
var BaseLineItem = require('./BaseLineItem.js');
var TransactionLinksModel = require('./../Models/TransactionLinksModel.js');
var ClosedLinkTxnModel = require('./../Models/ClosedLinkTxnModel.js');
var TransactionModel = require('./../Models/TransactionModel.js');
var TransactionManager = require('./../DBManager/TransactionManager.js');
var ChequeLogic = require('./../BizLogic/ChequeLogic.js');
var ItemStockTrackingModel = require('./../Models/ItemStockTrackingModel.js');

var BaseTransaction = function BaseTransaction() {
	var dataloader = new DataLoader();
	this.txnDate = null;
	this.description = '';
	this.cashAmount = 0;
	this.balanceAmount = 0;
	this.discountAmount = 0;
	this.paymentTypeReference = null;
	this.txnDueDate = null;
	this.txnId = null;
	this.customFields = '';
	this.reverseCharge = 0;
	this.displayName = '';
	this.paymentTypeId = 1;
	this.paymentTypeReference = '';
	this.lineItems = null;
	this.groupId = null;
	this.taxPercent = 0;
	this.imagePath = '';
	this.imageId = null;
	this.imageBlob = null;
	this.ac1Amount = 0;
	this.ac2Amount = 0;
	this.ac3Amount = 0;
	this.firmId = null;
	this.invoicePrefix = null;
	this.txnSubType = 0;
	this.bankId = 0;
	this.transactionTaxId = null;
	this.placeOfSupply = '';
	this.roundOffValue = 0;
	this.discountPercent = 0.0;
	this.txnType = null;
	this.txnITCApplicable = TxnITCConstants.ITC_ELIGIBLE;
	this.PONumber = '';
	this.PODate = null;
	this.txnReturnDate = null;
	this.txnReturnRefNumber = '';
	this.createdAt = null;
	this.ewayBillNumber = '';
	this.status = TransactionStatus.TXN_OPEN;
	this.txnCurrentBalanceAmount = 0;
	this.txnPaymentStatus = null;
	this.txnSelectedTxnMapForB2B = null;
	var txnPaymentTermId = null;
	var extraFieldObjectArray = [];
	var invoicePrefixId = void 0;
	var billingAddress = '';
	var shippingAddress = '';

	this.setBillingAddress = function (value) {
		billingAddress = value;
	};

	this.getBillingAddress = function () {
		return billingAddress;
	};

	this.setShippingAddress = function (value) {
		shippingAddress = value;
	};

	this.getShippingAddress = function () {
		return shippingAddress;
	};
	var taxInclusive = TxnTaxType.ITEM_TXN_TAX_EXCLUSIVE;

	this.getTaxTypeInclusive = function () {
		return taxInclusive;
	};

	this.setTaxTypeInclusive = function (tax_inclusive) {
		taxInclusive = tax_inclusive;
	};

	this.setInvoicePrefixId = function (invoice_prefix_id) {
		invoicePrefixId = invoice_prefix_id;
	};

	this.getInvoicePrefixId = function () {
		return invoicePrefixId;
	};

	this.getNameId = function () {
		return this.nameId;
	};

	this.setNameId = function (nameId) {
		this.nameId = nameId;
	};

	this.getUdfObjectArray = function () {
		return extraFieldObjectArray;
	};

	this.setUdfObjectArray = function (ExtraFieldObjectArray) {
		if (ExtraFieldObjectArray && ExtraFieldObjectArray.length) {
			if (!Array.isArray(ExtraFieldObjectArray)) {
				throw new Error('udf fields are not in an array');
			}
			extraFieldObjectArray = ExtraFieldObjectArray;
		}
	};

	this.getTxnPaymentTermId = function () {
		return txnPaymentTermId;
	};
	this.setTxnPaymentTermId = function (txnpaymenttermId) {
		txnPaymentTermId = txnpaymenttermId;
	};
	this.getSelectedTxnMapForB2B = function () {
		return this.txnSelectedTxnMapForB2B;
	};
	this.setSelectedTxnMapForB2B = function (txnSelectedTxnMapForB2B) {
		this.txnSelectedTxnMapForB2B = txnSelectedTxnMapForB2B;
	};

	this.getTxnCurrentBalanceAmount = function () {
		return this.txnCurrentBalanceAmount;
	};
	this.setTxnCurrentBalanceAmount = function (txnCurrentBalanceAmount) {
		this.txnCurrentBalanceAmount = txnCurrentBalanceAmount;
	};

	this.getTxnPaymentStatus = function () {
		return this.txnPaymentStatus;
	};
	this.setTxnPaymentStatus = function (txnPaymentStatus) {
		this.txnPaymentStatus = txnPaymentStatus;
	};

	this.getCreatedAt = function () {
		return this.createdAt;
	};
	this.setCreatedAt = function (createdAt) {
		this.createdAt = createdAt;
	};

	this.getEwayBillNumber = function () {
		return this.ewayBillNumber;
	};
	this.setEwayBillNumber = function (ewayBillNumber) {
		this.ewayBillNumber = ewayBillNumber;
	};

	this.getTxnReturnDate = function () {
		return this.txnReturnDate;
	};
	this.setTxnReturnDate = function (txnReturnDate) {
		this.txnReturnDate = txnReturnDate;
	};

	this.getTxnReturnRefNumber = function () {
		return this.txnReturnRefNumber;
	};
	this.setTxnReturnRefNumber = function (txnReturnRefNumber) {
		this.txnReturnRefNumber = txnReturnRefNumber;
	};

	this.getPONumber = function () {
		return this.PONumber;
	};

	this.setPONumber = function (PONumber) {
		this.PONumber = PONumber;
	};

	this.getPODate = function () {
		return this.PODate;
	};
	this.setPODate = function (PODate) {
		this.PODate = PODate;
	};

	this.getTxnITCApplicable = function () {
		return this.txnITCApplicable;
	};
	this.setTxnITCApplicable = function (txnITCApplicable) {
		this.txnITCApplicable = txnITCApplicable;
	};

	this.getTxnType = function () {
		return this.txnType;
	};
	this.setTxnType = function (txnType) {
		this.txnType = txnType;
	};

	this.getDiscountPercent = function () {
		return this.discountPercent;
	};
	this.setDiscountPercent = function (discountPercent) {
		this.discountPercent = discountPercent;
	};

	this.getRoundOffValue = function () {
		return this.roundOffValue;
	};
	this.setRoundOffValue = function (roundOffValue) {
		this.roundOffValue = roundOffValue;
	};

	this.getPlaceOfSupply = function () {
		return this.placeOfSupply;
	};
	this.setPlaceOfSupply = function (placeOfSupply) {
		this.placeOfSupply = placeOfSupply;
	};

	this.getTransactionTaxId = function () {
		return this.transactionTaxId;
	};
	this.setTransactionTaxId = function (transactionTaxId) {
		this.transactionTaxId = transactionTaxId;
	};

	this.getTxnSubType = function () {
		return this.txnSubType;
	};
	this.setTxnSubType = function (txnSubType) {
		this.txnSubType = txnSubType;
	};

	this.getFirmId = function () {
		return this.firmId;
	};
	this.setFirmId = function (firmId) {
		this.firmId = firmId || '';
	};

	this.getInvoicePrefix = function () {
		return this.invoicePrefix;
	};
	this.setInvoicePrefix = function (invoicePrefix) {
		this.invoicePrefix = invoicePrefix || '';
	};

	this.getAc1Amount = function () {
		return this.ac1Amount;
	};
	this.setAc1Amount = function (ac1Amount) {
		this.ac1Amount = ac1Amount;
	};

	this.getAc2Amount = function () {
		return this.ac2Amount;
	};
	this.setAc2Amount = function (ac2Amount) {
		this.ac2Amount = ac2Amount;
	};

	this.getAc3Amount = function () {
		return this.ac3Amount;
	};
	this.setAc3Amount = function (ac3Amount) {
		this.ac3Amount = ac3Amount;
	};

	this.setImageBlob = function (imageBlob) {
		this.imageBlob = imageBlob;
	};
	this.getImageBlob = function () {
		return this.imageBlob;
	};

	this.getImageId = function () {
		return this.imageId;
	};
	this.setImageId = function (imageId) {
		this.imageId = imageId;
	};

	this.getTxnId = function () {
		return this.txnId;
	};
	this.setTxnId = function (txnId) {
		this.txnId = txnId;
	};

	this.getStatus = function () {
		return this.status;
	};
	this.setStatus = function (status) {
		this.status = status;
	};

	this.getNameRef = function () {
		var nameId = this.getNameId();
		var nameCache = new NameCache();
		if (nameId) {
			var nameObj = nameCache.findNameObjectByNameId(nameId);
			if (nameObj) {
				return nameObj;
			}
			var NameType = require('../Constants/NameType');
			var NameLogic = require('../BizLogic/nameLogic');
			var StateCodes = require('../Constants/StateCode');
			if (nameId == NameType.SAMPLE_TRANSACTION_ID) {
				var name = new NameLogic();
				name.setFullName('Vyapar tech solutions (Sample Party Name)');
				name.setPhoneNumber('+91-9916137039');
				name.setAddress('Sarjapur Road, Bangalore');
				name.setNameType(NameType.NAME_TYPE_PARTY);
				name.setGstinNumber(NameType.DEFAULT_GTTIN_NUMBER);
				name.setTinNumber(NameType.DEFAULT_TIN_NUMBER);
				name.setShippingAddress(NameType.DEFAULT_SHIPPING_ADDRESS);
				name.setCustomerType(NameType.DEFAULT_CUSTOMER_TYPE);
				name.setContactState(StateCodes.getStateName(29));
				name.setAmount(800);
				name.setGroupId(0);
				name.setNameId(NameType.SAMPLE_TRANSACTION_ID);
				return name;
			}
		}
		return null;
	};

	this.setNameRef = function (nameRef) {
		this.nameId = nameRef.getNameId();
	};

	this.getTxnDate = function () {
		return this.txnDate;
	};
	this.setTxnDate = function (txnDate) {
		this.txnDate = txnDate;
	};

	this.getDescription = function () {
		return this.description;
	};
	this.setDescription = function (description) {
		if (description) {
			this.description = description;
		} else {
			this.description = '';
		}
	};

	this.getBalanceAmount = function () {
		return this.balanceAmount;
	};
	this.setBalanceAmount = function (balanceAmount) {
		this.balanceAmount = balanceAmount;
	};

	this.getCashAmount = function () {
		return this.cashAmount;
	};
	this.setCashAmount = function (cashAmount) {
		this.cashAmount = cashAmount;
	};

	this.getTxnDueDate = function () {
		return this.txnDueDate;
	};
	this.setTxnDueDate = function (txnDueDate) {
		this.txnDueDate = txnDueDate;
	};

	this.getDiscountAmount = function () {
		return this.discountAmount;
	};
	this.setDiscountAmount = function (discountAmount) {
		this.discountAmount = discountAmount;
	};

	this.getBankId = function () {
		return this.bankId;
	};
	this.setBankId = function (bankId) {
		this.bankId = bankId;
	};

	this.getImagePath = function () {
		return this.imagePath;
	};
	this.setImagePath = function (imagePath) {
		this.imagePath = imagePath;
	};

	this.getTaxAmount = function () {
		return this.taxAmount;
	};
	this.setTaxAmount = function (taxAmount) {
		this.taxAmount = taxAmount;
	};

	this.getTaxPercent = function () {
		var taxPercent = 0;
		if (this.getTransactionTaxId() && this.getTransactionTaxId() > 0) {
			var taxCodeCache = new TaxCodeCache();
			taxPercent = taxCodeCache.getRateForTaxId(this.getTransactionTaxId());
		}
		this.taxPercent = taxPercent;
		return taxPercent;
	};

	this.getImagePath = function () {
		return this.imagePath;
	};

	// this.getFullImagePath = function () {
	// 	if (getImagePath()) {
	// 		return FolderConstants.getImageFolderPath() + getImagePath();
	// 	}
	// 	return '';
	// };

	this.setTaxAmount = function (taxAmount) {
		this.taxAmount = taxAmount;
	};

	this.getPaymentTypeId = function () {
		return this.paymentTypeId;
	};
	this.setPaymentTypeId = function (paymentTypeId) {
		this.paymentTypeId = paymentTypeId;
	};

	this.getPaymentTypeReference = function () {
		return this.paymentTypeReference;
	};
	this.setPaymentTypeReference = function (paymentTypeReference) {
		this.paymentTypeReference = paymentTypeReference;
	};

	this.getLineItems = function () {
		if (this.lineItems == null) {
			this.loadAllLineItems();
		}
		return this.lineItems;
	};

	this.setLineItems = function (lineItems) {
		this.lineItems = lineItems;
	};

	this.getCustomFields = function () {
		return this.customFields;
	};
	this.setCustomFields = function (customFields) {
		this.customFields = customFields;
	};

	this.getDisplayName = function () {
		return this.displayName;
	};
	this.setDisplayName = function (displayName) {
		this.displayName = displayName;
	};

	this.getReverseCharge = function () {
		return this.reverseCharge;
	};
	this.setReverseCharge = function (reverseCharge) {
		this.reverseCharge = reverseCharge;
	};

	this.loadAllLineItems = function () {
		this.lineItems = dataloader.LoadAllLineItems(this.txnId);
	};

	this.setPaymentStatusBasedOnAmount = function (currentBalance, totalAmount) {
		if (currentBalance > 0.000001) {
			if (Math.abs(totalAmount - currentBalance) < 0.000001) {
				this.txnPaymentStatus = TxnPaymentStatusConstants.UNPAID;
			} else {
				this.txnPaymentStatus = TxnPaymentStatusConstants.PARTIAL;
			}
		} else {
			this.txnPaymentStatus = TxnPaymentStatusConstants.PAID;
		}
	};

	this.deleteTransactionLinks = function (txnId) {
		var deleteDataFromClosedTxns = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

		var transactionLinksModel = new TransactionLinksModel();

		var closedLinkTxnModel = new ClosedLinkTxnModel();

		var dataLoader = new DataLoader();

		var txnLinksList = dataLoader.loadAllTransactionLinksByTxnId(txnId);

		var statusCode = ErrorCode.ERROR_TXN_LINK_DELETE_FAILED;

		var selectTxnMap = this.getSelectedTxnMapForB2B();

		if (txnLinksList.length > 0) {
			for (var i = 0; i < txnLinksList.length; i++) {
				var linkObj = txnLinksList[i];
				var newAmount = MyDouble.convertStringToDouble(linkObj.getTxnLinksAmount());

				if (txnId == linkObj.getTxnLinksTxn1Id()) {
					var txnIdToUpdate = linkObj.getTxnLinksTxn2Id();
				} else if (txnId == linkObj.getTxnLinksTxn2Id()) {
					txnIdToUpdate = linkObj.getTxnLinksTxn1Id();
				}

				if (txnIdToUpdate != null) {
					var newTxnObj = dataLoader.LoadTransactionFromId(txnIdToUpdate);

					var oldBalance = MyDouble.convertStringToDouble(newTxnObj.getTxnCurrentBalanceAmount());

					var newCurrentBalance = oldBalance + newAmount;

					statusCode = this.updateLinkedTransactionForB2B(newTxnObj, newCurrentBalance);

					if (selectTxnMap && selectTxnMap.size > 0) {
						var mapVal = selectTxnMap.get(Number(txnIdToUpdate));

						if (mapVal) {
							var selectedTxnObject = mapVal[0];

							selectedTxnObject.setTxnCurrentBalanceAmount(newTxnObj.getTxnCurrentBalanceAmount());
						}
					}
				} else {
					// for closed txn links   id is null
					statusCode = ErrorCode.ERROR_TXN_LINKED_TRANSACTION_UPDATE_SUCCESS;
				}

				if (statusCode == ErrorCode.ERROR_TXN_LINKED_TRANSACTION_UPDATE_SUCCESS) {
					statusCode = transactionLinksModel.deleteTransactionLinkByLinkId(linkObj.getTxnLinksId());

					if (statusCode == ErrorCode.ERROR_TXN_LINK_DELETE_FAILED) {
						return statusCode;
					} else {
						statusCode = ErrorCode.ERROR_TXN_LINK_DELETE_SUCCESS;
					}
				} else {
					return statusCode;
				}

				if (statusCode == ErrorCode.ERROR_TXN_LINK_DELETE_SUCCESS) {
					//  delete closed txns
					if (deleteDataFromClosedTxns) {
						statusCode = ErrorCode.ERROR_CLOSED_TXN_LINK_DELETE_FAILED;

						statusCode = closedLinkTxnModel.deleteClosedLinkTxnDetail(MyDouble.convertStringToDouble(linkObj.getTxnLinkClosedTxnRefId()));

						if (statusCode == ErrorCode.ERROR_CLOSED_TXN_LINK_DELETE_SUCCESS) {
							statusCode = ErrorCode.ERROR_TXN_LINK_DELETE_SUCCESS;
						} else {
							return statusCode;
						}
					}
				} else {
					return statusCode;
				}
			}
		} else {
			statusCode = ErrorCode.ERROR_TXN_LINK_DELETE_SUCCESS;
		}

		return statusCode;
	};

	this.updateTransactionLinks = function () {
		var statusCode = this.deleteTransactionLinks(this.getTxnId());

		if (statusCode == ErrorCode.ERROR_TXN_LINK_DELETE_SUCCESS) {
			statusCode = this.createTransactionLinks();

			if (statusCode == ErrorCode.ERROR_TXN_LINK_CREATE_SUCCESS) {
				statusCode = ErrorCode.ERROR_TXN_LINK_UPDATE_SUCCESS;
			} else {
				statusCode = ErrorCode.ERROR_TXN_LINK_UPDATE_FAILED;
			}
		} else {
			statusCode = ErrorCode.ERROR_TXN_LINK_UPDATE_FAILED;
		}

		return statusCode;
	};

	this.updateLinkedTransactionForB2B = function (txn2Object, newCurrentBalance) {
		var txn2Total = MyDouble.convertStringToDouble(txn2Object.getCashAmount()) + MyDouble.convertStringToDouble(txn2Object.getBalanceAmount());

		txn2Object.setTxnCurrentBalanceAmount(newCurrentBalance);
		txn2Object.setPaymentStatusBasedOnAmount(MyDouble.convertStringToDouble(newCurrentBalance), MyDouble.convertStringToDouble(txn2Total));

		var newTransactionModel = new TransactionModel();

		var statusCode = txn2Object.updateTransactions(newTransactionModel);

		if (statusCode == ErrorCode.ERROR_TXN_SAVE_SUCCESS) {
			statusCode = ErrorCode.ERROR_TXN_LINKED_TRANSACTION_UPDATE_SUCCESS;
		}

		return statusCode;
	};

	this.createTransactionLinks = function () {
		var selectTxnMap = this.getSelectedTxnMapForB2B();

		var statusCode = ErrorCode.ERROR_TXN_LINK_CREATE_SUCCESS;

		var currentObj = this;

		if (selectTxnMap && selectTxnMap.size > 0) {
			selectTxnMap.forEach(function (value, key) {
				var txn2Id = MyDouble.convertStringToDouble(key);
				var txn2Object = value[0]; // for normal transactions( txn2Object  = base transaction object)   for closed txn ( txn2Object = Closed txn model object)
				var newAmount = MyDouble.convertStringToDouble(value[1]);

				var transactionLinksModel = new TransactionLinksModel();

				transactionLinksModel.setTxnLinksTxn1Id(currentObj.getTxnId());
				transactionLinksModel.setTxnLinksAmount(newAmount);
				transactionLinksModel.setTxnLinksTxn1Type(currentObj.getTxnType());

				if (txn2Id > 0) {
					// normal transactions
					var dataLoader = new DataLoader();

					txn2Object = dataLoader.LoadTransactionFromId(txn2Object.getTxnId()); // specific use case for sync

					transactionLinksModel.setTxnLinksTxn2Id(txn2Object.getTxnId());
					transactionLinksModel.setTxnLinksTxn2Type(txn2Object.getTxnType());
					transactionLinksModel.setTxnLinkClosedTxnRefId(null);
				} else {
					// for closed link transactions
					var txnLink = value[2];
					transactionLinksModel.setTxnLinksTxn2Id(null);
					transactionLinksModel.setTxnLinksTxn2Type(txn2Object.getClosedLinkTxnType());
					transactionLinksModel.setTxnLinkClosedTxnRefId(txnLink.getTxnLinkClosedTxnRefId());
				}

				statusCode = transactionLinksModel.createTransactionLinks();

				if (statusCode == ErrorCode.ERROR_TXN_LINK_CREATE_SUCCESS) {
					if (txn2Id > 0) {
						var oldBalance = MyDouble.convertStringToDouble(txn2Object.getTxnCurrentBalanceAmount());

						if (transactionLinksModel.getTxnLinksAmount() <= oldBalance) {
							var newCurrentBalance = MyDouble.convertStringToDouble(oldBalance) - MyDouble.convertStringToDouble(newAmount);

							statusCode = currentObj.updateLinkedTransactionForB2B(txn2Object, newCurrentBalance);

							if (statusCode == ErrorCode.ERROR_TXN_LINKED_TRANSACTION_UPDATE_SUCCESS) {
								statusCode = ErrorCode.ERROR_TXN_LINK_CREATE_SUCCESS;
							} else {
								statusCode = ErrorCode.ERROR_TXN_LINK_CREATE_FAILED;

								return statusCode;
							}
						} else {
							statusCode = ErrorCode.ERROR_TXN_LINK_CREATE_FAILED;

							return statusCode;
						}
					}
				} else {
					return statusCode;
				}
			});
		}

		return statusCode;
	};

	function addUdfValues(extraFieldObjectArray, txnId) {
		var statusCode = ErrorCode.ERROR_TXN_SAVE_SUCCESS;
		if (extraFieldObjectArray && extraFieldObjectArray.length) {
			for (var i = 0; i < extraFieldObjectArray.length; i++) {
				var extraFieldObject = extraFieldObjectArray[i];
				if (extraFieldObject.getUdfFieldValue()) {
					extraFieldObject.setUdfRefId(txnId);
					statusCode = extraFieldObject.updateUdfValues();
					if (statusCode != ErrorCode.ERROR_UDF_SAVE_SUCCESS) {
						break;
					}
				}
			}
		}
		if (statusCode == ErrorCode.ERROR_UDF_SAVE_SUCCESS) {
			statusCode = ErrorCode.ERROR_TXN_SAVE_SUCCESS;
		}
		return statusCode;
	}

	this.addTransaction = function () {
		var statusCode = ErrorCode['FAILED'];
		var transactionmanager = new TransactionManager();
		var settingCache = new SettingCache();
		var settingsModelForInclusiveTax = null;

		var isTransactionBeginSuccess = transactionmanager.BeginTransaction();

		if (isTransactionBeginSuccess) {
			try {
				var txnModel = new TransactionModel();
				txnModel.set_txn_description(this.getDescription());
				txnModel.set_txn_name_id(this.getNameId());
				txnModel.set_image_path(this.getImagePath());
				txnModel.set_cash_amount(this.getCashAmount());
				txnModel.set_balance_amount(this.getBalanceAmount());
				txnModel.set_txn_type(this.getTxnType());
				txnModel.set_ac1_amount(this.getAc1Amount());
				txnModel.set_ac2_amount(this.getAc2Amount());
				txnModel.set_ac3_amount(this.getAc3Amount());
				txnModel.set_txn_date(this.getTxnDate());
				txnModel.set_txn_due_date(this.getTxnDueDate() ? this.getTxnDueDate() : new Date());
				txnModel.set_discount_percent(this.getDiscountPercent());
				txnModel.set_discount_amount(this.getDiscountAmount());
				txnModel.set_tax_percent(this.getTaxPercent());
				txnModel.set_tax_amount(this.getTaxAmount());
				txnModel.set_txn_ref_no(this.getTxnRefNumber());
				txnModel.setPaymentTypeId(this.getPaymentTypeId());
				txnModel.setPaymentTypeReference(this.getPaymentTypeReference());
				txnModel.set_txn_status(this.getStatus());
				txnModel.set_firm_id(this.getFirmId());
				txnModel.setInvoicePrefixId(this.getInvoicePrefixId());
				txnModel.set_txn_sub_type(this.getTxnSubType());
				txnModel.set_transaction_tax_id(this.getTransactionTaxId());
				txnModel.set_custom_fields(this.getCustomFields());
				txnModel.set_display_name(this.getDisplayName());
				txnModel.set_reverse_charge(this.getReverseCharge());
				txnModel.setPlaceOfSupply(this.getPlaceOfSupply());
				txnModel.setRoundOffValue(this.getRoundOffValue());
				txnModel.setTxnITCApplicable(this.getTxnITCApplicable());
				txnModel.setPODate(this.getPODate());
				txnModel.setPONumber(this.getPONumber());
				txnModel.setTxnReturnRefNumber(this.getTxnReturnRefNumber());
				txnModel.setTxnReturnDate(this.getTxnReturnDate());
				txnModel.setEwayBillNumber(this.getEwayBillNumber());
				txnModel.setTxnCurrentBalanceAmount(this.getTxnCurrentBalanceAmount());
				txnModel.setTxnPaymentStatus(this.getTxnPaymentStatus());
				txnModel.setTxnPaymentTermId(this.getTxnPaymentTermId());
				txnModel.setUdfObjectArray(this.getUdfObjectArray());
				txnModel.setBillingAddress(this.getBillingAddress());
				txnModel.setShippingAddress(this.getShippingAddress());
				txnModel.setTaxTypeInclusive(this.getTaxTypeInclusive());

				statusCode = txnModel.addTransaction();

				if (statusCode == ErrorCode.ERROR_TXN_SAVE_SUCCESS) {
					this.setTxnId(txnModel.get_txn_id());
				}

				if (statusCode == ErrorCode.ERROR_TXN_SAVE_SUCCESS && settingCache.isBillToBillEnabled() && this.getSelectedTxnMapForB2B() != null) {
					statusCode = this.createTransactionLinks();

					if (statusCode == ErrorCode.ERROR_TXN_LINK_CREATE_SUCCESS) {
						statusCode = ErrorCode.ERROR_TXN_SAVE_SUCCESS;
					} else {
						statusCode = ErrorCode.ERROR_TXN_SAVE_FAILED;
					}
				}

				if (statusCode == ErrorCode.ERROR_TXN_SAVE_SUCCESS) {
					var lineItemsInTxn = this.lineItems;
					if (lineItemsInTxn && lineItemsInTxn.length && lineItemsInTxn.length > 0) {
						for (var i in lineItemsInTxn) {
							var baselineitem = lineItemsInTxn[i];
							baselineitem.setTransactionId(this.txnId);
							baselineitem.setTxnTaxTypeInclusive(this.getTaxTypeInclusive());
							statusCode = baselineitem.addLineItems(this.getTxnType(), this.getNameId());
							if (statusCode != ErrorCode.ERROR_TXN_SAVE_SUCCESS) {
								break;
							}
						}
					}

					if (statusCode == ErrorCode.ERROR_TXN_SAVE_SUCCESS) {
						statusCode = addUdfValues(this.getUdfObjectArray(), this.getTxnId());
					}
					if (statusCode == ErrorCode['ERROR_TXN_SAVE_SUCCESS']) {
						statusCode = this.getNameRef().updateNameBalance(this, null);
						if (statusCode == ErrorCode['ERROR_NAME_SAVE_SUCCESS']) {
							statusCode = ErrorCode['ERROR_TXN_SAVE_SUCCESS'];
						}
					}
					// Cheque entry if cheque type is selected
					if (statusCode == ErrorCode['ERROR_TXN_SAVE_SUCCESS'] && Number(this.getPaymentTypeId()) === 2 && this.getCashAmount() > 0) {
						var chequelogic = new ChequeLogic();
						chequelogic.setChequeTxnId(this.getTxnId());
						statusCode = chequelogic.addCheque();
						if (statusCode == ErrorCode.ERROR_CHEQUE_SAVE_SUCCESS) {
							statusCode = ErrorCode.ERROR_TXN_SAVE_SUCCESS;
						}
					}
				}
				debugger;
				// update tax type inclusive setting based on txn
				if (statusCode == ErrorCode.ERROR_TXN_SAVE_SUCCESS) {
					var SettingsModel = require('./../Models/SettingsModel.js');
					if ([TxnTypeConstant.TXN_TYPE_PURCHASE, TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER].includes(this.getTxnType())) {
						settingsModelForInclusiveTax = new SettingsModel();
						settingsModelForInclusiveTax.setSettingKey(Queries.SETTING_INCLUSIVE_TAX_ON_INWARD_TXN);
						statusCode = settingsModelForInclusiveTax.UpdateSetting(this.getTaxTypeInclusive());
					} else if ([TxnTypeConstant.TXN_TYPE_SALE, TxnTypeConstant.TXN_TYPE_SALE_ORDER, TxnTypeConstant.TXN_TYPE_ESTIMATE, TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN].includes(this.getTxnType())) {
						settingsModelForInclusiveTax = new SettingsModel();
						settingsModelForInclusiveTax.setSettingKey(Queries.SETTING_INCLUSIVE_TAX_ON_OUTWARD_TXN);
						statusCode = settingsModelForInclusiveTax.UpdateSetting(this.getTaxTypeInclusive());
					}
					if (statusCode == ErrorCode.ERROR_SETTING_SAVE_FAILED) {
						statusCode = ErrorCode.ERROR_TXN_SAVE_FAILED;
					} else {
						statusCode = ErrorCode.ERROR_TXN_SAVE_SUCCESS;
					}
				}

				if (statusCode == ErrorCode['ERROR_TXN_SAVE_SUCCESS']) {
					try {
						txnModel.updateFTSTxnRecord(this.getLineItems());
					} catch (err) {
						logger.error('BaseTransaction update ftxtxnn record error : ' + err);
					}

					if (!transactionmanager.CommitTransaction()) {
						statusCode = ErrorCode.ERROR_TXN_SAVE_FAILED;
					}
				}
			} catch (err) {
				logger.error('Unhndled exception ==> ' + err);
				statusCode = ErrorCode.ERROR_TXN_SAVE_FAILED;
			}

			transactionmanager.EndTransaction();
		} else {
			statusCode = ErrorCode.ERROR_TXN_SAVE_FAILED;
		}

		if (statusCode != ErrorCode.ERROR_TXN_SAVE_SUCCESS) {
			var nameCache = new NameCache();
			settingCache = new SettingCache();
			var itemCache = new ItemCache();
			nameCache.reloadNameCache();
			itemCache.reloadItemCache();
			settingCache.reloadSettingCache();
		}
		return statusCode;
	};

	this.deleteLineItems = function () {
		var statusCode = ErrorCode.ERROR_TXN_DELETE_SUCCESS;
		var lineItemList = this.getLineItems();
		var lineItemLen = lineItemList.length;
		if (lineItemLen > 0) {
			for (var i = 0; i < lineItemLen; i++) {
				var lineItem = lineItemList[i];

				if ((this.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || this.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || this.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || this.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN) && lineItem.getLineItemIstId() > 0) {
					var itemStockTracking = new ItemStockTrackingModel();
					var itemStockTrackingData = itemStockTracking.getItemStockTrackingRecordsByIstId(lineItem.getLineItemIstId());

					if (itemStockTrackingData) {
						var oldQuantity = itemStockTrackingData.getIstCurrentQuantity();

						if (this.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || this.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
							statusCode = itemStockTracking.changeISTQuantity(lineItem.getLineItemIstId(), Number(oldQuantity) + Number(lineItem.getTotalQuantityIncludingFreeQuantity()), false, false);
						} else {
							statusCode = itemStockTracking.changeISTQuantity(lineItem.getLineItemIstId(), Number(oldQuantity) - Number(lineItem.getTotalQuantityIncludingFreeQuantity()), false, false);
						}

						if (statusCode == ErrorCode.ERROR_ITEM_STOCK_QUANITY_UPDATE_SUCCESS) {
							statusCode = ErrorCode.ERROR_TXN_DELETE_SUCCESS;
						}
					}
				}

				statusCode = lineItem.deleteLineItem();
				if (statusCode == ErrorCode.SUCCESS) {
					var itemCache = new ItemCache();
					var item = itemCache.getItemById(lineItem.getItemId());
					console.log(item);
					statusCode = item.deleteItemStockQuantity(this.getTxnType(), lineItem.getTotalQuantityIncludingFreeQuantity(), this.getStatus());
					if (statusCode != ErrorCode.ERROR_ITEM_SAVE_SUCCESS) {
						statusCode = ErrorCode.ERROR_TXN_DELETE_FAILED;
					} else {
						statusCode = ErrorCode.ERROR_TXN_DELETE_SUCCESS;
					}
				} else {
					statusCode = ErrorCode.ERROR_TXN_DELETE_FAILED;
					break;
				}
			}
		}
		return statusCode;
	};

	this.updateTransactions = function (newTransactionModel) {
		newTransactionModel.set_txn_id(this.getTxnId());
		newTransactionModel.set_txn_type(this.getTxnType());
		newTransactionModel.set_txn_date(this.getTxnDate());
		newTransactionModel.set_txn_due_date(this.getTxnDueDate());
		newTransactionModel.set_balance_amount(this.getBalanceAmount());
		newTransactionModel.set_cash_amount(this.getCashAmount());
		newTransactionModel.set_txn_description(this.getDescription());
		newTransactionModel.set_image_path(this.getImagePath());
		newTransactionModel.set_image_id(this.getImageId());
		newTransactionModel.set_txn_name_id(this.getNameRef().getNameId());
		newTransactionModel.set_tax_percent(this.getTaxPercent());
		newTransactionModel.set_tax_amount(this.getTaxAmount());
		newTransactionModel.set_discount_percent(this.getDiscountPercent());
		newTransactionModel.set_discount_amount(this.getDiscountAmount());
		newTransactionModel.set_txn_ref_no(this.getTxnRefNumber());
		newTransactionModel.set_firm_id(this.getFirmId());
		newTransactionModel.setPaymentTypeId(this.getPaymentTypeId());
		newTransactionModel.setPaymentTypeReference(this.getPaymentTypeReference());
		newTransactionModel.set_txn_status(this.getStatus());
		newTransactionModel.setInvoicePrefixId(this.getInvoicePrefixId());
		newTransactionModel.set_txn_sub_type(this.getTxnSubType());
		newTransactionModel.set_transaction_tax_id(this.getTransactionTaxId());
		newTransactionModel.set_ac1_amount(this.getAc1Amount());
		newTransactionModel.set_ac2_amount(this.getAc2Amount());
		newTransactionModel.set_ac3_amount(this.getAc3Amount());
		newTransactionModel.set_custom_fields(this.getCustomFields());
		newTransactionModel.set_display_name(this.getDisplayName());
		newTransactionModel.set_reverse_charge(this.getReverseCharge());
		newTransactionModel.setPlaceOfSupply(this.getPlaceOfSupply());
		newTransactionModel.setRoundOffValue(this.getRoundOffValue());
		newTransactionModel.setTxnITCApplicable(this.getTxnITCApplicable());
		newTransactionModel.setPODate(this.getPODate());
		newTransactionModel.setPONumber(this.getPONumber());
		newTransactionModel.setTxnReturnRefNumber(this.getTxnReturnRefNumber());
		newTransactionModel.setTxnReturnDate(this.getTxnReturnDate());
		newTransactionModel.setEwayBillNumber(this.getEwayBillNumber());
		newTransactionModel.setTxnCurrentBalanceAmount(this.getTxnCurrentBalanceAmount());
		newTransactionModel.setTxnPaymentStatus(this.getTxnPaymentStatus());
		newTransactionModel.setTxnPaymentTermId(this.getTxnPaymentTermId());
		newTransactionModel.setBillingAddress(this.getBillingAddress());
		newTransactionModel.setShippingAddress(this.getShippingAddress());
		newTransactionModel.setTaxTypeInclusive(this.getTaxTypeInclusive());
		return newTransactionModel.updateTransaction();
	};

	this.updateTransaction = function (oldTxnObject) {
		var statusCode = ErrorCode.FAILED;
		var transactionmanager = new TransactionManager();

		var isTransactionBeginSuccess = transactionmanager.BeginTransaction();

		if (isTransactionBeginSuccess) {
			try {
				var newTransactionModel = new TransactionModel();

				statusCode = this.updateTransactions(newTransactionModel);

				if (statusCode == ErrorCode.ERROR_TXN_SAVE_SUCCESS && this.getSelectedTxnMapForB2B()) {
					statusCode = this.updateTransactionLinks();

					if (statusCode == ErrorCode.ERROR_TXN_LINK_UPDATE_SUCCESS) {
						statusCode = ErrorCode.ERROR_TXN_SAVE_SUCCESS;
					} else {
						statusCode = ErrorCode.ERROR_TXN_SAVE_FAILED;
					}
				}

				if (statusCode == ErrorCode.ERROR_TXN_SAVE_SUCCESS) {
					statusCode = oldTxnObject.deleteLineItems();
					if (statusCode == ErrorCode.ERROR_TXN_DELETE_SUCCESS) {
						statusCode = ErrorCode.ERROR_TXN_SAVE_SUCCESS;
						var lineItemList = this.getLineItems();
						var lineItemLen = lineItemList.length;
						if (lineItemLen > 0) {
							for (var i = 0; i < lineItemLen; i++) {
								var lineItem = lineItemList[i];
								if (lineItem.getLineItemId() == 0) {
									lineItem.setTransactionId(this.txnId);
									lineItem.setTxnTaxTypeInclusive(this.getTaxTypeInclusive());
									statusCode = lineItem.addLineItems(this.getTxnType(), this.getNameRef().getNameId(), true);
								}
								if (statusCode != ErrorCode.ERROR_TXN_SAVE_SUCCESS) {
									break;
								}
							}
						}
						if (statusCode == ErrorCode.ERROR_TXN_SAVE_SUCCESS && (this.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || this.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN || this.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || this.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN)) {
							var oldLineitems = oldTxnObject.getLineItems();
							if (oldLineitems.length > 0) {
								var settingCache = new SettingCache();
								if (settingCache.isAnyAdditionalItemDetailsEnabled()) {
									var oldLineItemItemIds = [];
									for (var _i = 0; _i < oldLineitems.length; _i++) {
										oldLineItemItemIds.push(oldLineitems[_i].getItemId());
									}
									statusCode = newTransactionModel.deleteUnUsedIST(oldLineItemItemIds);
								}
							}
						}
					} else {
						statusCode = ErrorCode.ERROR_TXN_SAVE_FAILED;
					}
					if (statusCode == ErrorCode.ERROR_TXN_SAVE_SUCCESS) {
						statusCode = this.getNameRef().updateNameBalance(this, oldTxnObject);
						if (statusCode == ErrorCode.ERROR_NAME_SAVE_SUCCESS) {
							statusCode = ErrorCode.ERROR_TXN_SAVE_SUCCESS;
						}
					}
					if (statusCode == ErrorCode.ERROR_TXN_SAVE_SUCCESS) {
						statusCode = this.deleteCheque();
						if (statusCode == ErrorCode.ERROR_CHEQUE_DELETE_SUCCESS) {
							statusCode = ErrorCode.ERROR_TXN_SAVE_SUCCESS;
						}

						if (statusCode == ErrorCode.ERROR_TXN_SAVE_SUCCESS && Number(this.getPaymentTypeId()) === 2 && this.getCashAmount() > 0) {
							var chequelogic = new ChequeLogic();
							chequelogic.setChequeTxnId(this.getTxnId());
							statusCode = chequelogic.addCheque();
							if (statusCode == ErrorCode.ERROR_CHEQUE_SAVE_SUCCESS) {
								statusCode = ErrorCode.ERROR_TXN_SAVE_SUCCESS;
							}
						}
					}

					if (statusCode == ErrorCode.ERROR_TXN_SAVE_SUCCESS) {
						if (oldTxnObject && oldTxnObject.getUdfObjectArray() && oldTxnObject.getUdfObjectArray().length) {
							var udfValueModel = new UDFValueModel();
							var fieldType = UDFFieldsConstant.FIELD_TYPE_TRANSACTION;
							statusCode = udfValueModel.deleteExtraFieldsValue(this.getTxnId(), fieldType);
							if (statusCode == ErrorCode.ERROR_UDF_VALUE_DELETE_SUCCESSFUL) {
								statusCode = ErrorCode.ERROR_TXN_SAVE_SUCCESS;
							}
						}
						if (statusCode == ErrorCode.ERROR_TXN_SAVE_SUCCESS) {
							statusCode = addUdfValues(this.getUdfObjectArray(), this.getTxnId());
						}
					}
				}
				if (statusCode == ErrorCode.ERROR_TXN_SAVE_SUCCESS) {
					try {
						newTransactionModel.updateFTSTxnRecord(this.getLineItems());
					} catch (err) {
						logger.error('BaseTransaction update ftxtxnn record error : ' + err);
					}

					if (!transactionmanager.CommitTransaction()) {
						statusCode = ErrorCode.ERROR_TXN_SAVE_FAILED;
					}
				}
			} catch (err) {
				logger.error('Update Transaction failed: ' + err);
				statusCode = ErrorCode.ERROR_TXN_SAVE_FAILED;
			}
			transactionmanager.EndTransaction();
		} else {
			statusCode = ErrorCode.ERROR_TXN_SAVE_FAILED;
		}

		if (statusCode != ErrorCode.ERROR_TXN_SAVE_SUCCESS) {
			var itemCache = new ItemCache();
			var nameCache = new NameCache();
			var _settingCache = new SettingCache();
			itemCache.reloadItemCache();
			nameCache.reloadNameCache();
			_settingCache.reloadSettingCache();
		}
		return statusCode;
	};

	this.validateTotalAmount = function (amount) {
		var statusCode = 'SUCCESS';
		if (amount === 0) {
			return statusCode;
		}
		if (!amount) {
			statusCode = ErrorCode['ERROR_TXN_TOTAL_EMPTY'];
		}
		if (isNaN(Number(amount))) {
			statusCode = ErrorCode['ERROR_TXN_INVALID_AMOUNT'];
		}
		return statusCode;
	};

	this.validateAmount = function (amount) {
		var statusCode = ErrorCode.SUCCESS;
		if (!amount) {
			return statusCode;
		}
		if (isNaN(amount)) {
			statusCode = ErrorCode.ERROR_TXN_INVALID_AMOUNT;
		}
		return statusCode;
	};

	this.StockAlertDialogue = function (txnObject) {
		this.saveTransaction(txnObject);
	};

	this.getSubTotalAmount = function () {
		var subTotalAmount = 0.0;
		var lineItemList = this.getLineItems();
		var lineItemLen = lineItemList.length;
		if (lineItemLen > 0) {
			$.each(lineItemList, function (key, lineItem) {
				subTotalAmount += Number(lineItem.getLineItemTotal());
			});
		}
		return subTotalAmount;
	};

	this.addLineItem = function (lineItemUIData) {
		var statusCode = ErrorCode.SUCCESS;
		var itemcache = new ItemCache();
		var lineItem = new BaseLineItem();
		lineItem.setItemName(lineItemUIData['name']);
		lineItem.setItemId(itemcache.getItemIdByNameAndTxn(this.getTxnType(), lineItemUIData['name']));
		statusCode = this.validateAmount(lineItemUIData['quantity']);
		if (statusCode == ErrorCode.SUCCESS) {
			statusCode = this.validateAmount(lineItemUIData['unitprice']);
		}
		if (statusCode == ErrorCode.SUCCESS) {
			statusCode = this.validateAmount(lineItemUIData['totalamount']);
		}
		debugger;
		if (statusCode == ErrorCode.SUCCESS) {
			lineItem.setLineItemMRP(lineItemUIData['mrp']);
			lineItem.setLineItemSize(lineItemUIData['size']);
			lineItem.setLineItemBatchNumber(lineItemUIData['batchno']);
			lineItem.setLineItemExpiryDate(lineItemUIData['expirydate']);
			lineItem.setLineItemManufacturingDate(lineItemUIData['manufacturingdate']);
			lineItem.setLineItemSerialNumber(lineItemUIData['serialnumber']);
			lineItem.setLineItemCount(lineItemUIData['count']);
			lineItem.setLineItemDescription(lineItemUIData['description']);
			lineItem.setItemQuantity(lineItemUIData['quantity']);
			lineItem.setItemFreeQuantity(lineItemUIData['freequantity']);
			lineItem.setItemUnitPrice(lineItemUIData['unitprice']);
			lineItem.setLineItemTotal(lineItemUIData['totalamount']);
			lineItem.setLineItemTaxAmount(lineItemUIData['taxamount']);
			lineItem.setLineItemDiscountAmount(lineItemUIData['discountamount']);
			lineItem.setLineItemUnitId(lineItemUIData['unitid']);
			lineItem.setLineItemUnitMappingId(lineItemUIData['mappingid']);
			lineItem.setLineItemTaxId(lineItemUIData['taxid']);
			lineItem.setLineItemDescription(lineItemUIData['description']);
			lineItem.setLineItemAdditionalCESS(lineItemUIData['cessamount']);
			lineItem.setLineItemITCValue(lineItemUIData['itcvalue']);
			lineItem.setLineItemDiscountPercent(lineItemUIData['discountpercent']);
			lineItem.setUnitPriceFromUI(lineItemUIData['unitPriceFromUI']);
		}
		if (this.getLineItems() == null) {
			this.lineItems = [];
		}
		this.lineItems.push(lineItem);
		return statusCode;
	};

	this.getTransactionFromId = function (value) {
		var dataloader = new DataLoader();
		var txnObject = dataloader.LoadTransactionFromId(value);
		return txnObject;
	};

	this.deleteTransactionsForName = function (nameRef) {
		var statusCode = ErrorCode.ERROR_TXN_DELETE_FAILED;
		var transactionManager = new TransactionManager();
		var transactionModel = new TransactionModel();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_TXN_DELETE_FAILED;
			return statusCode;
		}

		try {
			if (transactionModel.deleteTransactionForNameId(nameRef.getNameId())) {
				statusCode = ErrorCode.ERROR_TXN_DELETE_SUCCESS;
			}
		} catch (err) {
			console.log(err);
		}

		if (statusCode == ErrorCode.ERROR_TXN_DELETE_SUCCESS) {
			if (!transactionManager.CommitTransaction()) {
				statusCode = ErrorCode.ERROR_TXN_DELETE_FAILED;
			}
		}
		transactionManager.EndTransaction();
		return statusCode;
	};

	this.canEditOrDeleteTransaction = function (txnId) {
		var transactionModel = new TransactionModel();
		transactionModel.set_txn_id(txnId);
		return transactionModel.canEditOrDeleteTransaction();
	};

	this.canDeleteTransactionForTxn = function (txnId) {
		return this.canEditOrDeleteTransaction(txnId);
	};

	this.canDeleteTransaction = function () {
		return this.canDeleteTransactionForTxn(this.getTxnId());
	};

	this.canEditTransaction = function () {
		var statusCode = ErrorCode.FAILED;
		if ((this.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || this.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE || this.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || this.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) && this.getStatus() == TransactionStatus.TXN_ORDER_COMPLETE) {
			statusCode = ErrorCode.ERROR_TRANSACTION_CANT_EDIT_ORDER_CLOSED;
		} else {
			if (this.canEditTransactionForTxn(this.getTxnId())) {
				statusCode = ErrorCode.SUCCESS;
			} else {
				statusCode = ErrorCode.ERROR_TRANSACTION_CANT_EDIT_CLOSE_CHQUE;
			}
		}
		return statusCode;
	};

	this.canEditTransactionForTxn = function (txnId) {
		return this.canEditOrDeleteTransaction(txnId);
	};

	this.deleteCheque = function () {
		var cheque = new ChequeLogic();
		cheque.setChequeTxnId(this.getTxnId());
		return cheque.deleteCheque();
	};

	this.deleteTransaction = function () {
		var statusCode = ErrorCode.ERROR_TXN_DELETE_FAILED;
		var transactionManager = new TransactionManager();
		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (isTransactionBeginSuccess) {
			try {
				var canDelete = this.canDeleteTransaction();
				if (canDelete) {
					statusCode = this.deleteLineItems();
					if (statusCode == ErrorCode.ERROR_TXN_DELETE_SUCCESS) {
						var txnModel = new TransactionModel();
						txnModel.set_txn_id(this.getTxnId());
						txnModel.set_txn_type(this.getTxnType());
						txnModel.set_txn_sub_type(this.getTxnSubType());
						txnModel.set_firm_id(this.getFirmId());
						txnModel.set_image_id(this.getImageId());
						statusCode = this.deleteCheque();
						if (statusCode == ErrorCode.ERROR_CHEQUE_DELETE_SUCCESS) {
							statusCode = ErrorCode.ERROR_TXN_DELETE_SUCCESS;
						}

						if (statusCode == ErrorCode.ERROR_TXN_DELETE_SUCCESS) {
							statusCode = this.deleteTransactionLinks(this.getTxnId(), true);

							if (statusCode == ErrorCode.ERROR_TXN_LINK_DELETE_SUCCESS) {
								statusCode = ErrorCode.ERROR_TXN_DELETE_SUCCESS;
							}
						}
						if (statusCode == ErrorCode.ERROR_TXN_DELETE_SUCCESS && (this.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || this.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN)) {
							statusCode = txnModel.deleteLinkedTransactions();
							if (statusCode == ErrorCode.ERROR_TXN_LINK_DELETE_SUCCESS) {
								statusCode = ErrorCode.ERROR_TXN_DELETE_SUCCESS;
							}
						}
						if (statusCode == ErrorCode.ERROR_TXN_DELETE_SUCCESS) {
							if (this.getUdfObjectArray() && this.getUdfObjectArray().length) {
								var udfValueModel = new UDFValueModel();
								var fieldType = UDFFieldsConstant.FIELD_TYPE_TRANSACTION;
								statusCode = udfValueModel.deleteExtraFieldsValue(this.getTxnId(), fieldType);
								if (statusCode == ErrorCode.ERROR_UDF_VALUE_DELETE_SUCCESSFUL) {
									statusCode = ErrorCode.ERROR_TXN_DELETE_SUCCESS;
								}
							}
						}

						if (statusCode == ErrorCode.ERROR_TXN_DELETE_SUCCESS) {
							if (txnModel.deleteTransaction()) {
								statusCode = this.getNameRef().updateNameBalance(null, this);
								if (statusCode == ErrorCode.ERROR_NAME_SAVE_SUCCESS) {
									statusCode = ErrorCode.ERROR_TXN_DELETE_SUCCESS;
								}
							} else {
								statusCode = ErrorCode.ERROR_TXN_DELETE_FAILED;
							}
						}
					}
				} else {
					statusCode = ErrorCode.ERROR_TRANSACTION_CANT_DELETE_CLOSE_CHQUE;
				}
			} catch (err) {
				console.log(err);
				statusCode = ErrorCode.ERROR_TXN_DELETE_FAILED;
			}

			if (statusCode == ErrorCode.ERROR_TXN_DELETE_SUCCESS) {
				if (!transactionManager.CommitTransaction()) {
					statusCode = ErrorCode.ERROR_TXN_DELETE_FAILED;
				}
			}
			transactionManager.EndTransaction();
		} else {
			statusCode = ErrorCode.ERROR_TXN_DELETE_FAILED;
		}

		if (statusCode != ErrorCode.ERROR_TXN_DELETE_SUCCESS) {
			var itemCache = new ItemCache();
			var nameCache = new NameCache();
			var settingsCache = new SettingCache();
			itemCache.reloadItemCache();
			nameCache.reloadNameCache();
			settingsCache.reloadSettingCache();
		}

		return statusCode;
	};

	this.getFullTxnRefNumber = function () {
		var refNo = '';
		if (this.getTxnRefNumber()) {
			if (this.getInvoicePrefix()) {
				refNo = this.getInvoicePrefix();
			}
			refNo += this.getTxnRefNumber();
		}
		return refNo;
	};

	this.getTxnRefNumber = function () {
		return '';
	};

	this.getReverseChargeAmount = function () {
		if ((this.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || this.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) && Number(this.getReverseCharge())) {
			var reverseChargeAmount = MyDouble.convertStringToDouble(this.getTaxAmount(), true);
			var lineItemList = this.getLineItems();
			var lineItemLen = lineItemList.length;
			if (lineItemLen && lineItemLen > 0) {
				$.each(lineItemList, function (key, lineItem) {
					reverseChargeAmount += MyDouble.convertStringToDouble(lineItem.getLineItemTaxAmount(), true);
					reverseChargeAmount += MyDouble.convertStringToDouble(lineItem.getLineItemAdditionalCESS(), true);
				});
			}
			return reverseChargeAmount;
		}
		return 0;
	};
};

module.exports = BaseTransaction;