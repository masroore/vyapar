var DiscountReportObjectModel = function DiscountReportObjectModel() {
	this.partyName = 0;
	this.saleDiscount = 0;
	this.purchaseDiscount = 0;

	this.getPartyName = function () {
		return this.partyName;
	};

	this.setPartyName = function (partyName) {
		this.partyName = partyName;
	};

	this.getSaleDiscount = function () {
		return this.saleDiscount;
	};

	this.setSaleDiscount = function (saleDiscount) {
		this.saleDiscount = saleDiscount;
	};

	this.getPurchaseDiscount = function () {
		return this.purchaseDiscount;
	};

	this.setPurchaseDiscount = function (purchaseDiscount) {
		this.purchaseDiscount = purchaseDiscount;
	};
};

module.exports = DiscountReportObjectModel;