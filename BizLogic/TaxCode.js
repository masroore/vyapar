var TaxCodeModel = require('./../Models/TaxCodeModel.js');
var ErrorCode = require('./../Constants/ErrorCode.js');
var TaxCodeConstants = require('./../Constants/TaxCodeConstants.js');
var TransactionManager = require('./../DBManager/TransactionManager.js');
var TaxCode = function TaxCode() {
	this.taxCodeId;
	this.taxCodeName;
	this.taxRate;
	this.taxType;
	this.taxDateCreated;
	this.taxDateModified;

	this.getTaxRateType = function () {
		return this.taxRateType;
	};
	this.setTaxRateType = function (taxRateType) {
		this.taxRateType = taxRateType;
	};

	this.getTaxCodeId = function () {
		return this.taxCodeId;
	};
	this.setTaxCodeId = function (taxCodeId) {
		this.taxCodeId = taxCodeId;
	};

	this.getTaxCodeName = function () {
		return this.taxCodeName;
	};
	this.setTaxCodeName = function (taxCodeName) {
		this.taxCodeName = taxCodeName;
	};

	this.getTaxRate = function () {
		return this.taxRate;
	};
	this.setTaxRate = function (taxRate) {
		this.taxRate = taxRate;
	};

	this.getTaxType = function () {
		return this.taxType;
	};
	this.setTaxType = function (taxType) {
		this.taxType = taxType;
	};

	this.getTaxDateCreated = function () {
		return this.taxDateCreated;
	};
	this.setTaxDateCreated = function (taxDateCreated) {
		this.taxDateCreated = taxDateCreated;
	};

	this.getTaxDateModified = function () {
		return this.taxDateModified;
	};
	this.setTaxDateModified = function (taxDateModified) {
		this.taxDateModified = taxDateModified;
	};

	this.getTaxCodeMap = function () {
		return this.taxCodeMap;
	};
	this.setTaxCodeMap = function (taxCodeMap) {
		this.taxCodeMap = taxCodeMap;
	};

	this.addNewTaxRate = function () {
		var TaxCodeCache = require('./../Cache/TaxCodeCache.js');
		var taxCodeCache = new TaxCodeCache();
		var statusCode = ErrorCode.ERROR_TAX_CODE_NAME_SAVE_FAILED;
		if (taxCodeCache.isTaxCodeNameExists(this.getTaxCodeName(), null)) {
			statusCode = ErrorCode.ERROR_TAX_CODE_NAME_EXISTS;
			return statusCode;
		}
		var taxCodeModel = new TaxCodeModel();
		taxCodeModel.setTaxCodeName(this.getTaxCodeName());
		if (this.getTaxType() == TaxCodeConstants.taxGroup) {
			var taxRate = taxCodeCache.getRateForListOfIds(this.getTaxCodeMap());
			taxCodeModel.setTaxRate(taxRate);
		} else {
			taxCodeModel.setTaxRate(this.getTaxRate());
		}
		taxCodeModel.setTaxType(this.getTaxType());
		taxCodeModel.setTaxRateType(this.getTaxRateType());
		taxCodeModel.setTaxDateCreated(this.getTaxDateCreated());
		taxCodeModel.setTaxDateModified(this.getTaxDateModified());
		taxCodeModel.setTaxCodeMap(this.getTaxCodeMap());
		var transactionManager = new TransactionManager();
		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_TAX_CODE_NAME_SAVE_FAILED;
			return statusCode;
		}
		var taxCodeId = taxCodeModel.addNewTaxRate();
		if (taxCodeId > 0) {
			taxCodeCache.reloadTaxRateCache();
			statusCode = this.getTaxType() == TaxCodeConstants.taxRate ? ErrorCode.ERROR_TAX_CODE_NAME_SAVE_SUCCESS : ErrorCode.ERROR_TAX_GROUP_NAME_SAVE_SUCCESS;
			if (!transactionManager.CommitTransaction()) {
				statusCode = this.getTaxType() == TaxCodeConstants.taxRate ? ErrorCode.ERROR_TAX_CODE_NAME_SAVE_FAILED : ERROR_TAX_GROUP_NAME_SAVE_FAILED;
			}
			this.setTaxCodeId(taxCodeId);
		}
		transactionManager.EndTransaction();
		return statusCode;
	};

	this.updateTaxRate = function () {
		var statusCode = ErrorCode.ERROR_TAX_CODE_NAME_EDIT_FAILED;
		var TaxCodeCache = require('./../Cache/TaxCodeCache.js');
		var taxCodeCache = new TaxCodeCache();
		if (taxCodeCache.isTaxCodeNameExists(this.getTaxCodeName(), this.getTaxCodeId())) {
			statusCode = ErrorCode.ERROR_TAX_CODE_NAME_EXISTS;
			return statusCode;
		}
		var taxCodeModel = new TaxCodeModel();
		taxCodeModel.setTaxCodeName(this.getTaxCodeName());
		if (this.getTaxType() == TaxCodeConstants.taxGroup) {
			var taxRate = taxCodeCache.getRateForListOfIds(this.getTaxCodeMap());
			taxCodeModel.setTaxRate(taxRate);
		} else {
			taxCodeModel.setTaxRate(this.getTaxRate());
		}
		taxCodeModel.setTaxCodeId(this.getTaxCodeId());
		taxCodeModel.setTaxType(this.getTaxType());
		taxCodeModel.setTaxRateType(this.getTaxRateType());
		taxCodeModel.setTaxDateModified(this.getTaxDateModified());
		taxCodeModel.setTaxCodeMap(this.getTaxCodeMap());
		var transactionManager = new TransactionManager();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_TAX_CODE_NAME_EDIT_FAILED;
			return statusCode;
		}

		statusCode = taxCodeModel.updatesTaxRate();

		if (statusCode == ErrorCode.ERROR_TAX_CODE_NAME_EDIT_SUCCESS) {
			statusCode = this.getTaxType() == TaxCodeConstants.taxRate ? ErrorCode.ERROR_TAX_CODE_NAME_EDIT_SUCCESS : ErrorCode.ERROR_TAX_GROUP_NAME_EDIT_SUCCESS;
			if (!transactionManager.CommitTransaction()) {
				statusCode = this.getTaxType() == TaxCodeConstants.taxRate ? ErrorCode.ERROR_TAX_CODE_NAME_EDIT_FAILED : ErrorCode.ERROR_TAX_GROUP_NAME_EDIT_FAILED;
			} else {
				taxCodeCache.reloadTaxRateCache();
			}
		}
		transactionManager.EndTransaction();
		return statusCode;
	};

	this.canUpdateTaxRate = function () {
		var taxCodeModel = new TaxCodeModel();
		taxCodeModel.setTaxCodeId(this.getTaxCodeId());
		taxCodeModel.setTaxType(this.getTaxType());
		var statusCode = taxCodeModel.canUpdateTaxRate();
		if (statusCode == ErrorCode.ERROR_TAX_CAN_DELETE_EDIT) {
			var taxRateOrGroup = this.getTaxType() == TaxCodeConstants.taxRate ? 'tax rate' : 'tax group';
			var conf = confirm('Do you want to update this ' + taxRateOrGroup + '?');
			return !!conf;
		} else if (statusCode == ErrorCode.ERROR_TAX_RATE_EDIT_USED_IN_ITEM) {
			var conf = confirm(statusCode);
			return conf;
		} else {
			ToastHelper.error(statusCode);
			return false;
		}
	};

	this.deleteTaxRate = function () {
		var TaxCodeCache = require('./../Cache/TaxCodeCache.js');
		var taxCodeCache = new TaxCodeCache();
		var statusCode = ErrorCode.ERROR_TAX_CODE_NAME_DELETE_FAILED;
		var taxCodeModel = new TaxCodeModel();
		taxCodeModel.setTaxCodeId(this.getTaxCodeId());
		taxCodeModel.setTaxType(this.getTaxType());
		var transactionManager = new TransactionManager();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_TAX_CODE_NAME_DELETE_FAILED;
			return statusCode;
		}
		statusCode = taxCodeModel.deleteTaxRate();

		if (statusCode == ErrorCode.ERROR_TAX_CODE_NAME_DELETE_SUCCESS) {
			statusCode = this.getTaxType() == TaxCodeConstants.taxRate ? ErrorCode.ERROR_TAX_CODE_NAME_DELETE_SUCCESS : ErrorCode.ERROR_TAX_GROUP_NAME_DELETE_SUCCESS;
			if (!transactionManager.CommitTransaction()) {
				statusCode = this.getTaxType() == TaxCodeConstants.taxRate ? ErrorCode.ERROR_TAX_CODE_NAME_DELETE_FAILED : ErrorCode.ERROR_TAX_GROUP_NAME_DELETE_FAILED;
			} else {
				taxCodeCache.reloadTaxRateCache();
			}
		}
		transactionManager.EndTransaction();
		return statusCode;
	};

	this.canDeleteTaxRate = function () {
		var taxCodeModel = new TaxCodeModel();
		taxCodeModel.setTaxCodeId(this.getTaxCodeId());
		taxCodeModel.setTaxType(this.getTaxType());
		var statusCode = taxCodeModel.canDeleteTaxRate();
		if (statusCode == ErrorCode.ERROR_TAX_CAN_DELETE_EDIT) {
			var taxRateOrGroup = this.getTaxType() == TaxCodeConstants.taxRate ? 'tax rate' : 'tax group';
			var conf = confirm('Do you want to delete this ' + taxRateOrGroup + '?');
			return !!conf;
		} else {
			ToastHelper.error(statusCode);
			return false;
		}
	};
};

module.exports = TaxCode;