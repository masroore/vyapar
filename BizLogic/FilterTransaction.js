var _typeof2 = require('babel-runtime/helpers/typeof');

var _typeof3 = _interopRequireDefault(_typeof2);

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SettingCache = require('./../Cache/SettingCache.js');
var MyDouble = require('./../Utilities/MyDouble.js');
var TxnPaymentStatusConstants = require('./../Constants/TxnPaymentStatusConstants.js');
var FilterTransaction = {
	getTransactionList: function getTransactionList(txnId, txnList) {
		var paymentStatId = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;

		var settingCache = new SettingCache();
		var rtnTxnList = [];
		paymentStatId = Number(paymentStatId);
		if (txnList) {
			var len = txnList.length;
			for (var i = 0; i < len; i++) {
				var x = txnList[i];
				var paymentFilter = false;
				if (settingCache.isPaymentTermEnabled() && paymentStatId == TxnPaymentStatusConstants.OVERDUE) {
					// for due dates
					var dueByDays = MyDouble.getDueDays(x.getTxnDueDate());
					paymentFilter = dueByDays > 0 && Number(x.getTxnPaymentStatus()) != TxnPaymentStatusConstants.PAID;
				} else {
					paymentFilter = paymentStatId ? paymentStatId == Number(x.getTxnPaymentStatus()) : true;
				}
				if ((!Number(txnId) || Number(x.getTxnType()) == Number(txnId)) && paymentFilter) {
					rtnTxnList.push(x);
				}
			}
		}
		return rtnTxnList;
	},

	/**
   * function to get the list for search field
   * @param  {array} listOfDetails
   * @param  {string} [text='']
   * @param  {any} type
   * @return {array} array
   */
	filterDetails: function filterDetails(listOfDetails) {
		var text = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
		var filters = arguments[2];

		if (!text || !filters) {
			return (0, _values2.default)(listOfDetails);
		}
		var lowerCaseText = text.toLocaleLowerCase();
		var listOfFilters = [];
		if ((typeof filters === 'undefined' ? 'undefined' : (0, _typeof3.default)(filters)) != 'object') {
			listOfFilters.push(filters);
		} else {
			listOfFilters = filters;
		}

		return listOfDetails.filter(function (item) {
			for (var i = 0; i < listOfFilters.length; i++) {
				var element = item[listOfFilters[i]];
				if (element && element.toLowerCase().indexOf(lowerCaseText) > -1) {
					return true;
				}
			}
		});
	},
	filterObjDetails: function filterObjDetails(listOfDetailsObj) {
		var text = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
		var type = arguments[2];

		var listToReturn = {};
		$.each(listOfDetailsObj, function (key, value) {
			var curName = value[type].toLowerCase();
			if (curName.indexOf(text.toLowerCase()) > -1) {
				listToReturn[key] = value;
			}
		});
		return listToReturn;
	}

};

module.exports = FilterTransaction;