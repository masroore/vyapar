var PaymentInfo = function PaymentInfo() {
	this.accountNumber;
	this.bankName;
	this.id;
	this.type;
	this.name;
	this.currentBalance;
	this.openingBalance = 0.0;
	this.openingDate;

	var ErrorCode = require('./../Constants/ErrorCode.js');
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');

	this.setId = function (id) {
		this.id = id;
	};
	this.getId = function () {
		return this.id;
	};

	this.setName = function (name) {
		this.name = name;
	};
	this.getName = function () {
		return this.name;
	};

	this.setAccountNumber = function (accountNumber) {
		this.accountNumber = accountNumber;
	};
	this.getAccountNumber = function () {
		return this.accountNumber;
	};

	this.setBankName = function (bankName) {
		this.bankName = bankName;
	};
	this.getBankName = function () {
		return this.bankName;
	};

	this.setType = function (type) {
		this.type = type;
	};
	this.getType = function () {
		return this.type;
	};

	this.setCurrentBalance = function (openingBalance) {
		this.currentBalance = currentBalance;
	};
	this.getCurrentBalance = function () {
		return this.currentBalance;
	};

	this.setOpeningBalance = function (openingBalance) {
		this.openingBalance = openingBalance;
	};
	this.getOpeningBalance = function () {
		return this.openingBalance;
	};

	this.setOpeningDate = function (openingDate) {
		this.openingDate = openingDate;
	};
	this.getOpeningDate = function () {
		return this.openingDate;
	};

	this.saveNewInfo = function () {
		console.log(this);
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var transactionmanager = new TransactionManager();

		var isTransactionBeginSuccess = transactionmanager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_NEW_BANK_INFO_FAILED;
			return statusCode;
		}

		var PaymentInfoModel = require('./../Models/PaymentInfoModel.js');
		var paymentinfomodel = new PaymentInfoModel();
		paymentinfomodel.setName(this.getName());
		paymentinfomodel.setAccountNumber(this.getAccountNumber());
		paymentinfomodel.setOpeningBalance(this.getOpeningBalance());
		paymentinfomodel.setOpeningDate(this.getOpeningDate());
		paymentinfomodel.setBankName(this.getBankName());
		paymentinfomodel.setType(this.getType());
		var statusCode = paymentinfomodel.saveNewInfo();
		console.log(statusCode);
		if (statusCode == ErrorCode.ERROR_NEW_BANK_INFO_SUCCESS) {
			if (!transactionmanager.CommitTransaction()) {
				statusCode = ErrorCode.ERROR_NEW_BANK_INFO_FAILED;
			} else {
				var PaymentCache = require('./../Cache/PaymentInfoCache.js');
				var paymentCache = new PaymentCache();
				paymentCache.refreshPaymentInfoCache();
			}
		}
		transactionmanager.EndTransaction();
		return statusCode;
	};

	this.updateInfo = function () {
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var transactionmanager = new TransactionManager();

		var isTransactionBeginSuccess = transactionmanager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_NEW_BANK_INFO_FAILED;
			return statusCode;
		}

		var PaymentInfoModel = require('./../Models/PaymentInfoModel.js');
		var paymentinfomodel = new PaymentInfoModel();
		paymentinfomodel.setName(this.getName());
		paymentinfomodel.setAccountNumber(this.getAccountNumber());
		paymentinfomodel.setOpeningBalance(this.getOpeningBalance());
		paymentinfomodel.setOpeningDate(this.getOpeningDate());
		paymentinfomodel.setBankName(this.getBankName());
		paymentinfomodel.setType(this.getType());
		paymentinfomodel.setId(this.getId());
		var statusCode = paymentinfomodel.updateInfo();
		if (statusCode == ErrorCode.ERROR_UPDATE_BANK_INFO_SUCCESS) {
			if (!transactionmanager.CommitTransaction()) {
				statusCode = ErrorCode.ERROR_NEW_BANK_INFO_FAILED;
			}
		}
		transactionmanager.EndTransaction();
		return statusCode;
	};

	this.canDeletePaymentInfo = function () {
		var paymentInfoModel = new PaymentInfoModel();
		paymentInfoModel.setId(this.getId());
		return paymentInfoModel.canDeletePaymentInfo();
	};

	this.deletePaymentInfo = function () {
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var transactionManager = new TransactionManager();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_BANK_DELETE_FAILED;
			return statusCode;
		}

		var PaymentInfoModel = require('./../Models/PaymentInfoModel.js');
		var paymentInfoModel = new PaymentInfoModel();
		var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
		var paymentInfoCache = new PaymentInfoCache();
		paymentInfoModel.setId(this.getId());
		var statusCode = paymentInfoModel.deletePaymentInfo();

		if (statusCode == ErrorCode.ERROR_BANK_DELETE_SUCCESS) {
			if (!transactionManager.CommitTransaction()) {
				statusCode = ErrorCode.ERROR_BANK_DELETE_FAILED;
			} else {
				paymentInfoCache.refreshPaymentInfoCache();
			}
		}
		transactionManager.EndTransaction();
		return statusCode;
	};

	var calculateBalanceForwardBasedOnTxn = function calculateBalanceForwardBasedOnTxn(txnType, balanceForward, amount) {
		switch (txnType) {
			case TxnTypeConstant.TXN_TYPE_CASHIN:
			case TxnTypeConstant.TXN_TYPE_SALE:
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
			case TxnTypeConstant.TXN_TYPE_ESTIMATE:
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
			case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
			case TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD:
			case TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD_WITHOUT_CASH:
			case TxnTypeConstant.TXN_TYPE_BANK_TO_BANK: // For bank to bank transfer, amount is passed based on to_id or from_id
			case TxnTypeConstant.TXN_TYPE_BANK_OPENING:
			case TxnTypeConstant.TXN_TYPE_OTHER_INCOME:
			case TxnTypeConstant.TXN_TYPE_LOAN_STARTING_BALANCE:
			case TxnTypeConstant.TXN_TYPE_LOAN_CLOSE_BOOK_ADJ:
			case TxnTypeConstant.TXN_TYPE_LOAN_ADJUSTMENT:
				balanceForward += Number(amount);
				break;
			case TxnTypeConstant.TXN_TYPE_CASHOUT:
			case TxnTypeConstant.TXN_TYPE_PURCHASE:
			case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
			case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
			case TxnTypeConstant.TXN_TYPE_EXPENSE:
			case TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE:
			case TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE_WITHOUT_CASH:
			case TxnTypeConstant.TXN_TYPE_LOAN_EMI_PAYMENT:
			case TxnTypeConstant.TXN_TYPE_LOAN_PROCESSING_FEE:
				balanceForward -= Number(amount);
				break;
		}
		return balanceForward;
	};

	this.getBankStatementDetailObjectList = function (bankId, fromDate, toDate) {
		var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
		var paymentInfoCache = new PaymentInfoCache();
		var bankObject = paymentInfoCache.getPaymentInfoObjById(bankId);
		var bankDetailObjectListToBeReturn = [];
		if (bankObject) {
			var DataLoader = require('./../DBManager/DataLoader.js');
			var dataLoader = new DataLoader();
			var bankDetailObjectList = dataLoader.getBankDetailList(bankId);
			var balanceForward = 0;
			if (bankDetailObjectList && bankDetailObjectList.length > 0) {
				$.each(bankDetailObjectList, function (key, bankDetailModel) {
					if (bankDetailModel.getTxnDate().getTime() > toDate.getTime()) {
						// skip this transaction
					} else if (bankDetailModel.getTxnDate().getTime() < fromDate.getTime()) {
						switch (bankDetailModel.getTxnType()) {
							case TxnTypeConstant.TXN_TYPE_CHEQUE_TRANSFER:
								balanceForward = calculateBalanceForwardBasedOnTxn(bankDetailModel.getSubTxnType(), balanceForward, bankDetailModel.getAmount());
								break;
							case TxnTypeConstant.TXN_TYPE_BANK_TO_BANK:
								if (bankDetailModel.getFromBankId() == bankId) {
									balanceForward = calculateBalanceForwardBasedOnTxn(bankDetailModel.getTxnType(), balanceForward, -bankDetailModel.getAmount());
								} else {
									balanceForward = calculateBalanceForwardBasedOnTxn(bankDetailModel.getTxnType(), balanceForward, bankDetailModel.getAmount());
								}
								break;
							default:
								balanceForward = calculateBalanceForwardBasedOnTxn(bankDetailModel.getTxnType(), balanceForward, bankDetailModel.getAmount());
								break;
						}
					} else {
						bankDetailObjectListToBeReturn.push(bankDetailModel);
					}
				});

				if (bankDetailObjectListToBeReturn && bankDetailObjectListToBeReturn.length > 0) {
					bankDetailObjectListToBeReturn.sort(function (lhs, rhs) {
						var timeDiff = lhs.getTxnDate().getTime() - rhs.getTxnDate().getTime();
						if (!timeDiff) {
							timeDiff = lhs.getTxnModifiedDate().getTime() - rhs.getTxnModifiedDate().getTime();
						}
						return timeDiff;
					});
				}

				if (balanceForward != 0) {
					var BankDetailObject = require('./../BizLogic/BankDetail.js');
					var bankDetailObject = new BankDetailObject();
					bankDetailObject.setTxnId(-1);
					bankDetailObject.setDescription('Balance B/F');
					bankDetailObject.setTxnType(TxnTypeConstant.TXN_TYPE_BANK_BALANCE_FORWARD);
					bankDetailObject.setAmount(balanceForward);
					bankDetailObject.setTxnDate(fromDate);
					bankDetailObject.setUserId(null);
					bankDetailObject.setSubTxnType(null);
					bankDetailObject.setToBankId(null);
					bankDetailObject.setFromBankId(null);
					bankDetailObjectListToBeReturn.unshift(bankDetailObject);
				}
			}
		}
		return bankDetailObjectListToBeReturn;
	};
};

module.exports = PaymentInfo;