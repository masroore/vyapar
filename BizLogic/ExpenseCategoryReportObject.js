var ExpenseCategoryReportObject = function ExpenseCategoryReportObject() {
	this.categoryId;
	this.isCategoryLoanType = false;
	this.categoryName = '';
	this.amount = 0;

	this.getCategoryId = function () {
		return this.categoryId;
	};

	this.setCategoryId = function (categoryId) {
		this.categoryId = categoryId;
	};

	this.getIsCategoryLoanType = function () {
		return this.isCategoryLoanType;
	};

	this.setIsCategoryLoanType = function (isCategoryLoanType) {
		this.isCategoryLoanType = isCategoryLoanType;
	};

	this.getCategoryName = function () {
		return this.categoryName;
	};

	this.setCategoryName = function (categoryName) {
		this.categoryName = categoryName;
	};

	this.getAmount = function () {
		return this.amount;
	};

	this.setAmount = function (amount) {
		this.amount = amount;
	};
};

module.exports = ExpenseCategoryReportObject;