var BankAdjustmentTxn = function BankAdjustmentTxn() {
	this.adjId;
	this.adjBankId;
	this.adjType;
	this.adjAmount;
	this.adjDescription;
	this.adjDate;
	var ErrorCode = require('./../Constants/ErrorCode.js');

	this.getAdjId = function () {
		return this.adjId;
	};
	this.setAdjId = function (adjId) {
		this.adjId = adjId;
	};

	this.getAdjToBankId = function () {
		return this.adjToBankId;
	};
	this.setAdjToBankId = function (adjToBankId) {
		this.adjToBankId = adjToBankId;
	};

	this.getAdjBankId = function () {
		return this.adjBankId;
	};
	this.setAdjBankId = function (adjBankId) {
		this.adjBankId = adjBankId;
	};

	this.getAdjType = function () {
		return this.adjType;
	};
	this.setAdjType = function (adjType) {
		this.adjType = adjType;
	};

	this.getAdjAmount = function () {
		return this.adjAmount;
	};
	this.setAdjAmount = function (adjAmount) {
		this.adjAmount = adjAmount;
	};

	this.getAdjDescription = function () {
		return this.adjDescription;
	};
	this.setAdjDescription = function (adjDescription) {
		this.adjDescription = adjDescription;
	};

	this.getAdjDate = function () {
		return this.adjDate;
	};
	this.setAdjDate = function (adjDate) {
		this.adjDate = adjDate;
	};

	this.createAdjustment = function () {
		var statusCode = ErrorCode.ERROR_NEW_BANK_ADJUSTMENT_FAILED;
		var BankAdjustmentTxnModel = require('./../Models/BankAdjustmentTxnModel.js');
		var bankAdjustmentTxnModel = new BankAdjustmentTxnModel();
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var transactionmanager = new TransactionManager();

		var isTransactionBeginSuccess = transactionmanager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_NEW_BANK_ADJUSTMENT_FAILED;
			return statusCode;
		}

		try {
			bankAdjustmentTxnModel.setAdjBankId(this.getAdjBankId());
			bankAdjustmentTxnModel.setAdjType(this.getAdjType());
			bankAdjustmentTxnModel.setAdjAmount(this.getAdjAmount());
			bankAdjustmentTxnModel.setAdjToBankId(this.getAdjToBankId());
			bankAdjustmentTxnModel.setAdjDate(this.getAdjDate());
			bankAdjustmentTxnModel.setAdjDescription(this.getAdjDescription());
			statusCode = bankAdjustmentTxnModel.createAdjustment();
			if (statusCode == ErrorCode.ERROR_NEW_BANK_ADJUSTMENT_SUCCESS) {
				if (!transactionmanager.CommitTransaction()) {
					statusCode = ErrorCode.ERROR_NEW_BANK_ADJUSTMENT_FAILED;
				}
			}
		} catch (err) {
			logger.error('Create bank adjustment exception ' + err);
		}
		transactionmanager.EndTransaction();
		return statusCode;
	};

	this.updateAdjustment = function () {
		console.log(this);
		var statusCode = ErrorCode.ERROR_NEW_BANK_ADJUSTMENT_FAILED;
		var BankAdjustmentTxnModel = require('./../Models/BankAdjustmentTxnModel.js');
		var bankAdjustmentTxnModel = new BankAdjustmentTxnModel();
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var transactionmanager = new TransactionManager();

		var isTransactionBeginSuccess = transactionmanager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_NEW_BANK_ADJUSTMENT_FAILED;
			return statusCode;
		}

		try {
			bankAdjustmentTxnModel.setAdjBankId(this.getAdjBankId());
			bankAdjustmentTxnModel.setAdjToBankId(this.getAdjToBankId());
			bankAdjustmentTxnModel.setAdjType(this.getAdjType());
			bankAdjustmentTxnModel.setAdjAmount(this.getAdjAmount());
			bankAdjustmentTxnModel.setAdjDate(this.getAdjDate());
			bankAdjustmentTxnModel.setAdjId(this.getAdjId());
			bankAdjustmentTxnModel.setAdjDescription(this.getAdjDescription());
			statusCode = bankAdjustmentTxnModel.updateAdjustment();
			if (statusCode == ErrorCode.ERROR_UPDATE_BANK_ADJUSTMENT_SUCCESS) {
				if (!transactionmanager.CommitTransaction()) {
					statusCode = ErrorCode.ERROR_NEW_BANK_ADJUSTMENT_FAILED;
				}
			}
		} catch (err) {
			logger.error('Update bank adj exception ' + err);
		}

		transactionmanager.EndTransaction();
		return statusCode;
	};

	this.deleteAdjTxn = function () {
		var statusCode = ErrorCode.ERROR_BANK_ADJ_DELETE_FAILED;
		var BankAdjustmentTxnModel = require('./../Models/BankAdjustmentTxnModel.js');
		var bankAdjustmentTxnModel = new BankAdjustmentTxnModel();
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var transactionManager = new TransactionManager();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_BANK_ADJ_DELETE_FAILED;
			return statusCode;
		}

		try {
			bankAdjustmentTxnModel.setAdjId(this.getAdjId());
			statusCode = bankAdjustmentTxnModel.deleteAdjTxn();
			if (statusCode == ErrorCode.ERROR_BANK_ADJ_DELETE_SUCCESS) {
				if (!transactionManager.CommitTransaction()) {
					statusCode = ErrorCode.ERROR_BANK_ADJ_DELETE_FAILED;
				}
			}
		} catch (err) {
			logger.error('Delete bank adj exception ' + err);
		}
		transactionManager.EndTransaction();
		return statusCode;
	};
};

module.exports = BankAdjustmentTxn;