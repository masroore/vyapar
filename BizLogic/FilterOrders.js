var FilterOrders = {
	getOrderListWithName: function getOrderListWithName(name, orderStatus, txnList) {
		var txnType = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

		var rtnTxnList = [];
		var len = txnList.length;
		for (var i = 0; i < len; i++) {
			var x = txnList[i];
			if (Number(orderStatus) != 0 && Number(x['status']) != Number(orderStatus)) {
				continue;
			}
			if (txnType && Number(txnType) && txnType != x.getTxnType()) {
				continue;
			}
			var nameRef = x.getNameRef();
			if (name && nameRef && nameRef.getFullName() != name) {
				continue;
			}
			rtnTxnList.push(x);
		}
		return rtnTxnList;
	},
	getOrderList: function getOrderList(orderStatus, txnList) {
		var txnType = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

		return this.getOrderListWithName(null, orderStatus, txnList, txnType);
	}

};

module.exports = FilterOrders;