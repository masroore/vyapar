var constants, itemcache;
var ItemLogic = function ItemLogic() {
	if (!constants) {
		ErrorCode = require('./../Constants/ErrorCode.js');
		ItemType = require('./../Constants/ItemType.js');
		TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	}
	var IstType = require('./../Constants/IstType.js');

	this.itemId = null;
	this.itemName = '';
	this.itemSaleUnitPrice;
	this.itemPurchaseUnitPrice;
	this.itemStockQuantity;
	this.itemMinStockQuantity;
	this.itemLocation;
	this.itemStockValue;
	this.itemCategoryId;
	this.itemTaxId;
	this.itemTaxType;
	this.itemTaxTypePurchase;
	this.itemType;
	var isItemActive = 1;
	this.itemStockQuantity = 0;
	this.itemSaleUnitPrice = 0;
	this.itemPurchaseUnitPrice = 0;
	this.itemMinStockQuantity = 0;
	this.itemCategoryId = 1;
	this.unitMappingId = null;
	this.itemDateCreated = new Date();
	this.itemHSNCode = '';
	this.itemDescription = '';
	this.itemAtPrice;
	this.reservedQty = 0;
	this.itemTaxTypeSale = ItemType.ITEM_TXN_TAX_EXCLUSIVE;
	this.itemTaxTypePurchase = ItemType.ITEM_TXN_TAX_EXCLUSIVE;

	this.getItemAtPrice = function () {
		return this.itemAtPrice;
	};
	this.setItemAtPrice = function (itemAtPrice) {
		this.itemAtPrice = itemAtPrice;
	};
	this.getItemDescription = function () {
		return this.itemDescription;
	};
	this.setItemDescription = function (itemDescription) {
		this.itemDescription = itemDescription;
	};

	this.getItemAdditionalCESS = function () {
		return this.itemAdditionalCess;
	};
	this.setItemAdditionalCESS = function (cess) {
		this.itemAdditionalCess = cess;
	};

	this.getItemHSNCode = function () {
		return this.itemHSNCode;
	};
	this.setItemHSNCode = function (itemHSNCode) {
		this.itemHSNCode = itemHSNCode;
	};

	this.getItemTaxId = function () {
		return this.itemTaxId;
	};
	this.setItemTaxId = function (itemTaxId) {
		this.itemTaxId = itemTaxId;
	};

	this.getItemTaxTypeSale = function () {
		return this.itemTaxTypeSale;
	};
	this.setItemTaxTypeSale = function (itemTaxType) {
		this.itemTaxTypeSale = itemTaxType;
	};

	this.getItemTaxTypePurchase = function () {
		return this.itemTaxTypePurchase;
	};
	this.setItemTaxTypePurchase = function (itemTaxType) {
		this.itemTaxTypePurchase = itemTaxType;
	};

	this.getItemDateCreated = function () {
		return this.itemDateCreated;
	};
	this.setItemDateCreated = function (itemDateCreated) {
		this.itemDateCreated = itemDateCreated;
	};

	this.getItemOpeningStock = function () {
		return this.itemOpeningStock;
	};
	this.setItemOpeningStock = function (itemOpeningStock) {
		this.itemOpeningStock = itemOpeningStock;
	};

	this.getItemOpeningDate = function () {
		return this.itemOpeningDate;
	};
	this.setItemOpeningDate = function (itemOpeningDate) {
		this.itemOpeningDate = itemOpeningDate;
	};

	this.getITemDateModified = function () {
		return this.itemDateModified;
	};
	this.setItemDateModified = function (itemDateModified) {
		this.itemDateModified = itemDateModified;
	};

	var statusCode = ErrorCode['SUCCESS'];

	this.getUnitMappingId = function () {
		return this.unitMappingId;
	};
	this.setUnitMappingId = function (unitMappingId) {
		this.unitMappingId = unitMappingId;
	};

	this.getSecondaryUnitId = function () {
		return this.secondaryUnitId;
	};
	this.setSecondaryUnitId = function (secondaryUnitId) {
		this.secondaryUnitId = secondaryUnitId;
	};

	this.getBaseUnitId = function () {
		return this.baseUnitId;
	};
	this.setBaseUnitId = function (baseUnitId) {
		this.baseUnitId = baseUnitId;
	};

	this.getItemCode = function () {
		return this.itemCode;
	};
	this.setItemCode = function (itemCode) {
		this.itemCode = itemCode;
	};

	this.itemLocation = '';
	this.itemType = ItemType['ITEM_TYPE_INCENTORY'];

	this.getItemCategoryId = function () {
		if (!this.itemCategoryId) {
			this.itemCategoryId = 1;
		}
		return this.itemCategoryId;
	};
	this.setItemCategoryId = function (categoryId) {
		this.itemCategoryId = categoryId;
	};

	this.getItemType = function () {
		return this.itemType;
	};
	this.setItemType = function (itemType) {
		this.itemType = itemType;
	};

	this.isItemInventory = function () {
		return this.itemType == ItemType.ITEM_TYPE_INVENTORY;
	};

	this.getItemStockValue = function () {
		if (this.isItemService()) {
			return 0;
		}
		return this.itemStockValue;
	};
	this.setItemStockValue = function (itemStockValue) {
		this.itemStockValue = itemStockValue;
	};

	this.getItemLocation = function () {
		return this.itemLocation;
	};
	this.setItemLocation = function (itemLocation) {
		this.itemLocation = itemLocation;
	};

	this.getItemId = function () {
		return this.itemId;
	};
	this.setItemId = function (itemId) {
		this.itemId = itemId;
	};

	this.getIsItemActive = function () {
		return isItemActive;
	};
	this.setIsItemActive = function (IsItemActive) {
		isItemActive = IsItemActive;
	};

	this.getItemName = function () {
		return this.itemName;
	};
	this.setItemName = function (itemName) {
		this.itemName = itemName;
	};

	this.getItemSaleUnitPrice = function () {
		return this.itemSaleUnitPrice;
	};
	this.setItemSaleUnitPrice = function (itemSaleUnitPrice) {
		this.itemSaleUnitPrice = itemSaleUnitPrice;
	};

	this.getItemPurchaseUnitPrice = function () {
		return this.itemPurchaseUnitPrice;
	};
	this.setItemPurchaseUnitPrice = function (itemPurchaseUnitPrice) {
		this.itemPurchaseUnitPrice = itemPurchaseUnitPrice;
	};

	this.getItemStockQuantity = function () {
		// if(this.isItemService()){
		//     return 0;
		// }
		return this.itemStockQuantity;
	};
	this.setItemStockQuantity = function (itemStockQuantity) {
		this.itemStockQuantity = itemStockQuantity;
	};

	this.getItemMinStockQuantity = function () {
		return this.itemMinStockQuantity;
	};
	this.setItemMinStockQuantity = function (itemMinStockQuantity) {
		this.itemMinStockQuantity = itemMinStockQuantity;
	};

	this.isItemService = function () {
		var itemType = this.getItemType();
		if (itemType == ItemType.ITEM_TYPE_SERVICE) {
			return true;
		}
		return false;
	};

	function addItemImages(itemImageList, itemId) {
		var statusCode = ErrorCode.ERROR_ITEM_SAVE_SUCCESS;
		if (itemImageList && itemImageList.length > 0) {
			try {
				for (var i = 0; i < itemImageList.length; i++) {
					var itemImage = itemImageList[i];
					statusCode = itemImage.addItemImage(itemId);
					if (statusCode == ErrorCode.ERROR_ITEM_IMAGE_SAVE_SUCCESS) {
						statusCode = ErrorCode.ERROR_ITEM_SAVE_SUCCESS;
					} else {
						statusCode = ErrorCode.ERROR_ITEM_SAVE_FAILED;
						break;
					}
				}
			} catch (ex) {
				if (logger) {
					logger.error('Error occurred adding item images', ex);
				}
				statusCode = ErrorCode.ERROR_ITEM_SAVE_FAILED;
			}
		}
		return statusCode;
	}

	this.addItem = function () {
		statusCode = ErrorCode['SUCCESS'];
		ItemCache = require('./../Cache/ItemCache.js');
		itemcache = new ItemCache();
		this.unitMappingId = this.unitMappingId ? this.unitMappingId : null;
		this.baseUnitId = this.baseUnitId ? this.baseUnitId : null;
		this.secondaryUnitId = this.secondaryUnitId ? this.secondaryUnitId : null;

		if (itemcache.isItemExistsByItemType(this.itemType, this.itemName)) {
			statusCode = ErrorCode['ERROR_ITEM_ALREADY_EXISTS'];
		} else if (this.itemCode && itemcache.itemCodeExists(this.itemCode)) {
			statusCode = ErrorCode['ERROR_ITEM_CODE_ALREADY_EXISTS'];
		} else {
			var ItemModel = require('./../Models/ItemModel.js');
			itemModel = new ItemModel();
			itemModel.setItemCategoryId(this.getItemCategoryId());
			itemModel.setItemName(this.itemName);
			itemModel.setItemSaleUnitPrice(this.itemSaleUnitPrice);
			itemModel.setItemPurchaseUnitPrice(this.itemPurchaseUnitPrice);
			itemModel.setItemMinStockQuantity(this.itemMinStockQuantity);
			itemModel.setItemLocation(this.itemLocation);
			itemModel.setItemType(this.itemType);
			itemModel.setItemCode(this.itemCode);
			itemModel.setUnitMappingId(this.unitMappingId);
			itemModel.setBaseUnitId(this.baseUnitId);
			itemModel.setSecondaryUnitId(this.secondaryUnitId);
			itemModel.setItemTaxId(this.getItemTaxId());
			itemModel.setItemTaxTypeSale(this.getItemTaxTypeSale());
			itemModel.setItemTaxTypePurchase(this.getItemTaxTypePurchase());
			itemModel.setItemHSNCode(this.getItemHSNCode());
			itemModel.setItemAdditionalCESS(this.getItemAdditionalCESS());
			itemModel.setItemDescription(this.getItemDescription());
			statusCode = itemModel.addItem();
			if (statusCode == ErrorCode['ERROR_ITEM_SAVE_SUCCESS']) {
				this.setItemId(itemModel.getItemId());
				itemcache.refreshItemCache(this);
			}
		}
		return statusCode;
	};

	this.SaveNewItem = function (obj, newItemStockTrackingList, itemImageList) {
		statusCode = ErrorCode['SUCCESS'];
		ItemCache = require('./../Cache/ItemCache.js');
		itemcache = new ItemCache();

		// Varibales to be used
		this.itemName = obj['itemName'].trim();
		this.itemSaleUnitPrice = obj['salePricePerUnit'] ? obj['salePricePerUnit'] : 0.0;
		this.itemPurchaseUnitPrice = obj['purchasePricePerUnit'] ? obj['purchasePricePerUnit'] : 0.0;
		this.itemMinStockQuantity = obj['minStockQuantity'] ? obj['minStockQuantity'] : 0.0;
		this.setItemTaxId(obj['itemTaxId']);
		this.setItemTaxTypeSale(obj['itemTaxType'] == ItemType.ITEM_TXN_TAX_INCLUSIVE ? ItemType.ITEM_TXN_TAX_INCLUSIVE : ItemType.ITEM_TXN_TAX_EXCLUSIVE);
		this.setItemTaxTypePurchase(obj['itemTaxTypePurchase'] == ItemType.ITEM_TXN_TAX_INCLUSIVE ? ItemType.ITEM_TXN_TAX_INCLUSIVE : ItemType.ITEM_TXN_TAX_EXCLUSIVE);
		this.itemLocation = obj['itemLocation'];
		if (obj['date']) {
			// var getDate = new GetDate();
			this.setItemDateCreated(obj['date']);
		}
		this.itemType = obj['itemType'];
		this.itemCode = obj['itemCode'];
		this.itemCategoryId = obj['itemCategoryId'];
		this.unitMappingId = obj['mappingId'] ? obj['mappingId'] : null;
		this.baseUnitId = obj['baseUnitId'] ? obj['baseUnitId'] : null;
		this.secondaryUnitId = obj['secondaryUnitId'] ? obj['secondaryUnitId'] : null;
		this.setItemHSNCode(obj['itemHSNCode']);
		this.setItemOpeningStock(obj['stockQuantity']);
		this.setItemAdditionalCESS(obj['itemAdditionalCess']);
		this.setItemDescription(obj['itemDescription']);
		this.setItemAtPrice(obj['atPrice']);
		if (this.itemName.trim().length < 1) {
			statusCode = ErrorCode.ERROR_ITEM_NAME_EMPTY;
			return statusCode;
		}
		if (isNaN(this.itemSaleUnitPrice) || isNaN(this.itemPurchaseUnitPrice) || isNaN(this.itemMinStockQuantity)) {
			statusCode = ErrorCode['ERROR_TXN_INVALID_AMOUNT'];
			return statusCode;
		}
		this.itemStockQuantity = 0;
		this.itemStockValue = 0;
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var transactionmanager = new TransactionManager();
		var isTransactionBeginSuccess = transactionmanager.BeginTransaction();

		if (isTransactionBeginSuccess) {
			try {
				if (statusCode == ErrorCode['SUCCESS']) {
					statusCode = this.addItem();
				}
				if (statusCode == ErrorCode['ERROR_ITEM_SAVE_SUCCESS']) {
					if (newItemStockTrackingList != null && newItemStockTrackingList.length > 0) {
						for (var i = 0; i < newItemStockTrackingList.length; i++) {
							var itemStockTracking = newItemStockTrackingList[i];

							var itemStockTrackingData = itemStockTracking.getISTWithParameters(itemStockTracking.getIstBatchNumber(), itemStockTracking.getIstSerialNumber(), itemStockTracking.getIstMrp(), itemStockTracking.getIstExpiryDate(), itemStockTracking.getIstManufacturingDate(), itemStockTracking.getIstSize(), this.getItemId());

							if (itemStockTrackingData.getIstId()) {
								var istId = itemStockTrackingData.getIstId();
								var currentQuantity = itemStockTrackingData.getIstCurrentQuantity();
								var openingQuantity = itemStockTrackingData.getIstOpeningQuantity();

								var statusCode = itemStockTracking.changeISTQuantity(istId, Number(currentQuantity) + Number(itemStockTracking.getIstOpeningQuantity()), Number(openingQuantity) + Number(itemStockTracking.getIstOpeningQuantity()), IstType.OPENING);

								if (statusCode == ErrorCode.ERROR_ITEM_STOCK_QUANITY_UPDATE_SUCCESS) {
									statusCode = ErrorCode.ERROR_ITEM_SAVE_SUCCESS;
								} else {
									break;
								}
							} else {
								var istId = itemStockTracking.addItemStockTracking(itemStockTracking.getIstBatchNumber(), itemStockTracking.getIstSerialNumber(), itemStockTracking.getIstMrp(), itemStockTracking.getIstExpiryDate(), itemStockTracking.getIstManufacturingDate(), itemStockTracking.getIstSize(), this.getItemId(), itemStockTracking.getIstOpeningQuantity(), IstType.OPENING);

								statusCode = itemStockTracking.getErrorCodeForResult(istId);

								if (statusCode == ErrorCode.ERROR_IST_SUCCESS) {
									statusCode = ErrorCode.ERROR_ITEM_SAVE_SUCCESS;
								} else {
									break;
								}
							}
						}
					}

					if (statusCode === ErrorCode.ERROR_ITEM_SAVE_SUCCESS) {
						statusCode = addItemImages(itemImageList, this.getItemId());
					}

					if (statusCode == ErrorCode.ERROR_ITEM_SAVE_SUCCESS) {
						var newOpeningStockQuantity = MyDouble.convertStringToDouble(this.getItemOpeningStock(), true);

						if (newOpeningStockQuantity != 0) {
							var date = obj['date'];
							var ItemAdjustmentTxn = require('./ItemAdjustmentTxn.js');
							var itemadjustmenttxn = new ItemAdjustmentTxn();
							statusCode = itemadjustmenttxn.addItemAdjustment(this.getItemId(), TxnTypeConstant.TXN_TYPE_ADJ_OPENING, newOpeningStockQuantity, date, StringConstant.itemOpeningStockString, this.itemAtPrice);
							if (statusCode == ErrorCode['ERROR_ITEM_ADJ_SAVE_SUCCESS']) {
								statusCode = ErrorCode.ERROR_ITEM_SAVE_SUCCESS;
								this.setItemOpeningStock(newOpeningStockQuantity);
							} else {
								statusCode = ErrorCode.ERROR_ITEM_SAVE_FAILED;
							}
						}
					}
				}
				if (statusCode == ErrorCode['ERROR_ITEM_SAVE_SUCCESS']) {
					if (!transactionmanager.CommitTransaction()) {
						statusCode = ErrorCode.ERROR_ITEM_SAVE_FAILED;
					} else {
						itemcache.refreshItemCache(this);
					}
				}
			} catch (err) {
				logger.error('Item adding exception ' + err);
			}

			transactionmanager.EndTransaction();
		} else {
			statusCode = ErrorCode.ERROR_ITEM_SAVE_FAILED;
		}

		if (statusCode != ErrorCode['ERROR_ITEM_SAVE_SUCCESS']) {
			itemcache.reloadItemCache();
		}

		return statusCode;
	};

	this.updateItem = function () {
		ItemCache = require('./../Cache/ItemCache.js');
		itemcache = new ItemCache();
		console.log(this.getItemType());
		this.unitMappingId = this.unitMappingId ? this.unitMappingId : null;
		this.baseUnitId = this.baseUnitId ? this.baseUnitId : null;
		this.secondaryUnitId = this.secondaryUnitId ? this.secondaryUnitId : null;

		if (itemcache.isItemExistsByItemType(this.getItemType(), this.itemName)) {
			var ItemModel = require('./../Models/ItemModel.js');
			itemModel = new ItemModel();
			itemModel.setItemId(this.itemId);
			itemModel.setItemName(this.itemName);
			itemModel.setItemSaleUnitPrice(this.itemSaleUnitPrice);
			itemModel.setItemStockValue(this.itemStockValue);
			itemModel.setItemPurchaseUnitPrice(this.itemPurchaseUnitPrice);
			itemModel.setItemMinStockQuantity(this.itemMinStockQuantity);
			itemModel.setItemStockQuantity(this.itemStockQuantity);
			itemModel.setItemLocation(this.itemLocation);
			itemModel.setItemType(this.itemType);
			itemModel.setUnitMappingId(this.unitMappingId);
			itemModel.setBaseUnitId(this.baseUnitId);
			itemModel.setSecondaryUnitId(this.secondaryUnitId);
			itemModel.setItemTaxId(this.getItemTaxId());
			itemModel.setItemTaxTypeSale(this.getItemTaxTypeSale());
			itemModel.setItemTaxTypePurchase(this.getItemTaxTypePurchase());
			itemModel.setItemCode(this.itemCode);
			itemModel.setItemCategoryId(this.getItemCategoryId());
			itemModel.setItemHSNCode(this.getItemHSNCode());
			itemModel.setItemAdditionalCESS(this.getItemAdditionalCESS());
			itemModel.setItemDescription(this.getItemDescription());
			itemModel.setIsItemActive(this.getIsItemActive());
			statusCode = itemModel.updateItem();
		} else {
			statusCode = TxnTypeConstant.ERROR_ITEM_SAVE_FAILED;
		}
		return statusCode;
	};

	this.updateItems = function (obj, originalItemStockTrackingList, newItemStockTrackingList, itemImageList) {
		var ItemCache = require('./../Cache/ItemCache.js');
		var itemcache = new ItemCache();
		var statusCode = ErrorCode.SUCCESS;
		if (this.getBaseUnitId() && this.getBaseUnitId() != obj['baseUnitId'] && !this.canEditBaseUnit()) {
			statusCode = ErrorCode.ERROR_ITEM_UNIT_CANT_CHANGE;
			return statusCode;
		}
		if (this.itemCode != obj['itemCode'] && obj['itemCode'] && itemcache.itemCodeExists(obj['itemCode'])) {
			statusCode = ErrorCode['ERROR_ITEM_CODE_ALREADY_EXISTS'];
			return statusCode;
		}
		obj['itemName'] = obj['itemName'].trim();
		if (obj['itemName'].length < 1) {
			statusCode = ErrorCode.ERROR_ITEM_NAME_EMPTY;
			return statusCode;
		}
		if (isNaN(this.itemSaleUnitPrice) || isNaN(this.itemPurchaseUnitPrice) || isNaN(this.itemMinStockQuantity)) {
			statusCode = ErrorCode['ERROR_TXN_INVALID_AMOUNT'];
			return statusCode;
		}
		var newItemType = obj['itemType'];
		if (itemcache.isItemExistsWithDifferentItemId(newItemType, obj['itemName'], this.itemId)) {
			statusCode = ErrorCode.ERROR_ITEM_ALREADY_EXISTS;
			return statusCode;
		}

		this.itemSaleUnitPrice = obj['salePricePerUnit'] ? obj['salePricePerUnit'] : 0.0;
		this.itemPurchaseUnitPrice = obj['purchasePricePerUnit'] ? obj['purchasePricePerUnit'] : 0.0;
		this.itemMinStockQuantity = obj['minStockQuantity'] ? obj['minStockQuantity'] : 0.0;
		this.itemCategoryId = obj['itemCategoryId'];
		this.setItemTaxId(obj['itemTaxId']);
		this.setItemAdditionalCESS(obj['itemAdditionalCess']);
		this.setItemTaxTypeSale(obj['itemTaxType']);
		this.setItemTaxTypePurchase(obj['itemTaxTypePurchase']);
		this.itemLocation = obj['itemLocation'];
		this.unitMappingId = obj['mappingId'] ? obj['mappingId'] : null;
		this.baseUnitId = obj['baseUnitId'] ? obj['baseUnitId'] : null;
		this.secondaryUnitId = obj['secondaryUnitId'] ? obj['secondaryUnitId'] : null;
		this.setItemOpeningStock(obj['stockQuantity']);
		this.itemCode = obj['itemCode'];
		this.setItemHSNCode(obj['itemHSNCode']);
		this.itemType = obj.itemType;
		this.setItemAtPrice(obj['atPrice']);
		this.setItemDescription(obj['itemDescription']);
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var transactionManager = new TransactionManager();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (isTransactionBeginSuccess) {
			try {
				if (statusCode == ErrorCode.SUCCESS) {
					this.updateItemStockValue();
					this.itemName = obj['itemName'];
					// this.itemStockValue = itemcache.getItemById(this.itemId).getItemStockQuantity();
					var ItemModel = require('./../Models/ItemModel.js');
					itemModel = new ItemModel();
					itemModel.setItemId(this.itemId);
					itemModel.setItemName(this.itemName);
					itemModel.setItemSaleUnitPrice(this.itemSaleUnitPrice);
					itemModel.setItemStockValue(this.itemStockValue);
					itemModel.setItemPurchaseUnitPrice(this.itemPurchaseUnitPrice);
					itemModel.setItemMinStockQuantity(this.itemMinStockQuantity);
					itemModel.setItemStockQuantity(this.itemStockQuantity);
					itemModel.setItemLocation(this.itemLocation);
					itemModel.setItemTaxId(this.getItemTaxId());
					itemModel.setItemTaxTypeSale(this.getItemTaxTypeSale());
					itemModel.setItemTaxTypePurchase(this.getItemTaxTypePurchase());
					itemModel.setItemType(this.itemType);
					itemModel.setItemCode(this.itemCode);
					itemModel.setUnitMappingId(this.unitMappingId);
					itemModel.setBaseUnitId(this.baseUnitId);
					itemModel.setSecondaryUnitId(this.secondaryUnitId);
					itemModel.setItemCategoryId(this.getItemCategoryId());
					itemModel.setItemHSNCode(this.getItemHSNCode());
					itemModel.setItemAdditionalCESS(this.getItemAdditionalCESS());
					itemModel.setItemDescription(this.getItemDescription());
					itemModel.setIsItemActive(this.getIsItemActive());
					statusCode = itemModel.updateItem();
				} else if (statusCode != ErrorCode.ERROR_ITEM_ALREADY_EXISTS) {
					statusCode = ErrorCode.ERROR_ITEM_SAVE_FAILED;
				}
				if (statusCode == ErrorCode.ERROR_ITEM_SAVE_SUCCESS && this.itemType == ItemType.ITEM_TYPE_INVENTORY) {
					if (originalItemStockTrackingList && originalItemStockTrackingList.length && originalItemStockTrackingList.length > 0) {
						for (var i = 0; i < originalItemStockTrackingList.length; i++) {
							var itemStockTracking = originalItemStockTrackingList[i];
							if (itemStockTracking.isDataEditable()) {
								statusCode = itemStockTracking.deleteItemStockTracking(itemStockTracking.getIstId());

								if (statusCode == ErrorCode.ERROR_IST_FAILED) {
									break;
								} else {
									statusCode = ErrorCode.ERROR_ITEM_SAVE_SUCCESS;
								}
							}
						}
					}

					if (newItemStockTrackingList && newItemStockTrackingList.length && newItemStockTrackingList.length > 0 && statusCode == ErrorCode.ERROR_ITEM_SAVE_SUCCESS) {
						for (var i = 0; i < newItemStockTrackingList.length; i++) {
							itemStockTracking = newItemStockTrackingList[i];

							if (itemStockTracking.isDataEditable()) {
								var itemStockTrackingData = itemStockTracking.getISTWithParameters(itemStockTracking.getIstBatchNumber(), itemStockTracking.getIstSerialNumber(), itemStockTracking.getIstMrp(), itemStockTracking.getIstExpiryDate(), itemStockTracking.getIstManufacturingDate(), itemStockTracking.getIstSize(), this.getItemId());

								if (itemStockTrackingData.getIstId()) {
									if (itemStockTrackingData.isDataEditable()) {
										var istId = itemStockTrackingData.getIstId();
										var currentQuantity = itemStockTrackingData.getIstCurrentQuantity();
										var openingQuantity = itemStockTrackingData.getIstOpeningQuantity();

										var statusCode = itemStockTracking.changeISTQuantity(istId, Number(currentQuantity) + Number(itemStockTracking.getIstOpeningQuantity()), Number(openingQuantity) + Number(itemStockTracking.getIstOpeningQuantity()), IstType.OPENING);

										if (statusCode == ErrorCode.ERROR_ITEM_STOCK_QUANITY_UPDATE_SUCCESS) {
											statusCode = ErrorCode.ERROR_ITEM_SAVE_SUCCESS;
										} else {
											break;
										}
									} else {
										statusCode = ErrorCode.ERROR_IST_CANNOT_BE_ADDED;
										break;
									}
								} else {
									var istId = itemStockTracking.addItemStockTracking(itemStockTracking.getIstBatchNumber(), itemStockTracking.getIstSerialNumber(), itemStockTracking.getIstMrp(), itemStockTracking.getIstExpiryDate(), itemStockTracking.getIstManufacturingDate(), itemStockTracking.getIstSize(), this.getItemId(), itemStockTracking.getIstOpeningQuantity(), IstType.OPENING);

									statusCode = itemStockTracking.getErrorCodeForResult(istId);

									if (statusCode == ErrorCode.ERROR_IST_SUCCESS) {
										statusCode = ErrorCode.ERROR_ITEM_SAVE_SUCCESS;
									} else {
										break;
									}
								}
							}
						}
					}
					if (statusCode == ErrorCode.ERROR_ITEM_SAVE_SUCCESS) {
						var newOpeningStockQuantity = MyDouble.convertStringToDouble(this.getItemOpeningStock(), true);

						var adjObj = dataLoader.getAdjObjForOpening(this.itemId);
						itemcache.refreshItemCache(this);
						if (adjObj.itemAdjId) {
							var date = MyDate.getDateObj($('#datepickerItem').val(), 'dd/mm/yyyy', '/');
							var ItemAdjustmentTxn = require('./ItemAdjustmentTxn.js');
							var itemadjustmenttxn = new ItemAdjustmentTxn();
							itemadjustmenttxn.loadItemAdjustmentTxn(adjObj.itemAdjId);
							statusCode = itemadjustmenttxn.updateItemAdjustment(this.getItemId(), TxnTypeConstant.TXN_TYPE_ADJ_OPENING, newOpeningStockQuantity, date, StringConstant.itemOpeningStockString, this.getItemAtPrice());
							if (statusCode == ErrorCode.ERROR_ITEM_ADJ_SAVE_SUCCESS) {
								statusCode = ErrorCode.ERROR_ITEM_SAVE_SUCCESS;
							}
						} else if (newOpeningStockQuantity != 0) {
							var date = MyDate.getDateObj($('#datepickerItem').val(), 'dd/mm/yyyy', '/');
							var ItemAdjustmentTxn = require('./ItemAdjustmentTxn.js');
							var itemadjustmenttxn = new ItemAdjustmentTxn();
							statusCode = itemadjustmenttxn.addItemAdjustment(this.getItemId(), TxnTypeConstant.TXN_TYPE_ADJ_OPENING, newOpeningStockQuantity, date, StringConstant.itemOpeningStockString, this.getItemAtPrice());
							if (statusCode == ErrorCode.ERROR_ITEM_ADJ_SAVE_SUCCESS) {
								statusCode = ErrorCode.ERROR_ITEM_SAVE_SUCCESS;
							}
						}
					}
				}

				if (statusCode === ErrorCode.ERROR_ITEM_SAVE_SUCCESS) {
					statusCode = addItemImages(itemImageList, this.getItemId());
				}

				if (statusCode == ErrorCode.ERROR_ITEM_SAVE_SUCCESS) {
					if (!transactionManager.CommitTransaction()) {
						statusCode = ErrorCode.ERROR_ITEM_SAVE_FAILED;
					}
				}
			} catch (err) {
				logger.error('Update item exception ' + err);
				statusCode = ErrorCode.ERROR_ITEM_SAVE_FAILED;
			}

			transactionManager.EndTransaction();
		} else {
			statusCode = ErrorCode.ERROR_ITEM_SAVE_FAILED;
		}
		itemcache.reloadItemCache();
		return statusCode;
	};

	this.deleteItemStockQuantity = function (txnType, itemQuantity, txnStatus) {
		this.itemStockQuantity = this.getItemStockQuantityFromDbByItemId(this.getItemId());
		itemQuantity = Number(itemQuantity);
		switch (txnType) {
			case TxnTypeConstant.TXN_TYPE_SALE:
			case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
				this.itemStockQuantity = Number(this.itemStockQuantity) + itemQuantity;
				this.updateItemStockValue();
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE:
			case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
				this.itemStockQuantity = Number(this.itemStockQuantity) - itemQuantity;
				this.updateItemStockValue();
				break;
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
				var TxnStatus = require('./../Constants/TransactionStatus.js');
				if (txnStatus != TxnStatus.TXN_ORDER_COMPLETE) {
					this.reservedQty = this.reservedQty - itemQuantity;
				}
				break;
		}
		return this.updateItem();
	};

	this.deleteItem = function () {
		debugger;
		var statusCode = ErrorCode.ERROR_ITEM_DELETE_FAILED;
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var ItemModel = require('./../Models/ItemModel.js');
		var ItemCache = require('./../Cache/ItemCache.js');

		var transactionManager = new TransactionManager();
		try {
			var isTransactionBeginSuccess = transactionManager.BeginTransaction();

			if (!isTransactionBeginSuccess) {
				statusCode = ErrorCode.ERROR_ITEM_DELETE_FAILED;
				return statusCode;
			}

			var itemModel = new ItemModel();
			itemModel.setItemId(this.itemId);

			statusCode = itemModel.deleteItem();

			if (statusCode == ErrorCode.ERROR_ITEM_DELETE_SUCCESS) {
				if (!transactionManager.CommitTransaction()) {
					statusCode = ErrorCode.ERROR_ITEM_DELETE_FAILED;
				} else {
					var itemCache = new ItemCache();
					itemCache.reloadItemCache(this);
				}
			}
		} catch (err) {
			console.log(err);
			statusCode = ErrorCode.ERROR_ITEM_DELETE_FAILED;
		}

		transactionManager.EndTransaction();
		return statusCode;
	};

	this.getItemStockQuantityFromDbByItemId = function (itemId) {
		var DataLoader = require('./../DBManager/DataLoader.js');
		var dataloader = new DataLoader();
		var itemStockQuantity = dataloader.getItemStockQuantityFromDbByItemId(itemId);
		return itemStockQuantity;
	};

	this.updateItemStockQuantity = function (txnType, itemQuantity) {
		itemQuantity = Number(itemQuantity);
		var statusCode = ErrorCode.ERROR_ITEM_SAVE_SUCCESS;
		var itemStockQuantity = this.getItemStockQuantityFromDbByItemId(this.getItemId());

		switch (txnType) {
			case TxnTypeConstant.TXN_TYPE_SALE:
				this.itemStockQuantity = itemStockQuantity - itemQuantity;
				statusCode = this.updateItemStockValue();
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE:
				this.itemStockQuantity = itemStockQuantity + itemQuantity;
				statusCode = this.updateItemStockValue();
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
				this.itemStockQuantity = itemStockQuantity + itemQuantity;
				statusCode = this.updateItemStockValue();
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
				this.itemStockQuantity = itemStockQuantity - itemQuantity;
				statusCode = this.updateItemStockValue();
				break;
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
				this.reservedQty = this.reservedQty + itemQuantity;
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
				this.reservedQty = this.reservedQty + itemQuantity;
				break;
		}
		return statusCode;
	};

	this.updateItemStockValue = function () {
		// console.log(this.itemStockQuantity);
		if (this.itemStockQuantity > 0) {
			var DataLoader = require('./../DBManager/DataLoader.js');
			dataloader = new DataLoader();
			var remainingStockQuantity = Number(this.itemStockQuantity);
			var stockValue = 0.0;
			var stockLineItemList = [];
			var purchaseMap = {};

			var purchaseDateList = [];

			var purchaseLineItems = [];
			stockLineItemList = dataloader.getPurchaseLineItemList(this.itemId, Number(this.itemStockQuantity));
			console.log(stockLineItemList);
			var len = stockLineItemList.length;

			// var StockValueDataModel = require('./../Models/StockValueDataModel.js');
			for (var _i = len - 1; _i >= 0; _i--) {
				// purchaseItem = new StockValueDataModel();
				purchaseLineItem = stockLineItemList[_i];
				if (purchaseLineItem.getItemTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE) {
					purchaseDateList.push(purchaseLineItem.getItemPurchaseDate());
					purchaseMap[purchaseLineItem.getItemPurchaseDate()] = purchaseLineItem;
				}
			}
			// console.log("purchase Date list");
			// console.log(purchaseDateList);
			// console.log("purchase mapt");
			// console.log(purchaseMap);
			// purchaseMap.push(purchaseDate);
			// purchaseMap.push(purchaseLineItems);
			if (stockLineItemList.length == 0) {
				this.itemStockValue = Number(this.itemPurchaseUnitPrice) * Number(this.itemStockQuantity);
			} else {
				if (stockLineItemList && stockLineItemList.length > 0) {
					for (i in stockLineItemList) {
						model = stockLineItemList[i];
						if (remainingStockQuantity > 0) {
							if (model.getItemTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE) {
								if (remainingStockQuantity >= model.getTotalQuantityIncludingFreeQuantity()) {
									stockValue = Number(stockValue) + Number(model.getItemPurchaseAmount());
									remainingStockQuantity = Number(remainingStockQuantity) - Number(model.getTotalQuantityIncludingFreeQuantity());
								} else {
									if (model.getTotalQuantityIncludingFreeQuantity() > 0) {
										var pricePerUnit = Number(model.getItemPurchaseAmount()) / Number(model.getTotalQuantityIncludingFreeQuantity());
										stockValue = Number(stockValue) + pricePerUnit * remainingStockQuantity;
										remainingStockQuantity = 0;
									}
								}
							} else if (model.getItemTxnType() == TxnTypeConstant.TXN_TYPE_ADJ_ADD || model.getItemTxnType() == TxnTypeConstant.TXN_TYPE_ADJ_OPENING) {
								if (model.getItemPurchaseAmount() >= 0.0) {
									if (model.getTotalQuantityIncludingFreeQuantity() > 0.0) {
										if (remainingStockQuantity >= model.getTotalQuantityIncludingFreeQuantity()) {
											stockValue += model.getTotalQuantityIncludingFreeQuantity() * model.getItemPurchaseAmount();
											remainingStockQuantity -= model.getTotalQuantityIncludingFreeQuantity();
										} else {
											stockValue += model.getItemPurchaseAmount() * remainingStockQuantity;
											remainingStockQuantity = 0.0;
										}
									}
								} else {
									var oldDate = null;
									for (var i in purchaseDateList) {
										var dateValue = purchaseDateList[i];
										if (dateValue.getTime() > model.getItemPurchaseDate().getTime()) {
											break;
										} else {
											oldDate = dateValue;
										}
									}
									if (oldDate) {
										var purchaseModel = purchaseMap[oldDate];
										if (purchaseModel && purchaseModel.getTotalQuantityIncludingFreeQuantity() > 0.0) {
											var pricePerUnit = Number(purchaseModel.getItemPurchaseAmount()) / Number(purchaseModel.getTotalQuantityIncludingFreeQuantity());
											if (remainingStockQuantity >= model.getTotalQuantityIncludingFreeQuantity()) {
												stockValue += model.getTotalQuantityIncludingFreeQuantity() * pricePerUnit;
												remainingStockQuantity -= model.getTotalQuantityIncludingFreeQuantity();
											} else {
												stockValue += pricePerUnit * remainingStockQuantity;
												remainingStockQuantity = 0.0;
											}
										}
									}
								}
							}
						}
					}
					console.log(stockValue);
					if (remainingStockQuantity > 0) {
						if (purchaseDateList.length > 0) {
							var firstDate = purchaseDateList[0];
							var firstPurchaseModel = purchaseMap[firstDate];
							if (firstPurchaseModel && firstPurchaseModel.getTotalQuantityIncludingFreeQuantity() > 0.0) {
								var pricePerUnit = firstPurchaseModel.getItemPurchaseAmount() / firstPurchaseModel.getTotalQuantityIncludingFreeQuantity();
								stockValue += remainingStockQuantity * pricePerUnit;
								remainingStockQuantity = 0.0;
							}
						} else {
							stockValue += remainingStockQuantity * Number(this.itemPurchaseUnitPrice);
							remainingStockQuantity = 0.0;
						}
					}
					this.itemStockValue = stockValue;
				} else {
					this.itemStockValue = 0.0;
				}
			}
		} else {
			this.itemStockValue = 0.0;
		}
		console.log(this.itemStockValue);
		return this.updateItem();
	};

	this.sortTransactionList = function (txnList) {
		txnList.sort(function (a, b) {
			a = a.txnDate;
			b = b.txnDate;
			return a > b ? -1 : a < b ? 1 : 0;
		});
		// console.log(txnList);
		return txnList;
	};

	this.getItemDetailList = function () {
		var DataLoader = require('./../DBManager/DataLoader.js');
		var dataLoader = new DataLoader();
		var list = [];
		list = dataLoader.getItemDetailList(this.itemId);
		// console.log(list);
		return this.sortTransactionList(list);
	};

	this.canDeleteItem = function () {
		var ItemModel = require('./../Models/ItemModel.js');
		var itemModel = new ItemModel();
		itemModel.setItemId(this.getItemId());
		return itemModel.canDeleteItem();
	};

	this.canEditBaseUnit = function () {
		var DataLoader = require('./../DBManager/DataLoader.js');
		var dataLoader = new DataLoader();
		return dataLoader.canEditBaseUnitForItem(this.itemId);
	};
	this.getReservedQuantity = function () {
		return this.reservedQty;
	};

	this.setReservedQuantity = function (qty) {
		this.reservedQty = qty;
	};

	this.getAvailableQuantity = function () {
		var currentStock = this.getItemStockQuantity();
		var reservedQty = this.getReservedQuantity();
		currentStock = currentStock ? Number(currentStock) : 0;
		reservedQty = reservedQty ? Number(reservedQty) : 0;
		return currentStock - reservedQty;
	};
};

module.exports = ItemLogic;