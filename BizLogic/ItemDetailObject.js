var ItemDetailObject = function ItemDetailObject() {
	this.txnId = 0;
	this.txnType = 0;
	this.txnDate = '';
	this.itemQuantity = 0.0;
	this.itemFreeQuantity = 0.0;
	this.userId = 0;
	this.description = '';
	this.txnPaymentStatus;

	this.getTxnPaymentStatus = function () {
		return this.txnPaymentStatus;
	};

	this.setTxnPaymentStatus = function (txnPaymentStatus) {
		this.txnPaymentStatus = txnPaymentStatus;
	};

	this.getItemFreeQuantity = function () {
		return this.itemFreeQuantity;
	};
	this.setItemFreeQuantity = function (itemFreeQuantity) {
		this.itemFreeQuantity = itemFreeQuantity;
	};

	this.getDescription = function () {
		return this.description;
	};
	this.setDescription = function (description) {
		this.description = description;
	};

	this.getTxnId = function () {
		return this.txnId;
	};
	this.setTxnId = function (txnId) {
		this.txnId = txnId;
	};

	this.getTxnType = function () {
		return this.txnType;
	};
	this.setTxnType = function (txnType) {
		this.txnType = txnType;
	};

	this.getTxnDate = function () {
		return this.txnDate;
	};
	this.setTxnDate = function (txnDate) {
		this.txnDate = txnDate;
	};

	this.getItemQuantity = function () {
		return this.itemQuantity;
	};
	this.setItemQuantity = function (itemQuantity) {
		this.itemQuantity = itemQuantity;
	};

	this.getUserId = function () {
		return this.userId;
	};
	this.setUserId = function (userId) {
		this.userId = userId;
	};

	this.getMappingId = function () {
		return this.mappingId;
	};
	this.setMappingId = function (mappingId) {
		this.mappingId = mappingId;
	};

	this.getLineItemUnitId = function () {
		return this.lineItemUnitId;
	};
	this.setLineItemUnitId = function (lineItemUnitId) {
		this.lineItemUnitId = lineItemUnitId;
	};

	this.getPricePerUnit = function () {
		return this.pricePerUnit;
	};
	this.setPricePerUnit = function (pricePerUnit) {
		this.pricePerUnit = pricePerUnit;
	};
};

module.exports = ItemDetailObject;