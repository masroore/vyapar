var GSTR2ReportObject = function GSTR2ReportObject() {
	this.transactionId;
	this.transactionType;
	this.firmId;
	this.nameId;
	this.transactionDescription = '';
	this.gstinNo;
	this.invoiceNo;
	this.invoiceDate;
	this.invoiceValue = 0;
	this.invoiceTaxableValue = 0;
	this.rate = 0.0;
	this.cessRate = 0.0;
	this.IGSTAmt = 0;
	this.SGSTAmt = 0;
	this.CGSTAmt = 0;
	this.CESSAmt = 0;
	this.OTHERAmt = 0;
	this.AdditionalCessAmt = 0;
	this.POS = '';
	this.reverseCharge = 0;
	this.partyName = '';
	this.txnRefNumber = '';
	this.taxRateId;
	this.itcTypeForReport;
	this.txnReturnRefNumber;
	this.txnReturnDate;

	this.getTxnReturnRefNumber = function () {
		return this.txnReturnRefNumber;
	};
	this.setTxnReturnRefNumber = function (txnReturnRefNumber) {
		this.txnReturnRefNumber = txnReturnRefNumber || '';
	};
	this.getTxnReturnDate = function () {
		return this.txnReturnDate;
	};
	this.setTxnReturnDate = function (txnReturnDate) {
		this.txnReturnDate = txnReturnDate || '';
	};

	this.getTotalAmount = function () {
		var totalValue = this.invoiceTaxableValue;
		if (this.rate && this.rate > 0) {
			totalValue = totalValue + totalValue * this.rate / 100;
		}
		if (this.AdditionalCessAmt && this.AdditionalCessAmt > 0) {
			totalValue += this.AdditionalCessAmt;
		}
		return totalValue;
	};

	this.getItcTypeForReport = function () {
		return this.itcTypeForReport;
	};
	this.setItcTypeForReport = function (itcTypeForReport) {
		this.itcTypeForReport = itcTypeForReport;
	};
	this.getTxnRefNumber = function () {
		return this.txnRefNumber;
	};
	this.setTxnRefNumber = function (txnRefNumber) {
		this.txnRefNumber = txnRefNumber;
	};
	this.getTaxRateId = function () {
		return this.taxRateId;
	};
	this.setTaxRateId = function (taxRateId) {
		this.taxRateId = taxRateId;
	};
	this.getFirmId = function () {
		return this.firmId;
	};
	this.setFirmId = function (firmId) {
		this.firmId = firmId;
	};
	this.getPartyName = function () {
		return this.partyName;
	};
	this.setPartyName = function (partyName) {
		this.partyName = partyName;
	};
	this.getTransactionId = function () {
		return this.transactionId;
	};
	this.setTransactionId = function (transactionId) {
		this.transactionId = transactionId;
	};
	this.getNameId = function () {
		return this.nameId;
	};
	this.setNameId = function (nameId) {
		this.nameId = nameId;
	};

	this.getGstinNo = function () {
		return this.gstinNo;
	};

	this.setGstinNo = function (gstinNo) {
		this.gstinNo = gstinNo;
	};

	this.getInvoiceNo = function () {
		return this.invoiceNo;
	};

	this.setInvoiceNo = function (invoiceNo) {
		this.invoiceNo = invoiceNo;
	};

	this.getInvoiceDate = function () {
		return this.invoiceDate;
	};

	this.setInvoiceDate = function (invoiceDate) {
		this.invoiceDate = invoiceDate;
	};

	this.getInvoiceValue = function () {
		return this.invoiceValue;
	};

	this.setInvoiceValue = function (invoiceValue) {
		this.invoiceValue = invoiceValue;
	};

	this.getRate = function () {
		return this.rate;
	};

	this.setRate = function (rate) {
		this.rate = rate;
	};

	this.getCessRate = function () {
		return this.cessRate;
	};

	this.setCessRate = function (cessRate) {
		this.cessRate = cessRate;
	};

	this.getInvoiceTaxableValue = function () {
		return this.invoiceTaxableValue;
	};

	this.setInvoiceTaxableValue = function (taxableValue) {
		this.invoiceTaxableValue = taxableValue;
	};

	this.getIGSTAmt = function () {
		return this.IGSTAmt;
	};

	this.setIGSTAmt = function (IGSTAmt) {
		this.IGSTAmt = IGSTAmt;
	};

	this.getSGSTAmt = function () {
		return this.SGSTAmt;
	};

	this.setSGSTAmt = function (SGSTAmt) {
		this.SGSTAmt = SGSTAmt;
	};

	this.getCGSTAmt = function () {
		return this.CGSTAmt;
	};

	this.setCGSTAmt = function (CGSTAmt) {
		this.CGSTAmt = CGSTAmt;
	};

	this.getCESSAmt = function () {
		return this.CESSAmt;
	};

	this.setCESSAmt = function (CESSAmt) {
		this.CESSAmt = CESSAmt;
	};

	this.getOTHERAmt = function () {
		return this.OTHERAmt;
	};

	this.setOTHERAmt = function (OTHERAmt) {
		this.OTHERAmt = OTHERAmt;
	};

	this.getAdditionalCessAmt = function () {
		return this.AdditionalCessAmt;
	};

	this.setAdditionalCessAmt = function (AdditionalCessAmt) {
		this.AdditionalCessAmt = AdditionalCessAmt;
	};

	this.getPlaceOfSupply = function () {
		return this.POS;
	};

	this.setPlaceOfSupply = function (POS) {
		this.POS = POS;
	};

	this.getEntryIncorrect = function () {
		return this.isEntryIncorrect;
	};
	this.setEntryIncorrect = function (entryIncorrect) {
		this.isEntryIncorrect = entryIncorrect;
	};

	this.getReverseCharge = function () {
		return this.reverseCharge;
	};
	this.setReverseCharge = function (reverseCharge) {
		this.reverseCharge = reverseCharge;
	};

	this.getTransactionType = function () {
		return this.transactionType;
	};
	this.setTransactionType = function (transactionType) {
		this.transactionType = transactionType;
	};
	this.getTransactionDescription = function () {
		return this.transactionDescription;
	};
	this.setTransactionDescription = function (transactionDescription) {
		this.transactionDescription = transactionDescription;
	};
};

module.exports = GSTR2ReportObject;