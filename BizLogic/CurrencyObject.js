var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require("babel-runtime/helpers/createClass");

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CurrencyObject = function () {
	function CurrencyObject(symbol) {
		var integerPartWord = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
		var decimalPartWord = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
		(0, _classCallCheck3.default)(this, CurrencyObject);

		this._symbol = symbol;
		this._integerPartWord = integerPartWord;
		this._decimalPartWord = decimalPartWord;
	}

	(0, _createClass3.default)(CurrencyObject, [{
		key: "symbol",
		get: function get() {
			return this._symbol;
		}
	}, {
		key: "integerPartWord",
		get: function get() {
			return this._integerPartWord;
		}
	}, {
		key: "decimalPartWord",
		get: function get() {
			return this._decimalPartWord;
		}
	}]);
	return CurrencyObject;
}();

module.exports = CurrencyObject;