var _setImmediate2 = require('babel-runtime/core-js/set-immediate');

var _setImmediate3 = _interopRequireDefault(_setImmediate2);

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var logger = require('../Utilities/logger');
var GoogleDrive = function GoogleDrive() {
	var fs = require('fs');
	var path = require('path');
	var callbackAfterUploadCompleted;
	var google = require('googleapis');
	//google.options({ adapter: require('axios/lib/adapters/http') });
	var GoogleAuth = google.auth.OAuth2;

	// If modifying these scopes, delete your previously saved credentials
	// at ~/.credentials/drive-nodejs-quickstart.json
	var SCOPES = ['https://www.googleapis.com/auth/drive.file', 'https://www.googleapis.com/auth/gmail.send'];

	var app = require('electron').remote.app;

	var appPath = app.getPath('userData');
	var TOKEN_DIR = appPath + '/BusinessNames/Credentials/';
	var TOKEN_PATH = TOKEN_DIR + 'accessToken.json';
	var clientSecret = null;

	this.UploadDBToDrive = function (name, fileNameForBackup, onUploadComplete) {
		// Load client secrets from a local file.
		callbackAfterUploadCompleted = onUploadComplete;
		var filePath = path.join(__dirname, '/../Assets/client_secret_drive.json');

		fs.readFile(filePath, function processClientSecrets(err, content) {
			if (err) {
				logger.error('Error loading client secret file: ' + err);
				if (fs.existsSync(fileNameForBackup)) {
					fs.unlink(fileNameForBackup, function (err) {
						if (err) {
							logger.error(err);
						}
					});
				}
				return false;
			}
			// Authorize a client with the loaded credentials, then call the
			// Drive API.
			clientSecret = JSON.parse(content);
			return authorize(clientSecret, uploadFile);
		});

		/**
     * Create an OAuth2 client with the given credentials, and then execute the
     * given callback function.
     *
     * @param {Object} credentials The authorization client credentials.
     * @param {function} callback The callback to call with the authorized client.
     */
		function authorize(credentials, callback) {
			var clientSecret = credentials.installed.client_secret;
			var clientId = credentials.installed.client_id;
			var redirectUrl = credentials.installed.redirect_uris[0];
			var oauth2Client = new GoogleAuth(clientId, clientSecret, redirectUrl);

			// Check if we have previously stored a token.
			fs.readFile(TOKEN_PATH, function (err, token) {
				if (err) {
					return getNewToken(oauth2Client, callback);
				} else {
					oauth2Client.setCredentials(JSON.parse(token));
					oauth2Client.refreshAccessToken(function (err) {
						if (err && (err.code === 401 || err.message === 'invalid_grant' || err.code == 403 || err.message.toLowerCase() === 'insufficient permission')) {
							fs.unlink(TOKEN_PATH, function (err) {});
							authorize(credentials, callback);
							return false;
						}
						return callback(oauth2Client);
					});
				}
			});
		}

		/**
     * Get and store new token after prompting for user authorization, and then
     * execute the given callback with the authorized OAuth2 client.
     *
     * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
     * @param {getEventsCallback} callback The callback to call with the authorized
     *     client.
     */
		function getNewToken(oauth2Client, callback) {
			var authUrl = oauth2Client.generateAuthUrl({
				access_type: 'offline',
				scope: SCOPES
			});
			var code = function code() {
				return authorizeApp(authUrl);
			};
			code().then(function (result) {
				oauth2Client.getToken(result, function (err, token) {
					if (err) {
						logger.error('Error while trying to retrieve access token', err);
						if (fs.existsSync(fileNameForBackup)) {
							fs.unlink(fileNameForBackup, function (err) {
								if (err) {
									logger.error(err);
								}
							});
						}
						return false;
					}
					oauth2Client.credentials = token;
					storeToken(token);
					ToastHelper.info('Started Backup to Google Drive. Please continue with your work.');
					return callback(oauth2Client);
				});
			}, callbackAfterUploadCompleted);
		}

		/**
     * Store token to disk be used in later program executions.
     *
     * @param {Object} token The token to store to disk.
     */
		function storeToken(token) {
			try {
				fs.mkdirSync(TOKEN_DIR);
			} catch (err) {
				if (err.code != 'EEXIST') {
					throw err;
				}
			}
			fs.writeFile(TOKEN_PATH, (0, _stringify2.default)(token), function (err) {});
		}

		function uploadFile(auth) {
			var drive = google.drive({ version: 'v3', auth: auth, client: auth });
			// var name = fileName.split('\\');
			// name = name[name.length - 1];
			drive.files.create({
				resource: {
					name: name,
					mimeType: 'application/x-sqlite3'
				},
				media: {
					mimeType: 'application/x-sqlite3',
					body: fs.createReadStream(fileNameForBackup) // read streams are awesome!
				}
			}, function (err, response) {
				if (fs.existsSync(fileNameForBackup)) {
					fs.unlink(fileNameForBackup, function (err) {
						if (err) {
							logger.error(err);
						}
					});
				}
				if (err) {
					if (err.code === 401 || err.message === 'invalid_grant' || err.code == 403 || err.message.toLowerCase() === 'insufficient permission') {
						fs.unlink(TOKEN_PATH, function (err) {});
						authorize(clientSecret, uploadFile);
						return false;
					}
					logger.error('error:', err);
				} else {
					var SettingsModel = require('./../Models/SettingsModel.js');
					var settingsModel = new SettingsModel();
					settingsModel.setBackupDate();
				}
				callbackAfterUploadCompleted && callbackAfterUploadCompleted(err, response);
			});
		}
		// ------------------------------------------------------------------------------------

		function authorizeApp(url) {
			return new _promise2.default(function (resolve, reject) {

				try {
					//Changes for GAuth wrt. bug user is not able to take backup on GDrive.
					//Creating two browser window to authenticate user.
					//GAuth is not working directly with user-agent.
					//Workaround: First request is without user-agent, once we load it without user-agent then again we are trying to load it with user-agent.
					var browserWindowParams = {
						useContentSize: true,
						center: true,
						show: false,
						resizable: false,
						autoHideMenuBar: true,
						alwaysOnTop: false
					};

					var BrowserWindow = require('electron').remote.BrowserWindow;

					var win = new BrowserWindow(browserWindowParams || { 'use-content-size': true });
					win.webContents.on('did-finish-load', function () {
						var browserWindowParams1 = {
							useContentSize: true,
							center: true,
							show: true,
							resizable: false,
							autoHideMenuBar: true,
							alwaysOnTop: true
						};
						var win1 = new BrowserWindow(browserWindowParams1 || { 'use-content-size': true });
						win.close();
						win1.loadURL(url, { userAgent: 'Chrome' });
						win1.on('closed', function () {
							reject(new Error('User closed the window'));
							if (fs.existsSync(fileNameForBackup)) {
								fs.unlink(fileNameForBackup, function (err) {
									if (err) {
										logger.error(err);
									}
								});
							}
						});
						win1.on('page-title-updated', function () {
							(0, _setImmediate3.default)(function () {
								var title = win1.getTitle();
								if (title.startsWith('Denied')) {
									reject(new Error(title.split(/[ =]/)[2]));
									win1.removeAllListeners('closed');
									callbackAfterUploadCompleted(new Error('User closed the window'));
									// ToastHelper.error('You have denied access for auto backup on google drive.');
									win1.close();
								} else if (title.startsWith('Success')) {
									resolve(title.split(/[ =]/)[2]);
									win1.removeAllListeners('closed');
									win1.close();
									ToastHelper.info('Started Backup to Google Drive. Please continue with your work.');
								}
							});
						});
					});
					win.loadURL(url);
				} catch (ex) {
					reject(new Error('Unable to load the window. Please contact Vyapar team.'));
				}
			});
		}
	};
};

module.exports = GoogleDrive;