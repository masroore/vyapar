var ExpenseTransaction = function ExpenseTransaction() {
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var ErrorCode = require('./../Constants/ErrorCode.js');
	var cashAmount, txnType, txnRefNumber;
	this.taxPercent = '0.0';
	this.discountPercent = '0.0';
	this.groupId = null;

	this.getGroupId = function () {
		return this.groupId;
	};

	this.getCashAmount = function () {
		return this.cashAmount;
	};
	this.setCashAmount = function (cashAmount) {
		if (!cashAmount) {
			this.cashAmount = '0.0';
		} else if (!isNaN(Number(cashAmount))) {
			this.cashAmount = Number(cashAmount);
		} else {
			return 'ERROR_TXN_INVALID_AMOUNT';
		}
		return 'SUCCESS';
	};

	this.getBalanceAmount = function () {
		return 0;
	};
	this.setBalanceAmount = function (balanceAmount) {
		return 'SUCCESS';
	};

	this.setAmounts = function (totalAmount, cashAmount) {
		var statusCode = 'SUCCESS';
		console.log('Cash Amount:' + cashAmount);
		console.log(totalAmount);
		statusCode = this.validateTotalAmount(totalAmount);
		if (statusCode == 'SUCCESS') {
			totalAmount = Number(totalAmount);
			cashAmount = Number(cashAmount);
			if (!cashAmount) {
				cashAmount = 0.0;
			}
			if (totalAmount < cashAmount) {
				statusCode = ErrorCode.ERROR_TXN_TOTAL_LESS_THAN_CASH;
			} else {
				this.setCashAmount(cashAmount);
				this.setBalanceAmount(totalAmount - cashAmount);
			}
		}
		return statusCode;
	};

	this.getTxnType = function () {
		this.txnType = TxnTypeConstant['TXN_TYPE_EXPENSE'];
		return this.txnType;
	};

	this.setDiscountPercent = function (lineItems) {
		this.discountPercent = 0.0;
	};
	this.getDiscountPercent = function () {
		this.discountPercent = 0.0;
		return this.discountPercent;
	};

	this.setTaxPercent = function (lineItems) {
		this.taxPercent = 0.0;
	};
	this.getTaxPercent = function () {
		this.taxPercent = 0.0;
		return this.taxPercent;
	};

	this.setTxnRefNumber = function (txnRefNumber) {
		this.txnRefNumber = txnRefNumber;
	};
	this.getTxnRefNumber = function () {
		return this.txnRefNumber;
	};

	this.getTxnCurrentBalanceAmount = function () {
		return 0; // Todo, Not reading from DB due to bug. Has to be fixed by DB migration
	};
};

module.exports = ExpenseTransaction;