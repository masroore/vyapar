var _typeof2 = require('babel-runtime/helpers/typeof');

var _typeof3 = _interopRequireDefault(_typeof2);

var _assign = require('babel-runtime/core-js/object/assign');

var _assign2 = _interopRequireDefault(_assign);

var _set = require('babel-runtime/core-js/set');

var _set2 = _interopRequireDefault(_set);

var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _create = require('babel-runtime/core-js/object/create');

var _create2 = _interopRequireDefault(_create);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ImportItemsXL = function ImportItemsXL() {
	var _Object$create2;

	var StringConstants = require('../Constants/StringConstants');
	var logger = require('../Utilities/logger');
	var itemFieldHeaders = {
		ITEM_NAME: 'Item name*',
		ITEM_CODE: 'Item code',
		ITEM_DESCRIPTION: 'Description',
		ITEM_CATEGORY: 'Category',
		HSN: 'HSN',
		SALE_PRICE: 'Sale price',
		PURCHASE_PRICE: 'Purchase price',
		OPENING_STOCK_QUANTITY: 'Opening stock quantity',
		MINIMUM_STOCK_QUANTITY: 'Minimum stock quantity',
		ITEM_LOCATION: 'Item Location',
		TAX_RATE: 'Tax Rate',
		INCLUSIVE_OF_TAX: 'Inclusive Of Tax',
		BASE_UNIT: 'Base Unit (x)',
		SECONDARY_UNIT: 'Secondary Unit (y)',
		CONVERSION_RATE: 'Conversion Rate (n) (x = ny)'
	};

	var itemDefinations = (0, _create2.default)((_Object$create2 = {}, (0, _defineProperty3.default)(_Object$create2, itemFieldHeaders.ITEM_CODE, {
		validExcelHeaders: [itemFieldHeaders.ITEM_CODE.toLowerCase()],
		mappableExcelHeaders: [itemFieldHeaders.ITEM_CODE.toLowerCase()],
		validateAndSetValue: function validateAndSetValue(val) {
			return val ? val.trim() : '';
		}
	}), (0, _defineProperty3.default)(_Object$create2, itemFieldHeaders.ITEM_NAME, {
		validExcelHeaders: [itemFieldHeaders.ITEM_NAME.toLowerCase()],
		mappableExcelHeaders: [itemFieldHeaders.ITEM_NAME.toLowerCase()],
		validateAndSetValue: function validateAndSetValue(val) {
			return val ? val.trim() : '';
		}
	}), (0, _defineProperty3.default)(_Object$create2, itemFieldHeaders.HSN, {
		validExcelHeaders: [itemFieldHeaders.HSN.toLowerCase()],
		mappableExcelHeaders: [itemFieldHeaders.HSN.toLowerCase()],
		validateAndSetValue: function validateAndSetValue(val) {
			val = val ? val.trim() : '';
			if (!StringConstants.positiveIntRegex.test(val)) {
				var err = new Error(StringConstants.IMPORT_ITEM_INVALID_HSN);
				return err;
			}
			return val;
		}
	}), (0, _defineProperty3.default)(_Object$create2, itemFieldHeaders.SALE_PRICE, {
		validExcelHeaders: [itemFieldHeaders.SALE_PRICE.toLowerCase()],
		mappableExcelHeaders: [itemFieldHeaders.SALE_PRICE.toLowerCase()],
		validateAndSetValue: function validateAndSetValue(val) {
			val = val ? val.trim() : '';
			if (!StringConstants.positiveNumRegex.test(val)) {
				var err = new Error(StringConstants.IMPORT_ITEM_INVALID_SALE_PRICE);
				return err;
			}
			return val;
		}
	}), (0, _defineProperty3.default)(_Object$create2, itemFieldHeaders.PURCHASE_PRICE, {
		validExcelHeaders: [itemFieldHeaders.PURCHASE_PRICE.toLowerCase()],
		mappableExcelHeaders: [itemFieldHeaders.PURCHASE_PRICE.toLowerCase()],
		validateAndSetValue: function validateAndSetValue(val) {
			val = val ? val.trim() : '';
			if (!StringConstants.positiveNumRegex.test(val)) {
				var err = new Error(StringConstants.IMPORT_ITEM_INVALID_PURCHASE_PRICE);
				return err;
			}
			return val;
		}
	}), (0, _defineProperty3.default)(_Object$create2, itemFieldHeaders.OPENING_STOCK_QUANTITY, {
		validExcelHeaders: [itemFieldHeaders.OPENING_STOCK_QUANTITY.toLowerCase(), 'current stock quantity', 'current stock'],
		mappableExcelHeaders: [itemFieldHeaders.OPENING_STOCK_QUANTITY.toLowerCase(), 'opening stock', 'current stock', 'current quantity', 'current stock quantity'],
		validateAndSetValue: function validateAndSetValue(val) {
			val = val ? val.trim() : '';
			val = val == '-' || val == '.' ? '' : val;
			if (isNaN(Number(val))) {
				var err = new Error(StringConstants.IMPORT_ITEM_INVALID_OPEN_STOCK);
				return err;
			}
			return val;
		}
	}), (0, _defineProperty3.default)(_Object$create2, itemFieldHeaders.MINIMUM_STOCK_QUANTITY, {
		validExcelHeaders: [itemFieldHeaders.MINIMUM_STOCK_QUANTITY.toLowerCase(), 'min stock'],
		mappableExcelHeaders: [itemFieldHeaders.MINIMUM_STOCK_QUANTITY.toLowerCase(), 'min stock', 'minimum stock', 'min quantity', 'minimum quantity'],
		validateAndSetValue: function validateAndSetValue(val) {
			val = val ? val.trim() : '';
			val = val == '-' || val == '.' ? '' : val;
			if (isNaN(Number(val))) {
				var err = new Error(StringConstants.IMPORT_ITEM_INVALID_MIN_STOCK);
				return err;
			}
			return val;
		}
	}), (0, _defineProperty3.default)(_Object$create2, itemFieldHeaders.ITEM_LOCATION, {
		validExcelHeaders: [itemFieldHeaders.ITEM_LOCATION.toLowerCase()],
		mappableExcelHeaders: [itemFieldHeaders.ITEM_LOCATION.toLowerCase()],
		validateAndSetValue: function validateAndSetValue(val) {
			return val ? val.trim() : '';
		}
	}), (0, _defineProperty3.default)(_Object$create2, itemFieldHeaders.TAX_RATE, {
		validExcelHeaders: [itemFieldHeaders.TAX_RATE.toLowerCase()],
		mappableExcelHeaders: [itemFieldHeaders.TAX_RATE.toLowerCase()],
		validateAndSetValue: function validateAndSetValue(val) {
			return val ? val.trim() : '';
		}
	}), (0, _defineProperty3.default)(_Object$create2, itemFieldHeaders.INCLUSIVE_OF_TAX, {
		validExcelHeaders: [itemFieldHeaders.INCLUSIVE_OF_TAX.toLowerCase(), 'tax type'],
		mappableExcelHeaders: [itemFieldHeaders.INCLUSIVE_OF_TAX.toLowerCase()],
		validateAndSetValue: function validateAndSetValue(val) {
			val = val ? val.trim() : '';
			var inclusiveSynonyms = ['inclusive', 'y'];
			if (inclusiveSynonyms.includes(val.toLowerCase())) {
				val = 'Inclusive';
			} else {
				val = 'Exclusive';
			}
			return val;
		}
	}), (0, _defineProperty3.default)(_Object$create2, itemFieldHeaders.ITEM_CATEGORY, {
		validExcelHeaders: [itemFieldHeaders.ITEM_CATEGORY.toLowerCase()],
		mappableExcelHeaders: [itemFieldHeaders.ITEM_CATEGORY.toLowerCase()],
		validateAndSetValue: function validateAndSetValue(val) {
			return val ? val.trim() : '';
		}
	}), (0, _defineProperty3.default)(_Object$create2, itemFieldHeaders.BASE_UNIT, {
		validExcelHeaders: [itemFieldHeaders.BASE_UNIT.toLowerCase()],
		mappableExcelHeaders: [itemFieldHeaders.BASE_UNIT.toLowerCase()],
		validateAndSetValue: function validateAndSetValue(val) {
			return val ? val.trim() : '';
		}
	}), (0, _defineProperty3.default)(_Object$create2, itemFieldHeaders.SECONDARY_UNIT, {
		validExcelHeaders: [itemFieldHeaders.SECONDARY_UNIT.toLowerCase()],
		mappableExcelHeaders: [itemFieldHeaders.SECONDARY_UNIT.toLowerCase()],
		validateAndSetValue: function validateAndSetValue(val) {
			return val ? val.trim() : '';
		}
	}), (0, _defineProperty3.default)(_Object$create2, itemFieldHeaders.CONVERSION_RATE, {
		validExcelHeaders: [itemFieldHeaders.CONVERSION_RATE.toLowerCase()],
		mappableExcelHeaders: [itemFieldHeaders.CONVERSION_RATE.toLowerCase()],
		validateAndSetValue: function validateAndSetValue(val) {
			val = val ? val.trim() : '';
			if (!StringConstants.positiveNumRegex.test(val)) {
				var err = new Error(StringConstants.IMPORT_ITEM_INVALID_CONVERSION_RATE);
				return err;
			}
			return val;
		}
	}), (0, _defineProperty3.default)(_Object$create2, itemFieldHeaders.ITEM_DESCRIPTION, {
		validExcelHeaders: [itemFieldHeaders.ITEM_DESCRIPTION.toLowerCase()],
		mappableExcelHeaders: [itemFieldHeaders.ITEM_DESCRIPTION.toLowerCase()],
		validateAndSetValue: function validateAndSetValue(val) {
			return val ? val.trim() : '';
		}
	}), _Object$create2));

	this.ItemDefinations = itemDefinations;
	this.ItemFieldHeaders = itemFieldHeaders;

	this.validateHeaders = function (excelHeaders, enabledItems) {
		if (excelHeaders.length != enabledItems.length) {
			return false;
		}
		return excelHeaders.every(function (element, index) {
			var field = element || '';
			return itemDefinations[enabledItems[index]].validExcelHeaders.includes(field.trim().toLowerCase());
		});
	};

	this.getEnabledItemFields = function () {
		var Queries = require('./../Constants/Queries.js');
		var SettingCache = require('./../Cache/SettingCache.js');
		var settingCache = new SettingCache();
		var isItemCategoryEnabled = settingCache.isItemCategoryEnabled();
		var isItemUnitEnabled = settingCache.getItemUnitEnabled();
		var isStockEnabled = settingCache.getStockEnabled();
		var isItemDescriptionEnabled = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_DESCRIPTION);

		var headers = (0, _extends3.default)({}, itemFieldHeaders);
		if (!isStockEnabled) {
			delete headers.MINIMUM_STOCK_QUANTITY;
			delete headers.OPENING_STOCK_QUANTITY;
			delete headers.ITEM_LOCATION;
		}
		if (!isItemUnitEnabled) {
			delete headers.BASE_UNIT;
			delete headers.SECONDARY_UNIT;
			delete headers.CONVERSION_RATE;
		}
		if (!isItemCategoryEnabled) {
			delete headers.ITEM_CATEGORY;
		}
		if (!isItemDescriptionEnabled) {
			delete headers.ITEM_DESCRIPTION;
		}
		return (0, _values2.default)(headers);
	};

	this.parseExcelData = function (mappedData, enabledItems) {
		var validItems = [];
		var invalidItems = [];

		var _getCurrentItemNamesA = getCurrentItemNamesAndItemCodes(),
		    itemNamesArr = _getCurrentItemNamesA.itemNamesArr,
		    itemCodesArr = _getCurrentItemNamesA.itemCodesArr;

		var validItemNames = new _map2.default();
		var validItemCodes = new _map2.default();
		itemNamesArr.forEach(function (itemName) {
			validItemNames.set(itemName.trim().toLowerCase());
		});
		itemCodesArr.forEach(function (itemCode) {
			validItemCodes.set(itemCode.trim().toLowerCase());
		});
		var invalidItemNames = new _set2.default();
		var invalidItemCodes = new _set2.default();
		var validatedData = mappedData.map(function (item) {
			return getValidatedItemRow({ itemRow: item, itemFields: enabledItems });
		});
		var validateItemNamesAndCodes = function validateItemNamesAndCodes(itemRow) {
			var itemName = itemRow[itemFieldHeaders.ITEM_NAME];
			var itemCode = itemRow[itemFieldHeaders.ITEM_CODE];
			// Validate for item name and item code
			if (itemRow.isRowValid) {
				var value = itemName.value || '';
				value = value.trim().toLowerCase();
				if (validItemNames.has(value)) {
					itemName.isValid = false;
					itemName.error = new Error(StringConstants.IMPORT_ITEM_NAME_DUPLICATE);
					itemRow.isRowValid = false;
				}
				if (itemCode.value) {
					if (validItemCodes.has(itemCode.value.trim().toLowerCase())) {
						itemCode.isValid = false;
						itemCode.error = new Error(StringConstants.IMPORT_ITEM_CODE_DUPLICATE);
						itemRow.isRowValid = false;
					}
				}
			} else {
				var _value = itemName.value || '';
				_value = _value.trim().toLowerCase();
				if (validItemNames.has(_value) || invalidItemNames.has(_value)) {
					itemName.isValid = false;
					itemName.error = new Error(StringConstants.IMPORT_ITEM_NAME_DUPLICATE);
				}
				if (itemCode.value) {
					var _value2 = itemCode.value.trim().toLowerCase();
					if (validItemCodes.has(_value2) || invalidItemCodes.has(_value2)) {
						itemCode.isValid = false;
						itemCode.error = new Error(StringConstants.IMPORT_ITEM_CODE_DUPLICATE);
					}
				}
			}
			// Update the valid and invalid sets
			if (itemRow.isRowValid) {
				if (itemName.value && !validItemNames.has(itemName.value.trim().toLowerCase())) {
					validItemNames.set(itemName.value.trim().toLowerCase());
				}
				if (itemCode.value && !validItemCodes.has(itemCode.value.trim().toLowerCase())) {
					validItemCodes.set(itemCode.value.trim().toLowerCase());
				}
			} else {
				if (itemName.value && !invalidItemNames.has(itemName.value.trim().toLowerCase())) {
					invalidItemNames.add(itemName.value.trim().toLowerCase());
				}
				if (itemCode.value && !invalidItemCodes.has(itemCode.value.trim().toLowerCase())) {
					invalidItemCodes.add(itemCode.value.trim().toLowerCase());
				}
			}
		};
		validatedData.forEach(validateItemNamesAndCodes);
		validatedData.forEach(function (itemRow) {
			if (itemRow.isRowValid) {
				validItems.push(itemRow);
			} else {
				invalidItems.push(itemRow);
			}
		});
		invalidItemCodes = null;
		invalidItemNames = null;
		validatedData = null;
		mappedData = null;
		return {
			validItems: validItems,
			invalidItems: invalidItems,
			itemNamesMap: validItemNames,
			itemCodesMap: validItemCodes
		};
	};

	function getCurrentItemNamesAndItemCodes() {
		var itemNamesArr = [];
		var itemCodesArr = [];
		var ItemCache = require('../Cache/ItemCache');
		var itemCache = new ItemCache();
		var items = itemCache.getListOfItemsObject();
		items.forEach(function (item) {
			var itemName = item.getItemName() || '';
			itemNamesArr.push(itemName.trim().toLowerCase());
			var itemCode = item.getItemCode();
			if (itemCode) {
				itemCodesArr.push(itemCode.trim().toLowerCase());
			}
		});
		return {
			itemNamesArr: itemNamesArr,
			itemCodesArr: itemCodesArr
		};
	}

	function getValidatedItemRow(_ref) {
		var itemRow = _ref.itemRow,
		    _ref$itemFields = _ref.itemFields,
		    itemFields = _ref$itemFields === undefined ? [] : _ref$itemFields;

		itemRow.isRowValid = true; // resetting.
		itemFields.forEach(function (itemField) {
			switch (itemField) {
				case itemFieldHeaders.ITEM_NAME:
					var itemName = itemRow[itemField].value ? itemRow[itemField].value.trim() : '';
					if (!itemName) {
						itemRow[itemField].isValid = false;
						var error = new Error(StringConstants.IMPORT_ITEM_NAME_EMPTY);
						itemRow[itemField].error = error;
						itemRow[itemField].value = '';
					} else {
						itemRow[itemField].isValid = true;
						itemRow[itemField].error = '';
					}
					itemRow.isRowValid = itemRow.isRowValid && itemRow[itemField].isValid;
					break;
				case itemFieldHeaders.BASE_UNIT:
					break; // preventing default
				case itemFieldHeaders.SECONDARY_UNIT:
					break; // preventing default
				case itemFieldHeaders.CONVERSION_RATE:
					var baseUnit = itemRow[itemFieldHeaders.BASE_UNIT];
					var secondaryUnit = itemRow[itemFieldHeaders.SECONDARY_UNIT];
					var conversionRate = itemRow[itemFieldHeaders.CONVERSION_RATE];
					// reset values
					(0, _assign2.default)(secondaryUnit, {
						error: '',
						isValid: true,
						value: secondaryUnit.value || ''
					});
					(0, _assign2.default)(conversionRate, {
						error: '',
						isValid: true,
						value: conversionRate.value || ''
					});
					(0, _assign2.default)(baseUnit, {
						error: '',
						isValid: true,
						value: baseUnit.value || ''
					});

					// if (conversionRate.value && !(baseUnit.value && secondaryUnit.value)) {
					// 	let error = new Error('Provide Base Unit and Secondary Unit for conversion rate.');
					// 	conversionRate.error = error;
					// 	conversionRate.isValid = false;
					// } else
					if (conversionRate.value) {
						var _validatedRes = itemDefinations[itemFieldHeaders.CONVERSION_RATE].validateAndSetValue(conversionRate.value);
						conversionRate.isValid = !(_validatedRes instanceof Error);
						conversionRate.error = _validatedRes instanceof Error ? _validatedRes : '';
						conversionRate.value = _validatedRes instanceof Error ? conversionRate.value : _validatedRes;
					}
					if (secondaryUnit.value && !(baseUnit.value && conversionRate.value)) {
						var _error = new Error(StringConstants.IMPORT_ITEM_BASEUNIT_CONVERSIONRATE_EMPTY);
						secondaryUnit.error = _error;
						secondaryUnit.isValid = false;
					}
					if (baseUnit.value && secondaryUnit.value && secondaryUnit.value.toLowerCase() === baseUnit.value.toLowerCase()) {
						var _error2 = new Error(StringConstants.IMPORT_ITEM_INVALID_SECONDARY_UNIT);
						secondaryUnit.error = _error2;
						secondaryUnit.isValid = false;
					}
					itemRow.isRowValid = itemRow.isRowValid && itemRow[itemFieldHeaders.SECONDARY_UNIT].isValid && itemRow[itemFieldHeaders.CONVERSION_RATE].isValid;
					break;
				default:
					var validatedRes = itemDefinations[itemField].validateAndSetValue(itemRow[itemField].value);
					itemRow[itemField].isValid = !(validatedRes instanceof Error);
					itemRow[itemField].error = validatedRes instanceof Error ? validatedRes : '';
					itemRow[itemField].value = validatedRes instanceof Error ? itemRow[itemField].value : validatedRes;
					itemRow.isRowValid = itemRow.isRowValid && itemRow[itemField].isValid;
					break;
			}
		});
		return itemRow;
	}

	this.saveImportedItems = function (listOfValidItems) {
		var ErrorCode = require('../Constants/ErrorCode');
		var DataInserter = require('./../DBManager/DataInserter.js');
		var dataInserter = new DataInserter();
		var statusCode = dataInserter.insertImportedItems(listOfValidItems);

		var dialog = require('electron').remote.dialog;

		dialog.showMessageBox({
			type: 'info',
			title: 'Import Message',
			message: statusCode
		});
		MyAnalytics.pushEvent(statusCode == ErrorCode.ERROR_ITEM_IMPORT_SUCCESS ? 'Import Items Successful' : 'Import Items Failed');
	};

	this.downloadXLFile = function () {
		var _dataObj;

		var Excel = require('excel4node');
		var Queries = require('./../Constants/Queries.js');
		var ItemCategoryCache = require('./../Cache/ItemCategoryCache');
		var TaxCodeCache = require('./../Cache/TaxCodeCache');
		var StringConstants = require('../Constants/StringConstants');
		var SettingCache = require('./../Cache/SettingCache.js');
		var settingCache = new SettingCache();
		var itemCategoryCache = new ItemCategoryCache();
		var taxCodeCache = new TaxCodeCache();
		var dataObj = (_dataObj = {
			rowCount: 7
		}, (0, _defineProperty3.default)(_dataObj, itemFieldHeaders.ITEM_NAME, ['Item 1', 'Item 2', 'Item 3', 'Item 4', 'Item 5', 'Item 6', 'Item 7']), (0, _defineProperty3.default)(_dataObj, itemFieldHeaders.ITEM_CODE, ['a101', 'a102', 'a103', 'a104', 'a105', 'a106', 'a107']), (0, _defineProperty3.default)(_dataObj, itemFieldHeaders.SALE_PRICE, [20, 30, 35, 20, 30, 35, 20]), (0, _defineProperty3.default)(_dataObj, itemFieldHeaders.PURCHASE_PRICE, [25, 35, 40, 25, 35, 37, 25]), (0, _defineProperty3.default)(_dataObj, itemFieldHeaders.OPENING_STOCK_QUANTITY, [10, 5, 15, 10, 5, 15, 10]), (0, _defineProperty3.default)(_dataObj, itemFieldHeaders.MINIMUM_STOCK_QUANTITY, [2, 0, 1, 2, 7, 1, 2]), (0, _defineProperty3.default)(_dataObj, itemFieldHeaders.ITEM_LOCATION, ['Store 1', 'Store 2', 'Store 1', 'Store 1', 'Store 2', 'Store 1', 'Store 1']), (0, _defineProperty3.default)(_dataObj, itemFieldHeaders.TAX_RATE, ['GST@3%', 'GST@18%', 'IGST@0%', 'GST@5%', 'IGST@3%', 'GST@28%', 'IGST@0%']), (0, _defineProperty3.default)(_dataObj, itemFieldHeaders.INCLUSIVE_OF_TAX, ['Inclusive', 'Exclusive', 'Inclusive', 'Exclusive', 'Inclusive', 'Inclusive', 'Inclusive']), _dataObj);

		var dialog = require('electron').remote.dialog;

		var wb = new Excel.Workbook();
		var ws = wb.addWorksheet('Template');
		//
		var columns = [itemFieldHeaders.ITEM_NAME, itemFieldHeaders.ITEM_CODE];

		if (settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_DESCRIPTION)) {
			columns.push(itemFieldHeaders.ITEM_DESCRIPTION);
		}
		if (settingCache.isItemCategoryEnabled()) {
			columns.push(itemFieldHeaders.ITEM_CATEGORY);
		}

		columns.push(itemFieldHeaders.HSN);
		columns.push(itemFieldHeaders.SALE_PRICE);
		columns.push(itemFieldHeaders.PURCHASE_PRICE);

		if (settingCache.getStockEnabled()) {
			columns.push(itemFieldHeaders.OPENING_STOCK_QUANTITY);
			columns.push(itemFieldHeaders.MINIMUM_STOCK_QUANTITY);
			columns.push(itemFieldHeaders.ITEM_LOCATION);
		}
		columns.push(itemFieldHeaders.TAX_RATE);
		columns.push(itemFieldHeaders.INCLUSIVE_OF_TAX);

		if (settingCache.getItemUnitEnabled()) {
			columns.push(itemFieldHeaders.BASE_UNIT);
			columns.push(itemFieldHeaders.SECONDARY_UNIT);
			columns.push(itemFieldHeaders.CONVERSION_RATE);
		}
		var headerStyle = wb.createStyle({
			font: {
				color: 'white',
				bold: true
			},
			alignment: {
				wrapText: true,
				horizontal: 'center',
				vertical: 'center'
			},
			fill: {
				type: 'pattern',
				patternType: 'solid',
				bgColor: '#4D82B8',
				fgColor: '#4D82B8' // HTML style hex value. defaults to black
			},
			border: { // §18.8.4 border (Border)
				left: {
					style: 'thin', // §18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
					color: 'black' // HTML style hex value
				},
				right: {
					style: 'thin',
					color: 'black'
				},
				top: {
					style: 'thin',
					color: 'black'
				},
				bottom: {
					style: 'thin',
					color: 'black'
				}
			}
		});
		var cellStyle = wb.createStyle({
			border: { // §18.8.4 border (Border)
				left: {
					style: 'thin', // §18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
					color: 'black' // HTML style hex value
				},
				right: {
					style: 'thin',
					color: 'black'
				},
				top: {
					style: 'thin',
					color: 'black'
				},
				bottom: {
					style: 'thin',
					color: 'black'
				}
			}
		});
		var notificationStyle = wb.createStyle({
			fill: {
				type: 'pattern',
				patternType: 'solid',
				bgColor: '#F79646',
				fgColor: '#F79646' // HTML style hex value. defaults to black
			},
			font: {
				extend: true
			}
		});
		for (var i = 1; i <= columns.length; i++) {
			ws.cell(1, i).string(columns[i - 1]).style(headerStyle);
			var arr = dataObj[columns[i - 1]] || [];
			var rowCount = dataObj.rowCount || 0;
			for (var rowIndex = 0; rowIndex < rowCount; rowIndex++) {
				var cellValue = arr[rowIndex];
				switch (typeof cellValue === 'undefined' ? 'undefined' : (0, _typeof3.default)(cellValue)) {
					case 'string':
						ws.cell(rowIndex + 2, i).string(arr[rowIndex]).style(cellStyle);
						break;
					case 'number':
						ws.cell(rowIndex + 2, i).number(arr[rowIndex]).style(cellStyle);
						break;
					default:
						ws.cell(rowIndex + 2, i).style(cellStyle);
						break;
				}
			}
			ws.cell(dataObj.rowCount + 2, i).style(notificationStyle);
		}
		ws.cell(dataObj.rowCount + 2, 1).string(StringConstants.SAMPLE_EXCEL_FOOTER_MESSAGE);

		var alphabet = 'ABCDEFGHIJKLMNOPQ'.split('');
		var categoryColLetter = columns.indexOf(itemFieldHeaders.ITEM_CATEGORY);
		var taxRateColLetter = columns.indexOf(itemFieldHeaders.TAX_RATE);
		var inclOfTaxColLetter = columns.indexOf(itemFieldHeaders.INCLUSIVE_OF_TAX);
		var conversionLetter = columns.indexOf(itemFieldHeaders.CONVERSION_RATE);
		var openStockLetter = columns.indexOf(itemFieldHeaders.OPENING_STOCK_QUANTITY);
		var minStockLetter = columns.indexOf(itemFieldHeaders.MINIMUM_STOCK_QUANTITY);
		// let baseUnitLetter = columns.indexOf('Base Unit');
		// let secondaryUnitLetter = columns.indexOf('Secondary Unit');
		var categoryList = itemCategoryCache.getItemCategoryNamesForExcel();
		var taxRateList = taxCodeCache.getTaxListForTransaction();
		taxRateList = taxRateList.map(function (obj) {
			return obj.getTaxCodeName();
		});
		taxRateList.unshift('');
		for (var _i = 0; _i < taxRateList.length; _i++) {
			ws.cell(_i + 2, 30).string(taxRateList[_i]);
		}
		ws.column(30).hide();
		if (settingCache.isItemCategoryEnabled()) {
			for (var _i2 = 0; _i2 < categoryList.length; _i2++) {
				ws.cell(_i2 + 2, 31).string(categoryList[_i2]);
			}
			var categoryListLength = categoryList.length + 1;
			ws.column(31).hide();
			ws.addDataValidation({
				type: 'list',
				allowBlank: true,
				showDropDown: true,
				sqref: alphabet[categoryColLetter] + '2:' + alphabet[categoryColLetter] + '10000',
				formulas: ['=$AE$2:$AE$' + categoryListLength]
			});
		}
		var taxRateListLength = taxRateList.length + 1;
		ws.addDataValidation({
			type: 'list',
			allowBlank: true,
			error: 'Invalid choice was chosen',
			showDropDown: true,
			sqref: alphabet[taxRateColLetter] + '2:' + alphabet[taxRateColLetter] + '10000',
			formulas: ['=$AD$2:$AD$' + taxRateListLength]
		});
		ws.addDataValidation({
			type: 'list',
			allowBlank: true,
			error: 'Invalid choice was chosen',
			showDropDown: true,
			sqref: alphabet[inclOfTaxColLetter] + '2:' + alphabet[inclOfTaxColLetter] + '10000',
			formulas: ['Inclusive, Exclusive']
		});
		if (settingCache.getStockEnabled()) {
			ws.addDataValidation({
				type: 'decimal',
				allowBlank: true,
				operator: 'between',
				error: 'Please enter a number',
				showDropDown: true,
				sqref: alphabet[openStockLetter] + '2:' + alphabet[openStockLetter] + '10000',
				formulas: [-9999999999, 99999999999]
			});
			ws.addDataValidation({
				type: 'decimal',
				allowBlank: true,
				operator: 'between',
				error: 'Please enter a number',
				showDropDown: true,
				sqref: alphabet[minStockLetter] + '2:' + alphabet[minStockLetter] + '10000',
				formulas: [-9989898898, 99999999999]
			});
		}
		if (settingCache.getItemUnitEnabled()) {
			ws.addDataValidation({
				type: 'whole',
				allowBlank: true,
				operator: 'between',
				error: 'Please enter a value greater than 0',
				showDropDown: true,
				sqref: alphabet[conversionLetter] + '2:' + alphabet[conversionLetter] + '10000',
				formulas: [1, 99999999999]
			});
		}

		var fileName = dialog.showSaveDialog({
			title: 'Save sample pdf',
			defaultPath: 'Import Items Template',
			filters: [{
				name: 'Excel File',
				extensions: ['xlsx']
			}]
		});
		if (!fileName) {
			return;
		}
		wb.write(fileName, function (err, stats) {
			if (err) {
				logger.error(err);
			} else {
				ToastHelper.success('Download Successful'); // Prints out an instance of a node.js fs.Stats object
				var FileUtil = require('../Utilities/FileUtil');
				FileUtil.showFileInFolder(fileName);
			}
		});
	};
};
var importItemsXL = new ImportItemsXL();
module.exports = importItemsXL;