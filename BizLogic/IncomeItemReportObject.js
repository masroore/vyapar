var IncomeItemReportObject = function IncomeItemReportObject() {
	var itemId;
	var itemName = '';
	var qty = 0;
	var amount = 0;

	this.getItemId = function () {
		return this.itemId;
	};

	this.setItemId = function (itemId) {
		this.itemId = itemId;
	};

	this.getItemName = function () {
		return this.itemName;
	};

	this.setItemName = function (itemName) {
		this.itemName = itemName;
	};

	this.getQty = function () {
		return this.qty;
	};

	this.setQty = function (qty) {
		this.qty = qty;
	};

	this.getAmount = function () {
		return this.amount;
	};

	this.setAmount = function (amount) {
		this.amount = amount;
	};
};

module.exports = IncomeItemReportObject;