var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require("babel-runtime/helpers/createClass");

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function () {
	return function () {
		function TransactionMessageConfig(type, id, name, value) {
			(0, _classCallCheck3.default)(this, TransactionMessageConfig);

			this.txnType = type;
			this.txnFieldId = id;
			this.txnFieldName = name;
			this.txnFieldValue = value;
		}

		(0, _createClass3.default)(TransactionMessageConfig, [{
			key: "txnType",
			get: function get() {
				return this._txnType;
			},
			set: function set(txnType) {
				this._txnType = txnType;
			}
		}, {
			key: "txnFieldId",
			get: function get() {
				return this._txnFieldId;
			},
			set: function set(txnFieldId) {
				this._txnFieldId = txnFieldId;
			}
		}, {
			key: "txnFieldName",
			get: function get() {
				return this._txnFieldName;
			},
			set: function set(txnFieldName) {
				this._txnFieldName = txnFieldName;
			}
		}, {
			key: "txnFieldValue",
			get: function get() {
				return this._txnFieldValue;
			},
			set: function set(txnFieldValue) {
				this._txnFieldValue = txnFieldValue;
			}
		}]);
		return TransactionMessageConfig;
	}();
}();