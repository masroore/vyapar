var PaymentTermLogic = function PaymentTermLogic() {
	var paymenttermId = void 0;
	var paymenttermName = void 0;
	var paymenttermDays = void 0;
	var paymenttermIsdefault = void 0;

	this.getPaymentTermId = function () {
		return paymenttermId;
	};
	this.setPaymentTermId = function (id) {
		paymenttermId = id;
	};
	this.getPaymentTermName = function () {
		return paymenttermName;
	};
	this.setPaymentTermName = function (paymentTermName) {
		paymenttermName = paymentTermName;
	};
	this.getPaymentTermDays = function () {
		return paymenttermDays;
	};
	this.setPaymentTermDays = function (paymentTermDays) {
		paymenttermDays = paymentTermDays;
	};
	this.getPaymentTermIsDefault = function () {
		return paymenttermIsdefault;
	};
	this.setPaymentTermIsDefault = function (paymentTermIsDefault) {
		paymenttermIsdefault = paymentTermIsDefault;
	};
};

module.exports = PaymentTermLogic;