var ItemCategoryCache = require('./../Cache/ItemCategoryCache.js');
var TransactionManager = require('./../DBManager/TransactionManager.js');
var ItemCategoryModel = require('./../Models/ItemCategoryModel.js');
var ItemCategory = function ItemCategory() {
	this.categoryId = 0;
	this.categoryName = '';
	this.memberCount = 0;

	var ErrorCode = require('./../Constants/ErrorCode.js');
	var ItemType = require('./../Constants/ItemType.js');

	this.getCategoryId = function () {
		return this.categoryId;
	};
	this.setCategoryId = function (categoryId) {
		this.categoryId = categoryId;
	};

	this.getCategoryName = function () {
		return this.categoryName;
	};
	this.setCategoryName = function (categoryName) {
		this.categoryName = categoryName;
	};

	this.getMemberCount = function () {
		return this.memberCount;
	};
	this.setMemberCount = function (memberCount) {
		this.memberCount = memberCount;
	};

	this.getCategoryNameOnId = function (categoryId) {
		var categoryName = '';
		if (categoryId > 0) {
			var ItemCategoryModel = require('./../Models/ItemCategoryModel.js');
			var model = new ItemCategoryModel();
			model.setCategoryId(categoryId);
			model.loadCategoryNameForId();
			categoryName = model.getCategoryName();
		}
		return categoryName;
	};

	this.saveNewCategory = function (categoryName) {
		var itemCategoryCache = new ItemCategoryCache();
		var statusCode = ErrorCode.ERROR_ITEMCATEGORY_SAVE_FAILED;
		if (!categoryName || !categoryName.trim()) {
			statusCode = ErrorCode.ERROR_ITEMCATEGORY_NAME_EMPTY_FAILED;
			return statusCode;
		}
		this.categoryName = categoryName.trim();
		if (itemCategoryCache.getItemCategoryId(this.categoryName) > 0) {
			statusCode = ErrorCode.ERROR_ITEMCATEGORY_ALREADYEXISTS;
			return statusCode;
		}
		var itemCategoryModel = new ItemCategoryModel();
		itemCategoryModel.setCategoryName(this.categoryName);
		var transactionManager = new TransactionManager();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_ITEM_SAVE_FAILED;
			return statusCode;
		}

		statusCode = itemCategoryModel.addNewCategory();
		if (statusCode == ErrorCode.ERROR_ITEMCATEGORY_SAVE_SUCCESS) {
			this.categoryId = itemCategoryModel.getCategoryId();

			if (!transactionManager.CommitTransaction()) {
				statusCode = ErrorCode.ERROR_ITEM_SAVE_FAILED;
			} else {
				itemCategoryCache.refreshItemCategory(this);
			}
		}
		transactionManager.EndTransaction();
		return statusCode;
	};

	this.editItemCategory = function (oldName, newName) {
		var itemCategoryCache = new ItemCategoryCache();
		var statusCode = ErrorCode.ERROR_ITEMCATEGORY_UPDATE_FAILED;
		if (!newName || !newName.trim()) {
			statusCode = ErrorCode.ERROR_ITEMCATEGORY_NAME_EMPTY_FAILED;
			return statusCode;
		}
		isCategoryGeneral = itemCategoryCache.getItemCategoryId(oldName) == 1;
		if (isCategoryGeneral) {
			statusCode = ErrorCode.ERROR_GENERALCATEGORY_NOT_EDITABLE;
			return statusCode;
		}
		this.categoryName = newName.trim();
		if (itemCategoryCache.getItemCategoryId(this.categoryName) > 0) {
			statusCode = ErrorCode.ERROR_ITEMCATEGORY_ALREADYEXISTS;
			return statusCode;
		}
		var oldCategoryId = itemCategoryCache.getItemCategoryId(oldName);
		var itemCategoryModel = new ItemCategoryModel();
		itemCategoryModel.setCategoryName(this.categoryName);
		var transactionManager = new TransactionManager();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_ITEMCATEGORY_UPDATE_FAILED;
			return statusCode;
		}

		statusCode = itemCategoryModel.editItemCategory(oldCategoryId);
		if (statusCode == ErrorCode.ERROR_ITEMCATEGORY_UPDATE_SUCCESS) {
			if (!transactionManager.CommitTransaction()) {
				statusCode = ErrorCode.ERROR_ITEMCATEGORY_UPDATE_FAILED;
			} else {
				itemCategoryCache.refreshItemCategory(this);
			}
		}
		transactionManager.EndTransaction();
		return statusCode;
	};

	this.deleteItemCategory = function () {
		// let ItemCategoryModel = require('./../Model/ItemCategoryModel.js');
		var itemCategoryModel = new ItemCategoryModel();
		var canDelete = false;
		var statusCode = ErrorCode.ERROR_ITEMCATEGORY_DELETE_FAILED;
		var categoryId = this.getCategoryId();
		isCategoryGeneral = categoryId == 1;
		if (isCategoryGeneral) {
			statusCode = ErrorCode.ERROR_GENERALCATEGORY_NOT_EDITABLE;
			return statusCode;
		}
		canDelete = itemCategoryModel.canDeleteItemCategory(categoryId);
		if (!canDelete) {
			ToastHelper.error('This category cannot be deleted since there are few items under this. Please move them and try again later.');
			return false;
		} else {
			var conf = confirm('Do you want to delete this category?');
			if (!conf) {
				return false;
			} else {
				statusCode = itemCategoryModel.deleteItemCategory(this.getCategoryId());
				return statusCode;
			}
		}
	};
};

module.exports = ItemCategory;