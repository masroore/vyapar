var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require("babel-runtime/helpers/createClass");

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CountryObject = function () {
	function CountryObject(countryCode, countryName, currencySymbols, dialCode) {
		var oldNames = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : [];
		(0, _classCallCheck3.default)(this, CountryObject);

		this._countryCode = countryCode;
		this._countryName = countryName;
		this._dialCode = dialCode;
		this._currencySymbols = currencySymbols;
		this._oldNames = oldNames;
	}

	(0, _createClass3.default)(CountryObject, [{
		key: "countryCode",
		get: function get() {
			return this._countryCode;
		}
	}, {
		key: "countryName",
		get: function get() {
			return this._countryName;
		}
	}, {
		key: "currencySymbols",
		get: function get() {
			return this._currencySymbols;
		}
	}, {
		key: "oldNames",
		get: function get() {
			return this._oldNames;
		}
	}, {
		key: "dialCode",
		get: function get() {
			return this._dialCode;
		}
	}]);
	return CountryObject;
}();

module.exports = CountryObject;