var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function () {
	var TransactionFactory = require('../BizLogic/TransactionFactory');

	var FirmCache = require('../Cache/FirmCache');
	var ItemUnitCache = require('../Cache/ItemUnitCache');
	var MessageCache = require('../Cache/MessageCache');
	var ItemUnitMappingCache = require('../Cache/ItemUnitMappingCache');
	var PaymentInfoCache = require('../Cache/PaymentInfoCache');

	var TxnMessageFieldConstant = require('../Constants/TxnMessageFieldConstants');
	var TxnTypeConstants = require('../Constants/TxnTypeConstant');
	var StringConstants = require('../Constants/StringConstants');

	var MyDate = require('../Utilities/MyDate');
	var MyDouble = require('../Utilities/MyDouble');

	var SettingCache = require('./../Cache/SettingCache.js');

	var balanceAmountRegex = StringConstants.balanceAmountRegex;
	var LicenseUtility = require('../Utilities/LicenseUtility');

	//   const isValidLicense = false
	return function () {
		function TxnMessageFormatter() {
			(0, _classCallCheck3.default)(this, TxnMessageFormatter);
		}

		(0, _createClass3.default)(TxnMessageFormatter, [{
			key: 'getDefaultSampleMessage',
			value: function getDefaultSampleMessage(txnType) {
				return this.createMessageForTxn(new TransactionFactory().getSampleTxn(txnType));
			}
		}, {
			key: 'createMessageForTxn',
			value: function createMessageForTxn(txnObj) {
				var header = '';
				var date = '';
				var dueDate = '';
				var txnRefNumber = '';
				var itemList = '';
				var txnAmount = '';
				var cashAmount = '';
				var paymentType = '';
				var balanceAmount = '';
				var totalBalance = '';
				var description = '';
				var footer = '';
				var message = '';
				var transportationDetail = '';

				var licenseInfoDetails = LicenseUtility.getLicenseInfo();

				var messageCache = new MessageCache();
				var txnType = txnObj.getTxnType();
				var fields = messageCache.getByTxnType(txnType);
				var itemUnitMappingCache = new ItemUnitMappingCache();
				var itemUnitCache = new ItemUnitCache();
				var paymentInfoCache = new PaymentInfoCache();
				var firm = new FirmCache();
				var UsageTypeConstants = require('../Constants/UsageTypeConstants');

				fields.forEach(function (field) {
					switch (Number(field.txnFieldId)) {
						case TxnMessageFieldConstant.TXN_MESSAGE_HEADER:
							header = field.txnFieldValue;
							break;
						case TxnMessageFieldConstant.TXN_MESSAGE_DATE:
							if (field.txnFieldValue == '1') {
								date = field.txnFieldName + ': ';
							}
							break;
						case TxnMessageFieldConstant.TXN_MESSAGE_DUE_DATE:
							if (field.txnFieldValue == '1') {
								dueDate = field.txnFieldName + ': ';
							}
							break;
						case TxnMessageFieldConstant.TXN_MESSAGE_REF_NUMBER:
							if (field.txnFieldValue == '1') {
								txnRefNumber = field.txnFieldName + ': ';
							}
							break;
						case TxnMessageFieldConstant.TXN_MESSAGE_ITEM_LIST:
							if (field.txnFieldValue == '1') {
								itemList = field.txnFieldName + ': ';
							}
							break;
						case TxnMessageFieldConstant.TXN_MESSAGE_TXN_AMOUNT:
							if (field.txnFieldValue == '1') {
								txnAmount = field.txnFieldName + ': ';
							}
							break;
						case TxnMessageFieldConstant.TXN_MESSAGE_CASH_AMOUNT:
							if (field.txnFieldValue == '1') {
								cashAmount = field.txnFieldName + ': ';
							}
							break;
						case TxnMessageFieldConstant.TXN_MESSAGE_PAYMENT_TYPE:
							if (field.txnFieldValue == '1') {
								paymentType = field.txnFieldName + ': ';
							}
							break;
						case TxnMessageFieldConstant.TXN_MESSAGE_BALANCE_AMOUNT:
							if (field.txnFieldValue == '1') {
								balanceAmount = field.txnFieldName + ': ';
							}
							break;
						case TxnMessageFieldConstant.TXN_MESSAGE_TOTAL_BALANCE_AMOUNT:
							if (field.txnFieldValue == '1') {
								totalBalance = field.txnFieldName + ': ';
							}
							break;
						case TxnMessageFieldConstant.TXN_MESSAGE_DESCRIPTION:
							if (field.txnFieldValue == '1') {
								description = field.txnFieldName + ': ';
							}
							break;
						case TxnMessageFieldConstant.TXN_MESSAGE_TRANSPORTATION_DETAIL:
							var CustomFieldsCache = require('./../Cache/CustomFieldsCache.js');
							var customFieldsCache = new CustomFieldsCache();
							if (field.txnFieldValue == '1' && customFieldsCache.isDeliveryDetailsEnable() && txnObj.getCustomFields()) {
								var deliveryFields = [];
								try {
									var customFieldstringObj = JSON.parse(txnObj.getCustomFields());
									var transportationDetailArray = customFieldstringObj['transportation_details'] || [];
									for (var i = 0; i < transportationDetailArray.length; i++) {
										var customFieldModel = customFieldsCache.getCustomFieldById(transportationDetailArray[i]['id']);
										if (customFieldModel && transportationDetailArray[i]['value']) {
											deliveryFields.push(customFieldModel.getCustomFieldName() + ': ' + transportationDetailArray[i]['value']);
										}
									}
									if (deliveryFields.length) {
										transportationDetail = field.txnFieldName + ':\n' + deliveryFields.join('\n');
									}
								} catch (e) {
									if (logger !== 'undefined' && logger && logger.error) {
										logger.error(e);
									}
								}
							}
							break;
						case TxnMessageFieldConstant.TXN_MESSAGE_FOOTER:
							footer = field.txnFieldValue;
							if (!footer) {
								footer += firm.getTransactionFirmWithDefault(txnObj).getFirmName();
							}
							break;
					}
				});

				if (date) {
					message += date + MyDate.convertDateToStringForUI(txnObj.getTxnDate()) + '\n';
				}
				if (dueDate && txnObj.getTxnDueDate()) {
					message += dueDate + MyDate.convertDateToStringForUI(txnObj.getTxnDueDate()) + '\n';
				}
				if (txnRefNumber && txnObj.getTxnRefNumber()) {
					message += txnRefNumber + txnObj.getFullTxnRefNumber() + '\n';
				}
				if (itemList) {
					var lineItems = txnObj.getLineItems();
					if (lineItems && lineItems.length > 0) {
						message += itemList + '\n';

						lineItems.forEach(function (lineItem) {
							message += lineItem.getItemName() + '  ';
							var pricePerUnitToBeShown = Number(lineItem.getItemUnitPrice());
							var quantityToBeShown = Number(lineItem.getItemQuantity());
							var freeQuantityToBeShown = Number(lineItem.getItemFreeQuantity());
							var unitToBeShown = '';

							var itemUnitMappingId = lineItem.getLineItemUnitMappingId() || 0;
							var itemMappings = itemUnitMappingCache.getItemUnitMapping(itemUnitMappingId);

							if (itemUnitMappingId > 0) {
								if (lineItem.getLineItemUnitId() == itemMappings.getSecondaryUnitId()) {
									var conversionRate = Number(itemMappings.getConversionRate());
									quantityToBeShown = Number(lineItem.getItemQuantity()) * conversionRate;
									freeQuantityToBeShown = Number(lineItem.getItemFreeQuantity()) * conversionRate;
									pricePerUnitToBeShown = Number(lineItem.getItemUnitPrice()) / conversionRate;
								}
							}
							var qtyText = MyDouble.getQuantityWithDecimalWithoutColor(quantityToBeShown) + MyDouble.quantityDoubleToStringWithSignExplicitly(freeQuantityToBeShown, true);
							if (lineItem.getLineItemUnitId()) {
								unitToBeShown = ' ' + itemUnitCache.getItemUnitShortNameById(lineItem.getLineItemUnitId());
							}
							qtyText += unitToBeShown;
							message += qtyText + '  ' + MyDouble.doubleToStringForAmountWithPrecisionSetting(pricePerUnitToBeShown) + '  ' + MyDouble.doubleToStringForAmountWithPrecisionSetting(lineItem.getLineItemTotal()) + '\n';
						});
					}
				}
				if (txnAmount) {
					message += txnAmount + MyDouble.doubleToStringForAmountWithPrecisionSetting(Number(txnObj.getCashAmount()) + Number(txnObj.getBalanceAmount())) + '\n';
				}
				if (cashAmount && MyDouble.convertStringToDouble(txnObj.getCashAmount()) > 0) {
					if (txnType == TxnTypeConstants.TXN_TYPE_CASHIN || txnType == TxnTypeConstants.TXN_TYPE_CASHOUT) {
						var discountAmount = txnObj.getDiscountAmount() ? MyDouble.doubleToStringForAmountWithPrecisionSetting(txnObj.getDiscountAmount()) : 0;
						message += cashAmount + MyDouble.doubleToStringForAmountWithPrecisionSetting(txnObj.getCashAmount()) + '\n';
						if (discountAmount) {
							message += 'Discount: ' + discountAmount + '\n';
						}
					} else {
						message += cashAmount + MyDouble.doubleToStringForAmountWithPrecisionSetting(txnObj.getCashAmount()) + '\n';
					}
					if (paymentType) {
						var paymentInfo = paymentInfoCache.getPaymentInfoObjById(String(txnObj.getPaymentTypeId()));
						message += paymentType + paymentInfo.getName() + '\n';
					}
				}

				if (balanceAmount && MyDouble.convertStringToDouble(txnObj.getBalanceAmount()) > 0) {
					message += balanceAmount + MyDouble.doubleToStringForAmountWithPrecisionSetting(txnObj.getBalanceAmount()) + '\n';
				}
				if (totalBalance) {
					message += totalBalance + MyDouble.doubleToStringForAmountWithPrecisionSetting(txnObj.getNameRef().getAmount()) + '\n';
				}
				if (description && txnObj.getDescription()) {
					message += description + txnObj.getDescription() + '\n';
				}
				if (transportationDetail) {
					message += transportationDetail;
				}
				var branding = '';
				if (licenseInfoDetails['licenseUsageType'] !== UsageTypeConstants.VALID_LICENSE) {
					branding = 'See more details at https://billing.vyaparapp.in/Accounting-App';
				}
				return {
					header: header,
					message: message,
					footer: footer,
					branding: branding
				};
			}
		}, {
			key: 'createMessageForPaymentReminder',
			value: function createMessageForPaymentReminder(amount) {
				var settingCache = new SettingCache();
				var firmCache = new FirmCache();
				var UsageTypeConstants = require('../Constants/UsageTypeConstants');
				var message = settingCache.getPaymentReminderMessage();
				var messageTo = message.replace(balanceAmountRegex, MyDouble.getAmountForInvoicePrint(amount));
				var defaultFirm = firmCache.getDefaultFirm();
				var branding = '';
				var footer = '';
				var licenseInfoDetails = LicenseUtility.getLicenseInfo();
				if (defaultFirm) {
					footer = defaultFirm.getFirmName();
				}
				if (licenseInfoDetails['licenseUsageType'] !== UsageTypeConstants.VALID_LICENSE) {
					branding = 'See more details at https://billing.vyaparapp.in/billingapp';
				}
				return {
					message: messageTo,
					footer: footer,
					branding: branding
				};
			}
		}, {
			key: 'createMessageForCatalogue',
			value: function createMessageForCatalogue(catalogueURL) {
				var settingCache = new SettingCache();
				var firmCache = new FirmCache();
				var message = settingCache.getCatalogueMessage();
				var messageTo = message.replace(StringConstants.catalogueLinkRegex, catalogueURL);
				var defaultFirm = firmCache.getDefaultFirm();
				var footer = '\nThank You,';
				if (defaultFirm) {
					footer += '\n' + defaultFirm.getFirmName() + ',\n' + defaultFirm.getFirmPhone();
				}
				return {
					message: messageTo,
					footer: footer
				};
			}
		}]);
		return TxnMessageFormatter;
	}();
}();