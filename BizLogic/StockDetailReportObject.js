/**
 * Created by Ashish on 9/21/2017.
 */
var StockDetailReportObject = function StockDetailReportObject() {
	this.date;
	this.itemName;
	this.openingQuantity = 0;
	this.quantityIn = 0;
	this.quantityOut = 0;
	this.closingQuantity = 0;

	this.getDate = function () {
		return this.date;
	};
	this.setDate = function (date) {
		this.date = date;
	};
	this.getItemName = function () {
		return this.itemName;
	};
	this.setItemName = function (itemName) {
		this.itemName = itemName;
	};
	this.getOpeningQuantity = function () {
		return this.openingQuantity;
	};
	this.setOpeningQuantity = function (openingQuantity) {
		this.openingQuantity = openingQuantity;
	};
	this.getQuantityIn = function () {
		return this.quantityIn;
	};
	this.setQuantityIn = function (quantityIn) {
		this.quantityIn = quantityIn;
	};
	this.getQuantityOut = function () {
		return this.quantityOut;
	};
	this.setQuantityOut = function (quantityOut) {
		this.quantityOut = quantityOut;
	};
	this.getClosingQuantity = function () {
		return this.quantityOut;
	};
	this.setClosingQuantity = function (closingQuantity) {
		this.closingQuantity = closingQuantity;
	};
};

module.exports = StockDetailReportObject;