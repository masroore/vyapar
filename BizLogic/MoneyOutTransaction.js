var MoneyOutTransaction = function MoneyOutTransaction() {
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var cashAmount, balanceAmount, txnType, txnRefNumber;

	this.getCashAmount = function () {
		return this.cashAmount;
	};
	// this.setCashAmount = function(cashAmount) {
	//     this.cashAmount = cashAmount;
	// }

	this.getBalanceAmount = function () {
		this.balanceAmount = 0;
		return this.balanceAmount;
	};
	this.setBalanceAmount = function (balanceAmount) {
		if (!balanceAmount) {
			this.cashAmount = '0.0';
		} else if (!isNaN(Number(balanceAmount))) {
			this.cashAmount = Number(balanceAmount);
		} else {
			return 'ERROR_TXN_INVALID_AMOUNT';
		}
		return 'SUCCESS';
	};
	this.setCashAmount = function (cashAmount) {
		if (isNaN(Number(cashAmount))) {} else {
			this.cashAmount = Number(cashAmount);
			return 'SUCCESS';
		}
	};

	this.setAmounts = function (totalAmount, cashAmount) {
		var statusCode = this.setCashAmount(totalAmount);
		return statusCode;
	};

	this.getTxnType = function () {
		this.txnType = TxnTypeConstant['TXN_TYPE_CASHOUT'];
		return this.txnType;
	};

	this.getTxnTypeString = function () {
		this.txnTypeString = 'TXN_TYPE_CASHOUT';
		return this.txnTypeString;
	};

	this.setDiscountPercent = function (lineItems) {
		this.discountPercent = 0.0;
	};
	this.getDiscountPercent = function () {
		this.discountPercent = 0.0;
		return this.discountPercent;
	};

	this.setTaxPercent = function (lineItems) {
		this.taxPercent = 0.0;
	};
	this.getTaxPercent = function () {
		this.taxPercent = 0.0;
		return this.taxPercent;
	};

	this.setTxnRefNumber = function (txnRefNumber) {
		this.txnRefNumber = txnRefNumber;
	};
	this.getTxnRefNumber = function () {
		return this.txnRefNumber;
	};
};

module.exports = MoneyOutTransaction;