var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ProfitAndLossReportObject = function ProfitAndLossReportObject() {
	var SettingCache = require('./../Cache/SettingCache.js');
	var settingsCache = new SettingCache();
	var DataLoader = require('./../DBManager/DataLoader.js');
	var dataLoader = new DataLoader();

	this.fromDate = null;
	this.toDate = null;
	this.saleAmount = 0;
	this.saleReturnAmount = 0;
	this.purchaseAmount = 0;
	this.purchaseReturnAmount = 0;
	this.paymentInDiscountAmount = 0;
	this.otherExpenseAmount = 0;
	this.paymentOutDiscountAmount = 0;
	this.otherIncomeAmount = 0;
	this.openingStockValue = 0;
	this.closingStockValue = 0;
	this.netProfitAndLossAmount = 0;
	this.grossProfitAndLossAmount = 0;
	this.itemValues = {};
	this.receivableTax = 0;
	this.payableTax = 0;

	this.getFromDate = function () {
		return this.fromDate;
	};

	this.setFromDate = function (fromDate) {
		this.fromDate = fromDate;
	};

	this.getToDate = function () {
		return this.toDate;
	};

	this.setToDate = function (toDate) {
		this.toDate = toDate;
	};

	this.getSaleAmount = function () {
		return this.saleAmount;
	};

	this.setSaleAmount = function (saleAmount) {
		this.saleAmount = saleAmount;
	};

	this.getSaleReturnAmount = function () {
		return this.saleReturnAmount;
	};

	this.setSaleReturnAmount = function (saleReturnAmount) {
		this.saleReturnAmount = saleReturnAmount;
	};

	this.getPurchaseAmount = function () {
		return this.purchaseAmount;
	};

	this.setPurchaseAmount = function (purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	};

	this.getPurchaseReturnAmount = function () {
		return this.purchaseReturnAmount;
	};

	this.setPurchaseReturnAmount = function (purchaseReturnAmount) {
		this.purchaseReturnAmount = purchaseReturnAmount;
	};

	this.getPaymentInDiscountAmount = function () {
		return this.paymentInDiscountAmount;
	};

	this.setPaymentInDiscountAmount = function (paymentInDiscountAmount) {
		this.paymentInDiscountAmount = paymentInDiscountAmount;
	};

	this.getPaymentOutDiscountAmount = function () {
		return this.paymentOutDiscountAmount;
	};

	this.setPaymentOutDiscountAmount = function (paymentOutDiscountAmount) {
		this.paymentOutDiscountAmount = paymentOutDiscountAmount;
	};

	this.getOtherExpenseAmount = function () {
		return this.otherExpenseAmount;
	};

	this.setOtherExpenseAmount = function (otherExpenseAmount) {
		this.otherExpenseAmount = otherExpenseAmount;
	};

	this.getOtherIncomeAmount = function () {
		return this.otherIncomeAmount;
	};

	this.setOtherIncomeAmount = function (otherIncomeAmount) {
		this.otherIncomeAmount = otherIncomeAmount;
	};

	this.getLoanProcessingFeeExpense = function () {
		return this.loanProcessingFeeExpense || '';
	};

	this.setLoanProcessingFeeExpense = function (loanProcessingFeeExpense) {
		this.loanProcessingFeeExpense = loanProcessingFeeExpense;
	};

	this.getLoanInterestExpense = function () {
		return this.loanInterestExpense || '';
	};

	this.setLoanInterestExpense = function (loanInterestExpense) {
		this.loanInterestExpense = loanInterestExpense;
	};

	this.getItemValues = function () {
		return this.itemValues;
	};

	this.setItemValues = function (itemValues) {
		this.itemValues = itemValues;
	};

	this.getOpeningStockValue = function () {
		return this.openingStockValue;
	};

	this.getClosingStockValue = function () {
		return this.closingStockValue;
	};

	this.getReceivableTax = function () {
		return this.receivableTax;
	};

	this.getPayableTax = function () {
		return this.payableTax;
	};

	this.setReceivableTax = function (receivableTax) {
		this.receivableTax = receivableTax;
	};

	this.setPayableTax = function (payableTax) {
		this.payableTax = payableTax;
	};

	this.clearItemValues = function () {
		if (this.itemValues && (0, _keys2.default)(this.itemValues).length > 0) {
			var ItemCache = require('./../Cache/ItemCache.js');
			var itemcache = new ItemCache();
			var tempItemValues = this.itemValues;

			var itemList = itemcache.getListOfItemsObject();
			if (itemList && itemList.length > 0) {
				$.each(itemList, function (key, tem) {
					delete tempItemValues[item.getItemId()];
				});
			}
		}
	};

	this.clearServiceItemValues = function () {
		if (this.itemValues && (0, _keys2.default)(this.itemValues).length > 0) {
			var ItemCache = require('./../Cache/ItemCache.js');
			var itemcache = new ItemCache();
			var tempItemValues = this.itemValues;

			var serviceList = itemcache.getListOfServicesObject();
			if (serviceList && serviceList.length > 0) {
				$.each(serviceList, function (key, serviceItem) {
					delete tempItemValues[serviceItem.getItemId()];
				});
			}
		}
	};

	this.setStockValuesUsingItemValues = function () {
		var openingValue = 0;
		var closingValue = 0;
		$.each(this.itemValues, function (key, itemStockValue) {
			openingValue += itemStockValue[0];
			closingValue += itemStockValue[1];
		});
		this.openingStockValue = MyDouble.getAmountWithDecimal(openingValue);
		this.closingStockValue = MyDouble.getAmountWithDecimal(closingValue);
	};

	this.getNetProfitAndLossAmount = function () {
		return this.netProfitAndLossAmount;
	};

	this.getGrossProfitAndLossAmount = function () {
		return this.grossProfitAndLossAmount;
	};

	this.setProfitAndLossAmount = function () {
		this.grossProfitAndLossAmount = MyDouble.getAmountWithDecimal(this.saleAmount - this.saleReturnAmount - this.purchaseAmount + this.purchaseReturnAmount + this.receivableTax - this.payableTax - this.openingStockValue + this.closingStockValue - this.paymentInDiscountAmount + this.paymentOutDiscountAmount);
		this.netProfitAndLossAmount = MyDouble.getAmountWithDecimal(this.grossProfitAndLossAmount - this.otherExpenseAmount + this.otherIncomeAmount - this.loanProcessingFeeExpense - this.loanInterestExpense);
	};

	this.getProfitAndLossReportObject = function (fromDate, toDate) {
		var profitAndLossReportObject = new ProfitAndLossReportObject();
		profitAndLossReportObject.fromDate = fromDate;
		profitAndLossReportObject.toDate = toDate;
		if (fromDate <= toDate) {
			dataLoader.loadProfitAndLossReportDataForTransactions(profitAndLossReportObject);
			if (settingsCache.getItemEnabled) {
				dataLoader.loadProfitAndLossReportDataForStockValue(profitAndLossReportObject);
				profitAndLossReportObject.clearServiceItemValues();
				profitAndLossReportObject.setStockValuesUsingItemValues();
			}
			profitAndLossReportObject.setProfitAndLossAmount();
		}
		return profitAndLossReportObject;
	};
};

module.exports = ProfitAndLossReportObject;