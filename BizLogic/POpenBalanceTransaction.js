var POpenBalanceTransaction = function POpenBalanceTransaction() {
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var balanceAmount;
	var txnType;
	var cashAmount;
	var description;
	var txnRefNumber;
	var txnMessage;
	this.getBalanceAmount = function () {
		return this.balanceAmount;
	};
	this.getTxnType = function () {
		this.txnType = TxnTypeConstant['TXN_TYPE_POPENBALANCE'];
		return this.txnType;
	};

	this.getTaxAmount = function () {
		return 0.0;
	};

	this.getDiscountAmount = function () {
		return 0.0;
	};

	this.getTxnTypeString = function () {
		this.txnTypeString = 'TXN_TYPE_POPENBALANCE';
		return this.txnTypeString;
	};
	this.setBalanceAmount = function (balanceAmount) {
		if (!balanceAmount) {
			this.balanceAmount = '0.0';
		} else if (!isNaN(Number(balanceAmount))) {
			this.balanceAmount = Number(balanceAmount);
		} else {
			return 'ERROR_TXN_INVALID_AMOUNT';
		}
		return 'SUCCESS';
	};

	this.setCashAmount = function (cashAmount) {
		if (!isNaN(Number(cashAmount))) {
			return 'SUCCESS';
		} else {}
	};

	this.getCashAmount = function () {
		this.cashAmount = 0.0;
		return this.cashAmount;
	};

	this.setDescription = function (description) {
		this.description = 'Payable opening balance';
	};
	this.getDescription = function () {
		this.description = 'Payable opening balance';
		return this.description;
	};
	this.setAmounts = function (totalAmount, cashAmount) {
		return this.setBalanceAmount(totalAmount);
	};
	this.getTxnRefNumber = function () {
		this.txnRefNumber = '';
		return this.txnRefNumber;
	};
	this.setTxnRefNumber = function (txnRefNumber) {};

	this.getTransactionMessage = function () {
		this.txnMessage = '';
		return this.txnMessage;
	};
};

module.exports = POpenBalanceTransaction;