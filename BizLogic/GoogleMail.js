var _setImmediate2 = require('babel-runtime/core-js/set-immediate');

var _setImmediate3 = _interopRequireDefault(_setImmediate2);

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var GoogleMail = function GoogleMail() {
	var fs = require('fs');

	var google = require('googleapis');
	var GoogleAuth = google.auth.OAuth2;
	var alertUser = false;
	var receiverMailId = '';
	var clientSecret = null;
	// If modifying these scopes, delete your previously saved credentials
	// at ~/.credentials/drive-nodejs-quickstart.json
	var SCOPES = ['https://www.googleapis.com/auth/drive.file', 'https://www.googleapis.com/auth/gmail.send'];

	var app = require('electron').remote.app;

	var appPath = app.getPath('userData');
	var TOKEN_DIR = appPath + '/BusinessNames/Credentials/';
	var TOKEN_PATH = TOKEN_DIR + 'accessToken.json';

	this.sendMail = function (_ref) {
		var fileNames = _ref.fileNames,
		    receiverId = _ref.receiverId,
		    alertUser = _ref.alertUser,
		    subject = _ref.subject,
		    message = _ref.message,
		    attachFileNames = _ref.attachFileNames,
		    successCallBack = _ref.successCallBack,
		    errorCallBack = _ref.errorCallBack,
		    base64Attachments = _ref.base64Attachments;

		receiverMailId = receiverId;
		var hasMutipleReceivers = receiverMailId && receiverMailId.split(/;|,/).filter(function (el) {
			return el && el.trim().length != 0;
		}).length > 1;
		// Load client secrets from a local file.
		var path = require('path');
		var filePath = __dirname + '/../Assets/client_secret_drive.json';
		fs.readFile(filePath, function processClientSecrets(err, content) {
			if (err) {
				console.log('Error loading client secret  file: ' + err);
				if (errorCallBack && typeof errorCallBack === 'function') {
					errorCallBack();
				}
				return;
			}
			// Authorize a client with the loaded credentials, then call the
			// Drive API.
			clientSecret = JSON.parse(content);
			authorize(clientSecret, sendMessage);
		});

		/**
         * Create an OAuth2 client with the given credentials, and then execute the
         * given callback function.
         *
         * @param {Object} credentials The authorization client credentials.
         * @param {function} callback The callback to call with the authorized client.
         */
		function authorize(credentials, callback) {
			var client_secret = credentials.installed.client_secret;
			var clientId = credentials.installed.client_id;
			var redirectUrl = credentials.installed.redirect_uris[0];
			var oauth2Client = new GoogleAuth(clientId, client_secret, redirectUrl);

			// Check if we have previously stored a token.
			fs.readFile(TOKEN_PATH, function (err, token) {
				if (err) {
					getNewToken(oauth2Client, callback);
				} else {
					oauth2Client.setCredentials(JSON.parse(token));
					oauth2Client.refreshAccessToken(function (err) {
						if (err && (err.message === 'invalid_grant' || err.code === 401 || err.code == 403 || err.message.toLowerCase() === 'insufficient permission')) {
							fs.unlink(TOKEN_PATH);
							authorize(credentials, callback);
							return false;
						}
						callback(oauth2Client, subject, message, attachFileNames);
					});
				}
			});
		}

		/**
         * Get and store new token after prompting for user authorization, and then
         * execute the given callback with the authorized OAuth2 client.
         *
         * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
         * @param {getEventsCallback} callback The callback to call with the authorized
         *     client.
         */
		function getNewToken(oauth2Client, callback) {
			var authUrl = oauth2Client.generateAuthUrl({
				access_type: 'offline',
				scope: SCOPES
			});
			var code = function code() {
				return authorizeApp(authUrl);
			};

			// console.log("Code : " + code);
			code().then(function (result) {
				console.dir(result);
				oauth2Client.getToken(result, function (err, token) {
					if (err) {
						console.log('Error while trying to retrieve access token', err);
						return;
					}
					oauth2Client.credentials = token;
					storeToken(token);
					callback(oauth2Client, subject, message, attachFileNames);
				});
			}, function (err) {});
		}

		/**
         * Store token to disk be used in later program executions.
         *
         * @param {Object} token The token to store to disk.
         */
		function storeToken(token) {
			try {
				fs.mkdirSync(TOKEN_DIR);
			} catch (err) {
				if (err.code != 'EEXIST') {
					throw err;
				}
			}
			fs.writeFile(TOKEN_PATH, (0, _stringify2.default)(token), function (err) {});
		}

		function makeBody(subject, message, attachFileNames) {
			var boundary = '__vyaparapp__';
			var nl = '\n';
			var str;
			if (fileNames) {
				if (!Array.isArray(fileNames)) {
					fileNames = [fileNames];
				}
				if (!Array.isArray(attachFileNames)) {
					attachFileNames = [attachFileNames];
				}
				var count = void 0;
				var attachments = [];
				fileNames.forEach(function (fileName, index) {
					if (fs.existsSync(fileName)) {
						var file = new Buffer(fs.readFileSync(fileName)).toString('base64');
						var extensionContent = 'application/pdf';
						var name = attachFileNames[index] || 'Attachment_' + ++count;
						attachments.push('Content-Type: ' + extensionContent + '; name=' + name, 'Content-Disposition: attachment; filename=' + name, 'Content-Transfer-Encoding: base64' + nl, file, nl, '--' + boundary);
					}
				});
				str = ['MIME-Version: 1.0', 'Content-Transfer-Encoding: 7bit', '' + (hasMutipleReceivers ? 'bcc:' : 'to:') + receiverId, 'subject: ' + subject, 'Content-Type: multipart/alternate; boundary=' + boundary + nl, '--' + boundary, 'Content-Type: text/plain; charset=UTF-8', 'Content-Transfer-Encoding: 7bit' + nl, message + nl, '--' + boundary, '--' + boundary].concat(attachments, ['--' + boundary + '--']).join('\n');
			} else if (base64Attachments && base64Attachments.length > 0) {
				var _count = 0;
				var extensionType = { 'image/jpeg': 'jpg', 'application/pdf': 'pdf', 'image/png': 'png' };
				var _attachments = [];
				base64Attachments.forEach(function (value, index) {
					if (value && value.indexOf(';base64,') >= 0) {
						var base64Contents = value.split(';base64,');
						var extensionContent = base64Contents[0].split('data:');
						var extension = '';
						if (extensionType[extensionContent[1]]) {
							extension = extensionType[extensionContent[1]];
						}
						var name = 'Attachment_' + ++_count;
						_attachments.push('Content-Type: ' + extensionContent[1] + '; name=' + name, 'Content-Disposition: attachment; filename=' + name + '.' + extension, 'Content-Transfer-Encoding: base64' + nl, base64Contents[1], nl, '--' + boundary);
					}
				});
				str = ['MIME-Version: 1.0', 'Content-Transfer-Encoding: 7bit', '' + (hasMutipleReceivers ? 'bcc:' : 'to:') + receiverId, 'subject: ' + subject, 'Content-Type: multipart/alternate; boundary=' + boundary + nl, '--' + boundary, 'Content-Type: text/plain; charset=UTF-8', 'Content-Transfer-Encoding: 7bit' + nl, message + nl, '--' + boundary, '--' + boundary].concat(_attachments, ['--' + boundary + '--']).join('\n');
			} else {
				str = ['MIME-Version: 1.0', 'Content-Transfer-Encoding: 7bit', '' + (hasMutipleReceivers ? 'bcc:' : 'to:') + receiverId, 'subject: ' + subject, 'Content-Type: multipart/alternate; boundary=' + boundary + nl, '--' + boundary, 'Content-Type: text/plain; charset=UTF-8', 'Content-Transfer-Encoding: 7bit' + nl, nl + message + nl].join('\n');
			}

			// console.dir(attach);

			var encodedMail = new Buffer(str).toString('base64').replace(/\+/g, '-').replace(/\//g, '_');
			return encodedMail;
		}

		function sendMessage(auth, subject, message, attachFileNames) {
			var gmail = google.gmail({ auth: auth, version: 'v1' });
			var raw = makeBody(subject, message, attachFileNames);
			gmail.users.messages.send({
				auth: auth,
				userId: 'me',
				resource: {
					raw: raw
				}
			}, function (err, response) {
				if (err) {
					if (err.code == 401 || err.message === 'invalid_grant' || err.code == 403 || err.message.toLowerCase() === 'insufficient permission') {
						// invalid credentials
						fs.unlink(TOKEN_PATH);
						authorize(clientSecret, sendMessage);
						return;
					}
					if (errorCallBack && typeof errorCallBack === 'function') {
						errorCallBack();
					}
					if (alertUser) {
						switch (err.code) {
							case 'ECONNREFUSED':
							case 'ECONNRESET':
							case 'ETIMEDOUT':
								ToastHelper.error('Email send Failed. Please check your internet connection.');
								break;
							default:
								ToastHelper.error('Email send Failed');
						}
					}
				} else {
					if (successCallBack && typeof successCallBack === 'function') {
						successCallBack();
					}
					if (alertUser) {
						ToastHelper.success('Email sent successfully to ' + receiverMailId);
					}
					console.log(response);
				}
			});
		}

		// ------------------------------------------------------------------------------------
		function authorizeApp(url) {
			return new _promise2.default(function (resolve, reject) {
				try {
					// Changes for GAuth wrt. bug user is not able to take backup on GDrive.
					// Creating two browser window to authenticate user.
					// GAuth is not working directly with user-agent.
					// Workaround: First request is without user-agent, once we load it without user-agent then again we are trying to load it with user-agent.
					var browserWindowParams = {
						useContentSize: true,
						center: true,
						show: false,
						resizable: false,
						autoHideMenuBar: true,
						alwaysOnTop: false
					};

					var BrowserWindow = require('electron').remote.BrowserWindow;

					var win = new BrowserWindow(browserWindowParams || { 'use-content-size': true });
					win.webContents.on('did-finish-load', function () {
						var browserWindowParams1 = {
							useContentSize: true,
							center: true,
							show: true,
							resizable: false,
							autoHideMenuBar: true,
							alwaysOnTop: true
						};
						var win1 = new BrowserWindow(browserWindowParams1 || { 'use-content-size': true });
						win.close();
						win1.loadURL(url, { userAgent: 'Chrome' });
						win1.on('closed', function () {
							reject(new Error('User closed the window'));
							try {
								$('#loading').hide();
							} catch (err) {}
						});
						win1.on('page-title-updated', function () {
							(0, _setImmediate3.default)(function () {
								var title = win1.getTitle();
								if (title.startsWith('Denied')) {
									reject(new Error(title.split(/[ =]/)[2]));
									win1.removeAllListeners('closed');
									win1.close();
								} else if (title.startsWith('Success')) {
									resolve(title.split(/[ =]/)[2]);
									win1.removeAllListeners('closed');
									win1.close();
								}
							});
						});
					});
					win.loadURL(url);
				} catch (ex) {
					reject(new Error('Unable to load the window. Please contact Vyapar team.'));
				}
			});
		}
	};
};

module.exports = GoogleMail;