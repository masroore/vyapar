var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ErrorCode = require('./../Constants/ErrorCode.js');
var ImportDB = function ImportDB() {
	this.ImportDatabase = function (cb) {
		var _require = require('electron'),
		    ipcRenderer = _require.ipcRenderer;

		var _require$remote = require('electron').remote,
		    app = _require$remote.app,
		    dialog = _require$remote.dialog,
		    getCurrentWindow = _require$remote.getCurrentWindow;

		var appPath = app.getPath('userData');
		var window = getCurrentWindow();
		var selectedFile = dialog.showOpenDialog(window, {
			title: "Select DB to Import '.vyb'",
			buttonLabel: 'Select',
			properties: ['openFile'],
			filters: [{
				name: 'Vyapar DB File', extensions: ['vyb']
			}]
		});
		var fs = require('fs');
		var yauzl = require('yauzl');
		var path = require('path');
		if (selectedFile) {
			var backupFilePath = selectedFile[0];
			yauzl.open(backupFilePath, { lazyEntries: true }, function (err, zipFile) {
				if (err || zipFile.entryCount > 3) {
					alert(ErrorCode.ERROR_DB_UNSUPPORTED);
					cb && cb();
					return;
				}
				zipFile.readEntry();
				zipFile.on('entry', function (entry) {
					if (/(.vyp-shm$)|(.vyp-wal$)/.test(entry.fileName)) {
						zipFile.readEntry();
						return;
					}
					if (!/(.vyp$)/.test(entry.fileName)) {
						alert(ErrorCode.ERROR_DB_UNSUPPORTED);
						zipFile.close();
						cb && cb();
						return;
					}
					var dbName = entry.fileName.trim();
					if (fs.existsSync(appPath + '/BusinessNames/' + dbName)) {
						var dbNameWithOutExtension = dbName.substr(0, dbName.length - 4); // Removing .vyp from the file name
						if (dbNameWithOutExtension.length > 13) {
							var lastThirteen = dbNameWithOutExtension.substr(-13); // Last 13 characters
							if (!isNaN(lastThirteen)) {
								// if numeric
								var len = dbNameWithOutExtension.length - 13;
								var initials = dbNameWithOutExtension.substr(0, len);
								dbName = initials + new Date().getTime() + '.vyp';
							} else {
								dbName = dbNameWithOutExtension + new Date().getTime() + '.vyp';
							}
						} else {
							dbName = dbNameWithOutExtension + new Date().getTime() + '.vyp';
						}
						entry.fileName = dbName = dbName.trim();
					}
					zipFile.openReadStream(entry, function (err, readStream) {
						if (err) {
							alert(ErrorCode.ERROR_DB_UNSUPPORTED);
							cb && cb();
							return;
						}
						var writeStream = fs.createWriteStream(path.join(appPath, 'BusinessNames/', dbName));
						readStream.pipe(writeStream);
						writeStream.on('close', function (err) {
							if (err) {
								alert(ErrorCode.ERROR_DB_UNSUPPORTED);
								cb && cb();
								return;
							}
							var CompanyListUtility = require('./../Utilities/CompanyListUtility.js');
							var fileNameObj = CompanyListUtility.getCompaniesFileNameObject();
							var businessName = dbName.substr(0, dbName.length - 4); // Removing .vyp from the file
							if ((0, _keys2.default)(fileNameObj).includes(businessName)) {
								businessName = businessName + '_' + new Date().getTime();
							}
							fileNameObj[businessName] = { 'path': appPath + '/BusinessNames/' + dbName, 'company_creation_type': 0 };

							CompanyListUtility.updateCompanyListFile(fileNameObj);
							fs.writeFileSync(appPath + '/BusinessNames/currentDB.txt', appPath + '/BusinessNames/' + dbName);

							var connection = require('./../DBManager/connection.js');
							var dbHandle = connection.createDB(appPath + '/BusinessNames/' + dbName);
							var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
							var sqliteDBHelper = new SqliteDBHelper();
							sqliteDBHelper.emptySyncSettings(dbHandle);
							ipcRenderer.send('changeURL', 'Index1.html');
						});
					});
				});
			});
		} else {
			cb && cb();
		}
	};
};

module.exports = ImportDB;