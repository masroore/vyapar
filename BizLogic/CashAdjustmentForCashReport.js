var CashAdjustmentForCashReport = function CashAdjustmentForCashReport(txnTypeForTransaction) {
	var cashAmount;
	var txnType = txnTypeForTransaction;
	this.txnType = txnTypeForTransaction;
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	this.getBalanceAmount = function () {
		return 0;
	};
	this.getTxnType = function () {
		return this.txnType;
	};

	this.getTaxAmount = function () {
		return 0.0;
	};

	this.getDiscountAmount = function () {
		return 0.0;
	};

	this.getTxnTypeString = function () {
		switch (this.txnType) {
			case TxnTypeConstant.TXN_TYPE_CASH_ADJ_ADD:
				return 'TXN_TYPE_CASH_ADD_ADJUSTMENT';
			case TxnTypeConstant.TXN_TYPE_CASH_ADJ_REDUCE:
				return 'TXN_TYPE_CASH_REDUCE_ADJUSTMENT';
			case TxnTypeConstant.TXN_TYPE_CASH_OPENINGBALANCE:
				return 'TXN_TYPE_OPENING_CAH_IN_HAND';
		}
		return '';
	};
	this.setBalanceAmount = function (balanceAmount) {
		return 'SUCCESS';
	};

	this.setCashAmount = function (cashAmount) {
		if (!cashAmount) {
			this.cashAmount = '0.0';
		} else if (!isNaN(Number(cashAmount))) {
			this.cashAmount = Number(cashAmount);
		} else {
			return 'ERROR_TXN_INVALID_AMOUNT';
		}
		return 'SUCCESS';
	};

	this.getCashAmount = function () {
		return this.cashAmount;
	};

	this.setAmounts = function (totalAmount, cashAmount) {
		return this.setCashAmount(cashAmount);
	};

	this.getTxnRefNumber = function () {
		this.txnRefNumber = '';
		return this.txnRefNumber;
	};
	this.setTxnRefNumber = function (txnRefNumber) {};

	this.getTransactionMessage = function () {
		return '';
	};

	this.loadAllLineItems = function () {
		this.lineItems = [];
	};
};

module.exports = CashAdjustmentForCashReport;