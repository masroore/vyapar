var ItemStockTracking = function ItemStockTracking() {
	this.istId;
	this.istBatchNumber = '';
	this.istSerialNumber = '';
	this.istMrp = 0;
	this.istExpiryDate = null;
	this.istManufacturingDate = null;
	this.istSize = '';
	this.istItemId;
	this.istCurrentQuantity = 0;
	this.istOpeningQuantity = 0;
	this.istType;
	this.dataEditable = true;

	this.isDataEditable = function () {
		return this.dataEditable;
	};
	this.setDataEditable = function (dataEditable) {
		this.dataEditable = dataEditable;
	};

	this.getIstOpeningQuantity = function () {
		return this.istOpeningQuantity;
	};
	this.setIstOpeningQuantity = function (istOpeningQuantity) {
		this.istOpeningQuantity = istOpeningQuantity;
	};

	this.getIstType = function () {
		return this.istType;
	};
	this.setIstType = function (istType) {
		this.istType = istType;
	};

	this.getIstId = function () {
		return this.istId;
	};
	this.setIstId = function (istId) {
		this.istId = istId;
	};

	this.getIstBatchNumber = function () {
		return this.istBatchNumber;
	};
	this.setIstBatchNumber = function (istBatchNuber) {
		this.istBatchNumber = istBatchNuber;
	};

	this.getIstSerialNumber = function () {
		return this.istSerialNumber;
	};
	this.setIstSerialNumber = function (istSerialNumber) {
		this.istSerialNumber = istSerialNumber;
	};

	this.getIstMrp = function () {
		return this.istMrp;
	};
	this.setIstMrp = function (istMrp) {
		this.istMrp = istMrp;
	};

	this.getIstExpiryDate = function () {
		return this.istExpiryDate;
	};
	this.setIstExpiryDate = function (istExpiryDate) {
		this.istExpiryDate = istExpiryDate;
	};

	this.getIstManufacturingDate = function () {
		return this.istManufacturingDate;
	};
	this.setIstManufacturingDate = function (istManufacturingDate) {
		this.istManufacturingDate = istManufacturingDate;
	};

	this.getIstSize = function () {
		return this.istSize;
	};
	this.setIstSize = function (istSize) {
		this.istSize = istSize;
	};

	this.getIstItemId = function () {
		return this.istItemId;
	};
	this.setIstItemId = function (istItemId) {
		this.istItemId = istItemId;
	};

	this.getIstCurrentQuantity = function () {
		return this.istCurrentQuantity;
	};
	this.setIstCurrentQuantity = function (istCurrentQuantity) {
		this.istCurrentQuantity = istCurrentQuantity;
	};

	this.getItemStockTrackingDataByIstId = function (istId) {
		var ItemStockTrackingModel = require('./../Models/ItemStockTrackingModel.js');
		var itemStockTrackingModel = new ItemStockTrackingModel();
		var itemStockTrackingData = itemStockTrackingModel.getItemStockTrackingRecordsByIstId(istId);
		return itemStockTrackingData;
	};

	this.getItemStockTrackingData = function (itemId, filterZeroAndNegativeQty, serialNumber, checkSerialNumber, checkType, istType, nameId, txnType) {
		var ItemStockTrackingModel = require('./../Models/ItemStockTrackingModel.js');
		var itemStockTrackingModel = new ItemStockTrackingModel();
		var itemStockTrackingList = itemStockTrackingModel.getItemStockTrackingList(itemId, filterZeroAndNegativeQty, serialNumber, checkSerialNumber, checkType, istType, nameId, txnType);
		return itemStockTrackingList;
	};

	this.deleteItemStockTracking = function (istId) {
		var ItemStockTrackingModel = require('./../Models/ItemStockTrackingModel.js');
		var itemStockTrackingModel = new ItemStockTrackingModel();
		var statusCode = itemStockTrackingModel.deleteItemStockTracking(istId);
		return statusCode;
	};

	this.addItemStockTracking = function (istBatchNo, istSerialNo, istMrp, istExpiryDate, istMfgDate, istSize, istItemId, itemQuantityStr, istType) {
		var ItemStockTrackingModel = require('./../Models/ItemStockTrackingModel.js');
		var itemStockTrackingModel = new ItemStockTrackingModel();
		var result = itemStockTrackingModel.addItemStockTracking(istBatchNo, istSerialNo, istMrp, istExpiryDate, istMfgDate, istSize, istItemId, itemQuantityStr, istType);
		return result;
	};

	this.getISTWithParameters = function (itemBatchNo, itemSerialNumber, itemMRP, itemExpiryDate, itemManufacturingDate, itemSize, itemId) {
		var ItemStockTrackingModel = require('./../Models/ItemStockTrackingModel.js');
		var itemStockTrackingModel = new ItemStockTrackingModel();
		var itemStockTracking = itemStockTrackingModel.getISTWithParameters(itemBatchNo, itemSerialNumber, itemMRP, itemExpiryDate, itemManufacturingDate, itemSize, itemId);
		return itemStockTracking;
	};

	this.changeISTQuantity = function (istId, currentQty, OpeningQty, istType) {
		var ItemStockTrackingModel = require('./../Models/ItemStockTrackingModel.js');
		var itemStockTrackingModel = new ItemStockTrackingModel();
		var statusCode = itemStockTrackingModel.changeISTQuantity(istId, currentQty, OpeningQty, istType);
		return statusCode;
	};

	this.getErrorCodeForResult = function (istId) {
		if (istId > 0) {
			return ErrorCode.ERROR_IST_SUCCESS;
		} else {
			return ErrorCode.ERROR_IST_FAILED;
		}
	};
};

module.exports = ItemStockTracking;