var ItemAdjustmentTxn = function ItemAdjustmentTxn() {
	var ErrorCode = require('./../Constants/ErrorCode.js');
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var IstType = require('./../Constants/IstType.js');
	var ItemCache = require('./../Cache/ItemCache.js');
	// var GetDate = require('./GetDate.js');
	var itemAdjId, itemAdjItemId, itemAdjType, itemAdjQuantity, itemAdjDescription, itemAdjDate, itemAdjDateCreated, itemAdjDateModified;
	this.itemAdjAtPrice;
	this.itemAdjIstId;

	this.getItemAdjIstId = function () {
		return this.itemAdjIstId;
	};
	this.setItemAdjIstId = function (itemAdjIstId) {
		this.itemAdjIstId = itemAdjIstId;
	};

	this.getItemAdjAtPrice = function () {
		return this.itemAdjAtPrice;
	};
	this.setItemAdjAtPrice = function (itemAdjAtPrice) {
		this.itemAdjAtPrice = itemAdjAtPrice;
	};

	this.getItemAdjDateCreated = function () {
		this.itemAdjDateCreated;
	};
	this.setItemAdjDateCreated = function (itemAdjDateCreated) {
		this.itemAdjDateCreated = itemAdjDateCreated;
	};

	this.getItemAdjDateModified = function () {
		this.itemAdjDateModified;
	};
	this.setItemAdjDateModified = function (itemAdjDateCreated) {
		this.itemAdjDateModified = itemAdjDateModified;
	};

	this.getItemAdjId = function () {
		return this.itemAdjId;
	};
	this.setItemAdjId = function (itemAdjId) {
		this.itemAdjId = itemAdjId;
	};

	this.getItemAdjItemId = function () {
		return this.itemAdjItemId;
	};
	this.setItemAdjItemId = function (itemAdjItemId) {
		this.itemAdjItemId = itemAdjItemId;
	};

	this.getItemAdjType = function () {
		return this.itemAdjType;
	};
	this.setItemAdjType = function (itemAdjType) {
		this.itemAdjType = itemAdjType;
	};

	this.getItemAdjQuantity = function () {
		return this.itemAdjQuantity;
	};
	this.setItemAdjQuantity = function (itemAdjQuantity) {
		this.itemAdjQuantity = itemAdjQuantity;
	};

	this.getItemAdjDescription = function () {
		return this.itemAdjDescription;
	};
	this.setItemAdjDescription = function (itemAdjDescription) {
		this.itemAdjDescription = itemAdjDescription;
	};

	this.getItemAdjDate = function () {
		return this.itemAdjDate;
	};
	this.setItemAdjDate = function (itemAdjDate) {
		this.itemAdjDate = itemAdjDate;
	};

	this.updatItemStockQuantity = function (itemId, adjType, adjQuantity) {
		var statusCode = ErrorCode.SUCCESS;
		var ItemCache = require('./../Cache/ItemCache.js');
		Itemcache = new ItemCache();
		var item = Itemcache.getItemById(itemId);
		if (item) {
			switch (adjType) {
				case TxnTypeConstant.TXN_TYPE_ADJ_OPENING:
					item.setItemStockQuantity(Number(item.getItemStockQuantity()) + Number(adjQuantity));
					break;
				case TxnTypeConstant.TXN_TYPE_ADJ_ADD:
					item.setItemStockQuantity(Number(item.getItemStockQuantity()) + Number(adjQuantity));
					break;
				case TxnTypeConstant.TXN_TYPE_ADJ_REDUCE:
					item.setItemStockQuantity(Number(item.getItemStockQuantity()) - Number(adjQuantity));
					break;
			}
		}
		statusCode = item.updateItemStockValue();
		if (statusCode == ErrorCode.ERROR_ITEM_SAVE_SUCCESS) {
			statusCode = ErrorCode.ERROR_ITEM_ADJ_SAVE_SUCCESS;
		} else {
			statusCode = ErrorCode.ERROR_ITEM_ADJ_SAVE_FAILED;
		}

		return statusCode;
	};

	this.deleteAdjTxn = function () {
		var statusCode = ErrorCode.ERROR_ITEM_ADJ_DELETE_FAILED;
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var transactionManager = new TransactionManager();
		var ItemModel = require('./../Models/ItemAdjModel.js');
		var itemModel = new ItemModel();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_ITEM_ADJ_DELETE_FAILED;
			return statusCode;
		}

		try {
			if (this.itemAdjType == TxnTypeConstant.TXN_TYPE_ADJ_ADD || this.itemAdjType == TxnTypeConstant.TXN_TYPE_ADJ_OPENING) {
				statusCode = this.updatItemStockQuantity(this.itemAdjItemId, TxnTypeConstant.TXN_TYPE_ADJ_REDUCE, this.itemAdjQuantity);
			} else if (this.itemAdjType == TxnTypeConstant.TXN_TYPE_ADJ_REDUCE) {
				statusCode = this.updatItemStockQuantity(this.itemAdjItemId, TxnTypeConstant.TXN_TYPE_ADJ_ADD, this.itemAdjQuantity);
			}
			if (statusCode == ErrorCode.ERROR_ITEM_ADJ_SAVE_SUCCESS) {
				itemModel.setItemAdjId(this.itemAdjId);
				statusCode = itemModel.deleteItemAdjustment();
			} else {
				statusCode = ErrorCode.ERROR_ITEM_ADJ_DELETE_FAILED;
			}
			if (statusCode == ErrorCode.ERROR_ITEM_ADJ_DELETE_SUCCESS) {
				if (this.getItemAdjIstId()) {
					var ItemStockTracking = require('./../BizLogic/ItemStockTracking.js');
					var itemStockTracking = new ItemStockTracking();
					var itemStockTrackingData = itemStockTracking.getItemStockTrackingDataByIstId(this.getItemAdjIstId());
					var quantity = itemStockTrackingData.getIstCurrentQuantity();
					if (this.getItemAdjType() == TxnTypeConstant.TXN_TYPE_ADJ_ADD) {
						statusCode = itemStockTracking.changeISTQuantity(itemStockTrackingData.getIstId(), Number(quantity) - Number(this.getItemAdjQuantity()), false, false);
					} else {
						statusCode = itemStockTracking.changeISTQuantity(itemStockTrackingData.getIstId(), Number(quantity) + Number(this.getItemAdjQuantity()), false, false);
					}

					if (statusCode == ErrorCode.ERROR_IST_SUCCESS || statusCode == ErrorCode.ERROR_ITEM_STOCK_QUANITY_UPDATE_SUCCESS) {
						statusCode = ErrorCode.ERROR_ITEM_ADJ_DELETE_SUCCESS;
					}
				}
			}

			if (statusCode == ErrorCode.ERROR_ITEM_ADJ_DELETE_SUCCESS) {
				if (!transactionManager.CommitTransaction()) {
					statusCode = ErrorCode.ERROR_ITEM_ADJ_DELETE_FAILED;
				}
			}
		} catch (err) {
			logger.error('Delete transaction exception ' + err);
		}
		transactionManager.EndTransaction();
		return statusCode;
	};

	this.loadItemAdjustmentTxn = function (txnId) {
		var DataLoader = require('./../DBManager/DataLoader.js');
		var dataloader = new DataLoader();
		var txnModel = dataloader.getItemAdjRowForItemAdjId(txnId);
		console.log(txnModel);
		txnModel = txnModel[0];
		if (txnModel) {
			this.itemAdjId = txnModel.itemAdjId;
			this.itemAdjDate = txnModel.itemAdjDate;
			this.itemAdjDescription = txnModel.itemAdjDescription;
			this.itemAdjItemId = txnModel.itemAdjItemId;
			this.itemAdjType = txnModel.itemAdjType;
			this.itemAdjQuantity = txnModel.itemAdjQuantity;
			this.itemAdjIstId = txnModel.itemAdjIstId;
			this.itemAdjAtPrice = txnModel.itemAdjAtPrice;
		}
	};

	this.updateItemAdjustment = function (itemId, adjType, adjQuantity, adjDate, adjDesc, atPrice) {
		var istData = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : false;

		var statusCode = ErrorCode.SUCCESS;
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var transactionManager = new TransactionManager();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_ITEM_ADJ_SAVE_FAILED;
			return statusCode;
		}

		try {
			statusCode = this.deleteAdjTxn();

			if (statusCode == ErrorCode.ERROR_ITEM_ADJ_DELETE_SUCCESS) {
				statusCode = this.addItemAdjustment(itemId, adjType, adjQuantity, adjDate, adjDesc, atPrice, istData);
			} else {
				statusCode = ErrorCode.ERROR_ITEM_ADJ_SAVE_FAILED;
			}
			if (statusCode == ErrorCode.ERROR_ITEM_ADJ_SAVE_SUCCESS) {
				if (!transactionManager.CommitTransaction()) {
					statusCode = ErrorCode.ERROR_ITEM_ADJ_SAVE_FAILED;
				}
			}
		} catch (err) {
			logger.error('update item adjustment exception ' + err);
		}

		transactionManager.EndTransaction();
		return statusCode;
	};

	this.addItemAdjustment = function (itemId, adjType, adjQuantity, adjDate, adjDesc, atPrice) {
		var istData = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : false;

		var statusCode = ErrorCode.SUCCESS;
		var ItemAdjModel = require('./../Models/ItemAdjModel.js');
		itemadjmodel = new ItemAdjModel();
		itemadjmodel.setItemAdjItemId(itemId);
		itemadjmodel.setItemAdjType(adjType);

		if (adjQuantity == null || adjQuantity == '') {
			adjQuantity = '0.0';
		}
		if (!atPrice || atPrice == null || atPrice == '' || (atPrice + '').trim() == '') {
			atPrice = '-1';
		}

		itemadjmodel.setItemAdjAtPrice(atPrice);
		itemadjmodel.setItemAdjQuantity(adjQuantity);
		itemadjmodel.setItemAdjDate(adjDate);
		itemadjmodel.setItemAdjDescription(adjDesc);

		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var transactionmanager = new TransactionManager();

		var isTransactionBeginSuccess = transactionmanager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_ITEM_ADJ_SAVE_FAILED;
			var itemCache = new ItemCache();
			itemCache.reloadItemCache();
			return statusCode;
		}

		try {
			var istId;

			if (!istData) {
				istId = 0;
			} else {
				var ItemStockTracking = require('./../BizLogic/ItemStockTracking.js');
				var itemStockTracking = new ItemStockTracking();

				var itemStockTrackingData = itemStockTracking.getISTWithParameters(istData['istBatchNo'], istData['istSerialNo'], istData['istMRP'], istData['istExpDate'], istData['istMfgDate'], istData['istSize'], itemId);

				if (itemStockTrackingData.getIstId()) {
					istId = itemStockTrackingData.getIstId();
					var currentQuantity = itemStockTrackingData.getIstCurrentQuantity();
					var quantity = adjType == TxnTypeConstant.TXN_TYPE_ADJ_ADD ? Number(currentQuantity) + Number(adjQuantity) : Number(currentQuantity) - Number(adjQuantity);
					statusCode = itemStockTracking.changeISTQuantity(istId, quantity, false, false);
				} else {
					var quantity = adjType == TxnTypeConstant.TXN_TYPE_ADJ_ADD ? Number(adjQuantity) : -Number(adjQuantity);

					istId = itemStockTracking.addItemStockTracking(istData['istBatchNo'], istData['istSerialNo'], istData['istMRP'], istData['istExpDate'], istData['istMfgDate'], istData['istSize'], itemId, quantity, IstType.NORMAL);

					statusCode = itemStockTracking.getErrorCodeForResult(istId);
				}
			}
			if (istId == 0) {
				istId = null;
			}
			itemadjmodel.setItemAdjIstId(istId);

			if (statusCode == ErrorCode.ERROR_IST_SUCCESS || statusCode == ErrorCode.SUCCESS || statusCode == ErrorCode.ERROR_ITEM_STOCK_QUANITY_UPDATE_SUCCESS) {
				statusCode = itemadjmodel.addItemAdjustment();
			} else {
				statusCode = ErrorCode.ERROR_ITEM_ADJ_SAVE_FAILED;
			}

			if (statusCode == ErrorCode.ERROR_ITEM_ADJ_SAVE_SUCCESS) {
				statusCode = this.updatItemStockQuantity(itemadjmodel.getItemAdjItemId(), adjType, itemadjmodel.getItemAdjQuantity());
			}
			if (statusCode == ErrorCode.ERROR_ITEM_ADJ_SAVE_SUCCESS) {
				if (!transactionmanager.CommitTransaction()) {
					statusCode = ErrorCode.ERROR_ITEM_ADJ_SAVE_FAILED;
				}
			} else {
				var itemCache = new ItemCache();
				itemCache.reloadItemCache();
			}
		} catch (err) {
			logger.error('Adding item adj exception ' + err);
			var itemCache = new ItemCache();
			itemCache.reloadItemCache();
		}

		transactionmanager.EndTransaction();
		return statusCode;
	};
};
module.exports = ItemAdjustmentTxn;