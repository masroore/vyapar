var TaxReportObjectModel = function TaxReportObjectModel() {
	this.taxIn = 0;
	this.taxOut = 0;
	this.partyName = '';

	this.getPartyName = function () {
		return this.partyName;
	};

	this.setPartyName = function (name) {
		this.partyName = name;
	};

	this.getTaxIn = function () {
		return this.taxIn;
	};

	this.setTaxIn = function (taxIn) {
		this.taxIn = taxIn;
	};

	this.getTaxOut = function () {
		return this.taxOut;
	};

	this.setTaxOut = function (taxOut) {
		this.taxOut = taxOut;
	};
};

module.exports = TaxReportObjectModel;