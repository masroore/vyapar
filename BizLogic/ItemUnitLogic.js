var ItemUnitLogic = function ItemUnitLogic() {
	var ErrorCode = require('./../Constants/ErrorCode.js');
	var Defaults = require('./../Constants/Defaults.js');
	var ItemUnitCache = require('./../Cache/ItemUnitCache.js');
	var ItemUnitModel = require('./../Models/ItemUnitModel.js');
	var TransactionManager = require('./../DBManager/TransactionManager.js');
	this.unitId;
	this.unitName;
	this.unitShortName;
	this.fullNameEditable = Defaults.DEFAULT_FULL_NAME_EDITABLE;
	this.unitDeletable = Defaults.DEFAULT_UNIT_DELETABLE;

	this.getUnitId = function () {
		return this.unitId;
	};

	this.setUnitId = function (unitId) {
		this.unitId = unitId;
	};

	this.getUnitName = function () {
		return this.unitName;
	};

	this.setUnitName = function (unitName) {
		this.unitName = unitName;
	};

	this.getUnitShortName = function () {
		return this.unitShortName;
	};

	this.setUnitShortName = function (unitShortName) {
		this.unitShortName = unitShortName;
	};

	this.isFullNameEditable = function () {
		return this.fullNameEditable;
	};

	this.setFullNameEditable = function (fullNameEditable) {
		this.fullNameEditable = fullNameEditable;
	};

	this.isUnitDeletable = function () {
		return this.unitDeletable;
	};

	this.setUnitDeletable = function (unitDeletable) {
		this.unitDeletable = unitDeletable;
	};

	this.addNewUnit = function (fullname, shortname) {
		var errorCode = ErrorCode.SUCCESS;
		if (!fullname || !fullname.trim()) {
			errorCode = ErrorCode.ERROR_UNIT_FULL_NAME_EMPTY_FAILED;
			return errorCode;
		}
		if (!shortname || !shortname.trim()) {
			errorCode = ErrorCode.ERROR_UNIT_SHORT_NAME_EMPTY_FAILED;
			return errorCode;
		}
		fullname = fullname.trim();
		shortname = shortname.trim();
		var itemUnitCache = new ItemUnitCache();
		if (itemUnitCache.containsItemUnitWithShortName(shortname)) {
			errorCode = ErrorCode.ERROR_UNIT_WITH_SAME_SHORT_NAME_EXISTS;
			return errorCode;
		}
		//
		if (itemUnitCache.containsItemUnitWithFullName(fullname)) {
			errorCode = ErrorCode.ERROR_UNIT_WITH_SAME_FULL_NAME_EXISTS;
			return errorCode;
		}
		var transactionManager = new TransactionManager();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			errorCode = ErrorCode.ERROR_UNIT_SAVE_FAILED;
			return errorCode;
		}

		if (errorCode == ErrorCode.SUCCESS) {
			var itemUnitModel = new ItemUnitModel();
			itemUnitModel.setUnitName(fullname);
			itemUnitModel.setUnitShortName(shortname);
			errorCode = itemUnitModel.addItemUnit();
		}

		if (errorCode == ErrorCode.ERROR_UNIT_SAVE_SUCCESS) {
			if (!transactionManager.CommitTransaction()) {
				errorCode = ErrorCode.ERROR_UNIT_SAVE_FAILED;
			} else {
				itemUnitCache.refreshItemUnitCache();
			}
		}

		transactionManager.EndTransaction();
		return errorCode;
	};

	this.updateUnit = function (unitId, fullname, shortname) {
		var errorCode = ErrorCode.SUCCESS;
		var transactionManager = new TransactionManager();
		var itemUnitCache = new ItemUnitCache();
		if (!fullname || !fullname.trim()) {
			errorCode = ErrorCode.ERROR_UNIT_FULL_NAME_EMPTY_FAILED;
			return errorCode;
		}
		if (!shortname || !shortname.trim()) {
			errorCode = ErrorCode.ERROR_UNIT_SHORT_NAME_EMPTY_FAILED;
			return errorCode;
		}
		if (itemUnitCache.containsItemUnitWithShortNameWithDifferentId(shortname, unitId)) {
			errorCode = ErrorCode.ERROR_UNIT_WITH_SAME_SHORT_NAME_EXISTS;
		}
		if (itemUnitCache.containsItemUnitWithFullNameWithDifferentId(fullname, unitId)) {
			errorCode = ErrorCode.ERROR_UNIT_WITH_SAME_FULL_NAME_EXISTS;
		}

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			errorCode = ErrorCode.ERROR_UNIT_UPDATE_FAILED;
			return errorCode;
		}

		if (errorCode == ErrorCode.SUCCESS) {
			var itemUnitModel = new ItemUnitModel();
			itemUnitModel.setUnitId(unitId);
			itemUnitModel.setUnitName(fullname);
			itemUnitModel.setUnitShortName(shortname);

			errorCode = itemUnitModel.updateUnit();
		}

		if (errorCode == ErrorCode.ERROR_UNIT_UPDATE_SUCCESS) {
			if (!transactionManager.CommitTransaction()) {
				errorCode = ErrorCode.ERROR_UNIT_UPDATE_FAILED;
			} else {
				itemUnitCache.refreshItemUnitCache();
			}
		}

		transactionManager.EndTransaction();
		return errorCode;
	};

	this.deleteUnit = function () {
		var statusCode = ErrorCode.ERROR_UNIT_DELETE_FAILED;
		// var itemUnitModel = new ItemUnitModel();
		var transactionManager = new TransactionManager();
		var itemUnitCache = new ItemUnitCache();
		try {
			var isTransactionBeginSuccess = transactionManager.BeginTransaction();

			if (!isTransactionBeginSuccess) {
				statusCode = ErrorCode.ERROR_UNIT_DELETE_FAILED;
				return statusCode;
			}

			var itemUnitModel = new ItemUnitModel();
			itemUnitModel.setUnitId(this.unitId);
			statusCode = itemUnitModel.deleteUnit();

			if (statusCode == ErrorCode.ERROR_UNIT_DELETE_SUCCESS) {
				if (!transactionManager.CommitTransaction()) {
					statusCode = ErrorCode.ERROR_UNIT_DELETE_FAILED;
				} else {
					itemUnitCache.refreshItemUnitCache();
				}
			}
		} catch (err) {
			console.log(err);
		}

		transactionManager.EndTransaction();
		return statusCode;
	};
};

module.exports = ItemUnitLogic;