var GSTR9HsnReportObject = function GSTR9HsnReportObject() {
    this.itemId;
    this.itemName = '';
    this.itemUnitShort = '';
    this.itemUnitFull;
    this.itemQuantity = 0;
    this.itemFreeQuantity = 0;
    this.itemTotalValue = 0;
    this.itemTaxableValue = 0;
    this.itemIGSTAmount = 0;
    this.itemSGSTAmount = 0;
    this.itemCGSTAmount = 0;
    this.itemCESSAmount = 0;
    this.AdditionalCessAmt = 0;
    this.itemOTHERAmount = 0;
    this.cessRate = 0.0;
    this.itemHSN = '';
    this.rateOfTax = 0;

    this.getRateOfTax = function () {
        return this.rateOfTax;
    };

    this.setRateOfTax = function (rateOfTax) {
        this.rateOfTax = rateOfTax;
    };

    this.getItemHSN = function () {
        return this.itemHSN;
    };
    this.setItemHSN = function (itemHSN) {
        this.itemHSN = itemHSN;
    };
    this.getItemId = function () {
        return this.itemId;
    };
    this.setItemId = function (itemId) {
        this.itemId = itemId;
    };
    this.getItemName = function () {
        return this.itemName;
    };
    this.setItemName = function (itemName) {
        this.itemName = itemName;
    };
    this.getItemUnitShort = function () {
        return this.itemUnitShort;
    };
    this.setItemUnitShort = function (itemUnitShort) {
        this.itemUnitShort = itemUnitShort;
    };
    this.getItemUnitFull = function () {
        return this.itemUnitFull;
    };
    this.setItemUnitFull = function (itemUnitFull) {
        this.itemUnitFull = itemUnitFull;
    };
    this.getItemQuantity = function () {
        return this.itemQuantity;
    };
    this.setItemQuantity = function (itemQuantity) {
        this.itemQuantity = itemQuantity;
    };
    this.getItemFreeQuantity = function () {
        return this.itemFreeQuantity;
    };
    this.setItemFreeQuantity = function (itemFreeQuantity) {
        this.itemFreeQuantity = itemFreeQuantity;
    };
    this.getItemTotalValue = function () {
        return this.itemTotalValue;
    };
    this.setItemTotalValue = function (itemTotalValue) {
        this.itemTotalValue = itemTotalValue;
    };
    this.getItemTaxableValue = function () {
        return this.itemTaxableValue;
    };
    this.setItemTaxableValue = function (itemTaxableValue) {
        this.itemTaxableValue = itemTaxableValue;
    };
    this.getIGSTAmt = function () {
        return this.itemIGSTAmount;
    };
    this.setIGSTAmt = function (itemIGSTAmount) {
        this.itemIGSTAmount = itemIGSTAmount;
    };
    this.getSGSTAmt = function () {
        return this.itemSGSTAmount;
    };
    this.setSGSTAmt = function (itemSGSTAmount) {
        this.itemSGSTAmount = itemSGSTAmount;
    };
    this.getCGSTAmt = function () {
        return this.itemCGSTAmount;
    };
    this.setCGSTAmt = function (itemCGSTAmount) {
        this.itemCGSTAmount = itemCGSTAmount;
    };
    this.getOTHERAmt = function () {
        return this.itemOTHERAmount;
    };
    this.setOTHERAmt = function (itemOTHERAmount) {
        this.itemOTHERAmount = itemOTHERAmount;
    };
    this.getCESSAmt = function () {
        return this.itemCESSAmount;
    };
    this.setCESSAmt = function (itemCESSAmount) {
        this.itemCESSAmount = itemCESSAmount;
    };
    this.getAdditionalCessAmt = function () {
        return this.AdditionalCessAmt;
    };
    this.setAdditionalCessAmt = function (AdditionalCessAmt) {
        this.AdditionalCessAmt = AdditionalCessAmt;
    };
    this.getCessRate = function () {
        return this.cessRate;
    };
    this.setCessRate = function (cessRate) {
        this.cessRate = cessRate;
    };
};

module.exports = GSTR9HsnReportObject;