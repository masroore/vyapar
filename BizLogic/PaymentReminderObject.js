var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
var ErrorCode = require('../Constants/ErrorCode');
var PaymentReminderObject = function PaymentReminderObject(nameId, name, balanceAmount, remindOnDate, sendSMSOnDate) {
	this.getNameId = function () {
		return nameId;
	};

	this.setNameId = function (NameId) {
		nameId = NameId;
	};

	this.getName = function () {
		return name;
	};

	this.setName = function (Name) {
		name = Name;
	};

	this.getBalanceAmount = function () {
		return balanceAmount;
	};

	this.setBalanceAmount = function (BalanceAmount) {
		balanceAmount = BalanceAmount;
	};

	this.getRemindOnDate = function () {
		return remindOnDate;
	};

	this.setRemindOnDate = function (RemindOnDate) {
		remindOnDate = RemindOnDate;
	};

	this.getSendSMSOnDate = function () {
		return sendSMSOnDate;
	};

	this.setSendSMSOnDate = function (SendSMSOnDate) {
		sendSMSOnDate = SendSMSOnDate;
	};

	this.updateRemindOnDate = function (remindOnDate) {
		var sqlitedbhelper = new SqliteDBHelper();
		var statusCode = ErrorCode.ERROR_PAYMENT_ALERT_DATE_SAVE_FAILURE;
		var retVal = sqlitedbhelper.updatePaymentReminderAlertDates(this.getNameId(), remindOnDate, 'remindon');
		if (retVal == 1) {
			statusCode = ErrorCode.ERROR_PAYMENT_ALERT_DATE_SAVE_SUCCESS;
		}
		return statusCode;
	};

	this.updateSendSMSOnDate = function (sendSMSOnDate) {
		var sqlitedbhelper = new SqliteDBHelper();
		var statusCode = ErrorCode.ERROR_PAYMENT_ALERT_DATE_SAVE_FAILURE;
		var retVal = sqlitedbhelper.updatePaymentReminderAlertDates(this.getNameId(), sendSMSOnDate, 'sendsms');
		if (retVal == 1) {
			statusCode = ErrorCode.ERROR_PAYMENT_ALERT_DATE_SAVE_SUCCESS;
		}
		return statusCode;
	};

	this.updateNoneDate = function () {
		var sqlitedbhelper = new SqliteDBHelper();
		var statusCode = ErrorCode.ERROR_PAYMENT_ALERT_DATE_SAVE_FAILURE;
		var retVal = sqlitedbhelper.updatePaymentReminderAlertDates(this.getNameId(), null, null);
		if (retVal == 1) {
			statusCode = ErrorCode.ERROR_PAYMENT_ALERT_DATE_SAVE_SUCCESS;
		}
		return statusCode;
	};
};

module.exports = PaymentReminderObject;