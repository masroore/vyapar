var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CloseBookBiz = function () {
	function CloseBookBiz(closingDate) {
		(0, _classCallCheck3.default)(this, CloseBookBiz);

		this.closingDate = closingDate;
		this.newOpeningDate = new Date(closingDate.getFullYear(), closingDate.getMonth(), closingDate.getDate() + 1);
		this.isSuccess = false;
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		this.transactionManager = new TransactionManager();
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		this.sqliteDBHelper = new SqliteDBHelper();
		var DataLoader = require('./../DBManager/DataLoader.js');
		this.dataLoader = new DataLoader();
		this.ErrorCode = require('./../Constants/ErrorCode.js');
		this.MyDate = require('./../Utilities/MyDate.js');
	}

	(0, _createClass3.default)(CloseBookBiz, [{
		key: 'canCloseBook',
		value: function canCloseBook(closeBookDate) {
			return this.sqliteDBHelper.canCloseBook(closeBookDate);
		}
	}, {
		key: 'startCondensing',
		value: function startCondensing() {
			var isTransactionBeginSuccess = this.transactionManager.BeginTransaction();
			return isTransactionBeginSuccess;
		}
	}, {
		key: 'stopCondensing',
		value: function stopCondensing(isSuccess) {
			var beginWasSuccessFull = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
			var isCloseBook = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

			var isTransactionCommitSuccess = false;
			if (beginWasSuccessFull) {
				isTransactionCommitSuccess = true;
				if (isSuccess) {
					isTransactionCommitSuccess = this.transactionManager.CommitTransaction(isCloseBook);
				}
				this.transactionManager.EndTransaction();
			}
			return isTransactionCommitSuccess;
		}
	}, {
		key: 'condensePrefixFlow',
		value: function condensePrefixFlow(prefixObj) {
			return this.sqliteDBHelper.setPrefixForCloseBook(prefixObj);
		}
	}, {
		key: 'condenseParties',
		value: function condenseParties() {
			var statusCode = this.ErrorCode.ERROR_NAME_SAVE_SUCCESS;
			var retVal = true;
			try {
				retVal = this.sqliteDBHelper.deleteLinkDataBeforeClosingDate(this.closingDate);

				if (retVal) {
					retVal = this.sqliteDBHelper.condenseOpeningBalanceTxnLinks();
				}
				var openingBalancesAfterClosingDate = this.sqliteDBHelper.getOpeningBalancesAfterClosingDate(this.closingDate);
				var expectedValues = this.sqliteDBHelper.getExpectedNameBalance(this.closingDate);
				var currentValues = this.sqliteDBHelper.getExpectedNameBalance(null);

				if (retVal) {
					var TransactionModel = require('./../Models/TransactionModel.js');
					var _iteratorNormalCompletion = true;
					var _didIteratorError = false;
					var _iteratorError = undefined;

					try {
						for (var _iterator = (0, _getIterator3.default)(expectedValues.keys()), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
							var nameId = _step.value;

							var openingBalanceAfterClosingDate = openingBalancesAfterClosingDate.has(nameId) ? MyDouble.convertStringToDouble(openingBalancesAfterClosingDate.get(nameId)) : 0;
							var newOpeningBal = MyDouble.convertStringToDouble(expectedValues.get(nameId), true) + openingBalanceAfterClosingDate;
							var isReceivable = newOpeningBal >= 0.0 ? TxnTypeConstant['TXN_TYPE_ROPENBALANCE'] : TxnTypeConstant['TXN_TYPE_POPENBALANCE'];
							var oldOpeningBalanceTxnModel = this.sqliteDBHelper.getOpeningBalanceTransactionOnNameId(nameId);
							var status = false;
							var TxnPaymentStatusConstants = require('./../Constants/TxnPaymentStatusConstants.js');
							if (oldOpeningBalanceTxnModel) {
								oldOpeningBalanceTxnModel.set_txn_type(isReceivable);
								oldOpeningBalanceTxnModel.set_txn_date(this.newOpeningDate);
								oldOpeningBalanceTxnModel.set_balance_amount(Math.abs(newOpeningBal));
								oldOpeningBalanceTxnModel.setTxnPaymentStatus(TxnPaymentStatusConstants.UNPAID);
								oldOpeningBalanceTxnModel.setTxnCurrentBalanceAmount(Math.abs(newOpeningBal));
								statusCode = this.sqliteDBHelper.UpdateTransactionRecord(oldOpeningBalanceTxnModel);
								if (statusCode == this.ErrorCode.ERROR_TXN_SAVE_SUCCESS) {
									status = true;
								}
							} else if (Math.abs(newOpeningBal) > 0) {
								var txnModel = new TransactionModel();
								txnModel.set_txn_date(this.newOpeningDate);
								txnModel.set_balance_amount(Math.abs(newOpeningBal));
								txnModel.set_txn_type(isReceivable);
								txnModel.set_txn_name_id(nameId);
								txnModel.setTxnPaymentStatus(TxnPaymentStatusConstants.UNPAID);
								txnModel.setTxnCurrentBalanceAmount(Math.abs(newOpeningBal));
								var txnId = this.sqliteDBHelper.createTransactionRecord(txnModel);
								if (txnId) {
									status = true;
								}
							}
							if (status) {
								var newCurrentBalance = MyDouble.convertStringToDouble(currentValues.get(nameId), true);
								statusCode = this.sqliteDBHelper.updateNameBalanceByNameId(nameId, newCurrentBalance);
							}
							if (statusCode != this.ErrorCode.ERROR_NAME_SAVE_SUCCESS) {
								retVal = false;
								break;
							}
						}
					} catch (err) {
						_didIteratorError = true;
						_iteratorError = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion && _iterator.return) {
								_iterator.return();
							}
						} finally {
							if (_didIteratorError) {
								throw _iteratorError;
							}
						}
					}
				}
			} catch (err) {
				retVal = false;
				logger.error('Close Book Condense Parties exception: ' + err);
			}
			return retVal;
		}
	}, {
		key: 'condenseItems',
		value: function condenseItems() {
			var openingStockAfterClosingDate = this.sqliteDBHelper.getItemOpeningQtyAfterClosingDate(this.closingDate);
			var expectedValues = this.sqliteDBHelper.getItemExpectedData(this.closingDate);
			var currentValues = this.sqliteDBHelper.getItemExpectedData();
			var retVal = true;
			try {
				var ItemCache = require('./../Cache/ItemCache.js');
				var itemCache = new ItemCache();

				var _iteratorNormalCompletion2 = true;
				var _didIteratorError2 = false;
				var _iteratorError2 = undefined;

				try {
					for (var _iterator2 = (0, _getIterator3.default)(expectedValues.keys()), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
						var itemId = _step2.value;

						var item = itemCache.getItemById(itemId);
						if (item) {
							var openingQuantityAfterClosingDate = openingStockAfterClosingDate.has(itemId) ? Number(openingStockAfterClosingDate.get(itemId)) : 0;
							var newOpeningQuantity = expectedValues.get(itemId) + openingQuantityAfterClosingDate;
							var newOpeningStockValue = this.sqliteDBHelper.getItemStockValue(itemId, newOpeningQuantity, this.closingDate);
							var newAdjAtPrice = -1.0;
							if (newOpeningQuantity > 0.0 && newOpeningStockValue >= 0.0) {
								newAdjAtPrice = newOpeningStockValue / newOpeningQuantity;
							}

							// create opening adjustment transaction with new unit price
							if (this.updateOpeningStockAdjTxn(itemId, newOpeningQuantity, newAdjAtPrice, this.newOpeningDate) == false) {
								retVal = false;
								break;
							}
							// delete all old adjustments and opening adjustment
							if (this.sqliteDBHelper.deleteAllItemAdj(itemId, this.closingDate) == false) {
								retVal = false;
								break;
							}

							// update item values in item table
							if (retVal == true) {
								var currentStockQuantity = currentValues.get(itemId);
								var currentStockValue = this.sqliteDBHelper.getItemStockValue(itemId, currentStockQuantity, null);
								item.setItemStockQuantity(currentStockQuantity);
								item.setItemStockValue(currentStockValue);
								var statusCode = item.updateItem();

								if (statusCode != this.ErrorCode.ERROR_ITEM_SAVE_SUCCESS) {
									retVal = false;
									break;
								}
							}
						}
					}
				} catch (err) {
					_didIteratorError2 = true;
					_iteratorError2 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion2 && _iterator2.return) {
							_iterator2.return();
						}
					} finally {
						if (_didIteratorError2) {
							throw _iteratorError2;
						}
					}
				}
			} catch (err) {
				retVal = false;
				logger.error('Close Book Condense Items exception: ' + err);
			}
			return retVal;
		}
	}, {
		key: 'updateOpeningStockAdjTxn',
		value: function updateOpeningStockAdjTxn(itemId, newQuantity, newAtPrice, newOpeningDate) {
			var adjModel = this.dataLoader.getAdjObjForOpening(itemId);
			var statusCode = this.ErrorCode.FAILED;
			var retVal = false;
			var StringConstant = require('./../Constants/StringConstants.js');
			var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');

			if (adjModel.getItemAdjId() > 0) {
				adjModel.setItemAdjQuantity(newQuantity);
				adjModel.setItemAdjAtPrice(newAtPrice);
				adjModel.setItemAdjDate(newOpeningDate);
				statusCode = adjModel.updateItemAdjustment();
			} else {
				adjModel.setItemAdjItemId(itemId);
				adjModel.setItemAdjQuantity(newQuantity);
				adjModel.setItemAdjAtPrice(newAtPrice);
				adjModel.setItemAdjDate(newOpeningDate);
				adjModel.setItemAdjType(TxnTypeConstant.TXN_TYPE_ADJ_OPENING);
				adjModel.setItemAdjDescription(StringConstant.itemOpeningStockString);
				statusCode = adjModel.addItemAdjustment();
			}

			if (statusCode == this.ErrorCode.ERROR_ITEM_ADJ_UPDATE_SUCCESS || statusCode == this.ErrorCode.ERROR_ITEM_ADJ_SAVE_SUCCESS) {
				retVal = true;
			}

			return retVal;
		}
	}, {
		key: 'condenseBanks',
		value: function condenseBanks() {
			var retVal = true;
			try {
				var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
				var paymentInfoCache = new PaymentInfoCache();
				var bankList = paymentInfoCache.getBankListObj(false);
				var bankBalanceMap = this.sqliteDBHelper.getBankBalanceForAllBanks(this.closingDate);
				if (bankList && bankList.length > 0) {
					for (var i = 0; i < bankList.length; i++) {
						var bankObject = bankList[i];
						var bankOpeningBalance = bankBalanceMap.get(Number(bankObject.getId()));
						if (typeof bankOpeningBalance == 'number') {
							if (this.MyDate.compareOnlyDatePart(this.closingDate, bankObject.getOpeningDate()) < 0) {
								bankOpeningBalance += Number(bankObject.getOpeningBalance());
							}
							bankObject.setOpeningBalance(bankOpeningBalance);
							bankObject.setOpeningDate(this.newOpeningDate);
							if (bankObject.updateInfo() != this.ErrorCode.ERROR_UPDATE_BANK_INFO_SUCCESS) {
								retVal = false;
								break;
							}
						}
					}
				}
			} catch (err) {
				logger.error('Error condensing banks during close book', err);
				retVal = false;
			}
			return retVal;
		}
	}, {
		key: 'condenseCashInHand',
		value: function condenseCashInHand() {
			var openingCashInHandAmount = this.sqliteDBHelper.getCashInHandAmount(this.closingDate, true);
			var retVal = false;

			var cashAdjustmentTxn = this.dataLoader.getOpeningCashInHandTxn();

			if (cashAdjustmentTxn && cashAdjustmentTxn.getAdjId() > 0) {
				if (this.MyDate.compareOnlyDatePart(this.closingDate, cashAdjustmentTxn.getAdjDate()) < 0) {
					openingCashInHandAmount += Number(cashAdjustmentTxn.getAdjAmount());
				}
				cashAdjustmentTxn.setAdjAmount(openingCashInHandAmount);
				cashAdjustmentTxn.setAdjDate(this.newOpeningDate);
				var status = cashAdjustmentTxn.updateCashAdjustment();
				if (status == this.ErrorCode.ERROR_UPDATE_CASH_ADJUSTMENT_SUCCESS) {
					retVal = true;
				} else {
					retVal = false;
				}
			} else if (openingCashInHandAmount != 0) {
				var StringConstant = require('./../Constants/StringConstants.js');
				var _TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
				cashAdjustmentTxn.setAdjAmount(openingCashInHandAmount);
				cashAdjustmentTxn.setAdjDate(this.newOpeningDate);
				cashAdjustmentTxn.setAdjDescription(StringConstant.openCashInHandString);
				cashAdjustmentTxn.setAdjType(_TxnTypeConstant.TXN_TYPE_CASH_OPENINGBALANCE);
				var statusCode = cashAdjustmentTxn.saveNewAdjustment();
				if (statusCode == this.ErrorCode.ERROR_NEW_CASH_ADJUSTMENT_SUCCESS) {
					retVal = true;
				} else {
					retVal = false;
				}
			} else {
				retVal = true;
			}

			if (retVal) {
				retVal = this.sqliteDBHelper.deleteAllCashAdjs(this.closingDate);
				if (retVal == true) {
					retVal = this.sqliteDBHelper.deleteAllBankAdjs(this.closingDate);
				}
			}

			return retVal;
		}
	}, {
		key: 'condenseCheques',
		value: function condenseCheques() {
			var isSuccess = this.sqliteDBHelper.migrateOpenCheques(this.closingDate);
			if (isSuccess) {
				isSuccess = this.sqliteDBHelper.deleteAllClosedCheques(this.closingDate);
			}
			return isSuccess;
		}
	}, {
		key: 'condenseLoans',
		value: function condenseLoans() {
			var _this = this;

			var LoanTransactionModel = require('../Models/LoanTransactionModel');
			var TxnTypeConstant = require('../Constants/TxnTypeConstant');
			var LoanAccountCache = require('../Cache/LoanAccountCache');
			var loanAccountCache = new LoanAccountCache();
			var isSuccess = true;
			try {
				var loanAccounts = loanAccountCache.getAllLoanAccounts();
				// Close only the loan accounts starting before closing date
				loanAccounts = loanAccounts.filter(function (loanAccount) {
					var openingDate = loanAccount.openingDate;
					var closeBookDate = loanAccount.closeBookDate;
					if (closeBookDate) {
						return _this.closingDate.getTime() >= closeBookDate.getTime();
					}
					return _this.closingDate.getTime() >= openingDate.getTime();
				});
				var loanAccountBalances = this.sqliteDBHelper.getLoanAccountBalances({
					tillDate: this.closingDate
				});
				isSuccess = isSuccess && this.sqliteDBHelper.deleteAllLoanTransactions(this.closingDate);
				if (!isSuccess) {
					return isSuccess;
				}
				var _iteratorNormalCompletion3 = true;
				var _didIteratorError3 = false;
				var _iteratorError3 = undefined;

				try {
					for (var _iterator3 = (0, _getIterator3.default)(loanAccounts), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var loanAccount = _step3.value;

						// Add close book transaction
						var loanAccountId = loanAccount.getLoanAccountId();
						var loanTransaction = new LoanTransactionModel();
						var closeBookBalance = loanAccountBalances.get(Number(loanAccountId)) || 0;
						closeBookBalance = Number(closeBookBalance);
						loanTransaction.setLoanAccountId(loanAccountId);
						loanTransaction.setLoanType(TxnTypeConstant.TXN_TYPE_LOAN_CLOSE_BOOK_ADJ);
						loanTransaction.setPrincipalAmount(closeBookBalance);
						loanTransaction.setInterestAmount(0);
						loanTransaction.setPaymentTypeId(loanAccount.getLoanReceivedIn());
						loanTransaction.setTransactionDate(this.newOpeningDate);
						isSuccess = isSuccess && loanTransaction.saveLoanTxn(false, false);
						if (!isSuccess) {
							return isSuccess;
						}
					}
				} catch (err) {
					_didIteratorError3 = true;
					_iteratorError3 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError3) {
							throw _iteratorError3;
						}
					}
				}
			} catch (ex) {
				isSuccess = false;
			}
			if (isSuccess) {}
			return isSuccess;
		}
	}, {
		key: 'condenseTransactions',
		value: function condenseTransactions() {
			var isSuccess = this.sqliteDBHelper.deleteAllTransactions(this.closingDate, this.newOpeningDate);

			if (isSuccess) {
				isSuccess = this.sqliteDBHelper.processIST();
			}
			return isSuccess;
		}
	}]);
	return CloseBookBiz;
}();

module.exports = CloseBookBiz;