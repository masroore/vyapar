var ViewTransaction = function ViewTransaction() {
	this.viewTransactionFile = function (param) {
		var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
		var DataLoader = require('./../DBManager/DataLoader.js');
		var txnId = param.split(':')[0];
		var txnType = param.split(':')[1];
		var txnType = TxnTypeConstant[txnType];
		var salePurchaseFile = [TxnTypeConstant.TXN_TYPE_SALE, TxnTypeConstant.TXN_TYPE_PURCHASE, TxnTypeConstant.TXN_TYPE_CASHIN, TxnTypeConstant.TXN_TYPE_CASHOUT, TxnTypeConstant.TXN_TYPE_EXPENSE, TxnTypeConstant.TXN_TYPE_OTHER_INCOME, TxnTypeConstant.TXN_TYPE_SALE_RETURN, TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN, TxnTypeConstant.TXN_TYPE_SALE_ORDER, TxnTypeConstant.TXN_TYPE_ESTIMATE, TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN, TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER];
		var bankAdj = [TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD, TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE, TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD_WITHOUT_CASH, TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE_WITHOUT_CASH];
		var cashAdjFile = [TxnTypeConstant.TXN_TYPE_CASH_ADJ_ADD, TxnTypeConstant.TXN_TYPE_CASH_ADJ_REDUCE];
		var itemAdjFile = [TxnTypeConstant.TXN_TYPE_ADJ_ADD, TxnTypeConstant.TXN_TYPE_ADJ_REDUCE];
		var partyOpen = [TxnTypeConstant.TXN_TYPE_POPENBALANCE, TxnTypeConstant.TXN_TYPE_ROPENBALANCE];
		var chequeFile = [TxnTypeConstant.TXN_TYPE_CHEQUE_TRANSFER];
		if ($.inArray(txnType, salePurchaseFile) != -1) {
			param = param.split(':');
			var txnIdReq = param[0];
			var value = param[1];
			var MountComponent = require('../UIComponent/jsx/MountComponent').default;
			var _txnType = TxnTypeConstant[value];
			if (_txnType === TxnTypeConstant.TXN_TYPE_CASHOUT || _txnType === TxnTypeConstant.TXN_TYPE_CASHIN) {
				var Component = require('../UIComponent/jsx/SalePurchaseContainer/PaymentTransaction').default;
				MountComponent(Component, document.querySelector('#salePurchaseContainer'), {
					txnType: _txnType,
					txnId: txnIdReq
				});
			} else {
				var _Component = require('../UIComponent/jsx/SalePurchaseContainer').default;
				MountComponent(_Component, document.querySelector('#salePurchaseContainer'), {
					txnType: _txnType,
					txnId: txnIdReq
				});
			}
		} else if ($.inArray(txnType, bankAdj) != -1) {
			var dataLoader = new DataLoader();
			var bankAdjObj = dataLoader.LoadAccountAdjustmentForId(txnId);
			var bankId = bankAdjObj['adjBankId'];
			var adjId = bankAdjObj['adjId'];
			var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
			var paymentInfoCache = new PaymentInfoCache();
			var bankName = paymentInfoCache.getPaymentBankName(bankId);
			accountAdjustmentIdGlobal = adjId;
			// angular.element(document.getElementById('body')).scope().accountAdjustmentId = adjId;
			accountAdjustmentNameGlobal = bankName;
			// angular.element(document.getElementById('body')).scope().accountAdjustmentName = bankName;
			$('#frameDiv').load('./../UIComponent/NewAccountAdjustment.html');
			$('#modelContainer').css('display', 'block');
			$('.viewItems').css('display', 'none');
		} else if ($.inArray(txnType, cashAdjFile) != -1) {
			var dataLoader = new DataLoader();
			var txnObj = dataLoader.getCashAdjObjOnId(txnId);
			console.log(txnObj);
			var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
			txnObj.getAdjType() == TxnTypeConstant.TXN_TYPE_CASH_ADJ_ADD ? $('#adjType option:eq(0)').prop('selected', true) : $('#adjType option:eq(1)').prop('selected', true);
			$('#adjAmount').val(txnObj.getAdjAmount());
			$('#adjDate').val(MyDate.getDate('d/m/y', txnObj.getAdjDate()));
			$('#adjDescription').val(txnObj.getAdjDescription());
			$('#adjId').val(txnObj.getAdjId());
			$('#saveCashAdj').val('Update');
			$('#dialogCashAdj').dialog({
				'width': 400,
				appendTo: '#defaultPage',
				modal: true
			});
		} else if ($.inArray(txnType, partyOpen) != -1) {
			ToastHelper.info('Edit the party to edit this transaction');
		} else if ($.inArray(txnType, chequeFile) != -1) {
			ToastHelper.info('Re-open the cheque to edit this transaction');
		} else if ($.inArray(txnType, itemAdjFile) != -1) {
			// alert(txnId);
			itemAdjustmentNameGlobal = $('#currentItemName')[0].textContent;
			// angular.element(document.getElementById('body')).scope().itemAdjustmentName = $('#currentItemName')[0].textContent;
			itemAdjustmentIdGlobal = txnId;
			// angular.element(document.getElementById('body')).scope().itemAdjustmentId = txnId;
			$('#frameDiv').load('./../UIComponent/StockAdjustment.html');
			$('#modelContainer').css('display', 'block');
			$('.viewItems').css('display', 'none');
		} else {
			ToastHelper.info('Cannot edit this transaction');
		}
	};
};

module.exports = ViewTransaction;