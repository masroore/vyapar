var BaseOpenBalanceTransaction = function BaseOpenBalanceTransaction() {
	var discountPercent;
	var taxPercent;
	this.discountPercent = 0.0;
	this.taxPercent = 0.0;
	var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
	sqlitedbhelper = new SqliteDBHelper();
	var DataLoader = require('./../DBManager/DataLoader.js');
	var dataloader = new DataLoader();

	this.getOpeningBalanceTransactionOnName = function (nameRef) {
		if (nameRef) {
			var txnModel = dataloader.getOpeningBalanceTransactionOnNameId(nameRef.getNameId());
			return txnModel;
		}
	};

	this.getDiscountPercent = function () {
		return this.discountPercent;
	};

	this.getTaxPercent = function () {
		return 0;
	};

	var newFunc = function newFunc() {
		return 'Hello';
	};

	this.newFunction = function () {
		return newFunc();
	};
};

module.exports = BaseOpenBalanceTransaction;