var sqlitedbhelperForName = void 0;
var nameLogic = function nameLogic(obj) {
	var namecache;
	this.isUpdateContact = 0;
	var ErrorCode = require('./../Constants/ErrorCode.js');
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var NameTypeConstant = require('./../Constants/NameType.js');
	var ErrorCode = require('./../Constants/ErrorCode.js');
	var UDFValueModel = require('./../Models/udfValueModel');

	var ItemCache = require('./../Cache/ItemCache.js');
	// var MyDate = require('./../Utilities/MyDate.js');

	var TxnPaymentStatusConstants = require('./../Constants/TxnPaymentStatusConstants.js');

	var fullName;
	var amount;
	var opening_balance;
	var opening_date;
	var phoneNumber;
	var email;
	var amount;
	var address;
	var nameType;
	var nameId;
	var newTxnObject;
	var oldTxnObject;
	var groupId;
	var tinNumber;
	var self = this;
	this.amount = 0;
	this.groupId;
	this.tinNumber = '';
	this.gstinNumber;
	this.contactState;
	this.customerType;
	this.opening_balance = 0;
	var extraFieldObjectArray = [];

	var lastTxnDate = void 0;
	this.getNameLastTxnDate = function () {
		return lastTxnDate;
	};
	this.setNameLastTxnDate = function (txnDate) {
		lastTxnDate = txnDate;
	};

	this.getOpeningBalance = function () {
		return this.opening_balance;
	};
	this.setOpeningBalance = function (opening_balance) {
		this.opening_balance = opening_balance;
	};

	this.getCustomerType = function () {
		return this.customerType;
	};
	this.setCustomerType = function (customerType) {
		this.customerType = customerType;
	};

	if (obj) {
		this.fullName = obj.name ? obj.name : this.fullName;
		this.phoneNumber = obj.phone_number ? obj.phone_number : this.phoneNumber;
		this.address = obj.addressStr ? obj.addressStr : this.address;
		this.email = obj.email ? obj.email : this.email;
		this.tinNumber = obj.tinNo ? obj.tinNo : this.tinNumber;
		this.nameId = obj.nameId ? obj.nameId : this.nameId;
	}

	this.getGstinNumber = function () {
		return this.gstinNumber;
	};
	this.setGstinNumber = function (gstinNumber) {
		this.gstinNumber = gstinNumber;
	};

	this.getContactState = function () {
		return this.contactState;
	};
	this.setContactState = function (contactState) {
		this.contactState = contactState;
	};

	this.getTinNumber = function () {
		return this.tinNumber;
	};
	this.setTinNumber = function (tinNumber) {
		this.tinNumber = tinNumber;
	};

	this.getGroupId = function () {
		return this.groupId;
	};
	this.setGroupId = function (groupId) {
		this.groupId = groupId;
	};

	this.getNameId = function () {
		return Number(this.nameId);
	};
	this.setNameId = function (nameId) {
		this.nameId = nameId;
	};

	this.setOpeningDate = function (opening_date) {
		this.opening_date = opening_date;
	};

	this.getOpeningDate = function () {
		return this.opening_date;
	};

	this.getFullName = function () {
		return this.fullName;
	};
	this.setFullName = function (fullName) {
		this.fullName = fullName;
	};

	this.getPhoneNumber = function () {
		return this.phoneNumber;
	};
	this.setPhoneNumber = function (phoneNumber) {
		this.phoneNumber = phoneNumber || '';
	};

	this.getEmail = function () {
		return this.email;
	};

	this.setEmail = function (email) {
		this.email = email || '';
	};

	this.getAmount = function () {
		return this.amount;
	};

	this.setAmount = function (amount) {
		this.amount = Number(amount) ? Number(amount) : 0;
	};

	this.getNameType = function () {
		return this.nameType;
	};

	this.setNameType = function (nameType) {
		this.nameType = nameType;
	};

	this.setIsReceivable = function (isReceivable) {
		this.isReceivable = isReceivable;
	};
	this.getIsReceivable = function () {
		return this.isReceivable;
	};

	this.getShippingAddress = function () {
		return this.shippingAddress;
	};

	this.setShippingAddress = function (shippingAddress) {
		this.shippingAddress = shippingAddress || '';
	};

	this.getAddress = function () {
		return this.address;
	};

	this.setAddress = function (address) {
		this.address = address || '';
	};

	this.updateNameToDB = function () {
		var nameModel = require('./../Models/nameModal.js');
		var namemodel = new nameModel();
		namemodel.set_name_id(this.getNameId());
		namemodel.set_phone_number(this.getPhoneNumber());
		namemodel.set_email(this.getEmail());
		namemodel.set_amount(this.getAmount());
		namemodel.set_full_name(this.getFullName());
		namemodel.set_address(this.getAddress());
		namemodel.set_name_type(this.getNameType());
		namemodel.set_group_id(this.getGroupId());
		namemodel.set_tin_number(this.tinNumber);
		namemodel.set_gstin_number(this.gstinNumber);
		namemodel.set_contact_state(this.contactState);
		namemodel.set_tin_number(this.getTinNumber());
		namemodel.set_shipping_address(this.getShippingAddress());
		namemodel.setCustomerType(this.getCustomerType());
		namemodel.setNameLastTxnDate(this.getNameLastTxnDate());
		return namemodel.updateName();
	};

	this.updateNameToDBTransaction = function () {
		var statusCode = ErrorCode.ERROR_NAME_UPDATE_FAILED;
		var NameModel = require('./../Models/nameModal.js');
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var transactionManager = new TransactionManager();
		var isTransactionBeginSuccess = transactionManager.BeginTransaction();
		var NameCache = require('./../Cache/NameCache.js');
		var namecache = new NameCache();
		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_NAME_UPDATE_FAILED;
			return statusCode;
		}

		var namemodel = new NameModel();
		namemodel.set_name_id(this.getNameId());
		namemodel.set_phone_number(this.getPhoneNumber());
		namemodel.set_email(this.getEmail());
		namemodel.set_amount(this.getAmount());
		namemodel.set_full_name(this.getFullName());
		namemodel.set_address(this.getAddress());
		namemodel.set_name_type(this.getNameType());
		namemodel.set_group_id(this.getGroupId());
		namemodel.set_tin_number(this.tinNumber);
		namemodel.set_gstin_number(this.gstinNumber);
		namemodel.set_contact_state(this.contactState);
		namemodel.set_tin_number(this.getTinNumber());
		namemodel.set_shipping_address(this.getShippingAddress());
		namemodel.setCustomerType(this.getCustomerType());

		statusCode = namemodel.updateName();
		if (statusCode == ErrorCode.ERROR_NAME_SAVE_SUCCESS) {
			if (!transactionManager.CommitTransaction()) {
				statusCode = ErrorCode.ERROR_NAME_UPDATE_FAILED;
			}
		}
		if (statusCode !== ErrorCode.ERROR_NAME_SAVE_SUCCESS) {
			namecache.reloadNameCache();
		}

		transactionManager.EndTransaction();
		return statusCode;
	};

	this.updateCurrentBalance = function (balance) {
		this.setAmount(balance);
		return this.updateNameToDB();
	};

	this.getPartyBalanceFromDbByNameId = function (nameId) {
		var DataLoader = require('./../DBManager/DataLoader.js');
		var dataloader = new DataLoader();
		return dataloader.getPartyBalanceAmountById(nameId);
	};

	this.updateAmount = function (transactionObject) {
		var transType = transactionObject.getTxnType();
		var cashAmount = Number(transactionObject.getCashAmount());
		var balanceAmount = Number(transactionObject.getBalanceAmount());
		var discountAmount = Number(transactionObject.getDiscountAmount());
		var partyBalanceAmount = this.getPartyBalanceFromDbByNameId(this.getNameId());

		if (transType == TxnTypeConstant['TXN_TYPE_CASHIN'] || transType == 'TXN_TYPE_CASHIN') {
			partyBalanceAmount -= cashAmount + discountAmount;
		} else if (transType == TxnTypeConstant['TXN_TYPE_CASHOUT'] || transType == 'TXN_TYPE_CASHOUT') {
			partyBalanceAmount += cashAmount + discountAmount;
		} else if (transType == TxnTypeConstant['TXN_TYPE_PURCHASE'] || transType == 'TXN_TYPE_PURCHASE') {
			partyBalanceAmount -= balanceAmount;
		} else if (transType == TxnTypeConstant['TXN_TYPE_SALE'] || transType == 'TXN_TYPE_SALE') {
			partyBalanceAmount += balanceAmount;
		} else if (transType == TxnTypeConstant['TXN_TYPE_POPENBALANCE'] || transType == 'TXN_TYPE_POPENBALANCE') {
			partyBalanceAmount -= balanceAmount;
		} else if (transType == TxnTypeConstant['TXN_TYPE_ROPENBALANCE'] || transType == 'TXN_TYPE_ROPENBALANCE') {
			partyBalanceAmount += balanceAmount;
		} else if (transType == TxnTypeConstant['TXN_TYPE_SALE_RETURN'] || transType == 'TXN_TYPE_SALE_RETURN') {
			partyBalanceAmount -= balanceAmount;
		} else if (transType == TxnTypeConstant['TXN_TYPE_PURCHASE_RETURN'] || transType == 'TXN_TYPE_PURCHASE_RETURN') {
			partyBalanceAmount += balanceAmount;
		}
		this.amount = partyBalanceAmount;
		setCurrentTimeStampInLastTxnDate();
		return this.updateNameToDB();
	};

	var setCurrentTimeStampInLastTxnDate = function () {
		if (!sqlitedbhelperForName) {
			var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
			sqlitedbhelperForName = new SqliteDBHelper();
		}
		var date = sqlitedbhelperForName.getLastTxnDateFromDB(this.getNameId());
		if (date) {
			var tempDate = new Date();
			date.setHours(tempDate.getHours());
			date.setMinutes(tempDate.getMinutes());
			date.setSeconds(tempDate.getSeconds());
			this.setNameLastTxnDate(date);
		} else {
			this.setNameLastTxnDate(null);
		}
	}.bind(this);

	this.deleteAmount = function (txnObject) {
		var txnType = txnObject.getTxnType();
		var cashAmount = Number(txnObject.getCashAmount());
		var balanceAmount = Number(txnObject.getBalanceAmount());
		var discountAmount = Number(txnObject.getDiscountAmount());
		var partyBalanceAmount = this.getPartyBalanceFromDbByNameId(this.getNameId());

		switch (txnType) {
			case TxnTypeConstant.TXN_TYPE_CASHIN:
				partyBalanceAmount += cashAmount + discountAmount;
				break;
			case TxnTypeConstant.TXN_TYPE_CASHOUT:
				partyBalanceAmount -= cashAmount + discountAmount;
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE:
				partyBalanceAmount += balanceAmount;
				break;
			case TxnTypeConstant.TXN_TYPE_SALE:
				partyBalanceAmount -= balanceAmount;
				break;
			case TxnTypeConstant.TXN_TYPE_POPENBALANCE:
				partyBalanceAmount += balanceAmount;
				break;
			case TxnTypeConstant.TXN_TYPE_ROPENBALANCE:
				partyBalanceAmount -= balanceAmount;
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
				partyBalanceAmount += balanceAmount;
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
				partyBalanceAmount -= balanceAmount;
				break;
		}
		this.amount = partyBalanceAmount;
		setCurrentTimeStampInLastTxnDate();
		return this.updateNameToDB();
	};

	this.updateNameBalance = function (newTxnObject, oldTxnObject) {
		var statusCode = ErrorCode['ERROR_NAME_SAVE_SUCCESS'];
		if (oldTxnObject) {
			// if oldTxnObject != null
			if (!newTxnObject || oldTxnObject.getNameId() != newTxnObject.getNameId()) {
				statusCode = oldTxnObject.getNameRef().deleteAmount(oldTxnObject);
			} else {
				statusCode = this.deleteAmount(oldTxnObject);
			}
		}
		if (newTxnObject && statusCode == ErrorCode['ERROR_NAME_SAVE_SUCCESS']) {
			statusCode = this.updateAmount(newTxnObject);
		}
		if (statusCode == ErrorCode['ERROR_NAME_SAVE_SUCCESS']) {
			if (!namecache) {
				var NameCache = require('./../Cache/NameCache.js');
				namecache = new NameCache();
			}
			console.log(this);
			namecache.refreshNameCache(this);
		}
		return statusCode;
	};

	this.ValidateEmail = function (mail) {
		var mailValidator = /^[^@]+@[^@]+\.[^@]+$/;
		if (mailValidator.test(mail)) {
			return ErrorCode['SUCCESS'];
		}
		errorCode = ErrorCode['ERROR_INVALID_EMAILID'];
		return errorCode;
	};

	this.validateData = function () {
		var errorCode = ErrorCode['SUCCESS'];
		// validate name
		if (!this.fullName) {
			errorCode = ErrorCode['ERROR_NAME_EMPTY'];
		}
		if (errorCode != ErrorCode['SUCCESS']) return errorCode;
		// validate phoneNumber
		if (!this.phoneNumber) {
			this.phoneNumber = '';
		}
		if (errorCode != ErrorCode['SUCCESS']) return errorCode;
		// Validate email id
		if (!this.email) {
			this.email = '';
		} else if (this.email.length > 0) {
			errorCode = this.ValidateEmail(this.email);
		}
		return errorCode;
	};

	// passing boolean to check if its expense or name
	this.addName = function (expense, income) {
		console.log('Namelogic page, addName function');
		var errorCode = this.validateData();
		var NameCache = require('./../Cache/NameCache.js');
		namecache = new NameCache();
		if (namecache.nameExists(this.fullName, expense, income) == 1) {
			errorCode = ErrorCode['ERROR_NAME_ALREADY_EXISTS'];
		} else if (errorCode == ErrorCode['SUCCESS']) {
			var nameModel = require('./../Models/nameModal.js');
			var namemodel = new nameModel();

			namemodel.set_full_name(this.fullName);
			namemodel.set_phone_number(this.phoneNumber);
			namemodel.set_email(this.email);
			namemodel.set_address(this.address);
			namemodel.set_group_id(this.groupId);
			namemodel.set_tin_number(this.tinNumber);
			namemodel.set_gstin_number(this.gstinNumber);
			namemodel.set_contact_state(this.contactState);
			namemodel.set_shipping_address(this.shippingAddress);
			namemodel.set_name_type(this.nameType);
			namemodel.setCustomerType(this.getCustomerType());
			errorCode = namemodel.addName();

			if (errorCode == ErrorCode['ERROR_NAME_SAVE_SUCCESS']) {
				this.nameId = namemodel.get_name_id();
				console.log(this.nameId);
			}
		}
		return errorCode;
	};
	var TransactionFactory = require('./TransactionFactory.js');
	var transactionfactory = new TransactionFactory();
	function addUdfValue(extraFieldObjectArray, nameId) {
		var statusCode = ErrorCode.ERROR_NAME_SAVE_SUCCESS;
		if (extraFieldObjectArray && extraFieldObjectArray.length) {
			for (var i = 0; i < extraFieldObjectArray.length; i++) {
				var extraFieldObject = extraFieldObjectArray[i];
				if (extraFieldObject.getUdfFieldValue()) {
					extraFieldObject.setUdfRefId(nameId);
					statusCode = extraFieldObject.updateUdfValues();
					if (statusCode == ErrorCode.ERROR_UDF_SAVE_SUCCESS) {
						statusCode = ErrorCode.ERROR_NAME_SAVE_SUCCESS;
					} else {
						break;
					}
				}
			}
		}
		return statusCode;
	}
	// boolean exnese will be true if it of an expense category
	this.saveNewName = function (obj, expense, income) {
		var statusCode = void 0;
		if (!obj['opening_balance']) {
			this.opening_balance = '0.0';
		} else {
			this.opening_balance = obj['opening_balance'];
		}

		this.setOpeningBalance(obj['opening_balance'] ? obj['opening_balance'] : '0.0');

		this.setFullName(obj['name']);
		this.setPhoneNumber(obj['phone_number']);
		this.setEmail(obj['email']);
		this.setAmount(0.0);
		this.setAddress(obj['addressStr']);
		this.setNameType(obj['nameType']);
		this.setGroupId(obj['groupId']);
		this.setTinNumber(obj['tinNumber']);
		this.setGstinNumber(obj['gstinNumber']);
		this.setContactState(obj['contactState']);
		this.setShippingAddress(obj['shippingAddress']);
		this.setCustomerType(obj['customerType']);
		this.setUdfObjectArray(obj['extraFieldValueArray']);
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var transactionmanager = new TransactionManager();

		var isTransactionBeginSuccess = transactionmanager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_NAME_SAVE_FAILED;
			return statusCode;
		}

		try {
			statusCode = this.addName(expense, income);
			if (!expense) {
				this.setIsReceivable(obj['isReceivable']);
				txnType = this.isReceivable ? TxnTypeConstant['TXN_TYPE_ROPENBALANCE'] : TxnTypeConstant['TXN_TYPE_POPENBALANCE'];
				if (statusCode == ErrorCode.ERROR_NAME_SAVE_SUCCESS && this.opening_balance != '0.0') {
					var newTxn = transactionfactory.getTransactionObject(txnType);
					newTxn.setNameRef(this);
					txnValue = newTxn.setBalanceAmount(this.getOpeningBalance());

					newTxn.setTxnCurrentBalanceAmount(this.getOpeningBalance());
					newTxn.setTxnPaymentStatus(TxnPaymentStatusConstants.UNPAID);

					var selectDateObj = obj['opening_balance_date'];

					newTxn.setTxnDate(selectDateObj);
					newTxn.setTxnDueDate(new Date());
					newTxn.setDescription(null);
					newTxn.setReverseCharge(null);
					namecache.refreshNameCache(this);
					statusCode = newTxn.addTransaction();
					if (statusCode == ErrorCode.ERROR_TXN_SAVE_SUCCESS) {
						statusCode = ErrorCode.ERROR_NAME_SAVE_SUCCESS;
					}
				}
			}

			if (statusCode == ErrorCode.ERROR_NAME_SAVE_SUCCESS) {
				statusCode = addUdfValue(this.getUdfObjectArray(), this.getNameId());
			}

			if (statusCode == ErrorCode.ERROR_NAME_SAVE_SUCCESS) {
				if (!transactionmanager.CommitTransaction()) {
					statusCode = ErrorCode.ERROR_NAME_SAVE_FAILED;
				} else {
					namecache.refreshNameCache(this);
				}
			}
			if (statusCode != ErrorCode.ERROR_NAME_SAVE_SUCCESS) {
				namecache.reloadNameCache();
			}
		} catch (err) {
			logger.error('New item adding exception ' + err);
			statusCode = ErrorCode.ERROR_TXN_SAVE_FAILED;
		}
		transactionmanager.EndTransaction();
		return statusCode;
	};

	this.GetNameObjByNameAndTxn = function (txnType, fullName) {
		switch (txnType) {
			case TxnTypeConstant['TXN_TYPE_EXPENSE']:
				return this.GetExpenseByName(fullName);
			case TxnTypeConstant['TXN_TYPE_OTHER_INCOME']:
				return this.GetIncomeByName(fullName);
			default:
				return this.GetNamebyName(fullName);
		}
	};

	this.GetNamebyName = function (fullName) {
		if (!namecache) {
			var NameCache = require('./../Cache/NameCache.js');
			namecache = new NameCache();
		}
		return namecache.findNameModelByName(fullName);
	};

	this.GetExpenseByName = function (fullName) {
		if (!namecache) {
			var NameCache = require('./../Cache/NameCache.js');
			namecache = new NameCache();
		}
		return namecache.findExpenseModelByName(fullName);
	};

	this.GetIncomeByName = function (fullName) {
		if (!namecache) {
			var NameCache = require('./../Cache/NameCache.js');
			namecache = new NameCache();
		}
		return namecache.findIncomeModelByName(fullName);
	};

	this.getTransactionObjectOnNameId = function (nameId) {
		var DataLoader = require('./../DBManager/DataLoader.js');
		dataloader = new DataLoader();
		var txnModel = dataloader.getOpeningBalanceTransactionOnNameId(nameId);
		return txnModel;
	};

	this.getOpeningBalanceTransaction = function () {
		var BaseOpenBalanceTransaction = require('./BaseOpenBalanceTransaction.js');
		var baseopenbalancetransaction = new BaseOpenBalanceTransaction();
		var openBalanceTransaction = baseopenbalancetransaction.getOpeningBalanceTransactionOnName(this);
		return openBalanceTransaction;
	};

	this.updateExpenseName = function (expName) {
		var NameCache = require('./../Cache/NameCache.js');
		var nameCache = new NameCache();
		var id = this.nameId;

		if (nameCache.nameExistsWithDifferentNameId(expName, id, NameTypeConstant.NAME_TYPE_EXPENSE)) {
			return ErrorCode.ERROR_EXPENSE_NAME_EXISTS;
		}

		var NameModel = require('./../Models/nameModal.js');
		var nameModel = new NameModel();
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var transactionManager = new TransactionManager();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_NAME_SAVE_FAILED;
			return statusCode;
		}

		var statusCode = nameModel.updateExpense(expName, id);

		if (statusCode != ErrorCode.ERROR_NAME_SAVE_FAILED) {
			this.setFullName(expName);

			if (!transactionManager.CommitTransaction()) {
				statusCode = ErrorCode.ERROR_NAME_SAVE_FAILED;
			} else {
				nameCache.refreshNameCache(this);
			}
		}
		transactionManager.EndTransaction();
		return statusCode;
	};

	this.updateIncomeName = function (name) {
		var NameCache = require('./../Cache/NameCache.js');
		var nameCache = new NameCache();
		var id = this.nameId;

		if (nameCache.nameExistsWithDifferentNameId(name, id, NameTypeConstant.NAME_TYPE_INCOME)) {
			return ErrorCode.ERROR_INCOME_NAME_EXISTS;
		}

		var NameModel = require('./../Models/nameModal.js');
		var nameModel = new NameModel();
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var transactionManager = new TransactionManager();
		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_NAME_SAVE_FAILED;
			return statusCode;
		}

		var statusCode = nameModel.updateIncome(name, id);

		if (statusCode != ErrorCode.ERROR_NAME_SAVE_FAILED) {
			this.setFullName(name);

			if (!transactionManager.CommitTransaction()) {
				statusCode = ErrorCode.ERROR_NAME_SAVE_FAILED;
			} else {
				nameCache.refreshNameCache(this);
			}
		}
		transactionManager.EndTransaction();
		return statusCode;
	};

	this.updateContactName = function (oldTxnObj, obj) {
		var statusCode = ErrorCode.ERROR_NAME_SAVE_SUCCESS;
		if (!obj['opening_balance']) {
			var newOpeningBalance = '0.0';
		} else {
			var newOpeningBalance = obj['opening_balance'];
		}
		var NameCache = require('./../Cache/NameCache.js');
		var namecache = new NameCache();
		if (obj['name'].trim().length == 0) {
			statusCode = ErrorCode.ERROR_NAME_EMPTY;
		} else if (namecache.nameExistsWithDifferentNameId(obj.name, obj.nameId, obj.nameType)) {
			statusCode = ErrorCode.ERROR_NAME_ALREADY_EXISTS;
		} else {
			this.setFullName(obj['name'].trim());
		}
		if (statusCode == ErrorCode.ERROR_NAME_SAVE_SUCCESS) {
			this.setPhoneNumber(obj['phone_number'].trim());
			this.setEmail(obj['email'].trim());
			this.setTinNumber(obj['tinNumber']);
			this.setGstinNumber(obj['gstinNumber']);
			this.setContactState(obj['contactState']);
			this.setGroupId(obj['groupId']);
			this.setAddress(obj['addressStr'].trim());
			this.setNameType(obj['nameType']);
			this.setGroupId(obj['groupId']);
			this.setShippingAddress(obj['shippingAddress']);
			this.setCustomerType(obj['customerType']);
			this.setOpeningBalance(newOpeningBalance);
			// this.setUdfObjectArray(obj['extraFieldValueArray']);

			statusCode = this.validateData();

			var newTxnObject;
			var newTxnType = obj['isReceivable'];

			var TransactionManager = require('./../DBManager/TransactionManager.js');
			var transactionmanager = new TransactionManager();

			var isTransactionBeginSuccess = transactionmanager.BeginTransaction();

			if (!isTransactionBeginSuccess) {
				statusCode = ErrorCode.ERROR_NAME_UPDATE_FAILED;
				return statusCode;
			}

			try {
				debugger;
				if (statusCode == ErrorCode.SUCCESS) {
					console.log(oldTxnObj);
					if (!oldTxnObj && newOpeningBalance != '0.0') {
						newTxnObject = transactionfactory.getTransactionObject(newTxnType);
						newTxnObject.setNameRef(this);
						statusCode = newTxnObject.setBalanceAmount(newOpeningBalance);
						newTxnObject.setTxnCurrentBalanceAmount(newOpeningBalance);
						newTxnObject.setTxnPaymentStatus(TxnPaymentStatusConstants.UNPAID);
						if (statusCode == 'SUCCESS') {
							statusCode = ErrorCode.SUCCESS;
						}
						if (statusCode == ErrorCode.SUCCESS) {
							var selectDateObj = obj['opening_balance_date'];
							newTxnObject.setTxnDate(selectDateObj);
							newTxnObject.setTxnDueDate(new Date());
							console.log(newTxnObject);
							statusCode = newTxnObject.addTransaction();
							if (statusCode == ErrorCode.ERROR_TXN_SAVE_SUCCESS) {
								statusCode = ErrorCode.ERROR_NAME_SAVE_SUCCESS;
							}
						}
					} else if (oldTxnObj && (oldTxnObj.getTxnType() != newTxnType || oldTxnObj.getBalanceAmount() != Number(newOpeningBalance) || oldTxnObj.getTxnDate() != obj['opening_balance_date'])) {
						var newTxnObject = transactionfactory.getTransactionObject(newTxnType);
						var oldTxnLinkAmount = Number(oldTxnObj.getBalanceAmount()) - Number(oldTxnObj.getTxnCurrentBalanceAmount());
						var newCurrentBalance = Number(newOpeningBalance) - Number(oldTxnLinkAmount);
						var txnPaymentStatus = TxnPaymentStatusConstants.UNPAID;
						if (newCurrentBalance == 0) {
							txnPaymentStatus = TxnPaymentStatusConstants.PAID;
						} else if (newCurrentBalance < newOpeningBalance) {
							txnPaymentStatus = TxnPaymentStatusConstants.PARTIAL;
						}
						newTxnObject.setTxnId(oldTxnObj.getTxnId());
						newTxnObject.setNameRef(this);
						var selectDateObj = obj['opening_balance_date'];
						newTxnObject.setTxnDate(selectDateObj);
						newTxnObject.setTxnDueDate(oldTxnObj.txnDueDate);
						newTxnObject.setBalanceAmount(newOpeningBalance);
						newTxnObject.setTxnCurrentBalanceAmount(newCurrentBalance);
						newTxnObject.setTxnPaymentStatus(txnPaymentStatus);
						statusCode = newTxnObject.updateTransaction(oldTxnObj);
						if (statusCode == ErrorCode.ERROR_TXN_SAVE_SUCCESS) {
							statusCode = ErrorCode.ERROR_NAME_SAVE_SUCCESS;
						}
					} else {
						statusCode = this.updateNameToDB();
					}

					if (statusCode == ErrorCode.SUCCESS || statusCode == ErrorCode.ERROR_NAME_SAVE_SUCCESS) {
						if (this.getUdfObjectArray() && this.getUdfObjectArray().length) {
							var udfValueModel = new UDFValueModel();
							var UDFFieldsConstant = require('./../Constants/UDFFieldsConstant.js');
							var fieldType = UDFFieldsConstant.FIELD_TYPE_PARTY;
							statusCode = udfValueModel.deleteExtraFieldsValue(this.getNameId(), fieldType);
							if (statusCode == ErrorCode.ERROR_UDF_VALUE_DELETE_SUCCESSFUL) {
								statusCode = ErrorCode.ERROR_NAME_SAVE_SUCCESS;
							}
						}
						if (statusCode == ErrorCode.SUCCESS || statusCode == ErrorCode.ERROR_NAME_SAVE_SUCCESS) {
							statusCode = addUdfValue(obj['extraFieldValueArray'], this.getNameId());
						}
					}
					this.setUdfObjectArray(obj['extraFieldValueArray']);

					if (statusCode == ErrorCode.ERROR_NAME_SAVE_SUCCESS) {
						namecache.refreshNameCache(this);
					}
				}
				if (statusCode == ErrorCode.SUCCESS || statusCode == ErrorCode.ERROR_NAME_SAVE_SUCCESS) {
					var commitstat = transactionmanager.CommitTransaction();
					if (commitstat) {
						statusCode = ErrorCode.ERROR_NAME_UPDATE_SUCCESS;
					} else {
						statusCode = ErrorCode.ERROR_NAME_UPDATE_FAILED;
					}
				}
			} catch (err) {
				logger.error('Update name exception : ' + err);
				statusCode = ErrorCode.ERROR_NAME_UPDATE_FAILED;
			}
			transactionmanager.EndTransaction();
		}
		return statusCode;
	};

	this.deleteName = function () {
		var statusCode = ErrorCode.ERROR_NAME_DELETE_FAILED;
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var transactionManager = new TransactionManager();
		var NameModel = require('./../Models/nameModal.js');
		var BaseTransaction = require('./../BizLogic/BaseTransaction.js');
		var baseTransaction = new BaseTransaction();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_NAME_DELETE_FAILED;
			return statusCode;
		}

		statusCode = baseTransaction.deleteTransactionsForName(this);
		if (statusCode == ErrorCode.ERROR_TXN_DELETE_SUCCESS) {
			var nameModel = new NameModel();
			nameModel.set_name_id(this.getNameId());
			statusCode = nameModel.deleteName();
		}
		if (statusCode == ErrorCode.ERROR_NAME_DELETE_SUCCESS) {
			if (this.getUdfObjectArray() && this.getUdfObjectArray().length) {
				var udfValueModel = new UDFValueModel();
				var UDFFieldsConstant = require('./../Constants/UDFFieldsConstant.js');
				var fieldType = UDFFieldsConstant.FIELD_TYPE_PARTY;
				statusCode = udfValueModel.deleteExtraFieldsValue(this.getNameId(), fieldType);
				if (statusCode == ErrorCode.ERROR_UDF_VALUE_DELETE_SUCCESSFUL) {
					statusCode = ErrorCode.ERROR_NAME_DELETE_SUCCESS;
				}
			}
		}
		if (statusCode == ErrorCode.ERROR_NAME_DELETE_SUCCESS) {
			if (!transactionManager.CommitTransaction()) {
				statusCode = ErrorCode.ERROR_NAME_DELETE_FAILED;
			} else {
				var NameCache = require('./../Cache/NameCache.js');
				var nameCache = new NameCache();
				nameCache.reloadNameCache();
			}
		}
		transactionManager.EndTransaction();
		return statusCode;
	};

	this.canDeleteParty = function () {
		var NameModel = require('./../Models/nameModal.js');
		var nameModel = new NameModel();
		nameModel.set_name_id(this.getNameId());
		return nameModel.canDeleteParty();
	};

	this.setUdfObjectArray = function (ExtraFieldObjectArray) {
		if (ExtraFieldObjectArray && ExtraFieldObjectArray.length) {
			if (!Array.isArray(ExtraFieldObjectArray)) {
				throw 'udf fields are not in an array';
			}
			extraFieldObjectArray = ExtraFieldObjectArray;
		}
	};

	this.getUdfObjectArray = function () {
		return extraFieldObjectArray;
	};
};

module.exports = nameLogic;