var IncomeCategoryObject = function IncomeCategoryObject() {
	this.nameId = 0;
	this.incomeCategoryName = '';
	this.incomeCategoryAmount = 0.0;

	this.getNameId = function () {
		return this.nameId;
	};
	this.setNameId = function (nameId) {
		this.nameId = nameId;
	};

	this.getIncomeCategoryName = function () {
		return this.incomeCategoryName;
	};
	this.setIncomeCategoryName = function (incomeCategoryName) {
		this.incomeCategoryName = incomeCategoryName;
	};

	this.getIncomeCategoryAmount = function () {
		return this.incomeCategoryAmount;
	};
	this.setIncomeCategoryAmount = function (incomeCategoryAmount) {
		this.incomeCategoryAmount = incomeCategoryAmount;
	};
};

module.exports = IncomeCategoryObject;