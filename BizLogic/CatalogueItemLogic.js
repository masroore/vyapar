var CatalogueModel = require('../Models/CatalogueModel');
var ItemImageModel = require('../Models/ItemImagesModel');
var ErrorCode = require('../Constants/ErrorCode');
var DataLoader = require('./../DBManager/DataLoader.js');

var CatalogueLogic = function CatalogueLogic() {
	this._catalogueItemId;
	this._itemId;
	this._catalogueItemName;
	this._catalogueItemCode;
	this._catalogueItemDescription;
	this._catalogueItemSaleUnitPrice;
	this._catalogueItemCategoryName;
	this._catalogueItemImagesList;
};

CatalogueLogic.prototype.getCatalogueItemId = function () {
	return this._catalogueItemId;
};
CatalogueLogic.prototype.setCatalogueItemId = function (catalogueItemId) {
	this._catalogueItemId = catalogueItemId;
};

CatalogueLogic.prototype.getItemId = function () {
	return this._itemId;
};
CatalogueLogic.prototype.setItemId = function (itemId) {
	this._itemId = itemId;
};

CatalogueLogic.prototype.getCatalogueItemName = function () {
	return this._catalogueItemName;
};
CatalogueLogic.prototype.setCatalogueItemName = function (catalogueItemName) {
	this._catalogueItemName = catalogueItemName;
};

CatalogueLogic.prototype.getCatalogueItemDescription = function () {
	return this._catalogueItemDescription;
};

CatalogueLogic.prototype.setCatalogueItemDescription = function (catalogueItemDescription) {
	this._catalogueItemDescription = catalogueItemDescription;
};

CatalogueLogic.prototype.getCatalogueItemCode = function () {
	return this._catalogueItemCode;
};

CatalogueLogic.prototype.setCatalogueItemCode = function (catalogueItemCode) {
	this._catalogueItemCode = catalogueItemCode;
};

CatalogueLogic.prototype.getCatalogueItemSaleUnitPrice = function () {
	return this._catalogueItemSaleUnitPrice;
};

CatalogueLogic.prototype.setCatalogueItemSaleUnitPrice = function (catalogueItemSaleUnitPrice) {
	this._catalogueItemSaleUnitPrice = catalogueItemSaleUnitPrice;
};

CatalogueLogic.prototype.getCatalogueItemCategoryName = function () {
	return this._catalogueItemCategoryName;
};

CatalogueLogic.prototype.setCatalogueItemCategoryName = function (catalogueItemCategoryName) {
	this._catalogueItemCategoryName = catalogueItemCategoryName;
};

CatalogueLogic.prototype.getCatalogueItemImagesList = function () {
	return this._catalogueItemImagesList;
};

CatalogueLogic.prototype.setCatalogueItemImagesList = function (catalogueItemImagesList) {
	this._catalogueItemImagesList = catalogueItemImagesList;
};

CatalogueLogic.prototype.saveCatalogueItems = function (items) {
	var _this = this;

	var TransactionManager = require('./../DBManager/TransactionManager.js');
	var transactionmanager = new TransactionManager();
	var isTransactionBeginSuccess = transactionmanager.BeginTransaction();
	var statusCode = ErrorCode.ERROR_CATALOGUE_ITEM_SAVE_FAILED;

	if (isTransactionBeginSuccess) {
		try {
			items.forEach(function (item) {
				var catalogueModel = new CatalogueModel();

				// catalogueItemLogic.setCatalogueItemCode(item.itemCode);
				// catalogueItemLogic.setCatalogueItemDescription(item.itemDescription);
				// catalogueItemLogic.setItemId(item.itemId);
				// catalogueItemLogic.setCatalogueItemName(item.itemName);
				// catalogueItemLogic.setCatalogueItemSaleUnitPrice(item.itemSaleUnitPrice)
				// catalogueItemLogic.setCatalogueItemCategoryName(item.itemCategoryName);
				catalogueModel.setCatalogueItemId(item.catalogueItemId);
				catalogueModel.setItemId(item.itemId);
				catalogueModel.setCatalogueItemName(item.itemName);
				catalogueModel.setCatalogueItemCode(item.itemCode);
				catalogueModel.setCatalogueItemDescription(item.itemDescription);
				catalogueModel.setCatalogueItemSaleUnitPrice(item.itemSaleUnitPrice);
				catalogueModel.setCatalogueItemCategoryName(item.itemCategoryName);

				if (item.catalogueItemId) {
					statusCode = catalogueModel.updateCatalogueItem();
					statusCode = statusCode === ErrorCode.ERROR_CATALOGUE_ITEM_UPDATE_SUCCESS ? ErrorCode.SUCCESS : ErrorCode.FAILED;
				} else {
					var catalogueItemId = catalogueModel.addCatalogueItem();
					if (catalogueItemId > 0) {
						_this.setCatalogueItemId(catalogueItemId);
						statusCode = ErrorCode.ERROR_CATALOGUE_ITEM_SAVE_SUCCESS;
					}

					if (statusCode === ErrorCode.ERROR_CATALOGUE_ITEM_SAVE_SUCCESS) {
						//Logic to add catalogueItemId in item image table
						var catalogueItemImage = new ItemImageModel();
						catalogueItemImage.setItemId(item.itemId);
						catalogueItemImage.setCatalogueItemId(_this.getCatalogueItemId());
						var itemImageUpdateStatus = catalogueItemImage.updateCatalogueItemIdOfItemImages();
						statusCode = itemImageUpdateStatus === ErrorCode.ERROR_ITEM_IMAGE_UPDATE_SUCCESS ? ErrorCode.SUCCESS : ErrorCode.FAILED;
					}
				}
			});
			statusCode = statusCode === ErrorCode.SUCCESS && transactionmanager.CommitTransaction() ? ErrorCode.SUCCESS : ErrorCode.FAILED;
			transactionmanager.EndTransaction();
		} catch (ex) {
			if (logger) {
				logger.error('Catalogue Item adding exception ' + ex);
			}
			return ErrorCode.ERROR_CATALOGUE_ITEM_SAVE_FAILED;
		}
	}
	return statusCode;
};

CatalogueLogic.prototype.deleteCatalogueItems = function (itemIds) {
	var TransactionManager = require('./../DBManager/TransactionManager.js');
	var transactionmanager = new TransactionManager();
	var isTransactionBeginSuccess = transactionmanager.BeginTransaction();
	var statusCode = ErrorCode.FAILED;

	if (isTransactionBeginSuccess) {
		try {
			itemIds.forEach(function (itemId) {
				var catalogueModel = new CatalogueModel();
				catalogueModel.setItemId(itemId);

				var catalogueItemImage = new ItemImageModel();
				catalogueItemImage.setItemId(itemId);
				var itemImageUpdateStatus = catalogueItemImage.deleteAllCatalogueItemImagesByCatalogueId();
				statusCode = itemImageUpdateStatus === ErrorCode.ERROR_ITEM_IMAGE_DELETE_SUCCESS ? ErrorCode.SUCCESS : ErrorCode.FAILED;

				if (statusCode === ErrorCode.SUCCESS) {
					statusCode = catalogueModel.deleteCatalogueItem();
				}
			});
			statusCode = statusCode === ErrorCode.SUCCESS && transactionmanager.CommitTransaction() ? ErrorCode.SUCCESS : ErrorCode.FAILED;
			transactionmanager.EndTransaction();
		} catch (ex) {
			if (logger) {
				logger.error('Catalogue Item deletion exception ' + ex);
			}
			return ErrorCode.FAILED;
		}
	}
	return statusCode;
};

CatalogueLogic.prototype.getItemIdListOfCatalogue = function () {
	var dataloader = new DataLoader();
	return dataloader.getItemIdListOfCatalogue();
};

CatalogueLogic.prototype.getCatagoryListOfCatalogue = function () {
	var dataloader = new DataLoader();
	return dataloader.getCatagoryListOfCatalogue();
};

CatalogueLogic.prototype.getCatalogueItemsList = function (_ref) {
	var lastCatalogueId = _ref.lastCatalogueId,
	    itemIds = _ref.itemIds,
	    limit = _ref.limit,
	    catalogueItemCategoryName = _ref.catalogueItemCategoryName,
	    catalogueItemNameText = _ref.catalogueItemNameText;
	var isImageRequired = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

	var dataloader = new DataLoader();
	return dataloader.getCatalogueItemsList({ lastCatalogueId: lastCatalogueId, itemIds: itemIds, limit: limit, catalogueItemCategoryName: catalogueItemCategoryName, catalogueItemNameText: catalogueItemNameText }, isImageRequired);
};

CatalogueLogic.prototype.isCatalogueItemExists = function (text, type) {
	var dataloader = new DataLoader();
	return dataloader.isCatalogueItemExists(text, type) > 0;
};

// CatalogueLogic.prototype.getCatalogueItemIdList = function (lastCatalogueId, limit) {
// 	const dataloader = new DataLoader();
// 	return dataloader.getCatalogueItemsList(lastCatalogueId, limit);
// };

// CatalogueLogic.prototype.getImageListByItemId = function (itemId, appendDataImageTag = true) {
// 	let CatalogueModel = new Cata();
// 	let itemImageList = Cata.getImagesByItemId(itemId, appendDataImageTag);
// 	let CatalogueLogic = [];
// 	if (itemImageList && itemImageList.length > 0) {
// 		for (let i = 0; i < itemImageList.length; i++) {
//             let itemImage = itemImageList[i];
//             CatalogueLogic.push(itemImage.getBase64Image());
//         }
// 	}
// 	return CatalogueLogic;
// };

// CatalogueLogic.prototype.deleteItemImage = function () {
// 	let CatalogueModel = new Cata();
// 	Cata.setItemImageId(this.getItemImageId());
// 	let result = Cata.deleteItemImage();
// 	return result;
// };

module.exports = CatalogueLogic;