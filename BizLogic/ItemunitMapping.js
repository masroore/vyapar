var ItemUnitMapping = function ItemUnitMapping() {
	var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
	var ErrorCode = require('./../Constants/ErrorCode.js');
	var TransactionManager = require('./../DBManager/TransactionManager.js');
	var ItemUnitMappingModel = require('./../Models/ItemUnitMappingModel.js');
	var ItemCache = require('./../Cache/ItemCache.js');
	var ItemUnitMappingCache = require('./../Cache/ItemUnitMappingCache.js');
	this.mappingId;
	this.baseUnitId;
	this.secondaryUnitId;
	this.conversionRate;

	this.getMappingId = function () {
		return this.mappingId;
	};

	this.setMappingId = function (mappingId) {
		this.mappingId = mappingId;
	};

	this.getBaseUnitId = function () {
		return this.baseUnitId;
	};

	this.setBaseUnitId = function (baseUnitId) {
		this.baseUnitId = baseUnitId;
	};

	this.getSecondaryUnitId = function () {
		return this.secondaryUnitId;
	};

	this.setSecondaryUnitId = function (secondaryUnitId) {
		this.secondaryUnitId = secondaryUnitId;
	};

	this.getConversionRate = function () {
		return this.conversionRate;
	};

	this.setConversionRate = function (conversionRate) {
		this.conversionRate = conversionRate;
	};

	this.addNewUnitMapping = function (baseUnitId, secondaryUnitId, conversionRate) {
		var errorCode = ErrorCode.SUCCESS;
		var transactionManager = new TransactionManager();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			errorCode = ErrorCode.ERROR_UNIT_MAPPING_SAVE_FAILED;
			return errorCode;
		}

		if (errorCode == ErrorCode.SUCCESS) {
			var itemUnitMappingModel = new ItemUnitMappingModel();
			itemUnitMappingModel.setBaseUnitId(baseUnitId);
			itemUnitMappingModel.setSecondaryUnitId(secondaryUnitId);
			itemUnitMappingModel.setConversionRate(conversionRate);
			errorCode = itemUnitMappingModel.addItemUnitMapping();
		}

		if (errorCode == ErrorCode.ERROR_UNIT_MAPPING_SAVE_SUCCESS) {
			if (!transactionManager.CommitTransaction()) {
				errorCode = ErrorCode.ERROR_UNIT_MAPPING_SAVE_FAILED;
			} else {
				var itemCache = new ItemCache();
				var itemUnitMappingCache = new ItemUnitMappingCache();
				itemUnitMappingCache.refreshItemUnitMappingCache();
				itemCache.reloadItemCache();
			}
		}

		transactionManager.EndTransaction();
		return errorCode;
	};

	this.updateUnitMapping = function (mappingId, baseUnitId, secondaryUnitId, conversionRate) {
		var errorCode = ErrorCode.SUCCESS;
		var transactionManager = new TransactionManager();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			errorCode = ErrorCode.ERROR_UNIT_MAPPING_SAVE_FAILED;
			return errorCode;
		}

		if (errorCode == ErrorCode.SUCCESS) {
			var itemUnitMappingModel = new ItemUnitMappingModel();
			itemUnitMappingModel.setMappingId(mappingId);
			itemUnitMappingModel.setBaseUnitId(baseUnitId);
			itemUnitMappingModel.setSecondaryUnitId(secondaryUnitId);
			itemUnitMappingModel.setConversionRate(conversionRate);

			errorCode = itemUnitMappingModel.updateItemUnitMapping();
		}

		if (errorCode == ErrorCode.ERROR_UNIT_MAPPING_UPDATE_SUCCESS) {
			if (!transactionManager.CommitTransaction()) {
				errorCode = ErrorCode.ERROR_UNIT_MAPPING_SAVE_FAILED;
			} else {
				var itemUnitMappingCache = new ItemUnitMappingCache();
				itemUnitMappingCache.refreshItemUnitMappingCache();
			}
		}

		transactionManager.EndTransaction();
		return errorCode;
	};

	this.deleteUnitMapping = function (mappingId) {
		var transactionManager = new TransactionManager();
		var itemUnitMappingCache = new ItemUnitMappingCache();
		var statusCode = ErrorCode.ERROR_UNIT_MAPPING_DELETE_FAILED;
		try {
			var isTransactionBeginSuccess = transactionManager.BeginTransaction();

			if (!isTransactionBeginSuccess) {
				statusCode = ErrorCode.ERROR_UNIT_MAPPING_DELETE_FAILED;
				return statusCode;
			}

			var itemUnitMappingModel = new ItemUnitMappingModel();
			itemUnitMappingModel.setMappingId(mappingId);

			statusCode = itemUnitMappingModel.deleteUnitMapping();

			if (statusCode == ErrorCode.ERROR_UNIT_MAPPING_DELETE_SUCCESS) {
				if (!transactionManager.CommitTransaction()) {
					statusCode = ErrorCode.ERROR_UNIT_MAPPING_DELETE_FAILED;
				} else {
					itemUnitMappingCache.refreshItemUnitMappingCache();
				}
			}
		} catch (err) {
			console.log(err);
		}

		transactionManager.EndTransaction();
		return statusCode;
	};

	this.isMappingUsed = function (mappingId) {
		// var sqliteDBHelper = new SqliteDBHelper();
		var itemUnitMappingModel = new ItemmUnitMappingModel();
		return itemUnitMappingModel.isItemUnitMappingUsed(mappingId);
	};
};

module.exports = ItemUnitMapping;