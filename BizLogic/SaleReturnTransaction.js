var SaleReturnTransaction = function SaleReturnTransaction() {
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var cashAmount, balanceAmount, txnType, txnRefNumber;
	this.discountPercent = 0.0;
	this.taxPercent = 0.0;

	this.getCashAmount = function () {
		return this.cashAmount;
	};
	// this.setCashAmount = function(cashAmount) {
	//     this.cashAmount = cashAmount;
	// }

	this.getBalanceAmount = function () {
		return this.balanceAmount;
	};
	this.setBalanceAmount = function (balanceAmount) {
		console.log(balanceAmount);
		if (!balanceAmount) {
			this.balanceAmount = '0.0';
		} else if (!isNaN(Number(balanceAmount))) {
			this.balanceAmount = Number(balanceAmount);
		} else {
			return 'ERROR_TXN_INVALID_AMOUNT';
		}
		return 'SUCCESS';
	};
	this.setCashAmount = function (cashAmount) {
		if (!cashAmount) {
			this.cashAmount = '0.0';
		} else if (!isNaN(Number(cashAmount))) {
			this.cashAmount = Number(cashAmount);
		} else {
			return 'ERROR_TXN_INVALID_AMOUNT';
		}
		return 'SUCCESS';
	};

	this.setAmounts = function (totalAmount, cashAmount) {
		var statusCode = 'SUCCESS';
		console.log('Cash Amount:' + cashAmount);
		console.log(totalAmount);
		statusCode = this.validateTotalAmount(totalAmount);
		if (statusCode == 'SUCCESS') {
			totalAmount = Number(totalAmount);
			cashAmount = Number(cashAmount);
			if (!cashAmount) {
				cashAmount = 0.0;
			}
			if (totalAmount < cashAmount) {
				statusCode = ErrorCode.ERROR_TXN_TOTAL_LESS_THAN_CASH;
			} else {
				this.setCashAmount(cashAmount);
				this.setBalanceAmount(totalAmount - cashAmount);
			}
		}
		return statusCode;
	};

	this.getTxnType = function () {
		this.txnType = TxnTypeConstant['TXN_TYPE_SALE_RETURN'];
		return this.txnType;
	};

	this.getTxnTypeString = function () {
		this.txnTypeString = 'TXN_TYPE_SALE_RETURN';
		return this.txnTypeString;
	};

	this.setTxnRefNumber = function (txnRefNumber) {
		this.txnRefNumber = txnRefNumber;
	};
	this.getTxnRefNumber = function () {
		return this.txnRefNumber;
	};
};

module.exports = SaleReturnTransaction;