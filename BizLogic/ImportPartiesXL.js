var _typeof2 = require('babel-runtime/helpers/typeof');

var _typeof3 = _interopRequireDefault(_typeof2);

var _set = require('babel-runtime/core-js/set');

var _set2 = _interopRequireDefault(_set);

var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _create = require('babel-runtime/core-js/object/create');

var _create2 = _interopRequireDefault(_create);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ImportPartiesXL = function ImportPartiesXL() {
	var _Object$create2;

	var SettingCache = require('./../Cache/SettingCache');
	var logger = require('../Utilities/logger');
	var StringConstants = require('../Constants/StringConstants');

	var partyFieldHeaders = {
		PARTY_NAME: 'Name*',
		CONTACT_NO: 'Contact No.',
		EMAIL_ID: 'Email ID',
		ADDRESS: 'Address',
		OPENING_BALANCE: 'Opening Balance',
		OPENING_DATE: 'Opening Date (dd/MM/yyyy)',
		GSTIN_NO: 'GSTIN No.',
		PARTY_GROUP: 'Group Name',
		SHIPPING_ADDRESS: 'Shipping Address'
	};

	var partyDefinations = (0, _create2.default)((_Object$create2 = {}, (0, _defineProperty3.default)(_Object$create2, partyFieldHeaders.PARTY_NAME, {
		validExcelHeaders: [partyFieldHeaders.PARTY_NAME.toLowerCase()],
		mappableExcelHeaders: [partyFieldHeaders.PARTY_NAME.toLowerCase()],
		validateAndSetValue: function validateAndSetValue(val) {
			return val ? val.trim() : '';
		}
	}), (0, _defineProperty3.default)(_Object$create2, partyFieldHeaders.CONTACT_NO, {
		validExcelHeaders: [partyFieldHeaders.CONTACT_NO.toLowerCase()],
		mappableExcelHeaders: [partyFieldHeaders.CONTACT_NO.toLowerCase()],
		validateAndSetValue: function validateAndSetValue(val) {
			val = val ? val.trim() : '';
			if (!StringConstants.phoneRegex.test(val)) {
				var err = new Error(StringConstants.IMPORT_PARTY_INVALID_CONTACT_NO);
				return err;
			}
			return val;
		}
	}), (0, _defineProperty3.default)(_Object$create2, partyFieldHeaders.EMAIL_ID, {
		validExcelHeaders: [partyFieldHeaders.EMAIL_ID.toLowerCase()],
		mappableExcelHeaders: [partyFieldHeaders.EMAIL_ID.toLowerCase()],
		validateAndSetValue: function validateAndSetValue(val) {
			val = val ? val.trim() : '';
			if (val && !StringConstants.emailRegex.test(val)) {
				var err = new Error(StringConstants.IMPORT_PARTY_INVALID_EMAIL);
				return err;
			}
			return val;
		}
	}), (0, _defineProperty3.default)(_Object$create2, partyFieldHeaders.ADDRESS, {
		validExcelHeaders: [partyFieldHeaders.ADDRESS.toLowerCase()],
		mappableExcelHeaders: [partyFieldHeaders.ADDRESS.toLowerCase()],
		validateAndSetValue: function validateAndSetValue(val) {
			val = val ? val.trim() : '';
			return val;
		}
	}), (0, _defineProperty3.default)(_Object$create2, partyFieldHeaders.OPENING_BALANCE, {
		validExcelHeaders: [partyFieldHeaders.OPENING_BALANCE.toLowerCase()],
		mappableExcelHeaders: [partyFieldHeaders.OPENING_BALANCE.toLowerCase(), 'current balance'],
		validateAndSetValue: function validateAndSetValue(val) {
			val = val ? val.trim() : '';
			val = val == '-' || val == '.' ? '' : val;
			if (isNaN(Number(val))) {
				var err = new Error(StringConstants.IMPORT_PARTY_INVALID_OPENING_BALANCE);
				return err;
			}
			return val;
		}
	}), (0, _defineProperty3.default)(_Object$create2, partyFieldHeaders.OPENING_DATE, {
		validExcelHeaders: [partyFieldHeaders.OPENING_DATE.toLowerCase()],
		mappableExcelHeaders: [partyFieldHeaders.OPENING_DATE.toLowerCase(), 'opening date'],
		validateAndSetValue: function validateAndSetValue(val) {
			val = val ? val.trim() : '';
			if (val && !StringConstants.dateRegex.test(val)) {
				var err = new Error(StringConstants.IMPORT_PARTY_INVALID_OPENING_DATE_FORMAT);
				return err;
			}
			return val;
		}
	}), (0, _defineProperty3.default)(_Object$create2, partyFieldHeaders.GSTIN_NO, {
		validExcelHeaders: [partyFieldHeaders.GSTIN_NO.toLowerCase()],
		mappableExcelHeaders: [partyFieldHeaders.GSTIN_NO.toLowerCase(), 'tin no.', 'trn no.', 'gstin no.'],
		validateAndSetValue: function validateAndSetValue(val) {
			val = val ? val.trim() : '';
			if (val) {
				var trnOrTinLength = 15;
				var settingCache = new SettingCache();
				var GSTHelper = require('../Utilities/GSTHelper');
				var isGST = settingCache.getGSTEnabled();
				var isCountryIndia = settingCache.isCurrentCountryIndia();
				if (isCountryIndia && isGST && !GSTHelper.isGSTINValid(val)) {
					var err = new Error(StringConstants.IMPORT_PARTY_INVALID_GST_FORMAT);
					return err;
				} else if (val.length > trnOrTinLength) {
					var _err = new Error(StringConstants.IMPORT_PARTY_INVALID_TIN_TRN_FORMAT);
					return _err;
				}
			}
			return val;
		}
	}), (0, _defineProperty3.default)(_Object$create2, partyFieldHeaders.PARTY_GROUP, {
		validExcelHeaders: [partyFieldHeaders.PARTY_GROUP.toLowerCase()],
		mappableExcelHeaders: [partyFieldHeaders.PARTY_GROUP.toLowerCase()],
		validateAndSetValue: function validateAndSetValue(val) {
			return val ? val.trim() : '';
		}
	}), (0, _defineProperty3.default)(_Object$create2, partyFieldHeaders.SHIPPING_ADDRESS, {
		validExcelHeaders: [partyFieldHeaders.SHIPPING_ADDRESS.toLowerCase()],
		mappableExcelHeaders: [partyFieldHeaders.SHIPPING_ADDRESS.toLowerCase()],
		validateAndSetValue: function validateAndSetValue(val) {
			return val ? val.trim() : '';
		}
	}), _Object$create2));

	this.PartyDefinations = partyDefinations;
	this.PartyFieldHeaders = partyFieldHeaders;

	this.validateHeaders = function (excelHeaders, enabledParties) {
		if (excelHeaders.length != enabledParties.length) {
			return false;
		}
		return excelHeaders.every(function (element, index) {
			var field = element || '';
			return partyDefinations[enabledParties[index]].validExcelHeaders.includes(field.trim().toLowerCase());
		});
	};

	this.getEnabledPartyFields = function () {
		// Calling this function only once in ImportParties constructor and using the same object all the time. So remove inconsistency in case any sync writeback changes settings
		var settingCache = new SettingCache();
		var isPartyGroupEnabled = settingCache.isPartyGroupEnabled();
		var isGSTEnabled = settingCache.getGSTEnabled();
		var isPartyShippingAddressEnabled = settingCache.getPartyShippingAddressEnabled();
		var headers = (0, _extends3.default)({}, partyFieldHeaders);
		if (!isPartyGroupEnabled) {
			delete headers.PARTY_GROUP;
		}
		if (!isGSTEnabled) {
			delete headers.GSTIN_NO;
		}
		if (!isPartyShippingAddressEnabled) {
			delete headers.SHIPPING_ADDRESS;
		}
		return (0, _values2.default)(headers);
	};

	this.parseExcelData = function (mappedData, enabledParties) {
		var validParties = [];
		var invalidParties = [];
		var partyNamesArr = getPartyNames();
		var validPartyNames = new _map2.default();
		partyNamesArr.forEach(function (partyName) {
			validPartyNames.set(partyName.trim().toLowerCase());
		});
		var invalidPartyNames = new _set2.default();
		var validatedData = mappedData.map(function (party) {
			return getValidatedPartyRow({ partyRow: party, partyFields: enabledParties });
		});
		var validatePartyNames = function validatePartyNames(partyRow) {
			var partyName = partyRow[partyFieldHeaders.PARTY_NAME];
			var value = partyName.value || '';
			value = value.trim().toLowerCase();
			if (validPartyNames.has(value)) {
				partyName.isValid = false;
				partyName.error = new Error(StringConstants.IMPORT_PARTY_NAME_DUPLICATE);
				partyRow.isRowValid = false;
			}
			// Update the valid and invalid sets
			if (partyRow.isRowValid) {
				value && validPartyNames.set(value);
			} else {
				value && invalidPartyNames.add(value);
			}
		};
		validatedData.forEach(validatePartyNames);
		validatedData.forEach(function (partyRow) {
			if (partyRow.isRowValid) {
				validParties.push(partyRow);
			} else {
				invalidParties.push(partyRow);
			}
		});
		invalidPartyNames = null;
		validatedData = null;
		mappedData = null;
		return {
			validParties: validParties,
			invalidParties: invalidParties,
			partyNamesMap: validPartyNames
		};
	};

	function getPartyNames() {
		var NameCache = require('../Cache/NameCache');
		var partyCache = new NameCache();
		var partyNames = partyCache.getListOfNames(); // .map(party => party.toLowerCase())
		return partyNames;
	}

	function getValidatedPartyRow(_ref) {
		var partyRow = _ref.partyRow,
		    _ref$partyFields = _ref.partyFields,
		    partyFields = _ref$partyFields === undefined ? [] : _ref$partyFields;

		partyRow.isRowValid = true; // resetting.
		partyFields.forEach(function (partyField) {
			switch (partyField) {
				case partyFieldHeaders.PARTY_NAME:
					var partyName = partyRow[partyField].value ? partyRow[partyField].value.trim() : '';
					if (!partyName) {
						partyRow[partyField].isValid = false;
						var error = new Error(StringConstants.IMPORT_PARTY_NAME_EMPTY);
						partyRow[partyField].error = error;
						partyRow[partyField].value = '';
					} else {
						partyRow[partyField].isValid = true;
						partyRow[partyField].error = '';
					}
					partyRow.isRowValid = partyRow.isRowValid && partyRow[partyField].isValid;
					break;
				default:
					var validatedRes = partyDefinations[partyField].validateAndSetValue(partyRow[partyField].value);
					partyRow[partyField].isValid = !(validatedRes instanceof Error);
					partyRow[partyField].error = validatedRes instanceof Error ? validatedRes : '';
					partyRow[partyField].value = validatedRes instanceof Error ? partyRow[partyField].value : validatedRes;
					partyRow.isRowValid = partyRow.isRowValid && partyRow[partyField].isValid;
					break;
			}
		});
		return partyRow;
	}

	this.saveImportedParties = function (_ref2) {
		var partyObjs = _ref2.partyObjs,
		    partyGroupMap = _ref2.partyGroupMap;

		var ErrorCode = require('../Constants/ErrorCode');
		var DataInserter = require('./../DBManager/DataInserter.js');
		var dataInserter = new DataInserter();
		var statusCode = ErrorCode.ERROR_PARTY_IMPORT_FAILED;
		try {
			statusCode = dataInserter.saveImportedParties(partyObjs, partyGroupMap);
		} catch (err) {
			logger.error(err);
		}

		var dialog = require('electron').remote.dialog;

		dialog.showMessageBox({
			type: 'info',
			title: 'Import Message',
			message: statusCode
		});
		MyAnalytics.pushEvent(statusCode == ErrorCode.ERROR_PARTY_IMPORT_SUCCESS ? 'Import Parties Successful' : 'Import Parties Failed');
	};

	this.downloadXLFile = function () {
		var _dataObj;

		var Excel = require('excel4node');
		var PartyGroupCache = require('./../Cache/PartyGroupCache');
		var settingCache = new SettingCache();
		var partyGroupCache = new PartyGroupCache();
		var dataObj = (_dataObj = {
			rowCount: 7
		}, (0, _defineProperty3.default)(_dataObj, partyFieldHeaders.PARTY_NAME, ['Party 1', 'Party 2', 'Party 3', 'Party 4', 'Party 5', 'Party 6', 'Party 7']), (0, _defineProperty3.default)(_dataObj, partyFieldHeaders.CONTACT_NO, ['1234567891', '1234567892', '1234567893', '1234567894', '1234567895', '1234567896', '1234567897']), (0, _defineProperty3.default)(_dataObj, partyFieldHeaders.EMAIL_ID, ['abc1@xyz.com', 'abc2@xyz.com', 'abc3@xyz.com', 'abc4@xyz.com', 'abc5@xyz.com', 'abc6@xyz.com', 'abc7@xyz.com']), (0, _defineProperty3.default)(_dataObj, partyFieldHeaders.ADDRESS, ['Address 1', 'Address 2', 'Address 3', 'Address 4', 'Address 5', 'Address 6', 'Address 7']), (0, _defineProperty3.default)(_dataObj, partyFieldHeaders.OPENING_BALANCE, [200, 300, 40, 200, 300, -300, 200]), (0, _defineProperty3.default)(_dataObj, partyFieldHeaders.OPENING_DATE, ['07/07/2019', '08/08/2019', '26/08/2019', '29/08/2019', '28/08/2019', '12/08/2019', '14/08/2019']), (0, _defineProperty3.default)(_dataObj, partyFieldHeaders.GSTIN_NO, ['10AAAGM0289C1ZL', '09AAAGM0289C1ZL', '14AAAGM0289C1ZQ', '08AAAGM0289C1ZL', '16AAAGM0289C1ZM', '14AAAGM0289C1ZQ', '16AAAGM0289C1ZM']), (0, _defineProperty3.default)(_dataObj, partyFieldHeaders.PARTY_GROUP, ['Group 1', 'Group 2', 'Group 1', 'Group 1', 'Group 2', 'Group 1', 'Group 1']), (0, _defineProperty3.default)(_dataObj, partyFieldHeaders.SHIPPING_ADDRESS, ['XYZ Street, Bangalore', 'XYZ Street, Bangalore', 'XYZ Street, Bangalore', 'XYZ Street, Bangalore', 'XYZ Street, Bangalore', 'XYZ Street, Bangalore', 'XYZ Street, Bangalore']), _dataObj);

		var dialog = require('electron').remote.dialog;

		var wb = new Excel.Workbook();
		var ws = wb.addWorksheet('Template', {
			sheetFormat: {
				'baseColWidth': 15,
				'defaultColWidth': 15,
				'defaultRowHeight': 20
			}
		});
		var columns = [partyFieldHeaders.PARTY_NAME, partyFieldHeaders.CONTACT_NO, partyFieldHeaders.EMAIL_ID, partyFieldHeaders.ADDRESS, partyFieldHeaders.OPENING_BALANCE, partyFieldHeaders.OPENING_DATE];

		if (settingCache.getGSTEnabled()) {
			columns.push(partyFieldHeaders.GSTIN_NO);
		}
		if (settingCache.isPartyGroupEnabled()) {
			columns.push(partyFieldHeaders.PARTY_GROUP);
		}
		if (settingCache.getPartyShippingAddressEnabled()) {
			columns.push(partyFieldHeaders.SHIPPING_ADDRESS);
		}

		var headerStyle = wb.createStyle({
			font: {
				color: 'white',
				bold: true
			},
			alignment: {
				wrapText: true,
				horizontal: 'center',
				vertical: 'center'
			},
			fill: {
				type: 'pattern',
				patternType: 'solid',
				bgColor: '#4D82B8',
				fgColor: '#4D82B8' // HTML style hex value. defaults to black
			},
			border: { // §18.8.4 border (Border)
				left: {
					style: 'thin', // §18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
					color: 'black' // HTML style hex value
				},
				right: {
					style: 'thin',
					color: 'black'
				},
				top: {
					style: 'thin',
					color: 'black'
				},
				bottom: {
					style: 'thin',
					color: 'black'
				}
			}
		});
		var cellStyle = wb.createStyle({
			border: { // §18.8.4 border (Border)
				left: {
					style: 'thin', // §18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
					color: 'black' // HTML style hex value
				},
				right: {
					style: 'thin',
					color: 'black'
				},
				top: {
					style: 'thin',
					color: 'black'
				},
				bottom: {
					style: 'thin',
					color: 'black'
				}
			}
		});
		// var notificationStyle = wb.createStyle({
		//     fill: {
		//         type: 'pattern',
		//         patternType: 'solid',
		//         bgColor: '#F79646',
		//         fgColor: '#F79646' // HTML style hex value. defaults to black
		//     },
		//     font: {
		//         extend: true
		//     }
		// });
		for (var i = 1; i <= columns.length; i++) {
			ws.cell(1, i).string(columns[i - 1]).style(headerStyle);
			var arr = dataObj[columns[i - 1]] || [];
			var rowCount = dataObj.rowCount || 0;
			for (var rowIndex = 0; rowIndex < rowCount; rowIndex++) {
				var cellValue = arr[rowIndex];
				switch (typeof cellValue === 'undefined' ? 'undefined' : (0, _typeof3.default)(cellValue)) {
					case 'string':
						ws.cell(rowIndex + 2, i).string(cellValue).style(cellStyle);
						break;
					case 'number':
						ws.cell(rowIndex + 2, i).number(cellValue).style(cellStyle);
						break;
					default:
						ws.cell(rowIndex + 2, i).style(cellStyle);
						break;
				}
			}
			// ws.cell(dataObj.rowCount + 2, i).style(notificationStyle);
		}
		ws.row(1).setHeight(35);
		// ws.cell(dataObj.rowCount + 2, 1).string(StringConstants.SAMPLE_EXCEL_FOOTER_MESSAGE);

		var alphabet = 'ABCDEFGHIJKLMNOPQ'.split('');
		var groupColLetter = columns.indexOf(partyFieldHeaders.PARTY_GROUP);
		var openingBalanceLetter = columns.indexOf(partyFieldHeaders.OPENING_BALANCE);

		var partyGroupList = partyGroupCache.getNameGroupList();
		if (settingCache.isPartyGroupEnabled()) {
			for (var _i = 0; _i < partyGroupList.length; _i++) {
				ws.cell(_i + 2, 30).string(partyGroupList[_i].getGroupName());
			}
			var partyGroupListLength = partyGroupList.length + 1;
			ws.column(30).hide();
			ws.addDataValidation({
				type: 'list',
				allowBlank: true,
				showDropDown: true,
				sqref: alphabet[groupColLetter] + '2:' + alphabet[groupColLetter] + '10000',
				formulas: ['=$AD$2:$AD$' + partyGroupListLength]
			});
		}
		ws.addDataValidation({
			type: 'decimal',
			allowBlank: true,
			operator: 'between',
			error: 'Opening balance should be a decimal number',
			showDropDown: true,
			sqref: alphabet[openingBalanceLetter] + '2:' + alphabet[openingBalanceLetter] + '10000',
			formulas: [-9999999999, 99999999999]
		});

		var fileName = dialog.showSaveDialog({
			title: 'Save sample pdf',
			defaultPath: 'Import Parties Template',
			filters: [{
				name: 'Excel File',
				extensions: ['xlsx']
			}]
		});
		if (!fileName) {
			return;
		}
		wb.write(fileName, function (err, stats) {
			if (err) {
				logger.error(err);
			} else {
				ToastHelper.success('Download Successful'); // Prints out an instance of a node.js fs.Stats object
				var FileUtil = require('../Utilities/FileUtil');
				FileUtil.showFileInFolder(fileName);
			}
		});
	};
};
var importPartiesXL = new ImportPartiesXL();
module.exports = importPartiesXL;