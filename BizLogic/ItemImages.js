var ItemImagesModel = require('../Models/ItemImagesModel');
var DataLoader = require('./../DBManager/DataLoader.js');
var dataloader = new DataLoader();
var ItemImages = function ItemImages() {
	this._itemImageId;
	this._itemId;
	this._catalogueItemId;
	this._base64Image;
};

ItemImages.prototype.getItemImageId = function () {
	return this._itemImageId;
};
ItemImages.prototype.setItemImageId = function (itemImageId) {
	this._itemImageId = itemImageId;
};

ItemImages.prototype.getItemId = function () {
	return this._itemId;
};
ItemImages.prototype.setItemId = function (itemId) {
	this._itemId = itemId;
};

ItemImages.prototype.getCatalogueItemId = function () {
	return this._catalogueItemId;
};
ItemImages.prototype.setCatalogueItemId = function (catalogueItemId) {
	this._catalogueItemId = catalogueItemId;
};

ItemImages.prototype.getBase64Image = function () {
	return this._base64Image;
};

ItemImages.prototype.setBase64Image = function (base64Img) {
	this._base64Image = base64Img;
};

ItemImages.prototype.getHexImage = function () {
	return this._hexImage;
};

ItemImages.prototype.setHexImage = function (hexImg) {
	this._hexImage = hexImg;
};

ItemImages.prototype.addItemImage = function (itemId) {
	var itemImagesModel = new ItemImagesModel();
	itemImagesModel.setBase64Image(this.getBase64Image());
	var result = itemImagesModel.addItemImage(itemId);
	this.setItemImageId(itemImagesModel.getItemImageId());
	return result;
};

ItemImages.prototype.getItemImagesObjectListByItemId = function (itemId) {
	var appendDataImageTag = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

	var itemImageList = dataloader.getItemImagesByItemId(itemId, appendDataImageTag);
	var itemImages = [];
	if (itemImageList && itemImageList.length > 0) {
		for (var i = 0; i < itemImageList.length; i++) {
			var itemImage = itemImageList[i];
			itemImages.push({ id: itemImage.getItemImageId(), image: itemImage.getBase64Image() });
		}
	}
	return itemImages;
};

ItemImages.prototype.getItemImageListByItemId = function (itemId) {
	var appendDataImageTag = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
	var isHexImageRequired = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

	var itemImageList = dataloader.getItemImagesByItemId(itemId, appendDataImageTag, isHexImageRequired);
	var itemImages = [];
	if (itemImageList && itemImageList.length > 0) {
		for (var i = 0; i < itemImageList.length; i++) {
			var itemImage = itemImageList[i];
			if (isHexImageRequired) {
				itemImages.push(itemImage.getHexImage());
			} else {
				itemImages.push(itemImage.getBase64Image());
			}
		}
	}
	return itemImages;
};

ItemImages.prototype.getItemImagesObjectListByCatalogueItemId = function (catalogueItemId) {
	var limit = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
	var appendDataImageTag = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

	var itemImageList = dataloader.getCatalogueItemImagesByCatalogueId(catalogueItemId, limit, appendDataImageTag);
	var itemImages = [];
	if (itemImageList && itemImageList.length > 0) {
		for (var i = 0; i < itemImageList.length; i++) {
			var itemImage = itemImageList[i];
			itemImages.push({ id: itemImage.getItemImageId(), image: itemImage.getBase64Image() });
		}
	}
	return itemImages;
};

ItemImages.prototype.getImageListByCatalogueItemId = function (catalogueItemId) {
	var limit = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
	var appendDataImageTag = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

	var itemImageList = dataloader.getCatalogueItemImagesByCatalogueId(catalogueItemId, limit, appendDataImageTag);
	var itemImages = [];
	if (itemImageList && itemImageList.length > 0) {
		for (var i = 0; i < itemImageList.length; i++) {
			var itemImage = itemImageList[i];
			itemImages.push(itemImage.getBase64Image());
		}
	}
	return itemImages;
};

ItemImages.prototype.deleteItemImage = function () {
	var itemImagesModel = new ItemImagesModel();
	itemImagesModel.setItemImageId(this.getItemImageId());
	var result = itemImagesModel.deleteItemImage();
	return result;
};

ItemImages.prototype.updateCatalogueItemIdOfItemImages = function () {
	var itemImagesModel = new ItemImagesModel();
	itemImagesModel.setCatalogueItemId(this.getCatalogueItemId());
	itemImagesModel.setItemId(this.getItemId());
	var result = itemImagesModel.updateCatalogueItemIdOfItemImages();
	return result;
};

module.exports = ItemImages;