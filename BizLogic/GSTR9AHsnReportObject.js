var ItemCache = require('./../Cache/ItemCache.js');
var GSTR9AHsnReportObject = function GSTR9AHsnReportObject() {
    this.itemId;
    this.itemName;
    this.itemTotalValue = 0;
    this.itemTaxableValue = 0;
    this.itemIGSTAmount = 0;
    this.itemSGSTAmount = 0;
    this.itemCGSTAmount = 0;
    this.itemHSN = '';
    this.reverseChargeOnTxn = false;
    this.itemType;

    this.getItemType = function () {
        return this.itemType;
    };

    this.setItemType = function (itemType) {
        this.itemType = itemType;
    };

    this.isReverseChargeOnTxn = function () {
        return this.reverseChargeOnTxn;
    };

    this.setReverseChargeOnTxn = function (reverseChargeOnTxn) {

        this.reverseChargeOnTxn = reverseChargeOnTxn ? true : false;
    };

    this.getItemHSN = function () {
        if (!this.itemHSN) {
            this.itemHSN = '';
        }
        return this.itemHSN;
    };
    this.setItemHSN = function (itemHSN) {
        this.itemHSN = itemHSN;
    };
    this.getItemId = function () {
        return this.itemId;
    };
    this.setItemId = function (itemId) {
        this.itemId = itemId;
    };
    this.getItemName = function () {
        return this.itemName;
    };
    this.setItemName = function (itemName) {
        this.itemName = itemName;
    };
    this.getItemTaxableValue = function () {
        return Number(this.itemTaxableValue);
    };
    this.setItemTaxableValue = function (itemTaxableValue) {
        this.itemTaxableValue = Number(itemTaxableValue);
    };
    this.getIGSTAmt = function () {
        return this.itemIGSTAmount;
    };
    this.setIGSTAmt = function (itemIGSTAmount) {
        this.itemIGSTAmount = itemIGSTAmount;
    };
    this.getSGSTAmt = function () {
        return this.itemSGSTAmount;
    };
    this.setSGSTAmt = function (itemSGSTAmount) {
        this.itemSGSTAmount = itemSGSTAmount;
    };
    this.getCGSTAmt = function () {
        return this.itemCGSTAmount;
    };
    this.setCGSTAmt = function (itemCGSTAmount) {
        this.itemCGSTAmount = itemCGSTAmount;
    };
};

module.exports = GSTR9AHsnReportObject;