var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ChangeDB = function ChangeDB() {
	var _require$remote = require('electron').remote,
	    app = _require$remote.app,
	    dialog = _require$remote.dialog;

	var appPath = app.getPath('userData');
	this.changeDatabase = function () {
		var path = require('path');
		var dirname = appPath + '/BusinessNames';
		// var googleDrive = new GoogleDrive();
		var selectedFile = dialog.showOpenDialog({
			title: 'Select Database file to Backup',
			buttonLabel: 'Select',
			properties: ['openFile'],
			filters: [{
				name: 'Vyapar DB File', extensions: ['vyp']
			}]
		});
		if (selectedFile) {
			var name = selectedFile[0];
			var fs = require('fs');
			fs.writeFileSync(appPath + '/BusinessNames/currentDB.txt', name.replace(/\\/g, '/'));
			var CompanyListUtility = require('./../Utilities/CompanyListUtility.js');
			var fileNameObj = CompanyListUtility.getCompaniesFileNameObject();
			businessName = name.split('\\').splice(-1)[0].split('.')[0];
			var lastNotificationSentAt = new Date().getTime();
			fileNameObj[businessName] = { 'path': name.replace(/\\/g, '/'), 'company_creation_type': 0, 'lastNotificationSentAt': lastNotificationSentAt };
			var jsonToInsert = (0, _stringify2.default)(fileNameObj);
			jsonToInsert = jsonToInsert.replace(/\\/g, '/');
			CompanyListUtility.updateCompanyListFile(jsonToInsert);
		}
	};
};

module.exports = ChangeDB;