var PartyGroupCache = require('./../Cache/PartyGroupCache.js');
var PartyGroupModel = require('./../Models/PartyGroupModel.js');
var TransactionManager = require('./../DBManager/TransactionManager.js');
var PartyGroupLogic = function PartyGroupLogic() {
	this.groupId = 0;
	this.groupName = '';
	this.memberCount = 0;

	var ErrorCode = require('./../Constants/ErrorCode.js');
	var NameType = require('./../Constants/NameType.js');

	this.getGroupId = function () {
		return this.groupId;
	};
	this.setGroupId = function (groupId) {
		this.groupId = groupId;
	};

	this.getGroupName = function () {
		return this.groupName;
	};
	this.setGroupName = function (groupName) {
		this.groupName = groupName;
	};

	this.getMemberCount = function () {
		return this.memberCount;
	};
	this.setMemberCount = function (memberCount) {
		this.memberCount = memberCount;
	};

	this.saveNewGroup = function (groupName) {
		var partyGroupCache = new PartyGroupCache();
		var statusCode = ErrorCode.ERROR_PARTYGROUP_SAVE_FAILED;

		if (!groupName || !groupName.trim()) {
			statusCode = ErrorCode.ERROR_PARTYGROUP_NAME_EMPTY_FAILED;
			return statusCode;
		}
		this.groupName = groupName.trim();
		if (partyGroupCache.getPartyGroupId(this.groupName) > 0) {
			statusCode = ErrorCode.ERROR_PARTYGROUP_ALREADYEXISTS;
			return statusCode;
		}
		var partyGroupModel = new PartyGroupModel();
		partyGroupModel.setGroupName(this.groupName);
		var transactionManager = new TransactionManager();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_PARTYGROUP_SAVE_FAILED;
			return statusCode;
		}

		statusCode = partyGroupModel.addNewGroup();
		if (statusCode == ErrorCode.ERROR_PARTYGROUP_SAVE_SUCCESS) {
			this.groupId = partyGroupModel.getGroupId();

			if (!transactionManager.CommitTransaction()) {
				statusCode = ErrorCode.ERROR_PARTYGROUP_SAVE_FAILED;
			} else {
				partyGroupCache.refreshPartyGroup(this);
			}
		}
		transactionManager.EndTransaction();
		return statusCode;
	};

	this.editPartyGroup = function (oldName, newName) {
		var partyGroupCache = new PartyGroupCache();
		var statusCode = ErrorCode.ERROR_PARTYGROUP_UPDATE_FAILED;
		if (!newName || !newName.trim()) {
			statusCode = ErrorCode.ERROR_PARTYGROUP_NAME_EMPTY_FAILED;
			return statusCode;
		}
		this.groupName = newName.trim();
		if (partyGroupCache.getPartyGroupId(this.groupName) > 0) {
			statusCode = ErrorCode.ERROR_PARTYGROUP_ALREADYEXISTS;
			return statusCode;
		}
		var oldGroupId = partyGroupCache.getPartyGroupId(oldName);
		var partyGroupModel = new PartyGroupModel();
		partyGroupModel.setGroupName(this.groupName);
		var transactionManager = new TransactionManager();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_PARTYGROUP_UPDATE_FAILED;
			return statusCode;
		}

		statusCode = partyGroupModel.editPartyGroup(oldGroupId);
		if (statusCode == ErrorCode.ERROR_PARTYGROUP_UPDATE_SUCCESS) {
			if (!transactionManager.CommitTransaction()) {
				statusCode = ErrorCode.ERROR_PARTYGROUP_UPDATE_FAILED;
			} else {
				partyGroupCache.refreshPartyGroup(this);
			}
		}
		transactionManager.EndTransaction();
		return statusCode;
	};

	this.deletePartyGroup = function () {
		var partyGroupModel = new PartyGroupModel();
		var canDelete = false;
		var statusCode = ErrorCode.ERROR_PARTYGROUP_DELETE_FAILED;
		canDelete = partyGroupModel.canDeletePartyGroup(this.getGroupId());
		if (!canDelete) {
			ToastHelper.error('This group cannot be deleted since there are few parties under this. Please move them and try again later.');
			return false;
		} else {
			var conf = confirm('Do you want to delete this group?');
			if (!conf) {
				return false;
			} else {
				statusCode = partyGroupModel.deletePartyGroup(this.getGroupId());
				return statusCode;
			}
		}
	};
};

module.exports = PartyGroupLogic;