var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();
var ErrorCode = require('../Constants/ErrorCode');
var TxnTypeConstant = require('../Constants/TxnTypeConstant');

var DeleteTransaction = function DeleteTransaction() {
	this.delelteTransaction = function (param) {
		if (!param) {
			ToastHelper.error(ErrorCode.ERROR_TXN_DELETE_FAILED);
			return;
		}
		var txnId = param.split(':')[0];
		var txnType = param.split(':')[1];
		txnType = TxnTypeConstant[txnType];
		var salePurchaseFile = [TxnTypeConstant.TXN_TYPE_SALE, TxnTypeConstant.TXN_TYPE_PURCHASE, TxnTypeConstant.TXN_TYPE_CASHIN, TxnTypeConstant.TXN_TYPE_CASHOUT, TxnTypeConstant.TXN_TYPE_EXPENSE, TxnTypeConstant.TXN_TYPE_OTHER_INCOME, TxnTypeConstant.TXN_TYPE_SALE_RETURN, TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN, TxnTypeConstant.TXN_TYPE_SALE_ORDER, TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN, TxnTypeConstant.TXN_TYPE_ESTIMATE, TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER];

		var bankFile = [TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD, TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE, TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD_WITHOUT_CASH, TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE_WITHOUT_CASH, TxnTypeConstant.TXN_TYPE_BANK_TO_BANK];
		var cashFile = [TxnTypeConstant.TXN_TYPE_CASH_ADJ_ADD, TxnTypeConstant.TXN_TYPE_CASH_ADJ_REDUCE];
		var itemAdjFile = [TxnTypeConstant.TXN_TYPE_ADJ_ADD, TxnTypeConstant.TXN_TYPE_ADJ_REDUCE];
		var DataLoader = require('./../DBManager/DataLoader.js');
		var dataLoader = new DataLoader();
		var statusCode = 'Cannot Delete Transaction';
		if ($.inArray(txnType, salePurchaseFile) != -1) {
			var txnObj = dataLoader.LoadTransactionFromId(txnId);
			statusCode = txnObj.deleteTransaction();
		} else if ($.inArray(txnType, itemAdjFile) != -1) {
			var itemAdjObj = dataLoader.getItemAdjRowForItemAdjId(txnId);
			statusCode = itemAdjObj[0].deleteAdjTxn();
		} else if ($.inArray(txnType, bankFile) != -1) {
			var bankAdjObj = dataLoader.LoadAccountAdjustmentForId(txnId);
			statusCode = bankAdjObj.deleteAdjTxn();
		} else if ($.inArray(txnType, cashFile) != -1) {
			var cashAdjObj = dataLoader.getCashAdjForAdjId(txnId);
			statusCode = cashAdjObj.deleteAdjTxn();
		}

		if (statusCode == ErrorCode.ERROR_TXN_DELETE_SUCCESS || statusCode == ErrorCode.ERROR_BANK_ADJ_DELETE_SUCCESS || statusCode == ErrorCode.ERROR_CASH_ADJ_DELETE_SUCCESS || statusCode == ErrorCode.ERROR_ITEM_ADJ_DELETE_SUCCESS) {
			ToastHelper.success(statusCode);
		} else {
			ToastHelper.error(statusCode);
		}
		onResume();
	};

	this.RemoveTransaction = function (param) {
		var conf = confirm('Do you want to delete transaction?');
		if (conf) {
			var ifPasscode = settingCache.getDeletePasscodeEnabled();
			if (ifPasscode) {
				UIHelper.showDeleteTxnPasscodeDialog(param, this.delelteTransaction);
				$('#passCodeDeleteDialog input').val('');
			} else {
				this.delelteTransaction(param);
			}
		} else {
			return false;
		}
	};
};

module.exports = DeleteTransaction;