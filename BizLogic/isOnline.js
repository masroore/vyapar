/**
 * Created by rahi on 3/20/2017.
 */
var isOnline = function isOnline(user_callback) {
	/**
     * Show a warning to the user.
     * You can retry in the dialog until a internet connection
     * is active.
     */
	var message = function message() {
		var dialog = require('electron').remote.dialog;

		return dialog.showMessageBox({
			title: "There's no internet",
			message: 'No internet available, do you want to try again?',
			type: 'warning',
			buttons: ['Try again please'],
			defaultId: 0
		}, function (index) {
			$;
			// if clicked "Try again please"
			if (index == 0) {
				execute();
			}
		});
	};

	var execute = function execute() {
		if (navigator.onLine) {
			// Execute action if internet available.
			user_callback();
		} else {
			// Show warning to user
			// And "retry" to connect
			message();
		}
	};

	// Verify for first time
	execute();
};

module.exports = {
	isOnline: isOnline
};