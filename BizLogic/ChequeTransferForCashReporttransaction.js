var ChequeTransferForCashReporttransaction = function ChequeTransferForCashReporttransaction() {
	var cashAmount;
	var transferedTobankId = 0;
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	this.getBalanceAmount = function () {
		return 0;
	};
	this.getTxnType = function () {
		return TxnTypeConstant.TXN_TYPE_CHEQUE_TRANSFER;
	};

	this.getTaxAmount = function () {
		return 0.0;
	};

	this.getDiscountAmount = function () {
		return 0.0;
	};

	this.getBankId = function () {
		return this.bankId;
	};

	this.getTxnTypeString = function () {
		return 'TXN_TYPE_CHEQUE_TRANSFER';
	};
	this.setBalanceAmount = function (balanceAmount) {
		return 'SUCCESS';
	};

	this.setCashAmount = function (cashAmount) {
		if (!cashAmount) {
			this.cashAmount = '0.0';
		} else if (!isNaN(Number(cashAmount))) {
			this.cashAmount = Number(cashAmount);
		} else {
			return 'ERROR_TXN_INVALID_AMOUNT';
		}
		return 'SUCCESS';
	};

	this.getCashAmount = function () {
		return this.cashAmount;
	};

	this.setAmounts = function (totalAmount, cashAmount) {
		return this.setCashAmount(cashAmount);
	};

	this.getTxnRefNumber = function () {
		this.txnRefNumber = '';
		return this.txnRefNumber;
	};
	this.setTxnRefNumber = function (txnRefNumber) {};

	this.getTransferedTobankId = function () {
		return this.transferedTobankId;
	};

	this.setTransferedTobankId = function (transferedTobankId) {
		this.transferedTobankId = transferedTobankId;
	};

	this.getTransactionMessage = function () {
		return '';
	};
	this.loadAllLineItems = function () {
		this.lineItems = [];
	};
};

module.exports = ChequeTransferForCashReporttransaction;