var ExpenseCategoryObject = function ExpenseCategoryObject() {
	this.nameId = 0;
	this.expenseCategoryName = '';
	this.expenseCategoryAmount = 0.0;

	this.getNameId = function () {
		return this.nameId;
	};
	this.setNameId = function (nameId) {
		this.nameId = nameId;
	};

	this.getExpenseCategoryName = function () {
		return this.expenseCategoryName;
	};
	this.setExpenseCategoryName = function (expenseCategoryName) {
		this.expenseCategoryName = expenseCategoryName;
	};

	this.getExpenseCategoryAmount = function () {
		return this.expenseCategoryAmount;
	};
	this.setExpenseCategoryAmount = function (expenseCategoryAmount) {
		this.expenseCategoryAmount = expenseCategoryAmount;
	};
};

module.exports = ExpenseCategoryObject;