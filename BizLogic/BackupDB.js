var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _create = require('babel-runtime/core-js/object/create');

var _create2 = _interopRequireDefault(_create);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var backUpsInProgress = {
	'googleDrive': false
};
var BackupDB = function BackupDB() {
	var fs = require('fs');
	var path = require('path');

	var app = require('electron').remote.app;

	var appUserDataPath = app.getPath('userData');
	var MyDate = require('./../Utilities/MyDate.js');
	var currentDateString = MyDate.getDate('d-m-y_H.M.S', new Date());
	var yazl = require('yazl');

	this.backup = function () {
		var alertUser = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
		var sendToMail = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

		if (backUpsInProgress['googleDrive']) {
			if (alertUser) {
				ToastHelper.info('Backup is in progress.');
			}
			return;
		}
		var isOnline = require('is-online');
		isOnline().then(function (online) {
			if (!online) {
				if (alertUser) {
					ToastHelper.error('Please check internet connection.');
				}
				return false;
			}
			var GoogleDrive = require('./GoogleDrive.js');
			var googleDrive = new GoogleDrive();
			var selectedFile = fs.readFileSync(appUserDataPath + '/BusinessNames/currentDB.txt').toString();
			if (selectedFile) {
				backUpsInProgress['googleDrive'] = true;
				// if (alertUser) {
				// 	ToastHelper.info('Started Backup to Google Drive. Please continue with your work.');
				// }
				var selectedFileName = path.normalize(selectedFile);
				selectedFileName = path.basename(selectedFileName);
				var zipFileName = 'VypBackup_' + path.parse(selectedFileName)['name'] + '_' + currentDateString + '.vyb';
				var zipfile = new yazl.ZipFile();
				zipfile.addFile(selectedFile, selectedFileName);
				zipfile.outputStream.pipe(fs.createWriteStream(appUserDataPath + '/BusinessNames/' + zipFileName)).on('close', onStreamComplete).on('error', onStreamComplete);
				zipfile.end();
			}

			function onStreamComplete(err) {
				if (err) {
					backUpsInProgress['googleDrive'] = false;
					ToastHelper.error('Backup failed');
					return;
				}
				if (sendToMail) {
					googleDrive.UploadDBToDrive(zipFileName, appUserDataPath + '/BusinessNames/' + zipFileName, onUploadComplete);
				}
			}

			function onUploadComplete(err, response) {
				if (err && (err.message === 'invalid_grant' || err.code === 401)) {
					if (sendToMail) {
						googleDrive.UploadDBToDrive(zipFileName, appUserDataPath + '/BusinessNames/' + zipFileName, onUploadComplete);
					}
					return false;
				}
				if (alertUser) {
					if (err) {
						switch (err.code) {
							case 'ECONNREFUSED':
							case 'ECONNRESET':
							case 'ETIMEDOUT':
								ToastHelper.error('Backup unsuccessful. Please check your internet connection.');
								break;
							default:
								if (err.message == 'User closed the window') {
									ToastHelper.error('You have denied access for auto backup on google drive.');
								} else {
									ToastHelper.error('Backup unsuccessful');
								}
						}
					} else {
						ToastHelper.success('Back up successful');
					}
				}
				backUpsInProgress['googleDrive'] = false;
			}
		});
	};

	this.autoBackup = function () {
		var _this = this;

		var alertUser = false;
		var SettingCache = require('./../Cache/SettingCache.js');
		var settingCache = new SettingCache();
		var lastBackupTime = settingCache.getLastBackupTime();
		var autoBackupPeriod = settingCache.getAutoBackUpPeriod();
		var timeDiff = Math.abs(new Date().getTime() - lastBackupTime.getTime());
		var diffDays = Math.floor(timeDiff / (1000 * 3600 * 24));
		if (diffDays >= autoBackupPeriod) {
			var isOnline = require('is-online');

			isOnline().then(function (online) {
				if (online) {
					_this.backup(alertUser);
				} else {
					var intervalId = setInterval(function () {
						var _this2 = this;

						isOnline().then(function (online) {
							if (online) {
								_this2.backup(alertUser);
								clearInterval(intervalId);
							}
						});
					}.bind(_this), 10000); // 10 seconds
				}
			});
		}
	};

	this.localBackup = function () {
		var dialog = require('electron').remote.dialog;

		var selectedFile = fs.readFileSync(appUserDataPath + '/BusinessNames/currentDB.txt').toString();
		var selectedFileName = path.normalize(selectedFile);
		selectedFileName = path.basename(selectedFileName);

		var zipFileName = 'VypBackup_' + path.parse(selectedFileName)['name'] + '_' + currentDateString + '.vyb';
		var fileName = dialog.showSaveDialog({
			title: 'Save Database Backup',
			defaultPath: zipFileName,
			filters: [{ name: 'VYB Files', extensions: ['vyb'] }]
		});

		if (!fileName) {
			return;
		}
		var zipfile = new yazl.ZipFile();
		zipfile.addFile(selectedFile, selectedFileName);
		zipfile.outputStream.pipe(fs.createWriteStream(fileName)).on('close', onStreamComplete).on('error', onStreamComplete);
		zipfile.end();
		function onStreamComplete(err) {
			if (err) {
				ToastHelper.error('Backup failed');
			} else {
				var FileUtil = require('../Utilities/FileUtil');
				FileUtil.showFileInFolder(fileName);
			}
		}
	};

	this.closeBookBackup = function (callbackSuccess, callbackFailure) {
		var backupStatus = (0, _create2.default)(null);
		var errors = [];
		function handleWriteComplete(err, backupType) {
			if (err) {
				errors.push(err);
				backupStatus[backupType] = 'Error';
			} else {
				backupStatus[backupType] = 'Success';
			}
			if ((0, _keys2.default)(backupStatus).length == 2) {
				(0, _values2.default)(backupStatus).includes('Error') ? callbackFailure(errors) : callbackSuccess(fileName);
			}
		}
		try {
			var dialog = require('electron').remote.dialog;

			var selectedFile = fs.readFileSync(appUserDataPath + '/BusinessNames/currentDB.txt').toString();
			var selectedFileName = path.normalize(selectedFile);
			selectedFileName = path.basename(selectedFileName);
			var zipFileName = 'VypBackup_CloseBook_' + path.parse(selectedFileName)['name'] + '_' + currentDateString + '.vyb';
			var fileName = dialog.showSaveDialog({
				title: 'Save Current Database Backup',
				defaultPath: zipFileName,
				filters: [{ name: 'VYB Files', extensions: ['vyb'] }]
			});
			if (!fileName) {
				callbackFailure();
			}
			fileName = fileName.endsWith('.vyb') ? fileName : fileName + '.vyb';
			var zipFile = new yazl.ZipFile();
			zipFile.addFile(selectedFile, selectedFileName);
			var tempBackUpDir = appUserDataPath + '/BusinessNames/Temp_Backup/';
			if (!fs.existsSync(tempBackUpDir)) {
				fs.mkdir(tempBackUpDir, function () {});
			}
			zipFile.outputStream.pipe(fs.createWriteStream(tempBackUpDir + zipFileName)).on('close', function (err) {
				handleWriteComplete(err, 'tempBackup');
			}).on('error', function (err) {
				handleWriteComplete(err, 'tempBackup');
			});

			zipFile.outputStream.pipe(fs.createWriteStream(fileName)).on('close', function (err) {
				handleWriteComplete(err, 'userBackup');
			}).on('error', function (err) {
				handleWriteComplete(err, 'userBackup');
			});
			zipFile.end();
		} catch (err) {
			callbackFailure();
		}
	};
};

module.exports = BackupDB;