var TaxRateReportObject = function TaxRateReportObject() {
	var taxName = '';
	var taxPercent;
	var taxIn;
	var taxOut;
	var taxableSaleValue;
	var taxablePurchaseValue;

	this.getTaxName = function () {
		return this.taxName;
	};

	this.setTaxName = function (taxName) {
		this.taxName = taxName;
	};

	this.getTaxPercent = function () {
		return this.taxPercent;
	};

	this.setTaxPercent = function (taxPercent) {
		this.taxPercent = taxPercent;
	};

	this.getTaxIn = function () {
		return this.taxIn;
	};

	this.setTaxIn = function (taxIn) {
		this.taxIn = taxIn;
	};

	this.getTaxOut = function () {
		return this.taxOut;
	};

	this.setTaxOut = function (taxOut) {
		this.taxOut = taxOut;
	};
	this.getSaleTaxableValue = function () {
		return this.taxableSaleValue;
	};

	this.setSaleTaxableValue = function (taxableSaleValue) {
		this.taxableSaleValue = taxableSaleValue;
	};
	this.getPurchaseTaxableValue = function () {
		return this.taxablePurchaseValue;
	};

	this.setPurchaseTaxableValue = function (taxablePurchaseValue) {
		this.taxablePurchaseValue = taxablePurchaseValue;
	};
};

module.exports = TaxRateReportObject;