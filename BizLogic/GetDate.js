var GetDate = function GetDate() {
	// usage
	// getDate("y/m/d"));
	this.daysShort = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
	this.daysLong = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

	this.getDate = function (format, date) {
		if (!date) {
			date = new Date();
		}
		var mDate = {
			'S': date.getSeconds(),
			'M': date.getMinutes(),
			'H': date.getHours(),
			'd': date.getDate(),
			'm': date.getMonth() + 1,
			'y': date.getFullYear(),
			'q': this.daysShort[date.getDay()],
			'Q': this.daysLong[date.getDay()]
		};

		// Apply format and add leading zeroes
		return format.replace(/([SMHdmyqQ])/g, function (key) {
			return (mDate[key] < 10 ? '0' : '') + mDate[key];
		});
	};

	this.getDateObj = function (date, format, delimiter) {
		var formatLowerCase = format.toLowerCase();
		var formatItems = formatLowerCase.split(delimiter);
		date = date.split(' ')[0];
		var dateItems = date.split(delimiter);
		var monthIndex = formatItems.indexOf('mm');
		var dayIndex = formatItems.indexOf('dd');
		var yearIndex = formatItems.indexOf('yyyy');
		var month = parseInt(dateItems[monthIndex]);
		month -= 1;
		var formatedDate = new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
		return formatedDate;
	};
};

module.exports = GetDate;