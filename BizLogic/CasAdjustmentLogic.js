var CashAdjustmentLogic = function CashAdjustmentLogic() {
	var adjType, adjAmount, adjDate, adjDescription, adjId;

	this.getAdjId = function () {
		return this.adjId;
	};
	this.setAdjId = function (adjId) {
		this.adjId = adjId;
	};

	this.getAdjType = function () {
		return this.adjType;
	};
	this.setAdjType = function (adjType) {
		this.adjType = adjType;
	};

	this.getAdjAmount = function () {
		return this.adjAmount;
	};
	this.setAdjAmount = function (adjAmount) {
		this.adjAmount = adjAmount;
	};

	this.getAdjDate = function () {
		return this.adjDate;
	};
	this.setAdjDate = function (adjDate) {
		// var GetDate = require('./../BizLogic/GetDate.js');
		// var getDate = new GetDate();
		// var ajdDate = getDate.getDateObj(adjDate);
		this.adjDate = adjDate;
	};

	this.getAdjDescription = function () {
		return this.adjDescription;
	};
	this.setAdjDescription = function (adjDescription) {
		this.adjDescription = adjDescription;
	};

	this.saveNewAdjustment = function (adjType, adjAmount, adjDate, adjDescription) {
		this.setAdjType(adjType);
		this.setAdjAmount(adjAmount);
		this.setAdjDate(adjDate);
		this.setAdjDescription(adjDescription);
		var statusCode = void 0;

		var CashAdjustmentModel = require('./../Models/CashAdjustmentModel.js');
		var cashAdjustmentModel = new CashAdjustmentModel();
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
		var transactionManager = new TransactionManager();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_NEW_CASH_ADJUSTMENT_FAILED;
			return statusCode;
		}

		try {
			cashAdjustmentModel.setAdjAmount(this.adjAmount);
			cashAdjustmentModel.setAdjType(TxnTypeConstant[this.adjType]);
			cashAdjustmentModel.setAdjDate(this.adjDate);
			cashAdjustmentModel.setAdjDescription(this.adjDescription);
			statusCode = cashAdjustmentModel.saveNewAdjustment();
			if (statusCode == ErrorCode.ERROR_NEW_CASH_ADJUSTMENT_SUCCESS) {
				if (!transactionManager.CommitTransaction()) {
					statusCode = ErrorCode.ERROR_NEW_CASH_ADJUSTMENT_FAILED;
				}
			}
		} catch (err) {
			logger.error('New Cash Adjustment exception ' + err);
		}
		transactionManager.EndTransaction();
		return statusCode;
	};

	this.updateCashAdjustment = function (adjId, adjType, adjAmount, adjDate, adjDescription) {
		this.setAdjId(adjId);
		this.setAdjType(adjType);
		this.setAdjAmount(adjAmount);
		this.setAdjDate(adjDate);
		this.setAdjDescription(adjDescription);
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var transactionManager = new TransactionManager();
		var ErrorCode = require('./../Constants/ErrorCode.js');
		var CashAdjustmentModel = require('./../Models/CashAdjustmentModel.js');
		var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
		var cashAdjustmentModel = new CashAdjustmentModel();
		cashAdjustmentModel.setAdjId(this.adjId);
		cashAdjustmentModel.setAdjAmount(this.adjAmount);
		cashAdjustmentModel.setAdjType(TxnTypeConstant[this.adjType]);
		cashAdjustmentModel.setAdjDate(this.adjDate);
		cashAdjustmentModel.setAdjDescription(this.adjDescription);

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_UPDATE_CASH_ADJUSTMENT_FAILED;
			return statusCode;
		}
		try {
			var statusCode = cashAdjustmentModel.updateCashAdjustment();
			if (statusCode == ErrorCode.ERROR_UPDATE_CASH_ADJUSTMENT_SUCCESS) {
				if (!transactionManager.CommitTransaction()) {
					statusCode = ErrorCode.ERROR_UPDATE_CASH_ADJUSTMENT_FAILED;
				}
			}
		} catch (err) {
			logger.error('Update cash adjustment exception ' + err);
		}
		transactionManager.EndTransaction();
		return statusCode;
	};

	this.deleteAdjTxn = function () {
		var CashAdjustmentTxnModel = require('./../Models/CashAdjustmentModel.js');
		var statusCode = ErrorCode.ERROR_CASH_ADJ_DELETE_FAILED;
		var cashAdjustmentTxnModel = new CashAdjustmentTxnModel();
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var transactionManager = new TransactionManager();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();
		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_CASH_ADJ_DELETE_FAILED;
			return statusCode;
		}
		try {
			cashAdjustmentTxnModel.setAdjId(this.getAdjId());
			statusCode = cashAdjustmentTxnModel.deleteAdjTxn();
			if (statusCode == ErrorCode.ERROR_CASH_ADJ_DELETE_SUCCESS) {
				if (!transactionManager.CommitTransaction()) {
					statusCode = ErrorCode.ERROR_CASH_ADJ_DELETE_FAILED;
				}
			}
		} catch (err) {
			logger.error('Delete cash adj exception ' + err);
		}
		transactionManager.EndTransaction();
		return statusCode;
	};
};

module.exports = CashAdjustmentLogic;