var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _nameCacheRefreshNeeded = false;
var _itemCacheRefreshNeeded = false;
var _firmCacheRefreshNeeded = false;
var _itemUnitCacheRefreshNeeded = false;
var _partyGroupCacheRefreshNeeded = false;
var _itemCategoryCacheRefreshNeeded = false;
var _settingCacheRefreshNeeded = false;
var _taxCodeCacheRefreshNeeded = false;
var _customFieldsCacheRefreshNeeded = false;
var _paymentCacheRefreshNeeded = false;
var _loanAccountCacheRefreshNeeded = false;
var _messageConfigCacheRefreshNeeded = false;
var _paymentTermCacheRefreshNeeded = false;
var _itemUnitMappingCacheRefreshNeeded = false;
var _udfCacheRefreshNeeded = false;

var RefreshCacheTracker = function () {
	function RefreshCacheTracker() {
		(0, _classCallCheck3.default)(this, RefreshCacheTracker);
	}

	(0, _createClass3.default)(RefreshCacheTracker, [{
		key: 'refreshForAllCache',
		value: function refreshForAllCache(value) {
			this.nameCacheRefreshNeeded = value;
			this.itemCacheRefreshNeeded = value;
			this.itemCategoryCacheRefreshNeeded = value;
			this.partyGroupCacheRefreshNeeded = value;
			this.settingCacheRefreshNeeded = value;
			this.customFieldsCacheRefreshNeeded = value;
			this.itemUnitCacheRefreshNeeded = value;
			this.firmCacheRefreshNeeded = value;
			this.taxCodeCacheRefreshNeeded = value;
			this.paymentCacheRefreshNeeded = value;
			this.loanAccountCacheRefreshNeeded = value;
			this.messageConfigCacheRefreshNeeded = value;
			this.udfCacheRefreshNeeded = value;
			this.itemUnitMappingCacheRefreshNeeded = value;
		}
	}, {
		key: 'nameCacheRefreshNeeded',
		get: function get() {
			return _nameCacheRefreshNeeded;
		},
		set: function set(value) {
			_nameCacheRefreshNeeded = value;
		}
	}, {
		key: 'itemCacheRefreshNeeded',
		get: function get() {
			return _itemCacheRefreshNeeded;
		},
		set: function set(value) {
			_itemCacheRefreshNeeded = value;
		}
	}, {
		key: 'firmCacheRefreshNeeded',
		get: function get() {
			return _firmCacheRefreshNeeded;
		},
		set: function set(value) {
			_firmCacheRefreshNeeded = value;
		}
	}, {
		key: 'itemUnitCacheRefreshNeeded',
		get: function get() {
			return _itemUnitCacheRefreshNeeded;
		},
		set: function set(value) {
			_itemUnitCacheRefreshNeeded = value;
		}
	}, {
		key: 'partyGroupCacheRefreshNeeded',
		get: function get() {
			return _partyGroupCacheRefreshNeeded;
		},
		set: function set(value) {
			_partyGroupCacheRefreshNeeded = value;
		}
	}, {
		key: 'itemCategoryCacheRefreshNeeded',
		get: function get() {
			return _itemCategoryCacheRefreshNeeded;
		},
		set: function set(value) {
			_itemCategoryCacheRefreshNeeded = value;
		}
	}, {
		key: 'settingCacheRefreshNeeded',
		get: function get() {
			return _settingCacheRefreshNeeded;
		},
		set: function set(value) {
			_settingCacheRefreshNeeded = value;
		}
	}, {
		key: 'taxCodeCacheRefreshNeeded',
		get: function get() {
			return _taxCodeCacheRefreshNeeded;
		},
		set: function set(value) {
			_taxCodeCacheRefreshNeeded = value;
		}
	}, {
		key: 'customFieldsCacheRefreshNeeded',
		get: function get() {
			return _customFieldsCacheRefreshNeeded;
		},
		set: function set(value) {
			_customFieldsCacheRefreshNeeded = value;
		}
	}, {
		key: 'paymentCacheRefreshNeeded',
		get: function get() {
			return _paymentCacheRefreshNeeded;
		},
		set: function set(value) {
			_paymentCacheRefreshNeeded = value;
		}
	}, {
		key: 'loanAccountCacheRefreshNeeded',
		get: function get() {
			return _loanAccountCacheRefreshNeeded;
		},
		set: function set(value) {
			_loanAccountCacheRefreshNeeded = value;
		}
	}, {
		key: 'messageConfigCacheRefreshNeeded',
		get: function get() {
			return _messageConfigCacheRefreshNeeded;
		},
		set: function set(value) {
			_messageConfigCacheRefreshNeeded = value;
		}
	}, {
		key: 'paymentTermCacheRefreshNeeded',
		get: function get() {
			return _paymentTermCacheRefreshNeeded;
		},
		set: function set(value) {
			_paymentTermCacheRefreshNeeded = value;
		}
	}, {
		key: 'udfCacheRefreshNeeded',
		get: function get() {
			return _udfCacheRefreshNeeded;
		},
		set: function set(value) {
			_udfCacheRefreshNeeded = value;
		}
	}, {
		key: 'itemUnitMappingCacheRefreshNeeded',
		get: function get() {
			return _itemUnitMappingCacheRefreshNeeded;
		},
		set: function set(value) {
			_itemUnitMappingCacheRefreshNeeded = value;
		}
	}]);
	return RefreshCacheTracker;
}();

module.exports = RefreshCacheTracker;