var DataVerificationObject = function DataVerificationObject() {
	this.id = 0;
	this.expectedValue = 0;
	this.currentValue = 0;

	this.getId = function () {
		return this.id;
	};
	this.setId = function (id) {
		this.id = id;
	};

	this.getExpectedValue = function () {
		return this.expectedValue;
	};
	this.setExpectedValue = function (expectedValue) {
		this.expectedValue = expectedValue;
	};

	this.getCurrentValue = function () {
		return this.currentValue;
	};
	this.setCurrentValue = function (currentValue) {
		this.currentValue = currentValue;
	};
};

module.exports = DataVerificationObject;