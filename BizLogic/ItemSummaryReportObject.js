var ItemSummaryReportObject = function ItemSummaryReportObject() {
	this.itemId = 0;
	this.stockQuantity = 0;
	this.stockValue = 0;
	this.itemName = '';
	this.salePrice = 0;
	this.purchasePrice = 0;
	this.minimumStockQuantity = 0;

	this.getItemId = function () {
		return this.itemId;
	};

	this.setItemId = function (itemId) {
		this.itemId = itemId;
	};

	this.getItemStockQuantity = function () {
		return this.stockQuantity;
	};

	this.setItemStockQuantity = function (stockQuantity) {
		this.stockQuantity = stockQuantity;
	};

	this.getItemStockValue = function () {
		return this.stockValue;
	};

	this.setItemStockValue = function (stockValue) {
		this.stockValue = stockValue;
	};

	this.getItemName = function () {
		return this.itemName;
	};

	this.setItemName = function (itemName) {
		this.itemName = itemName;
	};

	this.getItemSaleUnitPrice = function () {
		return this.salePrice;
	};

	this.setItemSaleUnitPrice = function (salePrice) {
		this.salePrice = salePrice;
	};

	this.getItemPurchaseUnitPrice = function () {
		return this.purchasePrice;
	};

	this.setItemPurchaseUnitPrice = function (purchasePrice) {
		this.purchasePrice = purchasePrice;
	};
};

module.exports = ItemSummaryReportObject;