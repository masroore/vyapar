var instance;
var ChequeLogic = function ChequeLogic() {
	var ErrorCode = require('./../Constants/ErrorCode.js');
	if (!instance) {
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var ChequeModel = require('./../Models/ChequeModel.js');
		// var MyDate = require('./../Utilities/MyDate.js');
	}
	this.chequeId = 0;
	this.chequeTxnId = 0;
	this.transferDate;
	this.chequeCurrentStatus;
	this.transferredToAccount;
	this.chequeCloseDescription;
	this.chequeCreationDate;
	this.chequeModificationDate;
	this.txnDate;
	this.chequeAmount;
	this.chequeTxnType;
	this.chequeTxnRefNo;
	var chequeNameId = null;
	var chequeClosedTxnRefId = null;

	this.getChequeClosedTxnRefId = function () {
		return chequeClosedTxnRefId;
	};

	this.setChequeClosedTxnRefId = function (id) {
		chequeClosedTxnRefId = id;
	};

	this.setChequeNameId = function (nameId) {
		chequeNameId = nameId;
	};
	this.getChequeNameId = function () {
		return chequeNameId;
	};

	this.getChequeTxnRefNo = function () {
		return this.chequeTxnRefNo;
	};
	this.setChequeTxnRefNo = function (chequeTxnRefNo) {
		this.chequeTxnRefNo = chequeTxnRefNo;
	};

	this.getChequeId = function () {
		return this.chequeId;
	};
	this.setChequeId = function (chequeId) {
		this.chequeId = chequeId;
	};

	this.getChequeTxnId = function () {
		return this.chequeTxnId;
	};
	this.setChequeTxnId = function (chequeTxnId) {
		this.chequeTxnId = chequeTxnId;
	};

	this.getTransferDate = function () {
		return this.transferDate;
	};
	this.setTransferDate = function (transferDate) {
		this.transferDate = transferDate;
	};

	this.getChequeCurrentStatus = function () {
		return this.chequeCurrentStatus;
	};
	this.setChequeCurrentStatus = function (chequeCurrentStatus) {
		this.chequeCurrentStatus = chequeCurrentStatus;
	};

	this.getTransferredToAccount = function () {
		return this.transferredToAccount;
	};
	this.setTransferredToAccount = function (transferredToAccount) {
		this.transferredToAccount = transferredToAccount;
	};

	this.getChequeCloseDescription = function () {
		return this.chequeCloseDescription;
	};
	this.setChequeCloseDescription = function (chequeCloseDescription) {
		this.chequeCloseDescription = chequeCloseDescription;
	};

	this.getChequeCreationDate = function () {
		return this.chequeCreationDate;
	};
	this.setChequeCreationDate = function (chequeCurrentDate) {
		this.chequeCreationDate = chequeCurrentDate;
	};

	this.getChequeModificationDate = function () {
		return this.chequeModificationDate;
	};
	this.setChequeModificationDate = function (chequeModificationDate) {
		this.chequeModificationDate = chequeModificationDate;
	};

	this.getTxnDate = function () {
		return this.txnDate;
	};
	this.setTxnDate = function (txnDate) {
		this.txnDate = txnDate;
	};

	this.getChequeAmount = function () {
		return this.chequeAmount;
	};
	this.setChequeAmount = function (chequeAmount) {
		this.chequeAmount = chequeAmount;
	};

	this.getChequeTxnType = function () {
		return this.chequeTxnType;
	};
	this.setChequeTxnType = function (chequeTxnType) {
		this.chequeTxnType = chequeTxnType;
	};

	this.addCheque = function () {
		var statusCode = ErrorCode.ERROR_CHEQUE_SAVE_FAILED;
		var transactionmanager = new TransactionManager();

		var isTransactionBeginSuccess = transactionmanager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_CHEQUE_SAVE_FAILED;
			return statusCode;
		}

		try {
			var chequemodel = new ChequeModel();
			chequemodel.setChequeTxnId(this.getChequeTxnId());
			statusCode = chequemodel.addCheque();
			if (statusCode == ErrorCode.ERROR_CHEQUE_SAVE_SUCCESS) {
				if (!transactionmanager.CommitTransaction()) {
					statusCode = ErrorCode.ERROR_CHEQUE_SAVE_FAILED;
				}
			}
		} catch (err) {
			logger.error('Add cheque exception ' + err);
		}
		transactionmanager.EndTransaction();
		return statusCode;
	};

	this.updateChequeStatus = function () {
		var transactionmanager = new TransactionManager();
		var statusCode = void 0;

		var isTransactionBeginSuccess = transactionmanager.BeginTransaction();
		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_CHEQUE_STATUS_UPDATE_FAILED;
			return statusCode;
		}

		try {
			var chequeModel = new ChequeModel();
			chequeModel.setChequeId(this.getChequeId());
			chequeModel.setChequeCurrentStatus(this.getChequeCurrentStatus());
			chequeModel.setChequeCloseDescription(this.getChequeCloseDescription());
			chequeModel.setTransferredToAccount(this.getTransferredToAccount());
			chequeModel.setTransferDate(this.getTransferDate());
			var date = new Date();
			chequeModel.setChequeModificationDate(date);
			var errorCode = chequeModel.updateChequeStatus();
			var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
			var paymentInfoCache = new PaymentInfoCache();
			if (errorCode == ErrorCode.ERROR_CHEQUE_STATUS_UPDATE_SUCCESS) {
				if (!transactionmanager.CommitTransaction()) {
					statusCode = ErrorCode.ERROR_CHEQUE_STATUS_UPDATE_FAILED;
				} else {
					paymentInfoCache.refreshPaymentInfoCache();
				}
			}
		} catch (err) {
			logger.error('Update Cheque Exception ' + err);
		}
		transactionmanager.EndTransaction();
		return errorCode;
	};

	this.deleteCheque = function () {
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var transactionManager = new TransactionManager();
		var ChequeModel = require('./../Models/ChequeModel.js');
		var chequeModel = new ChequeModel();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_CHEQUE_DELETE_FAILED;
			return statusCode;
		}
		try {
			var chequeModel = new ChequeModel();
			chequeModel.setChequeTxnId(this.getChequeTxnId());
			var statusCode = chequeModel.deleteChequeForTxn();

			if (statusCode == ErrorCode.ERROR_CHEQUE_DELETE_SUCCESS) {
				if (!transactionManager.CommitTransaction()) {
					statusCode = ErrorCode.ERROR_CHEQUE_DELETE_FAILED;
				}
			}
		} catch (err) {
			logger.error('Delete cheque exception ' + err);
		}
		transactionManager.EndTransaction();
		return statusCode;
	};

	this.reOpenCheque = function () {
		var ChequeStatus = require('./../Constants/ChequeStatus.js');
		var transactionmanager = new TransactionManager();
		var isTransactionBeginSuccess = transactionmanager.BeginTransaction();
		var statusCode = void 0;
		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_CHEQUE_STATUS_UPDATE_FAILED;
			return statusCode;
		}
		try {
			var chequeModel = new ChequeModel();
			chequeModel.setChequeId(this.getChequeId());
			chequeModel.setChequeCurrentStatus(ChequeStatus.OPEN);
			chequeModel.setChequeCloseDescription('');
			chequeModel.setTransferredToAccount(2);
			var date = new Date();
			chequeModel.setChequeModificationDate(date);
			chequeModel.setTransferDate(date);
			var errorCode = chequeModel.updateChequeStatus();
			var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
			var paymentInfoCache = new PaymentInfoCache();
			if (errorCode == ErrorCode.ERROR_CHEQUE_STATUS_UPDATE_SUCCESS) {
				if (!transactionmanager.CommitTransaction()) {
					statusCode = ErrorCode.ERROR_CHEQUE_STATUS_UPDATE_FAILED;
				} else {
					paymentInfoCache.refreshPaymentInfoCache();
				}
			}
		} catch (err) {
			logger.error('Re open cheque exception ' + err);
		}
		transactionmanager.EndTransaction();
		return errorCode;
	};
};

module.exports = ChequeLogic;