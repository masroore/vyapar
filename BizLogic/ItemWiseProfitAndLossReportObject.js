var ItemWiseProfitAndLossReportObject = function ItemWiseProfitAndLossReportObject() {
	this.itemId = 0;
	this.itemName = '';
	this.saleValue = 0;
	this.saleReturnValue = 0;
	this.purchaseValue = 0;
	this.purchaseReturnValue = 0;
	this.openingStockValue = 0;
	this.closingStockValue = 0;

	this.receivableTax = 0;
	this.payableTax = 0;

	this.activeRow = false;

	this.netProfitAndLossAmount = 0;

	this.getItemId = function () {
		return this.itemId;
	};

	this.setItemId = function (itemId) {
		this.itemId = itemId;
	};

	this.getItemName = function () {
		return this.itemName;
	};

	this.setItemName = function (itemName) {
		this.itemName = itemName;
	};

	this.getSaleValue = function () {
		return this.saleValue;
	};

	this.setSaleValue = function (saleValue) {
		this.saleValue = saleValue;
	};

	this.getSaleReturnValue = function () {
		return this.saleReturnValue;
	};

	this.setSaleReturnValue = function (saleReturnValue) {
		this.saleReturnValue = saleReturnValue;
	};

	this.getPurchaseValue = function () {
		return this.purchaseValue;
	};

	this.setPurchaseValue = function (purchaseValue) {
		this.purchaseValue = purchaseValue;
	};

	this.getPurchaseReturnValue = function () {
		return this.purchaseReturnValue;
	};

	this.setPurchaseReturnValue = function (purchaseReturnValue) {
		this.purchaseReturnValue = purchaseReturnValue;
	};

	this.setOpeningStockValue = function (openingStockValue) {
		this.openingStockValue = openingStockValue;
	};

	this.setClosingStockValue = function (closingStockValue) {
		this.closingStockValue = closingStockValue;
	};

	this.ItemWiseProfitAndLossReportObject = function () {};

	this.getOpeningStockValue = function () {
		return this.openingStockValue;
	};

	this.getClosingStockValue = function () {
		return this.closingStockValue;
	};

	this.isActiveRow = function () {
		return this.activeRow;
	};

	this.setActiveRow = function (activeRow) {
		this.activeRow = activeRow;
	};

	this.getItemValues = function () {
		return this.itemValues;
	};

	this.setItemValues = function (itemValues) {
		this.itemValues = itemValues;
	};

	this.getNetProfitAndLossAmount = function () {
		return this.netProfitAndLossAmount;
	};

	this.clearItemValues = function () {
		this.itemValues = {};
	};

	this.calculateProfitAndLossAmount = function () {
		this.netProfitAndLossAmount = MyDouble.getAmountWithDecimal(this.closingStockValue - this.openingStockValue + (this.saleValue - this.saleReturnValue) - (this.purchaseValue - this.purchaseReturnValue) + this.receivableTax - this.payableTax);
	};

	this.getReceivableTax = function () {
		return this.receivableTax;
	};

	this.getPayableTax = function () {
		return this.payableTax;
	};

	this.setReceivableTax = function (receivableTax) {
		this.receivableTax = receivableTax;
	};

	this.setPayableTax = function (payableTax) {
		this.payableTax = payableTax;
	};
};

module.exports = ItemWiseProfitAndLossReportObject;