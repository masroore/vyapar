var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _create = require('babel-runtime/core-js/object/create');

var _create2 = _interopRequireDefault(_create);

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _assign = require('babel-runtime/core-js/object/assign');

var _assign2 = _interopRequireDefault(_assign);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function () {
	var fs = require('fs');

	var app = require('electron').remote.app;

	var idb = require('idb');
	var appPath = app.getPath('userData');
	var SettingCache = require('./../Cache/SettingCache.js');
	var FirmCache = require('./../Cache/FirmCache.js');
	var settingCache = new SettingCache();
	var firmCache = new FirmCache();
	var firmObj = firmCache.getDefaultFirm();
	var StringConstants = require('../Constants/StringConstants');
	var logger = require('../Utilities/logger');

	//   const MessageConfig = require('./../Cache/MessageCache');
	//   const messageConfig = new MessageConfig();
	var AuthHelper = require('./../Utilities/TokenForSync');
	var Domain = require('./../Constants/Domain');

	var domain = Domain.thisDomain;
	var MessageFormatter = require('./../BizLogic/TxnMessageFormatter');
	var messageFormatter = new MessageFormatter();

	var companyListUtility = require('../Utilities/CompanyListUtility');

	var companyNameKey = companyListUtility.getCurrentCompanyKey();

	var draftFile = appPath + '/BusinessNames/draft-messages.json';

	var txnTypeConstants = require('../Constants/TxnTypeConstant');
	var db = void 0;
	var currentDBVersion = 1;
	var numberRegex = StringConstants.numberRegex;
	var tableName = 'messages';
	var paymentReminderType = 'paymentReminder';
	idb.openDB(companyNameKey, currentDBVersion, {
		upgrade: function upgrade(db, oldVersion, newVersion) {
			switch (oldVersion) {
				case 0:
					// Create a store of objects
					if (!db.objectStoreNames.contains(tableName)) {
						var objectDb = db.createObjectStore(tableName, {
							// The 'id' property of the object will be the key.
							keyPath: 'id',
							// If it isn't explicitly set, create a value by auto incrementing.
							autoIncrement: true
						});

						if (fs.existsSync(draftFile)) {
							try {
								var draft = void 0;
								try {
									draft = JSON.parse(fs.readFileSync(draftFile, 'utf8'));
									if ((0, _keys2.default)(draft).length === 0) {
										fs.unlinkSync(draftFile);
										return;
									}
								} catch (e) {
									fs.unlinkSync(draftFile);
									return;
								}
								var current = draft[companyNameKey];
								if (current) {
									for (var key in current) {
										var msg = current[key];

										var _key$split = key.split('-'),
										    _key$split2 = (0, _slicedToArray3.default)(_key$split, 2),
										    type = _key$split2[1];

										objectDb.add((0, _extends3.default)({}, msg, { type: type }));
										// allRequests.push(draftLogic.saveInDraft(type, msg));
									}

									var newDraft = (0, _assign2.default)({}, draft);
									delete newDraft[companyNameKey];
									fs.writeFileSync(draftFile, (0, _stringify2.default)(newDraft), 'utf-8');
								}
							} catch (e) {
								logger.error(e);
							}
						}
					}
			}
		}
	}).then(function (result) {
		db = result;
	});

	var DraftLogic = function () {
		function DraftLogic() {
			(0, _classCallCheck3.default)(this, DraftLogic);
		}

		(0, _createClass3.default)(DraftLogic, [{
			key: 'getAll',
			value: function getAll() {
				return new _promise2.default(function (resolve, reject) {
					var timer = setInterval(function () {
						if (db && db.version === currentDBVersion) {
							clearInterval(timer);
							db.getAll(tableName).then(function (messages) {
								return resolve(messages);
							});
						}
					}, 500);
				});
			}
		}, {
			key: 'getById',
			value: function getById(id) {
				return db.get(tableName, id);
			}
		}, {
			key: 'deleteById',
			value: function deleteById(id) {
				var _this = this;

				return db.delete(tableName, id).then(function () {
					return _this.updateCounter();
				});
			}
		}, {
			key: 'isInDraft',
			value: function isInDraft(id) {
				return db.get(tableName, id).then(function (record) {
					return record.id == id;
				});
			}
		}, {
			key: 'updateCounter',
			value: function updateCounter() {
				return db.count(tableName).then(function (total) {
					$(document).trigger('outboxCounter:updated', [total]);
				});
			}
		}, {
			key: 'saveInDraft',
			value: function saveInDraft(type, msg) {
				var _this2 = this;

				return db.add(tableName, (0, _extends3.default)({}, msg, { type: type })).then(function () {
					_this2.updateCounter();
				});
			}
		}, {
			key: 'sendTransactionSMSToPartyAndOwner',
			value: function sendTransactionSMSToPartyAndOwner(txnObject, canDisplaySuccessToast, canDisplayErrorToast) {
				if (settingCache.getTransactionMessageEnabled() && settingCache.isTxnMessageEnabledForTxn(txnObject.getTxnType())) {
					this.sendTransactionSMSToParty(txnObject, canDisplaySuccessToast, canDisplayErrorToast);
				}
				if (settingCache.isOwnerTxnMsgEnabled() && settingCache.isTxnMessageEnabledForTxn(txnObject.getTxnType())) {
					this.sendTransactionSMSToOwner(txnObject, canDisplaySuccessToast, canDisplayErrorToast);
				}
			}
		}, {
			key: 'sendTransactionSMSToParty',
			value: function sendTransactionSMSToParty(txnObject, canDisplaySuccessToast, canDisplayErrorToast, customPhoneNumber) {
				try {
					var msg = messageFormatter.createMessageForTxn(txnObject);
					if (msg) {
						var newMessage = (0, _assign2.default)({}, msg);
						newMessage.to = customPhoneNumber || txnObject.getNameRef().getPhoneNumber();
						newMessage.party_name = txnObject.getNameRef().getFullName();
						this.sendSMS({
							id: null,
							type: 'transaction',
							msg: newMessage,
							callback: null
						}, canDisplaySuccessToast, canDisplayErrorToast);
					}
				} catch (err) {
					logger.error(err);
				}
			}
		}, {
			key: 'sendTransactionSMSToOwner',
			value: function sendTransactionSMSToOwner(txnObject, canDisplaySuccessToast, canDisplayErrorToast) {
				try {
					var msg = messageFormatter.createMessageForTxn(txnObject);
					var ownerNumber = settingCache.getOwnerTxnMsgPhoneNumber();
					if (msg && ownerNumber) {
						var newMessage = (0, _assign2.default)({}, msg);
						newMessage.to = ownerNumber;
						newMessage.party_name = txnObject.getNameRef().getFullName();
						newMessage.header = txnTypeConstants.getTxnTypeForUI(txnObject.getTxnType());
						this.sendSMS({
							id: null,
							type: 'owner',
							msg: newMessage,
							callback: null
						}, canDisplaySuccessToast, canDisplayErrorToast);
					}
				} catch (err) {
					logger.error(err);
				}
			}
		}, {
			key: 'sendPaymentReminderMessage',
			value: function sendPaymentReminderMessage(partyObject, type) {
				var _this3 = this;

				var canDisplaySuccessToast = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
				var canDisplayErrorToast = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;

				var isOnline = require('is-online');
				isOnline({ timeout: 2000 }).then(function (online) {
					if (!online) {
						canDisplayErrorToast && ToastHelper.error('SMS Failed. Please check your internet connection.');
						return;
					}
					try {
						var msg = messageFormatter.createMessageForPaymentReminder(partyObject.getAmount());
						if (msg) {
							var newMessage = (0, _assign2.default)({}, msg);
							newMessage.to = partyObject.getPhoneNumber();
							newMessage.party_name = partyObject.getFullName();
							newMessage.header = '';
							if (type == 'sms') {
								_this3.sendSMS({
									id: null,
									type: paymentReminderType,
									msg: newMessage
								}, canDisplaySuccessToast, canDisplayErrorToast);
							} else if (type == 'whatsapp') {
								_this3.sendWhatsApp(partyObject.getNameId() + '-paymentReminder-' + partyObject.getAmount(), newMessage);
							}
						}
					} catch (err) {
						logger.error(err);
					}
				});
			}
		}, {
			key: 'sendBulkSMS',
			value: function sendBulkSMS(partyObjects, _ref) {
				var _this4 = this;

				var _ref$bulkMessageType = _ref.bulkMessageType,
				    bulkMessageType = _ref$bulkMessageType === undefined ? '' : _ref$bulkMessageType,
				    _ref$catalogueURL = _ref.catalogueURL,
				    catalogueURL = _ref$catalogueURL === undefined ? '' : _ref$catalogueURL;

				var partyObjsWithValidPhoneNumber = partyObjects.filter(function (partyObj) {
					var phoneNumber = partyObj.getPhoneNumber() || '';
					phoneNumber = phoneNumber.split(/[ ,-]/).join('').slice(-10);
					if (isNaN(Number(phoneNumber)) || phoneNumber.length < 10) {
						return false;
					}
					return true;
				});
				var sentCount = 0;
				var invalidPhoneNumberStr = 'SMS Failed: Invalid Phone number in party';
				var errorCountMap = (0, _create2.default)(null);
				function msgCallBack() {
					var err = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
					var status = arguments[1];
					var message = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'SMS Failed';

					if (status) {
						sentCount++;
					} else {
						errorCountMap[message] = (errorCountMap[message] || 0) + 1;
					}
				}
				function displayToasts() {
					if (sentCount) {
						ToastHelper.success((sentCount == 1 ? '' : sentCount) + ' SMS' + (sentCount > 1 ? 's have' : ' has') + ' been sent successfully!');
					}
					for (var message in errorCountMap) {
						var count = errorCountMap[message];
						count && ToastHelper.error((count || '') + ' ' + message);
					}
				}
				if (partyObjsWithValidPhoneNumber.length < partyObjects.length) {
					errorCountMap[invalidPhoneNumberStr] = partyObjects.length - partyObjsWithValidPhoneNumber.length;
				}
				if (partyObjsWithValidPhoneNumber.length) {
					var promises = partyObjsWithValidPhoneNumber.map(function (partyObj) {
						var msg = '';
						switch (bulkMessageType) {
							case 'paymentReminder':
								msg = messageFormatter.createMessageForPaymentReminder(partyObj.getAmount());
								break;
							case 'catalogue':
								msg = messageFormatter.createMessageForCatalogue(catalogueURL);
								break;
							default:
								msg = messageFormatter.createMessageForPaymentReminder(partyObj.getAmount());
								break;
						}
						var newMessage = (0, _assign2.default)({}, msg);
						newMessage.to = partyObj.getPhoneNumber();
						newMessage.party_name = partyObj.getFullName();
						newMessage.header = '';
						return _this4.sendSMS({
							id: null,
							type: paymentReminderType,
							msg: newMessage,
							callback: msgCallBack
						}, false, false);
					});
					_promise2.default.all(promises).then(displayToasts);
				} else {
					displayToasts();
				}
			}
		}, {
			key: 'sendWhatsApp',
			value: function sendWhatsApp(id, msg, callback) {
				if (!msg.to) {
					ToastHelper.error('Recipient number required.');
					return false;
				}
				if (msg.branding) {
					msg.footer = msg.footer ? msg.footer + '\n' + msg.branding : msg.branding;
				}
				var send = require('../Utilities/whatsapp').default;
				msg.to = (msg.to || '').replace(/[^0-9]/g, '');
				if (settingCache.isCurrentCountryIndia()) {
					msg.to = '91' + msg.to.slice(-10);
				}
				send(msg, callback);
			}
		}, {
			key: 'sendSMS',
			value: function () {
				var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(_ref3) {
					var id = _ref3.id,
					    type = _ref3.type,
					    msg = _ref3.msg,
					    callback = _ref3.callback;

					var _this5 = this;

					var canDisplaySuccessToast = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
					var canDisplayErrorToast = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
					var defaultSenderId, security, body, message, token, tokenDetail, data, apiUrl, headers, DeviceHelper, deviceInfo, senderPhoneNumber, senderEmail;
					return _regenerator2.default.wrap(function _callee3$(_context3) {
						while (1) {
							switch (_context3.prev = _context3.next) {
								case 0:
									defaultSenderId = settingCache.getTxnMsgSenderId();
									security = '‌​​​‌​​‌‍‌​​​​‌‌​‍‌​​‌‌‌‌​‍‌​​​‌‌‌‌‍‌​​‌‌‌‌​‍‌​​​‌‌​‌';

									msg.to = (msg.to || '').replace(/[^0-9]/, '').slice(-12);

									if (msg.to) {
										_context3.next = 6;
										break;
									}

									canDisplayErrorToast && ToastHelper.error('SMS Failed: Recipient number required.');
									return _context3.abrupt('return', false);

								case 6:
									if (!(msg.to.length < 10)) {
										_context3.next = 9;
										break;
									}

									canDisplayErrorToast && ToastHelper.error('SMS Failed: Phone number is not valid.');
									return _context3.abrupt('return', false);

								case 9:
									body = msg.message;

									if (type == 'owner') {
										body = '\nName: ' + msg.party_name + '\n' + msg.message;
									}
									message = '' + security + msg.header + '\n' + body + '\n' + msg.footer;
									token = void 0;
									tokenDetail = AuthHelper.readToken();

									if (tokenDetail) {
										token = tokenDetail.auth_token;
									}

									data = {};
									apiUrl = void 0;
									headers = {
										'cache-control': 'no-cache'
									};

									if (token) {
										headers.authorization = 'Bearer ' + token;
										apiUrl = domain + '/api/sms/send';
										data = {
											'party_name': msg.party_name,
											'message': message,
											'recipient': msg.to,
											'type': type,
											'sender_id': defaultSenderId
										};
									} else {
										DeviceHelper = require('../Utilities/deviceHelper');
										deviceInfo = DeviceHelper.getDeviceInfo();
										senderPhoneNumber = firmObj.getFirmPhone();
										senderEmail = firmObj.getFirmEmail();

										msg.to = '91' + (msg.to || '').replace(/[^0-9]/, '').slice(-10);
										if (senderPhoneNumber && settingCache.getCountryCode().toUpperCase() === 'IN') {
											senderPhoneNumber = '91' + (senderPhoneNumber || '').replace(/[^0-9]/, '').slice(-10);
										}
										if (type != 'owner' && msg.branding) {
											message = '' + message + (msg.footer ? '\n' : '') + msg.branding;
										}
										apiUrl = domain + '/api/mobile/send/sms';
										data = {
											device_id: deviceInfo.deviceId,
											sender_phone: senderPhoneNumber,
											sender_email: senderEmail,
											msg_list: [{ id: 1, phone: msg.to, msg: message }]
										};
									}

									_context3.next = 21;
									return $.post({
										'url': apiUrl,
										'timeout': 10000,
										'data': data,
										'headers': headers,
										success: function () {
											var _ref4 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(data) {
												var successMsg;
												return _regenerator2.default.wrap(function _callee$(_context) {
													while (1) {
														switch (_context.prev = _context.next) {
															case 0:
																if (data.code == 201) {
																	canDisplaySuccessToast && ToastHelper.success('Sender Id not found. Please set sender id in SMS settings.');
																} else if (data.code == 202) {
																	canDisplaySuccessToast && ToastHelper.success('Sender id(' + defaultSenderId + ') is not approved.');
																} else {
																	successMsg = type == paymentReminderType ? 'Payment reminder sms sent successfully.' : 'SMS has been sent successfully.';

																	canDisplaySuccessToast && ToastHelper.success(successMsg);
																}
																_context.t0 = id;

																if (!_context.t0) {
																	_context.next = 6;
																	break;
																}

																_context.next = 5;
																return _this5.isInDraft(id);

															case 5:
																_context.t0 = _context.sent;

															case 6:
																if (!_context.t0) {
																	_context.next = 8;
																	break;
																}

																_this5.deleteById(id);

															case 8:
																if (callback) {
																	callback(null, true);
																}

															case 9:
															case 'end':
																return _context.stop();
														}
													}
												}, _callee, _this5);
											}));

											return function success(_x8) {
												return _ref4.apply(this, arguments);
											};
										}(),
										error: function () {
											var _ref5 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(xhr, textStatus) {
												var message;
												return _regenerator2.default.wrap(function _callee2$(_context2) {
													while (1) {
														switch (_context2.prev = _context2.next) {
															case 0:
																message = 'SMS for transaction sent failed. SMS saved in draft.';


																if (xhr.status == 421) {
																	message = 'SMS Failed: Insufficient balance to send owner sms.';
																	if (type == paymentReminderType) {
																		message = 'Payment Reminder ' + message;
																	}
																} else if (xhr.status == 422) {
																	message = 'Invalid Message';
																} else if (xhr.status == 401) {
																	message = 'SMS Failed: Invalid User. SMS saved in draft. Please login and resend.';
																	if (type == paymentReminderType) {
																		message = 'Payment Reminder ' + message;
																	}
																} else if (xhr.status == 0) {
																	message = 'SMS Failed. Please check your internet connection. SMS saved in draft.';
																	if (type == paymentReminderType) {
																		message = 'Payment Reminder ' + message;
																	}
																}
																canDisplayErrorToast && ToastHelper.error(message);
																_context2.t0 = !id;

																if (_context2.t0) {
																	_context2.next = 8;
																	break;
																}

																_context2.next = 7;
																return _this5.isInDraft(id);

															case 7:
																_context2.t0 = !_context2.sent;

															case 8:
																if (!_context2.t0) {
																	_context2.next = 12;
																	break;
																}

																msg.created = new Date().toISOString();
																_context2.next = 12;
																return _this5.saveInDraft(type, msg);

															case 12:
																if (callback) {
																	callback(null, false, message);
																}

															case 13:
															case 'end':
																return _context2.stop();
														}
													}
												}, _callee2, _this5);
											}));

											return function error(_x9, _x10) {
												return _ref5.apply(this, arguments);
											};
										}()
									});

								case 21:
								case 'end':
									return _context3.stop();
							}
						}
					}, _callee3, this);
				}));

				function sendSMS(_x5) {
					return _ref2.apply(this, arguments);
				}

				return sendSMS;
			}()
		}]);
		return DraftLogic;
	}();

	;
	return DraftLogic;
}();