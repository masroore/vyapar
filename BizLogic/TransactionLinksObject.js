/**
 * Created by Ashish on 2/21/2018.
 */
var TransactionLinksObject = function TransactionLinksObject() {
	this.txnLinksId;
	this.txnLinksTxn1Id;
	this.txnLinksTxn2Id;
	this.txnLinksAmount;
	this.txnLinksTxn1Type;
	this.txnLinksTxn2Type;

	this.getTxnLinksId = function () {
		return this.txnLinksId;
	};
	this.setTxnLinksId = function (txnLinksId) {
		this.txnLinksId = txnLinksId;
	};

	this.getTxnLinksTxn1Id = function () {
		return this.txnLinksTxn1Id;
	};
	this.setTxnLinksTxn1Id = function (txnLinksTxn1Id) {
		this.txnLinksTxn1Id = txnLinksTxn1Id;
	};

	this.getTxnLinksTxn2Id = function () {
		return this.txnLinksTxn2Id;
	};
	this.setTxnLinksTxn2Id = function (txnLinksTxn2Id) {
		this.txnLinksTxn2Id = txnLinksTxn2Id;
	};

	this.getTxnLinksAmount = function () {
		return this.txnLinksAmount;
	};
	this.setTxnLinksAmount = function (txnLinksAmount) {
		this.txnLinksAmount = txnLinksAmount;
	};

	this.getTxnLinksTxn1Type = function () {
		return this.txnLinksTxn1Type;
	};
	this.setTxnLinksTxn1Type = function (txnLinksTxn1Type) {
		this.txnLinksTxn1Type = txnLinksTxn1Type;
	};

	this.getTxnLinksTxn2Type = function () {
		return this.txnLinksTxn2Type;
	};
	this.setTxnLinksTxn2Type = function (txnLinksTxn2Type) {
		this.txnLinksTxn2Type = txnLinksTxn2Type;
	};

	this.isTransactionLinked = function (txnId) {
		var TransactionLinksModel = require('./../Models/TransactionLinksModel.js');
		var transactionLinksModel = new TransactionLinksModel();

		var statusCode = transactionLinksModel.isTransactionLinked(txnId);

		return statusCode;
	};
};

module.exports = TransactionLinksObject;