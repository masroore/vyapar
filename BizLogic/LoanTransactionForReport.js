var LoanTransactionForReport = function LoanTransactionForReport() {
	this.setCashAmount = function (cashAmount) {
		if (!cashAmount) {
			this.cashAmount = 0;
		} else if (!isNaN(Number(cashAmount))) {
			this.cashAmount = Number(cashAmount);
		} else {
			return 'ERROR_TXN_INVALID_AMOUNT';
		}
		return 'SUCCESS';
	};

	this.getCashAmount = function () {
		return this.cashAmount || 0;
	};

	this.getDescription = function () {
		return this.description || '';
	};

	this.setDescription = function (description) {
		this.description = description;
	};

	this.getTransactionMessage = function () {
		return 'Loan related';
	};
};

module.exports = LoanTransactionForReport;