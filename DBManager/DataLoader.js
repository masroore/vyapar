var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var sqlitedbhelper, NameLogic, ItemLogic;
var DataLoader = function DataLoader() {
	var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
	var TaxCodeConstants = require('./../Constants/TaxCodeConstants.js');
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant');
	if (!sqlitedbhelper) {
		sqlitedbhelper = new SqliteDBHelper();
	}
	if (!NameLogic) {
		NameLogic = require('./../BizLogic/nameLogic.js');
	}
	if (!ItemLogic) {
		ItemLogic = require('./../BizLogic/ItemLogic.js');
	}

	this.isPrefixExist = function (prefixVal, txnType, firmId) {
		return sqlitedbhelper.isPrefixExist(prefixVal, txnType, firmId);
	};

	this.isPrefixUsedInTxn = function (prefixId) {
		return sqlitedbhelper.isPrefixUsedInTxn(prefixId);
	};

	this.loadAllPaymentTerms = function () {
		var PaymentTermLogic = require('./../BizLogic/PaymentTermLogic.js');
		var paymentTermModelList = sqlitedbhelper.fetchAllPaymentTerms();
		return paymentTermModelList.map(function (data) {
			var paymentTermObj = new PaymentTermLogic();
			var paymentTermModel = data;
			paymentTermObj.setPaymentTermId(Number(paymentTermModel.getPaymentTermId()));
			paymentTermObj.setPaymentTermName(paymentTermModel.getPaymentTermName());
			paymentTermObj.setPaymentTermDays(paymentTermModel.getPaymentTermDays());
			paymentTermObj.setPaymentTermIsDefault(paymentTermModel.getPaymentTermIsDefault());
			return paymentTermObj;
		});
	};

	this.LoadAllNames = function () {
		var nameLists = {};
		var nameModelList = sqlitedbhelper.fetchAllNameRecords();
		// console.log(nameModelList);
		var len = nameModelList.length;
		// console.log(len);
		for (var i = 0; i < len; i++) {
			var name = new NameLogic();
			// console.log(nameModelList[i]);
			var nameModel = nameModelList[i];
			name.setFullName(nameModel.full_name);
			name.setNameId(nameModel.name_id);
			name.setEmail(nameModel.email);
			name.setPhoneNumber(nameModel.phone_number);
			name.setAmount(nameModel.amount);
			name.setAddress(nameModel.address);
			name.setNameType(nameModel.nameType);
			name.setGroupId(nameModel.groupId);
			name.setOpeningDate(nameModel.get_opening_date());
			name.setTinNumber(nameModel.tinNumber);
			name.setContactState(nameModel.contact_state);
			name.setGstinNumber(nameModel.gstin_number);
			name.setShippingAddress(nameModel.get_shipping_address());
			name.setCustomerType(nameModel.getCustomerType());
			name.setUdfObjectArray(nameModel.getUdfObjectArray());
			name.setNameLastTxnDate(nameModel.getNameLastTxnDate());
			nameLists[name.getNameId()] = name;
		}
		return nameLists;
	};

	this.getItemStockQuantityFromDbByItemId = function (itemId) {
		var itemStockQuantity = sqlitedbhelper.getItemStockQuantityFromDbByItemId(itemId);
		return itemStockQuantity;
	};

	this.getPartyBalanceAmountById = function (nameId) {
		var partyBalance = sqlitedbhelper.getPartyBalanceAmountById(nameId);
		return partyBalance;
	};

	this.LoadMessageConfigs = function () {
		var messageModels = sqlitedbhelper.fetchMessageConfig();
		var TransactionMessageConfig = require('../BizLogic/TransactionMessageConfig');
		var msgConfigLogicList = [];
		messageModels.forEach(function (field) {
			msgConfigLogicList.push(new TransactionMessageConfig(field.txnType, field.txnFieldId, field.txnFieldName, field.txnFieldValue));
		});
		return msgConfigLogicList;
	};

	this.getMaxRefNumber = function (firmId, txnType, prefixId) {
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.getMaxRefNumber(firmId, txnType, prefixId);
	};

	this.getTransactionCount = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.getTransactionCount();
	};

	this.getTransactionCountByTxnType = function (txnType) {
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.getTransactionCountByTxnType(txnType);
	};

	this.getPartyCount = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.getPartyCount();
	};

	this.getImageCount = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.getImageCount();
	};

	this.getFirstTransactionDate = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.getFirstTransactionDate();
	};

	this.getLastTransactionDate = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.getLastTransactionDate();
	};

	this.getAmountRelatedDataForUsageDetails = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.getAmountRelatedDataForUsageDetails();
	};

	this.getLoginTime = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.getLoginTime();
	};

	this.getOpeningBalanceTransactionOnNameId = function (nameId) {
		var txnOpeningBalanceModel = sqlitedbhelper.getOpeningBalanceTransactionOnNameId(nameId);
		if (txnOpeningBalanceModel) {
			var transactionObj = convertDataFromTransactionModelToTransactionObject(txnOpeningBalanceModel);
			transactionObj.setTransactionTaxId(0);
			return transactionObj;
		} else {
			return null;
		}
	};

	this.getExpectedNameBalance = function () {
		var endDate = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

		return sqlitedbhelper.getExpectedNameBalance(endDate);
	};

	this.getPartyReportList = function (endDate) {
		var returnNameList = [];
		var nameListBalance = sqlitedbhelper.getExpectedNameBalance(endDate);
		var nameList = nameCache.getListOfNamesObject();
		for (var i = 0; i < nameList.length; i++) {
			var nameObj = nameList[i];
			var name = new NameLogic();
			name.setNameId(nameObj.getNameId());
			name.setNameLastTxnDate(nameObj.getNameLastTxnDate());
			name.setOpeningBalance(nameObj.getOpeningBalance());
			name.setCustomerType(nameObj.getCustomerType());
			name.setGstinNumber(nameObj.getGstinNumber());
			name.setContactState(nameObj.getContactState());
			name.setTinNumber(nameObj.getTinNumber());
			name.setGroupId(nameObj.getGroupId());
			name.setOpeningDate(nameObj.getOpeningDate());
			name.setFullName(nameObj.getFullName());
			name.setPhoneNumber(nameObj.getPhoneNumber());
			name.setEmail(nameObj.getEmail());
			name.setNameType(nameObj.getNameType());
			name.setIsReceivable(nameObj.getIsReceivable());
			name.setShippingAddress(nameObj.getShippingAddress());
			name.setAddress(nameObj.getAddress());
			name.setAmount(nameListBalance.get(Number(nameObj.getNameId())) || 0);
			returnNameList.push(name);
		}
		return returnNameList;
	};

	this.getAdjObjForOpening = function (itemId) {
		var sqliteDBHelper = new SqliteDBHelper();
		var adjModel = sqliteDBHelper.getAdjObjForOpening(itemId);
		return adjModel;
	};

	this.LoadAllExpense = function () {
		var nameLists = {};
		var nameModelList = sqlitedbhelper.fetchAllExpenseRecords();
		var len = nameModelList.length;
		// console.log(len);
		for (var i = 0; i < len; i++) {
			var name = new NameLogic();
			name.setFullName(nameModelList[i].full_name);
			name.setNameId(nameModelList[i].name_id);
			name.setEmail(nameModelList[i].email);
			name.setPhoneNumber(nameModelList[i].phone_number);
			name.setAmount(nameModelList[i].amount);
			name.setAddress(nameModelList[i].address);
			name.setNameType(nameModelList[i].nameType);
			name.setGroupId(nameModelList[i].groupId);
			name.setOpeningDate(nameModelList[i].get_opening_date());
			name.setTinNumber('');
			name.setContactState('');
			name.setGstinNumber('');
			name.setShippingAddress('');
			name.setNameLastTxnDate(nameModelList[i].getNameLastTxnDate());
			nameLists[name.getNameId()] = name;
		}
		return nameLists;
	};

	this.LoadAllIncome = function () {
		var nameLists = {};
		var nameModelList = sqlitedbhelper.fetchAllIncomeRecords();
		var len = nameModelList.length;
		// console.log(len);
		for (var i = 0; i < len; i++) {
			var name = new NameLogic();
			name.setFullName(nameModelList[i].full_name);
			name.setNameId(nameModelList[i].name_id);
			name.setEmail(nameModelList[i].email);
			name.setPhoneNumber(nameModelList[i].phone_number);
			name.setAmount(nameModelList[i].amount);
			name.setAddress(nameModelList[i].address);
			name.setNameType(nameModelList[i].nameType);
			name.setGroupId(nameModelList[i].groupId);
			name.setOpeningDate(nameModelList[i].get_opening_date());
			name.setTinNumber('');
			name.setContactState('');
			name.setGstinNumber('');
			name.setShippingAddress('');
			name.setNameLastTxnDate(nameModelList[i].getNameLastTxnDate());
			nameLists[name.getNameId()] = name;
		}
		return nameLists;
	};

	this.LoadAllServiceItem = function () {
		var serviceLists = {};
		var itemModelList = sqlitedbhelper.fetchAllServiceRecords();
		var len = itemModelList.length;
		for (var i = 0; i < len; i++) {
			var item = new ItemLogic();
			item.setItemId(Number(itemModelList[i]['itemId']));
			item.setItemName(itemModelList[i]['itemName']);
			item.setItemSaleUnitPrice(itemModelList[i]['itemSaleUnitPrice']);
			item.setItemPurchaseUnitPrice(itemModelList[i]['itemPurchaseUnitPrice']);
			item.setItemMinStockQuantity(itemModelList[i]['itemMinStockQuantity']);
			item.setItemLocation(itemModelList[i]['itemLocation']);
			item.setItemStockQuantity(itemModelList[i]['itemStockQuantity']);
			item.setItemStockValue(itemModelList[i]['itemStockValue']);
			item.setItemType(itemModelList[i]['itemType']);
			item.setItemCategoryId(itemModelList[i]['itemCategoryId']);
			item.setBaseUnitId(itemModelList[i]['baseUnitId']);
			item.setSecondaryUnitId(itemModelList[i]['secondaryUnitId']);
			item.setUnitMappingId(itemModelList[i]['unitMappingId']);
			item.setItemCode(itemModelList[i]['itemCode']);
			item.setItemHSNCode(itemModelList[i].getItemHSNCode());
			item.setItemOpeningDate(itemModelList[i].getItemOpeningDate());
			item.setItemOpeningStock(itemModelList[i].getItemOpeningStock());
			item.setItemTaxId(itemModelList[i].getItemTaxId());
			item.setItemTaxTypeSale(itemModelList[i].getItemTaxTypeSale());
			item.setItemTaxTypePurchase(itemModelList[i].getItemTaxTypePurchase());
			item.setItemAdditionalCESS(itemModelList[i].getItemAdditionalCESS());
			item.setItemDescription(itemModelList[i].getItemDescription());
			item.setItemAtPrice(itemModelList[i].getItemAtPrice());
			item.setIsItemActive(itemModelList[i].getIsItemActive());
			serviceLists[item.getItemId()] = item;
		}
		return serviceLists;
	};

	this.getItemDetailReportData = function (itemId) {
		var ItemDetailReportObjectModelList = sqlitedbhelper.getItemDetailReportData(itemId);
		var ItemDetailReportObjectList = [];
		if (ItemDetailReportObjectModelList != null && ItemDetailReportObjectModelList.length > 0) {
			for (var i = 0; i < ItemDetailReportObjectModelList.length; i++) {
				var txn = ItemDetailReportObjectModelList[i];
				var ItemDetailReportObject = require('./../BizLogic/ItemDetailReportObject.js');
				var itemDetailReportObject = new ItemDetailReportObject();
				itemDetailReportObject.setDate(txn.getDate());
				itemDetailReportObject.setSaleQuantity(txn.getSaleQuantity());
				itemDetailReportObject.setSaleFreeQuantity(txn.getSaleFreeQuantity());
				itemDetailReportObject.setPurchaseQuantity(txn.getPurchaseQuantity());
				itemDetailReportObject.setPurchaseFreeQuantity(txn.getPurchaseFreeQuantity());
				itemDetailReportObject.setAdjustmentQuantity(txn.getAdjustmentQuantity());
				itemDetailReportObject.isForwardedStock(false);

				ItemDetailReportObjectList.push(itemDetailReportObject);
			}
		}
		return ItemDetailReportObjectList;
	};

	var sortOnBasisOfItemName = function sortOnBasisOfItemName(txnList) {
		if (txnList) {
			txnList.sort(function (lhs, rhs) {
				var firstTxn = lhs;
				var secondTxn = rhs;
				if (firstTxn.getItemName().toLowerCase() < secondTxn.getItemName().toLowerCase()) {
					return -1;
				}
				if (firstTxn.getItemName().toLowerCase() > secondTxn.getItemName().toLowerCase()) {
					return 1;
				}
				return 0;
			});
		}
	};

	this.getStockDetailReportData = function (itemCateGoryId, fromDate, endDate, showOnlyActiveItems) {
		var ItemCache = require('./../Cache/ItemCache.js');
		var StockDetailReportObject = require('./../BizLogic/StockDetailReportObject.js');
		var itemCache = new ItemCache();
		var txnList = sqlitedbhelper.getStockDetailReportData(fromDate, endDate);
		var stockReportDataList = [];

		for (var index = 0; index < txnList.length; index++) {
			var stockDetailReportObjectModel = txnList[index];
			var itemObj = itemCache.getItemById(stockDetailReportObjectModel.getItemId());
			var categoryIdFromCache = Number(itemObj.getItemCategoryId());
			if (showOnlyActiveItems & itemObj.getIsItemActive() != 1) continue;

			if (categoryIdFromCache == itemCateGoryId || itemCateGoryId == 0) {
				var stockDetailReportObject = new StockDetailReportObject();
				stockDetailReportObject.setItemName(itemCache.findItemNameById(stockDetailReportObjectModel.getItemId()));
				stockDetailReportObject.setOpeningQuantity(stockDetailReportObjectModel.getOpeningQuantity());
				stockDetailReportObject.setQuantityIn(stockDetailReportObjectModel.getQuantityIn());
				stockDetailReportObject.setQuantityOut(stockDetailReportObjectModel.getQuantityOut());

				stockReportDataList.push(stockDetailReportObject);
			}
		}
		sortOnBasisOfItemName(stockReportDataList);
		return stockReportDataList;
	};

	this.LoadAllItems = function () {
		var itemLists = {};
		var itemModelList = sqlitedbhelper.fetchAllItemRecords();
		var reservedQtyMap = sqlitedbhelper.getReservedQtyForItems();
		var len = itemModelList.length;
		// console.log(len);
		for (var i = 0; i < len; i++) {
			var itemModel = itemModelList[i];
			var item = new ItemLogic();
			item.setItemId(Number(itemModel['itemId']));
			item.setItemName(itemModel['itemName']);
			item.setItemSaleUnitPrice(itemModel['itemSaleUnitPrice']);
			item.setItemPurchaseUnitPrice(itemModel['itemPurchaseUnitPrice']);
			item.setItemMinStockQuantity(itemModel['itemMinStockQuantity']);
			item.setItemLocation(itemModel['itemLocation']);
			item.setItemStockQuantity(itemModel['itemStockQuantity']);
			item.setItemStockValue(itemModel['itemStockValue']);
			item.setItemType(itemModel['itemType']);
			item.setItemCategoryId(itemModel['itemCategoryId']);
			item.setBaseUnitId(itemModel['baseUnitId']);
			item.setSecondaryUnitId(itemModel['secondaryUnitId']);
			item.setUnitMappingId(itemModel['unitMappingId']);
			item.setItemCode(itemModel['itemCode']);
			item.setItemHSNCode(itemModel.getItemHSNCode());
			item.setItemOpeningDate(itemModel.getItemOpeningDate());
			item.setItemOpeningStock(itemModel.getItemOpeningStock());
			item.setItemTaxId(itemModel.getItemTaxId());
			item.setItemTaxTypeSale(itemModel.getItemTaxTypeSale());
			item.setItemTaxTypePurchase(itemModel.getItemTaxTypePurchase());
			item.setItemAdditionalCESS(itemModel.getItemAdditionalCESS());
			item.setItemDescription(itemModel.getItemDescription());
			item.setItemAtPrice(itemModel.getItemAtPrice());
			item.setIsItemActive(itemModel.getIsItemActive());
			var reservedQty = reservedQtyMap.get(item.getItemId());
			item.setReservedQuantity(reservedQty || 0);
			itemLists[item.getItemId()] = item;
		}
		// console.log(itemLists);
		return itemLists;
	};

	this.getBankDetailList = function (bankId, descending) {
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqliteDBHelper = new SqliteDBHelper();
		var BankDetailObject = require('./../BizLogic/BankDetail.js');
		var bankDetailModelList = sqliteDBHelper.getBankDetailModelList(bankId);
		var bankDetailObjectList = [];
		if (bankDetailModelList && bankDetailModelList.length > 0) {
			$.each(bankDetailModelList, function (key, bankDetailModel) {
				var bankDetailObject = new BankDetailObject();
				bankDetailObject.setTxnId(bankDetailModel.getTxnId());
				bankDetailObject.setTxnType(bankDetailModel.getTxnType());
				bankDetailObject.setAmount(bankDetailModel.getAmount());
				bankDetailObject.setTxnDate(bankDetailModel.getTxnDate());
				bankDetailObject.setTxnModifiedDate(bankDetailModel.getTxnModifiedDate());
				bankDetailObject.setUserId(bankDetailModel.getUserId());
				bankDetailObject.setDescription(bankDetailModel.getDescription());
				bankDetailObject.setTxnDescription(bankDetailModel.getTxnDescription());
				bankDetailObject.setSubTxnType(bankDetailModel.getSubTxnType());
				bankDetailObject.setToBankId(bankDetailModel.getToBankId());
				bankDetailObject.setFromBankId(bankDetailModel.getFromBankId()); // Only used for bank to bank tranfer

				bankDetailObjectList.push(bankDetailObject);
			});
		}
		var bankDetailComparator = function bankDetailComparator(bankDetailA, bankDetailB) {
			var timeDiff = bankDetailB.getTxnDate().getTime() - bankDetailA.getTxnDate().getTime();
			if (!timeDiff) {
				timeDiff = bankDetailB.getTxnModifiedDate().getTime() - bankDetailA.getTxnModifiedDate().getTime();
			}
			return timeDiff;
		};
		bankDetailObjectList.sort(bankDetailComparator);
		return bankDetailObjectList;
	};

	this.getPaymentReminderNotificationString = function (numberOfDays) {
		return sqlitedbhelper.getPaymentReminderNotificationString(numberOfDays);
	};

	this.getRemindTodayList = function () {
		return sqlitedbhelper.getRemindTodayList();
	};

	this.sendSMSToday = function () {
		return sqlitedbhelper.sendSMSToday();
	};

	this.getPaymentReminderList = function () {
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqliteDBHelper = new SqliteDBHelper();
		var reminderList = sqliteDBHelper.getPaymentReminderList();

		var PaymentReminderObject = require('./../BizLogic/PaymentReminderObject.js');
		var paymentReminderObjectList = [];
		for (var x = 0; x < reminderList.length; x++) {
			var model = reminderList[x];
			var paymentReminderObject = new PaymentReminderObject();
			paymentReminderObject.setNameId(model.getNameId());
			paymentReminderObject.setName(model.getName());
			paymentReminderObject.setBalanceAmount(model.getBalanceAmount());
			paymentReminderObjectList.push(paymentReminderObject);
		}
		return paymentReminderObjectList;
	};

	this.getPaymentReminderStatus = function (nameId) {
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.getPaymentReminderStatus(nameId);
	};

	this.getItemDetailList = function (itemId) {
		var itemDetailModelList = sqlitedbhelper.getItemDetailModelList(itemId);
		var itemDetailObjectList = [];
		// var LineItemModel = require('./../Models/LineItemModel.js');
		if (itemDetailModelList.length > 0) {
			var ItemDetailObject = require('./../BizLogic/ItemDetailObject.js');
			var len = itemDetailModelList.length;
			for (var i = 0; i < len; i++) {
				var x = itemDetailModelList[i];
				var itemDetailObject = new ItemDetailObject();
				itemDetailObject.setTxnId(x.getTxnId());

				itemDetailObject.setTxnPaymentStatus(x.getPaymentStatus());
				itemDetailObject.setTxnType(x.getTxnType());
				itemDetailObject.setItemQuantity(x.getTxnQuantity());
				itemDetailObject.setTxnDate(x.getTxnDate());
				itemDetailObject.setUserId(x.getUserId());
				itemDetailObject.setPricePerUnit(x.getPricePerUnit());
				itemDetailObject.setDescription(x.getDescription());
				itemDetailObject.setMappingId(x.getMappingId());
				itemDetailObject.setLineItemUnitId(x.getLineItemUnitId());
				itemDetailObject.setItemFreeQuantity(x.getItemFreeQuantity());
				// if(x.getLineItemUnitId() && itemDetailObject.getTxnId()){
				//     itemDetailObject.setPricePerUnit(sqlitedbhelper.getLineItemPricePerUnit(x.getLineItemUnitId(), itemDetailObject.getTxnId()));
				// }
				itemDetailObjectList.push(itemDetailObject);
			}
		}
		return itemDetailObjectList;
	};

	this.getItemIdListOfCatalogue = function () {
		return sqlitedbhelper.getItemIdListOfCatalogue();
	};

	this.getCatagoryListOfCatalogue = function () {
		return sqlitedbhelper.getCatagoryListOfCatalogue();
	};

	this.getCatalogueItemsList = function (_ref) {
		var lastCatalogueId = _ref.lastCatalogueId,
		    itemIds = _ref.itemIds,
		    limit = _ref.limit,
		    catalogueItemCategoryName = _ref.catalogueItemCategoryName,
		    catalogueItemNameText = _ref.catalogueItemNameText;
		var isImageRequired = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

		var catalogueItemModelList = sqlitedbhelper.getCatalogueItemsModelList({ lastCatalogueId: lastCatalogueId, itemIds: itemIds, limit: limit, catalogueItemCategoryName: catalogueItemCategoryName, catalogueItemNameText: catalogueItemNameText });
		var catalogueItemList = [];
		if (catalogueItemModelList.length > 0) {
			var CatalogueItemLogic = require('./../BizLogic/CatalogueItemLogic');
			for (var i = 0; i < catalogueItemModelList.length; i++) {
				var catalogueItemModel = catalogueItemModelList[i];
				var catalogueItemLogic = new CatalogueItemLogic();
				catalogueItemLogic.setCatalogueItemId(catalogueItemModel.getCatalogueItemId());
				catalogueItemLogic.setItemId(catalogueItemModel.getItemId());
				catalogueItemLogic.setCatalogueItemCode(catalogueItemModel.getCatalogueItemCode());
				catalogueItemLogic.setCatalogueItemDescription(catalogueItemModel.getCatalogueItemDescription());
				catalogueItemLogic.setCatalogueItemId(catalogueItemModel.getCatalogueItemId());
				catalogueItemLogic.setCatalogueItemName(catalogueItemModel.getCatalogueItemName());
				catalogueItemLogic.setCatalogueItemSaleUnitPrice(catalogueItemModel.getCatalogueItemSaleUnitPrice());
				catalogueItemLogic.setCatalogueItemCategoryName(catalogueItemModel.getCatalogueItemCategoryName());

				if (isImageRequired) {
					var catalogueItemImages = this.getCatalogueItemImagesByCatalogueId(catalogueItemModel.getCatalogueItemId(), 1);
					var catalogueImageList = [];
					for (var j = 0; j < catalogueItemImages.length; j++) {
						var catalogueItemImage = catalogueItemImages[j];
						catalogueImageList.push(catalogueItemImage.getBase64Image());
					}
					catalogueItemLogic.setCatalogueItemImagesList(catalogueImageList);
				}
				catalogueItemList.push(catalogueItemLogic);
			}
		}
		return catalogueItemList;
	};

	this.isCatalogueItemExists = function (text, type) {
		return sqlitedbhelper.isCatalogueItemExists(text, type);
	};

	this.LoadAllItemExpense = function () {
		var expenseLists = {};
		var expenseModelList = sqlitedbhelper.fetchAllItemExpenseRecords();
		var len = expenseModelList.length;
		// console.log(len);
		for (var i = 0; i < len; i++) {
			var item = new ItemLogic();
			item.setItemId(Number(expenseModelList[i]['itemId']));
			item.setItemName(expenseModelList[i]['itemName']);
			item.setItemSaleUnitPrice(expenseModelList[i]['itemSaleUnitPrice']);
			item.setItemPurchaseUnitPrice(expenseModelList[i]['itemPurchaseUnitPrice']);
			item.setItemMinStockQuantity(expenseModelList[i]['itemMinStockQuantity']);
			item.setItemLocation(expenseModelList[i]['itemLocation']);
			item.setItemStockQuantity(expenseModelList[i]['itemStockQuantity']);
			item.setItemStockValue(expenseModelList[i]['itemStockValue']);
			item.setItemType(expenseModelList[i]['itemType']);
			expenseLists[item.getItemId()] = item;
		}
		return expenseLists;
	};

	this.LoadAllItemIncome = function () {
		var incomeLists = {};
		var incomeModelList = sqlitedbhelper.fetchAllItemIncomeRecords();
		var len = incomeModelList.length;
		// console.log(len);
		for (var i = 0; i < len; i++) {
			var item = new ItemLogic();
			item.setItemId(Number(incomeModelList[i]['itemId']));
			item.setItemName(incomeModelList[i]['itemName']);
			item.setItemSaleUnitPrice(incomeModelList[i]['itemSaleUnitPrice']);
			item.setItemPurchaseUnitPrice(incomeModelList[i]['itemPurchaseUnitPrice']);
			item.setItemMinStockQuantity(incomeModelList[i]['itemMinStockQuantity']);
			item.setItemLocation(incomeModelList[i]['itemLocation']);
			item.setItemStockQuantity(incomeModelList[i]['itemStockQuantity']);
			item.setItemStockValue(incomeModelList[i]['itemStockValue']);
			item.setItemType(incomeModelList[i]['itemType']);
			incomeLists[item.getItemId()] = item;
		}
		return incomeLists;
	};

	this.getPurchaseLineItemList = function (itemId, stockValue) {
		return sqlitedbhelper.getPurchaseLineItemList(itemId, stockValue);
	};

	this.LoadAllLineItems = function (transaction_id) {
		var lineItemList = [];
		var lineItemModels = sqlitedbhelper.getLineItemOnTransaction(transaction_id);
		var BaseLineItem = require('./../BizLogic/BaseLineItem.js');
		var ItemCache = require('./../Cache/ItemCache.js');
		var itemCache = new ItemCache();
		if (lineItemModels && lineItemModels.length > 0) {
			for (var i in lineItemModels) {
				var lineItemModel = lineItemModels[i];
				var lineItemObj = new BaseLineItem();
				lineItemObj.setTransactionId(transaction_id);
				lineItemObj.setItemName(itemCache.findItemNameById(lineItemModel.getItemId()));
				lineItemObj.setLineItemId(lineItemModel.getLineItemId());
				lineItemObj.setItemId(lineItemModel.getItemId());
				lineItemObj.setItemQuantity(lineItemModel.getItemQuantity());
				lineItemObj.setItemFreeQuantity(lineItemModel.getItemFreeQuantity());
				lineItemObj.setItemUnitPrice(lineItemModel.getItemUnitPrice());
				lineItemObj.setLineItemDiscountAmount(lineItemModel.getLineItemDiscountAmount());
				lineItemObj.setLineItemTaxAmount(lineItemModel.getLineItemTaxAmount());
				lineItemObj.setLineItemTotal(lineItemModel.getLineItemTotal());
				lineItemObj.setLineItemUnitId(lineItemModel.getLineItemUnitId());
				lineItemObj.setLineItemUnitMappingId(lineItemModel.getLineItemUnitMappingId());
				lineItemObj.setLineItemTaxId(lineItemModel.getLineItemTaxId());
				lineItemObj.setLineItemITCValue(lineItemModel.getLineItemITCValue());
				lineItemObj.setLineItemMRP(lineItemModel.getLineItemMRP() ? lineItemModel.getLineItemMRP() : '');
				lineItemObj.setLineItemSize(lineItemModel.getLineItemSize() ? lineItemModel.getLineItemSize() : '');
				lineItemObj.setLineItemBatchNumber(lineItemModel.getLineItemBatchNumber());
				lineItemObj.setLineItemExpiryDate(lineItemModel.getLineItemExpiryDate() ? lineItemModel.getLineItemExpiryDate() : '');
				lineItemObj.setLineItemManufacturingDate(lineItemModel.getLineItemManufacturingDate() ? lineItemModel.getLineItemManufacturingDate() : '');
				lineItemObj.setLineItemSerialNumber(lineItemModel.getLineItemSerialNumber());
				lineItemObj.setLineItemCount(lineItemModel.getLineItemCount());
				lineItemObj.setLineItemDescription(lineItemModel.getLineItemDescription());
				lineItemObj.setLineItemSize(lineItemModel.getLineItemSize());
				lineItemObj.setLineItemAdditionalCESS(lineItemModel.getLineItemAdditionalCESS());
				lineItemObj.setLineItemIstId(lineItemModel.getLineItemIstId());
				lineItemObj.setLineItemDiscountPercent(lineItemModel.getLineItemDiscountPercent());
				lineItemList.push(lineItemObj);
			}
		}
		return lineItemList;
	};

	this.isItemUnitUsedInLineItems = function () {
		return sqlitedbhelper.isItemUnitUsedInLineItems();
	};

	this.getListOfCheques = function () {
		var chequeModelList = sqlitedbhelper.getAllCheques();
		var listOfChequeObj = [];
		if (chequeModelList != null && chequeModelList.length > 0) {
			var len = chequeModelList.length;
			var Cheque = require('./../BizLogic/ChequeLogic.js');
			for (var i = 0; i < len; i++) {
				var cheque = new Cheque();
				var chequeModel = chequeModelList[i];
				cheque.setChequeId(chequeModel.getChequeId());
				cheque.setChequeTxnId(chequeModel.getChequeTxnId());
				cheque.setTransferDate(chequeModel.getTransferDate());
				cheque.setTransferredToAccount(chequeModel.getTransferredToAccount());
				cheque.setChequeCurrentStatus(chequeModel.getChequeCurrentStatus());
				cheque.setChequeCloseDescription(chequeModel.getChequeCloseDescription());
				cheque.setChequeCreationDate(chequeModel.getChequeCreationDate());
				cheque.setChequeModificationDate(chequeModel.getChequeModificationDate());
				cheque.setTxnDate(chequeModel.getTxnDate());
				cheque.setChequeAmount(chequeModel.getChequeAmount());
				cheque.setChequeTxnRefNo(chequeModel.getChequeTxnRefNo());
				cheque.setChequeTxnType(chequeModel.getChequeTxnType());
				cheque.setChequeNameId(chequeModel.getChequeNameId());
				cheque.setChequeClosedTxnRefId(chequeModel.getChequeClosedTxnRefId());
				listOfChequeObj.push(cheque);
			}
		}
		return listOfChequeObj;
	};

	this.loadChequeById = function (chequeId) {
		var chequeModel = sqlitedbhelper.loadChequeById(chequeId);
		var Cheque = require('./../BizLogic/ChequeLogic.js');
		var cheque = new Cheque();
		cheque.setChequeId(chequeModel.getChequeId());
		cheque.setChequeTxnId(chequeModel.getChequeTxnId());
		cheque.setTransferDate(chequeModel.getTransferDate());
		cheque.setTransferredToAccount(chequeModel.getTransferredToAccount());
		cheque.setChequeCurrentStatus(chequeModel.getChequeCurrentStatus());
		cheque.setChequeCloseDescription(chequeModel.getChequeCloseDescription());
		cheque.setChequeCreationDate(chequeModel.getChequeCreationDate());
		cheque.setChequeModificationDate(chequeModel.getChequeModificationDate());
		cheque.setTxnDate(chequeModel.getTxnDate());
		cheque.setChequeAmount(chequeModel.getChequeAmount());
		cheque.setChequeTxnRefNo(chequeModel.getChequeTxnRefNo());
		cheque.setChequeTxnType(chequeModel.getChequeTxnType());
		cheque.setChequeNameId(chequeModel.getChequeNameId());
		cheque.setChequeClosedTxnRefId(chequeModel.getChequeClosedTxnRefId());
		return cheque;
	};

	this.getOpenChequeStatus = function (endDate) {
		return sqlitedbhelper.getOpenChequeStatus(endDate);
	};

	this.getAdvanceAmountForOpenOrder = function (endDate) {
		return sqlitedbhelper.getAdvanceAmountForOpenOrder(endDate);
	};

	this.getItemStockTrackingReportData = function () {
		var itemStockTrackingList = sqlitedbhelper.getAllItemStockTrackingData();
		return itemStockTrackingList;
	};

	this.getBankBalanceForAllBanks = function (endDate) {
		return sqlitedbhelper.getBankBalanceForAllBanks(endDate);
	};

	function convertDataFromTransactionModelToTransactionObject(txnModel) {
		var TransactionFactory = require('./../BizLogic/TransactionFactory.js');
		var transactionfactory = new TransactionFactory();

		var transactionObj = transactionfactory.getTransactionObject(txnModel.get_txn_type());
		transactionObj.setTxnType(txnModel.get_txn_type());
		transactionObj.setTxnId(txnModel.get_txn_id());
		transactionObj.setImageId(txnModel.get_image_id());
		transactionObj.setTxnDate(txnModel.get_txn_date());
		transactionObj.setTxnDueDate(txnModel.get_txn_due_date());
		transactionObj.setImagePath(txnModel.get_image_path());
		transactionObj.setBalanceAmount(txnModel.get_balance_amount());
		transactionObj.setCashAmount(txnModel.get_cash_amount());
		transactionObj.setNameId(txnModel.get_txn_name_id());
		transactionObj.setDescription(txnModel.get_txn_description());
		transactionObj.setAc1Amount(txnModel.get_ac1_amount());
		transactionObj.setAc2Amount(txnModel.get_ac2_amount());
		transactionObj.setAc3Amount(txnModel.get_ac3_amount());
		transactionObj.setDiscountAmount(txnModel.get_discount_amount());
		transactionObj.setTaxAmount(txnModel.get_tax_amount());
		transactionObj.setTxnRefNumber(txnModel.get_txn_ref_no());
		transactionObj.setPaymentTypeId(txnModel.getPaymentTypeId());
		transactionObj.setPaymentTypeReference(txnModel.getPaymentTypeReference());
		transactionObj.setStatus(txnModel.get_txn_status());
		transactionObj.setFirmId(txnModel.get_firm_id());
		transactionObj.setTxnSubType(txnModel.get_txn_sub_type());
		transactionObj.setInvoicePrefix(txnModel.get_invoice_prefix());
		transactionObj.setTransactionTaxId(txnModel.get_transaction_tax_id());
		transactionObj.setCustomFields(txnModel.get_custom_fields());
		transactionObj.setDisplayName(txnModel.get_display_name());
		transactionObj.setReverseCharge(txnModel.get_reverse_charge());
		transactionObj.setPlaceOfSupply(txnModel.getPlaceOfSupply());
		transactionObj.setRoundOffValue(txnModel.getRoundOffValue());
		transactionObj.setTxnITCApplicable(txnModel.getTxnITCApplicable());
		transactionObj.setPODate(txnModel.getPODate());
		transactionObj.setPONumber(txnModel.getPONumber());
		transactionObj.setTxnReturnRefNumber(txnModel.getTxnReturnRefNumber());
		transactionObj.setTxnReturnDate(txnModel.getTxnReturnDate());
		transactionObj.setCreatedAt(txnModel.getCreatedAt());
		transactionObj.setEwayBillNumber(txnModel.getEwayBillNumber());
		transactionObj.setTxnCurrentBalanceAmount(txnModel.getTxnCurrentBalanceAmount());
		transactionObj.setTxnPaymentStatus(txnModel.getTxnPaymentStatus());
		transactionObj.setDiscountPercent(txnModel.get_discount_percent());
		transactionObj.setTxnPaymentTermId(txnModel.getTxnPaymentTermId());
		transactionObj.setUdfObjectArray(txnModel.getUdfObjectArray());
		transactionObj.setInvoicePrefixId(txnModel.getInvoicePrefixId());
		transactionObj.setBillingAddress(txnModel.getBillingAddress());
		transactionObj.setShippingAddress(txnModel.getShippingAddress());
		transactionObj.setTaxTypeInclusive(txnModel.getTaxTypeInclusive());
		return transactionObj;
	}

	this.getUneditablePaymentTermIds = function () {
		return sqlitedbhelper.getUneditablePaymentTermIds();
	};

	function getTransactionObjectList(txnModelList) {
		var TransactionList = [];
		var len = txnModelList.length;
		for (var txnmodel = 0; txnmodel < len; txnmodel++) {
			var txnModel = txnModelList[txnmodel];
			var transactionObj = convertDataFromTransactionModelToTransactionObject(txnModel);
			TransactionList.push(transactionObj);
		}
		return TransactionList;
	}

	this.loadTransactionsForBillToBill = function (firmId, nameId, transactionType, loadDataForBillToBill) {
		var TransactionList = [];
		if (nameId) {
			switch (transactionType) {
				case TxnTypeConstant.TXN_TYPE_CASHIN:
					var transactionTypes = [TxnTypeConstant.TXN_TYPE_SALE, TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN, TxnTypeConstant.TXN_TYPE_CASHOUT, TxnTypeConstant.TXN_TYPE_ROPENBALANCE];
					var txnModelListForBillToBill = sqlitedbhelper.getTransactionsForBillToBill(firmId, nameId, transactionTypes, loadDataForBillToBill);
					break;
				case TxnTypeConstant.TXN_TYPE_CASHOUT:
					var transactionTypes = [TxnTypeConstant.TXN_TYPE_PURCHASE, TxnTypeConstant.TXN_TYPE_SALE_RETURN, TxnTypeConstant.TXN_TYPE_CASHIN, TxnTypeConstant.TXN_TYPE_POPENBALANCE];
					var txnModelListForBillToBill = sqlitedbhelper.getTransactionsForBillToBill(firmId, nameId, transactionTypes, loadDataForBillToBill);
					break;
				case TxnTypeConstant.TXN_TYPE_SALE:
					var transactionTypes = [TxnTypeConstant.TXN_TYPE_PURCHASE, TxnTypeConstant.TXN_TYPE_SALE_RETURN, TxnTypeConstant.TXN_TYPE_CASHIN, TxnTypeConstant.TXN_TYPE_POPENBALANCE];
					var txnModelListForBillToBill = sqlitedbhelper.getTransactionsForBillToBill(firmId, nameId, transactionTypes, loadDataForBillToBill);
					break;
				case TxnTypeConstant.TXN_TYPE_PURCHASE:
					var transactionTypes = [TxnTypeConstant.TXN_TYPE_SALE, TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN, TxnTypeConstant.TXN_TYPE_CASHOUT, TxnTypeConstant.TXN_TYPE_ROPENBALANCE];
					var txnModelListForBillToBill = sqlitedbhelper.getTransactionsForBillToBill(firmId, nameId, transactionTypes, loadDataForBillToBill);
					break;
				case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
					var transactionTypes = [TxnTypeConstant.TXN_TYPE_SALE, TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN, TxnTypeConstant.TXN_TYPE_CASHOUT, TxnTypeConstant.TXN_TYPE_ROPENBALANCE];
					var txnModelListForBillToBill = sqlitedbhelper.getTransactionsForBillToBill(firmId, nameId, transactionTypes, loadDataForBillToBill);
					break;
				case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
					var transactionTypes = [TxnTypeConstant.TXN_TYPE_PURCHASE, TxnTypeConstant.TXN_TYPE_SALE_RETURN, TxnTypeConstant.TXN_TYPE_CASHIN, TxnTypeConstant.TXN_TYPE_POPENBALANCE];
					var txnModelListForBillToBill = sqlitedbhelper.getTransactionsForBillToBill(firmId, nameId, transactionTypes, loadDataForBillToBill);
					break;
				case TxnTypeConstant.TXN_TYPE_POPENBALANCE:
					var transactionTypes = [TxnTypeConstant.TXN_TYPE_SALE, TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN, TxnTypeConstant.TXN_TYPE_CASHOUT];
					var txnModelListForBillToBill = sqlitedbhelper.getTransactionsForBillToBill(firmId, nameId, transactionTypes, loadDataForBillToBill);
					break;
				case TxnTypeConstant.TXN_TYPE_ROPENBALANCE:
					var transactionTypes = [TxnTypeConstant.TXN_TYPE_PURCHASE, TxnTypeConstant.TXN_TYPE_SALE_RETURN, TxnTypeConstant.TXN_TYPE_CASHIN];
					var txnModelListForBillToBill = sqlitedbhelper.getTransactionsForBillToBill(firmId, nameId, transactionTypes, loadDataForBillToBill);
					break;
			}

			if (txnModelListForBillToBill && txnModelListForBillToBill.length > 0) {
				TransactionList = getTransactionObjectList(txnModelListForBillToBill);
			}
			sortTransactionListBasedOnDate(TransactionList);
		}
		return TransactionList;
	};

	// Loading all transaction for nameID
	this.LoadAllTransactions = function (nameId, descending) {
		var TransactionList = [];
		var txnModelList = sqlitedbhelper.getTransactionOnName(nameId);
		if (txnModelList && txnModelList.length > 0) {
			TransactionList = getTransactionObjectList(txnModelList);
		}
		sortTransactionListBasedOnDate(TransactionList, descending);
		return TransactionList;
	};

	// Load all transaction for bank ID
	this.LoadAllTransactionsForBankId = function (bankId) {
		var TransactionList = [];
		var txnModelList = sqlitedbhelper.getTransactionOnBankId(bankId);
		if (txnModelList && txnModelList.length > 0) {
			TransactionList = getTransactionObjectList(txnModelList);
		}
		return TransactionList;
	};

	this.loadAllLinkedTransactionByTxnId = function (txnId) {
		var txnLinksModelList = sqlitedbhelper.getAllLinkedTransactionByTxnId(txnId);
		return txnLinksModelList;
	};

	this.loadAllTransactionLinksByTxnId = function (txnId) {
		var txnLinksModelList = sqlitedbhelper.getAllTransactionLinksByTxnId(txnId);

		return txnLinksModelList;
	};

	this.loadClosedTransactionDataById = function (txnId) {
		var closedTxnModel = sqlitedbhelper.getClosedTransactionDataById(txnId);

		return closedTxnModel;
	};

	this.LoadTransactionFromIdForClosedTxn = function (txnId) {
		var txnModel = sqlitedbhelper.LoadTransactionFromIdForClosedTxn(txnId);
		var transactionObj = convertDataFromTransactionModelToTransactionObject(txnModel, true);
		if (txnModel.get_image_id() > 0) {
			transactionObj.setImageBlob(this.getImageFromId(txnModel.get_image_id()));
		}
		return transactionObj;
	};

	this.LoadTransactionFromId = function (txnId) {
		var transactionObj = void 0;
		var txnModel = sqlitedbhelper.LoadTransactionFromId(txnId);
		if (txnModel) {
			transactionObj = convertDataFromTransactionModelToTransactionObject(txnModel);
			if (txnModel.get_image_id() > 0) {
				transactionObj.setImageBlob(this.getImageFromId(txnModel.get_image_id()));
			}
		}
		return transactionObj;
	};

	this.LoadAllOrders = function (startDate, endDate, nameId, firmId, sortInAscendingOrder, orderType) {
		var txnStatus = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : -1;

		var txnModelList = sqlitedbhelper.getAllOrders(startDate, endDate, nameId, firmId, txnStatus, orderType);
		var transactionList = [];
		if (txnModelList != null && txnModelList.length > 0) {
			transactionList = getTransactionObjectList(txnModelList);
		}
		if (sortInAscendingOrder) {
			sortTransactionListBasedOnDate(transactionList);
		} else {
			sortTransactionListBasedOnDate(transactionList, true);
		}
		return transactionList;
	};

	this.LoadAllDeliveryChallans = function () {
		var txnModelList = sqlitedbhelper.getAllDeliveryChallans();
		var transactionList = [];
		if (txnModelList != null && txnModelList.length > 0) {
			transactionList = getTransactionObjectList(txnModelList);
		}
		sortTransactionListBasedOnDate(transactionList, true);
		return transactionList;
	};

	this.LoadAllEstimates = function () {
		var txnModelList = sqlitedbhelper.getAllEstimates();
		var transactionList = [];
		if (txnModelList != null && txnModelList.length > 0) {
			transactionList = getTransactionObjectList(txnModelList);
		}
		sortTransactionListBasedOnDate(transactionList, true);
		return transactionList;
	};

	this.getItemWiseProfitAndLossReportObjectList = function (fromDate, toDate, showOnlyActiveRows, showOnlyActiveItems) {
		var ItemCache = require('./../Cache/ItemCache.js');
		var itemCache = new ItemCache();
		var itemTxnValuesListMap = sqlitedbhelper.getItemTxnValuesListForItemWiseProfitAndLossReport(fromDate, toDate, showOnlyActiveItems);
		var ProfitAndLossReportObject = require('./../BizLogic/ProfitAndLossReportObject.js');
		var profitAndLossReportObject = new ProfitAndLossReportObject();
		profitAndLossReportObject.setFromDate(fromDate);
		profitAndLossReportObject.setToDate(toDate);
		profitAndLossReportObject.clearItemValues();
		this.loadProfitAndLossReportDataForStockValue(profitAndLossReportObject);
		var ItemWiseProfitAndLossReportObject = require('./../BizLogic/ItemWiseProfitAndLossReportObject.js');

		var itemWiseProfitAndLossReportObjectList = [];
		var listOfItems = itemCache.getListOfItemsAndServiceObject();

		$.each(listOfItems, function (k, v) {
			var item = v;
			if (showOnlyActiveItems & item.getIsItemActive() != 1) return;
			var itemWiseProfitAndLossReportObject = new ItemWiseProfitAndLossReportObject();
			itemWiseProfitAndLossReportObject.setItemId(item.getItemId());
			itemWiseProfitAndLossReportObject.setItemName(item.getItemName());
			var itemId = Number(item.getItemId());
			var itemTxnValues = itemTxnValuesListMap.get(itemId);
			if (itemTxnValues) {
				var saleValueObject = itemTxnValues.get(TxnTypeConstant.TXN_TYPE_SALE);
				var purchaseValueObject = itemTxnValues.get(TxnTypeConstant.TXN_TYPE_PURCHASE);
				var purchaseReturnValueObject = itemTxnValues.get(TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN);
				var saleReturnValueObject = itemTxnValues.get(TxnTypeConstant.TXN_TYPE_SALE_RETURN);

				if (saleValueObject != null) {
					itemWiseProfitAndLossReportObject.setActiveRow(true);
				}

				var saleValue = saleValueObject == null ? 0 : saleValueObject;
				var purchaseValue = purchaseValueObject == null ? 0 : purchaseValueObject;
				var purchaseReturnValue = purchaseReturnValueObject == null ? 0 : purchaseReturnValueObject;
				var saleReturnValue = saleReturnValueObject == null ? 0 : saleReturnValueObject;

				itemWiseProfitAndLossReportObject.setSaleValue(saleValue);
				itemWiseProfitAndLossReportObject.setPurchaseValue(purchaseValue);
				itemWiseProfitAndLossReportObject.setPurchaseReturnValue(purchaseReturnValue);
				itemWiseProfitAndLossReportObject.setSaleReturnValue(saleReturnValue);
				itemWiseProfitAndLossReportObject.setReceivableTax(itemTxnValues.get('taxReceivable'));
				itemWiseProfitAndLossReportObject.setPayableTax(itemTxnValues.get('taxPayable'));
			}
			var itemStockValue = profitAndLossReportObject.getItemValues()[itemId];
			if (itemStockValue != null) {
				itemWiseProfitAndLossReportObject.setOpeningStockValue(itemStockValue[0]);
				itemWiseProfitAndLossReportObject.setClosingStockValue(itemStockValue[1]);
			}
			if (itemWiseProfitAndLossReportObject.isActiveRow() || !showOnlyActiveRows) {
				itemWiseProfitAndLossReportObjectList.push(itemWiseProfitAndLossReportObject);
			}
		});
		return itemWiseProfitAndLossReportObjectList;
	};

	this.getSalePurchaseReportByParty = function (fromDate, toDate, selectedFirmId) {
		var salePurchaseReportByPartyMap = sqlitedbhelper.getSalePurchaseReportByParty(fromDate, toDate, selectedFirmId);
		var salePurchaseReportByPartyList = [];
		salePurchaseReportByPartyMap.forEach(function (value, key) {
			var partyList = new _map2.default();
			var txn = value;
			var name = txn['name'];
			var saleAmt = txn[TxnTypeConstant.TXN_TYPE_SALE];
			var purchaseAmt = txn[TxnTypeConstant.TXN_TYPE_PURCHASE];
			var purchaseReturnAmt = txn[TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN];
			var saleReturnAmt = txn[TxnTypeConstant.TXN_TYPE_SALE_RETURN];
			var netSaleAmt = (saleAmt == null ? 0 : saleAmt) - (saleReturnAmt == null ? 0 : saleReturnAmt);
			var netPurchaseAmt = (purchaseAmt == null ? 0 : purchaseAmt) - (purchaseReturnAmt == null ? 0 : purchaseReturnAmt);
			partyList.set('nameId', key);
			partyList.set('partyName', name);
			partyList.set('saleAmount', netSaleAmt);
			partyList.set('purchaseAmount', netPurchaseAmt);
			salePurchaseReportByPartyList.push(partyList);
		});
		return salePurchaseReportByPartyList;
	};

	this.getSalePurchaseReportByPartyGroup = function (fromDate, toDate, selectedFirmId) {
		var salePurchaseReportByPartyGroupMap = sqlitedbhelper.getSalePurchaseReportByPartyGroup(fromDate, toDate, selectedFirmId);
		var salePurchaseReportByPartyList = [];
		salePurchaseReportByPartyGroupMap.forEach(function (value, key) {
			var groupList = new _map2.default();
			var txn = value;
			var name = txn['name'];
			var saleAmt = txn[TxnTypeConstant.TXN_TYPE_SALE];
			var purchaseAmt = txn[TxnTypeConstant.TXN_TYPE_PURCHASE];
			var purchaseReturnAmt = txn[TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN];
			var saleReturnAmt = txn[TxnTypeConstant.TXN_TYPE_SALE_RETURN];
			var netSaleAmt = (saleAmt == null ? 0 : saleAmt) - (saleReturnAmt == null ? 0 : saleReturnAmt);
			var netPurchaseAmt = (purchaseAmt == null ? 0 : purchaseAmt) - (purchaseReturnAmt == null ? 0 : purchaseReturnAmt);
			groupList.set('groupName', name);
			groupList.set('saleAmount', netSaleAmt);
			groupList.set('purchaseAmount', netPurchaseAmt);
			salePurchaseReportByPartyList.push(groupList);
		});
		return salePurchaseReportByPartyList;
	};

	this.getPartyReportByItemList = function (itemId, fromDate, toDate, selectedFirmId) {
		if (itemId == 0) {
			// return [];
		}
		var partyReportItemList = [];
		var txnPartyList = sqlitedbhelper.getPartyReportByItemList(itemId, fromDate, toDate, selectedFirmId);

		$.each(txnPartyList, function (key, value) {
			var nameList = {};
			var txn = value;
			var name = txn['name'];
			var saleQty = txn[TxnTypeConstant.TXN_TYPE_SALE];
			var purchaseQty = txn[TxnTypeConstant.TXN_TYPE_PURCHASE];
			var purchaseReturnQty = txn[TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN];
			var saleReturnQty = txn[TxnTypeConstant.TXN_TYPE_SALE_RETURN];

			var netSaleQty = (saleQty == null ? 0 : saleQty[0]) - (saleReturnQty == null ? 0 : saleReturnQty[0]);
			var netPurchaseQty = (purchaseQty == null ? 0 : purchaseQty[0]) - (purchaseReturnQty == null ? 0 : purchaseReturnQty[0]);
			var netSaleFreeQuantity = (saleQty == null ? 0 : saleQty[1]) - (saleReturnQty == null ? 0 : saleReturnQty[1]);
			var netPurchaseFreeQuantity = (purchaseQty == null ? 0 : purchaseQty[1]) - (purchaseReturnQty == null ? 0 : purchaseReturnQty[1]);
			var netSaleAmount = (saleQty == null ? 0 : saleQty[2]) - (saleReturnQty == null ? 0 : saleReturnQty[2]);
			var netPurchaseAmount = (purchaseQty == null ? 0 : purchaseQty[2]) - (purchaseReturnQty == null ? 0 : purchaseReturnQty[2]);

			nameList['partyName'] = name;
			nameList['saleQuantity'] = netSaleQty;
			nameList['purchaseQuantity'] = netPurchaseQty;
			nameList['saleFreeQuantity'] = netSaleFreeQuantity;
			nameList['purchaseFreeQuantity'] = netPurchaseFreeQuantity;
			nameList['saleAmount'] = netSaleAmount;
			nameList['purchaseAmount'] = netPurchaseAmount;

			partyReportItemList.push(nameList);
		});
		return partyReportItemList;
	};

	this.getItemReportByPartyList = function (nameId, fromDate, toDate, selectedFirmId) {
		if (nameId == 0) {
			// return [];
		}
		var itemReportPartyList = [];
		var txnItemList = sqlitedbhelper.getItemReportByPartyList(nameId, fromDate, toDate, selectedFirmId);

		txnItemList.forEach(function (value) {
			var itemList = {};
			var txn = value;
			var name = txn['name'];

			var saleQty = txn[TxnTypeConstant.TXN_TYPE_SALE];
			var purchaseQty = txn[TxnTypeConstant.TXN_TYPE_PURCHASE];
			var purchaseReturnQty = txn[TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN];
			var saleReturnQty = txn[TxnTypeConstant.TXN_TYPE_SALE_RETURN];

			var netSaleQty = (saleQty == null ? 0 : saleQty[0]) - (saleReturnQty == null ? 0 : saleReturnQty[0]);
			var netPurchaseQty = (purchaseQty == null ? 0 : purchaseQty[0]) - (purchaseReturnQty == null ? 0 : purchaseReturnQty[0]);

			var netSaleFreeQty = (saleQty == null ? 0 : saleQty[1]) - (saleReturnQty == null ? 0 : saleReturnQty[1]);
			var netPurchaseFreeQty = (purchaseQty == null ? 0 : purchaseQty[1]) - (purchaseReturnQty == null ? 0 : purchaseReturnQty[1]);
			var netSaleAmount = (saleQty == null ? 0 : saleQty[2]) - (saleReturnQty == null ? 0 : saleReturnQty[2]);
			var netPurchaseAmount = (purchaseQty == null ? 0 : purchaseQty[2]) - (purchaseReturnQty == null ? 0 : purchaseReturnQty[2]);

			itemList['itemName'] = name;
			itemList['saleQuantity'] = netSaleQty;
			itemList['purchaseQuantity'] = netPurchaseQty;
			itemList['saleFreeQuantity'] = netSaleFreeQty;
			itemList['purchaseFreeQuantity'] = netPurchaseFreeQty;
			itemList['saleAmount'] = netSaleAmount;
			itemList['purchaseAmount'] = netPurchaseAmount;
			itemReportPartyList.push(itemList);
		});
		return itemReportPartyList;
	};

	this.LoadAllPaymentType = function () {
		var PaymentInfoModelList = sqlitedbhelper.loadAllPaymentType();
		var PaymentInfoModel = require('./../Models/PaymentInfoModel.js');
		if (PaymentInfoModelList.length > 0) {
			return PaymentInfoModelList.reduce(function (all, current) {
				var paymentInfo = new PaymentInfoModel();
				var model = current;
				paymentInfo.setId(model.getId());
				paymentInfo.setType(model.getType());
				paymentInfo.setName(model.getName());
				paymentInfo.setBankName(model.getBankName());
				paymentInfo.setAccountNumber(model.getAccountNumber());
				paymentInfo.setCurrentBalance(model.getCurrentBalance());
				paymentInfo.setOpeningBalance(model.getOpeningBalance());
				paymentInfo.setOpeningDate(model.getOpeningDate());

				all[Number(paymentInfo.getId())] = paymentInfo;
				return all;
			}, {});
		}
		return {};
	};

	this.getAllLoanAccounts = function (firmId) {
		var temp = sqlitedbhelper.loadAllLoanAccounts(firmId);
		return temp.reduce(function (prev, value) {
			prev[value.getLoanAccountId()] = value;
			return prev;
		}, {});
	};

	this.getLoanTransactions = function (_ref2) {
		var loanAccountId = _ref2.loanAccountId,
		    fromDate = _ref2.fromDate,
		    toDate = _ref2.toDate,
		    paymentTypeIds = _ref2.paymentTypeIds,
		    loanTxnTypes = _ref2.loanTxnTypes,
		    firmId = _ref2.firmId,
		    latestFirst = _ref2.latestFirst;

		var temp = sqlitedbhelper.getLoanTransactions({ loanAccountId: loanAccountId, fromDate: fromDate, toDate: toDate, paymentTypeIds: paymentTypeIds, loanTxnTypes: loanTxnTypes, firmId: firmId, latestFirst: latestFirst });
		return temp;
	};

	this.getLoanAccountBalances = function (_ref3) {
		var loanAccountIds = _ref3.loanAccountIds,
		    tillDate = _ref3.tillDate;

		var balance = sqlitedbhelper.getLoanAccountBalances({ loanAccountIds: loanAccountIds, tillDate: tillDate });
		return balance;
	};

	this.getCashAdjForAdjId = function (adjId) {
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqlitedbhelper = new SqliteDBHelper();
		var cashAdjModel = sqlitedbhelper.getCashAdjForAdjId(adjId);
		var CashAdjustmentLogic = require('./../BizLogic/CasAdjustmentLogic.js');
		var cashAdjustmentLogic = new CashAdjustmentLogic();
		if (cashAdjModel) {
			cashAdjustmentLogic.setAdjAmount(cashAdjModel.getAdjAmount());
			cashAdjustmentLogic.setAdjId(cashAdjModel.getAdjId());
			cashAdjustmentLogic.setAdjType(cashAdjModel.getAdjType());
			cashAdjustmentLogic.setAdjDate(cashAdjModel.getAdjDate());
			cashAdjustmentLogic.setAdjDescription(cashAdjModel.getAdjDescription());
		}
		return cashAdjustmentLogic;
	};

	this.LoadAccountAdjustmentForId = function (bankId) {
		var accountAdjustmentModel = sqlitedbhelper.LoadAccountAdjustmentForId(bankId);
		var BankAdjustmentTxn = require('./../BizLogic/BankAdjustmentTxn.js');
		var bankadjustmenttxn = new BankAdjustmentTxn();
		var model = accountAdjustmentModel;
		bankadjustmenttxn.setAdjId(model.getAdjId());
		bankadjustmenttxn.setAdjToBankId(model.getAdjToBankId());
		bankadjustmenttxn.setAdjBankId(model.getAdjBankId());
		bankadjustmenttxn.setAdjType(model.getAdjType());
		bankadjustmenttxn.setAdjAmount(model.getAdjAmount());
		bankadjustmenttxn.setAdjDescription(model.getAdjDescription());
		bankadjustmenttxn.setAdjDate(model.getAdjDate());
		return bankadjustmenttxn;
	};

	this.LoadListOfAccountAdjustment = function () {
		var accountAdjustmentList = [];
		var accountAdjustmentModel = sqlitedbhelper.LoadListOfAccountAdjustment();
		var BankAdjustmentTxn = require('./../BizLogic/BankAdjustmentTxn.js');
		if (accountAdjustmentModel.length > 0) {
			var len = accountAdjustmentModel.length;
			for (var i = 0; i < len; i++) {
				var bankadjustmenttxnmodel = new BankAdjustmentTxn();
				var model = accountAdjustmentModel[i];
				bankadjustmenttxnmodel.setAdjId(model.getAdjId());
				bankadjustmenttxnmodel.setAdjBankId(model.getAdjBankId());
				bankadjustmenttxnmodel.setAdjType(model.getAdjType());
				bankadjustmenttxnmodel.setAdjAmount(model.getAdjAmount());
				bankadjustmenttxnmodel.setAdjDescription(model.getAdjDescription());
				bankadjustmenttxnmodel.setAdjDate(model.getAdjDate());
				accountAdjustmentList.push(bankadjustmenttxnmodel);
			}
		}
		return accountAdjustmentList;
	};

	this.getListOfAdjustmentForItemId = function (itemId) {
		var itemAdjustmentList = [];
		var itemAdjustmentModel = sqlitedbhelper.getListOfAdjustmentForItemId(itemId);
		var ItemAdjustmentTxn = require('./../BizLogic/ItemAdjustmentTxn.js');
		if (itemAdjustmentModel.length > 0) {
			var len = itemAdjustmentModel.length;
			for (var i = 0; i < len; i++) {
				var itemadjmodel = new ItemAdjustmentTxn();
				var model = itemAdjustmentModel[i];
				itemadjmodel.setItemAdjId(model.getItemAdjId());
				itemadjmodel.setItemAdjItemId(model.getItemAdjItemId());
				itemadjmodel.setItemAdjType(model.getItemAdjType());
				itemadjmodel.setItemAdjQuantity(Number(model.getItemAdjQuantity()));
				itemadjmodel.setItemAdjDescription(model.getItemAdjDescription());
				itemadjmodel.setItemAdjDate(model.getItemAdjDate());
				itemAdjustmentList.push(itemadjmodel);
			}
		}
		return itemAdjustmentList;
	};

	this.getItemAdjRowForItemAdjId = function (itemAdjId) {
		var itemAdjustmentList = [];
		var itemAdjustmentModel = sqlitedbhelper.getItemAdjRowForItemAdjId(itemAdjId);
		var ItemAdjustmentTxn = require('./../BizLogic/ItemAdjustmentTxn.js');
		if (itemAdjustmentModel.length > 0) {
			var len = itemAdjustmentModel.length;
			for (var i = 0; i < len; i++) {
				var itemadjmodel = new ItemAdjustmentTxn();
				var model = itemAdjustmentModel[i];
				itemadjmodel.setItemAdjId(model.getItemAdjId());
				itemadjmodel.setItemAdjItemId(model.getItemAdjItemId());
				itemadjmodel.setItemAdjType(model.getItemAdjType());
				itemadjmodel.setItemAdjQuantity(Number(model.getItemAdjQuantity()));
				itemadjmodel.setItemAdjDescription(model.getItemAdjDescription());
				itemadjmodel.setItemAdjDate(model.getItemAdjDate());
				itemadjmodel.setItemAdjIstId(model.getItemAdjIstId());
				itemadjmodel.setItemAdjAtPrice(model.getItemAdjAtPrice());

				itemAdjustmentList.push(itemadjmodel);
			}
		}
		return itemAdjustmentList;
	};

	this.loadTransactionsForGSTR3Report = function (fromDate, toDate, firmId) {
		var TransactionList = [];
		var txnModelList = sqlitedbhelper.getTransactionsForGSTR3Report(fromDate, toDate, firmId);
		if (txnModelList && txnModelList.length > 0) {
			TransactionList = getTransactionObjectList(txnModelList);
		}
		sortTransactionListBasedOnDate(TransactionList);
		return TransactionList;
	};

	this.loadTransactionsForGSTR1Report = function (fromDate, toDate, firmId) {
		var TransactionList = [];
		var txnModelList = sqlitedbhelper.getTransactionsForGSTR1Report(fromDate, toDate, firmId);
		if (txnModelList && txnModelList.length > 0) {
			TransactionList = getTransactionObjectList(txnModelList);
		}
		sortTransactionListBasedOnDate(TransactionList, false, true);
		return TransactionList;
	};

	this.loadTransactionsForGSTR1HsnReport = function (fromDate, toDate, firmId, transactionType) {
		var TransactionList = [];
		var txnModelList = sqlitedbhelper.getTransactionsForGSTR1HsnReport(fromDate, toDate, firmId, transactionType);
		if (txnModelList && txnModelList.length > 0) {
			TransactionList = getTransactionObjectList(txnModelList);
		}
		sortTransactionListBasedOnDate(TransactionList);
		return TransactionList;
	};

	this.loadTransactionsForGSTR2HsnReport = function (fromDate, toDate, firmId, transactionType) {
		var TransactionList = [];
		var txnModelList = sqlitedbhelper.getTransactionsForGSTR2HsnReport(fromDate, toDate, firmId, transactionType);
		if (txnModelList && txnModelList.length > 0) {
			TransactionList = getTransactionObjectList(txnModelList);
		}
		sortTransactionListBasedOnDate(TransactionList);
		return TransactionList;
	};

	this.loadTransactionsForGSTR2Report = function (fromDate, toDate, firmId) {
		var TransactionList = [];
		var txnModelList = sqlitedbhelper.getTransactionsForGSTR2Report(fromDate, toDate, firmId);
		if (txnModelList && txnModelList.length > 0) {
			TransactionList = getTransactionObjectList(txnModelList);
		}
		sortTransactionListBasedOnDate(TransactionList);
		return TransactionList;
	};

	this.loadTransactionsForGSTR4Report = function (fromDate, toDate, firmId) {
		var TransactionList = [];
		var txnModelList = sqlitedbhelper.getTransactionsForGSTR4Report(fromDate, toDate, firmId);
		if (txnModelList && txnModelList.length > 0) {
			TransactionList = getTransactionObjectList(txnModelList);
		}
		sortTransactionListBasedOnDate(TransactionList);
		return TransactionList;
	};

	this.loadTransactionsForGSTR9AReport = function (fromDate, toDate, firmId) {
		var TransactionList = [];
		var txnModelList = sqlitedbhelper.getTransactionsForGSTR9AReport(fromDate, toDate, firmId);
		if (txnModelList && txnModelList.length > 0) {
			TransactionList = getTransactionObjectList(txnModelList);
		}
		sortTransactionListBasedOnDate(TransactionList);
		return TransactionList;
	};

	this.loadTransactionsForGSTR9Report = function (fromDate, toDate, firmId) {
		var TransactionList = [];
		var txnModelList = sqlitedbhelper.getTransactionsForGSTR9Report(fromDate, toDate, firmId);
		if (txnModelList && txnModelList.length > 0) {
			TransactionList = getTransactionObjectList(txnModelList);
		}
		sortTransactionListBasedOnDate(TransactionList);
		return TransactionList;
	};

	this.loadTurnOverForGSTR4Report = function (fromDate, toDate, firmId) {
		var turnOver = sqlitedbhelper.getTurnOverForGSTR4(fromDate, toDate, firmId);
		return turnOver;
	};

	this.loadTurnOverForGSTR9AReport = function (fromDate, toDate, firmId) {
		var turnOver = sqlitedbhelper.getTurnOverForGSTR9A(fromDate, toDate, firmId);
		return turnOver;
	};

	this.getCashAdjObjOnId = function (id) {
		var sqliteDBHelper = new SqliteDBHelper();
		var txnModel = sqliteDBHelper.getCashAdjustmentForId(id);
		var CashAdjustmentLogic = require('./../BizLogic/CasAdjustmentLogic.js');
		var cashAdjustmentLogic = new CashAdjustmentLogic();
		cashAdjustmentLogic.setAdjId(txnModel.getAdjId());
		cashAdjustmentLogic.setAdjType(txnModel.getAdjType());
		var date = txnModel.getAdjDate();
		cashAdjustmentLogic.setAdjDate(date);
		cashAdjustmentLogic.setAdjDescription(txnModel.getAdjDescription());
		cashAdjustmentLogic.setAdjAmount(txnModel.getAdjAmount());
		return cashAdjustmentLogic;
	};

	// Functio to fetch cashInHand
	this.getCashInHandAmount = function () {
		var endDate = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

		return sqlitedbhelper.getCashInHandAmount(endDate);
	};

	this.getAddReduceAmountForCashAndBank = function () {
		var startDate = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
		var endDate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

		return sqlitedbhelper.getAddReduceAmountForCashAndBank(startDate, endDate);
	};

	this.getOpeningBalancePartyCashAndBank = function () {
		var startDate = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
		var endDate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

		var openingPartyBalance = sqlitedbhelper.getOpeningBalancesByDate(startDate, endDate);
		var openingCashInHand = sqlitedbhelper.getOpeningCashInHandTxn(startDate, endDate);
		var openingCashInHandAmount = openingCashInHand.getAdjAmount();
		var openingBankBalance = sqlitedbhelper.getOpeningBankBalance(startDate, endDate);
		return {
			openingCashInHandAmount: openingCashInHandAmount,
			openingBankBalance: openingBankBalance,
			openingPartyBalance: openingPartyBalance
		};
	};

	this.getOpeningLoanBalanceAfterCloseBook = function () {
		var startDate = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
		var endDate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

		return sqlitedbhelper.getOpeningLoanBalanceAfterCloseBook(startDate, endDate);
	};

	this.LoadAllSettings = function () {
		var settingList = sqlitedbhelper.getAllSettings();
		var settingMap = {};
		// //console.log(settingList);
		if (settingList.length > 0) {
			var len = settingList.length;
			for (var i = 0; i < len; i++) {
				var x = settingList[i];
				var key = x['settingKey'];
				var value = x['settingValue'];
				settingMap[key] = value;
			}
		}
		// //console.log(settingMap);
		return settingMap;
	};

	this.loadAllCategories = function () {
		var itemCategoryModelList = sqlitedbhelper.getItemCategoryList();
		var itemCategoryList = {};
		// console.log(itemCategoryModelList);
		var ItemCategoryLogic = require('./../BizLogic/ItemCategoryLogic.js');
		if (itemCategoryModelList) {
			var len = itemCategoryModelList.length;
			for (var i = 0; i < len; i++) {
				var model = itemCategoryModelList[i];
				var itemCategory = new ItemCategoryLogic();
				itemCategory.setCategoryId(model.getCategoryId());
				itemCategory.setCategoryName(model.getCategoryName());
				itemCategory.setMemberCount(model.getMemberCount());
				itemCategoryList[itemCategory.getCategoryId()] = itemCategory;
			}
		}
		return itemCategoryList;
	};

	this.loadAllPartyGroups = function () {
		var partyGroupModelList = sqlitedbhelper.getPartyGroupList();
		var partyGroupList = {};
		// console.log(partyGroupModelList);
		var PartyGroupLogic = require('./../BizLogic/PartyGroupLogic.js');
		if (partyGroupModelList) {
			var len = partyGroupModelList.length;
			for (var i = 0; i < len; i++) {
				var model = partyGroupModelList[i];
				var partyGroup = new PartyGroupLogic();
				partyGroup.setGroupId(model.getGroupId());
				partyGroup.setGroupName(model.getGroupName());
				partyGroup.setMemberCount(model.getMemberCount());

				partyGroupList[partyGroup.getGroupId()] = partyGroup;
			}
		}
		return partyGroupList;
	};

	// function to fetch the list of txn Ref number for
	this.isRefNoUnique = function (refNoObject) {
		return sqlitedbhelper.isRefNoUnique(refNoObject);
	};

	this.getImageFromId = function (imageId) {
		var rtnBlob = sqlitedbhelper.getImageFromId(imageId);
		return rtnBlob;
	};

	this.getCurrentExtraChargesValue = function (ECID) {
		var ECValue = sqlitedbhelper.getCurrentExtraChargesValue(ECID);
		return ECValue;
	};

	this.getCashInHandTxnList = function () {
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqliteDBHelper = new SqliteDBHelper();
		var listOfTxnModel = sqliteDBHelper.getCashInHandTxnModelList();
		// console.log(listOfTxnModel);
		var cashInHandDetailObjectList = [];
		if (listOfTxnModel != null && listOfTxnModel.length > 0) {
			var CashInHand = require('./../BizLogic/CashInHand.js');
			$.each(listOfTxnModel, function (key, cashInHandDetailObjectModel) {
				var cashInHandDetailObject = new CashInHand();
				cashInHandDetailObject.setTxnId(cashInHandDetailObjectModel.getTxnId());
				cashInHandDetailObject.setTxnType(cashInHandDetailObjectModel.getTxnType());
				cashInHandDetailObject.setAmount(cashInHandDetailObjectModel.getAmount());
				cashInHandDetailObject.setTxnDate(cashInHandDetailObjectModel.getTxnDate());
				cashInHandDetailObject.setUserId(cashInHandDetailObjectModel.getUserId());
				cashInHandDetailObject.setDescription(cashInHandDetailObjectModel.getDescription());
				cashInHandDetailObject.setBankId(cashInHandDetailObjectModel.getBankId());
				cashInHandDetailObject.setSubTxnType(cashInHandDetailObjectModel.getSubTxnType());
				cashInHandDetailObject['lastModifiedDate'] = cashInHandDetailObjectModel['lastModifiedDate'];

				cashInHandDetailObjectList.push(cashInHandDetailObject);
			});
		}
		return cashInHandDetailObjectList;
	};

	this.LoadPartyWiseItemRate = function (nameId, itemId) {
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqliteDBHelper = new SqliteDBHelper();
		var partyRate = sqliteDBHelper.getPartyWiseItemRateModel(nameId, itemId);
		return partyRate;
	};

	this.LoadSearchedTransactions = function (search_key, fromDate, toDate) {
		var sqliteDBHelper = new SqliteDBHelper();
		var txnMap = sqliteDBHelper.searchTransactionsWithText(search_key, fromDate, toDate);
		var transactionList = [];
		if (txnMap != null && txnMap && txnMap.length > 0) {
			transactionList = getTransactionObjectList(txnMap);
		}
		return transactionList;
	};

	this.getAllItemUnits = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		var ItemUnitLogic = require('./../BizLogic/ItemUnitLogic.js');
		var itemUnitModelList = sqliteDBHelper.loadAllItemUnit();
		var itemUnitMap = {};
		if (itemUnitModelList != null && itemUnitModelList.length > 0) {
			$.each(itemUnitModelList, function (key, itemUnitModel) {
				var itemUnit = new ItemUnitLogic();
				itemUnit.setUnitId(itemUnitModel.getUnitId());
				itemUnit.setUnitName(itemUnitModel.getUnitName());
				itemUnit.setUnitShortName(itemUnitModel.getUnitShortName());
				itemUnit.setFullNameEditable(itemUnitModel.isFullNameEditable());
				itemUnit.setUnitDeletable(itemUnitModel.isUnitDeletable());

				itemUnitMap[itemUnit.getUnitShortName()] = itemUnit;
			});
		}
		return itemUnitMap;
	};

	this.loadLastFiveRecordsOfLineItemOfSaleTransactions = function (nameid, itemid, txnType) {
		var sqliteDBHelper = new SqliteDBHelper();
		var transactionsList = sqliteDBHelper.loadLastFiveRecordsOfLineItemOfSaleTransactions(nameid, itemid, txnType);

		return transactionsList;
	};
	this.getAllItemUnitsMapping = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		var ItemUnitMapping = require('./../BizLogic/ItemunitMapping.js');
		var itemUnitModelList = sqliteDBHelper.loadAllItemUnitMapping();
		var itemUnitMap = {};
		if (itemUnitModelList != null && itemUnitModelList.length > 0) {
			$.each(itemUnitModelList, function (key, itemUnitMappingModel) {
				var itemUnitMapping = new ItemUnitMapping();
				itemUnitMapping.setMappingId(itemUnitMappingModel.getMappingId());
				itemUnitMapping.setBaseUnitId(itemUnitMappingModel.getBaseUnitId());
				itemUnitMapping.setSecondaryUnitId(itemUnitMappingModel.getSecondaryUnitId());
				itemUnitMapping.setConversionRate(itemUnitMappingModel.getConversionRate());

				itemUnitMap[itemUnitMappingModel.getMappingId()] = itemUnitMapping;
			});
		}
		return itemUnitMap;
	};

	this.loadAllItemsWithoutUnits = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		var itemList = sqliteDBHelper.loadAllItemsWithoutUnits();
		return itemList;
	};

	this.loadAllServicesWithoutUnits = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		var itemList = sqliteDBHelper.loadAllServicesWithoutUnits();
		return itemList;
	};

	this.getPrefixDetailsOnFirmId = function (firmId) {
		var txnType = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
		var onlyDefault = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.loadAllPrefixesByFirmId(firmId, txnType, onlyDefault);
	};

	this.getMaxRefNoForCashIn = function (firmId) {
		var sqliteDBHelper = new SqliteDBHelper();
		var txnType = TxnTypeConstant.TXN_TYPE_CASHIN;
		var txnSubType = null;
		var maxRefForCashIn = sqliteDBHelper.getLatestTxnRefNumber(txnType, txnSubType, firmId);

		return maxRefForCashIn;
	};

	this.getMaxRefNoForSaleOrder = function (firmId) {
		var sqliteDBHelper = new SqliteDBHelper();
		var txnType = TxnTypeConstant.TXN_TYPE_SALE_ORDER;
		var txnSubType = null;
		var maxRefForSaleOrder = sqliteDBHelper.getLatestTxnRefNumber(txnType, txnSubType, firmId);

		return maxRefForSaleOrder;
	};

	this.getMaxRefNoForSaleReturn = function (firmId) {
		var sqliteDBHelper = new SqliteDBHelper();
		var txnType = TxnTypeConstant.TXN_TYPE_SALE_RETURN;
		var txnSubType = null;
		var maxRefForSaleOrder = sqliteDBHelper.getLatestTxnRefNumber(txnType, txnSubType, firmId);

		return maxRefForSaleOrder;
	};
	this.getMaxRefNoForPurchaseOrder = function (firmId) {
		var sqliteDBHelper = new SqliteDBHelper();
		var txnType = TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER;
		var txnSubType = null;
		var maxRefForPurchaseOrder = sqliteDBHelper.getLatestTxnRefNumber(txnType, txnSubType, firmId);

		return maxRefForPurchaseOrder;
	};

	this.getMaxRefNoForSale = function (firmId, invoiceType) {
		var sqliteDBHelper = new SqliteDBHelper();
		var txnType = TxnTypeConstant.TXN_TYPE_SALE;
		var txnSubType = invoiceType;
		var maxRefForSale = sqliteDBHelper.getLatestTxnRefNumber(txnType, txnSubType, firmId);

		return maxRefForSale;
	};

	this.getMaxRefNoForDeliveryChallan = function (firmId) {
		var sqliteDBHelper = new SqliteDBHelper();
		var txnType = TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN;
		var txnSubType = null;
		return sqliteDBHelper.getLatestTxnRefNumber(txnType, txnSubType, firmId);
	};
	this.getMaxRefNoForEstimate = function (firmId) {
		var sqliteDBHelper = new SqliteDBHelper();
		var txnType = TxnTypeConstant.TXN_TYPE_ESTIMATE;
		var txnSubType = null;
		return sqliteDBHelper.getLatestTxnRefNumber(txnType, txnSubType, firmId);
	};

	this.getFirmList = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		var firmModelList = sqliteDBHelper.loadAllFirms();
		var FirmObj = require('./../BizLogic/FirmObject.js');
		var firmObjectList = {};
		if (firmModelList != null && firmModelList.length > 0) {
			$.each(firmModelList, function (key, firmModel) {
				var firmObj = new FirmObj();
				firmObj.setFirmId(firmModel.getFirmId());
				firmObj.setFirmName(firmModel.getFirmName());
				firmObj.setFirmInvoiceNumber(firmModel.getFirmInvoiceNumber());
				firmObj.setFirmInvoicePrefix(firmModel.getFirmInvoicePrefix());
				firmObj.setFirmTaxInvoicePrefix(firmModel.getFirmTaxInvoicePrefix());
				firmObj.setFirmTaxInvoiceNumber(firmModel.getFirmTaxInvoiceNumber());
				firmObj.setFirmDeliveryChallanPrefix(firmModel.getFirmDeliveryChallanPrefix());
				firmObj.setFirmEstimatePrefix(firmModel.getFirmEstimatePrefix());
				firmObj.setFirmEstimateNumber(firmModel.getFirmEstimateNumber());
				firmObj.setFirmCashInPrefix(firmModel.getFirmCashInPrefix());
				firmObj.setFirmEmail(firmModel.getFirmEmail());
				firmObj.setFirmPhone(firmModel.getFirmPhone());
				firmObj.setFirmAddress(firmModel.getFirmAddress());
				firmObj.setFirmTin(firmModel.getFirmTin());
				firmObj.setFirmHSNSAC(firmModel.getFirmHSNSAC());
				firmObj.setFirmState(firmModel.getFirmState());
				firmObj.setFirmLogoId(firmModel.getFirmLogoId());
				firmObj.setFirmSignId(firmModel.getFirmSignId());
				firmObj.setBankName(firmModel.getBankName());
				firmObj.setBankIFSC(firmModel.getBankIFSC());
				firmObj.setAccountNumber(firmModel.getAccountNumber());
				firmObj.setUpiAccountNumber(firmModel.getUpiAccountNumber());
				firmObj.setUpiIFSC(firmModel.getUpiIFSC());
				firmObj.setUdfObjectArray(firmModel.getUdfObjectArray());
				firmObj.setBusinessType(firmModel.getBusinessType());
				firmObj.setBusinessCategory(firmModel.getBusinessCategory());
				if (firmModel.getFirmLogoId()) {
					firmObj.setFirmImagePath(Number(firmModel.getFirmLogoId()) ? sqlitedbhelper.getImageFromId(firmModel.getFirmLogoId()) : '');
				}
				if (firmModel.getFirmSignId()) {
					firmObj.setFirmSignImagePath(Number(firmModel.getFirmSignId()) ? sqlitedbhelper.getImageFromId(firmModel.getFirmSignId()) : '');
				}

				firmObjectList[firmModel.getFirmName()] = firmObj;
			});
		}
		return firmObjectList;
	};

	var addDummyDates = function addDummyDates(transactionList, fromDate, toDate) {
		var transactionWithDummyDates = new _map2.default();
		var listKeys = (0, _keys2.default)(transactionList);
		if (listKeys.length > 0) {
			var prevDate = new Date(fromDate);
			while (MyDate.compareOnlyDatePart(prevDate, toDate) <= 0) {
				var dateToBeAdded = MyDate.getDate('y-m-d', prevDate);
				if (!(dateToBeAdded in transactionList)) {
					transactionWithDummyDates.set(dateToBeAdded, 0);
				} else {
					transactionWithDummyDates.set(dateToBeAdded, transactionList[dateToBeAdded]);
				}
				prevDate.setDate(prevDate.getDate() + 1);
			}
		}
		return transactionWithDummyDates;
	};

	this.loadSalesForGraph = function (fromDate, toDate, firmId) {
		var salesModelList = sqlitedbhelper.getSalesForGraph(fromDate, toDate, firmId);
		salesModelList.sales = addDummyDates(salesModelList.sales, fromDate, toDate);
		return salesModelList;
	};

	this.loadExpenseForGraph = function (fromDate, toDate, firmId) {
		var expenseModelList = sqlitedbhelper.getExpenseForGraph(fromDate, toDate, firmId);
		expenseModelList.expense = addDummyDates(expenseModelList.expense, fromDate, toDate);
		return expenseModelList;
	};

	this.loadTransactionsForSaleReport = function (fromDate, toDate, firmId) {
		var TransactionList = [];
		var txnModelList = sqlitedbhelper.getTransactionsForSaleReport(fromDate, toDate, firmId);
		if (txnModelList && txnModelList.length > 0) {
			TransactionList = getTransactionObjectList(txnModelList);
		}
		sortTransactionListBasedOnDate(TransactionList);
		return TransactionList;
	};

	this.loadTransactionsForPurchaseReport = function (fromDate, toDate, firmId) {
		var TransactionList = [];
		var txnModelList = sqlitedbhelper.getTransactionsForPurchaseReport(fromDate, toDate, firmId);
		if (txnModelList && txnModelList.length > 0) {
			TransactionList = getTransactionObjectList(txnModelList);
		}
		sortTransactionListBasedOnDate(TransactionList);
		return TransactionList;
	};

	this.loadTransactionsForExpenseReport = function (fromDate, toDate, nameId, firmId, loanAccountName, latestFirst) {
		var TxnTypeConstant = require('../Constants/TxnTypeConstant');
		var LoanAccountCache = require('./../Cache/LoanAccountCache');
		if (nameId == 0) {
			return [];
		}
		var TransactionList = [];
		if (!loanAccountName) {
			var txnModelList = sqlitedbhelper.getTransactionsForExpenseReport(fromDate, toDate, nameId, firmId);
			if (txnModelList && txnModelList.length > 0) {
				TransactionList = getTransactionObjectList(txnModelList);
			}
		}
		// Get loan transactions
		var loanTxnList = [];
		if (!(nameId && nameId > 0)) {
			var loanAccountCache = new LoanAccountCache();
			var loanAccountId = loanAccountCache.getLoanAccountIdByName(loanAccountName) || '';
			if (!loanAccountName || loanAccountName && loanAccountId) {
				loanTxnList = sqlitedbhelper.getLoanTransactions({
					fromDate: fromDate,
					toDate: toDate,
					loanTxnTypes: [TxnTypeConstant.TXN_TYPE_LOAN_EMI_PAYMENT, TxnTypeConstant.TXN_TYPE_LOAN_PROCESSING_FEE],
					loanAccountId: loanAccountId,
					firmId: firmId,
					latestFirst: latestFirst
				});
			}
			var TransactionFactory = require('./../BizLogic/TransactionFactory.js');
			var transactionfactory = new TransactionFactory();
			if (loanTxnList && loanTxnList.length > 0) {
				for (var i = 0; i < loanTxnList.length; i++) {
					var loanTxn = loanTxnList[i];
					var transactionObj = transactionfactory.getTransactionObject(loanTxn.getLoanType());
					var loanAccount = loanAccountCache.getLoanAccountById(loanTxn.getLoanAccountId());
					var name = loanAccount.getLoanAccName();
					var cashAmount = 0;
					var description = '';
					if (loanTxn.getLoanType() == TxnTypeConstant.TXN_TYPE_LOAN_PROCESSING_FEE) {
						cashAmount = loanTxn.getPrincipalAmount();
						description = 'Processing Fee for [' + name + ']';
					} else {
						cashAmount = loanTxn.getInterestAmount();
						description = 'Interest Payment for [' + name + ']';
					}
					transactionObj.setTxnId(loanTxn.getLoanTxnId());
					transactionObj.setTxnType(loanTxn.getLoanType());
					transactionObj.setTxnDate(loanTxn.getTransactionDate());
					transactionObj.setCashAmount(cashAmount);
					transactionObj.setDescription(description);
					Number(cashAmount) && TransactionList.push(transactionObj);
				}
			}
		}
		sortTransactionListBasedOnDate(TransactionList);
		return TransactionList;
	};

	this.loadTransactionsForIncomeReport = function (fromDate, toDate, nameId, firmId) {
		if (nameId == 0) {
			return [];
		}
		var TransactionList = [];
		var txnModelList = sqlitedbhelper.getTransactionsForIncomeReport(fromDate, toDate, nameId, firmId);
		if (txnModelList && txnModelList.length > 0) {
			TransactionList = getTransactionObjectList(txnModelList);
		}
		sortTransactionListBasedOnDate(TransactionList);
		return TransactionList;
	};

	this.loadExpenseList = function () {
		var _ref4 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
		    _ref4$fromDate = _ref4.fromDate,
		    fromDate = _ref4$fromDate === undefined ? null : _ref4$fromDate,
		    _ref4$toDate = _ref4.toDate,
		    toDate = _ref4$toDate === undefined ? null : _ref4$toDate,
		    _ref4$firmId = _ref4.firmId,
		    firmId = _ref4$firmId === undefined ? null : _ref4$firmId,
		    _ref4$allowZeroExpens = _ref4.allowZeroExpenses,
		    allowZeroExpenses = _ref4$allowZeroExpens === undefined ? true : _ref4$allowZeroExpens;

		var expenseCategoryReportObjectModelList = sqlitedbhelper.getTransactionsForExpenseCategoryReport(fromDate, toDate, firmId);
		var expenseCategoryReportObjectList = [];
		var NameCache = require('./../Cache/NameCache.js');
		var ExpenseCategoryReportObject = require('./../BizLogic/ExpenseCategoryReportObject.js');
		var nameCache = new NameCache();
		var expenseList = nameCache.getListOfExpenseObject();
		var expenseCategoryReportObjectModelMap = new _map2.default();
		if (expenseCategoryReportObjectModelList != null && expenseCategoryReportObjectModelList.length > 0) {
			for (var i = 0; i < expenseCategoryReportObjectModelList.length; i++) {
				var txn = expenseCategoryReportObjectModelList[i];
				expenseCategoryReportObjectModelMap.set(Number(txn.getCategoryId()), txn);
			}
		}

		if (expenseList != null && expenseList.length > 0) {
			for (var _i = 0; _i < expenseList.length; _i++) {
				var expenseObject = expenseList[_i];
				var expenseCategoryReportObject = new ExpenseCategoryReportObject();
				expenseCategoryReportObject.setCategoryName(expenseObject.getFullName());
				expenseCategoryReportObject.setCategoryId(expenseObject.getNameId());
				var nameId = Number(expenseObject.getNameId());
				if (expenseCategoryReportObjectModelMap.has(nameId)) {
					var _txn = expenseCategoryReportObjectModelMap.get(nameId);
					expenseCategoryReportObject.setAmount(_txn.getAmount());
				} else {
					expenseCategoryReportObject.setAmount(0);
				}
				if (allowZeroExpenses || expenseCategoryReportObject.getAmount()) {
					expenseCategoryReportObjectList.push(expenseCategoryReportObject);
				}
			}
		}

		// From Loan Accounts
		var LoanAccountCache = require('./../Cache/LoanAccountCache.js');
		var totalProcessingFee = 0;
		var loanAccountCache = new LoanAccountCache();
		var loanAccounts = loanAccountCache.getAllLoanAccounts(firmId);
		var loanAcccountExpenseMap = new _map2.default();
		loanAcccountExpenseMap = sqlitedbhelper.getLoanAccountExpenseMap({ fromDate: fromDate, toDate: toDate, firmId: firmId });
		loanAccounts.forEach(function (loanAccount) {
			var expenseCategoryReportObject = new ExpenseCategoryReportObject();
			var categoryName = 'Interest Payment for [' + loanAccount.getLoanAccName() + ']';
			expenseCategoryReportObject.setCategoryName(categoryName);
			expenseCategoryReportObject.setCategoryId(loanAccount.getLoanAccountId());
			expenseCategoryReportObject.setIsCategoryLoanType(true);
			var loanAccountId = loanAccount.getLoanAccountId();
			if (loanAcccountExpenseMap.has(loanAccountId)) {
				var interestPaid = loanAcccountExpenseMap.get(loanAccountId);
				expenseCategoryReportObject.setAmount(interestPaid);
			} else {
				expenseCategoryReportObject.setAmount(0);
			}
			if (allowZeroExpenses || expenseCategoryReportObject.getAmount()) {
				expenseCategoryReportObjectList.push(expenseCategoryReportObject);
			}
			// Total processing fee calc.
			var processingFeeDate = loanAccount.getOpeningDate();
			var includeProcessingFee = true;
			if (toDate) {
				includeProcessingFee = includeProcessingFee && MyDate.compareOnlyDatePart(processingFeeDate, toDate) <= 0;
			}
			if (fromDate) {
				includeProcessingFee = includeProcessingFee && MyDate.compareOnlyDatePart(fromDate, processingFeeDate) <= 0;
			}
			if (includeProcessingFee) {
				totalProcessingFee += Number(loanAccount.getProcessingFee() || '');
			}
		});
		if (loanAccounts.length) {
			var processingFeeCategoryObj = new ExpenseCategoryReportObject();
			processingFeeCategoryObj.setCategoryId('ProcessingFeeCategory');
			processingFeeCategoryObj.setCategoryName('Processing Fee for Loans');
			processingFeeCategoryObj.setIsCategoryLoanType(true);
			processingFeeCategoryObj.setAmount(totalProcessingFee);
			if (allowZeroExpenses || totalProcessingFee) {
				expenseCategoryReportObjectList.push(processingFeeCategoryObj);
			}
		}
		return expenseCategoryReportObjectList;
	};

	this.loadTransactionsForExpenseCategoryReport = function () {
		var fromDate = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
		var toDate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
		var firmId = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

		var expenseCategoryReportObjectModelList = sqlitedbhelper.getTransactionsForExpenseCategoryReport(fromDate, toDate, firmId);
		var expenseCategoryReportObjectList = [];
		var NameCache = require('./../Cache/NameCache.js');
		var ExpenseCategoryReportObject = require('./../BizLogic/ExpenseCategoryReportObject.js');
		var nameCache = new NameCache();

		if (expenseCategoryReportObjectModelList != null && expenseCategoryReportObjectModelList.length > 0) {
			for (var i = 0; i < expenseCategoryReportObjectModelList.length; i++) {
				var txn = expenseCategoryReportObjectModelList[i];
				var name = nameCache.findNameByNameId(txn.getCategoryId());
				if (name != null) {
					var expenseCategoryReportObject = new ExpenseCategoryReportObject();
					expenseCategoryReportObject.setCategoryName(name);
					expenseCategoryReportObject.setAmount(txn.getAmount());
					expenseCategoryReportObject.setCategoryId(txn.getCategoryId());
					expenseCategoryReportObjectList.push(expenseCategoryReportObject);
				}
			}
		}
		return expenseCategoryReportObjectList;
	};

	this.loadIncomeList = function () {
		var incomeCategoryReportObjectModelList = sqlitedbhelper.getTransactionsForIncomeCategoryReport();
		var incomeCategoryReportObjectList = [];
		var NameCache = require('./../Cache/NameCache.js');
		var IncomeCategoryReportObject = require('./../BizLogic/IncomeCategoryReportObject.js');
		var nameCache = new NameCache();
		var incomeList = nameCache.getListOfIncomeObject();
		var incomeCategoryReportObjectModelMap = new _map2.default();
		if (incomeCategoryReportObjectModelList != null && incomeCategoryReportObjectModelList.length > 0) {
			for (var i = 0; i < incomeCategoryReportObjectModelList.length; i++) {
				var txn = incomeCategoryReportObjectModelList[i];
				incomeCategoryReportObjectModelMap.set(Number(txn.getCategoryId()), txn);
			}
		}

		if (incomeList != null && incomeList.length > 0) {
			for (var _i2 = 0; _i2 < incomeList.length; _i2++) {
				var incomeObject = incomeList[_i2];
				var incomeCategoryReportObject = new IncomeCategoryReportObject();
				incomeCategoryReportObject.setCategoryName(incomeObject.getFullName());
				incomeCategoryReportObject.setCategoryId(incomeObject.getNameId());
				var nameId = Number(incomeObject.getNameId());
				if (incomeCategoryReportObjectModelMap.has(nameId)) {
					var _txn2 = incomeCategoryReportObjectModelMap.get(nameId);
					incomeCategoryReportObject.setAmount(_txn2.getAmount());
				} else {
					incomeCategoryReportObject.setAmount(0);
				}
				incomeCategoryReportObjectList.push(incomeCategoryReportObject);
			}
		}

		return incomeCategoryReportObjectList;
	};

	this.loadTransactionsForIncomeCategoryReport = function () {
		var fromDate = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
		var toDate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
		var firmId = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

		var incomeCategoryReportObjectModelList = sqlitedbhelper.getTransactionsForIncomeCategoryReport(fromDate, toDate, firmId);
		var incomeCategoryReportObjectList = [];
		var NameCache = require('./../Cache/NameCache.js');
		var IncomeCategoryReportObject = require('./../BizLogic/IncomeCategoryReportObject.js');
		var nameCache = new NameCache();

		if (incomeCategoryReportObjectModelList != null && incomeCategoryReportObjectModelList.length > 0) {
			for (var i = 0; i < incomeCategoryReportObjectModelList.length; i++) {
				var txn = incomeCategoryReportObjectModelList[i];
				var name = nameCache.findNameByNameId(txn.getCategoryId());
				if (name != null) {
					var incomeCategoryReportObject = new IncomeCategoryReportObject();
					incomeCategoryReportObject.setCategoryName(name);
					incomeCategoryReportObject.setAmount(txn.getAmount());
					incomeCategoryReportObject.setCategoryId(txn.getCategoryId());
					incomeCategoryReportObjectList.push(incomeCategoryReportObject);
				}
			}
		}
		return incomeCategoryReportObjectList;
	};

	this.getExpenseItemReportList = function (fromDate, toDate, firmId) {
		var expenseItemReportObjectModelList = sqlitedbhelper.getExpenseItemReportList(fromDate, toDate, firmId);
		var expenseItemReportObjectList = [];
		var ExpenseItemReportObject = require('./../BizLogic/ExpenseItemReportObject.js');
		if (expenseItemReportObjectModelList != null && expenseItemReportObjectModelList.length > 0) {
			for (var i = 0; i < expenseItemReportObjectModelList.length; i++) {
				var txn = expenseItemReportObjectModelList[i];
				var expenseItemReportObject = new ExpenseItemReportObject();
				expenseItemReportObject.setItemId(txn.getItemId());
				expenseItemReportObject.setItemName(txn.getItemName());
				expenseItemReportObject.setQty(txn.getQty());
				expenseItemReportObject.setAmount(txn.getAmount());
				expenseItemReportObjectList.push(expenseItemReportObject);
			}
		}

		return expenseItemReportObjectList;
	};

	this.getIncomeItemReportList = function (fromDate, toDate, firmId) {
		var incomeItemReportObjectModelList = sqlitedbhelper.getIncomeItemReportList(fromDate, toDate, firmId);
		var incomeItemReportObjectList = [];
		var IncomeItemReportObject = require('./../BizLogic/IncomeItemReportObject.js');
		if (incomeItemReportObjectModelList != null && incomeItemReportObjectModelList.length > 0) {
			for (var i = 0; i < incomeItemReportObjectModelList.length; i++) {
				var txn = incomeItemReportObjectModelList[i];
				var incomeItemReportObject = new IncomeItemReportObject();
				incomeItemReportObject.setItemId(txn.getItemId());
				incomeItemReportObject.setItemName(txn.getItemName());
				incomeItemReportObject.setQty(txn.getQty());
				incomeItemReportObject.setAmount(txn.getAmount());
				incomeItemReportObjectList.push(incomeItemReportObject);
			}
		}

		return incomeItemReportObjectList;
	};

	this.loadProfitAndLossReportDataForTransactions = function (profitAndLossReportObject) {
		sqlitedbhelper.getProfitAndLossReportDataForTransactions(profitAndLossReportObject);
		var taxData = sqlitedbhelper.getToTalTaxAmount(profitAndLossReportObject.getFromDate(), profitAndLossReportObject.getToDate());
		profitAndLossReportObject.setPayableTax(taxData.payable);
		profitAndLossReportObject.setReceivableTax(taxData.receivable);
	};

	this.loadProfitAndLossReportDataForStockValue = function (profitAndLossReportObject) {
		sqlitedbhelper.getProfitAndLossReportDataForStockValue(profitAndLossReportObject);
	};

	this.getTaxListForTaxRateReport = function (fromDate, toDate, firmId) {
		// sql
		var taxRateReportObjectList = [];
		var TaxCodeCache = require('./../Cache/TaxCodeCache.js');
		var taxCodeCache = new TaxCodeCache();
		var TaxCodeType = require('./../Constants/TaxCodeConstants.js');
		var TaxRateReportObject = require('./../BizLogic/TaxRateReportObject.js');
		var taxRateReportObjectModelHashMap = sqlitedbhelper.getTaxListForTaxRateReport(fromDate, toDate, firmId);
		if (taxRateReportObjectModelHashMap != null && (0, _keys2.default)(taxRateReportObjectModelHashMap).length > 0) {
			var addTaxObject = function addTaxObject(taxCodeName, taxPercentage, taxIn, taxOut, taxableSale, taxablePurchase) {
				var findTax = taxRateReportObjectList.find(function (x) {
					return x.taxName == taxCodeName;
				});
				if (findTax) {
					findTax.setTaxIn(findTax.getTaxIn() + taxIn);
					findTax.setTaxOut(findTax.getTaxOut() + taxOut);
					findTax.setSaleTaxableValue(findTax.getSaleTaxableValue() + taxableSale);
					findTax.setPurchaseTaxableValue(findTax.getPurchaseTaxableValue() + taxablePurchase);
				} else {
					var taxRateReportObject = new TaxRateReportObject();
					taxRateReportObject.setTaxPercent(taxPercentage);
					taxRateReportObject.setTaxName(taxCodeName);
					taxRateReportObject.setTaxIn(taxIn);
					taxRateReportObject.setTaxOut(taxOut);
					taxRateReportObject.setSaleTaxableValue(taxableSale);
					taxRateReportObject.setPurchaseTaxableValue(taxablePurchase);
					taxRateReportObjectList.push(taxRateReportObject);
				}
			};

			$.each(taxRateReportObjectModelHashMap, function (key, value) {
				var taxRateReportObjectModel = taxRateReportObjectModelHashMap[key];
				if (taxRateReportObjectModel.getTaxid() == -1) {
					addTaxObject('Additional CESS', -1, taxRateReportObjectModel.getTaxIn(), taxRateReportObjectModel.getTaxOut(), taxRateReportObjectModel.getSaleTaxableValue(), taxRateReportObjectModel.getPurchaseTaxableValue());
				} else {
					var taxCode = taxCodeCache.getTaxCodeObjectById(taxRateReportObjectModel.getTaxid());
					if (taxCode != null) {
						if (taxCode.getTaxType() == TaxCodeType.taxRate) {
							var taxCodeName = taxCode.getTaxCodeName();
							var taxIn = taxRateReportObjectModel.getTaxIn();
							var taxOut = taxRateReportObjectModel.getTaxOut();
							var taxableSale = taxRateReportObjectModel.getSaleTaxableValue();
							var taxablePurchase = taxRateReportObjectModel.getPurchaseTaxableValue();
							var taxPercentage = -1;

							if (taxCode.getTaxRateType() != TaxCodeConstants.Exempted) {
								taxPercentage = taxCode.getTaxRate();
							}

							addTaxObject(taxCodeName, taxPercentage, taxIn, taxOut, taxableSale, taxablePurchase);
						} else if (taxCode.getTaxType() == TaxCodeType.taxGroup) {
							$.each(taxCode.getTaxCodeMap(), function (key, taxId) {
								var code = taxCodeCache.getTaxCodeObjectById(taxId);
								var taxCodeName = code.getTaxCodeName();

								var taxIn = taxCode.getTaxRate() == 0 ? 0 : code.getTaxRate() * taxRateReportObjectModel.getTaxIn() / taxCode.getTaxRate();

								var taxOut = taxCode.getTaxRate() == 0 ? 0 : code.getTaxRate() * taxRateReportObjectModel.getTaxOut() / taxCode.getTaxRate();

								var taxPercentage = -1;

								if (code.getTaxRateType() != TaxCodeConstants.Exempted) {
									taxPercentage = code.getTaxRate();
								}

								var taxableSale = taxRateReportObjectModel.getSaleTaxableValue();
								var taxablePurchase = taxRateReportObjectModel.getPurchaseTaxableValue();

								addTaxObject(taxCodeName, taxPercentage, taxIn, taxOut, taxableSale, taxablePurchase);
							});
						}
					}
				}
			});
		}

		return taxRateReportObjectList;
	};

	this.getTaxListForTaxReport = function (startDate, endDate, firmId) {
		var taxReportObjectList = [];
		var NameCache = require('./../Cache/NameCache.js');
		var nameCache = new NameCache();
		var TaxReportObject = require('./../BizLogic/TaxReportObject.js');
		var taxReportObjectModelHashMap = sqlitedbhelper.getTaxListForTaxReport(startDate, endDate, firmId);
		if (taxReportObjectModelHashMap != null && (0, _keys2.default)(taxReportObjectModelHashMap).length > 0) {
			$.each(taxReportObjectModelHashMap, function (key, value) {
				var taxReportObjectModel = taxReportObjectModelHashMap[key];
				var partyName = nameCache.findNameByNameId(taxReportObjectModel.getNameId());
				if (partyName != null) {
					var taxReportObject = new TaxReportObject();
					taxReportObject.setPartyName(partyName);
					taxReportObject.setTaxIn(taxReportObjectModel.getTaxIn());
					taxReportObject.setTaxOut(taxReportObjectModel.getTaxOut());
					taxReportObjectList.push(taxReportObject);
				}
			});
		}
		sortOnBasisOfName(taxReportObjectList);
		return taxReportObjectList;
	};

	var sortOnBasisOfName = function sortOnBasisOfName(txnList) {
		if (txnList) {
			txnList.sort(function (lhs, rhs) {
				var firstTxn = lhs;
				var secondTxn = rhs;
				if (firstTxn.getPartyName().toLowerCase() < secondTxn.getPartyName().toLowerCase()) {
					return -1;
				}
				if (firstTxn.getPartyName().toLowerCase() > secondTxn.getPartyName().toLowerCase()) {
					return 1;
				}
				return 0;
			});
		}
	};

	this.getListForDiscountReport = function (startDate, endDate, firmId) {
		var discountReportObjectList = [];
		var NameCache = require('./../Cache/NameCache.js');
		var nameCache = new NameCache();
		var DiscountReportObject = require('./../BizLogic/DiscountReportObject.js');
		var discountReportObjectModelHashMap = sqlitedbhelper.getListForDiscountReport(startDate, endDate, firmId);
		if (discountReportObjectModelHashMap != null && (0, _keys2.default)(discountReportObjectModelHashMap).length > 0) {
			$.each(discountReportObjectModelHashMap, function (key, value) {
				var discountReportObjectModel = discountReportObjectModelHashMap[key];
				var partyName = nameCache.findNameByNameId(discountReportObjectModel.getNameId());
				if (partyName != null) {
					var discountReportObject = new DiscountReportObject();
					discountReportObject.setPartyName(partyName);
					discountReportObject.setSaleDiscount(discountReportObjectModel.getSaleDiscount());
					discountReportObject.setPurchaseDiscount(discountReportObjectModel.getPurchaseDiscount());
					discountReportObjectList.push(discountReportObject);
				}
			});
		}
		sortOnBasisOfName(discountReportObjectList);
		return discountReportObjectList;
	};

	this.loadTransactionsForCustomReport = function (fromDate, toDate, nameId, transactionId, firmId) {
		if (nameId == 0) {
			return [];
		}
		var TransactionList = [];
		var txnModelList = sqlitedbhelper.getTransactionsForCustomReport(fromDate, toDate, nameId, transactionId, firmId);
		if (txnModelList && txnModelList.length > 0) {
			TransactionList = getTransactionObjectList(txnModelList);
		}
		sortTransactionListBasedOnDate(TransactionList);
		return TransactionList;
	};

	this.loadTransactionsForPrintCenter = function (fromDate, toDate, nameId, transactionId, firmId) {
		if (nameId == 0) {
			return [];
		}
		var TransactionList = [];
		var txnModelList = sqlitedbhelper.getTransactionsForPrintCenter(fromDate, toDate, nameId, transactionId, firmId);
		if (txnModelList && txnModelList.length > 0) {
			TransactionList = getTransactionObjectList(txnModelList);
		}
		sortTransactionListBasedOnDate(TransactionList);
		return TransactionList;
	};

	this.loadTransactionsForPartyStatement = function (fromDate, toDate, nameId, viewMode) {
		if (nameId == 0) {
			return [];
		}
		var TransactionList = [];
		if (nameId > 0) {
			var txnModelList = sqlitedbhelper.getTransactionsForPartyStatement(fromDate, toDate, nameId, viewMode);
			if (txnModelList && txnModelList.length > 0) {
				TransactionList = getTransactionObjectList(txnModelList);
			}
			sortTransactionListBasedOnDate(TransactionList);
			var beginningBalanceTxn = sqlitedbhelper.getPartyBeginningBalanceForStatement(fromDate, nameId);
			if (beginningBalanceTxn) {
				var TransactionFactory = require('./../BizLogic/TransactionFactory.js');
				var transactionfactory = new TransactionFactory();
				var NameCache = require('./../Cache/NameCache.js');
				var namecache = new NameCache();
				var transactionObj = transactionfactory.getTransactionObject(beginningBalanceTxn.get_txn_type());
				transactionObj.setBalanceAmount(beginningBalanceTxn.get_balance_amount());
				transactionObj.setTxnId(-1);
				transactionObj.setTxnDate(beginningBalanceTxn.get_txn_date());
				transactionObj.setNameRef(namecache.findNameObjectByNameId(beginningBalanceTxn.get_txn_name_id()));
				TransactionList.unshift(transactionObj);
			}
		}
		return TransactionList;
	};

	this.loadTransactionsForDayBook = function (date, firmId) {
		var TransactionList = [];
		var TxnTypeConstant = require('../Constants/TxnTypeConstant');
		var TransactionFactory = require('./../BizLogic/TransactionFactory.js');
		var transactionfactory = new TransactionFactory();
		var txnModelList = sqlitedbhelper.getTransactionsForDayBook(date, firmId);
		if (txnModelList && txnModelList.length > 0) {
			TransactionList = getTransactionObjectList(txnModelList);
		}
		if (firmId == 0) {
			var bankAdjTxnList = sqlitedbhelper.getBankAdjustmentTxnsForDayBook(date);
			if (bankAdjTxnList && bankAdjTxnList.length > 0) {
				for (var i = 0; i < bankAdjTxnList.length; i++) {
					var txnModel = bankAdjTxnList[i];
					var transactionObj = transactionfactory.getTransactionObject(txnModel.get_txn_type());
					transactionObj.setTxnId(txnModel.get_txn_id());
					transactionObj.setTxnDate(txnModel.get_txn_date());
					transactionObj.setCashAmount(txnModel.get_cash_amount());
					transactionObj.setDescription(txnModel.get_txn_description());
					transactionObj.setBankId(txnModel.getBankId());
					transactionObj.setToBankId(txnModel.getToBankId());
					TransactionList.push(transactionObj);
				}
			}

			var cashAdjTxnList = sqlitedbhelper.getCashAdjustmentTxnsForDayBook(date);
			if (cashAdjTxnList && cashAdjTxnList.length > 0) {
				for (var _i3 = 0; _i3 < cashAdjTxnList.length; _i3++) {
					txnModel = cashAdjTxnList[_i3];
					var _transactionObj = transactionfactory.getTransactionObject(txnModel.get_txn_type());
					_transactionObj.setTxnId(txnModel.get_txn_id());
					_transactionObj.setTxnDate(txnModel.get_txn_date());
					_transactionObj.setCashAmount(txnModel.get_cash_amount());
					_transactionObj.setDescription(txnModel.get_txn_description());

					TransactionList.push(_transactionObj);
				}
			}
		}

		var BaseTransaction = require('./../BizLogic/BaseTransaction.js');
		var baseTxn = new BaseTransaction();
		//
		// Get closed cheque transfered to cash
		var chequeTransferTxnList = sqlitedbhelper.getChequeTransferForDayBook(date, firmId);

		if (chequeTransferTxnList && chequeTransferTxnList.length > 0) {
			for (var _i4 = 0; _i4 < chequeTransferTxnList.length; _i4++) {
				txnModel = chequeTransferTxnList[_i4];
				var _transactionObj2 = transactionfactory.getTransactionObject(TxnTypeConstant.TXN_TYPE_CHEQUE_TRANSFER);
				_transactionObj2.setTxnId(txnModel.getChequeId());
				_transactionObj2.setTxnDate(txnModel.getTransferDate());
				_transactionObj2.setTxnSubType(txnModel.getChequeTxnType());
				_transactionObj2.setCashAmount(txnModel.getChequeAmount());
				var chequeTxn = baseTxn.getTransactionFromId(txnModel.getChequeTxnId());
				_transactionObj2.setNameRef(chequeTxn.getNameRef());
				_transactionObj2.setTransferedTobankId(txnModel.getTransferredToAccount());
				TransactionList.push(_transactionObj2);
			}
		}

		var loanTransactions = sqlitedbhelper.getLoanTransactions({
			loanTxnTypes: [TxnTypeConstant.TXN_TYPE_LOAN_STARTING_BALANCE, TxnTypeConstant.TXN_TYPE_LOAN_ADJUSTMENT, TxnTypeConstant.TXN_TYPE_LOAN_EMI_PAYMENT, TxnTypeConstant.TXN_TYPE_LOAN_PROCESSING_FEE],
			fromDate: date,
			toDate: date,
			firmId: firmId
		});

		var LoanAccountCache = require('./../Cache/LoanAccountCache');
		var loanAccountCache = new LoanAccountCache();
		if (loanTransactions && loanTransactions.length > 0) {
			for (var _i5 = 0; _i5 < loanTransactions.length; _i5++) {
				var loanTxn = loanTransactions[_i5];
				var loanAccount = loanAccountCache.getLoanAccountById(loanTxn.getLoanAccountId());
				var _transactionObj3 = transactionfactory.getTransactionObject(loanTxn.getLoanType());
				_transactionObj3.setTxnId(loanTxn.getLoanTxnId());
				_transactionObj3.setTxnType(loanTxn.getLoanType());
				_transactionObj3.setTxnDate(loanTxn.getTransactionDate());
				_transactionObj3.setCashAmount(loanTxn.getTotalAmount());
				_transactionObj3.setDescription('[Loan Acc] ' + loanAccount.getLoanAccName());
				TransactionList.push(_transactionObj3);
			}
		}

		sortTransactionListBasedOnDate(TransactionList);
		return TransactionList;
	};

	this.loadAllCustomFields = function () {
		return sqlitedbhelper.getAllCustomFields();
	};

	this.loadAllUDF = function () {
		return sqlitedbhelper.getAllUDF();
	};

	this.loadTransactionsForCashFlowReport = function (fromDate, toDate, firmId, showZeroCashAmountTransaction) {
		var TransactionList = [];
		var TransactionFactory = require('./../BizLogic/TransactionFactory.js');
		var transactionfactory = new TransactionFactory();
		var txnModelList = sqlitedbhelper.getTransactionsForCashFlowReport(fromDate, toDate, firmId, showZeroCashAmountTransaction);
		if (txnModelList && txnModelList.length > 0) {
			TransactionList = getTransactionObjectList(txnModelList);
		}

		if (firmId == 0) {
			var bankAdjTxnList = sqlitedbhelper.getBankAdjustmentTxnsForCashReport(fromDate, toDate);
			if (bankAdjTxnList && bankAdjTxnList.length > 0) {
				for (var i = 0; i < bankAdjTxnList.length; i++) {
					var txnModel = bankAdjTxnList[i];
					if (Number(txnModel.get_cash_amount()) != 0) {
						var transactionObj = transactionfactory.getTransactionObject(txnModel.get_txn_type());
						transactionObj.setTxnId(txnModel.get_txn_id());
						transactionObj.setTxnDate(txnModel.get_txn_date());
						transactionObj.setCashAmount(txnModel.get_cash_amount());
						transactionObj.setDescription(txnModel.get_txn_description());
						transactionObj.setBankId(txnModel.getBankId());
						TransactionList.push(transactionObj);
					}
				}
			}
		}
		if (firmId == 0) {
			var cashAdjTxnList = sqlitedbhelper.getCashAdjustmentTxnsForCashReport(fromDate, toDate);
			if (cashAdjTxnList && cashAdjTxnList.length > 0) {
				for (var _i6 = 0; _i6 < cashAdjTxnList.length; _i6++) {
					var _txnModel = cashAdjTxnList[_i6];
					if (Number(_txnModel.get_cash_amount()) != 0) {
						var _transactionObj4 = transactionfactory.getTransactionObject(_txnModel.get_txn_type());
						_transactionObj4.setTxnId(_txnModel.get_txn_id());
						_transactionObj4.setTxnDate(_txnModel.get_txn_date());
						_transactionObj4.setCashAmount(_txnModel.get_cash_amount());
						_transactionObj4.setDescription(_txnModel.get_txn_description());

						TransactionList.push(_transactionObj4);
					}
				}
			}
		}

		var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
		var BaseTransaction = require('./../BizLogic/BaseTransaction.js');
		var baseTxn = new BaseTransaction();

		// Get closed cheque transfered to cash
		var closedChequeTxnList = sqlitedbhelper.getClosedChequeToCash(fromDate, toDate, firmId);
		if (closedChequeTxnList && closedChequeTxnList.length > 0) {
			for (var _i7 = 0; _i7 < closedChequeTxnList.length; _i7++) {
				var _txnModel2 = closedChequeTxnList[_i7];
				var _transactionObj5 = transactionfactory.getTransactionObject(TxnTypeConstant.TXN_TYPE_CHEQUE_TRANSFER);
				_transactionObj5.setTxnId(_txnModel2.getChequeId());
				_transactionObj5.setTxnDate(_txnModel2.getTransferDate());
				_transactionObj5.setTxnSubType(_txnModel2.getChequeTxnType());
				_transactionObj5.setCashAmount(_txnModel2.getChequeAmount());
				var chequeTxn = baseTxn.getTransactionFromId(_txnModel2.getChequeTxnId());
				if (!chequeTxn) {
					// closed cheque
					chequeTxn = this.LoadTransactionFromIdForClosedTxn(_txnModel2.getChequeTxnId());
				}
				_transactionObj5.setNameRef(chequeTxn.getNameRef());
				TransactionList.push(_transactionObj5);
			}
		}

		// Get loan transactions
		var loanTxnList = sqlitedbhelper.getLoanTransactions({
			loanTxnTypes: [TxnTypeConstant.TXN_TYPE_LOAN_STARTING_BALANCE, TxnTypeConstant.TXN_TYPE_LOAN_ADJUSTMENT, TxnTypeConstant.TXN_TYPE_LOAN_EMI_PAYMENT, TxnTypeConstant.TXN_TYPE_LOAN_PROCESSING_FEE],
			paymentTypeIds: [1],
			fromDate: fromDate,
			toDate: toDate,
			firmId: firmId
		});
		var LoanAccountCache = require('../Cache/LoanAccountCache');
		var loanAccountCache = new LoanAccountCache();
		if (loanTxnList && loanTxnList.length > 0) {
			for (var _i8 = 0; _i8 < loanTxnList.length; _i8++) {
				var loanTxn = loanTxnList[_i8];
				var loanAccount = loanAccountCache.getLoanAccountById(loanTxn.getLoanAccountId());
				var _transactionObj6 = transactionfactory.getTransactionObject(loanTxn.getLoanType());
				_transactionObj6.setTxnId(loanTxn.getLoanTxnId());
				_transactionObj6.setTxnType(loanTxn.getLoanType());
				_transactionObj6.setTxnDate(loanTxn.getTransactionDate());
				_transactionObj6.setCashAmount(loanTxn.getTotalAmount());
				_transactionObj6.setDescription('[Loan Acc] ' + loanAccount.getLoanAccName());
				TransactionList.push(_transactionObj6);
			}
		}

		sortTransactionListBasedOnTransactionIdAndDate(TransactionList);

		return TransactionList;
	};

	var sortTransactionListBasedOnTransactionIdAndDate = function sortTransactionListBasedOnTransactionIdAndDate(txnList, descendingOrder) {
		if (txnList) {
			txnList.sort(function (lhs, rhs) {
				var firstTxn = lhs;
				var secondTxn = rhs;
				if (descendingOrder) {
					firstTxn = rhs;
					secondTxn = lhs;
				}
				var timeDiff = firstTxn.getTxnDate().getTime() - secondTxn.getTxnDate().getTime();
				if (timeDiff == 0 && firstTxn.getTxnId() && secondTxn.getTxnId()) {
					return MyDouble.convertStringToDouble(firstTxn.getTxnId(), true) - MyDouble.convertStringToDouble(secondTxn.getTxnId(), true);
				}
				return timeDiff;
			});
		}
	};

	var sortTransactionListBasedOnDate = function sortTransactionListBasedOnDate(txnList, descendingOrder) {
		var useInvoiceNumberIfDateSame = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

		if (txnList) {
			txnList.sort(function (lhs, rhs) {
				var firstTxn = lhs;
				var secondTxn = rhs;
				if (descendingOrder) {
					firstTxn = rhs;
					secondTxn = lhs;
				}
				var timeDiff = firstTxn.getTxnDate().getTime() - secondTxn.getTxnDate().getTime();
				if (timeDiff == 0) {
					if (useInvoiceNumberIfDateSame && firstTxn.getTxnRefNumber() && secondTxn.getTxnRefNumber()) {
						return MyDouble.convertStringToDouble(firstTxn.getTxnRefNumber(), true) - MyDouble.convertStringToDouble(secondTxn.getTxnRefNumber(), true);
						// fallback to handle txns on same date in case useInvoiceNumberIfDateSame is not true or getTxnRefNumber is not provided
					} else if (firstTxn.getTxnId() && secondTxn.getTxnId()) {
						return MyDouble.convertStringToDouble(firstTxn.getTxnId(), true) - MyDouble.convertStringToDouble(secondTxn.getTxnId(), true);
					}
				}
				return timeDiff;
			});
		}
	};

	this.getClosedCheques = function (endDate) {
		return sqlitedbhelper.getClosedCheques(endDate);
	};

	this.canEditBaseUnitForItem = function (itemId) {
		return !sqlitedbhelper.isItemUnitUsedInLineItems(itemId);
	};

	// function to get list of all the tax codes
	this.getListOfTax = function () {
		var TaxCode = require('./../BizLogic/TaxCode.js');
		var taxListObject = {};
		var taxListModel = sqlitedbhelper.getListOfTaxCode();
		$.each(taxListModel, function (id, object) {
			var taxCode = new TaxCode();
			taxCode.setTaxCodeId(object[0].getTaxCodeId());
			taxCode.setTaxCodeName(object[0].getTaxCodeName());
			taxCode.setTaxRate(object[0].getTaxRate());
			taxCode.setTaxType(object[0].getTaxType());
			taxCode.setTaxRateType(object[0].getTaxRateType());
			taxCode.setTaxDateCreated(object[0].getTaxDateCreated());
			taxCode.setTaxDateModified(object[0].getTaxDateModified());
			taxCode.setTaxCodeMap(object[0].getTaxCodeMap());
			taxListObject[id] = [taxCode];
		});
		return taxListObject;
	};

	this.getItemStockSummaryReportData = function (toDate, selectedCategoryId, showOnlyActiveItems) {
		return sqlitedbhelper.getItemStockSummaryReportData(toDate, selectedCategoryId, showOnlyActiveItems);
	};

	this.getOrderItemReportList = function (startDate, endDate, nameId, firmId, txnStatus, orderType) {
		var orderItemReportObjectModelList = sqlitedbhelper.getOrderItemReportList(startDate, endDate, nameId, firmId, txnStatus, orderType);
		var orderItemReportObjectList = [];
		var OrderItemReportObject = require('./../BizLogic/OrderItemReportObject.js');
		if (orderItemReportObjectModelList != null && orderItemReportObjectModelList.length > 0) {
			for (var i = 0; i < orderItemReportObjectModelList.length; i++) {
				//
				var txn = orderItemReportObjectModelList[i];
				var orderItemReportObject = new OrderItemReportObject();
				orderItemReportObject.setItemId(txn.getItemId());
				orderItemReportObject.setItemName(txn.getItemName());
				orderItemReportObject.setQty(txn.getQty());
				orderItemReportObject.setFreeQty(txn.getFreeQty());
				orderItemReportObject.setAmount(txn.getAmount());
				orderItemReportObjectList.push(orderItemReportObject);
			}
		}

		return orderItemReportObjectList;
	};

	this.loadListForItemCategorySalePurchaseReport = function (nameId, startDate, endDate, firmId) {
		return sqlitedbhelper.getListForItemCategorySalePurchaseReport(nameId, startDate, endDate, firmId);
	};

	this.getItemCategoryStockList = function () {
		return sqlitedbhelper.getItemCategoryStockList();
	};

	this.getOpeningCashInHandTxn = function () {
		return sqlitedbhelper.getOpeningCashInHandTxn();
	};
	this.isLatestTransaction = function (partyId, txnId) {
		if (partyId) {
			var id = sqlitedbhelper.getLatestTransactionIdForParty(partyId);
			if (id == txnId) {
				return true;
			}
		}
		return false;
	};
	this.hasTransactionsOfTxnType = function (filters) {
		return sqlitedbhelper.hasTransactionsOfTxnType(filters);
	};
	this.isPurchaseTxnCreated = function () {
		var limit = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;

		return sqlitedbhelper.isPurchaseTxnCreated(limit);
	};
	this.getAllTransactionsForName = function (nameId) {
		var transactionList = this.LoadAllTransactions(nameId);
		this.sortTransactionList(transactionList);
		return transactionList;
	};

	this.searchTransactions = function (query, fromDate, toDate) {
		var transactionList = this.LoadSearchedTransactions(query, fromDate, toDate);
		if (transactionList != null && transactionList.length > 0) {
			this.sortTransactionList(transactionList);
		}
		return transactionList;
	};

	this.getTxnModelForTxnId = function (txnId) {
		return sqlitedbhelper.LoadTransactionFromId(txnId);
	};

	this.sortTransactionList = function (txnList) {
		txnList.sort(function (a, b) {
			if (a.txnDate > b.txnDate) {
				return -1;
			} else if (a.txnDate < b.txnDate) {
				return 1;
			} else {
				if (a.getCreatedAt() && a.getCreatedAt().getTime && b.getCreatedAt() && b.getCreatedAt().getTime) {
					return b.getCreatedAt().getTime() - a.getCreatedAt().getTime();
				}
				return 0;
			}
		});
	};

	this.getCatalogueItemImagesByCatalogueId = function (catalogueItemId) {
		var limit = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
		var appendDataImageTag = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

		try {
			var itemImageModelList = sqlitedbhelper.getCatalogueItemImagesByCatalogueId(catalogueItemId, limit);
			var itemImageList = [];
			if (itemImageModelList.length > 0) {
				itemImageList = this.prepareItemImageList(itemImageModelList, appendDataImageTag);
			}
			return itemImageList;
		} catch (ex) {
			if (logger) {
				logger.error('Error getting item images', ex);
			}
			return [];
		}
	};

	this.getItemImagesByItemId = function (itemId) {
		var appendDataImageTag = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
		var isHexImageRequired = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

		try {
			var itemImageModelList = sqlitedbhelper.getItemImagesByItemId(itemId, null, null, isHexImageRequired);
			var itemImageList = [];
			if (itemImageModelList.length > 0) {
				itemImageList = this.prepareItemImageList(itemImageModelList, appendDataImageTag, isHexImageRequired);
			}
			return itemImageList;
		} catch (ex) {
			if (logger) {
				logger.error('Error getting item images', ex);
			}
			return [];
		}
	};

	this.prepareItemImageList = function (itemImageModelList) {
		var appendDataImageTag = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
		var isHexImageRequired = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

		var ItemImages = require('../BizLogic/ItemImages');
		var itemImageList = [];
		for (var i = 0; i < itemImageModelList.length; i++) {
			var itemImageModel = itemImageModelList[i];
			var itemImage = new ItemImages();
			itemImage.setItemId(itemImageModel.getItemId());
			var itemImageId = itemImageModel.getItemImageId();
			itemImage.setItemImageId(itemImageId);
			itemImage.setCatalogueItemId(itemImageModel.getCatalogueItemId());
			if (isHexImageRequired) {
				itemImage.setHexImage(itemImageModel.getHexImage());
			} else {
				itemImage.setBase64Image((appendDataImageTag ? 'data:image/jpeg;base64,' : '') + itemImageModel.getBase64Image());
			}
			itemImageList.push(itemImage);
		}
		return itemImageList;
	};
};
var singletonDataLoader = void 0;
module.exports = function () {
	singletonDataLoader = singletonDataLoader || new DataLoader();
	return singletonDataLoader;
};