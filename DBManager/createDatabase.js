var app = require('electron').remote.app;

var appPath = app.getPath('userData');
var Queries = require('./../Constants/Queries.js');
var ItemType = require('./../Constants/ItemType.js');
var MyString = require('./../Utilities/MyString.js');

var connection = require(__dirname + '/connection.js');
var fs = require('fs');
var newDBToCreate = fs.readFileSync(appPath + '/BusinessNames/currentDB.txt').toString();
var dbHandleForCreation = connection.createDB(newDBToCreate);
var libsqlite3 = connection.libsqlite3;
var ffi = connection.ffi;

var DB_CREATION = {
  setDefaultSettings: function setDefaultSettings() {
    var SettingsModel = require('./../Models/SettingsModel.js');
    var settingsModel = new SettingsModel();
    var listOfKeys = settingsModel.getSettingsKey();
    var lastIndex = listOfKeys.length - 1;
    try {
      var queryInsert = 'insert into ' + Queries.DB_TABLE_SETTINGS + ' (' + Queries.COL_SETTING_KEY + ',' + Queries.COL_SETTING_VALUE + ') values \n                  ' + listOfKeys.reduce(function (acc, key, index) {
        var value = settingsModel.getSettingDefaultValue(key);
        value = value ? '\'' + MyString.escapeStringForDBInsertion(value) + '\'' : null;
        return acc + ('(\'' + key + '\',' + value + ')' + (index != lastIndex ? ',' : ''));
      }, '');
      var conn = libsqlite3.sqlite3_exec(dbHandleForCreation, queryInsert, null, null, null);
    } catch (error) {
      console.log(error);
    }
  },

  createDatabase: function createDatabase() {
    var queryToCreatePaymentTermTable = 'create table ' + Queries.DB_TABLE_PAYMENT_TERMS + ' (\n      ' + Queries.COL_PAYMENT_TERM_ID + ' integer primary key autoincrement,\n      ' + Queries.COL_PAYMENT_TERM_NAME + ' varchar(50) unique,\n      ' + Queries.COL_PAYMENT_TERM_DAYS + ' integer unique,\n      ' + Queries.COL_PAYMENT_TERM_IS_DEFAULT + ' integer default 0)';
    var conn = libsqlite3.sqlite3_exec(dbHandleForCreation, queryToCreatePaymentTermTable, null, null, null);

    // insert default values in payment term table
    var defaultFieldsList = [{ term_id: 1, term_name: 'Due on Receipt', term_days: 0, is_default: 1 }, { term_id: 2, term_name: 'Net 15', term_days: 15, is_default: 0 }, { term_id: 3, term_name: 'Net 30', term_days: 30, is_default: 0 }, { term_id: 4, term_name: 'Net 45', term_days: 45, is_default: 0 }, { term_id: 5, term_name: 'Net 60', term_days: 60, is_default: 0 }];

    for (var i = 0; i < defaultFieldsList.length; i++) {
      var obj = defaultFieldsList[i];
      var query = 'insert into ' + Queries.DB_TABLE_PAYMENT_TERMS + '(\n\t\t\t' + Queries.COL_PAYMENT_TERM_ID + ',\n\t\t\t' + Queries.COL_PAYMENT_TERM_NAME + ',\n\t\t\t' + Queries.COL_PAYMENT_TERM_DAYS + ',\n\t\t\t' + Queries.COL_PAYMENT_TERM_IS_DEFAULT + ')\n\t\t\tVALUES (\n\t\t\t' + obj['term_id'] + ',\n\t\t\t\'' + obj['term_name'] + '\',\n\t\t\t' + obj['term_days'] + ',\n\t\t\t' + obj['is_default'] + ')';
      var conn = libsqlite3.sqlite3_exec(dbHandleForCreation, query, null, null, null);
    }

    var queryCreatePartyGroupTable = 'CREATE TABLE ' + Queries.DB_TABLE_PARTY_GROUPS + ' ( ' + Queries.COL_PARTY_GROUP_ID + ' integer primary key autoincrement, ' + Queries.COL_PARTY_GROUP_NAME + ' varchar(1024) unique)';
    var conn = libsqlite3.sqlite3_exec(dbHandleForCreation, queryCreatePartyGroupTable, null, null, null);

    var defaultPartyGroupInsertQuery = 'insert INTO ' + Queries.DB_TABLE_PARTY_GROUPS + ' values(1,\'General\')';
    libsqlite3.sqlite3_exec(dbHandleForCreation, defaultPartyGroupInsertQuery, null, null, null);

    var queryCreateItemCategoryTable = 'CREATE TABLE ' + Queries.DB_TABLE_ITEM_CATEGORIES + '( ' + Queries.COL_ITEM_CATEGORY_ID + ' integer primary key autoincrement, ' + Queries.COL_ITEM_CATEGORY_NAME + ' varchar(1024) unique)';
    var conn = libsqlite3.sqlite3_exec(dbHandleForCreation, queryCreateItemCategoryTable, null, null, null);

    var defaultItemCategoryInsertQuery = "insert INTO kb_item_categories values(1,'General')";
    libsqlite3.sqlite3_exec(dbHandleForCreation, defaultItemCategoryInsertQuery, null, null, null);

    var nameTableCreateQuery = 'CREATE TABLE ' + Queries.DB_TABLE_NAMES + '( ' + Queries.COL_NAME_ID + ' integer primary key autoincrement ,' + Queries.COL_DATE_CREATED + ' datetime default CURRENT_TIMESTAMP, ' + (Queries.COL_DATE_MODIFIED + ' datetime default CURRENT_TIMESTAMP, ' + Queries.COL_NAME + ' varchar(50), ' + Queries.COL_NAME_PHONE + ' varchar(11), ' + Queries.COL_NAME_EMAIL + ' varchar(50), ') + (Queries.COL_NAME_AMOUNT + ' double, ' + Queries.COL_NAME_REMINDON + ' datetime, ' + Queries.COL_NAME_SENDSMSON + ' datetime, ' + Queries.COL_NAME_IGNORETILL + ' datetime, ' + Queries.COL_NAME_ADDRESS + ' varchar(2000), ') + (Queries.COL_NAME_TYPE + ' integer default 1, ' + Queries.COL_NAME_GROUP + ' integer default 1, ' + Queries.COL_NAME_TIN_NUMBER + ' varchar(20) default \'\', ') + (Queries.COL_NAME_GSTIN_NUMBER + ' varchar(32) default \'\' , ' + Queries.COL_NAME_STATE + '  varchar(32) default \'\',  ') + (Queries.COL_NAME_SHIPPING_ADDRESS + ' VARCHAR(2000) DEFAULT \'\', ' + Queries.COL_NAME_CUSTOMER_TYPE + ' INTEGER DEFAULT 0, ' + Queries.COL_IS_PARTY_DETAILS_SENT + ' integer default 0,') + (Queries.COL_NAME_LAST_TXN_DATE + ' datetime default null,') + (' foreign key(' + Queries.COL_NAME_GROUP + ') references ' + Queries.DB_TABLE_PARTY_GROUPS + '(' + Queries.COL_PARTY_GROUP_ID + '))');

    libsqlite3.sqlite3_exec(dbHandleForCreation, nameTableCreateQuery, null, null, null);

    var imageTableCreateQuery = 'CREATE TABLE ' + Queries.DB_TABLE_TRANSACTION_IMAGE + ' ( ' + Queries.COL_TRANSACTION_IMAGE_ID + ' integer primary key autoincrement, ' + Queries.COL_TRANSACTION_IMAGE + ' BLOB )';
    libsqlite3.sqlite3_exec(dbHandleForCreation, imageTableCreateQuery, null, null, null);

    var firmTableCreateQuery = 'CREATE TABLE ' + Queries.DB_TABLE_FIRMS + ' ( ' + Queries.COL_FIRM_ID + ' integer primary key autoincrement, ' + (Queries.COL_FIRM_NAME + ' varchar(256), ' + Queries.COL_FIRM_INVOICE_PREFIX + ' varchar(10), ' + Queries.COL_FIRM_INVOICE_NUMBER + ' integer default 0, ') + (Queries.COL_FIRM_TAX_INVOICE_PREFIX + ' varchar(10), ' + Queries.COL_FIRM_TAX_INVOICE_NUMBER + ' integer default 0, ' + Queries.COL_FIRM_EMAIL + ' varchar(256), ') + (Queries.COL_FIRM_PHONE + ' varchar(20), ' + Queries.COL_FIRM_ADDRESS + ' varchar(256), ' + Queries.COL_FIRM_TIN_NUM + ' varchar(20), ' + Queries.COL_FIRM_LOGO + ' integer default null, ') + (Queries.COL_FIRM_SIGNATURE + ' integer default null, ' + Queries.COL_FIRM_GSTIN_NUMBER + ' varchar(32) default \'\' ,  ' + Queries.COL_FIRM_STATE + ' varchar(32) default \'\',  ') + (Queries.COL_FIRM_BANK_NAME + ' VARCHAR(32) DEFAULT \'\', ' + Queries.COL_FIRM_BANK_ACCOUNT_NUMBER + ' VARCHAR(32) DEFAULT \'\', ' + Queries.COL_FIRM_BANK_IFSC_CODE + ' VARCHAR(32) DEFAULT \'\',') + (Queries.COL_FIRM_ESTIMATE_PREFIX + ' varchar(10) default \'\', ' + Queries.COL_FIRM_ESTIMATE_NUMBER + ' integer default 0, ') + (Queries.COL_FIRM_CASH_IN_PREFIX + ' varchar(10) default \'\', ') + (Queries.COL_FIRM_DELIVERY_CHALLAN_PREFIX + ' varchar(32) default \'\',') + (Queries.COL_FIRM_UPI_BANK_ACCOUNT_NUMBER + ' varchar(18) default \'\',') + (Queries.COL_FIRM_UPI_BANK_IFSC + ' varchar(11) default \'\',') + (Queries.COL_FIRM_BUSINESS_TYPE + ' integer default 0,') + (Queries.COL_FIRM_BUSINESS_CATEGORY + ' varchar(50) default \'\',') + (' foreign key (' + Queries.COL_FIRM_LOGO + ') references ' + Queries.DB_TABLE_TRANSACTION_IMAGE + '(' + Queries.COL_TRANSACTION_IMAGE_ID + '),') + (' foreign key (' + Queries.COL_FIRM_SIGNATURE + ') references ' + Queries.DB_TABLE_TRANSACTION_IMAGE + '(' + Queries.COL_TRANSACTION_IMAGE_ID + '))');

    libsqlite3.sqlite3_exec(dbHandleForCreation, firmTableCreateQuery, null, null, null);

    var TaxCodeType = require('./../Constants/TaxCodeConstants.js');

    var taxCodeTableCreateQuery = 'create table ' + Queries.DB_TABLE_TAX_CODE + ' ( ' + Queries.COL_TAX_CODE_ID + ' integer primary key autoincrement,\n            ' + Queries.COL_TAX_CODE_NAME + ' varchar(32) unique, ' + Queries.COL_TAX_RATE + ' double, ' + Queries.COL_TAX_CODE_TYPE + ' integer default 0,\n            ' + Queries.COL_TAX_RATE_TYPE + ' integer default ' + TaxCodeType['OTHER'] + ', ' + Queries.COL_TAX_CODE_DATE_CREATED + ' datetime default CURRENT_TIMESTAMP,\n            ' + Queries.COL_TAX_CODE_DATE_MODIFIED + ' datetime default CURRENT_TIMESTAMP)';

    libsqlite3.sqlite3_exec(dbHandleForCreation, taxCodeTableCreateQuery, null, null, null);

    var taxCodeMappingTableCreateQuery = 'create table ' + Queries.DB_TABLE_TAX_MAPPING + ' (\n            ' + Queries.COL_TAX_MAPPING_ID + ' integer primary key autoincrement,\n            ' + Queries.COL_TAX_MAPPING_GROUP_ID + ' integer references ' + Queries.DB_TABLE_TAX_CODE + ' ( ' + Queries.COL_TAX_CODE_ID + ' ),\n            ' + Queries.COL_TAX_MAPPING_CODE_ID + ' integer references ' + Queries.DB_TABLE_TAX_CODE + ' (' + Queries.COL_TAX_CODE_ID + '),\n            ' + Queries.COL_TAX_MAPPING_DATE_CREATED + ' datetime default CURRENT_TIMESTAMP,\n            ' + Queries.COL_TAX_MAPPING_DATE_MODIFIED + ' datetime default CURRENT_TIMESTAMP)';

    libsqlite3.sqlite3_exec(dbHandleForCreation, taxCodeMappingTableCreateQuery, null, null, null);

    var createPrefixTableQuery = 'create table ' + Queries.DB_TABLE_PREFIX + ' (\n      ' + Queries.COL_PREFIX_ID + ' integer primary key autoincrement,\n      ' + Queries.COL_PREFIX_FIRM_ID + ' integer references ' + Queries.DB_TABLE_FIRMS + ' (' + Queries.COL_FIRM_ID + '),\n      ' + Queries.COL_PREFIX_TXN_TYPE + ' integer default 1,\n      ' + Queries.COL_PREFIX_VALUE + ' varchar(50) default \'\',\n      ' + Queries.COL_PREFIX_IS_DEFAULT + ' integer default 0,\n    \tunique(' + Queries.COL_PREFIX_FIRM_ID + ', ' + Queries.COL_PREFIX_TXN_TYPE + ', ' + Queries.COL_PREFIX_VALUE + '))';
    libsqlite3.sqlite3_exec(dbHandleForCreation, createPrefixTableQuery, null, null, null);

    var txnTableCreateQuery = 'create table ' + Queries.DB_TABLE_TRANSACTIONS + '( ' + Queries.COL_TXN_ID + ' integer primary key autoincrement, ' + Queries.COL_TXN_DATE_CREATED + ' datetime default CURRENT_TIMESTAMP, ' + Queries.COL_TXN_DATE_MODIFIED + ' datetime default CURRENT_TIMESTAMP, ' + Queries.COL_TXN_NAME_ID + ' integer, ' + Queries.COL_TXN_CASH_AMOUNT + ' double, ' + Queries.COL_TXN_BALANCE_AMOUNT + ' double, ' + Queries.COL_TXN_TYPE + ' integer, ' + Queries.COL_TXN_DATE + ' date, ' + Queries.COL_TXN_DISCOUNT_PERCENT + ' double, ' + Queries.COL_TXN_TAX_PERCENT + ' double, ' + Queries.COL_TXN_DISCOUNT_AMOUNT + ' double, ' + Queries.COL_TXN_TAX_AMOUNT + ' double, ' + Queries.COL_TXN_DUE_DATE + ' date, ' + Queries.COL_TXN_DESCRIPTION + ' varchar(1024), ' + Queries.COL_TXN_IMAGE_PATH + ' varchar(256), ' + Queries.COL_TXN_PAYMENT_TYPE + ' integer default 1, ' + Queries.COL_TXN_PAYMENT_REFERENCE + " varchar(50) default '', " + Queries.COL_TXN_REF_NUMBER_CHAR + " varchar(50) default '', " + Queries.COL_TXN_STATUS + ' integer default 1, ' + Queries.COL_TXN_AC1_AMOUNT + ' double default 0, ' + Queries.COL_TXN_AC2_AMOUNT + ' double default 0, ' + Queries.COL_TXN_AC3_AMOUNT + ' double default 0, ' + Queries.COL_TXN_FIRM_ID + ' integer default null, ' + Queries.COL_TXN_SUB_TYPE + ' integer default 0, ' + Queries.COL_TXN_INVOICE_PREFIX + ' varchar(10) default null, ' + Queries.COL_TXN_IMAGE_ID + ' integer default 0,  ' + Queries.COL_TXN_TAX_ID + (' INTEGER DEFAULT NULL REFERENCES ' + Queries.DB_TABLE_TAX_CODE + '(' + Queries.COL_TAX_CODE_ID + '), ') + (Queries.COL_TXN_CUSTOM_FIELDS + ' text default \'\', ' + Queries.COL_TXN_DISPLAY_NAME + ' varchar(256) default \'\', ' + Queries.COL_TXN_REVERSE_CHARGE + ' integer default 0, ') + (Queries.COL_TXN_PLACE_OF_SUPPLY + ' varchar(256) default \'\', ' + Queries.COL_TXN_ROUND_OFF_AMOUNT + ' double default 0,') + (Queries.COL_TXN_ITC_APPLICABLE + ' integer default 0, ' + Queries.COL_TXN_PO_DATE + ' date default null, ' + Queries.COL_TXN_PO_REF_NUMBER + ' varchar(50) default \'\',') + (Queries.COL_TXN_RETURN_DATE + ' datetime default null, ' + Queries.COL_TXN_RETURN_REF_NUMBER + ' varchar(50) default \'\',') + (Queries.COL_TXN_EWAY_BILL_NUMBER + ' varchar(50) default \'\',') + (Queries.COL_TXN_CURRENT_BALANCE + ' double default 0, ') + (Queries.COL_TXN_PAYMENT_STATUS + ' integer default 1, ') + (Queries.COL_TXN_PAYMENT_TERM_ID + ' integer default null, ') + (Queries.COL_TXN_PREFIX_ID + ' integer default null references ' + Queries.DB_TABLE_PREFIX + ' (' + Queries.COL_PREFIX_ID + '), ') + (Queries.COL_TXN_TAX_TYPE + ' integer default ' + ItemType.ITEM_TXN_TAX_EXCLUSIVE + ', ') + (Queries.COL_TXN_BILLING_ADDRESS + ' text default \'\', ') + (Queries.COL_TXN_SHIPPING_ADDRESS + ' text default \'\', ') + 'foreign key(' + Queries.COL_TXN_PAYMENT_TERM_ID + ') references ' + Queries.DB_TABLE_PAYMENT_TERMS + '(' + Queries.COL_PAYMENT_TERM_ID + '), ' + 'foreign key(' + Queries.COL_TXN_NAME_ID + ') references ' + Queries.DB_TABLE_NAMES + '(' + Queries.COL_NAME_ID + '), ' + 'foreign key(' + Queries.COL_TXN_FIRM_ID + ') references ' + Queries.DB_TABLE_FIRMS + ' (' + Queries.COL_FIRM_ID + '))';

    libsqlite3.sqlite3_exec(dbHandleForCreation, txnTableCreateQuery, null, null, null);

    var CREATE_CLOSED_LINKED_TXN_DETAILS = 'create table ' + Queries.DB_TABLE_CLOSED_LINK_TXN_TABLE + '( ' + Queries.COL_CLOSED_LINK_TXN_ID + ' integer primary key autoincrement, ' + Queries.COL_CLOSED_LINK_TXN_AMOUNT + ' double default 0, ' + Queries.COL_CLOSED_LINK_TXN_TYPE + ' integer, ' + Queries.COL_CLOSED_LINK_TXN_DATE + ' date, ' + Queries.COL_CLOSED_LINK_TXN_REF_NUMBER + " varchar(50) default '', " + Queries.COL_CLOSED_LINK_TXN_NAME_ID + ' integer default null, ' + 'foreign key(' + Queries.COL_CLOSED_LINK_TXN_NAME_ID + ') references ' + Queries.DB_TABLE_NAMES + '(' + Queries.COL_NAME_ID + '))';

    libsqlite3.sqlite3_exec(dbHandleForCreation, CREATE_CLOSED_LINKED_TXN_DETAILS, null, null, null);

    var txnLinksTableCreateQuery = ' create table ' + Queries.DB_TABLE_TXN_LINKS + '(' + Queries.COL_TXN_LINKS_ID + ' integer primary key autoincrement, ' + Queries.COL_TXN_LINKS_TXN_1_ID + ' integer, ' + Queries.COL_TXN_LINKS_TXN_2_ID + ' integer, ' + Queries.COL_TXN_LINKS_AMOUNT + ' double default 0, ' + Queries.COL_TXN_LINKS_TXN_1_TYPE + ' integer, ' + Queries.COL_TXN_LINKS_TXN_2_TYPE + ' integer, ' + Queries.COL_TXN_LINKS_CLOSED_TXN_REF_ID + ' integer default null, ' + 'foreign key(' + Queries.COL_TXN_LINKS_TXN_1_ID + ') references ' + Queries.DB_TABLE_TRANSACTIONS + '(' + Queries.COL_TXN_ID + '), ' + 'foreign key(' + Queries.COL_TXN_LINKS_TXN_2_ID + ') references ' + Queries.DB_TABLE_TRANSACTIONS + '(' + Queries.COL_TXN_ID + '), ' + 'foreign key (' + Queries.COL_TXN_LINKS_CLOSED_TXN_REF_ID + ') references ' + Queries.DB_TABLE_CLOSED_LINK_TXN_TABLE + '(' + Queries.COL_CLOSED_LINK_TXN_ID + '))';

    var conn = libsqlite3.sqlite3_exec(dbHandleForCreation, txnLinksTableCreateQuery, null, null, null);

    var settingTableCreateQuery = 'CREATE TABLE ' + Queries.DB_TABLE_SETTINGS + '( ' + Queries.COL_SETTING_ID + ' integer primary key autoincrement ,' + Queries.COL_SETTING_KEY + ' varchar(256),' + (' ' + Queries.COL_SETTING_VALUE + ' varchar(1024), unique(' + Queries.COL_SETTING_KEY + '))');

    libsqlite3.sqlite3_exec(dbHandleForCreation, settingTableCreateQuery, null, null, null);

    var ftsTableCreateQuery = 'CREATE VIRTUAL TABLE ' + Queries.FTS_VTABLE + ' USING fts3 (' + Queries.FTS_NAME_ID + ', ' + Queries.FTS_TXN_ID + ', ' + Queries.FTS_TEXT + ')';

    libsqlite3.sqlite3_exec(dbHandleForCreation, ftsTableCreateQuery, null, null, null);

    var itemTableCreateQuery = 'create table ' + Queries.DB_TABLE_ITEMS + '( ' + Queries.COL_ITEM_ID + ' integer primary key autoincrement ,' + Queries.COL_ITEM_NAME + ' varchar(256), ' + Queries.COL_ITEM_SALE_UNIT_PRICE + ' double, ' + Queries.COL_ITEM_PURCHASE_UNIT_PRICE + ' double, ' + Queries.COL_ITEM_STOCK_QUANTITY + ' double default 0, ' + Queries.COL_ITEM_MINIMUM_STOCK_QUANTITY + ' double default 0, ' + Queries.COL_ITEM_LOCATION + " varchar(256) default '', " + Queries.COL_ITEM_STOCK_VALUE + ' double default 0, ' + Queries.COL_ITEM_DATE_CREATED + ' datetime default CURRENT_TIMESTAMP, ' + Queries.COL_ITEM_DATE_MODIFIED + ' datetime default CURRENT_TIMESTAMP, ' + Queries.COL_ITEM_TYPE + ' integer default 1, ' + Queries.COL_ITEM_CATEGORY + ' integer default 1, ' + Queries.COL_ITEM_CODE + ' varchar(32) unique default null, ' + Queries.COL_ITEM_BASE_UNIT_ID + ' integer references ' + Queries.DB_TABLE_ITEM_UNITS + '(' + Queries.COL_UNIT_ID + '), ' + Queries.COL_ITEM_SECONDARY_UNIT_ID + ' integer references ' + Queries.DB_TABLE_ITEM_UNITS + '(' + Queries.COL_UNIT_ID + '), ' + Queries.COL_ITEM_UNIT_MAPPING_ID + ' integer references ' + Queries.DB_TABLE_ITEM_UNITS_MAPPING + '(' + Queries.COL_UNIT_MAPPING_ID + '), ' + (Queries.COL_ITEM_HSN_SAC_CODE + ' varchar(32) default \'\', ' + Queries.COL_ITEM_TAX_ID + ' INTEGER DEFAULT NULL REFERENCES ' + Queries.DB_TABLE_TAX_CODE + ' (' + Queries.COL_TAX_CODE_ID + '),') + (Queries.COL_ITEM_TAX_TYPE_SALE + ' integer default ' + ItemType.ITEM_TXN_TAX_EXCLUSIVE + ', ' + Queries.COL_ITEM_ADDITIONAL_CESS_PER_UNIT + ' double default null,') + (Queries.COL_ITEM_DESCRIPTION + ' varchar(256) default \'\', ') + Queries.COL_ITEM_IS_ACTIVE + ' integer default 1, ' + (Queries.COL_ITEM_TAX_TYPE_PURCHASE + ' integer default ' + ItemType.ITEM_TXN_TAX_EXCLUSIVE + ', ') + 'foreign key(' + Queries.COL_ITEM_CATEGORY + ') references ' + Queries.DB_TABLE_ITEM_CATEGORIES + '(' + Queries.COL_ITEM_CATEGORY_ID + '))';

    libsqlite3.sqlite3_exec(dbHandleForCreation, itemTableCreateQuery, null, null, null);

    var itemAdjustmentTableCreateQuery = 'CREATE TABLE ' + Queries.DB_TABLE_ITEM_ADJUSTMENT + '(' + Queries.COL_ITEM_ADJ_ID + ' integer primary key autoincrement, ' + Queries.COL_ITEM_ADJ_ITEM_ID + ' integer, ' + (Queries.COL_ITEM_ADJ_TYPE + ' integer, ' + Queries.COL_ITEM_ADJ_QUANTITY + ' double default 0, ' + Queries.COL_ITEM_ADJ_DATE + ' date, ' + Queries.COL_ITEM_ADJ_DESCRIPTION + ' varchar(1024), ') + (Queries.COL_ITEM_ADJ_DATE_CREATED + ' datetime default CURRENT_TIMESTAMP, ' + Queries.COL_ITEM_ADJ_DATE_MODIFIED + ' datetime default CURRENT_TIMESTAMP, ' + Queries.COL_ITEM_ADJ_ATPRICE + ' double default -1, ' + Queries.COL_ITEM_ADJ_IST_ID + ' integer default null references ' + Queries.DB_ITEM_STOCK_TRACKING_TABLE + '(' + Queries.COL_IST_ID + '), ') + ('foreign key(' + Queries.COL_ITEM_ADJ_ITEM_ID + ') references ' + Queries.DB_TABLE_ITEMS + '(' + Queries.COL_ITEM_ID + '))');

    libsqlite3.sqlite3_exec(dbHandleForCreation, itemAdjustmentTableCreateQuery, null, null, null);

    var partyItemRateTableCreateQuery = 'create table ' + Queries.DB_TABLE_PARTY_ITEM_RATE + '( ' + Queries.COL_PARTY_ITEM_RATE_ID + '  integer primary key autoincrement, ' + Queries.COL_PARTY_ITEM_ITEM_ID + ' integer, ' + Queries.COL_PARTY_ITEM_PARTY_ID + ' integer, ' + Queries.COL_PARTY_ITEM_SALE_PRICE + ' double default 0, ' + Queries.COL_PARTY_ITEM_PURCHASE_PRICE + ' double default 0, foreign key(' + Queries.COL_PARTY_ITEM_ITEM_ID + ') references ' + Queries.DB_TABLE_ITEMS + '(' + Queries.COL_ITEM_ID + '), ' + 'foreign key(' + Queries.COL_PARTY_ITEM_PARTY_ID + ') references ' + Queries.DB_TABLE_NAMES + '(' + Queries.COL_NAME_ID + '), unique(' + Queries.COL_PARTY_ITEM_ITEM_ID + ',' + Queries.COL_PARTY_ITEM_PARTY_ID + ') on conflict replace)';

    libsqlite3.sqlite3_exec(dbHandleForCreation, partyItemRateTableCreateQuery, null, null, null);

    var paymentTableCreateQuery = 'CREATE TABLE ' + Queries.DB_TABLE_PAYMENT_TYPE + '( ' + Queries.COL_PAYMENT_TYPE_ID + ' integer primary key autoincrement, ' + ('  ' + Queries.COL_PAYMENT_TYPE_TYPE + ' varchar(30) DEFAULT \'BANK\', ' + Queries.COL_PAYMENT_TYPE_NAME + ' varchar(30) UNIQUE, ') + (' ' + Queries.COL_PAYMENT_TYPE_BANK_NAME + ' varchar(30) DEFAULT \'\', ' + Queries.COL_PAYMENT_TYPE_ACCOUNT_NUMBER + ' varchar(30) DEFAULT \'\', ') + (' ' + Queries.COL_PAYMENT_TYPE_OPENING_BALANCE + ' double default 0, ' + Queries.COL_PAYMENT_TYPE_OPENING_DATE + ' datetime default CURRENT_TIMESTAMP)');

    libsqlite3.sqlite3_exec(dbHandleForCreation, paymentTableCreateQuery, null, null, null);

    var defaultPaymentValueInsertQuery = 'insert into ' + Queries.DB_TABLE_PAYMENT_TYPE + '(' + Queries.COL_PAYMENT_TYPE_ID + ', ' + Queries.COL_PAYMENT_TYPE_TYPE + ', ' + Queries.COL_PAYMENT_TYPE_NAME + ') values(1, \'CASH\', \'Cash\'), (2, \'CHEQUE\', \'Cheque\')';
    libsqlite3.sqlite3_exec(dbHandleForCreation, defaultPaymentValueInsertQuery, null, null, null);

    var bankAdjustmentTableCreateQuery = 'CREATE TABLE ' + Queries.DB_TABLE_BANK_ADJUSTMENT + '( ' + Queries.COL_BANK_ADJ_ID + ' integer primary key autoincrement, ' + (' ' + Queries.COL_BANK_ADJ_BANK_ID + ' integer, ' + Queries.COL_BANK_ADJ_TYPE + ' integer, ' + Queries.COL_BANK_ADJ_AMOUNT + ' double default 0, ' + Queries.COL_BANK_ADJ_DATE + ' date, ') + (' ' + Queries.COL_BANK_ADJ_DESCRIPTION + ' varchar(1024) default \'\',  ' + Queries.COL_BANK_ADJ_TO_BANK_ID + '  integer references ' + Queries.DB_TABLE_PAYMENT_TYPE + '(' + Queries.COL_PAYMENT_TYPE_ID + '), ') + (' foreign key(' + Queries.COL_BANK_ADJ_BANK_ID + ') references ' + Queries.DB_TABLE_PAYMENT_TYPE + '(' + Queries.COL_PAYMENT_TYPE_ID + '))');
    libsqlite3.sqlite3_exec(dbHandleForCreation, bankAdjustmentTableCreateQuery, null, null, null);

    var cashAdjustmentTableCreateQuery = 'create table ' + Queries.DB_TABLE_CASH_ADJUSTMENT + '( ' + Queries.COL_CASH_ADJ_ID + ' integer primary key autoincrement, ' + Queries.COL_CASH_ADJ_TYPE + ' integer, ' + Queries.COL_CASH_ADJ_AMOUNT + ' double default 0, ' + Queries.COL_CASH_ADJ_DATE + ' date, ' + Queries.COL_CASH_ADJ_DESCRIPTION + " varchar(1024) default '')";

    libsqlite3.sqlite3_exec(dbHandleForCreation, cashAdjustmentTableCreateQuery, null, null, null);

    var chequeStatusTableCreateQuery = 'CREATE TABLE ' + Queries.DB_TABLE_CHEQUE_STATUS + '( ' + Queries.COL_CHEQUE_ID + ' integer primary key autoincrement, ' + Queries.COL_CHEQUE_TXN_ID + ' integer, ' + (' ' + Queries.COL_CHEQUE_CURRENT_STATUS + ' integer default 0, ' + Queries.COL_CHEQUE_TRANSFER_DATE + ' date default CURRENT_TIMESTAMP, ') + (' ' + Queries.COL_CHEQUE_CLOSE_DESCRIPTION + ' varchar(1024) default \'\',' + Queries.COL_CHEQUE_TRANSFERRED_TO_ACCOUNT + ' integer default 2, ') + (' ' + Queries.COL_CHEQUE_CREATION_DATE + ' date default CURRENT_TIMESTAMP, ' + Queries.COL_CHECK_MODIFICATION_DATE + ' date default CURRENT_TIMESTAMP, ') + (' ' + Queries.COL_CHEQUE_CLOSED_LINK_TXN_REF_ID + ' integer default null,') + (' foreign key(' + Queries.COL_CHEQUE_TXN_ID + ') references ' + Queries.DB_TABLE_TRANSACTIONS + '(' + Queries.COL_TXN_ID + ')') + (' foreign key (' + Queries.COL_CHEQUE_CLOSED_LINK_TXN_REF_ID + ') references ' + Queries.DB_TABLE_CLOSED_LINK_TXN_TABLE + ' (' + Queries.COL_CLOSED_LINK_TXN_ID + '))');

    libsqlite3.sqlite3_exec(dbHandleForCreation, chequeStatusTableCreateQuery, null, null, null);

    debugger;

    var txnMsgConfigTableCreateQuery = 'create table ' + Queries.DB_TABLE_TXN_MESSAGE_CONFIG + ' ( ' + Queries.COL_TXN_MESSAGE_TXN_TYPE + ' integer, ' + Queries.COL_TXN_MESSAGE_FIELD_ID + ' integer, ' + Queries.COL_TXN_MESSAGE_FIELD_NAME + ' varchar(50), ' + Queries.COL_TXN_MESSAGE_FIELD_VALUE + ' varchar(256), primary key(' + Queries.COL_TXN_MESSAGE_TXN_TYPE + ',' + Queries.COL_TXN_MESSAGE_FIELD_ID + '))';

    libsqlite3.sqlite3_exec(dbHandleForCreation, txnMsgConfigTableCreateQuery, null, null, null);
    var txnMsgDefaultConfigInsertQuery = 'INSERT INTO ' + Queries.DB_TABLE_TXN_MESSAGE_CONFIG + ' \n        (' + Queries.COL_TXN_MESSAGE_FIELD_VALUE + ', ' + Queries.COL_TXN_MESSAGE_FIELD_NAME + ', ' + Queries.COL_TXN_MESSAGE_FIELD_ID + ', ' + Queries.COL_TXN_MESSAGE_TXN_TYPE + ') \n        VALUES \n        \n            (0,\'Date\',1,1),\n            (0,\'Invoice No.\',2,1),\n            (0,\'Description\',3,1),\n            (1,\'Amount\',4,1),\n            (1,\'Received\',5,1),\n            (1,\'Balance\',6,1),\n            (1,\'Total Balance\',7,1),\n            (0,\'Item Details\',8,1),\n            (0,\'Payment mode\',9,1),\n            (0,\'Transportation Detail\',14,1),\n            (\'Thanks for your purchase with us!!\nPurchase Details:\',\'Header\',11,1),\n            (\'\',\'Footer\',12,1),\n        \n            (0,\'Date\',1,27),\n            (0,\'Estimate No.\',2,27),\n            (0,\'Description\',3,27),\n            (1,\'Amount\',4,27),\n            (0,\'Item Details\',8,27),\n            (\'Thanks for your query!!\nEstimate Details:\',\'Header\',11,27),\n            (\'\',\'Footer\',12,27),\n            \n            (0,\'Date\',1,30),\n            (0,\'Challan No.\',2,30),\n            (0,\'Description\',3,30),\n            (1,\'Amount\',4,30),\n            (0,\'Item Details\',8,30),\n            (\'Delivery has been initiated!!\nDelivery Challan Details:\',\'Header\',11,30),\n            (\'\',\'Footer\',12,30),\n            \n            (0,\'Date\',1,2),\n            (0,\'Bill No.\',2,2),\n            (0,\'Description\',3,2),\n            (1,\'Amount\',4,2),\n            (1,\'Paid\',5,2),\n            (1,\'Balance\',6,2),\n            (1,\'Total Balance\',7,2),\n            (0,\'Item Details\',8,2),\n            (0,\'Payment mode\',9,2),\n            (0,\'Transportation Detail\',14,2),\n            (\'We have made a purchase with you.\nPurchase Details:\',\'Header\',11,2),\n            (\'\',\'Footer\',12,2),\n            \n            (0,\'Date\',1,28),\n            (0,\'Order No.\',2,28),\n            (0,\'Description\',3,28),\n            (1,\'Amount\',4,28),\n            (1,\'Paid\',5,28),\n            (1,\'Balance\',6,28),\n            (1,\'Total Balance\',7,28),\n            (0,\'Item Details\',8,28),\n            (0,\'Payment mode\',9,28),\n            (0,\'Transportation Detail\',14,28),\n            (0,\'Due Date\',13,28),\n            (\'We have placed an order with you.\nOrder Details:\',\'Header\',11,28),\n            (\'\',\'Footer\',12,28),\n            \n            (0,\'Date\',1,3),\n            (0,\'Description\',3,3),\n            (1,\'Received\',5,3),\n            (1,\'Total Balance\',7,3),\n            (0,\'Payment mode\',9,3),\n            (\'Thanks for making a payment to us!!\nPayment Details:\',\'Header\',11,3),\n            (\'\',\'Footer\',12,3),\n            (0,\'Date\',1,4),\n            (0,\'Description\',3,4),\n            (1,\'Paid\',5,4),\n            (1,\'Total Balance\',7,4),\n            (0,\'Payment mode\',9,4),\n            (\'We have made a payment to you.\nPayment Details:\',\'Header\',11,4),\n            (\'\',\'Footer\',12,4),\n            \n            (0,\'Date\',1,21),\n            (0,\'Return No.\',2,21),\n            (0,\'Description\',3,21),\n            (1,\'Amount\',4,21),\n            (1,\'Paid\',5,21),\n            (1,\'Balance\',6,21),\n            (1,\'Total Balance\',7,21),\n            (0,\'Item Details\',8,21),\n            (0,\'Payment mode\',9,21),\n            (0,\'Transportation Detail\',14,21),\n            (\'We have made a credit note to you.\nReturn Details:\',\'Header\',11,21),\n            (\'\',\'Footer\',12,21),\n            \n            (0,\'Date\',1,23),\n            (0,\'Return No.\',2,23),\n            (0,\'Description\',3,  23),\n            (1,\'Amount\',4,23),\n            (1,\'Received\',5,23),\n            (1,\'Balance\',6,23),\n            (1,\'Total Balance\',7,23),\n            (0,\'Item Details\',8,23),\n            (0,\'Payment mode\',9,23),\n            (0,\'Transportation Detail\',14,23),\n            (\'We have created a debit note to you.\nReturn Details:\',\'Header\',11,23),\n            (\'\',\'Footer\',12,23),\n            \n            (0,\'Date\',1,24),\n            (0,\'Order No.\',2,24),\n            (0,\'Description\',3,24),\n            (1,\'Amount\',4,24),\n            (1,\'Advance\',5,24),\n            (1,\'Balance\',6,24),\n            (0,\'Item Details\',8,24),\n            (0,\'Payment mode\',9,24),\n            (0,\'Transportation Detail\',14,24),\n            (\'Thanks for placing order with us.\nOrder Details:\',\'Header\',11,24),\n            (\'\',\'Footer\',12,24),\n            (0,\'Due Date\',13,24)';

    libsqlite3.sqlite3_exec(dbHandleForCreation, txnMsgDefaultConfigInsertQuery, null, null, null);

    var extraChargeTableCreateQuery = 'CREATE TABLE ' + Queries.DB_TABLE_EXTRA_CHARGES + '(' + Queries.COL_EXTRA_CHARGES_ID + ' integer primary key autoincrement, ' + Queries.COL_EXTRA_CHARGES_NAME + ' varchar(1024))';
    libsqlite3.sqlite3_exec(dbHandleForCreation, extraChargeTableCreateQuery, null, null, null);

    var itemUnitTableCreateQuery = 'create table ' + Queries.DB_TABLE_ITEM_UNITS + '(' + Queries.COL_UNIT_ID + ' integer primary key autoincrement, ' + Queries.COL_UNIT_NAME + ' varchar(50) unique, ' + Queries.COL_UNIT_SHORT_NAME + ' varchar(10) unique, ' + Queries.COL_UNIT_FULL_NAME_EDITABLE + ' integer default 1, ' + Queries.COL_UNIT_UNIT_DELETABLE + ' integer default 1)';
    libsqlite3.sqlite3_exec(dbHandleForCreation, itemUnitTableCreateQuery, null, null, null);

    var itemUnitMappingTableCreateQuery = 'create table ' + Queries.DB_TABLE_ITEM_UNITS_MAPPING + '(' + Queries.COL_UNIT_MAPPING_ID + ' integer primary key autoincrement, ' + Queries.COL_BASE_UNIT_ID + ' integer references ' + Queries.DB_TABLE_ITEM_UNITS + '(' + Queries.COL_UNIT_ID + '), ' + Queries.COL_SECONDARY_UNIT_ID + ' integer references ' + Queries.DB_TABLE_ITEM_UNITS + '(' + Queries.COL_UNIT_ID + '), ' + Queries.COL_CONVERSION_RATE + ' double)';

    libsqlite3.sqlite3_exec(dbHandleForCreation, itemUnitMappingTableCreateQuery, null, null, null);

    var CREATE_ITEM_STOCK_TRACKING_TABLE = 'create table ' + Queries.DB_ITEM_STOCK_TRACKING_TABLE + '(' + Queries.COL_IST_ID + ' integer primary key autoincrement, ' + Queries.COL_IST_BATCH_NUMBER + " varchar(30) default '', " + Queries.COL_IST_SERIAL_NUMBER + " varchar(30) default '', " + Queries.COL_IST_MRP + ' double default 0, ' + Queries.COL_IST_EXPIRY_DATE + ' datetime default null, ' + Queries.COL_IST_MANUFACTURING_DATE + ' datetime default null, ' + Queries.COL_IST_SIZE + " varchar(100) default '', " + Queries.COL_IST_ITEM_ID + ' integer  references ' + Queries.DB_TABLE_ITEMS + '(' + Queries.COL_ITEM_ID + '), ' + Queries.COL_IST_CURRENT_QUANTITY + ' double default 0, ' + Queries.COL_IST_OPENING_QUANTITY + ' double default 0, ' + Queries.COL_IST_TYPE + ' integer default 0 )';

    libsqlite3.sqlite3_exec(dbHandleForCreation, CREATE_ITEM_STOCK_TRACKING_TABLE, null, null, null);

    var lineItemTableCreateQuery = 'create table ' + Queries.DB_TABLE_LINEITEMS + '( ' + Queries.COL_LINEITEM_ID + ' integer primary key autoincrement ,' + Queries.COL_LINEITEM_TXN_ID + ' integer, ' + Queries.COL_LINEITEM_ITEM_ID + ' integer, ' + Queries.COL_LINEITEM_QUANTITY + ' double, ' + Queries.COL_LINEITEM_UNITPRICE + ' double, ' + Queries.COL_LINEITEM_TOTAL + ' double, ' + Queries.COL_LINEITEM_TAX_AMOUNT + ' double default 0, ' + Queries.COL_LINEITEM_DISCOUNT_AMOUNT + ' double default 0, ' + Queries.COL_LINEITEM_UNIT_ID + ' integer references ' + Queries.DB_TABLE_ITEM_UNITS + '(' + Queries.COL_UNIT_ID + '), ' + Queries.COL_LINEITEM_UNIT_MAPPING_ID + ' integer references ' + Queries.DB_TABLE_ITEM_UNITS_MAPPING + '(' + Queries.COL_UNIT_MAPPING_ID + '), ' + Queries.COL_LINEITEM_TAX_ID + ' integer default null references ' + Queries.DB_TABLE_TAX_CODE + '(' + Queries.COL_TAX_CODE_ID + '), ' + Queries.COL_LINEITEM_MRP + ' double default null, ' + Queries.COL_LINEITEM_BATCH_NUMBER + " varchar(30) default '', " + Queries.COL_LINEITEM_EXPIRY_DATE + ' datetime default null, ' + Queries.COL_LINEITEM_MANUFACTURING_DATE + ' datetime default null, ' + Queries.COL_LINEITEM_SERIAL_NUMBER + " varchar(30) default '', " + Queries.COL_LINEITEM_COUNT + ' double default null, ' + Queries.COL_LINEITEM_DESCRIPTION + " varchar(1024) default '', " + Queries.COL_LINEITEM_ADDITIONAL_CESS + ' double default null, ' + Queries.COL_LINEITEM_TOTAL_AMOUNT_EDITED + ' integer default 0, ' + Queries.COL_LINEITEM_ITC_APPLICABLE + ' integer default 0, ' + (Queries.COL_LINEITEM_SIZE + ' varchar(100) default \'\', ' + Queries.COL_LINEITEM_IST_ID + ' integer default null references ' + Queries.DB_ITEM_STOCK_TRACKING_TABLE + '(' + Queries.COL_IST_ID + '), ') + Queries.COL_LINEITEM_FREE_QUANTITY + ' double default 0, ' + Queries.COL_LINEITEM_DISCOUNT_PERCENT + ' double default 0, ' + 'foreign key(' + Queries.COL_LINEITEM_TXN_ID + ') references ' + Queries.DB_TABLE_TRANSACTIONS + '(' + Queries.COL_TXN_ID + '), ' + 'foreign key(' + Queries.COL_LINEITEM_ITEM_ID + ') references ' + Queries.DB_TABLE_ITEMS + '(' + Queries.COL_ITEM_ID + '))';

    libsqlite3.sqlite3_exec(dbHandleForCreation, lineItemTableCreateQuery, null, null, null);

    var CustomFieldsType = require('./../Constants/CustomFieldsType.js');

    var createCustomFieldsTable = 'CREATE TABLE ' + Queries.DB_TABLE_CUSTOM_FIELDS + '(\n            ' + Queries.COL_CUSTOM_FIELDS_ID + ' INTEGER PRIMARY KEY AUTOINCREMENT,\n            ' + Queries.COL_CUSTOM_FIELDS_DISPLAY_NAME + ' VARCHAR(32) DEFAULT \'\',\n            ' + Queries.COL_CUSTOM_FIELDS_TYPE + ' INTEGER DEFAULT NULL,\n            ' + Queries.COL_CUSTOM_FIELDS_VISIBILITY + ' INTEGER DEFAULT 0)';

    libsqlite3.sqlite3_exec(dbHandleForCreation, createCustomFieldsTable, null, null, null);

    // insert default values in custom fields table
    var customFieldList = [{ 'name': 'Transport Name', 'type': CustomFieldsType.delivery, 'visibility': 0 }, { 'name': 'Vehicle Number', 'type': CustomFieldsType.delivery, 'visibility': 0 }, { 'name': 'Delivery Date', 'type': CustomFieldsType.delivery, 'visibility': 0 }, { 'name': 'Delivery Location', 'type': CustomFieldsType.delivery, 'visibility': 0 }, { 'name': 'Field 5', 'type': CustomFieldsType.delivery, 'visibility': 0 }, { 'name': 'Field 6', 'type': CustomFieldsType.delivery, 'visibility': 0 }];

    customFieldList.forEach(function (obj) {
      var query = 'INSERT INTO ' + Queries.DB_TABLE_CUSTOM_FIELDS + '(' + Queries.COL_CUSTOM_FIELDS_DISPLAY_NAME + ', ' + Queries.COL_CUSTOM_FIELDS_TYPE + ', ' + Queries.COL_CUSTOM_FIELDS_VISIBILITY + ')\n                VALUES (\'' + obj['name'] + '\', ' + obj['type'] + ', ' + obj['visibility'] + ')';
      var conn = libsqlite3.sqlite3_exec(dbHandleForCreation, query, null, null, null);
    });

    var logTableCreateQuery = 'create table ' + Queries.DB_TABLE_LOG + '(' + Queries.COL_LOG_ID + ' integer primary key autoincrement, ' + Queries.COL_LOG_REASON + " varchar(1024) default '', " + Queries.COL_LOG_DETAILS + " varchar(1024) default '') ";

    libsqlite3.sqlite3_exec(dbHandleForCreation, logTableCreateQuery, null, null, null);

    var defaultExtraChargesKeys = ['Shipping', 'Packaging', 'Adjustment'];
    defaultExtraChargesKeys.forEach(function (value) {
      var newQuery = 'INSERT INTO ' + Queries.DB_TABLE_EXTRA_CHARGES + ' (' + Queries.COL_EXTRA_CHARGES_NAME + ') ' + " values('" + value + "')";
      libsqlite3.sqlite3_exec(dbHandleForCreation, newQuery, null, null, null);
    });

    var GSTRUnits = require('./../Constants/GSTRUnits.js');
    var gstrUnitV28Map = GSTRUnits.get(28);
    gstrUnitV28Map.forEach(function (gstrUnit, key) {
      try {
        var shortName = gstrUnit[0];
        var fullName = gstrUnit[1];

        var queryInsert = 'INSERT INTO ' + Queries.DB_TABLE_ITEM_UNITS + '(' + Queries.COL_UNIT_NAME + ', ' + Queries.COL_UNIT_SHORT_NAME + ', ' + Queries.COL_UNIT_FULL_NAME_EDITABLE + ', ' + Queries.COL_UNIT_UNIT_DELETABLE + ') values(' + ('\'' + MyString.escapeStringForDBInsertion(fullName) + '\'') + ', ' + ('\'' + MyString.escapeStringForDBInsertion(shortName) + '\'') + ', 0, 0)';
        var _conn = libsqlite3.sqlite3_exec(dbHandleForCreation, queryInsert, null, null, null);
      } catch (err) {}
    });
    var CREATE_UDF_FIELDS_TABLE = 'create table ' + Queries.DB_TABLE_UDF_FIELDS + ' (\n\t\t\t' + Queries.COL_UDF_FIELD_ID + ' integer primary key autoincrement, \n\t\t\t' + Queries.COL_UDF_FIELD_NAME + ' varchar(150) default \'\', \n\t\t\t' + Queries.COL_UDF_FIELD_TYPE + ' integer default 0, \n\t\t\t' + Queries.COL_UDF_FIELD_DATA_TYPE + ' integer default 0, \n\t\t\t' + Queries.COL_UDF_FIELD_DATA_FORMAT + ' integer default 0, \n\t\t\t' + Queries.COL_PRINT_ON_INVOICE + ' integer default 0, \n\t\t\t' + Queries.COL_UDF_TXN_TYPE + '\tinteger default 0, \n\t\t\t' + Queries.COL_FIELD_NUMBER + ' integer default 0, \n\t\t\t' + Queries.COL_UDF_FIELD_STATUS + ' integer default 0, \n\t\t\t' + Queries.COL_UDF_FIRM_ID + ' integer default null,  \n\t\t\tunique(' + Queries.COL_UDF_FIRM_ID + ', ' + Queries.COL_UDF_FIELD_TYPE + ', ' + Queries.COL_UDF_TXN_TYPE + ', ' + Queries.COL_FIELD_NUMBER + '))';
    libsqlite3.sqlite3_exec(dbHandleForCreation, CREATE_UDF_FIELDS_TABLE, null, null, null);

    var CREATE_UDF_VALUES_TABLE = 'create table ' + Queries.DB_TABLE_UDF_VALUES + ' (\n\t\t\t' + Queries.COL_UDF_VALUE_ID + ' integer primary key autoincrement, \n            ' + Queries.COL_UDF_VALUE_FIELD_ID + ' integer  references ' + Queries.DB_TABLE_UDF_FIELDS + ' (' + Queries.COL_UDF_FIELD_ID + '), \n\t\t\t' + Queries.COL_UDF_REF_ID + ' integer default 0, \n\t\t\t' + Queries.COL_UDF_VALUE + ' varchar(150) default \'\', \n\t\t\t' + Queries.COL_UDF_VALUE_FIELD_TYPE + ' integer default 0, \n\t\t\tunique( ' + Queries.COL_UDF_VALUE_FIELD_ID + ', ' + Queries.COL_UDF_REF_ID + '))';
    libsqlite3.sqlite3_exec(dbHandleForCreation, CREATE_UDF_VALUES_TABLE, null, null, null);

    var linkedTransactionQuery = 'create table ' + Queries.DB_TABLE_LINKED_TRANSACTION + ' (\n        ' + Queries.COL_LINKED_ID + ' integer primary key autoincrement,\n        ' + Queries.COL_TRANSACTION_SOURCE_ID + ' integer NOT NULL, \n        ' + Queries.COL_TRANSACTION_DESTINATION_ID + ' integer NOT NULL,\n        ' + Queries.COL_LINKED_DATE + ' DATE NOT NULL DEFAULT (datetime(\'now\')),\n        FOREIGN KEY(' + Queries.COL_TRANSACTION_SOURCE_ID + ') REFERENCES ' + Queries.DB_TABLE_TRANSACTIONS + '(' + Queries.COL_TXN_ID + '),\n        FOREIGN KEY(' + Queries.COL_TRANSACTION_SOURCE_ID + ') REFERENCES ' + Queries.DB_TABLE_TRANSACTIONS + '(' + Queries.COL_TXN_ID + ')\n         )';

    libsqlite3.sqlite3_exec(dbHandleForCreation, linkedTransactionQuery, null, null, null);

    var loanAccounts = 'CREATE TABLE "' + Queries.DB_TABLE_LOAN_ACCOUNTS + '" (\n                  "' + Queries.COL_LOAN_ACCOUNT_ID + '"\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n                  "' + Queries.COL_LOAN_ACCOUNT_NAME + '"\tTEXT NOT NULL UNIQUE,\n                  "' + Queries.COL_LOAN_ACCOUNT_LENDER + '"\tTEXT,\n                  "' + Queries.COL_LOAN_ACCOUNT_NUMBER + '"\tTEXT,\n                  "' + Queries.COL_LOAN_ACCOUNT_FIRM_ID + '" INTEGER NOT NULL,\n                  "' + Queries.COL_LOAN_ACCOUNT_DESC + '"\tTEXT,\n                  "' + Queries.COL_LOAN_ACCOUNT_STARTING_BAL + '"\tDOUBLE NOT NULL,\n                  "' + Queries.COL_LOAN_ACCOUNT_OPENING_DATE + '"\tDATE NOT NULL,\n                  "' + Queries.COL_LOAN_ACCOUNT_MODIFIED_DATE + '"\tDATETIME NOT NULL,\n                  "' + Queries.COL_LOAN_ACCOUNT_CREATED_DATE + '"\tDATETIME NOT NULL,\n                  "' + Queries.COL_LOAN_ACCOUNT_INTEREST_RATE + '"\tDOUBLE,\n                  "' + Queries.COL_LOAN_ACCOUNT_TERM_DURATION + '"\tINTEGER,\n                  FOREIGN KEY("' + Queries.COL_LOAN_ACCOUNT_FIRM_ID + '") REFERENCES "' + Queries.DB_TABLE_FIRMS + '" ("' + Queries.COL_FIRM_ID + '")\n            )';
    libsqlite3.sqlite3_exec(dbHandleForCreation, loanAccounts, null, null, null);

    var loanTransactions = 'CREATE TABLE "' + Queries.DB_TABLE_LOAN_TRANSACTIONS + '" (\n                  "' + Queries.COL_LOAN_TXN_ID + '"\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n                  "' + Queries.COL_LOAN_ACCOUNT_ID + '"\tINTEGER NOT NULL,\n                  "' + Queries.COL_LOAN_TXN_TYPE + '"\tINTEGER NOT NULL,\n                  "' + Queries.COL_LOAN_TXN_PRINCIPAL + '"\tDOUBLE NOT NULL,\n                  "' + Queries.COL_LOAN_TXN_INTEREST + '"\tDOUBLE NOT NULL,\n                  "' + Queries.COL_LOAN_TXN_PAYMENT_ID + '"      INTEGER NOT NULL,\n                  "' + Queries.COL_LOAN_TXN_DATE + '"\tDATE NOT NULL,\n                  "' + Queries.COL_LOAN_TXN_CREATED_DATE + '"\tDATETIME NOT NULL,\n                  "' + Queries.COL_LOAN_TXN_MODIFIED_DATE + '"\tDATETIME NOT NULL,\n                  "' + Queries.COL_LOAN_TXN_DESC + '"\tTEXT,\n                  "' + Queries.COL_LOAN_TXN_IMAGE_ID + '"\tINTEGER DEFAULT NULL,\n                  FOREIGN KEY("' + Queries.COL_LOAN_ACCOUNT_ID + '") REFERENCES "' + Queries.DB_TABLE_LOAN_ACCOUNTS + '" ("' + Queries.COL_LOAN_ACCOUNT_ID + '"),\n                  FOREIGN KEY("' + Queries.COL_LOAN_TXN_PAYMENT_ID + '") REFERENCES "' + Queries.DB_TABLE_PAYMENT_TYPE + '" ("' + Queries.COL_PAYMENT_TYPE_ID + '"),\n                  FOREIGN KEY("' + Queries.COL_LOAN_TXN_IMAGE_ID + '") REFERENCES "' + Queries.DB_TABLE_TRANSACTION_IMAGE + '" ("' + Queries.COL_TRANSACTION_IMAGE_ID + '")\n            )';
    libsqlite3.sqlite3_exec(dbHandleForCreation, loanTransactions, null, null, null);

    var catalogueTableCreateQuery = 'create table ' + Queries.DB_TABLE_CATALOGUE_ITEMS + '( ' + Queries.COL_CATALOGUE_ITEM_ID + ' integer primary key autoincrement ,' + Queries.COL_ITEM_ID + ' integer, ' + Queries.COL_CATALOGUE_ITEM_NAME + ' varchar(256),' + Queries.COL_CATALOGUE_ITEM_CODE + ' varchar(32),' + Queries.COL_CATALOGUE_ITEM_DESCRIPTION + ' varchar(1024),' + Queries.COL_CATALOGUE_ITEM_SALE_UNIT_PRICE + ' double,' + Queries.COL_CATALOGUE_ITEM_CATEGORY_NAME + ' varchar(1024) )';

    libsqlite3.sqlite3_exec(dbHandleForCreation, catalogueTableCreateQuery, null, null, null);

    var itemImagesTableCreateQuery = 'create table ' + Queries.DB_TABLE_ITEM_IMAGES + '( ' + Queries.COL_ITEM_IMAGE_ID + ' integer primary key autoincrement ,' + Queries.COL_ITEM_ID + ' integer references ' + Queries.DB_TABLE_ITEMS + '(' + Queries.COL_ITEM_ID + '), ' + Queries.COL_ITEM_IMAGE_BITMAP + ' BLOB,' + Queries.COL_CATALOGUE_ITEM_ID + ' integer references ' + Queries.DB_TABLE_CATALOGUE_ITEMS + '(' + Queries.COL_CATALOGUE_ITEM_ID + ') )';

    libsqlite3.sqlite3_exec(dbHandleForCreation, itemImagesTableCreateQuery, null, null, null);

    DB_CREATION.setDefaultSettings();
  }

};

module.exports = {
  dbHandleForCreation: dbHandleForCreation,
  DB_CREATION: DB_CREATION

};