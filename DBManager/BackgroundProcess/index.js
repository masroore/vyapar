var _extends3 = require('babel-runtime/helpers/extends');

var _extends4 = _interopRequireDefault(_extends3);

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _extends2;

var processDeliveryChallanTransactions = function () {
	var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(data) {
		var _this = this;

		var rows;
		return _regenerator2.default.wrap(function _callee2$(_context2) {
			while (1) {
				switch (_context2.prev = _context2.next) {
					case 0:
						rows = data.map(function () {
							var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(row) {
								var prefix, txnStatus, txnStatusString, linkedTxn, link, transaction;
								return _regenerator2.default.wrap(function _callee$(_context) {
									while (1) {
										switch (_context.prev = _context.next) {
											case 0:
												prefix = row.prefix && row.prefix.prefixValue ? row.prefix.prefixValue : '';
												txnStatus = row.txnStatus;
												txnStatusString = StringConstant.deliveryChallanOpen;
												linkedTxn = null;

												if (!(txnStatus != TxnStatus.TXN_ORDER_OPEN)) {
													_context.next = 14;
													break;
												}

												txnStatusString = StringConstant.deliveryChallanCompletedString;
												_context.next = 8;
												return db.linkedTransactions.findOne({
													attributes: ['txnDestinationId'],
													where: (0, _defineProperty3.default)({}, db.Sequelize.Op.or, {
														txnSourceId: row.txnId,
														txnDestinationId: row.txnId
													})
												});

											case 8:
												link = _context.sent;

												if (!link) {
													_context.next = 14;
													break;
												}

												_context.next = 12;
												return db.transactions.findOne({
													attributes: ['txnDate', 'txnRefNumber'],
													where: { txnId: link.txnDestinationId },
													include: [{
														model: db.prefixes,
														attributes: ['prefixValue'],
														required: false
													}]
												});

											case 12:
												transaction = _context.sent;

												if (transaction) {
													linkedTxn = {
														txnId: link.txnDestinationId,
														txnDate: transaction.txnDate,
														prefixValue: transaction.prefix && transaction.prefix.prefixValue || '',
														txnRefNumber: transaction.txnRefNumber
													};
												}

											case 14:
												return _context.abrupt('return', {
													name: row.name.fullName || row.txnDescription,
													date: row.txnDate,
													challanNoWithPrefix: prefix + row.txnRefNumber,
													challanNo: row.txnRefNumber,
													txnDueDays: MyDate.getDiffInDays(new Date(), new Date(row.txnDueDate)),
													dueDate: row.txnDueDate,
													amount: row.txnCashAmount + row.txnBalanceAmount,
													txnId: row.txnId,
													txnType: row.txnType,
													txnTypeConstant: TxnTypeConstant.getTxnType(row.txnType),
													txnStatus: txnStatus,
													txnStatusString: txnStatusString,
													linkedTxn: linkedTxn
												});

											case 15:
											case 'end':
												return _context.stop();
										}
									}
								}, _callee, _this);
							}));

							return function (_x2) {
								return _ref2.apply(this, arguments);
							};
						}());
						return _context2.abrupt('return', _promise2.default.all(rows));

					case 2:
					case 'end':
						return _context2.stop();
				}
			}
		}, _callee2, this);
	}));

	return function processDeliveryChallanTransactions(_x) {
		return _ref.apply(this, arguments);
	};
}();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var IPCActions = require('../../Constants/IPCActions');
var ColorConstants = require('../../Constants/ColorConstants');
var Queries = require('../../Constants/Queries');
var ItemDetailObject = require('../../BizLogic/ItemDetailObject');
var TxnTypeConstant = require('../../Constants/TxnTypeConstant.js');
var TxnPaymentStatusConstants = require('../../Constants/TxnPaymentStatusConstants');
var TxnStatus = require('../../Constants/TransactionStatus');
var StringConstant = require('../../Constants/StringConstants');
var MyDate = require('../../Utilities/MyDate');
var MyDouble = require('../../Utilities/MyDouble');
var TransactionFactory = require('./../../BizLogic/TransactionFactory.js');
var DB = require('../../DBManager/GetConnection');

var db = require('../../DBManager/models');
var electron = require('electron');

var _require = require('../../Utilities/eiphop'),
    setupBgRendererHandler = _require.setupBgRendererHandler;

var logger = require('../../Utilities/logger');

var dashboardActions = require('./dashboard');

var transactionFactory = new TransactionFactory();
var ShowPaymentStatusForTheseTransactions = [TxnTypeConstant.TXN_TYPE_SALE, TxnTypeConstant.TXN_TYPE_SALE_ORDER, TxnTypeConstant.TXN_TYPE_SALE_RETURN, TxnTypeConstant.TXN_TYPE_PURCHASE, TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER, TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN, TxnTypeConstant.TXN_TYPE_CASHIN, TxnTypeConstant.TXN_TYPE_CASHOUT, TxnTypeConstant.TXN_TYPE_ROPENBALANCE, TxnTypeConstant.TXN_TYPE_POPENBALANCE];

var processTransactions = function processTransactions(records) {
	try {
		return records.map(function (x) {
			var itemDetailObject = new ItemDetailObject();
			itemDetailObject.setTxnId(x[Queries.COL_LINEITEM_TXN_ID]);
			itemDetailObject.setTxnType(x[Queries.COL_TXN_TYPE]);
			itemDetailObject.setItemQuantity(x[Queries.COL_LINEITEM_QUANTITY]);
			itemDetailObject.setTxnDate(MyDate.getDateObj(x[Queries.COL_TXN_DATE], 'yyyy-mm-dd', '-'));
			itemDetailObject.setMappingId(x[Queries.COL_LINEITEM_UNIT_MAPPING_ID]);
			itemDetailObject.setLineItemUnitId(x[Queries.COL_LINEITEM_UNIT_ID]);
			itemDetailObject.setPricePerUnit(x[Queries.COL_LINEITEM_UNITPRICE]);
			itemDetailObject.setItemFreeQuantity(x[Queries.COL_LINEITEM_FREE_QUANTITY]);
			itemDetailObject.setTxnPaymentStatus(x[Queries.COL_TXN_PAYMENT_STATUS]);
			itemDetailObject.setDescription(x[Queries.COL_ITEM_ADJ_DESCRIPTION]);

			var fullName = x[Queries.COL_NAME];
			var typeTxn = TxnTypeConstant.getTxnType(itemDetailObject.getTxnType());
			var typeId = MyDouble.convertStringToDouble(itemDetailObject.txnType);
			var type = TxnTypeConstant.getTxnTypeForUI(typeId);
			var name = fullName || itemDetailObject.getDescription();
			var date = itemDetailObject.txnDate;
			// date = MyDate.getDate('d/m/y', date);
			var quantityToBeDisplayed = itemDetailObject.getItemQuantity();
			var itemFreeQuantityToBeDisplayed = itemDetailObject.getItemFreeQuantity() ? itemDetailObject.getItemFreeQuantity() : 0;
			var pricePerUnitToBeDisplayed = itemDetailObject.getPricePerUnit();
			if (itemDetailObject.getMappingId() > 0) {
				// let mappingObj = itemUnitMappingCache.getItemUnitMapping(itemDetailObject.getMappingId());
				var conversionRate = Number(x[Queries.COL_CONVERSION_RATE]);
				quantityToBeDisplayed = itemDetailObject.getItemQuantity() * conversionRate;
				itemFreeQuantityToBeDisplayed = itemDetailObject.getItemFreeQuantity() * conversionRate;
				pricePerUnitToBeDisplayed = itemDetailObject.getPricePerUnit() / conversionRate;
			}

			var intQuantity = MyDouble.getQuantityWithDecimalWithoutColor(quantityToBeDisplayed);
			var intFreeQuantity = MyDouble.getQuantityWithDecimalWithoutColor(itemFreeQuantityToBeDisplayed);
			var stringQuantityToBeShown = intQuantity + MyDouble.quantityDoubleToStringWithSignExplicitly(itemFreeQuantityToBeDisplayed, true);
			if (itemDetailObject.getLineItemUnitId() > 0) {
				var unitShortName = x[Queries.COL_UNIT_SHORT_NAME];
				if (unitShortName) {
					stringQuantityToBeShown = stringQuantityToBeShown + ' ' + unitShortName;
				}
			}

			// let stringPricePerUnitToBeShown = '-';
			// if (pricePerUnitToBeDisplayed > 0) {
			// 	stringPricePerUnitToBeShown = '-';
			// } else {
			// 	stringPricePerUnitToBeShown = MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(pricePerUnitToBeDisplayed);
			// }

			var paymentStatusId = itemDetailObject.getTxnPaymentStatus();
			var paymentStatusString = '';

			if (ShowPaymentStatusForTheseTransactions.includes(typeId)) {
				paymentStatusString = TxnPaymentStatusConstants.getPaymentStatusForUI(typeId, paymentStatusId);
			}

			var color = ColorConstants[typeId];

			return {
				color: '<div style=\'background-color: ' + color + '; border-radius:50%;width:8px;height:8px; top: 19px; position:relative;\'></div>',
				type: type,
				typeId: typeId,
				name: name,
				date: date,
				quantity: stringQuantityToBeShown,
				intQuantity: intQuantity,
				freeQuantity: intFreeQuantity,
				price: MyDouble.convertStringToDouble(pricePerUnitToBeDisplayed),
				paymentStatus: paymentStatusString,
				paymentStatusId: paymentStatusId,
				id: itemDetailObject.getTxnId(),
				txnTypeConstant: typeTxn
			};
		});
	} catch (e) {
		logger.error(e);
	}
};
var processOrderTransactions = function processOrderTransactions(orders) {
	return orders.map(function (order) {
		var transactionObj = transactionFactory.getTransactionObject(order[Queries.COL_TXN_TYPE]);
		transactionObj.setTxnId(order[Queries.COL_TXN_ID]);
		transactionObj.setTxnDate(MyDate.getDateObj(order[Queries.COL_TXN_DATE], 'yyyy-mm-dd', '-'));
		transactionObj.setTxnDueDate(MyDate.getDateObj(order[Queries.COL_TXN_DUE_DATE], 'yyyy-mm-dd', '-'));
		transactionObj.setTxnRefNumber(order[Queries.COL_TXN_REF_NUMBER_CHAR]);
		transactionObj.setInvoicePrefix(order[Queries.COL_PREFIX_VALUE]);
		transactionObj.setCashAmount(order[Queries.COL_TXN_CASH_AMOUNT]);
		transactionObj.setBalanceAmount(order[Queries.COL_TXN_BALANCE_AMOUNT]);
		transactionObj.setTxnType(order[Queries.COL_TXN_TYPE]);
		transactionObj.setDescription(order[Queries.COL_TXN_DESCRIPTION]);
		transactionObj.setDiscountAmount(order[Queries.COL_TXN_DISCOUNT_AMOUNT]);
		transactionObj.setTaxAmount(order[Queries.COL_TXN_TAX_AMOUNT]);
		transactionObj.setStatus(order[Queries.COL_TXN_STATUS]);
		transactionObj.setTxnCurrentBalanceAmount(order[Queries.COL_TXN_CURRENT_BALANCE]);
		transactionObj.setTxnPaymentStatus(order[Queries.COL_TXN_PAYMENT_STATUS]);

		var orderNumber = transactionObj.getTxnRefNumber();
		var orderNumberWithPrefix = transactionObj.getFullTxnRefNumber();
		var typeId = transactionObj.getTxnType();
		var typeTxn = TxnTypeConstant.getTxnType(typeId);
		var fullName = order[Queries.COL_NAME];
		var name = fullName || transactionObj.description;
		var date = transactionObj.txnDate;
		var dueDate = transactionObj.txnDueDate;
		var txnTypeString = TxnTypeConstant.getTxnTypeForUI(typeId);
		var transactionType = void 0;
		if (transactionObj.txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER) {
			transactionType = transactionObj.status == TxnStatus.TXN_ORDER_OPEN ? 'CONVERT TO SALE' : '';
		} else if (transactionObj.txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
			transactionType = transactionObj.status == TxnStatus.TXN_ORDER_OPEN ? 'CONVERT TO PURCHASE' : '';
		}
		// date = MyDate.getDate('d/m/y', date);
		var status = transactionObj.getStatus();
		var statusString = void 0;
		var statusColor = void 0;
		if (status == TxnStatus.TXN_ORDER_OPEN) {
			if (dueDate.getTime() < new Date().getTime()) {
				statusString = StringConstant.orderOverdueString;
				statusColor = '#fbac50;';
			} else {
				statusString = StringConstant.orderOpen;
				statusColor = '#fbac50;';
			}
		} else {
			statusString = StringConstant.orderCompletedString;
			statusColor = '#118109;';
		}
		var totalAmount = Number(transactionObj.cashAmount) + Number(transactionObj.balanceAmount);
		var balanceAmount = transactionObj.balanceAmount;
		// const color = ColorConstants[typeId];
		return {
			// color: `<div style='background-color: ${color}; border-radius:50%;width:8px;height:8px; top: 19px; position:relative;'></div>`,
			name: name,
			date: date,
			orderNumberWithPrefix: orderNumberWithPrefix,
			orderNumber: orderNumber,
			dueDate: dueDate,
			amount: totalAmount,
			balance: balanceAmount,
			typeId: typeId,
			type: txnTypeString,
			status: statusString,
			statusColor: statusColor,
			action: transactionType,
			actionHTML: '',
			id: transactionObj.txnId,
			txnTypeConstant: typeTxn,
			filter: {
				txnType: transactionObj.txnType,
				txnStatus: status
			}
		};
	});
};

var processSaleAgingReportData = function processSaleAgingReportData(saleAgingData, selectedDate) {
	var bucketData = {};
	for (var i in saleAgingData) {
		var data = saleAgingData[i];
		var name = data[Queries.COL_NAME];
		var date = MyDate.getDateObj(data[Queries.COL_TXN_DATE], 'yyyy-mm-dd', '-');
		var dueDate = MyDate.getDateObj(data[Queries.COL_TXN_DUE_DATE], 'yyyy-mm-dd', '-');
		var totalAmount = Number(data[Queries.COL_TXN_CASH_AMOUNT]) + Number(data[Queries.COL_TXN_BALANCE_AMOUNT]);
		var balanceAmount = MyDouble.getBalanceAmountWithDecimal(data[Queries.COL_TXN_CURRENT_BALANCE]);
		var refNo = data[Queries.COL_TXN_REF_NUMBER_CHAR];
		var invoicePrefix = data[Queries.COL_PREFIX_VALUE];
		var fullRefNo = invoicePrefix ? invoicePrefix + refNo : refNo;
		var nameId = data[Queries.COL_TXN_NAME_ID];
		var partyBal = data[Queries.COL_NAME_AMOUNT];
		var txnId = data[Queries.COL_TXN_ID];
		if (!bucketData[name]) {
			bucketData[name] = {
				nameId: nameId,
				name: name,
				current: 0,
				bucket1: 0,
				bucket2: 0,
				bucket3: 0,
				bucket4: 0,
				total: 0,
				partyBal: partyBal,
				txnArray: []
			};
		}

		var rowObj = bucketData[name];
		var dueDays = MyDouble.getDueDays(dueDate, MyDate.getDateObj(selectedDate.replace(/'/g, ''), 'yyyy-mm-dd', '-'));
		var bucketName = '';
		if (dueDays >= 1 && dueDays <= 30) {
			rowObj.bucket1 += balanceAmount;
			bucketName = 'bucket1';
		} else if (dueDays >= 31 && dueDays <= 45) {
			rowObj.bucket2 += balanceAmount;
			bucketName = 'bucket2';
		} else if (dueDays >= 46 && dueDays <= 60) {
			rowObj.bucket3 += balanceAmount;
			bucketName = 'bucket3';
		} else if (dueDays > 60) {
			rowObj.bucket4 += balanceAmount;
			bucketName = 'bucket4';
		} else {
			rowObj.current += balanceAmount;
			bucketName = 'current';
		}
		rowObj.total = rowObj.current + rowObj.bucket1 + rowObj.bucket2 + rowObj.bucket3 + rowObj.bucket4;

		var txnUIObj = {
			txnId: txnId,
			refNo: refNo,
			invoiceNo: fullRefNo,
			date: MyDate.getDateStringForUI(date),
			dueDate: MyDate.getDateStringForUI(dueDate),
			dueDays: dueDays >= 0 ? dueDays : 0,
			amount: totalAmount,
			balance: balanceAmount,
			bucketName: bucketName
		};

		rowObj.txnArray.push(txnUIObj);
	}
	return (0, _values2.default)(bucketData);
};

function getItemDetailModelListByLimit(itemId, start, noOfRecords) {
	var getItemDetailsQuery = '\n  select\n  ' + Queries.COL_LINEITEM_TXN_ID + ',\n  ' + Queries.COL_LINEITEM_QUANTITY + ',\n  ' + Queries.COL_LINEITEM_UNITPRICE + ',\n  ' + Queries.COL_LINEITEM_FREE_QUANTITY + ',\n  ' + Queries.COL_TXN_DATE + ',\n  ' + Queries.COL_TXN_TYPE + ',\n  ' + Queries.COL_TXN_NAME_ID + ',\n  ' + Queries.COL_ITEM_ADJ_DESCRIPTION + ',\n  ' + Queries.COL_LINEITEM_UNIT_MAPPING_ID + ',\n  ' + Queries.COL_LINEITEM_UNIT_ID + ',\n  ' + Queries.COL_TXN_PAYMENT_STATUS + ',\n  ' + Queries.COL_UNIT_NAME + ',\n  ' + Queries.COL_UNIT_SHORT_NAME + ',\n  ' + Queries.COL_CONVERSION_RATE + ',\n  ' + Queries.COL_NAME + '\n  from (\n  SELECT * FROM\n      (SELECT\n        ' + Queries.COL_LINEITEM_TXN_ID + ',\n        ' + Queries.COL_LINEITEM_QUANTITY + ',\n        ' + Queries.COL_LINEITEM_UNITPRICE + ',\n        ' + Queries.COL_LINEITEM_FREE_QUANTITY + ',\n        ' + Queries.COL_TXN_DATE + ',\n        ' + Queries.COL_TXN_TYPE + ',\n        ' + Queries.COL_TXN_NAME_ID + ',\n        "" as ' + Queries.COL_ITEM_ADJ_DESCRIPTION + ',\n        ' + Queries.COL_LINEITEM_UNIT_MAPPING_ID + ',\n        ' + Queries.COL_LINEITEM_UNIT_ID + ',\n        ' + Queries.COL_TXN_PAYMENT_STATUS + '\n      FROM\n        ' + Queries.DB_TABLE_LINEITEMS + ', ' + Queries.DB_TABLE_TRANSACTIONS + '\n      WHERE\n            ' + Queries.COL_LINEITEM_TXN_ID + ' = ' + Queries.COL_TXN_ID + '\n        AND ' + Queries.COL_LINEITEM_ITEM_ID + ' = ' + itemId + '\n        AND ' + Queries.COL_TXN_STATUS + ' != ' + TxnStatus.TXN_ORDER_COMPLETE + '\n    UNION ALL\n      SELECT\n        ' + Queries.COL_ITEM_ADJ_ID + ',\n        ' + Queries.COL_ITEM_ADJ_QUANTITY + ',\n        ifnull(' + Queries.COL_ITEM_ADJ_ATPRICE + ', -1),\n        0,\n        ' + Queries.COL_ITEM_ADJ_DATE + ',\n        ' + Queries.COL_ITEM_ADJ_TYPE + ',\n        0,\n        ' + Queries.COL_ITEM_ADJ_DESCRIPTION + ',\n        0,\n        0,\n        0\n      FROM\n         ' + Queries.DB_TABLE_ITEM_ADJUSTMENT + '\n      WHERE\n         ' + Queries.COL_ITEM_ADJ_ITEM_ID + ' = ' + itemId + ')\n  ORDER BY ' + Queries.COL_TXN_DATE + ' DESC\n  LIMIT ' + start + ', ' + noOfRecords + '  ) AS t\n  LEFT JOIN ' + Queries.DB_TABLE_NAMES + ' n ON n.' + Queries.COL_NAME_ID + ' = t.' + Queries.COL_TXN_NAME_ID + '\n  LEFT JOIN ' + Queries.DB_TABLE_ITEM_UNITS + ' AS u ON u.' + Queries.COL_UNIT_ID + ' = t.' + Queries.COL_LINEITEM_UNIT_ID + '\n  LEFT JOIN ' + Queries.DB_TABLE_ITEM_UNITS_MAPPING + ' AS um ON um.' + Queries.COL_UNIT_MAPPING_ID + ' = t.' + Queries.COL_LINEITEM_UNIT_MAPPING_ID + '\n';
	return getItemDetailsQuery;
}

var getProductTransactions = function getProductTransactions(req, res) {
	var _req$payload = req.payload,
	    id = _req$payload.id,
	    start = _req$payload.start,
	    noOfRecords = _req$payload.noOfRecords,
	    key = _req$payload.key,
	    next = _req$payload.next;
	// or res.error({msg: 'failed'})

	var query = getItemDetailModelListByLimit(id, start, noOfRecords);
	DB.all(query, function (err, rows) {
		res.send({
			meta: { id: id, key: key, next: next },
			data: err ? [] : processTransactions(rows)
		});
	});
};

var getServiceTransactions = function getServiceTransactions(req, res) {
	var _req$payload2 = req.payload,
	    id = _req$payload2.id,
	    start = _req$payload2.start,
	    noOfRecords = _req$payload2.noOfRecords,
	    key = _req$payload2.key,
	    next = _req$payload2.next;

	var query = getItemDetailModelListByLimit(id, start, noOfRecords);
	DB.all(query, function (err, rows) {
		res.send({
			meta: { id: id, key: key, next: next },
			data: err ? [] : processTransactions(rows)
		});
	});
};

var getOrderTransactions = function getOrderTransactions(req, res) {
	var _req$payload3 = req.payload,
	    start = _req$payload3.start,
	    noOfRecords = _req$payload3.noOfRecords,
	    next = _req$payload3.next;

	var query = '\n\tselect *\n  from ' + Queries.DB_TABLE_TRANSACTIONS + ' as t\n  LEFT JOIN ' + Queries.DB_TABLE_NAMES + ' as n ON n.' + Queries.COL_NAME_ID + ' = t.' + Queries.COL_TXN_NAME_ID + '\n  LEFT JOIN ' + Queries.DB_TABLE_PREFIX + ' as p ON p.' + Queries.COL_PREFIX_ID + ' = t.' + Queries.COL_TXN_PREFIX_ID + '\n  where txn_type in (' + TxnTypeConstant.TXN_TYPE_SALE_ORDER + ', ' + TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER + ')\n  ORDER BY ' + Queries.COL_TXN_DATE + ' DESC\n  LIMIT ' + start + ', ' + noOfRecords + '\n  ';

	DB.all(query, function (err, rows) {
		res.send({
			meta: { next: next },
			data: err ? [] : processOrderTransactions(rows)
		});
	});
};

var getSaleAgingReportData = function getSaleAgingReportData(req, res) {
	var _req$payload4 = req.payload,
	    date = _req$payload4.date,
	    firmId = _req$payload4.firmId,
	    start = _req$payload4.start,
	    noOfRecords = _req$payload4.noOfRecords,
	    next = _req$payload4.next;

	var filterCondition = ' ' + Queries.COL_TXN_TYPE + ' = ' + TxnTypeConstant.TXN_TYPE_SALE + ' AND ' + Queries.COL_TXN_CURRENT_BALANCE + ' > 0';
	if (firmId) {
		filterCondition += ' AND ' + Queries.COL_TXN_FIRM_ID + ' = ' + firmId;
	}
	filterCondition += ' AND ' + Queries.COL_TXN_DATE + '  < ' + date;

	var query = '\n  SELECT\n  ' + Queries.COL_TXN_TYPE + ',\n  ' + Queries.COL_NAME + ',\n  ' + Queries.COL_TXN_DATE + ',\n  ' + Queries.COL_TXN_DUE_DATE + ',\n  ' + Queries.COL_TXN_CASH_AMOUNT + ',\n  ' + Queries.COL_TXN_BALANCE_AMOUNT + ',\n  ' + Queries.COL_TXN_CURRENT_BALANCE + ',\n  ' + Queries.COL_TXN_REF_NUMBER_CHAR + ',\n  ' + Queries.COL_PREFIX_VALUE + ',\n  ' + Queries.COL_TXN_NAME_ID + ',\n  ' + Queries.COL_NAME_AMOUNT + ',\n  ' + Queries.COL_TXN_ID + '\n  FROM ' + Queries.DB_TABLE_TRANSACTIONS + ' as t \n  LEFT JOIN ' + Queries.DB_TABLE_PREFIX + ' as prfx ON t.' + Queries.COL_TXN_PREFIX_ID + ' = prfx.' + Queries.COL_PREFIX_ID + '\n  LEFT JOIN ' + Queries.DB_TABLE_NAMES + ' as n ON n.' + Queries.COL_NAME_ID + ' = t.' + Queries.COL_TXN_NAME_ID + '\n  WHERE txn_name_id in (\n  \tSELECT ' + Queries.COL_TXN_NAME_ID + ' from ' + Queries.DB_TABLE_TRANSACTIONS + ' where ' + filterCondition + ' GROUP BY ' + Queries.COL_TXN_NAME_ID + ' LIMIT ' + start + ', ' + noOfRecords + '\n  )\n  AND ' + filterCondition + '\n\tORDER BY ' + Queries.COL_TXN_DATE + ' DESC\n  ';
	DB.all(query, function (err, rows) {
		res.send({
			meta: { next: next },
			data: err ? [] : processSaleAgingReportData(rows, date)
		});
	});
};

var getDeliveryChallanTransactions = function getDeliveryChallanTransactions(req, res) {
	var _req$payload5 = req.payload,
	    queryFields = _req$payload5.queryFields,
	    start = _req$payload5.start,
	    noOfRecords = _req$payload5.noOfRecords,
	    next = _req$payload5.next;


	db.transactions.getTransactions((0, _extends4.default)({}, queryFields, {
		fields: ['txnId', 'txnDate', 'txnCashAmount', 'txnBalanceAmount', 'txnType', 'txnDiscountAmount', 'txnDueDate', 'txnRefNumber', 'txnStatus', 'txnCurrentBalance', 'txnPaymentStatus', 'txnDescription', 'txnTaxAmount'],
		include: [{
			model: db.names,
			attributes: ['fullName'],
			required: false
		}, {
			model: db.prefixes,
			attributes: ['prefixValue'],
			required: false
		}],
		start: start,
		limit: noOfRecords
	})).then(function (data) {
		processDeliveryChallanTransactions(data).then(function (processedData) {
			res.send({
				meta: { queryFields: queryFields, next: next },
				data: processedData
			});
		});
	}).catch(function (err) {
		logger.error(err);
		res.send({
			meta: { queryFields: queryFields, next: next },
			data: []
		});
	});
};

function getNamesList(req, res) {
	var nameType = req.payload.nameType;

	db.names.findAll({
		attributes: ['nameId', 'fullName', 'amount', 'phoneNumber', 'email', 'address', 'gstinNumber', 'tinNumber'],
		where: { nameType: nameType },
		raw: true
	}).then(function (data) {
		return res.send(data);
	});
}

function getNamesTransactions(req, res) {
	var TransactionStatus = require('../../Constants/TransactionStatus');
	var _req$payload6 = req.payload,
	    id = _req$payload6.id,
	    start = _req$payload6.start,
	    noOfRecords = _req$payload6.noOfRecords,
	    key = _req$payload6.key,
	    next = _req$payload6.next;

	db.transactions.findAll({
		attributes: ['txnId', 'txnDate', 'txnCashAmount', 'txnBalanceAmount', 'txnType', 'txnDiscountAmount', 'txnDueDate', 'txnRefNumber', 'txnStatus', 'txnCurrentBalance', 'txnPaymentStatus'],
		order: [['txnDate', 'DESC'], ['txnId', 'DESC']],
		where: {
			txnNameId: id,
			txnStatus: (0, _defineProperty3.default)({}, db.Sequelize.Op.ne, TransactionStatus.TXN_ORDER_COMPLETE)
		},
		include: [{
			model: db.prefixes,
			attributes: ['prefixValue'],
			required: false
		}],
		offset: start,
		limit: noOfRecords,
		raw: true
	}).then(function (data) {
		res.send({
			meta: { id: id, key: key, next: next },
			data: data
		});
	}).catch(function (err) {
		logger.error(err);
		res.send({
			meta: { id: id, key: key },
			data: []
		});
	});
}
function getNamesGroupList(req, res) {
	var nameType = req.payload.nameType;

	db.partyGroups.findAll({
		attributes: {
			include: [[db.Sequelize.fn('COUNT', db.Sequelize.col(db.names.rawAttributes.nameId.field)), 'partyCount'], [db.Sequelize.fn('SUM', db.Sequelize.col('amount')), 'partyTotal']]
		},
		include: [{
			model: db.names,
			attributes: [],
			where: { nameType: nameType },
			required: false
		}],
		group: ['partyGroupId'],
		raw: true
	}).then(function (data) {
		return res.send(data);
	});
}

function getNamesByGroupId(req, res) {
	var _req$payload7 = req.payload,
	    nameType = _req$payload7.nameType,
	    id = _req$payload7.id,
	    start = _req$payload7.start,
	    noOfRecords = _req$payload7.noOfRecords,
	    key = _req$payload7.key,
	    next = _req$payload7.next;

	db.names.findAll({
		attributes: ['nameId', 'fullName', 'amount'],
		where: { nameGroupId: id, nameType: nameType },
		offset: start,
		limit: noOfRecords,
		raw: true
	}).then(function (data) {
		return res.send({
			meta: { id: id, key: key, next: next },
			data: data
		});
	});
}
function getNamesListNotInGroup(req, res) {
	var _req$payload8 = req.payload,
	    id = _req$payload8.id,
	    nameType = _req$payload8.nameType;

	db.names.findAll({
		attributes: ['nameId', 'fullName'],
		where: {
			nameType: nameType,
			nameGroupId: (0, _defineProperty3.default)({}, db.Sequelize.Op.ne, id)
		},
		raw: true
	}).then(function (data) {
		return res.send(data);
	});
}

function processSalePurchaseReportTransactions(data) {
	return data.map(function (row) {
		var prefix = row.prefix && row.prefix.prefixValue ? row.prefix.prefixValue : '';
		var txnStatus = row.txnStatus;
		var txnStatusString = StringConstant.deliveryChallanOpen;

		return {
			date: row.txnDate,
			txnRefNumber: prefix + row.txnRefNumber,
			refNumber: row.txnRefNumber,
			name: row.name && row.name.fullName || row.txnDescription,
			email: row.name && row.name.email || '',
			nameId: row.name && row.name.nameId || '',
			phoneNumber: row.name && row.name.phoneNumber || '',
			paymentType: row.paymentType && row.paymentType.paymentTypeName || '',
			paymentTypeId: row.paymentType && row.paymentType.paymentTypeId || '',
			paymentStatus: row.txnPaymentStatus,
			txnDueDays: MyDate.getDiffInDays(new Date(), new Date(row.txnDueDate)),
			dueDate: row.txnDueDate,
			amount: row.txnCashAmount + row.txnBalanceAmount,
			currentBalance: row.txnCurrentBalance,
			id: row.txnId,
			txnType: row.txnType,
			txnTypeConstant: TxnTypeConstant.getTxnType(row.txnType),
			txnStatus: txnStatus,
			txnStatusString: txnStatusString
		};
	});
}

var getTransactionsCount = function getTransactionsCount(req, res) {
	db.transactions.count({
		where: {
			txnType: req.payload.txnTypes
		}
	}).then(function (count) {
		res.send({
			count: count
		});
	});
};

var getSaleReportTransactions = function getSaleReportTransactions(req, res) {
	var _req$payload9 = req.payload,
	    queryFields = _req$payload9.queryFields,
	    start = _req$payload9.start,
	    noOfRecords = _req$payload9.noOfRecords,
	    next = _req$payload9.next;


	db.transactions.getTransactions((0, _extends4.default)({}, queryFields, {
		fields: ['txnId', 'txnDate', 'txnCashAmount', 'txnBalanceAmount', 'txnType', 'txnDiscountAmount', 'txnDueDate', 'txnRefNumber', 'txnStatus', 'txnCurrentBalance', 'txnPaymentStatus', 'txnDescription', 'txnTaxAmount'],
		include: [{
			model: db.names,
			attributes: ['fullName', 'email', 'phoneNumber', 'nameId'],
			required: false
		}, {
			model: db.prefixes,
			attributes: ['prefixValue'],
			required: false
		}, {
			model: db.paymentTypes,
			attributes: ['paymentTypeName', 'paymentTypeId'],
			required: false
		}],
		start: start,
		limit: noOfRecords
	})).then(function (data) {
		res.send({
			meta: { queryFields: queryFields, next: next },
			data: processSalePurchaseReportTransactions(data)
		});
	}).catch(function (err) {
		logger.error(err);
		res.send({
			meta: { queryFields: queryFields, next: next },
			data: []
		});
	});
};
setupBgRendererHandler(electron, (0, _extends4.default)({}, dashboardActions, (_extends2 = {}, (0, _defineProperty3.default)(_extends2, IPCActions.GET_ITEM_TRANSACTIONS, getProductTransactions), (0, _defineProperty3.default)(_extends2, IPCActions.GET_SERVICE_TRANSACTIONS, getServiceTransactions), (0, _defineProperty3.default)(_extends2, IPCActions.GET_ORDER_TRANSACTIONS, getOrderTransactions), (0, _defineProperty3.default)(_extends2, IPCActions.GET_SALE_AGING_REPORT_DATA, getSaleAgingReportData), (0, _defineProperty3.default)(_extends2, IPCActions.GET_NAMES_LIST, getNamesList), (0, _defineProperty3.default)(_extends2, IPCActions.GET_NAME_TRANSACTIONS, getNamesTransactions), (0, _defineProperty3.default)(_extends2, IPCActions.GET_NAMES_GROUP_LIST, getNamesGroupList), (0, _defineProperty3.default)(_extends2, IPCActions.GET_NAMES_BY_GROUP_ID, getNamesByGroupId), (0, _defineProperty3.default)(_extends2, IPCActions.GET_NAMES_LIST_NOT_IN_GROUP, getNamesListNotInGroup), (0, _defineProperty3.default)(_extends2, IPCActions.GET_DELIVERY_CHALLAN_TRANSACTIONS, getDeliveryChallanTransactions), (0, _defineProperty3.default)(_extends2, IPCActions.GET_SALE_REPORT_TRANSACTIONS, getSaleReportTransactions), (0, _defineProperty3.default)(_extends2, IPCActions.GET_TRANSACTIONS_COUNT, getTransactionsCount), (0, _defineProperty3.default)(_extends2, IPCActions.CLOSE_DB_CONNECTION, function (req, res) {
	DB.close(function (err) {
		if (err) {
			logger.error(err);
		}
		db.sequelize.close().then(function () {
			res.send({});
		});
	});
}), _extends2)));