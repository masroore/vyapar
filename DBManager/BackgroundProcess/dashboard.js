var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _create = require('babel-runtime/core-js/object/create');

var _create2 = _interopRequireDefault(_create);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _module$exports;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var IPCActions = require('../../Constants/IPCActions');
var Queries = require('../../Constants/Queries');
var TxnTypeConstant = require('../../Constants/TxnTypeConstant.js');
var TransactionStatus = require('../../Constants/TransactionStatus');
var ChequeStatus = require('../../Constants/ChequeStatus');
var NameType = require('../../Constants/NameType');
var MyDate = require('../../Utilities/MyDate');
var MyDouble = require('../../Utilities/MyDouble');
var DB = require('../../DBManager/GetConnection');

function runQueryPromise(query) {
	return new _promise2.default(function (resolve, reject) {
		DB.all(query, function (err, rows) {
			if (err) return reject(err);
			resolve(rows);
		});
	});
}

var getCardsEnabledInSettings = function getCardsEnabledInSettings(req, res) {
	var _req$payload$cardsToC = req.payload.cardsToCheck,
	    cardsToCheck = _req$payload$cardsToC === undefined ? [] : _req$payload$cardsToC;

	var requiredSettings = [Queries.SETTING_DELIVERY_CHALLAN_ENABLED, Queries.SETTING_ORDER_FORM_ENABLED, Queries.SETTING_OTHER_INCOME_ENABLED, Queries.SETTING_ITEM_ENABLED, Queries.SETTING_STOCK_ENABLED, Queries.SETTING_ENABLE_ITEM_EXPIRY_DATE];
	var query = '\n\tSELECT ' + Queries.COL_SETTING_KEY + ', ' + Queries.COL_SETTING_VALUE + '\n\tFROM ' + Queries.DB_TABLE_SETTINGS + '\n\tWHERE ' + Queries.COL_SETTING_KEY + ' IN (' + requiredSettings.map(function (key) {
		return '"' + key + '"';
	}) + ')\n\t';
	runQueryPromise(query).then(function (rows) {
		var cardsEnabledInSettings = [];
		var settingsMap = rows.reduce(function (map, row) {
			map[row[Queries.COL_SETTING_KEY]] = row[Queries.COL_SETTING_VALUE];
			return map;
		}, {});
		cardsToCheck.forEach(function (cardKey) {
			switch (cardKey) {
				case 'lowStocks':
					if (settingsMap[Queries.SETTING_ITEM_ENABLED] == 1 && settingsMap[Queries.SETTING_STOCK_ENABLED] == 1) {
						cardsEnabledInSettings.push(cardKey);
					}
					break;
				case 'expiringStocks':
					if (settingsMap[Queries.SETTING_ITEM_ENABLED] == 1 && settingsMap[Queries.SETTING_STOCK_ENABLED] == 1 && settingsMap[Queries.SETTING_ENABLE_ITEM_EXPIRY_DATE] == 1) {
						cardsEnabledInSettings.push(cardKey);
					}
					break;
				case 'saleOrders':
					if (settingsMap[Queries.SETTING_ORDER_FORM_ENABLED] == 1) {
						cardsEnabledInSettings.push(cardKey);
					}
					break;
				case 'purchaseOrders':
					if (settingsMap[Queries.SETTING_ORDER_FORM_ENABLED] == 1) {
						cardsEnabledInSettings.push(cardKey);
					}
					break;
				case 'deliveryChallans':
					if (settingsMap[Queries.SETTING_DELIVERY_CHALLAN_ENABLED] == 1) {
						cardsEnabledInSettings.push(cardKey);
					}
					break;
				case 'otherIncome':
					if (settingsMap[Queries.SETTING_OTHER_INCOME_ENABLED] == 1) {
						cardsEnabledInSettings.push(cardKey);
					}
					break;
				default:
					break;
			}
		});
		res.send({
			cardsEnabledInSettings: cardsEnabledInSettings
		});
	});
};

var getCashInHandAmount = function getCashInhandAmount(req, res) {
	var getAmountFromTxns = function () {
		var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
			var queryFromTxns, SQL_GET_TXN_ENDDATE, SQL_GET_TXN_GROUPBY, balance, rows;
			return _regenerator2.default.wrap(function _callee$(_context) {
				while (1) {
					switch (_context.prev = _context.next) {
						case 0:
							queryFromTxns = '\n        SELECT ' + Queries.COL_TXN_TYPE + ', TOTAL(' + Queries.COL_TXN_CASH_AMOUNT + ') AS ' + Queries.COL_TXN_CASH_AMOUNT + '\n        FROM ' + Queries.DB_TABLE_TRANSACTIONS + '\n        WHERE ' + Queries.COL_TXN_PAYMENT_TYPE + ' = 1 AND ' + Queries.COL_TXN_STATUS + ' != ' + TransactionStatus.TXN_ORDER_COMPLETE + ' AND ' + Queries.COL_TXN_CASH_AMOUNT + ' > 0\n    ';

							if (endDate) {
								SQL_GET_TXN_ENDDATE = ' AND ' + Queries.COL_TXN_DATE + ' <= \'' + MyDate.getDate('y-m-d', endDate) + ' 23:59:59\'';

								queryFromTxns += SQL_GET_TXN_ENDDATE;
							}
							SQL_GET_TXN_GROUPBY = ' GROUP BY ' + Queries.COL_TXN_TYPE;

							queryFromTxns += SQL_GET_TXN_GROUPBY;
							balance = 0;
							_context.next = 7;
							return runQueryPromise(queryFromTxns);

						case 7:
							rows = _context.sent;

							if (rows && rows.length) {
								balance = rows.reduce(function (currentBalance, rowData) {
									var transactionType = Number(rowData[Queries.COL_TXN_TYPE]);
									var cashAmount = Number(rowData[Queries.COL_TXN_CASH_AMOUNT]);
									switch (transactionType) {
										case TxnTypeConstant.TXN_TYPE_CASHIN:
										case TxnTypeConstant.TXN_TYPE_SALE:
										case TxnTypeConstant.TXN_TYPE_OTHER_INCOME:
										case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
											return currentBalance + cashAmount;
										case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
											if (!isForClosingBooks) {
												return currentBalance + cashAmount;
											}
											break;
										case TxnTypeConstant.TXN_TYPE_CASHOUT:
										case TxnTypeConstant.TXN_TYPE_PURCHASE:
										case TxnTypeConstant.TXN_TYPE_EXPENSE:
										case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
											return currentBalance - cashAmount;
										case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
											if (!isForClosingBooks) {
												return currentBalance - cashAmount;
											}
											break;
										default:
											return currentBalance;
									}
								}, 0);
							}
							return _context.abrupt('return', balance);

						case 10:
						case 'end':
							return _context.stop();
					}
				}
			}, _callee, this);
		}));

		return function getAmountFromTxns() {
			return _ref.apply(this, arguments);
		};
	}();

	var getAmountFromBankAdjs = function () {
		var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2() {
			var queryFromBankAdjs, SQL_GET_TXN_ENDDATE, SQL_GET_BANK_GROUPBY, balance, rows;
			return _regenerator2.default.wrap(function _callee2$(_context2) {
				while (1) {
					switch (_context2.prev = _context2.next) {
						case 0:
							queryFromBankAdjs = 'SELECT ' + Queries.COL_BANK_ADJ_TYPE + ', TOTAL(' + Queries.COL_BANK_ADJ_AMOUNT + ') AS ' + Queries.COL_BANK_ADJ_AMOUNT + ' FROM ' + Queries.DB_TABLE_BANK_ADJUSTMENT + '\n            WHERE ' + Queries.COL_BANK_ADJ_TYPE + ' in (' + TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD + ', ' + TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE + ')';

							if (endDate) {
								SQL_GET_TXN_ENDDATE = ' AND ' + Queries.COL_BANK_ADJ_DATE + "<= '" + MyDate.getDate('y-m-d', endDate) + " 23:59:59'";

								queryFromBankAdjs += SQL_GET_TXN_ENDDATE;
							}
							SQL_GET_BANK_GROUPBY = ' GROUP BY ' + Queries.COL_BANK_ADJ_TYPE;

							queryFromBankAdjs += SQL_GET_BANK_GROUPBY;
							balance = 0;
							_context2.next = 7;
							return runQueryPromise(queryFromBankAdjs);

						case 7:
							rows = _context2.sent;

							if (rows) {
								balance = rows.reduce(function (currentBalance, rowData) {
									var transactionType = Number(rowData[Queries.COL_BANK_ADJ_TYPE]);
									var cashAmount = Number(rowData[Queries.COL_BANK_ADJ_AMOUNT]);
									switch (transactionType) {
										case TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD:
											return currentBalance - cashAmount;
										case TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE:
											return currentBalance + cashAmount;
										default:
											return currentBalance;
									}
								}, 0);
							}
							return _context2.abrupt('return', balance);

						case 10:
						case 'end':
							return _context2.stop();
					}
				}
			}, _callee2, this);
		}));

		return function getAmountFromBankAdjs() {
			return _ref2.apply(this, arguments);
		};
	}();

	var getAmountFromCashAdjs = function () {
		var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3() {
			var queryFromCashAdjs, SQL_GET_CASH_GROUPBY, SQL_GET_TXN_ENDDATE, balance, rows;
			return _regenerator2.default.wrap(function _callee3$(_context3) {
				while (1) {
					switch (_context3.prev = _context3.next) {
						case 0:
							queryFromCashAdjs = 'SELECT ' + Queries.COL_CASH_ADJ_TYPE + ', TOTAL(' + Queries.COL_CASH_ADJ_AMOUNT + ') AS ' + Queries.COL_CASH_ADJ_AMOUNT + ' FROM ' + Queries.DB_TABLE_CASH_ADJUSTMENT + ' ';
							SQL_GET_CASH_GROUPBY = ' GROUP BY ' + Queries.COL_CASH_ADJ_TYPE;


							if (endDate) {
								SQL_GET_TXN_ENDDATE = ' WHERE ' + Queries.COL_CASH_ADJ_DATE + "<= '" + MyDate.getDate('y-m-d', endDate) + " 23:59:59'";

								queryFromCashAdjs += SQL_GET_TXN_ENDDATE;
							}
							queryFromCashAdjs += SQL_GET_CASH_GROUPBY;
							balance = 0;
							_context3.next = 7;
							return runQueryPromise(queryFromCashAdjs);

						case 7:
							rows = _context3.sent;

							if (rows) {
								balance = rows.reduce(function (currentBalance, rowData) {
									var transactionType = Number(rowData[Queries.COL_CASH_ADJ_TYPE]);
									var cashAmount = Number(rowData[Queries.COL_CASH_ADJ_AMOUNT]);
									switch (transactionType) {
										case TxnTypeConstant.TXN_TYPE_CASH_OPENINGBALANCE:
										case TxnTypeConstant.TXN_TYPE_CASH_ADJ_ADD:
											return currentBalance + cashAmount;
										case Number(TxnTypeConstant.TXN_TYPE_CASH_ADJ_REDUCE):
											return currentBalance - cashAmount;
										default:
											return currentBalance;
									}
								}, 0);
							}
							return _context3.abrupt('return', balance);

						case 10:
						case 'end':
							return _context3.stop();
					}
				}
			}, _callee3, this);
		}));

		return function getAmountFromCashAdjs() {
			return _ref3.apply(this, arguments);
		};
	}();

	var getAmountFromCheques = function () {
		var _ref4 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4() {
			var queryFromChequeStatus, SQL_GET_CHEQUE_TRANSFER_ENDDATE, SQL_GET_CHEQUE_TRANSFER_GROUPBY, _SQL_GET_CHEQUE_TRANSFER_ENDDATE, balance, rows;

			return _regenerator2.default.wrap(function _callee4$(_context4) {
				while (1) {
					switch (_context4.prev = _context4.next) {
						case 0:
							queryFromChequeStatus = 'SELECT ' + Queries.COL_TXN_TYPE + ', TOTAL(' + Queries.COL_TXN_CASH_AMOUNT + ') AS ' + Queries.COL_TXN_CASH_AMOUNT + '\n        FROM ' + Queries.DB_TABLE_TRANSACTIONS + ', ' + Queries.DB_TABLE_CHEQUE_STATUS + '\n        WHERE ' + Queries.COL_CHEQUE_TXN_ID + ' = ' + Queries.COL_TXN_ID + '  AND ' + Queries.COL_CHEQUE_CURRENT_STATUS + ' = ' + ChequeStatus.CLOSE + ' AND ' + Queries.COL_CHEQUE_TRANSFERRED_TO_ACCOUNT + ' = 1';


							if (endDate) {
								SQL_GET_CHEQUE_TRANSFER_ENDDATE = '';

								if (isForClosingBooks) {
									SQL_GET_CHEQUE_TRANSFER_ENDDATE = ' AND ' + Queries.COL_TXN_DATE + "<= '" + MyDate.getDate('y-m-d', endDate) + " 23:59:59'";
								} else {
									SQL_GET_CHEQUE_TRANSFER_ENDDATE = ' AND ' + Queries.COL_CHEQUE_TRANSFER_DATE + "<= '" + MyDate.getDate('y-m-d', endDate) + " 23:59:59'";
								}
								queryFromChequeStatus += SQL_GET_CHEQUE_TRANSFER_ENDDATE;
							}
							SQL_GET_CHEQUE_TRANSFER_GROUPBY = ' GROUP BY ' + Queries.COL_TXN_TYPE;

							queryFromChequeStatus += SQL_GET_CHEQUE_TRANSFER_GROUPBY;
							queryFromChequeStatus += ' UNION ALL SELECT ' + Queries.COL_CLOSED_LINK_TXN_TYPE + ', TOTAL(' + Queries.COL_CLOSED_LINK_TXN_AMOUNT + ') AS ' + Queries.COL_TXN_CASH_AMOUNT + ' FROM ' + Queries.DB_TABLE_CLOSED_LINK_TXN_TABLE + ' INNER JOIN ' + Queries.DB_TABLE_CHEQUE_STATUS + ' ON ' + Queries.COL_CHEQUE_CLOSED_LINK_TXN_REF_ID + ' = ' + Queries.COL_CLOSED_LINK_TXN_ID + ' WHERE ' + Queries.COL_CHEQUE_CURRENT_STATUS + ' = ' + ChequeStatus.CLOSE + ' AND ' + Queries.COL_CHEQUE_TRANSFERRED_TO_ACCOUNT + ' = 1';

							if (endDate) {
								_SQL_GET_CHEQUE_TRANSFER_ENDDATE = '';

								if (isForClosingBooks) {
									_SQL_GET_CHEQUE_TRANSFER_ENDDATE = ' AND ' + Queries.COL_CLOSED_LINK_TXN_DATE + "<= '" + MyDate.getDate('y-m-d', endDate) + " 23:59:59'";
								} else {
									_SQL_GET_CHEQUE_TRANSFER_ENDDATE = ' AND ' + Queries.COL_CHEQUE_TRANSFER_DATE + "<= '" + MyDate.getDate('y-m-d', endDate) + " 23:59:59'";
								}
								queryFromChequeStatus += _SQL_GET_CHEQUE_TRANSFER_ENDDATE;
							}
							queryFromChequeStatus += ' GROUP BY ' + Queries.COL_CLOSED_LINK_TXN_TYPE;
							balance = 0;
							_context4.next = 10;
							return runQueryPromise(queryFromChequeStatus);

						case 10:
							rows = _context4.sent;

							if (rows) {
								balance = rows.reduce(function (currentBalance, rowData) {
									var transactionType = Number(rowData[Queries.COL_TXN_TYPE]);
									var cashAmount = Number(rowData[Queries.COL_TXN_CASH_AMOUNT]);
									switch (transactionType) {
										case TxnTypeConstant.TXN_TYPE_SALE:
										case TxnTypeConstant.TXN_TYPE_CASHIN:
										case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
										case TxnTypeConstant.TXN_TYPE_OTHER_INCOME:
											return currentBalance + cashAmount;
										case TxnTypeConstant.TXN_TYPE_CASHOUT:
										case TxnTypeConstant.TXN_TYPE_EXPENSE:
										case TxnTypeConstant.TXN_TYPE_PURCHASE:
										case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
											return currentBalance - cashAmount;
										default:
											return currentBalance;
									}
								}, 0);
							}
							return _context4.abrupt('return', balance);

						case 13:
						case 'end':
							return _context4.stop();
					}
				}
			}, _callee4, this);
		}));

		return function getAmountFromCheques() {
			return _ref4.apply(this, arguments);
		};
	}();

	var getAmountFromLoans = function () {
		var _ref5 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee5() {
			var datePart, GET_LOAN_TRANSACTION_BALANCE;
			return _regenerator2.default.wrap(function _callee5$(_context5) {
				while (1) {
					switch (_context5.prev = _context5.next) {
						case 0:
							datePart = '';

							if (endDate) {
								datePart = ' AND ' + Queries.COL_LOAN_TXN_DATE + ' <= \'' + MyDate.getDate('y-m-d', endDate) + ' 23:59:59\'';
							}
							GET_LOAN_TRANSACTION_BALANCE = '\n\t\t\t\t\t\tSELECT TOTAL(CASE WHEN ' + Queries.COL_LOAN_TXN_TYPE + ' = ' + TxnTypeConstant.TXN_TYPE_LOAN_STARTING_BALANCE + ' THEN ' + Queries.COL_LOAN_TXN_PRINCIPAL + '\n\t\t\t\t\t\t\tWHEN ' + Queries.COL_LOAN_TXN_TYPE + ' = ' + TxnTypeConstant.TXN_TYPE_LOAN_ADJUSTMENT + ' THEN ' + Queries.COL_LOAN_TXN_PRINCIPAL + '\n\t\t\t\t\t\t\tWHEN ' + Queries.COL_LOAN_TXN_TYPE + ' = ' + TxnTypeConstant.TXN_TYPE_LOAN_EMI_PAYMENT + ' THEN -(' + Queries.COL_LOAN_TXN_PRINCIPAL + '+' + Queries.COL_LOAN_TXN_INTEREST + ')\n\t\t\t\t\t\t\tWHEN ' + Queries.COL_LOAN_TXN_TYPE + ' = ' + TxnTypeConstant.TXN_TYPE_LOAN_PROCESSING_FEE + ' THEN -' + Queries.COL_LOAN_TXN_PRINCIPAL + '\n\t\t\t\t\t\t\tEND) AS BALANCE\n\t\t\t\t\t\t\tFROM ' + Queries.DB_TABLE_LOAN_TRANSACTIONS + '\n\t\t\t\t\t\t\tWHERE ' + Queries.COL_LOAN_TXN_PAYMENT_ID + ' = 1 ' + datePart + '\n\t\t\t\t\t';
							return _context5.abrupt('return', runQueryPromise(GET_LOAN_TRANSACTION_BALANCE).then(function (rows) {
								if (rows && rows.length) {
									return rows.reduce(function (currBal, row) {
										return currBal + Number(row['BALANCE']);
									}, 0);
								}
							}));

						case 4:
						case 'end':
							return _context5.stop();
					}
				}
			}, _callee5, this);
		}));

		return function getAmountFromLoans() {
			return _ref5.apply(this, arguments);
		};
	}();

	var _req$payload = req.payload,
	    _req$payload$endDate = _req$payload.endDate,
	    endDate = _req$payload$endDate === undefined ? null : _req$payload$endDate,
	    _req$payload$isForClo = _req$payload.isForClosingBooks,
	    isForClosingBooks = _req$payload$isForClo === undefined ? false : _req$payload$isForClo;

	isForClosingBooks = JSON.parse(isForClosingBooks);

	_promise2.default.all([getAmountFromTxns(), getAmountFromBankAdjs(), getAmountFromCashAdjs(), getAmountFromCheques(), getAmountFromLoans()]).then(function (amounts) {
		var cashInHand = amounts.reduce(function (total, amount) {
			return amount + total;
		});
		res.send({
			cashInHand: cashInHand
		});
	});
};

var getOpenOrderDetails = function getOpenOrderDetails(req, res) {
	var _req$payload$txnTypes = req.payload.txnTypes,
	    txnTypes = _req$payload$txnTypes === undefined ? [] : _req$payload$txnTypes;

	var queryForOpenOrderDetails = '\n    SELECT SUM(1) AS count, SUM(' + Queries.COL_TXN_CASH_AMOUNT + ') + SUM(' + Queries.COL_TXN_BALANCE_AMOUNT + ') AS amount\n    FROM ' + Queries.DB_TABLE_TRANSACTIONS + '\n    WHERE ' + Queries.COL_TXN_TYPE + ' IN (' + txnTypes + ')\n    AND ' + Queries.COL_TXN_STATUS + ' != ' + TransactionStatus.TXN_ORDER_COMPLETE;
	runQueryPromise(queryForOpenOrderDetails).then(function (rows) {
		var orderDetails = (0, _create2.default)(null);
		orderDetails['count'] = 0;
		orderDetails['amount'] = 0;
		rows.forEach(function (row) {
			orderDetails['count'] += Number(row['count']);
			orderDetails['amount'] += Number(row['amount']);
		});
		res.send(orderDetails);
	});
};

var getOpenChequeDetails = function getOpenChequeDetails(req, res) {
	var _req$payload$endDate2 = req.payload.endDate,
	    endDate = _req$payload$endDate2 === undefined ? null : _req$payload$endDate2;

	var endDateStr = '';
	var nextDayStr = '';
	if (endDate) {
		endDateStr = "'" + MyDate.getDate('y-m-d', endDate) + " 23:59:59'";
		var nextDate = MyDate.addDays(endDate, 1);
		nextDayStr = "'" + MyDate.getDate('y-m-d', nextDate) + " 00:00:00'";
	}

	var querySearch = 'select sum(TT.' + Queries.COL_TXN_CASH_AMOUNT + ') as ' + Queries.COL_TXN_CASH_AMOUNT + ', \n\t\t\tTT.' + Queries.COL_TXN_TYPE + ', count(TT.' + Queries.COL_TXN_ID + ') as cheque_count \n\t\t\tfrom ' + Queries.DB_TABLE_CHEQUE_STATUS + ' CST inner join ' + Queries.DB_TABLE_TRANSACTIONS + ' TT \n\t\t\tON CST.' + Queries.COL_CHEQUE_TXN_ID + ' =  TT.' + Queries.COL_TXN_ID + ' WHERE TT.' + Queries.COL_TXN_STATUS + ' != ' + TransactionStatus.TXN_ORDER_COMPLETE + ' \n\t\t\tand ' + Queries.COL_TXN_CASH_AMOUNT + ' > 0 ';

	if (endDate) {
		querySearch += ' and TT.' + Queries.COL_TXN_DATE + ' <= ' + endDateStr + ' ';
	}

	// Either cheque is open or cheque is closed after end date
	querySearch += ' and (CST.' + Queries.COL_CHEQUE_CURRENT_STATUS + ' = ' + ChequeStatus.OPEN + ' ';
	if (endDate) {
		querySearch += ' or (CST.' + Queries.COL_CHEQUE_CURRENT_STATUS + ' = ' + ChequeStatus.CLOSE + ' and CST.' + Queries.COL_CHEQUE_TRANSFER_DATE + ' >= ' + nextDayStr + ')';
	}
	querySearch += ') ';

	querySearch += ' group by TT.' + Queries.COL_TXN_TYPE + '\n\t\t\tunion all \n\t\t\tselect sum(' + Queries.COL_CLOSED_LINK_TXN_AMOUNT + '), \n\t\t\t' + Queries.COL_CLOSED_LINK_TXN_TYPE + ', count(' + Queries.COL_CHEQUE_CLOSED_LINK_TXN_REF_ID + ')  \n\t\t\tfrom ' + Queries.DB_TABLE_CHEQUE_STATUS + ' inner join ' + Queries.DB_TABLE_CLOSED_LINK_TXN_TABLE + '  \n\t\t\tON ' + Queries.COL_CLOSED_LINK_TXN_ID + ' =  ' + Queries.COL_CHEQUE_CLOSED_LINK_TXN_REF_ID + ' WHERE \n\t\t\t' + Queries.COL_CLOSED_LINK_TXN_TYPE + ' not in (' + TxnTypeConstant.TXN_TYPE_SALE_ORDER + ', ' + TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER + ', ' + TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN + ', ' + TxnTypeConstant.TXN_TYPE_ESTIMATE + ') ';

	if (endDate) {
		querySearch += ' and ' + Queries.COL_CLOSED_LINK_TXN_DATE + ' <= ' + endDateStr;
	}

	// Either cheque is open or cheque is closed after end date
	querySearch += ' and (' + Queries.COL_CHEQUE_CURRENT_STATUS + ' = ' + ChequeStatus.OPEN + ' ';
	if (endDate) {
		querySearch += ' or (' + Queries.COL_CHEQUE_CURRENT_STATUS + ' = ' + ChequeStatus.CLOSE + ' and ' + Queries.COL_CHEQUE_TRANSFER_DATE + ' >= ' + nextDayStr + ')';
	}
	querySearch += ') ';

	querySearch += ' group by ' + Queries.COL_CLOSED_LINK_TXN_TYPE + ' ';

	var receivableTxnTypes = [TxnTypeConstant.TXN_TYPE_SALE, TxnTypeConstant.TXN_TYPE_CASHIN, TxnTypeConstant.TXN_TYPE_OTHER_INCOME, TxnTypeConstant.TXN_TYPE_SALE_ORDER, TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN];
	var payableTxnTypes = [TxnTypeConstant.TXN_TYPE_PURCHASE, TxnTypeConstant.TXN_TYPE_CASHOUT, TxnTypeConstant.TXN_TYPE_EXPENSE, TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER, TxnTypeConstant.TXN_TYPE_SALE_RETURN];
	var paidCount = 0;
	var paidAmount = 0;
	var receivedCount = 0;
	var receivedAmount = 0;
	runQueryPromise(querySearch).then(function (rows) {
		if (rows) {
			rows.forEach(function (row) {
				var count = MyDouble.convertStringToDouble(row['cheque_count']);
				var amount = MyDouble.convertStringToDouble(row[Queries.COL_TXN_CASH_AMOUNT]);
				var txnType = Number(row[Queries.COL_TXN_TYPE]);
				if (receivableTxnTypes.includes(txnType)) {
					receivedCount += count;
					receivedAmount += amount;
				} else if (payableTxnTypes.includes(txnType)) {
					paidCount += count;
					paidAmount += amount;
				}
			});
		}
		var openChequeDetails = [[receivedCount, receivedAmount], [paidCount, paidAmount]];
		res.send({
			openChequeDetails: openChequeDetails
		});
	});
};

var getSaleDetails = function getSaleDetails(req, res) {
	var _req$payload2 = req.payload,
	    _req$payload2$fromDat = _req$payload2.fromDate,
	    fromDate = _req$payload2$fromDat === undefined ? null : _req$payload2$fromDat,
	    _req$payload2$toDate = _req$payload2.toDate,
	    toDate = _req$payload2$toDate === undefined ? null : _req$payload2$toDate;

	if (toDate) {
		toDate = new Date(toDate);
	}
	if (fromDate) {
		fromDate = new Date(fromDate);
	}

	function getQuery(fromDateStr, toDateStr) {
		return '\n\t\tSELECT TOTAL(' + Queries.COL_TXN_CASH_AMOUNT + ' + ' + Queries.COL_TXN_BALANCE_AMOUNT + ') AS TOTAL_AMOUNT, ' + Queries.COL_TXN_TYPE + ', ' + Queries.COL_TXN_DATE + '\n\t\tFROM ' + Queries.DB_TABLE_TRANSACTIONS + '\n\t\tWHERE ' + Queries.COL_TXN_TYPE + ' IN (' + TxnTypeConstant.TXN_TYPE_SALE + ', ' + TxnTypeConstant.TXN_TYPE_SALE_RETURN + ')\n\t\tAND ' + Queries.COL_TXN_DATE + ' >= ' + fromDateStr + ' AND ' + Queries.COL_TXN_DATE + ' <= ' + toDateStr + '\n\t\tGROUP BY ' + Queries.COL_TXN_DATE + ', ' + Queries.COL_TXN_TYPE + '\n\t\tORDER BY ' + Queries.COL_TXN_DATE + '\n\t';
	}
	var toDateStr = "'" + MyDate.getDate('y-m-d', toDate) + " 23:59:59'";
	var fromDateStr = "'" + MyDate.getDate('y-m-d', fromDate) + " 00:00:00'";

	var queryForTotalAmount = getQuery(fromDateStr, toDateStr);
	var totalAmountPromise = runQueryPromise(queryForTotalAmount).then(function (rows) {
		var totalAmount = 0;
		rows.forEach(function (row) {
			if (row[Queries.COL_TXN_TYPE] == TxnTypeConstant.TXN_TYPE_SALE) {
				totalAmount += Number(row['TOTAL_AMOUNT']);
			} else if (row[Queries.COL_TXN_TYPE] == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
				totalAmount -= Number(row['TOTAL_AMOUNT']);
			}
		});
		return totalAmount;
	});

	var currentDateStr = '\'' + MyDate.getDate('y-m-d') + ' 23:59:59\'';
	var before6MonthsDate = new Date();
	before6MonthsDate.setMonth(before6MonthsDate.getMonth() - 6, 1);
	var before6MonthsDateStr = '\'' + MyDate.getDate('y-m-d', before6MonthsDate) + ' 00:00:00\'';
	var queryForGraphData = getQuery(before6MonthsDateStr, currentDateStr);
	var graphDataPromise = runQueryPromise(queryForGraphData).then(function (rows) {
		var daily = (0, _create2.default)(null);
		var weekly = (0, _create2.default)(null);
		var monthly = (0, _create2.default)(null);
		var ENTRY_COUNT = 6;
		for (var i = 0; i < ENTRY_COUNT; i++) {
			var dayDate = new Date();
			dayDate.setDate(dayDate.getDate() - i);
			daily[MyDate.getDate('m-d--y', dayDate)] = 0;

			var weekDate = new Date();
			weekDate.setDate(weekDate.getDate() - i * 7);
			var weekForWeekDate = MyDate.getWeekOfMonth(weekDate);
			var monthForWeekDate = MyDate.getMonthName(weekDate);
			var yearForWeekDate = MyDate.getLastTwoDigitOfYear(weekDate);
			weekly['Week: ' + weekForWeekDate + '--' + monthForWeekDate + '-' + yearForWeekDate] = 0;

			var monthDate = new Date();
			monthDate.setMonth(monthDate.getMonth() - i);
			var monthForMonthDate = MyDate.getMonthName(monthDate);
			var yearForMonthDate = MyDate.getLastTwoDigitOfYear(monthDate);
			monthly[monthForMonthDate + '-' + yearForMonthDate] = 0;
		}
		rows.forEach(function (row) {
			var date = MyDate.getDateObj(row[Queries.COL_TXN_DATE], 'yyyy-mm-dd', '-');
			var week = MyDate.getWeekOfMonth(date);
			var month = MyDate.getMonthName(date);
			var year = MyDate.getLastTwoDigitOfYear(date);
			var weeklyPropName = 'Week: ' + week + '--' + month + '-' + year;
			var dailyPropName = MyDate.getDate('m-d--y', date);
			var monthlyPropName = month + '-' + year;
			if (dailyPropName in daily) {
				daily[dailyPropName] = Number(row['TOTAL_AMOUNT']) + daily[dailyPropName];
			}
			if (weeklyPropName in weekly) {
				weekly[weeklyPropName] = Number(row['TOTAL_AMOUNT']) + weekly[weeklyPropName];
			}
			if (monthlyPropName in monthly) {
				monthly[monthlyPropName] = Number(row['TOTAL_AMOUNT']) + monthly[monthlyPropName];
			}
		});
		return {
			daily: (0, _keys2.default)(daily).map(function (key) {
				return [key, daily[key]];
			}).reverse(),
			weekly: (0, _keys2.default)(weekly).map(function (key) {
				return [key, weekly[key]];
			}).reverse(),
			monthly: (0, _keys2.default)(monthly).map(function (key) {
				return [key, monthly[key]];
			}).reverse()
		};
	});
	_promise2.default.all([totalAmountPromise, graphDataPromise]).then(function (_ref6) {
		var _ref7 = (0, _slicedToArray3.default)(_ref6, 2),
		    totalAmount = _ref7[0],
		    graphData = _ref7[1];

		res.send({
			totalAmount: totalAmount,
			graphData: graphData
		});
	});
};

var getExpenseDetails = function getExpenseDetails(req, res) {
	var _req$payload3 = req.payload,
	    _req$payload3$fromDat = _req$payload3.fromDate,
	    fromDate = _req$payload3$fromDat === undefined ? null : _req$payload3$fromDat,
	    _req$payload3$toDate = _req$payload3.toDate,
	    toDate = _req$payload3$toDate === undefined ? null : _req$payload3$toDate;

	if (toDate) {
		toDate = new Date(toDate);
	}
	if (fromDate) {
		fromDate = new Date(fromDate);
	}

	function getQuery(fromDateStr, toDateStr) {
		return '\n\t\tSELECT TOTAL(' + Queries.COL_TXN_CASH_AMOUNT + ' + ' + Queries.COL_TXN_BALANCE_AMOUNT + ') AS TOTAL_AMOUNT, ' + Queries.COL_TXN_DATE + ' AS TXN_DATE\n\t\tFROM ' + Queries.DB_TABLE_TRANSACTIONS + '\n\t\tWHERE ' + Queries.COL_TXN_TYPE + ' = ' + TxnTypeConstant.TXN_TYPE_EXPENSE + '\n\t\tAND ' + Queries.COL_TXN_DATE + ' >= ' + fromDateStr + ' AND ' + Queries.COL_TXN_DATE + ' <= ' + toDateStr + '\n\t\tGROUP BY  TXN_DATE\n\t\tUNION ALL\n\t\tSELECT TOTAL(CASE \n\t\t\tWHEN ' + Queries.COL_LOAN_TXN_TYPE + ' = ' + TxnTypeConstant.TXN_TYPE_LOAN_EMI_PAYMENT + ' THEN ' + Queries.COL_LOAN_TXN_INTEREST + '\n\t\t\tWHEN ' + Queries.COL_LOAN_TXN_TYPE + ' = ' + TxnTypeConstant.TXN_TYPE_LOAN_PROCESSING_FEE + ' THEN ' + Queries.COL_LOAN_TXN_PRINCIPAL + '\n\t\t\tEND) AS TOTAL_AMOUNT, ' + Queries.COL_LOAN_TXN_DATE + ' AS TXN_DATE\n\t\t\tFROM ' + Queries.DB_TABLE_LOAN_TRANSACTIONS + '\n\t\t\tWHERE ' + Queries.COL_LOAN_TXN_TYPE + ' IN (' + TxnTypeConstant.TXN_TYPE_LOAN_PROCESSING_FEE + ', ' + TxnTypeConstant.TXN_TYPE_LOAN_EMI_PAYMENT + ')\n\t\t\tAND ' + Queries.COL_LOAN_TXN_DATE + ' >= ' + fromDateStr + ' AND ' + Queries.COL_LOAN_TXN_DATE + ' <= ' + toDateStr + '\n\t\tGROUP BY  TXN_DATE\n\t\tORDER BY  TXN_DATE\n\t';
	}
	var toDateStr = "'" + MyDate.getDate('y-m-d', toDate) + " 23:59:59'";
	var fromDateStr = "'" + MyDate.getDate('y-m-d', fromDate) + "'";

	var queryForTotalAmount = getQuery(fromDateStr, toDateStr);
	var totalAmountPromise = runQueryPromise(queryForTotalAmount).then(function (rows) {
		var totalAmount = 0;
		rows.forEach(function (row) {
			totalAmount += Number(row['TOTAL_AMOUNT']);
		});
		return totalAmount;
	});

	var currentDateStr = '\'' + MyDate.getDate('y-m-d') + ' 23:59:59\'';
	var before6MonthsDate = new Date();
	before6MonthsDate.setMonth(before6MonthsDate.getMonth() - 6, 1);
	var before6MonthsDateStr = '\'' + MyDate.getDate('y-m-d', before6MonthsDate) + ' 00:00:00\'';
	var queryForGraphData = getQuery(before6MonthsDateStr, currentDateStr);
	var graphDataPromise = runQueryPromise(queryForGraphData).then(function (rows) {
		var daily = (0, _create2.default)(null);
		var weekly = (0, _create2.default)(null);
		var monthly = (0, _create2.default)(null);
		var ENTRY_COUNT = 6;
		for (var i = 0; i < ENTRY_COUNT; i++) {
			var dayDate = new Date();
			dayDate.setDate(dayDate.getDate() - i);
			daily[MyDate.getDate('m-d--y', dayDate)] = 0;

			var weekDate = new Date();
			weekDate.setDate(weekDate.getDate() - i * 7);
			var weekForWeekDate = MyDate.getWeekOfMonth(weekDate);
			var monthForWeekDate = MyDate.getMonthName(weekDate);
			var yearForWeekDate = MyDate.getLastTwoDigitOfYear(weekDate);
			weekly['Week: ' + weekForWeekDate + '--' + monthForWeekDate + '-' + yearForWeekDate] = 0;

			var monthDate = new Date();
			monthDate.setMonth(monthDate.getMonth() - i);
			var monthForMonthDate = MyDate.getMonthName(monthDate);
			var yearForMonthDate = MyDate.getLastTwoDigitOfYear(monthDate);
			monthly[monthForMonthDate + '-' + yearForMonthDate] = 0;
		}
		rows.forEach(function (row) {
			var date = MyDate.getDateObj(row['TXN_DATE'], 'yyyy-mm-dd', '-');
			var week = MyDate.getWeekOfMonth(date);
			var month = MyDate.getMonthName(date);
			var year = MyDate.getLastTwoDigitOfYear(date);
			var weeklyPropName = 'Week: ' + week + '--' + month + '-' + year;
			var dailyPropName = MyDate.getDate('m-d--y', date);
			var monthlyPropName = month + '-' + year;
			if (dailyPropName in daily) {
				daily[dailyPropName] = Number(row['TOTAL_AMOUNT']) + daily[dailyPropName];
			}
			if (weeklyPropName in weekly) {
				weekly[weeklyPropName] = Number(row['TOTAL_AMOUNT']) + weekly[weeklyPropName];
			}
			if (monthlyPropName in monthly) {
				monthly[monthlyPropName] = Number(row['TOTAL_AMOUNT']) + monthly[monthlyPropName];
			}
		});
		return {
			daily: (0, _keys2.default)(daily).map(function (key) {
				return [key, daily[key]];
			}).reverse(),
			weekly: (0, _keys2.default)(weekly).map(function (key) {
				return [key, weekly[key]];
			}).reverse(),
			monthly: (0, _keys2.default)(monthly).map(function (key) {
				return [key, monthly[key]];
			}).reverse()
		};
	});
	_promise2.default.all([totalAmountPromise, graphDataPromise]).then(function (_ref8) {
		var _ref9 = (0, _slicedToArray3.default)(_ref8, 2),
		    totalAmount = _ref9[0],
		    graphData = _ref9[1];

		res.send({
			totalAmount: totalAmount,
			graphData: graphData
		});
	});
};

function getQueryForTotalTxnAmount(fromDateStr, toDateStr) {
	var txnTypes = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

	return '\n\tSELECT TOTAL(' + Queries.COL_TXN_CASH_AMOUNT + ' + ' + Queries.COL_TXN_BALANCE_AMOUNT + ') AS TOTAL_AMOUNT, ' + Queries.COL_TXN_TYPE + '\n\tFROM ' + Queries.DB_TABLE_TRANSACTIONS + '\n\tWHERE ' + Queries.COL_TXN_TYPE + ' IN (' + txnTypes + ')\n\tAND ' + Queries.COL_TXN_DATE + ' >= ' + fromDateStr + ' AND ' + Queries.COL_TXN_DATE + ' <= ' + toDateStr + '\n\tGROUP BY ' + Queries.COL_TXN_TYPE + '\n\t';
}
var getPurchaseAmount = function getPurchaseAmount(req, res) {
	var _req$payload4 = req.payload,
	    _req$payload4$fromDat = _req$payload4.fromDate,
	    fromDate = _req$payload4$fromDat === undefined ? null : _req$payload4$fromDat,
	    _req$payload4$toDate = _req$payload4.toDate,
	    toDate = _req$payload4$toDate === undefined ? null : _req$payload4$toDate;

	if (toDate) {
		toDate = new Date(toDate);
	}
	if (fromDate) {
		fromDate = new Date(fromDate);
	}
	var txnTypes = [TxnTypeConstant.TXN_TYPE_PURCHASE, TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN];
	var toDateStr = '\'' + MyDate.getDate('y-m-d', toDate) + ' 23:59:59\'';
	var fromDateStr = '\'' + MyDate.getDate('y-m-d', fromDate) + ' 00:00:00\'';
	runQueryPromise(getQueryForTotalTxnAmount(fromDateStr, toDateStr, txnTypes)).then(function (rows) {
		var totalAmount = 0;
		rows.forEach(function (row) {
			if (row[Queries.COL_TXN_TYPE] == TxnTypeConstant.TXN_TYPE_PURCHASE) {
				totalAmount += Number(row['TOTAL_AMOUNT']);
			} else if (row[Queries.COL_TXN_TYPE] == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
				totalAmount -= Number(row['TOTAL_AMOUNT']);
			}
		});
		res.send({
			totalAmount: totalAmount
		});
	});
};
var getOtherIncomeAmount = function getOtherIncomeAmount(req, res) {
	var _req$payload5 = req.payload,
	    _req$payload5$fromDat = _req$payload5.fromDate,
	    fromDate = _req$payload5$fromDat === undefined ? null : _req$payload5$fromDat,
	    _req$payload5$toDate = _req$payload5.toDate,
	    toDate = _req$payload5$toDate === undefined ? null : _req$payload5$toDate;

	if (toDate) {
		toDate = new Date(toDate);
	}
	if (fromDate) {
		fromDate = new Date(fromDate);
	}
	var txnTypes = [TxnTypeConstant.TXN_TYPE_OTHER_INCOME];
	var toDateStr = '\'' + MyDate.getDate('y-m-d', toDate) + ' 23:59:59\'';
	var fromDateStr = '\'' + MyDate.getDate('y-m-d', fromDate) + ' 00:00:00\'';
	runQueryPromise(getQueryForTotalTxnAmount(fromDateStr, toDateStr, txnTypes)).then(function (rows) {
		var totalAmount = 0;
		rows.forEach(function (row) {
			if (row[Queries.COL_TXN_TYPE] == TxnTypeConstant.TXN_TYPE_OTHER_INCOME) {
				totalAmount += Number(row['TOTAL_AMOUNT']);
			}
		});
		res.send({
			totalAmount: totalAmount
		});
	});
};

var getPayableAmount = function getPayableAmount(req, res) {
	var query = '\n\tSELECT TOTAL(AMOUNT) AS TOTAL_AMOUNT\n\tFROM ' + Queries.DB_TABLE_NAMES + '\n\tWHERE ' + Queries.COL_NAME_TYPE + ' = ' + NameType.NAME_TYPE_PARTY + ' AND AMOUNT < 0\n\t';
	runQueryPromise(query).then(function (rows) {
		var totalAmount = 0;
		rows.forEach(function (row) {
			totalAmount += Number(row['TOTAL_AMOUNT']);
		});
		res.send({
			totalAmount: totalAmount
		});
	});
};
var getReceivableAmount = function getReceivableAmount(req, res) {
	var query = '\n\tSELECT TOTAL(AMOUNT) AS TOTAL_AMOUNT\n\tFROM ' + Queries.DB_TABLE_NAMES + '\n\tWHERE ' + Queries.COL_NAME_TYPE + ' = ' + NameType.NAME_TYPE_PARTY + ' AND AMOUNT > 0\n\t';
	runQueryPromise(query).then(function (rows) {
		var totalAmount = 0;
		rows.forEach(function (row) {
			totalAmount += Number(row['TOTAL_AMOUNT']);
		});
		res.send({
			totalAmount: totalAmount
		});
	});
};
var getExpiringStocks = function getExpiringStocks(req, res) {
	var _req$payload$endDate3 = req.payload.endDate,
	    endDate = _req$payload$endDate3 === undefined ? null : _req$payload$endDate3;

	if (endDate) {
		endDate = new Date(endDate);
	}
	var dateStr = '\'' + MyDate.getDate('y-m-d', endDate) + ' 23:59:59\'';
	var queryForExpiringStocks = '\n\tSELECT ' + Queries.COL_IST_CURRENT_QUANTITY + ', ' + Queries.COL_ITEM_NAME + ', ' + Queries.COL_IST_EXPIRY_DATE + '\n\tFROM ' + Queries.DB_ITEM_STOCK_TRACKING_TABLE + ' ITEM_STOCK INNER JOIN ' + Queries.DB_TABLE_ITEMS + ' ITEMS\n\tON ITEM_STOCK.' + Queries.COL_IST_ITEM_ID + ' = ITEMS.' + Queries.COL_ITEM_ID + '\n\tWHERE ' + Queries.COL_IST_EXPIRY_DATE + ' <= ' + dateStr + ' AND ' + Queries.COL_ITEM_IS_ACTIVE + ' = 1 AND ' + Queries.COL_IST_CURRENT_QUANTITY + ' > 0\n\tORDER BY ' + Queries.COL_IST_EXPIRY_DATE + '\n\t';
	runQueryPromise(queryForExpiringStocks).then(function (rows) {
		var stockList = rows.map(function (row) {
			return {
				name: row[Queries.COL_ITEM_NAME],
				quantity: row[Queries.COL_IST_CURRENT_QUANTITY]
			};
		});
		res.send({
			stockList: stockList
		});
	});
};
function getIsPrivacyEnabled(req, res) {
	var query = '\n\tSELECT ' + Queries.COL_SETTING_VALUE + ' FROM ' + Queries.DB_TABLE_SETTINGS + '\n\tWHERE ' + Queries.COL_SETTING_KEY + ' = \'' + Queries.SETTING_PRIVACY_MODE + '\'\n\t';
	runQueryPromise(query).then(function (rows) {
		var row = rows[0];
		var isPrivacyEnabled = false;
		if (row) {
			isPrivacyEnabled = row[Queries.COL_SETTING_VALUE] == '1';
		}
		res.send(isPrivacyEnabled);
	});
}
function getCanDisplayLowStocks(req, res) {
	var query = '\n\tSELECT 1 FROM ' + Queries.DB_TABLE_TRANSACTIONS + ' WHERE ' + Queries.COL_TXN_TYPE + ' = \'' + TxnTypeConstant.TXN_TYPE_PURCHASE + '\'\n\tUNION ALL\n\tSELECT 1 FROM ' + Queries.DB_TABLE_ITEM_ADJUSTMENT + '\n\tLIMIT 3\n\t';
	runQueryPromise(query).then(function (rows) {
		var canDisplayLowStocks = rows && rows.length >= 3;
		res.send(canDisplayLowStocks);
	});
}
module.exports = (_module$exports = {}, (0, _defineProperty3.default)(_module$exports, IPCActions.GET_CASH_IN_HAND_AMOUNT, getCashInHandAmount), (0, _defineProperty3.default)(_module$exports, IPCActions.GET_OPEN_ORDER_DETAILS, getOpenOrderDetails), (0, _defineProperty3.default)(_module$exports, IPCActions.GET_OPEN_CHEQUE_DETAILS, getOpenChequeDetails), (0, _defineProperty3.default)(_module$exports, IPCActions.GET_SALE_DETAILS, getSaleDetails), (0, _defineProperty3.default)(_module$exports, IPCActions.GET_PAYABLE_AMOUNT, getPayableAmount), (0, _defineProperty3.default)(_module$exports, IPCActions.GET_RECEIVABLE_AMOUNT, getReceivableAmount), (0, _defineProperty3.default)(_module$exports, IPCActions.GET_PURCHASE_AMOUNT, getPurchaseAmount), (0, _defineProperty3.default)(_module$exports, IPCActions.GET_OTHER_INCOME_AMOUNT, getOtherIncomeAmount), (0, _defineProperty3.default)(_module$exports, IPCActions.GET_EXPIRING_STOCK_DETAILS, getExpiringStocks), (0, _defineProperty3.default)(_module$exports, IPCActions.GET_EXPENSE_DETAILS, getExpenseDetails), (0, _defineProperty3.default)(_module$exports, IPCActions.GET_CARDS_ENABLED_IN_SETTINGS, getCardsEnabledInSettings), (0, _defineProperty3.default)(_module$exports, IPCActions.GET_IS_PRIVACY_ENABLED, getIsPrivacyEnabled), (0, _defineProperty3.default)(_module$exports, IPCActions.GET_CAN_DISPLAY_LOW_STOCKS, getCanDisplayLowStocks), _module$exports);