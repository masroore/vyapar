var transactionOpenCount = 0;
var TransactionManager = function TransactionManager() {
	var SqliteDBHelper = require('./SqliteDBHelper.js');
	var sqlitedbhelper = new SqliteDBHelper();

	this.BeginTransaction = function () {
		var isTransactionBeginSuccess = true;

		if (transactionOpenCount == 0) {
			if (sqlitedbhelper.BeginTransaction()) {
				isTransactionBeginSuccess = true;
			} else {
				isTransactionBeginSuccess = false;
			}
		}
		if (isTransactionBeginSuccess) {
			transactionOpenCount++;
		}
		return isTransactionBeginSuccess;
	};

	this.CommitTransaction = function () {
		var isClosebook = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

		try {
			if (transactionOpenCount == 1) {
				// return true;
				return sqlitedbhelper.CommitTransaction(isClosebook);
			}
		} catch (err) {
			return false;
		}
		return true;
	};

	this.EndTransaction = function () {
		// alert('End txn '+transactionOpenCount);
		console.log(transactionOpenCount);
		if (transactionOpenCount == 1) {
			sqlitedbhelper.EndTransaction();
			transactionOpenCount--;
		} else if (transactionOpenCount > 1) {
			transactionOpenCount--;
		} else {}
	};

	this.closeConnection = function () {
		return sqlitedbhelper.closeConnection();
	};
};

module.exports = TransactionManager;