var sqlite3 = require('sqlite3');
var fs = require('fs');

var app = require('electron').remote.app;

var appPath = app.getPath('userData');
var newDB = fs.readFileSync(appPath + '/BusinessNames/currentDB.txt').toString();
var DB = new sqlite3.Database(newDB);

process.on('exit', function () {
  return DB.close();
});
process.on('SIGINT', function () {
  return DB.close();
});
process.on('SIGHUP', function () {
  return DB.close();
});
process.on('SIGTERM', function () {
  return DB.close();
});

module.exports = DB;