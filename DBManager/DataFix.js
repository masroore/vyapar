var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
var sqliteDBHelper = new SqliteDBHelper();
var TransactionManager = require('./../DBManager/TransactionManager.js');
var DataVerificationObject = require('./../BizLogic/DataVerificationObject.js');

var DataFix = function () {
	function DataFix() {
		(0, _classCallCheck3.default)(this, DataFix);
	}

	(0, _createClass3.default)(DataFix, [{
		key: 'verifyItemQty',
		value: function verifyItemQty() {
			try {
				var expectedValues = sqliteDBHelper.getItemExpectedData();
				var results = [];
				var ItemCache = require('./../Cache/ItemCache.js');
				var itemCache = new ItemCache();
				itemCache.getListOfItemsAndServiceObject().forEach(function (item, key) {
					var itemId = Number(item.getItemId());
					var expectedValue = MyDouble.roundOffToNDecimalPlaces(expectedValues.get(itemId) ? expectedValues.get(itemId) : 0, 7);
					var currentValue = MyDouble.roundOffToNDecimalPlaces(item.getItemStockQuantity(), 7);
					if (expectedValue != currentValue) {
						var dataVerificationObject = new DataVerificationObject();
						dataVerificationObject.setId(itemId);
						dataVerificationObject.setExpectedValue(expectedValue);
						dataVerificationObject.setCurrentValue(currentValue);
						results.push(dataVerificationObject);
					}
				});
				return results;
			} catch (err) {
				logger.error('Error in verify my data' + err);
				ToastHelper.error('There was some error while verifying your items. Please contact Vyapar Tech support.');
				return [];
			}
		}
	}, {
		key: 'verifyNameBalances',
		value: function verifyNameBalances() {
			try {
				var NameCache = require('./../Cache/NameCache.js');
				var nameCache = new NameCache();
				var sqliteDBHelper = new SqliteDBHelper();
				var expectedValues = new _map2.default();
				expectedValues = sqliteDBHelper.getExpectedNameBalance();
				var result = [];
				var danglingCount = 0;

				var nameList = nameCache.getListOfNamesObject();

				for (var i = 0; i < nameList.length; i++) {
					var name = nameList[i];
					var nameId = Number(name.getNameId());
					if (name && name != null) {
						var expectedValue = MyDouble.roundOffToNDecimalPlaces(expectedValues.get(nameId) ? expectedValues.get(nameId) : 0, 5);
						var currentValue = MyDouble.roundOffToNDecimalPlaces(name.getAmount(), 5);
						if (expectedValue != currentValue) {
							var dataVerificationObject = new DataVerificationObject();
							dataVerificationObject.setId(nameId);
							dataVerificationObject.setExpectedValue(expectedValue);
							dataVerificationObject.setCurrentValue(currentValue);
							result.push(dataVerificationObject);
						}
					} else {
						danglingCount++;
					}
				}
				return result;
			} catch (err) {
				logger.error('Error in verify my data' + err);
				ToastHelper.error('There was some error while verifying your parties. Please contact Vyapar Tech support.');
				return [];
			}
		}
	}, {
		key: 'fixFaultyDataInDB',
		value: function fixFaultyDataInDB(itemListToBeFixed, partyListToBeFixed) {
			var result = void 0;
			var transactionManager = new TransactionManager();
			var isTransactionBeginSuccess = transactionManager.BeginTransaction();
			if (!isTransactionBeginSuccess) {
				result = false;
				return result;
			}
			var itemFixResult = sqliteDBHelper.fixItemStockQuantity(itemListToBeFixed);
			var nameBalanceFixResult = sqliteDBHelper.fixNameBalances(partyListToBeFixed);
			result = itemFixResult && nameBalanceFixResult;
			if (result) {
				if (!transactionManager.CommitTransaction()) {
					result = false;
				}
			}
			transactionManager.EndTransaction();
			return result;
		}
	}]);
	return DataFix;
}();

module.exports = DataFix;