/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var itemStockTracking = sequelize.define('itemStockTracking', {
		istId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'ist_id'
		},
		istBatchNumber: {
			type: DataTypes.STRING(30),
			allowNull: true,
			defaultValue: '',
			field: 'ist_batch_number'
		},
		istSerialNumber: {
			type: DataTypes.STRING(30),
			allowNull: true,
			defaultValue: '',
			field: 'ist_serial_number'
		},
		istMrp: {
			type: 'double',
			allowNull: true,
			defaultValue: '0',
			field: 'ist_mrp'
		},
		istExpiryDate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: 'null',
			field: 'ist_expiry_date'
		},
		istManufacturingDate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: 'null',
			field: 'ist_manufacturing_date'
		},
		istSize: {
			type: DataTypes.STRING(100),
			allowNull: true,
			defaultValue: '',
			field: 'ist_size'
		},
		istItemId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'kb_items',
				key: 'item_id'
			},
			field: 'ist_item_id'
		},
		istCurrentQuantity: {
			type: 'double',
			allowNull: true,
			defaultValue: '0',
			field: 'ist_current_quantity'
		},
		istOpeningQuantity: {
			type: 'double',
			allowNull: true,
			defaultValue: '0',
			field: 'ist_opening_quantity'
		},
		istType: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			field: 'ist_type'
		}
	}, {
		tableName: 'kb_item_stock_tracking'
	});
	return itemStockTracking;
};