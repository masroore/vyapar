/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var partyGroups = sequelize.define('partyGroups', {
		partyGroupId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'party_group_id'
		},
		partyGroupName: {
			type: DataTypes.STRING(1024),
			allowNull: true,
			field: 'party_group_name'
		}
	}, {
		tableName: 'kb_party_groups'
	});
	partyGroups.associate = function (models) {
		partyGroups.hasMany(models.names, { foreignKey: 'name_group_id' });
	};
	return partyGroups;
};