/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var BankAdjustments = sequelize.define('bankAdjustments', {
		bankAdjId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'bank_adj_id'
		},
		bankAdjBankId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'kb_paymentTypes',
				key: 'paymentType_id'
			},
			field: 'bank_adj_bank_id'
		},
		bankAdjType: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'bank_adj_type'
		},
		bankAdjAmount: {
			type: 'double',
			allowNull: true,
			defaultValue: '0',
			field: 'bank_adj_amount'
		},
		bankAdjDate: {
			type: DataTypes.DATEONLY,
			allowNull: true,
			field: 'bank_adj_date'
		},
		bankAdjDescription: {
			type: DataTypes.STRING(1024),
			allowNull: true,
			defaultValue: '',
			field: 'bank_adj_description'
		},
		bankAdjToBankId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'kb_paymentTypes',
				key: 'paymentType_id'
			},
			field: 'bank_adj_to_bank_id'
		}
	}, {
		tableName: 'kb_bank_adjustments'
	});
	return BankAdjustments;
};