/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var udfFields = sequelize.define('udfFields', {
		udfFieldId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'udf_field_id'
		},
		udfFieldName: {
			type: DataTypes.STRING(150),
			allowNull: true,
			defaultValue: '',
			field: 'udf_field_name'
		},
		udfFieldType: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			field: 'udf_field_type'
		},
		udfFieldDataType: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			field: 'udf_field_data_type'
		},
		udfFieldDataFormat: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			field: 'udf_field_data_format'
		},
		udfPrintOnInvoice: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			field: 'udf_print_on_invoice'
		},
		udfTxnType: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			field: 'udf_txn_type'
		},
		udfFieldNo: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			field: 'udf_field_no'
		},
		udfFieldStatus: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			field: 'udf_field_status'
		},
		udfFirmId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: 'null',
			field: 'udf_firm_id'
		}
	}, {
		tableName: 'kb_udf_fields'
	});
	return udfFields;
};