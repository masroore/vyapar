/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var firms = sequelize.define('firms', {
		firmId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'firm_id'
		},
		firmName: {
			type: DataTypes.STRING(256),
			allowNull: true,
			field: 'firm_name'
		},
		firmInvoicePrefix: {
			type: DataTypes.STRING(10),
			allowNull: true,
			field: 'firm_invoice_prefix'
		},
		firmInvoiceNumber: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			field: 'firm_invoice_number'
		},
		firmTaxInvoicePrefix: {
			type: DataTypes.STRING(10),
			allowNull: true,
			field: 'firm_tax_invoice_prefix'
		},
		firmTaxInvoiceNumber: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			field: 'firm_tax_invoice_number'
		},
		firmEmail: {
			type: DataTypes.STRING(256),
			allowNull: true,
			field: 'firm_email'
		},
		firmPhone: {
			type: DataTypes.STRING(20),
			allowNull: true,
			field: 'firm_phone'
		},
		firmAddress: {
			type: DataTypes.STRING(256),
			allowNull: true,
			field: 'firm_address'
		},
		firmTinNumber: {
			type: DataTypes.STRING(20),
			allowNull: true,
			field: 'firm_tin_number'
		},
		firmLogo: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: 'null',
			references: {
				model: 'kb_images',
				key: 'image_id'
			},
			field: 'firm_logo'
		},
		firmSignature: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: 'null',
			references: {
				model: 'kb_images',
				key: 'image_id'
			},
			field: 'firm_signature'
		},
		firmGstinNumber: {
			type: DataTypes.STRING(32),
			allowNull: true,
			defaultValue: '',
			field: 'firm_gstin_number'
		},
		firmState: {
			type: DataTypes.STRING(32),
			allowNull: true,
			defaultValue: '',
			field: 'firm_state'
		},
		firmBankName: {
			type: DataTypes.STRING(32),
			allowNull: true,
			defaultValue: '',
			field: 'firm_bank_name'
		},
		firmBankAccountNumber: {
			type: DataTypes.STRING(32),
			allowNull: true,
			defaultValue: '',
			field: 'firm_bank_account_number'
		},
		firmBankIfscCode: {
			type: DataTypes.STRING(32),
			allowNull: true,
			defaultValue: '',
			field: 'firm_bank_ifsc_code'
		},
		firmEstimatePrefix: {
			type: DataTypes.STRING(10),
			allowNull: true,
			defaultValue: '',
			field: 'firm_estimate_prefix'
		},
		firmEstimateNumber: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			field: 'firm_estimate_number'
		},
		firmCashInPrefix: {
			type: DataTypes.STRING(10),
			allowNull: true,
			defaultValue: '',
			field: 'firm_cash_in_prefix'
		},
		firmDeliveryChallanPrefix: {
			type: DataTypes.STRING(32),
			allowNull: true,
			defaultValue: '',
			field: 'firm_delivery_challan_prefix'
		}
	}, {
		tableName: 'kb_firms'
	});
	return firms;
};