/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var ftsVtableSegdir = sequelize.define('ftsVtableSegdir', {
		level: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'level'
		},
		idx: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'idx'
		},
		startBlock: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'start_block'
		},
		leavesEndBlock: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'leaves_end_block'
		},
		endBlock: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'end_block'
		},
		root: {
			type: 'BLOB',
			allowNull: true,
			field: 'root'
		}
	}, {
		tableName: 'kb_fts_vtable_segdir'
	});
	return ftsVtableSegdir;
};