/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var itemCategories = sequelize.define('itemCategories', {
		itemCategoryId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'item_category_id'
		},
		itemCategoryName: {
			type: DataTypes.STRING(1024),
			allowNull: true,
			field: 'item_category_name'
		}
	}, {
		tableName: 'kb_item_categories'
	});
	return itemCategories;
};