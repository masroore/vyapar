/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var Prefixes = sequelize.define('prefixes', {
		prefixId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'prefix_id'
		},
		prefixFirmId: {
			type: DataTypes.INTEGER,
			references: {
				model: 'kb_firm',
				key: 'firm_id'
			},
			field: 'txn_firm_id'
		},
		prefixTxnType: {
			type: DataTypes.INTEGER,
			field: 'txn_txn_type'
		},
		prefixValue: {
			type: DataTypes.STRING(1024),
			field: 'prefix_value'
		},
		prefixIsDefault: {
			type: DataTypes.INTEGER,
			field: 'prefix_is_default'
		}
	}, {
		tableName: 'kb_prefix'
	});
	Prefixes.associate = function (models) {
		Prefixes.hasMany(models.firms, { foreignKey: 'firm_invoice_prefix' });
		Prefixes.hasMany(models.transactions, { foreignKey: 'txn_prefix_id' });
	};
	return Prefixes;
};