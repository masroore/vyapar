/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var customFields = sequelize.define('customFields', {
		customFieldId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'custom_field_id'
		},
		customFieldDisplayName: {
			type: DataTypes.STRING(32),
			allowNull: true,
			defaultValue: '',
			field: 'custom_field_display_name'
		},
		customFieldType: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'custom_field_type'
		},
		customFieldVisibility: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			field: 'custom_field_visibility'
		}
	}, {
		tableName: 'kb_custom_fields'
	});
	return customFields;
};