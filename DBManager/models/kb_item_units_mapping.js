/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var itemUnitsMapping = sequelize.define('itemUnitsMapping', {
		unitMappingId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'unit_mapping_id'
		},
		baseUnitId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'kb_item_units',
				key: 'unit_id'
			},
			field: 'base_unit_id'
		},
		secondaryUnitId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'kb_item_units',
				key: 'unit_id'
			},
			field: 'secondary_unit_id'
		},
		conversionRate: {
			type: 'double',
			allowNull: true,
			field: 'conversion_rate'
		}
	}, {
		tableName: 'kb_item_units_mapping'
	});
	return itemUnitsMapping;
};