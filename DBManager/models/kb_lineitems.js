/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var lineItems = sequelize.define('lineItems', {
		lineitemId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'lineitem_id'
		},
		lineitemTxnId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'kb_transactions',
				key: 'txn_id'
			},
			field: 'lineitem_txn_id'
		},
		itemId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'kb_items',
				key: 'item_id'
			},
			field: 'item_id'
		},
		quantity: {
			type: 'double',
			allowNull: true,
			field: 'quantity'
		},
		priceperunit: {
			type: 'double',
			allowNull: true,
			field: 'priceperunit'
		},
		totalAmount: {
			type: 'double',
			allowNull: true,
			field: 'total_amount'
		},
		lineitemTaxAmount: {
			type: 'double',
			allowNull: true,
			defaultValue: '0',
			field: 'lineitem_tax_amount'
		},
		lineitemDiscountAmount: {
			type: 'double',
			allowNull: true,
			defaultValue: '0',
			field: 'lineitem_discount_amount'
		},
		lineitemUnitId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'kb_item_units',
				key: 'unit_id'
			},
			field: 'lineitem_unit_id'
		},
		lineitemUnitMappingId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'kb_item_units_mapping',
				key: 'unit_mapping_id'
			},
			field: 'lineitem_unit_mapping_id'
		},
		lineitemTaxId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: 'null',
			references: {
				model: 'kb_tax_code',
				key: 'tax_code_id'
			},
			field: 'lineitem_tax_id'
		},
		lineitemMrp: {
			type: 'double',
			allowNull: true,
			defaultValue: 'null',
			field: 'lineitem_mrp'
		},
		lineitemBatchNumber: {
			type: DataTypes.STRING(30),
			allowNull: true,
			defaultValue: '',
			field: 'lineitem_batch_number'
		},
		lineitemExpiryDate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: 'null',
			field: 'lineitem_expiry_date'
		},
		lineitemManufacturingDate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: 'null',
			field: 'lineitem_manufacturing_date'
		},
		lineitemSerialNumber: {
			type: DataTypes.STRING(30),
			allowNull: true,
			defaultValue: '',
			field: 'lineitem_serial_number'
		},
		lineitemCount: {
			type: 'double',
			allowNull: true,
			defaultValue: 'null',
			field: 'lineitem_count'
		},
		lineitemDescription: {
			type: DataTypes.STRING(1024),
			allowNull: true,
			defaultValue: '',
			field: 'lineitem_description'
		},
		lineitemAdditionalCess: {
			type: 'double',
			allowNull: true,
			defaultValue: 'null',
			field: 'lineitem_additional_cess'
		},
		lineitemTotalAmountEdited: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			field: 'lineitem_total_amount_edited'
		},
		lineitemItcApplicable: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			field: 'lineitem_itc_applicable'
		},
		lineitemSize: {
			type: DataTypes.STRING(100),
			allowNull: true,
			defaultValue: '',
			field: 'lineitem_size'
		},
		lineitemIstId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: 'null',
			references: {
				model: 'kb_item_stock_tracking',
				key: 'ist_id'
			},
			field: 'lineitem_ist_id'
		},
		lineitemFreeQuantity: {
			type: 'double',
			allowNull: true,
			defaultValue: '0',
			field: 'lineitem_free_quantity'
		}
	}, {
		tableName: 'kb_lineitems'
	});
	lineItems.associate = function (models) {
		lineItems.belongsTo(models.items, { foreignKey: 'item_id' });
		lineItems.belongsTo(models.transactions, { foreignKey: 'lineitem_txn_id' });
	};
	return lineItems;
};