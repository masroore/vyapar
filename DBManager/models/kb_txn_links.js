/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var txnLinks = sequelize.define('txnLinks', {
		txnLinksId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'txn_links_id'
		},
		txnLinksTxn1Id: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'kb_transactions',
				key: 'txn_id'
			},
			field: 'txn_links_txn_1_id'
		},
		txnLinksTxn2Id: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'kb_transactions',
				key: 'txn_id'
			},
			field: 'txn_links_txn_2_id'
		},
		txnLinksAmount: {
			type: 'double',
			allowNull: true,
			defaultValue: '0',
			field: 'txn_links_amount'
		},
		txnLinksTxn1Type: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'txn_links_txn_1_type'
		},
		txnLinksTxn2Type: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'txn_links_txn_2_type'
		},
		txnLinksClosedTxnRefId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: 'null',
			references: {
				model: 'kb_closed_link_txn_table',
				key: 'closed_link_txn_id'
			},
			field: 'txn_links_closed_txn_ref_id'
		}
	}, {
		tableName: 'kb_txn_links'
	});
	txnLinks.associate = function (models) {
		txnLinks.belongsTo(models.transactions, { foreignKey: 'txn_links_txn_2_id' });
		txnLinks.belongsTo(models.transactions, { foreignKey: 'txn_links_txn_1_id' });
	};
	return txnLinks;
};