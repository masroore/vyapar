/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var itemUnits = sequelize.define('itemUnits', {
		unitId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'unit_id'
		},
		unitName: {
			type: DataTypes.STRING(50),
			allowNull: true,
			field: 'unit_name'
		},
		unitShortName: {
			type: DataTypes.STRING(10),
			allowNull: true,
			field: 'unit_short_name'
		},
		unitFullNameEditable: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '1',
			field: 'unit_full_name_editable'
		},
		unitDeletable: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '1',
			field: 'unit_deletable'
		}
	}, {
		tableName: 'kb_item_units'
	});
	return itemUnits;
};