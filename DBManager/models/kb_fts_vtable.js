/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var ftsVtable = sequelize.define('ftsVtable', {
		ftsNameId: {
			type: '',
			allowNull: true,
			field: 'fts_name_id'
		},
		ftsTxnId: {
			type: '',
			allowNull: true,
			field: 'fts_txn_id'
		},
		ftsText: {
			type: '',
			allowNull: true,
			field: 'fts_text'
		}
	}, {
		tableName: 'kb_fts_vtable'
	});
	return ftsVtable;
};