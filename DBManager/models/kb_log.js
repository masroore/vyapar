/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var log = sequelize.define('log', {
		logId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'log_id'
		},
		reason: {
			type: DataTypes.STRING(1024),
			allowNull: true,
			defaultValue: '',
			field: 'reason'
		},
		details: {
			type: DataTypes.STRING(1024),
			allowNull: true,
			defaultValue: '',
			field: 'details'
		}
	}, {
		tableName: 'kb_log'
	});
	return log;
};