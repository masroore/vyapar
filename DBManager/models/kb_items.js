/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var items = sequelize.define('items', {
		itemId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'item_id'
		},
		itemName: {
			type: DataTypes.STRING(256),
			allowNull: true,
			field: 'item_name'
		},
		itemSaleUnitPrice: {
			type: 'double',
			allowNull: true,
			field: 'item_sale_unit_price'
		},
		itemPurchaseUnitPrice: {
			type: 'double',
			allowNull: true,
			field: 'item_purchase_unit_price'
		},
		itemStockQuantity: {
			type: 'double',
			allowNull: true,
			defaultValue: '0',
			field: 'item_stock_quantity'
		},
		itemMinStockQuantity: {
			type: 'double',
			allowNull: true,
			defaultValue: '0',
			field: 'item_min_stock_quantity'
		},
		itemLocation: {
			type: DataTypes.STRING(256),
			allowNull: true,
			defaultValue: '',
			field: 'item_location'
		},
		itemStockValue: {
			type: 'double',
			allowNull: true,
			defaultValue: '0',
			field: 'item_stock_value'
		},
		itemDateCreated: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'item_date_created'
		},
		itemDateModified: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'item_date_modified'
		},
		itemType: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '1',
			field: 'item_type'
		},
		categoryId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '1',
			references: {
				model: 'kb_item_categories',
				key: 'item_category_id'
			},
			field: 'category_id'
		},
		itemCode: {
			type: DataTypes.STRING(32),
			allowNull: true,
			defaultValue: 'null',
			field: 'item_code'
		},
		baseUnitId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'kb_item_units',
				key: 'unit_id'
			},
			field: 'base_unit_id'
		},
		secondaryUnitId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'kb_item_units',
				key: 'unit_id'
			},
			field: 'secondary_unit_id'
		},
		unitMappingId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'kb_item_units_mapping',
				key: 'unit_mapping_id'
			},
			field: 'unit_mapping_id'
		},
		itemHsnSacCode: {
			type: DataTypes.STRING(32),
			allowNull: true,
			defaultValue: '',
			field: 'item_hsn_sac_code'
		},
		itemTaxId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'kb_tax_code',
				key: 'tax_code_id'
			},
			field: 'item_tax_id'
		},
		itemTaxType: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '2',
			field: 'item_tax_type'
		},
		itemAdditionalCessPerUnit: {
			type: 'double',
			allowNull: true,
			defaultValue: 'null',
			field: 'item_additional_cess_per_unit'
		},
		itemDescription: {
			type: DataTypes.STRING(256),
			allowNull: true,
			defaultValue: '',
			field: 'item_description'
		},
		itemIsActive: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '1',
			field: 'item_is_active'
		}
	}, {
		tableName: 'kb_items'
	});
	items.associate = function (models) {
		items.belongsToMany(models.transactions, { through: models.lineItems, foreignKey: 'item_id' });
	};
	return items;
};