/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var closedLinkTxnTable = sequelize.define('closedLinkTxnTable', {
		closedLinkTxnId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'closed_link_txn_id'
		},
		closedLinkTxnAmount: {
			type: 'double',
			allowNull: true,
			defaultValue: '0',
			field: 'closed_link_txn_amount'
		},
		closedLinkTxnType: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'closed_link_txn_type'
		},
		closedLinkTxnDate: {
			type: DataTypes.DATEONLY,
			allowNull: true,
			field: 'closed_link_txn_date'
		},
		closedLinkTxnRefNumber: {
			type: DataTypes.STRING(50),
			allowNull: true,
			defaultValue: '',
			field: 'closed_link_txn_ref_number'
		}
	}, {
		tableName: 'kb_closed_link_txn_table'
	});
	return closedLinkTxnTable;
};