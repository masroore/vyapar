/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var txnMessageConfig = sequelize.define('txnMessageConfig', {
		txnType: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'txn_type'
		},
		txnFieldId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'txn_field_id'
		},
		txnFieldName: {
			type: DataTypes.STRING(50),
			allowNull: true,
			field: 'txn_field_name'
		},
		txnFieldValue: {
			type: DataTypes.STRING(256),
			allowNull: true,
			field: 'txn_field_value'
		}
	}, {
		tableName: 'kb_txn_message_config'
	});
	return txnMessageConfig;
};