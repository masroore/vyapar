/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var itemAdjustments = sequelize.define('itemAdjustments', {
		itemAdjId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'item_adj_id'
		},
		itemAdjItemId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'kb_items',
				key: 'item_id'
			},
			field: 'item_adj_item_id'
		},
		itemAdjType: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'item_adj_type'
		},
		itemAdjQuantity: {
			type: 'double',
			allowNull: true,
			defaultValue: '0',
			field: 'item_adj_quantity'
		},
		itemAdjDate: {
			type: DataTypes.DATEONLY,
			allowNull: true,
			field: 'item_adj_date'
		},
		itemAdjDescription: {
			type: DataTypes.STRING(1024),
			allowNull: true,
			field: 'item_adj_description'
		},
		itemAdjDateCreated: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'item_adj_date_created'
		},
		itemAdjDateModified: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'item_adj_date_modified'
		},
		itemAdjAtprice: {
			type: 'double',
			allowNull: true,
			defaultValue: '-1',
			field: 'item_adj_atprice'
		},
		itemAdjIstId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: 'null',
			references: {
				model: 'kb_item_stock_tracking',
				key: 'ist_id'
			},
			field: 'item_adj_ist_id'
		}
	}, {
		tableName: 'kb_item_adjustments'
	});
	return itemAdjustments;
};