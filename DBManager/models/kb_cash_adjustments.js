/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var cashAdjustments = sequelize.define('cashAdjustments', {
		cashAdjId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'cash_adj_id'
		},
		cashAdjType: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'cash_adj_type'
		},
		cashAdjAmount: {
			type: 'double',
			allowNull: true,
			defaultValue: '0',
			field: 'cash_adj_amount'
		},
		cashAdjDate: {
			type: DataTypes.DATEONLY,
			allowNull: true,
			field: 'cash_adj_date'
		},
		cashAdjDescription: {
			type: DataTypes.STRING(1024),
			allowNull: true,
			defaultValue: '',
			field: 'cash_adj_description'
		}
	}, {
		tableName: 'kb_cash_adjustments'
	});
	return cashAdjustments;
};