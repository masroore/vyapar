/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var ftsVtableSegments = sequelize.define('ftsVtableSegments', {
		blockid: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'blockid'
		},
		block: {
			type: 'BLOB',
			allowNull: true,
			field: 'block'
		}
	}, {
		tableName: 'kb_fts_vtable_segments'
	});
	return ftsVtableSegments;
};