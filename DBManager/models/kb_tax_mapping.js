/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var taxMapping = sequelize.define('taxMapping', {
		taxMappingId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'tax_mapping_id'
		},
		taxMappingGroupId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'kb_tax_code',
				key: 'tax_code_id'
			},
			field: 'tax_mapping_group_id'
		},
		taxMappingCodeId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'kb_tax_code',
				key: 'tax_code_id'
			},
			field: 'tax_mapping_code_id'
		},
		taxMappingDateCreated: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'tax_mapping_date_created'
		},
		taxMappingDateModified: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'tax_mapping_date_modified'
		}
	}, {
		tableName: 'kb_tax_mapping'
	});
	return taxMapping;
};