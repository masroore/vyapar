/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var settings = sequelize.define('settings', {
		settingId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'setting_id'
		},
		settingKey: {
			type: DataTypes.STRING(256),
			allowNull: true,
			field: 'setting_key'
		},
		settingValue: {
			type: DataTypes.STRING(1024),
			allowNull: true,
			field: 'setting_value'
		}
	}, {
		tableName: 'kb_settings'
	});
	return settings;
};