/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var images = sequelize.define('images', {
		imageId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'image_id'
		},
		imageBitmap: {
			type: 'BLOB',
			allowNull: true,
			field: 'image_bitmap'
		}
	}, {
		tableName: 'kb_images'
	});
	return images;
};