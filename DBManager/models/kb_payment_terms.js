/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var paymentTerms = sequelize.define('paymentTerms', {
		paymentTermId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'payment_term_id'
		},
		termName: {
			type: DataTypes.STRING(50),
			allowNull: true,
			field: 'term_name'
		},
		termDays: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'term_days'
		},
		isDefault: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			field: 'is_default'
		}
	}, {
		tableName: 'kb_payment_terms'
	});
	return paymentTerms;
};