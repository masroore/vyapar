/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var partyItemRate = sequelize.define('partyItemRate', {
		partyItemRateId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'party_item_rate_id'
		},
		partyItemRateItemId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'kb_items',
				key: 'item_id'
			},
			field: 'party_item_rate_item_id'
		},
		partyItemRatePartyId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'kb_names',
				key: 'name_id'
			},
			field: 'party_item_rate_party_id'
		},
		partyItemRateSalePrice: {
			type: 'double',
			allowNull: true,
			defaultValue: '0',
			field: 'party_item_rate_sale_price'
		},
		partyItemRatePurchasePrice: {
			type: 'double',
			allowNull: true,
			defaultValue: '0',
			field: 'party_item_rate_purchase_price'
		}
	}, {
		tableName: 'kb_party_item_rate'
	});
	return partyItemRate;
};