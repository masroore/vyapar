module.exports = function (sequelize, DataTypes) {
	var chequeStatus = sequelize.define('chequeStatus', {
		chequeId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'cheque_id'
		},
		chequeTxnId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'kb_transactions',
				key: 'txn_id'
			},
			field: 'cheque_txn_id'
		},
		chequeCurrentStatus: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			field: 'cheque_current_status'
		},
		chequeTransferDate: {
			type: DataTypes.DATEONLY,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'cheque_transfer_date'
		},
		chequeCloseDesc: {
			type: DataTypes.STRING(1024),
			allowNull: true,
			defaultValue: '',
			field: 'cheque_close_desc'
		},
		transferredToAccount: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '2',
			field: 'transferred_To_Account'
		},
		checkCreationDate: {
			type: DataTypes.DATEONLY,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'check_creation_date'
		},
		checkModificationDate: {
			type: DataTypes.DATEONLY,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'check_modification_date'
		}
	}, {
		tableName: 'kb_cheque_status'
	});
	return chequeStatus;
};