/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var udfValues = sequelize.define('udfValues', {
		udfValueId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'udf_value_id'
		},
		udfValueFieldId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'kb_udf_fields',
				key: 'udf_field_id'
			},
			field: 'udf_value_field_id'
		},
		udfRefId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			field: 'udf_ref_id'
		},
		udfValue: {
			type: DataTypes.STRING(150),
			allowNull: true,
			defaultValue: '',
			field: 'udf_value'
		},
		udfValueFieldType: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			field: 'udf_value_field_type'
		}
	}, {
		tableName: 'kb_udf_values'
	});
	return udfValues;
};