/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var Name = sequelize.define('names', {
		nameId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'name_id'
		},
		dateCreated: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'date_created'
		},
		dateModified: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'date_modified'
		},
		fullName: {
			type: DataTypes.STRING(50),
			allowNull: true,
			field: 'full_name'
		},
		phoneNumber: {
			type: DataTypes.STRING(11),
			allowNull: true,
			field: 'phone_number'
		},
		email: {
			type: DataTypes.STRING(50),
			allowNull: true,
			field: 'email'
		},
		amount: {
			type: 'double',
			allowNull: true,
			field: 'amount'
		},
		dateRemindon: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'date_remindon'
		},
		dateSendSMSOn: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'date_sendsmson'
		},
		dateIgnoretill: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'date_ignoretill'
		},
		address: {
			type: DataTypes.STRING(2000),
			allowNull: true,
			field: 'address'
		},
		nameType: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '1',
			field: 'name_type'
		},
		nameGroupId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '1',
			references: {
				model: 'kb_party_groups',
				key: 'party_group_id'
			},
			field: 'name_group_id'
		},
		tinNumber: {
			type: DataTypes.STRING(20),
			allowNull: true,
			defaultValue: '',
			field: 'name_tin_number'
		},
		gstinNumber: {
			type: DataTypes.STRING(32),
			allowNull: true,
			defaultValue: '',
			field: 'name_gstin_number'
		},
		nameState: {
			type: DataTypes.STRING(32),
			allowNull: true,
			defaultValue: '',
			field: 'name_state'
		},
		nameShippingAddress: {
			type: DataTypes.STRING(2000),
			allowNull: true,
			defaultValue: '',
			field: 'name_shipping_address'
		},
		nameCustomerType: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			field: 'name_customer_type'
		},
		isPartyDetailsSent: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			field: 'is_party_details_sent'
		}
	}, {
		tableName: 'kb_names'
	});
	Name.associate = function (models) {
		Name.hasMany(models.transactions, { foreignKey: 'txn_name_id' });
		// Name.belongsTo(models.transactions, {foreignKey: 'name_group_id'});
		Name.belongsTo(models.partyGroups, { foreignKey: 'name_group_id' });
	};
	return Name;
};