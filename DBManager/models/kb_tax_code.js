/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var taxCode = sequelize.define('taxCode', {
		taxCodeId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'tax_code_id'
		},
		taxCodeName: {
			type: DataTypes.STRING(32),
			allowNull: true,
			field: 'tax_code_name'
		},
		taxRate: {
			type: 'double',
			allowNull: true,
			field: 'tax_rate'
		},
		taxCodeType: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			field: 'tax_code_type'
		},
		taxRateType: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '4',
			field: 'tax_rate_type'
		},
		taxCodeDateCreated: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'tax_code_date_created'
		},
		taxCodeDateModified: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'tax_code_date_modified'
		}
	}, {
		tableName: 'kb_tax_code'
	});
	return taxCode;
};