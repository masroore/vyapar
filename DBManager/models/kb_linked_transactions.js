/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var linkedTransactions = sequelize.define('linkedTransactions', {
		linkedId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'linked_id'
		},
		txnSourceId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			references: {
				model: 'kb_transactions',
				key: 'txn_id'
			},
			field: 'txn_source_id'
		},
		txnDestinationId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'txn_destination_id'
		},
		txnsLinkedDate: {
			type: DataTypes.DATEONLY,
			allowNull: false,
			defaultValue: 'datetime(now)',
			field: 'txns_linked_date'
		}
	}, {
		tableName: 'kb_linked_transactions'
	});
	return linkedTransactions;
};