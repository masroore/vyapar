/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var extraCharges = sequelize.define('extraCharges', {
		extraChargesId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'extra_charges_id'
		},
		extraChargesName: {
			type: DataTypes.STRING(1024),
			allowNull: true,
			field: 'extra_charges_name'
		}
	}, {
		tableName: 'kb_extra_charges'
	});
	return extraCharges;
};