var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _TxnTypeConstant = require('../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _MyDate = require('./../../Utilities/MyDate');

var _MyDate2 = _interopRequireDefault(_MyDate);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* jshint indent: 2 */
module.exports = function (sequelize, DataTypes) {
	var Transactions = sequelize.define('transactions', {
		txnId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'txn_id'
		},
		txnDateCreated: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'txn_date_created'
		},
		txnDateModified: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'txn_date_modified'
		},
		txnNameId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'kb_names',
				key: 'name_id'
			},
			field: 'txn_name_id'
		},
		txnCashAmount: {
			type: 'double',
			allowNull: true,
			field: 'txn_cash_amount'
		},
		txnBalanceAmount: {
			type: 'double',
			allowNull: true,
			field: 'txn_balance_amount'
		},
		txnType: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'txn_type'
		},
		txnDate: {
			type: 'string',
			allowNull: true,
			field: 'txn_date'
		},
		txnDiscountPercent: {
			type: 'double',
			allowNull: true,
			field: 'txn_discount_percent'
		},
		txnTaxPercent: {
			type: 'double',
			allowNull: true,
			field: 'txn_tax_percent'
		},
		txnDiscountAmount: {
			type: 'double',
			allowNull: true,
			field: 'txn_discount_amount'
		},
		txnTaxAmount: {
			type: 'double',
			allowNull: true,
			field: 'txn_tax_amount'
		},
		txnDueDate: {
			type: DataTypes.DATEONLY,
			allowNull: true,
			field: 'txn_due_date'
		},
		txnDescription: {
			type: DataTypes.STRING(1024),
			allowNull: true,
			field: 'txn_description'
		},
		txnImagePath: {
			type: DataTypes.STRING(256),
			allowNull: true,
			field: 'txn_image_path'
		},
		txnPaymentTypeId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '1',
			field: 'txn_payment_type_id'
		},
		txnPaymentReference: {
			type: DataTypes.STRING(50),
			allowNull: true,
			defaultValue: '',
			field: 'txn_payment_reference'
		},
		txnRefNumber: {
			type: DataTypes.STRING(50),
			allowNull: true,
			defaultValue: '',
			field: 'txn_ref_number_char'
		},
		txnStatus: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '1',
			field: 'txn_status'
		},
		txnAc1Amount: {
			type: 'double',
			allowNull: true,
			defaultValue: '0',
			field: 'txn_ac1_amount'
		},
		txnAc2Amount: {
			type: 'double',
			allowNull: true,
			defaultValue: '0',
			field: 'txn_ac2_amount'
		},
		txnAc3Amount: {
			type: 'double',
			allowNull: true,
			defaultValue: '0',
			field: 'txn_ac3_amount'
		},
		txnFirmId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: 'null',
			references: {
				model: 'kb_firms',
				key: 'firm_id'
			},
			field: 'txn_firm_id'
		},
		txnSubType: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			field: 'txn_sub_type'
		},
		txnInvoicePrefix: {
			type: DataTypes.STRING(10),
			allowNull: true,
			defaultValue: 'null',
			field: 'txn_invoice_prefix'
		},
		txnPrefixId: {
			type: DataTypes.STRING(10),
			allowNull: true,
			defaultValue: 'null',
			field: 'txn_prefix_id'
		},
		txnImageId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			field: 'txn_image_id'
		},
		txnTaxId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'kb_tax_code',
				key: 'tax_code_id'
			},
			field: 'txn_tax_id'
		},
		txnCustomField: {
			type: DataTypes.TEXT,
			allowNull: true,
			defaultValue: '',
			field: 'txn_custom_field'
		},
		txnDisplayName: {
			type: DataTypes.STRING(256),
			allowNull: true,
			defaultValue: '',
			field: 'txn_display_name'
		},
		txnReverseCharge: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			field: 'txn_reverse_charge'
		},
		txnPlaceOfSupply: {
			type: DataTypes.STRING(256),
			allowNull: true,
			defaultValue: '',
			field: 'txn_place_of_supply'
		},
		txnRoundOffAmount: {
			type: 'double',
			allowNull: true,
			defaultValue: '0',
			field: 'txn_round_off_amount'
		},
		txnItcApplicable: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			field: 'txn_itc_applicable'
		},
		txnPoDate: {
			type: DataTypes.DATEONLY,
			allowNull: true,
			defaultValue: 'null',
			field: 'txn_po_date'
		},
		txnPoRefNumber: {
			type: DataTypes.STRING(50),
			allowNull: true,
			defaultValue: '',
			field: 'txn_po_ref_number'
		},
		txnReturnDate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: 'null',
			field: 'txn_return_date'
		},
		txnReturnRefNumber: {
			type: DataTypes.STRING(50),
			allowNull: true,
			defaultValue: '',
			field: 'txn_return_ref_number'
		},
		txnEwayBillNumber: {
			type: DataTypes.STRING(50),
			allowNull: true,
			defaultValue: '',
			field: 'txn_eway_bill_number'
		},
		txnCurrentBalance: {
			type: 'double',
			allowNull: true,
			defaultValue: '0',
			field: 'txn_current_balance'
		},
		txnPaymentStatus: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '1',
			field: 'txn_payment_status'
		},
		txnPaymentTermId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: 'null',
			references: {
				model: 'kb_payment_terms',
				key: 'payment_term_id'
			},
			field: 'txn_payment_term_id'
		}
	}, {
		tableName: 'kb_transactions'
	});

	Transactions.getTransactions = function (params) {
		var _sequelize$Sequelize$ = sequelize.Sequelize.Op,
		    or = _sequelize$Sequelize$.or,
		    gte = _sequelize$Sequelize$.gte,
		    lte = _sequelize$Sequelize$.lte;
		var fields = params.fields,
		    _params$include = params.include,
		    include = _params$include === undefined ? [] : _params$include,
		    _params$txnTypes = params.txnTypes,
		    txnTypes = _params$txnTypes === undefined ? [] : _params$txnTypes,
		    fromDate = params.fromDate,
		    toDate = params.toDate,
		    nameId = params.nameId,
		    transactionId = params.transactionId,
		    firmId = params.firmId,
		    _params$order = params.order,
		    order = _params$order === undefined ? [['txnDate', 'DESC'], ['txnId', 'DESC']] : _params$order,
		    _params$start = params.start,
		    start = _params$start === undefined ? 0 : _params$start,
		    _params$limit = params.limit,
		    limit = _params$limit === undefined ? -1 : _params$limit;


		var condition = {};

		if (transactionId) {
			condition.txnType = transactionId;
		}

		if (txnTypes.length) {
			condition.txnType = txnTypes;
		}

		if (fromDate || toDate) {
			if (fromDate && toDate) {
				var _condition$txnDate;

				var fromDateString = _MyDate2.default.getDate('y-m-d', new Date(fromDate)) + ' 00:00:00';
				var toDateString = _MyDate2.default.getDate('y-m-d', new Date(toDate)) + ' 23:59:59';

				condition.txnDate = (_condition$txnDate = {}, (0, _defineProperty3.default)(_condition$txnDate, gte, fromDateString), (0, _defineProperty3.default)(_condition$txnDate, lte, toDateString), _condition$txnDate);
			} else if (fromDate) {
				var _fromDateString = _MyDate2.default.getDate('y-m-d', new Date(fromDate)) + ' 00:00:00';

				condition.txnDate = (0, _defineProperty3.default)({}, gte, _fromDateString);
			} else if (toDate) {
				var _toDateString = _MyDate2.default.getDate('y-m-d', new Date(toDate)) + ' 23:59:59';
				condition.txnDate = (0, _defineProperty3.default)({}, lte, _toDateString);
			}
		}

		if (nameId && nameId > 0) {
			condition.txnNameId = nameId;
		}
		if (firmId) {
			condition[or] = {
				txnFirmId: firmId,
				txnType: [_TxnTypeConstant2.default.TXN_TYPE_POPENBALANCE, _TxnTypeConstant2.default.TXN_TYPE_ROPENBALANCE]
			};
		}

		var query = {
			attributes: fields,
			where: condition,
			offset: start,
			order: order,
			limit: limit
		};
		if (include.length) {
			query.include = include;
		}
		return this.findAll(query);
	};

	Transactions.associate = function (models) {
		Transactions.belongsTo(models.firms, { foreignKey: 'txn_firm_id' });
		Transactions.belongsTo(models.images, { foreignKey: 'txn_image_id' });
		Transactions.belongsTo(models.taxCode, { foreignKey: 'txn_tax_id' });
		Transactions.belongsTo(models.names, { foreignKey: 'txn_name_id' });
		Transactions.belongsTo(models.paymentTerms, {
			foreignKey: 'txn_payment_term_id'
		});
		Transactions.belongsTo(models.prefixes, { foreignKey: 'txn_prefix_id' });
		Transactions.belongsTo(models.paymentTypes, {
			foreignKey: 'txn_payment_type_id'
		});

		Transactions.belongsToMany(models.items, {
			through: models.lineItems,
			foreignKey: 'lineitem_txn_id'
		});
		Transactions.hasMany(models.lineItems, { foreignKey: 'lineitem_txn_id' });
		Transactions.hasMany(models.txnLinks, {
			foreignKey: 'txn_links_txn_1_id',
			as: 'linksToSource'
		});
		Transactions.hasMany(models.txnLinks, {
			foreignKey: 'txn_links_txn_2_id',
			as: 'linksToDestination'
		});
	};
	return Transactions;
};