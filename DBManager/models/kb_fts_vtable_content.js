/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var ftsVtableContent = sequelize.define('ftsVtableContent', {
		docid: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'docid'
		},
		c0FtsNameId: {
			type: '',
			allowNull: true,
			field: 'c0fts_name_id'
		},
		c1FtsTxnId: {
			type: '',
			allowNull: true,
			field: 'c1fts_txn_id'
		},
		c2FtsText: {
			type: '',
			allowNull: true,
			field: 'c2fts_text'
		}
	}, {
		tableName: 'kb_fts_vtable_content'
	});
	return ftsVtableContent;
};