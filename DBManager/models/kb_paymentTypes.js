/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
	var paymentTypes = sequelize.define('paymentTypes', {
		paymentTypeId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			primaryKey: true,
			field: 'paymentType_id'
		},
		paymentTypeType: {
			type: DataTypes.STRING(30),
			allowNull: true,
			defaultValue: 'BANK',
			field: 'paymentType_type'
		},
		paymentTypeName: {
			type: DataTypes.STRING(30),
			allowNull: true,
			field: 'paymentType_name'
		},
		paymentTypeBankName: {
			type: DataTypes.STRING(30),
			allowNull: true,
			defaultValue: '',
			field: 'paymentType_bankName'
		},
		paymentTypeAccountNumber: {
			type: DataTypes.STRING(30),
			allowNull: true,
			defaultValue: '',
			field: 'paymentType_accountNumber'
		},
		paymentTypeOpeningBalance: {
			type: 'double',
			allowNull: true,
			defaultValue: '0',
			field: 'paymentType_opening_balance'
		},
		paymentTypeOpeningDate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'paymentType_opening_date'
		}
	}, {
		tableName: 'kb_paymentTypes'
	});
	return paymentTypes;
};