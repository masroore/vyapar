var DataInserter = function DataInserter() {
	var statusCode = false;
	var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
	var sqlitedbhelper = new SqliteDBHelper();
	var TransactionManager = require('./../DBManager/TransactionManager.js');
	var ItemCache = require('./../Cache/ItemCache.js');
	var PartyModel = require('./../Models/nameModal.js');
	var ErrorCode = require('./../Constants/ErrorCode');
	var UDFValueModel = require('./../Models/udfValueModel.js');
	var UDFFieldsConstant = require('./../Constants/UDFFieldsConstant');
	var ItemType = require('../Constants/ItemType');
	this.updateItemCategoryId = function (catId, listOfItems) {
		var transactionManager = new TransactionManager();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = false;
			return statusCode;
		}

		statusCode = sqlitedbhelper.updateItemCategoryId(catId, listOfItems);
		if (statusCode) {
			if (!transactionManager.CommitTransaction()) {
				statusCode = false;
			} else {
				var itemCache = new ItemCache();
				itemCache.reloadItemCache();
				statusCode = true;
			}
		}
		transactionManager.EndTransaction();
		return statusCode;
	};

	this.updatePartyGroupId = function (grpId, listOfNames) {
		var statusCode = false;
		var transactionManager = new TransactionManager();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = false;
			return statusCode;
		}

		statusCode = sqlitedbhelper.updatePartyGroupId(grpId, listOfNames);
		if (statusCode) {
			var NameCache = require('./../Cache/NameCache.js');
			var nameCache = new NameCache();
			nameCache.reloadNameCache();
			if (!transactionManager.CommitTransaction()) {
				statusCode = false;
			} else {
				statusCode = true;
			}
		}
		transactionManager.EndTransaction();
		return statusCode;
	};

	this.saveImportedParties = function () {
		function partyLogicToModal(partyLogic) {
			var modal = new PartyModel();

			// modal.set_name_id()
			modal.set_full_name(partyLogic.getFullName());
			modal.set_phone_number(partyLogic.getPhoneNumber());
			modal.set_email(partyLogic.getEmail());
			modal.set_amount(partyLogic.getAmount());
			modal.set_opening_date(partyLogic.getOpeningDate());
			modal.set_address(partyLogic.getAddress());
			modal.set_group_id(partyLogic.getGroupId());
			modal.set_tin_number(partyLogic.getTinNumber());
			modal.set_name_type(partyLogic.getNameType());
			modal.set_gstin_number(partyLogic.getGstinNumber());
			modal.set_contact_state(partyLogic.getContactState());
			modal.set_shipping_address(partyLogic.getShippingAddress());
			modal.setCustomerType(partyLogic.getCustomerType());

			return modal;
		}

		return function saveImportedParties() {
			var parties = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
			var partyGroupMap = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

			var partyModels = [];
			var statusCode = ErrorCode.ERROR_PARTY_IMPORT_FAILED;
			try {
				var _sqlitedbhelper = new SqliteDBHelper();
				partyModels = parties.map(partyLogicToModal);
				statusCode = _sqlitedbhelper.saveImportedParties(partyModels, partyGroupMap);
			} catch (error) {
				logger && logger.error('Failed to import parties ' + error.message);
			}
			// Since we are not updating anything in Caches before import is successful, we only reload if import is successful
			if (statusCode == ErrorCode.ERROR_PARTY_IMPORT_SUCCESS) {
				var NameCache = require('./../Cache/NameCache.js');
				var PartyGroupCache = require('./../Cache/PartyGroupCache');
				var nameCache = new NameCache();
				var partyGroupCache = new PartyGroupCache();
				partyGroupCache.reloadPartyGroup();
				nameCache.reloadNameCache();
			}
			return statusCode;
		};
	}();

	this.updateActiveInactiveItems = function (itemIds, IsActive) {
		var statusCode = ErrorCode.ERROR_ITEM_UPDATE_SUCCESS;
		var sqliteDBHelper = new SqliteDBHelper();
		var transactionManager = new TransactionManager();
		var ItemCache = require('./../Cache/ItemCache.js');

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_ITEM_UPDATE_FAILED;
			return statusCode;
		}

		for (var i = 0; i < itemIds.length; i++) {
			statusCode = sqliteDBHelper.updateActiveInactiveItems(itemIds[i], IsActive);
			if (statusCode != ErrorCode.ERROR_ITEM_UPDATE_SUCCESS) {
				break;
			}
		}
		if (statusCode == ErrorCode.ERROR_ITEM_UPDATE_SUCCESS) {
			if (!transactionManager.CommitTransaction()) {
				statusCode = ErrorCode.ERROR_ITEM_UPDATE_FAILED;
			} else {
				var itemCache = new ItemCache();
				itemCache.reloadItemCache();
			}
		} else {
			statusCode = ErrorCode.ERROR_ITEM_UPDATE_FAILED;
		}
		transactionManager.EndTransaction();
		return statusCode;
	};

	this.insertImportedItems = function (itemObjList) {
		var ItemCategoryCache = require('../Cache/ItemCategoryCache');
		var ItemCategoryLogic = require('../BizLogic/ItemCategoryLogic');
		var ItemUnitLogic = require('../BizLogic/ItemUnitLogic');
		var ItemUnitCache = require('../Cache/ItemUnitCache');
		var ItemCache = require('../Cache/ItemCache');
		var ItemUnitMapping = require('../BizLogic/ItemunitMapping');
		var ItemUnitMappingCache = require('../Cache/ItemUnitMappingCache');

		var statusCode = ErrorCode.ERROR_ITEM_IMPORT_SUCCESS;
		var listOfItemModel = [];
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqlitedbhelper = new SqliteDBHelper();
		var ItemModel = require('./../Models/ItemModel.js');
		var itemCategoryCache = new ItemCategoryCache();
		var newItemUnitLogic = new ItemUnitLogic();
		var itemUnitCache = new ItemUnitCache();
		var itemUnitMappingCache = new ItemUnitMappingCache();
		var itemCache = new ItemCache();
		var transactionManager = new TransactionManager();

		var getCategoryIdAndStatusCode = function getCategoryIdAndStatusCode(category) {
			var categoryId = ItemType.DEFAULT_CATEGORYID;
			var statusCode = ErrorCode.ERROR_ITEM_IMPORT_SUCCESS;
			if (category) {
				var itemCategoryLogic = new ItemCategoryLogic();
				if (!itemCategoryCache.categoryExists(category)) {
					statusCode = itemCategoryLogic.saveNewCategory(category);
					if (statusCode == ErrorCode.ERROR_ITEMCATEGORY_ALREADYEXISTS || statusCode == ErrorCode.ERROR_ITEMCATEGORY_SAVE_SUCCESS) {
						categoryId = itemCategoryCache.getItemCategoryId(category);
						statusCode = ErrorCode.ERROR_ITEM_IMPORT_SUCCESS;
					}
				} else {
					categoryId = itemCategoryCache.getItemCategoryId(category);
				}
			}
			return { categoryId: categoryId, statusCode: statusCode };
		};
		var getItemUnitIdsAndStatus = function getItemUnitIdsAndStatus(value) {
			var baseUnitId = null;
			var secondaryUnitId = null;
			var unitMappingId = null;
			var validItemUnitData = true;
			var statusCode = ErrorCode.ERROR_ITEM_IMPORT_SUCCESS;

			if (value.baseunit && !value.secondaryunit) {
				var fullName = value.baseunit;
				var shortName = value.baseunit;
				if (!itemUnitCache.containsItemUnitWithFullName(value.baseunit) && !itemUnitCache.containsItemUnitWithShortName(value.baseunit)) {
					statusCode = newItemUnitLogic.addNewUnit(fullName, shortName);
					if (statusCode == ErrorCode.ERROR_UNIT_SAVE_SUCCESS) {
						baseUnitId = itemUnitCache.getItemUnitIdByUnitName(value.baseunit);
						statusCode = ErrorCode.ERROR_ITEM_IMPORT_SUCCESS;
					} else {
						validItemUnitData = false;
					}
				} else {
					if (itemUnitCache.containsItemUnitWithFullName(value.baseunit)) {
						baseUnitId = itemUnitCache.getItemUnitIdByUnitName(value.baseunit);
					} else if (itemUnitCache.containsItemUnitWithShortName(value.baseunit)) {
						baseUnitId = itemUnitCache.getItemUnitIdByShortName(value.baseunit);
					}
				}
			} else if (!value.baseunit && value.secondaryunit) {
				validItemUnitData = false;
			} else if (value.baseunit && value.secondaryunit && value.conversion <= 0) {
				validItemUnitData = false;
			} else if (value.baseunit && value.secondaryunit && value.conversion > 0) {
				fullName = value.baseunit;
				shortName = value.baseunit;
				if (!itemUnitCache.containsItemUnitWithFullName(value.baseunit) && !itemUnitCache.containsItemUnitWithShortName(value.baseunit)) {
					statusCode = newItemUnitLogic.addNewUnit(fullName, shortName);
				}
				if (statusCode == ErrorCode.ERROR_UNIT_SAVE_SUCCESS || statusCode == ErrorCode.ERROR_ITEM_IMPORT_SUCCESS) {
					statusCode = ErrorCode.ERROR_ITEM_IMPORT_SUCCESS;
					fullName = value.secondaryunit;
					shortName = value.secondaryunit;

					if (itemUnitCache.containsItemUnitWithFullName(value.baseunit)) {
						baseUnitId = itemUnitCache.getItemUnitIdByUnitName(value.baseunit);
					} else if (itemUnitCache.containsItemUnitWithShortName(value.baseunit)) {
						baseUnitId = itemUnitCache.getItemUnitIdByShortName(value.baseunit);
					}

					if (!itemUnitCache.containsItemUnitWithFullName(value.secondaryunit) && !itemUnitCache.containsItemUnitWithShortName(value.secondaryunit)) {
						statusCode = newItemUnitLogic.addNewUnit(fullName, shortName);
					}

					if (statusCode == ErrorCode.ERROR_UNIT_SAVE_SUCCESS || statusCode == ErrorCode.ERROR_ITEM_IMPORT_SUCCESS) {
						if (itemUnitCache.containsItemUnitWithFullName(value.secondaryunit)) {
							secondaryUnitId = itemUnitCache.getItemUnitIdByUnitName(value.secondaryunit);
						} else if (itemUnitCache.containsItemUnitWithShortName(value.secondaryunit)) {
							secondaryUnitId = itemUnitCache.getItemUnitIdByShortName(value.secondaryunit);
						}
						statusCode = ErrorCode.ERROR_ITEM_IMPORT_SUCCESS;
					} else {
						validItemUnitData = false;
					}
				} else {
					validItemUnitData = false;
				}
				var itemUnitMapping = new ItemUnitMapping();
				statusCode = itemUnitMapping.addNewUnitMapping(baseUnitId, secondaryUnitId, value.conversion);

				if (statusCode == ErrorCode.ERROR_UNIT_MAPPING_SAVE_SUCCESS) {
					var mappings = itemUnitMappingCache.getMappingIdFromBaseAndSecondaryUnitIds(baseUnitId, secondaryUnitId, value.conversion);
				} else {
					validItemUnitData = false;
				}
			}
			if (mappings) {
				unitMappingId = mappings.getMappingId();
			}

			return { baseUnitId: baseUnitId, secondaryUnitId: secondaryUnitId, unitMappingId: unitMappingId, validItemUnitData: validItemUnitData, statusCode: statusCode };
		};

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_ITEM_UPDATE_FAILED;
			return statusCode;
		}
		$.each(itemObjList, function (key, value) {
			statusCode = ErrorCode.ERROR_ITEM_IMPORT_SUCCESS;
			var itemmodel = new ItemModel();
			var category = value.category || '';

			var catIdAndStatusCode = getCategoryIdAndStatusCode(category);
			var categoryId = catIdAndStatusCode.categoryId;

			statusCode = catIdAndStatusCode.statusCode;
			if (categoryId === 0) {
				return false;
			}

			var itemUnitIdsAndStatusObj = getItemUnitIdsAndStatus(value);
			if (!itemUnitIdsAndStatusObj.validItemUnitData) {
				return false;
			}
			var baseUnitId = itemUnitIdsAndStatusObj.baseUnitId,
			    secondaryUnitId = itemUnitIdsAndStatusObj.secondaryUnitId,
			    unitMappingId = itemUnitIdsAndStatusObj.unitMappingId;

			statusCode = itemUnitIdsAndStatusObj.statusCode;

			itemmodel.setItemCode(value.getItemCode());
			itemmodel.setItemName(value.getItemName());;
			itemmodel.setItemHSNCode(value.getItemHSNCode());
			itemmodel.setItemSaleUnitPrice(value.getItemSaleUnitPrice());
			itemmodel.setItemPurchaseUnitPrice(value.getItemPurchaseUnitPrice());
			itemmodel.setItemOpeningStock(value.getItemOpeningStock());
			itemmodel.setItemStockQuantity(value.getItemStockQuantity());
			itemmodel.setItemMinStockQuantity(value.getItemMinStockQuantity());
			itemmodel.setItemLocation(value.getItemLocation());
			itemmodel.setItemStockValue(value.getItemStockValue());
			itemmodel.setItemType(value.getItemType());
			itemmodel.setItemCategoryId(categoryId);
			itemmodel.setItemTaxTypeSale(value.getItemTaxTypeSale());
			itemmodel.setItemTaxTypePurchase(value.getItemTaxTypeSale());
			itemmodel.setItemTaxId(value.getItemTaxId());
			itemmodel.setBaseUnitId(baseUnitId);
			itemmodel.setSecondaryUnitId(secondaryUnitId);
			itemmodel.setUnitMappingId(unitMappingId);
			itemmodel.setItemDescription(value.getItemDescription());
			listOfItemModel.push(itemmodel);
		});
		if (listOfItemModel.length == itemObjList.length) {
			statusCode = sqlitedbhelper.SaveImportItem(listOfItemModel);
			if (statusCode) {
				if (!transactionManager.CommitTransaction()) {
					statusCode = ErrorCode.ERROR_ITEM_IMPORT_FAILED;
				} else {
					statusCode = ErrorCode.ERROR_ITEM_IMPORT_SUCCESS;
				}
			} else {
				statusCode = ErrorCode.ERROR_ITEM_IMPORT_FAILED;
			}
		} else {
			statusCode = ErrorCode.ERROR_ITEM_IMPORT_FAILED;
		}
		transactionManager.EndTransaction();
		itemCategoryCache.reloadItemCategory();
		itemUnitCache.reloadItemUnitCache();
		itemUnitMappingCache.refreshItemUnitMappingCache();
		if (statusCode == ErrorCode.ERROR_ITEM_IMPORT_SUCCESS) {
			itemCache.reloadItemCache();
		}
		return statusCode;
	};

	this.insertUdfValue = function (udfValueArray, firmId) {
		var transactionManager = new TransactionManager();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();
		var statusCode = ErrorCode.ERROR_SETTING_SAVE_SUCCESS;
		if (!isTransactionBeginSuccess) {
			statusCode = false;
			return statusCode;
		}

		var udfValueModel = new UDFValueModel();
		var UDFFieldsConstant = require('./../Constants/UDFFieldsConstant.js');
		var fieldType = UDFFieldsConstant.FIELD_TYPE_FIRM;
		statusCode = udfValueModel.deleteExtraFieldsValue(firmId, fieldType);
		if (statusCode == ErrorCode.ERROR_UDF_VALUE_DELETE_SUCCESSFUL) {
			statusCode = ErrorCode.ERROR_SETTING_SAVE_SUCCESS;
		}
		if (udfValueArray && udfValueArray.length > 0) {
			for (var i = 0; i < udfValueArray.length; i++) {
				udfValueModel = udfValueArray[i];
				// var uDFValueModel = new UDFValueModel();
				// uDFValueModel.setUdfFieldValue(udfValueModel.getUdfFieldValue());
				// uDFValueModel.setUdfFieldId(udfValueModel.getUdfFieldId());
				// uDFValueModel.setUdfRefId(udfValueModel.getUdfRefId());
				// uDFValueModel.setUdfFieldType(udfValueModel.getUdfFieldType());
				statusCode = udfValueModel.updateUdfValues();
				if (statusCode == ErrorCode.ERROR_UDF_SAVE_SUCCESS) {
					statusCode = ErrorCode.ERROR_SETTING_SAVE_SUCCESS;
				} else {
					break;
				}
			}
		}
		if (statusCode == ErrorCode.ERROR_SETTING_SAVE_SUCCESS) {
			if (!transactionManager.CommitTransaction()) {
				statusCode = ErrorCode.ERROR_SETTING_SAVE_FAILED;
			} else {
				statusCode = ErrorCode.ERROR_SETTING_SAVE_SUCCESS;
			}
		} else {
			statusCode = ErrorCode.ERROR_SETTING_SAVE_FAILED;
		}

		transactionManager.EndTransaction();
		return statusCode;
	};

	this.insertUdfData = function (udfModelMap) {
		var transactionManager = new TransactionManager();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			var statusCode = false;
			return statusCode;
		}
		var UDFCache = require('./../Cache/UDFCache.js');
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqlitedbhelper = new SqliteDBHelper();
		var UDFModel = require('./../Models/UDFModel.js');
		var listOfFieldModel = [];
		var udfCache = new UDFCache();
		udfModelMap.forEach(function (value, key) {
			var udfModel = value;
			var uDFModel = new UDFModel();
			uDFModel.setUdfFieldName(udfModel.getUdfFieldName());
			uDFModel.setUdfFieldType(udfModel.getUdfFieldType());
			uDFModel.setUdfFieldDataType(udfModel.getUdfFieldDataType());
			uDFModel.setUdfFieldDataFormat(udfModel.getUdfFieldDataFormat());
			uDFModel.setUdfFieldPrintOnInvoice(udfModel.getUdfFieldPrintOnInvoice());
			uDFModel.setUdfTxnType(udfModel.getUdfTxnType());
			uDFModel.setUdfFirmId(udfModel.getUdfFirmId());
			uDFModel.setUdfFieldNuber(udfModel.getUdfFieldNumber());
			uDFModel.setUdfFieldStatus(udfModel.getUdfFieldStatus());
			if (udfModel.getUdfTxnType() != 0 || udfModel.getUdfFieldType() == UDFFieldsConstant.FIELD_TYPE_PARTY || udfModel.getUdfFieldType() == UDFFieldsConstant.FIELD_TYPE_FIRM) {
				listOfFieldModel.push(uDFModel);
			}
		});
		statusCode = sqlitedbhelper.updateUDFData(listOfFieldModel);
		if (statusCode) {
			if (!transactionManager.CommitTransaction()) {
				statusCode = false;
			} else {
				udfCache.refreshUDF();
				statusCode = ErrorCode.ERROR_SETTING_SAVE_SUCCESS;
			}
		} else {
			statusCode = ErrorCode.ERROR_SETTING_SAVE_FAILED;
		}
		transactionManager.EndTransaction();
		return statusCode;
	};

	this.addMappingToItems = function (itemIds, mappingId, selectedBaseUnitId) {
		var sqliteDBHelper = new SqliteDBHelper();
		var transactionManager = new TransactionManager();
		var ItemUnitMappingCache = require('./../Cache/ItemUnitMappingCache.js');
		var ItemCache = require('./../Cache/ItemCache.js');
		var itemUnitMappingCache = new ItemUnitMappingCache();
		var mappingObj = itemUnitMappingCache.getItemUnitMapping(mappingId);
		var baseUnitId = null;
		var secondaryUnitId = null;
		if (mappingObj) {
			baseUnitId = mappingObj['baseUnitId'];
			secondaryUnitId = mappingObj['secondaryUnitId'];
		} else if (selectedBaseUnitId) {
			baseUnitId = selectedBaseUnitId;
		} else {
			return ErrorCode.ERROR_SELECT_BASE_UNIT;
		}

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			var statusCode = ErrorCode.ERROR_ITEM_SAVE_FAILED;
			return statusCode;
		}

		statusCode = ErrorCode.ERROR_UNIT_MAPPING_UPDATE_SUCCESS;
		$.each(itemIds, function (key, itemId) {
			statusCode = sqliteDBHelper.addMappingToItems(itemId, baseUnitId, secondaryUnitId, mappingId);
		});
		if (statusCode == ErrorCode.ERROR_UNIT_MAPPING_UPDATE_SUCCESS) {
			if (!transactionManager.CommitTransaction()) {
				statusCode = ErrorCode.ERROR_ITEM_SAVE_FAILED;
			} else {
				var itemCache = new ItemCache();
				itemCache.reloadItemCache();
			}
		}
		transactionManager.EndTransaction();
		return statusCode;
	};

	this.UpdateItemType = function (newItemType, oldItemType) {
		var sqliteDBHelper = new SqliteDBHelper();
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var transactionManager = new TransactionManager();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			var statusCode = ErrorCode.ERROR_ITEM_TYPE_UPDATE_FAILED;
			return statusCode;
		}

		try {
			statusCode = sqliteDBHelper.UpdateItemType(newItemType, oldItemType);
		} catch (err) {
			logger.error('Update Item Type Failuer..', err);
		}
		if (statusCode != ErrorCode.ERROR_ITEM_TYPE_UPDATE_FAILED) {
			if (!transactionManager.CommitTransaction()) {
				statusCode = ErrorCode.ERROR_ITEM_TYPE_UPDATE_FAILED;
			} else {
				var _ItemCache = require('./../Cache/ItemCache.js');
				var itemCache = new _ItemCache();
				itemCache.reloadItemCache();
			}
		}
		transactionManager.EndTransaction();
		return statusCode;
	};

	this.setCurrentExtraChargesValue = function (key, value) {
		var sqliteDBHelper = new SqliteDBHelper();
		var statusCode = sqliteDBHelper.setCurrentExtraChargesValue(key, value);
		return statusCode;
	};

	this.setupIndiaTax = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.setupIndiaTax();
	};

	this.setupGulfTax = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.setupGulfTax();
	};

	this.setupNepaliTax = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.setupNepaliTax();
	};
};

module.exports = DataInserter;