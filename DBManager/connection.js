var ffi, ref, sqlite3, sqlite3Ptr, sqlite3PtrPtr, stringPtr, libsqlite3, dbPtrPtr, dbHandle;
ffi = require('ffi');
ref = require('ref');
// typedef
sqlite3 = ref.types.void; // we don't know what the layout of "sqlite3" looks like
sqlite3_stmt = ref.types.void;
sqlite3Ptr = ref.refType(sqlite3);
sqlite3PtrPtr = ref.refType(sqlite3Ptr);
stringPtr = ref.refType(ref.types.CString);
stmtPtr = ref.refType(sqlite3_stmt);
stmtPtrPtr = ref.refType(stmtPtr);

// binding to a few "libsqlite3" functions...
libsqlite3 = ffi.Library('sqlite3', {
	'sqlite3_open': ['int', ['string', sqlite3PtrPtr]],
	'sqlite3_close': ['int', [sqlite3Ptr]],
	'sqlite3_exec': ['int', [sqlite3Ptr, 'string', 'pointer', 'pointer', stringPtr]],
	'sqlite3_changes': ['int', [sqlite3Ptr]],
	'sqlite3_user_add': ['int', [sqlite3Ptr, 'string', 'string', 'int', 'int']],
	'sqlite3_user_authenticate': ['int', [sqlite3Ptr, 'string', 'string', 'int']],
	'sqlite3_prepare_v2': ['int', [sqlite3Ptr, 'string', 'int', stmtPtrPtr, stringPtr]],
	'sqlite3_bind_blob': ['int', [stmtPtr, 'int', 'string', 'int', 'pointer']],
	'sqlite3_step': ['int', [stringPtr]],
	'sqlite3_finalize': ['int', [stringPtr]],
	'sqlite3_column_bytes': ['int', [stringPtr, 'int']],
	'sqlite3_column_blob': ['pointer', [stringPtr, 'int']]
	// 'sqlite3_create_module': ['int',[sqlite3Ptr, 'string', 'pointer', 'pointer']]
});

var createDB = function createDB(dbName) {
	// now use them:
	// console.log(dbName);
	dbPtrPtr = ref.alloc(sqlite3PtrPtr);
	// Creating databse
	var res1 = libsqlite3.sqlite3_open(dbName, dbPtrPtr);
	// console.log(res1);
	if (!res1) {
		// console.log('DB connection success');
	} else {
			// console.log('DB connection error with errorcode '+res1);
		}
	dbHandle = dbPtrPtr.deref();

	(function () {
		var foreignKeySetQuery = 'PRAGMA foreign_keys = ON;';
		var conn = libsqlite3.sqlite3_exec(dbHandle, foreignKeySetQuery, null, null, null);
		if (conn) {
			console.error('Error', 'Unable to set foreign key constrain ');
		}
	})();

	return dbHandle;
};

module.exports = {
	ffi: ffi,
	ref: ref,
	libsqlite3: libsqlite3,
	createDB: createDB
};