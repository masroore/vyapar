var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

var _set = require('babel-runtime/core-js/set');

var _set2 = _interopRequireDefault(_set);

var _create = require('babel-runtime/core-js/object/create');

var _create2 = _interopRequireDefault(_create);

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = require('electron').remote.app;

var appPath = app.getPath('userData');
var Queries = require('./../Constants/Queries.js');
var ErrorCode = require('./../Constants/ErrorCode.js');
var MyString = require('./../Utilities/MyString.js');
var TxnPaymentStatusConstants = require('./../Constants/TxnPaymentStatusConstants.js');
var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var Domain = require('./../Constants/Domain');
var domain = Domain.thisDomain;
var retVal;

var connection = require(__dirname + '/connection.js');
var fs = require('fs');
var updateDB = fs.readFileSync(appPath + '/BusinessNames/currentDB.txt').toString();
var dbHandleForUpdate = connection.createDB(updateDB);
var libsqlite3 = connection.libsqlite3;
var ffi = connection.ffi;
var ref = connection.ref;
var DeviceHelper = require('../Utilities/deviceHelper');

var mainobj = [];
var queryForUpgrade = [];

var CURRENT_DB_VERSION;
var DUMMY_QUERY_FOR_SYNC = 'update kb_settings set setting_value = 0 where setting_key = \'dummy\'';
var SyncHandler = require('./../Utilities/SyncHandler.js');

var addUpgradeQueries = function addUpgradeQueries(query, table, key) {
	var curId = retVal;
	var obj = {};
	obj['query'] = query;

	if (table && table != Queries.DB_TABLE_SETTINGS && table != Queries.DB_TABLE_TXN_MESSAGE_CONFIG) {
		obj['table_name'] = table;
		obj['insert_id'] = curId;
		obj['insert_key'] = key;
	}

	queryForUpgrade.push((0, _stringify2.default)(obj));
};

var calculateLastSQLiteQueryAffectedRowCountForDbUpgrade = function calculateLastSQLiteQueryAffectedRowCountForDbUpgrade() {
	return libsqlite3.sqlite3_changes(dbHandleForUpdate);
};

var beginTransactionForDBUpgrade = function beginTransactionForDBUpgrade() {
	queryForUpgrade.length = 0;

	var conn = libsqlite3.sqlite3_exec(dbHandleForUpdate, 'begin', null, null, null);

	if (!conn) {
		console.log('Begin Transaction Success');
	} else {
		console.log('Error Begin Transaction');
	}
};

var commitTransactionForDBUpgrade = function commitTransactionForDBUpgrade() {
	var statusCode = true;

	if (SyncHandler.isSyncEnabled()) {
		statusCode = sendQueryToServerForDbUpgrade();
	}

	if (statusCode) {
		queryForUpgrade.length = 0;

		var conn = libsqlite3.sqlite3_exec(dbHandleForUpdate, 'commit', null, null, null);

		if (!conn) {
			console.log('Commit Transaction Success');
			return true;
		} else {
			console.log('Error Commit Transaction');
			return false;
		}
	}
	return false;
};

var endTransactionForDBUpgrade = function endTransactionForDBUpgrade() {
	queryForUpgrade.length = 0;

	var conn = libsqlite3.sqlite3_exec(dbHandleForUpdate, 'rollback', null, null, null);

	if (!conn) {
		console.log('End Transaction Success');
	} else {
		console.log('Error End Transaction');
	}

	alert('Something went wrong in update. Please contact Vyapar tech support.');

	var _require = require('electron'),
	    ipcRenderer = _require.ipcRenderer;

	ipcRenderer.send('changeURL', 'SwitchCompany.html');
};

var sendQueryToServerForDbUpgrade = function sendQueryToServerForDbUpgrade() {
	var successCode = false;
	mainobj = [];
	var querySelect = "select setting_value from kb_settings where setting_key = '" + Queries.SETTING_COMPANY_GLOBAL_ID + "'";
	var comp = libsqlite3.sqlite3_exec(dbHandleForUpdate, querySelect, callbackForFetchAllNameRecords, null, null);
	var globId = mainobj[0].setting_value;

	mainobj = [];
	var querySelect = "select setting_value from kb_settings where setting_key = '" + Queries.SETTING_CHANGE_LOG_NUMBER + "'";
	var comp = libsqlite3.sqlite3_exec(dbHandleForUpdate, querySelect, callbackForFetchAllNameRecords, null, null);
	var chngLog = mainobj[0].setting_value;

	var TokenForSync = require('./../Utilities/TokenForSync');

	if (TokenForSync.ifExistToken()) {
		var tokenFile = TokenForSync.readToken();
		if (tokenFile) {
			var tokenSync = tokenFile.auth_token;
		}
	}
	var deviceInfo = DeviceHelper.getDeviceInfo();
	$.ajax({
		async: false,
		type: 'POST',
		url: domain + '/api/sync/upgrade',
		headers: {
			'Accept': 'application/json',
			'Authorization': 'Bearer ' + tokenSync
		},
		data: {
			company_global_id: globId,
			db_version: CURRENT_DB_VERSION,
			last_change_log_number: chngLog,
			changeLog: {
				'queries': queryForUpgrade,
				'db_version': CURRENT_DB_VERSION
			},
			platform: 0,
			device_id: deviceInfo.deviceId

		},
		success: function success(data) {
			if (data.code && data.code == '200') {
				console.log('queries successfully sent to server.');
				successCode = true;
			}
		},
		error: function error(xhr, ajaxOptions, thrownError) {
			successCode = false;
			if (xhr.status == 401) {
				var SyncHelper = require('./../Utilities/SyncHelper');
				SyncHelper.loginToGetAuthToken();
			} else if (xhr.status == 500) {
				alert(ErrorCode.ERROR_WENT_WRONG);
			} else if (xhr.status == 0 && xhr.statusText == 'timeout') {
				alert(ErrorCode.ERROR_NO_NETWORK);
			} else if (xhr.status != 0 && xhr.status != 401 && xhr.status != 500 && xhr.status != 200) {
				alert(ErrorCode.ERROR_WENT_WRONG);
			}
		}
	});

	return successCode;
};

var setDbToCurrentVersion = function setDbToCurrentVersion(dbVersion) {
	var currentDbVersionCallback = ffi.Callback('int32', ['pointer', 'int32', 'pointer', 'pointer'], function (tmp, cols, argv, colv) {});
	var dbVersionQuery = 'PRAGMA user_version = ' + dbVersion;
	libsqlite3.sqlite3_exec(dbHandleForUpdate, dbVersionQuery, null, null, null);
};

var isSyncEnabled = function isSyncEnabled() {
	mainobj = [];
	var querySelect = "select setting_value from kb_settings where setting_key = 'VYAPAR.SYNCENABLED'";
	var comp = libsqlite3.sqlite3_exec(dbHandleForUpdate, querySelect, callbackForFetchAllNameRecords, null, null);
	var settinValue = mainobj[0].setting_value;

	if (settinValue == '1') {
		return true;
	} else {
		return false;
	}
};

var callbackForFetchAllNameRecords = ffi.Callback('int32', ['pointer', 'int32', 'pointer', 'pointer'], function (tmp, cols, argv, colv) {
	var obj = {};
	var j = 0;
	// console.log(cols);
	var ArrOfNameList = [];
	var NameList = [];
	for (var i = 0; i < cols; i++) {
		// console.log(ref.types.size(colv));
		var val = ref.get(colv, j, ref.types.CString);

		// console.log(val);
		var val1 = ref.get(argv, j, ref.types.CString);
		obj[val] = val1;
		if (val == 'full_name') {
			var full_name = val1;
		}
		j = j + 4;
	}
	mainobj.push(obj);
});

var callbackForInsert = ffi.Callback('int32', ['pointer', 'int32', 'pointer', 'pointer'], function (tmp, cols, argv, colv) {
	retVal = 0;
	retVal = ref.get(argv, 0, ref.types.CString);
});

function insert(tableName, queryInsert, primaryKey) {
	var comp = libsqlite3.sqlite3_exec(dbHandleForUpdate, queryInsert, null, null, null);
	if (!comp) {
		var query = 'Select max(' + primaryKey + ') from ' + tableName;
		var res = libsqlite3.sqlite3_exec(dbHandleForUpdate, query, callbackForInsert, null, null);

		if (!res) {
			if (SyncHandler.isSyncEnabled()) {
				addUpgradeQueries(queryInsert, tableName, primaryKey);
			}
			return 0;
		} else {
			return 1;
		}
	}
	return 1;
}

function executeGeneralQuery(query) {
	// create, update, delete, alter
	var conn = libsqlite3.sqlite3_exec(dbHandleForUpdate, query, null, null, null);

	if (!conn) {
		if (SyncHandler.isSyncEnabled()) {
			addUpgradeQueries(query);
		}
	}

	return conn;
}

var DB_UPDATES = {
	setDefaultSettings: function setDefaultSettings() {
		var getExistingSettings = 'select ' + Queries.COL_SETTING_KEY + ' from ' + Queries.DB_TABLE_SETTINGS;
		var SettingsModel = require('./../Models/SettingsModel.js');
		var settingsModel = new SettingsModel();
		var listOfKeys = settingsModel.getSettingsKey();
		mainobj = [];
		try {
			var conn = libsqlite3.sqlite3_exec(dbHandleForUpdate, getExistingSettings, callbackForFetchAllNameRecords, null, null);
			var existingKeys = (0, _create2.default)(null);
			mainobj.forEach(function (value) {
				existingKeys[value[Queries.COL_SETTING_KEY]] = true;
			});
			var keysToInsert = [];
			if (!conn) {
				keysToInsert = listOfKeys.filter(function (key) {
					return !existingKeys[key];
				});
			}
			var len = keysToInsert.length;
			for (var i = 0; i < len; i++) {
				var key = keysToInsert[i];
				var value = settingsModel.getSettingDefaultValue(key);
				var queryInsert = 'insert or replace into ' + Queries.DB_TABLE_SETTINGS + ' (' + Queries.COL_SETTING_KEY + ',' + Queries.COL_SETTING_VALUE + ') ' + " values ('" + key + "'," + (typeof value == 'undefined' || value === null ? null : '\'' + MyString.escapeStringForDBInsertion(value, true) + '\'') + ')';
				var conn2 = insert(Queries.DB_TABLE_SETTINGS, queryInsert, Queries.COL_SETTING_ID);
			}
		} catch (err) {}
	},
	DB_VERSION_56: function DB_VERSION_56() {
		CURRENT_DB_VERSION = 56;
		beginTransactionForDBUpgrade();

		var query = 'ALTER TABLE ' + Queries.DB_TABLE_FIRMS + ' ADD COLUMN ' + Queries.COL_FIRM_BUSINESS_TYPE + ' INTEGER DEFAULT 0;';
		executeGeneralQuery(query);
		query = 'ALTER TABLE ' + Queries.DB_TABLE_FIRMS + ' ADD COLUMN ' + Queries.COL_FIRM_BUSINESS_CATEGORY + ' VARCHAR(50) DEFAULT \'\';';
		executeGeneralQuery(query);

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},
	DB_VERSION_55: function DB_VERSION_55() {
		CURRENT_DB_VERSION = 55;
		beginTransactionForDBUpgrade();

		var catalogueTableCreateQuery = 'create table ' + Queries.DB_TABLE_CATALOGUE_ITEMS + '( ' + Queries.COL_CATALOGUE_ITEM_ID + ' integer primary key autoincrement ,' + Queries.COL_ITEM_ID + ' integer, ' + Queries.COL_CATALOGUE_ITEM_NAME + ' varchar(256),' + Queries.COL_CATALOGUE_ITEM_CODE + ' varchar(32),' + Queries.COL_CATALOGUE_ITEM_DESCRIPTION + ' varchar(1024),' + Queries.COL_CATALOGUE_ITEM_SALE_UNIT_PRICE + ' double,' + Queries.COL_CATALOGUE_ITEM_CATEGORY_NAME + ' varchar(1024) )';

		var alterItemImageTableAddCatalogueItemId = 'Alter table ' + Queries.DB_TABLE_ITEM_IMAGES + ' add column ' + Queries.COL_CATALOGUE_ITEM_ID + ' integer default null References ' + Queries.DB_TABLE_CATALOGUE_ITEMS + '(' + Queries.COL_CATALOGUE_ITEM_ID + ')';

		executeGeneralQuery(catalogueTableCreateQuery);
		executeGeneralQuery(alterItemImageTableAddCatalogueItemId);

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},
	DB_VERSION_54: function DB_VERSION_54() {
		CURRENT_DB_VERSION = 54;
		beginTransactionForDBUpgrade();
		var StringConstants = require('./../Constants/StringConstants.js');

		var loanAccounts = 'CREATE TABLE "' + Queries.DB_TABLE_LOAN_ACCOUNTS + '" (\n\t\t\t"' + Queries.COL_LOAN_ACCOUNT_ID + '"\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n\t\t\t"' + Queries.COL_LOAN_ACCOUNT_NAME + '"\tTEXT NOT NULL UNIQUE,\n\t\t\t"' + Queries.COL_LOAN_ACCOUNT_LENDER + '"\tTEXT,\n\t\t\t"' + Queries.COL_LOAN_ACCOUNT_NUMBER + '"\tTEXT,\n\t\t\t"' + Queries.COL_LOAN_ACCOUNT_FIRM_ID + '" INTEGER NOT NULL,\n\t\t\t"' + Queries.COL_LOAN_ACCOUNT_DESC + '"\tTEXT,\n\t\t\t"' + Queries.COL_LOAN_ACCOUNT_STARTING_BAL + '"\tDOUBLE NOT NULL,\n\t\t\t"' + Queries.COL_LOAN_ACCOUNT_OPENING_DATE + '"\tDATE NOT NULL,\n\t\t\t"' + Queries.COL_LOAN_ACCOUNT_MODIFIED_DATE + '"\tDATETIME NOT NULL,\n\t\t\t"' + Queries.COL_LOAN_ACCOUNT_CREATED_DATE + '"\tDATETIME NOT NULL,\n\t\t\t"' + Queries.COL_LOAN_ACCOUNT_INTEREST_RATE + '"\tDOUBLE,\n\t\t\t"' + Queries.COL_LOAN_ACCOUNT_TERM_DURATION + '"\tINTEGER,\n\t\t\tFOREIGN KEY("' + Queries.COL_LOAN_ACCOUNT_FIRM_ID + '") REFERENCES "' + Queries.DB_TABLE_FIRMS + '" ("' + Queries.COL_FIRM_ID + '")\n\t\t)';

		var loanTransactions = 'CREATE TABLE "' + Queries.DB_TABLE_LOAN_TRANSACTIONS + '" (\n\t\t\t"' + Queries.COL_LOAN_TXN_ID + '"\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n\t\t\t"' + Queries.COL_LOAN_ACCOUNT_ID + '"\tINTEGER NOT NULL,\n\t\t\t"' + Queries.COL_LOAN_TXN_TYPE + '"\tINTEGER NOT NULL,\n\t\t\t"' + Queries.COL_LOAN_TXN_PRINCIPAL + '"\tDOUBLE NOT NULL,\n\t\t\t"' + Queries.COL_LOAN_TXN_INTEREST + '"\tDOUBLE NOT NULL,\n\t\t\t"' + Queries.COL_LOAN_TXN_PAYMENT_ID + '"      INTEGER NOT NULL,\n\t\t\t"' + Queries.COL_LOAN_TXN_DATE + '"\tDATE NOT NULL,\n\t\t\t"' + Queries.COL_LOAN_TXN_CREATED_DATE + '"\tDATETIME NOT NULL,\n\t\t\t"' + Queries.COL_LOAN_TXN_MODIFIED_DATE + '"\tDATETIME NOT NULL,\n\t\t\t"' + Queries.COL_LOAN_TXN_DESC + '"\tTEXT,\n\t\t\t"' + Queries.COL_LOAN_TXN_IMAGE_ID + '"\tINTEGER DEFAULT NULL,\n\t\t\tFOREIGN KEY("' + Queries.COL_LOAN_ACCOUNT_ID + '") REFERENCES "' + Queries.DB_TABLE_LOAN_ACCOUNTS + '" ("' + Queries.COL_LOAN_ACCOUNT_ID + '"),\n\t\t\tFOREIGN KEY("' + Queries.COL_LOAN_TXN_PAYMENT_ID + '") REFERENCES "' + Queries.DB_TABLE_PAYMENT_TYPE + '" ("' + Queries.COL_PAYMENT_TYPE_ID + '"),\n\t\t\tFOREIGN KEY("' + Queries.COL_LOAN_TXN_IMAGE_ID + '") REFERENCES "' + Queries.DB_TABLE_TRANSACTION_IMAGE + '" ("' + Queries.COL_TRANSACTION_IMAGE_ID + '")\n\t\t)';
		executeGeneralQuery(loanAccounts);
		executeGeneralQuery(loanTransactions);

		// update NaN date values to null
		var NanValue = 'NaN%';
		var updateIstExpDate = 'update ' + Queries.DB_ITEM_STOCK_TRACKING_TABLE + ' set ' + Queries.COL_IST_EXPIRY_DATE + ' = null where ' + Queries.COL_IST_EXPIRY_DATE + ' like \'' + NanValue + '\'';
		var updateIstMfgDate = 'update ' + Queries.DB_ITEM_STOCK_TRACKING_TABLE + ' set ' + Queries.COL_IST_MANUFACTURING_DATE + ' = null where ' + Queries.COL_IST_MANUFACTURING_DATE + ' like \'' + NanValue + '\'';
		var updateLineitemExpDate = 'update ' + Queries.DB_TABLE_LINEITEMS + ' set ' + Queries.COL_LINEITEM_EXPIRY_DATE + ' = null where ' + Queries.COL_LINEITEM_EXPIRY_DATE + ' like \'' + NanValue + '\'';
		var updateLineitemMfgDate = 'update ' + Queries.DB_TABLE_LINEITEMS + ' set ' + Queries.COL_LINEITEM_MANUFACTURING_DATE + ' = null where ' + Queries.COL_LINEITEM_MANUFACTURING_DATE + ' like \'' + NanValue + '\'';
		executeGeneralQuery(updateIstExpDate);
		executeGeneralQuery(updateIstMfgDate);
		executeGeneralQuery(updateLineitemExpDate);
		executeGeneralQuery(updateLineitemMfgDate);

		var migrateExpiryDate = false;
		var migrateManufacturingDate = false;
		// check if date format is month year for expiry date and manufacturing date
		var selectExpiryDateQuery = 'select ' + Queries.COL_SETTING_KEY + ' from ' + Queries.DB_TABLE_SETTINGS + ' where ' + Queries.COL_SETTING_KEY + ' = \'' + Queries.SETTING_EXPIRY_DATE_TYPE + '\' and ' + Queries.COL_SETTING_VALUE + ' = \'' + StringConstants.monthYear + '\'';
		var selectManufacturingDateQuery = 'select ' + Queries.COL_SETTING_KEY + ' from ' + Queries.DB_TABLE_SETTINGS + ' where ' + Queries.COL_SETTING_KEY + ' = \'' + Queries.SETTING_MANUFACTURING_DATE_TYPE + '\' and ' + Queries.COL_SETTING_VALUE + ' = \'' + StringConstants.monthYear + '\'';

		mainobj = [];
		libsqlite3.sqlite3_exec(dbHandleForUpdate, selectExpiryDateQuery, callbackForFetchAllNameRecords, null, null);
		if (mainobj.length > 0) {
			migrateExpiryDate = true;
		}
		mainobj = [];
		libsqlite3.sqlite3_exec(dbHandleForUpdate, selectManufacturingDateQuery, callbackForFetchAllNameRecords, null, null);
		if (mainobj.length > 0) {
			migrateManufacturingDate = true;
		}

		// migrate expiry date
		if (migrateExpiryDate) {
			// replace date value with '01'
			var newExpiryDateIST = 'substr(' + Queries.COL_IST_EXPIRY_DATE + ', 1, 8) || \'01\'';
			var newExpiryDateLineItem = 'substr(' + Queries.COL_LINEITEM_EXPIRY_DATE + ', 1, 8) || \'01 00:00:00\'';

			var updateExpiryDateISTQuery = 'update ' + Queries.DB_ITEM_STOCK_TRACKING_TABLE + ' set ' + Queries.COL_IST_EXPIRY_DATE + ' = ' + newExpiryDateIST + ' where ' + Queries.COL_IST_EXPIRY_DATE + ' is not null and ' + Queries.COL_IST_EXPIRY_DATE + ' != \'\'';
			var updateExpiryDateLineItemQuery = 'update ' + Queries.DB_TABLE_LINEITEMS + ' set ' + Queries.COL_LINEITEM_EXPIRY_DATE + ' = ' + newExpiryDateLineItem + ' where ' + Queries.COL_LINEITEM_EXPIRY_DATE + ' is not null and ' + Queries.COL_LINEITEM_EXPIRY_DATE + ' != \'\'';
			executeGeneralQuery(updateExpiryDateISTQuery);
			executeGeneralQuery(updateExpiryDateLineItemQuery);
		}

		// migrate manufacturing date
		if (migrateManufacturingDate) {
			// replace date value with '01'
			var newManufacturingDateIST = 'substr(' + Queries.COL_IST_MANUFACTURING_DATE + ', 1, 8) || \'01\'';
			var newManufacturingDateLineItem = 'substr(' + Queries.COL_LINEITEM_MANUFACTURING_DATE + ', 1, 8) || \'01 00:00:00\'';

			var updateManufacturingDateISTQuery = 'update ' + Queries.DB_ITEM_STOCK_TRACKING_TABLE + ' set ' + Queries.COL_IST_MANUFACTURING_DATE + ' = ' + newManufacturingDateIST + ' where ' + Queries.COL_IST_MANUFACTURING_DATE + ' is not null and ' + Queries.COL_IST_MANUFACTURING_DATE + ' != \'\'';
			var updateManufacturingDateLineItemQuery = 'update ' + Queries.DB_TABLE_LINEITEMS + ' set ' + Queries.COL_LINEITEM_MANUFACTURING_DATE + ' = ' + newManufacturingDateLineItem + ' where ' + Queries.COL_LINEITEM_MANUFACTURING_DATE + ' is not null and ' + Queries.COL_LINEITEM_MANUFACTURING_DATE + ' != \'\'';
			executeGeneralQuery(updateManufacturingDateISTQuery);
			executeGeneralQuery(updateManufacturingDateLineItemQuery);
		}

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},
	DB_VERSION_53: function DB_VERSION_53() {
		CURRENT_DB_VERSION = 53;
		beginTransactionForDBUpgrade();

		var itemImagesTableCreateQuery = 'create table ' + Queries.DB_TABLE_ITEM_IMAGES + '( ' + Queries.COL_ITEM_IMAGE_ID + ' integer primary key autoincrement ,' + Queries.COL_ITEM_ID + ' integer references ' + Queries.DB_TABLE_ITEMS + '(' + Queries.COL_ITEM_ID + '), ' + Queries.COL_ITEM_IMAGE_BITMAP + ' BLOB)';

		executeGeneralQuery(itemImagesTableCreateQuery);

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},
	DB_VERSION_52: function DB_VERSION_52() {
		CURRENT_DB_VERSION = 52;
		beginTransactionForDBUpgrade();

		var query = 'ALTER TABLE ' + Queries.DB_TABLE_TRANSACTIONS + ' ADD COLUMN ' + Queries.COL_TXN_BILLING_ADDRESS + ' TEXT DEFAULT \'\';';
		executeGeneralQuery(query);
		query = 'ALTER TABLE ' + Queries.DB_TABLE_TRANSACTIONS + ' ADD COLUMN ' + Queries.COL_TXN_SHIPPING_ADDRESS + ' TEXT DEFAULT \'\';';
		executeGeneralQuery(query);

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},
	DB_VERSION_51: function DB_VERSION_51() {
		CURRENT_DB_VERSION = 51;
		beginTransactionForDBUpgrade();

		var ItemType = require('./../Constants/ItemType.js');

		var alterLineItemquery = 'alter table ' + Queries.DB_TABLE_LINEITEMS + ' add column ' + Queries.COL_LINEITEM_DISCOUNT_PERCENT + ' double default 0';

		var alterTransactionTable = 'alter table ' + Queries.DB_TABLE_TRANSACTIONS + ' add column ' + Queries.COL_TXN_TAX_TYPE + ' integer default ' + ItemType.ITEM_TXN_TAX_EXCLUSIVE;

		var alterItemTable = 'alter table ' + Queries.DB_TABLE_ITEMS + ' add column ' + Queries.COL_ITEM_TAX_TYPE_PURCHASE + ' integer default ' + ItemType.ITEM_TXN_TAX_EXCLUSIVE;

		var updatetLineItemQuery = 'update ' + Queries.DB_TABLE_LINEITEMS + ' set ' + Queries.COL_LINEITEM_DISCOUNT_PERCENT + ' = (' + Queries.COL_LINEITEM_DISCOUNT_AMOUNT + ' * 100)/(' + Queries.COL_LINEITEM_UNITPRICE + ' * ' + Queries.COL_LINEITEM_QUANTITY + ')\n          \twhere ' + Queries.COL_LINEITEM_QUANTITY + ' > 0 and ' + Queries.COL_LINEITEM_UNITPRICE + ' > 0 and ' + Queries.COL_LINEITEM_DISCOUNT_AMOUNT + ' > 0';

		executeGeneralQuery(alterTransactionTable);
		executeGeneralQuery(alterItemTable);
		executeGeneralQuery(alterLineItemquery);
		executeGeneralQuery(updatetLineItemQuery);

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	DB_VERSION_50: function DB_VERSION_50() {
		CURRENT_DB_VERSION = 50;
		beginTransactionForDBUpgrade();

		var ALTER_NAME_TABLE = 'ALTER TABLE ' + Queries.DB_TABLE_NAMES + ' ADD COLUMN ' + Queries.COL_NAME_LAST_TXN_DATE + ' DATETIME DEFAULT NULL';
		var MIGRATE_DATA_NAME_TABLE = 'UPDATE ' + Queries.DB_TABLE_NAMES + ' SET ' + Queries.COL_NAME_LAST_TXN_DATE + ' = (SELECT MAX(' + Queries.COL_TXN_DATE + ') from \n\t\t' + Queries.DB_TABLE_TRANSACTIONS + ' where ' + Queries.COL_TXN_NAME_ID + ' = ' + Queries.DB_TABLE_NAMES + '.' + Queries.COL_NAME_ID + ')';

		executeGeneralQuery(ALTER_NAME_TABLE);
		executeGeneralQuery(MIGRATE_DATA_NAME_TABLE);

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	DB_VERSION_49: function DB_VERSION_49() {
		CURRENT_DB_VERSION = 49;
		beginTransactionForDBUpgrade();

		var ALTER_FIRM_TABLE_UPI_ACCOUNT_NUMBER = 'alter table ' + Queries.DB_TABLE_FIRMS + ' add column ' + Queries.COL_FIRM_UPI_BANK_ACCOUNT_NUMBER + ' varchar(18) default \'\'';
		var ALTER_FIRM_TABLE_UPI_IFSC_CODE = 'alter table ' + Queries.DB_TABLE_FIRMS + ' add column ' + Queries.COL_FIRM_UPI_BANK_IFSC + ' varchar(11) default \'\'';
		executeGeneralQuery(ALTER_FIRM_TABLE_UPI_ACCOUNT_NUMBER);
		executeGeneralQuery(ALTER_FIRM_TABLE_UPI_IFSC_CODE);

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	DB_VERSION_48: function DB_VERSION_48() {
		CURRENT_DB_VERSION = 48;
		beginTransactionForDBUpgrade();

		var CREATE_CLOSED_LINKED_TXN_DETAILS_IF_NOT_EXIST = 'CREATE TABLE IF NOT EXISTS ' + Queries.DB_TABLE_CLOSED_LINK_TXN_TABLE + '( ' + Queries.COL_CLOSED_LINK_TXN_ID + ' integer primary key autoincrement, ' + Queries.COL_CLOSED_LINK_TXN_AMOUNT + ' double default 0, ' + Queries.COL_CLOSED_LINK_TXN_TYPE + ' integer, ' + Queries.COL_CLOSED_LINK_TXN_DATE + ' date, ' + Queries.COL_CLOSED_LINK_TXN_REF_NUMBER + " varchar(50) default '', " + Queries.COL_CLOSED_LINK_TXN_NAME_ID + ' integer default null, ' + 'foreign key(' + Queries.COL_CLOSED_LINK_TXN_NAME_ID + ') references ' + Queries.DB_TABLE_NAMES + '(' + Queries.COL_NAME_ID + '))';

		executeGeneralQuery(CREATE_CLOSED_LINKED_TXN_DETAILS_IF_NOT_EXIST);

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	DB_VERSION_47: function DB_VERSION_47() {
		CURRENT_DB_VERSION = 47;
		beginTransactionForDBUpgrade();
		var IstType = require('./../Constants/IstType.js');
		var TxnPaymentStatusConstants = require('./../Constants/TxnPaymentStatusConstants.js');

		// name id migration closed link txn table
		var updateNameIdInClosedTxntable = 'update ' + Queries.DB_TABLE_CLOSED_LINK_TXN_TABLE + ' set ' + Queries.COL_CLOSED_LINK_TXN_NAME_ID + '= ( select' + ' ' + Queries.COL_TXN_NAME_ID + ' from ' + Queries.DB_TABLE_TRANSACTIONS + ' inner join (select ' + Queries.COL_TXN_LINKS_TXN_1_ID + ' as link_txn_id from ' + Queries.DB_TABLE_TXN_LINKS + ' where ' + Queries.COL_TXN_LINKS_CLOSED_TXN_REF_ID + '=' + Queries.COL_CLOSED_LINK_TXN_ID + ' union select ' + Queries.COL_TXN_LINKS_TXN_2_ID + ' as link_txn_id from ' + Queries.DB_TABLE_TXN_LINKS + ' where ' + Queries.COL_TXN_LINKS_CLOSED_TXN_REF_ID + '=' + Queries.COL_CLOSED_LINK_TXN_ID + ')' + ' on link_txn_id=' + Queries.COL_TXN_ID + ' group by link_txn_id) where ' + Queries.COL_CLOSED_LINK_TXN_NAME_ID + ' is null';
		executeGeneralQuery(updateNameIdInClosedTxntable);

		// update ist
		var UPDATE_IST_TABLE = 'update ' + Queries.DB_ITEM_STOCK_TRACKING_TABLE + ' set ' + Queries.COL_IST_TYPE + ' = ' + IstType.NORMAL + ' where ' + Queries.COL_IST_TYPE + ' = ' + IstType.CLOSED;
		executeGeneralQuery(UPDATE_IST_TABLE);

		// migration for undefined payment status in transaction table
		var updateTransactionTablePaymentStatus = 'update ' + Queries.DB_TABLE_TRANSACTIONS + ' set ' + Queries.COL_TXN_PAYMENT_STATUS + ' = ' + TxnPaymentStatusConstants.UNPAID + ' where ' + Queries.COL_TXN_TYPE + ' in (' + TxnTypeConstant.TXN_TYPE_ROPENBALANCE + ', ' + TxnTypeConstant.TXN_TYPE_POPENBALANCE + ') and ' + Queries.COL_TXN_PAYMENT_STATUS + ' = \'undefined\'';
		executeGeneralQuery(updateTransactionTablePaymentStatus);

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	DB_VERSION_46: function DB_VERSION_46() {
		CURRENT_DB_VERSION = 46;
		beginTransactionForDBUpgrade();
		var IstType = require('./../Constants/IstType.js');

		var ALTER_CHEQUE_TABLE = 'alter table ' + Queries.DB_TABLE_CHEQUE_STATUS + ' add column ' + Queries.COL_CHEQUE_CLOSED_LINK_TXN_REF_ID + ' integer default null references ' + Queries.DB_TABLE_CLOSED_LINK_TXN_TABLE + ' (' + Queries.COL_CLOSED_LINK_TXN_ID + ')';
		executeGeneralQuery(ALTER_CHEQUE_TABLE);

		var ALTER_CLOSED_LINK_TXN_TABLE = 'alter table ' + Queries.DB_TABLE_CLOSED_LINK_TXN_TABLE + ' add column ' + Queries.COL_CLOSED_LINK_TXN_NAME_ID + ' integer default null';
		executeGeneralQuery(ALTER_CLOSED_LINK_TXN_TABLE);

		// name id migration closed link txn table

		var updateNameIdInClosedTxntable = 'update ' + Queries.DB_TABLE_CLOSED_LINK_TXN_TABLE + ' set ' + Queries.COL_CLOSED_LINK_TXN_NAME_ID + '= ( select' + ' ' + Queries.COL_TXN_NAME_ID + ' from ' + Queries.DB_TABLE_TRANSACTIONS + ' inner join (select ' + Queries.COL_TXN_LINKS_TXN_1_ID + ' as link_txn_id from ' + Queries.DB_TABLE_TXN_LINKS + ' where ' + Queries.COL_TXN_LINKS_CLOSED_TXN_REF_ID + '=' + Queries.COL_CLOSED_LINK_TXN_ID + ' union select ' + Queries.COL_TXN_LINKS_TXN_2_ID + ' as link_txn_id from ' + Queries.DB_TABLE_TXN_LINKS + ' where ' + Queries.COL_TXN_LINKS_CLOSED_TXN_REF_ID + '=' + Queries.COL_CLOSED_LINK_TXN_ID + ')' + ' on link_txn_id=' + Queries.COL_TXN_ID + ' group by link_txn_id)';
		executeGeneralQuery(updateNameIdInClosedTxntable);

		// update ist
		var UPDATE_IST_TABLE = 'update ' + Queries.DB_ITEM_STOCK_TRACKING_TABLE + ' set ' + Queries.COL_IST_TYPE + ' = ' + IstType.NORMAL + ' where ' + Queries.COL_IST_TYPE + ' = ' + IstType.CLOSED;
		executeGeneralQuery(UPDATE_IST_TABLE);

		// create prefix table
		var createPrefixTableQuery = 'create table ' + Queries.DB_TABLE_PREFIX + ' (\n      ' + Queries.COL_PREFIX_ID + ' integer primary key autoincrement,\n      ' + Queries.COL_PREFIX_FIRM_ID + ' integer references ' + Queries.DB_TABLE_FIRMS + ' (' + Queries.COL_FIRM_ID + '),\n      ' + Queries.COL_PREFIX_TXN_TYPE + ' integer default ' + TxnTypeConstant.TXN_TYPE_SALE + ',\n      ' + Queries.COL_PREFIX_VALUE + ' varchar(50) default \'\',\n      ' + Queries.COL_PREFIX_IS_DEFAULT + ' integer default 0,\n    \tunique(' + Queries.COL_PREFIX_FIRM_ID + ', ' + Queries.COL_PREFIX_TXN_TYPE + ', ' + Queries.COL_PREFIX_VALUE + '))';
		executeGeneralQuery(createPrefixTableQuery);

		// insert into prefix table
		var selectPrefixQuery = 'select ' + Queries.COL_TXN_FIRM_ID + ', ' + Queries.COL_TXN_TYPE + ', ' + Queries.COL_TXN_INVOICE_PREFIX + ' from ' + Queries.DB_TABLE_TRANSACTIONS + ' where ' + Queries.COL_TXN_TYPE + ' in (' + TxnTypeConstant.TXN_TYPE_SALE + ', ' + TxnTypeConstant.TXN_TYPE_CASHIN + ', ' + TxnTypeConstant.TXN_TYPE_ESTIMATE + ', ' + TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN + ') and ' + Queries.COL_TXN_INVOICE_PREFIX + ' is not null and ' + Queries.COL_TXN_INVOICE_PREFIX + ' != \'\' group by ' + Queries.COL_TXN_FIRM_ID + ', ' + Queries.COL_TXN_TYPE + ', ' + Queries.COL_TXN_INVOICE_PREFIX;
		mainobj = [];
		libsqlite3.sqlite3_exec(dbHandleForUpdate, selectPrefixQuery, callbackForFetchAllNameRecords, null, null);
		if (mainobj.length > 0) {
			for (var i = 0; i < mainobj.length; i++) {
				var firm_id = mainobj[i][Queries.COL_TXN_FIRM_ID];
				var txn_type = mainobj[i][Queries.COL_TXN_TYPE];
				var txn_prefix = mainobj[i][Queries.COL_TXN_INVOICE_PREFIX];

				var insertInvoicePrefixQuery = 'insert into ' + Queries.DB_TABLE_PREFIX + ' (' + Queries.COL_PREFIX_FIRM_ID + ', ' + Queries.COL_PREFIX_TXN_TYPE + ', ' + Queries.COL_PREFIX_VALUE + ') values (' + firm_id + ', ' + txn_type + ', \'' + txn_prefix + '\')';
				insert(Queries.DB_TABLE_PREFIX, insertInvoicePrefixQuery, Queries.COL_PREFIX_ID);
			}
		}

		// add prefix id column in transaction table
		var alterTransactionTableQuery = 'alter table ' + Queries.DB_TABLE_TRANSACTIONS + ' add column ' + Queries.COL_TXN_PREFIX_ID + ' integer default null references ' + Queries.DB_TABLE_PREFIX + ' (' + Queries.COL_PREFIX_ID + ')';
		executeGeneralQuery(alterTransactionTableQuery);

		// migrate prefix in transaction table
		var updatePrefixIdInTransactionTableQuery = 'update ' + Queries.DB_TABLE_TRANSACTIONS + ' set ' + Queries.COL_TXN_PREFIX_ID + ' = (select prfx.' + Queries.COL_PREFIX_ID + ' from ' + Queries.DB_TABLE_PREFIX + ' prfx where ' + Queries.DB_TABLE_TRANSACTIONS + '.' + Queries.COL_TXN_INVOICE_PREFIX + ' = prfx.' + Queries.COL_PREFIX_VALUE + ' and ' + Queries.DB_TABLE_TRANSACTIONS + '.' + Queries.COL_TXN_FIRM_ID + ' = prfx.' + Queries.COL_PREFIX_FIRM_ID + ' and ' + Queries.DB_TABLE_TRANSACTIONS + '.' + Queries.COL_TXN_TYPE + ' = prfx.' + Queries.COL_PREFIX_TXN_TYPE + ') where ' + Queries.DB_TABLE_TRANSACTIONS + '.' + Queries.COL_TXN_INVOICE_PREFIX + ' is not null and ' + Queries.DB_TABLE_TRANSACTIONS + '.' + Queries.COL_TXN_INVOICE_PREFIX + ' != \'\'';
		executeGeneralQuery(updatePrefixIdInTransactionTableQuery);

		// set defaults in prefix table
		var setDefaultInPrefixTable = 'select ' + Queries.COL_FIRM_ID + ', ' + Queries.COL_FIRM_INVOICE_PREFIX + ', ' + Queries.COL_FIRM_ESTIMATE_PREFIX + ', ' + Queries.COL_FIRM_CASH_IN_PREFIX + ', ' + Queries.COL_FIRM_DELIVERY_CHALLAN_PREFIX + ' from ' + Queries.DB_TABLE_FIRMS;
		mainobj = [];
		libsqlite3.sqlite3_exec(dbHandleForUpdate, setDefaultInPrefixTable, callbackForFetchAllNameRecords, null, null);
		if (mainobj.length > 0) {
			for (var i = 0; i < mainobj.length; i++) {
				var _firm_id = mainobj[i][Queries.COL_FIRM_ID];
				var invoice_prefix = mainobj[i][Queries.COL_FIRM_INVOICE_PREFIX];
				var estimate_prefix = mainobj[i][Queries.COL_FIRM_ESTIMATE_PREFIX];
				var cash_in_prefix = mainobj[i][Queries.COL_FIRM_CASH_IN_PREFIX];
				var delivery_challan_prefix = mainobj[i][Queries.COL_FIRM_DELIVERY_CHALLAN_PREFIX];

				if (invoice_prefix) {
					var updatePrefixTable = 'update ' + Queries.DB_TABLE_PREFIX + ' set ' + Queries.COL_PREFIX_IS_DEFAULT + ' = 1 where ' + Queries.COL_PREFIX_FIRM_ID + ' = ' + _firm_id + ' and ' + Queries.COL_PREFIX_TXN_TYPE + ' = ' + TxnTypeConstant.TXN_TYPE_SALE + ' and ' + Queries.COL_PREFIX_VALUE + ' = \'' + invoice_prefix + '\'';
					executeGeneralQuery(updatePrefixTable);
					if (!calculateLastSQLiteQueryAffectedRowCountForDbUpgrade()) {
						var _insertInvoicePrefixQuery = 'insert into ' + Queries.DB_TABLE_PREFIX + ' (' + Queries.COL_PREFIX_FIRM_ID + ', ' + Queries.COL_PREFIX_TXN_TYPE + ', ' + Queries.COL_PREFIX_VALUE + ', ' + Queries.COL_PREFIX_IS_DEFAULT + ') values (' + _firm_id + ', ' + TxnTypeConstant.TXN_TYPE_SALE + ', \'' + invoice_prefix + '\', 1)';
						insert(Queries.DB_TABLE_PREFIX, _insertInvoicePrefixQuery, Queries.COL_PREFIX_ID);
					}
				}
				if (estimate_prefix) {
					var _updatePrefixTable = 'update ' + Queries.DB_TABLE_PREFIX + ' set ' + Queries.COL_PREFIX_IS_DEFAULT + ' = 1 where ' + Queries.COL_PREFIX_FIRM_ID + ' = ' + _firm_id + ' and ' + Queries.COL_PREFIX_TXN_TYPE + ' = ' + TxnTypeConstant.TXN_TYPE_ESTIMATE + ' and ' + Queries.COL_PREFIX_VALUE + ' = \'' + estimate_prefix + '\'';
					executeGeneralQuery(_updatePrefixTable);
					if (!calculateLastSQLiteQueryAffectedRowCountForDbUpgrade()) {
						var _insertInvoicePrefixQuery2 = 'insert into ' + Queries.DB_TABLE_PREFIX + ' (' + Queries.COL_PREFIX_FIRM_ID + ', ' + Queries.COL_PREFIX_TXN_TYPE + ', ' + Queries.COL_PREFIX_VALUE + ', ' + Queries.COL_PREFIX_IS_DEFAULT + ') values (' + _firm_id + ', ' + TxnTypeConstant.TXN_TYPE_ESTIMATE + ', \'' + estimate_prefix + '\', 1)';
						insert(Queries.DB_TABLE_PREFIX, _insertInvoicePrefixQuery2, Queries.COL_PREFIX_ID);
					}
				}
				if (cash_in_prefix) {
					var _updatePrefixTable2 = 'update ' + Queries.DB_TABLE_PREFIX + ' set ' + Queries.COL_PREFIX_IS_DEFAULT + ' = 1 where ' + Queries.COL_PREFIX_FIRM_ID + ' = ' + _firm_id + ' and ' + Queries.COL_PREFIX_TXN_TYPE + ' = ' + TxnTypeConstant.TXN_TYPE_CASHIN + ' and ' + Queries.COL_PREFIX_VALUE + ' = \'' + cash_in_prefix + '\'';
					executeGeneralQuery(_updatePrefixTable2);
					if (!calculateLastSQLiteQueryAffectedRowCountForDbUpgrade()) {
						var _insertInvoicePrefixQuery3 = 'insert into ' + Queries.DB_TABLE_PREFIX + ' (' + Queries.COL_PREFIX_FIRM_ID + ', ' + Queries.COL_PREFIX_TXN_TYPE + ', ' + Queries.COL_PREFIX_VALUE + ', ' + Queries.COL_PREFIX_IS_DEFAULT + ') values (' + _firm_id + ', ' + TxnTypeConstant.TXN_TYPE_CASHIN + ', \'' + cash_in_prefix + '\', 1)';
						insert(Queries.DB_TABLE_PREFIX, _insertInvoicePrefixQuery3, Queries.COL_PREFIX_ID);
					}
				}
				if (delivery_challan_prefix) {
					var _updatePrefixTable3 = 'update ' + Queries.DB_TABLE_PREFIX + ' set ' + Queries.COL_PREFIX_IS_DEFAULT + ' = 1 where ' + Queries.COL_PREFIX_FIRM_ID + ' = ' + _firm_id + ' and ' + Queries.COL_PREFIX_TXN_TYPE + ' = ' + TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN + ' and ' + Queries.COL_PREFIX_VALUE + ' = \'' + delivery_challan_prefix + '\'';
					executeGeneralQuery(_updatePrefixTable3);
					if (!calculateLastSQLiteQueryAffectedRowCountForDbUpgrade()) {
						var _insertInvoicePrefixQuery4 = 'insert into ' + Queries.DB_TABLE_PREFIX + ' (' + Queries.COL_PREFIX_FIRM_ID + ', ' + Queries.COL_PREFIX_TXN_TYPE + ', ' + Queries.COL_PREFIX_VALUE + ', ' + Queries.COL_PREFIX_IS_DEFAULT + ') values (' + _firm_id + ', ' + TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN + ', \'' + delivery_challan_prefix + '\', 1)';
						insert(Queries.DB_TABLE_PREFIX, _insertInvoicePrefixQuery4, Queries.COL_PREFIX_ID);
					}
				}
			}
		}

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	DB_VERSION_45: function DB_VERSION_45() {
		CURRENT_DB_VERSION = 45;
		beginTransactionForDBUpgrade();
		var Country = require('./../Constants/Country.js');
		var StringConstants = require('./../Constants/StringConstants.js');

		var selectQuery = 'select ' + Queries.COL_SETTING_VALUE + ' from ' + Queries.DB_TABLE_SETTINGS + ' where ' + Queries.COL_SETTING_KEY + ' = \'' + Queries.SETTING_USER_COUNTRY + '\' and ' + Queries.COL_SETTING_VALUE + ' = \'' + Country.Countries.NEPAL.countryCode + '\'';

		mainobj = [];
		libsqlite3.sqlite3_exec(dbHandleForUpdate, selectQuery, callbackForFetchAllNameRecords, null, null);
		if (mainobj.length > 0) {
			var updateQuery1 = 'insert or replace into ' + Queries.DB_TABLE_SETTINGS + ' (' + Queries.COL_SETTING_KEY + ', ' + Queries.COL_SETTING_VALUE + ') values(\'' + Queries.SETTING_EXPIRY_DATE_TYPE + '\', ' + StringConstants.dateMonthYear + ')';
			var updateQuery2 = 'insert or replace into ' + Queries.DB_TABLE_SETTINGS + ' (' + Queries.COL_SETTING_KEY + ', ' + Queries.COL_SETTING_VALUE + ') values(\'' + Queries.SETTING_MANUFACTURING_DATE_TYPE + '\', ' + StringConstants.dateMonthYear + ')';
			var updateQuery3 = 'update ' + Queries.DB_TABLE_UDF_FIELDS + ' set ' + Queries.COL_UDF_FIELD_DATA_FORMAT + ' = ' + StringConstants.dateMonthYear + ' where ' + Queries.COL_UDF_FIELD_DATA_FORMAT + ' = ' + StringConstants.monthYear;
			executeGeneralQuery(updateQuery1);
			executeGeneralQuery(updateQuery2);
			executeGeneralQuery(updateQuery3);
		} else {
			executeGeneralQuery(DUMMY_QUERY_FOR_SYNC);
		}

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	DB_VERSION_44: function DB_VERSION_44() {
		CURRENT_DB_VERSION = 44;
		beginTransactionForDBUpgrade();

		var query = 'alter table ' + Queries.DB_TABLE_ITEMS + ' add ' + Queries.COL_ITEM_IS_ACTIVE + ' integer default 1';

		var conn = executeGeneralQuery(query);

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	DB_VERSION_43: function DB_VERSION_43() {
		CURRENT_DB_VERSION = 43;
		beginTransactionForDBUpgrade();

		// create table payment_term
		var queryToCreatePaymentTermTable = 'create table ' + Queries.DB_TABLE_PAYMENT_TERMS + ' (\n\t\t ' + Queries.COL_PAYMENT_TERM_ID + ' integer primary key autoincrement,\n\t\t ' + Queries.COL_PAYMENT_TERM_NAME + ' varchar(50) unique,\n\t\t ' + Queries.COL_PAYMENT_TERM_DAYS + ' integer unique,\n\t\t ' + Queries.COL_PAYMENT_TERM_IS_DEFAULT + ' integer default 0)';

		executeGeneralQuery(queryToCreatePaymentTermTable);

		// insert default values in payment term table
		var defaultFieldsList = [{ term_id: 1, term_name: 'Due on Receipt', term_days: 0, is_default: 1 }, { term_id: 2, term_name: 'Net 15', term_days: 15, is_default: 0 }, { term_id: 3, term_name: 'Net 30', term_days: 30, is_default: 0 }, { term_id: 4, term_name: 'Net 45', term_days: 45, is_default: 0 }, { term_id: 5, term_name: 'Net 60', term_days: 60, is_default: 0 }];
		for (var i = 0; i < defaultFieldsList.length; i++) {
			var obj = defaultFieldsList[i];
			var query = 'insert into ' + Queries.DB_TABLE_PAYMENT_TERMS + '( ' + Queries.COL_PAYMENT_TERM_ID + ', ' + Queries.COL_PAYMENT_TERM_NAME + ', ' + Queries.COL_PAYMENT_TERM_DAYS + ', ' + Queries.COL_PAYMENT_TERM_IS_DEFAULT + ') VALUES (' + obj['term_id'] + ', \'' + obj['term_name'] + '\', ' + obj['term_days'] + ', ' + obj['is_default'] + ')';
			insert(Queries.DB_TABLE_PAYMENT_TERMS, query, Queries.COL_PAYMENT_TERM_ID);
		}

		// add payment term id (column) in kb_transactions
		var alterTransactionTable = 'alter table ' + Queries.DB_TABLE_TRANSACTIONS + ' add column ' + Queries.COL_TXN_PAYMENT_TERM_ID + ' integer default null references ' + Queries.DB_TABLE_PAYMENT_TERMS + ' ( ' + Queries.COL_PAYMENT_TERM_ID + ')';
		executeGeneralQuery(alterTransactionTable);

		// due date migration
		var dueDateMigrationQuery = 'update ' + Queries.DB_TABLE_TRANSACTIONS + ' set ' + Queries.COL_TXN_DUE_DATE + ' = ' + Queries.COL_TXN_DATE + ', ' + Queries.COL_TXN_PAYMENT_TERM_ID + ' = 1 where ' + Queries.COL_TXN_TYPE + ' not in (' + TxnTypeConstant.TXN_TYPE_SALE_ORDER + ', ' + TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER + ', ' + TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN + ')';
		executeGeneralQuery(dueDateMigrationQuery);

		DB_UPDATES.setDefaultSettings();

		// payment term setting migration
		var settingQuery = 'insert or replace into ' + Queries.DB_TABLE_SETTINGS + ' (' + Queries.COL_SETTING_KEY + ', ' + Queries.COL_SETTING_VALUE + ') values (\'' + Queries.SETTING_PAYMENT_TERM_ENABLED + '\', exists( select ' + Queries.COL_SETTING_VALUE + ' from ' + Queries.DB_TABLE_SETTINGS + ' where ' + Queries.COL_SETTING_KEY + ' = \'' + Queries.SETTING_BILL_TO_BILL_ENABLED + '\' and  ' + Queries.COL_SETTING_VALUE + ' = 1))';
		executeGeneralQuery(settingQuery);

		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},
	DB_VERSION_42: function DB_VERSION_42() {
		CURRENT_DB_VERSION = 42;
		beginTransactionForDBUpgrade();

		var CREATE_UDF_FIELDS_TABLE = 'create table ' + Queries.DB_TABLE_UDF_FIELDS + ' (\n\t\t\t' + Queries.COL_UDF_FIELD_ID + ' integer primary key autoincrement,\n\t\t\t' + Queries.COL_UDF_FIELD_NAME + ' varchar(150) default \'\',\n\t\t\t' + Queries.COL_UDF_FIELD_TYPE + ' integer default 0,\n\t\t\t' + Queries.COL_UDF_FIELD_DATA_TYPE + ' integer default 0,\n\t\t\t' + Queries.COL_UDF_FIELD_DATA_FORMAT + ' integer default 0,\n\t\t\t' + Queries.COL_PRINT_ON_INVOICE + ' integer default 0,\n\t\t\t' + Queries.COL_UDF_TXN_TYPE + '\tinteger default 0,\n\t\t\t' + Queries.COL_FIELD_NUMBER + ' integer default 0,\n\t\t\t' + Queries.COL_UDF_FIELD_STATUS + ' integer default 0,\n\t\t\t' + Queries.COL_UDF_FIRM_ID + ' integer default null,\n\t\t\tunique(' + Queries.COL_UDF_FIRM_ID + ', ' + Queries.COL_UDF_FIELD_TYPE + ', ' + Queries.COL_UDF_TXN_TYPE + ', ' + Queries.COL_FIELD_NUMBER + '))';

		var CREATE_UDF_VALUES_TABLE = 'create table ' + Queries.DB_TABLE_UDF_VALUES + ' (\n\t\t\t' + Queries.COL_UDF_VALUE_ID + ' integer primary key autoincrement,\n            ' + Queries.COL_UDF_VALUE_FIELD_ID + ' integer  references ' + Queries.DB_TABLE_UDF_FIELDS + ' (' + Queries.COL_UDF_FIELD_ID + '),\n\t\t\t' + Queries.COL_UDF_REF_ID + ' integer default 0,\n\t\t\t' + Queries.COL_UDF_VALUE + ' varchar(150) default \'\',\n\t\t\t' + Queries.COL_UDF_VALUE_FIELD_TYPE + ' integer default 0,\n\t\t\tunique( ' + Queries.COL_UDF_VALUE_FIELD_ID + ', ' + Queries.COL_UDF_REF_ID + '))';

		var conn1 = executeGeneralQuery(CREATE_UDF_FIELDS_TABLE);

		var conn2 = executeGeneralQuery(CREATE_UDF_VALUES_TABLE);

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},
	DB_VERSION_41: function DB_VERSION_41() {
		CURRENT_DB_VERSION = 41;
		beginTransactionForDBUpgrade();

		var query = 'alter table ' + Queries.DB_TABLE_FIRMS + ' add column ' + Queries.COL_FIRM_DELIVERY_CHALLAN_PREFIX + ' varchar(32) default ""';
		executeGeneralQuery(query);
		query = 'INSERT OR REPLACE INTO ' + Queries.DB_TABLE_TXN_MESSAGE_CONFIG + '\n        (' + Queries.COL_TXN_MESSAGE_FIELD_VALUE + ', ' + Queries.COL_TXN_MESSAGE_FIELD_NAME + ', ' + Queries.COL_TXN_MESSAGE_FIELD_ID + ', ' + Queries.COL_TXN_MESSAGE_TXN_TYPE + ')\n        VALUES\n            (0,\'Date\',1,30),\n            (0,\'Challan No.\',2,30),\n            (0,\'Description\',3,30),\n            (1,\'Amount\',4,30),\n            (0,\'Item Details\',8,30),\n            (\'Delivery has been initiated!!\nDelivery Challan Details:\',\'Header\',11,30),\n            (\'\',\'Footer\',12,30)\n        ';
		insert(Queries.DB_TABLE_TXN_MESSAGE_CONFIG, query, Queries.COL_TXN_MESSAGE_TXN_TYPE);

		query = 'create table ' + Queries.DB_TABLE_LINKED_TRANSACTION + ' (\n        ' + Queries.COL_LINKED_ID + ' integer primary key autoincrement,\n        ' + Queries.COL_TRANSACTION_SOURCE_ID + ' integer NOT NULL,\n        ' + Queries.COL_TRANSACTION_DESTINATION_ID + ' integer NOT NULL,\n        ' + Queries.COL_LINKED_DATE + ' DATE NOT NULL DEFAULT (datetime(\'now\')),\n        FOREIGN KEY(' + Queries.COL_TRANSACTION_SOURCE_ID + ') REFERENCES ' + Queries.DB_TABLE_TRANSACTIONS + '(' + Queries.COL_TXN_ID + '),\n        FOREIGN KEY(' + Queries.COL_TRANSACTION_DESTINATION_ID + ') REFERENCES ' + Queries.DB_TABLE_TRANSACTIONS + '(' + Queries.COL_TXN_ID + ')\n         )';
		executeGeneralQuery(query);

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},
	DB_VERSION_40: function DB_VERSION_40() {
		CURRENT_DB_VERSION = 40;
		beginTransactionForDBUpgrade();

		var query = 'INSERT INTO ' + Queries.DB_TABLE_TXN_MESSAGE_CONFIG + '\n        (' + Queries.COL_TXN_MESSAGE_FIELD_VALUE + ', ' + Queries.COL_TXN_MESSAGE_FIELD_NAME + ', ' + Queries.COL_TXN_MESSAGE_FIELD_ID + ', ' + Queries.COL_TXN_MESSAGE_TXN_TYPE + ')\n        VALUES\n\n            (0,\'Date\',1,27),\n            (0,\'Estimate No.\',2,27),\n            (0,\'Description\',3,27),\n            (1,\'Amount\',4,27),\n            (0,\'Item Details\',8,27),\n            (\'Thanks for your query!!\nEstimate Details:\',\'Header\',11,27),\n            (\'\',\'Footer\',12,27),\n\n            (0,\'Date\',1,28),\n            (0,\'Order No.\',2,28),\n            (0,\'Description\',3,28),\n            (1,\'Amount\',4,28),\n            (1,\'Paid\',5,28),\n            (1,\'Balance\',6,28),\n            (1,\'Total Balance\',7,28),\n            (0,\'Item Details\',8,28),\n            (0,\'Payment mode\',9,28),\n            (0,\'Due Date\',13,28),\n            (\'We have placed an order with you.\nOrder Details:\',\'Header\',11,28),\n            (\'\',\'Footer\',12,28),\n\n            (0,\'Transportation Detail\',14,1),\n            (0,\'Transportation Detail\',14,2),\n            (0,\'Transportation Detail\',14,21),\n            (0,\'Transportation Detail\',14,23),\n            (0,\'Transportation Detail\',14,24),\n            (0,\'Transportation Detail\',14,28)';

		console.log(query);

		insert(Queries.DB_TABLE_TXN_MESSAGE_CONFIG, query, Queries.COL_TXN_MESSAGE_TXN_TYPE);

		query = 'UPDATE ' + Queries.DB_TABLE_TXN_MESSAGE_CONFIG + '\n    SET\n        txn_field_value=\'Thanks for your purchase with us!!\nPurchase Details:\'\n    WHERE ' + Queries.COL_TXN_MESSAGE_TXN_TYPE + '=\'1\'\n        and ' + Queries.COL_TXN_MESSAGE_FIELD_ID + ' = \'11\'\n        and ' + Queries.COL_TXN_MESSAGE_FIELD_VALUE + ' = \'Thanks for your purchase with us!!Purchase Details:\';';

		query += 'UPDATE ' + Queries.DB_TABLE_TXN_MESSAGE_CONFIG + '\n    SET\n        ' + Queries.COL_TXN_MESSAGE_FIELD_VALUE + '=\'We have made a purchase with you.\nPurchase Details:\'\n    WHERE ' + Queries.COL_TXN_MESSAGE_TXN_TYPE + '=\'2\'\n        and ' + Queries.COL_TXN_MESSAGE_FIELD_ID + ' = \'11\'\n        and ' + Queries.COL_TXN_MESSAGE_FIELD_VALUE + ' = \'We have made a purchase with you.Purchase Details:\';';

		query += 'UPDATE ' + Queries.DB_TABLE_TXN_MESSAGE_CONFIG + '\n    SET\n        ' + Queries.COL_TXN_MESSAGE_FIELD_VALUE + '=\'Thanks for making a payment to us!!\nPayment Details:\'\n    WHERE ' + Queries.COL_TXN_MESSAGE_TXN_TYPE + '=\'3\'\n        and ' + Queries.COL_TXN_MESSAGE_FIELD_ID + ' = \'11\'\n        and ' + Queries.COL_TXN_MESSAGE_FIELD_VALUE + ' = \'Thanks for making a payment to us!!Payment Details:\';';

		query += 'UPDATE ' + Queries.DB_TABLE_TXN_MESSAGE_CONFIG + '\n    SET\n        ' + Queries.COL_TXN_MESSAGE_FIELD_VALUE + '=\'We have made a payment to you.\nPayment Details:\'\n    WHERE ' + Queries.COL_TXN_MESSAGE_TXN_TYPE + '=\'4\'\n        and ' + Queries.COL_TXN_MESSAGE_FIELD_ID + ' = \'11\'\n        and ' + Queries.COL_TXN_MESSAGE_FIELD_VALUE + ' = \'We have made a payment to you.Payment Details:\';';

		query += 'UPDATE ' + Queries.DB_TABLE_TXN_MESSAGE_CONFIG + '\n    SET\n        ' + Queries.COL_TXN_MESSAGE_FIELD_VALUE + '=\'We have made a credit note to you.\nReturn Details:\'\n    WHERE ' + Queries.COL_TXN_MESSAGE_TXN_TYPE + '=\'21\'\n        and ' + Queries.COL_TXN_MESSAGE_FIELD_ID + ' = \'11\'\n        and ' + Queries.COL_TXN_MESSAGE_FIELD_VALUE + ' = \'We have made a credit note to you.Return Details:\';';

		query += 'UPDATE ' + Queries.DB_TABLE_TXN_MESSAGE_CONFIG + '\n    SET\n        ' + Queries.COL_TXN_MESSAGE_FIELD_VALUE + '=\'We have created a debit note to you.\nReturn Details:\'\n    WHERE ' + Queries.COL_TXN_MESSAGE_TXN_TYPE + '=\'23\'\n        and ' + Queries.COL_TXN_MESSAGE_FIELD_ID + ' = \'11\'\n        and ' + Queries.COL_TXN_MESSAGE_FIELD_VALUE + ' = \'We have created a debit note to you.Return Details:\';';

		query += 'UPDATE ' + Queries.DB_TABLE_TXN_MESSAGE_CONFIG + '\n    SET\n        ' + Queries.COL_TXN_MESSAGE_FIELD_VALUE + '=\'Thanks for placing order with us.\nOrder Details:\'\n    WHERE ' + Queries.COL_TXN_MESSAGE_TXN_TYPE + '=\'24\'\n        and ' + Queries.COL_TXN_MESSAGE_FIELD_ID + ' = \'11\'\n        and ' + Queries.COL_TXN_MESSAGE_FIELD_VALUE + ' = \'Thanks for placing order with us.Order Details:\';';

		executeGeneralQuery(query);

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},
	DB_VERSION_39: function DB_VERSION_39() {
		CURRENT_DB_VERSION = 39;
		beginTransactionForDBUpgrade();

		var settingQuery = 'INSERT OR REPLACE INTO ' + Queries.DB_TABLE_SETTINGS + ' (' + Queries.COL_SETTING_KEY + ', ' + Queries.COL_SETTING_VALUE + ") values ('" + Queries.SETTING_OTHER_INCOME_ENABLED + "', '0')";
		var conn = insert(Queries.DB_TABLE_SETTINGS, settingQuery, Queries.COL_SETTING_ID);

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	DB_VERSION_38: function DB_VERSION_38() {
		CURRENT_DB_VERSION = 38;
		beginTransactionForDBUpgrade();

		var settingQuery = 'INSERT OR REPLACE INTO ' + Queries.DB_TABLE_SETTINGS + ' (' + Queries.COL_SETTING_KEY + ', ' + Queries.COL_SETTING_VALUE + ") values ('" + Queries.SETTING_THERMAL_PRINTER_NATIVE_LANG + "', '0')";
		var conn = insert(Queries.DB_TABLE_SETTINGS, settingQuery, Queries.COL_SETTING_ID);

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	DB_VERSION_37: function DB_VERSION_37() {
		CURRENT_DB_VERSION = 37;
		beginTransactionForDBUpgrade();

		var deleteQuery = 'update ' + Queries.DB_TABLE_SETTINGS + ' set ' + Queries.COL_SETTING_VALUE + " = 'Sale Order' where " + Queries.COL_SETTING_KEY + " = '" + Queries.SETTING_CUSTOM_NAME_FOR_SALE_ORDER + "' " + ' and ' + Queries.COL_SETTING_VALUE + " = 'Order Form'";

		var conn = executeGeneralQuery(deleteQuery);

		var UPGRADE_QUERY = 'delete from ' + Queries.DB_TABLE_LINEITEMS + ' where ' + Queries.COL_LINEITEM_TXN_ID + ' in ( select ' + Queries.COL_TXN_ID + ' from ' + Queries.DB_TABLE_TRANSACTIONS + ' where ' + Queries.COL_TXN_TYPE + ' in (' + TxnTypeConstant.TXN_TYPE_CASHIN + ', ' + TxnTypeConstant.TXN_TYPE_CASHOUT + ' ) )';

		executeGeneralQuery(UPGRADE_QUERY);

		var updateTransactionCurrentBalForSaleOrder = ' update ' + Queries.DB_TABLE_TRANSACTIONS + ' set ' + Queries.COL_TXN_CURRENT_BALANCE + ' = ifnull(' + Queries.COL_TXN_BALANCE_AMOUNT + ',0) where ' + Queries.COL_TXN_TYPE + ' = ' + TxnTypeConstant.TXN_TYPE_SALE_ORDER;
		var updateTransactionCurrentBalForExpense = ' update ' + Queries.DB_TABLE_TRANSACTIONS + ' set ' + Queries.COL_TXN_CURRENT_BALANCE + ' = 0 where ' + Queries.COL_TXN_TYPE + ' = ' + TxnTypeConstant.TXN_TYPE_EXPENSE;

		executeGeneralQuery(updateTransactionCurrentBalForSaleOrder);
		executeGeneralQuery(updateTransactionCurrentBalForExpense);

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	// CODE FOR DB UPGRADE FOR SYNC

	DB_VERSION_36: function DB_VERSION_36() {
		CURRENT_DB_VERSION = 36;
		beginTransactionForDBUpgrade();

		var deleteQuery = 'delete from ' + Queries.DB_TABLE_LINEITEMS + ' where ' + Queries.COL_LINEITEM_TXN_ID + ' in ( select ' + Queries.COL_TXN_ID + ' from ' + Queries.DB_TABLE_TRANSACTIONS + ' where ' + Queries.COL_TXN_TYPE + ' in (' + TxnTypeConstant.TXN_TYPE_CASHIN + ', ' + TxnTypeConstant.TXN_TYPE_CASHOUT + ' ) )';

		var conn = executeGeneralQuery(deleteQuery);

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	DB_VERSION_35: function DB_VERSION_35() {
		CURRENT_DB_VERSION = 35;
		beginTransactionForDBUpgrade();

		function addVAT0ForUAE() {
			try {
				var Country = require('./../Constants/Country.js');
				var currentCountryQuery = ' Select ' + Queries.COL_SETTING_VALUE + ' from ' + Queries.DB_TABLE_SETTINGS + ' where ' + Queries.COL_SETTING_KEY + " = '" + Queries.SETTING_USER_COUNTRY + "'";
				mainobj = [];
				var conn2 = libsqlite3.sqlite3_exec(dbHandleForUpdate, currentCountryQuery, callbackForFetchAllNameRecords, null, null);

				if (mainobj.length > 0) {
					var countryCode = mainobj[0][Queries.COL_SETTING_VALUE];
					if (countryCode && Country.isGulfCountryByCountryCode(countryCode)) {
						var checkTaxSetupDone = ' Select ' + Queries.COL_SETTING_VALUE + ' from ' + Queries.DB_TABLE_SETTINGS + ' where ' + Queries.COL_SETTING_KEY + " = '" + Queries.SETTING_TAX_SETUP_COMPLETED + "' and " + Queries.COL_SETTING_VALUE + "='1'";
						mainobj = [];
						libsqlite3.sqlite3_exec(dbHandleForUpdate, checkTaxSetupDone, callbackForFetchAllNameRecords, null, null);
						if (mainobj.length > 0) {
							var vat0RatePresent = 'Select * from ' + Queries.DB_TABLE_TAX_CODE + ' where lower(' + Queries.COL_TAX_CODE_NAME + ") = 'vat@0%'";
							mainobj = [];

							var conn = libsqlite3.sqlite3_exec(dbHandleForUpdate, vat0RatePresent, callbackForFetchAllNameRecords, null, null);
							if (!conn) {
								if (mainobj.length == 0) {
									var TaxCodeConst = require('./../Constants/TaxCodeConstants.js');
									var queryForVAT0 = 'INSERT INTO ' + Queries.DB_TABLE_TAX_CODE + '(' + Queries.COL_TAX_CODE_NAME + ', ' + Queries.COL_TAX_RATE + ', ' + Queries.COL_TAX_CODE_TYPE + ', ' + Queries.COL_TAX_RATE_TYPE + ')\n                                                    VALUES (\'VAT@0%\', 0, ' + TaxCodeConst.taxRate + ', ' + TaxCodeConst.OTHER + ')';

									var conn1 = insert(Queries.DB_TABLE_TAX_CODE, queryForVAT0, Queries.COL_TAX_CODE_ID);
								}
							}
						}
					}
				}
			} catch (err) {}
		}

		addVAT0ForUAE();

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	DB_VERSION_34: function DB_VERSION_34() {
		CURRENT_DB_VERSION = 34;
		beginTransactionForDBUpgrade();

		var CREATE_CLOSED_LINKED_TXN_DETAILS = 'create table ' + Queries.DB_TABLE_CLOSED_LINK_TXN_TABLE + '( ' + Queries.COL_CLOSED_LINK_TXN_ID + ' integer primary key autoincrement, ' + Queries.COL_CLOSED_LINK_TXN_AMOUNT + ' double default 0, ' + Queries.COL_CLOSED_LINK_TXN_TYPE + ' integer, ' + Queries.COL_CLOSED_LINK_TXN_DATE + ' date, ' + Queries.COL_CLOSED_LINK_TXN_REF_NUMBER + " varchar(50) default '')";

		var ADD_CLOSED_LINKED_TXN_REF_ID = 'alter table ' + Queries.DB_TABLE_TXN_LINKS + ' add ' + Queries.COL_TXN_LINKS_CLOSED_TXN_REF_ID + ' integer default null references ' + Queries.DB_TABLE_CLOSED_LINK_TXN_TABLE + '(' + Queries.COL_CLOSED_LINK_TXN_ID + ')';

		var conn1 = executeGeneralQuery(CREATE_CLOSED_LINKED_TXN_DETAILS);

		var conn2 = executeGeneralQuery(ADD_CLOSED_LINKED_TXN_REF_ID);

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	DB_VERSION_33: function DB_VERSION_33() {
		CURRENT_DB_VERSION = 33;
		beginTransactionForDBUpgrade();

		var ALTER_TRANSACTION_TABLE1 = ' alter table ' + Queries.DB_TABLE_TRANSACTIONS + ' add ' + Queries.COL_TXN_CURRENT_BALANCE + ' double default 0 ';
		var ALTER_TRANSACTION_TABLE2 = ' alter table ' + Queries.DB_TABLE_TRANSACTIONS + ' add ' + Queries.COL_TXN_PAYMENT_STATUS + ' integer default 1 ';
		var ALTER_FIRM_TABLE = ' alter table ' + Queries.DB_TABLE_FIRMS + ' add ' + Queries.COL_FIRM_CASH_IN_PREFIX + " varchar(10) default '' ";

		var conn1 = executeGeneralQuery(ALTER_TRANSACTION_TABLE1);

		var conn2 = executeGeneralQuery(ALTER_TRANSACTION_TABLE2);

		var conn3 = executeGeneralQuery(ALTER_FIRM_TABLE);

		var txnLinksTableCreateQuery = ' create table ' + Queries.DB_TABLE_TXN_LINKS + '(' + Queries.COL_TXN_LINKS_ID + ' integer primary key autoincrement, ' + Queries.COL_TXN_LINKS_TXN_1_ID + ' integer, ' + Queries.COL_TXN_LINKS_TXN_2_ID + ' integer, ' + Queries.COL_TXN_LINKS_AMOUNT + ' double default 0, ' + Queries.COL_TXN_LINKS_TXN_1_TYPE + ' integer, ' + Queries.COL_TXN_LINKS_TXN_2_TYPE + ' integer, ' + 'foreign key(' + Queries.COL_TXN_LINKS_TXN_1_ID + ') references ' + Queries.DB_TABLE_TRANSACTIONS + '(' + Queries.COL_TXN_ID + '), ' + 'foreign key(' + Queries.COL_TXN_LINKS_TXN_2_ID + ') references ' + Queries.DB_TABLE_TRANSACTIONS + '(' + Queries.COL_TXN_ID + ')) ';

		var conn4 = executeGeneralQuery(txnLinksTableCreateQuery);

		function migrateTxnForBill2Bill() {
			var updateTransactionCurrentBal = ' update ' + Queries.DB_TABLE_TRANSACTIONS + ' set ' + Queries.COL_TXN_CURRENT_BALANCE + ' = ifnull(' + Queries.COL_TXN_BALANCE_AMOUNT + ',0) where ' + Queries.COL_TXN_TYPE + ' in (' + TxnTypeConstant.TXN_TYPE_SALE + ', ' + TxnTypeConstant.TXN_TYPE_PURCHASE + ', ' + TxnTypeConstant.TXN_TYPE_SALE_RETURN + ', ' + TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN + ', ' + TxnTypeConstant.TXN_TYPE_SALE_ORDER + ', ' + TxnTypeConstant.TXN_TYPE_ESTIMATE + ', ' + TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER + ', ' + TxnTypeConstant.TXN_TYPE_EXPENSE + ', ' + TxnTypeConstant.TXN_TYPE_POPENBALANCE + ', ' + TxnTypeConstant.TXN_TYPE_ROPENBALANCE + ')';

			var updateTransactionCurrentBalForPaymentTxns = ' update ' + Queries.DB_TABLE_TRANSACTIONS + ' set ' + Queries.COL_TXN_CURRENT_BALANCE + ' = (ifnull(' + Queries.COL_TXN_CASH_AMOUNT + ',0) + ifnull(' + Queries.COL_TXN_DISCOUNT_AMOUNT + ', 0)) ' + ' where ' + Queries.COL_TXN_TYPE + ' in (' + TxnTypeConstant.TXN_TYPE_CASHIN + ', ' + TxnTypeConstant.TXN_TYPE_CASHOUT + ')';

			var updateTransactionPaidStatus = ' update ' + Queries.DB_TABLE_TRANSACTIONS + ' set ' + Queries.COL_TXN_PAYMENT_STATUS + ' = ' + TxnPaymentStatusConstants.PAID + ' where ' + Queries.COL_TXN_TYPE + ' in (' + TxnTypeConstant.TXN_TYPE_SALE + ', ' + TxnTypeConstant.TXN_TYPE_PURCHASE + ', ' + TxnTypeConstant.TXN_TYPE_SALE_RETURN + ', ' + TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN + ', ' + TxnTypeConstant.TXN_TYPE_SALE_ORDER + ', ' + TxnTypeConstant.TXN_TYPE_ESTIMATE + ', ' + TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER + ', ' + TxnTypeConstant.TXN_TYPE_EXPENSE + ', ' + TxnTypeConstant.TXN_TYPE_POPENBALANCE + ', ' + TxnTypeConstant.TXN_TYPE_ROPENBALANCE + ', ' + TxnTypeConstant.TXN_TYPE_CASHIN + ', ' + TxnTypeConstant.TXN_TYPE_CASHOUT + ') and ' + Queries.COL_TXN_CURRENT_BALANCE + '<= 0.000001';

			var updateTransactionPartialStatus = ' update ' + Queries.DB_TABLE_TRANSACTIONS + ' set ' + Queries.COL_TXN_PAYMENT_STATUS + ' = ' + TxnPaymentStatusConstants.PARTIAL + ' where ' + Queries.COL_TXN_TYPE + ' in (' + TxnTypeConstant.TXN_TYPE_SALE + ', ' + TxnTypeConstant.TXN_TYPE_PURCHASE + ', ' + TxnTypeConstant.TXN_TYPE_SALE_RETURN + ', ' + TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN + ', ' + TxnTypeConstant.TXN_TYPE_SALE_ORDER + ', ' + TxnTypeConstant.TXN_TYPE_ESTIMATE + ', ' + TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER + ', ' + TxnTypeConstant.TXN_TYPE_EXPENSE + ', ' + TxnTypeConstant.TXN_TYPE_POPENBALANCE + ', ' + TxnTypeConstant.TXN_TYPE_ROPENBALANCE + ') and ' + Queries.COL_TXN_CURRENT_BALANCE + '> 0.000001 and ifnull(' + Queries.COL_TXN_CASH_AMOUNT + ',0) > 0.000001';

			var conn5 = executeGeneralQuery(updateTransactionCurrentBal);

			var conn6 = executeGeneralQuery(updateTransactionCurrentBalForPaymentTxns);

			var conn7 = executeGeneralQuery(updateTransactionPaidStatus);

			var conn8 = executeGeneralQuery(updateTransactionPartialStatus);
		}

		function migrateCashInCashOutHeader() {
			var cashInHeaderFixQuery = 'update ' + Queries.DB_TABLE_SETTINGS + ' set ' + Queries.COL_SETTING_VALUE + " = 'Payment Receipt' where " + Queries.COL_SETTING_KEY + " = '" + Queries.SETTING_CUSTOM_NAME_FOR_CASH_IN + "' " + ' and ' + Queries.COL_SETTING_VALUE + " = 'Cash-In'";
			var cashOutHeaderFixQuery = 'update ' + Queries.DB_TABLE_SETTINGS + ' set ' + Queries.COL_SETTING_VALUE + " = 'Payment Out' where " + Queries.COL_SETTING_KEY + " = '" + Queries.SETTING_CUSTOM_NAME_FOR_CASH_OUT + "' " + ' and ' + Queries.COL_SETTING_VALUE + " = 'Cash-Out'";

			try {
				var conn9 = executeGeneralQuery(cashInHeaderFixQuery);

				var conn10 = executeGeneralQuery(cashOutHeaderFixQuery);
			} catch (err) {}
		}

		migrateTxnForBill2Bill();
		migrateCashInCashOutHeader();

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	DB_VERSION_32: function DB_VERSION_32() {
		CURRENT_DB_VERSION = 32;
		beginTransactionForDBUpgrade();

		var Country = require('./../Constants/Country.js');

		function migrateCountryCode() {
			var newCountryCode = '';
			var currentCountryQuery = ' Select ' + Queries.COL_SETTING_VALUE + ' from ' + Queries.DB_TABLE_SETTINGS + ' where ' + Queries.COL_SETTING_KEY + " = '" + Queries.SETTING_USER_COUNTRY + "'";
			mainobj = [];
			libsqlite3.sqlite3_exec(dbHandleForUpdate, currentCountryQuery, callbackForFetchAllNameRecords, null, null);

			if (mainobj.length > 0) {
				var countryName = mainobj[0][Queries.COL_SETTING_VALUE];
				if (countryName) {
					var correspondingCountry = Country.getCountryFromName(countryName, true);
					newCountryCode = correspondingCountry.countryCode;
					var queryUpdateCountryCode = 'update ' + Queries.DB_TABLE_SETTINGS + ' set ' + Queries.COL_SETTING_VALUE + ' = \'' + correspondingCountry.countryCode + '\' where ' + Queries.COL_SETTING_KEY + ' =\'' + Queries.SETTING_USER_COUNTRY + '\'';

					var conn1 = executeGeneralQuery(queryUpdateCountryCode);
				}
			}

			return newCountryCode;
		}

		function deleteGSTTaxRateForOutsideIndia() {
			try {
				var currentCountryQuery = ' Select ' + Queries.COL_SETTING_VALUE + ' from ' + Queries.DB_TABLE_SETTINGS + ' where ' + Queries.COL_SETTING_KEY + " = '" + Queries.SETTING_USER_COUNTRY + "' and " + Queries.COL_SETTING_VALUE + " = '" + Country.Countries.INDIA.countryCode + "'";
				var isCountryIndia = false;
				mainobj = [];
				libsqlite3.sqlite3_exec(dbHandleForUpdate, currentCountryQuery, callbackForFetchAllNameRecords, null, null);
				if (mainobj.length == 1) {
					isCountryIndia = true;
				}
				if (!isCountryIndia) {
					var TaxCodeConst = require('./../Constants/TaxCodeConstants.js');
					var getGSTTaxRates = 'select ' + Queries.COL_TAX_CODE_ID + ' from ' + Queries.DB_TABLE_TAX_CODE + ' where ' + Queries.COL_TAX_CODE_TYPE + ' = ' + TaxCodeConst.taxRate + ' and ' + Queries.COL_TAX_RATE_TYPE + ' in (' + TaxCodeConst.IGST + ', ' + TaxCodeConst.CGST + ', ' + TaxCodeConst.SGST + ', ' + TaxCodeConst.CESS + ', ' + TaxCodeConst.Exempted + ')';

					var getGSTTaxGroups = 'select ' + Queries.COL_TAX_MAPPING_GROUP_ID + ' from ' + Queries.DB_TABLE_TAX_MAPPING + ' where ' + Queries.COL_TAX_MAPPING_CODE_ID + ' in (' + getGSTTaxRates + ') ' + ' group by ' + Queries.COL_TAX_MAPPING_GROUP_ID;

					var itemTaxQuery = 'select ' + Queries.COL_ITEM_TAX_ID + ' as tax_id from ' + Queries.DB_TABLE_ITEMS + ' where ' + Queries.COL_ITEM_TAX_ID + ' is not null group by ' + Queries.COL_ITEM_TAX_ID;
					var transactionTaxQuery = 'select ' + Queries.COL_TXN_TAX_ID + ' as tax_id from ' + Queries.DB_TABLE_TRANSACTIONS + ' where ' + Queries.COL_TXN_TAX_ID + ' is not null group by ' + Queries.COL_TXN_TAX_ID;
					var lineItemTaxQuery = 'select ' + Queries.COL_LINEITEM_TAX_ID + ' as tax_id from ' + Queries.DB_TABLE_LINEITEMS + ' where ' + Queries.COL_LINEITEM_TAX_ID + ' is not null group by ' + Queries.COL_LINEITEM_TAX_ID;

					var taxInUsedQuery = itemTaxQuery + ' union all ' + transactionTaxQuery + ' union all ' + lineItemTaxQuery;

					var gstTaxGroups = [];
					mainobj = [];
					libsqlite3.sqlite3_exec(dbHandleForUpdate, getGSTTaxGroups, callbackForFetchAllNameRecords, null, null);
					if (mainobj.length > 0) {
						$.each(mainobj, function (key, cursor) {
							var taxGroupId = Number(cursor[Queries.COL_TAX_MAPPING_GROUP_ID]);
							gstTaxGroups.push(taxGroupId);
						});
					}

					var usedTaxCodeSet = new _set2.default();
					mainobj = [];
					libsqlite3.sqlite3_exec(dbHandleForUpdate, taxInUsedQuery, callbackForFetchAllNameRecords, null, null);
					if (mainobj.length > 0) {
						$.each(mainobj, function (key, cursor) {
							var taxGroupId = Number(cursor['tax_id']);
							usedTaxCodeSet.add(taxGroupId);
						});
					}

					$.each(gstTaxGroups, function (key, value) {
						if (!usedTaxCodeSet.has(value)) {
							var queryDeleteMapping = 'delete from ' + Queries.DB_TABLE_TAX_MAPPING + ' where ' + Queries.COL_TAX_MAPPING_GROUP_ID + ' = ' + value;

							var comm = executeGeneralQuery(queryDeleteMapping);

							if (!comm) {
								addUpgradeQueries(queryDeleteMapping);
								var queryDeleteTax = 'delete from ' + Queries.DB_TABLE_TAX_CODE + ' where ' + Queries.COL_TAX_CODE_ID + ' = ' + value;

								var conn1 = executeGeneralQuery(queryDeleteTax);
							}
						}
					});

					var ratesToBeDeleted = [];
					var gstRatesToBeDeletedQuery = getGSTTaxRates + ' and ' + Queries.COL_TAX_CODE_ID + ' not in (' + 'select ' + Queries.COL_TAX_MAPPING_CODE_ID + ' from ' + Queries.DB_TABLE_TAX_MAPPING + ')';
					mainobj = [];
					libsqlite3.sqlite3_exec(dbHandleForUpdate, gstRatesToBeDeletedQuery, callbackForFetchAllNameRecords, null, null);
					if (mainobj.length > 0) {
						$.each(mainobj, function (key, cursor) {
							var taxId = Number(cursor[Queries.COL_TAX_CODE_ID]);
							ratesToBeDeleted.push(taxId);
						});
					}

					$.each(ratesToBeDeleted, function (key, value) {
						if (!usedTaxCodeSet.has(value)) {
							var queryDeleteMapping = 'delete from ' + Queries.DB_TABLE_TAX_CODE + ' where ' + Queries.COL_TAX_CODE_ID + ' = ' + value;

							var conn2 = executeGeneralQuery(queryDeleteMapping);
						}
					});
				}
			} catch (err) {}
		}

		function completeTaxSetup() {
			var currentTaxSetup = ' Select ' + Queries.COL_SETTING_VALUE + ' from ' + Queries.DB_TABLE_SETTINGS + ' where ' + Queries.COL_SETTING_KEY + " = '" + Queries.SETTING_TAX_SETUP_COMPLETED + "'";
			mainobj = [];
			libsqlite3.sqlite3_exec(dbHandleForUpdate, currentTaxSetup, callbackForFetchAllNameRecords, null, null);

			if (mainobj.length == 1) {
				queryCompleteTaxSetup = 'update ' + Queries.DB_TABLE_SETTINGS + ' set ' + Queries.COL_SETTING_VALUE + ' = \'1\' where ' + Queries.COL_SETTING_KEY + ' =\'' + Queries.SETTING_TAX_SETUP_COMPLETED + '\'';

				var conn3 = executeGeneralQuery(queryCompleteTaxSetup);
			} else {
				var _queryCompleteTaxSetup = 'insert into ' + Queries.DB_TABLE_SETTINGS + ' (' + Queries.COL_SETTING_KEY + ',' + Queries.COL_SETTING_VALUE + ') ' + " values ('" + Queries.SETTING_TAX_SETUP_COMPLETED + "', '1' )";

				var conn3 = insert(Queries.DB_TABLE_SETTINGS, _queryCompleteTaxSetup, Queries.COL_SETTING_KEY);
			}
		}

		function fixAdditionalCESSHeaderInPrint() {
			try {
				var currentHeaderQuery = ' Select ' + Queries.COL_SETTING_VALUE + ' from ' + Queries.DB_TABLE_SETTINGS + ' where ' + Queries.COL_SETTING_KEY + " = '" + Queries.SETTING_ADDITIONALCESS_COLUMNHEADER_VALUE + "' and " + Queries.COL_SETTING_VALUE + " = '1'";
				mainobj = [];
				libsqlite3.sqlite3_exec(dbHandleForUpdate, currentHeaderQuery, callbackForFetchAllNameRecords, null, null);

				if (mainobj.length == 1) {
					var Defaults = require('./../Constants/Defaults.js');
					var queryUpdateHeader = 'update ' + Queries.DB_TABLE_SETTINGS + ' set ' + Queries.COL_SETTING_VALUE + ' = \'' + Defaults.DEFAULT_ADDITIONALCESS_COLUMNHEADER_VALUE + '\' where ' + Queries.COL_SETTING_KEY + ' =\'' + Queries.SETTING_ADDITIONALCESS_COLUMNHEADER_VALUE + '\'';

					var conn4 = executeGeneralQuery(queryUpdateHeader);
				}
			} catch (err) {}
		}

		function fixSettingKeys() {
			try {
				var FIX_SETTING_KEY = 'update ' + Queries.DB_TABLE_SETTINGS + ' set ' + Queries.COL_SETTING_KEY + ' = replace(' + Queries.COL_SETTING_KEY + ", ' ', '') where " + Queries.COL_SETTING_KEY + ' is not null';

				var conn5 = executeGeneralQuery(FIX_SETTING_KEY);
			} catch (err) {}
		}

		try {
			var migratedCountryCode = migrateCountryCode();
			deleteGSTTaxRateForOutsideIndia();
			if (migratedCountryCode) {
				completeTaxSetup();
			}
			fixAdditionalCESSHeaderInPrint();
			fixSettingKeys();
		} catch (err) {}

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	DB_VERSION_31: function DB_VERSION_31() {
		CURRENT_DB_VERSION = 31;
		beginTransactionForDBUpgrade();

		var ALTER_LINEITEM_TABLE1 = 'alter table ' + Queries.DB_TABLE_LINEITEMS + ' add ' + Queries.COL_LINEITEM_FREE_QUANTITY + ' double default 0';

		var conn = executeGeneralQuery(ALTER_LINEITEM_TABLE1);

		var ALTER_TXN_TABLE = 'alter table ' + Queries.DB_TABLE_TRANSACTIONS + ' add ' + Queries.COL_TXN_EWAY_BILL_NUMBER + " varchar(50) default ''";

		var conn1 = executeGeneralQuery(ALTER_TXN_TABLE);

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	DB_VERSION_30: function DB_VERSION_30() {
		CURRENT_DB_VERSION = 30;
		beginTransactionForDBUpgrade();

		var TaxCodeConst = require('./../Constants/TaxCodeConstants.js');

		try {
			mainobj = [];
			var query_to_get_correct_gst = 'Select ' + Queries.COL_TAX_CODE_ID + ' from ' + Queries.DB_TABLE_TAX_CODE + ' where ' + Queries.COL_TAX_CODE_ID + ' = 28  and ' + Queries.COL_TAX_CODE_NAME + ' = \'GST@28%\' and ' + Queries.COL_TAX_CODE_TYPE + ' = ' + TaxCodeConst.taxGroup + ' and ' + Queries.COL_TAX_RATE + ' = 28.0';
			libsqlite3.sqlite3_exec(dbHandleForUpdate, query_to_get_correct_gst, callbackForFetchAllNameRecords, null, null);

			if (mainobj.length == 1) {
				var query_to_get_correct_sgst = 'Select ' + Queries.COL_TAX_CODE_ID + ' from ' + Queries.DB_TABLE_TAX_CODE + ' where ' + Queries.COL_TAX_CODE_ID + ' = 14 and  ' + Queries.COL_TAX_CODE_NAME + ' = \'SGST@14%\' and ' + Queries.COL_TAX_CODE_TYPE + ' = ' + TaxCodeConst.taxRate + ' and ' + Queries.COL_TAX_RATE + ' = 14.0';
				var query_to_get_correct_cgst = 'Select ' + Queries.COL_TAX_CODE_ID + ' from ' + Queries.DB_TABLE_TAX_CODE + ' where ' + Queries.COL_TAX_CODE_ID + ' = 7 and  ' + Queries.COL_TAX_CODE_NAME + ' = \'CGST@14%\' and ' + Queries.COL_TAX_CODE_TYPE + ' = ' + TaxCodeConst.taxRate + ' and ' + Queries.COL_TAX_RATE + ' = 14.0';
				mainobj = [];
				libsqlite3.sqlite3_exec(dbHandleForUpdate, query_to_get_correct_sgst, callbackForFetchAllNameRecords, null, null);
				if (mainobj.length == 1) {
					mainobj = [];
					libsqlite3.sqlite3_exec(dbHandleForUpdate, query_to_get_correct_cgst, callbackForFetchAllNameRecords, null, null);
					if (mainobj.length == 1) {
						mainobj = [];
						var find_gst_28_mapping = 'Select * from ' + Queries.DB_TABLE_TAX_MAPPING + ' where ' + Queries.COL_TAX_MAPPING_GROUP_ID + ' = 28';
						libsqlite3.sqlite3_exec(dbHandleForUpdate, find_gst_28_mapping, callbackForFetchAllNameRecords, null, null);
						if (mainobj.length == 1) {
							var rate_id = Number(mainobj[0][Queries.COL_TAX_MAPPING_CODE_ID]);
							if (rate_id == 7) {
								var create_sgst_mapping = 'INSERT INTO ' + Queries.DB_TABLE_TAX_MAPPING + ' (' + Queries.COL_TAX_MAPPING_GROUP_ID + ', ' + Queries.COL_TAX_MAPPING_CODE_ID + ', ' + Queries.COL_TAX_MAPPING_DATE_CREATED + ', ' + Queries.COL_TAX_MAPPING_DATE_MODIFIED + ') values(28, 14, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)';

								var conn1 = insert(Queries.DB_TABLE_TAX_MAPPING, create_sgst_mapping, Queries.COL_TAX_MAPPING_GROUP_ID);
							}
						}
					}
				}
			}
		} catch (err) {}

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},
	DB_VERSION_29: function DB_VERSION_29() {
		CURRENT_DB_VERSION = 29;
		beginTransactionForDBUpgrade();

		try {
			var shippingSettingQuery = 'Select * from ' + Queries.DB_TABLE_SETTINGS + ' where ' + Queries.COL_SETTING_KEY + ' = \'VYAPAR.PRINTSHIPPINGADDRESSENABLED\' and ' + Queries.COL_SETTING_VALUE + ' = \'1\'';
			mainobj = [];
			var connSetting = libsqlite3.sqlite3_exec(dbHandleForUpdate, shippingSettingQuery, callbackForFetchAllNameRecords, null, null);

			if (mainobj.length > 0) {
				var queryShippingSettingUpdate = 'update ' + Queries.DB_TABLE_SETTINGS + ' set ' + Queries.COL_SETTING_VALUE + ' = \'1\' where ' + Queries.COL_SETTING_KEY + ' =\'' + Queries.SETTING_PRINT_PARTY_SHIPPING_ADDRESS + '\'';

				var conn = executeGeneralQuery(queryShippingSettingUpdate);
			}
		} catch (errSetting) {}

		var CREATE_ITEM_STOCK_TRACKING_TABLE = 'create table ' + Queries.DB_ITEM_STOCK_TRACKING_TABLE + '(' + Queries.COL_IST_ID + ' integer primary key autoincrement, ' + Queries.COL_IST_BATCH_NUMBER + " varchar(30) default '', " + Queries.COL_IST_SERIAL_NUMBER + " varchar(30) default '', " + Queries.COL_IST_MRP + ' double default 0, ' + Queries.COL_IST_EXPIRY_DATE + ' datetime default null, ' + Queries.COL_IST_MANUFACTURING_DATE + ' datetime default null, ' + Queries.COL_IST_SIZE + " varchar(100) default '', " + Queries.COL_IST_ITEM_ID + ' integer  references ' + Queries.DB_TABLE_ITEMS + '(' + Queries.COL_ITEM_ID + '), ' + Queries.COL_IST_CURRENT_QUANTITY + ' double default 0, ' + Queries.COL_IST_OPENING_QUANTITY + ' double default 0, ' + Queries.COL_IST_TYPE + ' integer default 0 )';

		var ALTER_ITEM_TABLE2 = 'alter table ' + Queries.DB_TABLE_ITEMS + ' add ' + Queries.COL_ITEM_DESCRIPTION + " varchar(256) default ''";

		var ALTER_LINEITEM_TABLE1 = 'alter table ' + Queries.DB_TABLE_LINEITEMS + ' add ' + Queries.COL_LINEITEM_SIZE + " varchar(100) default ''";
		var ALTER_LINEITEM_TABLE2 = 'alter table ' + Queries.DB_TABLE_LINEITEMS + ' add ' + Queries.COL_LINEITEM_IST_ID + ' integer default null references ' + Queries.DB_ITEM_STOCK_TRACKING_TABLE + '(' + Queries.COL_IST_ID + ') ';

		var ALTER_ITEM_ADJUSTMENT_TABLE = 'alter table ' + Queries.DB_TABLE_ITEM_ADJUSTMENT + ' add ' + Queries.COL_ITEM_ADJ_IST_ID + ' integer default null references ' + Queries.DB_ITEM_STOCK_TRACKING_TABLE + '(' + Queries.COL_IST_ID + ') ';

		var conn1 = executeGeneralQuery(CREATE_ITEM_STOCK_TRACKING_TABLE);

		var conn2 = executeGeneralQuery(ALTER_LINEITEM_TABLE1);

		var conn4 = executeGeneralQuery(ALTER_ITEM_TABLE2);

		var conn5 = executeGeneralQuery(ALTER_LINEITEM_TABLE2);

		var conn6 = executeGeneralQuery(ALTER_ITEM_ADJUSTMENT_TABLE);

		try {
			var rateQuery = 'Select ' + Queries.DB_TABLE_TAX_MAPPING + '.' + Queries.COL_TAX_MAPPING_GROUP_ID + ' as ' + Queries.COL_TAX_MAPPING_GROUP_ID + ', total(' + Queries.COL_TAX_RATE + ') as ' + Queries.COL_TAX_RATE + ' from ' + Queries.DB_TABLE_TAX_MAPPING + ' join ' + Queries.DB_TABLE_TAX_CODE + ' on ' + Queries.DB_TABLE_TAX_MAPPING + '.' + Queries.COL_TAX_MAPPING_CODE_ID + '=' + Queries.DB_TABLE_TAX_CODE + '.' + Queries.COL_TAX_CODE_ID + ' group by ' + Queries.DB_TABLE_TAX_MAPPING + '.' + Queries.COL_TAX_MAPPING_GROUP_ID;

			mainobj = [];
			var conn = libsqlite3.sqlite3_exec(dbHandleForUpdate, rateQuery, callbackForFetchAllNameRecords, null, null);

			if (mainobj.length > 0) {
				$.each(mainobj, function (key, cursor) {
					var taxRateId = Number(cursor[Queries.COL_TAX_MAPPING_GROUP_ID]);
					var rate = Number(cursor[Queries.COL_TAX_RATE]);

					var queryUpdate = 'UPDATE ' + Queries.DB_TABLE_TAX_CODE + ' SET ' + Queries.COL_TAX_RATE + ' = ' + rate + '\n                                where ' + Queries.COL_TAX_CODE_ID + ' = ' + taxRateId;
					try {
						var conn3 = executeGeneralQuery(queryUpdate);
					} catch (err) {}
				});
			}
		} catch (err) {}

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},
	DB_VERSION_28: function DB_VERSION_28() {
		CURRENT_DB_VERSION = 28;
		beginTransactionForDBUpgrade();

		var GSTRUnits = require('./../Constants/GSTRUnits.js');

		var queryAlter1 = 'alter table ' + Queries.DB_TABLE_ITEM_UNITS + ' add ' + Queries.COL_UNIT_FULL_NAME_EDITABLE + ' integer default 1';
		var queryAlter2 = 'alter table ' + Queries.DB_TABLE_ITEM_UNITS + ' add ' + Queries.COL_UNIT_UNIT_DELETABLE + ' integer default 1';

		var conn1 = executeGeneralQuery(queryAlter1);

		var conn2 = executeGeneralQuery(queryAlter2);

		var OLD_KILOGRAM_UNIT_FULLNAME = 'Kilogram';
		var OLD_KILOGRAM_UNIT_SHORTNAME = 'kg';
		var OLD_GRAM_UNIT_FULLNAME = 'Gram';
		var OLD_GRAM_UNIT_SHORTNAME = 'gm';
		var OLD_LITRE_UNIT_FULLNAME = 'Litre';
		var OLD_LITRE_UNIT_SHORTNAME = 'ltr';
		var OLD_MILLILITRE_UNIT_FULLNAME = 'Millilitre';
		var OLD_MILLILITRE_UNIT_SHORTNAME = 'ml';
		var OLD_METER_UNIT_FULLNAME = 'Meter';
		var OLD_METER_UNIT_SHORTNAME = 'm';
		var OLD_CENTIMETER_UNIT_FULLNAME = 'Centimeter';
		var OLD_CENTIMETER_UNIT_SHORTNAME = 'cm';

		var gstrUnitV28Map = GSTRUnits.get(28);

		var oldValueMap = new _map2.default([[gstrUnitV28Map.get('KILOGRAMS'), [OLD_KILOGRAM_UNIT_SHORTNAME, OLD_KILOGRAM_UNIT_FULLNAME]], [gstrUnitV28Map.get('GRAMMES'), [OLD_GRAM_UNIT_SHORTNAME, OLD_GRAM_UNIT_FULLNAME]], [gstrUnitV28Map.get('LITRE'), [OLD_LITRE_UNIT_SHORTNAME, OLD_LITRE_UNIT_FULLNAME]], [gstrUnitV28Map.get('MILILITRE'), [OLD_MILLILITRE_UNIT_SHORTNAME, OLD_MILLILITRE_UNIT_FULLNAME]], [gstrUnitV28Map.get('METERS'), [OLD_METER_UNIT_SHORTNAME, OLD_METER_UNIT_FULLNAME]], [gstrUnitV28Map.get('CENTIMETERS'), [OLD_CENTIMETER_UNIT_SHORTNAME, OLD_CENTIMETER_UNIT_FULLNAME]]]);

		try {
			var pair;
			//
			gstrUnitV28Map.forEach(function (gstrUnit, key) {
				try {
					if (gstrUnit == gstrUnitV28Map.get('KILOGRAMS') || gstrUnit == gstrUnitV28Map.get('GRAMMES') || gstrUnit == gstrUnitV28Map.get('LITRE') || gstrUnit == gstrUnitV28Map.get('MILILITRE') || gstrUnit == gstrUnitV28Map.get('METERS') || gstrUnit == gstrUnitV28Map.get('CENTIMETERS')) {
						pair = oldValueMap.get(gstrUnit);
						var unitId = getUnitIdWithShortNameAndFullNameExists(pair[0], pair[1]);

						if (unitId > 0) {
							replaceShortNameAndFullNameWithId(gstrUnit, unitId);
						} else if (getUnitsWithShortNameOrFullNameExists(gstrUnit[0], gstrUnit[1]) > 0) {
							//                        skip
						} else {
							insertUnit(gstrUnit);
						}
					} else {
						if (getUnitsWithShortNameOrFullNameExists(gstrUnit[0], gstrUnit[1]) > 0) {
							//                        skip
						} else {
							insertUnit(gstrUnit);
						}
					}
				} catch (err) {}
			});
		} catch (err) {
			console.log(err);
		}

		function getUnitIdWithShortNameAndFullNameExists(shortName, fullName) {
			var unitId = 0;
			var query = 'select ' + Queries.COL_UNIT_ID + ' from ' + Queries.DB_TABLE_ITEM_UNITS + ' where upper(' + Queries.COL_UNIT_SHORT_NAME + ') = ' + ('\'' + MyString.escapeStringForDBInsertion(shortName.toUpperCase()) + '\'') + ' and upper(' + Queries.COL_UNIT_NAME + ') = ' + ('\'' + MyString.escapeStringForDBInsertion(fullName.toUpperCase()) + '\'');

			mainobj = [];
			var conn = libsqlite3.sqlite3_exec(dbHandleForUpdate, query, callbackForFetchAllNameRecords, null, null);

			if (!conn) {
				if (mainobj.length > 0) {
					unitId = mainobj[0][Queries.COL_UNIT_ID];
				}
			}
			return unitId;
		}

		function replaceShortNameAndFullNameWithId(units, unitId) {
			var shortName = units[0];
			var fullName = units[1];

			var queryUpdate = 'update ' + Queries.DB_TABLE_ITEM_UNITS + ' set ' + Queries.COL_UNIT_SHORT_NAME + ' = ' + ('\'' + MyString.escapeStringForDBInsertion(shortName) + '\'') + ', ' + Queries.COL_UNIT_NAME + ' = ' + ('\'' + MyString.escapeStringForDBInsertion(fullName) + '\'') + ',' + Queries.COL_UNIT_FULL_NAME_EDITABLE + ' = 0,' + Queries.COL_UNIT_UNIT_DELETABLE + ' = 0 where ' + Queries.COL_UNIT_ID + ' = ' + unitId;

			var conn = executeGeneralQuery(queryUpdate);
		}

		function getUnitsWithShortNameOrFullNameExists(shortName, fullName) {
			var count = 0;
			var query = 'select ' + Queries.COL_UNIT_ID + ' from ' + Queries.DB_TABLE_ITEM_UNITS + ' where upper(' + Queries.COL_UNIT_SHORT_NAME + ') = ' + ('\'' + MyString.escapeStringForDBInsertion(shortName.toUpperCase()) + '\'') + ' or upper(' + Queries.COL_UNIT_NAME + ') = ' + ('\'' + MyString.escapeStringForDBInsertion(fullName.toUpperCase()) + '\'');

			mainobj = [];
			var conn = libsqlite3.sqlite3_exec(dbHandleForUpdate, query, callbackForFetchAllNameRecords, null, null);
			console.log(conn);

			if (!conn) {
				if (mainobj.length > 0) {
					count = mainobj.length;
				}
			}
			return count;
		}

		function insertUnit(units) {
			var shortName = units[0];
			var fullName = units[1];

			var queryInsert = 'INSERT INTO ' + Queries.DB_TABLE_ITEM_UNITS + '(' + Queries.COL_UNIT_NAME + ', ' + Queries.COL_UNIT_SHORT_NAME + ', ' + Queries.COL_UNIT_FULL_NAME_EDITABLE + ', ' + Queries.COL_UNIT_UNIT_DELETABLE + ') values(' + ('\'' + MyString.escapeStringForDBInsertion(fullName) + '\'') + ', ' + ('\'' + MyString.escapeStringForDBInsertion(shortName) + '\'') + ', 0, 0)';

			var conn = insert(Queries.DB_TABLE_ITEM_UNITS, queryInsert, Queries.COL_UNIT_ID);
		}

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},
	DB_VERSION_27: function DB_VERSION_27() {
		// for supporting android

		return true;
	},
	DB_VERSION_26: function DB_VERSION_26() {
		CURRENT_DB_VERSION = 26;
		beginTransactionForDBUpgrade();

		var queryAlter1 = 'alter table ' + Queries.DB_TABLE_TRANSACTIONS + ' add ' + Queries.COL_TXN_RETURN_DATE + ' date default null';
		var queryAlter2 = 'alter table ' + Queries.DB_TABLE_TRANSACTIONS + ' add ' + Queries.COL_TXN_RETURN_REF_NUMBER + " varchar(50) default ''";
		var ALTER_NAME_TABLE_V26 = 'alter table ' + Queries.DB_TABLE_NAMES + ' add ' + Queries.COL_IS_PARTY_DETAILS_SENT + ' integer default 0';

		var conn1 = executeGeneralQuery(queryAlter1);

		var conn2 = executeGeneralQuery(queryAlter2);

		var conn3 = executeGeneralQuery(ALTER_NAME_TABLE_V26);

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	DB_VERSION_25: function DB_VERSION_25() {
		CURRENT_DB_VERSION = 25;
		beginTransactionForDBUpgrade();
		// adding txn return date in txn table
		try {
			var FIX_TXN_DATE = 'update ' + Queries.DB_TABLE_TRANSACTIONS + ' set ' + Queries.COL_TXN_DATE + ' = replace(' + Queries.COL_TXN_DATE + ", 'HH', '00') where " + Queries.COL_TXN_DATE + ' is not null';

			var conn1 = executeGeneralQuery(FIX_TXN_DATE);
		} catch (err) {}
		try {
			var FIX_TXN_DUE_DATE = 'update ' + Queries.DB_TABLE_TRANSACTIONS + ' set ' + Queries.COL_TXN_DUE_DATE + ' = replace(' + Queries.COL_TXN_DUE_DATE + ", 'HH', '00') where " + Queries.COL_TXN_DUE_DATE + ' is not null';

			var conn2 = executeGeneralQuery(FIX_TXN_DUE_DATE);
		} catch (err) {}
		try {
			var FIX_TXN_PO_DATE = 'update ' + Queries.DB_TABLE_TRANSACTIONS + ' set ' + Queries.COL_TXN_PO_DATE + ' = replace(' + Queries.COL_TXN_PO_DATE + ", 'HH', '00') where " + Queries.COL_TXN_PO_DATE + ' is not null';

			var conn3 = executeGeneralQuery(FIX_TXN_PO_DATE);
		} catch (err) {}
		try {
			var FIX_LINEITEMS_EXPIRY_DATE = 'update ' + Queries.DB_TABLE_LINEITEMS + ' set ' + Queries.COL_LINEITEM_EXPIRY_DATE + ' = replace(' + Queries.COL_LINEITEM_EXPIRY_DATE + ", 'HH', '00') where " + Queries.COL_LINEITEM_EXPIRY_DATE + ' is not null';

			var conn4 = executeGeneralQuery(FIX_LINEITEMS_EXPIRY_DATE);
		} catch (err) {}
		try {
			var FIX_LINEITEMS_MFG_DATE = 'update ' + Queries.DB_TABLE_LINEITEMS + ' set ' + Queries.COL_LINEITEM_MANUFACTURING_DATE + ' = replace(' + Queries.COL_LINEITEM_MANUFACTURING_DATE + ", 'HH', '00') where " + Queries.COL_LINEITEM_MANUFACTURING_DATE + ' is not null';

			var conn5 = executeGeneralQuery(FIX_LINEITEMS_MFG_DATE);
		} catch (err) {}
		try {
			var FIX_ITEM_ADJ_DATE = 'update ' + Queries.DB_TABLE_ITEM_ADJUSTMENT + ' set ' + Queries.COL_ITEM_ADJ_DATE + ' = replace(' + Queries.COL_ITEM_ADJ_DATE + ", 'HH', '00') where " + Queries.COL_ITEM_ADJ_DATE + ' is not null';

			var conn6 = executeGeneralQuery(FIX_ITEM_ADJ_DATE);
		} catch (err) {}
		try {
			var FIX_FIRST_TIME_LOGIN_DATE = 'update ' + Queries.DB_TABLE_SETTINGS + ' set ' + Queries.COL_SETTING_VALUE + ' = replace(' + Queries.COL_SETTING_VALUE + ", 'HH', '00') where " + Queries.COL_SETTING_KEY + " = '" + Queries.SETTING_FIRST_TIME_LOGIN_DATE + "' and " + Queries.COL_SETTING_VALUE + ' is not null';

			var conn7 = executeGeneralQuery(FIX_FIRST_TIME_LOGIN_DATE);
		} catch (err) {}
		try {
			var FIX_FREE_TRIAL_START_DATE = 'update ' + Queries.DB_TABLE_SETTINGS + ' set ' + Queries.COL_SETTING_VALUE + ' = replace(' + Queries.COL_SETTING_VALUE + ", 'HH', '00') where " + Queries.COL_SETTING_KEY + " = '" + Queries.SETTING_FREE_TRIAL_START_DATE + "' and " + Queries.COL_SETTING_VALUE + ' is not null';

			var conn8 = executeGeneralQuery(FIX_FREE_TRIAL_START_DATE);
		} catch (err) {}

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	DB_VERSION_24: function DB_VERSION_24() {
		CURRENT_DB_VERSION = 24;
		beginTransactionForDBUpgrade();

		// adding po date in txn table
		var queryAlter1 = 'alter table ' + Queries.DB_TABLE_TRANSACTIONS + ' add ' + Queries.COL_TXN_PO_DATE + ' date default null';
		var queryAlter2 = 'alter table ' + Queries.DB_TABLE_TRANSACTIONS + ' add ' + Queries.COL_TXN_PO_REF_NUMBER + " varchar(50) default ''";

		var conn1 = executeGeneralQuery(queryAlter1);

		var conn2 = executeGeneralQuery(queryAlter2);

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	DB_VERSION_23: function DB_VERSION_23() {
		CURRENT_DB_VERSION = 23;

		beginTransactionForDBUpgrade();

		var TaxCodeType = require('./../Constants/TaxCodeConstants.js');
		var NameCustomerType = require('./../Constants/NameCustomerType.js');

		// alter table transactions to add new column itc
		var alterTransactionTable = 'ALTER TABLE ' + Queries.DB_TABLE_TRANSACTIONS + ' ADD COLUMN ' + Queries.COL_TXN_ITC_APPLICABLE + ' integer default 0';

		var conn1 = executeGeneralQuery(alterTransactionTable);

		// alter table lineitems to add new column itc
		var alterLineItemTable = 'ALTER TABLE ' + Queries.DB_TABLE_LINEITEMS + ' ADD COLUMN ' + Queries.COL_LINEITEM_ITC_APPLICABLE + ' integer default 0';

		var conn2 = executeGeneralQuery(alterLineItemTable);

		// alter table parties to add customer type
		var alterPartyTable = 'ALTER TABLE ' + Queries.DB_TABLE_NAMES + ' ADD COLUMN ' + Queries.COL_NAME_CUSTOMER_TYPE + ' INTEGER DEFAULT 0';

		var conn3 = executeGeneralQuery(alterPartyTable);

		(function migratePartyTable() {
			var updateQuery = 'UPDATE ' + Queries.DB_TABLE_NAMES + ' SET ' + Queries.COL_NAME_CUSTOMER_TYPE + ' = ' + NameCustomerType.REGISTERED_NORMAL + '\n            WHERE LENGTH(' + Queries.COL_NAME_GSTIN_NUMBER + ') > 1';

			var conn4 = executeGeneralQuery(updateQuery);
		})();

		var updateAndhraInNames = 'update ' + Queries.DB_TABLE_NAMES + ' set ' + Queries.COL_NAME_STATE + ' = \'Andhra Pradesh\' where ' + Queries.COL_NAME_STATE + ' = \'Andhra Pradesh (New)\'';

		var conn5 = executeGeneralQuery(updateAndhraInNames);

		var updateAndhraInNamesInFirm = 'update ' + Queries.DB_TABLE_FIRMS + ' set ' + Queries.COL_FIRM_STATE + ' = \'Andhra Pradesh\' where ' + Queries.COL_FIRM_STATE + ' = \'Andhra Pradesh (New)\'';

		var conn6 = executeGeneralQuery(updateAndhraInNamesInFirm);

		var updateAndhraInNamesInTxn = 'update ' + Queries.DB_TABLE_TRANSACTIONS + ' set ' + Queries.COL_TXN_PLACE_OF_SUPPLY + ' = \'Andhra Pradesh\' where ' + Queries.COL_TXN_PLACE_OF_SUPPLY + ' = \'Andhra Pradesh (New)\'';

		var conn7 = executeGeneralQuery(updateAndhraInNamesInTxn);

		var updateWrongPlaceOfSupply = 'update ' + Queries.DB_TABLE_TRANSACTIONS + ' set ' + Queries.COL_TXN_PLACE_OF_SUPPLY + ' = \'\' where ' + Queries.COL_TXN_PLACE_OF_SUPPLY + ' = 0';

		var conn8 = executeGeneralQuery(updateWrongPlaceOfSupply);

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	DB_VERSION_22: function DB_VERSION_22() {
		CURRENT_DB_VERSION = 22;

		beginTransactionForDBUpgrade();

		var TaxCodeType = require('./../Constants/TaxCodeConstants.js');

		// alter table transactions to add new column round off value
		var alterTransactionTable1 = 'ALTER TABLE ' + Queries.DB_TABLE_TRANSACTIONS + ' ADD COLUMN ' + Queries.COL_TXN_ROUND_OFF_AMOUNT + ' double default 0';

		var conn1 = executeGeneralQuery(alterTransactionTable1);

		try {
			var _query = 'INSERT INTO ' + Queries.DB_TABLE_TAX_CODE + '(' + Queries.COL_TAX_CODE_NAME + ', ' + Queries.COL_TAX_RATE + ', ' + Queries.COL_TAX_CODE_TYPE + ', ' + Queries.COL_TAX_RATE_TYPE + ')\n            VALUES("Exempted", 0, ' + TaxCodeType.taxRate + ',' + TaxCodeType.Exempted + ')';
			var conn2 = insert(Queries.DB_TABLE_TAX_CODE, _query, Queries.COL_TAX_CODE_ID);
		} catch (err) {
			console.log('exception in adding exempted tax');
		}
		// alter table firms to add estimate prefix and estimate ref no
		var alterFirmTable1 = 'ALTER TABLE ' + Queries.DB_TABLE_FIRMS + ' ADD COLUMN ' + Queries.COL_FIRM_ESTIMATE_PREFIX + ' varchar(10) default \'\'';

		var conn3 = executeGeneralQuery(alterFirmTable1);

		var alterFirmTable2 = 'ALTER TABLE ' + Queries.DB_TABLE_FIRMS + ' ADD COLUMN ' + Queries.COL_FIRM_ESTIMATE_NUMBER + ' integer default 0';

		var conn4 = executeGeneralQuery(alterFirmTable2);

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	DB_VERSION_21: function DB_VERSION_21() {
		CURRENT_DB_VERSION = 21;
		beginTransactionForDBUpgrade();

		// creating logs table
		var CREATE_LOG_TABLE = 'create table ' + Queries.DB_TABLE_LOG + '(' + Queries.COL_LOG_ID + ' integer primary key autoincrement, ' + Queries.COL_LOG_REASON + " varchar(1024) default '', " + Queries.COL_LOG_DETAILS + " varchar(1024) default '') ";

		var conn1 = executeGeneralQuery(CREATE_LOG_TABLE);

		// updating default for sale/purchase return to credit/debit note
		var UPDATE_SETTINGS_TABLE_1 = 'UPDATE ' + Queries.DB_TABLE_SETTINGS + ' SET ' + Queries.COL_SETTING_VALUE + ' = \'Credit Note\'\n        WHERE ' + Queries.COL_SETTING_KEY + ' = \'' + Queries.SETTING_CUSTOM_NAME_FOR_SALE_RETURN + '\'';
		var UPDATE_SETTINGS_TABLE_2 = 'UPDATE ' + Queries.DB_TABLE_SETTINGS + ' SET ' + Queries.COL_SETTING_VALUE + ' = \'Debit Note\'\n        WHERE ' + Queries.COL_SETTING_KEY + ' = \'' + Queries.SETTING_CUSTOM_NAME_FOR_PURCHASE_RETURN + '\'';

		var conn2 = executeGeneralQuery(UPDATE_SETTINGS_TABLE_1);

		var conn3 = executeGeneralQuery(UPDATE_SETTINGS_TABLE_2);

		// add new column in lineitems as a flag to check if total amount was edited
		var queryUpdate = 'ALTER TABLE ' + Queries.DB_TABLE_LINEITEMS + ' ADD COLUMN ' + Queries.COL_LINEITEM_TOTAL_AMOUNT_EDITED + ' integer default 0';

		var conn4 = executeGeneralQuery(queryUpdate);

		// adding place of supply column in transaction table
		var alterTransactionTable1 = 'ALTER TABLE ' + Queries.DB_TABLE_TRANSACTIONS + ' ADD COLUMN ' + Queries.COL_TXN_PLACE_OF_SUPPLY + ' varchar(1000) default \'\'';

		var conn5 = executeGeneralQuery(alterTransactionTable1);

		// Update reverse charge setting value if gst is enabled
		var gstSettingValue = 'SELECT ' + Queries.COL_SETTING_VALUE + ' FROM ' + Queries.DB_TABLE_SETTINGS + ' where ' + Queries.COL_SETTING_KEY + ' = \'' + Queries.SETTING_GST_ENABLED + '\'';
		mainobj = [];
		var conn6 = libsqlite3.sqlite3_exec(dbHandleForUpdate, gstSettingValue, callbackForFetchAllNameRecords, null, null);
		if (!conn6) {
			if (mainobj && mainobj.length > 0 && mainobj[0][Queries.COL_SETTING_VALUE] == 1) {
				var reverseChargeUpdateQuery = 'UPDATE ' + Queries.DB_TABLE_SETTINGS + ' SET ' + Queries.COL_SETTING_VALUE + ' = \'1\' WHERE ' + Queries.COL_SETTING_KEY + ' = \'' + Queries.SETTING_ENABLE_PLACE_OF_SUPPLY + '\'';

				var conn7 = executeGeneralQuery(reverseChargeUpdateQuery);
			}
		}

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	DB_VERSION_20: function DB_VERSION_20() {
		CURRENT_DB_VERSION = 20;
		beginTransactionForDBUpgrade();

		// adding txn_display_name in transaction table
		var alterTransactionTable = 'ALTER TABLE ' + Queries.DB_TABLE_TRANSACTIONS + ' ADD COLUMN ' + Queries.COL_TXN_DISPLAY_NAME + ' varchar(256) default \'\'';

		var conn1 = executeGeneralQuery(alterTransactionTable);

		// adding 6 additional columns in line item tables
		var extraColumns = [{ 'name': Queries.COL_LINEITEM_MRP, 'type': 'double', 'default': 'null' }, { 'name': Queries.COL_LINEITEM_BATCH_NUMBER, 'type': 'varchar(30)', 'default': "''" }, { 'name': Queries.COL_LINEITEM_EXPIRY_DATE, 'type': 'datetime', 'default': 'null' }, { 'name': Queries.COL_LINEITEM_MANUFACTURING_DATE, 'type': 'datetime', 'default': 'null' }, { 'name': Queries.COL_LINEITEM_SERIAL_NUMBER, 'type': 'varchar(30)', 'default': "''" }, { 'name': Queries.COL_LINEITEM_COUNT, 'type': 'double', 'default': 'null' }, { 'name': Queries.COL_LINEITEM_DESCRIPTION, 'type': 'varchar(1024)', 'default': "''" }];

		$.each(extraColumns, function (key, value) {
			var alterQuery = 'ALTER TABLE ' + Queries.DB_TABLE_LINEITEMS + ' ADD COLUMN ' + value['name'] + ' ' + value['type'] + ' default ' + value['default'];

			var conn2 = executeGeneralQuery(alterQuery);
		});
		//
		// adding additonal CESS in items table
		var alterQuery1 = 'ALTER TABLE ' + Queries.DB_TABLE_ITEMS + ' ADD COLUMN ' + Queries.COL_ITEM_ADDITIONAL_CESS_PER_UNIT + ' double default null';

		var conn3 = executeGeneralQuery(alterQuery1);

		// additional CESS in lineitems table
		var alterQuery2 = 'ALTER TABLE ' + Queries.DB_TABLE_LINEITEMS + ' ADD COLUMN ' + Queries.COL_LINEITEM_ADDITIONAL_CESS + ' double default null';

		var conn4 = executeGeneralQuery(alterQuery2);

		// adding reverse charge in transaction table
		var alterTransactionTable1 = 'ALTER TABLE ' + Queries.DB_TABLE_TRANSACTIONS + ' ADD COLUMN ' + Queries.COL_TXN_REVERSE_CHARGE + ' integer default 0';

		var conn5 = executeGeneralQuery(alterTransactionTable1);

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	DB_VERSION_19: function DB_VERSION_19() {
		CURRENT_DB_VERSION = 19;
		beginTransactionForDBUpgrade();

		// DB_UPDATES.setDefaultSettings();
		var CustomFieldsType = require('./../Constants/CustomFieldsType.js');
		var TaxCodeType = require('./../Constants/TaxCodeConstants.js');

		// adding shipping address in patry table
		var alterPartyTable = 'ALTER TABLE ' + Queries.DB_TABLE_NAMES + ' ADD COLUMN ' + Queries.COL_NAME_SHIPPING_ADDRESS + ' VARCHAR(2000) DEFAULT \'\'';

		var conn1 = executeGeneralQuery(alterPartyTable);

		// adding bank info in firm table
		var alterFirmTable1 = 'ALTER TABLE ' + Queries.DB_TABLE_FIRMS + ' ADD COLUMN ' + Queries.COL_FIRM_BANK_NAME + ' VARCHAR(32) DEFAULT \'\'';
		var alterFirmTable2 = 'ALTER TABLE ' + Queries.DB_TABLE_FIRMS + ' ADD COLUMN ' + Queries.COL_FIRM_BANK_ACCOUNT_NUMBER + ' VARCHAR(32) DEFAULT \'\'';
		var alterFirmTable3 = 'ALTER TABLE ' + Queries.DB_TABLE_FIRMS + ' ADD COLUMN ' + Queries.COL_FIRM_BANK_IFSC_CODE + ' VARCHAR(32) DEFAULT \'\'';

		var conn2 = executeGeneralQuery(alterFirmTable1);

		var conn3 = executeGeneralQuery(alterFirmTable2);

		var conn4 = executeGeneralQuery(alterFirmTable3);

		// creating custom fields table
		var createCustomFieldsTable = 'CREATE TABLE ' + Queries.DB_TABLE_CUSTOM_FIELDS + '(\n         ' + Queries.COL_CUSTOM_FIELDS_ID + ' INTEGER PRIMARY KEY AUTOINCREMENT,\n         ' + Queries.COL_CUSTOM_FIELDS_DISPLAY_NAME + ' VARCHAR(32) DEFAULT \'\',\n         ' + Queries.COL_CUSTOM_FIELDS_TYPE + ' INTEGER DEFAULT NULL,\n         ' + Queries.COL_CUSTOM_FIELDS_VISIBILITY + ' INTEGER DEFAULT 0)';

		var conn5 = executeGeneralQuery(createCustomFieldsTable);

		// insert default values in custom fields table
		var customFieldList = [{ 'name': 'Transport Name', 'type': CustomFieldsType.delivery, 'visibility': 0 }, { 'name': 'Vehicle Number', 'type': CustomFieldsType.delivery, 'visibility': 0 }, { 'name': 'Delivery Date', 'type': CustomFieldsType.delivery, 'visibility': 0 }, { 'name': 'Delivery Location', 'type': CustomFieldsType.delivery, 'visibility': 0 }, { 'name': 'Field 5', 'type': CustomFieldsType.delivery, 'visibility': 0 }, { 'name': 'Field 6', 'type': CustomFieldsType.delivery, 'visibility': 0 }];

		$.each(customFieldList, function (index, obj) {
			var query = 'INSERT INTO ' + Queries.DB_TABLE_CUSTOM_FIELDS + '(\n            ' + Queries.COL_CUSTOM_FIELDS_DISPLAY_NAME + ', ' + Queries.COL_CUSTOM_FIELDS_TYPE + ', ' + Queries.COL_CUSTOM_FIELDS_VISIBILITY + '\n            )VALUES (\'' + obj['name'] + '\', ' + obj['type'] + ', ' + obj['visibility'] + ')';

			var conn6 = insert(Queries.DB_TABLE_CUSTOM_FIELDS, query, Queries.COL_CUSTOM_FIELDS_ID);
		});

		// adding txn_custom_fields in transaction table
		var alterTransactionTable3 = 'ALTER TABLE ' + Queries.DB_TABLE_TRANSACTIONS + ' ADD COLUMN ' + Queries.COL_TXN_CUSTOM_FIELDS + ' text default \'\'';

		var conn7 = executeGeneralQuery(alterTransactionTable3);

		// StateCode Migration
		var updateChhattisgarh = 'update ' + Queries.DB_TABLE_NAMES + ' set ' + Queries.COL_NAME_STATE + ' = ' + "'Chattisgarh' where " + Queries.COL_NAME_STATE + ' = ' + "'Chhattisgarh'";
		var updateLakshdweep = 'update ' + Queries.DB_TABLE_NAMES + ' set ' + Queries.COL_NAME_STATE + ' = ' + "'Lakshadweep Islands' where " + Queries.COL_NAME_STATE + ' = ' + "'Lakshdweep'";
		var updateOrissa = 'update ' + Queries.DB_TABLE_NAMES + ' set ' + Queries.COL_NAME_STATE + ' = ' + "'Odisha' where " + Queries.COL_NAME_STATE + ' = ' + "'Orissa'";
		var updateUttranchal = 'update ' + Queries.DB_TABLE_NAMES + ' set ' + Queries.COL_NAME_STATE + ' = ' + "'Uttarakhand' where " + Queries.COL_NAME_STATE + ' = ' + "'Uttranchal'";
		var updateSelectYourState = 'update ' + Queries.DB_TABLE_NAMES + ' set ' + Queries.COL_NAME_STATE + ' = ' + "'' where " + Queries.COL_NAME_STATE + ' = ' + "'Select Your State'";

		var conn8 = executeGeneralQuery(updateChhattisgarh);

		var conn9 = executeGeneralQuery(updateLakshdweep);

		var conn10 = executeGeneralQuery(updateOrissa);

		var conn11 = executeGeneralQuery(updateUttranchal);

		var conn12 = executeGeneralQuery(updateSelectYourState);

		updateChhattisgarh = 'update ' + Queries.DB_TABLE_FIRMS + ' set ' + Queries.COL_FIRM_STATE + ' = ' + "'Chattisgarh' where " + Queries.COL_FIRM_STATE + ' = ' + "'Chhattisgarh'";
		updateLakshdweep = 'update ' + Queries.DB_TABLE_FIRMS + ' set ' + Queries.COL_FIRM_STATE + ' = ' + "'Lakshadweep Islands' where " + Queries.COL_FIRM_STATE + ' = ' + "'Lakshdweep'";
		updateOrissa = 'update ' + Queries.DB_TABLE_FIRMS + ' set ' + Queries.COL_FIRM_STATE + ' = ' + "'Odisha' where " + Queries.COL_FIRM_STATE + ' = ' + "'Orissa'";
		updateUttranchal = 'update ' + Queries.DB_TABLE_FIRMS + ' set ' + Queries.COL_FIRM_STATE + ' = ' + "'Uttarakhand' where " + Queries.COL_FIRM_STATE + ' = ' + "'Uttranchal'";
		updateUttranchal = 'update ' + Queries.DB_TABLE_FIRMS + ' set ' + Queries.COL_FIRM_STATE + ' = ' + "'' where " + Queries.COL_FIRM_STATE + ' = ' + "'Select Your State'";

		var conn13 = executeGeneralQuery(updateChhattisgarh);

		var conn14 = executeGeneralQuery(updateLakshdweep);

		var conn15 = executeGeneralQuery(updateOrissa);

		var conn16 = executeGeneralQuery(updateUttranchal);

		var conn17 = executeGeneralQuery(updateSelectYourState);

		// insert new TaxCode (0.25%)
		var rateList = [{ 'name': 'CGST@0.125%', 'val': '0.125', 'rateType': TaxCodeType['CGST'], 'codeType': TaxCodeType['taxRate'] }, { 'name': 'SGST@0.125%', 'val': '0.125', 'rateType': TaxCodeType['SGST'], 'codeType': TaxCodeType['taxRate'] }, { 'name': 'GST@0.25%', 'val': '0.25', 'rateType': TaxCodeType['GST'], 'codeType': TaxCodeType['taxGroup'] }, { 'name': 'IGST@0.25%', 'val': '0.25', 'rateType': TaxCodeType['IGST'], 'codeType': TaxCodeType['taxRate'] }];

		var curGroupCodeIds = [];
		try {
			$.each(rateList, function (key, value) {
				var query = 'INSERT INTO ' + Queries.DB_TABLE_TAX_CODE + '(' + Queries.COL_TAX_CODE_NAME + ', ' + Queries.COL_TAX_RATE + ', ' + Queries.COL_TAX_CODE_TYPE + ', ' + Queries.COL_TAX_RATE_TYPE + ')\n            VALUES("' + value['name'] + '", ' + value['val'] + ', ' + value['codeType'] + ',' + value['rateType'] + ')';
				var conn18 = insert(Queries.DB_TABLE_TAX_CODE, query, Queries.COL_TAX_CODE_ID);

				if (!conn18) {
					if (value['rateType'] == TaxCodeType['CGST'] || value['rateType'] == TaxCodeType['SGST']) {
						curGroupCodeIds.push(retVal);
					} else if (value['rateType'] == TaxCodeType['GST'] && retVal > 0) {
						$.each(curGroupCodeIds, function (keyNew, valueNew) {
							var queryInsertMapping = 'INSERT INTO ' + Queries.DB_TABLE_TAX_MAPPING + '(' + Queries.COL_TAX_MAPPING_GROUP_ID + ', ' + Queries.COL_TAX_MAPPING_CODE_ID + ' )\n                    VALUES (' + retVal + ', ' + valueNew + ')';

							var conn19 = insert(Queries.DB_TABLE_TAX_MAPPING, queryInsertMapping, Queries.COL_TAX_MAPPING_ID);
						});
						curGroupCodeIds = [];
					}
				}
			});
		} catch (err) {
			console.log('exception in adding 0.25% tax');
		}

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	DB_VERSION_18: function DB_VERSION_18() {
		CURRENT_DB_VERSION = 18;
		beginTransactionForDBUpgrade();

		var TaxCodeType = require('./../Constants/TaxCodeConstants.js');

		// function to add 3% tax
		var rateList = [{ 'name': 'CGST@1.5%', 'val': '1.5', 'rateType': TaxCodeType['CGST'], 'codeType': TaxCodeType['taxRate'] }, { 'name': 'SGST@1.5%', 'val': '1.5', 'rateType': TaxCodeType['SGST'], 'codeType': TaxCodeType['taxRate'] }, { 'name': 'GST@3%', 'val': '3', 'rateType': TaxCodeType['GST'], 'codeType': TaxCodeType['taxGroup'] }, { 'name': 'IGST@3%', 'val': '3', 'rateType': TaxCodeType['IGST'], 'codeType': TaxCodeType['taxRate'] }];

		var curGroupCodeIds = [];
		try {
			$.each(rateList, function (key, value) {
				var query = 'INSERT INTO ' + Queries.DB_TABLE_TAX_CODE + '(' + Queries.COL_TAX_CODE_NAME + ', ' + Queries.COL_TAX_RATE + ', ' + Queries.COL_TAX_CODE_TYPE + ', ' + Queries.COL_TAX_RATE_TYPE + ')\n            VALUES("' + value['name'] + '", ' + value['val'] + ', ' + value['codeType'] + ',' + value['rateType'] + ')';
				var conn = insert(Queries.DB_TABLE_TAX_CODE, query, Queries.COL_TAX_CODE_ID);

				if (!conn) {
					if (value['rateType'] == TaxCodeType['CGST'] || value['rateType'] == TaxCodeType['SGST']) {
						curGroupCodeIds.push(retVal);
					} else if (value['rateType'] == TaxCodeType['GST'] && retVal > 0) {
						$.each(curGroupCodeIds, function (keyNew, valueNew) {
							var queryInsertMapping = 'INSERT INTO ' + Queries.DB_TABLE_TAX_MAPPING + '(' + Queries.COL_TAX_MAPPING_GROUP_ID + ', ' + Queries.COL_TAX_MAPPING_CODE_ID + ' )\n                    VALUES (' + retVal + ', ' + valueNew + ')';

							var conn1 = insert(Queries.DB_TABLE_TAX_MAPPING, queryInsertMapping, Queries.COL_TAX_MAPPING_ID);
						});
						curGroupCodeIds = [];
					}
				}
			});
		} catch (err) {
			console.log('exception in adding 3%');
		}

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	},

	DB_VERSION_17: function DB_VERSION_17() {
		CURRENT_DB_VERSION = 17;
		beginTransactionForDBUpgrade();

		var TaxCodeType = require('./../Constants/TaxCodeConstants.js');

		// create tax code table
		var query1 = CREATE_TAX_CODE_TABLE = 'create table ' + Queries.DB_TABLE_TAX_CODE + ' (\n        ' + Queries.COL_TAX_CODE_ID + ' integer primary key autoincrement,\n        ' + Queries.COL_TAX_CODE_NAME + ' varchar(32) unique,\n        ' + Queries.COL_TAX_RATE + ' double,\n        ' + Queries.COL_TAX_CODE_TYPE + ' integer default ' + TaxCodeType['OTHER'] + ',\n        ' + Queries.COL_TAX_RATE_TYPE + ' integer default null,\n        ' + Queries.COL_TAX_CODE_DATE_CREATED + ' datetime default CURRENT_TIMESTAMP,\n        ' + Queries.COL_TAX_CODE_DATE_MODIFIED + ' datetime default CURRENT_TIMESTAMP)';
		try {
			var conn = libsqlite3.sqlite3_exec(dbHandleForUpdate, query1, null, null, null);
			if (!conn) {
				addUpgradeQueries(query1);
			}
		} catch (err) {
			console.log('Query ==>' + query + ' ErrorLog ==> ' + err);
			// logger.error('Query ==>',query, 'Status Code ==>', conn , 'ErrorLog ==> ',err);
		}

		// create tax mapping table
		var query2 = 'create table ' + Queries.DB_TABLE_TAX_MAPPING + ' (\n        ' + Queries.COL_TAX_MAPPING_ID + ' integer primary key autoincrement,\n        ' + Queries.COL_TAX_MAPPING_GROUP_ID + ' integer references ' + Queries.DB_TABLE_TAX_CODE + ' ( ' + Queries.COL_TAX_CODE_ID + ' ),\n        ' + Queries.COL_TAX_MAPPING_CODE_ID + ' integer references ' + Queries.DB_TABLE_TAX_CODE + ' (' + Queries.COL_TAX_CODE_ID + '),\n        ' + Queries.COL_TAX_MAPPING_DATE_CREATED + ' datetime default CURRENT_TIMESTAMP,\n        ' + Queries.COL_TAX_MAPPING_DATE_MODIFIED + ' datetime default CURRENT_TIMESTAMP)';
		try {
			var conn1 = executeGeneralQuery(query2);
		} catch (err) {
			console.log('Query ==>' + query + ' ErrorLog ==> ' + err);
		}

		// insert default values in tax code table
		var codeRate = {
			'CGST@0%': { 'val': '0', 'type': TaxCodeType['CGST'] },
			'CGST@2.5%': { 'val': '2.5', 'type': TaxCodeType['CGST'] },
			'CGST@6%': { 'val': '6', 'type': TaxCodeType['CGST'] },
			'CGST@9%': { 'val': '9', 'type': TaxCodeType['CGST'] },
			'CGST@14%': { 'val': '14', 'type': TaxCodeType['CGST'] },
			'SGST@0%': { 'val': '0', 'type': TaxCodeType['SGST'] },
			'SGST@2.5%': { 'val': '2.5', 'type': TaxCodeType['SGST'] },
			'SGST@6%': { 'val': '6', 'type': TaxCodeType['SGST'] },
			'SGST@9%': { 'val': '9', 'type': TaxCodeType['SGST'] },
			'SGST@14%': { 'val': '14', 'type': TaxCodeType['SGST'] },
			'IGST@0%': { 'val': '0', 'type': TaxCodeType['IGST'] },
			'IGST@5%': { 'val': '5', 'type': TaxCodeType['IGST'] },
			'IGST@12%': { 'val': '12', 'type': TaxCodeType['IGST'] },
			'IGST@18%': { 'val': '18', 'type': TaxCodeType['IGST'] },
			'IGST@28%': { 'val': '28', 'type': TaxCodeType['IGST'] },
			'GST@0%': { 'val': '0', 'type': null },
			'GST@5%': { 'val': '5', 'type': null },
			'GST@12%': { 'val': '12', 'type': null },
			'GST@18%': { 'val': '18', 'type': null },
			'GST@28%': { 'val': '28', 'type': null }
		};
		var taxCodeIndex = 1;
		$.each(codeRate, function (key, value) {
			var queryForInsert = 'INSERT INTO ' + Queries.DB_TABLE_TAX_CODE + ' VALUES(' + taxCodeIndex + ', "' + key + '", ' + value['val'] + ', ' + (taxCodeIndex < 16 ? 0 : 1) + ',' + value['type'] + ',  CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)';
			try {
				var conn2 = insert(Queries.DB_TABLE_TAX_CODE, queryForInsert, Queries.COL_TAX_CODE_ID);
			} catch (err) {}
			taxCodeIndex++;
		});

		var taxMapping = {
			'1': '16',
			'6': '16',
			'2': '17',
			'7': '17',
			'3': '18',
			'8': '18',
			'4': '19',
			'9': '19',
			'5': '20',
			'10': '20'
		};

		var taxMappingIndex = 1;
		$.each(taxMapping, function (key, value) {
			var queryForInsert = 'INSERT INTO ' + Queries.DB_TABLE_TAX_MAPPING + ' values(' + taxMappingIndex + ', ' + value + ', ' + key + ', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)';
			try {
				var conn3 = insert(Queries.DB_TABLE_TAX_MAPPING, queryForInsert, Queries.COL_TAX_MAPPING_ID);
			} catch (err) {
				console.log(err);
			}
			taxMappingIndex++;
		});

		function migrateToVersion17() {
			addItemTaxColumn();
			addTxnTaxColumn();
			addLineItemTaxColumn();
			migrateTaxPercentInLineItems();
			migrateTaxPercentInTransaction();
		};

		function checkIfTaxCodeExists(taxRate) {
			var query = 'select * from ' + Queries.DB_TABLE_TAX_CODE + ' where ' + Queries.COL_TAX_RATE + ' = ' + taxRate + ' and ' + Queries.COL_TAX_CODE_ID + ' > 20';
			try {
				mainobj = [];
				var _conn = libsqlite3.sqlite3_exec(dbHandleForUpdate, query, callbackForFetchAllNameRecords, null, null);
				if (!_conn) {
					if (mainobj.length > 0) {
						return mainobj[0][Queries.COL_TAX_CODE_ID];
					}
				}
			} catch (err) {
				console.log(err);
			}
			return 0;
		}

		function migrateTaxPercentInLineItems() {
			var fetchTaxAmount = 'select * from ' + Queries.DB_TABLE_LINEITEMS;
			try {
				mainobj = [];
				var _conn2 = libsqlite3.sqlite3_exec(dbHandleForUpdate, fetchTaxAmount, callbackForFetchAllNameRecords, null, null);
				if (!_conn2) {
					if (mainobj.length > 0) {
						$.each(mainobj, function (key, cursor) {
							var lineItemId = Number(cursor[Queries.COL_LINEITEM_ID]);
							var quantity = Number(cursor[Queries.COL_LINEITEM_QUANTITY]);
							var unitPrice = Number(cursor[Queries.COL_LINEITEM_UNITPRICE]);
							var taxAmount = Number(cursor[Queries.COL_LINEITEM_TAX_AMOUNT]);
							var discountAmount = Number(cursor[Queries.COL_LINEITEM_DISCOUNT_AMOUNT]);
							var taxCodeId;

							if (quantity != 0 && unitPrice != 0 && taxAmount != 0) {
								var taxPercent = taxAmount * 100 / (unitPrice * quantity - discountAmount);
								taxPercent = Number(taxPercent).toFixed(2);

								if ((taxCodeId = checkIfTaxCodeExists(taxPercent)) == 0) {
									var queryInsert = 'INSERT INTO ' + Queries.DB_TABLE_TAX_CODE + ' (' + Queries.COL_TAX_CODE_NAME + ',' + Queries.COL_TAX_RATE + ', ' + Queries.COL_TAX_CODE_TYPE + ' , ' + Queries.COL_TAX_RATE_TYPE + ')\n                                     VALUES ("tax@' + taxPercent + '%", ' + taxPercent + ',' + TaxCodeType.taxRate + ', ' + TaxCodeType['OTHER'] + ')';

									var querySelect = 'SELECT MAX(' + Queries.COL_TAX_CODE_ID + ') as count FROM ' + Queries.DB_TABLE_TAX_CODE;
									try {
										var conn2 = insert(Queries.DB_TABLE_TAX_CODE, queryInsert, Queries.COL_TAX_CODE_ID);

										mainobj = [];
										var conn3 = libsqlite3.sqlite3_exec(dbHandleForUpdate, querySelect, callbackForFetchAllNameRecords, null, null);
										if (!conn3 && mainobj.length > 0) {
											taxCodeId = mainobj[0]['count'];
										}
									} catch (err) {
										console.log(err);
										// logger.error('Query ==>',query, 'Status Code ==>', conn , 'ErrorLog ==> ',err);
									}
								}
								var queryUpdate = 'UPDATE ' + Queries.DB_TABLE_LINEITEMS + ' SET ' + Queries.COL_LINEITEM_TAX_ID + ' = ' + taxCodeId + '\n                                where ' + Queries.COL_LINEITEM_ID + ' = ' + lineItemId;
								try {
									var conn4 = executeGeneralQuery(queryUpdate);
								} catch (err) {
									console.log('Query ==>' + query);
								}
							}
						});
					}
				}
			} catch (err) {
				console.log(err);
			}
		}

		function migrateTaxPercentInTransaction() {
			try {
				var fetchTaxAmount = 'select * from ' + Queries.DB_TABLE_TRANSACTIONS;
				mainobj = [];
				var _conn3 = libsqlite3.sqlite3_exec(dbHandleForUpdate, fetchTaxAmount, callbackForFetchAllNameRecords, null, null);
				if (!_conn3 && mainobj.length > 0) {
					$.each(mainobj, function (key, cursor) {
						var txnId = cursor[Queries.COL_TXN_ID];
						var txnTaxPercent = cursor[Queries.COL_TXN_TAX_PERCENT];
						var taxCodeId;

						if (txnTaxPercent != 0) {
							txnTaxPercent = Number(txnTaxPercent).toFixed(2);

							if ((taxCodeId = checkIfTaxCodeExists(txnTaxPercent)) == 0) {
								var queryInsert = 'INSERT INTO ' + Queries.DB_TABLE_TAX_CODE + ' (' + Queries.COL_TAX_CODE_NAME + ',' + Queries.COL_TAX_RATE + ' ,' + Queries.COL_TAX_CODE_TYPE + ', ' + Queries.COL_TAX_RATE_TYPE + ')\n                                    VALUES ("tax@' + txnTaxPercent + '%",' + txnTaxPercent + ' , ' + TaxCodeType.taxRate + ', ' + TaxCodeType['OTHER'] + ')';
								var querySelect = 'SELECT MAX(' + Queries.COL_TAX_CODE_ID + ') as count FROM ' + Queries.DB_TABLE_TAX_CODE;

								var conn2 = insert(Queries.DB_TABLE_TAX_CODE, queryInsert, Queries.COL_TAX_CODE_ID);

								mainobj = [];
								var conn = libsqlite3.sqlite3_exec(dbHandleForUpdate, querySelect, callbackForFetchAllNameRecords, null, null);
								if (!conn && mainobj.length > 0) {
									taxCodeId = mainobj[0]['count'];
								}
							}
							var queryUpdate = 'UPDATE ' + Queries.DB_TABLE_TRANSACTIONS + ' SET ' + Queries.COL_TXN_TAX_ID + ' = ' + taxCodeId + '\n                            where ' + Queries.COL_TXN_ID + ' = ' + txnId;

							var conn4 = executeGeneralQuery(queryUpdate);
						}
					});
				}
			} catch (err) {
				console.log(err);
			}
		}

		function addItemTaxColumn() {
			var queryAlter = 'ALTER TABLE ' + Queries.DB_TABLE_ITEMS + ' ADD COLUMN ' + Queries.COL_ITEM_TAX_ID + ' INTEGER DEFAULT NULL REFERENCES ' + Queries.DB_TABLE_TAX_CODE + '\n            (' + Queries.COL_TAX_CODE_ID + ')';
			var queryAlter2 = 'ALTER TABLE ' + Queries.DB_TABLE_ITEMS + ' ADD COLUMN ' + Queries.COL_ITEM_TAX_TYPE_SALE + ' INTEGER DEFAULT ' + 2;
			try {
				var _conn4 = executeGeneralQuery(queryAlter2);
			} catch (err) {
				console.log('Query ==>' + query);
			}
			try {
				var conn2 = executeGeneralQuery(queryAlter);
			} catch (err) {
				console.log('Query ==>' + query);
			}
		}

		function addLineItemTaxColumn() {
			var queryAlter = 'ALTER TABLE ' + Queries.DB_TABLE_LINEITEMS + ' ADD COLUMN ' + Queries.COL_LINEITEM_TAX_ID + ' INTEGER DEFAULT NULL\n            REFERENCES ' + Queries.DB_TABLE_TAX_CODE + '(' + Queries.COL_TAX_CODE_ID + ')';
			try {
				var _conn5 = executeGeneralQuery(queryAlter);
			} catch (err) {
				console.log('Query ==>' + query);
			}
		}

		function addTxnTaxColumn() {
			var queryAlter = 'ALTER TABLE ' + Queries.DB_TABLE_TRANSACTIONS + ' ADD COLUMN ' + Queries.COL_TXN_TAX_ID + ' INTEGER DEFAULT NULL\n            REFERENCES ' + Queries.DB_TABLE_TAX_CODE + '(' + Queries.COL_TAX_CODE_ID + ')';
			try {
				var _conn6 = executeGeneralQuery(queryAlter);
			} catch (err) {
				console.log('Query ==>' + query);
			}
		}

		migrateToVersion17();

		DB_UPDATES.setDefaultSettings();
		setDbToCurrentVersion(CURRENT_DB_VERSION);
		var status = commitTransactionForDBUpgrade();
		if (!status) {
			endTransactionForDBUpgrade();
			return false;
		}
		return true;
	}
};

var callbackForInsertRow = ffi.Callback('int32', ['pointer', 'int32', 'pointer', 'pointer'], function (tmp, cols, argv, colv) {
	rowPresent = 1;
});

module.exports = {
	dbHandleForUpdate: dbHandleForUpdate,
	DB_UPDATES: DB_UPDATES

};