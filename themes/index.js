Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createMuiTheme = require('@material-ui/core/styles/createMuiTheme');

var _createMuiTheme2 = _interopRequireDefault(_createMuiTheme);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (theme) {
	switch (theme) {
		case 'blue':
			break;
		default:
			return (0, _createMuiTheme2.default)({
				palette: {
					primary: {
						main: '#1789FC'
					},
					secondary: {
						main: '#0044ff'
					}
				},
				typography: { useNextVariants: true }
			});
	}
};