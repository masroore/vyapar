var _create = require('babel-runtime/core-js/object/create');

var _create2 = _interopRequireDefault(_create);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var electron = require('electron');
var ipcMain = electron.ipcMain,
    Menu = electron.Menu,
    dialog = electron.dialog,
    Tray = electron.Tray;

var app = electron.app;
var mainWindow = null;
var eventsLaunchedOnScreenFocus = false;
var localStorageKeyValues = (0, _create2.default)(null);
var autoUpdateEventListenersAdded = false;
var manualUpdateCheck = false;
var referralCode = void 0,
    campaignId = void 0;
var gotTheLock = app.requestSingleInstanceLock();

app.on('second-instance', function (event, commandLine, workingDirectory) {
	// Someone tried to run a second instance, we should focus our window.
	if (mainWindow && commandLine && commandLine[1] !== '--squirrel-updated' && commandLine[1] !== '--squirrel-obsolete') {
		if (!mainWindow.isMaximized()) mainWindow.maximize();
		if (!mainWindow.isVisible()) mainWindow.show();
		if (!mainWindow.isFocused()) mainWindow.focus();
		if (!eventsLaunchedOnScreenFocus) {
			appLaunchEvents(true);
			eventsLaunchedOnScreenFocus = true;
		}
	}
});

function appLaunchEvents() {
	var checkForPassCode = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
	var shouldCheckForUpdate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

	shouldCheckForUpdate && checkForUpdate();
	mainWindow.webContents.send('launchEventsOnScreenFocus', checkForPassCode);
}

function checkForUpdate(manual) {
	try {
		var showDownloadingUpdateMessage = function showDownloadingUpdateMessage() {
			mainWindow.webContents.send('ShowUpdatingMessage');
		};

		manualUpdateCheck = manual;
		var autoUpdater = electron.autoUpdater;

		var isDev = require('electron-is-dev');

		autoUpdater.setFeedURL('https://s3-us-west-2.amazonaws.com/vyapardesktop/desktop/');
		if (!autoUpdateEventListenersAdded) {
			autoUpdateEventListenersAdded = true;
			autoUpdater.on('error', function (error) {
				try {
					var logger = require('./Utilities/logger');
					logger.error('error occured in app update');
					logger.error(error);
				} catch (e) {}
			});

			autoUpdater.on('checking-for-update', function () {});

			autoUpdater.on('update-available', function () {
				try {
					if (eventsLaunchedOnScreenFocus) {
						dialog.showMessageBox(mainWindow, {
							type: 'info',
							buttons: ['Ok'],
							title: 'Vyapar',
							message: 'A new version of vyapar app is getting downloaded. Please do not turn off the App till its completed. Once completed you will get notification. You can continue with your normal work.'
						});

						showDownloadingUpdateMessage();
					}
					autoUpdater.once('update-downloaded', function (event, releaseName) {
						// # confirm install or not to user
						try {
							if (eventsLaunchedOnScreenFocus) {
								var index = dialog.showMessageBox(mainWindow, {
									type: 'info',
									buttons: ['Restart', 'Later'],
									title: 'Vyapar',
									message: 'The new version has been downloaded. Please restart the application to apply the updates.',
									detail: releaseName
								});
								if (index === 1) {
									app.quitFromTray = true;
									return;
								}

								// # restart app, then update will be applied
								app.quitFromTray = true;
								autoUpdater.quitAndInstall();
							} else {
								app.quitFromTray = true;
								app.quit();
							}
						} catch (e) {}
					});
				} catch (e) {}
			});

			autoUpdater.on('update-not-available', function () {
				try {
					if (!manualUpdateCheck) {} else {
						dialog.showMessageBox({
							type: 'info',
							buttons: ['Ok'],
							title: 'Vyapar',
							message: "You've got the latest version of Vyapar. Thanks for staying updated."
						});
					}
				} catch (e) {}
			});
		}

		if (!isDev) {
			autoUpdater.checkForUpdates();
		}
	} catch (e) {}
}

if (!gotTheLock) {
	app.quit();
} else {
	var getLocalStorageValueAsync = function getLocalStorageValueAsync(localStorageKey) {
		return mainWindow && mainWindow.webContents.executeJavaScript('localStorage.getItem(\'' + localStorageKey + '\')').then(function (value) {
			localStorageKeyValues[localStorageKey] = value;
		});
	};

	var setLocalStorageValue = function setLocalStorageValue(key, value) {
		mainWindow && mainWindow.webContents.executeJavaScript('try { localStorage.setItem(\'' + key + '\', \'' + value + '\'); } catch(err) {console.log(err)}');
		localStorageKeyValues[key] = String(value);
	};

	var showLoader = function showLoader() {
		mainWindow.webContents.send('ShowLoader');
	};

	var checkingForUpdate = function checkingForUpdate() {
		mainWindow.webContents.send('CheckingForUpdate');
	};

	var hideLoader = function hideLoader() {
		mainWindow.webContents.send('HideLoader');
	};

	// Code to execute if no instance is in running state.
	var BrowserWindow = electron.BrowserWindow;

	var sq = require('electron-squirrel-startup'); // Don't remove this line. We know that sq variable is not getting used. But this require gets used internally.
	var shortcut = require('electron-localshortcut');
	var path = require('path');
	var mainPage = 'Index.html';
	var AutoLaunch = require('auto-launch');
	app.hasUserPassCodeEntered = false;
	global.notificationClicked = false;
	var isOnline = require('is-online');
	var MyAnalytics = require('./Utilities/analyticsHelper.js');
	app.quitFromTray = false;
	var autoLaunch = void 0;

	var createWindow = function createWindow() {
		// var path = require('path');
		mainWindow = new BrowserWindow({
			name: 'Vyapar App',
			show: false,
			webPreferences: {
				webSecurity: false
			},
			frame: false
		});
		// var url = './UIComponent/'+mainPage;
		var isAppLaunched = true;

		mainWindow.webContents.on('did-finish-load', function () {
			try {
				if (isAppLaunched) {
					getLocalStorageValueAsync('confirmBeforeQuit');
				}
				isAppLaunched = false;
			} catch (ex) {
				var logger = require('./Utilities/logger');
				logger.error(ex);
			}
		});

		mainWindow.webContents.executeJavaScript('localStorage.getItem("autoLaunch")', true).then(function (result) {
			autoLaunch && autoLaunch.disable();
			if (result != '0') {
				autoLaunch && autoLaunch.enable();
			} // Will be the JSON object from the fetch call
		});

		if (process.argv[1] === '--squirrel-firstrun' && (referralCode || campaignId)) {
			mainWindow && mainWindow.webContents.executeJavaScript('try { \n\t\t\t\tlet existingReferralCode = localStorage.getItem(\'referral_code\');\n\t\t\t\tlet existingCampaignId = localStorage.getItem(\'campaignId\');\n\t\t\t\tif(!existingReferralCode) {\n\t\t\t\t\tlocalStorage.setItem(\'referral_code\', \'' + referralCode + '\');\n\t\t\t\t}\n\t\t\t\tif(!existingCampaignId) {\n\t\t\t\t\tlocalStorage.setItem(\'campaignId\', \'' + campaignId + '\');\n\t\t\t\t}\n\t\t\t\t} catch(err) {console.log(err)}');
		}

		mainWindow.loadURL(__dirname + '/UIComponent/' + mainPage);

		mainWindow.on('closed', function () {
			if (app.quitFromTray) {
				mainWindow = null;
			}
		});
		mainWindow.on('close', function (event) {
			var quitOrHideApp = function quitOrHideApp() {
				if (app.quitFromTray) {
					destroyDBWindow(function () {
						mainWindow = null;
						app.quit();
					});
				} else {
					eventsLaunchedOnScreenFocus = false;
					event.preventDefault();
					app.hasUserPassCodeEntered = false;
					mainWindow.hide();
				}
			};
			if (mainWindow) {
				var confirmBeforeQuit = localStorageKeyValues['confirmBeforeQuit'];
				if (confirmBeforeQuit == 'false' || app.quitFromTray) {
					quitOrHideApp();
				} else {
					event.preventDefault();
					var options = {
						type: 'question',
						buttons: ['Yes', 'No'],
						defaultId: 0,
						title: 'Vyapar',
						message: 'Are you sure you want to quit the app?',
						checkboxLabel: "Don't ask again",
						checkboxChecked: false,
						icon: path.join(__dirname, 'inlineSVG', 'vyapar.ico')
					};
					try {
						dialog.showMessageBox(mainWindow, options, function (closeConfirm, rememberSelection) {
							if (closeConfirm == 0) {
								setLocalStorageValue('confirmBeforeQuit', !rememberSelection);
								quitOrHideApp();
							}
						});
					} catch (e) {
						// For some system icon conversion is not working
						options['icon'] = path.join(__dirname, 'inlineSVG', 'vyapar_1.png');
						try {
							dialog.showMessageBox(mainWindow, options, function (closeConfirm, rememberSelection) {
								if (closeConfirm == 0) {
									setLocalStorageValue('confirmBeforeQuit', !rememberSelection);
									quitOrHideApp();
								}
							});
						} catch (ex) {
							delete options['icon'];
							try {
								dialog.showMessageBox(mainWindow, options, function (closeConfirm, rememberSelection) {
									if (closeConfirm == 0) {
										setLocalStorageValue('confirmBeforeQuit', !rememberSelection);
										quitOrHideApp();
									}
								});
							} catch (ex1) {
								quitOrHideApp();
							}
						}
					}
				}
			}
		});
	};

	try {
		var isDev = require('electron-is-dev');
		var appPath = app.getPath('exe');
		var allowAutoLaunch = !isDev && appPath.endsWith('Vyapar.exe');
		if (allowAutoLaunch) {
			autoLaunch = new AutoLaunch({
				name: 'VyaparApp',
				path: app.getPath('exe'),
				isHidden: true
			});
			autoLaunch.enable();
		}
	} catch (err) {
		var logger = require('./Utilities/logger');
		logger.error(err);
	}

	var tray = null;

	app.on('ready', function () {
		createWindow();
		try {
			var isComingFromAutoLaunch = (process.argv || []).indexOf('--hidden') !== -1;
			var isComingForFirstTimeRun = (process.argv || []).indexOf('--squirrel-firstrun') !== -1;
			if (isComingFromAutoLaunch) {
				mainWindow.hide();
			} else {
				mainWindow.maximize();
				if (!eventsLaunchedOnScreenFocus) {
					mainWindow.webContents.once('did-finish-load', function () {
						appLaunchEvents(false, !isComingForFirstTimeRun);
					});
					eventsLaunchedOnScreenFocus = true;
				}
			}
		} catch (err) {
			var _logger = require('./Utilities/logger');
			_logger.error(err);
		}

		// shortcut to register for home page
		shortcut.register(mainWindow, 'Ctrl+H', function () {
			mainWindow && mainWindow.setMenuBarVisibility(true);
			destroyDBWindow(function () {
				mainWindow.loadURL(__dirname + '/UIComponent/' + mainPage);
			});
		});

		shortcut.register(mainWindow, 'ctrl+shift+o', function () {
			mainWindow.webContents.openDevTools();
		});

		shortcut.register(mainWindow, 'ctrl+shift+-', function () {
			mainWindow && mainWindow.setMenuBarVisibility(true);
			app.hasUserPassCodeEntered = true;
			mainWindow.loadURL(__dirname + '/UIComponent/' + mainPage);
		});

		var trayIconPath = path.join(__dirname, 'inlineSVG', 'tray.ico');
		try {
			tray = new Tray(trayIconPath);
		} catch (e) {
			trayIconPath = path.join(__dirname, 'inlineSVG', 'tray_1.png');
			try {
				tray = new Tray(trayIconPath);
			} catch (ex) {
				try {
					tray = new Tray(electron.nativeImage.createEmpty());
				} catch (ex1) {}
			}
		}
		if (tray) {
			var contextMenu = Menu.buildFromTemplate([{
				label: 'Check for update',
				click: function click(menuItem, browserWindow, event) {
					showLoader();
					isOnline().then(function (online) {
						if (online) {
							menuItem.enabled = false;
							hideLoader();
							checkingForUpdate();
							checkForUpdate(true);
						} else {
							hideLoader();
							var index = dialog.showMessageBox({
								type: 'info',
								buttons: ['Ok'],
								title: 'Vyapar',
								message: 'No internet connection available. Please check your connection and try again.'
							});
						}
					});
				}
			}, {
				label: 'Exit',
				click: function click() {
					var index = 0;
					try {
						index = dialog.showMessageBox({
							type: 'info',
							buttons: ['Ok', 'Cancel'],
							title: 'Vyapar',
							message: 'Are you sure you want to quit the app?',
							icon: path.join(__dirname, 'inlineSVG', 'vyapar.ico')
						});
					} catch (e) {
						try {
							index = dialog.showMessageBox({
								type: 'info',
								buttons: ['Ok', 'Cancel'],
								title: 'Vyapar',
								message: 'Are you sure you want to quit the app?',
								icon: path.join(__dirname, 'inlineSVG', 'vyapar_1.png')
							});
						} catch (ex) {
							try {
								index = dialog.showMessageBox({
									type: 'info',
									buttons: ['Ok', 'Cancel'],
									title: 'Vyapar',
									message: 'Are you sure you want to quit the app?'
								});
							} catch (ex1) {}
						}
					}

					if (index == 1) {} else {
						app.quitFromTray = true;
						app.quit();
					}
				}
			}]);
			tray.setToolTip('Vyapar App');
			tray.setContextMenu(contextMenu);
			tray.on('click', function () {
				if (mainWindow) {
					if (!mainWindow.isMaximized()) mainWindow.maximize();
					if (!mainWindow.isVisible()) mainWindow.show();
					if (!mainWindow.isFocused()) mainWindow.focus();
					if (!eventsLaunchedOnScreenFocus) {
						appLaunchEvents(true);
						eventsLaunchedOnScreenFocus = true;
					}
				} else {
					app.quitFromTray = true;
					app.quit();
				}
			});
		}

		var squirrelCommand = process.argv[1];
		if (squirrelCommand != '--squirrel-firstrun') {
			//Todo Shubham. Check if it should be there. Update might be checked on system restart
			checkForUpdate();
		}
	});

	ipcMain.on('changeURL', function (event, url) {
		mainWindow && mainWindow.setMenuBarVisibility(true);
		destroyDBWindow(function () {
			if (global.notificationClicked == true && url == 'Index1.html') {
				global.notificationClicked = false;
				mainWindow.loadURL(__dirname + '/UIComponent/' + url + '?redirectTo=PaymentReminder.html');
			} else {
				var loadURL = __dirname + '/UIComponent/' + (app.hasUserPassCodeEntered ? url : mainPage);
				mainWindow.loadURL(loadURL);
			}
		});
	});

	ipcMain.on('openWindowOnnotificationClicked', function (event, val) {
		if (mainWindow) {
			var isVisible = mainWindow.isVisible();
			mainWindow.maximize();
			mainWindow.focus();
			global.notificationClicked = true;
			mainWindow.webContents.send('openPaymentReminderWindow', !isVisible);
		}
	});

	ipcMain.on('enableDisableAutoLaunch', function (event, val) {
		autoLaunch && autoLaunch.disable();
		if (val !== '0') {
			autoLaunch && autoLaunch.enable();
		}
	});

	ipcMain.on('checkForUpdates', function (event, val) {
		checkForUpdate(true);
	});

	app.on('window-all-closed', function () {
		if (app) {
			app.quit();
		}
	});

	app.on('activate', function () {
		if (mainWindow === null) {
			createWindow();
		}
	});

	//
	// app.on('ready', () =>{
	//
	// })

	var handleStartupEvent = function handleStartupEvent() {
		var fs = require('fs');
		var appPath = app.getPath('userData');

		var squirrelCommand = process.argv[1];
		switch (squirrelCommand) {
			case '--squirrel-install':
				app.quitFromTray = true;
				break;
			case '--squirrel-uninstall':
				app.quitFromTray = true;
				break;
			case '--squirrel-firstrun':
				try {
					var downloadFolderPath = appPath + '/../../../Downloads/';
					var files = fs.readdirSync(downloadFolderPath);
					var vyaparAppFileNameRegex = /^VyaparApp(_([^_]+))?(__([^_]+))?_installer.*\.exe$/;
					if (files && files.length && files.length > 0) {
						for (var i = 0; i < files.length; i++) {
							var fileName = files[i];
							if (fileName) {
								var match = vyaparAppFileNameRegex.exec(fileName);
								if (match && match.length && match.length >= 5) {
									var stat = fs.statSync(downloadFolderPath + '/' + fileName);
									if (stat.isFile()) {
										referralCode = match[2];
										campaignId = match[4];
									}
								}
							}
						}
					}
				} catch (ex) {
					var _logger2 = require('./Utilities/logger');
					_logger2.error(ex);
				}
				break;
		}
	};

	handleStartupEvent();

	var destroyDBWindow = function destroyDBWindow(cb) {
		mainWindow.webContents.executeJavaScript('try { const win = require("../Utilities/DBWindow"); win.destroy(); } catch(err) { console.log(err); }', true).then(function (result) {
			cb && cb();
		});
	};
}