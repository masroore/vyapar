var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var itemCategoryMap = {};
var instance;
var dataLoader;
var RefreshCacheTracker = require('./../BizLogic/RefreshCacheTracker.js');
var refreshCacheTracker = new RefreshCacheTracker();
var ItemCategoryCache = function ItemCategoryCache() {
	if (!dataLoader) {
		var DataLoader = require('./../DBManager/DataLoader.js');
		dataLoader = new DataLoader();
	}
	if (!instance) {
		// initializeItemCategoryCache
		(function () {
			itemCategoryMap = dataLoader.loadAllCategories();
			instance = 1;
		})();
		refreshCacheTracker.itemCategoryCacheRefreshNeeded = false;
	}

	// function to sorty objects alphabetically
	var sortAlphabetically = function sortAlphabetically(objList) {
		objList = objList.sort(function (a, b) {
			var a1 = a['categoryName'] ? a['categoryName'].toLowerCase() : '';
			var b1 = b['categoryName'] ? b['categoryName'].toLowerCase() : '';
			if (a1 == b1) return 0;
			return a1 > b1 ? 1 : -1;
		});
		return objList;
	};

	this.refreshItemCategory = function (itemCategory) {
		if (itemCategory) {
			// console.log(itemCategory);
			itemCategoryMap[itemCategory['categoryId']] = itemCategory;
			// console.log(itemCategoryMap);
		}
	};

	this.reloadItemCategory = function () {
		itemCategoryMap = dataLoader.loadAllCategories();
	};

	this.categoryExists = function (categoryName) {
		categoryName = categoryName ? categoryName.toLowerCase().trim() : '';
		for (var itemCategoryCur in itemCategoryMap) {
			if (categoryName == itemCategoryMap[itemCategoryCur]['categoryName'].toLowerCase()) {
				return true;
			}
		}
		return false;
	};

	this.getItemCategoryId = function (categoryName) {
		var retVal = 0;
		categoryName = categoryName ? categoryName.toLowerCase().trim() : '';
		for (var itemCategoryCur in itemCategoryMap) {
			if (categoryName == itemCategoryMap[itemCategoryCur]['categoryName'].toLowerCase()) {
				retVal = itemCategoryCur;
				break;
			}
		}
		return retVal;
	};

	this.getItemCategoryObjectById = function (catId) {
		if (itemCategoryMap[catId]) {
			return itemCategoryMap[catId];
		} else {
			return false;
		}
	};

	// for displaying in item categoris page
	this.getItemCategoriesList = function () {
		return itemCategoryMap;
	};

	this.getItemCategoriesArray = function () {
		var categoriesArr = (0, _keys2.default)(itemCategoryMap).map(function (key) {
			return itemCategoryMap[key];
		});
		return sortAlphabetically(categoriesArr);
	};

	this.getItemCategoryNamesForExcel = function () {
		var array = [];
		for (var itemCategoryCur in itemCategoryMap) {
			array.push(String(itemCategoryMap[itemCategoryCur]['categoryName']));
		}
		return array;
	};

	if (refreshCacheTracker.itemCategoryCacheRefreshNeeded) {
		this.reloadItemCategory();
		refreshCacheTracker.itemCategoryCacheRefreshNeeded = false;
	};
};
module.exports = ItemCategoryCache;