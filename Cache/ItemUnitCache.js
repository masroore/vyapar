var itemUnitCacheMap = {};
var instance = 0;
var dataLoader;
var RefreshCacheTracker = require('./../BizLogic/RefreshCacheTracker.js');
var refreshCacheTracker = new RefreshCacheTracker();
var ItemUnitCache = function ItemUnitCache() {
	if (!dataLoader) {
		var DataLoader = require('./../DBManager/DataLoader.js');
		dataLoader = new DataLoader();
	}
	if (!instance) {
		initializeItemunitCache = function () {
			itemUnitCacheMap = dataLoader.getAllItemUnits();
			instance = 1;
		}();
		refreshCacheTracker.itemUnitCacheRefreshNeeded = false;
	}

	this.refreshItemUnitCache = function () {
		itemUnitCacheMap = {};
		itemUnitCacheMap = dataLoader.getAllItemUnits();
	};

	this.getItemUnitNameById = function (unitId) {
		var retVal = null;
		$.each(itemUnitCacheMap, function (key, itemUnit) {
			if (itemUnit.getUnitId() == unitId) {
				retVal = itemUnit.getUnitName();
			}
		});
		return retVal;
	};

	this.getItemUnitIdByUnitName = function (unitName) {
		var retVal = null;
		unitName = typeof unitName == 'string' ? unitName.toLowerCase() : '';
		$.each(itemUnitCacheMap, function (key, itemUnit) {
			if (itemUnit.getUnitName().toLowerCase() == unitName) {
				retVal = itemUnit.getUnitId();
				return false;
			}
		});
		return retVal;
	};

	this.getItemUnitIdByShortName = function (unitName) {
		var retVal = null;
		unitName = typeof unitName == 'string' ? unitName.toLowerCase() : '';
		$.each(itemUnitCacheMap, function (key, itemUnit) {
			if (itemUnit.getUnitShortName().toLowerCase() == unitName) {
				retVal = itemUnit.getUnitId();
				return false;
			}
		});
		return retVal;
	};

	this.getItemUnitObjectByUnitName = function (unitName) {
		var retVal = null;
		unitName = typeof unitName == 'string' ? unitName.toLowerCase() : '';
		$.each(itemUnitCacheMap, function (key, itemUnit) {
			if (itemUnit.getUnitName().toLowerCase() == unitName) {
				retVal = itemUnit;
			}
		});
		return retVal;
	};

	this.getItemUnitShortNameById = function (unitId) {
		var retVal = null;
		$.each(itemUnitCacheMap, function (key, itemUnit) {
			// console.log(itemUnit);
			if (itemUnit.getUnitId() == unitId) {
				retVal = itemUnit.getUnitShortName();
			}
		});
		return retVal;
	};

	this.containsItemUnitWithId = function (unitId) {
		$.each(itemUnitCacheMap, function (key, itemUnit) {
			if (itemUnit.getUnitId() == unitId) {
				return true;
			}
		});
		return false;
	};

	this.containsItemUnitWithShortName = function (shortName) {
		shortName = typeof shortName == 'string' ? shortName.toLowerCase() : '';
		var rtnVal = false;
		var MyString = require('./../Utilities/MyString.js');
		$.each(itemUnitCacheMap, function (key, itemUnit) {
			if (MyString.compareMyString(itemUnit.getUnitShortName(), shortName, true)) {
				rtnVal = true;
			}
		});
		return rtnVal;
	};

	this.containsItemUnitWithFullName = function (fullName) {
		var rtnVal = false;
		var MyString = require('./../Utilities/MyString.js');
		$.each(itemUnitCacheMap, function (key, itemUnit) {
			if (MyString.compareMyString(itemUnit.getUnitName(), fullName, true)) {
				rtnVal = true;
			}
		});
		return rtnVal;
	};

	this.containsItemUnitWithShortNameWithDifferentId = function (shortName, itemId) {
		var itemUnit = itemUnitCacheMap[shortName];
		if (itemUnit != null) {
			return itemUnit.getUnitId() != itemId;
		}
		return false;
	};

	this.containsItemUnitWithFullNameWithDifferentId = function (fullName, itemId) {
		var retVal = false;
		fullName = typeof fullName == 'string' ? fullName.toLowerCase() : '';
		$.each(itemUnitCacheMap, function (shortName, unitObj) {
			if (unitObj.getUnitName().toLowerCase() == fullName) {
				if (unitObj.getUnitId() != itemId) {
					retVal = true;
					return false;
				}
			}
		});
		return retVal;
	};

	this.containsItemUnitWithShortNameExcludingUnitWithId = function (shortName, unitId) {
		if (this.containsItemUnitWithShortName(shortName)) {
			if (itemUnitCacheMap[shortName].getUnitId() == unitId) {
				return false;
			}
			return true;
		} else {
			return false;
		}
	};

	this.getAllItemUnits = function () {
		var itemUnitsArray = [];
		$.each(itemUnitCacheMap, function (key, values) {
			itemUnitsArray.push(values);
		});
		// return itemUnitsArray;
		return this.sortList(itemUnitsArray);
	};

	this.getAllItemUnitsNameWithShortName = function () {
		// console.log(stopohere);
		var itemUnitNameId = {};
		var itemUnitCacheMap = this.getAllItemUnits();
		$.each(itemUnitCacheMap, function (key, itemUnit) {
			itemUnitNameId[itemUnit.getUnitName() + ' ( ' + itemUnit.getUnitShortName() + ' )'] = itemUnit;
		});
		return itemUnitNameId;
	};

	this.getItemUnitNameWithShortNameForUnitId = function (unitId) {
		var retVal = null;
		var itemUnitCacheMap = this.getAllItemUnits();
		$.each(itemUnitCacheMap, function (key, itemUnit) {
			if (itemUnit.getUnitId() == unitId) {
				retVal = itemUnit.getUnitName() + ' ( ' + itemUnit.getUnitShortName() + ' )';
			}
		});
		return retVal;
	};

	this.getItemUnitById = function (unitId) {
		var retVal = null;
		$.each(itemUnitCacheMap, function (key, itemUnit) {
			if (itemUnit.getUnitId() == unitId) {
				retVal = itemUnit;
				return false;
			}
		});
		return retVal;
	};

	this.sortList = function (itemUnits) {
		itemUnits.sort(function (a, b) {
			a = a.getUnitName();
			b = b.getUnitName();
			return a < b ? -1 : a > b ? 1 : 0;
		});
		return itemUnits;
	};

	this.getItemUnitList = function (searchString) {
		var itemUnits = [];
		if (searchString == null || searchString) {
			$.each(itemUnitCacheMap, function (key, itemUnits) {
				itemUnits.push(itemUnit);
			});
		} else {
			$.each(itemUnitCacheMap, function (key, itemUnits) {
				if (itemUnit.getUnitName().toLowerCase().includes(searchString.toLowerCase())) {
					itemUnits.push(itemUnit);
				}
			});
		}
		return this.sortList(itemUnits);
	};

	this.reloadItemUnitCache = function () {
		initializeItemunitCache = function () {
			itemUnitCacheMap = dataLoader.getAllItemUnits();
			instance = 1;
		}();
	};

	if (refreshCacheTracker.itemUnitCacheRefreshNeeded) {
		this.reloadItemUnitCache();
		refreshCacheTracker.itemUnitCacheRefreshNeeded = false;
	};
};

module.exports = ItemUnitCache;