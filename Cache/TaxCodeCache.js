var taxCodeCacheObject;

var ItemCache = require('./../Cache/ItemCache.js');
var itemCache = new ItemCache();
var MyString = require('./../Utilities/MyString.js');
var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();
var FirmCache = require('./../Cache/FirmCache.js');
var TaxCodeConstants = require('./../Constants/TaxCodeConstants.js');
var firmCache = new FirmCache();
var NameCustomerType = require('./../Constants/NameCustomerType.js');
var RefreshCacheTracker = require('./../BizLogic/RefreshCacheTracker.js');
var refreshCacheTracker = new RefreshCacheTracker();
var TaxCodeCache = function TaxCodeCache() {
	if (taxCodeCacheObject) {
		if (refreshCacheTracker.taxCodeCacheRefreshNeeded) {
			taxCodeCacheObject.reloadTaxRateCache();
		}
		return taxCodeCacheObject;
	}
	var DataLoader = require('./../DBManager/DataLoader.js');
	this.dataLoader = new DataLoader();
	var taxCodeList = this.dataLoader.getListOfTax();
	refreshCacheTracker.taxCodeCacheRefreshNeeded = false;

	this.getTaxCodeNameById = function (taxCodeId) {
		var retVal;
		$.each(taxCodeList, function (key, value) {
			if (value[0].getTaxCodeId() == taxCodeId) {
				retVal = key;
				return false;
			}
		});
		return retVal;
	};

	this.getTaxIdByTaxName = function (taxName) {
		var retVal = 0;
		if (taxName) {
			$.each(taxCodeList, function (key, value) {
				if (value[0].getTaxCodeName() == taxName) {
					retVal = value[0].getTaxCodeId();
					return false;
				}
			});
		}
		return retVal;
	};

	this.getTaxCodeObjectById = function (taxCodeId) {
		var retObj;
		$.each(taxCodeList, function (key, value) {
			if (value[0].getTaxCodeId() == taxCodeId) {
				retObj = value[0];
				return false;
			}
		});
		return retObj;
	};

	this.getListOfTax = function () {
		return taxCodeList;
	};

	this.isTaxCodeNameExists = function (taxCodeName, taxCodeId) {
		// second field is used to check for same name while update
		if (taxCodeList[taxCodeName]) {
			if (taxCodeId && taxCodeList[taxCodeName][0].getTaxCodeId() == taxCodeId) {
				return false;
			} else {
				return true;
			}
		};
		return false;
	};

	this.getRateForTaxId = function (taxId) {
		var taxRate = 0;
		$.each(taxCodeList, function (key, value) {
			if (value[0].getTaxCodeId() == taxId) {
				taxRate = value[0].getTaxRate();
				return false;
			}
		});
		return taxRate;
	};

	this.getRateForListOfIds = function (taxIdList) {
		var taxRate = 0;
		$.each(taxCodeList, function (key, value) {
			if (taxIdList.indexOf(value[0].getTaxCodeId()) > -1) {
				taxRate += Number(value[0].getTaxRate());
			}
		});
		return taxRate;
	};

	this.reloadTaxRateCache = function () {
		taxCodeList = this.dataLoader.getListOfTax();
		refreshCacheTracker.taxCodeCacheRefreshNeeded = false;
	};

	this.getTaxListForTransaction = function (txnType, party, firm, itemId, destinationState, taxCodeIdsToBeIncluded) {
		var StateCodes = require('./../Constants/StateCode.js');
		var list = [];
		if (!settingCache.isCurrentCountryIndia()) {
			$.each(taxCodeList, function (key, value) {
				list.push(value[0]);
			});
			return list;
		}

		if (!MyString.isEmpty(destinationState)) {
			if (!StateCodes.getStateCode(destinationState)) {
				destinationState = '';
			}
		}

		// TaxCode header = new TaxCode();
		// header.setTaxCodeName(headerText);
		// header.setTaxCodeId(0);

		// list.add(header);

		var shouldIGSTBePresent = true;
		var shouldGSTBePresent = true;
		var shouldCESSBePresent = true;
		var shouldStateSpecificCESSBePresent = true;
		var shouldExemptBePresent = true;
		var itemCodeToBeAdded = true;

		if (!settingCache.getGSTEnabled()) {
			shouldIGSTBePresent = false;
			shouldGSTBePresent = false;
			shouldCESSBePresent = false;
			shouldStateSpecificCESSBePresent = false;
			shouldExemptBePresent = false;
		} else if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_ESTIMATE || txnType == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER) {
			var isCompositeEnabled = settingCache.isCompositeSchemeEnabled();
			if (isCompositeEnabled) {
				shouldIGSTBePresent = false;
				shouldGSTBePresent = false;
				shouldCESSBePresent = false;
				shouldStateSpecificCESSBePresent = false;
				shouldExemptBePresent = false;
				itemCodeToBeAdded = false;
			} else {
				if (!MyString.isEmpty(destinationState)) {
					if (!firm) {
						firm = firmCache.getDefaultFirm();
					}
					if (firm) {
						var sourceState = firm.getFirmState();
						if (!MyString.isEmpty(sourceState)) {
							if (MyString.compareMyString(sourceState, destinationState)) {
								shouldIGSTBePresent = false;
							} else {
								shouldGSTBePresent = false;
							}
						}
					}
				}
			}
		} else if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
			if (party) {
				if (party.getCustomerType() == NameCustomerType.REGISTERED_COMPOSITE) {
					shouldIGSTBePresent = false;
					shouldGSTBePresent = false;
					shouldCESSBePresent = false;
					shouldStateSpecificCESSBePresent = false;
					shouldExemptBePresent = false;
					itemCodeToBeAdded = false;
				} else if (!MyString.isEmpty(destinationState)) {
					var _sourceState = party.getContactState();
					if (!MyString.isEmpty(_sourceState)) {
						if (MyString.compareMyString(_sourceState, destinationState)) {
							shouldIGSTBePresent = false;
						} else {
							shouldGSTBePresent = false;
						}
					}
				}
			}
		}

		try {
			var itemTaxId = 0;
			if (itemId > 0) {
				var item = itemCache.getItemById(itemId);
				if (item != null) {
					itemTaxId = item.getItemTaxId();
				}
			}
			var currentObject = this;
			$.each(taxCodeList, function (key, value) {
				var taxCode = value[0];
				if (itemCodeToBeAdded && itemTaxId > 0 && taxCode.getTaxCodeId() == itemTaxId) {
					list.push(taxCode);
				} else if (taxCodeIdsToBeIncluded != null && taxCodeIdsToBeIncluded.length > 0 && taxCodeIdsToBeIncluded.indexOf(taxCode.getTaxCodeId()) > -1) {
					list.push(taxCode);
				} else if (taxCode.getTaxType() == TaxCodeConstants.taxGroup) {
					var taxGroupToBeAdded = true;
					var taxCodeMap = taxCode.getTaxCodeMap();
					$.each(taxCodeMap, function (key, taxCodeId) {
						var taxRate = currentObject.getTaxCodeObjectById(taxCodeId);
						switch (taxRate.getTaxRateType()) {
							case TaxCodeConstants.IGST:
								if (!shouldIGSTBePresent) {
									taxGroupToBeAdded = false;
								}
								break;
							case TaxCodeConstants.CGST:
							case TaxCodeConstants.SGST:
								if (!shouldGSTBePresent) {
									taxGroupToBeAdded = false;
								}
								break;
							case TaxCodeConstants.CESS:
								if (!shouldCESSBePresent) {
									taxGroupToBeAdded = false;
								}
								break;
							case TaxCodeConstants.STATE_SPECIFIC_CESS:
								if (!shouldStateSpecificCESSBePresent) {
									taxGroupToBeAdded = false;
								}
								break;
							case TaxCodeConstants.Exempted:
								if (!shouldExemptBePresent) {
									taxGroupToBeAdded = false;
								}
								break;
						}
						if (taxGroupToBeAdded == false) {
							return false;
						}
					});
					if (taxGroupToBeAdded) {
						list.push(taxCode);
					}
				} else {
					switch (taxCode.getTaxRateType()) {
						case TaxCodeConstants.IGST:
							if (shouldIGSTBePresent) {
								list.push(taxCode);
							}
							break;
						case TaxCodeConstants.OTHER:
							list.push(taxCode);
							break;
						case TaxCodeConstants.Exempted:
							if (shouldExemptBePresent) {
								list.push(taxCode);
							}
							break;
					}
				}
			});
		} catch (err) {}
		return list;
		// return sortTaxList(list);
	};

	this.getTaxCodeBasedOnPlaceOfSupply = function (taxId, txnType, party, firm, destinationState) {
		// let taxId = taxId;
		var StateCodes = require('./../Constants/StateCode.js');
		if (!settingCache.isCurrentCountryIndia()) {
			return taxId;
		}
		var taxCode = this.getTaxCodeObjectById(taxId);
		if (!taxCode) {
			return taxId;
		}
		if (!MyString.isEmpty(destinationState)) {
			if (!StateCodes.getStateCode(destinationState)) {
				destinationState = '';
			}
		}

		var isInterStateSupply = false;
		var isIntraStateSupply = false;

		if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_ESTIMATE || txnType == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER) {
			if (!MyString.isEmpty(destinationState)) {
				if (!firm) {
					firm = firmCache.getDefaultFirm();
				}
				if (firm) {
					var sourceState = firm.getFirmState();
					if (!MyString.isEmpty(sourceState)) {
						if (MyString.compareMyString(sourceState, destinationState)) {
							isIntraStateSupply = true;
							isInterStateSupply = false;
						} else {
							isIntraStateSupply = false;
							isInterStateSupply = true;
						}
					}
				}
			}
		} else if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
			if (party) {
				var _sourceState2 = party.getContactState();
				if (!MyString.isEmpty(_sourceState2)) {
					if (MyString.compareMyString(_sourceState2, destinationState)) {
						isIntraStateSupply = true;
						isInterStateSupply = false;
					} else {
						isIntraStateSupply = false;
						isInterStateSupply = true;
					}
				}
			}
		}

		if (isIntraStateSupply) {
			if (this.isTaxCodeIGST(taxCode)) {
				var code = this.getCorrespondingGSTCode(taxCode);
				if (code != null) {
					return code.getTaxCodeId();
				}
			}
		} else if (isInterStateSupply) {
			if (this.isTaxCodeGST(taxCode)) {
				var _code = this.getCorrespondingIGSTCode(taxCode);
				if (_code != null) {
					return _code.getTaxCodeId();
				}
			}
		}
		return taxId;
	};

	this.isTaxCodeIGST = function (taxCode) {
		if (taxCode) {
			if (taxCode.getTaxType() == TaxCodeConstants.taxRate) {
				if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
					return true;
				} else {
					return false;
				}
			} else {
				var isIGSTPresent = false;
				var taxOtherThanIGSTCESSPresent = false;
				var taxCodeMap = taxCode.getTaxCodeMap();
				var currentObject = this;
				$.each(taxCodeMap, function (key, taxCodeId) {
					var taxRate = currentObject.getTaxCodeObjectById(taxCodeId);
					switch (taxRate.getTaxRateType()) {
						case TaxCodeConstants.IGST:
							isIGSTPresent = true;
							break;
						case TaxCodeConstants.CGST:
						case TaxCodeConstants.SGST:
						case TaxCodeConstants.Exempted:
						case TaxCodeConstants.OTHER:
							taxOtherThanIGSTCESSPresent = true;
							break;
					}
				});
				if (isIGSTPresent && !taxOtherThanIGSTCESSPresent) {
					return true;
				} else {
					return false;
				}
			}
		}
		return false;
	};

	this.isTaxCodeGST = function (taxCode) {
		if (taxCode) {
			if (taxCode.getTaxType() == TaxCodeConstants.taxRate) {
				return false;
			} else {
				var isCGSTPresent = false;
				var isSGSTPresent = false;
				var taxOtherThanCGSTSGSTCESSPresent = false;
				var taxCodeMap = taxCode.getTaxCodeMap();
				var currentObject = this;
				$.each(taxCodeMap, function (key, taxCodeId) {
					var taxRate = currentObject.getTaxCodeObjectById(taxCodeId);
					switch (taxRate.getTaxRateType()) {
						case TaxCodeConstants.CGST:
							isCGSTPresent = true;
							break;
						case TaxCodeConstants.SGST:
							isSGSTPresent = true;
							break;
					}
				});
				if (isCGSTPresent && isSGSTPresent && !taxOtherThanCGSTSGSTCESSPresent) {
					return true;
				} else {
					return false;
				}
			}
		}
		return false;
	};

	this.getCorrespondingGSTCode = function (taxCode) {
		if (!taxCode) {
			return null;
		}
		var retVal = null;
		var currentObject = this;
		$.each(taxCodeList, function (key, tax) {
			if (tax[0].getTaxRate() == taxCode.getTaxRate()) {
				if (currentObject.isTaxCodeGST(tax[0])) {
					retVal = tax[0];
					return false;
				}
			}
		});
		return retVal;
	};

	this.getCorrespondingIGSTCode = function (taxCode) {
		if (!taxCode) {
			return null;
		}
		var retVal = null;
		var currentObject = this;
		$.each(taxCodeList, function (key, tax) {
			if (tax[0].getTaxRate() == taxCode.getTaxRate()) {
				if (currentObject.isTaxCodeIGST(tax[0])) {
					retVal = tax[0];
					return false;
				}
			}
		});
		return retVal;
	};

	taxCodeCacheObject = this;
	return taxCodeCacheObject;
};

module.exports = TaxCodeCache;