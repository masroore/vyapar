var instance;
var customFieldMap = {};
var dataLoader;
var CustomFieldsType;
var RefreshCacheTracker = require('./../BizLogic/RefreshCacheTracker.js');
var refreshCacheTracker = new RefreshCacheTracker();
var CustomFieldCache = function CustomFieldCache() {
	if (!dataLoader) {
		var DataLoader = require('./../DBManager/DataLoader.js');
		dataLoader = new DataLoader();
	}
	if (!CustomFieldsType) {
		CustomFieldsType = require('./../Constants/CustomFieldsType.js');
	}

	if (!instance) {
		customFieldMap = dataLoader.loadAllCustomFields();
		instance = 1;
		refreshCacheTracker.customFieldsCacheRefreshNeeded = false;
	}

	this.getListOfDeliveryCustomFields = function () {
		var listToReturn = {};
		$.each(customFieldMap, function (key, value) {
			if (value.getCustomFieldType() == CustomFieldsType.delivery) {
				listToReturn[key] = value;
			}
		});
		return listToReturn;
	};

	this.isDeliveryDetailsEnable = function () {
		var isDeliveryDetailsEnableFlag = false;
		$.each(customFieldMap, function (key, value) {
			if (value.getCustomFieldType() == CustomFieldsType.delivery && value.getCustomFieldVisibility()) {
				isDeliveryDetailsEnableFlag = true;
				return false;
			}
		});
		return isDeliveryDetailsEnableFlag;
	};

	this.refreshCustomFields = function () {
		customFieldMap = dataLoader.loadAllCustomFields();
	};

	this.getCustomFieldById = function (id) {
		return customFieldMap[id];
	};

	if (refreshCacheTracker.customFieldsCacheRefreshNeeded) {
		this.refreshCustomFields();
		refreshCacheTracker.customFieldsCacheRefreshNeeded = false;
	};
};

module.exports = CustomFieldCache;