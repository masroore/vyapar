var itemUnitMappingCacheMap = {};
var instance = 0;
var dataLoader;
var RefreshCacheTracker = require('./../BizLogic/RefreshCacheTracker.js');
var refreshCacheTracker = new RefreshCacheTracker();
var ItemUnitMappingCache = function ItemUnitMappingCache() {
	if (!dataLoader) {
		var DataLoader = require('./../DBManager/DataLoader.js');
		dataLoader = new DataLoader();
	}
	var ItemUnitCache = require('./../Cache/ItemUnitCache.js');
	if (!instance) {
		initializItemUnitMappingCache = function () {
			var DataLoader = require('./../DBManager/DataLoader.js');
			dataLoader = new DataLoader();
			itemUnitMappingCacheMap = dataLoader.getAllItemUnitsMapping();
			instance = 1;
		}();
		refreshCacheTracker.itemUnitMappingCacheRefreshNeeded = false;
	}

	this.clear = function () {
		instance = 0;
		itemUnitMappingCacheMap = {};
	};

	this.refreshItemUnitMappingCache = function () {
		itemUnitMappingCacheMap = {};
		itemUnitMappingCacheMap = dataLoader.getAllItemUnitsMapping();
		instance = 1;
	};

	this.getItemUnitMappings = function () {
		var mappingValues = [];
		$.each(itemUnitMappingCacheMap, function (key, value) {
			mappingValues.push(values);
		});
		return mappingValues;
	};

	this.getItemUnitMapping = function (id) {
		return itemUnitMappingCacheMap[id];
	};

	this.getMappingIdFromBaseAndSecondaryUnitIds = function (baseUnitId, secondaryUnitId, conversionRate) {
		var found = null;
		$.each(itemUnitMappingCacheMap, function (key, itemUnitMapping) {
			if (itemUnitMapping.getBaseUnitId() == baseUnitId && itemUnitMapping.getSecondaryUnitId() == secondaryUnitId && Number(itemUnitMapping.getConversionRate()) == Number(conversionRate)) {
				found = itemUnitMapping;
			}
		});
		return found;
	};

	this.getMappingFromBaseAndSecondaryUnitIds = function (baseUnitId, secondaryUnitId) {
		var itemUnitMappings = [];
		$.each(itemUnitMappingCacheMap, function (key, itemUnitMapping) {
			if (itemUnitMapping.getBaseUnitId() == baseUnitId && itemUnitMapping.getSecondaryUnitId() == secondaryUnitId) {
				itemUnitMappings.push(itemUnitMapping);
			}
		});
		return itemUnitMappings;
	};

	this.getItemUnitWhereBaseUnit = function (baseUnitId) {
		var itemUnitMappings = [];
		$.each(itemUnitMappingCacheMap, function (key, itemUnitMapping) {
			if (itemUnitMapping.getBaseUnitId() == baseUnitId) {
				itemUnitMappings.push(itemUnitMapping);
			}
		});
		return itemUnitMappings;
	};

	this.getItemUnitNameWithShortnameWhereBaseUnit = function (baseId) {
		// SortedMap<String,ItemUnitMapping> itemUnitMappingMap = new TreeMap<>();
		var itemUnitMappingMap = {};
		$.each(itemUnitMappingCacheMap, function (key, itemUnitMapping) {
			if (itemUnitMapping.getBaseUnitId() == baseId) {
				var itemUnitCache = new ItemUnitCache();
				var key = itemUnitCache.getItemUnitNameById(itemUnitMapping.getSecondaryUnitId()) + '( ' + itemUnitCache.getItemUnitShortNameById(itemUnitMapping.getSecondaryUnitId()) + ' )';
				itemUnitMapping[key] = itemUnitMapping;
			}
		});
		return itemUnitMappingMap;
	};

	this.sortList = function (itemUnitMappings) {
		itemUnitMappings.sort(function (a, b) {
			a = a.txnDate;
			b = b.txnDate;
			return a > b ? -1 : a < b ? 1 : 0;
		});
		// console.log(itemUnitMappings);
		return itemUnitMappings;
	};

	if (refreshCacheTracker.itemUnitMappingCacheRefreshNeeded) {
		this.refreshItemUnitMappingCache();
		refreshCacheTracker.itemUnitMappingCacheRefreshNeeded = false;
	};
};

module.exports = ItemUnitMappingCache;