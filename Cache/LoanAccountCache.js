var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var dataLoader, instance, loanAccountsMap;
instance = 0;
var RefreshCacheTracker = require('./../BizLogic/RefreshCacheTracker.js');
var refreshCacheTracker = new RefreshCacheTracker();
var LoanAccountCache = function LoanAccountCache() {
	if (!dataLoader) {
		var DataLoader = require('./../DBManager/DataLoader.js');
		dataLoader = new DataLoader();
	}
	if (!instance) {
		(function initializeLoanAccountCache() {
			loanAccountsMap = dataLoader.getAllLoanAccounts();
			instance = 1;
		})();
		refreshCacheTracker.loanAccountCacheRefreshNeeded = false;
	}

	// function to sorty objects alphabetically
	var sortAlphabetically = function sortAlphabetically(objList) {
		objList = objList.sort(function (a, b) {
			var a1 = a['accountName'].toLowerCase();var b1 = b['accountName'].toLowerCase();
			if (a1 == b1) return 0;
			return a1 > b1 ? 1 : -1;
		});
		return objList;
	};

	this.getLoanAccountIdByName = function (accountName) {
		if (!accountName) {
			return false;
		}
		accountName = accountName.trim();
		for (var accountId in loanAccountsMap) {
			var loanAccount = loanAccountsMap[accountId];
			if (loanAccount.getLoanAccName().toLowerCase() == accountName.toLowerCase()) {
				return accountId;
			}
		}
		return false;
	};

	this.refreshLoanAccountCache = function () {
		instance = 1;
		loanAccountsMap = dataLoader.getAllLoanAccounts();
	};

	this.getLoanAccountById = function (accId) {
		return loanAccountsMap[accId] || null;
	};

	this.getLoanAccAsExpenseCategories = function (firmId) {
		var list = this.getAllLoanAccounts(firmId);
		var categories = list.map(function (loanAccount) {
			return 'Interest Payment for [' + loanAccount.getLoanAccName() + ']';
		});
		categories.push('Processing Fee for Loans');
		return categories;
	};

	this.getAllLoanAccounts = function (firmId) {
		var sortListOnName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

		var list = (0, _values2.default)(loanAccountsMap);
		if (Number(firmId)) {
			list = list.filter(function (loanAccount) {
				return loanAccount.getFirmId() == firmId;
			});
		}
		if (sortListOnName) {
			list = sortAlphabetically(list);
		}
		return list;
	};

	if (refreshCacheTracker.loanAccountCacheRefreshNeeded) {
		this.refreshLoanAccountCache();
		refreshCacheTracker.loanAccountCacheRefreshNeeded = false;
	};
};

module.exports = LoanAccountCache;