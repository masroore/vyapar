var _set = require('babel-runtime/core-js/set');

var _set2 = _interopRequireDefault(_set);

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var itemCacheObject;

var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var ItemType = require('./../Constants/ItemType.js');
var MemoryCleanup = require('./../Utilities/MemoryCleanup.js');
var ErrorCode = require('./../Constants/ErrorCode.js');
var RefreshCacheTracker = require('./../BizLogic/RefreshCacheTracker.js');
var refreshCacheTracker = new RefreshCacheTracker();
var MyDouble = require('./../Utilities/MyDouble.js');
var ItemCache = function ItemCache() {
	if (itemCacheObject) {
		if (refreshCacheTracker.itemCacheRefreshNeeded) {
			itemCacheObject.reloadItemCache();
		}
		return itemCacheObject;
	}
	var DataLoader = require('./../DBManager/DataLoader.js');
	this.dataLoader = new DataLoader();
	var itemCacheMap = this.dataLoader.LoadAllItems();
	var serviceCacheMap = this.dataLoader.LoadAllServiceItem();
	var itemExpenseCacheMap = this.dataLoader.LoadAllItemExpense();
	var itemIncomeCacheMap = this.dataLoader.LoadAllItemIncome();

	// function to sorty objects alphabetically
	this.sortAlphabetically = function (objList) {
		if (objList) {
			objList = objList.sort(function (a, b) {
				var a1 = a['itemName'].toLowerCase();
				var b1 = b['itemName'].toLowerCase();
				if (a1 == b1) return 0;
				return a1 > b1 ? 1 : -1;
			});
		}
		return objList;
	};

	this.sortStringArray = function (arr) {
		if (arr) {
			arr = arr.sort(function (a, b) {
				return a.toLowerCase().localeCompare(b.toLowerCase());
			});
		}
		return arr;
	};

	this.reloadItemCache = function () {
		MemoryCleanup.cleanObject(itemCacheMap);
		MemoryCleanup.cleanObject(itemExpenseCacheMap);
		MemoryCleanup.cleanObject(itemIncomeCacheMap);

		itemCacheMap = this.dataLoader.LoadAllItems();
		itemExpenseCacheMap = this.dataLoader.LoadAllItemExpense();
		serviceCacheMap = this.dataLoader.LoadAllServiceItem();
		itemIncomeCacheMap = this.dataLoader.LoadAllItemIncome();

		refreshCacheTracker.itemCacheRefreshNeeded = false;
	};

	this.findItemByNameInMap = function (map, itemName) {
		var item = null;
		if (map && itemName) {
			itemName = itemName.trim().toLowerCase();
			$.each(map, function (key, val) {
				if (val.getItemName().toLowerCase().trim() == itemName) {
					item = val;
					return false;
				}
			});
		}
		return item;
	};

	this.findItemByCode = function (itemCode) {
		var itemObj = null;
		if (itemCode && typeof itemCode === 'string') {
			itemCode = itemCode.trim().toLowerCase();
			if (itemCode) {
				$.each(itemCacheMap, function (key, val) {
					var code = val.getItemCode();
					if (code && code.toLowerCase().trim() === itemCode) {
						itemObj = val;
						return false;
					}
				});
				if (!itemObj) {
					$.each(serviceCacheMap, function (key, val) {
						var code = val.getItemCode();
						if (code && code.toLowerCase().trim() === itemCode) {
							itemObj = val;
							return false;
						}
					});
				}
			}
		}
		return itemObj;
	};

	this.getProductItemByName = function (itemName) {
		return this.findItemByNameInMap(itemCacheMap, itemName);
	};

	this.getServiceItemByName = function (itemName) {
		return this.findItemByNameInMap(serviceCacheMap, itemName);
	};

	this.getExpenseItemByName = function (itemName) {
		return this.findItemByNameInMap(itemExpenseCacheMap, itemName);
	};

	this.getIncomeItemByName = function (itemName) {
		return this.findItemByNameInMap(itemIncomeCacheMap, itemName);
	};

	this.getProductServiceItemByName = function (itemName) {
		var item = this.getProductItemByName(itemName);
		if (!item) {
			item = this.getServiceItemByName(itemName);
		}
		return item;
	};

	this.itemExists = function (itemName) {
		var item = this.getProductServiceItemByName(itemName);
		return !!item;
	};

	this.expenseItemExists = function (itemName) {
		var item = this.getExpenseItemByName(itemName);
		return !!item;
	};

	this.incomeItemExists = function (itemName) {
		var item = this.getIncomeItemByName(itemName);
		return !!item;
	};

	this.isItemExistsByItemType = function (itemType, itemName) {
		if (itemType == ItemType.ITEM_TYPE_EXPENSE) {
			return this.expenseItemExists(itemName);
		} else if (itemType == ItemType.ITEM_TYPE_INCOME) {
			return this.incomeItemExists(itemName);
		} else {
			return this.itemExists(itemName);
		}
	};

	this.isItemExistsWithDifferentItemId = function (itemType, itemName, itemId) {
		var itemObj = this.findItemByName(itemName, null, itemType);
		if (itemObj && itemObj.getItemId() != itemId) {
			return true;
		} else {
			return false;
		}
	};

	this.getItemIdByNameAndTxn = function (txnType, itemName) {
		var item = null;

		if (txnType == TxnTypeConstant.TXN_TYPE_EXPENSE) {
			item = this.getExpenseItemByName(itemName);
		} else if (txnType == TxnTypeConstant.TXN_TYPE_OTHER_INCOME) {
			item = this.getIncomeItemByName(itemName);
		} else {
			item = this.getProductServiceItemByName(itemName);
		}
		return item ? item.getItemId() : 0;
	};

	this.itemCodeExists = function (itemCode) {
		return !!this.findItemByCode(itemCode);
	};

	this.getListOfItems = function () {
		var showInactiveItems = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

		var itemList = [];
		$.each(itemCacheMap, function (key, item) {
			if (showInactiveItems || item.getIsItemActive() == 1) {
				itemList.push(item.getItemName());
			}
		});
		$.each(serviceCacheMap, function (key, item) {
			if (showInactiveItems || item.getIsItemActive() == 1) {
				itemList.push(item.getItemName());
			}
		});
		return this.sortStringArray(itemList);
	};

	this.findItemByName = function (itemName) {
		var txnType = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
		var itemType = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

		if (txnType == TxnTypeConstant.TXN_TYPE_EXPENSE || itemType == ItemType.ITEM_TYPE_EXPENSE) {
			return this.getExpenseItemByName(itemName) || false;
		} else if (txnType == TxnTypeConstant.TXN_TYPE_OTHER_INCOME || itemType == ItemType.ITEM_TYPE_INCOME) {
			return this.getIncomeItemByName(itemName) || false;
		} else {
			return this.getProductServiceItemByName(itemName) || false;
		}
	};

	this.isExistsProductItem = function () {
		var retVal = false;
		if (itemCacheMap && (0, _keys2.default)(itemCacheMap).length > 0) {
			retVal = true;
		}
		return retVal;
	};

	this.isExistsServiceItem = function () {
		var retVal = false;
		if (serviceCacheMap && (0, _keys2.default)(serviceCacheMap).length > 0) {
			retVal = true;
		}
		return retVal;
	};

	this.IsAnyItemInactive = function () {
		var retVal = false;
		$.each(itemCacheMap, function (key, value) {
			if (value.getIsItemActive() == 0) {
				retVal = true;
				return false;
			}
		});
		if (!retVal) {
			$.each(serviceCacheMap, function (key, value) {
				if (value.getIsItemActive() == 0) {
					retVal = true;
					return false;
				}
			});
		}
		return retVal;
	};

	this.getItemById = function (itemId) {
		if (itemId) {
			return itemCacheMap[itemId] || serviceCacheMap[itemId] || itemExpenseCacheMap[itemId] || itemIncomeCacheMap[itemId] || null;
		}
		return null;
	};

	this.findItemNameById = function (itemId) {
		var item = this.getItemById(itemId);
		return item ? item.getItemName() : '';
	};

	this.getTotalStockValue = function () {
		var itemStockValue = 0;
		$.each(itemCacheMap, function (key, value) {
			itemStockValue += MyDouble.convertStringToDouble(value.getItemStockValue(), true);
		});
		return itemStockValue;
	};

	this.getLowStockItemList = function () {
		var itemList = [];
		$.each(itemCacheMap, function (key, value) {
			if (Number(value.getItemStockQuantity()) <= Number(value.getItemMinStockQuantity()) && value.getIsItemActive() == 1) {
				itemList.push(value);
			}
		});
		return this.sortAlphabetically(itemList);
	};

	this.getListOfItemsObject = function () {
		var showInactiveItems = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

		var itemList = [];
		$.each(itemCacheMap, function (key, item) {
			if (showInactiveItems || item.getIsItemActive() == 1) {
				itemList.push(item);
			}
		});
		return this.sortAlphabetically(itemList);
	};

	this.getListOfServicesObject = function () {
		var showInactiveItems = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

		var itemList = [];
		$.each(serviceCacheMap, function (key, item) {
			if (showInactiveItems || item.getIsItemActive() == 1) {
				itemList.push(item);
			}
		});
		return this.sortAlphabetically(itemList);
	};

	this.getListOfItemsAndServiceObject = function () {
		var showInactiveItems = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

		var itemList = [];
		itemList.push.apply(itemList, (0, _toConsumableArray3.default)(this.getListOfItemsObject(showInactiveItems)));
		itemList.push.apply(itemList, (0, _toConsumableArray3.default)(this.getListOfServicesObject(showInactiveItems)));
		return this.sortAlphabetically(itemList);
	};

	this.getListOfExpenseObject = function () {
		var itemList = [];
		$.each(itemExpenseCacheMap, function (key, item) {
			itemList.push(item);
		});
		return this.sortAlphabetically(itemList);
	};

	this.getListOfIncomeObject = function () {
		var itemList = [];
		$.each(itemIncomeCacheMap, function (key, item) {
			itemList.push(item);
		});
		return this.sortAlphabetically(itemList);
	};

	this.getItemListNotInCategoryId = function (catId) {
		var listToReturn = [];
		catId = Number(catId);
		// console.log(itemCacheMapValue);
		$.each(itemCacheMap, function (key, value) {
			if (Number(value.getItemCategoryId()) != catId && value.getIsItemActive() == 1) {
				listToReturn.push(value);
			}
		});
		$.each(serviceCacheMap, function (key, value) {
			if (Number(value.getItemCategoryId()) != catId && value.getIsItemActive() == 1) {
				listToReturn.push(value);
			}
		});
		return this.sortAlphabetically(listToReturn);
	};

	this.getItemListForCategoryId = function (catId) {
		var listToReturn = [];
		catId = Number(catId);
		// console.log(itemCacheMapValue);
		$.each(itemCacheMap, function (key, value) {
			if (Number(value.getItemCategoryId()) == catId && value.getIsItemActive() == 1) {
				listToReturn.push(value);
			}
		});
		$.each(serviceCacheMap, function (key, value) {
			if (Number(value.getItemCategoryId()) == catId && value.getIsItemActive() == 1) {
				listToReturn.push(value);
			}
		});
		return this.sortAlphabetically(listToReturn);
	};

	this.refreshItemCache = function (item) {
		// console.log(item);
		var ItemType = require('./../Constants/ItemType.js');
		if (item.getItemType() == ItemType.ITEM_TYPE_EXPENSE) {
			itemExpenseCacheMap[item.getItemId()] = item;
		} else if (item.getItemType() == ItemType.ITEM_TYPE_INCOME) {
			itemIncomeCacheMap[item.getItemId()] = item;
		} else {
			// These delete statements are important. will be used if type of item is getting changed from service to product or vice versa
			if (serviceCacheMap[item.getItemId()]) {
				delete serviceCacheMap[item.getItemId()];
			}
			if (itemCacheMap[item.getItemId()]) {
				delete itemCacheMap[item.getItemId()];
			}
			if (item.getItemType() == ItemType.ITEM_TYPE_SERVICE) {
				serviceCacheMap[item.getItemId()] = item;
			} else {
				itemCacheMap[item.getItemId()] = item;
			}
		}
	};

	this.fixStockValue = function (itemVerificationResult) {
		var result = true;
		var itemIdsToBeFixed = new _set2.default();
		if (itemVerificationResult && itemVerificationResult.length > 0) {
			$.each(itemVerificationResult, function (k, dataVerificationObject) {
				itemIdsToBeFixed.add(dataVerificationObject.getId());
			});
		}
		if (itemIdsToBeFixed.size > 0) {
			$.each(itemCacheMap, function (k, item) {
				if (itemIdsToBeFixed.has(Number(item.getItemId()))) {
					var errorCode = item.updateItemStockValue();
					if (errorCode == ErrorCode.ERROR_ITEM_SAVE_FAILED) {
						result = false;
					}
				}
			});
			$.each(serviceCacheMap, function (k, item) {
				if (itemIdsToBeFixed.has(Number(item.getItemId()))) {
					var errorCode = item.updateItemStockValue();
					if (errorCode == ErrorCode.ERROR_ITEM_SAVE_FAILED) {
						result = false;
					}
				}
			});
		}
		return result;
	};

	itemCacheObject = this;
	return itemCacheObject;
};

module.exports = ItemCache;