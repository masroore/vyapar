var instance = 0;
var _firmCacheMap = {};
var dataLoader;
var RefreshCacheTracker = require('./../BizLogic/RefreshCacheTracker.js');
var refreshCacheTracker = new RefreshCacheTracker();
var FirmCache = function FirmCache() {
	if (!dataLoader) {
		var DataLoader = require('./../DBManager/DataLoader.js');
		dataLoader = new DataLoader();
	}

	if (!instance) {
		initializeFirmCache = function () {
			_firmCacheMap = dataLoader.getFirmList();
			instance = 1;
		}();
		refreshCacheTracker.firmCacheRefreshNeeded = false;
	}

	this.refreshFirmCache = function () {
		_firmCacheMap = {};
		_firmCacheMap = dataLoader.getFirmList();
		instance = 1;
	};

	//
	this.getFirmByName = function (searchFullName) {
		var retFirm = null;
		if (_firmCacheMap[searchFullName]) {
			retFirm = _firmCacheMap[searchFullName];
		}
		return retFirm;
	};

	this.getFirmList = function () {
		return _firmCacheMap;
	};

	this.getFirmNameList = function () {
		var firmNames = [];
		if (firmNames != null && firmNames.length > 0) {
			firmNames = [];
		}

		if (_firmCacheMap != null) {
			$.each(_firmCacheMap, function (key, firm) {
				firmNames.push(firm.getFirmName());
			});
		}
		return firmNames;
	};

	this.isFirmNameUnique = function (firmName) {
		var unique = true;
		if (_firmCacheMap[firmName]) {
			unique = false;
		}
		return unique;
	};

	this.isFirmNameUniqueForUpdate = function (firmName, oldName) {
		var unique = true;
		if (_firmCacheMap[firmName]) {
			if (_firmCacheMap[firmName] != oldName) {
				unique = false;
			}
		}
		return unique;
	};

	this.getFirmById = function (firmId) {
		var firm = null;
		// $.each(_firmCacheMap, function(key,value){
		for (i in _firmCacheMap) {
			value = _firmCacheMap[i];
			if (value.getFirmId() == firmId) {
				firm = value;
			}
		}
		return firm;
	};

	this.getFirmNameById = function (firmId) {
		var firm = null;
		// $.each(_firmCacheMap, function(key,value){
		for (i in _firmCacheMap) {
			value = _firmCacheMap[i];
			if (value.getFirmId() == firmId) {
				firm = i;
			}
		}
		return firm;
	};

	this.getDefaultFirm = function () {
		var SettingCache = require('./../Cache/SettingCache.js');
		var settingCache = new SettingCache();
		var firmId = settingCache.getDefaultFirmId();
		var firm = null;
		$.each(_firmCacheMap, function (key, lfirm) {
			if (lfirm.getFirmId() == firmId) {
				firm = lfirm;
			}
		});
		return firm;
	};

	this.getFirmGSTINByFirmName = function (firmName) {
		var retVal = '';
		if (_firmCacheMap[firmName].getFirmHSNSAC()) {
			retVal = _firmCacheMap[firmName].getFirmHSNSAC();
		}
		return retVal;
	};

	this.getTransactionFirmWithDefault = function (txn) {

		if (!txn || !txn.getFirmId() || txn.getFirmId() == 0) {
			return this.getDefaultFirm();
		} else {
			return this.getFirmById(txn.getFirmId());
		}
	};

	this.getInvoiceAndPrefix = function (firmName, invoiceType) {
		var retInvoiceAndPrefix = [];
		var TxnSubtypeConstant = require('./../Constants/TxnSubtypeConstant.js');
		if (!firmName) {
			var firm = this.getDefaultFirm();
			var firmName = firm.getFirmName();
			if (invoiceType == 1) {
				// 1 for invoice
				retInvoiceAndPrefix.push(_firmCacheMap[firmName.trim()]['firmInvoicePrefix']);
			} else if (invoiceType == 2) {
				// 2 for tax invoice
				retInvoiceAndPrefix.push(_firmCacheMap[firmName.trim()]['firmTaxInvoicePrefix']);
			} else if (invoiceType == TxnTypeConstant.TXN_TYPE_ESTIMATE) {
				retInvoiceAndPrefix.push(_firmCacheMap[firmName.trim()]['firmEstimatePrefix']);
			} else if (invoiceType == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN) {
				retInvoiceAndPrefix.push(_firmCacheMap[firmName.trim()]['firmDeliveryChallanPrefix']);
			} else if (invoiceType == TxnTypeConstant.TXN_TYPE_CASHIN) {
				retInvoiceAndPrefix.push(_firmCacheMap[firmName.trim()]['firmCashInPrefix']);
			}
		} else if (_firmCacheMap[firmName.trim()]) {
			// 1 for invoice
			if (invoiceType == 1) {
				retInvoiceAndPrefix.push(_firmCacheMap[firmName.trim()]['firmInvoicePrefix']);
			} else if (invoiceType == 2) {
				// 2 for tax invoice
				retInvoiceAndPrefix.push(_firmCacheMap[firmName.trim()]['firmTaxInvoicePrefix']);
			} else if (invoiceType == TxnTypeConstant.TXN_TYPE_ESTIMATE) {
				retInvoiceAndPrefix.push(_firmCacheMap[firmName.trim()]['firmEstimatePrefix']);
			} else if (invoiceType == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN) {
				retInvoiceAndPrefix.push(_firmCacheMap[firmName.trim()]['firmDeliveryChallanPrefix']);
			} else if (invoiceType == TxnTypeConstant.TXN_TYPE_CASHIN) {
				retInvoiceAndPrefix.push(_firmCacheMap[firmName.trim()]['firmCashInPrefix']);
			}
		}
		return retInvoiceAndPrefix;
	};

	if (refreshCacheTracker.firmCacheRefreshNeeded) {
		this.refreshFirmCache();
		refreshCacheTracker.firmCacheRefreshNeeded = false;
	};
};

module.exports = FirmCache;