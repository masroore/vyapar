var settingCacheObject;
var settingCacheMap = {};
var RefreshCacheTracker = require('./../BizLogic/RefreshCacheTracker.js');
var refreshCacheTracker = new RefreshCacheTracker();
var Queries = require('./../Constants/Queries.js');
var CurrencyModeInWords = require('./../Constants/CurrencyModeInWords.js');
var TransactionPDFPaperSize = require('./../Constants/TransactionPDFPaperSize.js');
var ThermalPrinterConstant = require('./../Constants/ThermalPrinterConstant.js');
var ItemType = require('./../Constants/ItemType.js');
var Country = require('./../Constants/Country.js');
var InvoiceTheme = require('./../Constants/InvoiceTheme.js');

var SettingCache = function SettingCache() {
	if (settingCacheObject) {
		if (refreshCacheTracker.settingCacheRefreshNeeded) {
			settingCacheObject.reloadSettingCache();
		}
		return settingCacheObject;
	}
	var DataLoader = require('./../DBManager/DataLoader.js');
	this.dataLoader = new DataLoader();

	settingCacheMap = this.dataLoader.LoadAllSettings();
	refreshCacheTracker.settingCacheRefreshNeeded = false;

	settingCacheObject = this;
	return settingCacheObject;
};

SettingCache.prototype.LoadAllSettings = function () {
	return settingCacheMap;
};

SettingCache.prototype.reloadSettingCache = function () {
	settingCacheMap = this.dataLoader.LoadAllSettings();
	refreshCacheTracker.settingCacheRefreshNeeded = false;
};

SettingCache.prototype.refreshSettingsCache = function (settingModel) {
	if (settingModel) {
		var key = settingModel.getSettingKey();
		var value = settingModel.getSettingValue();
		settingCacheMap[key] = value;
	}
};

SettingCache.prototype.getCountryCode = function () {
	var countryCode = settingCacheMap[Queries.SETTING_USER_COUNTRY];
	return countryCode;
};

SettingCache.prototype.getItemUnitEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_IS_ITEM_UNIT_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isPaymentTermEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PAYMENT_TERM_ENABLED];
	return value == 1;
};

SettingCache.prototype.isBillToBillEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_BILL_TO_BILL_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getBarcodeScanEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_BARCODE_SCANNING_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getDirectBarcodeScanEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_DIRECT_BARCODE_SCANNING_MODE_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getServiceItemEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_ITEM_TYPE];
	var StringConstant = require('./../Constants/StringConstants.js');
	if (value != null && (value == StringConstant.settingServiceType || value == StringConstant.settingProductAndService)) {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isProductItemEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_ITEM_TYPE];
	var StringConstant = require('./../Constants/StringConstants.js');
	if (value != null && (value == StringConstant.settingProductType || value == StringConstant.settingProductAndService)) {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getItemType = function () {
	var value = settingCacheMap[Queries.SETTING_ITEM_TYPE];
	var retVal = 3;
	if (value) {
		retVal = Number(value);
	}
	return retVal;
};

SettingCache.prototype.getUserBankEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PRINT_BANK_DETAIL];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getAdditionalCessEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_ENABLE_ADDITIONAL_CESS_ON_ITEM];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getItemEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_ITEM_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getTaxEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_TAX_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getDiscountEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_DISCOUNT_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getDefaultCashSaleEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_ENABLE_DEFAULT_CASH_SALE];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isSettingPushedToCleverTap = function () {
	var value = settingCacheMap[Queries.SETTING_PUSH_SETTING_TO_CLEVERTAP_DESKTOP];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getPurchasePriceDisplayEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_SHOW_PURCHASE_PRICE_IN_ITEM_DROP_DOWN];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getDisplayNameEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_ENABLE_DISPLAY_NAME];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isFreeQuantityEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_FREE_QTY_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isExtraIncomeEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_OTHER_INCOME_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getDiscountInMoneyTxnEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_DISCOUNT_IN_MONEY_TXN];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.ifShowLastFiveSalePrice = function () {
	var value = settingCacheMap[Queries.SETTING_SHOW_LAST_FIVE_SALE_PRICE];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getPasscodeEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PASSCODE_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getDeletePasscodeEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_DELETE_PASSCODE_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getDeletePasscode = function () {
	var passcode = settingCacheMap[Queries.SETTING_DELETE_PASSCODE];
	return passcode;
};

SettingCache.prototype.getValue = function (key, defaultValue) {
	if (key != null) {
		var value = settingCacheMap[key];
		if (value != null) {
			return value;
		}
	}
	return defaultValue;
};

SettingCache.prototype.getPaymentReminderDays = function () {
	if (settingCacheMap[Queries.SETTING_PAYMENT_REMINDER_DAYS]) {
		return Number(settingCacheMap[Queries.SETTING_PAYMENT_REMINDER_DAYS]);
	} else {
		return 3;
	}
};

SettingCache.prototype.getPaymentReminderMessage = function () {
	var value = settingCacheMap[Queries.SETTING_CUSTOM_PAYMENT_REMINDER_MESSAGE];
	if (!value) {
		var StringConstants = require('./../Constants/StringConstants.js');
		return StringConstants.DEFAULT_REMINDER_MESSAGE;
	} else {
		return value;
	}
};

SettingCache.prototype.getCatalogueMessage = function () {
	var StringConstants = require('./../Constants/StringConstants.js');
	return StringConstants.DEFAULT_CATALOGUE_MESSAGE;
};

SettingCache.prototype.getAutoBackUpEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_AUTO_BACKUP_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getReverseChargeEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_ENABLE_REVERSE_CHARGE];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getAutoBackUpPeriod = function () {
	var autoBackUpPeriod = Number(settingCacheMap[Queries.SETTING_AUTO_BACKUP_DURATION_DAYS]);
	return autoBackUpPeriod;
};

SettingCache.prototype.getGSTEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_GST_ENABLED];
	if (value && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getHSNSACEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_HSN_SAC_ENABLED];
	if (value && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getLastBackupTime = function () {
	var lastBackupTime = settingCacheMap[Queries.SETTING_LAST_BACKUP_TIME];
	var lastBackupDateTime;
	try {
		lastBackupDateTime = new Date(lastBackupTime);
	} catch (ex) {}
	return lastBackupDateTime;
};

SettingCache.prototype.getFreeTrialStartDate = function () {
	var freeTrialDate;
	if (!settingCacheMap[Queries.SETTING_FREE_TRIAL_START_DATE_DESKTOP]) {
		var SettingsModel = require('./../Models/SettingsModel.js');
		var settingsModel = new SettingsModel();
		settingsModel.setSettingKey(Queries.SETTING_FREE_TRIAL_START_DATE_DESKTOP);
		var MyDate = require('./../Utilities/MyDate.js');
		freeTrialDate = new Date();
		var settingValue = MyDate.getDate('y-m-d H:M:S');
		settingsModel.setSettingValue(settingValue);
		settingsModel.UpdateSetting(settingValue, { nonSyncableSetting: true });
	} else {
		freeTrialDate = new Date(settingCacheMap[Queries.SETTING_FREE_TRIAL_START_DATE_DESKTOP]);
	}
	return freeTrialDate;
};

SettingCache.prototype.getMacAddress = function () {
	var retVal;
	var address = settingCacheMap[Queries.SETTING_CURRENT_MAC_ADDRESS];
	if (address) {
		retVal = address;
	} else {
		retVal = false;
	}
	return retVal;
};

SettingCache.prototype.getPartyShippingAddressEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PARTY_SHIPPING_ADDRESS_ENABLED];
	if (value && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isPrintPartyShippingAddressEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PRINT_PARTY_SHIPPING_ADDRESS];
	if (value && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getLeadsSent = function () {
	var value = settingCacheMap[Queries.SETTING_LEADS_INFO_SENT];
	if (value && value != '0') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getCurrencySymbol = function () {
	var currencySymbol = settingCacheMap[Queries.SETTING_CURRENCY_SYMBOL];
	return currencySymbol;
};

SettingCache.prototype.getStockEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_STOCK_ENABLED];
	if (value && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getTxnRefNoEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_TXNREFNO_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getPartyWiseItemRateEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PARTY_ITEM_RATE];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getPaymentReminderEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PAYMENTREMIDNER_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isOwnerTxnMsgEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_TXN_MSG_TO_OWNER];
	if (value && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getOwnerTxnMsgPhoneNumber = function () {
	var value = settingCacheMap[Queries.SETTING_TXN_MSG_OWNER_NUMBER];
	if (!value) {
		value = '';
	}
	return value;
};
SettingCache.prototype.getTxnMsgSenderId = function () {
	var value = settingCacheMap[Queries.SETTING_TXN_MSG_SENDER_ID];
	if (!value) {
		value = '';
	}
	return value;
};

SettingCache.prototype.getTransactionMessageEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_TRANSACTION_MESSAGE_ENABLED];
	if (value && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isTxnMessageEnabledForTxn = function (txnType) {
	var value = '';

	switch (txnType) {
		case TxnTypeConstant.TXN_TYPE_SALE:
			value = settingCacheMap[Queries.SETTING_TXN_MESSAGE_ENABLED_SALE];
			break;
		case TxnTypeConstant.TXN_TYPE_PURCHASE:
			value = settingCacheMap[Queries.SETTING_TXN_MESSAGE_ENABLED_PURCHASE];
			break;
		case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
			value = settingCacheMap[Queries.SETTING_TXN_MESSAGE_ENABLED_SALERETURN];
			break;
		case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
			value = settingCacheMap[Queries.SETTING_TXN_MESSAGE_ENABLED_PURCHASERETURN];
			break;
		case TxnTypeConstant.TXN_TYPE_CASHIN:
			value = settingCacheMap[Queries.SETTING_TXN_MESSAGE_ENABLED_CASHIN];
			break;
		case TxnTypeConstant.TXN_TYPE_CASHOUT:
			value = settingCacheMap[Queries.SETTING_TXN_MESSAGE_ENABLED_CASHOUT];
			break;
		case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
			value = settingCacheMap[Queries.SETTING_TXN_MESSAGE_ENABLED_SALE_ORDER];
			break;
		case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
			value = settingCacheMap[Queries.SETTING_TXN_MESSAGE_ENABLED_PURCHASE_ORDER];
			break;
		case TxnTypeConstant.TXN_TYPE_ESTIMATE:
			value = settingCacheMap[Queries.SETTING_TXN_MESSAGE_ENABLED_ESTIMATE_FORM];
			break;
		case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
			value = settingCacheMap[Queries.SETTING_TXN_MESSAGE_ENABLED_DELIVERY_CHALLAN];
			break;
	}
	if (value && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isItemwiseTaxEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_ITEMWISE_TAX_ENABLED];
	if (value && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isACEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_AC_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isAC1Enabled = function () {
	var value = settingCacheMap[Queries.SETTING_AC1_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isAC2Enabled = function () {
	var value = settingCacheMap[Queries.SETTING_AC2_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isAC3Enabled = function () {
	var value = settingCacheMap[Queries.SETTING_AC3_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isCompositeSchemeEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_IS_COMPOSITE_SCHEME_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isRoundOffEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_IS_ROUND_OFF_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};
SettingCache.prototype.isInclusiveExclusiveTaxEnabledForTransactions = function () {
	var value = settingCacheMap[Queries.SETTING_ENABLE_INCLUSIVE_EXCLUSIVE_TAX_ON_TRANSACTION];
	if (value && value != '0') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getTaxTypeOnInwardTxn = function () {
	var value = settingCacheMap[Queries.SETTING_INCLUSIVE_TAX_ON_INWARD_TXN];
	if (value) {
		return value;
	} else {
		return ItemType.ITEM_TXN_TAX_EXCLUSIVE;
	}
};

SettingCache.prototype.getTaxTypeOnOutWardTxn = function () {
	var value = settingCacheMap[Queries.SETTING_INCLUSIVE_TAX_ON_OUTWARD_TXN];
	if (value) {
		return value;
	} else {
		return ItemType.ITEM_TXN_TAX_EXCLUSIVE;
	}
};

SettingCache.prototype.getTxnThermalTheme = function () {
	var value = settingCacheMap[Queries.SETTING_THERMAL_PRINTER_THEME];
	if (value) {
		return Number(value);
	} else {
		return InvoiceTheme.THERMAL_THEME_1;
	}
};

SettingCache.prototype.isPODetailsEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PO_DETAILS_ENABLED];
	if (value && value != '0') {
		return true;
	} else {
		return false;
	}
};

// function to get the nearest or max value for round off
SettingCache.prototype.getRoundOffToValue = function () {
	var value = settingCacheMap[Queries.SETTING_ROUND_OFF_TYPE];
	return value || 1;
};

// function to get the value to round off to
SettingCache.prototype.getRoundOffToPlace = function () {
	var value = settingCacheMap[Queries.SETTING_ROUND_OFF_UPTO];
	return value || 1;
};

SettingCache.prototype.getCompositeUserType = function () {
	var value = settingCacheMap[Queries.SETTING_COMPOSITE_USER_TYPE];
	return value || 1;
};

SettingCache.prototype.isPartyGroupEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PARTY_GROUP];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isItemCategoryEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_ITEM_CATEGORY];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isItemwiseDiscountEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_ITEMWISE_DISCOUNT_ENABLED];
	if (value && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isOrderFormEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_ORDER_FORM_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isDeliveryChallanEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_DELIVERY_CHALLAN_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isDeliveryChallanReturnEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_DELIVERY_CHALLAN_RETURN_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isEstimateEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_ESTIMATE_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getAdditionalCessEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_ENABLE_ADDITIONAL_CESS_ON_ITEM];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isAdditionalItemDetailsEnabled = function (key) {
	// pass the key
	var value = settingCacheMap[key];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isPrintDetailsEnabled = function (key) {
	// pass the key
	var value = settingCacheMap[key];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isAnyAdditionalItemDetailsEnabled = function () {
	return this.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_MRP) || this.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_BATCH_NUMBER) || this.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_EXPIRY_DATE) || this.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_MANUFACTURING_DATE) || this.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_SERIAL_ITEM_NUMBER) || this.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_SIZE);
};

SettingCache.prototype.getExpiryDateFormat = function () {
	var value = settingCacheMap[Queries.SETTING_EXPIRY_DATE_TYPE];
	if (value != null) {
		return value;
	} else {
		return false;
	}
};

SettingCache.prototype.getManufacturingDateFormat = function () {
	var value = settingCacheMap[Queries.SETTING_MANUFACTURING_DATE_TYPE];
	if (value != null) {
		return Number(value);
	} else {
		return false;
	}
};

SettingCache.prototype.getAdditionalItemDetailsHeaderValue = function (key) {
	// pass the key
	var value = settingCacheMap[key];
	return value || 'Custom Field';
};

SettingCache.prototype.getQuantityDecimal = function () {
	var quantityDecimal = 2;
	try {
		quantityDecimal = Number(settingCacheMap[Queries.SETTING_QUANTITY_DECIMAL]);
		if (isNaN(quantityDecimal) || quantityDecimal < 0 || quantityDecimal > 5) {
			quantityDecimal = 2;
		}
	} catch (ex) {
		logger.error('Error getting decimal quantity', ex);
	}
	return quantityDecimal;
};

SettingCache.prototype.getAmountDecimal = function () {
	var amountDecimal = 2;
	try {
		amountDecimal = Number(settingCacheMap[Queries.SETTING_AMOUNT_DECIMAL]);
		if (isNaN(amountDecimal) || amountDecimal < 0 || amountDecimal > 5) {
			amountDecimal = 2;
		}
	} catch (ex) {
		logger.error('Error getting decimal amount', ex);
	}
	return amountDecimal;
};

SettingCache.prototype.getLoginUserName = function () {
	var value = settingCacheMap[Queries.SETTING_USERNAME];
	if (!value) {
		value = '';
	}
	return value;
};

SettingCache.prototype.getTaxInvoiceEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_TAXINVOICE_ENABLED];
	if (value && value == '1') {
		value = true;;
	} else {
		value = false;
	}
	return value;
};

SettingCache.prototype.getLoginUserNumber = function () {
	var value = settingCacheMap[Queries.SETTING_USERNUMBER];
	if (!value) {
		value = '';
	}
	return value;
};

SettingCache.prototype.getLoginUserEmailId = function () {
	var value = settingCacheMap[Queries.SETTING_EMAILID];
	if (value == null) {
		value = '';
	}
	return value;
};

SettingCache.prototype.getCompanyAddress = function () {
	var value = settingCacheMap[Queries.SETTING_ADDRESS];
	if (!value) {
		value = '';
	}
	return value;
};

SettingCache.prototype.isPrintTINEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PRINT_TINNUMBER];
	if (value && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getCompanyGlobalId = function () {
	var value = settingCacheMap[Queries.SETTING_COMPANY_GLOBAL_ID];
	if (value) {
		return value;
	} else {
		return '';
	}
};

SettingCache.prototype.getExtraSpaceOnTxnPDF = function () {
	var extraSpace = 0;
	extraSpace = Number(settingCacheMap[Queries.SETTING_EXTRA_SPACE_ON_TXN_PDF]);
	return extraSpace;
};

SettingCache.prototype.isPrintLogoEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PRINT_LOGO_ON_TXN_PDF];
	return value && value == '1';
};

SettingCache.prototype.isPrintCompanyNameEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PRINT_COMPANY_NAME_ON_TXN_PDF];
	if (value && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isPrintCompanyAddressEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PRINT_COMPANY_ADDRESS_ON_TXN_PDF];
	if (value && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isPrintCompanyNumberEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PRINT_COMPANY_NUMBER_ON_TXN_PDF];
	if (value && value == '1') {
		return true;
	} else {
		return false;
	}
};
// old methods for terms and conditions
SettingCache.prototype.isPrintTermsAndConditionEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF];
	if (value && value == '0') {
		return false;
	} else {
		return true;
	}
};
SettingCache.prototype.getTermsAndCondition = function () {
	var value = settingCacheMap[Queries.SETTING_TERMS_AND_CONDITIONS];
	if (value) {
		return value;
	} else {
		return 'Thanks for doing business with us!';
	}
};
// new getter methods added for fetching multiple terms and conditions.
SettingCache.prototype.isPrintTermsAndConditionSaleInvoiceEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PRINT_SALE_INVOICE_TERM_AND_CONDITION_ON_TXN_PDF] || settingCacheMap[Queries.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF];
	if (value && value == '0') {
		return false;
	} else {
		return true;
	}
};
SettingCache.prototype.getTermsAndConditionSaleInvoivce = function () {
	var value = settingCacheMap[Queries.SETTING_SALE_INVOICE_TERMS_AND_CONDITIONS] || settingCacheMap[Queries.SETTING_TERMS_AND_CONDITIONS];
	if (value) {
		return value;
	} else {
		return 'Thanks for doing business with us!';
	}
};
SettingCache.prototype.isPrintTermsAndConditionSaleOrderEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PRINT_SALE_ORDER_TERM_AND_CONDITION_ON_TXN_PDF] || settingCacheMap[Queries.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF];
	if (value && value == '0') {
		return false;
	} else {
		return true;
	}
};
SettingCache.prototype.getTermsAndConditionSaleOrder = function () {
	var value = settingCacheMap[Queries.SETTING_SALE_ORDER_TERMS_AND_CONDITIONS] || settingCacheMap[Queries.SETTING_TERMS_AND_CONDITIONS];
	if (value) {
		return value;
	} else {
		return 'Thanks for doing business with us!';
	}
};
SettingCache.prototype.isPrintTermsAndConditionDeliveryChallanEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PRINT_DELIVERY_CHALLAN_TERM_AND_CONDITION_ON_TXN_PDF] || settingCacheMap[Queries.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF];
	if (value && value == '0') {
		return false;
	} else {
		return true;
	}
};
SettingCache.prototype.getTermsAndConditionDeliveryChallan = function () {
	var value = settingCacheMap[Queries.SETTING_DELIVERY_CHALLAN_TERMS_AND_CONDITIONS] || settingCacheMap[Queries.SETTING_TERMS_AND_CONDITIONS];
	if (value) {
		return value;
	} else {
		return 'Thanks for doing business with us!';
	}
};
SettingCache.prototype.isPrintTermsAndConditionEstimateQuotationEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PRINT_ESTIMATE_QUOTATION_TERM_AND_CONDITION_ON_TXN_PDF] || settingCacheMap[Queries.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF];
	if (value && value == '0') {
		return false;
	} else {
		return true;
	}
};
SettingCache.prototype.getTermsAndConditionEstimateQuotation = function () {
	var value = settingCacheMap[Queries.SETTING_ESTIMATE_QUOTATION_TERMS_AND_CONDITIONS] || settingCacheMap[Queries.SETTING_TERMS_AND_CONDITIONS];
	if (value) {
		return value;
	} else {
		return 'Thanks for doing business with us!';
	}
};

SettingCache.prototype.isPrintDescriptionEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PRINT_DESCRIPTION_ON_TXN_PDF];
	if (value && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getTxnPDFTheme = function () {
	var theme = InvoiceTheme.THEME_1;
	theme = Number(settingCacheMap[Queries.SETTING_TXN_PDF_THEME]);
	return theme;
};

SettingCache.prototype.getTxnPDFThemeColor = function () {
	var value = settingCacheMap[Queries.SETTING_TXN_PDF_THEME_COLOR];
	if (value) {
		return value;
	} else {
		return InvoiceTheme.THEME_COLOR_24;
	}
};

SettingCache.prototype.getIsInvoicePreviewDisabled = function () {
	var value = settingCacheMap[Queries.SETTING_DISABLE_INVOICE_PREVIEW];
	return value && value == '1';
};

SettingCache.prototype.getIsPrintQRCodeEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PRINT_ENABLE_QR_CODE];
	return value && value == '1';
};

SettingCache.prototype.isTINNumberEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_TIN_NUMBER_ENABLED];
	if (value && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isSignatureEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_SIGNATURE_ENABLED];
	if (value && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isZeroBalPartyAllowedInReceivable = function () {
	var value = settingCacheMap[Queries.SETTING_IS_ZERO_BAL_PARTY_ALLOWED_IN_RECEIVABLE];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isMultiLanguageEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_MULTI_LANGUAGE_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getMultipleFirmEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_MULTIFIRM_ENABLED];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getDefaultFirmId = function () {
	var value = settingCacheMap[Queries.SETTING_DEFAULT_FIRM_ID];
	if (value) {
		return value;
	} else {
		return 1;
	}
};

SettingCache.prototype.isPrintItemQuantityTotalEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PRINT_ITEM_QUANTITY_TOTAL_ON_TXN_PDF];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getHeaderForSale = function () {
	var value = settingCacheMap[Queries.SETTING_CUSTOM_NAME_FOR_SALE];
	if (!value) {
		if (this.isCurrentCountryGulf()) {
			value = 'Tax Invoice';
		} else {
			value = 'Invoice';
		}
	}
	return value;
};

SettingCache.prototype.getHeaderForTaxSale = function () {
	var value = settingCacheMap[Queries.SETTING_CUSTOM_NAME_FOR_TAX_INVOICE];
	if (!value) {
		value = 'Tax Invoice';
	}
	return value;
};

SettingCache.prototype.getHeaderForPurchase = function () {
	var value = settingCacheMap[Queries.SETTING_CUSTOM_NAME_FOR_PURCHASE];
	if (!value) {
		value = 'Bill';
	}
	return value;
};

SettingCache.prototype.getHeaderForDeliveryChallan = function () {
	var value = settingCacheMap[Queries.SETTING_CUSTOM_NAME_FOR_DELIVERY_CHALLAN];
	if (!value) {
		value = 'Delivery Challan';
	}
	return value;
};

SettingCache.prototype.getHeaderForSaleReturn = function () {
	var value = settingCacheMap[Queries.SETTING_CUSTOM_NAME_FOR_SALE_RETURN];
	if (!value) {
		value = 'Credit Note';
	}
	return value;
};

SettingCache.prototype.getHeaderForPurchaseReturn = function () {
	var value = settingCacheMap[Queries.SETTING_CUSTOM_NAME_FOR_PURCHASE_RETURN];
	if (!value) {
		value = 'Debit Note';
	}
	return value;
};

SettingCache.prototype.getHeaderForExpense = function () {
	var value = settingCacheMap[Queries.SETTING_CUSTOM_NAME_FOR_EXPENSE];
	if (!value) {
		value = 'Expense';
	}
	return value;
};

SettingCache.prototype.getHeaderForIncome = function () {
	var value = settingCacheMap[Queries.SETTING_CUSTOM_NAME_FOR_INCOME];
	if (!value) {
		value = 'Other Income';
	}
	return value;
};

SettingCache.prototype.getHeaderForSaleOrder = function () {
	var value = settingCacheMap[Queries.SETTING_CUSTOM_NAME_FOR_SALE_ORDER];
	if (!value) {
		value = 'Sale Order';
	}
	return value;
};

SettingCache.prototype.getHeaderForPurchaseOrder = function () {
	var value = settingCacheMap[Queries.SETTING_CUSTOM_NAME_FOR_PURCHASE_ORDER];
	if (!value) {
		value = 'Purchase Order';
	}
	return value;
};

SettingCache.prototype.getHeaderForEstimate = function () {
	var value = settingCacheMap[Queries.SETTING_CUSTOM_NAME_FOR_ESTIMATE];
	if (!value) {
		value = 'Estimate';
	}
	return value;
};

SettingCache.prototype.getHeaderForCashIn = function () {
	var value = settingCacheMap[Queries.SETTING_CUSTOM_NAME_FOR_CASH_IN];
	if (!value) {
		value = 'Payment Receipt';
	}
	return value;
};

SettingCache.prototype.getHeaderForCashOut = function () {
	var value = settingCacheMap[Queries.SETTING_CUSTOM_NAME_FOR_CASH_OUT];
	if (!value) {
		value = 'Payment Out';
	}
	return value;
};

SettingCache.prototype.getMinimumLineCountForTheme5 = function () {
	var value = settingCacheMap[Queries.SETTING_MIN_ITEM_ROWS_ON_TXN_PDF];
	if (!value || isNaN(value)) {
		value = 0;
	}
	return Number(value);
};

SettingCache.prototype.amountInWordsMode = function () {
	var value = settingCacheMap[Queries.SETTING_AMOUNT_IN_WORD_FORMAT];
	if (!value || isNaN(value)) {
		return CurrencyModeInWords.INDIAN_MODE;
	}
	return Number(value);
};

SettingCache.prototype.isPrintTaxDetailEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PRINT_TAX_DETAILS];
	if (value != null && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getPrintPaperSize = function () {
	try {
		var value = settingCacheMap[Queries.SETTING_PRINT_PAGE_SIZE];
		if (value && Number(value) == TransactionPDFPaperSize.A5) {
			return TransactionPDFPaperSize.A5;
		}
	} catch (err) {
		if (logger) {
			logger.error(err);
		}
	}
	return TransactionPDFPaperSize.A4;
};

SettingCache.prototype.getPrintTextSize = function () {
	try {
		var value = settingCacheMap[Queries.SETTING_PRINT_TEXT_SIZE];
		if (value) {
			return Number(value);
		}
	} catch (err) {
		if (logger) {
			logger.error(err);
		}
	}
	return 4;
};

SettingCache.prototype.getCompanyNameHeaderPrintTextSize = function () {
	try {
		var value = settingCacheMap[Queries.SETTING_PRINT_COMPANY_NAME_TEXT_SIZE];
		if (value) {
			return Number(value);
		}
	} catch (err) {
		if (logger) {
			logger.error(err);
		}
	}
	return 4;
};

SettingCache.prototype.printCopyNumber = function () {
	var value = settingCacheMap[Queries.SETTING_PRINT_COPY_NUMBER];
	if (value && value != '0') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getSignatureText = function () {
	var value = settingCacheMap[Queries.SETTING_SIGNATURE_TEXT];
	if (value) {
		return value;
	} else {
		var StringConstants = require('./../Constants/StringConstants.js');
		return StringConstants.SIGNATURE_TEXT;
	}
};

SettingCache.prototype.isReceivedAmountEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_SHOW_RECEIVED_AMOUNT_OF_TRANSACTION];
	if (value && value != '0') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isBalanceAmountEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_SHOW_BALANCE_AMOUNT_OF_TRANSACTION];
	if (value && value != '0') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isAutoCutPaperEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_ENABLE_AUTOCUT_PAPER];
	if (value && value != '0') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isCurrentBalanceOfPartyEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_SHOW_CURRENT_BALANCE_OF_PARTY];
	if (value && value != '0') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isCompnayEmailEnabledOnPrint = function () {
	var value = settingCacheMap[Queries.SETTING_PRINT_COMPANY_EMAIL_ON_PDF];
	if (value && value != '0') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isPlaceOfSupplyEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_ENABLE_PLACE_OF_SUPPLY];
	if (value && value != '0') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isPODetailsEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PO_DETAILS_ENABLED];
	if (value && value != '0') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isAutoBackupEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_AUTO_BACKUP_ENABLED];
	if (value && value != '0') {
		return true;
	} else {
		return false;
	}
};
SettingCache.prototype.isEWayBillNumberEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_ENABLE_EWAY_BILL_NUMBER];
	if (value && value != '0') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isCurrentCountryGulf = function () {
	try {
		return Country.isGulfCountryByCountryCode(this.getCountryCode());
	} catch (ex) {}
	return false;
};

SettingCache.prototype.isThermalPrinterNativeLanguageEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_THERMAL_PRINTER_NATIVE_LANG];
	if (value && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.isCurrentCountryIndia = function () {
	try {
		return Country.isCountryIndia(this.getCountryCode());
	} catch (ex) {}
	return false;
};

SettingCache.prototype.isCurrentCountryNepal = function () {
	try {
		return Country.isCountryNepal(this.getCountryCode());
	} catch (ex) {}
	return false;
};

SettingCache.prototype.getTINText = function () {
	if (this.isCurrentCountryGulf()) {
		return 'TRN';
	}
	return 'TIN';
};

SettingCache.prototype.isTaxSetupCompleted = function () {
	var value = settingCacheMap[Queries.SETTING_TAX_SETUP_COMPLETED];
	if (value && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getThermalPrinterPageSize = function () {
	try {
		var value = settingCacheMap[Queries.SETTING_THERMAL_PRINTER_PAGE_SIZE];
		if (value) {
			return Number(value);
		}
	} catch (err) {}
	return ThermalPrinterConstant.INCH_3_PAGE_SIZE;
};

SettingCache.prototype.getThermalPrinterTextSize = function () {
	var value = settingCacheMap[Queries.SETTING_THERMAL_PRINTER_TEXT_SIZE];

	try {
		if (value) {
			switch (Number(value)) {
				case ThermalPrinterConstant.SMALL:
				case ThermalPrinterConstant.MEDIUM:
					return Number(value);
			}
		}
	} catch (err) {}

	return ThermalPrinterConstant.SMALL;
};

SettingCache.prototype.getThermalPrinterCustomizeCharacterCount = function () {
	try {
		var value = settingCacheMap[Queries.SETTING_THERMAL_PRINTER_CUSTOMIZE_CHARACTER_COUNT];
		if (value) {
			var integerValue = Number(value);
			if (integerValue > 0) {
				return integerValue;
			}
		}
	} catch (err) {}
	return ThermalPrinterConstant.CUSTOMIZED_DEFAULT_CHAR_COUNT;
};

SettingCache.prototype.useESCPOSCommandsInThermalPrinter = function () {
	var value = settingCacheMap[Queries.SETTING_USE_ESC_POS_CODES_IN_THERMAL_PRINTER];

	if (value && value == '1') {
		return true;
	}

	return false;
};

SettingCache.prototype.isOpenDrawerCommandEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_ENABLE_OPEN_DRAWER_COMMAND];

	if (value && value == '1') {
		return true;
	}

	return false;
};

SettingCache.prototype.printAmountDetailsInDeliveryChallan = function () {
	var value = settingCacheMap[Queries.SETTING_PRINT_AMOUNTS_IN_DELIVERY_CHALLAN];
	return value && value == '1';
};

SettingCache.prototype.printBillOfSupplyForNonTaxInvoice = function () {
	var value = settingCacheMap[Queries.SETTING_PRINT_BILL_OF_SUPPLY_FOR_NON_TAX_TXN];
	if (value && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.getDefaultPrinterType = function () {
	var value = settingCacheMap[Queries.SETTING_DEFAULT_PRINTER];
	if (value && value == '2') {
		return ThermalPrinterConstant.THERMAL_PRINTER;
	} else {
		return ThermalPrinterConstant.REGULAR_PRINTER;
	}
};

SettingCache.prototype.getDefaultThermalPrinter = function () {
	var value = settingCacheMap[Queries.SETTING_DEFAULT_THERMAL_PRINTER_ADDRESS];
	if (value) {
		return value;
	}
	return '';
};

SettingCache.prototype.getThermalPrinterExtraFooterLines = function () {
	try {
		var value = settingCacheMap[Queries.SETTING_THERMAL_PRINTER_EXTRA_FOOTER_LINES];
		if (value) {
			var integerValue = Number(value);
			if (integerValue > 0) {
				return integerValue;
			}
		}
	} catch (err) {}
	return 0;
};

SettingCache.prototype.amountToBeFixedTillSpecifiedDecimalPlaces = function () {
	var value = settingCacheMap[Queries.SETTING_PRINT_AMOUNT_TILL_SPECIFIED_PLACES];
	if (value && value == '1') {
		return true;
	}
	return false;
};

SettingCache.prototype.repeatHeaderInAllPagesForPrint = function () {
	var value = settingCacheMap[Queries.SETTING_PRINT_REPEAT_HEADER_IN_ALL_PAGES];
	if (value != null && value == '0') {
		return false;
	}
	return true;
};

SettingCache.prototype.getAmountFormatString = function () {
	if (this.amountInWordsMode() == CurrencyModeInWords.US_MODE) {
		return 'en-US';
	}
	return 'en-IN';
};

SettingCache.prototype.isAmountGroupingEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PRINT_AMOUNT_GROUPING];
	if (value != null && value == '0') {
		return false;
	} else {
		return true;
	}
};

SettingCache.prototype.printPaymentMode = function () {
	var value = settingCacheMap[Queries.SETTING_PRINT_PAYMENT_MODE];
	if (value && value == '1') {
		return true;
	}
	return false;
};

SettingCache.prototype.printAcknowledgment = function () {
	var value = settingCacheMap[Queries.SETTING_PRINT_ACKNOWLEDGMENT];
	if (value && value == '1') {
		return true;
	}
	return false;
};

SettingCache.prototype.getThermalPrintCopyCount = function () {
	var copyCount = 1;
	try {
		var value = settingCacheMap[Queries.SETTING_THERMAL_PRINTER_COPY_COUNT];
		if (value) {
			var integerValue = Number(value);
			if (integerValue > 1 && integerValue < 10) {
				copyCount = integerValue;
			}
		}
	} catch (err) {}
	return copyCount;
};

SettingCache.prototype.isPrivacyModeEnabled = function () {
	var value = settingCacheMap[Queries.SETTING_PRIVACY_MODE];
	if (value && value == '1') {
		return true;
	} else {
		return false;
	}
};

SettingCache.prototype.showItemCodeInLineItem = function () {
	var value = settingCacheMap[Queries.SETTING_SHOW_ITEM_CODE_IN_LINE_ITEM];
	return value && value == '1';
};

SettingCache.prototype.showItemHSNSACCodeInLineItem = function () {
	var value = settingCacheMap[Queries.SETTING_SHOW_ITEM_HSN_SAC_CODE_IN_LINE_ITEM];
	return value && value == '1';
};

SettingCache.prototype.showItemCategoryInLineItem = function () {
	var value = settingCacheMap[Queries.SETTING_SHOW_ITEM_CATEGORY_IN_LINE_ITEM];
	return value && value == '1';
};

SettingCache.prototype.getYoutubeSubscriptionTrialStartDate = function () {
	var value = settingCacheMap[Queries.SETTING_YOUTUBE_SUBSCRIPTION_TRIAL_START_DATE];
	if (value) {
		return value;
	} else {
		return '';
	}
};

SettingCache.prototype.getFacebookSubscriptionTrialStartDate = function () {
	var value = settingCacheMap[Queries.SETTING_FACEBOOK_SUBSCRIPTION_TRIAL_START_DATE];
	if (value) {
		return value;
	} else {
		return '';
	}
};

SettingCache.prototype.getCatalogueId = function () {
	var value = settingCacheMap[Queries.SETTING_CATALOGUE_ID];
	if (value) {
		return value;
	} else {
		return '';
	}
};

SettingCache.prototype.getCatalogueUID = function () {
	var value = settingCacheMap[Queries.SETTING_CATALOGUE_UID];
	if (value) {
		return value;
	} else {
		return '';
	}
};
SettingCache.prototype.showLowStockDialog = function () {
	var value = settingCacheMap[Queries.SETTING_SHOW_LOW_STOCK_DIALOG];
	return value != '0';
};

SettingCache.prototype.getTxnPDFDoubleThemeColor = function () {
	var value = settingCacheMap[Queries.SETTING_TXN_PDF_DOUBLE_THEME_COLOR];
	if (value) {
		return value;
	} else {
		return InvoiceTheme.DOUBLE_THEME_COLOR_COMBO_1;
	}
};

module.exports = SettingCache;