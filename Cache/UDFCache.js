var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var instance = void 0;
var firmFieldsMap = new _map2.default();
var txnFieldsMap = new _map2.default();
var partyFieldsMap = new _map2.default();
var activeTxnTypeMap = new _map2.default();
var dataLoader = void 0;
var RefreshCacheTracker = require('./../BizLogic/RefreshCacheTracker.js');
var refreshCacheTracker = new RefreshCacheTracker();
var UDFFieldsConstant = require('./../Constants/UDFFieldsConstant.js');
var UDFModel = require('./../Models/UDFModel.js');
var UDFCache = function UDFCache() {
	this.setUdfFieldsMap = function () {
		var udfMap = dataLoader.loadAllUDF();
		firmFieldsMap = new _map2.default();
		txnFieldsMap = new _map2.default();
		partyFieldsMap = new _map2.default();
		activeTxnTypeMap = new _map2.default();
		for (var i = 0; i < udfMap.length; i++) {
			var udfModel = udfMap[i];
			var firmId = Number(udfModel.getUdfFirmId());
			var fieldId = Number(udfModel.getUdfFieldId());
			var fieldTxnType = Number(udfModel.getUdfTxnType());

			if (udfModel.getUdfFieldType() == UDFFieldsConstant.FIELD_TYPE_FIRM) {
				var udfModelByFieldNumberMap = void 0;
				if (firmFieldsMap.has(firmId)) {
					udfModelByFieldNumberMap = firmFieldsMap.get(firmId);
				} else {
					udfModelByFieldNumberMap = new _map2.default();
				}
				udfModelByFieldNumberMap.set(fieldId, udfModel);
				firmFieldsMap.set(firmId, udfModelByFieldNumberMap);
			} else if (udfModel.getUdfFieldType() == UDFFieldsConstant.FIELD_TYPE_TRANSACTION) {
				var fieldMap = void 0;
				if (txnFieldsMap.has(firmId)) {
					fieldMap = txnFieldsMap.get(firmId);

					if (!fieldMap.has(fieldId)) {
						fieldMap.set(fieldId, udfModel);
					}
				} else {
					fieldMap = new _map2.default();
					fieldMap.set(fieldId, udfModel);
				}
				txnFieldsMap.set(firmId, fieldMap);
				var newFirmTxnMap = void 0;
				if (activeTxnTypeMap.has(firmId)) {
					newFirmTxnMap = activeTxnTypeMap.get(firmId);
				} else {
					newFirmTxnMap = new _map2.default();
				}
				if (!newFirmTxnMap.has(fieldTxnType)) {
					if (udfModel.getUdfFieldStatus() == 1) {
						newFirmTxnMap.set(fieldTxnType, 1);
					} else {
						newFirmTxnMap.set(fieldTxnType, 0);
					}
				} else {
					if (newFirmTxnMap.get(fieldTxnType) == 1) {} else {
						if (udfModel.getUdfFieldStatus() == 1) {
							newFirmTxnMap.set(fieldTxnType, 1);
						} else {
							newFirmTxnMap.set(fieldTxnType, 0);
						}
					}
				}

				activeTxnTypeMap.set(firmId, newFirmTxnMap);
			} else if (udfModel.getUdfFieldType() == UDFFieldsConstant.FIELD_TYPE_PARTY) {
				partyFieldsMap.set(fieldId, udfModel);
			}
		}
	};

	if (!dataLoader) {
		var DataLoader = require('./../DBManager/DataLoader');
		dataLoader = new DataLoader();
	}

	if (!instance) {
		this.setUdfFieldsMap();
		instance = 1;
		refreshCacheTracker.udfCacheRefreshNeeded = false;
	}

	this.refreshUDF = function () {
		this.setUdfFieldsMap();
	};

	this.getFirmFieldsMap = function () {
		return firmFieldsMap;
	};

	this.getFirmMap = function (firmId) {
		var map = firmFieldsMap.get(Number(firmId));
		if (map) {
			if (map.size < UDFFieldsConstant.TOTAL_FIRM_EXTRA_FIELDS) {
				var fieldNumber = [];
				map.forEach(function (value, key) {
					fieldNumber.push(Number(value.getUdfFieldNumber()));
				});
				for (var i = 1; i <= UDFFieldsConstant.TOTAL_FIRM_EXTRA_FIELDS; i++) {
					if (!fieldNumber.includes(i)) {
						var x = new UDFModel();
						x.setUdfFieldNuber(i);
						map.set(i, x);
					}
				}
			}
			var mapCopy = new _map2.default([].concat((0, _toConsumableArray3.default)(map.entries())).sort(function (a, b) {
				return a[1].getUdfFieldNumber() - b[1].getUdfFieldNumber();
			}));
			return mapCopy;
		} else {
			map = new _map2.default();
			for (var _i = 1; _i <= UDFFieldsConstant.TOTAL_FIRM_EXTRA_FIELDS; _i++) {
				var _x = new UDFModel();
				_x.setUdfFieldNuber(_i);
				map.set(_i, _x);
			}
			return map;
		}
	};

	this.getTransactionFieldsMap = function () {
		return txnFieldsMap;
	};

	this.getPartyFieldsMap = function () {
		var partyMap = partyFieldsMap;
		if (partyMap.size > 0) {
			if (partyMap.size < UDFFieldsConstant.TOTAL_EXTRA_FIELDS) {
				var fieldNumber = [];
				partyMap.forEach(function (value, key) {
					fieldNumber.push(Number(value.getUdfFieldNumber()));
				});
				for (var i = 1; i <= UDFFieldsConstant.TOTAL_EXTRA_FIELDS; i++) {
					if (!fieldNumber.includes(i)) {
						var x = new UDFModel();
						x.setUdfFieldNuber(i);
						partyMap.set(i, x);
					}
				}
			}
			var map = new _map2.default([].concat((0, _toConsumableArray3.default)(partyMap.entries())).sort(function (a, b) {
				return a[1].getUdfFieldNumber() - b[1].getUdfFieldNumber();
			}));
			return map;
		} else {
			partyMap = new _map2.default();
			for (var _i2 = 1; _i2 <= UDFFieldsConstant.TOTAL_EXTRA_FIELDS; _i2++) {
				var _x2 = new UDFModel();
				_x2.setUdfFieldNuber(_i2);
				partyMap.set(_i2, _x2);
			}
			return partyMap;
		}
	};

	this.getUdfActiveTransactionTypes = function () {
		return activeTxnTypeMap;
	};

	this.getActiveTxnType = function (firmId) {
		return activeTxnTypeMap.get(Number(firmId));
	};

	this.isUdfFieldActive = function (firmId, txntype) {
		if (activeTxnTypeMap.has(firmId)) {
			var txnMap = activeTxnTypeMap.get(firmId);
			return txnMap.has(txntype);
		} else {
			return false;
		}
	};

	this.getUdfModelByFirmAndFieldId = function (firmId, fieldId) {
		if (txnFieldsMap.has(Number(firmId))) {
			var fieldMap = txnFieldsMap.get(Number(firmId));
			return fieldMap.get(Number(fieldId));
		}
		return null;
	};

	this.getUdfFirmModelByFirmAndFieldId = function (firmId, fieldId) {
		if (firmFieldsMap.has(Number(firmId))) {
			var fieldMap = firmFieldsMap.get(Number(firmId));
			if (fieldMap.get(Number(fieldId))) {
				var fieldModel = fieldMap.get(Number(fieldId));
				return fieldModel;
			}
		}
		return null;
	};

	this.getUdfPartyModelByFieldId = function (fieldId) {
		if (partyFieldsMap.has(Number(fieldId))) {
			return partyFieldsMap.get(Number(fieldId));
		}
	};

	this.getUdfFieldNumberByFirmAndFieldId = function (firmId, fieldId) {
		if (txnFieldsMap.has(Number(firmId))) {
			var fieldMap = txnFieldsMap.get(Number(firmId));
			if (fieldMap.has(Number(fieldId))) {
				var fieldModel = fieldMap.get(Number(fieldId));
				return fieldModel.getUdfFieldNumber();
			}
		}
		return null;
	};

	this.getTxnExtraFields = function (firmId) {
		var firmSpecificTxnField = txnFieldsMap.get(Number(firmId));
		var fieldsMap = new _map2.default();
		if (firmSpecificTxnField) {
			firmSpecificTxnField.forEach(function (value, key) {
				var x = void 0;
				if (fieldsMap.has(value.getUdfFieldNumber())) {
					x = fieldsMap.get(value.getUdfFieldNumber());
					if (x.getUdfFieldStatus() == 1) {} else {
						fieldsMap.set(value.getUdfFieldNumber(), value);
					}
				} else {
					fieldsMap.set(value.getUdfFieldNumber(), value);
				}
			});
			for (var i = 1; i <= UDFFieldsConstant.TOTAL_EXTRA_FIELDS; i++) {
				if (!fieldsMap.has(String(i))) {
					fieldsMap.set(i, new UDFModel());
				}
			}
		} else {
			for (var _i3 = 1; _i3 <= UDFFieldsConstant.TOTAL_EXTRA_FIELDS; _i3++) {
				fieldsMap.set(_i3, new UDFModel());
			}
		}
		return fieldsMap;
	};

	this.getUdfDataTypeByFirmAndFieldId = function (firmId, fieldId) {
		if (txnFieldsMap.has(Number(firmId))) {
			var fieldMap = txnFieldsMap.get(Number(firmId));
			if (fieldMap.has(Number(fieldId))) {
				var fieldModel = fieldMap.get(Number(fieldId));
				return fieldModel.getUdfFieldDataType();
			}
		}
		return null;
	};

	this.getUdfPartyFieldNumberByFieldId = function (fieldId) {
		if (partyFieldsMap.get(Number(fieldId))) {
			var partyModel = partyFieldsMap.get(Number(fieldId));
			return partyModel.getUdfFieldNumber();
		}
	};

	this.getUdfPartyDataTypeByFieldId = function (fieldId) {
		var partyModel = partyFieldsMap.get(Number(fieldId));
		return partyModel.getUdfFieldDataType();
	};

	this.getFieldIdByFieldNumberAndFirmIdForFirm = function (fieldNumber, firmId) {
		var map = firmFieldsMap.get(Number(firmId));
		var fieldId = 0;
		map.forEach(function (value, key) {
			if (value.getUdfFieldNumber() == fieldNumber) {
				fieldId = value.getUdfFieldId();
			}
		});
		return fieldId;
	};

	this.hasAdditionalFieldsEnabledForTxnType = function (firmId, txnType) {
		var firmTxnMap = txnFieldsMap.get(Number(firmId));
		var hasAdditionalTxnFieldsEnabled = false;
		if (firmTxnMap) {
			firmTxnMap.forEach(function (udfModel) {
				if (udfModel.getUdfFieldStatus() == 1 && udfModel.getUdfTxnType() == txnType) {
					hasAdditionalTxnFieldsEnabled = true;
				}
			});
		}
		return hasAdditionalTxnFieldsEnabled;
	};

	if (refreshCacheTracker.udfCacheRefreshNeeded) {
		this.refreshUDF();
		refreshCacheTracker.udfCacheRefreshNeeded = false;
	};
};

module.exports = UDFCache;