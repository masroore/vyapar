var nameCacheObject;
var RefreshCacheTracker = require('./../BizLogic/RefreshCacheTracker.js');
var refreshCacheTracker = new RefreshCacheTracker();

var MemoryCleanup = require('./../Utilities/MemoryCleanup.js');
var SortHelper = require('../Utilities/SortHelper');
var sortHelper = new SortHelper();

var NameCache = function NameCache() {
	if (nameCacheObject) {
		if (refreshCacheTracker.nameCacheRefreshNeeded) {
			nameCacheObject.reloadNameCache();
		}
		return nameCacheObject;
	}
	var DataLoader = require('./../DBManager/DataLoader.js');
	this.dataLoader = new DataLoader();
	var nameCacheMap = this.dataLoader.LoadAllNames();
	var expenseCacheMap = this.dataLoader.LoadAllExpense();
	var incomeCacheMap = this.dataLoader.LoadAllIncome();

	refreshCacheTracker.nameCacheRefreshNeeded = false;

	// function to sorty objects alphabetically
	this.sortAlphabetically = function (objList) {
		objList = objList.sort(function (a, b) {
			var a1 = a['fullName'].toLowerCase();
			var b1 = b['fullName'].toLowerCase();
			if (a1 == b1) return 0;
			return a1 > b1 ? 1 : -1;
		});
		return objList;
	};

	this.getExpenseCache = function () {
		expenseCacheMap = this.dataLoader.LoadAllExpense();
	};
	this.getIncomeCache = function () {
		incomeCacheMap = this.dataLoader.LoadAllIncome();
	};
	this.loanExpenseCatExists = function (lowerCaseName) {
		var LoanAccountCache = require('./../Cache/LoanAccountCache.js');
		var loanAccountCache = new LoanAccountCache();
		var loanExpenseCategories = loanAccountCache.getLoanAccAsExpenseCategories();
		return loanExpenseCategories.find(function (name) {
			return name.toLowerCase() === lowerCaseName;
		});
	};

	this.findNameObjectByNameInMap = function (map, partyName) {
		var nameObj = null;
		if (map && partyName) {
			partyName = partyName.trim().toLowerCase();
			$.each(map, function (key, val) {
				if (val.getFullName().toLowerCase().trim() == partyName) {
					nameObj = val;
					return false;
				}
			});
		}
		return nameObj;
	};

	this.findNameModelByName = function (name) {
		var nameObj = this.findNameObjectByNameInMap(nameCacheMap, name);
		return nameObj;
	};

	this.findExpenseModelByName = function (name) {
		var nameObj = this.findNameObjectByNameInMap(expenseCacheMap, name);
		return nameObj;
	};

	this.findIncomeModelByName = function (name) {
		var nameObj = this.findNameObjectByNameInMap(incomeCacheMap, name);
		return nameObj;
	};

	this.nameExists = function (name, expense, income) {
		var nameObj = null;
		if (expense) {
			nameObj = this.findExpenseModelByName(name);
			if (!nameObj) {
				nameObj = !!this.loanExpenseCatExists(name.toLowerCase());
			}
		} else if (income) {
			nameObj = this.findIncomeModelByName(name);
		} else {
			nameObj = this.findNameModelByName(name);
		}
		return !!nameObj;
	};

	this.nameExistsWithDifferentNameId = function (name, id, type) {
		var NameType = require('../Constants/NameType');
		var nameObj = null;
		if (type == NameType.NAME_TYPE_EXPENSE) {
			nameObj = this.findExpenseModelByName(name);
			if (!nameObj) {
				nameObj = !!this.loanExpenseCatExists(name.toLowerCase());
			}
		} else if (type == NameType.NAME_TYPE_INCOME) {
			nameObj = this.findIncomeModelByName(name);
		} else {
			nameObj = this.findNameModelByName(name);
		}
		if (nameObj && nameObj.getNameId() != id) {
			return true;
		} else {
			return false;
		}
	};

	this.getNameObjectByName = function (name) {
		var nameType = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
		var txnType = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

		var NameType = require('./../Constants/NameType.js');
		var TxnType = require('../Constants/TxnTypeConstant');
		var nameObj = null;
		if (nameType == NameType.NAME_TYPE_EXPENSE || txnType == TxnType.TXN_TYPE_EXPENSE) {
			nameObj = this.findExpenseModelByName(name);
		} else if (nameType == NameType.NAME_TYPE_INCOME || txnType == TxnType.TXN_TYPE_OTHER_INCOME) {
			nameObj = this.findIncomeModelByName(name);
		} else {
			nameObj = this.findNameModelByName(name);
		}
		return nameObj;
	};

	this.reloadNameCache = function () {
		MemoryCleanup.cleanObject(nameCacheMap);
		MemoryCleanup.cleanObject(expenseCacheMap);
		MemoryCleanup.cleanObject(incomeCacheMap);
		nameCacheMap = this.dataLoader.LoadAllNames();
		expenseCacheMap = this.dataLoader.LoadAllExpense();
		incomeCacheMap = this.dataLoader.LoadAllIncome();
		refreshCacheTracker.nameCacheRefreshNeeded = false;
	};

	this.refreshNameCache = function (name) {
		var NameType = require('./../Constants/NameType.js');
		var nameId = name.getNameId();
		if (name.getNameType() == NameType.NAME_TYPE_EXPENSE) {
			expenseCacheMap[nameId] = name;
		} else if (name.getNameType() == NameType.NAME_TYPE_INCOME) {
			incomeCacheMap[nameId] = name;
		} else {
			nameCacheMap[nameId] = name;
		}
	};

	this.getListOfNames = function () {
		var listOfNames = [];
		$.each(nameCacheMap, function (key, name) {
			listOfNames.push(name.getFullName());
		});
		return listOfNames.sort(sortHelper.sort(false));
	};

	this.getListOfNamesObject = function () {
		var listOfNamesObj = [];
		$.each(nameCacheMap, function (key, name) {
			listOfNamesObj.push(name);
		});
		return this.sortAlphabetically(listOfNamesObj);
	};

	this.getListOfExpense = function () {
		var listOfExpense = [];
		$.each(expenseCacheMap, function (key, name) {
			listOfExpense.push(name.getFullName());
		});
		return listOfExpense.sort(sortHelper.sort(false));
	};

	this.getListOfIncome = function () {
		var listOfIncome = [];
		$.each(incomeCacheMap, function (key, name) {
			listOfIncome.push(name.getFullName());
		});
		return listOfIncome.sort(sortHelper.sort(false));
	};

	this.getListOfExpenseObject = function () {
		var listOfExpenseObj = [];
		$.each(expenseCacheMap, function (key, name) {
			listOfExpenseObj.push(name);
		});
		return this.sortAlphabetically(listOfExpenseObj);
	};

	this.getListOfIncomeObject = function () {
		var listOfIncomeObj = [];
		$.each(incomeCacheMap, function (key, name) {
			listOfIncomeObj.push(name);
		});
		return this.sortAlphabetically(listOfIncomeObj);
	};

	this.getNameIdOnName = function (name) {
		var nameId = 0;
		var nameObj = this.findNameModelByName(name);
		if (nameObj) {
			nameId = nameObj.getNameId();
		}
		return nameId;
	};

	this.getNameIdOnExpenseName = function (name) {
		var nameId = 0;
		var nameObj = this.findExpenseModelByName(name);
		if (nameObj) {
			nameId = nameObj.getNameId();
		}
		return nameId;
	};

	this.getNameIdOnIncomeName = function (name) {
		var nameId = 0;
		var nameObj = this.findIncomeModelByName(name);
		if (nameObj) {
			nameId = nameObj.getNameId();
		}
		return nameId;
	};

	this.findNameObjectByNameId = function (nameId) {
		var retName = null;
		if (nameCacheMap[nameId]) {
			retName = nameCacheMap[nameId];
		} else if (expenseCacheMap[nameId]) {
			retName = expenseCacheMap[nameId];
		} else if (incomeCacheMap[nameId]) {
			retName = incomeCacheMap[nameId];
		}

		return retName;
	};

	// to get just the name of the id
	this.findNameByNameId = function (nameId) {
		var retName = '';
		var nameObj = this.findNameObjectByNameId(nameId);
		if (nameObj) {
			retName = nameObj.getFullName();
		}
		return retName;
	};

	this.getListOfNamesObjectOfCurrentGroup = function (groupId) {
		var objToReturn = [];
		$.each(nameCacheMap, function (key, value) {
			if (Number(value['groupId']) != Number(groupId)) {
				objToReturn.push(value);
			}
		});
		return objToReturn;
	};

	this.getGroupItemList = function (groupId) {
		var objToReturn = {};
		$.each(nameCacheMap, function (key, value) {
			var nameObjGroupId = value.getGroupId();
			if (Number(nameObjGroupId) == Number(groupId)) {
				var nameId = value.getNameId();
				objToReturn[nameId] = value;
			}
		});
		return objToReturn;
	};

	this.getEmailList = function () {
		var listToReturn = [];
		$.each(nameCacheMap, function (k, v) {
			var email = v.getEmail();
			if (email) {
				listToReturn.push(email);
			}
		});
		return listToReturn;
	};

	nameCacheObject = this;
	return nameCacheObject;
};

module.exports = NameCache;