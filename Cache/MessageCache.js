var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
	var RefreshCacheTracker = require('./../BizLogic/RefreshCacheTracker.js');
	var MessageConfig = require('../BizLogic/TransactionMessageConfig');
	var DataLoader = require('./../DBManager/DataLoader.js');
	var refreshCacheTracker = new RefreshCacheTracker();
	var dataLoader = new DataLoader();
	var messageConfigs = {};

	var MessageCache = function () {
		function MessageCache() {
			(0, _classCallCheck3.default)(this, MessageCache);

			this.reload();
		}

		(0, _createClass3.default)(MessageCache, [{
			key: 'getByTxnType',
			value: function getByTxnType(txnType) {
				return messageConfigs[txnType];
			}
		}, {
			key: 'getField',
			value: function getField(txnType, fieldId) {
				var fields = this.getByTxnType(txnType);
				var _iteratorNormalCompletion = true;
				var _didIteratorError = false;
				var _iteratorError = undefined;

				try {
					for (var _iterator = (0, _getIterator3.default)(fields), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
						var field = _step.value;

						if (field.txnFieldId == fieldId) {
							return field;
						}
					}
				} catch (err) {
					_didIteratorError = true;
					_iteratorError = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion && _iterator.return) {
							_iterator.return();
						}
					} finally {
						if (_didIteratorError) {
							throw _iteratorError;
						}
					}
				}

				return false;
			}
		}, {
			key: 'reload',
			value: function reload() {
				if (refreshCacheTracker.messageCacheRefreshNeeded === false) return false;

				messageConfigs = {};
				dataLoader.LoadMessageConfigs().forEach(function (messageModel) {
					if (typeof messageConfigs[messageModel.txnType] === 'undefined') {
						messageConfigs[messageModel.txnType] = [];
					}
					messageConfigs[messageModel.txnType].push(new MessageConfig(messageModel.txnType, messageModel.txnFieldId, messageModel.txnFieldName, messageModel.txnFieldValue));
				});
				refreshCacheTracker.messageCacheRefreshNeeded = false;
			}
		}, {
			key: 'all',
			get: function get() {
				return messageConfigs;
			}
		}]);
		return MessageCache;
	}();

	module.exports = MessageCache;
})();