var _RefreshCacheTracker = require('./../BizLogic/RefreshCacheTracker.js');

var _RefreshCacheTracker2 = _interopRequireDefault(_RefreshCacheTracker);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var instance;
var paymentTermMap = {};
var dataLoader;
var refreshCacheTracker = new _RefreshCacheTracker2.default();
var PaymentTermCache = function PaymentTermCache() {
	if (!dataLoader) {
		var DataLoader = require('./../DBManager/DataLoader.js');
		dataLoader = new DataLoader();
	}

	if (!instance) {
		paymentTermMap = dataLoader.loadAllPaymentTerms();
		instance = 1;
		refreshCacheTracker.paymentTermCacheRefreshNeeded = false;
	}

	this.getPaymentTermObjectById = function (id) {
		return paymentTermMap.find(function (paymentTerm) {
			return paymentTerm.getPaymentTermId() == id;
		});
	};

	this.getDefaultPaymentTermObject = function () {
		return paymentTermMap.find(function (paymentTerm) {
			return paymentTerm.getPaymentTermIsDefault() == 1;
		});
	};

	this.getAllPayamentTerms = function () {
		return paymentTermMap;
	};

	this.refreshPaymentTermCache = function () {
		paymentTermMap = dataLoader.loadAllPaymentTerms();
	};

	if (refreshCacheTracker.paymentTermCacheRefreshNeeded) {
		this.refreshPaymentTermCache();
		refreshCacheTracker.paymentTermCacheRefreshNeeded = false;
	};
};

module.exports = PaymentTermCache;