var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var partyGroupMap = {};
var instance;
var dataLoader;
var RefreshCacheTracker = require('./../BizLogic/RefreshCacheTracker.js');
var refreshCacheTracker = new RefreshCacheTracker();
var PartyGroupCache = function PartyGroupCache() {
	if (!dataLoader) {
		var DataLoader = require('./../DBManager/DataLoader.js');
		dataLoader = new DataLoader();
	}
	if (!instance) {
		(function () {
			partyGroupMap = dataLoader.loadAllPartyGroups();
			instance = 1;
		})();
		refreshCacheTracker.partyGroupCacheRefreshNeeded = false;
	}

	this.reloadPartyGroup = function () {
		partyGroupMap = {};
		partyGroupMap = dataLoader.loadAllPartyGroups();
	};

	this.refreshPartyGroup = function (partyGroup) {
		if (partyGroup) {
			partyGroupMap[partyGroup['groupId']] = partyGroup;
		}
	};

	this.getPartyGroupId = function (categoryName) {
		categoryName = categoryName.toLowerCase();
		var retVal = 0;
		for (var partyGroupCur in partyGroupMap) {
			if (categoryName == partyGroupMap[partyGroupCur]['groupName'].toLowerCase()) {
				retVal = partyGroupCur;
				break;
			}
		}
		console.log(retVal);
		return retVal;
	};

	this.getPartyGroupObjectById = function (catId) {
		if (partyGroupMap[catId]) {
			return partyGroupMap[catId];
		} else {
			return false;
		}
	};

	// for displaying in item categoris page
	this.getNameGroupList = function () {
		var partyGroupList = [].concat((0, _toConsumableArray3.default)((0, _values2.default)(partyGroupMap)));
		partyGroupList.sort(function (groupA, groupB) {
			var a = groupA.getGroupName().toLowerCase();
			var b = groupB.getGroupName().toLowerCase();
			if (a == b) return 0;
			return a > b ? 1 : -1;
		});
		return partyGroupList;
	};

	if (refreshCacheTracker.partyGroupCacheRefreshNeeded) {
		this.reloadPartyGroup();
		refreshCacheTracker.partyGroupCacheRefreshNeeded = false;
	};
};
module.exports = PartyGroupCache;