var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var paymentInfoCacheObject;
var RefreshCacheTracker = require('./../BizLogic/RefreshCacheTracker.js');
var refreshCacheTracker = new RefreshCacheTracker();
var PaymentInfoCache = function PaymentInfoCache() {
	if (paymentInfoCacheObject) {
		if (refreshCacheTracker.paymentCacheRefreshNeeded) {
			paymentInfoCacheObject.refreshPaymentInfoCache();
		}
		return paymentInfoCacheObject;
	}
	var DataLoader = require('./../DBManager/DataLoader.js');
	this.dataLoader = new DataLoader();
	var paymentTypeCacheMap = this.dataLoader.LoadAllPaymentType();
	refreshCacheTracker.paymentCacheRefreshNeeded = false;

	// function to sorty objects alphabetically
	this.sortAlphabetically = function (objList) {
		objList = objList.sort(function (a, b) {
			var a1 = a['name'].toLowerCase();
			var b1 = b['name'].toLowerCase();
			if (a1 == b1) return 0;
			return a1 > b1 ? 1 : -1;
		});
		return objList;
	};

	this.paymentInfoExists = function (accountName) {
		var found = (0, _values2.default)(paymentTypeCacheMap).find(function (obj) {
			return obj.getName() === accountName;
		});
		if (found) {
			return 1;
		}
		return -1;
	};

	this.refreshPaymentInfoCache = function () {
		paymentTypeCacheMap = this.dataLoader.LoadAllPaymentType();
		refreshCacheTracker.paymentCacheRefreshNeeded = false;
	};

	this.getListOfPaymentAccountName = function () {
		return (0, _values2.default)(paymentTypeCacheMap).reduce(function (list, current) {
			return [].concat((0, _toConsumableArray3.default)(list), [current.getName()]);
		}, []);
	};

	this.getListOfPaymentAccountObjects = function () {
		return (0, _values2.default)(paymentTypeCacheMap);
	};

	this.getPaymentBankId = function (accountName) {
		var found = (0, _values2.default)(paymentTypeCacheMap).find(function (obj) {
			return obj.getName() === accountName;
		});
		if (found) {
			return found.getId();
		}
	};

	this.getPaymentBankName = function (accId) {
		var found = paymentTypeCacheMap[accId];
		if (found) {
			return found.getName();
		}
		return '';
	};

	this.getPaymentInfoObjById = function (accId) {
		return paymentTypeCacheMap[accId];
	};

	this.getPaymentInfoObjOnName = function (accountName) {
		var found = (0, _values2.default)(paymentTypeCacheMap).find(function (obj) {
			return obj.getName() === accountName;
		});
		if (found) {
			return found;
		}
	};

	this.getBankList = function () {
		var bankList = {};
		var all = (0, _values2.default)(paymentTypeCacheMap);
		var len = all.length;
		for (var i = 0; i < len; i++) {
			var current = all[i];
			if (current.getId() != 1 && current.getId() != 2) {
				var y = current.getName();
				bankList[y] = current.getCurrentBalance();
			}
		}
		return bankList;
	};

	// to get the list of bank objects for displaying in view bank UI Component
	this.getBankListObj = function () {
		var sortListOnName = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

		var all = (0, _values2.default)(paymentTypeCacheMap);
		var len = all.length;
		var returnBankList = [];
		for (var i = 0; i < len; i++) {
			var current = all[i];
			if (current.getId() != 1 && current.getId() != 2) {
				returnBankList.push(current);
			}
		}
		if (sortListOnName) {
			return this.sortAlphabetically(returnBankList);
		} else {
			return returnBankList;
		}
	};

	this.getFilterOptionsForGrid = function () {
		return (0, _values2.default)(paymentTypeCacheMap).map(function (obj) {
			return {
				label: obj.getName(),
				value: Number(obj.getId()),
				selected: false
			};
		});
	};
	paymentInfoCacheObject = this;
	return paymentInfoCacheObject;
};

module.exports = PaymentInfoCache;