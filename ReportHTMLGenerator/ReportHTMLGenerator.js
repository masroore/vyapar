var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();
var ReportHTMLStyleSheet = {
  getReportStyle: function getReportStyle() {
    var textSizeSetting = settingCache.getPrintTextSize();
    var sizeRatio = 0.4 + 0.1 * textSizeSetting;
    var textSize = 16 * sizeRatio;

    var companyNameSizeSetting = settingCache.getCompanyNameHeaderPrintTextSize();
    var companyNameSizeRatio = 0.6 + 0.1 * companyNameSizeSetting;

    return '<style> ' + '@media print {body { -webkit-print-color-adjust: exact;}}' + '.pdfReportHTMLView table { border-collapse: collapse; } ' + '.reportTableForPrint table { page-break-after:auto } .reportTableForPrint tr { page-break-inside:avoid; page-break-after:auto } .reportTableForPrint td    { page-break-inside:avoid; page-break-after:auto } .reportTableForPrint thead { display:table-header-group }' + '.pdfReportHTMLView td,.pdfReportHTMLView th{ border-bottom: 1px solid ; border-color: gray; padding: ' + 5 * sizeRatio + 'px; font-size: ' + textSize + 'px;} ' + '.pdfReportHTMLView td.noBorder { border-bottom: 0px; border-color: white; padding: ' + 5 * sizeRatio + 'px; font-size: ' + textSize + 'px;} ' + '.pdfReportHTMLView .borderBottomForTxn { border-bottom: 1px solid; border-color: gray;}' + '.pdfReportHTMLView .borderTopForTxn { border-top: 1px solid; border-color: gray;}' + '.pdfReportHTMLView .borderLeftForTxn { border-left: 1px solid; border-color: gray;}' + '.pdfReportHTMLView .borderRightForTxn { border-right: 1px solid; border-color: gray;}' + '.pdfReportHTMLView .borderColorGrey { border-color: gray;}' + '.pdfReportHTMLView p { margin: 0; padding: ' + 2 * sizeRatio + 'px;font-size: ' + textSize + 'px;}' + '.pdfReportHTMLView td.extraTopPadding { padding-top: ' + 12 * sizeRatio + 'px;} ' + '.pdfReportHTMLView td.thickBorder { border-bottom: 2px solid ;} ' + '.pdfReportHTMLView td.profitLoss { padding: ' + 10 * sizeRatio + 'px; font-size: ' + textSize + 'px;} ' + '.pdfReportHTMLView th.profitLoss { padding: 10px; font-size: ' + textSize + 'px;} ' + '.pdfReportHTMLView .profitLossNetRow {font-weight: bold; padding-top: ' + 12 * sizeRatio + 'px; padding-bottom: ' + 12 * sizeRatio + 'px; padding-left: ' + 10 * sizeRatio + 'px; padding-left: ' + 10 * sizeRatio + 'px; font-size: ' + textSize + 'px;} ' + '.pdfReportHTMLView .paddingLeft { padding-left: ' + 5 * sizeRatio + 'px;}' + '.pdfReportHTMLView .paddingRight { padding-right: ' + 5 * sizeRatio + 'px;}' + '.pdfReportHTMLView .discountTaxTable{ border-bottom: 0px; border-color: white;} ' + '.pdfReportHTMLView .boldText {font-weight: bold;} ' + '.pdfReportHTMLView .copyPrintNumberClass { padding: ' + 2.5 * sizeRatio + 'px; padding-right:0px; font-size: ' + 10 * sizeRatio + 'px;}' + '.pdfReportHTMLView .copyPrintNumberCheckBoxClass { padding: 0px; font-size: ' + 15 * sizeRatio + 'px;}' + '.pdfReportHTMLView .normalTextSize {font-size: ' + 13 * sizeRatio + 'px;}' + '.pdfReportHTMLView .bigTextSize {font-size: ' + 15 * sizeRatio + 'px;}' + '.pdfReportHTMLView .largerTextSize {font-size: ' + 18 * sizeRatio + 'px;}' + '.pdfReportHTMLView .biggerTextSize {font-size: ' + 21 * sizeRatio + 'px;}' + '.pdfReportHTMLView .extraLargeTextSize {font-size: ' + 24 * sizeRatio + 'px;}' + '.pdfReportHTMLView .companyNameHeaderTextSize {font-size: ' + 24 * sizeRatio * companyNameSizeRatio + 'px;}' + '.pdfReportHTMLView .noTopPadding { padding-top: 0px}' + '.pdfReportHTMLView .noPadding { padding:0px;}' + '.pdfReportHTMLView .topPadding{ padding-top: ' + 5 * sizeRatio + 'px;}' + '.pdfReportHTMLView .lessMargin { margin: 0; padding-top: ' + 3 * sizeRatio + 'px;padding-bottom: ' + 3 * sizeRatio + 'px;}' + '.pdfReportHTMLView .tableFooter td {border-bottom: 0px; font-weight: bold;}' + '.pdfReportHTMLView .partyAddressPadding { margin: 0; padding: ' + 3 * sizeRatio + 'px ' + 50 * sizeRatio + 'px;}' + '.pdfReportHTMLView { font-family: Segoe, "Segoe UI", Calibri, Candara, Optima, Arial, sans-serif}' + '.pdfReportHTMLView .tableCellTextAlignRight { text-align: right !important;}' + '.pdfReportHTMLView .tableCellTextAlignLeft { text-align: left !important;}' + '.pdfReportHTMLView .balancesheet td {border-bottom: none;}' + '.pdfReportHTMLView .textRight {text-align: right}' + '.pdfReportHTMLView .padLeft5 {padding-left: 5%}' + '.pdfReportHTMLView .padRight5 {padding-right: 5%}' + '</style>';
  },
  getHTMLTextForDuration: function getHTMLTextForDuration(fromDateString, toDateString) {
    return '<h3>Duration: From ' + fromDateString + ' to ' + toDateString + '</h3>';
  },
  getHTMLTextForDate: function getHTMLTextForDate(fromDateString) {
    return '<h3>Date: ' + fromDateString + '</h3>';
  },
  getHTMLTextForFirm: function getHTMLTextForFirm(firmId) {
    var SettingCache = require('./../Cache/SettingCache.js');
    var settingCache = new SettingCache();
    if (settingCache.getMultipleFirmEnabled()) {
      if (firmId <= 0) {
        return '<h3>Firm: All firms</h3>';
      } else {
        var FirmCache = require('./../Cache/FirmCache.js');
        var firmCache = new FirmCache();
        var firm = firmCache.getFirmById(firmId);
        if (firm != null) {
          return '<h3>Firm: ' + firm.getFirmName() + '</h3>';
        } else {
          return '<h3>Firm: No firm selected</h3>';
        }
      }
    } else {
      return '';
    }
  }
};

module.exports = ReportHTMLStyleSheet;