var PartyStatementHTMLGenerator = function PartyStatementHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');
	var TransactionHTMLGenerator = require('./../ReportHTMLGenerator/TransactionHTMLGenerator.js');
	var transactionHTMLGenerator = new TransactionHTMLGenerator();
	var SettingCache = require('./../Cache/SettingCache.js');
	var settingCache = new SettingCache();

	this.getHTMLText = function (listOfTransactions, startDateString, endDateString, nameString, selectedMode, printItemDetails, printDescription, printPaymentInfo, printPaymentStatus) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'>" + transactionHTMLGenerator.getCompanyHeader(null, settingCache.getTxnPDFTheme(), settingCache.getTxnPDFThemeColor(), 0.4 + 0.1 * settingCache.getPrintTextSize()) + "<h2 align='center'><u>Party statement</u></h2>" + '<h3>Party name: ' + nameString + '</h3>';

		if (nameString) {
			var NameCache = require('./../Cache/NameCache.js');
			var nameCache = new NameCache();
			var nameModel = nameCache.findNameModelByName(nameString);
			if (nameModel) {
				if (nameModel.getPhoneNumber()) {
					htmlText += '<p>Contact No.: ' + nameModel.getPhoneNumber() + '</p>';
				}
				if (nameModel.getEmail()) {
					htmlText += '<p>Email: ' + nameModel.getEmail() + '</p>';
				}
				if (nameModel.getAddress()) {
					var partyAddress = nameModel.getAddress().replace(/(?:\r\n|\r|\n)/g, '<br/>');
					htmlText += '<p>Address: ' + partyAddress + '</p>';
				}
				if (settingCache.getGSTEnabled()) {
					if (nameModel.getGstinNumber()) {
						htmlText += '<p>GSTIN: ' + nameModel.getGstinNumber() + '</p>';
					}
				} else {
					if (nameModel.getTinNumber()) {
						htmlText += '<p>' + settingCache.getTINText() + ': ' + nameModel.getTinNumber() + '</p>';
					}
				}
			}
		}

		htmlText += ReportHTMLStyleSheet.getHTMLTextForDuration(startDateString, endDateString) + getHTMLTable(listOfTransactions, startDateString, endDateString, selectedMode, printItemDetails, printDescription, printPaymentInfo, printPaymentStatus);

		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(listOfTransactions, startDateString, endDateString, selectedMode, shouldPrintItemDetails, shouldPrintDescription, shouldPrintPaymentInfo, printPaymentStatus) {
		var len = listOfTransactions.length;
		var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
		var PaymentCache = require('./../Cache/PaymentInfoCache.js');
		var paymentCache = new PaymentCache();
		var dynamicRow = '';
		var dynamicFooter = '';
		var isTransactionRefNumberEnabled = settingCache.getTxnRefNoEnabled();
		var isBillToBillEnabled = printPaymentStatus;
		var isVyaparMode = selectedMode != 1;

		var dateColumnWidthRatio = 14;
		var txnTypeColumnWidthRatio = 13;
		var txnRefColumnWidthRatio = isTransactionRefNumberEnabled ? 8 : 0;
		var txnPaymentTypeColumnWidthRatio = 8;
		var txnPaymentStatusColumnWidthRatio = isBillToBillEnabled ? 14 : 0;
		var totalAmountColumnWidthRatio = isVyaparMode ? 14 : 0;
		var receivedPaidAmountColumnWidthRatio = isVyaparMode ? 13 : 0;
		var balanceAmountColumnWidthRatio = isVyaparMode ? 13 : 0;
		var receivableBalanceAmountColumnWidthRatio = isVyaparMode ? 13 : 0;
		var payableBalanceAmountColumnWidthRatio = isVyaparMode ? 13 : 0;
		var debitAmountColumnWidthRatio = !isVyaparMode ? 15 : 0;
		var creditAmountColumnWidthRatio = !isVyaparMode ? 15 : 0;
		var runningBalanceAmountColumnWidthRatio = !isVyaparMode ? 15 : 0;

		var totalColumnWidth = dateColumnWidthRatio + txnTypeColumnWidthRatio + txnRefColumnWidthRatio + txnPaymentTypeColumnWidthRatio + txnPaymentStatusColumnWidthRatio + totalAmountColumnWidthRatio + receivedPaidAmountColumnWidthRatio + balanceAmountColumnWidthRatio + receivableBalanceAmountColumnWidthRatio + payableBalanceAmountColumnWidthRatio + debitAmountColumnWidthRatio + creditAmountColumnWidthRatio + runningBalanceAmountColumnWidthRatio;

		var totalDr = 0;
		var totalCr = 0;
		var totalRunningBalance = 0;

		var ShowPaymentStatusForTheseTransactions = [TxnTypeConstant.TXN_TYPE_SALE, TxnTypeConstant.TXN_TYPE_PURCHASE, TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN, TxnTypeConstant.TXN_TYPE_SALE_RETURN, TxnTypeConstant.TXN_TYPE_CASHIN, TxnTypeConstant.TXN_TYPE_CASHOUT, TxnTypeConstant.TXN_TYPE_ROPENBALANCE, TxnTypeConstant.TXN_TYPE_POPENBALANCE];

		if (selectedMode == 1) {
			dynamicRow += "<table class='reportTableForPrint' width='100%'><thead><tr style='background-color: lightgrey;'><th width='" + dateColumnWidthRatio * 100 / totalColumnWidth + "%'  class='tableCellTextAlignLeft'>Date</th><th width='" + txnTypeColumnWidthRatio * 100 / totalColumnWidth + "%'  class='tableCellTextAlignLeft'>Txn Type</th>";
			dynamicFooter += "<tr style='background-color: lightgrey;'><th width='" + dateColumnWidthRatio * 100 / totalColumnWidth + "%'></th><th width='" + txnTypeColumnWidthRatio * 100 / totalColumnWidth + "%'  class='tableCellTextAlignLeft'>Total</th>";
			if (isTransactionRefNumberEnabled) {
				dynamicRow += "<th width='" + txnRefColumnWidthRatio * 100 / totalColumnWidth + "%'  class='tableCellTextAlignLeft'>Ref No.</th>";
				dynamicFooter += "<th width='" + txnRefColumnWidthRatio * 100 / totalColumnWidth + "%'  class='tableCellTextAlignLeft'></th>";
			}
			if (printPaymentStatus) {
				dynamicRow += "<th width='" + txnPaymentStatusColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>Payment Status</th>";
				dynamicFooter += "<th width='" + txnPaymentStatusColumnWidthRatio * 100 / totalColumnWidth + "%'  class='tableCellTextAlignLeft'></th>";
			}

			dynamicRow += "<th width='" + debitAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>Debit</th><th width='" + creditAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>Credit</th><th width='" + runningBalanceAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>Running Balance</th></tr></thead>";
		} else {
			dynamicRow += "<table class='reportTableForPrint' width='100%'><thead><tr style='background-color: lightgrey;'><th width='" + dateColumnWidthRatio * 100 / totalColumnWidth + "%'  class='tableCellTextAlignLeft'>Date</th><th width='" + txnTypeColumnWidthRatio * 100 / totalColumnWidth + "%'  class='tableCellTextAlignLeft'>Txn Type</th>";
			dynamicFooter += "<tr style='background-color: lightgrey;'><th width='" + dateColumnWidthRatio * 100 / totalColumnWidth + "%'></th><th width='" + txnTypeColumnWidthRatio * 100 / totalColumnWidth + "%'  class='tableCellTextAlignLeft'>Total</th>";
			if (isTransactionRefNumberEnabled) {
				dynamicRow += "<th width='" + txnRefColumnWidthRatio * 100 / totalColumnWidth + "%'  class='tableCellTextAlignLeft'>Ref No.</th>";
				dynamicFooter += "<th width='" + txnRefColumnWidthRatio * 100 / totalColumnWidth + "%'  class='tableCellTextAlignLeft'></th>";
			}
			if (printPaymentStatus) {
				dynamicRow += "<th width='" + txnPaymentStatusColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>Payment Status</th>";
				dynamicFooter += "<th width='" + txnPaymentStatusColumnWidthRatio * 100 / totalColumnWidth + "%'  class='tableCellTextAlignLeft'></th>";
			}
			dynamicRow += "<th width='" + totalAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>Total</th><th width='" + receivedPaidAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>Received / Paid</th><th width='" + balanceAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>Txn Balance</th><th width='" + receivableBalanceAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>Receivable Balance</th><th width='" + payableBalanceAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>Payable Balance</th></tr></thead>";
			dynamicFooter += "<th width='" + totalAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'></th><th width='" + receivedPaidAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'></th><th width='" + balanceAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'></th>";
		}
		var totalSale = 0;
		var totalPurchase = 0;
		var totalMoneyIn = 0;
		var totalMoneyOut = 0;

		for (var i = 0; i < len; i++) {
			var txn = listOfTransactions[i];
			var printItemDetails = false;
			var printDescription = false;
			var printPaymentName = false;
			var printPaymentRef = false;
			var printPaymentInfo = shouldPrintPaymentInfo;
			var classToBeIncluded = '';
			var txnItemLines = txn.getLineItems();
			var paymentTypeId = txn.getPaymentTypeId() + '';
			var paymentInfoObj = paymentCache.getPaymentInfoObjById(paymentTypeId);
			var paymentName = paymentInfoObj.getName();
			var paymentRef = txn.getPaymentTypeReference();
			var refNo = txn.getTxnRefNumber() === '' ? '' : txn.getFullTxnRefNumber();

			var txnType = txn.getTxnType();
			var date = MyDate.getDate('d/m/y', txn.getTxnDate());
			var typeString = TxnTypeConstant.getTxnTypeForUI(txnType);
			var cashAmt = txn.getCashAmount() ? Number(txn.getCashAmount()) : 0;
			var balanceAmt = txn.getBalanceAmount() ? Number(txn.getBalanceAmount()) : 0;
			var discountAmt = txn.getDiscountAmount() ? Number(txn.getDiscountAmount()) : 0;
			var totalAmt = 0;

			if (cashAmt <= 0) {
				printPaymentInfo = false;
			}

			printItemDetails = shouldPrintItemDetails && txnItemLines && txnItemLines.length;
			printDescription = shouldPrintDescription && txn.getDescription();
			printPaymentName = printPaymentInfo && paymentName;
			printPaymentRef = printPaymentInfo && paymentRef;

			var classToBeIncludedInStatementRow = '';

			if (shouldPrintItemDetails || shouldPrintDescription || shouldPrintPaymentInfo) {
				classToBeIncludedInStatementRow += 'boldText extraTopPadding ';
				if (printItemDetails || printDescription || printPaymentName || printPaymentRef) {
					classToBeIncludedInStatementRow += ' noBorder ';
				}
				classToBeIncludedInStatementRow = " class='" + classToBeIncludedInStatementRow + "'";
			}

			var classToBeIncludedInItemDetailRow = '';
			if (printDescription || printPaymentName || printPaymentRef) {
				classToBeIncludedInItemDetailRow = " class='noBorder' ";
			}
			var classToBeIncludedInDescriptionDetailRow = '';
			if (printPaymentName || printPaymentRef) {
				classToBeIncludedInDescriptionDetailRow = " class='noBorder' ";
			}

			if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
				totalAmt = Number(cashAmt) + Number(discountAmt);
			} else {
				totalAmt = Number(cashAmt) + Number(balanceAmt);
			}

			var row = '<tr>';
			row += '<td ' + classToBeIncludedInStatementRow + '>' + date + '</td><td ' + classToBeIncludedInStatementRow + '>' + typeString + '</td>';
			if (isTransactionRefNumberEnabled) {
				row += '<td ' + classToBeIncludedInStatementRow + '>' + refNo + '</td>';
			}
			if (printPaymentStatus) {
				var paymentStatus = txn.getTxnPaymentStatus();
				var paymentStatusString = TxnPaymentStatusConstants.getPaymentStatusForUI(txn.getTxnType(), paymentStatus);

				if (!ShowPaymentStatusForTheseTransactions.includes(txn.getTxnType())) {
					paymentStatusString = '';
				}

				row += "<td align='right' " + classToBeIncludedInStatementRow + '>' + paymentStatusString + '</td>';
			}

			if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
				if (txnType == TxnTypeConstant.TXN_TYPE_SALE) {
					totalSale += totalAmt;
					totalMoneyIn += cashAmt;
				} else if (txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
					totalSale -= totalAmt;
					totalMoneyOut += cashAmt;
				} else if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE) {
					totalPurchase += totalAmt;
					totalMoneyOut += cashAmt;
				} else if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
					totalPurchase -= totalAmt;
					totalMoneyIn += cashAmt;
				} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN) {
					totalMoneyIn += cashAmt;
				} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
					totalMoneyOut += cashAmt;
				}
			}

			if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
				totalDr += balanceAmt;
				totalRunningBalance += balanceAmt;
			} else if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
				totalCr += balanceAmt;
				totalRunningBalance -= balanceAmt;
			} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_BEGINNING_BALANCE_PAYABLE || txnType == TxnTypeConstant.TXN_TYPE_POPENBALANCE) {
				totalCr += totalAmt;
				totalRunningBalance -= totalAmt;
			} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHOUT || txnType == TxnTypeConstant.TXN_TYPE_BEGINNING_BALANCE_RECEIVABLE || txnType == TxnTypeConstant.TXN_TYPE_ROPENBALANCE) {
				totalDr += totalAmt;
				totalRunningBalance += totalAmt;
			}

			if (selectedMode == 1) {
				if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
					row += "<td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(balanceAmt) + '</td><td ' + classToBeIncludedInStatementRow + '></td>';
				} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHOUT || txnType == TxnTypeConstant.TXN_TYPE_BEGINNING_BALANCE_RECEIVABLE || txnType == TxnTypeConstant.TXN_TYPE_ROPENBALANCE) {
					row += "<td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalAmt) + '</td><td' + classToBeIncludedInStatementRow + '></td>';
				} else if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
					row += '<td ' + classToBeIncludedInStatementRow + " ></td><td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(balanceAmt) + '</td>';
				} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_BEGINNING_BALANCE_PAYABLE || txnType == TxnTypeConstant.TXN_TYPE_POPENBALANCE) {
					row += '<td ' + classToBeIncludedInStatementRow + "></td><td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalAmt) + '</td>';
				}
				if (totalRunningBalance < 0) {
					row += "<td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalRunningBalance) + ' (Cr)</td>';
				} else {
					row += "<td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalRunningBalance) + ' (Dr)</td>';
				}
			} else {
				if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
					row += "<td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalAmt) + '</td>';
					row += "<td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(cashAmt) + '</td>';
				} else {
					row += '<td ' + classToBeIncludedInStatementRow + '></td><td ' + classToBeIncludedInStatementRow + '></td>';
				}
				if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
					row += '<td ' + classToBeIncludedInStatementRow + '></td>';
				} else {
					row += "<td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(balanceAmt) + '</td>';
				}
				if (totalRunningBalance < 0) {
					row += '<td ' + classToBeIncludedInStatementRow + "></td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalRunningBalance) + '</td>';
				} else {
					row += "<td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalRunningBalance) + '</td><td ' + classToBeIncludedInStatementRow + '></td>';
				}
			}
			row += '</tr>';

			dynamicRow += row;

			var noOfColumns = selectedMode == 1 ? 5 : 7;
			if (isTransactionRefNumberEnabled) {
				noOfColumns++;
			}
			if (printPaymentStatus) {
				noOfColumns++;
			}

			if (printItemDetails) {
				dynamicRow += '<tr>\n' + '  <td ' + classToBeIncludedInItemDetailRow + ' colspan="1" ></td>\n' + '  <td ' + classToBeIncludedInItemDetailRow + ' colspan="' + (noOfColumns - 1) + '">' + transactionHTMLGenerator.getItemDetailsForReport(txn) + '</td>\n' + '</tr>\n';
			}
			if (printDescription) {
				dynamicRow += '<tr>\n' + '  <td ' + classToBeIncludedInDescriptionDetailRow + 'colspan="' + noOfColumns + '"> <span class="boldText"> Description: </span>' + txn.getDescription() + '</td>\n' + '</tr>\n';
			}
			if (printPaymentName || printPaymentRef) {
				dynamicRow += '<tr><td colspan="' + noOfColumns + '">';
				if (printPaymentName) {
					dynamicRow += "<span class='boldText'> Payment Type: </span>" + paymentName;
					if (printPaymentRef) {
						dynamicRow += ', ';
					}
				}
				if (printPaymentRef) {
					dynamicRow += "<span class='boldText'> Payment Ref No.: </span>" + paymentRef;
				}
				dynamicRow += '</td></tr>';
			}
		}
		if (selectedMode == 1) {
			dynamicFooter += "<th width='" + debitAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalDr) + "</th><th width='" + creditAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalCr) + "</th><th width='" + runningBalanceAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalRunningBalance) + (totalRunningBalance < 0 ? ' (Cr)' : ' (Dr)') + '</th></tr>';
		} else {
			if (totalRunningBalance >= 0) {
				dynamicFooter += "<th width='" + receivableBalanceAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalRunningBalance) + "</th><th width='" + payableBalanceAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'></th></tr>";
			} else {
				dynamicFooter += "<th width='" + receivableBalanceAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'></th><th width='" + payableBalanceAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalRunningBalance) + '</th></tr>';
			}
		}
		dynamicRow += dynamicFooter + '</table>';
		return dynamicRow;
	};
};

module.exports = PartyStatementHTMLGenerator;