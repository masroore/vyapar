var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FirmCache = require('./../Cache/FirmCache');
var firmCache = new FirmCache();
var SettingCache = require('./../Cache/SettingCache');
var settingCache = new SettingCache();
var TxnTypeConstant = require('./../Constants/TxnTypeConstant');
var MyDate = require('./../Utilities/MyDate');
var CurrencyHelper = require('./../Utilities/CurrencyHelper');
var TransactionHTMLGenerator = require('./../ReportHTMLGenerator/TransactionHTMLGenerator');
var transactionHTMLGenerator = new TransactionHTMLGenerator();
var TaxCodeCache = require('./../Cache/TaxCodeCache.js');
var taxCodeCache = new TaxCodeCache();
var PaymentCache = require('./../Cache/PaymentInfoCache');
var PaymentInfoCache = new PaymentCache();
var MyDouble = require('./../Utilities/MyDouble');
var TaxCodeConstants = require('./../Constants/TaxCodeConstants');
var TransactionPrintSettings = require('./TransactionPrintSettings.js');
var DataLoader = require('./../DBManager/DataLoader');
var dataLoader = new DataLoader();
var TransactionPrintHelper = require('./../Utilities/TransactionPrintHelper.js');
var InvoiceTheme = require('./../Constants/InvoiceTheme');
var StateCode = require('./../Constants/StateCode.js');
var TransactionFactory = require('./../BizLogic/TransactionFactory.js');
var UDFCache = require('./../Cache/UDFCache.js');
var CustomFieldsCache = require('./../Cache/CustomFieldsCache.js');
var customFieldsCache = new CustomFieldsCache();
var headingSize = 21;

var TransactionTheme11HTMLGenerator = {
	primaryColor: '',
	secondaryColor: '',
	getCompanyHeader: function getCompanyHeader(txn, textSizeRatio, printAsDeliveryChallan) {
		var firm = firmCache.getTransactionFirmWithDefault(txn);
		var imagePath = transactionHTMLGenerator.getCompanyLogoSource(firm, this.isForInvoicePreview);
		var companyLogoHtml = '';
		var htmlModelItems = [];
		var phoneEmailAddressHtml = '';
		var html = '';

		var companyLogoElement = imagePath ? '<img src=\'' + (imagePath || '') + '\' style=\'height: ' + 84 * textSizeRatio + 'px;\'></img>' : '';

		if (this.isForInvoicePreview) {
			var classText = ' class=\'invoicePreviewEditSection ' + transactionHTMLGenerator.getEditSectionClassName('firmLogo') + '\'';
			companyLogoElement = '\n\t\t\t<div style=\'width: 100%;\' editsection=\'firmLogo\' ' + classText + '>\n\t\t\t\t' + companyLogoElement + '\n\t\t\t</div>';
		}

		companyLogoHtml += '<td style=\'vertical-align: center; text-align:left; min-width: 100px; padding-right: 20px;\' align=\'left\'>\n\t\t\t\t\t\t\t\t<div style=\'padding: 4px\'>\n\t\t\t\t\t\t\t\t\t' + companyLogoElement + '\t\t\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</td>';

		var bitMapMobile = fs.readFileSync(__dirname + './../inlineSVG/mobile.png');
		var bitMapMail = fs.readFileSync(__dirname + './../inlineSVG/mail.png');
		var bitMapLocation = fs.readFileSync(__dirname + './../inlineSVG/marker.png');
		var logoBufferMobile = new Buffer(bitMapMobile).toString('base64');
		var logoBufferMail = new Buffer(bitMapMail).toString('base64');
		var logoBufferLocation = new Buffer(bitMapLocation).toString('base64');

		var companyPhoneToBePrinted = firm.getFirmPhone() && settingCache.isPrintCompanyNumberEnabled();
		var companyEmailToBePrinted = firm.getFirmEmail() && settingCache.isCompnayEmailEnabledOnPrint();

		if (companyPhoneToBePrinted) {
			htmlModelItems.push([firm.getFirmPhone(), logoBufferMobile]);
		}
		if (companyEmailToBePrinted) {
			htmlModelItems.push([firm.getFirmEmail(), logoBufferMail]);
		}

		var companyAddress = firm.getFirmAddress().trim();
		companyAddress = companyAddress.replace(/(?:\r\n|\r|\n)/g, '<br/>');

		if (companyAddress && settingCache.isPrintCompanyAddressEnabled()) {
			htmlModelItems.push([companyAddress, logoBufferLocation]);
		}

		phoneEmailAddressHtml += '<td style=\'min-width: 300px; vertical-align: center; text-align:center; background-color: ' + this.secondaryColor + '; padding: 12px; border: 0px solid #FFFFFF; border-radius: 0px 0px 0px 150px;\'>';

		var phoneEmailAddressElement = '<div style="color: #FFFFFF; width: 100%; display: flex; justify-content: flex-end">';

		for (var i = 0; i < htmlModelItems.length; i++) {
			phoneEmailAddressElement += '<div style=\'display:flex; align-items: center; margin-right: 30px; min-width: 120px; justify-content: flex-end;\'>\n\t\t\t\t\t<div style=\'padding: 5px;\'><img src=\'data:image/png;base64, ' + htmlModelItems[i][1] + '\' style=\'height: 12px; vertical-align: center;\' /></div>\n\t\t\t\t\t<div><p align=\'left\' class=\'\' style=\'border-left: 1px solid white; padding-left: 5px; font-size: ' + 17 * textSizeRatio + 'px \'>' + htmlModelItems[i][0] + '</p></div>\n\t\t\t\t\t</div>';
		}

		phoneEmailAddressElement += '</div>';

		if (this.isForInvoicePreview) {
			var _classText = ' class=\'invoicePreviewEditSection ' + transactionHTMLGenerator.getEditSectionClassName('firmDetails') + '\'';
			phoneEmailAddressElement = '\n\t\t\t<div style=\'width: 100%;\' editsection=\'firmDetails\' ' + _classText + '>\n\t\t\t\t' + phoneEmailAddressElement + '\n\t\t\t</div>';
		}

		phoneEmailAddressHtml += phoneEmailAddressElement;

		var extraColumn = '<td width=\'5%\'></td>';

		var taxInvoiceStringHtml = '<td width=\'35%\' align=\'left\' class=\'boldText\' style=\'color: black; font-weight: normal; font-size: ' + textSizeRatio * 40 + 'px; padding: 0px; vertical-align: bottom;\'>' + this.getTransactionHeader(txn, printAsDeliveryChallan) + '</td>';

		var companyDetailsTxnTypeHtml = '<table width=\'100%\' style=\'margin-top: -1px; color: #FFFFFF;\'>';
		companyDetailsTxnTypeHtml += '<tr>';
		companyDetailsTxnTypeHtml += '<td width=\'60%\' style=\'vertical-align: center; text-align:right; background-color: ' + this.primaryColor + '; border: 0px solid #FFFFFF;border-radius: 0px 0px 200px 0px;\'>';
		companyDetailsTxnTypeHtml += '<table style=\'color: white;\' width=\'100%\'>';

		var companyNamedata = '';

		if (firm.getFirmName() && settingCache.isPrintCompanyNameEnabled()) {
			companyNamedata += '<p align=\'left\' style=\'text-transform: capitalize; font-weight: normal;\' class=\'premiumThemeHeader\'>' + firm.getFirmName() + '</p>';

			if (this.isForInvoicePreview) {
				var _classText2 = ' class=\'invoicePreviewEditSection ' + transactionHTMLGenerator.getEditSectionClassName('firmDetails') + '\'';
				companyNamedata = '\n\t\t\t\t<div style=\'width: 100%;\' editsection=\'firmDetails\' ' + _classText2 + '>\n\t\t\t\t\t' + companyNamedata + '\n\t\t\t\t</div>';
			}
			companyDetailsTxnTypeHtml += '<tr><td width=\'100%\' colspan=\'2\'>' + companyNamedata + '</td></tr>';
		}

		companyDetailsTxnTypeHtml += '<tr>';

		var companydetailsElement = '<td>';

		if (settingCache.isPrintTINEnabled()) {
			if (settingCache.getGSTEnabled()) {
				var companyGSTINToBePrinted = firm.getFirmHSNSAC();
				var companyStateToBePrinted = firm.getFirmState();

				if (companyGSTINToBePrinted) {
					companydetailsElement += '<p align=\'left\' class=\'bigTextSize \'>GSTIN: ' + firm.getFirmHSNSAC() + '</p>';
				}
				if (companyStateToBePrinted) {
					companydetailsElement += '<p align=\'left\' class=\'bigTextSize\'>State: ' + StateCode.getStateCode(firm.getFirmState()) + ' - ' + firm.getFirmState() + '</p>';
				}
			} else {
				if (firm.getFirmTin()) {
					companydetailsElement += '<p align=\'left\' class=\'bigTextSize\'>State: ' + settingCache.getTINText() + ' : ' + firm.getFirmTin() + '</p>';
				}
			}
		}

		companyDetailsTxnTypeHtml += companydetailsElement + '</td><td width=\'65%\'>';

		var udfCache = new UDFCache();
		var udfArray = firm.getUdfObjectArray();

		if (udfArray.length) {
			for (var i = 0; i < udfArray.length; i++) {
				var udfValue = udfArray[i];
				var udfModel = udfCache.getUdfFirmModelByFirmAndFieldId(firm.getFirmId(), udfValue.getUdfFieldId());
				if (udfModel.getUdfFieldPrintOnInvoice() == 1 && udfModel.getUdfFieldStatus() == 1 && udfValue.getUdfFieldValue()) {
					companyDetailsTxnTypeHtml += '<p align=\'left\' class=\'bigTextSize \'>' + udfModel.getUdfFieldName() + ' : ' + udfValue.getUdfFieldValue() + '</p>';
				}
			}
		}

		companyDetailsTxnTypeHtml += '</td>';
		companyDetailsTxnTypeHtml += '</tr></table>';
		companyDetailsTxnTypeHtml += '</td>';
		companyDetailsTxnTypeHtml += extraColumn;
		companyDetailsTxnTypeHtml += taxInvoiceStringHtml;
		companyDetailsTxnTypeHtml += '</tr></table>';

		html += '<table width=\'100%\' style=\'background-color: ' + this.primaryColor + '\'><tr>';
		html += companyLogoHtml;
		html += phoneEmailAddressHtml;
		html += '</td></tr></table>';
		html += companyDetailsTxnTypeHtml;
		// final html construction

		return html;
	},
	getCopyCountHTML: function getCopyCountHTML(txn) {
		var copyNumberHTML = '';
		var classForCheckBox = ' borderLeftForTxn borderRightForTxn borderBottomForTxn borderTopForTxn copyPrintNumberCheckBoxClass ';
		var styleForCheckBox = ' border-color: black;  color:#ffffff; ';

		if (settingCache.printCopyNumber() && (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN)) {
			copyNumberHTML = "<table width='100%' style='margin-bottom: 10px'>" + '<tr>' + "<td width='55%'></td>" + "<td width='5px' class='" + classForCheckBox + "' style='" + styleForCheckBox + "'>O</td><td width='13%'  class='copyPrintNumberCheckBoxClass paddingLeft'>Original</td>" + "<td width='5px' class='" + classForCheckBox + "' style='" + styleForCheckBox + "'>O</td><td width='13%'  class='copyPrintNumberCheckBoxClass paddingLeft'>Duplicate</td>" + "<td width='5px' class='" + classForCheckBox + "' style='" + styleForCheckBox + "'>O</td><td width='13%'  class='copyPrintNumberCheckBoxClass paddingLeft'>Triplicate</td>" + '</tr></table>';
		}

		return copyNumberHTML;
	},
	getTransactionHeader: function getTransactionHeader(txn, printDeliveryChallan) {
		var transactionFactory = new TransactionFactory();

		var transactionString = transactionFactory.getTransTypeStringForTransactionPDF(txn.getTxnType(), txn.getTxnSubType(), printDeliveryChallan, transactionHTMLGenerator.isNonTaxBill(txn));

		return transactionString;
	},
	getTxnFooterDetails: function getTxnFooterDetails(txn, textSizeRatio, printAsDeliveryChallan) {
		var leftSideHTML = this.getLeftFooterTable(txn, printAsDeliveryChallan, textSizeRatio);
		var rightSideHTML = '';
		if (!printAsDeliveryChallan || settingCache.printAmountDetailsInDeliveryChallan()) {
			rightSideHTML = this.getAmountHTMLTable(txn);
		}
		var bankAccountHTML = this.getCompanyBankDetails(txn, printAsDeliveryChallan, textSizeRatio);
		var upiDetailsHTML = transactionHTMLGenerator.getUPIPaymentDetails(txn, textSizeRatio);
		if (upiDetailsHTML) {
			if (bankAccountHTML) {
				bankAccountHTML = '<div style="display: flex;">\n\t\t\t\t<div>' + upiDetailsHTML + '</div>\n\t\t\t\t<div>' + bankAccountHTML + '</div>\n\t\t\t\t</div>';
			} else {
				bankAccountHTML = upiDetailsHTML;
			}
		}
		var signatureHTML = this.getSignatureHTML(txn, textSizeRatio);

		var htmlText = "<table width='100% '><tr>" + "<td width='60% ' valign='top ' style='padding-right: 15.400000000000002px; padding-left: 0px; padding-top: 12px; '>" + bankAccountHTML + leftSideHTML + signatureHTML + '</td>' + "<td width='40% ' valign=' top ' style='padding-right: 0px; padding-left: 15.400000000000002px; padding-top: 0px; '>" + rightSideHTML + '</td></tr></table>';

		return htmlText;
	},
	getTransactionHTMLForPreview: function getTransactionHTMLForPreview(txn, doubleColorId, pageSize, textSizeRatio, printDeliveryChallan) {
		var colors = InvoiceTheme.getDoubleThemeColors(doubleColorId);
		this.primaryColor = colors.primary;
		this.secondaryColor = colors.secondary;

		this.isForInvoicePreview = true;
		var html = this.getTransactionHTML(txn, doubleColorId, pageSize, textSizeRatio, printDeliveryChallan);
		this.isForInvoicePreview = false;
		return html;
	},
	getPartyInfoAndTxnHeaderData: function getPartyInfoAndTxnHeaderData(txn, textSizeRatio) {
		var partyInfoHeaderString = this.getPartyInfoTable(txn, textSizeRatio);
		var invoiceHtml = this.getTxnInfoTable(txn, textSizeRatio);
		var transportAndShippingHtml = this.getTransportationAndShippingTable(txn, textSizeRatio);;

		var mainHtml = "<table width='100% ' class='theme10SectionPadding '><tr>" + "<td width='33% ' valign='top ' style='padding:0px; '>" + partyInfoHeaderString + '</td>' + "<td width='32% ' valign='top ' style='padding:0px; '>" + transportAndShippingHtml + '</td>' + "<td width='35% ' valign='top ' style='padding:0px; '>" + invoiceHtml + '</td>';

		return mainHtml;
	},
	getTransportationAndShippingTable: function getTransportationAndShippingTable(txn, textSizeRatio) {
		var mainHtml = '';
		var transportationHeader = '';

		switch (txn.getTxnType()) {
			case TxnTypeConstant.TXN_TYPE_SALE:
				transportationHeader = 'Transportation Details';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE:
				transportationHeader = 'Transportation Details';
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
				transportationHeader = 'Transportation Details';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
				transportationHeader = 'Transportation Details';
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
			case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
				transportationHeader = 'Transportation Details';
				break;
			case TxnTypeConstant.TXN_TYPE_ESTIMATE:
				transportationHeader = 'Transportation Details';
				break;
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
				transportationHeader = 'Transportation Details';
				break;
			case TxnTypeConstant.TXN_TYPE_CASHIN:
			case TxnTypeConstant.TXN_TYPE_CASHOUT:
				break;
		}

		// transportation info
		var transportationDetailsHtml = '';
		var transportCustomDetailsHtml = '';
		var showTransportHeader = false;
		if (transportationHeader && customFieldsCache.isDeliveryDetailsEnable() && txn.getCustomFields()) {
			transportationDetailsHtml += '<tr style=\'color:' + this.secondaryColor + '; \'><th align=\'left\' width=\'100% \' style=\'font-size: ' + textSizeRatio * headingSize + 'px; font-weight: normal;\' class=\'\'>\n\t\t\t' + transportationHeader + ': </th></tr>';

			transportCustomDetailsHtml += "<tr><td width='100% '><table width='100% ' \">";
			try {
				var customFieldstringObj = JSON.parse(txn.getCustomFields());
				var transportationDetailArray = customFieldstringObj.transportation_details;
				for (var i = 0; i < transportationDetailArray.length; i++) {
					var customFieldModel = customFieldsCache.getCustomFieldById(transportationDetailArray[i].id);
					if (customFieldModel && transportationDetailArray[i].value) {
						transportCustomDetailsHtml += "<tr><td width='50% ' align='left' style='padding: 0px; font-weight: bold'>" + customFieldModel.getCustomFieldName() + ': ' + '</td>';
						transportCustomDetailsHtml += "<td width='50% ' align='left'>" + transportationDetailArray[i].value + '</td></tr>';
						showTransportHeader = true;
					}
				}
			} catch (e) {}
			transportCustomDetailsHtml += '</table></td></tr>';
		}
		// transportation info

		mainHtml += '<table width="100%">';
		mainHtml += showTransportHeader ? transportationDetailsHtml : '';
		mainHtml += transportCustomDetailsHtml;
		mainHtml += '</table>';

		return mainHtml;
	},
	getTxnInfoTable: function getTxnInfoTable(txn, textSizeRatio) {
		var refText = '';
		var shipAddressHeader = '';

		switch (txn.getTxnType()) {
			case TxnTypeConstant.TXN_TYPE_SALE:
				refText = 'Invoice No.: ';
				shipAddressHeader = 'Ship To';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE:
				refText = 'Bill No.: ';
				shipAddressHeader = 'Ship From';
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
				refText = 'Return No.: ';
				shipAddressHeader = 'Ship From';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
				refText = 'Return No.: ';
				shipAddressHeader = 'Ship To';
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
			case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
				refText = 'Order No.: ';
				break;
			case TxnTypeConstant.TXN_TYPE_ESTIMATE:
				refText = 'Estimate No.: ';
				break;
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
				refText = 'Challan No.: ';
				shipAddressHeader = 'Ship To';
				break;
			case TxnTypeConstant.TXN_TYPE_CASHIN:
			case TxnTypeConstant.TXN_TYPE_CASHOUT:
				refText = 'Receipt No.: ';
				break;
		}

		var party = txn.getNameRef();

		var shippingAddressHtml = '';
		if (settingCache.isPrintPartyShippingAddressEnabled() && shipAddressHeader && party.getShippingAddress()) {
			var shippingAddress = party.getShippingAddress().replace(/(?:\r\n|\r|\n)/g, '<br/>');
			shippingAddressHtml += "<tr><th width='100% '>";
			shippingAddressHtml += "<p align='left ' width='100% ' style='color: " + this.secondaryColor + '; font-size: ' + textSizeRatio * headingSize + "px; font-weight: normal;' class=''>" + shipAddressHeader + ' :' + '</p>';
			shippingAddressHtml += "<p align='left ' width='100% ' style='font-weight: normal;'>" + shippingAddress + '</p>';
			shippingAddressHtml += '</th></tr>';
		}

		var mainHtml = '';
		var invoiceCustomDetailHtml = '';
		invoiceCustomDetailHtml += "<tr><th><table width='100%' style='font-weight: normal;text-align: right;'>";
		if (txn.getFullTxnRefNumber() && txn.getFullTxnRefNumber().trim()) {
			invoiceCustomDetailHtml += "<tr style='text-align: left;'><td width='50%' style='color:#000000; font-weight: bold; padding: 0px;'>" + refText + '</td>';
			invoiceCustomDetailHtml += "<td width='50%' style='color:#000000 '>" + ' #' + txn.getFullTxnRefNumber() + '</td></tr>';
		}

		if (settingCache.getGSTEnabled() && settingCache.isPlaceOfSupplyEnabled()) {
			if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
				var placeOfSupply = txn.getPlaceOfSupply();
				if (!placeOfSupply || !placeOfSupply.trim()) {
					if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
						var firm = firmCache.getTransactionFirmWithDefault(txn);
						placeOfSupply = firm.getFirmState();
					} else {
						placeOfSupply = party.getContactState();
					}
				}
				if (placeOfSupply && placeOfSupply.trim()) {
					invoiceCustomDetailHtml += '<tr style="text-align: left;"><td width="50%" style="color:#000000; font-weight: bold; padding: 0px; ">' + 'Place of Supply: ' + '</td>';
					invoiceCustomDetailHtml += '<td width="50%" style="color:#000000;">' + StateCode.getStateCode(placeOfSupply) + '-' + placeOfSupply + '</td></tr>';
				}
			}
		}

		invoiceCustomDetailHtml += '<tr style="text-align: left;"><td width="50%" style="color:#000000; font-weight: bold; padding: 0px; ">' + 'Date: ' + '</td>';
		invoiceCustomDetailHtml += '<td width="50%" style="color:#000000;">' + MyDate.convertDateToStringForUI(txn.getTxnDate()) + '</td></tr>';

		var dueDate = txn.getTxnDueDate();
		if (dueDate != null) {
			if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER || (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE) && settingCache.isPaymentTermEnabled() && !MyDate.isSameDate(txn.getTxnDate(), txn.getTxnDueDate())) {
				invoiceCustomDetailHtml += '<tr style="text-align: left;"><td width="50%" style="color:#000000; font-weight: bold; padding: 0px; ">' + 'Due Date: ' + '</td>';
				invoiceCustomDetailHtml += '<td width="50%" style="color:#000000 ">' + MyDate.convertDateToStringForUI(dueDate) + '</td></tr>';
			}
		}

		if (txn.getUdfObjectArray()) {
			var udfCache = new UDFCache();
			for (var i = 0; i < txn.getUdfObjectArray().length; i++) {
				var value = txn.getUdfObjectArray()[i];
				var fieldModel = udfCache.getUdfModelByFirmAndFieldId(txn.getFirmId(), value.getUdfFieldId());
				if (fieldModel.getUdfFieldPrintOnInvoice() == 1 && value.getUdfFieldValue() && fieldModel.getUdfTxnType() == txn.getTxnType() && value.getUdfRefId() == txn.getTxnId()) {
					var displayValue = value.getDisplayValue(fieldModel);
					invoiceCustomDetailHtml += '<tr style="text-align: left;"><td width="50%" style="color:#000000; font-weight: bold; padding: 0px; ">' + fieldModel.getUdfFieldName() + '</td>';
					invoiceCustomDetailHtml += '<td width="50%" style="color:#000000 ">' + displayValue + '</td></tr>';
				}
			}
		}

		if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
			if (txn.getTxnReturnRefNumber()) {
				invoiceCustomDetailHtml += '<tr style="text-align: left;"><td width="50%" style="color:#000000; font-weight: bold; padding: 0px; ">' + (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN ? 'Invoice No.: ' : 'Bill No.: ') + '</td>';
				invoiceCustomDetailHtml += '<td width="50%" style="color:#000000 ">' + txn.getTxnReturnRefNumber() + '</td></tr>';
			}
			if (txn.getTxnReturnDate()) {
				invoiceCustomDetailHtml += '<tr style="text-align: left;"><td width="50%" style="color:#000000; font-weight: bold; padding: 0px; ">' + (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN ? 'Invoice date: ' : 'Bill date: ') + '</td>';
				invoiceCustomDetailHtml += '<td width="50%" style="color:#000000 ">' + MyDate.convertDateToStringForUI(txn.getTxnReturnDate()) + '</td></tr>';
			}
		}

		if (txn.getPODate()) {
			invoiceCustomDetailHtml += '<tr style="text-align: left;"><td width="50%" style="color:#000000; font-weight: bold; padding: 0px; ">' + 'PO date: ' + '</td>';
			invoiceCustomDetailHtml += '<td width="50%" style="color:#000000 ">' + MyDate.convertDateToStringForUI(txn.getPODate()) + '</td></tr>';
		}
		if (txn.getPONumber()) {
			invoiceCustomDetailHtml += '<tr style="text-align: left;"><td width="50%" style="color:#000000; font-weight: bold; padding: 0px; ">' + 'PO number: ' + '</td>';
			invoiceCustomDetailHtml += '<td width="50%" style="color:#000000 ">' + txn.getPONumber() + '</td></tr>';
		}
		if (txn.getEwayBillNumber()) {
			invoiceCustomDetailHtml += '<tr style="text-align: left;"><td width="50%" style="color:#000000; font-weight: bold; padding: 0px; ">' + 'E-way Bill number: ' + '</td>';
			invoiceCustomDetailHtml += '<td width="50%" style="color:#000000 ">' + txn.getEwayBillNumber() + '</td></tr>';
		}

		if (settingCache.getReverseChargeEnabled() && (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN)) {
			invoiceCustomDetailHtml += '<tr style="text-align: left;"><td width="50%" style="color:#000000; font-weight: bold; padding: 0px; ">' + 'Reverse charge: ' + '</td>';
			invoiceCustomDetailHtml += '<td width="50%" style="color:#000000 ">' + (txn.getReverseCharge() ? 'Yes' : 'No') + '</td></tr>';
		}

		invoiceCustomDetailHtml += '</table></td></tr>';

		mainHtml = '<table width=\'100%\'>';
		mainHtml += invoiceCustomDetailHtml;
		mainHtml += shippingAddressHtml;
		mainHtml += '</table>';
		return mainHtml;
	},
	getPartyInfoTable: function getPartyInfoTable(txn, textSizeRatio) {
		var partyDetailHTML = '';
		var partyHeaderTextHtml = '';
		var partyHeaderText = 'Party details';

		switch (txn.getTxnType()) {
			case TxnTypeConstant.TXN_TYPE_SALE:
				partyHeaderText = 'Bill To';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE:
				partyHeaderText = 'Bill From';
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
				partyHeaderText = 'Return From';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
				partyHeaderText = 'Return To';
				break;
			case TxnTypeConstant.TXN_TYPE_CASHIN:
				partyHeaderText = 'Received From';
				break;
			case TxnTypeConstant.TXN_TYPE_CASHOUT:
				partyHeaderText = 'Paid To';
				break;
			case TxnTypeConstant.TXN_TYPE_EXPENSE:
				partyHeaderText = 'Expense For';
				break;
			case TxnTypeConstant.TXN_TYPE_OTHER_INCOME:
				partyHeaderText = 'Other Income From';
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
				partyHeaderText = 'Order From';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
				partyHeaderText = 'Order To';
				break;
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
				partyHeaderText = 'Delivery Challan For';
				break;
			case TxnTypeConstant.TXN_TYPE_ESTIMATE:
				partyHeaderText = 'Estimate For';
				break;
		}

		partyHeaderText += ':';
		partyHeaderTextHtml += "<tr class='' style=' color: " + this.secondaryColor + ";'>" + "<th align='left' width='100%' style='color: " + this.secondaryColor + '; font-weight: normal; font-size: ' + textSizeRatio * headingSize + "px ;'>" + partyHeaderText + '</th></tr>';

		var party = txn.getNameRef();
		var partyFullName = '';
		if (txn.getDisplayName() && txn.getDisplayName().trim()) {
			partyFullName = txn.getDisplayName().trim();
		} else {
			partyFullName = party.getFullName();
		}

		partyFullName = partyFullName.replace(/(?:\r\n|\r|\n)/g, '<br/>');

		var partyNameHtml = '';
		partyNameHtml += "<tr class='boldText'><td style='font-size: " + textSizeRatio * 30 + "px;'>" + partyFullName + '</td></tr>';

		var mobileNumber = party.getPhoneNumber();
		var partyAddress = txn.getBillingAddress() || party.getAddress();
		var tinNumber = party.getTinNumber();
		var gstinNumber = party.getGstinNumber();

		var partyAddressHtml = '';
		if (partyAddress) {
			partyAddress = partyAddress.replace(/(?:\r\n|\r|\n)/g, '<br/>');
			partyAddressHtml += '<tr><td>' + partyAddress + '</td></tr>';
		}

		var partyInfoHtml = '';
		partyInfoHtml += '<tr><td style="margin: 0px; padding: 0px;"><table>';

		if (mobileNumber) {
			partyInfoHtml += '<tr><td width="50%" style="font-weight: bold">Contact No.:</td><td width="50%">' + mobileNumber + '</td></tr>';
		}

		if (settingCache.isTINNumberEnabled()) {
			if (settingCache.getGSTEnabled()) {
				if (gstinNumber) {
					partyInfoHtml += '<tr><td width="50%" style="font-weight: bold">GSTIN Number:</td><td width="50%">' + gstinNumber + '</td></tr>';
				}
			} else {
				if (tinNumber) {
					partyInfoHtml += '<tr><td width="50%" style="font-weight: bold">' + settingCache.getTINText() + ' Number: ' + '</td><td width="50%">' + tinNumber + '</td></tr>';
				}
			}
		}

		if (settingCache.getGSTEnabled()) {
			var placeOfSupply = party.getContactState();

			if (placeOfSupply && placeOfSupply.trim()) {
				partyInfoHtml += '<tr><td width="50%" style="font-weight: bold">State: </td><td width="50%">' + StateCode.getStateCode(placeOfSupply) + '-' + placeOfSupply + '</td></tr>';
			}
		}

		var partyUDF = party.getUdfObjectArray();
		var UDFCache = require('./../Cache/UDFCache.js');
		var udfCache = new UDFCache();
		if (partyUDF && partyUDF.length > 0) {
			for (var i = 0; i < partyUDF.length; i++) {
				var value = partyUDF[i];
				var model = udfCache.getUdfPartyModelByFieldId(value.getUdfFieldId());
				if (value.getUdfFieldValue() && model.getUdfFieldPrintOnInvoice() == 1) {
					var displayValue = value.getDisplayValue(model);
					partyInfoHtml += '<tr><td width="50%" style="font-weight: bold">' + model.getUdfFieldName() + ': ' + displayValue + '</td></tr>';
				}
			}
		}

		partyInfoHtml += '</table></td></tr>';
		// party number and custom detail html

		// final party details html
		partyDetailHTML = "<table width='100%'>";
		partyDetailHTML += partyHeaderTextHtml;
		partyDetailHTML += partyNameHtml;
		partyDetailHTML += partyAddressHtml;
		partyDetailHTML += partyInfoHtml;
		partyDetailHTML += '</table>';
		// final party details html

		return partyDetailHTML;
	},

	getTransactionHTML: function getTransactionHTML(txn, doubleColorId, pageSize, textSizeRatio, printDeliveryChallan) {
		var colors = InvoiceTheme.getDoubleThemeColors(doubleColorId);
		this.primaryColor = colors.primary;
		this.secondaryColor = colors.secondary;

		var bodyHtml = '';
		bodyHtml = transactionHTMLGenerator.getExtraTopSpace(textSizeRatio) + this.getCopyCountHTML(txn) + this.getCompanyHeader(txn, textSizeRatio, printDeliveryChallan) + this.getPartyInfoAndTxnHeaderData(txn, textSizeRatio);

		var transactionType = txn.getTxnType();
		if (transactionType == TxnTypeConstant.TXN_TYPE_SALE || transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE || transactionType == TxnTypeConstant.TXN_TYPE_EXPENSE || transactionType == TxnTypeConstant.TXN_TYPE_OTHER_INCOME || transactionType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || transactionType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || transactionType == TxnTypeConstant.TXN_TYPE_ESTIMATE || transactionType == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
			bodyHtml += transactionHTMLGenerator.getItemDetails(txn, InvoiceTheme.THEME_11, this.secondaryColor, textSizeRatio, printDeliveryChallan);
		}
		bodyHtml += this.getTxnFooterDetails(txn, textSizeRatio, printDeliveryChallan);
		bodyHtml += this.addAcknowledgment(txn, textSizeRatio);
		bodyHtml = this.addBottomDesign(bodyHtml);
		return bodyHtml;
	},

	addBottomDesign: function addBottomDesign(bodyHTML) {
		bodyHTML = '\n\t\t\t<table width=\'100%\'>\n\t\t\t\t<tbody>\n\t\t\t\t\t<tr style=\'page-break-inside: auto !important;\'>\n\t\t\t\t\t\t<td class=\'noPadding noBorder\' width=\'100%\'>\n\t\t\t\t\t\t\t<div>' + bodyHTML + '</div>\n\t\t\t\t\t\t</td> \n\t\t\t\t\t</tr>\n\t\t\t\t</tbody>\n\t\t\t\t<tfoot><tr class=\'footerRow\'><td class=\'brandingDummyFooterForSpacing noBorder\'></td></tr></tfoot>\n\t\t\t</table>\n\t\t\t<table class=\'brandingFooter hideOnScreen\' width=\'100%\' style=\'\'>\n\t\t\t\t<tr width=\'100%\' style=\'height: 100px;\'></tr>\n\t\t\t\t<tr>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t<div style=\'width: 100%;\'>\n\t\t\t\t\t\t\t<div class="brandingFooter" style=\'height: 25px; background-color: ' + this.secondaryColor + ';  width: 100%;\'></div>\n\t\t\t\t\t\t\t<div class=\'brandingFooter\' style=\'background-color: ' + this.primaryColor + ';width: 60%;border-radius: 50px 0px 0px 0px; height: 45px;float: right; bottom: 0; right: 0;\'>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t</table>';

		return bodyHTML;
	},
	addAcknowledgment: function addAcknowledgment(txn, textSizeRatio) {
		if (settingCache.printAcknowledgment() && (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHOUT)) {
			var bodyHtml = '<br/>';

			bodyHtml += "<div style='page-break-inside: avoid; padding-top: 15px; position: relative;'><div style='border-top: 2px dashed #ABB2B6; padding-top: 1px;'>";

			bodyHtml += "<img style='height: 20px; position: absolute;margin-left: 25px; margin-top: -13px;'" + 'src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAIACAYAAAD0eNT6AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAB3RJTUUH5AUEBTYR4+SYfwAAVKVJREFUeNrt3Xl4nGW5BvD7eWeSLikIWCiLiuhBkKosnUnS0mQyQAWSCRWU4lGKiAooLrgAKio9elzAHVERBY8UFaMs7UwClZbJpG1IZqZl0YKAQFHElrVA0yXJfM/5Iy22pUsmmckz38z9u67zT2mT+xtz8j3zzne/L0BERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERFRcYh2gXLStWlVds2bzQV5VborksLcGsI8o9oZgb6g37rX/wr2oqgMCXa/i+sTl1mq/t2bjga97ds7Uqf3W10NEROWNA0Ae7uzu3s8brHq7p96RIu5tqt4RAneYBz1QgAMK+K2eB/AkRB4TT/+uoo+pF3jYq9p0/+yZM1+xfh2IiMj/OADsQkdPz95ev0yH56ZBdOj/VA41jqWAPgZx9yr0XqdYPtHrS0ej0U3WrxcREfkLB4AtFi26vyY3rr9RHZpUtQnAcQCC1rmGYTNEM+LJUoV314GTgstCodCAdSgiIiptFT0AdCR73qAu0KLwTgPkBADjrTMVwMtQ/bNCOgIumGhuPO5Z60BERFR6Km4AWLC4Z0ow6M5S0Q8AUlvmr0FOIEsg+vvqze62WbNCL1kHIiKi0lDON79XdXQ8Ok4nvnCGJzJXgFnwx9J+oW0G0A7R67INtXfNE/GsAxERkZ2yHgDiS7oPQTDwMUA+XuCn9P3uX4Dc5LzcNc3R+qeswxAR0dgrywEg0dnboJBLRdAMwFnnKWH9gN4EeN+LRaY/ZB2GiIjGTtkMAKoqiVQmJoLLABxvncdnPEDjgFwVi9R2W4chIqLiK4sBoH1pbwye+6ZC32WdpQwsgvMujzXUr7AOQkRExePrASDe2TvdiXxbgYh1lrIjWJyD9/nZjfUPWEchIqLC8+UA0LEk+1YNej9UoNU6S5nLKXBtVdXg106ZMeMF6zBERFQ4vhoA2rq7J0wYCFwmkMtQHpv2+MULCnx949rV18yZMydnHYaIiEbPNwNAvDPdKoJrALzJOkvlkhUO+FhzJHyvdRIiIhqdkh8Abkveu0+VG7gSwPnWWQgAMAjI913fPlc0Nx++2ToMERGNTEkPAO1dmfeq6k8BTLHOQq/xFxV3bmtjaKV1ECIiyl9JDgCLFt1fMzhh809U8WHrLLRbAwJ8LdMYvopbCxMR+UvJDQAdXT1Heer+AOAd1lloeBS42wsE586eedzT1lmIiGh4Smqb3HhX74WeuhXgzd9XBDghkBtcGe9Kn2idhYiIhqckVgCSyWSwTyb9AKKfss5Co5ID9PJYpO5K6yBERLR75gNAPJmdLAHvj1A0WWehAhH9/YZg7iNzZszYaB2FiIh2znQAWNDV866guoQCb7R+IajQtDc3oLNnn1S/1joJERG9ltkAsLArHXGK2wHsY/0iUNE8IU5PaWmoe8Q6CBERbc/kIcD2rsxsp7gDvPmXu8PUk+54Z+906yBERLS9MR8A4p3pj6nqLQAmWF88jYnXi+DPbAgQEZWWMR0A4p3pj4ngWgAB6wunsSSTRNHevrQ3Zp2EiIiGjNkzAIlU5nxArx3L70klpx+QM2OR8ELrIERElW5MVgDaU5mLePMnANWAtrUn06dYByEiqnRFvyG3d/b+t4rchBLbdZBMbYTqybGmuqXWQYiIKlVRB4COVO8JHqQDwDjrC6WS85J4aGqJ1t5nHYSIqBIVbQBY2JkOO9G7AZlkfZFUsv4d8HIzTo1OX20dhIio0hRlAIgvy74JOS8jwAHWF0gl76+5QP+M2TNnvmIdhIiokhT8c/m27u4JkvNu4c2fhukdAa+6ra2tjdVQIqIxVNABQFVlYn/gRgAh6wsjH1GcUnPgYVdZxyAiqiQFHQDaU5mvQuR91hdF/qOqn0t0Zd5vnYOIqFIU7BmALYf7LAF3+aMR0/WAVxuLTH/IOgkRUbkryADQ0bVyf08H7wNwsPUFke/9VWtcXWsotME6CBFRORv1RwCqKjkd/A1486fCeIf06dXWIYiIyt2oB4BEKv1ZAU61vhAqJ/qRRCpzmnUKIqJyNqqPAO5Yljkil9N7waN9qfCezQ1475x9Uv1a6yBEROVoxCsAbW1tgVxOfw3e/Kk49g9UuZ9bhyAiKlcjHgBqDjzscwCmW18AlbXT27sy77UOQURUjkb0EUA81XO4wD0AYLz1BVDZ+5cb5x3VXF//snUQIqJyEhzJPxLIj8Cb/3D0KdAnwHoA6yC6AeomALoXgEkAagC8zjpkiTvE2xT4XwCftg5CRFRO8l4BaF/aG1NP4tbBS4v+XSFpUTysgocDkEcCm6ofOfnko/v29C+z2WzV2o25w9RzR4rgCM/Tt4ngGADHgpsqbZVzkHBzJHyvdRAionKR1wCQTCbH9wVq/grFW62DG3taIIsV3t2DkLvfE6n9Z6G/wW3Je/epDgxGPNUTBDgBwDusL9qWJGOR8AnWKYiofCSTyWBfMLhXy8yZ60RErfOMtbwGgHhX+kui+JZ1aCOvQHEL4N2YjdSl5ol4Y/nNFy5LT3U5mQvoBwG8wfrFsCGzY5HwQusURFS6ksnk+I1u0ts96GEADlPgMIG+AZDJqpgsgskYaq9N3MWXeBnAOoGsU9EXFPpP58lTCjyl4v0dgeDfWmeG/mF9nYUw7AHgzu7u/QYHgo8B2Mc69NiSFRD9kU50t5bC9rTzVF2oM3sCnH4CwGwU4UjnUqWKhw+a5N4ZCoUGrLMQkb2OjkfHYdK6kKc6HYppELwLwNswwufbhk/XC+QhFWTVQ0ZUMhuefeKhOXPm5Kxfk3wMewBIpDLfAfQy68BjaKl4+FZLtPZO6yC7kkhm3qFOvyTAWaiY5wXkglgkfJ11CiIae/NU3bSl2ZBTnKrQWRg6en6cda4h8qIoUgrvbud0SXNj/YPWifaYeDh/acGylQcHcoOPYtdLJmVEewVyaUuktss6yXAtSK74r0Bg8OtQ+W/rLEUn+uSG109625ypU/utoxBR8bWtWlU98fn1s0TdHIU2A5hsnWmYHofqAs/Jgk1rVi8rxdWBYQ0A8VT6GgEusg5bZC8A8qVsY+hXY/35fqEkOnuikMBPAX27dZbi4ioAUbmLJ7MzxeU+DLjTAd3XOs8o/RvA79Tp/NaGuvutw2y1xwFg0fLlBwwMVq1G+W75q4D+Wr3AZa3R0HPWYUarbdWq6prn+j6nwFdRris2ok8eODFwOJ8FICovtyXv3adaBs5R4HwIplrnKZL7APl51abq3w6nKl5MexwA4qn0NwT4imXIIl7+iyreea2NdbdbJym0odYA2gAcZZ2lKARzY421N1nHIKLRiy/LvkkGcxdD8DFAJlnnGSMvCfB/zsv96NTo9NUWAXY7ALQlV02a6PqeBLCfRbji0l4ZrDqr5cTjnrROUiyLFt1fMzBu8zUQnGudpQhWxiK106xDENHIxVM9h4u6r0FwFoAq6zxGBlX09wHod8b6wcHdDgDxVPozAvzI9KUpAgWuPqjGfaFSlpDbu9LnquJalMzTsoWhnkRbo+FO6xxElJ/bU+k3BhRfk6E3J0Wu7PmGJ8AfPXhfbY3UPzoW33CXA4CqSiKVeUgER1i/KgWkAC6NRWq/Zx1krMW70ieK4jYAe1lnKaCFsUjtbOsQRDQ8bclVkya4vssFuBg8T2ZXBgRyfUDk66c0hv5dzG+0ywGgI9V7ggdZYv1KFFBORC9saaz7lXUQK4mlPdPUcx0CHGCdpUByg8BhxdiKmYgKK96ZbhWnP4HKodZZfKJPod/bOHnSt4pVe97lLnIe5ALrqy+gzeL0PZV88weAWEP9Cs8LHA/RcnnuIVAFfMg6BBHt2h3Je96cSKUXi2Ahb/55qRHIFROf7Vu5sCsdKcY32OkKwILFPVMCVe4fAKqtX4EC8KD6/lhT3R+tg5SKO5ZljsjldBn8s6HG7jyRbQz/l1/3biAqV6oq7anMxyF6ZQU92V8sqsDPNno1X5wTnbq+UF90pysArsq9H+Vx8wcUn+TNf3unzgw/rCItgBbsB8nQYbVd6SbrEET0H7en0m9MdGUWQ/BT3vwLQgS4aKLreyCezDQV6ovudAAQaFlsKSuQebGm2p9b5yhFrY3htBs6TGizdZbR8kTeb52BiIYkOnvPDAL3bTnGnArrMHF6dzyV/nE2mx11bfI1HwEsTPUe5iCPIc+jgkuO6p9iTXVnWscodYnO3k9B5GrrHKP0/IE17qBKqXUSlaLbkvfuUxXo/1lFnElSGpY6z/tAc7T+qZF+gdesADh1/w2/3/yBx8cNBD5qHcIPYk11PwFwq3WOUXr92le8E61DEFWqRCo9o8oNrODNf0w1eM7dm+jsiY70C7z2IwDBGdZXNUr94ulZs2aFXrIO4hcDXtVHADxhnWM01ClXe4jGWEfHo+MSqfT3ACwF8BbrPBVoMsTdGe9Mf2wk/3i7AeDOruxBgB5nfUWjInJZS7Quax3DT06PHrtOhz5HH7TOMnLSrKp+X7ki8o32VPadWrMuDeDz2E2lnIquWgTXJVLpH85Tzet/h+3+8qDmmuHr5X9ZsWHNEz+xTuFHrY3hNIBrrHOMwoGJpSuOtQ5BVO627BJ7vsLrUei7rPPQqy4OdWX+mEwmh73D4nYDgEJOtb6CUfDE8y6cM2dOzjqIX7lx3hUAnrbOMVICr8U6A1E5e7XeB/0FyvW4cX87o8/VJBYsWzasLd9fHQCSyWRQgFnW6UdM8Asu/Y9Oc339yxD5vHWOEVOcYh2BqFwlujLvDwIPsN5X8k4M5KrvGM4Q8OoAsAETjwGwt3XyEXoesvly6xDlINYYvhmCTuscIxSKZ7N8V0JUQLcl790nnkr/Fqq/B7CPdR4aluMDueo7O3p6dntP/89HAAHXYJ14xBRXxxoaXrSOUS48YJ51hhGqDvTl6q1DEJWLRGdPtMoN3C/AB6yzUN5meJvdHYsW3V+zq7/wnwHAQ6N12hF6GYHNfPCvgE5rrE1BdJl1jpHIwbc/x0QlI5vNVsVTvfMgbjGAN1nnoRGbMTBh8+1tq1btdGt/Bww91amix1snHRn5Od/9F57k5JvWGUaUGzLTOgORn7Wnsu9c26dZgVwB1vv8T3HShOfX/2pnNWkHAHd29hwKYH/rnCOwqSrY/wPrEOWoJVp7J4CV1jnyJ8dxPwCi/M1TdfHO3s8pvDTrfeVFVOa2d2W/seOfOwAYFOfL/rQAC04+/vhnrHOULcX11hFGEHrfeFf6zdYpiPykPZk+cFpXJiEi3wcw7B45+Yl+ub2zd7utmh0AiJNjrKON6HJE51tnKGfB6sGb4cPTAh3ElwMtkYVEZ++Z6rBKAD/vA0N7Jipyw8LOdHjrHwx9vqM4xjrZCKytyW1YZB2inJ0yY8YLADqsc+TNnz/PRGPq1XqfSBuA/azz0JgY75z+8c7u7v2ArQOAYKp1qhH4fTQa9fHe9f6g6sNVFsER1hGISlk8mWliva9CqRya66+6XlUlmM1mq9b0eYdaZ8qXALdZZ6gEk3TDHX1Sswn++lzwbdYBiEpRR8ej47yaF/8X0M+BT/hXLBV9T3sq/Un3r1fkUABB60B52iB9+/Zah6gE0Wh0EyD3WOfI0+FsAhBtL5HMvGPL6X1fAG/+JJjqAoHBt1rnGIHlzc2H++7hNL8S6N3WGfJUs3D5vQdZhyAqBaoq7V2Zz8JphvU+AvC8iLwvFqm70MGTt1inyZcKktYZKokCfhsAEFTvjdYZiKx1JHve0N6VuUtVfwB/fYxHxbEoKO6dLY3hW4ChpX//vVPytNM6QiU5sMZl1vR5G+Cj4z+9XO5g6wxEluKp9Fke5OeA7mudhcxtVJFLYw2hn4qIbv1DB4cDrZPlKzBeV1lnqCShUGgAwCPWOfLhxHEAoIrU0dOzdyKV/oUAN/PmTwD+khOvvrUxfM22N38AcFA9wDpdnv7dXF//snWIiqP4m3WE/OL6cGWLaJTiyUyTt9n9BcD51lnIXE4U3z6wxk2b3Vj/wM7+QlAhU3z1uLTgYesIlUhFHxb46CdFlBubUMXo6Hh0XG7iC98Q0c+DT/gT8IR67pxYNLTbU12DAuxjnTQvqhwADDjgER39lxk7HvayjkA0Fjq6eo7K6Ys3CbfAJgAQ+SNk0wWtkT2fkhuE6AT4qDItkNXWGSqRp3hC/PNjAhG8zjoDUTGpqnQszV7sqX5L+IQ/DdX7Ltj6hP9wBKHimye7AUAV/PzfgAblZclZp8gjLzgAUPnaUu/7PwAnWmehkrAoFwieN3vmcU/n84+CAGqsk+dDIK9YZ6hEAvcK4FnHGDYFqq0zEBVDvCvzPk/1WgCvt85C5jYBOi/bWPvdeSJ5/4IOAhhnfQX58OCtt85QiUQ3vuKnHxUHBKwzEBXSXXdlX7e52vspVD9onYVKwkogd3YsMv2hkX4B/z0t6oQfARioGRz01cqLcgCgMpJIpWdsHuetAMCbP3kKXL1hcs300dz8gaEVAA8++mUpnuefdegy0tTUlGvvyijgky6g+udnmmhXttb7AHwe6sM3bFRoq6F6TmtT3dJCfLGtA4BvKNwk6wyVaOHy5ZMCqPbHzR8ABD56ZJHotRLJzDs8efEmETnaOguVhN+4cd6nC7kRnu8GACfKfreJiXsBg9Yh8rHROgDRSKiqtC/Nfgaq3wbrfQQ8ryIXtjaG/1ToLxwE0AdggvUVDhdXAGxUI7eXz95Sb7IOQJSv9mT6wPau9PWANFtnoRIgWKwDg+e2njjjX8X48kEALwOYbH2dw8cVAAsDg7q3888HAFDlCgD5S7wr8z5VvRYQ1vtoqN7XMLJ633AFFXjJR7/XISI85c1AwMnBqv7ZDFgA1kXJF+66K/u6zVXeNVA92zoLlYSVTry5zY31Dxb7GwUd8LJ/fq0DqjjCOkMl8jzvCPHTXsCQZ60TEO1JvLN3+mbnzYfirdZZyJwq8JONk2sumTN1av9YfMOgAs9bX3V+PA4ABkTc2wBfjYrPWScg2pW2Vauqa57f8A1V/QLrfYQC1/uGywGa197B9uSwjo5H/bMlXdnQI60T5IkrAFSSOrp6jpr4XN89qnop/LgZGxWWyB/hNh8XG+ObPwAEVeRp8dMbOyAwuPeL/wVglXWQCuOrlRdx8ox1BqJtba33eaz30ZCi1fuGy0G1KPWCoobOyfHWGSpJPJk9Er5qigAqudXWGYi22lLvS0D1h+DNnwSLdXDwaMubPwAEA8BTvtoJCABUowCus45RKUS8qHWGfOlg8EnrDEQA0J5Kz1Hg54DsZ52FzG0UxRebG8M/EbFfew/2Qx4NWqfIkwpOUFUphRewMugJfjkCYItXWqMhPgRIpjp6evb2NrvvKnC+dRYqCX/NiffB2ZH6B6yDbOVmN4afwtBugL4hwAHx5ZmjrHNUgnmqDiJN1jnytNo6AFW2hV3piNcvD4A3fwJyCnxnw+SaabMbS+fmDwBBEdF4Kv2IAMdah8lHwJNm8EHAogsvWxFWn33+D+XPBdnYrt4H4RP+ZFLvGy4HAKJ42DpIvlR1rnWGSuB5nv9eZ9G/WkegyrNwWXrqxOf6elnvIwAQyI1unHe0Rb1vuIYGAMH91kFG4J3tyfQx1iHKWTabrRLgLOsc+VKVklpmo/KmqpJIZc53OaQBHGOdh8ytU+Dslkj4Q4U8urcYhqZUlRXWQUZCRc+xzlDO1m7QZvht+R+AcgWAxkh8Sfch7V2ZRYD+AsBE6zxkSyB/zgWCU1sjtb+1zjIcDgAC1QMr4K99XoeIfKBt1apq6xjlSlU/ZJ1hBJ49LVL3hHUIKn/tqfQcCQYfADDLOguZ2yiKzzQ3hk6ZPfM43+yu6wDglBkzXoA/n5yeUvP8eq4CFMGWzX9mW+cYgXusA1B56+jp2TuRSv9CgT8AYLef/qpOp7c01V7tt2r6qw+qqE9/cSrki8lk0m9bGZQ85/RL8OGDTCrosc5A5as9lW5kvY+28AC5csPkmmmtDXV+fI4O/7lxKjoh+IB1oLwp3rre1ZwFwBefufjBwlTvYQr1388CAPE8DgBUcG2rVlVPfG7D1xV6CZT1PsJqAT7UEgl3WQcZjf/8IIvXaR1mpAT4cltbW8A6R7lwkC9i2+HQPzbW6EZfrmRR6dpa7wP0MvhwVYwKa2u9ryVS6+ubP7DND3NrpP5RAL47GGiLo2qmHHahdYhy0J7KvhPAedY5RkIgS6PR6CbrHFQeWO+jHbzkl3rfcG03zaro3daBRkqh/7tgcc8U6xx+Nk/VKbxfwJ/v/qHQu6wzUHlgvY92cJcODvqm3jdc2w0A4iFuHWgU9nHV8l3rEH4W6kp/GMB06xwjpY4DAI1eorP3TNb7aItNClzc0hg+ufXEGX5dId+l7Y5423J61bMA/NqtV6h3YqypPmkdxG86ulbu7+ngg/Dhxj8AANEnWxpqD/NbDYdKx113ZV+3aVzuJ6Liv+2vqRju8wI4+7SZtWV7tsh2KwBbPtdIWYcaBYG4G+PJrD9vYkZUVVQHr4dfb/4A4OFW3vxppOKdvdM3j/NW8OZPAFSBq13fvvXlfPMHdvJZr4osFFU/L329QVzuN6oa4w1heBJLM18UoNU6xyjdZh2A/GdrvQ/QS6B8wr/iiT4pKufEyuAJ/+F4zQ+815/7I4BB62CjI82JVPoS6xR+EE9mZ4ri69Y5RmltNlK73DoE+UtHV89RE57r62G9jwBAReeP2xwoi3rfcL3mh372SfVrIVhsHWy0ROSb7Z0ZP69kFN3tqfQbxXk3w6dP/W8lwM3zRDzrHOQPqirtnelPe+qyAhxrnYfMvQDVOa2NdefMmhV6yTrMWNrp1Cue/M46WAEEVfT2eGevb59qL6ZbF/e+PghZBOAQ6yyj5uH/rCOQPyxY3DOlvSsbV8GPAUywzkPmljjPOzrWVPdH6yAWZGd/2JZcNWmi61sDoMY6YAE8K05ntjTUPWIdpFTEs9mJ0uctho8rf9u4Lxap5bs42qNEZ++ZELkWPMCHhup9X4w1hn13gE8h7XQFYE506npV/N46XIHsr57c2ZHseYN1kFLQ0fHoOOnTNpTHzR8Kvvun3evo6dm7PZX5DUTawJs/DdX7Qq2R2h9X8s0f2M2DL07xU+twBXSY51zvlm1uK1ZbctUkrVm3ENAW6ywFsmFgQG+yDkGlK97ZO93bLCsUymPDqWLqfcMlu/uPiVS6F0CtdcgCXu6LgMZikdpu6yRjbcHinimBKtcB4DjrLAV0XSxSe4F1CCo9yWQyuN5N/IpAvgKAB4XRP9STD7VGw53WQUrJHqov+jPrgIWl+wL4c6Krt1zeAQ9L+9LetwWq3XKU180fAneNdQYqPR1dPUetdzVpgVwB3vwrnorOH9fv3sWb/2vtdgWgo+PRcV7Ni48DONg6aIF5EP1OTW7DFdFo1Od7HuxePJU+S4DrAOxtnaWwJBmLhE+wTkGlQ1WlI5X5lAq+Az7hT8ALAny8JVLbZh2kVMme/kJ7V+YSVb3KOmiRdOng4AfK8ZCHZDI5fr2ruVKAT1tnKQbxcGpLtPZO6xxUGoY+4gpcX0bPt9DoLHGed25ztP4p6yClbI8DwIJly/YK5Kr/AWAf67BF8ixULyqnHmi8K3uceN4NEBxtnaVIsrFIbdg6BJWGeFfmfaJ6LYDXW2chc5sg8qWWhlDFP+E/HHscAAAgkcr8L6CXW4ct7gshf/aQ+2RrpP5R6ywjdVvy3n2qXP83APk4yvizTxF5T0tjeIF1DrLV0dOzt24O/IRP+NMWZX96X6ENawC4s7t7v8GB4GMo31WArTZDcVXV5nFXnnzy0X3WYYZrnqqb1pU+WyBXAZhinaeoFPe3RMLHcrqvbInO3gaI3AjgzdZZyJwnIt/re/3Er86ZOrXfOoyfDGsAAIBEZ+bLEP2mdeAx8pxCfyqu/8exhoYXrcPsyjxVF0ql3wuReQCOss4zFhTS0hoJd1jnIBus99EOWO8bhWEPAIsW3V8zMH7z3wEcaB16DK1T4BpvwLtm9kn1a63DbNXW3T1hQn/wbHG4DIq3WucZM4LOWGNt1DoG2ejo6jkqp+4mHuBDAADFTeMG3Ccr7QCfQhr2AAAAic70JyBltUPgcA2q4k4Ibprk9S2IRqObxjrAPFV33NJMg1M9B5D3oexqfXuknqLutKbajHUQGluqKu2p9CchciVY7yPW+womrwEgmUwG+1zNSgCVvKXuSyK4FZ7eBZVkS7R2TbG+UVty1aRJsqHBEz0BomdC5VDrizfUHYvUHm8dgsbWgmUrDw7mcr9W6Luts1AJECzWgcFzy7G6bSGvAQAA4slMkzi9eyT/tiwpVgFyN5ymPQ8PTxhwj4xkSaqj49Fx3oR1h0sAR6inR0NwAoa2Ya6yvsRSISq3iwuc39x43LPWWaj4WO+jbbDeVwQjuoknOtO/h+D91uFL2BoAj0JkjUDXqyfrBbpeRV+CyER4uhecTAJkH6juA+CtAA4FH2oajjUK+QgfBCxfHT09e3ub3dUAPmSdhUqA4n6onB2Lhv9qHaXcjGgAiC/pPkSCwQdReZ9DU2lQCK7Vie4LraHQBuswVDis99E2PFX9fmDDfl9tbj58s3WYcjTiZfz2VOYChV5rfQFU0R53Iuc0N4aXWweh0WG9j3bAet8YGPEAoKrS0ZW9kw/nkLFBQL5/YI18NRQKDViHofwlUve8HQjOB3SadRYqASJ/DAYHLjxlxowXrKOUu1E9yNe+ZOWhGhz8C4C9rC+EKp32KnSun7dyrjSqKu1d2Y8B+gMANdZ5yNxLEHwy1lh7k3WQSjHqJ/kTqcz5gP7C+kKIALwC6GdjkbrrrYPQ7i1YtvLgQG7wBgAnW2ehEiBY7HLeh3l639gqSJUv0dX7O6j8t/XFEAGAAnc4D+cVc48GGrlEKn0GgF8AmGydhcxtAnRetrH2u/NEPOswlaYgA8DQKXQDKwEcZn1BRACgwDNQfLS1qTZunYWGbDla/HsAzrfOQiVAsUoUZ7dEa++zjlKpCraZT0eyp95zrgvcuIZKhwL4uda4S1gXtBVPZmeK824E3yQQ630lo6C7+VXwWQFU2h4HMDcWqe22DlJpWO+jHfwD6p0ba6pPWgehImznm0j1XgvIBdYXRrQD1gXHWDyZPVKc3sR6HwFgva8EFXwAyGazVWs2eH+Gosn64ohei3XBYmO9j3bAel+JKsqBPouWLz9gIFfVXVFn1ZOfvALIxbFI+AbrIOXmzq7sQYPq/Rqs99GQJc7zzmW9rzQV7US/xNLMW+BpN4Ap1hdJtAu3qefOb42GnrMOUg5Y76NtsN7nA0U90jextGcaPOkEZJL1hRLtDOuCo8d6H22H9T7fKOoAAACJrt6TobIQQLX1xRLtAuuCI8R6H22D9T6fKfoAAAAdnZlTPdHbAIyzvmCi3fibeDq3JVqXtQ5S6ljvox2w3udDYzIAAEA8lWkW6K3gEEClbVCh39y49slvzJkzJ2cdphSx3kfbYb3Pt8ZsAAA4BJCfaG/OC549Ozrt79ZJSgXrfbQD1vt8bkwHAABIJDMnwemt4BHCVPpeAeQLsUj4Ousg1hYs7pnigu5XIohZZyF7CtydA859T6T2n9ZZaOTGfAAAgIWd6bATtAPY3/oFIBqGiq4Lst5H22C9r4yYDADA1s8RvUUA3mT9IhANw1px+tGWhrqEdZCxwnofbYf1vrJjNgAAwIJlKw8O5AZvA1Br/UIQDYMq8LONVYOXzJkxY6N1mGLq6Moc76nOB+t9BHgAfuD69v0K633lxXQAAICOjkfHeRNfvBaCc62zEA3T3+C8s2MN9SusgxQa6320LQH+qep9iPW+8mQ+AGwVT6U/I8D3wV865A9lVxdkvY+2w3pf2SuZAQB4tSEwH8CB1lmIhmm5B517WqTuCesgI6Wq0tGV/YRCvwtggnUesiYvQvCJWGP4ZuskVFwlNQAAQEfXyv1zOvgbAU61zkI0TL6tC7LeR9tiva+ylNwAAADzVN20pZnLRPE/AKqs8xAN063quQv8UhdkvY+2sUlVL18Rqf0R632VoyQHgK3aU9l3At71CoStsxANU8nXBVnvo+2w3lexSnoAALY+lVxzkQDfBLcfJX9QAL+s2jTucyeffHSfdZhtdSR76j0n8wH5L+ssZE4V+Emgb99LWe+rTCU/AGzVsST71lzQu06AE6yzEA1TydQFWe+jbbHeR4CPBgBg6GnlRCrzURF8A8AU6zxEwzCggis2rll9lVVdkPU+2pYCvxv0qi46PXrsOussZMtXA8BWixbdXzMwftMnAbkcPFSIfECBHs8LzB3L0wV5eh/t4CVR+VRLU3i+dRAqDb4cALaKL+k+RILBrwH4CLisSaVvzOqCi5YvP6B/oOp61vsIYL2Pds7XA8BW8aW9R4snVwCYDcBZ5/GxAQD94LvFolLgloEBveCMk+qeL8bXj3f2ni4i14H1PgI2A7g82xj+Iet9tKOyGAC22vKg4KdlqN403jqPf+h6hdyAgPu+2+xEg4M3Ami0TlXmCl4XZL2PtsN6H+1BWQ0AWy1Y3DPFVcnHBXIxgNdZ5ylhaxV6bVVV7upt9/uep+qmdWU+JcBVAKqtQ5axgtUF25dm69Tz5gM43PqiyBzrfTQsZTkAbHVb8t59goHBs6F6ngDHWucpEZ4CnQBu2Di55o9zpk7t39VfbE/2htTJfABHWocub/KQipzd2hhame+/zGazVWv69ApAvwg+B1PxWO+jfJT1ALCthcvSU11O5gL6EVTmZ6NPAfJbNyi/bD4x9Nhw/1Fbd/eEiQNVVwB6Cfh8RTHlfbrgUL3Pmw8gZB2eSgBP76M8VcwAsFUymRy/3tXMdsDpOnTg0N7WmYroX4Am1HM3x5pCKRHRkX6hjs7MqZ7oDeBJjcW2NODlzjk1On31rv6Cqkp7KvNxCL4LYKJ1YDK3TlQ/0dJU93vrIOQvFTcAbKutrS0wYf+3TIfzzhTgDABvsM5UAA8CEldPErGmactHc9PfUTyZnSzOuw7A6dYXWeZeBuSSndUFFy1ffsDgYNWvFGi1Dkn2WO+j0ajoAWBbqip3dGWPUdUGhTZAZCZK/92uAngIguWquiwHSY7FL4JEKnMeoD8CN2EqLtU/9Q/iwq11Qdb7aBus99GocQDYjXiq53ComykO06E4CpCjAN3XLJDok1D5m4jcr4rl/QPe8mJ1yfcksTTzFng6H8AMs9ejMjytoheJohWQ86zDUEn4i8B9sCUS+ot1EPI3DgB5WrBs5cGBgdxR6nSqAG8D9BAABwLyBgAHAKgaxZfvA/BPAM8A+KeqPg3IQ3CyynObH5o9c+Yr1te/rba2tsDEKYd+EZArRnndRLRnHoAfur59L2e9jwqBA0ABqap0dGamDAa8AwKCKk/dJJeTKhWdIND/bEzk8DJyLqcBvCySy6mHl6s3TXi61I6OHa72ZG/IE7lJBEdYZyEqRwL8U6DnNkfq7rbOQuWDAwAVRFt394QJA8HvCPAp8OeKqHBY76Mi4S9qKqh4qufdAvdrAAdbZyHyuZcAuXQsDo+iysQBgAounsxOdqK/VNH3WGch8qluOJkbawg/bh2EyhcHACqaeKr3HIFcA9YFiYZrQKHfWtFY+3XW+6jYOABQUd2RvOfNORf4DXi6INGePOggZzdHwvdaB6HKwL3dqahOjU5fvWHt6hO2HFbTP+ovSFR+FMB1WuPCvPnTWOIKAI0Z1gWJXmONUzmvuSl8h3UQqjxcAaAx0xKty26sHjxWgasx9K6HqHKJ/DFYNTiVN3+ywhUAMsG6IFWwXR72RDSWOACQmY6ulfurl7uOdUGqIKz3UcngAEDmWBekCsB6H5UcDgBUErbUBW8E0GCdhajAWO+jksSHAKkkbKkLRlkXpDLCeh+VNK4AUMlZ2JkOCzCfdUHyMdb7qORxBYBKzmlNtRnWBcm3WO8jn+AKAJU01gXJR1jvI1/hAEAlj3VB8gHW+8h3OACQbwzVBfFTQCZZZyHaYkCh39q49slvzJkzJ2cdhigfHADIV1gXpBLCeh/5Gh8CJF9hXZBKAOt9VBa4AkC+xbogGWC9j8oGVwDIt1gXpDGl+qf+AX0Hb/5ULrgCQGWBdUEqItb7qCxxAKCywbogFQHrfVS2OABQ2WFdkAqA9T4qexwAqCyxLkijwHofVQQ+BEhl6dTo9NU1Xt8JrAtSHljvo4rCFQAqe6wL0jCsUchHWiPhDusgRGOFKwBU9lgXpN3aUu/jzZ8qDVcAqKIkunpPhsoNYF2QWO+jCscBgCpOR9fK/T0d+CUgs62zkJl73KCb23xi6DHrIERWOABQxWJdsCKx3ke0BQcAqmh3JO95cy7g5kNlpnUWKjrW+4i2wYcAqaKdGp2+uia3gacLljfW+4h2gisARFuwLliWWO8j2gWuABBtwbpgmWG9j2i3uAJAtBOsC/oa631Ew8ABgGgXWBf0Jdb7iIaJAwDRHrAu6Aus9xHliQMA0TAsTPUe5gQ3si5Ykh5UcXNbG0MrrYMQ+QkfAiQahtMidU9sUxccsM5DALap9/HmT5Q/rgAQ5WlhZzrsBDcBeJt1lgrGeh/RKHEFgChPpzXVZjZUDR7DuqAR1vuICoIrAESjsKUu+GsAB1lnqQCs9xEVEFcAiEZBVRVcBSAiH+IKANEItHV3T5gwEPyOAJ8C//9obKn+qX8QF55xUt3z1lGI/Iy/uIjyxDMDSgIfAiQaJQ4ARMPU1tYWmDjl0C8A8nUA1dZ5CArgl1rjPtsaCm2wDkPkNxwAiIbhjuQ9b865wI0AGqyz0GtwIyCiEeAAQLQHQ1sByzUA9rLOQrvErYCJ8sQBgGgXOrpW7q9e7joVfY91Fho2HgZENEysARLtRDzV825PB+/jzd93pntBb2UilTnfOghRqeMKANE2WO8rI6wLEu0Wf8ERbcF6X1liXZBoFzgAUMVjva/ssS5ItBMcAKiisd5XUR50kLObI+F7rYMQlQIOAFSxWO+rSKwLEm3BAYAqDut9BNYFiVgDpMrCeh9twbogVTyuAFBFYL2Pdol1QapQ/EVIZa892RvyRG5ivY92g3VBqjgcAKhssd5HeWJdkCoKBwAqS6z30SiwLkgVgQMAlR3W+6gAWBeksscBgMoG631UBN1u0J3DuiCVI9YAqSyw3kdFMoN1QSpXXAEgX2O9j8YM64JUZvgLk3yL9T4ywLoglQ0OAOQ7rPeRMdYFqSxwACBf2VLv+w2ARussVPFYFyRf4wBAvsF6H5Ug1gXJtzgAUMmLJ7OTxXnXATjdOgvRLrAuSL7DGiCVtHiq593ivPvBmz+VNtYFyXe4AkAlifU+8i3WBckn+IuVSk57sjekTuYDONI6C9EIsS5IJY8DAJUM1vuozLAuSCWNAwCVBNb7qIyxLkgliQMAmWO9jyoA64JUcjgAkBnW+6gCdcPJ3FhD+HHrIESsAZKJ9s7MLNb7qALNgKf3si5IpYArADSmWO8j2kL1T8Hq3AWnzJjxgnUUqkz8BUxjZkFXz7sC6m4C8E7rLEQlYo1TOa+5KXyHdRCqPBwAqOjmqbppXZlPCXAVWO8j2hHrgmSCAwAVFet9RMPGuiCNKQ4AVDSJzt4zIXIdgH2ssxD5BOuCNGY4AFDB3Za8d5+qQP/PoPLf1lmIfIp1QSo61gCpoNo7M7Oq3MBfefMnGhXWBanouAJABZFMJsf3uUnzAL0EHCyJCkfkj8HgwIWsC1KhcQAoIFWVjs7MlMGAd0BAUIUc9oYGAireXgIERVCligGFbBKVjQrZJMHBjU6xcXNu3NOnR49dZ30NI8HT+4iK7ikVnNvaWLvEOgiVDw4AeepI9rwh53CUk8BUKN6m0IMBHAjgEABTAARH8eU3Avg3gH9D9d8A/qmQhwJOHvRk04OxhoYXra9/W1tO7/siIFcAqLLOQ1TmPBH50cTc+suj0egm6zDkfxwAduOOZZkjcjkcr6rTRTAVwFEAXmcY6WkIHhTIA57nLfMGtXv2SfVrLYIklmbeAk9vBHC84etRCZ5W0YvgSYsIPmodhkrCX3LinT27sf4B6yDkbxwAtpin6kLLeo+FBhqg2qDATAEOsM41DI9A0Q2RpXDoHIunhuOd6Q+L4Mfg6X3Fpfqn/kFceMZJdc8DQHtXZraq/hLA/tbRyNxmAF/JNoZ/ME/Esw5D/lTRA0AymQyux171cN6ZArwXQ8v4fve4AgnxJF6D9Z3RaHSwUF+Y9b4x8zIgl8Qi4et2/A+Lli8/YGCw6pcATrMOSSWBdUEasYobANq6uyfU9Afeo86dDtVTUN7vYteqIi4qf8g2he4ezTuF9mT6FHW4AcBB1hdV5pYGvNw5p0anr97VX1BVSSxNXyAq3wcw0TowmVsn0ItaInW/sw5C/lIxA8DCZempLidzAf0ogNdb5zHwL0BuynnuV7Oj0/4+3H/Eet+YGVToN/PZAY7PYdB2WBekPJX1AHBnd/d+uf7g2QqcB8HR1nlKhAJYCugNrm+/m5ubD9+8q7+YWNozDZ67Caz3FduDKm5ua2NoZb7/MJvNVq1Z730Vgi9hdA0UKgMC/FOg5zZH6u62zkKlrywHgPZk+kDP6YUC+SyAva3zlCoFngH051VVuau3fdfA0/vGjAL4ZdWmcZ87+eSj+0bzhdqXZuvU8+YDONz6osgcTxekYSmrAWBBcsV/OZf7lAAXABhnncc/dD0gv1N433OD1f0aHLwRPL2v2NZC9COxxrr2Qn3BeDY7EX3etwX4tPXFUUng6YK0W2UxAHSkMsd68K4ApBX8nHo0BjFUL6qxDlLOFLhlYEAv2FrvK7R4V+97ROU6sC5IrAvSbvh6AIgv6T5EgsGvAfgIgIB1HqI9eAWQL+ys3ldoi5YvP2BwsOpXCrRaXzSVBNYF6TV8OQAsWnR/zcD4TZ8E8BVAJlnnIdoTBXo8LzA3nwZGIcRTvecI5Gfgqg7tZn8Jqky+GgDmqbpwV/ZjHvTrPtmlj2hABVdsXLP6quHW+wrtjmWZI7yczlcgbP1iUAkQ/f1ArvoTfj18jArHNwNAx5LsW72g/hLQqHUWomH6G5x3dqyhfoV1kGQyGexzEz8PyDfAg5sIWONUzmtuCt9hHYTslPwAkEwmg+tdzUUCfBNcxiR/KFi9r9BYF6RtsC5Y4Up6AGhPZd8JeNdz6ZJ8ZK04/WhLQ13COsiusC5I21GsciJzWResPCU5AAx91p+5VIGvg8uV5BPFrvcVGuuCtI3NAC7PNoZ/yLpg5Si5AaCja+X+OR38jQCnWmchGqYxq/cVGuuCtAPWBStISQ0A7Z2ZWSo6H8AU6yxEwyK6zFOcc1qk7gnrKCOlqtKeylwIwffA0wUJWCeqn2hpqvu9dRAqrpIZAOKp9GcE+AG4kx/5Q96n95W6LacLzgcwwzoLlQCeLlj2zAeAjo5Hx2nNuusUeo51FqJhKpl6X6GxLkg7YF2wjJkOAPEl3Ye4YPA2PuVPPqEK/Gxj1eAlc2bM2GgdppgSqfQMAPMBvMU6C5nzAPzQ9e17+e6ODyf/MRsAEql73g4E7gTwJusXgWgYSr7eV2isC9J2FKtEcXZLtPY+6yhUGCYDQLwrUyuq7QAmW78ARMNwq3rugtZo6DnrIBY6OjOneqI3ADjQOguZG1Dot1Y01n6ddUH/G/MBYOhJf+9WHuJDPuDbel+hsS5I21Lg7hxw7nsitf+0zkIjN6YDQCKVPgPAzeDDRVTirE7vK3U8XZC28ZKofKqlKTzfOgiNzJgNAImu3hao3AJgnPVFE+1G2dX7Co11QdoO64K+NSYDQDyVaRboreDNn0rb38TTuS3Ruqx1kFLHuiBtS4B/qnofijXVJ62z0PAVfQBoT6ZPUYcFAKqtL5ZoF1SBn6HGXcpT0fLDuiBtg3VBnynqANCe7A2pQ5IP/FGpUuAZ5/QjlVTvK7QFy5btFchVfw/A+dZZqASwLugbRRsAOpZk3+oFveXgvv5Uum5Tz51fqfW+Qot39p4uIteB9V4CNgN6Rbax9rusC5auogwACxb3TAlUuXsAHGZ9gUQ78QogF8ci4Rusg5Sb9mT6QM/hBp7mSQDrgqWu4ANANputWtPnLQbQaH1xRK+lvQqd2xqpf9Q6SblSVWnvyn4M0B+AdUFiXbBkFfzkvTV93jXgzZ9Kz6BC/2fD2ieP582/uEREY5Hwdeq5EAA2Kuh1KnpjoivTdmd3937WYeg/CroCEO/KfFJUf2J9UUQ7eBzA3Fiktts6SKVJJpPB9W7iVwTyFQAB6zxki3XB0lKwAaAj2VPvOdcFdoKpdCiAn2uNu4T1PlsdXZnjPdUbwbogsS5YMgoyANyWvHefKjewEnzoj0qEAs9A8dHWptq4dRYawrogbYd1QXMFeQagSgZ+Dt78qUQocIfzcDRv/qVl9syZr8QitReo6hkAWL2sdIKp6tCTSPVeNk+14M+j0Z6NegUg3tV7oaj83PpCiMB6n2/c2ZU9aBDeDVCcYp2F7LEuaGNUA8AdyXvenHPuL9zpj+yx3uc3rAvSDlgXHGMjHgCG/p83swjALOuLoIo2CMj3D6yRr4ZCoQHrMJS/eDJ7pDi9CdBp1lmoBPB0wTEz4gGAS/9UAh53Iuc0N4aXWweh0WFdkLbFuuDYGNEAEF/SfYgEgw8C2Nv6AqgiKQTX6kT3Bdb7ysuWuuB88KFiGqoL/sD17fsV1gWLY0QDQKIz/XsI3m8dvoStAfAoRNbA0z6IrgdcH+C9CLi9AK2BoAaQfaC6D4C3AjgUfOczHGsU8pHWSLjDOggVx4Jly/YKetU/VsWHrbOQPYE8AMjZLZHQX6yzlJu8B4B4MtMkTrksM0SheBCQu+E07Xl4eMKAe2TWrNBL+X6hjo5Hx3kT1h0uARyhnh4NwQkAasGNlV4lKreLC5zf3Hjcs9ZZqPgSqfQZAH4Bni5IwCZVvXxFpPZHPF2wcPIaAJLJZLDP1dwL4B3WwQ29BOgtEHdXrj+XnH1S/dpifaNFi+6vGaze3OCJniAOc6ByqPXFG7onFqmdYR2CxhbrgrQt1gULK68BoD2VuUih11iHNjAIyCKFzp/k9S2IRqObxjqAqkp8aabRqZ4DyPtQec9fqDg3vaUh1GsdhMaWqkpHV/YTCv0ugAnWeciavAjBJ2KN4Zutk/jdsAeARYvurxkYv/nvAA60Dj2G1gHy09xA7ifFfKefr7bu7gkTB6rmAnoZKmtv9a5YpDZiHYJsJFL3vB0IzmddkABAgd8NelUXnR49dp11Fr8a9gDQnkp/RYFvWAceI88p9Kfi+n8ca2h40TrMrsxTdaFU+r0QmQfgKOs8Y0NmxyLhhdYpyAbrgrQt1gVHZ1gDwJ3d3fsNDgQfA7CPdeAi2wTBVTrRXemnetk8VRdOZT+ooleh/Fdo/pptDB/NB4EqWzyZnSnOuxGsCxLrgiM2rAEgkcr8L6CXW4ctskUK71N+3kp26FTG/m8A8nGU97uj98YitbdahyBbC5Yt2yswWH01BOdaZyF7AnlAPXwwFg3/1TqLX+xxANhyhOc/UL7v/p+F6kWxpro/WgcplHhX9jjxvBsgONo6S5GsbGkMh0RErYOQvS11wesAvN46C5nbpKqXxyK1P+Tvhz3b4xGMbrDq4yjfm3+XDg4eW043fwBobQytdBv2rVPgaussRXJc+9J0s3UIKg2xSO2tuQFvqioS1lnI3HgR+X6iK7P49lT6jdZhSt1uVwA6Oh4d59W8+DiAg62DFpgHyLdrvPXzotHooHWYYoqn0mfJ0LujcqsNshFA21FVSSzNXiSqV4F1QWJdcI92OwC0d6XPVcWvrUMWWJ9C5lTSVrLxVM/hIu4OKN5qnaWQxMOxLdHa+6xzUGkZqgsGbgJwnHUWKgm/HfCqPsm64Gvt/iMAxSesAxaWvAjg3ZV08weA1kj9o7l+73gAK62zFJI6+aR1Bio9scj0h2q8vjqF/g+AnHUeMvfBKjdwf6KzJ2odpNTscgWgPZk+Rh3utQ5YQE8L3CmVfKBEW3LVpBq34RaFvts6S4FsrgoOvOnk449/xjoIlaaOZE+952Q+IP9lnYXMqQI/CfTteynrgkN2uQLgCS6yDldAqweB+kq++QPAnOjU9dK3z2mAlssKyLj+geDZ1iGodDVH63vcOJ0Gxf9ZZyFzIsCnvYkv9iaSmUo+z+ZVO10BaEuumjTR9a0BUGMdcLQUeMY5bWhpqHvEOkupiGezE6XPWwxgunWWAvhLLFL7LusQVPrauzLvVdVfgHVBAjaJyJebG0I/quS64E5XAGpkw+kog5s/gFfEec28+W+vNRTaMK7fnTp0zrbvvTOxtId7w9MetTSGb2FdkLYYr6o/SHRlFncke95gHcbKTgcAFf2gdbACGIAnZ8Qa6ldYBylFs2aFXhIv1wLgX9ZZRs2Tc60jkD/MPql+bSwSPk1FPgVgo3UesiXACZ4LPBBPpc+yzmJ0/dtbsLhnSqDKPQUgaB1uVBcmcmlLY/i71jlKXUdX5nhPtRM+/t9bgWc2rl198Jw5c/jENw0b64K0g4qrC75mBcBVB86Ej28GQ6S9uSH0PesUftDcGF4O6Fesc4yGAAeMP/DNM61zkL/EItMf2jC5Zroovg3WBWlLXTCezDRZBxkrrxkABDrbOtQoPaWenFvJD3bkq6Wx9ioAvj5iVxRnWGcg/5kzdWp/S1Ptl53nzQT079Z5yNybxOnd8VT6xx0dj46zDlNs2w0AHT09e0PRaB1qFFQ9mdsaDT1nHcRPRESdBD8KwLevmwNOV9VhnW5JtKNX64LAb6yzkLmKqQtuNwB4m+RkANXWoUZKIPNbo+FO6xx+1Nx43LOAXGadY6QUeOMdXdljrHOQfzXX178ci9SeKyLvA/C8dR4yJjgaTjPtXZnPluubi+0GABEXsw40cvJiMNh/iXUKP2tpDP0aQLd1jpHKqTfLOgP5X0tj+JZcIPguAIuss5C58ar6g/auzF3lWBfcbgBQ6InWgUZM9XJuCTs6IqICdyEAX56QKE44AFBBzJ553NMtjeFTAbkAQJ91HjJ3oufcXxNd6bLaefTVASCe6jkcwCHWgUZEsWrDM6uvs45RDrZsl3yDdY4RUcxs6+7mMbBUECKisUj4OiAXRpkdpEUj8joo5idS6Zvuuiv7OuswhfCfFQB1TdZhRkoF32YHvHA86Hfgz1WA8ZMGAuWwvTGVkK11QQW+A9YFCfjg5mrvgXKoC/5nABD482IEj21cu/pm6xjl5LRI3RMAfmudYyS88jjfgErMnKlT+1sjtV+CahTAaus8ZO5N4nRJvLP3Kj/XBV8dAMSnvzgF+h2++y889dx3AHjWOfLOrVJvnYHKV6ypbqkb5x0NgB85khORS7yJL65oT6aPsQ4zogsAgMTSpfsCeLN1mBFY2/f6STdahyhHrdHQ3wAssM6RLxHUl2tlh0rDlrrgBSpyJlgXJMFUdbjHj3VBBwCq447DLo4GLmmqv5szdWq/dYxyJSJ+3BRlcvuy7GHWIaj8tTaG/5QLBN8lkD9bZyFzQ3XBpZk/+6ku6ABAVEPWQUZCVPjuv4imTJQO+HB3QFG80zoDVYbZM497urkxdArrggQAUJzkp7rg0AoA5F3WQUbgLy3R2vusQ5SzUCg0AKj/HrBU5QBAY2ZrXdCJV6vAvdZ5yNxQXbAzPb/U64JDKwCQI6yD5EtE5ltnqATiAjdZZxiBst6/m0pTc2P9gxsn19SzLkgAAMHZm8fl7l/YlY5YR9kVN/TQgue7ASDntMM6QyXIzJyWgc8+BlDgKOsMVJlYF6TtqBzqFHeXal3Q3dHZewggk6yD5EOBZ1qPDz9onaMSzBPxoNppnSNPb7EOQJWNdUHaRsnWBZ0nONw6RL5EcbeIqHWOyiF3WyfIU01H18r9rUNQZWNdkLazpS6Y6MpcXCp1Qacib7QOkS8F/HZD8jVVl7TOkHfm3MCh1hmIgC11wQFvKiDt1lnI3Hio/rBUThd0AhxkHSJfAectt85QSbZsCvSsdY68BBwHACoZs0+qX9vSGGoVxWcAbLTOQ+ZO9FzggfZUeo5lCAd1fjsBMLf+9Xv93TpExRF92DpCPtTTKdYZiLYlItrSVHs1kJsGni5I0H0V+EOiK9O2ZTfeMedU9GDrlyE/+gR3/zOgzlcDgIjwGQAqSVtPFwTkSrAuSKpnQqvvtagLOhG83vr684zsqxtRuVD1HrHOkFdeeJOtMxDtypypU/tjkfAXVbUBgses85CxobpgMp5K/3gs64IOir2trz0fIuAAYMA5nw1eKhwAqOS1NtXd46q94wTc1pwgAnzaq3mxZ+Gy9NSx+IYOgpLeqnBHqvq0dYZKlPP89bor4Ku9LahyNdfXv9wSCX9IRN4H1gUJOMblkE2kei+bp+qK+Y18twIAyCvWCSpRVVBets6QDxFMsM5AlI+WxvAtrAvSFuMB+U6oq7inCzoAE62vNB8Cb711hkrUj4DfBi8OAOQ7O9QFN1nnIXMnes7dX6y6oNvyf77hKVcAbGzw2+s+3joA0UhsrQs68abxdEECsJ8CfyjG6YK+GwC4AmDj3uOP7wPgWecYNkXAOgLRaGw9XXBLXdA//79HxVGE0wV9NwCoc77KWy4inZ3++lkR9qvJ/7apC85kXZAKXRf0zy/0rTz12UOL5aEvGNzLOkM+hBusUBlpbaq7Z9xmN01F51tnIXMFqws6QDdbX01+gR3rXQZUJvhqAPA4AFCZmTUr9FJrY905rAvSFqOuCzpA+qyvIh8K9dWNqFwoPF+97g7w1WBLNFwtjeFbdHDwaAB3WWchc0N1waWZRfEl3Xmf6+MAbLC+gnyI+G3fgvIgg7776MVX+xYQ5aP1xBn/amkMn6zAxWBdkBQnSTCY9+mCDuqvAUChb7bOUImc4DDrDPlQxUvWGYiKSUS0NVL7YyB3HHi6IG2tC+ZxuqCDYJ116ryIHGEdoRKp31534QoAVYYdThdkXbDS5XG6oAOw1jpvfhcHf92IyoWHt1lHyI+8aJ2AaKxsrQsKEIXok9Z5yNhQXfDu9q7MlW2rVlXv6q85iM8GAOCgjp4ev30e7X+CI60j5EX1X9YRiMZaS6S2y1XruwBcZ52FzDlVvXTic30r4kt7j97pXxAVvw0AyG2SMTkqkYZks9kqwF8rAOr039YZiCw019e/HIvUXiDAWQBesM5D5t4hnvTEU+nPqKps+x8cgDXW6fLmpMk6QiVZ0+eF4bNDowDnq+OLiQqtJVLbpoOD7wLrggSMF+BH7V3b1wUdFI9bJ8uXKKLWGSqJACdYZ8iX5wJPWWcgssa6IO1glgSDDyQ6e88EAJcT7+/WiUbg+ELsg0zDoxCfDQC6/rTjj+VHAET4T13QCyAE4D7rPGRuP4i0JbrS17lNa5/8B4B+60R5mqg1L9ZZh6gEbd3dEwCdbp0jHwp5VETUOgdRKTltZu2qDZNr6kTkKrAuSB7uc3PmzMkBWG2dZQTOsA5QCSb0B04BMN46Rz4c8Ih1BqJSNGfq1P6WxvBlnuAE1gUrmOqfYk21P9t6gMBfrfPkywP+O5lMBq1zlDsHd451hnyp4GHrDESl7LTG2tS4zYGjobjJOguNuccHtPpjwNbjgBX3WyfKlwAH9AUmnmydo5zd2d29n4qeap0jXwq91zoDUambNSv0Uqypdq6KnAmeLlghdH1OvNNPjx67DtgyACjgy1+YojLXOkM5G+wPvh+A7x62dANVvvx5JrLQ2hj+k3h4B6Ad1lmoqFQgH5ndWP/A1j9wAJATfz4ZqsDsBYt7pljnKFcq+Kh1hhF4ofmEY/9hHYLIT1qitWtaGmtjEPksWBcsT4p5LZHatm3/yAHAeyK1/4TfzgQYMj5QFfisdYhy1NGZOVWAY61z5E2wkg0AovyJiMYawz9iXbD8qOJXLZHwN3b8c/fqXwCWWYcc4aV9YrhHH9Lwec77snWGkRAVn/4cE5WG02bWrjqwxtUq9H8A5Kzz0OgIEJ+kfR/f2RujVwcAp+iyDjpCe4k3/tPWIcpJorMnCpWZ1jlGQjXn159jopIRCoUGWiN18zzBiawL+pigs69q8KxoNDq4s//86gAgIkuts46UQj/NVYDCEXFXWGcYof4N1V6PdQiicsG6oH8JkKraOC42Z8aMjbv6O68OAOvXPvEAgHXWoUdoP/HGf8s6RDloT/V+QIGIdY4Ryuzuh52I8vdqXRB4PyAvWuehPRMgFdw0ruXkk4/u293fe3UA2LIj4J+tg4+UQs9f2JkOW+fws46enr0V8j3rHCMlwJ3WGYjKVWuk9g/Oy70LgsXWWWg3BHcO5+YPbDMADPF1D9Q5kZ+3tbUFrIP4lW52XwdwkHWOkRJIu3UGonLWHK1/qqUh/G5ALgCwwToP7UBx04ET3WnDufkDOwwAVcHBO+DrQyJ0Ws2Bh/GBwBFoX5qtU+Ai6xyj8O9TG0P3WYcgKnciorFI+Dp4UufHXWTLl1zZEgmfEwqFBob7L7YbAE4+/vhnAM1YX8ZoqOp3+FFAfhJLl+6rnnczAB+frSAd7P8TjZ1YNPxXt2HfOlX9Lnz9xtH3Nin0Q7FI+Iv5/g50O/6BiLvF+mpGqdoJ/nRnd/d+1kH8QFUF3rjrAbzZOsuorgO5ttF/FSLKR3Pz4Ztbm+ouBdAAwWPWeSrQv53nRVsjdTeO5B+/ZgDwnPwBgN/fSb1pYCB4nXUIP0h0ZT4N4HTrHKP03CRv493WIYgqVSxS2z1us5sG4LfWWSrIEvFwXHO0fsTV59cMAK0zQ/+A6HLrKxstAd6b6Er/j3WOUpZIZk4S4CrrHKOnt+xqowsiGhuzZoVeikVqz+bpgkWXU+j/bFi7+uSWaO2a0Xwht7M/FHU3W19hQSi+luhMf8I6RilqX5qtg9PbAVRbZxk11T9YRyCiIa2N4T85zzuGdcGieARAY2ukbt6W6v6oyM7+MJ7MThbnPQUfHgW7E56KnNXaGP6TdZBSEU9mjxTnLQUw2TrLqAkea2kIH84HAIlKi6pKR1f2fIVeBWBv6zw+50H1xxuqc5cXcrOzna4AtEZDzyng94cBX71GUb0p0dXbYh2kFCxIrvgvcd4ilMPNHwA8uYE3f6LSIyLaEgn/QgPunQAWWefxsfsANMSa6j5X6J1O3W7+wy+sr7qAxkFlQXtXrx/Pty+YxNKeac7llgN4k3WWAhnMBQP/Zx2CiHatdWboH7FI7SmqOA3Aaus8PrJOgYs3rF0dikVqu4vxDWR3/zGRSq8CcJT1q1BAKiKXtTSGv2sdZKwlkpmT4PRWAHtZZykUUbm9pSns9wYDUcWIZ7MT0ed9SYDPA5hgnadEbRbgWs9z/9saDT1XzG+02wGgPZW5SKHXWL8aBafykwMnyefz2THJzxKpzHmA/hzl8MDfNjxB02mNtSnrHESUnwXLVh4czOW+qtCPAKiyzlMicir6u2DO+9qp0emrx+Ib7nYAiGezE6XPexLl8nnxdrQ34HnvH6sX2sKiRffXDI7v/5lCz7HOUniyIhYJh6xTENHIJZZm3oKcfhWCD6DM3qDkoV8VN0K8q1oj9Y+O5TeWPf2F9lTmCoXOM3tpinv5L6p457U21t1unaTQEsnMO+DQBujbrbMUg0A/2BKp+511DiIavfiS7kMQCHxGRC5A5TQGnheR672BgatbT5zxL4sAexwAbl3c+/rqKnkSQI1FwDGgIvg/L+cuLfbnLWOhbdWq6gnP931eFF9F+X7G9sSBNe6ISvkIh6hSdPT07O1tch+E4HwAx1jnKQ7tFZFrJ+b6bo5Go5ssk+xxAACA9lT6Rwp8xjLoGHhBIF/ONIZ+OU/ElwdbxLvSJ4riGgBHWmcpLv1oLFJ3vXUKIiqehZ3psHP4sCreK8AB1nlGabUCN8Fzv22Nhv5mHWarYQ0ACxb3TAlUyd8BmWQdeAykPcGlfnq4LJ7qOdzBfUOBs6yzjAG++yeqIG1tbYGaAw47wYPOEUELgIOsMw3Tg6JY4Dm5PdYQypTifiXDGgAAIJ5Kf0OAr1gHHjOiyyQn32yJ1t5pHWVXEsnMOxDwvgyVOQAC1nnGBt/9E1UqVZU7urLH5ERPEZVZgNYBmGidCwAUeEaAToHcPei5JbOj0/5unWlPhj0A3HVX9nWbq73HAVTaMbtZBX6EGndbayi0wTrMPFUX6syeIIKLVHQ28vjf0P/koRpv/bt48A8RAUA2m61auxHHQXWGenocgHdC8HYUv1GwDsBDUMnCaTrgJHPK8aFHSvFd/u7kdfNIpNJfAFBxm+hs8QoUtzjR+enG2s6xfk4gkcy8QwKYq6ofBHCI9YthQZy2tjTUJaxzEFHpymazVc9sGDw857m3OuAwCN6iIgcDuj8UkwFMBmQcoHsBCO7wz3MA1kGwDirrIPqsAP/2VP8hwD+h+rioe2i0p/CVirwGgLZVq6onPNv3gAiOsA5u7GmBLFZ4d2sgkGydGfpHob9BYunSfVWrI+K5EyB6IsprR8aRWBKL1J5kHYKIqFzkvXyc6Oo9GSol+7m4Df27QtKieFgFDwcgjwQ2VT9y8slH9+3pXw4tYeUOU88dKYIjPE/fJoJjAByLivlcf49y6nRaa0Pd/dZBiIjKxYg+P06kem8HZLZ1eB/oA7AegvVblpNegcpeqjpJBJMATALwOuuQpU6AH7dEai+2zkFEVE6CI/lHHvBZB8xCiTx9WcJqANRAMQVQQAFAIRX02F4BPDUY6P+qdQgionLjRvKPTovUPQGRy63DU0X4zOyZM1+xDkFEVG5GNAAAQLYhdDVEl1lfAJW122KR2lutQxARlaMRDwDzRDyI+xCg660vgsrSs7kB7+PWIYiIytWIBwAAiDWEHxflRwFUeOL0vNkn1a+1zkFEVK5GNQAAQHMk/BNVcHMWKhzBL7nhDxFRcY16ABARlcDmcyD6pPXFUFn4q050F1uHICIqd6MeAAAg1tDwIjzMxdA2ikQjpOuB3JxSOHOBiKjcFWQAAIBYU93SijotkApOIB+JRaY/ZJ2DiKgSFGwAAIDmxvCVEP299UWRL32vJVLbZh2CiKhSFHQAEBGtyW04D0Da+sLIPxS4Y8Pa1V+0zkFEVEmKsiltfEn3IRIMZgAcZH2BVNoE8kCfN/H4OdGp3E+CiGgMFW1X+vZk+hh16AQPu6Fd+5cG3IxiHKdMRES7V9CPALbVEq29Tz15D4BN1hdJJWmdOm3hzZ+IyEbRBgAAaI2GOwE5C6wH0vY2OJFYa0Pd/dZBiIgqVVEHAACIRcILAb0AgGd9sVQSNkH0jObG8HLrIERElazoAwAAxCJ11wPyMXAIqHSbVTEn1li3yDoIEVGlK9pDgDsT70x/WAS/whgNHlRSNojKe1qawndZByEiojEeAACgvSt9rip+CSBoffE0Zl6BerNjTfVJ6yBERDRkzAcAAIh3pltFcDOAidYvABXdWjivJdZQv8I6CBER/YfJAAAA7Uuzdep5CQCTrV8EKponFN7JrZH6R62DEBHR9sw+i29pCPU68SIAnrB+EagYtLcqOFDPmz8RUWkyfRivubH+wf4BDQNYYv1CUOEo8LsNVbnoyccf/4x1FiIi2jmzjwC2lUwmg31u0v8Cepl1FhqVHKCXxyJ1V1oHISKi3SuJAWCrLTXBa8CHA/1ojQrObm2s5WoOEZEPlNQAAADxZPZIEe9mCI62zkLDo8DdVeLOPqUx9G/rLERENDwltyFPazT0tw3Vg9MFcq11FtqjQRV8eUVjeBZv/kRE/lJyKwDbal/aG1NPfgHgYOss9Bp/gfM+zH4/EZE/ldwKwLZaGuoS4/rdUQCuA6DWeQgAMAjIla5v3zBv/kRE/lXSKwDb6ujMnOqJ/hTAYdZZKpUAGXg4vyVae591FiIiGp2SXgHYVnNT+I4Nk2uOVOBiQNdb56kwLyhwcd/a1dN58yciKg++WQHYVvuSlYd6wcHvC/Be6yxlbhDQa+H6vxZraHjROgwRERWOLweAreJdmVqofluAE6yzlBlVRbsT9+WWSOgv1mGIiKjwfD0AbBVP9bxb4L4JIGSdxe8UuCMAubw5Er7XOgsRERVPWQwAWyW6ek+GussAjVpn8RkPwAJx7sqWhlCvdRgiIiq+shoAtmpfmq3zPO8SAd4DIGCdp4RtBuSmQADfPXVm+GHrMERENHbKcgDY6s6u7EGDmjtHIBcp8EbrPCXkKYVeXx0c/BlP7CMiqkxlPQBslc1mq/69QWc71XMUOAVAlXUmA5sgEhcPv8xEQkvmiXjWgYiIyE5FDADbunVx7+urgzIHTj8AlRnw0V4IIzAIwWJV/X1gnN7eXF//snUgIiIqDRU3AGxrweKeKYEqiYm6mIq+G+VxDPFLEPmzQDsEwfbmxuOetQ5ERESlp6IHgG21dXdPqOmvmqmCCKBNAGrhj48KNkM0A6BLc+6ug/aS5aFQaMA6FBERlTYOALuwaNH9Nbnxm+pyipCITMPQHgNvMY7lAXgMIvcp9F7xdFmNbshEo9FN1q8XERH5CweAPNyWvHefcdh8pAbckfDwNk/wNgEOBXAIgCkozPMEqsCzAvwDir9D5DFVfQzQv23USX+ZE53KcxCIiGjUOAAUSDKZDK73xk1BVfUU8QZfJy6wt0L3gspEqDcO8p/nC1QxIOLWi3qvqMhmB12nCDwLz3u279nVz86ZMydnfT1ERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERESj8f//OZ86bgNetAAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAyMC0wNS0wNFQwNTo1NDoxNyswMDowMIq5JvQAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjAtMDUtMDRUMDU6NTQ6MTcrMDA6MDD75J5IAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAABJRU5ErkJggg==" \n' + '/>';

			bodyHtml += "<div class='boldText largerTextSize marginBottom0 theme11textUppercase ' " + "style='font-size: " + textSizeRatio * 45 + 'px; padding: 8px;; text-align: center; background-color: ' + this.primaryColor + '; width: 50%; ' + 'border-radius: 0px 0px 50px 0px; ' + "color: white; float: left; font-weight: 100; '>" + 'Acknowledgement' + '</div>';

			bodyHtml += "<div style='height: 25px; background-color: " + this.secondaryColor + "; '></div>";
			bodyHtml += "<div class='center textUppercase boldText extraLargeTextSize ackPdfMarginBottom7 theme11textUppercase' style='color:black; " + "padding-right: 25px; margin-top: 12px; font-weight: normal; '>" + transactionHTMLGenerator.getAcknowledgmentFooterFirmNamePremium(txn, this.primaryColor, InvoiceTheme.THEME_11, textSizeRatio) + '</div>';

			bodyHtml += "<table width='100%' style='text-transform: none;'><tr>" + "<td width='35% ' valign='top ' style='padding:0px; padding-left: 25px; padding-right: 25px; text-align: left; '>" + "<p style='color: " + this.secondaryColor + '; font-size: ' + textSizeRatio * headingSize + "px ;'>Invoice To:</p>" + transactionHTMLGenerator.getAcknowledgmentFooterPartyInfo(txn, textSizeRatio, InvoiceTheme.THEME_11) + '</td>';

			bodyHtml += "<td width='35% ' align='right ' style=' vertical-align: center; text-align: left; '>" + "<p style='color: " + this.secondaryColor + '; font-size: ' + textSizeRatio * headingSize + "px ;'>Invoice Details:</p>" + transactionHTMLGenerator.getAcknowledgmentFooterTxnInfo(txn, InvoiceTheme.THEME_11) + '</td>';

			bodyHtml += "<td width='30%' style='vertical-align:bottom; padding-right: 25px; '>" + "<div width='100%' style='padding-left: 12px; padding-right: 12px;'>" + "<hr style=' width: 100%; border: 0.5px solid #e0e0e0; ' /><p style=' font-size: " + textSizeRatio * 18 + "px; text-align: center; '>Receiver's Seal & Sign</p>" + '</div></td>';
			bodyHtml += '</tr></table>';

			bodyHtml += '</div></div>';
			return bodyHtml;
		} else {
			return '';
		}
	},

	getDescriptionData: function getDescriptionData(txn, textSizeRatio) {
		var description = txn.getDescription();

		if (settingCache.isPrintDescriptionEnabled() && description) {
			description = description.replace(/(?:\r\n|\r|\n)/g, '<br/>');
			return "<table width='100%' style='page-break-inside: avoid !important;'><tr><th align='left' width='100%' style='color: " + this.secondaryColor + '; font-weight: normal; font-size: ' + textSizeRatio * headingSize + "px;'> Description</th></tr>" + "<tr><td width='100%' >" + description + '</td></tr></table>';
		}

		return '';
	},

	getAmountInWordsHTML: function getAmountInWordsHTML(txn, printAsDeliveryChallan, textSizeRatio) {
		if (!printAsDeliveryChallan || settingCache.printAmountDetailsInDeliveryChallan()) {
			var balanceAmount = txn.getBalanceAmount() ? Number(txn.getBalanceAmount()) : 0;
			var cashAmount = txn.getCashAmount() ? Number(txn.getCashAmount()) : 0;
			var discountAmount = txn.getDiscountAmount() ? Number(txn.getDiscountAmount()) : 0;
			var reverseChargeAmount = txn.getReverseChargeAmount() ? Number(txn.getReverseChargeAmount()) : 0;
			var totalAmount = balanceAmount + cashAmount + reverseChargeAmount;

			if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHIN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHOUT) {
				totalAmount = totalAmount + discountAmount;
			} else {
				balanceAmount = txn.getTxnCurrentBalanceAmount() ? Number(txn.getTxnCurrentBalanceAmount()) : 0; // setting current balance amount
				cashAmount = totalAmount - balanceAmount - reverseChargeAmount; // setting total received amount
			}

			var amountHeading = 'Amount In Words';
			switch (txn.getTxnType()) {
				case TxnTypeConstant.TXN_TYPE_SALE:
					amountHeading = 'Invoice ' + amountHeading;
					break;
				case TxnTypeConstant.TXN_TYPE_PURCHASE:
					amountHeading = 'Bill ' + amountHeading;
					break;
				case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
				case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
					amountHeading = 'Order ' + amountHeading;
					break;
				case TxnTypeConstant.TXN_TYPE_ESTIMATE:
					amountHeading = 'Estimate ' + amountHeading;
					break;
				case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
					amountHeading = 'Delivery Challan ' + amountHeading;
					break;
			}

			var amountWordInString = CurrencyHelper.getAmountInWords(totalAmount);

			return "<table width='100%' class='theme10amountSection' style='page-break-inside: avoid !important;'><tr><th style='color: " + this.secondaryColor + '; font-size: ' + textSizeRatio * headingSize + "px; font-weight: normal' align='left' width='100%' class=''> " + amountHeading + '</th></tr>' + "<tr><td style='color: black; ' width='100%'>" + amountWordInString + '</td></tr></table>';
		}

		return '';
	},

	getTermsAndConditionsHTML: function getTermsAndConditionsHTML(txn, printAsDeliveryChallan, textSizeRatio) {
		var termsAndConditionText = '';
		switch (txn.getTxnType()) {
			case TxnTypeConstant.TXN_TYPE_SALE:
				termsAndConditionText = settingCache.getTermsAndConditionSaleInvoivce();
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
				termsAndConditionText = settingCache.getTermsAndConditionSaleOrder();
				break;
			case TxnTypeConstant.TXN_TYPE_ESTIMATE:
				termsAndConditionText = settingCache.getTermsAndConditionEstimateQuotation();
				break;
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
				termsAndConditionText = settingCache.getTermsAndConditionDeliveryChallan();
				break;
			default:
				termsAndConditionText = settingCache.getTermsAndCondition();
		}

		if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE && settingCache.isPrintTermsAndConditionSaleInvoiceEnabled() || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER && settingCache.isPrintTermsAndConditionSaleOrderEnabled() || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE && settingCache.isPrintTermsAndConditionEstimateQuotationEnabled()) {
			if (printAsDeliveryChallan && settingCache.isPrintTermsAndConditionDeliveryChallanEnabled()) {
				termsAndConditionText = settingCache.getTermsAndConditionDeliveryChallan();
			}
			if (!termsAndConditionText) {
				termsAndConditionText = 'Thanks for doing business with us!';
			}
			termsAndConditionText = termsAndConditionText.replace(/(?:\r\n|\r|\n)/g, '<br/>');

			var termsAndConditionsHTML = "<table width='100%' class='theme10amountSection' style='page-break-inside: avoid !important;'><tr><th style='color: " + this.secondaryColor + '; font-size: ' + textSizeRatio * headingSize + "px; font-weight: normal;' align='left' width='100%' class=''>Terms And Conditions</th></tr>" + "<tr><td style='color: black; ' width='100%' >" + termsAndConditionText + '</td></tr></table>';

			if (printAsDeliveryChallan && !settingCache.isPrintTermsAndConditionDeliveryChallanEnabled()) {
				termsAndConditionsHTML = '';
			}

			if (this.isForInvoicePreview) {
				var classText = ' class=\'invoicePreviewEditSection ' + transactionHTMLGenerator.getEditSectionClassName('termsAndConditions') + '\'';
				termsAndConditionsHTML = '\n\t\t\t\t<div style=\'width: 100%;\' editsection=\'termsAndConditions\' ' + classText + '>\n\t\t\t\t' + termsAndConditionsHTML + '\n\t\t\t\t</div>';
			}
			return termsAndConditionsHTML;
		}
		return '';
	},
	getCompanyBankDetails: function getCompanyBankDetails(txn, printAsDeliveryChallan, textSizeRatio) {
		if ((txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) && settingCache.getUserBankEnabled() && !printAsDeliveryChallan) {
			var bankDetails = '';
			var firm = firmCache.getTransactionFirmWithDefault(txn);
			if (firm != null) {
				if (firm.getBankName()) {
					bankDetails += '<tr><td width="100%">Bank Name: ' + firm.getBankName() + '</td></tr>';
				}
				if (firm.getAccountNumber()) {
					bankDetails += '<tr><td width="100%">Bank Account No.: ' + firm.getAccountNumber() + '</td></tr>';
				}
				if (firm.getBankIFSC()) {
					bankDetails += '<tr><td width="100%">Bank IFSC code: ' + firm.getBankIFSC() + '</td></tr>';
				}
				var bankDetailsHTML = '';

				if (bankDetails) {
					bankDetailsHTML = '<table>' + '<tr>' + "<th align='left' width='100%' style='font-weight: normal; font-size: " + textSizeRatio * headingSize + 'px;' + ' color: ' + this.secondaryColor + "; '>" + ' Pay To-' + '</th>' + '</tr>' + bankDetails + '</table>';
				}

				if (this.isForInvoicePreview) {
					bankDetailsHTML = bankDetailsHTML || "<table style='color:#2B4C56'><tr><th align='left' width='100%'> Pay To-</th></tr></table>";
					var showEditView = transactionHTMLGenerator.getEditSectionClassName('payToDetails');
					var classText = ' class=\'invoicePreviewEditSection ' + showEditView + '\' ';
					bankDetailsHTML = '<div style="width:100%;" editsection=\'payToDetails\' ' + classText + '>' + bankDetailsHTML + '</div>';
				}
				return bankDetailsHTML;
			}
		}
		return '';
	},

	getDeliveryChallanReceivedCopySign: function getDeliveryChallanReceivedCopySign(printAsDeliveryChallan) {
		var receivedByHTML = '';
		if (printAsDeliveryChallan) {
			receivedByHTML = "<table width='100%' class='theme10amountSection' style='page-break-inside: avoid !important'><tr><th align='left' width='100%' class='theme10BoxHeading'> Received By</th></tr>" + "<tr><td width='100%' class='theme10BoxStyle' ><p>Name: </p><p>Comment: </p><p>Date: </p><p>Signature: </p></td></tr></table>";
			receivedByHTML += "<table width='100%' class='theme10amountSection' style='page-break-inside: avoid !important'><tr><th align='left' width='100%' class='theme10BoxHeading'> Delivered By</th></tr>" + "<tr><td width='100%' class='theme10BoxStyle' ><p>Name: </p><p>Comment: </p><p>Date: </p><p>Signature: </p></td></tr></table>";
		}
		return receivedByHTML;
	},
	getAmountHTMLTable: function getAmountHTMLTable(txn) {
		var amountHTML = '<table width="100%" class=\'theme10amountSection theme11ItemTable\'>';
		var textColorForHeading = 'white';

		var itemSubTotalBeforeDiscAndTax = 0;
		var lineItemTotalDiscount = 0;

		var lineItems = txn.getLineItems();

		var txnType = txn.getTxnType();
		var totalAdditionalCESS = 0;
		var taxDetailMap = {};
		if (lineItems.length > 0 && (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_EXPENSE || txnType == TxnTypeConstant.TXN_TYPE_OTHER_INCOME || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txnType == TxnTypeConstant.TXN_TYPE_ESTIMATE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER)) {
			$.each(lineItems, function (key, lineItem) {
				var qty = lineItem.getItemQuantity();
				var price = lineItem.getItemUnitPrice();
				itemSubTotalBeforeDiscAndTax += qty * price;
				lineItemTotalDiscount += Number(lineItem.getLineItemDiscountAmount());
			});
			amountHTML += '<tr><td>Sub Total</td><td align="right">' + MyDouble.getAmountForInvoicePrint(itemSubTotalBeforeDiscAndTax) + '</td></tr>';
			if (lineItemTotalDiscount > 0) {
				amountHTML += '<tr><td>Discount</td><td align="right">' + MyDouble.getAmountForInvoicePrint(lineItemTotalDiscount) + '</td></tr>';
			}
			var taxCodeDistributionDataForLineItems = transactionHTMLGenerator.getTaxCodeDistributionData(txn, true, false);
			totalAdditionalCESS = taxCodeDistributionDataForLineItems.totalAdditionalCESS;
			taxDetailMap = taxCodeDistributionDataForLineItems.taxDetailMap;
			var taxIds = (0, _keys2.default)(taxDetailMap);
			for (var i = 0; i < taxIds.length; i++) {
				var taxId = taxIds[i];

				var taxCode = taxCodeCache.getTaxCodeObjectById(taxId);
				var taxAmount = 0;
				var amounts = taxDetailMap[taxId];
				for (var j = 0; j < amounts.length; j++) {
					var amount = amounts[j];
					taxAmount += (taxCode.getTaxRate() ? Number(taxCode.getTaxRate()) : 0) * amount / 100;
				}
				amountHTML += '<tr><td>';
				if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
					amountHTML += 'IGST';
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
					amountHTML += 'CGST';
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
					amountHTML += transactionHTMLGenerator.stateTaxPlace;
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
					amountHTML += 'CESS';
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.STATE_SPECIFIC_CESS) {
					amountHTML += taxCode.getTaxCodeName();
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.OTHER) {
					amountHTML += taxCode.getTaxCodeName();
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.Exempted) {
					amountHTML += 'Exempted';
				}
				amountHTML += '@' + MyDouble.getPercentageWithDecimal(taxCode.getTaxRate()) + '%</td>';
				amountHTML += "<td align='right' >" + MyDouble.getAmountForInvoicePrint(taxAmount) + '</td></tr>';
			}
			if (totalAdditionalCESS > 0) {
				amountHTML += '<tr><td>' + TransactionPrintSettings.getAdditionCESSColumnHeader() + "</td> <td align='right'>" + MyDouble.getAmountForInvoicePrint(totalAdditionalCESS) + '</td></tr>';
			}
		}

		var balanceAmount = txn.getBalanceAmount() ? Number(txn.getBalanceAmount()) : 0;
		var cashAmount = txn.getCashAmount() ? Number(txn.getCashAmount()) : 0;
		var reverseChargeAmount = txn.getReverseChargeAmount() ? Number(txn.getReverseChargeAmount()) : 0;
		var discountAmount = txn.getDiscountAmount() ? Number(txn.getDiscountAmount()) : 0;
		var totalAmount = Number(balanceAmount + cashAmount + reverseChargeAmount);

		if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
			totalAmount = totalAmount + discountAmount;
		} else {
			balanceAmount = txn.getTxnCurrentBalanceAmount() ? Number(txn.getTxnCurrentBalanceAmount()) : 0; // setting current balance amount
			cashAmount = totalAmount - balanceAmount - reverseChargeAmount; // setting total received amount
		}

		if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN) {
			amountHTML += '<tr><td>Received</td><td align="right">' + MyDouble.getAmountForInvoicePrint(cashAmount) + '</td></tr>';
		} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
			amountHTML += '<tr><td>Paid</td><td align="right">' + MyDouble.getAmountForInvoicePrint(cashAmount) + '</td></tr>';
		}

		if (discountAmount != 0) {
			if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
				amountHTML += '<tr><td>Discount</td><td align="right">' + MyDouble.getAmountForInvoicePrint(discountAmount) + '</td></tr>';
			} else {
				amountHTML += '<tr><td>Discount (' + MyDouble.getPercentageWithDecimal(txn.getDiscountPercent()) + '%)</td><td align="right">' + MyDouble.getAmountForInvoicePrint(discountAmount) + '</td></tr>';
			}
		}

		if (txn.getTransactionTaxId()) {
			var taxCodeDistributionDataForTransaction = transactionHTMLGenerator.getTaxCodeDistributionData(txn, false, true);
			var _taxDetailMap = taxCodeDistributionDataForTransaction.taxDetailMap;
			var _taxIds = (0, _keys2.default)(_taxDetailMap);
			for (var _i = 0; _i < _taxIds.length; _i++) {
				var _taxId = _taxIds[_i];
				var _taxCode = taxCodeCache.getTaxCodeObjectById(_taxId);
				var _taxAmount = 0;
				var _amounts = _taxDetailMap[_taxId];
				for (var _j = 0; _j < _amounts.length; _j++) {
					_taxAmount += (_taxCode.getTaxRate() ? Number(_taxCode.getTaxRate()) : 0) * _amounts[_j] / 100;
				}
				amountHTML += '<tr><td>';
				if (_taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
					amountHTML += 'IGST';
				} else if (_taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
					amountHTML += 'CGST';
				} else if (_taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
					amountHTML += transactionHTMLGenerator.stateTaxPlace;
				} else if (_taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
					amountHTML += 'CESS';
				} else if (_taxCode.getTaxRateType() == TaxCodeConstants.STATE_SPECIFIC_CESS) {
					amountHTML += _taxCode.getTaxCodeName();
				} else if (_taxCode.getTaxRateType() == TaxCodeConstants.OTHER) {
					amountHTML += _taxCode.getTaxCodeName();
				} else if (_taxCode.getTaxRateType() == TaxCodeConstants.Exempted) {
					amountHTML += 'Exempted';
				}
				amountHTML += '@' + MyDouble.getPercentageWithDecimal(_taxCode.getTaxRate()) + '%</td>';
				amountHTML += "<td align='right' >" + MyDouble.getAmountForInvoicePrint(_taxAmount) + '</td></tr>';
			}
		}

		if (txn.getAc1Amount() != 0) {
			amountHTML += '<tr><td>' + dataLoader.getCurrentExtraChargesValue('1') + '</td><td align="right">' + MyDouble.getAmountForInvoicePrint(txn.getAc1Amount()) + '</td></tr>';
		}
		if (txn.getAc2Amount() != 0) {
			amountHTML += '<tr><td>' + dataLoader.getCurrentExtraChargesValue('2') + '</td><td align="right">' + MyDouble.getAmountForInvoicePrint(txn.getAc2Amount()) + '</td></tr>';
		}
		if (txn.getAc3Amount() != 0) {
			amountHTML += '<tr><td>' + dataLoader.getCurrentExtraChargesValue('3') + '</td><td align="right">' + MyDouble.getAmountForInvoicePrint(txn.getAc3Amount()) + '</td></tr>';
		}

		if (txn.getRoundOffValue() != 0) {
			amountHTML += '<tr><td>Round off</td><td align="right">' + MyDouble.getAmountForInvoicePrint(txn.getRoundOffValue()) + '</td></tr>';
		}

		if (txnType != TxnTypeConstant.TXN_TYPE_CASHIN && txnType != TxnTypeConstant.TXN_TYPE_CASHOUT || (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) && discountAmount != 0) {
			amountHTML += "<tr style='background-color:" + this.secondaryColor + '; color:' + textColorForHeading + '; height: 25px;\'><td style="border-radius: 0px 0px 0px 12px;"><b>Total</b></td><td align="right"><b>' + MyDouble.getAmountForInvoicePrint(totalAmount) + '</b></td></tr>';
		}

		if ((txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) && txn.getReverseCharge() && reverseChargeAmount > 0) {
			var totalPayableString = 'Total payable amount';
			if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
				totalPayableString = 'Total receivable amount';
			}
			amountHTML += "<tr><td>Tax under reverse charge</td><td align='right'>" + MyDouble.getAmountForInvoicePrint(reverseChargeAmount) + '</td></tr>';
			amountHTML += '<tr><td>' + totalPayableString + "</td><td align='right'>" + MyDouble.getAmountForInvoicePrint(totalAmount - reverseChargeAmount) + '</td></tr>';
		}

		if (settingCache.isReceivedAmountEnabled()) {
			if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
				amountHTML += '<tr><td>Paid</td><td align="right">' + MyDouble.getAmountForInvoicePrint(cashAmount) + '</td></tr>';
			} else if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
				amountHTML += '<tr><td>Received</td><td align="right">' + MyDouble.getAmountForInvoicePrint(cashAmount) + '</td></tr>';
			} else if (txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
				amountHTML += '<tr><td>Advance</td><td align="right">' + MyDouble.getAmountForInvoicePrint(cashAmount) + '</td></tr>';
			}
		}

		if (settingCache.isReceivedAmountEnabled()) {
			if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
				amountHTML += '<tr><td>Balance</td><td align="right" >' + MyDouble.getAmountForInvoicePrint(balanceAmount) + '</td></tr>';
			}
		}

		if (settingCache.printPaymentMode() && txn.getTxnType() != TxnTypeConstant.TXN_TYPE_ESTIMATE && txn.getTxnType() != TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN) {
			var paymentTypeDescription = '';
			if (cashAmount == 0) {
				paymentTypeDescription = 'Credit';
			} else {
				paymentTypeDescription = PaymentInfoCache.getPaymentInfoObjById(txn.getPaymentTypeId());
				if (paymentTypeDescription) {
					paymentTypeDescription = paymentTypeDescription.getName();
				}
			}
			if (txn.getPaymentTypeReference()) {
				paymentTypeDescription += ' (' + txn.getPaymentTypeReference() + ')';
			}
			if (paymentTypeDescription) {
				amountHTML += '<tr><td>Payment Mode</td><td align="right" >' + paymentTypeDescription + '</td></tr>';
			}
		}

		if (settingCache.isCurrentBalanceOfPartyEnabled()) {
			try {
				var name = txn.getNameRef();
				switch (txnType) {
					case TxnTypeConstant.TXN_TYPE_CASHIN:
					case TxnTypeConstant.TXN_TYPE_SALE:
					case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
					case TxnTypeConstant.TXN_TYPE_CASHOUT:
					case TxnTypeConstant.TXN_TYPE_PURCHASE:
					case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
						if (name != null) {
							var currentAmount = MyDouble.getAmountForInvoicePrint(name.getAmount());
							var showPreviousBalance = true;
							if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
								var txnBalAmount = MyDouble.convertStringToDouble(txn.getBalanceAmount());
								var txnCurrentBalAmount = MyDouble.convertStringToDouble(txn.getTxnCurrentBalanceAmount());
								if (Math.abs(txnBalAmount - txnCurrentBalAmount) > 0.001) {
									showPreviousBalance = false;
								}
							}
							if (showPreviousBalance) {
								showPreviousBalance = TransactionPrintHelper.isLatestTransaction(txn);
							}
							if (showPreviousBalance) {
								amount = TransactionPrintHelper.getPreviousAmount(txn, name);
								amountHTML += '<tr><td>Previous Balance</td><td align="right">' + MyDouble.getAmountForInvoicePrint(amount) + '</td></tr>';
							}
							if (currentAmount) {
								amountHTML += '<tr><td>Current Balance</td><td align="right">' + currentAmount + '</td></tr>';
							}
						}
						break;
				}
			} catch (e) {}
		}

		// amountHTML +=
		//     '<tr><td class="borderTopForTxn theme10BorderColor"></td><td align="right" class="borderTopForTxn theme10BorderColor"></td></tr>';

		amountHTML += '</table>';

		return amountHTML;
	},

	getLeftFooterTable: function getLeftFooterTable(txn, printAsDeliveryChallan, textSizeRatio) {
		var leftText = this.getDescriptionData(txn, textSizeRatio);
		leftText += this.getAmountInWordsHTML(txn, printAsDeliveryChallan, textSizeRatio);
		leftText += this.getTermsAndConditionsHTML(txn, printAsDeliveryChallan, textSizeRatio);
		leftText += this.getDeliveryChallanReceivedCopySign(printAsDeliveryChallan);
		return leftText;
	},

	getSignatureHTML: function getSignatureHTML(txn, textSizeRatio) {
		if (settingCache.isSignatureEnabled()) {
			var firm = firmCache.getTransactionFirmWithDefault(txn);
			var signatureHTML = '';
			var imagePath = firm.getFirmSignImagePath();

			if (imagePath != null && imagePath != '') {
				signatureHTML = '<table><tr><td><div>' + "<p style=' text-align: center; '>For, " + firm.getFirmName() + '</p>' + "<img src='data:image/png;base64," + imagePath + "' style='height: " + 64 * textSizeRatio + 'px; width: ' + 168 * textSizeRatio + "px; margin-left: 10px; margin-right: 10px; ' />" + "<hr style='border: 0.5px solid #ececec;  '/>" + "<p style=' color: black; font-weight: bold; font-size:" + textSizeRatio * 20 + "; text-align: center; '>" + settingCache.getSignatureText() + '</p>' + '</div></td></tr></table>';
			} else {
				signatureHTML = '<table><tr><td><div>' + "<p style=' text-align: center; '>For, " + firm.getFirmName() + '</p>' + "<img style='height: " + 64 * textSizeRatio + 'px; width: ' + 168 * textSizeRatio + "px; margin-left: 10px; margin-right: 10px; ' />" + "<hr style='border: 0.5px solid #ececec;  '/>" + "<p style=' color: black; font-weight: bold; font-size: " + textSizeRatio * 20 + "px; text-align: center; '>" + settingCache.getSignatureText() + '</p>' + '</div></td></tr></table>';
			}

			if (this.isForInvoicePreview) {
				var invoicePreviewAttrs = ' class="invoicePreviewEditSection ' + transactionHTMLGenerator.getEditSectionClassName('signatureLogo') + '"';
				signatureHTML = '\n\t\t\t\t<div style="width:100%;" editsection="signatureLogo" ' + invoicePreviewAttrs + '>\n\t\t\t\t\t' + signatureHTML + '\n\t\t\t\t</div>';
			}

			return signatureHTML;
		}
		return '';
	}
};

module.exports = TransactionTheme11HTMLGenerator;