var TaxRateReportHTMLGenerator = function TaxRateReportHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');
	var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
	var paymentInfoCache = new PaymentInfoCache();
	var NameCache = require('./../Cache/NameCache.js');
	var nameCache = new NameCache();

	this.getHTMLText = function (transactionList, fromDate, toDate, selectedFirmId) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'><h2 align='center'><u>Tax Rate Report</u></h2>" + ReportHTMLStyleSheet.getHTMLTextForDuration(fromDate, toDate) + ReportHTMLStyleSheet.getHTMLTextForFirm(selectedFirmId) + getHTMLTable(transactionList);
		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(listOfTransactions) {
		var len = listOfTransactions.length;
		var dynamicRow = '';
		dynamicRow += "<table class='reportTableForPrint' width='100%'><thead><tr style='background-color: lightgrey;'><th width='20%'>Tax Name</th><th width='12%' class='tableCellTextAlignRight'>Tax Percent </th><th width='17%' class='tableCellTextAlignRight'>Taxable Sale Amount </th><th width='17%' class='tableCellTextAlignRight'>Tax In</th><th width='17%' class='tableCellTextAlignRight'>Taxable Purchase Amount</th><th width='17%' class='tableCellTextAlignRight'>Tax Out</th></tr></thead>";

		//
		var totalTaxIn = 0;
		var totalTaxOut = 0;
		for (var i = 0; i < len; i++) {
			var txn = listOfTransactions[i];
			var taxName = txn.getTaxName();
			var taxIn = txn.getTaxIn();
			var taxOut = txn.getTaxOut();
			var taxPercent = txn.getTaxPercent();
			var sale_taxable = txn.getSaleTaxableValue();
			var purchase_taxable = txn.getPurchaseTaxableValue();
			var row = "<tr><td width='20%'>" + taxName + '</td>';
			row += "<td class='tableCellTextAlignRight' width='12%'>";
			if (taxPercent == -1) {
				row += ' - ';
			} else {
				row += MyDouble.getPercentageWithDecimal(taxPercent) + '%';
			}
			if (taxName == 'Additional CESS') {
				row += "</td><td width='17%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(sale_taxable) + ' (Quantity)</td>';
			} else {
				row += "</td><td width='17%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(sale_taxable) + '</td>';
			}

			row += "<td class='tableCellTextAlignRight' width='17%'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(taxIn) + '</td>';

			if (taxName == 'Additional CESS') {
				row += "<td width='17%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(purchase_taxable) + ' (Quantity)</td>';
			} else {
				row += "<td width='17%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(purchase_taxable) + '</td>';
			}
			row += "<td class='tableCellTextAlignRight' width='17%'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(taxOut) + '</td>';

			row += '</tr>';
			totalTaxIn += taxIn;
			totalTaxOut += taxOut;
			dynamicRow += row;
		}
		dynamicRow += '</table>';
		dynamicRow += "<table width='100%'><tr style='background-color: lightgrey;'><th width='20%'></th><th width='29%' class='tableCellTextAlignLeft'>Total</th><th width='17%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalTaxIn) + "</th><th width='17%'></th><th width='17%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalTaxOut) + '</th></tr></table>';

		return dynamicRow;
	};
};

module.exports = TaxRateReportHTMLGenerator;