var DayBookReportHTMLGenerator = function DayBookReportHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');

	this.getHTMLText = function (transactionList, fromDate, selectedFirmId, shouldPrintItemDetails, shouldPrintDescription) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div  class='pdfReportHTMLView'><h2 align='center'><u>DayBook Report</u></h2>" + ReportHTMLStyleSheet.getHTMLTextForDate(fromDate) + ReportHTMLStyleSheet.getHTMLTextForFirm(selectedFirmId) + getHTMLTable(transactionList, shouldPrintItemDetails, shouldPrintDescription);

		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(listOfTransactions, shouldPrintItemDetails, shouldPrintDescription) {
		var len = listOfTransactions.length;
		var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
		var TransactionHTMLGenerator = require('./../ReportHTMLGenerator/TransactionHTMLGenerator.js');
		var PaymentCache = require('./../Cache/PaymentInfoCache.js');
		var paymentCache = new PaymentCache();
		var transactionHTMLGenerator = new TransactionHTMLGenerator();
		var SettingCache = require('./../Cache/SettingCache.js');
		var settingCache = new SettingCache();
		var dynamicRow = '';
		var isTransactionRefNumberEnabled = settingCache.getTxnRefNoEnabled();
		var extraColumnWidth = isTransactionRefNumberEnabled ? 0 : 2;

		dynamicRow += "<table class='reportTableForPrint' width='100%'><thead><tr style='background-color: lightgrey;'><th width='" + (19 + extraColumnWidth) + "%' class='tableCellTextAlignLeft'>Name</th>";
		if (isTransactionRefNumberEnabled) {
			dynamicRow += "<th width='12%' class='tableCellTextAlignLeft'>Ref No.</th>";
		}
		dynamicRow += "<th width='" + (15 + extraColumnWidth) + "%' class='tableCellTextAlignLeft'>Type</th><th width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>Total</th><th width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>Money in</th><th width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>Money out</th></tr></thead>";

		var totalCashIn = 0;
		var totalCashOut = 0;

		for (var i = 0; i < len; i++) {
			var txn = listOfTransactions[i];
			var printItemDetails = false;
			var printDescription = false;
			var classToBeIncluded = '';
			var txnItemLines = null;
			if (txn.getLineItems) {
				txnItemLines = txn.getLineItems();
			}
			var txnType = txn.getTxnType();
			printItemDetails = shouldPrintItemDetails && txnItemLines && txnItemLines.length && !TxnTypeConstant.isLoanTxnType(txnType);
			printDescription = shouldPrintDescription && txn.getDescription() && !TxnTypeConstant.isLoanTxnType(txnType);

			var classToBeIncludedInStatementRow = '';

			if (shouldPrintItemDetails || shouldPrintDescription) {
				classToBeIncludedInStatementRow += 'boldText extraTopPadding ';
				if (printItemDetails || printDescription) {
					classToBeIncludedInStatementRow += ' noBorder ';
				}
				classToBeIncludedInStatementRow = " class='" + classToBeIncludedInStatementRow + "'";
			}

			var classToBeIncludedInItemDetailRow = '';
			if (printDescription) {
				classToBeIncludedInItemDetailRow = " class='noBorder' ";
			}

			var typeString = TxnTypeConstant.getTxnTypeForUI(txnType);
			var cashAmt = txn.getCashAmount() ? Number(txn.getCashAmount()) : 0;
			var balanceAmt = txn.getBalanceAmount() ? Number(txn.getBalanceAmount()) : 0;
			var totalAmt = 0;
			var discountAmt = 0;
			var refNo = '';
			if (!TxnTypeConstant.isLoanTxnType(txnType)) {
				discountAmt = txn.getDiscountAmount() ? txn.getDiscountAmount() : 0;
				refNo = txn.getTxnRefNumber() === '' ? '' : txn.getFullTxnRefNumber();
			} else {
				var subTxnType = txn.getTxnSubType();
				typeString = TxnTypeConstant.getTxnTypeForUI(txnType, subTxnType);
			}
			if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
				totalAmt = Number(cashAmt) + Number(discountAmt);
			} else {
				totalAmt = Number(cashAmt) + Number(balanceAmt);
			}
			var date = MyDate.getDate('d/m/y', txn.getTxnDate());

			if (txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD || txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE || txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD_WITHOUT_CASH || txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE_WITHOUT_CASH || txnType == TxnTypeConstant.TXN_TYPE_BANK_TO_BANK) {
				if (txnType == TxnTypeConstant.TXN_TYPE_BANK_TO_BANK) {
					var row = '<tr><td ' + classToBeIncludedInStatementRow + '>' + paymentCache.getPaymentBankName(txn.getBankId()) + ' TO ' + paymentCache.getPaymentBankName(txn.getToBankId()) + '</td>';
				} else {
					var row = '<tr><td ' + classToBeIncludedInStatementRow + '>' + paymentCache.getPaymentBankName(txn.getBankId()) + '</td>';
				}
			} else if (txnType == TxnTypeConstant.TXN_TYPE_CASH_ADJ_ADD || txnType == TxnTypeConstant.TXN_TYPE_CASH_ADJ_REDUCE) {
				var row = '<tr><td ' + classToBeIncludedInStatementRow + '> Cash Adjustment </td>';
			} else if (txnType == TxnTypeConstant.TXN_TYPE_CHEQUE_TRANSFER) {
				var name = txn.getNameRef().getFullName();
				if (txn.getTransferedTobankId()) {
					var row = '<tr><td ' + classToBeIncludedInStatementRow + ' >Cheque to ' + paymentCache.getPaymentBankName(txn.getTransferedTobankId()) + '</td>';
				} else {
					var row = '<tr><td ' + classToBeIncludedInStatementRow + ' >' + name + '</td>';
				}
			} else if (TxnTypeConstant.isLoanTxnType(txnType)) {
				var row = '<tr><td ' + classToBeIncludedInStatementRow + '>' + txn.getDescription() + '</td>';
			} else {
				var name = txn.getNameRef().getFullName();
				var row = '<tr><td ' + classToBeIncludedInStatementRow + '>' + name + '</td>';
			}

			if (isTransactionRefNumberEnabled) {
				row += '<td ' + classToBeIncludedInStatementRow + '>' + refNo + '</td>';
			}

			row += "<td width='" + (15 + extraColumnWidth) + "%'>" + typeString + "</td><td width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalAmt) + '</td>';

			if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_OTHER_INCOME || txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD_WITHOUT_CASH || txnType == TxnTypeConstant.TXN_TYPE_CASH_ADJ_ADD) {
				totalCashIn += cashAmt;
				row += "<td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(cashAmt) + "</td><td align='right' " + classToBeIncludedInStatementRow + '></td>';
			} else if (txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT || txnType == TxnTypeConstant.TXN_TYPE_EXPENSE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE_WITHOUT_CASH || txnType == TxnTypeConstant.TXN_TYPE_CASH_ADJ_REDUCE) {
				totalCashOut += cashAmt;
				row += "<td align='right' " + classToBeIncludedInStatementRow + "></td><td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(cashAmt) + '</td>';
			} else if (TxnTypeConstant.isLoanTxnType(txnType)) {
				if (txnType == TxnTypeConstant.TXN_TYPE_LOAN_STARTING_BALANCE || txnType == TxnTypeConstant.TXN_TYPE_LOAN_ADJUSTMENT) {
					totalCashIn += cashAmt;
					row += "<td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(cashAmt) + "</td><td align='right' " + classToBeIncludedInStatementRow + '></td>';
				} else {
					totalCashOut += cashAmt;
					row += "<td align='right' " + classToBeIncludedInStatementRow + "></td><td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(cashAmt) + '</td>';
				}
			} else {
				row += "<td align='right' " + classToBeIncludedInStatementRow + "></td><td align='right' " + classToBeIncludedInStatementRow + '></td>';
			}

			row += '</tr>';

			dynamicRow += row;

			var noOfColumns = isTransactionRefNumberEnabled ? 6 : 5;

			if (printItemDetails) {
				dynamicRow += '<tr>\n' + '  <td ' + classToBeIncludedInItemDetailRow + ' colspan="1" ></td>\n' + '  <td ' + classToBeIncludedInItemDetailRow + ' colspan="' + (noOfColumns - 1) + '">' + transactionHTMLGenerator.getItemDetailsForReport(txn) + '</td>\n' + '</tr>\n';
			}
			if (printDescription) {
				dynamicRow += '<tr>\n' + '  <td colspan="' + noOfColumns + '"> <span class="boldText"> Description: </span>' + txn.getDescription() + '</td>\n' + '</tr>\n';
			}
		}
		dynamicRow += '</table>';

		dynamicRow += "<table width='100%'>";
		dynamicRow += "<tr style='background-color: lightgrey;'><th width='" + (19 + extraColumnWidth) + "%' class='tableCellTextAlignLeft'>Total</th>";
		if (isTransactionRefNumberEnabled) {
			dynamicRow += "<th width='12%' class='tableCellTextAlignLeft'></th>";
		}
		dynamicRow += "<th width='" + (15 + extraColumnWidth) + "%' class='tableCellTextAlignLeft'></th><th width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'></th><th width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalCashIn) + "</th><th width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalCashOut) + '</th></tr>';

		var totalColSpan = 4;
		var totalColumnWidth = 100 - (18 + extraColumnWidth);
		if (isTransactionRefNumberEnabled) {
			totalColSpan = 5;
		}
		dynamicRow += "<tr style='background-color: lightgrey;'><th width='" + totalColumnWidth + "%' colspan='" + totalColSpan + "' class='tableCellTextAlignLeft'>Total Money In - Total Money Out</th>";

		dynamicRow += "<th width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalCashIn - totalCashOut) + '</th></tr>';
		dynamicRow += '</table>';
		return dynamicRow;
	};
};

module.exports = DayBookReportHTMLGenerator;