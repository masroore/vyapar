var SalePurchaseReportByPartyHTMLGenerator = function SalePurchaseReportByPartyHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');

	this.getHTMLText = function (transactionList, fromDate, toDate, selectedFirmId, nameString) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'><h2 align='center'><u>Sale Purchase By Party Report</u></h2>" + ReportHTMLStyleSheet.getHTMLTextForDuration(fromDate, toDate) + ReportHTMLStyleSheet.getHTMLTextForFirm(selectedFirmId) + getHTMLTable(transactionList);

		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(listOfTransactions) {
		var len = listOfTransactions.length;
		var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
		var dynamicRow = '';

		dynamicRow += "<table class='reportTableForPrint' width='100%'><thead><tr style='background-color: lightgrey;'><th width='40%'>Party Name</th><th width='30%' class='tableCellTextAlignRight'>Sale Amount</th><th width='30%' class='tableCellTextAlignRight'>Purchase Amount</th></tr></thead>";

		var totalSaleAmount = 0;
		var totalPurchaseAmount = 0;
		for (var i = 0; i < len; i++) {
			var row = '';
			var txn = listOfTransactions[i];
			var partyName = txn.get('partyName');
			var saleAmount = txn.get('saleAmount');
			var purchaseAmount = txn.get('purchaseAmount');
			row += "<tr><td width='40%'>" + partyName + "</td><td width='30%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(saleAmount) + "</td><td width='30%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(purchaseAmount) + '</td><tr>';
			dynamicRow += row;
			totalSaleAmount += saleAmount;
			totalPurchaseAmount += purchaseAmount;
		}
		dynamicRow += '</table>';
		dynamicRow += "<table width='100%'><tr style='background-color: lightgrey;'><th width='40%' class='tableCellTextAlignLeft'>Total</th><th width='30%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalSaleAmount) + "</th><th width='30%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalPurchaseAmount) + '</th></tr></table>';

		return dynamicRow;
	};
};

module.exports = SalePurchaseReportByPartyHTMLGenerator;