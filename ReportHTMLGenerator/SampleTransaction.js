var SampleTransaction = function SampleTransaction() {
	var BaseTransaction = require('./../BizLogic/BaseTransaction.js');
	var TransactionFactory = require('./../BizLogic/TransactionFactory.js');
	var NameLogic = require('./../BizLogic/nameLogic.js');
	var NameType = require('./../Constants/NameType.js');
	this.getSampleTxn = function (txnType) {
		var baseTransaction = new BaseTransaction();
		var transactionFactory = new TransactionFactory();
		var sampleTxn = transactionFactory.getTransactionObject(txnType);
		sampleTxn.setTxnDate(new Date());
		sampleTxn.setTxnRefNumber('INV234');
		sampleTxn.setDescription('Balance to be paid in 3 days');
		sampleTxn.setDiscountAmount(80.0);
		sampleTxn.setTaxAmount(72.0);
		sampleTxn.setCashAmount(300.0);
		sampleTxn.setBalanceAmount(492.0);

		var lineItemUIData1 = {
			'name': 'sample item 1',
			'quantity': 3,
			'freequantity': 0,
			'unitprice': 100,
			'discountamount': 0,
			'discountpercent': 0,
			'taxamount': 0,
			'totalamount': 300
		};

		var lineItemUIData2 = {
			'name': 'sample item 2',
			'quantity': 7,
			'freequantity': 0,
			'unitprice': 300,
			'discountamount': 0,
			'discountpercent': 0,
			'taxamount': 0,
			'totalamount': 2100
		};

		var lineItemUIData3 = {
			'name': 'sample item 3',
			'quantity': 5,
			'freequantity': 0,
			'unitprice': 100,
			'discountamount': 0,
			'discountpercent': 0,
			'taxamount': 0,
			'totalamount': 500
		};

		sampleTxn.addLineItem(lineItemUIData1);
		sampleTxn.addLineItem(lineItemUIData2);
		sampleTxn.addLineItem(lineItemUIData3);
		sampleTxn.setPaymentTypeId(1);

		var obj = {
			'name': 'Vyapar tech solutions',
			'phone_number': '+91-9916137039',
			'addressStr': 'Sarjapur Road, Bangalore',
			'email': '',
			'tinNo': '1234567890'
		};

		var name = new NameLogic(obj);
		sampleTxn.setNameRef(name);
		return sampleTxn;
	};
};

module.exports = SampleTransaction;