var ProfitAndLossReportHTMLGenerator = function ProfitAndLossReportHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');
	var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
	var paymentInfoCache = new PaymentInfoCache();
	var NameCache = require('./../Cache/NameCache.js');
	var nameCache = new NameCache();

	this.getHTMLText = function (transactionList, fromDate, toDate) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'><h2 align='center'><u>Profit And Loss Report</u></h2>" + ReportHTMLStyleSheet.getHTMLTextForDuration(fromDate, toDate) + getHTMLTable(transactionList);
		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(listOfTransactions) {
		var dynamicRow = '';
		dynamicRow += "<table width='100%'><thead><tr style='background-color: lightgrey;'><th width='50%' class='tableCellTextAlignLeft'>Particulars</th><th width='50%' class='tableCellTextAlignRight'>Amount</th></tr></thead>";
		var saleAmount = listOfTransactions.getSaleAmount();
		var saleReturnAmount = listOfTransactions.getSaleReturnAmount();
		var purchaseAmount = listOfTransactions.getPurchaseAmount();
		var purchaseReturnAmount = listOfTransactions.getPurchaseReturnAmount();
		var paymentInDiscountAmount = listOfTransactions.getPaymentInDiscountAmount();
		var loanInterestExpense = listOfTransactions.getLoanInterestExpense();
		var loanProcessingFeeExpense = listOfTransactions.getLoanProcessingFeeExpense();
		var otherExpenseAmount = listOfTransactions.getOtherExpenseAmount();
		var paymentOutDiscountAmount = listOfTransactions.getPaymentOutDiscountAmount();
		var otherIncomeAmount = listOfTransactions.getOtherIncomeAmount();
		var grossProfitAndLossAmount = listOfTransactions.getGrossProfitAndLossAmount();
		var openingStockValue = listOfTransactions.getOpeningStockValue();
		var closingStockValue = listOfTransactions.getClosingStockValue();
		var netProfitAndLossAmount = listOfTransactions.getNetProfitAndLossAmount();
		var taxReceivable = listOfTransactions.getReceivableTax();
		var taxPayable = listOfTransactions.getPayableTax();
		dynamicRow += '<tbody>';
		dynamicRow += "<tr><td width='50%'>Sale (+) </td><td width='50%' class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(saleAmount) + '</td></tr>';
		dynamicRow += "<tr><td width='50%'>Credit Note (-) </td><td width='50%' class='tableCellTextAlignRight' >" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(saleReturnAmount) + '</td></tr>';
		dynamicRow += "<tr><td width='50%'>Purchase (-) </td><td width='50%' class='tableCellTextAlignRight' >" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(purchaseAmount) + '</td></tr>';
		dynamicRow += "<tr><td width='50%'>Debit Note (+) </td><td width='50%' class='tableCellTextAlignRight' >" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(purchaseReturnAmount) + '</td></tr>';

		// income
		dynamicRow += Number(paymentOutDiscountAmount) != 0 ? "<tr class='currentRow'><td class='' style='' width='50%'>Payment-out Discount (+)</td><td width='50%' class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(paymentOutDiscountAmount) + '</td></tr>' : '';

		// expense
		dynamicRow += Number(paymentInDiscountAmount) != 0 ? "<tr class='currentRow'><td class='' style='' width='50%'>Payment-in Discount (-)</td><td width='50%' class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(paymentInDiscountAmount) + '</td></tr>' : '';

		// Tax
		dynamicRow += "<tr class='currentRow'><td colspan='2' width='100%'>Tax </td></tr>";
		dynamicRow += "<tr class='currentRow greybg'><td class='pl-20' width='50%'>Tax Payable(-)</td><td width='50%' class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(taxPayable) + '</td></tr>';
		dynamicRow += "<tr class='currentRow greybg'><td class='pl-20' width='50%'>Tax Receivable(+)</td><td width='50%' class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(taxReceivable) + '</td></tr>';

		dynamicRow += "<tr><td width='50%'>Opening Stock (-) </td><td width='50%' class='tableCellTextAlignRight' >" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(openingStockValue) + '</td></tr>';
		dynamicRow += "<tr><td width='50%'>Closing Stock (+) </td><td width='50%' class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(closingStockValue) + '</td></tr>';
		dynamicRow += "<tr style='background-color: lightgrey;'><td width='50%'>";
		if (grossProfitAndLossAmount >= 0) {
			dynamicRow += 'Gross Profit';
		} else {
			dynamicRow += 'Gross Loss';
		}
		dynamicRow += "</td><td width='50%' class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(grossProfitAndLossAmount) + '</td></tr>';
		dynamicRow += "<tr class='currentRow'><td width='50%'>Other Income(+)</td><td width='50%' class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(otherIncomeAmount) + '</td></tr>';
		// Indirect Expenses
		dynamicRow += "<tr class='currentRow whitebg'><td colspan='2' width='100%'>Indirect Expenses(-)</td></tr>";
		dynamicRow += "<tr class='currentRow greybg'><td class='pl-20' width='50%'>Other Expense</td><td width='50%' class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(otherExpenseAmount) + '</td></tr>';
		dynamicRow += "<tr class='currentRow greybg'><td class='pl-20' width='50%'>Loan Interest Expense</td><td width='50%' class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(loanInterestExpense) + '</td></tr>';
		dynamicRow += "<tr class='currentRow greybg'><td class='pl-20' width='50%'>Loan Processing Fee Expense</td><td width='50%' class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(loanProcessingFeeExpense) + '</td></tr>';
		dynamicRow += "<tr style='background-color: lightgrey;'><td width='50%'>";
		if (netProfitAndLossAmount >= 0) {
			dynamicRow += 'Net Profit';
		} else {
			dynamicRow += 'Net Loss';
		}
		dynamicRow += "</td><td width='50%' class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(netProfitAndLossAmount) + '</td></tr>';
		dynamicRow += '</tbody></table>';
		return dynamicRow;
	};
};

module.exports = ProfitAndLossReportHTMLGenerator;