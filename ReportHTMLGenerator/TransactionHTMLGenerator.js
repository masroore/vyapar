var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DeviceHelper = require('../Utilities/deviceHelper');
var TransactionHTMLGenerator = function TransactionHTMLGenerator() {
	var BaseTransaction = require('./../BizLogic/BaseTransaction.js');
	var SettingCache = require('./../Cache/SettingCache.js');
	var FirmCache = require('./../Cache/FirmCache.js');
	var firmCache = new FirmCache();
	var ItemCache = require('./../Cache/ItemCache.js');
	var ItemUnitMappingCache = require('./../Cache/ItemUnitMappingCache.js');
	var ItemUnitCache = require('./../Cache/ItemUnitCache.js');
	var TransactionFactory = require('./../BizLogic/TransactionFactory.js');
	var InvoiceTheme = require('./../Constants/InvoiceTheme.js');
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var TaxCodeConstants = require('./../Constants/TaxCodeConstants.js');
	var TransactionPDFPaperSize = require('./../Constants/TransactionPDFPaperSize.js');
	var DataLoader = require('./../DBManager/DataLoader.js');
	var CurrencyHelper = require('./../Utilities/CurrencyHelper.js');
	var dataLoader = new DataLoader();
	var TaxCodeCache = require('./../Cache/TaxCodeCache.js');
	var taxCodeCache = new TaxCodeCache();
	var Queries = require('./../Constants/Queries.js');
	var UDFCache = require('./../Cache/UDFCache.js');
	var StateCode = require('./../Constants/StateCode.js');
	var TransactionPrintSettings = require('./TransactionPrintSettings.js');
	var settingCache = new SettingCache();
	this.stateTaxPlace = 'SGST';
	var exemptedTaxString = 'Exmp.';
	var TransactionPrintHelper = require('./../Utilities/TransactionPrintHelper.js');
	var TransactionTheme10HTMLGenerator = require('./../ReportHTMLGenerator/TransactionTheme10HTMLGenerator');
	var TransactionTheme11HTMLGenerator = require('./../ReportHTMLGenerator/TransactionTheme11HTMLGenerator');
	var TransactionTheme12HTMLGenerator = require('./../ReportHTMLGenerator/TransactionTheme12HTMLGenerator');
	var TransactionUtil = require('./../Utilities/TransactionUtil');
	var QRImage = require('qr-image');
	var upiLogoBuffer = '';

	this.getEditSectionClassName = function (sectionName) {
		var _require = require('../Constants/StringConstants'),
		    maxOpenCountForEditPreviewSection = _require.maxOpenCountForEditPreviewSection;

		var openCount = window.localStorage.getItem('openInvoicePreviewCount') || '';
		openCount = Number(openCount);
		if (openCount > maxOpenCountForEditPreviewSection) {
			return '';
		} else {
			return window.localStorage.getItem(sectionName) === 'true' ? '' : 'newEditSection';
		}
	};

	this.getTransactionHTMLBody = function (txn, theme, themeColor, textSizeRatio, addPageBreakAfterTransaction, printDeliveryChallan) {
		var doubleColorId = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : null;

		if (!txn) {
			return '<h2>Something went wrong. Please contact Vyapar team.</h2>';
		}
		if (!settingCache) {
			settingCache = new SettingCache();
		}
		this.stateTaxPlace = 'SGST';

		var firmState = firmCache.getTransactionFirmWithDefault(txn).getFirmState();
		if (firmState && StateCode.isUnionTerritory(firmState)) {
			this.stateTaxPlace = 'UTGST';
		}

		var pageSize = settingCache.getPrintPaperSize();

		var bodyHtml = '<div class="pdfTransactionHTMLView';
		if (addPageBreakAfterTransaction) {
			bodyHtml += ' page-break';
		}
		bodyHtml += '">';
		if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN) {
			printDeliveryChallan = true;
		}
		//
		if (theme == InvoiceTheme.THEME_11) {
			doubleColorId = doubleColorId || settingCache.getTxnPDFDoubleThemeColor();
			if (this.isForInvoicePreview) {
				bodyHtml += TransactionTheme11HTMLGenerator.getTransactionHTMLForPreview(txn, doubleColorId, pageSize, textSizeRatio, printDeliveryChallan);
			} else {
				bodyHtml += TransactionTheme11HTMLGenerator.getTransactionHTML(txn, doubleColorId, pageSize, textSizeRatio, printDeliveryChallan);
			}
		} else if (theme == InvoiceTheme.THEME_12) {
			if (this.isForInvoicePreview) {
				bodyHtml += TransactionTheme12HTMLGenerator.getTransactionHTMLForPreview(txn, themeColor, pageSize, textSizeRatio, printDeliveryChallan);
			} else {
				bodyHtml += TransactionTheme12HTMLGenerator.getTransactionHTML(txn, themeColor, pageSize, textSizeRatio, printDeliveryChallan);
			}
		} else if (theme == InvoiceTheme.THEME_10) {
			if (this.isForInvoicePreview) {
				bodyHtml += TransactionTheme10HTMLGenerator.getTheme10TransactionHTMLForPreview(txn, themeColor, pageSize, textSizeRatio, printDeliveryChallan);
			} else {
				bodyHtml += TransactionTheme10HTMLGenerator.getTheme10TransactionHTML(txn, themeColor, pageSize, textSizeRatio, printDeliveryChallan);
			}
		} else if (theme == InvoiceTheme.THEME_4) {
			bodyHtml += this.getOldTransactionHTML(txn, themeColor, pageSize, textSizeRatio, printDeliveryChallan);
		} else if (theme == InvoiceTheme.THEME_5 || theme == InvoiceTheme.THEME_6 || theme == InvoiceTheme.THEME_7 || theme == InvoiceTheme.THEME_8) {
			bodyHtml += this.getTheme5And6And7And8TransactionHTML(txn, theme, themeColor, pageSize, textSizeRatio, printDeliveryChallan);
		} else if (theme == InvoiceTheme.THEME_9) {
			bodyHtml += this.getTheme9TransactionHTML(txn, themeColor, pageSize, textSizeRatio, printDeliveryChallan);
		} else {
			bodyHtml += this.getExtraTopSpace(textSizeRatio) + this.getCompanyHeader(txn.getFirmId(), theme, themeColor, textSizeRatio) + this.getTransactionHeader(txn, theme, themeColor, textSizeRatio, printDeliveryChallan) + this.getPartyInfoAndTxnHeaderData(txn, theme, themeColor, textSizeRatio);

			var transactionType = txn.getTxnType();
			if (transactionType == TxnTypeConstant.TXN_TYPE_SALE || transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE || transactionType == TxnTypeConstant.TXN_TYPE_EXPENSE || transactionType == TxnTypeConstant.TXN_TYPE_OTHER_INCOME || transactionType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || transactionType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER || transactionType == TxnTypeConstant.TXN_TYPE_ESTIMATE || transactionType == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN) {
				bodyHtml += this.getItemDetails(txn, theme, themeColor, textSizeRatio, printDeliveryChallan);
			}

			if (!printDeliveryChallan || settingCache.printAmountDetailsInDeliveryChallan()) {
				bodyHtml += this.getAmountAndDescriptionHTML(txn, theme, themeColor, textSizeRatio, printDeliveryChallan);
			}
			bodyHtml += this.getSignature(txn, theme, themeColor, textSizeRatio, printDeliveryChallan);
		}
		if (theme != InvoiceTheme.THEME_11 && theme != InvoiceTheme.THEME_12) {
			bodyHtml += this.addAcknowledgment(txn, themeColor, textSizeRatio);
		}
		bodyHtml += '</div>';
		return bodyHtml;
	};
	this.getUPIPaymentDetails = function (txn, textSizeRatio) {
		var txnType = txn.txnType;var firm;

		if (MyDouble.convertStringToDouble(txn.getTxnCurrentBalanceAmount()) > 0.01 && TransactionUtil.isUPIEnabledForTxnType(txnType)) {
			firm = firmCache.getTransactionFirmWithDefault(txn);
			if (firm != null) {
				var upiAPILink = this.getUPIAPILink(firm, txn);
				if (upiAPILink) {
					return this.generateUPIHTML(txn, firm, upiAPILink, textSizeRatio);
				}
			}
		}
		return '';
	};
	this.getUPIAddress = function (firmObj, txn) {
		if (!firmObj.getUpiAccountNumber() || !firmObj.getUpiIFSC()) {
			return '';
		}

		var upiAddress = 'upi://pay?' + 'pn=' + escape(firmObj.getFirmName()) + '&pa=' + firmObj.getUpiAccountNumber() + '@' + firmObj.getUpiIFSC() + '.ifsc.npci' + '&am=' + MyDouble.roundOffToNDecimalPlaces(txn.getTxnCurrentBalanceAmount(), 2) + '&mam=0.01' + (txn.getFullTxnRefNumber() ? '&tn=Vyapar_' + txn.getFullTxnRefNumber() : '');
		return upiAddress;
	};
	this.getUPIAPILink = function (firmObj, txn) {
		if (!firmObj.getUpiAccountNumber() || !firmObj.getUpiIFSC()) {
			return '';
		}
		var deviceInfo = DeviceHelper.getDeviceInfo();
		var upiLink = 'https://vyaparapp.in/api/upi?' + 'pn=' + escape(firmObj.getFirmName()) + '&pa=' + firmObj.getUpiAccountNumber() + '@' + firmObj.getUpiIFSC() + '.ifsc.npci' + '&am=' + MyDouble.roundOffToNDecimalPlaces(txn.getTxnCurrentBalanceAmount(), 2) + '&mam=0.01' + (txn.getFullTxnRefNumber() ? '&tn=Vyapar_' + txn.getFullTxnRefNumber() : '') + '&did=' + deviceInfo.deviceId;
		return encodeURI(upiLink);
	};

	this.generateUPIHTML = function (txn, firm, upiAPILink, textSizeRatio) {
		var upiDetails = '';
		var upiWidth = 120 * textSizeRatio;

		var qrCodeImage = this.initQRCode(txn, firm).toString('base64');

		upiDetails = "<table width='100%'>";
		if (qrCodeImage) {
			upiDetails += "<tr><td align='left'>" + "<a href='" + upiAPILink + "' style='text-decoration: none;'>" + ('<img src=\'data:image/png;base64,' + qrCodeImage + '\' style=\'height: ' + upiWidth + 'px; width: ' + upiWidth + 'px;\'/>') + '</a></td></tr>';
		}

		try {
			if (!upiLogoBuffer) {
				var bitMap = fs.readFileSync(__dirname + './../inlineSVG/upi_logo_pay_now.png');
				upiLogoBuffer = new Buffer(bitMap).toString('base64');
			}
		} catch (e) {
			console.error('UPI Logo image is not available....');
		}

		upiDetails += '<tr><td align=\'left\' class=\'noTopPadding\' ><a href=\'' + upiAPILink + '\' style=\'text-decoration: none;\'>' + ('<img src=\'data:image/png;base64,' + upiLogoBuffer + '\' style=\'width: ' + upiWidth + 'px; border-radius: 2px;\'/>') + '</a></td></tr></table>';

		return upiDetails;
	};

	this.generateQRCodeForPayment = function (upiAddress) {
		return QRImage.imageSync(upiAddress, { ec_level: 'M', type: 'png', margin: 1 });
	};
	this.addAcknowledgment = function (txn, themeColor, textSizeRatio) {
		if (settingCache.printAcknowledgment() && (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHOUT)) {
			var bodyHtml = '<br/>';
			bodyHtml += '<hr style="border-top: dashed 1px;" />';
			bodyHtml += '<div style="page-break-inside: avoid;"><div align="center" class="boldText largerTextSize marginBottom0 textUppercase">Acknowledgment</div>';
			bodyHtml += this.getAcknowledgmentFooterFirmName(txn, themeColor);
			bodyHtml += "<table width='100%'><tr><td width='50%' valign='top' style='padding:0px'>" + this.getAcknowledgmentFooterPartyInfo(txn) + '</td>';

			bodyHtml += "<td width='50%' align='right'  style=' vertical-align: center; '>" + this.getAcknowledgmentFooterTxnInfo(txn) + '</td></tr></table>';

			bodyHtml += '<div align="center" class=" bigTextSize">Receiver\'s Seal & Sign</div>';
			bodyHtml += '</div>';
			return bodyHtml;
		} else {
			return '';
		}
	};

	this.getAcknowledgmentFooterFirmName = function (txn, themeColor) {
		var firmName = firmCache.getTransactionFirmWithDefault(txn).getFirmName();
		if (themeColor == '#ffffff') {
			themeColor = 'black';
		}
		var htmlString = '<div align="center" class="textUppercase boldText extraLargeTextSize ackPdfMarginBottom7" style = "color:' + themeColor + ';" >' + firmName + '</div>';
		return htmlString;
	};

	this.getAcknowledgmentFooterFirmNamePremium = function (txn, themeColor, theme, textSizeRatio) {
		var firm = firmCache.getTransactionFirmWithDefault(txn);
		var firmName = firm.getFirmName();
		if (themeColor == '#ffffff') {
			themeColor = 'black';
		}
		var htmlString = '<div>';
		if (theme == InvoiceTheme.THEME_11) {
			htmlString += "<p class='center textUppercase boldText extraLargeTextSize ackPdfMarginBottom7' style='font-weight: normal; text-align: right; color: " + "'>" + firmName + '</p>';
		} else if (theme == InvoiceTheme.THEME_12) {
			htmlString += "<p class='center textUppercase extraLargeTextSize ackPdfMarginBottom7' style='font-weight: normal; font-size: " + textSizeRatio * 30 + 'px; text-align: right; color: ' + themeColor + ";'>" + firmName + '</p>';
		}

		if (settingCache.isPrintTINEnabled()) {
			if (settingCache.getGSTEnabled()) {
				var companyGSTINToBePrinted = firm.getFirmHSNSAC();
				var companyStateToBePrinted = firm.getFirmState();
				if (theme == InvoiceTheme.THEME_11) {
					if (companyGSTINToBePrinted) {
						htmlString += "<p style='text-align: right; font-weight: normal;'>GSTIN: " + firm.getFirmHSNSAC() + '</p>';
					}
					if (companyStateToBePrinted) {
						htmlString += "<p style='text-align: right; font-weight: normal;'>State: " + StateCode.getStateCode(firm.getFirmState()) + '-' + firm.getFirmState() + ' </p>';
					}
				} else if (theme == InvoiceTheme.THEME_12) {
					htmlString += "<table width='100%'>";
					if (companyGSTINToBePrinted) {
						htmlString += "<tr width='100%'><td width='50%' align='right' valign='top '><p style='font-weight: normal;'>GSTIN: " + firm.getFirmHSNSAC() + '</p></td></tr>';
					}
					if (companyStateToBePrinted) {
						htmlString += "<tr width='100%'><td width='50%' align='right' valign='top '><p style='font-weight: normal;'>State: " + StateCode.getStateCode(firm.getFirmState()) + '-' + firm.getFirmState() + '</p></td></tr>';
					}
					htmlString += '</table>';
				}
			} else {
				if (firm.getFirmTin()) {
					htmlString += "<p style='text-align: right; font-weight: normal;'>>State: " + settingCache.getTINText() + ': ' + firm.getFirmTin() + ' </p>';
				}
			}
		}
		htmlString += '</div>';
		return htmlString;
	};

	this.getAcknowledgmentFooterTxnInfo = function (txn, theme) {
		var txnInfoString = '';

		var refText = '';

		switch (txn.getTxnType()) {
			case TxnTypeConstant.TXN_TYPE_SALE:
				refText = 'Invoice';
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
				refText = 'Order';
				break;
			case TxnTypeConstant.TXN_TYPE_CASHOUT:
				refText = 'Receipt';
				break;
		}

		var classText = 'textUppercase';

		if (theme && (theme == InvoiceTheme.THEME_11 || theme == InvoiceTheme.THEME_12)) {
			classText = '';
		}

		if (txn.getTxnRefNumber()) {
			txnInfoString += '<p class="marginBottom0 ' + classText + ' largerTextSize">' + refText + ' No. : ' + txn.getFullTxnRefNumber() + '</p>';
		}
		txnInfoString += '<p class="marginBottom0 ' + classText + ' largerTextSize">' + refText + ' Date : ' + MyDate.getDate('d-m-y', txn.getTxnDate()) + '</p>';

		var balanceAmount = txn.getBalanceAmount() ? Number(txn.getBalanceAmount()) : 0;
		var cashAmount = txn.getCashAmount() ? Number(txn.getCashAmount()) : 0;
		var discountAmount = txn.getDiscountAmount() ? Number(txn.getDiscountAmount()) : 0;
		var reverseChargeAmount = txn.getReverseChargeAmount() ? Number(txn.getReverseChargeAmount()) : 0;
		var totalAmount = balanceAmount + cashAmount + reverseChargeAmount;
		if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHIN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHOUT) {
			totalAmount = totalAmount + discountAmount;
		} else {
			balanceAmount = txn.getTxnCurrentBalanceAmount() ? Number(txn.getTxnCurrentBalanceAmount()) : 0; // setting current balance amount
			cashAmount = totalAmount - balanceAmount - reverseChargeAmount; // setting total received amount
		}

		txnInfoString += '<p class="marginBottom0 ' + classText + ' largerTextSize">' + refText + ' Amount : ' + MyDouble.getAmountForInvoicePrint(totalAmount) + '</p>';

		return txnInfoString;
	};

	this.getAcknowledgmentFooterPartyInfo = function (txn, textSizeRatio, theme) {
		var party = txn.getNameRef();
		var partyInfoString = '';
		var partyFullName = '';
		if (txn.getDisplayName() && txn.getDisplayName().trim()) {
			partyFullName = txn.getDisplayName().trim();
		} else {
			partyFullName = party.getFullName();
		}
		if (partyFullName) {
			partyFullName = partyFullName.replace(/(?:\r\n|\r|\n)/g, '<br/>');
		}
		var partyAddress = party.getAddress();

		var classText = 'textUppercase';

		if (theme && (theme == InvoiceTheme.THEME_11 || theme == InvoiceTheme.THEME_12)) {
			partyInfoString += '<p class=\'theme11textUppercase \' style=\'font-weight: bold; font-size : ' + textSizeRatio * 30 + 'px; text-transform: capitalize; \'>' + partyFullName + '</p>';
			classText = '';
		} else {
			partyInfoString += '<p class="boldText largerTextSize">' + partyFullName + '</p>';
		}

		if (partyAddress) {
			partyAddress = partyAddress.replace(/(?:\r\n|\r|\n)/g, '<br/>');
			partyInfoString += '<p class="largerTextSize ' + classText + '"> ' + partyAddress + ' </p>';
		}
		return partyInfoString;
	};

	this.getTransactionHTMLLayoutForPreview = function (txn, theme, themeColor) {
		var doubleColorId = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

		this.isForInvoicePreview = true;
		var html = this.getTransactionHTMLLayout(txn, theme, themeColor, false, 2, doubleColorId);
		this.isForInvoicePreview = false;
		return html;
	};

	this.getTransactionHTMLLayout = function (txn, theme, themeColor, printDeliveryChallan, customTextSize) {
		var doubleColorId = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : null;

		if (!settingCache) {
			settingCache = new SettingCache();
		}
		var pageSize = settingCache.getPrintPaperSize();
		var textSizeSetting = customTextSize || settingCache.getPrintTextSize();

		var textSizeRatio = 0.4 + 0.1 * textSizeSetting;

		if (theme == InvoiceTheme.THEME_5 || theme == InvoiceTheme.THEME_6 || theme == InvoiceTheme.THEME_7 || theme == InvoiceTheme.THEME_8 || theme == InvoiceTheme.THEME_9) {
			textSizeRatio = textSizeRatio * 0.8;
		} else if (theme == InvoiceTheme.THEME_10) {
			textSizeRatio = textSizeRatio * 1.1;
		} else if (theme == InvoiceTheme.THEME_11 || theme == InvoiceTheme.THEME_12) {
			textSizeRatio = textSizeRatio * 0.8;
		}
		var bodyHtml = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + this.getStyleSheet(pageSize, textSizeRatio) + '</head> <body>' + this.getTransactionHTMLBody(txn, theme, themeColor, textSizeRatio, false, printDeliveryChallan, doubleColorId) + '</body></html>';

		return bodyHtml;
	};

	this.getOldTransactionHTML = function (txn, themeColor, pageSize, textSizeRatio, printDeliveryChallan) {
		var bodyHtml = this.getExtraTopSpace(textSizeRatio) + this.getOldCompanyHeader(txn, textSizeRatio) + this.getTransactionHeader(txn, InvoiceTheme.THEME_4, themeColor, textSizeRatio, printDeliveryChallan) + this.getPartyInfoAndTxnHeaderData(txn, InvoiceTheme.THEME_4, themeColor, textSizeRatio);

		var transactionType = txn.getTxnType();
		if (transactionType == TxnTypeConstant.TXN_TYPE_SALE || transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE || transactionType == TxnTypeConstant.TXN_TYPE_EXPENSE || transactionType == TxnTypeConstant.TXN_TYPE_OTHER_INCOME || transactionType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || transactionType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER || transactionType == TxnTypeConstant.TXN_TYPE_ESTIMATE || transactionType == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN) {
			bodyHtml += this.getItemDetails(txn, InvoiceTheme.THEME_4, themeColor, textSizeRatio, printDeliveryChallan);
		}
		if (!printDeliveryChallan || settingCache.printAmountDetailsInDeliveryChallan()) {
			bodyHtml += this.getAmountAndDescriptionHTML(txn, InvoiceTheme.THEME_4, themeColor, textSizeRatio, printDeliveryChallan);
		}
		bodyHtml += this.getSignature(txn, InvoiceTheme.THEME_4, themeColor, textSizeRatio, printDeliveryChallan);
		return bodyHtml;
	};

	this.getTheme5And6And7And8TransactionHTML = function (txn, theme, themeColor, pageSize, textSizeRatio, printDeliveryChallan) {
		var bodyHtml = this.getExtraTopSpace(textSizeRatio) + this.getTransactionHeader(txn, InvoiceTheme.THEME_5, themeColor, textSizeRatio, printDeliveryChallan) + this.getCompanyHeaderForTheme5(txn.getFirmId(), theme, textSizeRatio) + this.getPartyInfoAndTxnHeaderData(txn, InvoiceTheme.THEME_5, themeColor, textSizeRatio);

		var transactionType = txn.getTxnType();
		if (transactionType == TxnTypeConstant.TXN_TYPE_SALE || transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE || transactionType == TxnTypeConstant.TXN_TYPE_EXPENSE || transactionType == TxnTypeConstant.TXN_TYPE_OTHER_INCOME || transactionType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || transactionType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || transactionType == TxnTypeConstant.TXN_TYPE_ESTIMATE || transactionType == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
			bodyHtml += this.getItemDetails(txn, theme, themeColor, textSizeRatio, printDeliveryChallan);
		}

		if (!printDeliveryChallan || settingCache.printAmountDetailsInDeliveryChallan()) {
			bodyHtml += this.getAmountHTMLForTheme5(txn, theme, themeColor, textSizeRatio);
			bodyHtml += this.getDescriptionAndDueDateHTMLForTheme5(txn, theme, themeColor, textSizeRatio);
		}
		bodyHtml += this.getTermsAndSignature(txn, themeColor, textSizeRatio, printDeliveryChallan);
		return bodyHtml;
	};

	this.getTheme9TransactionHTML = function (txn, themeColor, pageSize, textSizeRatio, printDeliveryChallan) {
		var bodyHtml = this.getExtraTopSpace(textSizeRatio) + this.getTransactionHeader(txn, InvoiceTheme.THEME_5, themeColor, textSizeRatio, printDeliveryChallan) + this.getCompanyPartyAndTxnHeaderForTheme9(txn, themeColor, textSizeRatio);

		var transactionType = txn.getTxnType();
		if (transactionType == TxnTypeConstant.TXN_TYPE_SALE || transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE || transactionType == TxnTypeConstant.TXN_TYPE_EXPENSE || transactionType == TxnTypeConstant.TXN_TYPE_OTHER_INCOME || transactionType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || transactionType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || transactionType == TxnTypeConstant.TXN_TYPE_ESTIMATE || transactionType == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
			bodyHtml += this.getItemDetails(txn, InvoiceTheme.THEME_9, themeColor, textSizeRatio, printDeliveryChallan);
		}
		if (!printDeliveryChallan || settingCache.printAmountDetailsInDeliveryChallan()) {
			bodyHtml += this.getAmountAndDescriptionHTMLForTheme9(txn, themeColor, textSizeRatio);
			bodyHtml += this.getTaxCodeDistributionTableForTheme9(txn, themeColor, textSizeRatio);
		}
		bodyHtml += this.getTermsAndSignatureForTheme9(txn, themeColor, textSizeRatio, printDeliveryChallan);
		return bodyHtml;
	};

	this.getComputerGeneratedInvoiceText = function () {
		return "<table width='100%'><tr><td width='100%' align='center'>This is a Computer Generated Invoice (Powered by VYAPAR App)</td></tr></table>";
	};

	// case for adding different terms and conditions for different type of transactions ******
	this.getTermsAndSignatureForTheme9 = function (txn, themeColor, textSizeRatio, printDeliveryChallan) {
		var termsAndConditionHTML = '';
		switch (txn.getTxnType()) {
			case TxnTypeConstant.TXN_TYPE_SALE:
				var termsAndConditionText = settingCache.getTermsAndConditionSaleInvoivce();
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
				var termsAndConditionText = settingCache.getTermsAndConditionSaleOrder();
				break;
			case TxnTypeConstant.TXN_TYPE_ESTIMATE:
				var termsAndConditionText = settingCache.getTermsAndConditionEstimateQuotation();
				break;
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
				var termsAndConditionText = settingCache.getTermsAndConditionDeliveryChallan();
				break;
			default:
				var termsAndConditionText = settingCache.getTermsAndCondition();
		}
		if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE && settingCache.isPrintTermsAndConditionSaleInvoiceEnabled() || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER && settingCache.isPrintTermsAndConditionSaleOrderEnabled() || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE && settingCache.isPrintTermsAndConditionEstimateQuotationEnabled()) {
			if (printDeliveryChallan && settingCache.isPrintTermsAndConditionDeliveryChallanEnabled()) {
				var termsAndConditionText = settingCache.getTermsAndConditionDeliveryChallan();
			}
			if (!termsAndConditionText) {
				termsAndConditionText = settingCache.getTermsAndCondition() || 'Thanks for doing business with us!';
			}
			termsAndConditionText = termsAndConditionText.replace(/(?:\r\n|\r|\n)/g, '<br/>');

			termsAndConditionHTML += "<table width='100%'><tr><th align=\"left\" width=\"100%\"> Terms and conditions:</th></tr>" + '<tr><td width="100%">' + termsAndConditionText + '</td></tr></table>';
			if (printDeliveryChallan && !settingCache.isPrintTermsAndConditionDeliveryChallanEnabled()) {
				termsAndConditionHTML = '';
			}
			if (this.isForInvoicePreview) {
				var showEditView = this.getEditSectionClassName('termsAndConditions');
				var classText = ' class=\'invoicePreviewEditSection ' + showEditView + '\' ';
				termsAndConditionHTML = '<div style=\'width:100%;\' editsection=\'termsAndConditions\' ' + classText + '>' + termsAndConditionHTML + '</div>';
			}
		}
		if (printDeliveryChallan && settingCache.isPrintTermsAndConditionDeliveryChallanEnabled()) {
			termsAndConditionHTML += "<table width='100%'><tr><th align='left' width=\"100%\">Received By:</th></tr>" + "<tr><td width=\"100%\">Name: </td></tr><tr><td width=\"100%\">Comment: </td></tr><tr><td width=\"100%\">Date: </td></tr><tr><td width=\"100%\" style='padding-bottom: 10px;'>Signature: </td></tr>" + "<tr><th align='left' width='100%'>Delivered By:</th></tr>" + "<tr><td width=\"100%\">Name: </td></tr><tr><td width=\"100%\">Comment: </td></tr><tr><td width=\"100%\">Date: </td></tr><tr><td width=\"100%\" style='padding-bottom: 10px;'>Signature: </td></tr>" + '</table>';
		}

		var upiDetailsHTML = this.getUPIPaymentDetails(txn, textSizeRatio);
		if (upiDetailsHTML) {
			termsAndConditionHTML += upiDetailsHTML;
		}
		var firm = firmCache.getTransactionFirmWithDefault(txn);

		var bankDetailsHTML = '';
		if ((txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) && settingCache.getUserBankEnabled() && !printDeliveryChallan) {
			var bankDetails = '';
			if (firm) {
				if (firm.getBankName()) {
					bankDetails += '<tr><td width="100%">Bank Name: ' + firm.getBankName() + '</td></tr>';
				}
				if (firm.getAccountNumber()) {
					bankDetails += '<tr><td width="100%">Bank Account No.: ' + firm.getAccountNumber() + '</td></tr>';
				}
				if (firm.getBankIFSC()) {
					bankDetails += '<tr><td width="100%">Bank IFSC code: ' + firm.getBankIFSC() + '</td></tr>';
				}
				bankDetailsHTML = bankDetails ? '<table width=\'100%\'>\n\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<th align="left" width="100%">Company\'s Bank details:</th>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t' + bankDetails + '\n\t\t\t\t\t</table>' : '';
				if (this.isForInvoicePreview) {
					bankDetailsHTML = bankDetailsHTML || '<table width=\'100%\'><tr><th align="left" width="100%">Company\'s Bank details:</th></tr></table>';
					var _showEditView = this.getEditSectionClassName('payToDetails');
					var _classText = ' class=\'invoicePreviewEditSection ' + _showEditView + '\' ';
					bankDetailsHTML = '<div style="width:100%;" editsection=\'payToDetails\' ' + _classText + '>' + bankDetailsHTML + '</div>';
				}
			}
		}

		var signatureHTML = '';

		if (settingCache.isSignatureEnabled()) {
			var imageBlob = firm.getFirmSignImagePath();

			signatureHTML += "<table width='100%'>" + "<tr><td width='100%' style=\"text-align:center;\" align=\"center\">For, " + firm.getFirmName() + '</td></tr>' + '<tr><td style="height: ' + 84 * textSizeRatio + 'px; text-align:center;" align="center">';

			if (imageBlob) {
				signatureHTML += "<img src='data:image/png;base64," + imageBlob + "' style='height:" + 84 * textSizeRatio + 'px; width:' + 168 * textSizeRatio + "px;'></img>";
			}

			signatureHTML += '</td></tr><tr><td align="center" style="text-align:center;" >' + settingCache.getSignatureText() + '</td></tr></table>';
			if (this.isForInvoicePreview) {
				var _showEditView2 = this.getEditSectionClassName('signatureLogo');
				var _classText2 = ' class=\'invoicePreviewEditSection ' + _showEditView2 + '\' ';
				signatureHTML = '<div style="width:100%;" editsection="signatureLogo" ' + _classText2 + '>' + signatureHTML + '</div>';
			}
		}

		var htmlText = '';
		if (termsAndConditionHTML || bankDetailsHTML || signatureHTML) {
			htmlText = "<table width='100%'><tr><td width='50%' class='borderBottomForTxn borderLeftForTxn borderColorGrey vAlignTop noPadding'>" + termsAndConditionHTML + '</td>' + "<td width='50%' class='borderBottomForTxn borderRightForTxn borderColorGrey vAlignBottom noPadding'><table width='100%'>";
			if (bankDetailsHTML) {
				htmlText += "<tr><td width='100%'>" + bankDetailsHTML + '</td></tr>';
			}
			if (signatureHTML) {
				htmlText += "<tr><td width='100%' class='borderTopForTxn borderLeftForTxn borderColorGrey noPadding'>" + signatureHTML + '</td></tr>';
			}

			htmlText += '</table></td></tr></table>';
		}
		return htmlText;
	};

	var HSNTaxCodeDistribution = function () {
		function HSNTaxCodeDistribution(hsnCode, taxId) {
			(0, _classCallCheck3.default)(this, HSNTaxCodeDistribution);

			this._taxableValue = 0;
			this._additionalCESS = 0;
			this._taxTotal = 0;
			this._igstTaxRate = 0;
			this._cgstTaxRate = 0;
			this._sgstTaxRate = 0;
			this._cessTaxRate = 0;
			this._stateSpecificCESSTaxRate = 0;
			this._otherTaxRate = 0;
			this._igstTaxRatePresent = false;
			this._cgstTaxRatePresent = false;
			this._sgstTaxRatePresent = false;
			this._cessTaxRatePresent = false;
			this._stateSpecificCESSTaxRatePresent = false;
			this._otherTaxRatePresent = false;
			this._exemptedTaxRatePresent = false;
			this._taxId = taxId;
			this._hsnCode = hsnCode == null ? '' : hsnCode;
			if (taxId > 0) {
				var taxCode = taxCodeCache.getTaxCodeObjectById(taxId);
				if (taxCode) {
					if (taxCode.getTaxType() == TaxCodeConstants.taxGroup) {
						var taxCodeIds = (0, _values2.default)(taxCode.getTaxCodeMap());
						if (taxCodeIds.length > 0) {
							for (i = 0; i < taxCodeIds.length; i++) {
								var taxRate = taxCodeCache.getTaxCodeObjectById(taxCodeIds[i]);
								if (taxRate.getTaxRateType() == TaxCodeConstants.IGST) {
									this._igstTaxRate += taxRate.getTaxRate();
									this._igstTaxRatePresent = true;
								} else if (taxRate.getTaxRateType() == TaxCodeConstants.CGST) {
									this._cgstTaxRate += taxRate.getTaxRate();
									this._cgstTaxRatePresent = true;
								} else if (taxRate.getTaxRateType() == TaxCodeConstants.SGST) {
									this._sgstTaxRate += taxRate.getTaxRate();
									this._sgstTaxRatePresent = true;
								} else if (taxRate.getTaxRateType() == TaxCodeConstants.CESS) {
									this._cessTaxRate += taxRate.getTaxRate();
									this._cessTaxRatePresent = true;
								} else if (taxRate.getTaxRateType() == TaxCodeConstants.STATE_SPECIFIC_CESS) {
									this._stateSpecificCESSTaxRate += taxRate.getTaxRate();
									this._stateSpecificCESSTaxRatePresent = true;
								} else if (taxRate.getTaxRateType() == TaxCodeConstants.OTHER) {
									this._otherTaxRate += taxRate.getTaxRate();
									this._otherTaxRatePresent = true;
								} else if (taxRate.getTaxRateType() == TaxCodeConstants.Exempted) {
									this._exemptedTaxRatePresent = true;
								}
							}
						}
					} else {
						if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
							this._igstTaxRate += taxCode.getTaxRate();
							this._igstTaxRatePresent = true;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
							this._cgstTaxRate += taxCode.getTaxRate();
							this._cgstTaxRatePresent = true;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
							this._sgstTaxRate += taxCode.getTaxRate();
							this._sgstTaxRatePresent = true;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
							this._cessTaxRate += taxCode.getTaxRate();
							this._cessTaxRatePresent = true;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.STATE_SPECIFIC_CESS) {
							this._stateSpecificCESSTaxRate += taxCode.getTaxRate();
							this._stateSpecificCESSTaxRatePresent = true;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.OTHER) {
							this._otherTaxRate += taxCode.getTaxRate();
							this._otherTaxRatePresent = true;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.Exempted) {
							this._exemptedTaxRatePresent = true;
						}
					}
				}
			}
		}

		(0, _createClass3.default)(HSNTaxCodeDistribution, [{
			key: 'hsnCode',
			get: function get() {
				return this._hsnCode;
			}
		}, {
			key: 'taxId',
			get: function get() {
				return this._taxId;
			}
		}, {
			key: 'taxableValue',
			get: function get() {
				return this._taxableValue;
			},
			set: function set(value) {
				this._taxableValue = value;
			}
		}, {
			key: 'additionalCESS',
			get: function get() {
				return this._additionalCESS;
			},
			set: function set(value) {
				this._additionalCESS = value;
			}
		}, {
			key: 'taxTotal',
			get: function get() {
				return this._taxTotal;
			},
			set: function set(value) {
				this._taxTotal = value;
			}
		}, {
			key: 'igstTaxRate',
			get: function get() {
				return this._igstTaxRate;
			}
		}, {
			key: 'cgstTaxRate',
			get: function get() {
				return this._cgstTaxRate;
			}
		}, {
			key: 'sgstTaxRate',
			get: function get() {
				return this._sgstTaxRate;
			}
		}, {
			key: 'cessTaxRate',
			get: function get() {
				return this._cessTaxRate;
			}
		}, {
			key: 'stateSpecificCESSTaxRate',
			get: function get() {
				return this._stateSpecificCESSTaxRate;
			}
		}, {
			key: 'otherTaxRate',
			get: function get() {
				return this._otherTaxRate;
			}
		}, {
			key: 'igstTaxRatePresent',
			get: function get() {
				return this._igstTaxRatePresent;
			}
		}, {
			key: 'cgstTaxRatePresent',
			get: function get() {
				return this._cgstTaxRatePresent;
			}
		}, {
			key: 'sgstTaxRatePresent',
			get: function get() {
				return this._sgstTaxRatePresent;
			}
		}, {
			key: 'cessTaxRatePresent',
			get: function get() {
				return this._cessTaxRatePresent;
			}
		}, {
			key: 'stateSpecificCESSTaxRatePresent',
			get: function get() {
				return this._stateSpecificCESSTaxRatePresent;
			}
		}, {
			key: 'otherTaxRatePresent',
			get: function get() {
				return this._otherTaxRatePresent;
			}
		}, {
			key: 'exemptedTaxRatePresent',
			get: function get() {
				return this._exemptedTaxRatePresent;
			}
		}]);
		return HSNTaxCodeDistribution;
	}();

	this.getTaxCodeDistributionTableForTheme9 = function (txn, themeColor, textSizeRatio) {
		var taxDetailsHTML = '';
		var transactionType = txn.getTxnType();
		if (settingCache.isPrintTaxDetailEnabled() && settingCache.getGSTEnabled() && settingCache.getHSNSACEnabled() && (transactionType == TxnTypeConstant.TXN_TYPE_SALE || transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE || transactionType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || transactionType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER || transactionType == TxnTypeConstant.TXN_TYPE_ESTIMATE || transactionType == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN)) {
			var taxDetailMap = new _map2.default();
			var lineItems = txn.getLineItems();
			if (lineItems.length > 0) {
				var itemCache = new ItemCache();
				$.each(lineItems, function (key, lineItem) {
					var itemId = lineItem.getItemId();
					var item = itemCache.getItemById(lineItem.getItemId());
					if (item) {
						var _key = void 0,
						    hsnTaxCodeDistribution = void 0,
						    taxableValue = void 0;
						var lineItemTaxId = MyDouble.convertStringToDouble(lineItem.getLineItemTaxId(), true);
						var additionalCESS = MyDouble.convertStringToDouble(lineItem.getLineItemAdditionalCESS(), true);
						if (lineItemTaxId || additionalCESS) {
							_key = item.getItemHSNCode() + '_' + lineItemTaxId;
							hsnTaxCodeDistribution = taxDetailMap.get(_key);
							if (!hsnTaxCodeDistribution) {
								hsnTaxCodeDistribution = new HSNTaxCodeDistribution(item.getItemHSNCode(), lineItemTaxId);
							}
							hsnTaxCodeDistribution.additionalCESS = hsnTaxCodeDistribution.additionalCESS + additionalCESS;
							taxableValue = Number(lineItem.getLineItemTotal()) - additionalCESS - Number(lineItem.getLineItemTaxAmount());
							hsnTaxCodeDistribution.taxableValue = hsnTaxCodeDistribution.taxableValue + taxableValue;
							hsnTaxCodeDistribution.taxTotal = hsnTaxCodeDistribution.taxTotal + MyDouble.convertStringToDouble(lineItem.getLineItemTaxAmount(), true) + additionalCESS;
							taxDetailMap.set(_key, hsnTaxCodeDistribution);
						}
						var txnTaxId = MyDouble.convertStringToDouble(txn.getTransactionTaxId(), true);
						if (txnTaxId) {
							_key = item.getItemHSNCode() + '_' + txnTaxId;
							hsnTaxCodeDistribution = taxDetailMap.get(_key);
							if (!hsnTaxCodeDistribution) {
								hsnTaxCodeDistribution = new HSNTaxCodeDistribution(item.getItemHSNCode(), txnTaxId);
							}
							taxableValue = lineItem.getLineItemTotal() - lineItem.getLineItemTotal() * txn.getDiscountPercent() / 100;
							var txnTaxAmountForLineItem = taxableValue * txn.getTaxPercent() / 100;
							hsnTaxCodeDistribution.taxableValue = hsnTaxCodeDistribution.taxableValue + taxableValue;
							hsnTaxCodeDistribution.taxTotal = hsnTaxCodeDistribution.taxTotal + txnTaxAmountForLineItem;
							taxDetailMap.set(_key, hsnTaxCodeDistribution);
						}
					}
				});
			}

			if (taxDetailMap.size > 0) {
				var igstPresent = false;
				var cgstPresent = false;
				var sgstPresent = false;
				var cessPresent = false;
				var stateSpecificCESSPresent = false;
				var otherTaxPresent = false;
				var exemptedRatePresent = false;
				var additionalCESSPeresent = false;

				var totalTaxableValue = 0;
				var totalIGSTTax = 0;
				var totalCGSTTax = 0;
				var totalSGSTTax = 0;
				var totalCESSTax = 0;
				var totalStateSpecificCESS = 0;
				var totalOtherTax = 0;
				var totalAdditionalCESS = 0;
				var totalTax = 0;

				var firm = firmCache.getTransactionFirmWithDefault(txn);
				taxDetailMap.forEach(function (hsnTaxCodeDistribution, key) {
					if (hsnTaxCodeDistribution.igstTaxRatePresent) {
						igstPresent = true;
					}
					if (hsnTaxCodeDistribution.cgstTaxRatePresent) {
						cgstPresent = true;
					}
					if (hsnTaxCodeDistribution.sgstTaxRatePresent) {
						sgstPresent = true;
					}
					if (hsnTaxCodeDistribution.cessTaxRatePresent) {
						cessPresent = true;
					}
					if (hsnTaxCodeDistribution.stateSpecificCESSTaxRatePresent) {
						stateSpecificCESSPresent = true;
					}
					if (hsnTaxCodeDistribution.otherTaxRatePresent) {
						otherTaxPresent = true;
					}
					if (hsnTaxCodeDistribution.exemptedTaxRatePresent) {
						exemptedRatePresent = true;
					}
					if (hsnTaxCodeDistribution.additionalCESS > 0) {
						additionalCESSPeresent = true;
					}
				});

				var hsnTaxCodeDistributionList = [];

				var _iteratorNormalCompletion = true;
				var _didIteratorError = false;
				var _iteratorError = undefined;

				try {
					for (var _iterator = (0, _getIterator3.default)(taxDetailMap.values()), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
						var value = _step.value;

						hsnTaxCodeDistributionList.push(value);
					}
				} catch (err) {
					_didIteratorError = true;
					_iteratorError = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion && _iterator.return) {
							_iterator.return();
						}
					} finally {
						if (_didIteratorError) {
							throw _iteratorError;
						}
					}
				}

				hsnTaxCodeDistributionList.sort(function (lhs, rhs) {
					return lhs.hsnCode.toUpperCase().localeCompare(rhs.hsnCode.toUpperCase());
				});

				var hsnColumnWidth = 2;
				var taxableValueColumnWidth = 1.4;
				var igstColumnWidth = igstPresent ? 1.6 : 0;
				var cgstColumnWidth = cgstPresent ? 1.6 : 0;
				var sgstColumnWidth = sgstPresent ? 1.6 : 0;
				var cessColumnWidth = cessPresent ? 1.6 : 0;
				var stateSpecificCESSColumnWidth = stateSpecificCESSPresent ? 1.6 : 0;
				var otherTaxColumnWidth = otherTaxPresent ? 1.6 : 0;
				var exemptedColumnWidth = exemptedRatePresent ? 1.6 : 0;
				var additionalCESSColumnWidth = additionalCESSPeresent ? 1.2 : 0;
				var taxTotalValueColumnWidth = 1.6;

				var totalWidth = hsnColumnWidth + taxableValueColumnWidth + igstColumnWidth + cgstColumnWidth + sgstColumnWidth + cessColumnWidth + otherTaxColumnWidth + exemptedColumnWidth + additionalCESSColumnWidth + taxTotalValueColumnWidth + stateSpecificCESSColumnWidth;

				var col1BorderClass = " class='borderBottomForTxn borderRightForTxn borderLeftForTxn borderColorGrey' ";
				var col2BorderClass = " class='borderBottomForTxn borderRightForTxn borderColorGrey' ";

				taxDetailsHTML = "<table width='100%'><tr><th rowspan='2' width='" + hsnColumnWidth * 100 / totalWidth + "%' " + col1BorderClass + " align='center'>" + TransactionPrintSettings.getHsnCodeColumnHeader() + "</th><th rowspan='2' width='" + taxableValueColumnWidth * 100 / totalWidth + "%' " + col2BorderClass + " align='center'>" + TransactionPrintSettings.getTaxableAmountColumnHeader() + '</th>';
				if (igstPresent) {
					taxDetailsHTML += "<th colspan='2' width='" + igstColumnWidth * 100 / totalWidth + "%' " + col2BorderClass + " align='center'>" + TransactionPrintSettings.getIgstColumnHeader() + '</th>';
				}
				if (cgstPresent) {
					taxDetailsHTML += "<th colspan='2' width='" + cgstColumnWidth * 100 / totalWidth + "%' " + col2BorderClass + " align='center'>" + TransactionPrintSettings.getCgstColumnHeader() + '</th>';
				}
				if (sgstPresent) {
					taxDetailsHTML += "<th colspan='2' width='" + sgstColumnWidth * 100 / totalWidth + "%' " + col2BorderClass + " align='center'>" + TransactionPrintSettings.getSgstColumnHeader(firm) + '</th>';
				}
				if (cessPresent) {
					taxDetailsHTML += "<th colspan='2' width='" + cessColumnWidth * 100 / totalWidth + "%' " + col2BorderClass + " align='center'>" + TransactionPrintSettings.getCessColumnHeader() + '</th>';
				}
				if (stateSpecificCESSPresent) {
					taxDetailsHTML += "<th colspan='2' width='" + stateSpecificCESSColumnWidth * 100 / totalWidth + "%' " + col2BorderClass + " align='center'>" + TransactionPrintSettings.getStateSpecificCessColumnHeader() + '</th>';
				}
				if (additionalCESSPeresent) {
					taxDetailsHTML += "<th rowspan='2' width='" + additionalCESSColumnWidth * 100 / totalWidth + "%' " + col2BorderClass + " align='center'>" + TransactionPrintSettings.getAdditionCESSColumnHeader() + '</th>';
				}
				if (exemptedRatePresent) {
					taxDetailsHTML += "<th rowspan='2' width='" + exemptedColumnWidth * 100 / totalWidth + "%' " + col2BorderClass + " align='center'>Exempted Tax</th>";
				}
				if (otherTaxPresent) {
					taxDetailsHTML += "<th colspan='2' width='" + otherTaxColumnWidth * 100 / totalWidth + "%' " + col2BorderClass + " align='center'>" + TransactionPrintSettings.getOtherTaxColumnHeader() + '</th>';
				}
				taxDetailsHTML += "<th rowspan='2' width='" + taxTotalValueColumnWidth * 100 / totalWidth + "%' " + col2BorderClass + " align='center'>Total Tax Amount</th></tr>";

				taxDetailsHTML += '<tr>';
				if (igstPresent) {
					taxDetailsHTML += "<th width='" + igstColumnWidth * 50 / totalWidth + "%' " + col2BorderClass + " align='center'> Rate</th><th width='" + igstColumnWidth * 50 / totalWidth + "%' " + col2BorderClass + " align='center'>Amount</th>";
				}
				if (cgstPresent) {
					taxDetailsHTML += "<th width='" + igstColumnWidth * 50 / totalWidth + "%' " + col2BorderClass + " align='center'> Rate</th><th width='" + igstColumnWidth * 50 / totalWidth + "%' " + col2BorderClass + " align='center'>Amount</th>";
				}
				if (sgstPresent) {
					taxDetailsHTML += "<th width='" + igstColumnWidth * 50 / totalWidth + "%' " + col2BorderClass + " align='center'> Rate</th><th width='" + igstColumnWidth * 50 / totalWidth + "%' " + col2BorderClass + " align='center'>Amount</th>";
				}
				if (cessPresent) {
					taxDetailsHTML += "<th width='" + igstColumnWidth * 50 / totalWidth + "%' " + col2BorderClass + " align='center'> Rate</th><th width='" + igstColumnWidth * 50 / totalWidth + "%' " + col2BorderClass + " align='center'>Amount</th>";
				}
				if (stateSpecificCESSPresent) {
					taxDetailsHTML += "<th width='" + stateSpecificCESSColumnWidth * 50 / totalWidth + "%' " + col2BorderClass + " align='center'> Rate</th><th width='" + stateSpecificCESSColumnWidth * 50 / totalWidth + "%' " + col2BorderClass + " align='center'>Amount</th>";
				}
				if (otherTaxPresent) {
					taxDetailsHTML += "<th width='" + taxTotalValueColumnWidth * 50 / totalWidth + "%' " + col2BorderClass + " align='center'> Rate</th><th width='" + igstColumnWidth * 50 / totalWidth + "%' " + col2BorderClass + " align='center'>Amount</th>";
				}

				taxDetailsHTML += '</tr>';

				$.each(hsnTaxCodeDistributionList, function (key, hsnTaxCodeDistribution) {
					totalTaxableValue += hsnTaxCodeDistribution.taxableValue;

					taxDetailsHTML += '<tr><td ' + col1BorderClass + '>' + hsnTaxCodeDistribution.hsnCode + '</td><td ' + col2BorderClass + " align='right'>" + MyDouble.getAmountForInvoicePrint(hsnTaxCodeDistribution.taxableValue) + '</td>';
					if (igstPresent) {
						if (hsnTaxCodeDistribution.igstTaxRatePresent) {
							var igstTaxAmount = hsnTaxCodeDistribution.igstTaxRate * hsnTaxCodeDistribution.taxableValue / 100;
							totalIGSTTax += igstTaxAmount;
							taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'>" + MyDouble.getPercentageWithDecimal(hsnTaxCodeDistribution.igstTaxRate) + '%</td>' + '<td  ' + col2BorderClass + " align='right'>" + MyDouble.getAmountForInvoicePrint(igstTaxAmount) + '</td>';
						} else {
							taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'></td><td  " + col2BorderClass + " align='right'></td>";
						}
					}
					if (cgstPresent) {
						if (hsnTaxCodeDistribution.cgstTaxRatePresent) {
							var cgstTaxAmount = hsnTaxCodeDistribution.cgstTaxRate * hsnTaxCodeDistribution.taxableValue / 100;
							totalCGSTTax += cgstTaxAmount;
							taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'>" + MyDouble.getPercentageWithDecimal(hsnTaxCodeDistribution.cgstTaxRate) + '%</td>' + '<td  ' + col2BorderClass + " align='right'>" + MyDouble.getAmountForInvoicePrint(cgstTaxAmount) + '</td>';
						} else {
							taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'></td><td  " + col2BorderClass + " align='right'></td>";
						}
					}
					if (sgstPresent) {
						if (hsnTaxCodeDistribution.sgstTaxRatePresent) {
							var sgstTaxAmount = hsnTaxCodeDistribution.sgstTaxRate * hsnTaxCodeDistribution.taxableValue / 100;
							totalSGSTTax += sgstTaxAmount;
							taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'>" + MyDouble.getPercentageWithDecimal(hsnTaxCodeDistribution.sgstTaxRate) + '%</td>' + '<td  ' + col2BorderClass + " align='right'>" + MyDouble.getAmountForInvoicePrint(sgstTaxAmount) + '</td>';
						} else {
							taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'></td><td  " + col2BorderClass + " align='right'></td>";
						}
					}
					if (cessPresent) {
						if (hsnTaxCodeDistribution.cessTaxRatePresent) {
							var cessTax = hsnTaxCodeDistribution.cessTaxRate * hsnTaxCodeDistribution.taxableValue / 100;
							totalCESSTax += cessTax;
							taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'>" + MyDouble.getPercentageWithDecimal(hsnTaxCodeDistribution.cessTaxRate) + '%</td>' + '<td  ' + col2BorderClass + " align='right'>" + MyDouble.getAmountForInvoicePrint(cessTax) + '</td>';
						} else {
							taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'></td><td  " + col2BorderClass + " align='right'></td>";
						}
					}
					if (stateSpecificCESSPresent) {
						if (hsnTaxCodeDistribution.stateSpecificCESSTaxRatePresent) {
							var stateSpecificTax = hsnTaxCodeDistribution.stateSpecificCESSTaxRate * hsnTaxCodeDistribution.taxableValue / 100;
							totalStateSpecificCESS += stateSpecificTax;
							taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'>" + MyDouble.getPercentageWithDecimal(hsnTaxCodeDistribution.stateSpecificCESSTaxRate) + '%</td>' + '<td  ' + col2BorderClass + " align='right'>" + MyDouble.getAmountForInvoicePrint(stateSpecificTax) + '</td>';
						} else {
							taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'></td><td  " + col2BorderClass + " align='right'></td>";
						}
					}
					if (additionalCESSPeresent) {
						if (hsnTaxCodeDistribution.additionalCESS > 0) {
							totalAdditionalCESS += hsnTaxCodeDistribution.additionalCESS;
							taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'>" + MyDouble.getAmountForInvoicePrint(hsnTaxCodeDistribution.additionalCESS) + '</td>';
						} else {
							taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'></td>";
						}
					}
					if (exemptedRatePresent) {
						if (hsnTaxCodeDistribution.exemptedTaxRatePresent) {
							taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'>" + MyDouble.getAmountForInvoicePrint(0) + '</td>';
						} else {
							taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'></td>";
						}
					}
					if (otherTaxPresent) {
						if (hsnTaxCodeDistribution.otherTaxRatePresent) {
							var otherTax = hsnTaxCodeDistribution.otherTaxRate * hsnTaxCodeDistribution.taxableValue / 100;
							totalOtherTax += otherTax;
							taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'>" + MyDouble.getPercentageWithDecimal(hsnTaxCodeDistribution.otherTaxRate) + '%</td>' + '<td  ' + col2BorderClass + " align='right'>" + MyDouble.getAmountForInvoicePrint(otherTax) + '</td>';
						} else {
							taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'></td><td  " + col2BorderClass + " align='right'></td>";
						}
					}
					totalTax += hsnTaxCodeDistribution.taxTotal;
					taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'>" + MyDouble.getAmountForInvoicePrint(hsnTaxCodeDistribution.taxTotal) + '</td></tr>';
				});

				taxDetailsHTML += "<tr class='boldText'><td " + col1BorderClass + " align='right'>Total</td><td " + col2BorderClass + " align='right'>" + MyDouble.getAmountForInvoicePrint(totalTaxableValue) + '</td>';
				if (igstPresent) {
					taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'></td><td  " + col2BorderClass + " align='right'>" + MyDouble.getAmountForInvoicePrint(totalIGSTTax) + '</td>';
				}
				if (cgstPresent) {
					taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'></td><td  " + col2BorderClass + " align='right'>" + MyDouble.getAmountForInvoicePrint(totalCGSTTax) + '</td>';
				}
				if (sgstPresent) {
					taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'></td><td  " + col2BorderClass + " align='right'>" + MyDouble.getAmountForInvoicePrint(totalSGSTTax) + '</td>';
				}
				if (cessPresent) {
					taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'></td><td  " + col2BorderClass + " align='right'>" + MyDouble.getAmountForInvoicePrint(totalCESSTax) + '</td>';
				}
				if (stateSpecificCESSPresent) {
					taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'></td><td  " + col2BorderClass + " align='right'>" + MyDouble.getAmountForInvoicePrint(totalStateSpecificCESS) + '</td>';
				}
				if (additionalCESSPeresent) {
					taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'>" + MyDouble.getAmountForInvoicePrint(totalAdditionalCESS) + '</td>';
				}
				if (exemptedRatePresent) {
					taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'>" + MyDouble.getAmountForInvoicePrint(0) + '</td>';
				}
				if (otherTaxPresent) {
					taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'></td><td  " + col2BorderClass + " align='right'>" + MyDouble.getAmountForInvoicePrint(totalOtherTax) + '</td>';
				}
				taxDetailsHTML += '<td  ' + col2BorderClass + " align='right'>" + MyDouble.getAmountForInvoicePrint(totalTax) + '</td></tr></table>';
			}
		}

		return taxDetailsHTML;
	};

	this.getAmountAndDescriptionHTMLForTheme9 = function (txn, themeColor, textSizeRatio) {
		var amountInWordsTable = this.getAmountInWordsAndDescriptionTableForTheme9(txn, textSizeRatio);
		var amountTable = getAmountHTMLTable(txn, InvoiceTheme.THEME_9, themeColor, textSizeRatio);

		var htmlText = "<table width='100%'><tr><td width='50%' valign='top' style='padding: 0px' class='borderBottomForTxn borderTopForTxn  borderRightForTxn borderLeftForTxn borderColorGrey'>" + amountInWordsTable + '</td>' + "<td width='50%' valign='top' style=' padding: 0px;' class='borderBottomForTxn borderTopForTxn  borderRightForTxn borderColorGrey'>" + amountTable + '</td></tr></table>';

		return htmlText;
	};

	this.getAmountInWordsAndDescriptionTableForTheme9 = function (txn, textSizeRatio) {
		var txnType = txn.getTxnType();
		var amountInWordsTable = "<table width='100%'><tr><td width='100%'>";
		switch (txnType) {
			case TxnTypeConstant.TXN_TYPE_SALE:
				amountInWordsTable += 'Invoice ';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE:
				amountInWordsTable += 'Bill ';
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
				amountInWordsTable += 'Order ';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
				amountInWordsTable += 'Order ';
				break;
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
				amountInWordsTable += 'Delivery Challan ';
				break;
			case TxnTypeConstant.TXN_TYPE_ESTIMATE:
				amountInWordsTable += 'Estimate ';
				break;
		}
		amountInWordsTable += 'Amount In Words</td></tr>';
		amountInWordsTable += "<tr><td width='100%' class='boldText'>";

		var balanceAmount = txn.getBalanceAmount() ? Number(txn.getBalanceAmount()) : 0;
		var cashAmount = txn.getCashAmount() ? Number(txn.getCashAmount()) : 0;
		var discountAmount = txn.getDiscountAmount() ? Number(txn.getDiscountAmount()) : 0;
		var reverseChargeAmount = txn.getReverseChargeAmount() ? Number(txn.getReverseChargeAmount()) : 0;
		var totalAmount = balanceAmount + cashAmount + reverseChargeAmount;

		if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
			totalAmount = totalAmount + discountAmount;
		} else {
			balanceAmount = txn.getTxnCurrentBalanceAmount() ? Number(txn.getTxnCurrentBalanceAmount()) : 0; // setting current balance amount
			cashAmount = totalAmount - balanceAmount - reverseChargeAmount; // setting total received amount
		}
		var amountInWordsString = CurrencyHelper.getAmountInWords(totalAmount);
		amountInWordsTable += amountInWordsString + '</td></tr>';

		var description = txn.getDescription();

		if (settingCache.isPrintDescriptionEnabled() && description) {
			description = description.replace(/(?:\r\n|\r|\n)/g, '<br/>');
			amountInWordsTable += "<tr><td width='100%' class='borderTopForTxn borderColorGrey' >Description</td></tr><tr><td width='100%' class='boldText'>" + description + '</td></tr>';
		}
		if (settingCache.printPaymentMode() && txn.getTxnType() != TxnTypeConstant.TXN_TYPE_ESTIMATE && txn.getTxnType() != TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN) {
			try {
				var paymentModeString = 'Credit';
				if (cashAmount > 0) {
					var PaymentCache = require('./../Cache/PaymentInfoCache.js');
					var paymentCache = new PaymentCache();
					var paymentTypeId = txn.getPaymentTypeId() + '';
					var paymentInfoObj = paymentCache.getPaymentInfoObjById(paymentTypeId);
					var paymentName = paymentInfoObj.getName();
					var paymentRef = txn.getPaymentTypeReference();
					paymentModeString = paymentName;
					if (paymentRef) {
						paymentModeString += ' (' + paymentRef + ')';
					}
				}

				amountInWordsTable += "<tr><td width='100%' class='borderTopForTxn borderColorGrey'>Payment Mode</td></tr><tr><td width='100%' class='boldText'> " + paymentModeString + '</td></tr>';
			} catch (err) {}
		}
		amountInWordsTable += '</table>';

		return amountInWordsTable;
	};

	this.getCompanyPartyAndTxnHeaderForTheme9 = function (txn, themeColor, textSizeRatio) {
		var column2Class = "class='vAlignTop noPadding borderTopForTxn borderBottomForTxn borderRightForTxn borderColorGrey'";
		var column1Class = "class='vAlignTop noPadding borderTopForTxn borderBottomForTxn borderLeftForTxn borderRightForTxn borderColorGrey'";
		var htmlData = "<table width='100%'><tr><td width='50%' " + column1Class + '>' + this.getCompanyHeaderForTheme9(txn.getFirmId(), textSizeRatio) + this.getPartyInfoTable(txn, InvoiceTheme.THEME_9, themeColor, textSizeRatio) + "</td><td width='50%' " + column2Class + '>' + this.getTxnDataHeaderForTheme9(txn) + '</td></tr></table>';
		return htmlData;
	};

	this.getCompanyHeaderForTheme9 = function (firmId, textSizeRatio) {
		if (!settingCache) {
			settingCache = new SettingCache();
		}

		var firm = Number(firmId) ? firmCache.getFirmById(firmId) : firmCache.getDefaultFirm();

		var companyAddress = firm.getFirmAddress().trim();
		companyAddress = companyAddress.replace(/(?:\r\n|\r|\n)/g, '<br/>');

		var companyDetailsHTML = '';
		var imageDetailsHTML = '';

		var imagePath = this.getCompanyLogoSource(firm, this.isForInvoicePreview);

		if (firm.getFirmName() && settingCache.isPrintCompanyNameEnabled()) {
			companyDetailsHTML += "<p class='companyNameHeaderTextSize boldText'>" + firm.getFirmName() + '</p>';
		}

		if (companyAddress && settingCache.isPrintCompanyAddressEnabled()) {
			companyDetailsHTML += "<p class='bigTextSize'>" + companyAddress + '</p>';
		}

		var companyPhoneToBePrinted = firm.getFirmPhone() && settingCache.isPrintCompanyNumberEnabled();
		var companyEmailToBePrinted = firm.getFirmEmail() && settingCache.isCompnayEmailEnabledOnPrint();

		if (companyPhoneToBePrinted) {
			companyDetailsHTML += "<p class='bigTextSize'>Phone no.: " + firm.getFirmPhone() + '</p>';
		}
		if (companyEmailToBePrinted) {
			companyDetailsHTML += "<p class='bigTextSize'>Email: " + firm.getFirmEmail() + '</p>';
		}

		if (settingCache.isPrintTINEnabled()) {
			if (settingCache.getGSTEnabled()) {
				var companyGSTINToBePrinted = firm.getFirmHSNSAC();
				var companyStateToBePrinted = firm.getFirmState();

				if (companyGSTINToBePrinted) {
					companyDetailsHTML += "<p class='bigTextSize'>GSTIN: " + firm.getFirmHSNSAC() + '</p>';
				}
				if (companyStateToBePrinted) {
					companyDetailsHTML += "<p class='bigTextSize'>State: " + StateCode.getStateCode(firm.getFirmState()) + '-' + firm.getFirmState() + '</p>';
				}
			} else {
				if (firm.getFirmTin()) {
					companyDetailsHTML += "<p class='bigTextSize'>" + settingCache.getTINText() + ': ' + firm.getFirmTin() + '</p>';
				}
			}
		}

		companyDetailsHTML += this.getUdfHeaderDetails(firm, 'center', InvoiceTheme.THEME_9);

		if (imagePath) {
			imageDetailsHTML = "<img src='" + imagePath + "' style='height: " + 105 * textSizeRatio + "px;'></img>";
		}

		if (this.isForInvoicePreview) {
			var classTextForEditBusinessDetails = 'class=\'invoicePreviewEditSection ' + this.getEditSectionClassName('firmDetails') + '\'';
			var classTextForEditLogo = 'class=\'invoicePreviewEditSection ' + this.getEditSectionClassName('firmLogo') + '\'';
			if (companyDetailsHTML) {
				companyDetailsHTML = '<div editSection=\'firmDetails\' ' + classTextForEditBusinessDetails + ' > ' + companyDetailsHTML + ' </div>';
			}
			if (imageDetailsHTML) {
				imageDetailsHTML = '<div editSection=\'firmLogo\' ' + classTextForEditLogo + ' > ' + imageDetailsHTML + ' </div>';
			}
		}

		var headerDetails = "<table width='100%' style='vertical-align: center;' class='  borderBottomForTxn borderColorGrey'> <tr>";

		if (imagePath) {
			// This % of 2% and 98% is correct. Please don't change it. Or contact Shubham before making any change.
			headerDetails += "<td width='2%' style='vertical-align: center'>" + imageDetailsHTML + "</td><td width='98%' style='vertical-align: center'>" + companyDetailsHTML;
		} else {
			headerDetails += "<td width='100%' style='vertical-align: center'>" + companyDetailsHTML;
		}

		headerDetails += '</td></tr></table>';

		return headerDetails;
	};

	this.getTxnDataHeaderForTheme9 = function (txn) {
		var txnHeaderData = [];

		var party = txn.getNameRef();
		var shipAddressHeader = '';

		var refHeaderText = '';

		switch (txn.getTxnType()) {
			case TxnTypeConstant.TXN_TYPE_SALE:
				refHeaderText = 'Invoice No.';
				shipAddressHeader = 'Ship To';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE:
				refHeaderText = 'Bill No.';
				shipAddressHeader = 'Ship From';
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
				refHeaderText = 'Return No.';
				shipAddressHeader = 'Ship From';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
				refHeaderText = 'Return No.';
				shipAddressHeader = 'Ship To';
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
				refHeaderText = 'Order No.';
				shipAddressHeader = 'Ship To';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
				refHeaderText = 'Order No.';
				shipAddressHeader = 'Ship From';
				break;
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
				refHeaderText = 'Challan No.';
				shipAddressHeader = 'Ship To';
				break;
			case TxnTypeConstant.TXN_TYPE_ESTIMATE:
				refHeaderText = 'Estimate No.';
				break;
			case TxnTypeConstant.TXN_TYPE_CASHIN:
			case TxnTypeConstant.TXN_TYPE_CASHOUT:
				refHeaderText = 'Receipt No.';
				break;
			default:
				refHeaderText = 'No.';
		}

		if (txn.getTxnRefNumber()) {
			txnHeaderData.push([refHeaderText, txn.getFullTxnRefNumber()]);
		}

		txnHeaderData.push(['Date', MyDate.getDate('d-m-y', txn.getTxnDate())]);

		var dueDateVal = this.getDueDateForTheme9(txn);
		if (dueDateVal) {
			txnHeaderData.push(dueDateVal);
		}

		if (txn.getEwayBillNumber()) {
			txnHeaderData.push(['E-way Bill number', txn.getEwayBillNumber()]);
		}

		if (settingCache.getGSTEnabled() && settingCache.isPlaceOfSupplyEnabled()) {
			if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
				var placeOfSupply = txn.getPlaceOfSupply();
				if (!placeOfSupply || !placeOfSupply.trim()) {
					if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
						var firm = firmCache.getTransactionFirmWithDefault(txn);
						placeOfSupply = firm.getFirmState();
					} else {
						placeOfSupply = party.getContactState();
					}
				}
				if (placeOfSupply && placeOfSupply.trim()) {
					txnHeaderData.push(['Place of supply', StateCode.getStateCode(placeOfSupply) + '-' + placeOfSupply]);
				}
			}
		}

		if (settingCache.getReverseChargeEnabled() && (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN)) {
			txnHeaderData.push(['Reverse charge', Number(txn.getReverseCharge()) ? 'Yes' : 'No']);
		}

		if (txn.getPODate()) {
			txnHeaderData.push(['PO date', MyDate.getDate('d-m-y', txn.getPODate())]);
		}
		if (txn.getPONumber()) {
			txnHeaderData.push(['PO number', txn.getPONumber()]);
		}
		var udfCache = new UDFCache();
		if (txn.getUdfObjectArray()) {
			for (var i = 0; i < txn.getUdfObjectArray().length; i++) {
				var value = txn.getUdfObjectArray()[i];
				var fieldModel = udfCache.getUdfModelByFirmAndFieldId(txn.getFirmId(), value.getUdfFieldId());
				if (fieldModel.getUdfFieldPrintOnInvoice() == 1 && value.getUdfFieldValue() && fieldModel.getUdfTxnType() == txn.getTxnType() && value.getUdfRefId() == txn.getTxnId()) {
					var displayValue = value.getDisplayValue(fieldModel);
					txnHeaderData.push([fieldModel.getUdfFieldName(), displayValue]);
				}
			}
		}

		if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
			if (txn.getTxnReturnRefNumber()) {
				if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
					txnHeaderData.push(['Invoice No.', txn.getTxnReturnRefNumber()]);
				} else {
					txnHeaderData.push(['Bill No.', txn.getTxnReturnRefNumber()]);
				}
			}
			if (txn.getTxnReturnDate()) {
				if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
					txnHeaderData.push(['Invoice date', MyDate.getDate('d-m-y', txn.getTxnReturnDate())]);
				} else {
					txnHeaderData.push(['Bill date', MyDate.getDate('d-m-y', txn.getTxnReturnDate())]);
				}
			}
		}

		var CustomFieldsCache = require('./../Cache/CustomFieldsCache.js');
		var customFieldsCache = new CustomFieldsCache();

		if ((txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) && customFieldsCache.isDeliveryDetailsEnable() && txn.getCustomFields()) {
			try {
				var customFieldstringObj = JSON.parse(txn.getCustomFields());
				var transportationDetailArray = customFieldstringObj.transportation_details;
				for (var i = 0; i < transportationDetailArray.length; i++) {
					var customFieldModel = customFieldsCache.getCustomFieldById(transportationDetailArray[i].id);
					if (customFieldModel && transportationDetailArray[i].value) {
						txnHeaderData.push([customFieldModel.getCustomFieldName(), transportationDetailArray[i].value]);
					}
				}
			} catch (err) {}
		}

		var txnHeaderString = '';
		var extraCellNeeded = false;

		for (var _i = 0; _i < txnHeaderData.length; _i++) {
			var txnHeader = txnHeaderData[_i];
			var borderStyle = ' borderBottomForTxn ';
			var isEvenCell = _i % 2 == 0;
			if (isEvenCell) {
				borderStyle += ' borderRightForTxn ';
				txnHeaderString += '<tr>';
				extraCellNeeded = true;
			} else {
				extraCellNeeded = false;
			}
			txnHeaderString += "<td width='50%' class='" + borderStyle + "'>" + txnHeader[0] + '<br/><b>' + txnHeader[1] + '</b></td>';
			if (!isEvenCell) {
				txnHeaderString += '</tr>';
			}
		}

		if (extraCellNeeded) {
			txnHeaderString += "<td width='50%' class='borderBottomForTxn'></td></tr>";
		}

		if (settingCache.isPrintPartyShippingAddressEnabled() && shipAddressHeader) {
			var shippingAddress = txn.getShippingAddress() || party.getShippingAddress();
			if (shippingAddress) {
				shippingAddress = shippingAddress.replace(/(?:\r\n|\r|\n)/g, '<br/>');
				txnHeaderString += '<tr><td colspan=\'2\' width=\'100%\'>' + shipAddressHeader + '</td></tr><tr><td colspan=\'2\' width=\'100%\' >' + shippingAddress + '</td></tr>';
			}
		}

		return "<table id='txnHeader' width='100%'>" + txnHeaderString + '</table>';
	};

	this.getExtraTopSpace = function (textSizeRatio) {
		var extraSpaceInLine = settingCache.getExtraSpaceOnTxnPDF();
		if (extraSpaceInLine > 0) {
			var outerStyle = 'padding-bottom:' + 10 * textSizeRatio + 'px;padding-top:' + 20 * extraSpaceInLine * textSizeRatio + 'px;';
			return "<div style='" + outerStyle + "; color:#fffffe;'>.</div>";
		}
		return '';
	};

	this.getSignature = function (txn, theme, themeColor, textSizeRatio, printDeliveryChallan) {
		var signatureHTML = '';
		var deliveredByHTML = '';
		var receivedByHTML = '';
		var upiDetailsHTML = this.getUPIPaymentDetails(txn, textSizeRatio);

		var textColorForHeading = 'white';
		if (themeColor == '#ffffff') {
			textColorForHeading = 'black';
		}

		if (printDeliveryChallan) {
			var deliveredByHTMLText = '<tr><td width="100%">Name: </td></tr>';
			deliveredByHTMLText += '<tr><td width="100%">Comment: </td></tr>';
			deliveredByHTMLText += '<tr><td width="100%">Date: </td></tr>';
			deliveredByHTMLText += "<tr><td width=\"100%\" style='padding-bottom: 10px;'>Signature: </td></tr>";

			if (theme == InvoiceTheme.THEME_4) {
				deliveredByHTML += '<tr><td style="padding-top:' + 10 * textSizeRatio + 'px;"><b>Delivered By: </b></td></tr>';
			} else {
				deliveredByHTML += '<tr style="background-color:' + themeColor + '; color:' + textColorForHeading + ';"><th align="left" width="100%"> Delivered By:</th></tr>';
			}
			deliveredByHTML = "<table width='100%'>" + deliveredByHTML + deliveredByHTMLText + '</table>';

			if (theme == InvoiceTheme.THEME_4) {
				receivedByHTML += '<tr><td style="padding-top:' + 10 * textSizeRatio + 'px;"><b>Received By: </b></td></tr>';
			} else {
				receivedByHTML += '<tr style="background-color:' + themeColor + '; color:' + textColorForHeading + ';"><th align="left" width="100%"> Received By:</th></tr>';
			}
			receivedByHTML = "<table width='100%'>" + receivedByHTML + deliveredByHTMLText + '</table>';
		}

		if (settingCache.isSignatureEnabled()) {
			var firm = firmCache.getTransactionFirmWithDefault(txn);
			var imageBlob = firm.getFirmSignImagePath();
			signatureHTML = '\n\t\t\t<table width=\'100%\'>\n\t\t\t\t<tr>\n\t\t\t\t\t<td style="text-align:center;" align="center">For, ' + firm.getFirmName() + '</td>\n\t\t\t\t</tr>\n\t\t\t\t<tr>\n\t\t\t\t\t<td style="height:' + 84 * textSizeRatio + 'px; text-align:center;" align="center">\n\t\t\t\t\t\t' + (imageBlob ? '<img src=\'data:image/png;base64,' + imageBlob + '\' style=\'height:' + 84 * textSizeRatio + 'px; width:' + 168 * textSizeRatio + 'px;\'></img>' : '') + '\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t\t<tr>\n\t\t\t\t\t<td align="center" style="text-align:center;" >' + settingCache.getSignatureText() + '</td>\n\t\t\t\t</tr>\n\t\t\t</table>';
			if (this.isForInvoicePreview) {
				var showEditView = this.getEditSectionClassName('signatureLogo');
				var classText = ' class="invoicePreviewEditSection ' + showEditView + '" ';
				signatureHTML = '<div style="width:100%;" editsection="signatureLogo" ' + classText + '>' + signatureHTML + '</div>';
			}
		}
		if (signatureHTML || deliveredByHTML || receivedByHTML || upiDetailsHTML) {
			var upiHTMLRatio = upiDetailsHTML ? 1 : 0;
			var receivedByHTMLRatio = receivedByHTML ? 1 : 0;
			var deliveredByHTMLRatio = deliveredByHTML ? 1 : 0;
			var signatureHTMLRatio = signatureHTML ? 1 : 0;

			var totalRatio = upiHTMLRatio + receivedByHTMLRatio + deliveredByHTMLRatio + signatureHTMLRatio;

			if (signatureHTMLRatio == totalRatio) {
				// While printing only signature
				return "<table width='100%'><tr><td width='55%' valign='top' style='padding-right: " + 10 * textSizeRatio + "px; padding-left: 0px; padding-top: 0px; '></td>" + "<td width='45%' valign='top' style='padding-right: 0px; padding-left: 0px; padding-top: 0px; '>" + signatureHTML + '</td></tr></table>';
			} else {
				return "<table width='100%'><tr>" + (upiHTMLRatio ? "<td width='" + upiHTMLRatio * 100 / totalRatio + "%' valign='top' style='padding-right: " + 10 * textSizeRatio + "px; padding-left: 0px; padding-top: 0px; '>" + upiDetailsHTML + '</td>' : '') + (receivedByHTMLRatio ? "<td width='" + receivedByHTMLRatio * 100 / totalRatio + "%' valign='top' style='padding-right: " + 10 * textSizeRatio + "px; padding-left: 0px; padding-top: 0px; '>" + receivedByHTML + '</td>' : '') + (deliveredByHTMLRatio ? "<td width='" + deliveredByHTMLRatio * 100 / totalRatio + "%' valign='top' style='padding-right: " + 10 * textSizeRatio + "px; padding-left: 0px; padding-top: 0px; '>" + deliveredByHTML + '</td>' : '') + (signatureHTMLRatio ? "<td width='" + signatureHTMLRatio * 100 / totalRatio + "%' valign='top' style='padding-right: " + 10 * textSizeRatio + "px; padding-left: 0px; padding-top: 0px; '>" + signatureHTML + '</td>' : '') + '</tr></table>';
			}
		}

		return '';
	};

	this.getDueDateForTheme9 = function (txn) {
		var dueDate = txn.getTxnDueDate();
		if (dueDate != null) {
			if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER || (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE) && settingCache.isPaymentTermEnabled() && !MyDate.isSameDate(txn.getTxnDate(), txn.getTxnDueDate())) {
				return ['Due Date:', MyDate.getDate('d-m-y', dueDate)];
			}
		}
		return null;
	};

	this.getDueDate = function (txn, refTextColor) {
		var dueDate = txn.getTxnDueDate();
		if (dueDate != null) {
			if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER || (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE) && settingCache.isPaymentTermEnabled() && !MyDate.isSameDate(txn.getTxnDate(), txn.getTxnDueDate())) {
				return "<p class='topPadding' align='right'  style='color:" + refTextColor + "'><b>Due Date: " + MyDate.getDate('d-m-y', dueDate) + '</b></p>';
			}
		}
		return '';
	};

	this.getTermsAndConditionsHTML = function (txn, theme, themeColor, textSizeRatio, printDeliveryChallan) {
		var textColorForHeading = 'white';
		if (themeColor == '#ffffff') {
			textColorForHeading = 'black';
		}
		var descriptionHTML = '';
		// cases for adding different terms and conditions
		switch (txn.getTxnType()) {
			case TxnTypeConstant.TXN_TYPE_SALE:
				var termsAndConditionText = settingCache.getTermsAndConditionSaleInvoivce();
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
				var termsAndConditionText = settingCache.getTermsAndConditionSaleOrder();
				break;
			case TxnTypeConstant.TXN_TYPE_ESTIMATE:
				var termsAndConditionText = settingCache.getTermsAndConditionEstimateQuotation();
				break;
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
				var termsAndConditionText = settingCache.getTermsAndConditionDeliveryChallan();
				break;
		}

		if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE && settingCache.isPrintTermsAndConditionSaleInvoiceEnabled() || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER && settingCache.isPrintTermsAndConditionSaleOrderEnabled() || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE && settingCache.isPrintTermsAndConditionEstimateQuotationEnabled()) {
			if (printDeliveryChallan && settingCache.isPrintTermsAndConditionDeliveryChallanEnabled()) {
				var termsAndConditionText = settingCache.getTermsAndConditionDeliveryChallan();
			}
			if (!termsAndConditionText) {
				termsAndConditionText = settingCache.getTermsAndCondition() || 'Thanks for doing business with us!';
			}
			termsAndConditionText = termsAndConditionText.replace(/(?:\r\n|\r|\n)/g, '<br/>');
			var termsAndConditionHTML = '';
			if (theme == InvoiceTheme.THEME_4) {
				termsAndConditionHTML += "<tr><td style='padding-top:" + 10 * textSizeRatio + "px;'><b>Terms and conditions: </b>" + termsAndConditionText + '</td></tr>';
			} else {
				termsAndConditionHTML += "<tr style='background-color:" + themeColor + '; color:' + textColorForHeading + ";'><th align='left' width='100%'> Terms and conditions:</th></tr>" + '<tr><td>' + termsAndConditionText + '</td></tr>';
			}

			if (this.isForInvoicePreview) {
				var classText = ' class="invoicePreviewEditSection  ' + this.getEditSectionClassName('termsAndConditions') + '"';
				termsAndConditionHTML = '<div style="width:100%" editsection=\'termsAndConditions\' ' + classText + '><table width=\'100%\'>' + termsAndConditionHTML + '</table></div>';
			}
			if (printDeliveryChallan && !settingCache.isPrintTermsAndConditionDeliveryChallanEnabled()) {
				termsAndConditionHTML = '';
			}
			descriptionHTML += termsAndConditionHTML;
		}

		if (descriptionHTML) {
			descriptionHTML = "<tr><td valign='top' width='100%' style='padding-right: " + 10 * textSizeRatio + "px; padding-left: 0px; padding-top: 0px; vertical-align: top;'>" + descriptionHTML + '</td></tr>';
		}
		return descriptionHTML;
	};

	this.getAmountAndDescriptionHTML = function (txn, theme, themeColor, textSizeRatio, printDeliveryChallan) {
		var textColorForHeading = 'white';
		if (themeColor == '#ffffff') {
			textColorForHeading = 'black';
		}

		var descriptionHTML = '';

		var balanceAmount = txn.getBalanceAmount() ? Number(txn.getBalanceAmount()) : 0;
		var cashAmount = txn.getCashAmount() ? Number(txn.getCashAmount()) : 0;
		var discountAmount = txn.getDiscountAmount() ? Number(txn.getDiscountAmount()) : 0;
		var reverseChargeAmount = txn.getReverseChargeAmount() ? Number(txn.getReverseChargeAmount()) : 0;
		var totalAmount = balanceAmount + cashAmount + reverseChargeAmount;

		if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHIN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHOUT) {
			totalAmount = totalAmount + discountAmount;
		} else {
			balanceAmount = txn.getTxnCurrentBalanceAmount() ? Number(txn.getTxnCurrentBalanceAmount()) : 0; // setting current balance amount
			cashAmount = totalAmount - balanceAmount - reverseChargeAmount; // setting total received amount
		}

		var amountHeading = 'Amount In Words: ';
		switch (txn.getTxnType()) {
			case TxnTypeConstant.TXN_TYPE_SALE:
				amountHeading = 'Invoice ' + amountHeading;
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE:
				amountHeading = 'Bill ' + amountHeading;
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
				amountHeading = 'Order ' + amountHeading;
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
				amountHeading = 'Order ' + amountHeading;
				break;
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
				amountHeading = 'Delivery Challan ' + amountHeading;
				break;
			case TxnTypeConstant.TXN_TYPE_ESTIMATE:
				amountHeading = 'Estimate ' + amountHeading;
				break;
		}

		var amountWordInString = CurrencyHelper.getAmountInWords(totalAmount);

		if (theme == InvoiceTheme.THEME_4) {
			descriptionHTML += "<tr><td width='100%' style='padding-top:" + 20 * textSizeRatio + "px;'><b>" + amountHeading + '</b>' + amountWordInString + '</td></tr>';
		} else {
			descriptionHTML += "<tr style='background-color:" + themeColor + '; color:' + textColorForHeading + ";'><th align='left' width='100%'>" + amountHeading + '</th></tr>' + "<tr><td width='100%'  style='padding-bottom:" + 15 * textSizeRatio + "px;'>" + amountWordInString + '</td></tr>';
		}

		if (settingCache.printPaymentMode() && txn.getTxnType() != TxnTypeConstant.TXN_TYPE_ESTIMATE && txn.getTxnType() != TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN) {
			try {
				var paymentModeString = 'Credit';
				if (cashAmount > 0) {
					var PaymentCache = require('./../Cache/PaymentInfoCache.js');
					var paymentCache = new PaymentCache();
					var paymentTypeId = txn.getPaymentTypeId() + '';
					var paymentInfoObj = paymentCache.getPaymentInfoObjById(paymentTypeId);
					var paymentName = paymentInfoObj.getName();
					var paymentRef = txn.getPaymentTypeReference();
					paymentModeString = paymentName;
					if (paymentRef) {
						paymentModeString += ' (' + paymentRef + ')';
					}
				}

				if (theme == InvoiceTheme.THEME_4) {
					descriptionHTML += "<tr><td width='100%' style='padding-top:" + 10 * textSizeRatio + "px;'><b>Payment mode: </b>" + paymentModeString + '</td></tr>';
				} else {
					descriptionHTML += "<tr style='background-color:" + themeColor + '; color:' + textColorForHeading + ";'><th align='left' width='100%'> Payment mode:</th></tr>" + "<tr><td width='100%' style='padding-bottom:" + 15 * textSizeRatio + "px;'>" + paymentModeString + '</td></tr>';
				}
			} catch (err) {}
		}

		var description = txn.getDescription();

		if (settingCache.isPrintDescriptionEnabled() && description) {
			description = description.replace(/(?:\r\n|\r|\n)/g, '<br/>');
			if (theme == InvoiceTheme.THEME_4) {
				descriptionHTML += "<tr><td width='100%' style='padding-top:" + 10 * textSizeRatio + "px;'><b>Description: </b>" + description + '</td></tr>';
			} else {
				descriptionHTML += "<tr style='background-color:" + themeColor + '; color:' + textColorForHeading + ";'><th align='left' width='100%'> Description:</th></tr>" + "<tr><td width='100%' style='padding-bottom:" + 15 * textSizeRatio + "px;'>" + description + '</td></tr>';
			}
		}

		descriptionHTML += this.getTermsAndConditionsHTML(txn, theme, themeColor, textSizeRatio, printDeliveryChallan);

		if ((txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) && settingCache.getUserBankEnabled() && !printDeliveryChallan) {
			var bankDetails = '';
			var firm = firmCache.getTransactionFirmWithDefault(txn);
			if (firm) {
				if (firm.getBankName()) {
					bankDetails += '<tr><td width="100%">Bank Name: ' + firm.getBankName() + '</td></tr>';
				}
				if (firm.getAccountNumber()) {
					bankDetails += '<tr><td width="100%">Bank Account No.: ' + firm.getAccountNumber() + '</td></tr>';
				}
				if (firm.getBankIFSC()) {
					bankDetails += '<tr><td width="100%">Bank IFSC code: ' + firm.getBankIFSC() + '</td></tr>';
				}

				if (bankDetails || this.isForInvoicePreview) {
					if (theme == InvoiceTheme.THEME_4) {
						bankDetails = '<tr><td style="padding-top:' + 10 * textSizeRatio + 'px;"><b>Bank details: </b></td></tr>' + bankDetails;
					} else {
						bankDetails = '<tr style="background-color:' + themeColor + '; color:' + textColorForHeading + ';"><th align="left" width="100%"> Bank details:</th></tr>' + bankDetails;
					}
				}

				if (this.isForInvoicePreview) {
					var classText = ' class="invoicePreviewEditSection  ' + this.getEditSectionClassName('payToDetails') + '"';
					bankDetails = '<tr><td style="padding:0px" width=\'100%\'><div style="width:100%" editsection=\'payToDetails\' ' + classText + '><table width=\'100%\'>' + bankDetails + '</table></div></td></tr>';
				}

				descriptionHTML += bankDetails;
			}
		}

		var taxDetailsHTML = this.getTaxCodeDistributionTable(txn, theme, themeColor, textSizeRatio);

		if (descriptionHTML) {
			descriptionHTML = "<table width='100%'>" + descriptionHTML + '</table>';
		}

		var amountTable = getAmountHTMLTable(txn, theme, themeColor, textSizeRatio);

		var htmlText = "<table width='100%' style='margin-bottom:" + 10 * textSizeRatio + "px;'><tr><td valign='top' width='55%' style='padding-right: " + 10 * textSizeRatio + "px; padding-left: 0px; padding-top: 0px; vertical-align: top;'>" + taxDetailsHTML + descriptionHTML + '</td>' + "<td width='45%' valign='top' style='padding-right: 0px; padding-left: 0px; padding-top: 0px; vertical-align: top;'>" + amountTable + '</td></tr></table>';

		return htmlText;
	};

	this.getTaxCodeDistributionDataForLineItems = function (txn) {
		return this.getTaxCodeDistributionData(txn, true, false);
	};

	this.getTaxCodeDistributionDataForTransactionLevel = function (txn) {
		return this.getTaxCodeDistributionData(txn, false, true);
	};

	this.getTaxCodeDistributionData = function (txn) {
		var includeLineItemTax = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
		var includeTransactionLevelTax = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

		var taxDetailMap = {};
		var totalAdditionalCESS = 0;
		var lineItems = txn.getLineItems();

		if (includeLineItemTax) {
			if (lineItems.length > 0) {
				$.each(lineItems, function (key, lineItem) {
					if (lineItem.getLineItemAdditionalCESS()) {
						totalAdditionalCESS += Number(lineItem.getLineItemAdditionalCESS());
					}
					if (lineItem.getLineItemTaxId()) {
						var taxCode = taxCodeCache.getTaxCodeObjectById(lineItem.getLineItemTaxId());
						var taxableAmount = Number(lineItem.getLineItemTotal()) - Number(lineItem.getLineItemAdditionalCESS()) - Number(lineItem.getLineItemTaxAmount());
						if (taxCode.getTaxType() == TaxCodeConstants.taxGroup) {
							var taxCodeIds = (0, _values2.default)(taxCode.getTaxCodeMap());
							if (taxCodeIds.length > 0) {
								for (i = 0; i < taxCodeIds.length; i++) {
									var taxCodeId = taxCodeIds[i];
									if (taxDetailMap[taxCodeId]) {
										taxDetailMap[taxCodeId].push(taxableAmount);
									} else {
										var list = [];
										list.push(taxableAmount);
										taxDetailMap[taxCodeId] = list;
									}
								}
							}
						} else {
							var taxCodeId = taxCode.getTaxCodeId();
							if (taxDetailMap[taxCodeId]) {
								taxDetailMap[taxCodeId].push(taxableAmount);
							} else {
								var list = [];
								list.push(taxableAmount);
								taxDetailMap[taxCodeId] = list;
							}
						}
					}
				});
			}
		}
		if (txn.getTransactionTaxId() && includeTransactionLevelTax) {
			var taxCode = taxCodeCache.getTaxCodeObjectById(txn.getTransactionTaxId());

			if (taxCode.getTaxType() == TaxCodeConstants.taxGroup) {
				var taxCodeIds = (0, _values2.default)(taxCode.getTaxCodeMap());
				if (taxCodeIds.length > 0) {
					for (i = 0; i < taxCodeIds.length; i++) {
						var taxCodeId = taxCodeIds[i];
						if (taxDetailMap[taxCodeId]) {
							taxDetailMap[taxCodeId].push(txn.getSubTotalAmount() - txn.getDiscountAmount());
						} else {
							var list = [];
							list.push(txn.getSubTotalAmount() - txn.getDiscountAmount());
							taxDetailMap[taxCodeId] = list;
						}
					}
				}
			} else {
				var taxCodeId = taxCode.getTaxCodeId();
				if (taxDetailMap[taxCodeId]) {
					taxDetailMap[taxCodeId].push(txn.getSubTotalAmount() - txn.getDiscountAmount());
				} else {
					var list = [];
					list.push(txn.getSubTotalAmount() - txn.getDiscountAmount());
					taxDetailMap[taxCodeId] = list;
				}
			}
		}
		return {
			taxDetailMap: taxDetailMap,
			totalAdditionalCESS: totalAdditionalCESS
		};
	};

	this.getTaxCodeDistributionTable = function (txn, theme, themeColor, textSizeRatio) {
		var taxDetailsHTML = '';
		var textColorForHeading = 'white';
		if (themeColor == '#ffffff') {
			textColorForHeading = 'black';
		}
		var transactionType = txn.getTxnType();
		var lineItems = txn.getLineItems();
		var totalAdditionalCESS = 0;
		var taxDetailMap = {};

		if (settingCache.isPrintTaxDetailEnabled() && (transactionType == TxnTypeConstant.TXN_TYPE_SALE || transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE || transactionType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || transactionType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || transactionType == TxnTypeConstant.TXN_TYPE_ESTIMATE || transactionType == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER)) {
			var taxCodeDistributionData = this.getTaxCodeDistributionData(txn);
			totalAdditionalCESS = taxCodeDistributionData.totalAdditionalCESS;
			taxDetailMap = taxCodeDistributionData.taxDetailMap;

			var taxIds = (0, _keys2.default)(taxDetailMap);
			if (taxIds.length > 0 || totalAdditionalCESS > 0) {
				if (theme == InvoiceTheme.THEME_8) {
					var igstPresent = false;
					var cgstPresent = false;
					var sgstPresent = false;
					var cessPresent = false;
					var otherTaxPresent = false;
					var exemptedRatePresent = false;
					var stateSpecificCessPresent = false;

					var rateAmountMap = {};
					for (i = 0; i < taxIds.length; i++) {
						var taxId = taxIds[i];
						var taxCode = taxCodeCache.getTaxCodeObjectById(taxId);

						var amountValue = rateAmountMap[taxCode.getTaxRate()];
						if (!amountValue) {
							amountValue = [0, 0, 0, 0, 0, 0, 0];
							amountValue[0] = amountValue[1] = amountValue[2] = amountValue[3] = amountValue[4] = amountValue[5] = amountValue[6] = 0;
							amountValue[0] = amountValue[1] = amountValue[2] = amountValue[3] = amountValue[4] = amountValue[5] = amountValue[6] = 0;
							rateAmountMap[taxCode.getTaxRate()] = amountValue;
						}
						var taxAmount = 0;
						var amounts = taxDetailMap[taxId];
						for (var j = 0; j < amounts.length; j++) {
							taxAmount += MyDouble.getAmountWithDecimal(taxCode.getTaxRate() * amounts[j] / 100);
						}
						if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
							igstPresent = true;
							amountValue[0] += taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
							cgstPresent = true;
							amountValue[1] += taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
							sgstPresent = true;
							amountValue[2] += taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
							cessPresent = true;
							amountValue[3] += taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.STATE_SPECIFIC_CESS) {
							stateSpecificCessPresent = true;
							amountValue[6] += taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.OTHER) {
							otherTaxPresent = true;
							amountValue[4] += taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.Exempted) {
							exemptedRatePresent = true;
							amountValue[5] += taxAmount;
						}
					}
					var rateList = (0, _keys2.default)(rateAmountMap);
					if (rateList.length > 0 || totalAdditionalCESS > 0) {
						var classTaxColumn = ' borderBottomForTxn borderRightForTxn';
						rateList = rateList.sort();

						taxDetailsHTML += "<tr><td class='" + classTaxColumn + "' >Tax details</td>";

						for (var k = 0; k < rateList.length; k++) {
							taxDetailsHTML += "<td align='right' class='" + classTaxColumn + "' >" + MyDouble.getPercentageWithDecimal(rateList[k]) + '%</td>';
						}
						if (totalAdditionalCESS > 0 && rateList.length == 0) {
							taxDetailsHTML += "<td align='right' class='" + classTaxColumn + "' ></td>";
						}
						taxDetailsHTML += '</tr>';
						if (igstPresent) {
							taxDetailsHTML += "<tr><td class='" + classTaxColumn + "' >IGST</td>";
							for (var k = 0; k < rateList.length; k++) {
								var emptyForZero = true;
								if (rateList[k] == 0) {
									emptyForZero = false;
								}

								taxDetailsHTML += "<td align='right' class='" + classTaxColumn + "' >" + MyDouble.getAmountForInvoicePrint(rateAmountMap[rateList[k]][0], emptyForZero) + '</td>';
							}
							taxDetailsHTML += '</tr>';
						}
						if (cgstPresent) {
							taxDetailsHTML += "<tr><td class='" + classTaxColumn + "' >CGST</td>";
							for (var k = 0; k < rateList.length; k++) {
								var _emptyForZero = true;
								if (rateList[k] == 0) {
									_emptyForZero = false;
								}
								taxDetailsHTML += "<td align='right' class='" + classTaxColumn + "' >" + MyDouble.getAmountForInvoicePrint(rateAmountMap[rateList[k]][1], _emptyForZero) + '</td>';
							}
							taxDetailsHTML += '</tr>';
						}
						if (sgstPresent) {
							taxDetailsHTML += "<tr><td class='" + classTaxColumn + "' >" + this.stateTaxPlace + '</td>';
							for (var k = 0; k < rateList.length; k++) {
								var _emptyForZero2 = true;
								if (rateList[k] == 0) {
									_emptyForZero2 = false;
								}
								taxDetailsHTML += "<td align='right' class='" + classTaxColumn + "' >" + MyDouble.getAmountForInvoicePrint(rateAmountMap[rateList[k]][2], _emptyForZero2) + '</td>';
							}
							taxDetailsHTML += '</tr>';
						}
						if (cessPresent) {
							taxDetailsHTML += "<tr><td class='" + classTaxColumn + "' >CESS</td>";
							for (var k = 0; k < rateList.length; k++) {
								var _emptyForZero3 = true;
								if (rateList[k] == 0) {
									_emptyForZero3 = false;
								}
								taxDetailsHTML += "<td align='right' class='" + classTaxColumn + "' >" + MyDouble.getAmountForInvoicePrint(rateAmountMap[rateList[k]][3], _emptyForZero3) + '</td>';
							}
							taxDetailsHTML += '</tr>';
						}
						if (stateSpecificCessPresent) {
							taxDetailsHTML += "<tr><td class='" + classTaxColumn + "' >" + TransactionPrintSettings.getStateSpecificCessColumnHeader() + '</td>';
							for (var k = 0; k < rateList.length; k++) {
								var _emptyForZero4 = true;
								if (rateList[k] == 0) {
									_emptyForZero4 = false;
								}
								taxDetailsHTML += "<td align='right' class='" + classTaxColumn + "' >" + MyDouble.getAmountForInvoicePrint(rateAmountMap[rateList[k]][6], _emptyForZero4) + '</td>';
							}
							taxDetailsHTML += '</tr>';
						}
						if (otherTaxPresent) {
							taxDetailsHTML += "<tr><td class='" + classTaxColumn + "' >Other</td>";
							for (var k = 0; k < rateList.length; k++) {
								var _emptyForZero5 = true;
								if (rateList[k] == 0) {
									_emptyForZero5 = false;
								}
								taxDetailsHTML += "<td align='right' class='" + classTaxColumn + "' >" + MyDouble.getAmountForInvoicePrint(rateAmountMap[rateList[k]][4], _emptyForZero5) + '</td>';
							}
							taxDetailsHTML += '</tr>';
						}
						if (exemptedRatePresent) {
							taxDetailsHTML += "<tr><td class='" + classTaxColumn + "' >" + exemptedTaxString + '</td>';
							for (var k = 0; k < rateList.length; k++) {
								var _emptyForZero6 = true;
								if (rateList[k] == 0) {
									_emptyForZero6 = false;
								}
								taxDetailsHTML += "<td align='right' class='" + classTaxColumn + "' >" + MyDouble.getAmountForInvoicePrint(rateAmountMap[rateList[k]][5], _emptyForZero6) + '</td>';
							}
							taxDetailsHTML += '</tr>';
						}

						if (totalAdditionalCESS > 0) {
							taxDetailsHTML += "<tr><td class='" + classTaxColumn + "' >" + TransactionPrintSettings.getAdditionCESSColumnHeader() + '</td>';
							var colSpan = rateList.length;
							if (!colSpan || colSpan == 0) {
								colSpan = 1;
							}
							taxDetailsHTML += "<td align='right' colspan='" + colSpan + "' class='" + classTaxColumn + "' >" + MyDouble.getAmountWithDecimalAndCurrency(totalAdditionalCESS) + '</td>';
							taxDetailsHTML += '</tr>';
						}
						taxDetailsHTML = "<table width='100%'>" + taxDetailsHTML + '</table>';
					}
				} else {
					for (i = 0; i < taxIds.length; i++) {
						var taxId = taxIds[i];
						var taxCode = taxCodeCache.getTaxCodeObjectById(taxId);

						var _taxAmount = 0;
						var taxableAmount = 0;
						var amounts = taxDetailMap[taxId];
						for (var j = 0; j < amounts.length; j++) {
							_taxAmount += MyDouble.getAmountWithDecimal(taxCode.getTaxRate() * amounts[j] / 100);
							taxableAmount += MyDouble.getAmountWithDecimal(amounts[j]);
						}
						taxDetailsHTML += '<tr><td>';
						if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
							taxDetailsHTML += 'IGST';
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
							taxDetailsHTML += 'CGST';
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
							taxDetailsHTML += this.stateTaxPlace;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
							taxDetailsHTML += 'CESS';
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.Exempted) {
							taxDetailsHTML += exemptedTaxString;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.OTHER) {
							taxDetailsHTML += taxCode.getTaxCodeName();
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.STATE_SPECIFIC_CESS) {
							taxDetailsHTML += taxCode.getTaxCodeName();
						}
						taxDetailsHTML += '</td>';
						taxDetailsHTML += "<td align='right' >" + MyDouble.getAmountWithDecimalAndCurrency(taxableAmount) + '</td>';
						taxDetailsHTML += "<td align='right' >" + MyDouble.getPercentageWithDecimal(taxCode.getTaxRate()) + '%</td>';
						taxDetailsHTML += "<td align='right' >" + MyDouble.getAmountWithDecimalAndCurrency(_taxAmount) + '</td></tr>';
					}
					if (totalAdditionalCESS > 0) {
						taxDetailsHTML += "<tr><td colspan='2'>" + TransactionPrintSettings.getAdditionCESSColumnHeader() + "</td> <td colspan='2' align='right'>" + MyDouble.getAmountWithDecimalAndCurrency(totalAdditionalCESS) + '</td></tr>';
					}
					if (taxDetailsHTML) {
						if (theme == InvoiceTheme.THEME_4) {
							taxDetailsHTML = '<tr style="padding-top:' + 20 * textSizeRatio + "px;\"><th width='27%' align='left'><b>Tax type</b></th><th width='27%'  align='right'><b>Taxable amount</b></th><th width='19%'  align='right'><b>Rate</b></th><th width='27%'  align='right'><b>Tax amount</b></th></tr>" + taxDetailsHTML;
						} else {
							taxDetailsHTML = '<tr style="background-color:' + themeColor + '; color:' + textColorForHeading + ";\"><th width='27%' align='left'><b>Tax type</b></th><th width='27%'  align='right'><b>Taxable amount</b></th><th width='19%'  align='right'><b>Rate</b></th><th width='27%'  align='right'><b>Tax amount</b></th></tr>" + taxDetailsHTML;
						}

						if (theme == InvoiceTheme.THEME_5 || theme == InvoiceTheme.THEME_6 || theme == InvoiceTheme.THEME_7) {
							taxDetailsHTML = "<table width='100%'>" + taxDetailsHTML + '</table>';
						} else {
							taxDetailsHTML = "<table width='100%' style=\"margin-bottom:" + 20 * textSizeRatio + 'px;">' + taxDetailsHTML + '</table>';
						}
					}
				}
			}
		}
		return taxDetailsHTML;
	};

	this.getAmountInWordsTable = function (txn, theme, themeColor, textSizeRatio) {
		var textColorForHeading = 'white';

		if (themeColor == '#ffffff') {
			textColorForHeading = 'black';
		}
		var amountInWordsTable = "<table width='100%'><tr><th width='100%' style='background-color: " + themeColor + '; color: ' + textColorForHeading + "' align='center'>";

		switch (txn.getTxnType()) {
			case TxnTypeConstant.TXN_TYPE_SALE:
				amountInWordsTable += 'Invoice ';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE:
				amountInWordsTable += 'Bill ';
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
				amountInWordsTable += 'Order ';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
				amountInWordsTable += 'Order ';
				break;
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
				amountInWordsTable += 'Delivery Challan ';
				break;
			case TxnTypeConstant.TXN_TYPE_ESTIMATE:
				amountInWordsTable += 'Estimate ';
				break;
		}

		amountInWordsTable += 'Amount In Words</th></tr>';
		amountInWordsTable += "<tr><td align='center'>";

		var balanceAmount = txn.getBalanceAmount() ? Number(txn.getBalanceAmount()) : 0;
		var cashAmount = txn.getCashAmount() ? Number(txn.getCashAmount()) : 0;
		var discountAmount = txn.getDiscountAmount() ? Number(txn.getDiscountAmount()) : 0;
		var reverseChargeAmount = txn.getReverseChargeAmount() ? Number(txn.getReverseChargeAmount()) : 0;
		var totalAmount = balanceAmount + cashAmount + reverseChargeAmount;

		if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHIN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHOUT) {
			totalAmount = totalAmount + discountAmount;
		} else {
			balanceAmount = txn.getTxnCurrentBalanceAmount() ? Number(txn.getTxnCurrentBalanceAmount()) : 0; // setting current balance amount
			cashAmount = totalAmount - balanceAmount - reverseChargeAmount; // setting total received amount
		}

		var amountWordInString = CurrencyHelper.getAmountInWords(totalAmount);
		amountInWordsTable += amountWordInString + '</td></tr></table>';

		if (settingCache.printPaymentMode() && txn.getTxnType() != TxnTypeConstant.TXN_TYPE_ESTIMATE && txn.getTxnType() != TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN) {
			try {
				var paymentModeString = 'Credit';
				if (cashAmount > 0) {
					var PaymentCache = require('./../Cache/PaymentInfoCache.js');
					var paymentCache = new PaymentCache();
					var paymentTypeId = txn.getPaymentTypeId() + '';
					var paymentInfoObj = paymentCache.getPaymentInfoObjById(paymentTypeId);
					var paymentName = paymentInfoObj.getName();
					var paymentRef = txn.getPaymentTypeReference();
					paymentModeString = paymentName;
					if (paymentRef) {
						paymentModeString += ' (' + paymentRef + ')';
					}
				}

				amountInWordsTable += "<table width='100%'><tr><th width='100%' style='background-color: " + themeColor + '; color: ' + textColorForHeading + "' align='center'>Payment Mode</th></tr><tr><td align='center'> " + paymentModeString + '</td></tr></table>';
			} catch (err) {}
		}

		return amountInWordsTable;
	};

	this.getAmountHTMLForTheme5 = function (txn, theme, themeColor, textSizeRatio) {
		var box1 = '';
		if (theme == InvoiceTheme.THEME_5) {
			box1 = this.getAmountInWordsTable(txn, theme, themeColor, textSizeRatio);
		} else {
			box1 = this.getTaxCodeDistributionTable(txn, theme, themeColor, textSizeRatio);
		}
		var amountTable = getAmountHTMLTable(txn, InvoiceTheme.THEME_5, themeColor, textSizeRatio);

		var htmlText = "<table width='100%'><tr><td valign='top' width='50%' style='padding: 0px; vertical-align: top;' class='borderBottomForTxn borderTopForTxn borderLeftForTxn borderRightForTxn borderColorGrey'>" + box1 + '</td>' + "<td width='50%' valign='top' style='padding: 0px; vertical-align: top;' class='borderBottomForTxn borderTopForTxn borderLeftForTxn borderRightForTxn borderColorGrey'>" + amountTable + '</td></tr></table>';

		return htmlText;
	};

	this.getDescriptionAndDueDateTable = function (txn, theme, themeColor, textSizeRatio) {
		var txnType = txn.getTxnType();
		var textColorForHeading = 'white';
		if (themeColor == '#ffffff') {
			textColorForHeading = 'black';
		}
		var descriptionHTML = '';

		var description = txn.getDescription();

		if (settingCache.isPrintDescriptionEnabled() && description) {
			description = description || '';
			description = description.replace(/(?:\r\n|\r|\n)/g, '<br/>');
			descriptionHTML += "<tr style='background-color:" + themeColor + '; color:' + textColorForHeading + ";'><th align='left' width='100%'> Description:</th></tr>" + "<tr><td width='100%'>" + description + '</td></tr>';
		}

		if (descriptionHTML) {
			descriptionHTML = "<table width='100%'>" + descriptionHTML + '</table>';
		}
		return descriptionHTML;
	};

	this.getDescriptionAndDueDateHTMLForTheme5 = function (txn, theme, themeColor, textSizeRatio) {
		var box1 = '';
		if (theme == InvoiceTheme.THEME_5) {
			box1 = this.getTaxCodeDistributionTable(txn, theme, themeColor, textSizeRatio);
		} else {
			box1 = this.getAmountInWordsTable(txn, theme, themeColor, textSizeRatio);
		}

		var descriptionHTML = this.getDescriptionAndDueDateTable(txn, theme, themeColor, textSizeRatio);

		var htmlText = '';

		if (descriptionHTML || box1) {
			htmlText = "<table width='100%'><tr><td width='50%' valign='top' style=' padding: 0px' class='borderBottomForTxn borderTopForTxn borderRightForTxn borderLeftForTxn borderColorGrey'>" + box1 + '</td>' + "<td width='50%' valign='top' style=' padding: 0px;' class='borderBottomForTxn borderTopForTxn borderRightForTxn borderLeftForTxn borderColorGrey'>" + descriptionHTML + '</td></tr></table>';
		}
		return htmlText;
	};

	this.getTermsAndSignature = function (txn, themeColor, textSizeRatio, printDeliveryChallan) {
		var textColorForHeading = 'white';
		if (themeColor == '#ffffff') {
			textColorForHeading = 'black';
		}
		var termsAndConditionHTML = '';
		// case for getting different value for applying multiple terms and conditions.
		switch (txn.getTxnType()) {
			case TxnTypeConstant.TXN_TYPE_SALE:
				var termsAndConditionText = settingCache.getTermsAndConditionSaleInvoivce();
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
				var termsAndConditionText = settingCache.getTermsAndConditionSaleOrder();
				break;
			case TxnTypeConstant.TXN_TYPE_ESTIMATE:
				var termsAndConditionText = settingCache.getTermsAndConditionEstimateQuotation();
				break;
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
				var termsAndConditionText = settingCache.getTermsAndConditionDeliveryChallan();
				break;
			default:
				var termsAndConditionText = settingCache.getTermsAndCondition();
		}

		var bankDetailsHTML = '';

		if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE && settingCache.isPrintTermsAndConditionSaleInvoiceEnabled() || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER && settingCache.isPrintTermsAndConditionSaleOrderEnabled() || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE && settingCache.isPrintTermsAndConditionEstimateQuotationEnabled()) {
			if (printDeliveryChallan && settingCache.isPrintTermsAndConditionDeliveryChallanEnabled()) {
				var termsAndConditionText = settingCache.getTermsAndConditionDeliveryChallan();
			}
			if (!termsAndConditionText) {
				termsAndConditionText = 'Thanks for doing business with us!';
			}
			termsAndConditionText = termsAndConditionText.replace(/(?:\r\n|\r|\n)/g, '<br/>');
			termsAndConditionHTML = '\n\t\t\t\t<table width=\'100%\'>\n\t\t\t\t\t<tr style=\'background-color:' + themeColor + '; color:' + textColorForHeading + ';\'>\n\t\t\t\t\t\t<th align=\'left\' width=\'100%\'> Terms and conditions:</th>\n\t\t\t\t\t</tr>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<td width=\'100%\'>' + termsAndConditionText + '</td>\n\t\t\t\t\t</tr>\n\t\t\t\t</table>';
			if (printDeliveryChallan && !settingCache.isPrintTermsAndConditionDeliveryChallanEnabled()) {
				termsAndConditionHTML = '';
			}
			if (this.isForInvoicePreview) {
				var classText = ' class=\'invoicePreviewEditSection ' + this.getEditSectionClassName('termsAndConditions') + '\'';
				termsAndConditionHTML = '\n\t\t\t\t<div style="width:100%" editsection=\'termsAndConditions\' ' + classText + '>\n\t\t\t\t\t' + termsAndConditionHTML + '\n\t\t\t\t</div>';
			}
		}

		if ((txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) && settingCache.getUserBankEnabled() && !printDeliveryChallan) {
			var bankDetails = '';
			var firm = firmCache.getTransactionFirmWithDefault(txn);
			if (firm) {
				if (firm.getBankName()) {
					bankDetails += '<tr><td width="100%">Bank Name: ' + firm.getBankName() + '</td></tr>';
				}
				if (firm.getAccountNumber()) {
					bankDetails += '<tr><td width="100%">Bank Account No.: ' + firm.getAccountNumber() + '</td></tr>';
				}
				if (firm.getBankIFSC()) {
					bankDetails += '<tr><td width="100%">Bank IFSC code: ' + firm.getBankIFSC() + '</td></tr>';
				}
				bankDetailsHTML = bankDetails ? '<table width=\'100%\'><tr style=\'background-color: ' + themeColor + '; color: ' + textColorForHeading + '\'><th align="left" width="100%"> Bank details:</th></tr> ' + bankDetails + ' </table>' : '';

				if (this.isForInvoicePreview) {
					bankDetailsHTML = bankDetailsHTML || '<table width=\'100%\'><tr style=\'background-color: ' + themeColor + '; color: ' + textColorForHeading + '\'><th align="left" width="100%"> Bank details:</th></tr> </table>';
					var showEditView = this.getEditSectionClassName('payToDetails');
					var _classText3 = ' class=\'invoicePreviewEditSection ' + showEditView + '\' ';
					bankDetailsHTML = '<div style="width:100%;" editsection=\'payToDetails\' ' + _classText3 + '>' + bankDetailsHTML + '</div>';
				}
			}
		}

		var receivedByHTML = '';
		var deliveredByHTML = '';
		if (printDeliveryChallan) {
			receivedByHTML += "<table width='100%'><tr style=\"background-color:" + themeColor + '; color:' + textColorForHeading + ';"><th align="left" width="100%"> Received By:</th></tr>';
			receivedByHTML += '<tr><td width="100%">Name: </td></tr>';
			receivedByHTML += '<tr><td width="100%">Comment: </td></tr>';
			receivedByHTML += '<tr><td width="100%">Date: </td></tr>';
			receivedByHTML += '<tr><td width="100%">Signature: </td></tr>';
			receivedByHTML += '</table>';

			deliveredByHTML += "<table width='100%'><tr style=\"background-color:" + themeColor + '; color:' + textColorForHeading + ';"><th align="left" width="100%"> Delivered By:</th></tr>';
			deliveredByHTML += '<tr><td width="100%">Name: </td></tr>';
			deliveredByHTML += '<tr><td width="100%">Comment: </td></tr>';
			deliveredByHTML += '<tr><td width="100%">Date: </td></tr>';
			deliveredByHTML += '<tr><td width="100%">Signature: </td></tr>';
			deliveredByHTML += '</table>';
		}

		var signatureHTML = '';

		if (settingCache.isSignatureEnabled()) {
			var firm = firmCache.getTransactionFirmWithDefault(txn);

			var imageBlob = firm.getFirmSignImagePath();

			signatureHTML = '\n\t\t\t<table width="100%" style="text-align:right">\n\t\t\t\t<tr>\n\t\t\t\t\t<td align="right">\n\t\t\t\t\t\t<table align="right" width="100%">\n\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<td style="text-align:center;" align="center">For, ' + firm.getFirmName() + '</td>\n\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<td style="height:' + 84 * textSizeRatio + 'px; text-align:center;" align="center">\n\t\t\t\t\t\t\t\t\t' + (imageBlob ? '<img src=\'data:image/png;base64,' + imageBlob + '\' style=\'height:' + 84 * textSizeRatio + 'px;width:' + 168 * textSizeRatio + 'px;\'></img>' : '') + '\n\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<td align="center" style="text-align:center;" >' + settingCache.getSignatureText() + '</td>\n\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t</table>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t</table>';
			if (this.isForInvoicePreview) {
				var _classText4 = ' class="invoicePreviewEditSection ' + this.getEditSectionClassName('signatureLogo') + '"';
				signatureHTML = '\n\t\t\t\t<div style="width:100%" editsection="signatureLogo" ' + _classText4 + '>\n\t\t\t\t\t' + signatureHTML + '\n\t\t\t\t</div>';
			}
		}

		var upiDetailsHTML = this.getUPIPaymentDetails(txn, textSizeRatio);

		var htmlText = '';

		termsAndConditionHTML = termsAndConditionHTML + bankDetailsHTML;

		if (termsAndConditionHTML || signatureHTML || upiDetailsHTML || receivedByHTML || deliveredByHTML) {
			var upiHTMLRatio = upiDetailsHTML ? 1 : 0;
			var termsAndConditionHTMLRatio = termsAndConditionHTML ? 1 : 0;
			var receivedByHTMLRatio = receivedByHTML ? 1 : 0;
			var deliveredByHTMLRatio = deliveredByHTML ? 1 : 0;
			var signatureHTMLRatio = signatureHTML ? 1 : 0;

			var totalRatio = upiHTMLRatio + receivedByHTMLRatio + deliveredByHTMLRatio + signatureHTMLRatio + termsAndConditionHTMLRatio;

			if (signatureHTMLRatio == totalRatio) {
				// While printing only signature
				htmlText = "<table width='100%'><tr><td width='50%' valign='top' style='padding: 0px' class='borderBottomForTxn borderTopForTxn borderRightForTxn borderLeftForTxn borderColorGrey'></td>" + "<td width='50%' valign='top' style=' padding: 0px' class='borderBottomForTxn borderTopForTxn borderRightForTxn borderLeftForTxn borderColorGrey'>" + signatureHTML + '</td></tr></table>';
			} else {
				htmlText = "<table width='100%'><tr>" + (termsAndConditionHTMLRatio > 0 ? "<td width='" + termsAndConditionHTMLRatio * 100 / totalRatio + "%' valign='top' style='padding: 0px' class='borderBottomForTxn borderTopForTxn borderRightForTxn borderLeftForTxn borderColorGrey'>" + termsAndConditionHTML + '</td>' : '') + (upiHTMLRatio > 0 ? "<td width='" + upiHTMLRatio * 100 / totalRatio + "%' valign='top' style='padding: 0px' class='borderBottomForTxn borderTopForTxn borderRightForTxn borderLeftForTxn borderColorGrey'>" + upiDetailsHTML + '</td>' : '') + (receivedByHTMLRatio > 0 ? "<td width='" + receivedByHTMLRatio * 100 / totalRatio + "%' valign='top' style=' padding: 0px' class='borderBottomForTxn borderTopForTxn borderRightForTxn borderLeftForTxn borderColorGrey'>" + receivedByHTML + '</td>' : '') + (deliveredByHTMLRatio > 0 ? "<td width='" + deliveredByHTMLRatio * 100 / totalRatio + "%' valign='top' style='padding: 0px' class='borderBottomForTxn borderTopForTxn borderRightForTxn borderLeftForTxn borderColorGrey'>" + deliveredByHTML + '</td>' : '') + (signatureHTMLRatio > 0 ? "<td width='" + signatureHTMLRatio * 100 / totalRatio + "%' valign='top' style=' padding: 0px' class='borderBottomForTxn borderTopForTxn borderRightForTxn borderLeftForTxn borderColorGrey'>" + signatureHTML + '</td>' : '') + '</tr></table>';
			}
		}
		return htmlText;
	};

	this.getDiscountTaxExtraChargeHTML = function (txn) {
		var subTotalHtml = '';
		var discountHTML = '';
		var taxHTML = '';
		var extraChargeHTML = '';
		var textHTML = '';
		var discountAmount = Number(txn.getDiscountAmount());
		var taxAmount = Number(txn.getTaxAmount());
		var subTotalAmount = Number(txn.getSubTotalAmount());

		if (subTotalAmount != 0) {
			subTotalHtml = "<tr><td width='50%' class='discountTaxTable'></td><td class='discountTaxTable' align='right' width='24%'>Sub Total:</td><td align='right' class='discountTaxTable' width='26%'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(subTotalAmount) + '</td></tr>';
		}
		if (discountAmount != 0) {
			discountHTML = "<tr><td width='50%' class='discountTaxTable'></td><td class='discountTaxTable' align='right' width='24%'>Discount (" + MyDouble.getPercentageWithDecimal(Number(txn.getDiscountPercent())) + "%):</td><td align='right' class='discountTaxTable' width='26%'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(discountAmount) + '</td></tr>';
		}
		if (taxAmount != 0) {
			taxHTML = '<tr><td width="50%" class="discountTaxTable"></td><td class="discountTaxTable" align="right" width="24%">Tax (' + MyDouble.getPercentageWithDecimal(Number(txn.getTaxPercent())) + '%):</td><td align="right" class="discountTaxTable" width="26%">' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(taxAmount) + '</td></tr>';
		}
		{
			if (txn.getAc1Amount() != 0) {
				extraChargeHTML += '<tr><td width="50%" class="discountTaxTable"></td><td class="discountTaxTable" align="right" width="24%">' + dataLoader.getCurrentExtraChargesValue('1') + '</td><td align="right" class="discountTaxTable" width="26%">' + MyDouble.getAmountWithDecimalAndCurrency(txn.getAc1Amount()) + '</td></tr>';
			}
			if (txn.getAc2Amount() != 0) {
				extraChargeHTML += '<tr><td width="50%" class="discountTaxTable"></td><td class="discountTaxTable" align="right" width="24%">' + dataLoader.getCurrentExtraChargesValue('2') + '</td><td align="right" class="discountTaxTable" width="26%">' + MyDouble.getAmountWithDecimalAndCurrency(txn.getAc2Amount()) + '</td></tr>';
			}
			if (txn.getAc3Amount() != 0) {
				extraChargeHTML += '<tr><td width="50%" class="discountTaxTable"></td><td class="discountTaxTable" align="right" width="24%">' + dataLoader.getCurrentExtraChargesValue('3') + '</td><td align="right" class="discountTaxTable" width="26%">' + MyDouble.getAmountWithDecimalAndCurrency(txn.getAc3Amount()) + '</td></tr>';
			}
			var roundOffValue = MyDouble.convertStringToDouble(txn.getRoundOffValue(), true);
			if (roundOffValue != 0) {
				extraChargeHTML += '<tr><td width="50%" class="discountTaxTable"></td><td class="discountTaxTable" align="right" width="24%">Round off</td><td align="right" class="discountTaxTable" width="26%">' + MyDouble.getAmountWithDecimalAndCurrency(roundOffValue) + '</td></tr>';
			}
		}
		if (subTotalHtml || discountHTML || taxHTML || extraChargeHTML) {
			textHTML = '<table width="100%">' + subTotalHtml + discountHTML + taxHTML + extraChargeHTML + '</table>';
		}
		// Remove last 2 occurance of discountTaxTable for getting underline in last row
		if (textHTML) {
			textHTML = replaceLastOccurance(textHTML, 'discountTaxTable', '');
			textHTML = replaceLastOccurance(textHTML, 'discountTaxTable', '');
		}
		return textHTML;
	};

	var replaceLastOccurance = function replaceLastOccurance(text, searchText, replaceText) {
		var n = text.lastIndexOf(searchText);
		if (n >= 0) {
			text = text.slice(0, n) + text.slice(n).replace(searchText, replaceText);
		}
		return text;
	};

	var getAmountHTMLTable = function getAmountHTMLTable(txn, theme, themeColor, textSizeRatio) {
		var textColorForHeading = 'white';
		if (themeColor == '#ffffff') {
			textColorForHeading = 'black';
		}

		var balanceAmount = txn.getBalanceAmount() ? Number(txn.getBalanceAmount()) : 0;
		var cashAmount = txn.getCashAmount() ? Number(txn.getCashAmount()) : 0;
		var discountAmount = txn.getDiscountAmount() ? Number(txn.getDiscountAmount()) : 0;
		var reverseChargeAmount = txn.getReverseChargeAmount() ? Number(txn.getReverseChargeAmount()) : 0;
		var totalAmount = balanceAmount + cashAmount + reverseChargeAmount;
		var taxAmount = txn.getTaxAmount() ? Number(txn.getTaxAmount()) : 0;
		var subTotalAmount = txn.getSubTotalAmount() ? Number(txn.getSubTotalAmount()) : 0;

		var amountHTML = "<table width='100%'>";
		if (theme != InvoiceTheme.THEME_4) {
			if (theme == InvoiceTheme.THEME_9) {
				amountHTML += "<tr><th align='left' width='50%'> Amounts:</th><th width='50%'></th></tr>";
			} else {
				amountHTML += "<tr style='background-color:" + themeColor + '; color:' + textColorForHeading + ";'><th align='left' width='50%'> Amounts:</th><th width='50%'></th></tr>";
			}
		}

		var subTotalAmountHTML = '';

		var txnType = txn.getTxnType();

		if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
			totalAmount = totalAmount + discountAmount;
		} else {
			balanceAmount = txn.getTxnCurrentBalanceAmount() ? Number(txn.getTxnCurrentBalanceAmount()) : 0; // setting current balance amount
			cashAmount = totalAmount - balanceAmount - reverseChargeAmount; // setting total received amount
		}

		if (subTotalAmount != 0 && (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_EXPENSE || txnType == TxnTypeConstant.TXN_TYPE_OTHER_INCOME || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_ESTIMATE || txnType == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER)) {
			subTotalAmountHTML += "<tr><td>Sub Total</td><td align='right'>" + MyDouble.getAmountWithDecimalAndCurrency(subTotalAmount) + '</td></tr>';
		}

		if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN) {
			subTotalAmountHTML += "<tr><td>Received</td><td align='right'>" + MyDouble.getAmountWithDecimalAndCurrency(cashAmount) + '</td></tr>';
		} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
			subTotalAmountHTML += "<tr><td>Paid</td><td align='right'>" + MyDouble.getAmountWithDecimalAndCurrency(cashAmount) + '</td></tr>';
		}

		if (discountAmount != 0) {
			if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
				subTotalAmountHTML += "<tr><td>Discount</td><td align='right'>" + MyDouble.getAmountWithDecimalAndCurrency(discountAmount) + '</td></tr>';
			} else {
				subTotalAmountHTML += '<tr><td>Discount (' + MyDouble.getPercentageWithDecimal(txn.getDiscountPercent()) + '%)</td><td align="right">' + MyDouble.getAmountWithDecimalAndCurrency(discountAmount) + '</td></tr>';
			}
		}

		if (txn.getTransactionTaxId()) {
			subTotalAmountHTML += '<tr><td>Tax (' + MyDouble.getPercentageWithDecimal(txn.getTaxPercent()) + "%)</td><td align='right'>" + MyDouble.getAmountWithDecimalAndCurrency(taxAmount) + '</td></tr>';
		}

		{
			if (txn.getAc1Amount() != 0) {
				subTotalAmountHTML += '<tr><td>' + dataLoader.getCurrentExtraChargesValue('1') + "</td><td align='right'>" + MyDouble.getAmountWithDecimalAndCurrency(txn.getAc1Amount()) + '</td></tr>';
			}
			if (txn.getAc2Amount() != 0) {
				subTotalAmountHTML += '<tr><td>' + dataLoader.getCurrentExtraChargesValue('2') + "</td><td align='right'>" + MyDouble.getAmountWithDecimalAndCurrency(txn.getAc2Amount()) + '</td></tr>';
			}
			if (txn.getAc3Amount() != 0) {
				subTotalAmountHTML += '<tr><td>' + dataLoader.getCurrentExtraChargesValue('3') + "</td><td align='right'>" + MyDouble.getAmountWithDecimalAndCurrency(txn.getAc3Amount()) + '</td></tr>';
			}
			var roundOffValue = MyDouble.convertStringToDouble(txn.getRoundOffValue(), true);
			if (roundOffValue != 0) {
				subTotalAmountHTML += "<tr><td>Round off</td><td align='right'>" + MyDouble.getAmountWithDecimalAndCurrency(roundOffValue) + '</td></tr>';
			}
		}

		if (txnType != TxnTypeConstant.TXN_TYPE_CASHIN && txnType != TxnTypeConstant.TXN_TYPE_CASHOUT || (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) && discountAmount != 0) {
			if (!subTotalAmountHTML) {
				subTotalAmountHTML += "<tr><td><b>Total</b></td><td align='right'><b>" + MyDouble.getAmountWithDecimalAndCurrency(totalAmount) + '</b></td></tr>';
			} else {
				subTotalAmountHTML += "<tr><td class='borderTopForTxn borderColorGrey'><b>Total</b></td><td align='right' class='borderTopForTxn borderColorGrey'><b>" + MyDouble.getAmountWithDecimalAndCurrency(totalAmount) + '</b></td></tr>';
			}
		}

		if ((txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) && Number(txn.getReverseCharge()) && reverseChargeAmount > 0) {
			var totalPayableString = 'Total payable amount';
			if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
				totalPayableString = 'Total receivable amount';
			}
			subTotalAmountHTML += "<tr><td>Tax under reverse charge</td><td align='right'>" + MyDouble.getAmountWithDecimalAndCurrency(reverseChargeAmount) + '</td></tr>';
			subTotalAmountHTML += "<tr><td class='borderTopForTxn borderColorGrey'>" + totalPayableString + "</td><td class='borderTopForTxn borderColorGrey' align='right'>" + MyDouble.getAmountWithDecimalAndCurrency(totalAmount - reverseChargeAmount) + '</td></tr>';
		}

		if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
			subTotalAmountHTML += "<tr><td>Paid</td><td align='right'>" + MyDouble.getAmountWithDecimalAndCurrency(cashAmount) + '</td></tr>';
		}
		if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
			if (txnType != TxnTypeConstant.TXN_TYPE_SALE || settingCache.isReceivedAmountEnabled()) {
				subTotalAmountHTML += "<tr><td>Received</td><td align='right'>" + MyDouble.getAmountWithDecimalAndCurrency(cashAmount) + '</td></tr>';
			}
		}
		if (txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
			subTotalAmountHTML += "<tr><td>Advance</td><td align='right'>" + MyDouble.getAmountWithDecimalAndCurrency(cashAmount) + '</td></tr>';
		}
		if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
			if (txnType != TxnTypeConstant.TXN_TYPE_SALE || settingCache.isBalanceAmountEnabled()) {
				subTotalAmountHTML += "<tr><td class='borderTopForTxn borderColorGrey'>Balance</td><td align='right' class='borderTopForTxn borderColorGrey'>" + MyDouble.getAmountWithDecimalAndCurrency(balanceAmount) + '</td></tr>';
			}
		}

		if (settingCache.isCurrentBalanceOfPartyEnabled()) {
			amountHTML += subTotalAmountHTML + '<tr><td class="borderTopForTxn borderColorGrey"></td><td align="right" class="borderTopForTxn borderColorGrey"></td></tr>';
			try {
				switch (txnType) {
					case TxnTypeConstant.TXN_TYPE_CASHIN:
					case TxnTypeConstant.TXN_TYPE_SALE:
					case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
					case TxnTypeConstant.TXN_TYPE_CASHOUT:
					case TxnTypeConstant.TXN_TYPE_PURCHASE:
					case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
					case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
						var name = txn.getNameRef();
						if (name) {
							var currentAmount = MyDouble.getAmountWithDecimalAndCurrency(name.getAmount());
							var showPreviousBalance = true;
							if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
								var txnBalAmount = MyDouble.convertStringToDouble(txn.getBalanceAmount());
								var txnCurrentBalAmount = MyDouble.convertStringToDouble(txn.getTxnCurrentBalanceAmount());
								if (Math.abs(txnBalAmount - txnCurrentBalAmount) > 0.001) {
									showPreviousBalance = false;
								}
							}
							if (showPreviousBalance) {
								showPreviousBalance = TransactionPrintHelper.isLatestTransaction(txn);
							}
							if (showPreviousBalance) {
								var previousBal = TransactionPrintHelper.getPreviousAmount(txn, name);
								amountHTML += '<tr><td>Previous Balance</td><td align="right">' + MyDouble.getAmountWithDecimalAndCurrency(previousBal) + '</td></tr>';
							}
							if (currentAmount && txnType != TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN) {
								amountHTML += '<tr><td>Current Balance</td><td align="right">' + currentAmount + '</td></tr>';
							}
						}
						break;
				}
			} catch (e) {
				// Todo: Log later
			}
		} else if (theme != InvoiceTheme.THEME_5 && theme != InvoiceTheme.THEME_6 && theme != InvoiceTheme.THEME_7 && theme != InvoiceTheme.THEME_8 && theme != InvoiceTheme.THEME_9) {
			amountHTML += subTotalAmountHTML + '<tr><td class="borderTopForTxn borderColorGrey"></td><td align="right" class="borderTopForTxn borderColorGrey"></td></tr>';
		} else {
			amountHTML += subTotalAmountHTML;
		}

		amountHTML += '</table>';

		return amountHTML;
	};

	this.getItemDetailsForReport = function (txn) {
		if (!settingCache) {
			settingCache = new SettingCache();
		}

		var lineItems = txn.getLineItems();
		var htmlText = '';
		if (lineItems && lineItems.length > 0) {
			var textSizeSetting = settingCache.getPrintTextSize();
			var textSizeRatio = 0.4 + 0.1 * textSizeSetting;
			htmlText = this.getItemDetails(txn, InvoiceTheme.THEME_0, 'lightgrey', textSizeRatio, false);
			htmlText += this.getDiscountTaxExtraChargeHTML(txn);
		}

		return htmlText;
	};

	this.getPartyInfoTable = function (txn, theme, themeColor, textSizeRatio) {
		var party = txn.getNameRef();
		var backgroundColor = '#ffffff';
		var textColorForHeading = '#000000';
		var partyHeaderText = 'Party details';

		if (theme == InvoiceTheme.THEME_3 || theme == InvoiceTheme.THEME_5) {
			backgroundColor = themeColor;
			textColorForHeading = '#ffffff';
			if (themeColor == '#ffffff') {
				textColorForHeading = 'black';
			}
		}

		switch (txn.getTxnType()) {
			case TxnTypeConstant.TXN_TYPE_SALE:
				partyHeaderText = 'Bill To';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE:
				partyHeaderText = 'Bill From';
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
				partyHeaderText = 'Return From';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
				partyHeaderText = 'Return To';
				break;
			case TxnTypeConstant.TXN_TYPE_CASHIN:
				partyHeaderText = 'Received From';
				break;
			case TxnTypeConstant.TXN_TYPE_CASHOUT:
				partyHeaderText = 'Paid To';
				break;
			case TxnTypeConstant.TXN_TYPE_EXPENSE:
				partyHeaderText = 'Expense For';
				break;
			case TxnTypeConstant.TXN_TYPE_OTHER_INCOME:
				partyHeaderText = 'Other Income From';
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
				partyHeaderText = 'Order From';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
				partyHeaderText = 'Order To';
				break;
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
				partyHeaderText = 'Delivery Challan For';
				break;
			case TxnTypeConstant.TXN_TYPE_ESTIMATE:
				partyHeaderText = 'Estimate For';
				break;
		}

		if (theme != InvoiceTheme.THEME_3 && theme != InvoiceTheme.THEME_9) {
			partyHeaderText += ':';
		}

		var classForHeader = '';
		if (theme == InvoiceTheme.THEME_5) {
			classForHeader = " class='borderBottomForTxn' ";
		}

		var partyDetailHTML = "<table width='100%'>";
		if (theme != InvoiceTheme.THEME_4) {
			if (theme == InvoiceTheme.THEME_9) {
				partyDetailHTML += '<tr ' + classForHeader + " style='background-color:" + backgroundColor + '; color:' + textColorForHeading + ";'><td align=\"left\" width=\"100%\">" + partyHeaderText + '</td></tr>';
			} else {
				partyDetailHTML += '<tr ' + classForHeader + " style='background-color:" + backgroundColor + '; color:' + textColorForHeading + ";'><th align=\"left\" width=\"100%\">" + partyHeaderText + '</th></tr>';
			}
		}

		var partyFullName = '';
		if (txn.getDisplayName() && txn.getDisplayName().trim()) {
			partyFullName = txn.getDisplayName().trim();
		} else {
			partyFullName = party.getFullName();
		}
		if (partyFullName) {
			partyFullName = partyFullName.replace(/(?:\r\n|\r|\n)/g, '<br/>');
		}

		if (theme != InvoiceTheme.THEME_4) {
			partyDetailHTML += '<tr class="boldText bigTextSize"><td>' + partyFullName + '</td></tr>';
		} else {
			partyDetailHTML += '<tr class="boldText bigTextSize"><td>' + partyHeaderText + partyFullName + '</td></tr>';
		}

		var mobileNumber = party.getPhoneNumber();
		var partyAddress = txn.getBillingAddress() || party.getAddress();
		var tinNumber = party.getTinNumber();
		var gstinNumber = party.getGstinNumber();

		if (partyAddress) {
			partyAddress = partyAddress.replace(/(?:\r\n|\r|\n)/g, '<br/>');
			partyDetailHTML += '<tr><td>' + partyAddress + '</td></tr>';
		}
		if (mobileNumber) {
			partyDetailHTML += '<tr><td>Contact No.: ' + mobileNumber + '</td></tr>';
		}

		if (settingCache.isTINNumberEnabled()) {
			if (settingCache.getGSTEnabled()) {
				if (gstinNumber) {
					partyDetailHTML += '<tr><td>GSTIN Number: ' + gstinNumber + '</td></tr>';
				}
			} else {
				if (tinNumber) {
					partyDetailHTML += '<tr><td>' + settingCache.getTINText() + ' Number: ' + tinNumber + '</td></tr>';
				}
			}
		}

		if (settingCache.getGSTEnabled()) {
			var placeOfSupply = party.getContactState();

			if (placeOfSupply && placeOfSupply.trim()) {
				partyDetailHTML += '<tr><td>State: ' + StateCode.getStateCode(placeOfSupply) + '-' + placeOfSupply + '</td></tr>';
			}
		}
		var partyUDF = party.getUdfObjectArray();
		var UDFCache = require('./../Cache/UDFCache.js');
		var udfCache = new UDFCache();
		if (partyUDF && partyUDF.length > 0) {
			for (var i = 0; i < partyUDF.length; i++) {
				var value = partyUDF[i];
				var model = udfCache.getUdfPartyModelByFieldId(value.getUdfFieldId());
				if (value.getUdfFieldValue() && model.getUdfFieldPrintOnInvoice() == 1) {
					var displayValue = value.getDisplayValue(model);
					partyDetailHTML += '<tr><td>' + model.getUdfFieldName() + ': ' + displayValue + '</td></tr>';
				}
			}
		}

		partyDetailHTML += '</table>';

		return partyDetailHTML;
	};

	this.getPartyInfoAndTxnHeaderData = function (txn, theme, themeColor, textSizeRatio) {
		var partyDetailHTML = this.getPartyInfoTable(txn, theme, themeColor, textSizeRatio);

		var shipAddressHeader = '';
		var transportationHeader = '';

		var party = txn.getNameRef();

		var backgroundColor = '#ffffff';
		var textColorForHeading = '#000000';
		var refTextColor = '#000000';

		var classForHeader = '';
		if (theme == InvoiceTheme.THEME_5) {
			classForHeader = " class='borderBottomForTxn' ";
		}

		var refText = '';

		if (theme == InvoiceTheme.THEME_3 || theme == InvoiceTheme.THEME_5 || theme == InvoiceTheme.THEME_9) {
			backgroundColor = themeColor;
			textColorForHeading = '#ffffff';
			if (themeColor == '#ffffff') {
				textColorForHeading = 'black';
			}
		}
		if (theme == InvoiceTheme.THEME_2 || theme == InvoiceTheme.THEME_3) {
			refTextColor = themeColor;
			if (themeColor == '#ffffff') {
				refTextColor = 'black';
			}
		}

		switch (txn.getTxnType()) {
			case TxnTypeConstant.TXN_TYPE_SALE:
				refText = 'Invoice No.: ';
				shipAddressHeader = 'Shipping To';
				transportationHeader = 'Transportation Details';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE:
				refText = 'Bill No.: ';
				shipAddressHeader = 'Shipping From';
				transportationHeader = 'Transportation Details';
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
				refText = 'Return No.: ';
				shipAddressHeader = 'Shipping From';
				transportationHeader = 'Transportation Details';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
				refText = 'Return No.: ';
				shipAddressHeader = 'Shipping To';
				transportationHeader = 'Transportation Details';
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
				shipAddressHeader = 'Shipping To';
				refText = 'Order No.: ';
				transportationHeader = 'Transportation Details';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
				shipAddressHeader = 'Shipping From';
				refText = 'Order No.: ';
				transportationHeader = 'Transportation Details';
				break;
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
				refText = 'Challan No.: ';
				shipAddressHeader = 'Shipping To';
				transportationHeader = 'Transportation Details';
				break;
			case TxnTypeConstant.TXN_TYPE_ESTIMATE:
				refText = 'Estimate No.: ';
				transportationHeader = 'Transportation Details';
				break;
			case TxnTypeConstant.TXN_TYPE_CASHIN:
			case TxnTypeConstant.TXN_TYPE_CASHOUT:
				refText = 'Receipt No.: ';
				break;
		}

		refText += txn.getFullTxnRefNumber();

		var shippingAddressHtml = '';

		if (settingCache.isPrintPartyShippingAddressEnabled() && shipAddressHeader) {
			var shippingAddress = txn.getShippingAddress() || party.getShippingAddress();
			if (shippingAddress) {
				shippingAddress = shippingAddress.replace(/(?:\r\n|\r|\n)/g, '<br/>');
				shippingAddressHtml = '\n\t\t\t\t\t\t<table width="100%">\n\t\t\t\t\t\t\t<tr ' + classForHeader + ' style=\'background-color:' + backgroundColor + '; color:' + textColorForHeading + '\'>\n\t\t\t\t\t\t\t\t<th align=\'left\' width=\'100%\'>' + shipAddressHeader + '</th>\n\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<td>' + shippingAddress + '</td>\n\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t</table>\n\t\t\t\t';
			}
		}

		var transportationDetails = '';
		var CustomFieldsCache = require('./../Cache/CustomFieldsCache.js');
		var customFieldsCache = new CustomFieldsCache();
		if (transportationHeader && customFieldsCache.isDeliveryDetailsEnable() && txn.getCustomFields()) {
			transportationDetails = '<table width="100%"><tr ' + classForHeader + " style='background-color:" + backgroundColor + '; color:' + textColorForHeading + "'><th align='left' width='100%'>" + 'Transportation Details</th></tr>';

			try {
				var customFieldstringObj = JSON.parse(txn.getCustomFields());
				var transportationDetailArray = customFieldstringObj.transportation_details;
				for (var i = 0; i < transportationDetailArray.length; i++) {
					var customFieldModel = customFieldsCache.getCustomFieldById(transportationDetailArray[i].id);
					if (customFieldModel) {
						transportationDetails += "<tr width='100%'><td>" + customFieldModel.getCustomFieldName() + ': ' + transportationDetailArray[i].value + '</td></tr>';
					}
				}
			} catch (err) {}

			transportationDetails += '</table>';
		}

		var dateAndRefNoHTML = '';

		if (settingCache.getGSTEnabled() && settingCache.isPlaceOfSupplyEnabled()) {
			if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
				var placeOfSupply = txn.getPlaceOfSupply();
				if (!placeOfSupply || !placeOfSupply.trim()) {
					if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
						var firm = firmCache.getTransactionFirmWithDefault(txn);
						placeOfSupply = firm.getFirmState();
					} else {
						placeOfSupply = party.getContactState();
					}
				}
				if (placeOfSupply && placeOfSupply.trim()) {
					dateAndRefNoHTML += '<p align="right"  style="color:' + refTextColor + '">Place of supply: ' + StateCode.getStateCode(placeOfSupply) + '-' + placeOfSupply + '</p>';
				}
			}
		}

		if (txn.getTxnRefNumber()) {
			dateAndRefNoHTML += "<p align='right'  style='color:" + refTextColor + "'><b>" + refText + '</b></p>';
		}
		var udfCache = new UDFCache();
		dateAndRefNoHTML += "<p align='right'  style='color:" + refTextColor + "'><b>Date: " + MyDate.getDate('d-m-y', txn.getTxnDate()) + '</b></p>';

		dateAndRefNoHTML += this.getDueDate(txn, refTextColor);

		dateAndRefNoHTML += '<div id="txnPDFUDF">';
		if (txn.getUdfObjectArray()) {
			for (var i = 0; i < txn.getUdfObjectArray().length; i++) {
				var value = txn.getUdfObjectArray()[i];
				var fieldModel = udfCache.getUdfModelByFirmAndFieldId(txn.getFirmId(), value.getUdfFieldId());
				if (fieldModel.getUdfFieldPrintOnInvoice() == 1 && value.getUdfFieldValue() && fieldModel.getUdfTxnType() == txn.getTxnType() && value.getUdfRefId() == txn.getTxnId()) {
					var displayValue = value.getDisplayValue(fieldModel);
					dateAndRefNoHTML += "<p id='extra" + fieldModel.getUdfFieldNumber() + 1 + "' class='extra' align='right'  style='color:" + refTextColor + "'><span id='extraName" + fieldModel.getUdfFieldNumber() + 1 + "'>" + fieldModel.getUdfFieldName() + ": </span><span id='extraValue" + fieldModel.getUdfFieldNumber() + 1 + "'>" + displayValue + '</span></p>';
				}
			}
		}
		dateAndRefNoHTML += '</div>';
		if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
			if (txn.getTxnReturnRefNumber()) {
				dateAndRefNoHTML += "<p align='right'  style='color:" + refTextColor + "'>";
				if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
					dateAndRefNoHTML += 'Invoice No.: ';
				} else {
					dateAndRefNoHTML += 'Bill No.: ';
				}
				dateAndRefNoHTML += txn.getTxnReturnRefNumber() + '</p>';
			}
			if (txn.getTxnReturnDate()) {
				dateAndRefNoHTML += "<p align='right'  style='color:" + refTextColor + "'>";
				if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
					dateAndRefNoHTML += 'Invoice date: ';
				} else {
					dateAndRefNoHTML += 'Bill date: ';
				}
				dateAndRefNoHTML += MyDate.getDate('d-m-y', txn.getTxnReturnDate()) + '</p>';
			}
		}

		if (txn.getPODate()) {
			dateAndRefNoHTML += "<p align='right'  style='color:" + refTextColor + "'>PO date: " + MyDate.getDate('d-m-y', txn.getPODate()) + '</p>';
		}
		if (txn.getPONumber()) {
			dateAndRefNoHTML += "<p align='right'  style='color:" + refTextColor + "'>PO number: " + txn.getPONumber() + '</p>';
		}
		if (txn.getEwayBillNumber()) {
			dateAndRefNoHTML += "<p align='right'  style='color:" + refTextColor + "'>E-way Bill number: " + txn.getEwayBillNumber() + '</p>';
		}

		if (settingCache.getReverseChargeEnabled() && (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN)) {
			dateAndRefNoHTML += "<p align='right'  style='color:" + refTextColor + "'>Reverse charge: " + (Number(txn.getReverseCharge()) ? 'Yes' : 'No') + '</p>';
		}

		var partyDetailsBorderClass = '';
		if (theme == InvoiceTheme.THEME_5) {
			partyDetailsBorderClass = ' borderBottomForTxn borderTopForTxn borderRightForTxn borderLeftForTxn borderColorGrey ';
		}

		var billToColumnWidth = 1;
		var shippingColumnWidth = 0;
		var transportationColumnWidth = 0;
		var billColumnWidth = 1;

		if (shippingAddressHtml) {
			shippingColumnWidth = 1;
		}
		if (transportationDetails) {
			transportationColumnWidth = 1;
			billColumnWidth = 0.7;
		}

		var totalColumnWidth = billToColumnWidth + shippingColumnWidth + transportationColumnWidth + billColumnWidth;
		var tablePaddingStyle = '';
		if (theme == InvoiceTheme.THEME_10) {
			tablePaddingStyle = " class='theme10SectionPadding' ";
		}
		var htmlText = "<table width='100%' " + tablePaddingStyle + " ><tr><td width='" + 100 * billToColumnWidth / totalColumnWidth + "%' valign='top' style='padding:0px' class='" + partyDetailsBorderClass + "' >" + partyDetailHTML + '</td>';

		if (shippingAddressHtml) {
			htmlText += "<td width='" + 100 * shippingColumnWidth / totalColumnWidth + "%' valign='top' style='padding:0px' class='" + partyDetailsBorderClass + "' >" + shippingAddressHtml + '</td>';
		}
		if (transportationDetails) {
			htmlText += "<td width='" + 100 * transportationColumnWidth / totalColumnWidth + "%' valign='top' style='padding:0px' class='" + partyDetailsBorderClass + "' >" + transportationDetails + '</td>';
		}

		htmlText += "<td width='" + 100 * billColumnWidth / totalColumnWidth + "%' align='right'  style=' vertical-align: center; ' class='" + partyDetailsBorderClass + "'>" + dateAndRefNoHTML + '</td></tr></table>';

		return htmlText;
	};

	this.initQRCode = function (txn, firm) {
		var qrCode = '';
		if (!settingCache) {
			settingCache = new SettingCache();
		}
		if (settingCache.getIsPrintQRCodeEnabled()) {
			if (firm != null) {
				var upiAddress = this.getUPIAddress(firm, txn);
				qrCode = upiAddress ? this.generateQRCodeForPayment(upiAddress) : '';
			}
		}
		return qrCode;
	};

	this.getFullTransactionHTML = function (txn, perviewAsDeliveryChallan) {
		if (!settingCache) {
			settingCache = new SettingCache();
		}
		var themeColor = settingCache.getTxnPDFThemeColor();
		var theme = settingCache.getTxnPDFTheme();
		return this.getTransactionHTMLLayout(txn, theme, themeColor, perviewAsDeliveryChallan);
	};

	this.getTransactionHTML = function (transactionId, perviewAsDeliveryChallan) {
		var baseTransaction = new BaseTransaction();
		var txn = baseTransaction.getTransactionFromId(transactionId);
		return this.getFullTransactionHTML(txn, perviewAsDeliveryChallan);
	};

	this.getTransactionListHTML = function (txnIdsList) {
		if (!settingCache) {
			settingCache = new SettingCache();
		}

		var themeColor = settingCache.getTxnPDFThemeColor();
		var theme = settingCache.getTxnPDFTheme();

		var pageSize = settingCache.getPrintPaperSize();
		var textSizeSetting = settingCache.getPrintTextSize();

		var textSizeRatio = 0.4 + 0.1 * textSizeSetting;
		if (theme == InvoiceTheme.THEME_5 || theme == InvoiceTheme.THEME_6 || theme == InvoiceTheme.THEME_7 || theme == InvoiceTheme.THEME_8) {
			textSizeRatio = textSizeRatio * 0.8;
		}
		var bodyHtml = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + this.getStyleSheet(pageSize, textSizeRatio) + '</head> <body>';

		for (var i = 0; i < txnIdsList.length; i++) {
			var transactionId = txnIdsList[i];
			var baseTransaction = new BaseTransaction();
			var txn = baseTransaction.getTransactionFromId(transactionId);
			bodyHtml += this.getTransactionHTMLBody(txn, theme, themeColor, textSizeRatio, true);
		}

		bodyHtml += '</body></html>';

		return bodyHtml;
	};

	this.getStyleSheet = function (pageSize, sizeRatio) {
		var style = '<style> ';
		if (pageSize && pageSize == TransactionPDFPaperSize.A5) {
			style += '@page {size:A5;}';
		}

		if (!settingCache) {
			settingCache = new SettingCache();
		}

		var companyNameSizeSetting = settingCache.getCompanyNameHeaderPrintTextSize();
		var companyNameSizeRatio = 0.6 + 0.1 * companyNameSizeSetting;

		if (!sizeRatio) {
			sizeRatio = 1;
		}

		var textSize = 16 * sizeRatio;

		style += ' @media print {body { -webkit-print-color-adjust: exact;}}' + '.pdfTransactionHTMLView .marginBottom0 { margin-bottom: 0px !important;}' + '.pdfTransactionHTMLView .ackPdfMarginBottom7 {margin-bottom: 7px !important;}' + '.pdfTransactionHTMLView .textUppercase {text-transform: uppercase;}' + '.txnItemTableForPrint table { page-break-after:auto } .txnItemTableForPrint tr { page-break-inside:avoid; page-break-after:auto } .txnItemTableForPrint td    { page-break-inside:avoid; page-break-after:auto } .txnItemTableForPrint thead { display:table-header-group }' + '.pdfTransactionHTMLView table { border-collapse: collapse; } ' + '.pdfTransactionHTMLView td,.pdfTransactionHTMLView th { padding: ' + 5 * sizeRatio + 'px; font-size: ' + textSize + 'px;}' + '.pdfTransactionHTMLView .borderBottomForTxn { border-bottom: 1px solid; border-color: gray;}' + '.pdfTransactionHTMLView .borderTopForTxn { border-top: 1px solid; border-color: gray;}' + '.pdfTransactionHTMLView .borderLeftForTxn { border-left: 1px solid; border-color: gray;}' + '.pdfTransactionHTMLView .borderRightForTxn { border-right: 1px solid; border-color: gray;}' + '.pdfTransactionHTMLView .borderColorGrey { border-color: gray;}' + '.pdfTransactionHTMLView p { margin: 0; padding: ' + 2 * sizeRatio + 'px; font-size: ' + textSize + 'px;}' + '.pdfTransactionHTMLView .paddingLeft { padding-left: ' + 5 * sizeRatio + 'px !important;}' + '.pdfTransactionHTMLView .paddingRight { padding-right: ' + 5 * sizeRatio + 'px !important;}' + '.pdfTransactionHTMLView .discountTaxTable{ border-bottom: 0px; border-color: white;}' + '.pdfTransactionHTMLView .boldText {font-weight: bold;}' + '.pdfTransactionHTMLView .copyPrintNumberClass { padding: ' + 2.5 * sizeRatio + 'px; padding-right:0px; font-size: ' + 10 * sizeRatio + 'px;}' + '.copyPrintNumberCheckBoxClass { padding: 0px ' + 4 * sizeRatio + 'px !important; font-size: ' + 15 * sizeRatio + 'px; vertical-align: center;}' + '.pdfTransactionHTMLView .normalTextSize {font-size: ' + 13 * sizeRatio + 'px;}' + '.pdfTransactionHTMLView .bigTextSize {font-size: ' + 15 * sizeRatio + 'px;}' + '.pdfTransactionHTMLView .largerTextSize {font-size: ' + 18 * sizeRatio + 'px;}' + '.pdfTransactionHTMLView .biggerTextSize {font-size: ' + 21 * sizeRatio + 'px;}' + '.pdfTransactionHTMLView .extraLargeTextSize {font-size: ' + 24 * sizeRatio + 'px;}' + '.pdfTransactionHTMLView .companyNameHeaderTextSize {font-size: ' + 24 * sizeRatio * companyNameSizeRatio + 'px;}' + '.pdfTransactionHTMLView .noTopPadding { padding-top: 0px}' + '.pdfTransactionHTMLView .noBottomPadding { padding-bottom: 0px}' + '.pdfTransactionHTMLView .noPadding { padding:0px !important;}' + '.pdfTransactionHTMLView .topPadding{ padding-top: ' + 5 * sizeRatio + 'px;}' + '.pdfTransactionHTMLView .partyAddressPadding { margin: 0; padding: ' + 3 * sizeRatio + 'px ' + 50 * sizeRatio + 'px;}' + '.pdfTransactionHTMLView .vAlignTop {vertical-align: top;}' + '.pdfTransactionHTMLView .vAlignBottom {vertical-align: bottom;}' + '.pdfTransactionHTMLView{ font-family: Segoe, "Segoe UI", Calibri, Candara, Optima, Arial, sans-serif}' + '.page-break {page-break-after: always; }' + '.theme10ItemTablePadding td, .theme10ItemTablePadding th { padding: ' + 8 * sizeRatio + 'px ' + 5 * sizeRatio + 'px;}' + '.theme10BoxStyle {background-color: #F7F7F7 !important; color: #747474 !important;} ' + '.theme10BoxHeading { color: #747474 !important;}' + '.theme10CopyCountTablePadding {margin-bottom: ' + 10 * sizeRatio + 'px;}' + '.theme10TxnHeaderTextSize {font-size: ' + 27 * sizeRatio + 'px !important;}' + '.theme10SectionPadding {margin-bottom: ' + 18 * sizeRatio + 'px !important;}' + '.theme10amountSection {margin-bottom: ' + 10 * sizeRatio + 'px !important;}' + '.theme10BorderColor {border-color: #DDDDDD !important;}' + '.pdfTransactionHTMLView button {  font-size: ' + textSize + 'px;}' + '.pdfTransactionHTMLView .pay-online-upi {cursor:default;clear:both;float:left;background-color: #28DB89;padding: 1px;}' + '.pdfTransactionHTMLView .pay-online-upi .upi-image {display: inline-block;padding: 2px;background-color:white;}' + '.pdfTransactionHTMLView .pay-online-upi .pay-now-text {border-radius: 0 2px 2px 0;display:inline-block;padding: 3px 2px 3px 2px;color:white;}' + '.theme11TxnHeaderTextSize {font-size: 35px;}' + '.theme11TxnH2HeaderTextSize {font-size: 30px;}' + '.theme12ItemTable {border: 1px solid gainsboro !important;}' + '.theme12ItemTable th {border: 1px solid gainsboro !important;}' + '.theme12ItemTable td {border: 1px solid gainsboro !important;}' + '.theme12ItemTable tr:first-child td:first-child { border: none !important; }' + '.premiumThemeHeader {font-size: ' + 32 * sizeRatio * companyNameSizeRatio + 'px !important;}' + '.theme11ItemTable th:first-child { border: 0px solid white !important; border-radius: 12px 0 0 0 !important;}' + '.theme11ItemTable th:last-child { border: 0px solid white !important; border-radius: 0 12px 0 0 !important;}' + '.theme11ItemTable {border-radius: 12px 12px 0px 12px !important;}' + '.theme11ItemTable th {border: 1px solid gainsboro !important;}' + '.theme11ItemTable td {border: 1px solid gainsboro !important;}' + '.theme11ItemTable tr {page-break-inside: avoid !important;}' + '.theme11lastRow td:first-child {border: 0px solid white !important}' + '.theme11textUppercase {text-transform: uppercase;}' + '.maxlines{display: block;text-overflow: ellipsis;word-wrap: break-word;overflow: hidden;max-height: 3.6em;line-height: 1.8em;}' + '.maxlinestheme11{display: block;text-overflow: ellipsis;word-wrap: break-word;overflow: hidden;max-height: 2.8em;line-height: 1.3em;}' + '.brandingDummyFooterForSpacing {height:100px} ' + '.brandingFooter {position: fixed; bottom: 0;} ' + '@media screen { .hideOnScreen { display:none; }} ' + '</style>';
		return style;
	};

	this.isNonTaxBill = function (txn) {
		try {
			var txnTaxRate = txn.getTaxPercent();
			if (txnTaxRate > 0) {
				return false;
			}

			var txnLineItems = txn.getLineItems();

			if (txnLineItems && txnLineItems.length > 0) {
				for (var i = 0; i < txnLineItems.length; i++) {
					var currentLineItem = txnLineItems[i];
					if (currentLineItem.getLineItemTaxPercent() > 0) {
						return false;
					}
				}
			}
		} catch (err) {
			logger.error('Checking if bill is non tax ' + err);
			return false;
		}
		return true;
	};

	this.getTransactionHeader = function (txn, theme, themeColor, textSizeRatio, printDeliveryChallan) {
		var styleText = '';

		if (themeColor == '#ffffff') {
			themeColor = 'black';
		}

		if (theme == InvoiceTheme.THEME_1 || theme == InvoiceTheme.THEME_3 || theme == InvoiceTheme.THEME_10) {
			styleText += 'color: ' + themeColor + ';';
		}
		if (theme == InvoiceTheme.THEME_1) {
			styleText += 'border-bottom: ' + 3 * textSizeRatio + 'px solid; border-top: ' + 3 * textSizeRatio + 'px solid; border-color:' + themeColor + ';';
		}
		if (theme == InvoiceTheme.THEME_2) {
			styleText += 'border-top: ' + 3 * textSizeRatio + 'px solid;';
		}

		var transactionFactory = new TransactionFactory();

		var marginBottom = 15 * textSizeRatio;

		var transactionString = transactionFactory.getTransTypeStringForTransactionPDF(txn.getTxnType(), txn.getTxnSubType(), printDeliveryChallan, this.isNonTaxBill(txn));

		if (theme == InvoiceTheme.THEME_4) {
			marginBottom = 0;
			transactionString = '<u>' + transactionString + '</u>';
		} else if (theme == InvoiceTheme.THEME_5) {
			marginBottom = 0;
		}

		var copyNumberHTML = '';
		if (settingCache.printCopyNumber() && (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN)) {
			var copyNumberColumnStyle = ''; // "border-bottom: 1px solid; border-top: 1px solid; border-left: 1px solid; border-right: 1px solid;";
			var classForCheckBox = ' borderLeftForTxn borderRightForTxn borderBottomForTxn borderTopForTxn copyPrintNumberCheckBoxClass';
			var styleForCheckBox = 'border-color: gray;  color:#fffffe;';
			if (theme == InvoiceTheme.THEME_1 || theme == InvoiceTheme.THEME_3) {
				copyNumberColumnStyle += 'color: ' + themeColor + ';';
				styleForCheckBox = 'border-color: ' + themeColor + ';  color:#fffffe;';
			}
			copyNumberHTML = "<table width='100%'>" + "<tr><td width='95%' align=\"right\" style='" + copyNumberColumnStyle + "' class='copyPrintNumberClass'>Original for Recipient </td><td style='padding:1px;' align='left'><table><tr><td class='" + classForCheckBox + "' style='" + styleForCheckBox + "'>O</td></tr></table></td></tr>" + "<tr><td width='95%' align=\"right\" style='" + copyNumberColumnStyle + "'  class='copyPrintNumberClass'>Duplicate for Transporter</td><td style='padding:1px;' align='left'><table><tr><td class='" + classForCheckBox + "' style='" + styleForCheckBox + "'>O</td></tr></table></td></tr>" + "<tr><td width='95%' align=\"right\" style='" + copyNumberColumnStyle + "'  class='copyPrintNumberClass'>Triplicate for Supplier </td><td style='padding:1px;' align='left'><table><tr><td class='" + classForCheckBox + "' style='" + styleForCheckBox + "'>O</td></tr></table></td></tr></table>";
		}

		var headerStyle = " class='biggerTextSize boldText' ";

		var txnHeaderText = '<table width="100%" style="margin-bottom:' + marginBottom + "px;\"> <tr> <td width='33%' " + headerStyle + " style=' vertical-align: center; " + styleText + "'> </td> <td width='34%' align='center' " + headerStyle + " style='" + styleText + "'>" + transactionString + "</td><td width='33%' align='right' style=' padding:0px; " + styleText + "'>" + copyNumberHTML + '</td></tr></table>';

		return txnHeaderText;
	};

	this.getUdfHeaderDetails = function (firm) {
		var alignment = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'center';
		var theme = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;

		var headerDetails = '<div id = "uDFHeaderFields">';
		var udfCache = new UDFCache();
		var udfArray = firm.getUdfObjectArray();
		for (var i = 0; i < udfArray.length; i++) {
			var udfValue = udfArray[i];
			var udfModel = udfCache.getUdfFirmModelByFirmAndFieldId(firm.getFirmId(), udfValue.getUdfFieldId());
			if (udfModel.getUdfFieldPrintOnInvoice() == 1 && udfModel.getUdfFieldStatus() == 1) {
				var udfFieldValue = udfValue.getUdfFieldValue() || '';
				if (theme == InvoiceTheme.THEME_9) {
					headerDetails += "<p class='bigTextSize id='extraHeader" + udfModel.getUdfFieldNumber() + "'><span id='extraHeaderName" + udfModel.getUdfFieldNumber() + "'>" + udfModel.getUdfFieldName() + ": </span> <span id='extraHeaderValue" + udfModel.getUdfFieldNumber() + "'>" + udfFieldValue + '</span></p>';
				} else {
					headerDetails += '<p align=' + alignment + " class='bigTextSize' id='extraHeader" + udfModel.getUdfFieldNumber() + "'><span id='extraHeaderName" + udfModel.getUdfFieldNumber() + "'>" + udfModel.getUdfFieldName() + ": </span> <span id='extraHeaderValue" + udfModel.getUdfFieldNumber() + "'>" + udfFieldValue + '</span></p>';
				}
			}
		}
		headerDetails += '</div>';
		return headerDetails;
	};

	this.getCompanyHeader = function (firmId, theme, themeColor, textSizeRatio) {
		if (!settingCache) {
			settingCache = new SettingCache();
		}
		var headerDetails = '';

		var firm = Number(firmId) ? firmCache.getFirmById(firmId) : firmCache.getDefaultFirm();

		var backgroundColor = '#ffffff';
		var textColor = '#000000';
		if (theme == InvoiceTheme.THEME_3) {
			backgroundColor = themeColor;
			if (themeColor != '#ffffff') {
				textColor = '#ffffff';
			}
		}

		var outerStyle = 'padding-bottom:' + 10 * textSizeRatio + 'px;';

		var companyAddress = firm.getFirmAddress().trim();
		companyAddress = companyAddress.replace(/(?:\r\n|\r|\n)/g, '<br/>');

		headerDetails = "<div style='" + outerStyle + "'><div style='background-color: " + backgroundColor + '; color: ' + textColor + ";'>";

		var imagePath = this.getCompanyLogoSource(firm, this.isForInvoicePreview);

		var companyDetailsHTML = '';
		var imageDetailsHTML = '';

		if (imagePath) {
			if (firm.getFirmName() && settingCache.isPrintCompanyNameEnabled()) {
				companyDetailsHTML += "<p align='right' id='headerFirmName' class='companyNameHeaderTextSize boldText'>" + firm.getFirmName() + '</p>';
			}

			if (companyAddress && settingCache.isPrintCompanyAddressEnabled()) {
				companyDetailsHTML += "<p align='right' class='bigTextSize'>" + companyAddress + '</p>';
			}

			var companyPhoneToBePrinted = firm.getFirmPhone() && settingCache.isPrintCompanyNumberEnabled();
			var companyEmailToBePrinted = firm.getFirmEmail() && settingCache.isCompnayEmailEnabledOnPrint();

			if (companyPhoneToBePrinted || companyEmailToBePrinted) {
				companyDetailsHTML += "<p align='right' class='bigTextSize'>";
				if (companyPhoneToBePrinted) {
					companyDetailsHTML += 'Phone no.: ' + firm.getFirmPhone();
				}
				if (companyEmailToBePrinted) {
					companyDetailsHTML += ' Email: ' + firm.getFirmEmail();
				}
				companyDetailsHTML += '</p>';
			}
			if (settingCache.isPrintTINEnabled()) {
				if (settingCache.getGSTEnabled()) {
					var companyGSTINToBePrinted = firm.getFirmHSNSAC();
					var companyStateToBePrinted = firm.getFirmState();

					if (companyGSTINToBePrinted || companyStateToBePrinted) {
						companyDetailsHTML += "<p align='right' class='bigTextSize'>";
						if (companyGSTINToBePrinted) {
							companyDetailsHTML += 'GSTIN: ' + firm.getFirmHSNSAC();
						}
						if (companyStateToBePrinted) {
							if (companyGSTINToBePrinted) {
								companyDetailsHTML += ', ';
							}
							companyDetailsHTML += 'State: ' + StateCode.getStateCode(firm.getFirmState()) + '-' + firm.getFirmState();
						}
						companyDetailsHTML += '</p>';
					}
				} else {
					if (firm.getFirmTin()) {
						companyDetailsHTML += '<p align="right" class="bigTextSize">' + settingCache.getTINText() + ': ' + firm.getFirmTin() + '</p>';
					}
				}
			}

			companyDetailsHTML += this.getUdfHeaderDetails(firm, 'right');

			imageDetailsHTML = "<img src='" + imagePath + "' style='height:" + 84 * textSizeRatio + "px;'></img>";

			if (this.isForInvoicePreview) {
				var classTextForEditBusinessDetails = 'class=\'invoicePreviewEditSection ' + this.getEditSectionClassName('firmDetails') + '\'';
				var classTextForEditLogo = 'class=\'invoicePreviewEditSection ' + this.getEditSectionClassName('firmLogo') + '\'';
				if (companyDetailsHTML) {
					companyDetailsHTML = '<div style="width:100%" editSection=\'firmDetails\' ' + classTextForEditBusinessDetails + ' > ' + companyDetailsHTML + ' </div>';
				}
				if (imageDetailsHTML) {
					imageDetailsHTML = '<div editSection=\'firmLogo\' ' + classTextForEditLogo + ' > ' + imageDetailsHTML + ' </div>';
				}
			}

			headerDetails += "<table width='100%' style='color: " + textColor + ";'> <tr>";

			headerDetails += "<td width='40%' style='vertical-align: center;'>" + imageDetailsHTML + '</td>';

			headerDetails += "<td width='60%' style='vertical-align: center;'>" + companyDetailsHTML + '</td></tr></table>';
		} else {
			if (firm.getFirmName() && settingCache.isPrintCompanyNameEnabled()) {
				companyDetailsHTML += "<p align='center' id='headerFirmName' class='companyNameHeaderTextSize boldText'>" + firm.getFirmName() + '</p>';
			}
			var companyAddressToBePrinted = settingCache.isPrintCompanyAddressEnabled() && companyAddress;
			var _companyPhoneToBePrinted = firm.getFirmPhone() && settingCache.isPrintCompanyNumberEnabled();
			var _companyEmailToBePrinted = firm.getFirmEmail() && settingCache.isCompnayEmailEnabledOnPrint();
			if (companyAddressToBePrinted || _companyPhoneToBePrinted || _companyEmailToBePrinted) {
				companyDetailsHTML += "<p align='center' class='bigTextSize partyAddressPadding'>";
				if (companyAddressToBePrinted && _companyPhoneToBePrinted) {
					companyDetailsHTML += companyAddress + ', Ph. no.: ' + firm.getFirmPhone();
				} else if (companyAddressToBePrinted) {
					companyDetailsHTML += firm.getFirmAddress();
				} else if (_companyPhoneToBePrinted) {
					companyDetailsHTML += 'Phone no.: ' + firm.getFirmPhone();
				}
				if (_companyEmailToBePrinted) {
					companyDetailsHTML += ' Email: ' + firm.getFirmEmail();
				}
				companyDetailsHTML += '</p>';
			}

			if (settingCache.isPrintTINEnabled()) {
				if (settingCache.getGSTEnabled()) {
					var _companyGSTINToBePrinted = firm.getFirmHSNSAC();
					var _companyStateToBePrinted = firm.getFirmState();

					if (_companyGSTINToBePrinted || _companyStateToBePrinted) {
						companyDetailsHTML += '<p align="center" class="bigTextSize partyAddressPadding">';
						if (_companyGSTINToBePrinted) {
							companyDetailsHTML += 'GSTIN: ' + firm.getFirmHSNSAC();
						}
						if (_companyStateToBePrinted) {
							if (_companyGSTINToBePrinted) {
								companyDetailsHTML += ', ';
							}
							companyDetailsHTML += 'State: ' + StateCode.getStateCode(firm.getFirmState()) + '-' + firm.getFirmState();
						}
						companyDetailsHTML += '</p>';
					}
				} else {
					if (firm.getFirmTin()) {
						companyDetailsHTML += '<p align="center" class="bigTextSize partyAddressPadding">' + settingCache.getTINText() + ': ' + firm.getFirmTin() + '</p>';
					}
				}
			}

			companyDetailsHTML += this.getUdfHeaderDetails(firm);

			if (this.isForInvoicePreview) {
				var _classTextForEditBusinessDetails = 'class=\'invoicePreviewEditSection ' + this.getEditSectionClassName('firmDetails') + '\'';
				if (companyDetailsHTML) {
					companyDetailsHTML = '<div style="width:100%" editSection=\'firmDetails\' ' + _classTextForEditBusinessDetails + ' > ' + companyDetailsHTML + ' </div>';
				}
			}
			headerDetails += companyDetailsHTML;
		}
		headerDetails += '</div></div>';

		return headerDetails;
	};

	this.getCompanyHeaderForTheme5 = function (firmId, theme, textSizeRatio) {
		var firm = Number(firmId) ? firmCache.getFirmById(firmId) : firmCache.getDefaultFirm();

		var companyDetailsHTML = '';
		var imageDetailsHTML = '';

		var imagePath = this.getCompanyLogoSource(firm, this.isForInvoicePreview);

		var textColor = '#000000';

		if (imagePath) {
			imageDetailsHTML += "<img src='" + imagePath + "' style='height: " + 84 * textSizeRatio + "px;'></img>";
		}

		var companyAddress = firm.getFirmAddress().trim();
		companyAddress = companyAddress.replace(/(?:\r\n|\r|\n)/g, '<br/>');

		var alignProperty = " align='center' ";
		var alignment = 'center';

		if (imagePath) {
			alignProperty = " align='right' ";
			alignment = 'right';
		}

		if (firm.getFirmName() && settingCache.isPrintCompanyNameEnabled()) {
			companyDetailsHTML += '<p ' + alignProperty + " class='companyNameHeaderTextSize boldText'>" + firm.getFirmName() + '</p>';
		}

		if (companyAddress && settingCache.isPrintCompanyAddressEnabled()) {
			companyDetailsHTML += '<p ' + alignProperty + " class='bigTextSize'>" + companyAddress + '</p>';
		}

		var companyPhoneToBePrinted = firm.getFirmPhone() && settingCache.isPrintCompanyNumberEnabled();
		var companyEmailToBePrinted = firm.getFirmEmail() && settingCache.isCompnayEmailEnabledOnPrint();

		if (companyPhoneToBePrinted || companyEmailToBePrinted) {
			companyDetailsHTML += '<p ' + alignProperty + " class='bigTextSize'>";
			if (companyPhoneToBePrinted) {
				companyDetailsHTML += 'Phone no.: ' + firm.getFirmPhone();
			}
			if (companyEmailToBePrinted) {
				companyDetailsHTML += ' Email: ' + firm.getFirmEmail();
			}
			companyDetailsHTML += '</p>';
		}
		if (settingCache.isPrintTINEnabled()) {
			if (settingCache.getGSTEnabled()) {
				var companyGSTINToBePrinted = firm.getFirmHSNSAC();
				var companyStateToBePrinted = firm.getFirmState();

				if (companyGSTINToBePrinted || companyStateToBePrinted) {
					companyDetailsHTML += '<p ' + alignProperty + " class='bigTextSize'>";
					if (companyGSTINToBePrinted) {
						companyDetailsHTML += 'GSTIN: ' + firm.getFirmHSNSAC();
					}
					if (companyStateToBePrinted) {
						if (companyGSTINToBePrinted) {
							companyDetailsHTML += ', ';
						}
						companyDetailsHTML += 'State: ' + StateCode.getStateCode(firm.getFirmState()) + '-' + firm.getFirmState();
					}
					companyDetailsHTML += '</p>';
				}
			} else {
				if (firm.getFirmTin()) {
					companyDetailsHTML += '<p ' + alignProperty + " class='bigTextSize'>" + settingCache.getTINText() + ': ' + firm.getFirmTin() + '</p>';
				}
			}
		}
		companyDetailsHTML += this.getUdfHeaderDetails(firm, alignment);

		var headerDetails = "<div><div style=' color: " + textColor + ";'>";

		if (this.isForInvoicePreview) {
			var classTextForEditBusinessDetails = 'class=\'invoicePreviewEditSection ' + this.getEditSectionClassName('firmDetails') + '\'';
			var classTextForEditLogo = 'class=\'invoicePreviewEditSection ' + this.getEditSectionClassName('firmLogo') + '\'';
			if (companyDetailsHTML) {
				companyDetailsHTML = '<div ' + alignProperty + ' style="width:100%" editSection=\'firmDetails\' ' + classTextForEditBusinessDetails + ' > ' + companyDetailsHTML + ' </div>';
			}
			if (imageDetailsHTML) {
				imageDetailsHTML = '<div editSection=\'firmLogo\' ' + classTextForEditLogo + ' > ' + imageDetailsHTML + ' </div>';
			}
		}

		headerDetails += '<table width="100%" style="color: ' + textColor + ';" > <tr>';

		if (imagePath) {
			headerDetails += "<td width='35%' style='vertical-align: center;' class='borderTopForTxn  borderLeftForTxn borderColorGrey'>" + imageDetailsHTML;
			headerDetails += '</td><td width=\'65%\' ' + alignProperty + '  style=\'vertical-align: center\' class=\'borderTopForTxn  borderRightForTxn borderColorGrey\'>';
			headerDetails += companyDetailsHTML;
		} else {
			headerDetails += "<td width='100%' style='vertical-align: center' class='borderTopForTxn borderLeftForTxn  borderRightForTxn borderColorGrey'>";
			headerDetails += companyDetailsHTML;
		}

		headerDetails += '</td></tr></table></div></div>';

		return headerDetails;
	};

	this.getOldCompanyHeader = function (txn, textSizeRatio) {
		var headerDetails = '';

		var firm = firmCache.getTransactionFirmWithDefault(txn);

		if (firm.getFirmName() && settingCache.isPrintCompanyNameEnabled()) {
			headerDetails += "<p align='center' class='companyNameHeaderTextSize boldText'>" + firm.getFirmName() + '</p>';
		}

		var isPrintAddress = firm.getFirmAddress() && settingCache.isPrintCompanyAddressEnabled();
		var isPrintNumber = firm.getFirmPhone() && settingCache.isPrintCompanyNumberEnabled();
		var companyEmailToBePrinted = firm.getFirmEmail() && settingCache.isCompnayEmailEnabledOnPrint();

		if (isPrintAddress || isPrintNumber || companyEmailToBePrinted) {
			headerDetails += "<p align='center' class='bigTextSize partyAddressPadding'>";
			if (isPrintAddress && isPrintNumber) {
				headerDetails += 'Address: ' + firm.getFirmAddress() + ', Ph. no.: ' + firm.getFirmPhone();
			} else if (isPrintAddress) {
				headerDetails += 'Address: ' + firm.getFirmAddress();
			} else if (isPrintNumber) {
				headerDetails += 'Phone no.: ' + firm.getFirmPhone();
			}
			if (companyEmailToBePrinted) {
				headerDetails += ' Email: ' + firm.getFirmEmail();
			}
			headerDetails += '</p>';
		}

		if (settingCache.isPrintTINEnabled()) {
			if (settingCache.getGSTEnabled()) {
				var companyGSTINToBePrinted = firm.getFirmHSNSAC();
				var companyStateToBePrinted = firm.getFirmState();

				if (companyGSTINToBePrinted || companyStateToBePrinted) {
					headerDetails += "<p align='center' class='bigTextSize partyAddressPadding'>";
					if (companyGSTINToBePrinted) {
						headerDetails += 'GSTIN: ' + firm.getFirmHSNSAC();
					}
					if (companyStateToBePrinted) {
						if (companyGSTINToBePrinted) {
							headerDetails += ', ';
						}
						headerDetails += 'State: ' + StateCode.getStateCode(firm.getFirmState()) + '-' + firm.getFirmState();
					}
					headerDetails += '</p>';
				}
			} else {
				if (firm.getFirmTin()) {
					headerDetails += "<p align='center' class='bigTextSize partyAddressPadding'>" + settingCache.getTINText() + ': ' + firm.getFirmTin() + '</p>';
				}
			}
		}
		headerDetails += this.getUdfHeaderDetails(firm);

		if (this.isForInvoicePreview) {
			var classTextForEditBusinessDetails = 'class=\'invoicePreviewEditSection ' + this.getEditSectionClassName('firmDetails') + '\'';
			if (headerDetails) {
				headerDetails = '<div style="width:100%" editSection=\'firmDetails\' ' + classTextForEditBusinessDetails + ' > ' + headerDetails + ' </div>';
			}
		}

		return headerDetails;
	};

	this.getItemTableHeaderUsingPrintSetting = function (theme, themeColor, printSIColumn, printItemName, printItemQuantity, printItemPrice, printFinalItemPriceColumn, printTotalAmountColumn, printItemisedDiscountColumn, isHSNCodeEnabled, isItemCodeEnabled, printItemUnitColumn, printItemBatchColumn, printItemCountColumn, printItemExpiryDateColumn, printItemManufactureDateColumn, printItemMRPColumn, printSize, printTaxableItemPrice, printTotalTaxAmountColumn, printTaxableAmountColumn, printIGSTTaxColumn, printCGSTTaxColumn, printSGSTTaxColumn, printCESSTaxColumn, printOtherTaxColumn, printAdditionalCESSColumn, siColumnRatio, itemNameColumnRatio, hsnCodeColumnRatio, itemCodeColumnRatio, itemCountColumnRatio, itemBatchColumnRatio, itemExpDateColumnRatio, itemManufacturingDateColumnRatio, itemMRPColumnRatio, itemSizeColumnRatio, quantityColumnRatio, itemUnitColumnRatio, pricePerUnitColumnRatio, discountColumnRatio, taxableItemPriceRatio, taxTotalAmountColumnRatio, taxableAmountColumnRatio, igstColumnRatio, cgstColumnRatio, sgstColumnRatio, cessColumnRatio, otherTaxColumnRatio, additionCESSColumnRatio, finalItemPriceColumnRatio, totalAmountColumnRatio, siColumnHeader, itemNameColumnHeader, hsnCodeColumnHeader, itemCodeColumnHeader, pricePerUnitColumnHeader, quantityColumnHeader, itemUnitColumnHeader, discountColumnHeader, taxableItemPriceHeader, taxTotalAmountColumnHeader, taxableAmountColumnHeader, igstColumnHeader, cgstColumnHeader, sgstColumnHeader, cessColumnHeader, otherTaxColumnHeader, additionCESSColumnHeader, finalItemPriceColumnHeader, totalAmountColumnHeader, printStateSpecificCESSTaxColumn, stateSpecificTaxColumnHeader) {
		var stateSpecificCESSColumnRatio = arguments.length > 73 && arguments[73] !== undefined ? arguments[73] : 0;

		if (!settingCache) {
			settingCache = new SettingCache();
		}

		var totalColumnRatio = siColumnRatio + itemNameColumnRatio + hsnCodeColumnRatio + itemCodeColumnRatio + itemCountColumnRatio + itemBatchColumnRatio + itemExpDateColumnRatio + itemManufacturingDateColumnRatio + itemMRPColumnRatio + itemSizeColumnRatio + quantityColumnRatio + itemUnitColumnRatio + pricePerUnitColumnRatio + discountColumnRatio + taxableItemPriceRatio + taxTotalAmountColumnRatio + taxableAmountColumnRatio + igstColumnRatio + cgstColumnRatio + sgstColumnRatio + cessColumnRatio + stateSpecificCESSColumnRatio + otherTaxColumnRatio + additionCESSColumnRatio + totalAmountColumnRatio + finalItemPriceColumnRatio;

		var styleText = '';

		if (theme == InvoiceTheme.THEME_0) {
			styleText = " style='background-color: " + themeColor + ";' ";
		} else if (theme == InvoiceTheme.THEME_9) {
			styleText = ' ';
		} else {
			var textColorForHeading = 'white';
			if (themeColor == '#ffffff') {
				textColorForHeading = 'black';
			}
			styleText = " style='background-color: " + themeColor + '; color: ' + textColorForHeading + ";' ";
		}

		if (theme == InvoiceTheme.THEME_9) {
			styleText += " class='borderBottomForTxn ";
		} else if (theme == InvoiceTheme.THEME_10) {
			styleText += " class='";
			if (themeColor == '#ffffff') {
				styleText += 'borderBottomForTxn borderTopForTxn ';
			}
		} else {
			styleText += " class='borderBottomForTxn borderTopForTxn ";
		}

		if (theme == InvoiceTheme.THEME_3 || theme == InvoiceTheme.THEME_5 || theme == InvoiceTheme.THEME_6 || theme == InvoiceTheme.THEME_7 || theme == InvoiceTheme.THEME_8 || theme == InvoiceTheme.THEME_9) {
			styleText += '  borderLeftForTxn borderRightForTxn ';
		}

		var header = '<thead><tr>';

		if (theme == InvoiceTheme.THEME_12 || theme == InvoiceTheme.THEME_11) {
			header = '<thead><tr class="">';
		}

		if (theme == InvoiceTheme.THEME_11) {
			styleText += ' theme11borderRadius ';
		}

		styleText += " borderColorGrey'";

		if (printSIColumn) {
			header += '<th ' + styleText + " width='" + siColumnRatio * 100 / totalColumnRatio + "%' align='left'>" + siColumnHeader + '</th>';
		}

		if (printItemName) {
			header += '<th ' + styleText + " width='" + itemNameColumnRatio * 100 / totalColumnRatio + "%' align='left'>" + itemNameColumnHeader + '</th>';
		}

		if (isItemCodeEnabled) {
			header += '<th ' + styleText + " width='" + itemCodeColumnRatio * 100 / totalColumnRatio + "%' align='left'>" + itemCodeColumnHeader + '</th>';
		}

		if (isHSNCodeEnabled) {
			header += '<th ' + styleText + " width='" + hsnCodeColumnRatio * 100 / totalColumnRatio + "%' align='left'>" + hsnCodeColumnHeader + '</th>';
		}

		if (printItemCountColumn) {
			header += '<th ' + styleText + " width='" + itemCountColumnRatio * 100 / totalColumnRatio + "%' align='right'>" + settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_COUNT_VALUE) + '</th>';
		}

		if (printItemBatchColumn) {
			header += '<th ' + styleText + " width='" + itemBatchColumnRatio * 100 / totalColumnRatio + "%' align='left'>" + settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_BATCH_NUMBER_VALUE) + '</th>';
		}

		if (printItemExpiryDateColumn) {
			header += '<th ' + styleText + " width='" + itemExpDateColumnRatio * 100 / totalColumnRatio + "%' align='right'>" + settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_EXPIRY_DATE_VALUE) + '</th>';
		}

		if (printItemManufactureDateColumn) {
			header += '<th ' + styleText + " width='" + itemManufacturingDateColumnRatio * 100 / totalColumnRatio + "%' align='right'>" + settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_MANUFACTURING_DATE_VALUE) + '</th>';
		}

		if (printItemMRPColumn) {
			header += '<th ' + styleText + " width='" + itemMRPColumnRatio * 100 / totalColumnRatio + "%' align='right'>" + settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_MRP_VALUE) + '</th>';
		}

		if (printSize) {
			header += '<th ' + styleText + " width='" + itemSizeColumnRatio * 100 / totalColumnRatio + "%' align='right'>" + settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_SIZE_VALUE) + '</th>';
		}

		if (printItemQuantity) {
			header += '<th ' + styleText + " width='" + quantityColumnRatio * 100 / totalColumnRatio + "%' align='right'>" + quantityColumnHeader + '</th>';
		}

		if (printItemUnitColumn) {
			header += '<th ' + styleText + " width='" + itemUnitColumnRatio * 100 / totalColumnRatio + "%' align='right'>" + itemUnitColumnHeader + '</th>';
		}

		if (printItemPrice) {
			header += '<th ' + styleText + " width='" + pricePerUnitColumnRatio * 100 / totalColumnRatio + "%' align='right'>" + pricePerUnitColumnHeader + '</th>';
		}

		if (printItemisedDiscountColumn) {
			header += '<th ' + styleText + " width='" + discountColumnRatio * 100 / totalColumnRatio + "%' align='right'>" + discountColumnHeader + '</th>';
		}

		if (printTaxableItemPrice) {
			header += '<th ' + styleText + " width='" + taxableItemPriceRatio * 100 / totalColumnRatio + "%' align='right'>" + taxableItemPriceHeader + '</th>';
		}

		if (printTotalTaxAmountColumn) {
			header += '<th ' + styleText + " width='" + taxTotalAmountColumnRatio * 100 / totalColumnRatio + "%' align='right'>" + taxTotalAmountColumnHeader + '</th>';
		}

		if (printTaxableAmountColumn) {
			header += '<th ' + styleText + " width='" + taxableAmountColumnRatio * 100 / totalColumnRatio + "%' align='right'>" + taxableAmountColumnHeader + '</th>';
		}

		if (printIGSTTaxColumn) {
			header += '<th ' + styleText + " width='" + igstColumnRatio * 100 / totalColumnRatio + "%' align='right'>" + igstColumnHeader + '</th>';
		}
		if (printCGSTTaxColumn) {
			header += '<th ' + styleText + " width='" + cgstColumnRatio * 100 / totalColumnRatio + "%' align='right'>" + cgstColumnHeader + '</th>';
		}
		if (printSGSTTaxColumn) {
			header += '<th ' + styleText + " width='" + sgstColumnRatio * 100 / totalColumnRatio + "%' align='right'>" + sgstColumnHeader + '</th>';
		}
		if (printCESSTaxColumn) {
			header += '<th ' + styleText + " width='" + cessColumnRatio * 100 / totalColumnRatio + "%' align='right'>" + cessColumnHeader + '</th>';
		}
		if (printStateSpecificCESSTaxColumn) {
			header += '<th ' + styleText + " width='" + stateSpecificCESSColumnRatio * 100 / totalColumnRatio + "%' align='right'>" + stateSpecificTaxColumnHeader + '</th>';
		}
		if (printOtherTaxColumn) {
			header += '<th ' + styleText + " width='" + otherTaxColumnRatio * 100 / totalColumnRatio + "%' align='right'>" + otherTaxColumnHeader + '</th>';
		}

		if (printAdditionalCESSColumn) {
			header += '<th ' + styleText + " width='" + additionCESSColumnRatio * 100 / totalColumnRatio + "%' align='right'>" + additionCESSColumnHeader + '</th>';
		}
		if (printFinalItemPriceColumn) {
			header += '<th ' + styleText + " width='" + finalItemPriceColumnRatio * 100 / totalColumnRatio + "%' align='right'>" + finalItemPriceColumnHeader + '</th>';
		}

		if (printTotalAmountColumn) {
			header += '<th ' + styleText + " width='" + totalAmountColumnRatio * 100 / totalColumnRatio + "%' align='right'>" + totalAmountColumnHeader + '</th>';
		}

		header += '</tr></thead>';
		return header;
	};

	this.getItemTableHeader = function (firm, theme, themeColor, printSIColumn, printItemName, printItemQuantity, printItemPrice, printFinalItemPriceColumn, printTotalAmountColumn, printItemisedDiscountColumn, isHSNCodeEnabled, isItemCodeEnabled, printItemUnitColumn, printItemBatchColumn, printItemCountColumn, printItemExpiryDateColumn, printItemManufactureDateColumn, printItemMRPColumn, printSize, printTaxableItemPrice, printTotalTaxAmountColumn, printTaxableAmountColumn, printIGSTTaxColumn, printCGSTTaxColumn, printSGSTTaxColumn, printCESSTaxColumn, printOtherTaxColumn, printAdditionalCESSColumn, printStateSpecificCESSTaxColumn) {
		var siColumnRatio = printSIColumn ? TransactionPrintSettings.getSIColumnRatio() : 0;
		var itemNameColumnRatio = printItemName ? TransactionPrintSettings.getItemNameColumnRatio() : 0;
		var hsnCodeColumnRatio = isHSNCodeEnabled ? TransactionPrintSettings.getHsnCodeColumnRatio() : 0;
		var itemCodeColumnRatio = isItemCodeEnabled ? TransactionPrintSettings.getItemCodeColumnRatio() : 0;
		var itemCountColumnRatio = printItemCountColumn ? TransactionPrintSettings.getItemCountColumnRatio() : 0;
		var itemBatchColumnRatio = printItemBatchColumn ? TransactionPrintSettings.getItemBatchColumnRatio() : 0;
		var itemExpDateColumnRatio = printItemExpiryDateColumn ? TransactionPrintSettings.getItemExpDateColumnRatio() : 0;
		var itemManufacturingDateColumnRatio = printItemManufactureDateColumn ? TransactionPrintSettings.getItemManufacturingDateColumnRatio() : 0;
		var itemMRPColumnRatio = printItemMRPColumn ? TransactionPrintSettings.getItemMRPColumnRatio() : 0;
		var itemSizeColumnRatio = printSize ? TransactionPrintSettings.getItemSizeColumnRatio() : 0;
		var quantityColumnRatio = printItemQuantity ? TransactionPrintSettings.getQuantityColumnRatio() : 0;
		var itemUnitColumnRatio = printItemUnitColumn ? TransactionPrintSettings.getItemUnitColumnRatio() : 0;
		var pricePerUnitColumnRatio = printItemPrice ? TransactionPrintSettings.getPricePerUnitColumnRatio() : 0;
		var discountColumnRatio = printItemisedDiscountColumn ? TransactionPrintSettings.getDiscountColumnRatio() : 0;
		var taxableItemPriceRatio = printTaxableItemPrice ? TransactionPrintSettings.getTaxableItemPriceColumnRatio() : 0;
		var taxTotalAmountColumnRatio = printTotalTaxAmountColumn ? TransactionPrintSettings.getTaxTotalAmountColumnRatio() : 0;
		var taxableAmountColumnRatio = printTaxableAmountColumn ? TransactionPrintSettings.getTaxableAmountColumnRatio() : 0;
		var igstColumnRatio = printIGSTTaxColumn ? TransactionPrintSettings.getIgstColumnRatio() : 0;
		var cgstColumnRatio = printCGSTTaxColumn ? TransactionPrintSettings.getCgstColumnRatio() : 0;
		var sgstColumnRatio = printSGSTTaxColumn ? TransactionPrintSettings.getSgstColumnRatio() : 0;
		var cessColumnRatio = printCESSTaxColumn ? TransactionPrintSettings.getCessColumnRatio() : 0;
		var stateSpecificCESSColumnRatio = printStateSpecificCESSTaxColumn ? TransactionPrintSettings.getStateSpecificCESSTaxColumnRatio() : 0;
		var otherTaxColumnRatio = printOtherTaxColumn ? TransactionPrintSettings.getOtherTaxColumnRatio() : 0;
		var additionCESSColumnRatio = printAdditionalCESSColumn ? TransactionPrintSettings.getAdditionCESSColumnRatio() : 0;
		var finalItemPriceColumnRatio = printFinalItemPriceColumn ? TransactionPrintSettings.getFinalItemPriceColumnRatio() : 0;
		var totalAmountColumnRatio = printTotalAmountColumn ? TransactionPrintSettings.getTotalAmountColumnRatio() : 0;

		var siColumnHeader = TransactionPrintSettings.getSIColumnHeader();
		var itemNameColumnHeader = TransactionPrintSettings.getItemNameColumnHeader();
		var hsnCodeColumnHeader = TransactionPrintSettings.getHsnCodeColumnHeader();
		var itemCodeColumnHeader = TransactionPrintSettings.getItemCodeColumnHeader();
		var pricePerUnitColumnHeader = TransactionPrintSettings.getPricePerUnitColumnHeader();
		var quantityColumnHeader = TransactionPrintSettings.getQuantityColumnHeader();
		var itemUnitColumnHeader = TransactionPrintSettings.getItemUnitColumnHeader();
		var discountColumnHeader = TransactionPrintSettings.getDiscountColumnHeader();
		var taxableItemPriceHeader = TransactionPrintSettings.getTaxablePriceColumnHeader();
		var taxTotalAmountColumnHeader = TransactionPrintSettings.getTaxTotalAmountColumnHeader();
		var taxableAmountColumnHeader = TransactionPrintSettings.getTaxableAmountColumnHeader();
		var igstColumnHeader = TransactionPrintSettings.getIgstColumnHeader();
		var cgstColumnHeader = TransactionPrintSettings.getCgstColumnHeader();
		var sgstColumnHeader = TransactionPrintSettings.getSgstColumnHeader(firm);
		var cessColumnHeader = TransactionPrintSettings.getCessColumnHeader();
		var stateSpecificTaxColumnHeader = TransactionPrintSettings.getStateSpecificCessColumnHeader();
		var otherTaxColumnHeader = TransactionPrintSettings.getOtherTaxColumnHeader();
		var additionCESSColumnHeader = TransactionPrintSettings.getAdditionCESSColumnHeader();
		var finalItemPriceColumnHeader = TransactionPrintSettings.getFinalItemPriceColumnHeader();
		var totalAmountColumnHeader = TransactionPrintSettings.getTotalAmountColumnHeader();

		return this.getItemTableHeaderUsingPrintSetting(theme, themeColor, printSIColumn, printItemName, printItemQuantity, printItemPrice, printFinalItemPriceColumn, printTotalAmountColumn, printItemisedDiscountColumn, isHSNCodeEnabled, isItemCodeEnabled, printItemUnitColumn, printItemBatchColumn, printItemCountColumn, printItemExpiryDateColumn, printItemManufactureDateColumn, printItemMRPColumn, printSize, printTaxableItemPrice, printTotalTaxAmountColumn, printTaxableAmountColumn, printIGSTTaxColumn, printCGSTTaxColumn, printSGSTTaxColumn, printCESSTaxColumn, printOtherTaxColumn, printAdditionalCESSColumn, siColumnRatio, itemNameColumnRatio, hsnCodeColumnRatio, itemCodeColumnRatio, itemCountColumnRatio, itemBatchColumnRatio, itemExpDateColumnRatio, itemManufacturingDateColumnRatio, itemMRPColumnRatio, itemSizeColumnRatio, quantityColumnRatio, itemUnitColumnRatio, pricePerUnitColumnRatio, discountColumnRatio, taxableItemPriceRatio, taxTotalAmountColumnRatio, taxableAmountColumnRatio, igstColumnRatio, cgstColumnRatio, sgstColumnRatio, cessColumnRatio, otherTaxColumnRatio, additionCESSColumnRatio, finalItemPriceColumnRatio, totalAmountColumnRatio, siColumnHeader, itemNameColumnHeader, hsnCodeColumnHeader, itemCodeColumnHeader, pricePerUnitColumnHeader, quantityColumnHeader, itemUnitColumnHeader, discountColumnHeader, taxableItemPriceHeader, taxTotalAmountColumnHeader, taxableAmountColumnHeader, igstColumnHeader, cgstColumnHeader, sgstColumnHeader, cessColumnHeader, otherTaxColumnHeader, additionCESSColumnHeader, finalItemPriceColumnHeader, totalAmountColumnHeader, printStateSpecificCESSTaxColumn, stateSpecificTaxColumnHeader, stateSpecificCESSColumnRatio);
	};

	this.getItemRowHTML = function (lineItem, sNo, theme, themeColor, printSIColumn, printItemName, printItemQuantity, printItemPrice, printFinalItemPriceColumn, printTotalAmountColumn, printItemisedDiscountColumn, isHSNCodeEnabled, isItemCodeEnabled, printItemUnitColumn, printItemBatchNumber, printItemCount, printItemExpiryDate, printItemManufactureDate, printItemMRP, printSize, printDescription, printSerialNumber, printTaxableItemPrice, printTotalTaxAmountColumn, printTaxableAmountColumn, printIGSTTaxColumn, printCGSTTaxColumn, printSGSTTaxColumn, printCESSTaxColumn, printOtherTaxColumn, printAdditionalCESSColumn, printTaxAmountValueInTaxColumn, printTaxPercentValueInTaxColumn, printDiscountAmountValueInDiscountColumn, printDiscountPercentValueInDiscountColumn, printStateSpecificCESSTaxColumn) {
		var htmlText = '';
		var styleToBeAddedInColumns = " class='";
		if (lineItem != null) {
			if (theme == InvoiceTheme.THEME_0 || theme == InvoiceTheme.THEME_1 || theme == InvoiceTheme.THEME_2 || theme == InvoiceTheme.THEME_3 || theme == InvoiceTheme.THEME_4) {
				styleToBeAddedInColumns += 'borderBottomForTxn ';
			}
		}
		if (theme == InvoiceTheme.THEME_3 || theme == InvoiceTheme.THEME_5 || theme == InvoiceTheme.THEME_6 || theme == InvoiceTheme.THEME_7 || theme == InvoiceTheme.THEME_8 || theme == InvoiceTheme.THEME_9) {
			styleToBeAddedInColumns += 'borderLeftForTxn borderRightForTxn ';
		}
		if (theme == InvoiceTheme.THEME_10) {
			styleToBeAddedInColumns += " theme10BorderColor'";
		} else {
			styleToBeAddedInColumns += " borderColorGrey'";
		}
		if (lineItem) {
			var itemCache = new ItemCache();
			var item = itemCache.getItemById(lineItem.getItemId());
			var itemUnitMappingCache = new ItemUnitMappingCache();

			var unitPrice = Number(lineItem.getItemUnitPrice());
			var quantityToBeShown = Number(lineItem.getItemQuantity());
			var freeQuantityToBeShown = Number(lineItem.getItemFreeQuantity());
			if (lineItem.getLineItemUnitMappingId() > 0) {
				var mappingObj = itemUnitMappingCache.getItemUnitMapping(lineItem.getLineItemUnitMappingId());
				quantityToBeShown = Number(lineItem.getItemQuantity()) * mappingObj.getConversionRate();
				freeQuantityToBeShown = Number(lineItem.getItemFreeQuantity()) * mappingObj.getConversionRate();
				unitPrice = Number(lineItem.getItemUnitPrice()) / mappingObj.getConversionRate();
			}

			if (theme == InvoiceTheme.THEME_12 || theme == InvoiceTheme.THEME_11) {
				htmlText += '<tr style="background-color:white " class="">';
			} else {
				htmlText += '<tr>';
			}

			if (printSIColumn) {
				htmlText += '<td ' + styleToBeAddedInColumns + '>' + sNo + '</td>';
			}

			if (printItemName) {
				htmlText += '<td ' + styleToBeAddedInColumns + "><p class='noPadding'>" + lineItem.getItemName() + '</p>';
				if (printDescription && lineItem.getLineItemDescription()) {
					htmlText += "<p class='normalTextSize'>(" + lineItem.getLineItemDescription() + ')</p>';
				}

				if (printSerialNumber && lineItem.getLineItemSerialNumber()) {
					htmlText += "<p class='normalTextSize'>" + settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_SERIAL_NUMBER_VALUE) + ': ' + lineItem.getLineItemSerialNumber() + '</p>';
				}
				htmlText += '</td>';
			}

			if (isItemCodeEnabled) {
				if (item) {
					htmlText += '<td ' + styleToBeAddedInColumns + " align='left'>" + (item.getItemCode() ? item.getItemCode() : '') + '</td>';
				} else {
					htmlText += '<td ' + styleToBeAddedInColumns + " align='left'></td>";
				}
			}

			if (isHSNCodeEnabled) {
				if (item) {
					htmlText += '<td ' + styleToBeAddedInColumns + " align='left'>" + (item.getItemHSNCode() ? item.getItemHSNCode() : '') + '</td>';
				} else {
					htmlText += '<td ' + styleToBeAddedInColumns + " align='left'></td>";
				}
			}

			if (printItemCount) {
				if (lineItem.getLineItemCount() && Number(lineItem.getLineItemCount()) > 0) {
					htmlText += '<td ' + styleToBeAddedInColumns + " align='right'>" + MyDouble.trimQuantityToString(Number(lineItem.getLineItemCount())) + '</td>';
					if (item && !item.isItemService()) {
						this.countTotal += Number(lineItem.getLineItemCount());
					}
				} else {
					htmlText += '<td ' + styleToBeAddedInColumns + " align='right'></td>";
				}
			}
			if (printItemBatchNumber) {
				if (lineItem.getLineItemBatchNumber()) {
					htmlText += '<td ' + styleToBeAddedInColumns + " align='left'>" + lineItem.getLineItemBatchNumber() + '</td>';
				} else {
					htmlText += '<td ' + styleToBeAddedInColumns + " align='left'></td>";
				}
			}

			if (printItemExpiryDate) {
				if (lineItem.getLineItemExpiryDate()) {
					var format = settingCache.getExpiryDateFormat();
					htmlText += '<td ' + styleToBeAddedInColumns + " align='right'>" + MyDate.getDate(format == 1 ? 'd/m/y' : 'm/y', lineItem.getLineItemExpiryDate()) + '</td>';
				} else {
					htmlText += '<td ' + styleToBeAddedInColumns + " align='right'></td>";
				}
			}

			if (printItemManufactureDate) {
				if (lineItem.getLineItemManufacturingDate()) {
					var format = settingCache.getManufacturingDateFormat();
					htmlText += '<td ' + styleToBeAddedInColumns + " align='right'>" + MyDate.getDate(format == 1 ? 'd/m/y' : 'm/y', lineItem.getLineItemManufacturingDate()) + '</td>';
				} else {
					htmlText += '<td ' + styleToBeAddedInColumns + " align='right'></td>";
				}
			}

			if (printItemMRP) {
				if (lineItem.getLineItemMRP() && Number(lineItem.getLineItemMRP()) > 0) {
					htmlText += '<td ' + styleToBeAddedInColumns + " align='right'>" + MyDouble.getAmountWithDecimal(lineItem.getLineItemMRP()) + '</td>';
				} else {
					htmlText += '<td ' + styleToBeAddedInColumns + " align='right'></td>";
				}
			}
			if (printSize) {
				if (lineItem.getLineItemSize()) {
					htmlText += '<td ' + styleToBeAddedInColumns + " align='right'>" + lineItem.getLineItemSize() + '</td>';
				} else {
					htmlText += '<td ' + styleToBeAddedInColumns + " align='right'></td>";
				}
			}

			if (printItemQuantity) {
				htmlText += '<td ' + styleToBeAddedInColumns + " align='right'>" + MyDouble.getQuantityWithDecimalWithoutColor(quantityToBeShown) + MyDouble.quantityDoubleToStringWithSignExplicitly(freeQuantityToBeShown, true) + '</td>';
			}

			if (item && !item.isItemService()) {
				this.quantityTotal += quantityToBeShown;
				this.freeQuantityTotal += freeQuantityToBeShown;
			}

			if (printItemUnitColumn) {
				var itemUnitCache = new ItemUnitCache();
				if (lineItem.getLineItemUnitId() > 0) {
					htmlText += '<td ' + styleToBeAddedInColumns + ' align="right">' + itemUnitCache.getItemUnitShortNameById(lineItem.getLineItemUnitId()) + '</td>';
				} else {
					htmlText += '<td ' + styleToBeAddedInColumns + ' align="right"> - </td>';
				}
			}

			if (printItemPrice) {
				htmlText += '<td ' + styleToBeAddedInColumns + " align='right'>" + MyDouble.getAmountWithDecimalAndCurrency(unitPrice) + '</td>';
			}

			if (printItemisedDiscountColumn) {
				htmlText += '<td ' + styleToBeAddedInColumns + " align='right'>";
				if (printDiscountAmountValueInDiscountColumn) {
					htmlText += MyDouble.getAmountWithDecimalAndCurrency(lineItem.getLineItemDiscountAmount());
					if (printDiscountPercentValueInDiscountColumn) {
						htmlText += ' (';
					}
				}
				if (printDiscountPercentValueInDiscountColumn) {
					htmlText += MyDouble.getPercentageWithDecimal(lineItem.getLineItemDiscountPercent()) + '%';
					if (printDiscountAmountValueInDiscountColumn) {
						htmlText += ')';
					}
				}
				htmlText += '</td>';
				this.discountTotal += lineItem.getLineItemDiscountAmount() ? MyDouble.getAmountWithDecimal(Number(lineItem.getLineItemDiscountAmount())) : 0;
			}

			if (printTaxableItemPrice) {
				var itemPrice = unitPrice;
				var itemDiscountPercentage = lineItem.getLineItemDiscountPercent();
				if (itemDiscountPercentage > 0) {
					itemPrice = itemPrice - itemPrice * itemDiscountPercentage / 100;
				}
				htmlText += '<td ' + styleToBeAddedInColumns + ' align="right">' + MyDouble.getAmountWithDecimalAndCurrency(itemPrice) + '</td>';
			}

			if (printTotalTaxAmountColumn) {
				var _taxCode = null;

				if (lineItem.getLineItemTaxId()) {
					_taxCode = taxCodeCache.getTaxCodeObjectById(lineItem.getLineItemTaxId());
				}
				htmlText += '<td ' + styleToBeAddedInColumns + ' align="right">';
				if (printTaxAmountValueInTaxColumn) {
					htmlText += MyDouble.getAmountWithDecimalAndCurrency(lineItem.getLineItemTaxAmount());
					if (printTaxPercentValueInTaxColumn) {
						htmlText += ' (';
					}
				}
				if (printTaxPercentValueInTaxColumn) {
					if (_taxCode && _taxCode.getTaxRateType() == TaxCodeConstants.Exempted) {
						htmlText += exemptedTaxString;
					} else {
						htmlText += MyDouble.getPercentageWithDecimal(lineItem.getLineItemTaxPercent()) + '%';
					}
					if (printTaxAmountValueInTaxColumn) {
						htmlText += ')';
					}
				}
				htmlText += '</td>';

				this.taxTotal += lineItem.getLineItemTaxAmount() ? MyDouble.getAmountWithDecimal(Number(lineItem.getLineItemTaxAmount())) : 0;
			}

			var taxableAmount = Number(lineItem.getLineItemTotal()) - Number(lineItem.getLineItemAdditionalCESS()) - Number(lineItem.getLineItemTaxAmount());
			if (printTaxableAmountColumn) {
				this.taxableAmountTotal += MyDouble.getAmountWithDecimal(taxableAmount);
				htmlText += '<td ' + styleToBeAddedInColumns + ' align="right">' + MyDouble.getAmountWithDecimalAndCurrency(taxableAmount) + '</td>';
			}

			if (printTaxableAmountColumn) {
				var igstTaxAmount = 0;
				var igstTaxRate = 0;
				var igstRatePresent = false;
				var cgstTaxAmount = 0;
				var cgstTaxRate = 0;
				var cgstRatePresent = false;
				var sgstTaxAmount = 0;
				var sgstTaxRate = 0;
				var sgstRatePresent = false;
				var cessTaxAmount = 0;
				var cessTaxRate = 0;
				var cessRatePresent = false;
				var stateSpecificCESSTaxAmount = 0;
				var stateSpecificCESSTaxRate = 0;
				var stateSpecificCESSRatePresent = false;
				var otherTaxAmount = 0;
				var otherTaxRate = 0;
				var otherRatePresent = false;
				var exemptedRatePresent = false;

				if (lineItem.getLineItemTaxId()) {
					var taxCode = taxCodeCache.getTaxCodeObjectById(lineItem.getLineItemTaxId());

					if (taxCode.getTaxRateType() == TaxCodeConstants.Exempted) {
						exemptedRatePresent = true;
					} else if (taxCode.getTaxType() == TaxCodeConstants.taxGroup) {
						var taxCodeIds = (0, _values2.default)(taxCode.getTaxCodeMap());
						if (taxCodeIds.length > 0) {
							for (i = 0; i < taxCodeIds.length; i++) {
								var taxRate = taxCodeCache.getTaxCodeObjectById(taxCodeIds[i]);
								if (taxRate.getTaxRateType() == TaxCodeConstants.IGST) {
									igstRatePresent = true;
									igstTaxAmount += taxableAmount * Number(taxRate.getTaxRate()) / 100;
									igstTaxRate += taxRate.getTaxRate();
								} else if (taxRate.getTaxRateType() == TaxCodeConstants.CGST) {
									cgstRatePresent = true;
									cgstTaxAmount += taxableAmount * Number(taxRate.getTaxRate()) / 100;
									cgstTaxRate += taxRate.getTaxRate();
								} else if (taxRate.getTaxRateType() == TaxCodeConstants.SGST) {
									sgstRatePresent = true;
									sgstTaxAmount += taxableAmount * Number(taxRate.getTaxRate()) / 100;
									sgstTaxRate += taxRate.getTaxRate();
								} else if (taxRate.getTaxRateType() == TaxCodeConstants.CESS) {
									cessRatePresent = true;
									cessTaxAmount += taxableAmount * Number(taxRate.getTaxRate()) / 100;
									cessTaxRate += taxRate.getTaxRate();
								} else if (taxRate.getTaxRateType() == TaxCodeConstants.STATE_SPECIFIC_CESS) {
									stateSpecificCESSRatePresent = true;
									stateSpecificCESSTaxAmount += taxableAmount * Number(taxRate.getTaxRate()) / 100;
									stateSpecificCESSTaxRate += taxRate.getTaxRate();
								} else if (taxRate.getTaxRateType() == TaxCodeConstants.OTHER) {
									otherRatePresent = true;
									otherTaxAmount += taxableAmount * Number(taxRate.getTaxRate()) / 100;
									otherTaxRate += taxRate.getTaxRate();
								}
							}
						}
					} else {
						if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
							igstRatePresent = true;
							igstTaxAmount = taxableAmount * Number(taxCode.getTaxRate()) / 100;
							igstTaxRate = taxCode.getTaxRate();
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
							cgstRatePresent = true;
							cgstTaxAmount = taxableAmount * Number(taxCode.getTaxRate()) / 100;
							cgstTaxRate = taxCode.getTaxRate();
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
							sgstRatePresent = true;
							sgstTaxAmount = taxableAmount * Number(taxCode.getTaxRate()) / 100;
							sgstTaxRate = taxCode.getTaxRate();
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
							cessRatePresent = true;
							cessTaxAmount = taxableAmount * Number(taxCode.getTaxRate()) / 100;
							cessTaxRate = taxCode.getTaxRate();
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.STATE_SPECIFIC_CESS) {
							stateSpecificCESSRatePresent = true;
							stateSpecificCESSTaxAmount += taxableAmount * Number(taxCode.getTaxRate()) / 100;
							stateSpecificCESSTaxRate += taxRate.getTaxRate();
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.OTHER) {
							otherRatePresent = true;
							otherTaxAmount = taxableAmount * Number(taxCode.getTaxRate()) / 100;
							otherTaxRate = taxCode.getTaxRate();
						}
					}
				}

				this.taxIGSTTotal += MyDouble.getAmountWithDecimal(igstTaxAmount);
				this.taxCGSTTotal += MyDouble.getAmountWithDecimal(cgstTaxAmount);
				this.taxSGSTTotal += MyDouble.getAmountWithDecimal(sgstTaxAmount);
				this.taxCESSTotal += MyDouble.getAmountWithDecimal(cessTaxAmount);
				this.taxStateSpecificCESSTotal += MyDouble.getAmountWithDecimal(stateSpecificCESSTaxAmount);
				this.taxOtherTotal += MyDouble.getAmountWithDecimal(otherTaxAmount);

				if (printIGSTTaxColumn) {
					htmlText += '<td ' + styleToBeAddedInColumns + ' align="right">';
					if (printTaxAmountValueInTaxColumn) {
						if (igstRatePresent) {
							htmlText += MyDouble.getAmountWithDecimalAndCurrency(igstTaxAmount);
						} else if (exemptedRatePresent) {
							htmlText += MyDouble.getAmountWithDecimalAndCurrency(0);
						}

						if (printTaxPercentValueInTaxColumn && (igstRatePresent || exemptedRatePresent)) {
							htmlText += ' (';
						}
					}
					if (printTaxPercentValueInTaxColumn) {
						if (igstRatePresent) {
							htmlText += MyDouble.getPercentageWithDecimal(igstTaxRate) + '%';
						} else if (exemptedRatePresent) {
							htmlText += exemptedTaxString;
						}
						if (printTaxAmountValueInTaxColumn && (igstRatePresent || exemptedRatePresent)) {
							htmlText += ')';
						}
					}
					htmlText += '</td>';
				}
				if (printCGSTTaxColumn) {
					htmlText += '<td ' + styleToBeAddedInColumns + ' align="right">';
					if (printTaxAmountValueInTaxColumn) {
						if (cgstRatePresent) {
							htmlText += MyDouble.getAmountWithDecimalAndCurrency(cgstTaxAmount);
						} else if (exemptedRatePresent) {
							htmlText += MyDouble.getAmountWithDecimalAndCurrency(0);
						}

						if (printTaxPercentValueInTaxColumn && (cgstRatePresent || exemptedRatePresent)) {
							htmlText += ' (';
						}
					}
					if (printTaxPercentValueInTaxColumn) {
						if (cgstRatePresent) {
							htmlText += MyDouble.getPercentageWithDecimal(cgstTaxRate) + '%';
						} else if (exemptedRatePresent) {
							htmlText += exemptedTaxString;
						}
						if (printTaxAmountValueInTaxColumn && (cgstRatePresent || exemptedRatePresent)) {
							htmlText += ')';
						}
					}
					htmlText += '</td>';
				}
				if (printSGSTTaxColumn) {
					htmlText += '<td ' + styleToBeAddedInColumns + ' align="right">';
					if (printTaxAmountValueInTaxColumn) {
						if (sgstRatePresent) {
							htmlText += MyDouble.getAmountWithDecimalAndCurrency(sgstTaxAmount);
						} else if (exemptedRatePresent) {
							htmlText += MyDouble.getAmountWithDecimalAndCurrency(0);
						}

						if (printTaxPercentValueInTaxColumn && (sgstRatePresent || exemptedRatePresent)) {
							htmlText += ' (';
						}
					}
					if (printTaxPercentValueInTaxColumn) {
						if (sgstRatePresent) {
							htmlText += MyDouble.getPercentageWithDecimal(sgstTaxRate) + '%';
						} else if (exemptedRatePresent) {
							htmlText += exemptedTaxString;
						}
						if (printTaxAmountValueInTaxColumn && (sgstRatePresent || exemptedRatePresent)) {
							htmlText += ')';
						}
					}
					htmlText += '</td>';
				}
				if (printCESSTaxColumn) {
					htmlText += '<td ' + styleToBeAddedInColumns + ' align="right">';
					if (printTaxAmountValueInTaxColumn) {
						if (cessRatePresent) {
							htmlText += MyDouble.getAmountWithDecimalAndCurrency(cessTaxAmount);
						} else if (exemptedRatePresent) {
							htmlText += MyDouble.getAmountWithDecimalAndCurrency(0);
						}

						if (printTaxPercentValueInTaxColumn && (cessRatePresent || exemptedRatePresent)) {
							htmlText += ' (';
						}
					}
					if (printTaxPercentValueInTaxColumn) {
						if (cessRatePresent) {
							htmlText += MyDouble.getPercentageWithDecimal(cessTaxRate) + '%';
						} else if (exemptedRatePresent) {
							htmlText += exemptedTaxString;
						}
						if (printTaxAmountValueInTaxColumn && (cessRatePresent || exemptedRatePresent)) {
							htmlText += ')';
						}
					}
					htmlText += '</td>';
				}
				if (printStateSpecificCESSTaxColumn) {
					htmlText += '<td ' + styleToBeAddedInColumns + ' align="right">';
					if (printTaxAmountValueInTaxColumn) {
						if (stateSpecificCESSRatePresent) {
							htmlText += MyDouble.getAmountWithDecimalAndCurrency(stateSpecificCESSTaxAmount);
						} else if (exemptedRatePresent) {
							htmlText += MyDouble.getAmountWithDecimalAndCurrency(0);
						}

						if (printTaxPercentValueInTaxColumn && (stateSpecificCESSRatePresent || exemptedRatePresent)) {
							htmlText += ' (';
						}
					}
					if (printTaxPercentValueInTaxColumn) {
						if (stateSpecificCESSRatePresent) {
							htmlText += MyDouble.getPercentageWithDecimal(stateSpecificCESSTaxRate) + '%';
						} else if (exemptedRatePresent) {
							htmlText += exemptedTaxString;
						}
						if (printTaxAmountValueInTaxColumn && (stateSpecificCESSRatePresent || exemptedRatePresent)) {
							htmlText += ')';
						}
					}
					htmlText += '</td>';
				}
				if (printOtherTaxColumn) {
					htmlText += '<td ' + styleToBeAddedInColumns + ' align="right">';
					if (printTaxAmountValueInTaxColumn) {
						if (otherRatePresent) {
							htmlText += MyDouble.getAmountWithDecimalAndCurrency(otherTaxAmount);
						} else if (exemptedRatePresent) {
							htmlText += MyDouble.getAmountWithDecimalAndCurrency(0);
						}

						if (printTaxPercentValueInTaxColumn && (otherRatePresent || exemptedRatePresent)) {
							htmlText += ' (';
						}
					}
					if (printTaxPercentValueInTaxColumn) {
						if (otherRatePresent) {
							htmlText += MyDouble.getPercentageWithDecimal(otherTaxRate) + '%';
						} else if (exemptedRatePresent) {
							htmlText += exemptedTaxString;
						}
						if (printTaxAmountValueInTaxColumn && (otherRatePresent || exemptedRatePresent)) {
							htmlText += ')';
						}
					}
					htmlText += '</td>';
				}
			}

			if (printAdditionalCESSColumn) {
				htmlText += '<td ' + styleToBeAddedInColumns + ' align="right">' + MyDouble.getAmountForInvoicePrint(lineItem.getLineItemAdditionalCESS(), true) + '</td>';
				this.taxAdditionCESSTotal += MyDouble.getAmountWithDecimal(Number(lineItem.getLineItemAdditionalCESS()));
			}

			if (printFinalItemPriceColumn) {
				if (quantityToBeShown > 0) {
					htmlText += '<td ' + styleToBeAddedInColumns + " align='right'>" + MyDouble.getAmountForInvoicePrint(lineItem.getLineItemTotal() / quantityToBeShown) + '</td>';
				} else {
					htmlText += '<td ' + styleToBeAddedInColumns + " align='right'>-</td>";
				}
			}

			this.subTotal += lineItem.getLineItemTotal() ? MyDouble.getAmountWithDecimal(Number(lineItem.getLineItemTotal())) : 0;

			if (printTotalAmountColumn) {
				htmlText += '<td ' + styleToBeAddedInColumns + " align='right'>" + MyDouble.getAmountWithDecimalAndCurrency(lineItem.getLineItemTotal()) + '</td>';
			}
			htmlText += '</tr>';
		} else {
			if (theme == InvoiceTheme.THEME_12 || theme == InvoiceTheme.THEME_11) {
				htmlText += '<tr style="background-color:white " class="">';
			} else {
				htmlText += '<tr>';
			}
			if (printSIColumn) {
				htmlText += '<td ' + styleToBeAddedInColumns + '>&nbsp;</td>';
			}

			if (printItemName) {
				htmlText += '<td ' + styleToBeAddedInColumns + '>    </td>';
			}

			if (isItemCodeEnabled) {
				htmlText += '<td ' + styleToBeAddedInColumns + " align='left'></td>";
			}

			if (isHSNCodeEnabled) {
				htmlText += '<td ' + styleToBeAddedInColumns + " align='left'></td>";
			}

			if (printItemCount) {
				htmlText += '<td ' + styleToBeAddedInColumns + " align='right'></td>";
			}
			if (printItemBatchNumber) {
				htmlText += '<td ' + styleToBeAddedInColumns + " align='left'></td>";
			}
			if (printItemExpiryDate) {
				htmlText += '<td ' + styleToBeAddedInColumns + " align='right'></td>";
			}
			if (printItemManufactureDate) {
				htmlText += '<td ' + styleToBeAddedInColumns + " align='right'></td>";
			}
			if (printItemMRP) {
				htmlText += '<td ' + styleToBeAddedInColumns + " align='right'></td>";
			}
			if (printSize) {
				htmlText += '<td ' + styleToBeAddedInColumns + " align='right'></td>";
			}

			if (printItemQuantity) {
				htmlText += '<td ' + styleToBeAddedInColumns + ' align="right"></td>';
			}

			if (printItemUnitColumn) {
				htmlText += '<td ' + styleToBeAddedInColumns + ' align="right">  </td>';
			}

			if (printItemPrice) {
				htmlText += '<td ' + styleToBeAddedInColumns + ' align="right"></td>';
			}

			if (printItemisedDiscountColumn) {
				htmlText += '<td ' + styleToBeAddedInColumns + ' align="right"></td>';
			}

			if (printTaxableItemPrice) {
				htmlText += '<td ' + styleToBeAddedInColumns + ' align="right"></td>';
			}
			if (printTotalTaxAmountColumn) {
				htmlText += '<td ' + styleToBeAddedInColumns + ' align="right"></td>';
			}

			if (printTaxableAmountColumn) {
				htmlText += '<td ' + styleToBeAddedInColumns + ' align="right"></td>';
			}

			if (printTaxableAmountColumn) {
				if (printIGSTTaxColumn) {
					htmlText += '<td ' + styleToBeAddedInColumns + ' align="right"></td>';
				}
				if (printCGSTTaxColumn) {
					htmlText += '<td ' + styleToBeAddedInColumns + ' align="right"></td>';
				}
				if (printSGSTTaxColumn) {
					htmlText += '<td ' + styleToBeAddedInColumns + ' align="right"></td>';
				}
				if (printCESSTaxColumn) {
					htmlText += '<td ' + styleToBeAddedInColumns + ' align="right"></td>';
				}
				if (printStateSpecificCESSTaxColumn) {
					htmlText += '<td ' + styleToBeAddedInColumns + ' align="right"></td>';
				}
				if (printOtherTaxColumn) {
					htmlText += '<td ' + styleToBeAddedInColumns + ' align="right"></td>';
				}
			}
			if (printAdditionalCESSColumn) {
				htmlText += '<td ' + styleToBeAddedInColumns + ' align="right"></td>';
			}

			if (printFinalItemPriceColumn) {
				htmlText += '<td ' + styleToBeAddedInColumns + ' align="right"></td>';
			}

			if (printTotalAmountColumn) {
				htmlText += '<td ' + styleToBeAddedInColumns + ' align="right"></td>';
			}

			htmlText += '</tr>';
		}
		return htmlText;
	};

	this.getItemRowForTotalQuantity = function (theme, themeColor, printSIColumn, printItemName, printItemQuantity, printItemPrice, printFinalItemPriceColumn, printTotalAmountColumn, printItemisedDiscountColumn, isHSNCodeEnabled, isItemCodeEnabled, printItemUnitColumn, printItemBatchNumber, printItemCount, printItemExpiryDate, printItemManufactureDate, printItemMRP, printSize, printDescription, printSerialNumber, printTaxableItemPrice, printTotalTaxAmountColumn, printTaxableAmountColumn, printIGSTTaxColumn, printCGSTTaxColumn, printSGSTTaxColumn, printCESSTaxColumn, printOtherTaxColumn, printAdditionalCESSColumn, printTaxAmountValueInTaxColumn, printTaxPercentValueInTaxColumn, printDiscountAmountValueInDiscountColumn, printDiscountPercentValueInDiscountColumn, printStateSpecificCESSTaxColumn) {
		var htmlText = '';

		var styleToBeAddedInColumns = " class=' boldText borderBottomForTxn borderTopForTxn ";
		if (theme == InvoiceTheme.THEME_3 || theme == InvoiceTheme.THEME_5 || theme == InvoiceTheme.THEME_6 || theme == InvoiceTheme.THEME_7 || theme == InvoiceTheme.THEME_8 || theme == InvoiceTheme.THEME_9) {
			styleToBeAddedInColumns += '  borderLeftForTxn borderRightForTxn ';
		}
		styleToBeAddedInColumns += " borderColorGrey'";

		if (theme == InvoiceTheme.THEME_12) {
			htmlText += '<tr style=\'color: ' + (themeColor == "#ffffff" ? "black" : "white") + '\' class=\'theme12trlesspadding\'>';
		} else if (theme == InvoiceTheme.THEME_11) {
			htmlText += "<tr style='color:white' class=' theme11lastRow'>";
		} else {
			htmlText += '<tr>';
		}

		var theme11style = theme == InvoiceTheme.THEME_11 ? 'style = \'border-left: none; border-bottom: none;\'' : '';

		if (printSIColumn) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + '></td>';
			theme11style = '';
		}
		if (printItemName) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + '><b>Total</b></td>';
			theme11style = '';
		}

		if (isItemCodeEnabled) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + " align='right'></td>";
			theme11style = '';
		}

		if (isHSNCodeEnabled) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + " align='right'></td>";
			theme11style = '';
		}

		if (printItemCount) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + " align='right'>" + MyDouble.trimQuantityToString(this.countTotal, true) + '</td>';
			theme11style = '';
		}

		if (printItemBatchNumber) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + " align='left'></td>";
			theme11style = '';
		}
		if (printItemExpiryDate) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + " align='right'></td>";
			theme11style = '';
		}
		if (printItemManufactureDate) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + " align='right'></td>";
			theme11style = '';
		}
		if (printItemMRP) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + " align='right'></td>";
			theme11style = '';
		}
		if (printSize) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + " align='right'></td>";
			theme11style = '';
		}

		if (printItemQuantity) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + " align='right'> <b>" + MyDouble.trimQuantityToString(this.quantityTotal, true) + MyDouble.quantityDoubleToStringWithSignExplicitly(this.freeQuantityTotal, true) + '</b></td>';
			theme11style = '';
		}

		if (printItemUnitColumn) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + " align='right'>  </td>";
			theme11style = '';
		}

		if (printItemPrice) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + " align='right'></td>";
			theme11style = '';
		}

		if (printItemisedDiscountColumn) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + " align='right'><b>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(this.discountTotal) + '</b></td>';
			theme11style = '';
		}

		if (printTaxableItemPrice) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + " align='right'></td>";
			theme11style = '';
		}

		if (printTotalTaxAmountColumn) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + " align='right'><b>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(this.taxTotal) + '</b></td>';
			theme11style = '';
		}
		if (printTaxableAmountColumn) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + " align='right'><b>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(this.taxableAmountTotal) + '</b></td>';
			theme11style = '';
		}

		if (printIGSTTaxColumn) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + ' align="right"><b>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(this.taxIGSTTotal) + '</b></td>';
			theme11style = '';
		}
		if (printCGSTTaxColumn) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + ' align="right"><b>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(this.taxCGSTTotal) + '</b></td>';
			theme11style = '';
		}
		if (printSGSTTaxColumn) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + ' align="right"><b>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(this.taxSGSTTotal) + '</b></td>';
			theme11style = '';
		}
		if (printCESSTaxColumn) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + ' align="right"><b>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(this.taxCESSTotal) + '</b></td>';
			theme11style = '';
		}
		if (printStateSpecificCESSTaxColumn) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + ' align="right"><b>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(this.taxStateSpecificCESSTotal) + '</b></td>';
			theme11style = '';
		}
		if (printOtherTaxColumn) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + ' align="right"><b>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(this.taxOtherTotal) + '</b></td>';
			theme11style = '';
		}
		if (printAdditionalCESSColumn) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + ' align="right"><b>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(this.taxAdditionCESSTotal) + '</b></td>';
			theme11style = '';
		}
		if (printFinalItemPriceColumn) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + ' align="right"></td>';
			theme11style = '';
		}

		if (printTotalAmountColumn) {
			htmlText += '<td ' + theme11style + ' ' + styleToBeAddedInColumns + ' align="right"><b>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(this.subTotal) + '</b></td>';
			theme11style = '';
		}

		htmlText += '</tr>';
		return htmlText;
	};

	this.getItemDetails = function (txn, theme, themeColor, textSizeRatio, printDeliveryChallan) {
		var firm = firmCache.getTransactionFirmWithDefault(txn);
		this.quantityTotal = 0;
		this.freeQuantityTotal = 0;
		this.countTotal = 0;
		this.taxableAmountTotal = 0;
		this.taxTotal = 0;
		this.taxIGSTTotal = 0;
		this.taxCGSTTotal = 0;
		this.taxSGSTTotal = 0;
		this.taxCESSTotal = 0;
		this.taxStateSpecificCESSTotal = 0;
		this.taxAdditionCESSTotal = 0;
		this.taxOtherTotal = 0;
		this.discountTotal = 0;
		this.subTotal = 0;

		var lineItems = txn.getLineItems();
		var htmlText = '';

		var printSIColumn = TransactionPrintSettings.printSINumberColumnInInvoicePrint();
		var printItemName = TransactionPrintSettings.printItemNameInInvoicePrint();
		var isHSNCodeEnabled = settingCache.getHSNSACEnabled() && txn.getTxnType() != TxnTypeConstant.TXN_TYPE_EXPENSE && txn.getTxnType() != TxnTypeConstant.TXN_TYPE_OTHER_INCOME && TransactionPrintSettings.printHSNCodeInInvoicePrint();
		var isItemCodeEnabled = txn.getTxnType() != TxnTypeConstant.TXN_TYPE_EXPENSE && txn.getTxnType() != TxnTypeConstant.TXN_TYPE_OTHER_INCOME && TransactionPrintSettings.printItemCodeInInvoicePrint();
		var printItemQuantity = TransactionPrintSettings.printItemQuantityInInvoicePrint();
		var printItemPrice = TransactionPrintSettings.printItemPriceInInvoicePrint() && (!printDeliveryChallan || settingCache.printAmountDetailsInDeliveryChallan());
		var printTaxableItemPrice = TransactionPrintSettings.printTaxableItemPriceInInvoicePrint() && (!printDeliveryChallan || settingCache.printAmountDetailsInDeliveryChallan());

		var printItemisedDiscountColumn = false;
		var printTotalTaxAmountColumn = false;
		var printItemUnitColumn = false;

		var printItemCount = false;
		var printItemBatchNumber = false;
		var printItemExpiryDate = false;
		var printItemManufactureDate = false;
		var printItemMRP = false;
		var printSize = false;
		var printIGSTTaxColumn = false;
		var printCGSTTaxColumn = false;
		var printSGSTTaxColumn = false;
		var printCESSTaxColumn = false;
		var printStateSpecificCESSTaxColumn = false;
		var printOtherTaxColumn = false;
		var printAdditionalCESSColumn = false;

		if (txn.getTxnType() != TxnTypeConstant.TXN_TYPE_EXPENSE && txn.getTxnType() != TxnTypeConstant.TXN_TYPE_OTHER_INCOME && lineItems.length > 0) {
			$.each(lineItems, function (key, lineItem) {
				if (lineItem.getLineItemDiscountAmount() && Number(lineItem.getLineItemDiscountAmount()) > 0) {
					printItemisedDiscountColumn = true;
				}
				if (lineItem.getLineItemUnitId() && Number(lineItem.getLineItemUnitId()) > 0) {
					printItemUnitColumn = true;
				}
				if (lineItem.getLineItemAdditionalCESS() && Number(lineItem.getLineItemAdditionalCESS()) > 0) {
					printAdditionalCESSColumn = true;
				}
				if (lineItem.getLineItemCount() && Number(lineItem.getLineItemCount()) > 0) {
					printItemCount = true;
				}
				if (lineItem.getLineItemBatchNumber()) {
					printItemBatchNumber = true;
				}
				if (lineItem.getLineItemExpiryDate()) {
					printItemExpiryDate = true;
				}
				if (lineItem.getLineItemManufacturingDate()) {
					printItemManufactureDate = true;
				}
				if (lineItem.getLineItemMRP() && Number(lineItem.getLineItemMRP()) > 0) {
					printItemMRP = true;
				}
				if (lineItem.getLineItemSize()) {
					printSize = true;
				}

				if (lineItem.getLineItemTaxId() && Number(lineItem.getLineItemTaxId()) > 0) {
					printTotalTaxAmountColumn = true;

					var taxCode = taxCodeCache.getTaxCodeObjectById(lineItem.getLineItemTaxId());

					if (taxCode.getTaxType() == TaxCodeConstants.taxGroup) {
						var taxCodeIds = (0, _values2.default)(taxCode.getTaxCodeMap());
						if (taxCodeIds.length > 0) {
							for (i = 0; i < taxCodeIds.length; i++) {
								var taxRate = taxCodeCache.getTaxCodeObjectById(taxCodeIds[i]);
								if (taxRate.getTaxRateType() == TaxCodeConstants.IGST) {
									printIGSTTaxColumn = true;
								} else if (taxRate.getTaxRateType() == TaxCodeConstants.CGST) {
									printCGSTTaxColumn = true;
								} else if (taxRate.getTaxRateType() == TaxCodeConstants.SGST) {
									printSGSTTaxColumn = true;
								} else if (taxRate.getTaxRateType() == TaxCodeConstants.CESS) {
									printCESSTaxColumn = true;
								} else if (taxRate.getTaxRateType() == TaxCodeConstants.STATE_SPECIFIC_CESS) {
									printStateSpecificCESSTaxColumn = true;
								} else if (taxRate.getTaxRateType() == TaxCodeConstants.OTHER) {
									printOtherTaxColumn = true;
								}
							}
						}
					} else {
						if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
							printIGSTTaxColumn = true;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
							printCGSTTaxColumn = true;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
							printSGSTTaxColumn = true;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
							printCESSTaxColumn = true;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.STATE_SPECIFIC_CESS) {
							printStateSpecificCESSTaxColumn = true;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.OTHER) {
							printOtherTaxColumn = true;
						}
					}
				}
			});
		}

		printAdditionalCESSColumn = printAdditionalCESSColumn && (!printDeliveryChallan || settingCache.printAmountDetailsInDeliveryChallan());

		printItemCount = printItemCount && TransactionPrintSettings.printItemCountInInvoicePrint();
		printItemBatchNumber = printItemBatchNumber && TransactionPrintSettings.printItemBatchInInvoicePrint();
		printItemExpiryDate = printItemExpiryDate && TransactionPrintSettings.printItemExpiryDateInInvoicePrint();
		printItemManufactureDate = printItemManufactureDate && TransactionPrintSettings.printItemManufacturingDateInInvoicePrint();
		printItemMRP = printItemMRP && TransactionPrintSettings.printItemMRPInInvoicePrint();
		printSize = printSize && TransactionPrintSettings.printItemSizeInInvoicePrint();
		var printDescription = TransactionPrintSettings.printItemDescriptionInInvoicePrint();
		var printSerialNumber = TransactionPrintSettings.printItemSerialNumberInInvoicePrint();
		var printDiscountAmountValueInDiscountColumn = TransactionPrintSettings.printItemDiscountAmountInInvoicePrint() && (!printDeliveryChallan || settingCache.printAmountDetailsInDeliveryChallan());
		var printDiscountPercentValueInDiscountColumn = TransactionPrintSettings.printItemDiscountPercentageInInvoicePrint() && (!printDeliveryChallan || settingCache.printAmountDetailsInDeliveryChallan());

		printItemisedDiscountColumn = printItemisedDiscountColumn && (printDiscountAmountValueInDiscountColumn || printDiscountPercentValueInDiscountColumn);

		printItemUnitColumn = printItemUnitColumn && TransactionPrintSettings.printItemUnitInInvoicePrint();
		var printFinalItemPriceColumn = TransactionPrintSettings.printTaxInclusiveItemPriceInInvoicePrint() && (!printDeliveryChallan || settingCache.printAmountDetailsInDeliveryChallan());
		var printTotalAmountColumn = TransactionPrintSettings.printItemTotalAmountInInvoicePrint() && (!printDeliveryChallan || settingCache.printAmountDetailsInDeliveryChallan());
		var printTaxAmountValueInTaxColumn = TransactionPrintSettings.printItemTaxAmountInInvoicePrint() && (!printDeliveryChallan || settingCache.printAmountDetailsInDeliveryChallan());
		var printTaxPercentValueInTaxColumn = TransactionPrintSettings.printItemTaxPercentInInvoicePrint() && (!printDeliveryChallan || settingCache.printAmountDetailsInDeliveryChallan());

		var printTaxColumnsBasedOnSetting = printTaxAmountValueInTaxColumn || printTaxPercentValueInTaxColumn;

		printTotalTaxAmountColumn = printTotalTaxAmountColumn && theme != InvoiceTheme.THEME_7 && theme != InvoiceTheme.THEME_8 && printTaxColumnsBasedOnSetting;
		printIGSTTaxColumn = printIGSTTaxColumn && (theme == InvoiceTheme.THEME_7 || theme == InvoiceTheme.THEME_8) && printTaxColumnsBasedOnSetting;
		printCGSTTaxColumn = printCGSTTaxColumn && (theme == InvoiceTheme.THEME_7 || theme == InvoiceTheme.THEME_8) && printTaxColumnsBasedOnSetting;
		printSGSTTaxColumn = printSGSTTaxColumn && (theme == InvoiceTheme.THEME_7 || theme == InvoiceTheme.THEME_8) && printTaxColumnsBasedOnSetting;
		printCESSTaxColumn = printCESSTaxColumn && (theme == InvoiceTheme.THEME_7 || theme == InvoiceTheme.THEME_8) && printTaxColumnsBasedOnSetting;
		printOtherTaxColumn = printOtherTaxColumn && (theme == InvoiceTheme.THEME_7 || theme == InvoiceTheme.THEME_8) && printTaxColumnsBasedOnSetting;
		printStateSpecificCESSTaxColumn = printStateSpecificCESSTaxColumn && (theme == InvoiceTheme.THEME_7 || theme == InvoiceTheme.THEME_8) && printTaxColumnsBasedOnSetting;

		var printTaxableAmountColumn = (printIGSTTaxColumn || printCGSTTaxColumn || printSGSTTaxColumn || printCESSTaxColumn || printOtherTaxColumn || printStateSpecificCESSTaxColumn) && printTaxColumnsBasedOnSetting;

		var sNo = 1;

		var minimumLine = settingCache.getMinimumLineCountForTheme5();
		var txnItemTableForPrintClass = "class='txnItemTableForPrint'";
		if (theme == InvoiceTheme.THEME_0) {
			minimumLine = 0;
			txnItemTableForPrintClass = 0;
		}

		if (lineItems.length > 0 || minimumLine > 0) {
			var tableStyle = theme == InvoiceTheme.THEME_1 || theme == InvoiceTheme.THEME_2 || theme == InvoiceTheme.THEME_3 || theme == InvoiceTheme.THEME_10 ? " style='margin-bottom:" + 20 * textSizeRatio + "px;'" : '';

			if (theme == InvoiceTheme.THEME_12) {
				tableStyle = 'style= \'background-color:' + themeColor + '; margin-bottom: 0px;\'';
				txnItemTableForPrintClass = "class='txnItemTableForPrint theme10ItemTablePadding theme12ItemTable' ";
			}
			if (theme == InvoiceTheme.THEME_11) {
				txnItemTableForPrintClass = "class='txnItemTableForPrint theme10ItemTablePadding theme11ItemTable' ";
				tableStyle += 'style= \'background-color:' + themeColor + '; margin-bottom: 0px; border: 0px solid #FFFFFF; border-radius: 12px 12px 0px 12px;\'';
			}

			htmlText += '<table ' + txnItemTableForPrintClass + " width='100%' " + tableStyle + '><tbody>' + this.getItemTableHeader(firm, theme, themeColor, printSIColumn, printItemName, printItemQuantity, printItemPrice, printFinalItemPriceColumn, printTotalAmountColumn, printItemisedDiscountColumn, isHSNCodeEnabled, isItemCodeEnabled, printItemUnitColumn, printItemBatchNumber, printItemCount, printItemExpiryDate, printItemManufactureDate, printItemMRP, printSize, printTaxableItemPrice, printTotalTaxAmountColumn, printTaxableAmountColumn, printIGSTTaxColumn, printCGSTTaxColumn, printSGSTTaxColumn, printCESSTaxColumn, printOtherTaxColumn, printAdditionalCESSColumn, printStateSpecificCESSTaxColumn);
		}

		if (lineItems.length > 0) {
			for (i = 0; i < lineItems.length; i++) {
				htmlText += this.getItemRowHTML(lineItems[i], sNo, theme, themeColor, printSIColumn, printItemName, printItemQuantity, printItemPrice, printFinalItemPriceColumn, printTotalAmountColumn, printItemisedDiscountColumn, isHSNCodeEnabled, isItemCodeEnabled, printItemUnitColumn, printItemBatchNumber, printItemCount, printItemExpiryDate, printItemManufactureDate, printItemMRP, printSize, printDescription, printSerialNumber, printTaxableItemPrice, printTotalTaxAmountColumn, printTaxableAmountColumn, printIGSTTaxColumn, printCGSTTaxColumn, printSGSTTaxColumn, printCESSTaxColumn, printOtherTaxColumn, printAdditionalCESSColumn, printTaxAmountValueInTaxColumn, printTaxPercentValueInTaxColumn, printDiscountAmountValueInDiscountColumn, printDiscountPercentValueInDiscountColumn, printStateSpecificCESSTaxColumn);
				sNo++;
			}
		}

		if (minimumLine > 0) {
			var extraRowsToBeShown = minimumLine - lineItems.length;
			if (extraRowsToBeShown > 0) {
				for (var i = 0; i < extraRowsToBeShown; i++) {
					htmlText += this.getItemRowHTML(null, sNo, theme, themeColor, printSIColumn, printItemName, printItemQuantity, printItemPrice, printFinalItemPriceColumn, printTotalAmountColumn, printItemisedDiscountColumn, isHSNCodeEnabled, isItemCodeEnabled, printItemUnitColumn, printItemBatchNumber, printItemCount, printItemExpiryDate, printItemManufactureDate, printItemMRP, printSize, printDescription, printSerialNumber, printTaxableItemPrice, printTotalTaxAmountColumn, printTaxableAmountColumn, printIGSTTaxColumn, printCGSTTaxColumn, printSGSTTaxColumn, printCESSTaxColumn, printOtherTaxColumn, printAdditionalCESSColumn, printTaxAmountValueInTaxColumn, printTaxPercentValueInTaxColumn, printDiscountAmountValueInDiscountColumn, printDiscountPercentValueInDiscountColumn, printStateSpecificCESSTaxColumn);
					sNo++;
				}
			}
		}

		if (lineItems.length > 0 || minimumLine > 0) {
			if (settingCache.isPrintItemQuantityTotalEnabled() || theme == InvoiceTheme.THEME_0) {
				htmlText += this.getItemRowForTotalQuantity(theme, themeColor, printSIColumn, printItemName, printItemQuantity, printItemPrice, printFinalItemPriceColumn, printTotalAmountColumn, printItemisedDiscountColumn, isHSNCodeEnabled, isItemCodeEnabled, printItemUnitColumn, printItemBatchNumber, printItemCount, printItemExpiryDate, printItemManufactureDate, printItemMRP, printSize, printDescription, printSerialNumber, printTaxableItemPrice, printTotalTaxAmountColumn, printTaxableAmountColumn, printIGSTTaxColumn, printCGSTTaxColumn, printSGSTTaxColumn, printCESSTaxColumn, printOtherTaxColumn, printAdditionalCESSColumn, printTaxAmountValueInTaxColumn, printTaxPercentValueInTaxColumn, printDiscountAmountValueInDiscountColumn, printDiscountPercentValueInDiscountColumn, printStateSpecificCESSTaxColumn);
			}
			htmlText += '</tbody></table>';
		}

		return htmlText;
	};

	this.getCompanyLogoSource = function (firm, isForInvoicePreview) {
		var imageSrc = '';
		if (settingCache.isPrintLogoEnabled()) {
			var imageBlob = firm.getFirmImagePath();
			if (imageBlob) {
				imageSrc = 'data:image/png;base64,' + imageBlob;
			} else if (isForInvoicePreview) {
				imageSrc = './../Images/invoice_logo_sample.png';
			}
		}
		return imageSrc;
	};
};

module.exports = TransactionHTMLGenerator;