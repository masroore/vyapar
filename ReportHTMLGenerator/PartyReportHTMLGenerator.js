var PartyReportHTMLGenerator = function PartyReportHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');

	this.getHTMLText = function (nameList, printPhoneNumber, printEmail, printBalance, printAddress, printTIN) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'><h2 align='center'><u>Party Report</u></h2>" + getHTMLTable(nameList, printPhoneNumber, printEmail, printBalance, printAddress, printTIN);

		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(nameList, printPhoneNumber, printEmail, printBalance, printAddress, printTIN) {
		var len = nameList.length;

		var SettingCache = require('./../Cache/SettingCache.js');
		var settingCache = new SettingCache();

		var dynamicRow = '';
		var dynamicFooter = '';
		var totalReceivableAmount = 0;
		var totalPayableAmount = 0;

		var siColumnRatio = 8;
		var nameColumnRatio = 22;
		var phoneNumberColumnRation = printPhoneNumber || printEmail ? 14 : 0;
		var emailColumnRatio = printEmail ? 14 : 0;
		var addressColumnRatio = printAddress ? 22 : 0;
		var tinColumnRatio = printTIN ? 22 : 0;
		var receivableColumnRatio = printBalance ? 20 : 0;
		var payableColumnRatio = printBalance ? 20 : 0;

		var totalWidth = siColumnRatio + nameColumnRatio + phoneNumberColumnRation + emailColumnRatio + addressColumnRatio + tinColumnRatio + receivableColumnRatio + payableColumnRatio;

		dynamicRow += "<table class='reportTableForPrint' width='100%'><thead><tr style='background-color: lightgrey;'>" + "<th width='" + siColumnRatio * 100 / totalWidth + "%'></th><th width='" + nameColumnRatio * 100 / totalWidth + "%' class='tableCellTextAlignLeft'>Name</th>";
		dynamicFooter += "<tr style='background-color: lightgrey;'><th></th><th class='tableCellTextAlignLeft'>Total</th>";
		if (printEmail) {
			dynamicRow += "<th width='" + emailColumnRatio * 100 / totalWidth + "%' class='tableCellTextAlignLeft'>Email</th>";
			dynamicFooter += "<th class='tableCellTextAlignLeft'></th>";
		}
		if (printPhoneNumber) {
			dynamicRow += "<th width='" + phoneNumberColumnRation * 100 / totalWidth + "%' class='tableCellTextAlignLeft'>Phone No.</th>";
			dynamicFooter += "<th class='tableCellTextAlignLeft'></th>";
		}
		if (printAddress) {
			dynamicRow += "<th width='" + addressColumnRatio * 100 / totalWidth + "%' class='tableCellTextAlignLeft'>Address</th>";
			dynamicFooter += "<th class='tableCellTextAlignLeft'></th>";
		}
		if (printTIN) {
			if (settingCache.getGSTEnabled()) {
				dynamicRow += "<th width='" + tinColumnRatio * 100 / totalWidth + "%' class='tableCellTextAlignLeft'>GSTIN</th>";
			} else {
				dynamicRow += "<th width='" + tinColumnRatio * 100 / totalWidth + "%' class='tableCellTextAlignLeft'>" + settingCache.getTINText() + '</th>';
			}

			dynamicFooter += "<th class='tableCellTextAlignLeft'></th>";
		}
		if (printBalance) {
			dynamicRow += "<th width='" + receivableColumnRatio * 100 / totalWidth + "%' class='tableCellTextAlignRight'>Receivable Balance</th><th width='" + payableColumnRatio * 100 / totalWidth + "%' class='tableCellTextAlignRight'>Payable Balance</th>";
		}
		dynamicRow += '</tr></thead>';
		for (var i = 0; i < len; i++) {
			var nameModel = nameList[i];
			var row = '';
			row = '<tr><td>' + (i + 1) + '</td><td>' + nameModel.getFullName() + '</td>';
			if (printEmail) {
				row += '<td>' + nameModel.getEmail() + '</td>';
			}
			if (printPhoneNumber) {
				row += '<td>' + nameModel.getPhoneNumber() + '</td>';
			}
			if (printAddress) {
				var partyAddress = nameModel.getAddress();
				if (partyAddress) {
					partyAddress = partyAddress.replace(/(?:\r\n|\r|\n)/g, '<br/>');
				} else {
					partyAddress = '';
				}
				row += '<td>' + partyAddress + '</td>';
			}
			if (printTIN) {
				row += '<td>';
				if (settingCache.getGSTEnabled()) {
					var gstinNumber = nameModel.getGstinNumber();
					if (gstinNumber) {
						row += gstinNumber;
					}
				} else {
					var tinNumber = nameModel.getTinNumber();
					if (tinNumber) {
						row += tinNumber;
					}
				}
				row += '</td>';
			}
			if (printBalance) {
				if (nameModel.getAmount() < 0) {
					totalPayableAmount += nameModel.getAmount();
					row += "<td></td><td align='right'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(nameModel.getAmount()) + '</td>';
				} else {
					totalReceivableAmount += nameModel.getAmount();
					row += "<td align='right'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(nameModel.getAmount()) + '</td><td></td>';
				}
			}

			row += '</tr>';
			dynamicRow += row;
		}
		if (printBalance) {
			dynamicFooter += "<th  align='right'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalReceivableAmount) + "</th><th  align='right'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalPayableAmount) + '</th></tr>';
			dynamicRow += dynamicFooter;
		}
		dynamicRow += '</table>';
		return dynamicRow;
	};
};

module.exports = PartyReportHTMLGenerator;