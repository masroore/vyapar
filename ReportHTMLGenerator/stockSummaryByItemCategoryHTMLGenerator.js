var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var stockSummaryByItemCategoryHTMLGenerator = function stockSummaryByItemCategoryHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');
	var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
	var paymentInfoCache = new PaymentInfoCache();
	var NameCache = require('./../Cache/NameCache.js');
	var nameCache = new NameCache();

	this.getHTMLText = function (transactionList) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'><h2 align='center'><u>Stock Summary By Item Category Report</u></h2>" + getHTMLTable(transactionList);
		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(listOfTransactions) {
		var dynamicRow = '';
		dynamicRow += "<table class='reportTableForPrint' width='100%'><thead><tr style='background-color: lightgrey;'><th width='33%'>Item Category</th><th width='33%' class='tableCellTextAlignRight'>Stock Quantity </th><th width='33%' class='tableCellTextAlignRight'>Stock Value</th></tr></thead>";

		//
		var totalStockQuantity = 0;
		var totalStockValue = 0;
		var _iteratorNormalCompletion = true;
		var _didIteratorError = false;
		var _iteratorError = undefined;

		try {
			for (var _iterator = (0, _getIterator3.default)(listOfTransactions), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
				var _ref = _step.value;

				var _ref2 = (0, _slicedToArray3.default)(_ref, 2);

				var key = _ref2[0];
				var value = _ref2[1];

				var itemLine = listOfTransactions.get(key);
				var itemName = itemLine.get('name');
				var stockQty = itemLine.get('qty');
				var stockValue = itemLine.get('amount');
				var row = "<tr><td width='33%'>" + itemName + '</td>';

				row += "<td class='tableCellTextAlignRight' width='33%'>" + MyDouble.getQuantityWithDecimalWithoutColor(stockQty) + '</td>';
				row += "<td class='tableCellTextAlignRight' width='33%'>" + MyDouble.getQuantityWithDecimalWithoutColor(stockValue) + '</td>';

				row += '</tr>';
				totalStockQuantity += stockQty;
				totalStockValue += stockValue;
				dynamicRow += row;
			}
		} catch (err) {
			_didIteratorError = true;
			_iteratorError = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion && _iterator.return) {
					_iterator.return();
				}
			} finally {
				if (_didIteratorError) {
					throw _iteratorError;
				}
			}
		}

		dynamicRow += '</table>';
		dynamicRow += "<table width='100%'><tr style='background-color: lightgrey;'><th width='33%' class='tableCellTextAlignLeft'>Total</th><th width='33%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(totalStockQuantity) + "</th><th width='33%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(totalStockValue) + '</th></tr></table>';

		return dynamicRow;
	};
};

module.exports = stockSummaryByItemCategoryHTMLGenerator;