var ExpenseReportHTMLGenerator = function ExpenseReportHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');
	var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
	var paymentInfoCache = new PaymentInfoCache();

	this.getHTMLText = function (transactionList, fromDate, toDate, selectedFirmId, selectedExpenseCategoryName, shouldPrintItemDetails, shouldPrintDescription) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'><h2 align='center'><u>Expense Report</u></h2>" + ReportHTMLStyleSheet.getHTMLTextForDuration(fromDate, toDate) + ReportHTMLStyleSheet.getHTMLTextForFirm(selectedFirmId) + '<h3>Expense category: ' + selectedExpenseCategoryName + '</h3>' + getHTMLTable(transactionList, shouldPrintItemDetails, shouldPrintDescription);
		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(listOfTransactions, shouldPrintItemDetails, shouldPrintDescription) {
		var len = listOfTransactions.length;
		var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
		var TransactionHTMLGenerator = require('./../ReportHTMLGenerator/TransactionHTMLGenerator.js');
		var transactionHTMLGenerator = new TransactionHTMLGenerator();
		var dynamicRow = '';

		dynamicRow += "<table class='reportTableForPrint' width='100%'><thead><tr style='background-color: lightgrey;'><th width='18%' class='tableCellTextAlignLeft'>Date</th><th width='50%' class='tableCellTextAlignLeft'>Expense Category</th><th width='32%' class='tableCellTextAlignRight'>Paid Amount</th></tr></thead>";

		var totalAmount = 0;

		for (var i = 0; i < len; i++) {
			var txn = listOfTransactions[i];
			var printItemDetails = false;
			var printDescription = false;
			var classToBeIncluded = '';
			var txnItemLines = txn.getLineItems();
			var txnType = txn.getTxnType();
			printItemDetails = shouldPrintItemDetails && txnItemLines && txnItemLines.length && !TxnTypeConstant.isLoanTxnType(txnType);
			printDescription = shouldPrintDescription && txn.getDescription() && !TxnTypeConstant.isLoanTxnType(txnType);

			var classToBeIncludedInStatementRow = '';

			if (shouldPrintItemDetails || shouldPrintDescription) {
				classToBeIncludedInStatementRow += 'boldText extraTopPadding ';
				if (printItemDetails || printDescription) {
					classToBeIncludedInStatementRow += ' noBorder ';
				}
				classToBeIncludedInStatementRow = " class='" + classToBeIncludedInStatementRow + "'";
			}

			var classToBeIncludedInItemDetailRow = '';
			if (printDescription) {
				classToBeIncludedInItemDetailRow = " class='noBorder' ";
			}

			var name = TxnTypeConstant.isLoanTxnType(txn.getTxnType()) ? txn.getDescription() : txn.getNameRef().getFullName();
			var typeString = TxnTypeConstant.getTxnTypeForUI(txnType);

			var cashAmount = Number(txn.getCashAmount());
			var date = MyDate.getDate('d/m/y', txn.getTxnDate());
			totalAmount += cashAmount;

			var row = '<tr><td ' + classToBeIncludedInStatementRow + '>' + date + '</td>';
			row += '<td ' + classToBeIncludedInStatementRow + '>' + name + "</td><td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(cashAmount) + '</td></tr>';

			dynamicRow += row;

			if (printItemDetails) {
				dynamicRow += '<tr>\n' + '  <td ' + classToBeIncludedInItemDetailRow + ' colspan="1" ></td>\n' + '  <td ' + classToBeIncludedInItemDetailRow + ' colspan="2">' + transactionHTMLGenerator.getItemDetailsForReport(txn) + '</td>\n' + '</tr>\n';
			}
			if (printDescription) {
				dynamicRow += '<tr>\n' + '  <td colspan="3"> <span class="boldText"> Description: </span>' + txn.getDescription() + '</td>\n' + '</tr>\n';
			}
		}
		dynamicRow += '</table>';

		dynamicRow += "<table width='100%'><tr style='background-color: lightgrey;'><th width='18%'></th><th width='50%' class='tableCellTextAlignLeft'>Total</th><th width='32%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalAmount) + '</th></tr></table>';

		return dynamicRow;
	};
};

module.exports = ExpenseReportHTMLGenerator;