var ItemStockTrackingReportHTMLGenerator = function ItemStockTrackingReportHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');

	this.getHTMLText = function (transactionList) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'><h2 align='center'><u>Item Stock Tracking Report</u></h2>" +
		// "<h3>Item name: " + nameString + "</h3>" +
		// ReportHTMLStyleSheet.getHTMLTextForDuration(fromDate,toDate) +
		getHTMLTable(transactionList);

		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(listOfTransactions) {
		style = 'background-color: lightgrey;';
		var len = listOfTransactions.length;
		var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
		var dynamicRow = '';

		var mrpColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_MRP);
		var batchNumberColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_BATCH_NUMBER);
		var serialNumberColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_SERIAL_ITEM_NUMBER);
		var mfgDateColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_MANUFACTURING_DATE);
		var expDateColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_EXPIRY_DATE);
		var sizeColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_SIZE);

		dynamicRow += "<table width='100%'><thead><tr style='background-color: lightgrey;'><th width='12%' class='tableCellTextAlignLeft'>Item Name</th>";
		if (mrpColumnVisibility) {
			dynamicRow += "<th width='11%' class='tableCellTextAlignRight'> " + settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_MRP_VALUE) + ' </th>';
		}
		if (batchNumberColumnVisibility) {
			dynamicRow += "<th width='11%' class='tableCellTextAlignRight'> " + settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_BATCH_NUMBER_VALUE) + ' </th>';
		}
		if (serialNumberColumnVisibility) {
			dynamicRow += "<th width='11%' class='tableCellTextAlignRight'> " + settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_SERIAL_NUMBER_VALUE) + ' </th>';
		}
		if (mfgDateColumnVisibility) {
			dynamicRow += "<th width='11%' class='tableCellTextAlignRight'> " + settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_MANUFACTURING_DATE_VALUE) + ' </th>';
		}
		if (expDateColumnVisibility) {
			dynamicRow += "<th width='11%' class='tableCellTextAlignRight'> " + settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_EXPIRY_DATE_VALUE) + ' </th>';
		}
		if (sizeColumnVisibility) {
			dynamicRow += "<th width='11%' class='tableCellTextAlignRight'> " + settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_SIZE_VALUE) + ' </th>';
		}

		dynamicRow += "<th width='11%' class='tableCellTextAlignRight'>Current Quantity</th></tr></thead>";

		for (var i = 0; i < len; i++) {
			var row = '';
			var txn = listOfTransactions[i];
			var batchNo = txn.getIstBatchNumber();
			var serialNo = txn.getIstSerialNumber();
			var mrp = txn.getIstMrp();
			var expDate = '';
			var mfgDate = '';
			var size = txn.getIstSize();
			var currentQuantity = txn.getIstCurrentQuantity();
			var itemId = txn.getIstItemId();
			var ItemCache = require('./../Cache/ItemCache.js');
			var itemCache = new ItemCache();
			var itemName = itemCache.findItemNameById(itemId);

			if (txn.getIstExpiryDate() && settingCache.getExpiryDateFormat() == StringConstant.monthYear) {
				expDate = MyDate.getDate('m/y', txn.getIstExpiryDate());
			} else if (txn.getIstExpiryDate() && settingCache.getExpiryDateFormat() == StringConstant.dateMonthYear) {
				expDate = MyDate.getDate('d/m/y', txn.getIstExpiryDate());
			}

			if (txn.getIstManufacturingDate() && settingCache.getManufacturingDateFormat() == StringConstant.monthYear) {
				mfgDate = MyDate.getDate('m/y', txn.getIstManufacturingDate());
			} else if (txn.getIstManufacturingDate() && settingCache.getManufacturingDateFormat() == StringConstant.dateMonthYear) {
				mfgDate = MyDate.getDate('d/m/y', txn.getIstManufacturingDate());
			}

			row += "<tr class='currentRow'><td class='tableCellTextAlignLeft' width='12%'> " + itemName + ' </td>';
			if (mrpColumnVisibility) {
				row += "<td class='tableCellTextAlignRight' width='11%'> " + MyDouble.getAmountWithDecimal(mrp) + ' </td>';
			}
			if (batchNumberColumnVisibility) {
				row += "<td class='tableCellTextAlignRight' width='11%'> " + batchNo + ' </td>';
			}
			if (serialNumberColumnVisibility) {
				row += "<td class='tableCellTextAlignRight' width='11%'> " + serialNo + ' </td>';
			}
			if (mfgDateColumnVisibility) {
				row += "<td class='tableCellTextAlignRight' width='11%'> " + mfgDate + ' </td>';
			}
			if (expDateColumnVisibility) {
				row += "<td class='tableCellTextAlignRight' width='11%'> " + expDate + ' </td>';
			}
			if (sizeColumnVisibility) {
				row += "<td class='tableCellTextAlignRight' width='11%'> " + size + ' </td>';
			}
			row += "<td class='tableCellTextAlignRight' width='11%'>" + MyDouble.getQuantityWithDecimalWithoutColor(currentQuantity) + '</td></tr>';

			dynamicRow += row;
		}
		dynamicRow += '</table>';

		return dynamicRow;
	};
};

module.exports = ItemStockTrackingReportHTMLGenerator;