var StockDetailReportHTMLGenerator = function StockDetailReportHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');

	this.getHTMLText = function (transactionList, fromDate, toDate, itemCategoryId) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'><h2 align='center'><u>Stock Detail Report</u></h2>";
		if (itemCategoryId) {
			var ItemCategoryCache = require('./../Cache/ItemCategoryCache.js');
			var itemCategoryCache = new ItemCategoryCache();
			var categoryObj = itemCategoryCache.getItemCategoryObjectById(itemCategoryId);
			var categoryName = categoryObj.getCategoryName();
			htmlText += '<h3>Item Category: ' + categoryName + '</h3>';
		}
		htmlText += ReportHTMLStyleSheet.getHTMLTextForDuration(fromDate, toDate) + getHTMLTable(transactionList);

		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(listOfTransactions) {
		var len = listOfTransactions.length;
		var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
		var dynamicRow = '';

		var ItemCache = require('./../Cache/ItemCache.js');
		var itemCache = new ItemCache();

		dynamicRow += "<table width='100%'><thead><tr style='background-color: lightgrey;'><th width='20%'>Item name</th><th width='20%' class='tableCellTextAlignRight'>Opening Quantity</th><th width='20%' class='tableCellTextAlignRight'>Quantity In</th><th width='20%' class='tableCellTextAlignRight'>Quantity Out</th><th width='20%' class='tableCellTextAlignRight'>Closing Quantity</th></tr></tr></thead>";

		for (var i = 0; i < len; i++) {
			var row = '';
			var txn = listOfTransactions[i];
			var itemName = txn.getItemName();
			var itemObj = itemCache.findItemByName(itemName);
			var isItemService = itemObj.isItemService();
			var openingQuantity = MyDouble.getQuantityWithDecimalWithoutColor(txn.getOpeningQuantity());
			var quantityIn = txn.getQuantityIn();
			var quantityOut = txn.getQuantityOut();
			var closingQuantity = MyDouble.getQuantityWithDecimalWithoutColor(openingQuantity + quantityIn - quantityOut);

			if (isItemService) {
				openingQuantity = '';
				closingQuantity = '';
			}
			row += "<tr class='currentRow'><td width='20%'>" + itemName + "</td><td width='20%' class='tableCellTextAlignRight'>" + openingQuantity + "</td><td width='20%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(quantityIn) + "</td><td width='20%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(quantityOut) + "</td><td width='20%' class='tableCellTextAlignRight'>" + closingQuantity + '</td></tr>';
			dynamicRow += row;
		}
		dynamicRow += '</table>';

		return dynamicRow;
	};
};

module.exports = StockDetailReportHTMLGenerator;