var ItemReportByPartyHTMLGenerator = function ItemReportByPartyHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');

	this.getHTMLText = function (transactionList, fromDate, toDate, selectedFirmId, nameString) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'><h2 align='center'><u>Item By Party Report</u></h2>" + '<h3>Party name: ' + nameString + '</h3>' + ReportHTMLStyleSheet.getHTMLTextForDuration(fromDate, toDate) + ReportHTMLStyleSheet.getHTMLTextForFirm(selectedFirmId) + getHTMLTable(transactionList);

		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(listOfTransactions) {
		var len = listOfTransactions.length;
		var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
		var dynamicRow = '';

		dynamicRow += "<table class='reportTableForPrint' width='100%'><thead><tr style='background-color: lightgrey;'><th width='32%'>Item Name</th><th width='17%' class='tableCellTextAlignRight'>Sale Quantity</th><th width='17%' class='tableCellTextAlignRight'>Sale Amount</th><th width='17%' class='tableCellTextAlignRight'>Purchase Quantity</th><th width='17%' class='tableCellTextAlignRight'>Purchase Amount</th></tr></thead>";

		var totalSaleQuantity = 0;
		var totalPurchaseQuantity = 0;
		var totalSaleFreeQuantity = 0;
		var totalPurchaseFreeQuantity = 0;
		var totalSaleAmount = 0;
		var totalPurchaseAmount = 0;
		for (var i = 0; i < len; i++) {
			var row = '';
			var txn = listOfTransactions[i];
			var itemName = txn['itemName'];
			var saleQuantity = txn['saleQuantity'];
			var purchaseQuantity = txn['purchaseQuantity'];
			var saleFreeQuantity = txn['saleFreeQuantity'];
			var purchaseFreeQuantity = txn['purchaseFreeQuantity'];
			var saleAmount = txn['saleAmount'];
			var purchaseAmount = txn['purchaseAmount'];
			row += "<tr><td width='32%'>" + itemName + "</td><td width='17%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(saleQuantity) + MyDouble.quantityDoubleToStringWithSignExplicitly(saleFreeQuantity, true) + "</td><td width='17%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(saleAmount) + "</td><td width='17%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(purchaseQuantity) + MyDouble.quantityDoubleToStringWithSignExplicitly(purchaseFreeQuantity, true) + "</td><td width='17%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(purchaseAmount) + '</td><tr>';
			dynamicRow += row;
			totalSaleQuantity += saleQuantity;
			totalPurchaseQuantity += purchaseQuantity;
			totalSaleFreeQuantity += saleFreeQuantity;
			totalPurchaseFreeQuantity += purchaseFreeQuantity;
			totalSaleAmount += saleAmount;
			totalPurchaseAmount += purchaseAmount;
		}
		dynamicRow += '</table>';
		dynamicRow += "<table width='100%'><tr style='background-color: lightgrey;'><th width='32%' class='tableCellTextAlignLeft'>Total</th><th width='17%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(totalSaleQuantity) + MyDouble.quantityDoubleToStringWithSignExplicitly(totalSaleFreeQuantity, true) + "</th><th width='17%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalSaleAmount) + "</th><th width='17%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(totalPurchaseQuantity) + MyDouble.quantityDoubleToStringWithSignExplicitly(totalPurchaseFreeQuantity, true) + "</th><th width='17%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalPurchaseAmount) + '</th></tr></table>';

		return dynamicRow;
	};
};

module.exports = ItemReportByPartyHTMLGenerator;