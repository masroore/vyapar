var ItemDetailReportHTMLGenerator = function ItemDetailReportHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');

	this.getHTMLText = function (transactionList, fromDate, toDate, nameString) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'><h2 align='center'><u>Item Detail Report</u></h2>" + '<h3>Item name: ' + nameString + '</h3>' + ReportHTMLStyleSheet.getHTMLTextForDuration(fromDate, toDate) + getHTMLTable(transactionList);

		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(listOfTransactions) {
		var len = listOfTransactions.length;
		var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
		var dynamicRow = '';

		dynamicRow += "<table width='100%'><thead><tr style='background-color: lightgrey;'><th width='20%'>Date</th><th width='20%' class='tableCellTextAlignRight'>Sale Quantity</th><th width='20%' class='tableCellTextAlignRight'>Purchase Quantity</th><th width='20%' class='tableCellTextAlignRight'>Adjustment Quantity</th><th width='20%' class='tableCellTextAlignRight'>Closing Quantity</th></tr></tr></thead>";

		for (var i = 0; i < len; i++) {
			var row = '';
			var txn = listOfTransactions[i];
			var date = MyDate.convertDateToStringForUI(txn.getDate());
			var saleQuantity = txn.getSaleQuantity();
			var purchaseQuantity = txn.getPurchaseQuantity();
			var adjustmentQuantity = txn.getAdjustmentQuantity();
			var closingQuantity = txn.getClosingQuantity();
			var saleFreeQuantity = txn.getSaleFreeQuantity();
			var purchaseFreeQuantity = txn.getPurchaseFreeQuantity();

			if (txn.isForwardedStock()) {
				row += "<tr class='currentRow'><td width='20%'>" + date + "</td><td width='20%' class=''></td><td width='20%' class='tableCellTextAlignRight' >Beginning stock</td><td width='20%' class='tableCellTextAlignRight'></td><td width='20%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(closingQuantity) + '</td></tr>';
			} else {
				row += "<tr class='currentRow'><td width='20%'>" + date + "</td><td width='20%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(saleQuantity) + MyDouble.quantityDoubleToStringWithSignExplicitly(saleFreeQuantity, true) + "</td><td width='20%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(purchaseQuantity) + MyDouble.quantityDoubleToStringWithSignExplicitly(purchaseFreeQuantity, true) + "</td><td width='20%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(adjustmentQuantity) + "</td><td width='20%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(closingQuantity) + '</td></tr>';
			}
			dynamicRow += row;
		}
		dynamicRow += '</table>';

		return dynamicRow;
	};
};

module.exports = ItemDetailReportHTMLGenerator;