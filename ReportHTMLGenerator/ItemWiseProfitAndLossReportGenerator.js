var CustomReportHTMLGenerator = function CustomReportHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');
	var MyDouble = require('./../Utilities/MyDouble.js');

	this.getHTMLText = function (transactionList, fromDate, toDate, selectedFirmId, nameString) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'><h2 align='center'><u>Item Wise Profit And Loss</u></h2>" + ReportHTMLStyleSheet.getHTMLTextForDuration(fromDate, toDate) + ReportHTMLStyleSheet.getHTMLTextForFirm(selectedFirmId) + getHTMLTable(transactionList);

		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(listOfTransactions) {
		var len = listOfTransactions.length;
		var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
		var rowData = [];
		var dynamicRow = "<table class='reportTableForPrint'>";
		dynamicRow += "<thead><tr style='background-color: lightgrey;'><th width='12%'>Item Name</th>" + "<th width='10%' class='tableCellTextAlignRight'>Sale</th>" + "<th width='10%' class='tableCellTextAlignRight'>Cr. Note / Sale return</th>" + "<th width='10%' class='tableCellTextAlignRight'>Purchase</th>" + "<th width='10%' class='tableCellTextAlignRight'>Dr. Note / Purchase return</th>" + "<th width='10%' class='tableCellTextAlignRight'>Opening Stock</th>" + "<th width='10%' class='tableCellTextAlignRight'>Closing Stock</th>" + "<th width='9%' class='tableCellTextAlignRight'>Tax Receivable</th>" + "<th width='9%' class='tableCellTextAlignRight'>Tax Payable</th>" + "<th width='10%' class='tableCellTextAlignRight'>Net Profit/Loss</th>" + '</tr></thead>';
		//
		var totalNetAmount = 0;
		for (var i = 0; i < len; i++) {
			var row = '';
			var txn = listOfTransactions[i];
			var item = txn.getItemName();
			var saleVal = txn.getSaleValue();
			var saleRetVal = txn.getSaleReturnValue();
			var purchaseVal = txn.getPurchaseValue();
			var purchaseReturnValue = txn.getPurchaseReturnValue();
			var openingStock = txn.getOpeningStockValue();
			var closingStock = txn.getClosingStockValue();
			txn.calculateProfitAndLossAmount();
			var taxReceivable = txn.getReceivableTax();
			var taxPayable = txn.getPayableTax();
			var netAmount = txn.getNetProfitAndLossAmount();
			row += "<tr class='currentRow'><td>" + item + '</td>' + "<td class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimal(saleVal) + '</td>' + "<td class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimal(saleRetVal) + '</td>' + "<td class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimal(purchaseVal) + '</td>' + "<td class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimal(purchaseReturnValue) + '</td>' + "<td class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimal(openingStock) + '</td>' + "<td class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimal(closingStock) + '</td>' + "<td class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimal(taxReceivable) + '</td>' + "<td class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimal(taxPayable) + '</td>' + "<td class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimalAndCurrency(netAmount, true) + '</td></tr>';
			dynamicRow += row;
			totalNetAmount += MyDouble.convertStringToDouble(netAmount);
		}

		dynamicRow += "<tr class='currentRow'><td>Total</td><td class='tableCellTextAlignRight'></td><td class='tableCellTextAlignRight'></td><td class='tableCellTextAlignRight'></td>" + "<td class='tableCellTextAlignRight'></td><td class='tableCellTextAlignRight'></td><td class='tableCellTextAlignRight'></td><td class='tableCellTextAlignRight'></td>" + "<td class='tableCellTextAlignRight'></td><td class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimalAndCurrency(totalNetAmount, true) + '</td></tr>';

		dynamicRow += '</table>';

		return dynamicRow;
	};
};

module.exports = CustomReportHTMLGenerator;