var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var GSTR3BReportHTMLGenerator = function GSTR3BReportHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');
	var NameCache = require('./../Cache/NameCache.js');
	var nameCache = new NameCache();

	this.getHTMLText = function (GSTR_3_B_data) {
		var SettingCache = require('./../Cache/SettingCache.js');
		var settingCache = new SettingCache();
		var dateObj = GSTR_3_B_data.start_date;
		var month = MyDate.getMonthName(dateObj);
		var year = dateObj.getFullYear();

		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView' ><h2 align='center'><u>GSTR 3B Report (" + month + '-' + year + ')</u></h2>' + getHTMLTable(GSTR_3_B_data);
		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(GSTR_3_B_data) {
		var row = "<div class='padbottom' style='padding-top:50px;'>1. Details of outward supplies and inward supplies liable to reverse charge</div>";
		row += '<table style="width: 100%;">';
		row += "<thead><tr style='background-color: lightgrey;'><th width='40%'>Nature of supplies</th><th width='10%' class='tableCellTextAlignRight'>Total taxable value</th><th width='12%' class='tableCellTextAlignRight'>Integrated Tax</th><th width='12%' class='tableCellTextAlignRight'>Central Tax</th><th width='12%' class='tableCellTextAlignRight'>State/UT Tax</th><th width='12%' class='tableCellTextAlignRight'>Cess</th></tr></thead>";
		row += '<tbody>';
		row += "<tr><td>Outward taxable supplies (other than zero rated, nil rated and exempted)</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.totalTaxableValueOutwardTaxableSuppliesotherThanZNE) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.IGSTOutwardTaxableSuppliesotherThanZNE) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CGSTOutwardTaxableSuppliesotherThanZNE) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.SGSTOutwardTaxableSuppliesotherThanZNE) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CESSOutwardTaxableSuppliesotherThanZNE) + '</td></tr>';
		row += "<tr><td>Outward taxable supplies (zero rated)</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td></tr>";
		row += "<tr><td> Other outward supplies (nil rated, exempted)</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.totalTaxableValueOtherOutwardSuppliesNE) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.IGSTOtherOutwardSuppliesNE) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CGSTOtherOutwardSuppliesNE) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.SGSTOtherOutwardSuppliesNE) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CESSOtherOutwardSuppliesNE) + '</td></tr>';
		row += "<tr><td>Inward supplies (liable to reverse charge)</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.totalTaxableValueInwardSuppliesRC) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.IGSTInwardSuppliesRC) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CGSTInwardSuppliesRC) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.SGSTInwardSuppliesRC) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CESSInwardSuppliesRC) + '</td></tr>';
		row += "<tr><td>Non-GST outward supplies</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td></tr>";
		row += '</tbody></table>';

		row += "<div class='padbottom' style='padding-top:50px;'>2. Details of inter-State supplies made to unregistered persons, composition dealer and UIN holders</div>";
		row += '<table style="width: 100%;">';
		row += "<thead><tr style='background-color: lightgrey; border: none'><th width='20%' rowspan='2'>Place of supply (State/UT)</th><th colspan='2' width='30%' class='tableCellTextAlignRight' style='border-bottom: none;'>Supplies made to unregistered persons</th><th colspan='2' width='30%' class='tableCellTextAlignRight' style='border-bottom: none;'>Supplies made to composition taxable persons</th><th colspan='2' width='20%' class='tableCellTextAlignRight' style='border-bottom: none;'>Supplies made to UIN holders</th></tr>";
		row += "<tr style='background-color: lightgrey;'><th width='15%' class='tableCellTextAlignRight'>Total taxable value</th><th width='15%' class='tableCellTextAlignRight'>Amount of integrated tax</th><th width='15%' class='tableCellTextAlignRight'>Total taxable value</th><th width='15%' class='tableCellTextAlignRight'>Amount of integrated tax</th><th width='10%' class='tableCellTextAlignRight'>Total taxable value</th><th width='10%' class='tableCellTextAlignRight'>Amount of integrated tax</th></tr></thead>";
		row += '<tbody>';
		if (GSTR_3_B_data.interStateSupplyMap.size == 0) {
			row += "<tr><td></td><td class='tableCellTextAlignRight'></td><td class='tableCellTextAlignRight'></td><td class='tableCellTextAlignRight'></td><td class='tableCellTextAlignRight'></td><td class='tableCellTextAlignRight'></td><td class='tableCellTextAlignRight'></td></tr>";
		} else {
			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;

			try {
				for (var _iterator = (0, _getIterator3.default)(GSTR_3_B_data.interStateSupplyMap.entries()), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					var value = _step.value;

					var POS = value[0];
					var taxableValueUnregistered = value[1]['unregistered'][0];
					var IGSTAmountUnregistered = value[1]['unregistered'][1];
					var taxableValueComposite = value[1]['composite'][0];
					var IGSTAmountComposite = value[1]['composite'][1];
					row += '<tr><td>' + POS + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(taxableValueUnregistered) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(IGSTAmountUnregistered) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(taxableValueComposite) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(IGSTAmountComposite) + "</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td></tr>";
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}
		}

		row += '</tbody></table>';

		row += "<div class='padbottom' style='padding-top:50px;'>3. Details of eligible Input Tax Credit</div>";
		row += '<table style="width: 100%;">';
		row += "<thead><tr style='background-color: lightgrey;'><th width='40%'>Details</th><th width='15%' class='tableCellTextAlignRight'>Integrated Tax</th><th width='15%' class='tableCellTextAlignRight'>Central Tax</th><th width='15%' class='tableCellTextAlignRight'>State/UT Tax</th><th width='15%' class='tableCellTextAlignRight'>Cess</th></tr></thead>";
		row += '<tbody>';
		row += "<tr><td style='font-weight: bold;'>(A) ITC Available (whether in full or part)</td><td colspan='4'></td></tr>";
		row += "<tr><td style='padding-left: 20px;'>(1) Import of goods</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td></tr>";
		row += "<tr><td style='padding-left: 20px;'>(2) Import of services</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td></tr>";
		row += "<tr><td style='padding-left: 20px;'>(3) Inward supplies liable to reverse charge (other than 1 & 2 above)</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.IGSTInwardSuppliesRCITC) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CGSTInwardSuppliesRCITC) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.SGSTInwardSuppliesRCITC) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CESSInwardSuppliesRCITC) + '</td></tr>';
		row += "<tr><td style='padding-left: 20px;'>(4) Inward supplies from ISD</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td></tr>";
		row += "<tr><td style='padding-left: 20px;'>(5) All other ITC</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.IGSTinwardSuppliesAllOtherITC) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CGSTinwardSuppliesAllOtherITC) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.SGSTinwardSuppliesAllOtherITC) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CESSinwardSuppliesAllOtherITC) + '</td></tr>';
		row += "<tr><td style='font-weight: bold;'>(D) Ineleigible ITC</td><td colspan='4'></td></tr>";
		row += "<tr><td style='padding-left: 20px;'>(1) As per section 17(5)</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.IGSTineligibleITC175) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CGSTineligibleITC175) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.SGSTineligibleITC175) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CESSineligibleITC175) + '</td></tr>';
		row += "<tr><td style='padding-left: 20px;'>(2) Others</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.IGSTineligibleITCothers) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CGSTineligibleITCothers) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.SGSTineligibleITCothers) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CESSineligibleITCothers) + '</td></tr>';
		row += '</tbody></table>';

		row += "<div class='padbottom' style='padding-top:50px;'>4. Details of exempt, nil-rated and non-GST inward supplies</div>";
		row += '<table style="width: 100%;">';
		row += "<thead><tr style='background-color: lightgrey;'><th width='50%'>Nature of supplies</th><th width='25%' class='tableCellTextAlignRight'>Inter-State supplies</th><th width='25%' class='tableCellTextAlignRight'>Intra-State supplies</th></tr></thead>";
		row += '<tbody>';
		row += "<tr><td>From a supplier under composition scheme, Exempt and Nil rated supply </td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.interStateCompositiontaxableValue) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.intraStateCompositiontaxableValue) + '</td></tr>';
		row += "<tr><td>Non GST supply</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td></tr>";
		row += '</tbody></table>';

		if (GSTR_3_B_data.stateSpecificCESSMap && GSTR_3_B_data.stateSpecificCESSMap.size > 0) {
			var columnStyle = 'borderBottomForTxn';
			row += "<div class='padbottom' style='padding-top:50px;'>PARTICULARS OF Flood CESS PAYABLE</div>";
			row += '<table style="width: 100%;">';
			row += "<thead><tr style='background-color: lightgrey;'> <th rowspan='2' class='" + columnStyle + "' align='center' width='7%'>SI No.</th>" + "<th rowspan='2' class='" + columnStyle + "' align='center' width='20%'>Category of supply</th>" + "<th colspan='3' class='" + columnStyle + "' width='45%' align='center'>Value of intra-state supply</th>" + "<th rowspan='2' class='" + columnStyle + "' align='center' width='18%'>Rate of Flood CESS on value of supply</th>" + "<th rowspan='2' class='" + columnStyle + "' width='30%' align='center'>Flood CESS Payable</th></tr>" + "<tr style='background-color: lightgrey'>" + "<th  class='" + columnStyle + "' width='15%' align='center'>To taxable person having GST registration in the State, not in furtherance of business</th>" + "<th  class='" + columnStyle + "' width='15%' align='center'>To Unregistered person</th>" + "<th  class='" + columnStyle + "' width='15%' align='center'>Total</th></tr></thead>";

			var siNo = 0;
			var _iteratorNormalCompletion2 = true;
			var _didIteratorError2 = false;
			var _iteratorError2 = undefined;

			try {
				for (var _iterator2 = (0, _getIterator3.default)(GSTR_3_B_data.stateSpecificCESSMap.entries()), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
					var stateCESSRateEntry = _step2.value;

					var stateCESSRate = stateCESSRateEntry[0];
					var sgstMap = stateCESSRateEntry[1];
					var _iteratorNormalCompletion3 = true;
					var _didIteratorError3 = false;
					var _iteratorError3 = undefined;

					try {
						for (var _iterator3 = (0, _getIterator3.default)(sgstMap.entries()), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
							var sgstRateEntry = _step3.value;

							siNo++;
							var sgstRate = sgstRateEntry[0];
							var stateCESSValues = sgstRateEntry[1];
							row += "<tr><td class='" + columnStyle + "'> " + siNo + '</td>' + "<td class='" + columnStyle + "'> Taxable supply at the rate of " + MyDouble.getAmountDecimalForGSTRReport(sgstRate) + '% SGST</td>' + "<td class='" + columnStyle + "' align='right'>" + MyDouble.getAmountDecimalForGSTRReport(stateCESSValues[0]) + '</td>' + "<td class='" + columnStyle + "' align='right'>" + MyDouble.getAmountDecimalForGSTRReport(stateCESSValues[1]) + '</td>' + "<td class='" + columnStyle + "' align='right'>" + MyDouble.getAmountDecimalForGSTRReport(stateCESSValues[0] + stateCESSValues[1]) + '</td>' + "<td class='" + columnStyle + "' align='right'>" + MyDouble.getAmountDecimalForGSTRReport(stateCESSRate) + '</td>' + "<td class='" + columnStyle + "' align='right'>" + MyDouble.getAmountDecimalForGSTRReport(stateCESSValues[2]) + '</td></tr>';
						}
					} catch (err) {
						_didIteratorError3 = true;
						_iteratorError3 = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion3 && _iterator3.return) {
								_iterator3.return();
							}
						} finally {
							if (_didIteratorError3) {
								throw _iteratorError3;
							}
						}
					}
				}
			} catch (err) {
				_didIteratorError2 = true;
				_iteratorError2 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion2 && _iterator2.return) {
						_iterator2.return();
					}
				} finally {
					if (_didIteratorError2) {
						throw _iteratorError2;
					}
				}
			}

			row += '</table>';
		}

		return row;
	};
};

module.exports = GSTR3BReportHTMLGenerator;