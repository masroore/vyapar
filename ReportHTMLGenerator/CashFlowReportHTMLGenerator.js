var CashFlowReportHTMLGenerator = function CashFlowReportHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');
	var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
	var paymentInfoCache = new PaymentInfoCache();

	this.getHTMLText = function (transactionList, openingCashInHand, closingCashInHand, fromDate, toDate, selectedFirmId, shouldPrintItemDetails, shouldPrintDescription) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'><h2 align='center'><u>Cash-flow Report</u></h2>" + ReportHTMLStyleSheet.getHTMLTextForDuration(fromDate, toDate) + ReportHTMLStyleSheet.getHTMLTextForFirm(selectedFirmId) + getHTMLTable(transactionList, openingCashInHand, closingCashInHand, shouldPrintItemDetails, shouldPrintDescription, selectedFirmId);

		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(listOfTransactions, openingCashInHand, closingCashInHand, shouldPrintItemDetails, shouldPrintDescription, selectedFirmId) {
		var len = listOfTransactions.length;
		var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
		var TransactionHTMLGenerator = require('./../ReportHTMLGenerator/TransactionHTMLGenerator.js');
		var transactionHTMLGenerator = new TransactionHTMLGenerator();
		var SettingCache = require('./../Cache/SettingCache.js');
		var settingCache = new SettingCache();
		var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
		var paymentInfoCache = new PaymentInfoCache();
		var dynamicRow = '';
		var isTransactionRefNumberEnabled = settingCache.getTxnRefNoEnabled();
		var extraColumnWidth = isTransactionRefNumberEnabled ? 0 : 2;

		var showRunningCashInHandColumn = selectedFirmId == 0;

		dynamicRow += "<table class='reportTableForPrint' width='100%'><thead><tr style='background-color: lightgrey;'><th width='" + (13 + extraColumnWidth) + "%' class='tableCellTextAlignLeft'>DATE</th>";
		if (isTransactionRefNumberEnabled) {
			dynamicRow += "<th width='12%' class='tableCellTextAlignLeft'>Ref No.</th>";
		}
		dynamicRow += "<th width='" + (17 + extraColumnWidth) + "%' class='tableCellTextAlignLeft'>NAME</th><th width='" + (13 + extraColumnWidth) + "%' class='tableCellTextAlignLeft'>TYPE</th><th width='" + (15 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>Cash in</th><th width='" + (15 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>Cash out</th>";

		if (showRunningCashInHandColumn) {
			dynamicRow += "<th width='" + (15 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>Running Cash In Hand</th>";
		}
		dynamicRow += '</tr></thead>';

		var totalCashIn = 0;
		var totalCashOut = 0;
		if (showRunningCashInHandColumn && openingCashInHand != 0) {
			if (isTransactionRefNumberEnabled) {
				dynamicRow += "<tr><td></td><td></td><td></td><td>Beginning Cash In Hand</td><td></td><td></td><td align='right'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(openingCashInHand) + '</td></tr>';
			} else {
				dynamicRow += "<tr><td></td><td></td><td>Beginning Cash In Hand</td><td></td><td></td><td align='right'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(openingCashInHand) + '</td></tr>';
			}
		}
		var runningCashInHand = openingCashInHand;

		var noOfColumns = isTransactionRefNumberEnabled ? 6 : 5;
		noOfColumns += showRunningCashInHandColumn ? 1 : 0;

		for (var i = 0; i < len; i++) {
			var txn = listOfTransactions[i];
			var printItemDetails = false;
			var printDescription = false;
			var classToBeIncluded = '';
			var txnItemLines = null;
			if (txn.getLineItems) {
				txnItemLines = txn.getLineItems();
			}
			var txnType = txn.getTxnType();
			printItemDetails = shouldPrintItemDetails && txnItemLines && txnItemLines.length && !TxnTypeConstant.isLoanTxnType(txnType);
			printDescription = shouldPrintDescription && txn.getDescription() && !TxnTypeConstant.isLoanTxnType(txnType);

			var classToBeIncludedInStatementRow = '';

			if (shouldPrintItemDetails || shouldPrintDescription) {
				classToBeIncludedInStatementRow += 'boldText extraTopPadding ';
				if (printItemDetails || printDescription) {
					classToBeIncludedInStatementRow += ' noBorder ';
				}
				classToBeIncludedInStatementRow = " class='" + classToBeIncludedInStatementRow + "'";
			}

			var classToBeIncludedInItemDetailRow = '';
			if (printDescription) {
				classToBeIncludedInItemDetailRow = " class='noBorder' ";
			}

			var name = '';
			var typeString = TxnTypeConstant.getTxnTypeForUI(txnType, txn.getTxnSubType());
			if (txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE || txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD) {
				name = paymentInfoCache.getPaymentBankName(txn.getBankId());
			} else if (txnType == TxnTypeConstant.TXN_TYPE_CASH_ADJ_REDUCE || txnType == TxnTypeConstant.TXN_TYPE_CASH_ADJ_ADD) {
				name = 'Cash adjustment';
			} else if (txnType == TxnTypeConstant.TXN_TYPE_CASH_OPENINGBALANCE) {
				name = 'Opening Cash in Hand';
			} else if (TxnTypeConstant.isLoanTxnType(txnType)) {
				name = txn.getDescription();
			} else {
				var nameRef = txn.getNameRef();
				if (nameRef) {
					name = nameRef.getFullName();
				}
			}
			var cashAmount = Number(txn.getCashAmount());
			var date = MyDate.getDate('d/m/y', txn.getTxnDate());
			var refNo = txn.getTxnRefNumber() === '' ? '' : txn.getFullTxnRefNumber();
			var row = '<tr><td ' + classToBeIncludedInStatementRow + '>' + date + '</td>';
			if (isTransactionRefNumberEnabled) {
				row += '<td ' + classToBeIncludedInStatementRow + '>' + refNo + '</td>';
			}
			row += '<td ' + classToBeIncludedInStatementRow + '>' + name + '</td><td ' + classToBeIncludedInStatementRow + '>' + typeString + '</td>';

			var txnTypeToBeUsed = txnType == TxnTypeConstant.TXN_TYPE_CHEQUE_TRANSFER ? txn.getTxnSubType() : txnType;
			if (txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_SALE || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_CASHIN || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_CASH_ADJ_ADD || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_OTHER_INCOME) {
				totalCashIn += cashAmount;
				runningCashInHand += cashAmount;
				row += "<td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(cashAmount) + "</td><td align='right' " + classToBeIncludedInStatementRow + '></td>';
			} else if (txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_PURCHASE || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_CASHOUT || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_EXPENSE || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_CASH_ADJ_REDUCE || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
				totalCashOut += cashAmount;
				runningCashInHand -= cashAmount;
				row += "<td align='right' " + classToBeIncludedInStatementRow + "></td><td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(cashAmount) + '</td>';
			} else if (txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_CASH_OPENINGBALANCE) {
				if (cashAmount >= 0) {
					totalCashIn += cashAmount;
					row += "<td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(cashAmount) + "</td><td align='right' " + classToBeIncludedInStatementRow + '></td>';
				} else {
					totalCashOut += Math.abs(cashAmount);
					row += "<td align='right' " + classToBeIncludedInStatementRow + "></td><td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(cashAmount) + '</td>';
				}
				runningCashInHand += cashAmount;
			} else if (TxnTypeConstant.isLoanTxnType(txnTypeToBeUsed)) {
				if (txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_LOAN_EMI_PAYMENT || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_LOAN_PROCESSING_FEE) {
					totalCashOut += cashAmount;
					runningCashInHand -= cashAmount;
					row += "<td align='right' " + classToBeIncludedInStatementRow + "></td><td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(cashAmount) + '</td>';
				} else {
					totalCashIn += cashAmount;
					runningCashInHand += cashAmount;
					row += "<td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(cashAmount) + "</td><td align='right' " + classToBeIncludedInStatementRow + '></td>';
				}
			}
			if (showRunningCashInHandColumn) {
				row += "<td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(runningCashInHand) + '</td>';
			}

			row += '</tr>';

			dynamicRow += row;

			if (printItemDetails) {
				dynamicRow += '<tr>\n' + '  <td ' + classToBeIncludedInItemDetailRow + ' colspan="1" ></td>\n' + '  <td ' + classToBeIncludedInItemDetailRow + ' colspan="' + (noOfColumns - 1) + '">' + transactionHTMLGenerator.getItemDetailsForReport(txn) + '</td>\n' + '</tr>\n';
			}
			if (printDescription) {
				dynamicRow += '<tr>\n' + '  <td colspan="' + noOfColumns + '"> <span class="boldText"> Description: </span>' + txn.getDescription() + '</td>\n' + '</tr>\n';
			}
		}
		dynamicRow += '</table>';

		dynamicRow += "<table width='100%'><tr style='background-color: lightgrey;'><th width='" + (13 + extraColumnWidth) + "%' class='tableCellTextAlignLeft'></th>";
		if (isTransactionRefNumberEnabled) {
			dynamicRow += "<th width='12%' class='tableCellTextAlignLeft'></th>";
		}
		dynamicRow += "<th width='" + (17 + extraColumnWidth) + "%' class='tableCellTextAlignLeft'>Total</th><th width='" + (13 + extraColumnWidth) + "%' class='tableCellTextAlignLeft'></th><th width='" + (15 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalCashIn) + "</th><th width='" + (15 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalCashOut) + '</th>';
		if (showRunningCashInHandColumn) {
			dynamicRow += "<th align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(closingCashInHand) + '</th>';
		}
		dynamicRow += '</tr></table>';

		return dynamicRow;
	};
};

module.exports = CashFlowReportHTMLGenerator;