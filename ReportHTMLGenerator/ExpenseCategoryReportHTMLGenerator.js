var ExpenseReportHTMLGenerator = function ExpenseReportHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');
	var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
	var paymentInfoCache = new PaymentInfoCache();
	var NameCache = require('./../Cache/NameCache.js');
	var nameCache = new NameCache();

	this.getHTMLText = function (transactionList, fromDate, toDate, selectedFirmId) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'><h2 align='center'><u>Expense Category Report</u></h2>" + ReportHTMLStyleSheet.getHTMLTextForDuration(fromDate, toDate) + ReportHTMLStyleSheet.getHTMLTextForFirm(selectedFirmId) + getHTMLTable(transactionList);
		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(listOfTransactions) {
		var len = listOfTransactions.length;
		var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
		var dynamicRow = '';

		var expenseCategoryList = nameCache.getListOfExpense();
		var lenCategory = expenseCategoryList.length;
		var rowData = [];
		var totalAmount = 0;

		dynamicRow += "<table class='reportTableForPrint' width='100%'><thead><tr style='background-color: lightgrey;'><th width='50%' class='tableCellTextAlignLeft'>Expense Category</th><th width='50%' class='tableCellTextAlignRight'>Amount</th></tr></thead>";

		var totalAmount = 0;
		for (var i = 0; i < len; i++) {
			var classToBeIncluded = '';
			var classToBeIncludedInStatementRow = '';
			var txn = listOfTransactions[i];
			var categoryName = txn.getCategoryName();
			var cashAmount = txn.getAmount();
			totalAmount += MyDouble.getAmountWithDecimal(cashAmount);

			var row = '<tr><td ' + classToBeIncludedInStatementRow + '>' + categoryName + "</td><td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(cashAmount) + '</td></tr>';
			dynamicRow += row;
		}
		dynamicRow += '</table>';
		//
		dynamicRow += "<table width='100%'><tr style='background-color: lightgrey;'><th width='18%'></th><th width='50%' class='tableCellTextAlignLeft'>Total</th><th width='32%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalAmount) + '</th></tr></table>';

		return dynamicRow;
	};
};

module.exports = ExpenseReportHTMLGenerator;