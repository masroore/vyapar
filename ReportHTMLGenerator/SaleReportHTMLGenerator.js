var SaleReportHTMLGenerator = function SaleReportHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');

	this.getHTMLText = function (transactionList, fromDate, toDate, selectedFirmId, shouldPrintItemDetails, shouldPrintDescription, printPaymentStatus, paymentStatVal, txnType) {
		var showTransactionType = arguments.length > 9 && arguments[9] !== undefined ? arguments[9] : true;

		var dateString = fromDate && toDate ? ReportHTMLStyleSheet.getHTMLTextForDuration(fromDate, toDate) : '';

		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + ('</head><body><div class=\'pdfReportHTMLView\'><h2 align=\'center\'><u>' + TxnTypeConstant.getTxnTypeForUI(txnType) + ' Report</u></h2>') + dateString + ReportHTMLStyleSheet.getHTMLTextForFirm(selectedFirmId) + getHTMLTable(transactionList, shouldPrintItemDetails, shouldPrintDescription, printPaymentStatus, paymentStatVal, showTransactionType, txnType);

		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(listOfTransactions, shouldPrintItemDetails, shouldPrintDescription, printPaymentStatus, paymentStatVal, showTransactionType, txnType) {
		var len = listOfTransactions.length;
		var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
		var TransactionHTMLGenerator = require('./../ReportHTMLGenerator/TransactionHTMLGenerator.js');
		var TxnPaymentStatusConstants = require('./../Constants/TxnPaymentStatusConstants');
		var transactionHTMLGenerator = new TransactionHTMLGenerator();
		var SettingCache = require('./../Cache/SettingCache.js');
		var settingCache = new SettingCache();
		var dynamicRow = '';
		var isTransactionRefNumberEnabled = settingCache.getTxnRefNoEnabled();

		var isPaymentTermEnabled = settingCache.isPaymentTermEnabled();

		var dateColumnWidthRatio = 9;
		var refNumberColumnRatio = isTransactionRefNumberEnabled ? 11 : 0;
		var nameColumnRatio = 19;
		var txnTypeColumnRatio = showTransactionType ? 10 : 0;
		var totalAmountColumnRatio = 14;
		var cashAmountColumnRatio = 14;
		var balanceAmountColumnRatio = 14;
		var paymentStatusColumnRatio = printPaymentStatus && !isPaymentTermEnabled ? 18 : 0;
		var dueDateColumnRatio = isPaymentTermEnabled ? 10 : 0;
		var statusColumnRatio = isPaymentTermEnabled ? 8 : 0;

		var totalColumnWidth = dateColumnWidthRatio + refNumberColumnRatio + nameColumnRatio + txnTypeColumnRatio + totalAmountColumnRatio + cashAmountColumnRatio + balanceAmountColumnRatio + paymentStatusColumnRatio + dueDateColumnRatio + statusColumnRatio;

		dynamicRow += "<table class='reportTableForPrint' width='100%'><thead><tr style='background-color: lightgrey;'><th width='" + dateColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignLeft'>DATE</th>";
		if (isTransactionRefNumberEnabled) {
			dynamicRow += "<th width='" + refNumberColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignLeft'>INVOICE N0.</th>";
		}
		dynamicRow += "<th width='" + nameColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignLeft'>PARTY NAME</th>";

		if (showTransactionType) {
			dynamicRow += "<th width='" + txnTypeColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignLeft'>TRANSACTION TYPE</th>";
		}

		dynamicRow += "<th width='" + totalAmountColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>TOTAL</th><th width='" + cashAmountColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>RECEIVED / PAID</th>";

		dynamicRow += "<th width='" + balanceAmountColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>BALANCE DUE</th>";

		if (printPaymentStatus) {
			if (isPaymentTermEnabled) {
				dynamicRow += "<th width='" + dueDateColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>DUE DATE</th>";
				dynamicRow += "<th width='" + statusColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>STATUS</th>";
			} else {
				dynamicRow += "<th width='" + paymentStatusColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>PAYMENT STATUS</th>";
			}
		}

		dynamicRow += '</tr></thead>';

		var totalAmount = 0;

		for (var i = 0; i < len; i++) {
			var txn = listOfTransactions[i];
			var printItemDetails = false;
			var printDescription = false;
			var classToBeIncluded = '';
			var txnItemLines = txn.getLineItems();
			printItemDetails = shouldPrintItemDetails && txnItemLines && txnItemLines.length;
			printDescription = shouldPrintDescription && txn.getDescription();

			var classToBeIncludedInStatementRow = '';

			if (shouldPrintItemDetails || shouldPrintDescription) {
				classToBeIncludedInStatementRow += 'boldText extraTopPadding ';
				if (printItemDetails || printDescription) {
					classToBeIncludedInStatementRow += ' noBorder ';
				}
				classToBeIncludedInStatementRow = " class='" + classToBeIncludedInStatementRow + "'";
			}

			var classToBeIncludedInItemDetailRow = '';
			if (printDescription) {
				classToBeIncludedInItemDetailRow = " class='noBorder' ";
			}

			var typeString = TxnTypeConstant.getTxnTypeForUI(txn.getTxnType());
			var name = txn.getNameRef().getFullName();
			var displayName = txn.getDisplayName();
			if (displayName && displayName.trim() && name != displayName.trim()) {
				name = name + ' (' + displayName.trim() + ')';
			}
			var date = MyDate.getDate('d/m/y', txn.getTxnDate());
			var receivedAmt = txn.getCashAmount() ? txn.getCashAmount() : 0;
			var balanceAmt = txn.getBalanceAmount() ? txn.getBalanceAmount() : 0;
			var txnCurrentBalanceAmount = txn.getTxnCurrentBalanceAmount() ? txn.getTxnCurrentBalanceAmount() : 0;
			var totalAmt = Number(receivedAmt) + Number(balanceAmt);
			totalAmt = totalAmt || 0;

			var txnTotalReceivedAmt = totalAmt - txnCurrentBalanceAmount;

			var refNo = txn.getTxnRefNumber() === '' ? '' : txn.getFullTxnRefNumber();
			var paymentStatus = txn.getTxnPaymentStatus();
			var paymentStatusString = TxnPaymentStatusConstants.getPaymentStatusForUI(txn.getTxnType(), paymentStatus);

			var paymentStatFilter = void 0;

			if (isPaymentTermEnabled && paymentStatVal == TxnPaymentStatusConstants.OVERDUE) {
				paymentStatFilter = dueByDays > 0 && paymentStatus != TxnPaymentStatusConstants.PAID;
			} else {
				paymentStatFilter = paymentStatVal ? paymentStatVal == paymentStatus : true;
			}

			paymentStatFilter = settingCache.isBillToBillEnabled() ? paymentStatFilter : true;

			var dueDate = isPaymentTermEnabled ? MyDate.convertDateToStringForUI(txn.getTxnDueDate()) : new Date();
			var dueByDays = isPaymentTermEnabled ? MyDouble.getDueDays(txn.getTxnDueDate()) : 0;

			if (paymentStatFilter) {
				var row = '<tr><td ' + classToBeIncludedInStatementRow + '>' + date + '</td>';
				if (isTransactionRefNumberEnabled) {
					row += '<td ' + classToBeIncludedInStatementRow + '>' + refNo + '</td>';
				}
				row += '<td ' + classToBeIncludedInStatementRow + '>' + name + '</td>';

				if (showTransactionType) {
					row += '<td ' + classToBeIncludedInStatementRow + '>' + typeString + '</td>';
				}

				row += "<td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalAmt) + "</td><td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(txnTotalReceivedAmt) + '</td>';

				row += "<td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(txnCurrentBalanceAmount) + '</td>';

				if (printPaymentStatus) {
					if (isPaymentTermEnabled) {
						if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE) {
							row += '<td ' + classToBeIncludedInStatementRow + " align='right' width='" + dueDateColumnRatio * 100 / totalColumnWidth + "%' " + classToBeIncludedInStatementRow + '>' + dueDate + '</td>';
							if (dueByDays > 0 && paymentStatus != TxnPaymentStatusConstants.PAID) {
								paymentStatusString = 'Overdue';
								row += '<td ' + classToBeIncludedInStatementRow + " align='right' width='" + statusColumnRatio * 100 / totalColumnWidth + "%' " + classToBeIncludedInStatementRow + '>';
								row += '<div>' + paymentStatusString + '</div>';
								row += '<div>' + dueByDays + ' Days</div>';
							} else {
								row += '<td ' + classToBeIncludedInStatementRow + "  align='right' width='" + statusColumnRatio * 100 / totalColumnWidth + "%' " + classToBeIncludedInStatementRow + '>';
								row += '<div>' + paymentStatusString + '</div>';
							}
							row += '</td>';
						} else {
							row += '<td ' + classToBeIncludedInStatementRow + "  align='right' width='" + dueDateColumnRatio * 100 / totalColumnWidth + "%'></td>";
							row += '<td ' + classToBeIncludedInStatementRow + "  align='right' width='" + statusColumnRatio * 100 / totalColumnWidth + "%'>" + paymentStatusString + '</td>';
						}
					} else {
						row += '<td ' + classToBeIncludedInStatementRow + " align='right' width='" + paymentStatusColumnRatio * 100 / totalColumnWidth + "%'>" + paymentStatusString + '</td>';
					}
				}

				row += '</tr>';

				dynamicRow += row;

				var noOfColumns = isTransactionRefNumberEnabled ? 6 : 5;
				noOfColumns = printPaymentStatus ? noOfColumns + 1 : noOfColumns;
				noOfColumns = isPaymentTermEnabled ? noOfColumns + 1 : noOfColumns;
				noOfColumns = showTransactionType ? noOfColumns + 1 : noOfColumns;

				if (printItemDetails) {
					dynamicRow += '<tr>\n' + '  <td ' + classToBeIncludedInItemDetailRow + ' colspan="1" ></td>\n' + '  <td ' + classToBeIncludedInItemDetailRow + ' colspan="' + (noOfColumns - 1) + '">' + transactionHTMLGenerator.getItemDetailsForReport(txn) + '</td>\n' + '</tr>\n';
				}
				if (printDescription) {
					dynamicRow += '<tr>\n' + '  <td colspan="' + noOfColumns + '"> <span class="boldText"> Description: </span>' + txn.getDescription() + '</td>\n' + '</tr>\n';
				}

				if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE) {
					totalAmount += totalAmt;
				} else if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
					totalAmount -= totalAmt;
				}
			}
		}
		dynamicRow += '</table>';
		dynamicRow += '<h3 align=\'right\'> Total ' + TxnTypeConstant.getTxnTypeForUI(txnType) + ': ' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalAmount) + '</h3>';
		return dynamicRow;
	};
	this.prepareObjectForExcel = function (_ref) {
		var listOfTransactions = _ref.listOfTransactions,
		    showDescription = _ref.showDescription,
		    showPaymentStatus = _ref.showPaymentStatus,
		    paymentStatVal = _ref.paymentStatVal,
		    showTransactionType = _ref.showTransactionType,
		    showAdditionalFieds = _ref.showAdditionalFieds,
		    txnType = _ref.txnType;

		var SettingCache = require('../Cache/SettingCache');
		var UDFCache = require('./../Cache/UDFCache');
		var MyDouble = require('../Utilities/MyDouble');
		var MyDate = require('../Utilities/MyDate');
		var TxnPaymentStatusConstants = require('../Constants/TxnPaymentStatusConstants.js');
		var settingCache = new SettingCache();
		var uDFCache = new UDFCache();
		var isTransactionRefNumberEnabled = settingCache.getTxnRefNoEnabled();
		var len = listOfTransactions.length;
		var totalAmount = 0;
		var rowObj = [];
		var tableHeadArray = [];
		var totalArray = [];
		var ExcelDescription = showDescription;
		var ExcelAdditionalFields = showAdditionalFieds;

		var ExcelPaymentStatus = showPaymentStatus;

		tableHeadArray.push('Date');
		if (isTransactionRefNumberEnabled) {
			tableHeadArray.push('Invoice No');
			totalArray.push('');
		}
		tableHeadArray.push('Party Name');
		showTransactionType && tableHeadArray.push('Transaction Type');
		tableHeadArray.push('Total Amount');
		tableHeadArray.push('Received/Paid Amount');

		tableHeadArray.push('Balance Due');

		if (settingCache.isBillToBillEnabled() && ExcelPaymentStatus) {
			if (settingCache.isPaymentTermEnabled()) {
				tableHeadArray.push('Due Date');
				tableHeadArray.push('Status');
			} else {
				tableHeadArray.push('Payment Status');
			}
		}

		if (ExcelDescription) {
			tableHeadArray.push('Description');
		}

		var firmId = Number(settingCache.getDefaultFirmId());
		var txnMap = uDFCache.getTransactionFieldsMap();
		var firmTxnMap = txnMap.get(Number(firmId));
		if (ExcelAdditionalFields) {
			if (firmTxnMap) {
				firmTxnMap.forEach(function (udfModel) {
					if (udfModel.getUdfFieldStatus() == 1 && udfModel.getUdfTxnType() == txnType) {
						tableHeadArray.push(udfModel.getUdfFieldName());
					}
				});
			}
		}

		totalArray.push('');
		showTransactionType && totalArray.push('');
		rowObj.push(tableHeadArray);
		rowObj.push([]);

		var _loop = function _loop(j) {
			var txn = listOfTransactions[j];
			var typeString = TxnTypeConstant.getTxnTypeForUI(txn.getTxnType());
			var name = txn.getNameRef().getFullName();
			if (txn.getDisplayName() && txn.getDisplayName().trim() && name !== txn.getDisplayName().trim()) {
				name = name + ' (' + txn.getDisplayName().trim() + ')';
			}
			var date = MyDate.getDateStringForUI(txn.getTxnDate());
			var receivedAmt = txn.getCashAmount() ? txn.getCashAmount() : 0;
			var balanceAmt = txn.getBalanceAmount() ? txn.getBalanceAmount() : 0;
			var txnCurrentBalanceAmount = txn.getTxnCurrentBalanceAmount() ? txn.getTxnCurrentBalanceAmount() : 0;
			var totalAmt = Number(receivedAmt) + Number(balanceAmt);
			var paymentStatus = txn.getTxnPaymentStatus();
			var paymentStatusString = TxnPaymentStatusConstants.getPaymentStatusForUI(txn.getTxnType(), paymentStatus);

			var txnTotalReceivedAmount = totalAmt - txnCurrentBalanceAmount;
			var dueDate = MyDate.convertDateToStringForUI(txn.getTxnDueDate());
			var dueByDays = MyDouble.getDueDays(txn.getTxnDueDate());

			var paymentStatFilter = void 0;

			if (settingCache.isPaymentTermEnabled() && paymentStatVal == TxnPaymentStatusConstants.OVERDUE) {
				paymentStatFilter = dueByDays > 0 && paymentStatus != TxnPaymentStatusConstants.PAID;
			} else {
				paymentStatFilter = paymentStatVal ? paymentStatVal == paymentStatus : true;
			}

			paymentStatFilter = settingCache.isBillToBillEnabled() ? paymentStatFilter : true;

			totalAmt = totalAmt || 0;

			var refNo = txn.getTxnRefNumber() === '' ? '' : txn.getFullTxnRefNumber();
			var tempArray = [];
			tempArray.push(date);
			if (isTransactionRefNumberEnabled) {
				tempArray.push(refNo);
			}
			tempArray.push(name);
			showTransactionType && tempArray.push(typeString);
			tempArray.push(MyDouble.getAmountWithDecimal(totalAmt));
			tempArray.push(MyDouble.getAmountWithDecimal(txnTotalReceivedAmount));

			tempArray.push(MyDouble.getAmountWithDecimal(txnCurrentBalanceAmount));

			if (settingCache.isBillToBillEnabled() && ExcelPaymentStatus) {
				if (settingCache.isPaymentTermEnabled()) {
					if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE) {
						tempArray.push(dueDate);
						if (dueByDays > 0) {
							tempArray.push('Overdue ' + dueByDays + ' Days');
						} else {
							tempArray.push(paymentStatusString);
						}
					} else {
						tempArray.push('');
						tempArray.push(paymentStatusString);
					}
				} else {
					tempArray.push(paymentStatusString);
				}
			}
			if (ExcelDescription) {
				tempArray.push(txn.getDescription());
			}

			if (ExcelAdditionalFields) {
				if (firmTxnMap) {
					firmTxnMap.forEach(function (udfModel) {
						if (udfModel.getUdfFieldStatus() == 1 && udfModel.getUdfTxnType() == txnType) {
							var extraFieldObjectArray = txn.getUdfObjectArray();
							var udfValueModel = extraFieldObjectArray.find(function (udfValueModel) {
								return udfValueModel.getUdfFieldId() == udfModel.getUdfFieldId();
							});
							var displayValue = udfValueModel ? udfValueModel.getDisplayValue(udfModel) : '';
							tempArray.push(displayValue);
						}
					});
				}
			}

			if (paymentStatFilter) {
				if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE) {
					totalAmount += totalAmt;
				} else if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
					totalAmount -= totalAmt;
				}
				rowObj.push(tempArray);
			}
		};

		for (var j = 0; j < len; j++) {
			_loop(j);
		}
		rowObj.push([]);
		totalArray.push('Total');
		totalArray.push(MyDouble.getAmountWithDecimal(totalAmount));
		totalArray.push('');
		totalArray.push('');
		rowObj.push(totalArray);
		return rowObj;
	};
};

module.exports = SaleReportHTMLGenerator;