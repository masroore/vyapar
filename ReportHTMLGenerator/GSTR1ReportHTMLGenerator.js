var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var GSTR1ReportHTMLGenerator = function GSTR1ReportHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');

	this.getHTMLText = function (transactionList, fromDate, toDate, selectedFirmId, showOTHER, showAdditionalCess, showStateSpecificCESS) {
		var SettingCache = require('./../Cache/SettingCache.js');
		var settingCache = new SettingCache();

		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'><h2 align='center'><u>GSTR1 Report</u></h2>" + getCompanyDetails(fromDate, toDate, selectedFirmId) + getTxnTables(transactionList, showOTHER, showAdditionalCess, showStateSpecificCESS);
		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getCompanyDetails = function getCompanyDetails(fromDate, toDate, firmId) {
		var monthArray = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		fromDate = '01/' + fromDate;
		toDate = '01/' + toDate;
		var startDate = MyDate.getDateObj(fromDate, 'dd/MM/yyyy', '/');
		var startmonthIndex = startDate.getMonth();
		var startMonthName = monthArray[startmonthIndex];
		var startmonthYear = startDate.getFullYear();

		var endDate = MyDate.getDateObj(toDate, 'dd/MM/yyyy', '/');
		var endmonthIndex = endDate.getMonth();
		var endMonthName = monthArray[endmonthIndex];
		var endmonthYear = endDate.getFullYear();

		var FirmCache = require('./../Cache/FirmCache.js');
		var firmCache = new FirmCache();
		if (firmId == 0) {
			firmId = settingCache.getDefaultFirmId();
		}
		var firm = firmCache.getFirmById(firmId);
		var firmName = firm.getFirmName();
		var gstin = firmCache.getFirmGSTINByFirmName(firmName);

		var dynamicRow = '';

		dynamicRow += "<table align='right' width='40%' style='margin-top:15px' border='1px'><tr><td width='30%'>Period</td><td width='70%'>" + startMonthName + ' ' + startmonthYear + ' - ' + endMonthName + ' ' + endmonthYear + '</td></tr></table>';

		dynamicRow += "<table width='100%' style='margin-top:50px;' border='1px'><tr><td width='50%'>1. GSTIN</td><td width='50%'>" + gstin + "</td></tr><tr><td width='50%'>2.a Legal name of the registered person</td><td width='50%'>" + firmName + "</td></tr><tr><td width='50%'>2.b Trade name, if any</td><td width='50%'></td></tr><tr><td width='50%'>3.a Aggregate Turnover in the preceeding Financial Year.</td><td width='50%'></td></tr><tr><td width='50%'>3.b Aggregate Turnover, April to June 2017</td><td width='50%'></td></tr></table>";

		return dynamicRow;
	};

	var getTxnTables = function getTxnTables(listOfTransactions, showOTHER, showAdditionalCess, showStateSpecificCESS) {
		var dynamicRow1 = "<h3 align='center' style='margin-top:50px;'><u>Sale</u></h3>";
		var dynamicRow2 = "<h3 align='center' style='margin-top:50px;'><u>Sale return</u></h3>";

		var len = listOfTransactions.length;

		var gstinWidth = 5;
		var invoiceNoWidth = 5;
		var invoiceDateWidth = 5;
		var returnNoWidth = 5;
		var returnDateWidth = 5;

		var invoiceValueWidth = 6;
		var rateWidth = 5;
		var cessRateWidth = 5;
		var taxableWidth = 6;
		var IGSTAmtWidth = 6;
		var SGSTAmtWidth = 6;
		var CGSTAmtWidth = 6;
		var CESSAmtWidth = 6;
		var otherAmtWidth = 6;
		var additionalCessAmtWidth = 0;
		var stateSpecificCessAmtWidth = 6;
		var placeToSupply = 8;

		var amountSubCols = 7;

		if (!showOTHER) {
			amountSubCols--;
			otherAmtWidth = 0;
		}

		if (!showAdditionalCess) {
			amountSubCols--;
			additionalCessAmtWidth = 0;
		}

		if (!showStateSpecificCESS) {
			amountSubCols--;
			stateSpecificCessAmtWidth = 0;
		}

		var invoiceWidthSale = invoiceNoWidth + invoiceDateWidth + invoiceValueWidth;
		var invoiceWidthSaleReturn = invoiceWidthSale + returnNoWidth + returnDateWidth;

		var amountWidth = IGSTAmtWidth + SGSTAmtWidth + CGSTAmtWidth + CESSAmtWidth + otherAmtWidth + additionalCessAmtWidth + stateSpecificCessAmtWidth;

		var totalWidthSale = gstinWidth + invoiceNoWidth + invoiceDateWidth + invoiceValueWidth + rateWidth + taxableWidth + IGSTAmtWidth + SGSTAmtWidth + CGSTAmtWidth + CESSAmtWidth + otherAmtWidth + additionalCessAmtWidth + stateSpecificCessAmtWidth + placeToSupply;

		var totalWidthSaleReturn = totalWidthSale + returnNoWidth + returnDateWidth;

		dynamicRow1 += "<table class='reportTableForPrint' width='100%' style=' table-layout:fixed;' border='1px'><tr width='100%'><th width='" + gstinWidth * 100 / totalWidthSale + "%' rowspan='2' style='word-wrap: break-word;'>GSTIN/UIN</th><th width='" + invoiceWidthSale * 100 / totalWidthSale + "%' colspan='3' style='word-wrap: break-word;'>Invoice details</th><th width='" + rateWidth * 100 / totalWidthSale + "%' rowspan='2' style='word-wrap: break-word;'>Rate</th><th width='" + cessRateWidth * 100 / totalWidthSale + "%' rowspan='2' style='word-wrap: break-word;'>Cess Rate</th><th rowspan='2' width='" + taxableWidth * 100 / totalWidthSale + "%' style='word-wrap: break-word;'>Taxable value</th><th width='" + amountWidth * 100 / totalWidthSale + "%' colspan='" + amountSubCols + "' style='word-wrap: break-word;'>Amount</th><th width='" + placeToSupply * 100 / totalWidthSale + "%' rowspan='2' style='word-wrap: break-word;'>Place of Supply(Name of State)</th></tr><tr><th width='" + invoiceNoWidth * 100 / totalWidthSale + "%' style='word-wrap: break-word;'>No.</th><th width='" + invoiceDateWidth * 100 / totalWidthSale + "%' style='word-wrap: break-word;'>Date</th><th width='" + invoiceValueWidth * 100 / totalWidthSale + "%' style='word-wrap: break-word;'>Value</th><th width='" + IGSTAmtWidth * 100 / totalWidthSale + "%' style='word-wrap: break-word;'>Integrated Tax</th><th width='" + CGSTAmtWidth * 100 / totalWidthSale + "%' style='word-wrap: break-word;'>Central Tax</th><th width='" + SGSTAmtWidth * 100 / totalWidthSale + "%' style='word-wrap: break-word;'>State/UT Tax</th><th width='" + CESSAmtWidth * 100 / totalWidthSale + "%' style='word-wrap: break-word;'>Cess</th>";
		dynamicRow2 += "<table class='reportTableForPrint' width='100%' style='table-layout:fixed;' border='1px'><tr width='100%'><th width='" + gstinWidth * 100 / totalWidthSaleReturn + "%' rowspan='2' style='word-wrap: break-word;'>GSTIN/UIN</th><th width='" + invoiceWidthSaleReturn * 100 / totalWidthSaleReturn + "%' colspan='5' style='word-wrap: break-word;'>Cr. Note details</th><th width='" + rateWidth * 100 / totalWidthSaleReturn + "%' rowspan='2' style='word-wrap: break-word;'>Rate</th><th width='" + cessRateWidth * 100 / totalWidthSaleReturn + "%' rowspan='2' style='word-wrap: break-word;'>Cess Rate</th><th rowspan='2' width='" + taxableWidth * 100 / totalWidthSaleReturn + "%' style='word-wrap: break-word;'>Taxable value</th><th width='" + amountWidth * 100 / totalWidthSaleReturn + "%' colspan='" + amountSubCols + "' style='word-wrap: break-word;'>Amount</th><th width='" + placeToSupply * 100 / totalWidthSaleReturn + "%' rowspan='2' style='word-wrap: break-word;'>Place of Supply(Name of State)</th></tr><tr><th width='" + invoiceNoWidth * 100 / totalWidthSaleReturn + "%' style='word-wrap: break-word;'>Invoice No.</th><th width='" + invoiceDateWidth * 100 / totalWidthSaleReturn + "%' style='word-wrap: break-word;'>Invoice Date</th><th width='" + returnNoWidth * 100 / totalWidthSaleReturn + "%' style='word-wrap: break-word;'>Return No.</th><th width='" + returnDateWidth * 100 / totalWidthSaleReturn + "%' style='word-wrap: break-word;'>Return Date</th><th width='" + invoiceValueWidth * 100 / totalWidthSaleReturn + "%' style='word-wrap: break-word;'>Value</th><th width='" + IGSTAmtWidth * 100 / totalWidthSaleReturn + "%' style='word-wrap: break-word;'>Integrated Tax</th><th width='" + CGSTAmtWidth * 100 / totalWidthSaleReturn + "%' style='word-wrap: break-word;'>Central Tax</th><th width='" + SGSTAmtWidth * 100 / totalWidthSaleReturn + "%' style='word-wrap: break-word;'>State/UT Tax</th><th width='" + CESSAmtWidth * 100 / totalWidthSaleReturn + "%' style='word-wrap: break-word;'>Cess</th>";

		if (showOTHER) {
			dynamicRow1 += "<th width='" + otherAmtWidth * 100 / totalWidthSale + "%' style='word-wrap: break-word;'>Other</th>";
			dynamicRow2 += "<th width='" + otherAmtWidth * 100 / totalWidthSaleReturn + "%' style='word-wrap: break-word;'>Other</th>";
		}

		if (showStateSpecificCESS) {
			dynamicRow1 += "<th width='" + otherAmtWidth * 100 / totalWidthSale + "%' style='word-wrap: break-word;'>Flood CESS</th>";
			dynamicRow2 += "<th width='" + otherAmtWidth * 100 / totalWidthSaleReturn + "%' style='word-wrap: break-word;'>Flood CESS</th>";
		}
		if (showAdditionalCess) {
			dynamicRow1 += "<th width='" + additionalCessAmtWidth * 100 / totalWidthSale + "%' style='word-wrap: break-word;'>Additional Cess</th>";
			dynamicRow2 += "<th width='" + additionalCessAmtWidth * 100 / totalWidthSaleReturn + "%' style='word-wrap: break-word;'>Additional Cess</th>";
		}
		dynamicRow1 += '</tr>';
		dynamicRow2 += '</tr>';

		var invoiceNoMapSale = new _map2.default();
		var totalTaxableValueSale = 0;
		var totalInvoiceValueSale = 0;
		var totalIGSTAmountSale = 0;
		var totalCGSTAmountSale = 0;
		var totalSGSTAmountSale = 0;
		var totalCESSAmountSale = 0;
		var totalOTHERAmountSale = 0;
		var totalStateSpecificCESSAmountSale = 0;
		var totalAdditionalCessAmountSale = 0;

		var invoiceNoMapSaleReturn = new _map2.default();
		var totalTaxableValueSaleReturn = 0;
		var totalInvoiceValueSaleReturn = 0;
		var totalIGSTAmountSaleReturn = 0;
		var totalCGSTAmountSaleReturn = 0;
		var totalSGSTAmountSaleReturn = 0;
		var totalCESSAmountSaleReturn = 0;
		var totalOTHERAmountSaleReturn = 0;
		var totalStateSpecificCESSAmountSaleReturn = 0;
		var totalAdditionalCessAmountSaleReturn = 0;

		for (var i = 0; i < len; i++) {
			var row = '';
			var txn = listOfTransactions[i];
			var GstinNo = txn.getGstinNo();
			var invoiceDate = MyDate.getDate('d/m/y', txn.getInvoiceDate());
			var invoiceValue = txn.getInvoiceValue();
			var cessRate = txn.getCessRate();
			var invoiceRate = txn.getRate() - cessRate - txn.getStateSpecificCESSRate();
			var invoceTaxableValue = txn.getInvoiceTaxableValue();
			var igstAmt = txn.getIGSTAmt();
			var sgstAmt = txn.getSGSTAmt();
			var cgstAmt = txn.getCGSTAmt();
			var cessAmt = txn.getCESSAmt();
			var additionalCess = txn.getAdditionalCessAmt();
			var stateSpecificCESS = txn.getStateSpecificCESSAmt();
			var otherAmt = txn.getOTHERAmt();
			var POS = txn.getPlaceOfSupply();
			var transactionId = txn.getTransactionId();
			var transactionType = txn.getTransactionType();

			var refNo = txn.getTxnRefNumber() === '' ? '' : txn.getInvoiceNo();
			var returnRefNo = txn.getTxnReturnRefNumber();
			var returnDate = txn.getTxnReturnDate() ? MyDate.getDate('d/m/y', txn.getTxnReturnDate()) : '';

			var totalWidth = totalWidthSale;

			if (transactionType == TxnTypeConstant.TXN_TYPE_SALE) {
				if (!invoiceNoMapSale.has(transactionId)) {
					invoiceNoMapSale.set(transactionId, invoiceValue);
					totalInvoiceValueSale += invoiceValue;
				}
				totalTaxableValueSale += invoceTaxableValue;
				totalIGSTAmountSale += igstAmt;
				totalCGSTAmountSale += cgstAmt;
				totalSGSTAmountSale += sgstAmt;
				totalCESSAmountSale += cessAmt;
				totalOTHERAmountSale += otherAmt;
				totalAdditionalCessAmountSale += additionalCess;
				totalStateSpecificCESSAmountSale += stateSpecificCESS;
			} else if (transactionType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
				if (!invoiceNoMapSaleReturn.has(transactionId)) {
					invoiceNoMapSaleReturn.set(transactionId, invoiceValue);
					totalInvoiceValueSaleReturn += invoiceValue;
				}
				totalTaxableValueSaleReturn += invoceTaxableValue;
				totalIGSTAmountSaleReturn += igstAmt;
				totalCGSTAmountSaleReturn += cgstAmt;
				totalSGSTAmountSaleReturn += sgstAmt;
				totalCESSAmountSaleReturn += cessAmt;
				totalOTHERAmountSaleReturn += otherAmt;
				totalAdditionalCessAmountSaleReturn += additionalCess;
				totalStateSpecificCESSAmountSaleReturn += stateSpecificCESS;
				totalWidth = totalWidthSaleReturn;
			}

			row += "<tr><td width='" + gstinWidth * 100 / totalWidth + "%' style='word-wrap: break-word;'>" + GstinNo + '</td>';

			if (transactionType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
				row += "<td width='" + returnNoWidth * 100 / totalWidth + "%' align='right' style='word-wrap: break-word;'>" + returnRefNo + "</td><td width='" + returnDateWidth * 100 / totalWidth + "%' align='right' style='word-wrap: break-word;'>" + returnDate + '</td>';
			}

			row += "<td width='" + invoiceNoWidth * 100 / totalWidth + "%' style='word-wrap: break-word;' align='right'>" + refNo + "</td><td width='" + invoiceDateWidth * 100 / totalWidth + "%' style='word-wrap: break-word;' align='right'>" + invoiceDate + "</td><td width='" + invoiceValueWidth * 100 / totalWidth + "%' style='word-wrap: break-word;' align='right'>" + MyDouble.getAmountWithDecimal(invoiceValue) + "</td><td  width='" + rateWidth * 100 / totalWidth + "%' style='word-wrap: break-word;' align='right'>" + MyDouble.getPercentageWithDecimal(invoiceRate) + "</td><td  width='" + cessRateWidth * 100 / totalWidth + "%' style='word-wrap: break-word;' align='right'>" + MyDouble.getPercentageWithDecimal(cessRate) + "</td><td width='" + taxableWidth * 100 / totalWidth + "%' style='word-wrap: break-word;' align='right'>" + MyDouble.getAmountWithDecimal(invoceTaxableValue) + "</td><td width='" + IGSTAmtWidth * 100 / totalWidth + "%' style='word-wrap: break-word;' align='right'>" + MyDouble.getAmountWithDecimal(igstAmt) + "</td><td width='" + CGSTAmtWidth * 100 / totalWidth + "%' style='word-wrap: break-word;' align='right'>" + MyDouble.getAmountWithDecimal(cgstAmt) + "</td><td width='" + SGSTAmtWidth * 100 / totalWidth + "%' style='word-wrap: break-word;' align='right'>" + MyDouble.getAmountWithDecimal(sgstAmt) + "</td><td width='" + CESSAmtWidth * 100 / totalWidth + "%' style='word-wrap: break-word;' align='right'>" + MyDouble.getAmountWithDecimal(cessAmt) + '</td>';

			if (showOTHER) {
				row += "<td width='" + otherAmtWidth * 100 / totalWidth + "%' style='word-wrap: break-word;' align='right'>" + MyDouble.getAmountWithDecimal(otherAmt) + '</td>';
			}

			if (showStateSpecificCESS) {
				row += "<td width='" + stateSpecificCESS * 100 / totalWidth + "%' style='word-wrap: break-word;' align='right'>" + MyDouble.getAmountWithDecimal(stateSpecificCESS) + '</td>';
			}

			if (showAdditionalCess) {
				row += "<td width='" + additionalCessAmtWidth * 100 / totalWidth + "%' style='word-wrap: break-word;' align='right'>" + MyDouble.getAmountWithDecimal(additionalCess) + '</td>';
			}

			row += "<td width='" + placeToSupply * 100 / totalWidth + "%' style='word-wrap: break-word;' align='right'>" + (POS || '') + '</td></tr>';
			if (transactionType == TxnTypeConstant.TXN_TYPE_SALE) {
				dynamicRow1 += row;
			} else if (transactionType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
				dynamicRow2 += row;
			}
		}

		if (len > 0) {
			dynamicRow1 += "<tr><td width='" + gstinWidth * 100 / totalWidthSale + "%' style='word-wrap: break-word; font-weight:bold;'>Total</td>";
			dynamicRow2 += "<tr><td width='" + gstinWidth * 100 / totalWidthSaleReturn + "%' style='word-wrap: break-word; font-weight:bold;'>Total</td><td width='" + returnNoWidth * 100 / totalWidthSaleReturn + "%'></td><td width='" + returnDateWidth * 100 / totalWidthSaleReturn + "%'></td>";

			dynamicRow1 += "<td width='" + invoiceNoWidth * 100 / totalWidthSale + "%'></td><td width='" + invoiceDateWidth * 100 / totalWidthSale + "%'></td><td width='" + invoiceValueWidth * 100 / totalWidthSale + "%' style='word-wrap: break-word; font-weight:bold;' align='right'>" + MyDouble.getAmountWithDecimal(totalInvoiceValueSale) + "</td><td  width='" + rateWidth * 100 / totalWidthSale + "%'></td><td  width='" + cessRateWidth * 100 / totalWidthSale + "%'></td><td width='" + taxableWidth * 100 / totalWidthSale + "%' style='word-wrap: break-word; font-weight:bold;' align='right'>" + MyDouble.getAmountWithDecimal(totalTaxableValueSale) + "</td><td width='" + IGSTAmtWidth * 100 / totalWidthSale + "%' style='word-wrap: break-word; font-weight:bold;' align='right'>" + MyDouble.getAmountWithDecimal(totalIGSTAmountSale) + "</td><td width='" + CGSTAmtWidth * 100 / totalWidthSale + "%' style='word-wrap: break-word; font-weight:bold;' align='right'>" + MyDouble.getAmountWithDecimal(totalCGSTAmountSale) + "</td><td width='" + SGSTAmtWidth * 100 / totalWidthSale + "%' style='word-wrap: break-word; font-weight:bold;' align='right'>" + MyDouble.getAmountWithDecimal(totalSGSTAmountSale) + "</td><td width='" + CESSAmtWidth * 100 / totalWidthSale + "%' style='word-wrap: break-word; font-weight:bold;' align='right'>" + MyDouble.getAmountWithDecimal(totalCESSAmountSale) + '</td>';
			dynamicRow2 += "<td width='" + invoiceNoWidth * 100 / totalWidthSaleReturn + "%'></td><td width='" + invoiceDateWidth * 100 / totalWidthSaleReturn + "%'></td><td width='" + invoiceValueWidth * 100 / totalWidthSaleReturn + "%' style='word-wrap: break-word; font-weight:bold;' align='right'>" + MyDouble.getAmountWithDecimal(totalInvoiceValueSaleReturn) + "</td><td  width='" + rateWidth * 100 / totalWidthSaleReturn + "%'></td><td  width='" + cessRateWidth * 100 / totalWidthSaleReturn + "%'></td><td width='" + taxableWidth * 100 / totalWidthSaleReturn + "%' style='word-wrap: break-word; font-weight:bold;' align='right'>" + MyDouble.getAmountWithDecimal(totalTaxableValueSaleReturn) + "</td><td width='" + IGSTAmtWidth * 100 / totalWidthSaleReturn + "%' style='word-wrap: break-word; font-weight:bold;' align='right'>" + MyDouble.getAmountWithDecimal(totalIGSTAmountSaleReturn) + "</td><td width='" + CGSTAmtWidth * 100 / totalWidthSaleReturn + "%' style='word-wrap: break-word; font-weight:bold;' align='right'>" + MyDouble.getAmountWithDecimal(totalCGSTAmountSaleReturn) + "</td><td width='" + SGSTAmtWidth * 100 / totalWidthSaleReturn + "%' style='word-wrap: break-word; font-weight:bold;' align='right'>" + MyDouble.getAmountWithDecimal(totalSGSTAmountSaleReturn) + "</td><td width='" + CESSAmtWidth * 100 / totalWidthSaleReturn + "%' style='word-wrap: break-word; font-weight:bold;' align='right'>" + MyDouble.getAmountWithDecimal(totalCESSAmountSaleReturn) + '</td>';

			if (showOTHER) {
				dynamicRow1 += "<td width='" + otherAmtWidth * 100 / totalWidthSale + "%' style='word-wrap: break-word; font-weight:bold;'>" + MyDouble.getAmountWithDecimal(totalOTHERAmountSale) + '</td>';
				dynamicRow2 += "<td width='" + otherAmtWidth * 100 / totalWidthSaleReturn + "%' style='word-wrap: break-word; font-weight:bold;'>" + MyDouble.getAmountWithDecimal(totalOTHERAmountSaleReturn) + '</td>';
			}

			if (showStateSpecificCESS) {
				dynamicRow1 += "<td width='" + stateSpecificCessAmtWidth * 100 / totalWidthSale + "%' style='word-wrap: break-word; font-weight:bold;'>" + MyDouble.getAmountWithDecimal(totalStateSpecificCESSAmountSale) + '</td>';
				dynamicRow2 += "<td width='" + stateSpecificCessAmtWidth * 100 / totalWidthSaleReturn + "%' style='word-wrap: break-word; font-weight:bold;'>" + MyDouble.getAmountWithDecimal(totalStateSpecificCESSAmountSaleReturn) + '</td>';
			}
			if (showAdditionalCess) {
				dynamicRow1 += "<td width='" + additionalCessAmtWidth * 100 / totalWidthSale + "%' style='word-wrap: break-word; font-weight:bold;' align='right'>" + MyDouble.getAmountWithDecimal(totalAdditionalCessAmountSale) + '</td>';
				dynamicRow2 += "<td width='" + additionalCessAmtWidth * 100 / totalWidthSaleReturn + "%' style='word-wrap: break-word; font-weight:bold;' align='right'>" + MyDouble.getAmountWithDecimal(totalAdditionalCessAmountSaleReturn) + '</td>';
			}

			dynamicRow1 += "<td width='" + placeToSupply * 100 / totalWidthSale + "%' ></td></tr>";
			dynamicRow2 += "<td width='" + placeToSupply * 100 / totalWidthSaleReturn + "%' ></td></tr>";
		}
		dynamicRow1 += '</table>';
		dynamicRow2 += '</table>';
		return dynamicRow1 + dynamicRow2;
	};
};

module.exports = GSTR1ReportHTMLGenerator;