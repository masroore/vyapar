var CustomReportHTMLGenerator = function CustomReportHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');

	this.getHTMLText = function (transactionList, fromDate, toDate, selectedFirmId, nameString, shouldPrintItemQuantity, shouldPrintItemAmount) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'><h2 align='center'><u>Order Item Report</u></h2>" + '<h3>Party name: ' + nameString + '</h3>' + ReportHTMLStyleSheet.getHTMLTextForDuration(fromDate, toDate) + ReportHTMLStyleSheet.getHTMLTextForFirm(selectedFirmId) + getHTMLTable(transactionList, shouldPrintItemQuantity, shouldPrintItemAmount);

		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(listOfTransactions, shouldPrintItemQuantity, shouldPrintItemAmount) {
		var len = listOfTransactions.length;
		var dynamicRow = '';

		dynamicRow += "<table class='reportTableForPrint' width='100%'><thead><tr style='background-color: lightgrey;'><th width='40%'>Item Name</th>";
		if (shouldPrintItemQuantity) {
			dynamicRow += "<th width='30%' class='tableCellTextAlignRight'>Quantity</th>";
		}
		if (shouldPrintItemAmount) {
			dynamicRow += "<th width='30%' class='tableCellTextAlignRight'>Amount</th>";
		}
		dynamicRow += '</tr></thead>';

		var totalQuantity = 0;
		var totalFreeQuantity = 0;
		var totalAmount = 0;

		for (var i = 0; i < len; i++) {
			var txn = listOfTransactions[i];
			var itemName = txn.getItemName();
			var qty = txn.getQty();
			var freeQty = txn.getFreeQty();
			var amount = txn.getAmount();

			totalQuantity += qty;
			totalFreeQuantity += freeQty;
			totalAmount += amount;

			dynamicRow += '<tr><td>' + itemName + '</td>';
			if (shouldPrintItemQuantity) {
				dynamicRow += "<td align='right'>" + MyDouble.getQuantityWithDecimalWithoutColor(qty) + MyDouble.quantityDoubleToStringWithSignExplicitly(freeQty, true) + '</td>';
			}
			if (shouldPrintItemAmount) {
				dynamicRow += "<td align='right'>" + MyDouble.getAmountWithDecimalAndCurrency(amount) + '</td>';
			}
			dynamicRow += '</tr>';
		}
		dynamicRow += "<tr style='background-color: lightgrey;'><td>Total</td>";
		if (shouldPrintItemQuantity) {
			dynamicRow += "<td align='right'>" + MyDouble.getQuantityWithDecimalWithoutColor(totalQuantity) + MyDouble.quantityDoubleToStringWithSignExplicitly(totalFreeQuantity, true) + '</td>';
		}
		if (shouldPrintItemAmount) {
			dynamicRow += "<td align='right'>" + MyDouble.getAmountWithDecimalAndCurrency(totalAmount) + '</td>';
		}

		dynamicRow += '</tr>';

		dynamicRow += '</table>';
		return dynamicRow;
	};
};

module.exports = CustomReportHTMLGenerator;