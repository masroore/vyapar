var LoanStatementReportHTMLGenerator = function LoanStatementReportHTMLGenerator() {
    var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');
    var TxnTypeConstant = require('../Constants/TxnTypeConstant');

    this.getHTMLText = function (transactionList, fromDate, toDate, openingBalance, totalPrincipalPaid, currentBalance, totalInterestPaid, loanAccount, firmId) {
        var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'><h2 align='center'><u>Loan Statement</u></h2>" + ReportHTMLStyleSheet.getHTMLTextForDuration(fromDate, toDate) + ReportHTMLStyleSheet.getHTMLTextForFirm(firmId) + '<h3>Loan Account: ' + (loanAccount.accountName || '') + '</h3>' + (loanAccount.accountNumber ? '<h3>Account Number: ' + (loanAccount.accountNumber || ':~') + '</h3>' : '') + getHTMLTable(transactionList, openingBalance, totalPrincipalPaid, currentBalance, totalInterestPaid);
        htmlText += '</div></body></html>';
        return htmlText;
    };

    var getHTMLTable = function getHTMLTable() {
        var listOfTransactions = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
        var openingBalance = arguments[1];
        var totalPrincipalPaid = arguments[2];
        var currentBalance = arguments[3];
        var totalInterestPaid = arguments[4];

        var htmlString = '\n            <table class=\'reportTableForPrint\' width="100%">\n                <thead>\n                    <tr style=\'background-color: lightgrey;\'>\n                        <th width=\'14%\'  class=\'tableCellTextAlignLeft\'>Date</th>\n                        <th width=\'26%\'  class=\'tableCellTextAlignLeft\'>Type</th>\n                        <th width=\'20%\' class=\'tableCellTextAlignRight\'>Amount</th>\n                        <th width=\'20%\' class=\'tableCellTextAlignRight\'>Ending Balance</th>\n                    </tr>\n                </thead>\n                <tbody>\n                    ' + listOfTransactions.reduce(function (acc, txn) {
            var amount = Number(txn.principalAmount || '') + Number(txn.interestAmount || '');
            return acc + ('\n                            <tr>\n                                <td align=\'left\'>' + MyDate.getDate('d/m/y', txn.date) + '</td>\n                                <td align=\'left\'>' + TxnTypeConstant.getTxnTypeForUI(txn.loanTxnType) + '</td>\n                                <td align=\'right\'>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(amount) + '</td>\n                                <td align=\'right\'>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(txn.endingBalance) + '</td>\n                            </tr>\n                        ');
        }, '') + '\n                </tbody>\n            </table>\n            <table width=\'100%\'>\n                <tr align=\'left\'><td class=\'noBorder\' colspan=\'2\'></td></tr>\n                <tr align=\'left\'><th colspan=\'2\'>Loan Statement Summary</th></tr>\n                <tr>\n                    <td align=\'left\'>Opening Balance: ' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(openingBalance || 0) + '</td>\n                    <td align=\'right\'>Total Principal Paid: ' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalPrincipalPaid || 0) + '</td>\n                </tr>\n                <tr>\n                    <td align=\'left\'>Balance Due: ' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(currentBalance || 0) + '</td>\n                    <td align=\'right\'>Total Interest Paid: ' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalInterestPaid || 0) + '</td>\n                </tr>\n            </table>\n        ';
        return htmlString;
    };
};

module.exports = LoanStatementReportHTMLGenerator;