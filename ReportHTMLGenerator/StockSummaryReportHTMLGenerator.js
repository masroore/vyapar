var StockSummaryReportHTMLGenerator = function StockSummaryReportHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');
	var TransactionHTMLGenerator = require('./../ReportHTMLGenerator/TransactionHTMLGenerator.js');
	var transactionHTMLGenerator = new TransactionHTMLGenerator();
	var SettingCache = require('./../Cache/SettingCache.js');
	var settingCache = new SettingCache();

	this.getHTMLText = function (itemList, printSalePrice, printPurchasePrice, printStockQuantity, printStockValue) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'>" + transactionHTMLGenerator.getCompanyHeader(null, settingCache.getTxnPDFTheme(), settingCache.getTxnPDFThemeColor(), 0.4 + 0.1 * settingCache.getPrintTextSize()) + "<h2 align='center'><u>Stock summary Report</u></h2>" + getHTMLTable(itemList, printSalePrice, printPurchasePrice, printStockQuantity, printStockValue);

		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(itemList, printSalePrice, printPurchasePrice, printStockQuantity, printStockValue) {
		var len = itemList.length;

		var dynamicRow = '';
		var dynamicFooter = '';
		var totalStockQty = 0;
		var totalStockValue = 0;

		dynamicRow += "<table class='reportTableForPrint' width='100%'><thead><tr style='background-color: lightgrey;'><th width='6%'></th><th width='*' class='tableCellTextAlignLeft'>Item Name</th>";
		dynamicFooter += "<tr style='background-color: lightgrey;'><th width='6%'></th><th width='*' class='tableCellTextAlignLeft'>Total</th>";

		if (printSalePrice) {
			dynamicRow += "<th width='17%' class='tableCellTextAlignRight'>Sale Price</th>";
			dynamicFooter += "<th class='tableCellTextAlignLeft'></th>";
		}
		if (printPurchasePrice) {
			dynamicRow += "<th width='17%' class='tableCellTextAlignRight'>Purchase Price</th>";
			dynamicFooter += "<th class='tableCellTextAlignLeft'></th>";
		}
		if (printStockQuantity) {
			dynamicRow += "<th width='17%' class='tableCellTextAlignRight'>Stock Quantity</th>";
		}
		if (printStockValue) {
			dynamicRow += "<th width='17%' class='tableCellTextAlignRight'>Stock Value</th>";
		}
		dynamicRow += '</tr></thead>';

		for (var i = 0; i < len; i++) {
			var itemObject = itemList[i];
			var row = '';
			row = '<tr><td>' + (i + 1) + '</td><td>' + itemObject.getItemName() + '</td>';
			if (printSalePrice) {
				row += "<td align='right'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(itemObject.getItemSaleUnitPrice()) + '</td>';
			}
			if (printPurchasePrice) {
				row += "<td align='right'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(itemObject.getItemPurchaseUnitPrice()) + '</td>';
			}
			if (printStockQuantity) {
				row += "<td align='right'>" + MyDouble.getQuantityWithDecimalWithoutColor(itemObject.getItemStockQuantity()) + '</td>';
				totalStockQty += Number(itemObject.getItemStockQuantity());
			}
			if (printStockValue) {
				row += "<td align='right'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(itemObject.getItemStockValue()) + '</td>';
				totalStockValue += Number(itemObject.getItemStockValue());
			}

			row += '</tr>';
			dynamicRow += row;
		}
		dynamicFooter += printStockQuantity ? "<th width='17%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(totalStockQty) + "</th>" : "";
		dynamicFooter += printStockValue ? "<th width='17%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalStockValue) + "</th>" : "";
		dynamicFooter += '</tr>';
		if (printStockQuantity || printStockValue) {
			dynamicRow += dynamicFooter;
		}

		dynamicRow += '</table>';
		return dynamicRow;
	};
};

module.exports = StockSummaryReportHTMLGenerator;