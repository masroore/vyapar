var BankStatementHTMLGenerator = function BankStatementHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');
	this.getHTMLText = function (listOfTransactions, startDateString, endDateString, bankName, shouldPrintDescription) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'><h2 align='center'><u>Bank statement</u></h2>" + '<h3>Bank: ' + bankName + '</h3>' + ReportHTMLStyleSheet.getHTMLTextForDuration(startDateString, endDateString) + getHTMLTable(listOfTransactions, startDateString, endDateString, bankName, shouldPrintDescription);

		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(listOfTransactions, startDateString, endDateString, bankName, shouldPrintDescription) {
		var len = listOfTransactions.length;
		var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
		var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
		var NameCache = require('./../Cache/NameCache.js');
		var paymentInfoCache = new PaymentInfoCache();
		var nameCache = new NameCache();
		var dynamicRow = '';
		var dynamicFooter = '';
		var runningBalance = 0;

		dynamicRow += "<table class='reportTableForPrint' width='100%'><thead><tr style='background-color: lightgrey;'><th width='14%'  class='tableCellTextAlignLeft'>Date</th><th width='26%'  class='tableCellTextAlignLeft'>Transaction Details</th>";
		dynamicRow += "<th width='20%' class='tableCellTextAlignRight'>Withdrawal Amount</th><th width='20%' class='tableCellTextAlignRight'>Deposit Amount</th><th width='20%' class='tableCellTextAlignRight'>Balance Amount</th></tr></thead>";

		for (var i = 0; i < len; i++) {
			var bankDetailObject = listOfTransactions[i];
			var date = MyDate.getDate('d/m/y', bankDetailObject.getTxnDate());
			var printDescription = false;
			printDescription = shouldPrintDescription && bankDetailObject.getTxnDescription();
			var bankDetailObjectAmount = bankDetailObject.getAmount() ? Number(bankDetailObject.getAmount()) : 0;

			var classToBeIncludedInStatementRow = '';

			if (shouldPrintDescription) {
				classToBeIncludedInStatementRow += 'boldText extraTopPadding ';
				if (printDescription) {
					classToBeIncludedInStatementRow += ' noBorder ';
				}
				classToBeIncludedInStatementRow = " class='" + classToBeIncludedInStatementRow + "'";
			}

			var classToBeIncludedInItemDetailRow = '';
			if (printDescription) {
				classToBeIncludedInItemDetailRow = " class='noBorder' ";
			}

			var row = '';
			row = '<td>' + date + '</td>';

			var txnType = Number(bankDetailObject.getTxnType());
			var description = '';

			if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT || txnType == TxnTypeConstant.TXN_TYPE_EXPENSE || txnType == TxnTypeConstant.TXN_TYPE_OTHER_INCOME || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_ESTIMATE || txnType == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
				var userId = bankDetailObject.getUserId();
				var name = nameCache.findNameObjectByNameId(userId);
				description = '[' + TxnTypeConstant.getTxnTypeForUI(txnType) + '] ' + (name ? name.getFullName() : '');
			} else if (txnType == TxnTypeConstant.TXN_TYPE_BANK_OPENING || txnType == TxnTypeConstant.TXN_TYPE_BANK_BALANCE_FORWARD) {
				description = TxnTypeConstant.getTxnTypeForUI(txnType);
			} else if (txnType == TxnTypeConstant.TXN_TYPE_CHEQUE_TRANSFER) {
				var userId = bankDetailObject.getUserId();
				var name = nameCache.findNameObjectByNameId(userId);
				description = '[' + TxnTypeConstant.getTxnTypeForUI(txnType) + '] ' + (name ? name.getFullName() : '');
			} else if (txnType == TxnTypeConstant.TXN_TYPE_BANK_TO_BANK) {
				if (bankDetailObject.getToBankId() == bankId) {
					var bankName = paymentInfoCache.getPaymentBankName(bankDetailObject.getFromBankId());
					description = 'From ' + bankName;
				} else {
					var bankName = paymentInfoCache.getPaymentBankName(bankDetailObject.getToBankId());
					description = 'To ' + bankName;
				}
			} else {
				description = '[' + TxnTypeConstant.getTxnTypeForUI(txnType) + '] ' + (bankDetailObject.getDescription() ? bankDetailObject.getDescription() : '');
			}

			row += '<td>' + description + '</td>';

			var appliedTxnType = txnType == TxnTypeConstant.TXN_TYPE_CHEQUE_TRANSFER ? Number(bankDetailObject.getSubTxnType()) : txnType;

			switch (appliedTxnType) {
				case TxnTypeConstant.TXN_TYPE_PURCHASE:
				case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
				case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
				case TxnTypeConstant.TXN_TYPE_CASHOUT:
				case TxnTypeConstant.TXN_TYPE_EXPENSE:
				case TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE:
				case TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE_WITHOUT_CASH:
					runningBalance -= bankDetailObjectAmount;
					row += '<td align="right">' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(bankDetailObjectAmount) + '</td>';
					row += '<td></td>';
					row += '<td align="right">' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(runningBalance) + '</td>';
					break;
				case TxnTypeConstant.TXN_TYPE_OTHER_INCOME:
				case TxnTypeConstant.TXN_TYPE_SALE:
				case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
				case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
				case TxnTypeConstant.TXN_TYPE_ESTIMATE:
				case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
				case TxnTypeConstant.TXN_TYPE_CASHIN:
				case TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD:
				case TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD_WITHOUT_CASH:
					runningBalance += bankDetailObjectAmount;
					row += '<td></td>';
					row += '<td align="right">' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(bankDetailObjectAmount) + '</td>';
					row += '<td align="right">' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(runningBalance) + '</td>';
					break;
				case TxnTypeConstant.TXN_TYPE_BANK_BALANCE_FORWARD:
					runningBalance += bankDetailObjectAmount;
					row += '<td></td>';
					row += '<td></td>';
					row += '<td align="right">' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(runningBalance) + '</td>';
					break;
				case TxnTypeConstant.TXN_TYPE_BANK_OPENING:
					runningBalance += bankDetailObjectAmount;
					if (bankDetailObjectAmount > 0) {
						row += '<td></td>';
						row += '<td align="right">' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(Math.abs(bankDetailObjectAmount)) + '</td>';
					} else {
						row += '<td align="right">' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(Math.abs(bankDetailObjectAmount)) + '</td>';
						row += '<td></td>';
					}
					row += '<td align="right">' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(runningBalance) + '</td>';
					break;
				case TxnTypeConstant.TXN_TYPE_BANK_TO_BANK:
					if (bankDetailObject.getFromBankId() == bankId) {
						runningBalance -= bankDetailObjectAmount;
						row += '<td align="right">' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(bankDetailObjectAmount) + '</td>';
						row += '<td></td>';
					} else {
						runningBalance += bankDetailObjectAmount;
						row += '<td></td>';
						row += '<td align="right">' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(bankDetailObjectAmount) + '</td>';
					}
					row += '<td align="right">' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(runningBalance) + '</td>';
					break;
				case TxnTypeConstant.TXN_TYPE_LOAN_STARTING_BALANCE:
				case TxnTypeConstant.TXN_TYPE_LOAN_PROCESSING_FEE:
				case TxnTypeConstant.TXN_TYPE_LOAN_ADJUSTMENT:
				case TxnTypeConstant.TXN_TYPE_LOAN_EMI_PAYMENT:
				case TxnTypeConstant.TXN_TYPE_LOAN_CLOSE_BOOK_ADJ:
					if (txnType == TxnTypeConstant.TXN_TYPE_LOAN_EMI_PAYMENT || txnType == TxnTypeConstant.TXN_TYPE_LOAN_PROCESSING_FEE) {
						runningBalance -= bankDetailObjectAmount;
						row += "<td width='20%' align=\"right\">" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(bankDetailObjectAmount) + '</td>';
						row += '<td></td>';
					} else {
						runningBalance += bankDetailObjectAmount;
						row += '<td></td>';
						row += "<td width='20%' align=\"right\">" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(bankDetailObjectAmount) + '</td>';
					}
					row += "<td width='20%' align=\"right\">" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(runningBalance) + '</td>';
					break;
			}

			row = '<tr>' + row + '</tr>';
			if (printDescription) {
				row += '<tr>\n' + '  <td colspan="3"> <span class="boldText"> Description: </span>' + bankDetailObject.getTxnDescription() + '</td>\n' + '</tr>\n';
			}
			dynamicRow += row;
		}
		dynamicFooter = "<tr style='background-color: lightgrey;'><th width='14%'  class='tableCellTextAlignLeft'></th><th width='26%'  class='tableCellTextAlignLeft'></th><th width='20%' class='tableCellTextAlignRight'></th><th width='20%' class='tableCellTextAlignRight'>Balance</th><th width='20%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(runningBalance) + '</th></tr>';
		dynamicRow += dynamicFooter + '</table>';
		return dynamicRow;
	};
};

module.exports = BankStatementHTMLGenerator;