var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var BalanceSheetHTMLGenerator = function BalanceSheetHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');
	var TransactionHTMLGenerator = require('./../ReportHTMLGenerator/TransactionHTMLGenerator.js');
	var transactionHTMLGenerator = new TransactionHTMLGenerator();
	this.getHTMLText = function (balanceSheetData, endDate) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'>" + transactionHTMLGenerator.getCompanyHeader(null, settingCache.getTxnPDFTheme(), settingCache.getTxnPDFThemeColor(), 0.4 + 0.1 * settingCache.getPrintTextSize()) + getHTMLTable(balanceSheetData, endDate);

		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(balanceSheetData, endDate) {
		var PaymentInfoCache = require('../Cache/PaymentInfoCache');
		var LoanAccountCache = require('./../Cache/LoanAccountCache.js');
		var paymentInfoCache = new PaymentInfoCache();
		var loanAccountCache = new LoanAccountCache();
		var bankAccountTotal = 0;
		var loanAccountTotal = 0;

		function isActiveRow(value) {
			return Number(value) != 0;
		}

		function getRows() {
			var assetsCol = [];
			var liabilitiesCol = [];

			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;

			try {
				for (var _iterator = (0, _getIterator3.default)(balanceSheetData.bankAccountBalanceList), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					var _ref = _step.value;

					var _ref2 = (0, _slicedToArray3.default)(_ref, 2);

					var key = _ref2[0];
					var value = _ref2[1];

					bankAccountTotal += Number(value);
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}

			var _iteratorNormalCompletion2 = true;
			var _didIteratorError2 = false;
			var _iteratorError2 = undefined;

			try {
				for (var _iterator2 = (0, _getIterator3.default)(balanceSheetData.loanAccountBalanceList), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
					var _ref3 = _step2.value;

					var _ref4 = (0, _slicedToArray3.default)(_ref3, 2);

					var _key = _ref4[0];
					var _value = _ref4[1];

					loanAccountTotal += Number(_value);
				}
			} catch (err) {
				_didIteratorError2 = true;
				_iteratorError2 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion2 && _iterator2.return) {
						_iterator2.return();
					}
				} finally {
					if (_didIteratorError2) {
						throw _iteratorError2;
					}
				}
			}

			assetsCol.push(['<td style=\'background: #F7F7F7;font-weight: bold;\' class=\'\'>Current Assets</td><td style=\'background: #F7F7F7;\' class=\'borderRightForTxn \'></td>']);
			assetsCol.push(['<td class=\'\'>Cash in hand </td><td class=\'textRight boldText borderRightForTxn \'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.cashInHand) + '</td>']);
			assetsCol.push(['<td style=\'color: #553555;font-weight: bold;\' class=\'\'>\n                Bank Accounts</td><td class=\'textRight boldText borderRightForTxn \'>' + MyDouble.getBalanceAmountWithDecimal(bankAccountTotal) + '</td>']);
			var _iteratorNormalCompletion3 = true;
			var _didIteratorError3 = false;
			var _iteratorError3 = undefined;

			try {
				for (var _iterator3 = (0, _getIterator3.default)(balanceSheetData.bankAccountBalanceList), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var _ref5 = _step3.value;

					var _ref6 = (0, _slicedToArray3.default)(_ref5, 2);

					var _key2 = _ref6[0];
					var _value2 = _ref6[1];

					assetsCol.push(['<td class=\'padLeft5  padRight5\'>' + paymentInfoCache.getPaymentBankName(_key2) + ' </td><td class=\'textRight padRight5 borderRightForTxn \'>' + MyDouble.getBalanceAmountWithDecimal(_value2) + '</td>']);
				}
			} catch (err) {
				_didIteratorError3 = true;
				_iteratorError3 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError3) {
						throw _iteratorError3;
					}
				}
			}

			isActiveRow(balanceSheetData.undepositedChequeAmount) && assetsCol.push(['<td class=\'\'>Undeposited cheques </td><td class=\'textRight boldText borderRightForTxn \'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.undepositedChequeAmount) + '</td>']);
			assetsCol.push(['<td class=\'\'>Accounts receivable/ Sundry Debtors </td><td class=\'textRight boldText borderRightForTxn \'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.receivableAmount) + '</td>']);
			isActiveRow(balanceSheetData.advanceForPurchaseOrder) && assetsCol.push(['<td class=\'\'>Advance for purchase order </td><td class=\'textRight boldText borderRightForTxn \'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.advanceForPurchaseOrder) + '</td>']);
			assetsCol.push(['<td class=\'\'>Inventory on hand/ Closing stock </td><td class=\'textRight boldText borderRightForTxn \'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.inventoryClosingStock) + '</td>']);
			assetsCol.push(['<td class=\'\'>Tax Receivable </td><td class=\'textRight boldText borderRightForTxn \'>' + MyDouble.getBalanceAmountWithDecimal(Number(balanceSheetData.taxReceivable)) + '</td>']);

			liabilitiesCol.push(['<td style=\'background: #F7F7F7;font-weight: bold;\' class=\'\'>Current Liabilities</td><td style=\'background: #F7F7F7;\' class=\'\'></td>']);
			liabilitiesCol.push(['<td class=\'\'>Accounts Payable / Sundry Creditors </td><td class=\'textRight boldText \'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.payableAmount) + '</td>']);
			liabilitiesCol.push(['<td style=\'color: #553555;font-weight: bold;\' class=\'\'>\n                Loan Accounts</td><td class=\'textRight boldText borderRightForTxn \'>' + MyDouble.getBalanceAmountWithDecimal(loanAccountTotal) + '</td>']);
			var _iteratorNormalCompletion4 = true;
			var _didIteratorError4 = false;
			var _iteratorError4 = undefined;

			try {
				for (var _iterator4 = (0, _getIterator3.default)(balanceSheetData.loanAccountBalanceList), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
					var _ref7 = _step4.value;

					var _ref8 = (0, _slicedToArray3.default)(_ref7, 2);

					var _key3 = _ref8[0];
					var _value3 = _ref8[1];

					var loanAccount = loanAccountCache.getLoanAccountById(_key3);
					var accountName = loanAccount ? loanAccount.getLoanAccName() : '';
					liabilitiesCol.push(['<td class=\'padLeft5  padRight5\'>' + accountName + ' </td><td class=\'textRight padRight5 borderRightForTxn \'>' + MyDouble.getBalanceAmountWithDecimal(_value3) + '</td>']);
				}
			} catch (err) {
				_didIteratorError4 = true;
				_iteratorError4 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion4 && _iterator4.return) {
						_iterator4.return();
					}
				} finally {
					if (_didIteratorError4) {
						throw _iteratorError4;
					}
				}
			}

			liabilitiesCol.push(['<td class=\'\'>Tax payable </td><td class=\'textRight boldText \'>' + MyDouble.getBalanceAmountWithDecimal(Number(balanceSheetData.taxPayable)) + '</td>']);
			isActiveRow(balanceSheetData.unwithdrawnChequeAmount) && liabilitiesCol.push(['<td class=\'\'>Unwithdrawn cheques </td><td class=\'textRight boldText \'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.unwithdrawnChequeAmount) + '</td>']);
			isActiveRow(balanceSheetData.advanceForSaleOrder) && liabilitiesCol.push(['<td class=\'\'>Advance for sale order </td><td class=\'textRight boldText \'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.advanceForSaleOrder) + '</td>']);

			liabilitiesCol.push(['<td style=\'font-weight: bold; background: #F7F7F7;\' class=\'\'>Equity/Capital</td><td style=\'background: #F7F7F7;\' class=\'\'></td>']);
			liabilitiesCol.push(['<td style=\'color: #553555;font-weight: bold;\' class=\'\'>Opening balance equity</td><td class=\'textRight boldText \'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.openingBalnanceEquity) + '</td>']);
			isActiveRow(balanceSheetData.openingCashInHandAmount) && liabilitiesCol.push(['<td class=\'padLeft5  padRight5\'>Opening cash in hand </td><td class=\'padRight5 textRight \'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.openingCashInHandAmount) + '</td>']);
			isActiveRow(balanceSheetData.openingBankBalance) && liabilitiesCol.push(['<td class=\'padLeft5  padRight5\'>Opening bank balance </td><td class=\'padRight5 textRight \'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.openingBankBalance) + '</td>']);
			isActiveRow(balanceSheetData.openingPartyBalance) && liabilitiesCol.push(['<td class=\'padLeft5  padRight5\'>Opening party balance </td><td class=\'padRight5 textRight \'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.openingPartyBalance) + '</td>']);
			isActiveRow(balanceSheetData.inventoryOpeningStock) && liabilitiesCol.push(['<td class=\'padLeft5  padRight5\'>Opening stock </td><td class=\'padRight5 textRight \'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.inventoryOpeningStock) + '</td>']);
			isActiveRow(balanceSheetData.closedChequeAmount) && liabilitiesCol.push(['<td class=\'padLeft5  padRight5\'>Closed transaction cheque </td><td class=\'padRight5 textRight \'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.closedChequeAmount) + '</td>']);
			isActiveRow(balanceSheetData.openingLoanBalanceEquity) && liabilitiesCol.push(['<td class=\'padLeft5  padRight5\'>Opening Loan Balance(-) </td><td class=\'padRight5 textRight \'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.openingLoanBalanceEquity) + '</td>']);

			liabilitiesCol.push(['<td style=\'color: #553555;font-weight: bold;\' class=\'\'>\n                Owner\'s equity </td><td class=\'textRight boldText \'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.ownersEquity) + '</td>']);
			isActiveRow(balanceSheetData.addCashAmount) && liabilitiesCol.push(['<td class=\'padLeft5  padRight5\'>Add cash </td><td class=\'padRight5 textRight \'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.addCashAmount) + '</td>']);
			isActiveRow(balanceSheetData.reduceCashAmount) && liabilitiesCol.push(['<td class=\'padLeft5  padRight5\'>Reduce cash </td><td class=\'padRight5 textRight \'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.reduceCashAmount) + '</td>']);
			isActiveRow(balanceSheetData.addBankAmount) && liabilitiesCol.push(['<td class=\'padLeft5  padRight5\'>Increase bank balance </td><td class=\'padRight5 textRight \'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.addBankAmount) + '</td>']);
			isActiveRow(balanceSheetData.reduceBankAmount) && liabilitiesCol.push(['<td class=\'padLeft5  padRight5\'>Decrease bank balance </td><td class=\'padRight5 textRight \'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.reduceBankAmount) + '</td>']);

			liabilitiesCol.push(['<td class=\'\'>Retained Earnings </td><td class=\'textRight boldText \'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.retainedEarningAmount) + '</td>']);
			liabilitiesCol.push(['<td class=\'\'>Net Income (profit) </td><td class=\'textRight boldText \'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.profitAmount) + '</td>']);

			var maxLength = Math.max(liabilitiesCol.length, assetsCol.length);
			var rowHtml = '';
			for (var index = 0; index < maxLength; index++) {
				var assetsRow = assetsCol[index] || ['<td class=\'\'></td>', '<td class=\'borderRightForTxn\'></td>'];
				var liabilitiesRow = liabilitiesCol[index] || ['<td class=\'\'></td>', '<td class=\'\'></td>'];
				rowHtml += '<tr>' + assetsRow.join('') + liabilitiesRow.join('') + '</tr>';
			}
			return rowHtml;
		}
		var htmlText = '   \n            <div style=\'display: flex;\n            flex-direction: column;\'>\n                                <div style=\'align-self: center;\n                                color: #333;\n                                padding: 2px;\n                                font-size: 12px; padding: 20px !important;\n                                font-weight: bold;\'>Balance Sheet as on ' + endDate.toDateString().slice(4) + '</div>\n                            </div>\n    \n                <table class="balancesheet" style="width: 100%;border: 1px solid #bdbdbd;">\n                        <tbody>\n                            <tr style=\'background-color: rgb(231, 243, 255) !important;\' class=\'borderBottomForTxn\'>\n                                <td style=\'font-weight: bold\'>Assets</td>\n                                <td class="textRight borderRightForTxn" style=\'font-weight: bold;\' >Amount</td>\n                                <td style=\'font-weight: bold;\'>Liabilities</td>\n                                <td class="textRight" style=\'font-weight: bold;\'>Amount</td>\n                            </tr>\n                            ' + getRows();

		var assetsTotal = Number(balanceSheetData.cashInHand) + Number(bankAccountTotal) + Number(balanceSheetData.undepositedChequeAmount) + Number(balanceSheetData.receivableAmount) + Number(balanceSheetData.inventoryClosingStock) + Number(balanceSheetData.taxReceivable) + Number(balanceSheetData.advanceForPurchaseOrder);
		var liabilitiesTotal = Number(balanceSheetData.profitAmount) + Number(balanceSheetData.payableAmount) + Number(balanceSheetData.ownersEquity) + Number(balanceSheetData.openingBalnanceEquity) + Number(balanceSheetData.retainedEarningAmount) + Number(balanceSheetData.taxPayable) + Number(balanceSheetData.unwithdrawnChequeAmount) + Number(balanceSheetData.advanceForSaleOrder) + Number(loanAccountTotal);

		var diff = Math.abs(assetsTotal - liabilitiesTotal);

		// if difference is in between 0.01 and 0.1, then make them same bcoz this diff is bcoz of roundoff
		if (diff <= 0.1 && diff >= 0.01) {
			liabilitiesTotal = assetsTotal;
		}

		htmlText += '<tr style=\'background-color: rgb(231, 243, 255) !important;\'>\n                                <td></td>\n                                <td style=\'font-weight: bold;\' class="textRight borderRightForTxn">' + MyDouble.getBalanceAmountWithDecimal(assetsTotal) + '</td>\n                                <td></td>\n                                <td style=\'font-weight: bold;\' class="textRight">' + MyDouble.getBalanceAmountWithDecimal(liabilitiesTotal) + '</td>\n                            </tr>\n                        </tbody>\n                    </table>';

		return htmlText;
	};
};

module.exports = BalanceSheetHTMLGenerator;