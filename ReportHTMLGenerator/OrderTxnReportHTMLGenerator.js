var CustomReportHTMLGenerator = function CustomReportHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');

	this.getHTMLText = function (transactionList, fromDate, toDate, selectedFirmId, nameString, shouldPrintItemDetails, shouldPrintDescription) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'><h2 align='center'><u>Order Transaction Report</u></h2>" + '<h3>Party name: ' + nameString + '</h3>' + ReportHTMLStyleSheet.getHTMLTextForDuration(fromDate, toDate) + ReportHTMLStyleSheet.getHTMLTextForFirm(selectedFirmId) + getHTMLTable(transactionList, shouldPrintItemDetails, shouldPrintDescription);

		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(listOfTransactions, shouldPrintItemDetails, shouldPrintDescription) {
		var len = listOfTransactions.length;
		var TransactionStatus = require('./../Constants/TransactionStatus.js');
		var TransactionHTMLGenerator = require('./../ReportHTMLGenerator/TransactionHTMLGenerator.js');
		var transactionHTMLGenerator = new TransactionHTMLGenerator();
		var SettingCache = require('./../Cache/SettingCache.js');
		var settingCache = new SettingCache();
		var dynamicRow = '';
		var isTransactionRefNumberEnabled = settingCache.getTxnRefNoEnabled();
		var extraColumnWidth = isTransactionRefNumberEnabled ? 0 : 1.5;

		dynamicRow += "<table class='reportTableForPrint' width='100%'><thead><tr style='background-color: lightgrey;'>" + "<th width='" + (11 + extraColumnWidth) + "%' class='tableCellTextAlignLeft'>DATE</th>";
		if (isTransactionRefNumberEnabled) {
			dynamicRow += "<th width='10.5%' class='tableCellTextAlignLeft'>Order No.</th>";
		}
		dynamicRow += "<th width='" + (15 + extraColumnWidth) + "%' class='tableCellTextAlignLeft'>NAME</th><th width='" + (10.5 + extraColumnWidth) + "%'>Due date</th><th width='" + (11 + extraColumnWidth) + "%' class='tableCellTextAlignLeft'>Status</th><th width='" + (14 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>TOTAL</th><th width='" + (14 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>ADVANCE</th><th width='" + (14 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>BALANCE</th></tr></thead>";

		var totalAmount = 0;

		for (var i = 0; i < len; i++) {
			var txn = listOfTransactions[i];
			var printItemDetails = false;
			var printDescription = false;
			var classToBeIncluded = '';
			var txnItemLines = null;
			if (txn.getLineItems) {
				txnItemLines = txn.getLineItems();
			}
			printItemDetails = shouldPrintItemDetails && txnItemLines && txnItemLines.length;
			printDescription = shouldPrintDescription && txn.getDescription();

			var classToBeIncludedInStatementRow = '';

			if (shouldPrintItemDetails || shouldPrintDescription) {
				classToBeIncludedInStatementRow += 'boldText extraTopPadding ';
				if (printItemDetails || printDescription) {
					classToBeIncludedInStatementRow += ' noBorder ';
				}
				classToBeIncludedInStatementRow = " class='" + classToBeIncludedInStatementRow + "'";
			}

			var classToBeIncludedInItemDetailRow = '';
			if (printDescription) {
				classToBeIncludedInItemDetailRow = " class='noBorder' ";
			}

			var name = txn.getNameRef().getFullName();
			var date = MyDate.getDate('d/m/y', txn.getTxnDate());
			var dueDate = MyDate.getDate('d/m/y', txn.getTxnDueDate());
			var receivedAmt = txn.getCashAmount() ? txn.getCashAmount() : 0;
			var balanceAmt = txn.getBalanceAmount() ? txn.getBalanceAmount() : 0;
			var refNo = txn.getTxnRefNumber() === '' ? '' : txn.getFullTxnRefNumber();
			var totalAmt = Number(receivedAmt) + Number(balanceAmt);
			var txnStatus = txn.getStatus();

			totalAmount += totalAmt;

			var row = '<tr><td ' + classToBeIncludedInStatementRow + '>' + date + '</td>';
			if (isTransactionRefNumberEnabled) {
				row += '<td ' + classToBeIncludedInStatementRow + '>' + refNo + '</td>';
			}
			row += '<td ' + classToBeIncludedInStatementRow + '>' + name + '</td><td ' + classToBeIncludedInStatementRow + '>' + dueDate + '</td>' + '<td ' + classToBeIncludedInStatementRow + '>' + (txnStatus == TransactionStatus.TXN_ORDER_COMPLETE ? StringConstant.orderCompletedString : StringConstant.orderOpen) + '</td>' + "<td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalAmt) + "</td><td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(receivedAmt) + '</td>';
			row += "<td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(balanceAmt) + '</td></tr>';

			dynamicRow += row;

			var noOfColumns = isTransactionRefNumberEnabled ? 8 : 7;

			if (printItemDetails) {
				dynamicRow += '<tr>\n' + '  <td ' + classToBeIncludedInItemDetailRow + ' colspan="1" ></td>\n' + '  <td ' + classToBeIncludedInItemDetailRow + ' colspan="' + (noOfColumns - 1) + '">' + transactionHTMLGenerator.getItemDetailsForReport(txn) + '</td>\n' + '</tr>\n';
			}
			if (printDescription) {
				dynamicRow += '<tr>\n' + '  <td colspan="' + noOfColumns + '"> <span class="boldText"> Description: </span>' + txn.getDescription() + '</td>\n' + '</tr>\n';
			}
		}
		dynamicRow += '</table>';
		dynamicRow += "<h3 align='right'> Total: " + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalAmount) + '</h3>';
		return dynamicRow;
	};
};

module.exports = CustomReportHTMLGenerator;