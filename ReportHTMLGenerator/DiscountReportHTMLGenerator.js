var DiscountReportHTMLGenerator = function DiscountReportHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');
	var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
	var paymentInfoCache = new PaymentInfoCache();
	var NameCache = require('./../Cache/NameCache.js');
	var nameCache = new NameCache();

	this.getHTMLText = function (transactionList, fromDate, toDate, selectedFirmId) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'><h2 align='center'><u>Discount Rate Report</u></h2>" + ReportHTMLStyleSheet.getHTMLTextForDuration(fromDate, toDate) + ReportHTMLStyleSheet.getHTMLTextForFirm(selectedFirmId) + getHTMLTable(transactionList);
		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(listOfTransactions) {
		var len = listOfTransactions.length;
		var dynamicRow = '';
		dynamicRow += "<table class='reportTableForPrint' width='100%'><thead><tr style='background-color: lightgrey;'><th width='33%'>Party Name</th><th width='33%' class='tableCellTextAlignRight'>Sale Discount </th><th width='33%' class='tableCellTextAlignRight'>Purchase Discount </th></tr></thead>";

		//
		var totalSaleDiscount = 0;
		var totalPurchaseDiscount = 0;
		for (var i = 0; i < len; i++) {
			var txn = listOfTransactions[i];
			var partyName = txn.getPartyName();
			var saleDiscount = txn.getSaleDiscount();
			var purchaseDiscount = txn.getPurchaseDiscount();
			var row = "<tr><td width='33%'>" + partyName + '</td>';

			row += "<td class='tableCellTextAlignRight' width='33%'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(saleDiscount) + '</td>';
			row += "<td class='tableCellTextAlignRight' width='33%'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(purchaseDiscount) + '</td>';

			row += '</tr>';
			totalSaleDiscount += saleDiscount;
			totalPurchaseDiscount += purchaseDiscount;
			dynamicRow += row;
		}
		dynamicRow += '</table>';
		dynamicRow += "<table width='100%'><tr style='background-color: lightgrey;'><th width='33%' class='tableCellTextAlignLeft'>Total</th><th width='33%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalSaleDiscount) + "</th><th width='33%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalPurchaseDiscount) + '</th></tr></table>';

		return dynamicRow;
	};
};

module.exports = DiscountReportHTMLGenerator;