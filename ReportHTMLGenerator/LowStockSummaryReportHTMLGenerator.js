var LowStockSummaryReportHTMLGenerator = function LowStockSummaryReportHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');

	this.getHTMLText = function (itemList, printMinStockQuantity, printStockQuantity, printStockValue) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'><h2 align='center'><u>Low stock summary report</u></h2>" + getHTMLTable(itemList, printMinStockQuantity, printStockQuantity, printStockValue);

		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(itemList, printMinStockQuantity, printStockQuantity, printStockValue) {
		var len = itemList.length;

		var dynamicRow = '';

		dynamicRow += "<table class='reportTableForPrint' width='100%'><thead><tr style='background-color: lightgrey;'><th width='6%'></th><th width='*' class='tableCellTextAlignLeft'>Item Name</th>";

		if (printMinStockQuantity) {
			dynamicRow += "<th width='17%' class='tableCellTextAlignRight'>Minimum Stock Quantity</th>";
		}

		if (printStockQuantity) {
			dynamicRow += "<th width='17%' class='tableCellTextAlignRight'>Stock Quantity</th>";
		}
		if (printStockValue) {
			dynamicRow += "<th width='17%' class='tableCellTextAlignRight'>Stock Value</th>";
		}
		dynamicRow += '</tr></thead>';

		for (var i = 0; i < len; i++) {
			var itemObject = itemList[i];
			var row = '';
			row = '<tr><td>' + (i + 1) + '</td><td>' + itemObject.getItemName() + '</td>';
			if (printMinStockQuantity) {
				row += "<td align='right'>" + MyDouble.getQuantityWithDecimalWithoutColor(itemObject.getItemMinStockQuantity()) + '</td>';
			}
			if (printStockQuantity) {
				row += "<td align='right'>" + MyDouble.getQuantityWithDecimalWithoutColor(itemObject.getItemStockQuantity()) + '</td>';
			}
			if (printStockValue) {
				row += "<td align='right'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(itemObject.getItemStockValue()) + '</td>';
			}

			row += '</tr>';
			dynamicRow += row;
		}
		dynamicRow += '</table>';
		return dynamicRow;
	};
};

module.exports = LowStockSummaryReportHTMLGenerator;