var CustomReportHTMLGenerator = function CustomReportHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');
	var TxnPaymentStatusConstants = require('./../Constants/TxnPaymentStatusConstants.js');

	this.getHTMLText = function (transactionList, fromDate, toDate, selectedFirmId, nameString, selectedTransactionString, shouldPrintItemDetails, shouldPrintDescription, printPaymentStatus, paymentStatVal) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'><h2 align='center'><u>All Transactions Report</u></h2>" + '<h3>Party name: ' + nameString + '</h3>' + '<h3>Transaction type: ' + selectedTransactionString + '</h3>' + ReportHTMLStyleSheet.getHTMLTextForDuration(fromDate, toDate) + ReportHTMLStyleSheet.getHTMLTextForFirm(selectedFirmId) + getHTMLTable(transactionList, shouldPrintItemDetails, shouldPrintDescription, selectedTransactionString, printPaymentStatus, paymentStatVal);

		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(listOfTransactions, shouldPrintItemDetails, shouldPrintDescription, selectedTransactionString, printPaymentStatus, paymentStatVal) {
		var len = listOfTransactions.length;
		var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
		var TransactionHTMLGenerator = require('./../ReportHTMLGenerator/TransactionHTMLGenerator.js');
		var transactionHTMLGenerator = new TransactionHTMLGenerator();
		var SettingCache = require('./../Cache/SettingCache.js');
		var settingCache = new SettingCache();
		var dynamicRow = '';
		var isTransactionRefNumberEnabled = settingCache.getTxnRefNoEnabled();
		var isPaymentTermEnabled = settingCache.isPaymentTermEnabled();

		var dateColumnWidthRatio = 9;
		var refNumberColumnRatio = isTransactionRefNumberEnabled ? 11 : 0;
		var nameColumnRatio = 19;
		var txnTypeColumnRatio = 10;
		var totalAmountColumnRatio = 14;
		var cashAmountColumnRatio = 14;
		var balanceAmountColumnRatio = 14;
		var paymentStatusColumnRatio = printPaymentStatus && !isPaymentTermEnabled ? 18 : 0;
		var dueDateColumnRatio = isPaymentTermEnabled ? 10 : 0;
		var statusColumnRatio = isPaymentTermEnabled ? 8 : 0;

		var totalColumnWidth = dateColumnWidthRatio + refNumberColumnRatio + nameColumnRatio + txnTypeColumnRatio + totalAmountColumnRatio + cashAmountColumnRatio + balanceAmountColumnRatio + paymentStatusColumnRatio + dueDateColumnRatio + statusColumnRatio;

		var ShowPaymentStatusForTheseTransactions = [TxnTypeConstant.TXN_TYPE_SALE, TxnTypeConstant.TXN_TYPE_PURCHASE, TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN, TxnTypeConstant.TXN_TYPE_SALE_RETURN, TxnTypeConstant.TXN_TYPE_CASHIN, TxnTypeConstant.TXN_TYPE_CASHOUT, TxnTypeConstant.TXN_TYPE_ROPENBALANCE, TxnTypeConstant.TXN_TYPE_POPENBALANCE];

		dynamicRow += "<table class='reportTableForPrint' width='100%'><thead><tr style='background-color: lightgrey;'><th width='" + dateColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignLeft'>DATE</th>";
		if (isTransactionRefNumberEnabled) {
			dynamicRow += "<th width='" + refNumberColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignLeft'>Ref No.</th>";
		}
		dynamicRow += "<th width='" + nameColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignLeft'>NAME</th><th width='" + txnTypeColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignLeft'>TYPE</th>";

		if (printPaymentStatus) {
			if (isPaymentTermEnabled) {
				dynamicRow += "<th width='" + dueDateColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>DUE DATE</th>";
				dynamicRow += "<th width='" + statusColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>STATUS</th>";
			} else {
				dynamicRow += "<th width='" + paymentStatusColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>PAYMENT STATUS</th>";
			}
		}

		dynamicRow += "<th width='" + totalAmountColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>TOTAL</th><th width='" + cashAmountColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>RECEIVED / PAID</th><th width='" + balanceAmountColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>BALANCE</th></tr></thead>";

		var totalAmount = 0;

		for (var i = 0; i < len; i++) {
			var txn = listOfTransactions[i];
			var printItemDetails = false;
			var printDescription = false;
			var txnItemLines = null;
			if (txn.getLineItems) {
				txnItemLines = txn.getLineItems();
			}
			printItemDetails = shouldPrintItemDetails && txnItemLines && txnItemLines.length;
			printDescription = shouldPrintDescription && txn.getDescription();

			var classToBeIncludedInStatementRow = '';

			if (shouldPrintItemDetails || shouldPrintDescription) {
				classToBeIncludedInStatementRow += 'boldText extraTopPadding ';
				if (printItemDetails || printDescription) {
					classToBeIncludedInStatementRow += ' noBorder ';
				}
				classToBeIncludedInStatementRow = " class='" + classToBeIncludedInStatementRow + "'";
			}

			var classToBeIncludedInItemDetailRow = '';
			if (printDescription) {
				classToBeIncludedInItemDetailRow = " class='noBorder' ";
			}

			var typeString = TxnTypeConstant.getTxnTypeForUI(txn.getTxnType());
			var name = txn.getNameRef().getFullName();
			var date = MyDate.getDate('d/m/y', txn.getTxnDate());
			var receivedAmt = txn.getCashAmount() ? txn.getCashAmount() : 0;
			var balanceAmt = txn.getBalanceAmount() ? txn.getBalanceAmount() : 0;
			var txnCurrentBalanceAmount = txn.getTxnCurrentBalanceAmount() ? txn.getTxnCurrentBalanceAmount() : 0;

			var discountAmt = txn.getDiscountAmount() ? txn.getDiscountAmount() : 0;
			var refNo = txn.getTxnRefNumber() === '' ? '' : txn.getFullTxnRefNumber();
			var totalAmt = 0;
			if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHIN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHOUT) {
				totalAmt = Number(receivedAmt) + Number(discountAmt);
			} else {
				totalAmt = Number(receivedAmt) + Number(balanceAmt);
			}
			totalAmt = totalAmt || 0;

			var paymentStatus = txn.getTxnPaymentStatus();

			var paymentStatFilter = void 0;
			var dueDate = isPaymentTermEnabled ? MyDate.convertDateToStringForUI(txn.getTxnDueDate()) : new Date();
			var dueByDays = isPaymentTermEnabled ? MyDouble.getDueDays(txn.getTxnDueDate()) : 0;

			if (isPaymentTermEnabled && paymentStatVal == TxnPaymentStatusConstants.OVERDUE) {
				paymentStatFilter = dueByDays > 0 && paymentStatus != TxnPaymentStatusConstants.PAID;
			} else {
				paymentStatFilter = paymentStatVal ? paymentStatVal == paymentStatus : true;
			}

			paymentStatFilter = settingCache.isBillToBillEnabled() ? paymentStatFilter : true;

			var paymentStatusString = TxnPaymentStatusConstants.getPaymentStatusForUI(txn.getTxnType(), paymentStatus);

			if (!ShowPaymentStatusForTheseTransactions.includes(txn.getTxnType())) {
				paymentStatusString = '';
			}

			if (paymentStatFilter) {
				totalAmount += totalAmt;
				var row = '<tr><td ' + classToBeIncludedInStatementRow + '>' + date + '</td>';
				if (isTransactionRefNumberEnabled) {
					row += '<td ' + classToBeIncludedInStatementRow + '>' + refNo + '</td>';
				}
				row += '<td ' + classToBeIncludedInStatementRow + '>' + name + '</td><td ' + classToBeIncludedInStatementRow + '>' + typeString + '</td>';

				if (printPaymentStatus) {
					if (isPaymentTermEnabled) {
						if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE) {
							row += '<td ' + classToBeIncludedInStatementRow + " align='right' width='" + dueDateColumnRatio * 100 / totalColumnWidth + "%' " + classToBeIncludedInStatementRow + '>' + dueDate + '</td>';
							if (dueByDays > 0 && paymentStatus != TxnPaymentStatusConstants.PAID) {
								paymentStatusString = 'Overdue';
								row += '<td ' + classToBeIncludedInStatementRow + " align='right' width='" + statusColumnRatio * 100 / totalColumnWidth + "%' " + classToBeIncludedInStatementRow + '>';
								row += '<div>' + paymentStatusString + '</div>';
								row += '<div>' + dueByDays + ' Days</div>';
							} else {
								row += '<td ' + classToBeIncludedInStatementRow + " align='right' width='" + statusColumnRatio * 100 / totalColumnWidth + "%' " + classToBeIncludedInStatementRow + '>';
								row += '<div>' + paymentStatusString + '</div>';
							}
							row += '</td>';
						} else {
							row += '<td ' + classToBeIncludedInStatementRow + " align='right' width='" + dueDateColumnRatio * 100 / totalColumnWidth + "%'></td>";
							row += '<td ' + classToBeIncludedInStatementRow + " align='right' width='" + statusColumnRatio * 100 / totalColumnWidth + "%'>" + paymentStatusString + '</td>';
						}
					} else {
						row += '<td ' + classToBeIncludedInStatementRow + " align='right'  width='" + paymentStatusColumnRatio * 100 / totalColumnWidth + "%'>" + paymentStatusString + '</td>';
					}
				}

				row += "<td align='right' " + classToBeIncludedInStatementRow + '>' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalAmt) + '</td>';

				row += "<td align='right' " + classToBeIncludedInStatementRow + '>';

				if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHIN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHOUT || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_EXPENSE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_OTHER_INCOME) {
					row += MyDouble.getAmountWithDecimalAndCurrencyWithSign(Number(receivedAmt));
				} else if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
					var txnTotalReceivedAmt = totalAmt - txnCurrentBalanceAmount;
					row += MyDouble.getAmountWithDecimalAndCurrencyWithSign(txnTotalReceivedAmt);
				}

				row += '</td>';

				row += "<td align='right' " + classToBeIncludedInStatementRow + '>';

				if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
					row += MyDouble.getAmountWithDecimalAndCurrencyWithSign(txnCurrentBalanceAmount);
				}

				row += '</td></tr>';

				dynamicRow += row;

				var noOfColumns = isTransactionRefNumberEnabled ? 7 : 6;
				noOfColumns = printPaymentStatus ? noOfColumns + 1 : noOfColumns;
				noOfColumns = isPaymentTermEnabled ? noOfColumns + 1 : noOfColumns;

				if (printItemDetails) {
					dynamicRow += '<tr>\n' + '  <td ' + classToBeIncludedInItemDetailRow + ' colspan="1" ></td>\n' + '  <td ' + classToBeIncludedInItemDetailRow + ' colspan="' + (noOfColumns - 1) + '">' + transactionHTMLGenerator.getItemDetailsForReport(txn) + '</td>\n' + '</tr>\n';
				}
				if (printDescription) {
					dynamicRow += '<tr>\n' + '  <td colspan="' + noOfColumns + '"> <span class="boldText"> Description: </span>' + txn.getDescription() + '</td>\n' + '</tr>\n';
				}
			}
		}
		dynamicRow += '</table>';
		if (selectedTransactionString != 'All transactions') {
			dynamicRow += "<h3 align='right'> Total: " + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalAmount) + '</h3>';
		}
		return dynamicRow;
	};
};

module.exports = CustomReportHTMLGenerator;