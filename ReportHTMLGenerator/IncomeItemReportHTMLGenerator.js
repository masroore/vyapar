var IncomeReportHTMLGenerator = function IncomeReportHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');

	this.getHTMLText = function (transactionList, fromDate, toDate, selectedFirmId) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'><h2 align='center'><u>Income Item Report</u></h2>" + ReportHTMLStyleSheet.getHTMLTextForDuration(fromDate, toDate) + ReportHTMLStyleSheet.getHTMLTextForFirm(selectedFirmId) + getHTMLTable(transactionList);
		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(listOfTransactions) {
		var len = listOfTransactions.length;
		var dynamicRow = '';

		dynamicRow += "<table class='reportTableForPrint' width='100%'><thead><tr style='background-color: lightgrey;'><th width='50%' class='tableCellTextAlignLeft'>Income Item</th><th width='20%' class='tableCellTextAlignRight'>Quantity</th><th width='30%' class='tableCellTextAlignRight'>Amount</th></tr></thead>";

		var totalAmount = 0;
		var totalQuantity = 0;
		for (var i = 0; i < len; i++) {
			var txn = listOfTransactions[i];
			var name = txn.getItemName();
			var quantity = txn.getQty();
			var amount = txn.getAmount();
			totalAmount += amount;
			totalQuantity += quantity;
			var row = '<tr><td width="50%" class="tableCellTextAlignLeft">' + name + '</td><td width="20%" class="tableCellTextAlignRight">' + quantity + '</td><td class="tableCellTextAlignRight" width="30%">' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(amount) + '</td></tr>';
			dynamicRow += row;
		}
		dynamicRow += '</table>';

		dynamicRow += "<table width='100%'><tr style='background-color: lightgrey;'><th width='50%' class='tableCellTextAlignLeft'>Total</th><th width='20%' class='tableCellTextAlignRight'>" + totalQuantity + "</th><th width='30%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalAmount) + '</th></tr></table>';

		return dynamicRow;
	};
};

module.exports = IncomeReportHTMLGenerator;