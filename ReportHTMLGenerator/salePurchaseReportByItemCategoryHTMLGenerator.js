var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var salePurchaseReportByItemCategoryHTMLGenerator = function salePurchaseReportByItemCategoryHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');
	var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
	var paymentInfoCache = new PaymentInfoCache();
	var NameCache = require('./../Cache/NameCache.js');
	var nameCache = new NameCache();

	this.getHTMLText = function (transactionList, fromDate, toDate, nameString, selectedFirmId) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'><h2 align='center'><u>Sale Purchase By Item Category Report</u></h2>";
		// "<h3>Party name: " + nameString + "</h3>"+
		if (nameString) {
			var nameModel = nameCache.findNameModelByName(nameString);
			if (nameModel) {
				htmlText += '<h3>Party name: ' + nameModel.getFullName() + '</h3>';
				if (nameModel.getPhoneNumber()) {
					htmlText += '<h3>Party Contact: ' + nameModel.getPhoneNumber() + '</h3>';
				}
				if (nameModel.getEmail()) {
					htmlText += '<h3>Party Email: ' + nameModel.getEmail() + '</h3>';
				}
				if (nameModel.getAddress()) {
					htmlText += '<h3>Party Address: ' + nameModel.getAddress() + '</h3>';
				}
				if (settingCache.getGSTEnabled()) {
					if (nameModel.getGstinNumber()) {
						htmlText += '<h3>Party GSTIN: ' + nameModel.getGstinNumber() + '</h3>';
					}
				} else {
					if (nameModel.getTinNumber()) {
						htmlText += '<h3>Party ' + settingCache.getTINText() + ': ' + nameModel.getTinNumber() + '</h3>';
					}
				}
			}
		}
		htmlText += ReportHTMLStyleSheet.getHTMLTextForDuration(fromDate, toDate) + ReportHTMLStyleSheet.getHTMLTextForFirm(selectedFirmId) + getHTMLTable(transactionList);
		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(listOfTransactions) {
		var len = listOfTransactions.length;
		var dynamicRow = '';
		dynamicRow += "<table class='reportTableForPrint' width='100%'><thead><tr style='background-color: lightgrey;'><th width='28%'>Item Category</th><th width='18%' class='tableCellTextAlignRight'>Sale quantity </th><th width='18%' class='tableCellTextAlignRight'>Total Sale Amount</th><th width='18%' class='tableCellTextAlignRight'>Purchase Quantity </th><th width='18%' class='tableCellTextAlignRight'>Total Purchase Amount</th></tr></thead>";

		var totalSale = 0;
		var totalSaleFree = 0;
		var totalPurchase = 0;
		var totalPurchaseFree = 0;
		var totalSaleAmt = 0;
		var totalPurchaseAmt = 0;
		var _iteratorNormalCompletion = true;
		var _didIteratorError = false;
		var _iteratorError = undefined;

		try {
			for (var _iterator = (0, _getIterator3.default)(listOfTransactions), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
				var _ref = _step.value;

				var _ref2 = (0, _slicedToArray3.default)(_ref, 2);

				var key = _ref2[0];
				var value = _ref2[1];

				var itemLine = listOfTransactions.get(key);
				var itemName = itemLine.get('name');
				var saleQty = itemLine.get(TxnTypeConstant.TXN_TYPE_SALE);
				var purchaseQty = itemLine.get(TxnTypeConstant.TXN_TYPE_PURCHASE);
				var purchaseReturnQty = itemLine.get(TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN);
				var saleReturnQty = itemLine.get(TxnTypeConstant.TXN_TYPE_SALE_RETURN);

				var netSaleQty = (saleQty == null ? 0 : saleQty[0]) - (saleReturnQty == null ? 0 : saleReturnQty[0]);
				var netSaleFreeQty = (saleQty == null ? 0 : saleQty[1]) - (saleReturnQty == null ? 0 : saleReturnQty[1]);
				var netPurchaseQty = (purchaseQty == null ? 0 : purchaseQty[0]) - (purchaseReturnQty == null ? 0 : purchaseReturnQty[0]);
				var netPurchaseFreeQty = (purchaseQty == null ? 0 : purchaseQty[1]) - (purchaseReturnQty == null ? 0 : purchaseReturnQty[1]);
				var netSaleAmt = (saleQty == null ? 0 : saleQty[2]) - (saleReturnQty == null ? 0 : saleReturnQty[2]);
				var netPurchaseAmt = (purchaseQty == null ? 0 : purchaseQty[2]) - (purchaseReturnQty == null ? 0 : purchaseReturnQty[2]);
				var row = '';
				row += "<tr><td width='28%'>" + itemName + '</td>';

				row += "<td width='18%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(netSaleQty) + MyDouble.quantityDoubleToStringWithSignExplicitly(netSaleFreeQty, true) + '</td>';
				row += "<td width='18%' class='tableCellTextAlignRight'>" + MyDouble.getAmountForInvoicePrint(netSaleAmt) + '</td>';
				row += "<td width='18%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(netPurchaseQty) + MyDouble.quantityDoubleToStringWithSignExplicitly(netPurchaseFreeQty, true) + '</td>';
				row += "<td width='18%' class='tableCellTextAlignRight'>" + MyDouble.getAmountForInvoicePrint(netPurchaseAmt) + '</td>';

				row += '</tr>';

				dynamicRow += row;
				totalSale += netSaleQty;
				totalSaleFree += netSaleFreeQty;
				totalPurchase += netPurchaseQty;
				totalPurchaseFree += netPurchaseFreeQty;
				totalSaleAmt += netSaleAmt;
				totalPurchaseAmt += netPurchaseAmt;
			}
		} catch (err) {
			_didIteratorError = true;
			_iteratorError = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion && _iterator.return) {
					_iterator.return();
				}
			} finally {
				if (_didIteratorError) {
					throw _iteratorError;
				}
			}
		}

		dynamicRow += '</table>';
		dynamicRow += "<table width='100%'><tr style='background-color: lightgrey;'><th width='28%'>Total</th><th width='18%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(totalSale) + MyDouble.quantityDoubleToStringWithSignExplicitly(totalSaleFree, true) + "</th><th width='18%' class='tableCellTextAlignRight'>" + MyDouble.getAmountForInvoicePrint(totalSaleAmt) + "</th><th width='18%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(totalPurchase) + MyDouble.quantityDoubleToStringWithSignExplicitly(totalPurchaseFree, true) + "</th><th width='18%' class='tableCellTextAlignRight'>" + MyDouble.getAmountForInvoicePrint(totalPurchaseAmt) + '</th></tr></table>';

		return dynamicRow;
	};
};

module.exports = salePurchaseReportByItemCategoryHTMLGenerator;