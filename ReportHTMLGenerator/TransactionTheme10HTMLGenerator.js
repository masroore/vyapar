var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FirmCache = require('./../Cache/FirmCache');
var firmCache = new FirmCache();
var SettingCache = require('./../Cache/SettingCache');
var settingCache = new SettingCache();
var TxnTypeConstant = require('./../Constants/TxnTypeConstant');
var MyDate = require('./../Utilities/MyDate');
var CurrencyHelper = require('./../Utilities/CurrencyHelper');
var TransactionHTMLGenerator = require('./../ReportHTMLGenerator/TransactionHTMLGenerator');
var transactionHTMLGenerator = new TransactionHTMLGenerator();
var TaxCodeCache = require('./../Cache/TaxCodeCache.js');
var taxCodeCache = new TaxCodeCache();
var PaymentCache = require('./../Cache/PaymentInfoCache');
var PaymentInfoCache = new PaymentCache();
var MyDouble = require('./../Utilities/MyDouble');
var TaxCodeConstants = require('./../Constants/TaxCodeConstants');
var TransactionPrintSettings = require('./TransactionPrintSettings.js');
var DataLoader = require('./../DBManager/DataLoader');
var dataLoader = new DataLoader();
var TransactionPrintHelper = require('./../Utilities/TransactionPrintHelper.js');
var InvoiceTheme = require('./../Constants/InvoiceTheme');
var StateCode = require('./../Constants/StateCode.js');
var TransactionFactory = require('./../BizLogic/TransactionFactory.js');

var TransactionTheme10HTMLGenerator = {
	getCompanyHeaderForTheme10: function getCompanyHeaderForTheme10(txn, textSizeRatio) {
		var headerDetails = '';

		var firm = firmCache.getTransactionFirmWithDefault(txn);
		var imagePath = transactionHTMLGenerator.getCompanyLogoSource(firm, this.isForInvoicePreview);

		var companyDetailsHTML = '';
		var imageDetailsHTML = '';

		var textColor = '#000000';

		var companyAddress = firm.getFirmAddress().trim();
		companyAddress = companyAddress.replace(/(?:\r\n|\r|\n)/g, '<br/>');

		var alignProperty = " align='left' ";

		if (firm.getFirmName() && settingCache.isPrintCompanyNameEnabled()) {
			companyDetailsHTML += '<p ' + alignProperty + ' class="companyNameHeaderTextSize boldText">' + firm.getFirmName() + '</p>';
		}

		if (companyAddress && settingCache.isPrintCompanyAddressEnabled()) {
			companyDetailsHTML += '<p ' + alignProperty + ' class="bigTextSize">' + companyAddress + '</p>';
		}

		var companyPhoneToBePrinted = firm.getFirmPhone() && settingCache.isPrintCompanyNumberEnabled();
		var companyEmailToBePrinted = firm.getFirmEmail() && settingCache.isCompnayEmailEnabledOnPrint();

		if (companyPhoneToBePrinted) {
			companyDetailsHTML += '<p ' + alignProperty + ' class="bigTextSize">Phone no.: ' + firm.getFirmPhone() + '</p>';
		}
		if (companyEmailToBePrinted) {
			companyDetailsHTML += '<p ' + alignProperty + ' class="bigTextSize">Email: ' + firm.getFirmEmail() + '</p>';
		}

		if (settingCache.isPrintTINEnabled()) {
			if (settingCache.getGSTEnabled()) {
				var companyGSTINToBePrinted = firm.getFirmHSNSAC();
				var companyStateToBePrinted = firm.getFirmState();

				if (companyGSTINToBePrinted) {
					companyDetailsHTML += '<p ' + alignProperty + ' class="bigTextSize">GSTIN: ' + firm.getFirmHSNSAC() + '</p>';
				}
				if (companyStateToBePrinted) {
					companyDetailsHTML += '<p ' + alignProperty + ' class="bigTextSize">State: ' + StateCode.getStateCode(firm.getFirmState()) + '-' + firm.getFirmState() + '</p>';
				}
			} else {
				if (firm.getFirmTin()) {
					companyDetailsHTML += '<p ' + alignProperty + ' class="bigTextSize">' + settingCache.getTINText() + ': ' + firm.getFirmTin() + '</p>';
				}
			}
		}
		var alignment = 'left';
		companyDetailsHTML += transactionHTMLGenerator.getUdfHeaderDetails(firm, alignment);

		if (imagePath) {
			imageDetailsHTML += "<img src='" + imagePath + "' style='height: " + 84 * textSizeRatio + "px;'></img>";
		}

		if (this.isForInvoicePreview) {
			var classTextForEditBusinessDetails = 'class=\'invoicePreviewEditSection ' + transactionHTMLGenerator.getEditSectionClassName('firmDetails') + '\'';
			var classTextForEditLogo = 'class=\'invoicePreviewEditSection ' + transactionHTMLGenerator.getEditSectionClassName('firmLogo') + '\'';
			if (companyDetailsHTML) {
				companyDetailsHTML = '<div editSection=\'firmDetails\' ' + classTextForEditBusinessDetails + ' > ' + companyDetailsHTML + ' </div>';
			}
			if (imageDetailsHTML) {
				imageDetailsHTML = '<div editSection=\'firmLogo\' ' + classTextForEditLogo + ' > ' + imageDetailsHTML + ' </div>';
			}
		}

		headerDetails += '<table width="100%" style="color: ' + textColor + ';" > <tr>';

		if (imagePath) {
			headerDetails += "<td width='65%' style='vertical-align: center'>";
		} else {
			headerDetails += "<td width='100%' style='vertical-align: center'>";
		}

		headerDetails += companyDetailsHTML + '</td>';

		if (imagePath) {
			headerDetails += "<td width='35%' style='vertical-align: center;' text-align:right; align=\"right\">" + imageDetailsHTML + '</td>';
		}
		headerDetails += '</tr></table>';

		return headerDetails;
	},
	getCopyCountHTML: function getCopyCountHTML(txn) {
		var copyNumberHTML = '';
		var classForCheckBox = ' borderLeftForTxn borderRightForTxn borderBottomForTxn borderTopForTxn copyPrintNumberCheckBoxClass';
		var styleForCheckBox = 'border-color: black;  color:#ffffff;';

		if (settingCache.printCopyNumber() && (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN)) {
			copyNumberHTML = "<table width='100%'>" + '<tr>' + "<td width='55%'></td>" + "<td width='5px' class='" + classForCheckBox + "' style='" + styleForCheckBox + "'>O</td><td width='13%'  class='copyPrintNumberCheckBoxClass paddingLeft'>Original</td>" + "<td width='5px' class='" + classForCheckBox + "' style='" + styleForCheckBox + "'>O</td><td width='13%'  class='copyPrintNumberCheckBoxClass paddingLeft'>Duplicate</td>" + "<td width='5px' class='" + classForCheckBox + "' style='" + styleForCheckBox + "'>O</td><td width='13%'  class='copyPrintNumberCheckBoxClass paddingLeft'>Triplicate</td>" + '</tr></table>';
		}

		return copyNumberHTML;
	},
	getTransactionHeaderForTheme10: function getTransactionHeaderForTheme10(txn, theme, themeColor, textSizeRatio, printDeliveryChallan) {
		var styleText = '';

		if (themeColor == '#ffffff') {
			themeColor = 'black';
		}

		styleText += 'color: ' + themeColor + ';';

		var marginBottom = 15 * textSizeRatio;

		var transactionFactory = new TransactionFactory();

		var transactionString = transactionFactory.getTransTypeStringForTransactionPDF(txn.getTxnType(), txn.getTxnSubType(), printDeliveryChallan, transactionHTMLGenerator.isNonTaxBill(txn));

		var headerStyle = " class='theme10TxnHeaderTextSize boldText' ";

		return "<table width='100%' style='margin-bottom:" + marginBottom + "px;'> <tr> <td width='100%' align='center' " + headerStyle + " style='" + styleText + "'>" + transactionString + '</td></tr></table>';
	},
	getTxnFooterDetails: function getTxnFooterDetails(txn, themeColor, textSizeRatio, printAsDeliveryChallan) {
		var leftSideHTML = this.getLeftFooterTable(txn, printAsDeliveryChallan);
		var rightSideHTML = '';
		if (!printAsDeliveryChallan || settingCache.printAmountDetailsInDeliveryChallan()) {
			rightSideHTML = this.getAmountHTMLTable(txn, themeColor);
		}
		var bankAccountHTML = this.getCompanyBankDetails(txn, printAsDeliveryChallan);
		var upiDetailsHTML = transactionHTMLGenerator.getUPIPaymentDetails(txn, textSizeRatio);
		if (upiDetailsHTML) {
			if (bankAccountHTML) {
				bankAccountHTML = "<table width='100%'><tr><td width='50%' class='noPadding'>" + upiDetailsHTML + "</td><td width='50%' class='noPadding'>" + bankAccountHTML + '</td></tr></table>';
			} else {
				bankAccountHTML = upiDetailsHTML;
			}
		}
		var signatureHTML = this.getSignatureHTML(txn, textSizeRatio);

		if (!bankAccountHTML || !signatureHTML) {
			leftSideHTML += bankAccountHTML;
			rightSideHTML += signatureHTML;
			return "<table width=\"100%\"><tr><td width=\"50%\" valign='top' style=\"padding-right: " + 20 * textSizeRatio + 'px; padding-left: 0px; padding-top: 0px; ">' + leftSideHTML + '</td>' + "<td width=\"50%\" valign='top' style=\"padding-right: 0px; padding-left: " + 20 * textSizeRatio + 'px; padding-top: 0px; ">' + rightSideHTML + '</td></tr></table>';
		} else {
			var htmlText = "<table width=\"100%\"><tr><td width=\"50%\" valign='top' style=\"padding-right: " + 20 * textSizeRatio + 'px; padding-left: 0px; padding-top: 0px; ">' + leftSideHTML + '</td>' + "<td width=\"50%\" valign='top' style=\"padding-right: 0px; padding-left: " + 20 * textSizeRatio + 'px; padding-top: 0px; ">' + rightSideHTML + '</td></tr></table>';
			htmlText += "<table width=\"100%\"><tr><td width=\"50%\" valign='top' style=\"padding-right: " + 20 * textSizeRatio + 'px; padding-left: 0px; padding-top: 0px; ">' + bankAccountHTML + '</td>' + "<td width=\"50%\" valign='top' style=\"padding-right: 0px; padding-left: " + 20 * textSizeRatio + 'px; padding-top: 0px; ">' + signatureHTML + '</td></tr></table>';
			return htmlText;
		}
	},
	getTheme10TransactionHTMLForPreview: function getTheme10TransactionHTMLForPreview(txn, themeColor, pageSize, textSizeRatio, printDeliveryChallan) {
		this.isForInvoicePreview = true;
		var html = this.getTheme10TransactionHTML(txn, themeColor, pageSize, textSizeRatio, printDeliveryChallan);
		this.isForInvoicePreview = false;
		return html;
	},

	getTheme10TransactionHTML: function getTheme10TransactionHTML(txn, themeColor, pageSize, textSizeRatio, printDeliveryChallan) {
		var bodyHtml = '';

		bodyHtml = transactionHTMLGenerator.getExtraTopSpace(textSizeRatio) + this.getCopyCountHTML(txn) + this.getCompanyHeaderForTheme10(txn, textSizeRatio) + this.getTransactionHeaderForTheme10(txn, InvoiceTheme.THEME_10, themeColor, textSizeRatio, printDeliveryChallan) + transactionHTMLGenerator.getPartyInfoAndTxnHeaderData(txn, InvoiceTheme.THEME_10, themeColor, textSizeRatio);

		var transactionType = txn.getTxnType();
		if (transactionType == TxnTypeConstant.TXN_TYPE_SALE || transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE || transactionType == TxnTypeConstant.TXN_TYPE_EXPENSE || transactionType == TxnTypeConstant.TXN_TYPE_OTHER_INCOME || transactionType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || transactionType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || transactionType == TxnTypeConstant.TXN_TYPE_ESTIMATE || transactionType == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
			bodyHtml += transactionHTMLGenerator.getItemDetails(txn, InvoiceTheme.THEME_10, themeColor, textSizeRatio, printDeliveryChallan);
		}
		bodyHtml += this.getTxnFooterDetails(txn, themeColor, textSizeRatio, printDeliveryChallan);
		return bodyHtml;
	},
	getDescriptionData: function getDescriptionData(txn) {
		var description = txn.getDescription();

		if (settingCache.isPrintDescriptionEnabled() && description) {
			description = description.replace(/(?:\r\n|\r|\n)/g, '<br/>');
			return "<table width='100%' class='theme10amountSection'><tr><th align='left' width='100%' class='theme10BoxHeading'> DESCRIPTION</th></tr>" + "<tr><td width='100%' class='theme10BoxStyle' >" + description + '</td></tr></table>';
		}

		return '';
	},

	getAmountInWordsHTML: function getAmountInWordsHTML(txn, printAsDeliveryChallan) {
		if (!printAsDeliveryChallan || settingCache.printAmountDetailsInDeliveryChallan()) {
			var balanceAmount = txn.getBalanceAmount() ? Number(txn.getBalanceAmount()) : 0;
			var cashAmount = txn.getCashAmount() ? Number(txn.getCashAmount()) : 0;
			var discountAmount = txn.getDiscountAmount() ? Number(txn.getDiscountAmount()) : 0;
			var reverseChargeAmount = txn.getReverseChargeAmount() ? Number(txn.getReverseChargeAmount()) : 0;
			var totalAmount = balanceAmount + cashAmount + reverseChargeAmount;

			if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHIN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHOUT) {
				totalAmount = totalAmount + discountAmount;
			} else {
				balanceAmount = txn.getTxnCurrentBalanceAmount() ? Number(txn.getTxnCurrentBalanceAmount()) : 0; // setting current balance amount
				cashAmount = totalAmount - balanceAmount - reverseChargeAmount; // setting total received amount
			}

			var amountHeading = 'AMOUNT IN WORDS';
			switch (txn.getTxnType()) {
				case TxnTypeConstant.TXN_TYPE_SALE:
					amountHeading = 'INVOICE ' + amountHeading;
					break;
				case TxnTypeConstant.TXN_TYPE_PURCHASE:
					amountHeading = 'BILL ' + amountHeading;
					break;
				case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
				case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
					amountHeading = 'ORDER ' + amountHeading;
					break;
				case TxnTypeConstant.TXN_TYPE_ESTIMATE:
					amountHeading = 'ESTIMATE ' + amountHeading;
					break;
				case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
					amountHeading = 'DELIVERY CHALLAN ' + amountHeading;
					break;
			}

			var amountWordInString = CurrencyHelper.getAmountInWords(totalAmount);

			return "<table width='100%' class='theme10amountSection'><tr><th align='left' width='100%' class='theme10BoxHeading'> " + amountHeading + '</th></tr>' + "<tr><td width='100%' class='theme10BoxStyle' >" + amountWordInString + '</td></tr></table>';
		}

		return '';
	},

	getTermsAndConditionsHTML: function getTermsAndConditionsHTML(txn, printAsDeliveryChallan) {
		switch (txn.getTxnType()) {
			case TxnTypeConstant.TXN_TYPE_SALE:
				var termsAndConditionText = settingCache.getTermsAndConditionSaleInvoivce();
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
				var termsAndConditionText = settingCache.getTermsAndConditionSaleOrder();
				break;
			case TxnTypeConstant.TXN_TYPE_ESTIMATE:
				var termsAndConditionText = settingCache.getTermsAndConditionEstimateQuotation();
				break;
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
				var termsAndConditionText = settingCache.getTermsAndConditionDeliveryChallan();
				break;
		}

		if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE && settingCache.isPrintTermsAndConditionSaleInvoiceEnabled() || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER && settingCache.isPrintTermsAndConditionSaleOrderEnabled() || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE && settingCache.isPrintTermsAndConditionEstimateQuotationEnabled()) {
			if (printAsDeliveryChallan && settingCache.isPrintTermsAndConditionDeliveryChallanEnabled()) {
				var termsAndConditionText = settingCache.getTermsAndConditionDeliveryChallan();
			}

			if (!termsAndConditionText) {
				termsAndConditionText = settingCache.getTermsAndCondition() || 'Thanks for doing business with us!';
			}
			termsAndConditionText = termsAndConditionText.replace(/(?:\r\n|\r|\n)/g, '<br/>');
			var termsAndConditionsHTML = '<table width=\'100%\' class=\'theme10amountSection\'><tr><th align=\'left\' width=\'100%\' class=\'theme10BoxHeading\'>TERMS AND CONDITIONS</th></tr>' + "<tr><td width='100%' class='theme10BoxStyle' >" + termsAndConditionText + '</td></tr></table></div>';
			if (printAsDeliveryChallan && !settingCache.isPrintTermsAndConditionDeliveryChallanEnabled()) {
				termsAndConditionsHTML = '';
			}
			if (this.isForInvoicePreview) {
				var classText = ' class=\'invoicePreviewEditSection ' + transactionHTMLGenerator.getEditSectionClassName('termsAndConditions') + '\'';
				termsAndConditionsHTML = '\n\t\t\t\t\t<div style=\'width: 100%;\' editsection=\'termsAndConditions\' ' + classText + '>\n\t\t\t\t\t' + termsAndConditionsHTML + '\n\t\t\t\t\t</div>';
			}
			return termsAndConditionsHTML;
		}

		return '';
	},
	getCompanyBankDetails: function getCompanyBankDetails(txn, printAsDeliveryChallan) {
		if ((txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) && settingCache.getUserBankEnabled() && !printAsDeliveryChallan) {
			var bankDetails = '';
			var firm = firmCache.getTransactionFirmWithDefault(txn);
			if (firm != null) {
				if (firm.getBankName()) {
					bankDetails += '<tr><td width="100%">Bank Name: ' + firm.getBankName() + '</td></tr>';
				}
				if (firm.getAccountNumber()) {
					bankDetails += '<tr><td width="100%">Bank Account No.: ' + firm.getAccountNumber() + '</td></tr>';
				}
				if (firm.getBankIFSC()) {
					bankDetails += '<tr><td width="100%">Bank IFSC code: ' + firm.getBankIFSC() + '</td></tr>';
				}
				var bankDetailsHTML = bankDetails ? "<table style='color:#2B4C56'><tr><th align='left' width='100%'> Pay To-</th></tr>" + bankDetails + '</table>' : '';

				if (this.isForInvoicePreview) {
					bankDetailsHTML = bankDetailsHTML || "<table style='color:#2B4C56'><tr><th align='left' width='100%'> Pay To-</th></tr></table>";
					var showEditView = transactionHTMLGenerator.getEditSectionClassName('payToDetails');
					var classText = ' class=\'invoicePreviewEditSection ' + showEditView + '\' ';
					bankDetailsHTML = '<div style="width:100%;" editsection=\'payToDetails\' ' + classText + '>' + bankDetailsHTML + '</div>';
				}
				return bankDetailsHTML;
			}
		}
		return '';
	},

	getDeliveryChallanReceivedCopySign: function getDeliveryChallanReceivedCopySign(printAsDeliveryChallan) {
		var receivedByHTML = '';
		if (printAsDeliveryChallan) {
			receivedByHTML = "<table width='100%' class='theme10amountSection'><tr><th align='left' width='100%' class='theme10BoxHeading'> Received By</th></tr>" + "<tr><td width='100%' class='theme10BoxStyle' ><p>Name: </p><p>Comment: </p><p>Date: </p><p>Signature: </p></td></tr></table>";
			receivedByHTML += "<table width='100%' class='theme10amountSection'><tr><th align='left' width='100%' class='theme10BoxHeading'> Delivered By</th></tr>" + "<tr><td width='100%' class='theme10BoxStyle' ><p>Name: </p><p>Comment: </p><p>Date: </p><p>Signature: </p></td></tr></table>";
		}
		return receivedByHTML;
	},
	getAmountHTMLTable: function getAmountHTMLTable(txn, themeColor) {
		var amountHTML = '<table width="100%" class=\'theme10amountSection\'>';
		var textColorForHeading = 'white';
		if (themeColor == '#ffffff') {
			textColorForHeading = 'black';
		}

		var itemSubTotalBeforeDiscAndTax = 0;
		var lineItemTotalDiscount = 0;

		var lineItems = txn.getLineItems();

		var txnType = txn.getTxnType();
		var totalAdditionalCESS = 0;
		var taxDetailMap = {};
		if (lineItems.length > 0 && (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_EXPENSE || txnType == TxnTypeConstant.TXN_TYPE_OTHER_INCOME || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txnType == TxnTypeConstant.TXN_TYPE_ESTIMATE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER)) {
			$.each(lineItems, function (key, lineItem) {
				var qty = lineItem.getItemQuantity();
				var price = lineItem.getItemUnitPrice();
				itemSubTotalBeforeDiscAndTax += qty * price;
				lineItemTotalDiscount += Number(lineItem.getLineItemDiscountAmount());
			});
			amountHTML += '<tr><td>Sub Total</td><td align="right">' + MyDouble.getAmountForInvoicePrint(itemSubTotalBeforeDiscAndTax) + '</td></tr>';
			if (lineItemTotalDiscount > 0) {
				amountHTML += '<tr><td>Discount</td><td align="right">' + MyDouble.getAmountForInvoicePrint(lineItemTotalDiscount) + '</td></tr>';
			}
			var taxCodeDistributionDataForLineItems = transactionHTMLGenerator.getTaxCodeDistributionData(txn, true, false);
			totalAdditionalCESS = taxCodeDistributionDataForLineItems.totalAdditionalCESS;
			taxDetailMap = taxCodeDistributionDataForLineItems.taxDetailMap;
			var taxIds = (0, _keys2.default)(taxDetailMap);
			for (var i = 0; i < taxIds.length; i++) {
				var taxId = taxIds[i];

				var taxCode = taxCodeCache.getTaxCodeObjectById(taxId);
				var taxAmount = 0;
				var amounts = taxDetailMap[taxId];
				for (var j = 0; j < amounts.length; j++) {
					var amount = amounts[j];
					taxAmount += (taxCode.getTaxRate() ? Number(taxCode.getTaxRate()) : 0) * amount / 100;
				}
				amountHTML += '<tr><td>';
				if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
					amountHTML += 'IGST';
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
					amountHTML += 'CGST';
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
					amountHTML += transactionHTMLGenerator.stateTaxPlace;
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
					amountHTML += 'CESS';
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.STATE_SPECIFIC_CESS) {
					amountHTML += taxCode.getTaxCodeName();
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.OTHER) {
					amountHTML += taxCode.getTaxCodeName();
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.Exempted) {
					amountHTML += 'Exempted';
				}
				amountHTML += '@' + MyDouble.getPercentageWithDecimal(taxCode.getTaxRate()) + '%</td>';
				amountHTML += "<td align='right' >" + MyDouble.getAmountForInvoicePrint(taxAmount) + '</td></tr>';
			}
			if (totalAdditionalCESS > 0) {
				amountHTML += '<tr><td>' + TransactionPrintSettings.getAdditionCESSColumnHeader() + "</td> <td align='right'>" + MyDouble.getAmountForInvoicePrint(totalAdditionalCESS) + '</td></tr>';
			}
		}

		var balanceAmount = txn.getBalanceAmount() ? Number(txn.getBalanceAmount()) : 0;
		var cashAmount = txn.getCashAmount() ? Number(txn.getCashAmount()) : 0;
		var reverseChargeAmount = txn.getReverseChargeAmount() ? Number(txn.getReverseChargeAmount()) : 0;
		var discountAmount = txn.getDiscountAmount() ? Number(txn.getDiscountAmount()) : 0;
		var totalAmount = Number(balanceAmount + cashAmount + reverseChargeAmount);

		if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
			totalAmount = totalAmount + discountAmount;
		} else {
			balanceAmount = txn.getTxnCurrentBalanceAmount() ? Number(txn.getTxnCurrentBalanceAmount()) : 0; // setting current balance amount
			cashAmount = totalAmount - balanceAmount - reverseChargeAmount; // setting total received amount
		}

		if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN) {
			amountHTML += '<tr><td>Received</td><td align="right">' + MyDouble.getAmountForInvoicePrint(cashAmount) + '</td></tr>';
		} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
			amountHTML += '<tr><td>Paid</td><td align="right">' + MyDouble.getAmountForInvoicePrint(cashAmount) + '</td></tr>';
		}

		if (discountAmount != 0) {
			if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
				amountHTML += '<tr><td>Discount</td><td align="right">' + MyDouble.getAmountForInvoicePrint(discountAmount) + '</td></tr>';
			} else {
				amountHTML += '<tr><td>Discount (' + MyDouble.getPercentageWithDecimal(txn.getDiscountPercent()) + '%)</td><td align="right">' + MyDouble.getAmountForInvoicePrint(discountAmount) + '</td></tr>';
			}
		}

		if (txn.getTransactionTaxId()) {
			var taxCodeDistributionDataForTransaction = transactionHTMLGenerator.getTaxCodeDistributionData(txn, false, true);
			var _taxDetailMap = taxCodeDistributionDataForTransaction.taxDetailMap;
			var _taxIds = (0, _keys2.default)(_taxDetailMap);
			for (var _i = 0; _i < _taxIds.length; _i++) {
				var _taxId = _taxIds[_i];
				var _taxCode = taxCodeCache.getTaxCodeObjectById(_taxId);
				var _taxAmount = 0;
				var _amounts = _taxDetailMap[_taxId];
				for (var _j = 0; _j < _amounts.length; _j++) {
					_taxAmount += (_taxCode.getTaxRate() ? Number(_taxCode.getTaxRate()) : 0) * _amounts[_j] / 100;
				}
				amountHTML += '<tr><td>';
				if (_taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
					amountHTML += 'IGST';
				} else if (_taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
					amountHTML += 'CGST';
				} else if (_taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
					amountHTML += transactionHTMLGenerator.stateTaxPlace;
				} else if (_taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
					amountHTML += 'CESS';
				} else if (_taxCode.getTaxRateType() == TaxCodeConstants.STATE_SPECIFIC_CESS) {
					amountHTML += _taxCode.getTaxCodeName();
				} else if (_taxCode.getTaxRateType() == TaxCodeConstants.OTHER) {
					amountHTML += _taxCode.getTaxCodeName();
				} else if (_taxCode.getTaxRateType() == TaxCodeConstants.Exempted) {
					amountHTML += 'Exempted';
				}
				amountHTML += '@' + MyDouble.getPercentageWithDecimal(_taxCode.getTaxRate()) + '%</td>';
				amountHTML += "<td align='right' >" + MyDouble.getAmountForInvoicePrint(_taxAmount) + '</td></tr>';
			}
		}

		if (txn.getAc1Amount() != 0) {
			amountHTML += '<tr><td>' + dataLoader.getCurrentExtraChargesValue('1') + '</td><td align="right">' + MyDouble.getAmountForInvoicePrint(txn.getAc1Amount()) + '</td></tr>';
		}
		if (txn.getAc2Amount() != 0) {
			amountHTML += '<tr><td>' + dataLoader.getCurrentExtraChargesValue('2') + '</td><td align="right">' + MyDouble.getAmountForInvoicePrint(txn.getAc2Amount()) + '</td></tr>';
		}
		if (txn.getAc3Amount() != 0) {
			amountHTML += '<tr><td>' + dataLoader.getCurrentExtraChargesValue('3') + '</td><td align="right">' + MyDouble.getAmountForInvoicePrint(txn.getAc3Amount()) + '</td></tr>';
		}

		if (txn.getRoundOffValue() != 0) {
			amountHTML += '<tr><td>Round off</td><td align="right">' + MyDouble.getAmountForInvoicePrint(txn.getRoundOffValue()) + '</td></tr>';
		}

		if (txnType != TxnTypeConstant.TXN_TYPE_CASHIN && txnType != TxnTypeConstant.TXN_TYPE_CASHOUT || (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) && discountAmount != 0) {
			amountHTML += "<tr style='background-color:" + themeColor + '; color:' + textColorForHeading + ';\'><td><b>Total</b></td><td align="right"><b>' + MyDouble.getAmountForInvoicePrint(totalAmount) + '</b></td></tr>';
		}

		if ((txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) && txn.getReverseCharge() && reverseChargeAmount > 0) {
			var totalPayableString = 'Total payable amount';
			if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
				totalPayableString = 'Total receivable amount';
			}
			amountHTML += "<tr><td>Tax under reverse charge</td><td align='right'>" + MyDouble.getAmountForInvoicePrint(reverseChargeAmount) + '</td></tr>';
			amountHTML += '<tr><td>' + totalPayableString + "</td><td align='right'>" + MyDouble.getAmountForInvoicePrint(totalAmount - reverseChargeAmount) + '</td></tr>';
		}

		if (settingCache.isReceivedAmountEnabled()) {
			if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
				amountHTML += '<tr><td>Paid</td><td align="right">' + MyDouble.getAmountForInvoicePrint(cashAmount) + '</td></tr>';
			} else if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
				amountHTML += '<tr><td>Received</td><td align="right">' + MyDouble.getAmountForInvoicePrint(cashAmount) + '</td></tr>';
			} else if (txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
				amountHTML += '<tr><td>Advance</td><td align="right">' + MyDouble.getAmountForInvoicePrint(cashAmount) + '</td></tr>';
			}
		}

		if (settingCache.isReceivedAmountEnabled()) {
			if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
				amountHTML += '<tr><td>Balance</td><td align="right" >' + MyDouble.getAmountForInvoicePrint(balanceAmount) + '</td></tr>';
			}
		}

		if (settingCache.printPaymentMode() && txn.getTxnType() != TxnTypeConstant.TXN_TYPE_ESTIMATE && txn.getTxnType() != TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN) {
			var paymentTypeDescription = '';
			if (cashAmount == 0) {
				paymentTypeDescription = 'Credit';
			} else {
				paymentTypeDescription = PaymentInfoCache.getPaymentInfoObjById(txn.getPaymentTypeId());
				if (paymentTypeDescription) {
					paymentTypeDescription = paymentTypeDescription.getName();
				}
			}
			if (txn.getPaymentTypeReference()) {
				paymentTypeDescription += ' (' + txn.getPaymentTypeReference() + ')';
			}
			if (paymentTypeDescription) {
				amountHTML += '<tr><td>Payment Mode</td><td align="right" >' + paymentTypeDescription + '</td></tr>';
			}
		}

		if (settingCache.isCurrentBalanceOfPartyEnabled()) {
			try {
				var name = txn.getNameRef();
				switch (txnType) {
					case TxnTypeConstant.TXN_TYPE_CASHIN:
					case TxnTypeConstant.TXN_TYPE_SALE:
					case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
					case TxnTypeConstant.TXN_TYPE_CASHOUT:
					case TxnTypeConstant.TXN_TYPE_PURCHASE:
					case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
					case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
						if (name != null) {
							var currentAmount = MyDouble.getAmountForInvoicePrint(name.getAmount());
							var showPreviousBalance = true;
							if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
								var txnBalAmount = MyDouble.convertStringToDouble(txn.getBalanceAmount());
								var txnCurrentBalAmount = MyDouble.convertStringToDouble(txn.getTxnCurrentBalanceAmount());
								if (Math.abs(txnBalAmount - txnCurrentBalAmount) > 0.001) {
									showPreviousBalance = false;
								}
							}
							if (showPreviousBalance) {
								showPreviousBalance = TransactionPrintHelper.isLatestTransaction(txn);
							}
							if (showPreviousBalance) {
								var amount = TransactionPrintHelper.getPreviousAmount(txn, name);
								amountHTML += '<tr><td>Previous Balance</td><td align="right">' + MyDouble.getAmountForInvoicePrint(amount) + '</td></tr>';
							}
							if (currentAmount && txnType != TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN) {
								amountHTML += '<tr><td>Current Balance</td><td align="right">' + currentAmount + '</td></tr>';
							}
						}
						break;
				}
			} catch (e) {}
		}

		amountHTML += '<tr><td class="borderTopForTxn theme10BorderColor"></td><td align="right" class="borderTopForTxn theme10BorderColor"></td></tr>';

		amountHTML += '</table>';

		return amountHTML;
	},

	getLeftFooterTable: function getLeftFooterTable(txn, printAsDeliveryChallan) {
		var leftText = this.getDescriptionData(txn);
		leftText += this.getAmountInWordsHTML(txn, printAsDeliveryChallan);
		leftText += this.getTermsAndConditionsHTML(txn, printAsDeliveryChallan);
		leftText += this.getDeliveryChallanReceivedCopySign(printAsDeliveryChallan);
		return leftText;
	},

	getSignatureHTML: function getSignatureHTML(txn, textSizeRatio) {
		if (settingCache.isSignatureEnabled()) {
			var firm = firmCache.getTransactionFirmWithDefault(txn);

			var imagePath = firm.getFirmSignImagePath();
			var signatureHTML = '<table align="right" width="100%">' + '<tr><td style=\'text-align:center; color: #2B4C56\' align="center">For, ' + firm.getFirmName() + '</td></tr>' + '<tr><td style="height: ' + 84 * textSizeRatio + 'px; text-align:center;" align="center">';

			if (imagePath) {
				signatureHTML += '<img src="data:image/png;base64,' + imagePath + '" style="height: ' + 64 * textSizeRatio + 'px; width: ' + 168 * textSizeRatio + 'px;"></img>';
			}

			signatureHTML += '</td></tr><tr><td align="center" style="text-align:center;" >' + settingCache.getSignatureText() + '</td></tr></table>';
			if (this.isForInvoicePreview) {
				var invoicePreviewAttrs = ' class="invoicePreviewEditSection ' + transactionHTMLGenerator.getEditSectionClassName('signatureLogo') + '"';
				signatureHTML = '\n\t\t\t\t<div style="width:100%;" editsection="signatureLogo" ' + invoicePreviewAttrs + '>\n\t\t\t\t\t' + signatureHTML + '\n\t\t\t\t</div>';
			}

			return signatureHTML;
		}
		return '';
	}
};

module.exports = TransactionTheme10HTMLGenerator;