var TaxReportHTMLGenerator = function TaxReportHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');
	var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
	var paymentInfoCache = new PaymentInfoCache();
	var NameCache = require('./../Cache/NameCache.js');
	var nameCache = new NameCache();

	this.getHTMLText = function (transactionList, fromDate, toDate, selectedFirmId) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'><h2 align='center'><u>Tax Report</u></h2>" + ReportHTMLStyleSheet.getHTMLTextForDuration(fromDate, toDate) + ReportHTMLStyleSheet.getHTMLTextForFirm(selectedFirmId) + getHTMLTable(transactionList);
		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getHTMLTable = function getHTMLTable(listOfTransactions) {
		var len = listOfTransactions.length;
		var dynamicRow = '';
		dynamicRow += "<table class='reportTableForPrint' width='100%'><thead><tr style='background-color: lightgrey;'><th width='33%'>Party Name</th><th width='33%' class='tableCellTextAlignRight'>Sale Tax </th><th width='33%' class='tableCellTextAlignRight'>Purchase Tax </th></tr></thead>";

		//
		var totalTaxIn = 0;
		var totalTaxOut = 0;
		for (var i = 0; i < len; i++) {
			var txn = listOfTransactions[i];
			var partyName = txn.getPartyName();
			var taxIn = txn.getTaxIn();
			var taxOut = txn.getTaxOut();
			var row = "<tr><td width='33%'>" + partyName + '</td>';

			row += "<td class='tableCellTextAlignRight' width='33%'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(taxIn) + '</td>';
			row += "<td class='tableCellTextAlignRight' width='33%'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(taxOut) + '</td>';

			row += '</tr>';
			totalTaxIn += taxIn;
			totalTaxOut += taxOut;
			dynamicRow += row;
		}
		dynamicRow += '</table>';
		dynamicRow += "<table width='100%'><tr style='background-color: lightgrey;'><th width='33%' class='tableCellTextAlignLeft'>Total</th><th width='33%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalTaxIn) + "</th><th width='33%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalTaxOut) + '</th></tr></table>';

		return dynamicRow;
	};
};

module.exports = TaxReportHTMLGenerator;