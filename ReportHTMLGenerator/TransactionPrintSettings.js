var SettingCache = require('./../Cache/SettingCache.js');
var Queries = require('./../Constants/Queries.js');
var Defaults = require('./../Constants/Defaults.js');
var settingCache = new SettingCache();
var StateCode = require('./../Constants/StateCode.js');
var TransactionPrintSettings = {
	printSINumberColumnInInvoicePrint: function printSINumberColumnInInvoicePrint() {
		return settingCache.getValue(Queries.SETTING_PRINT_SINNUMBER_ENABLED, Defaults.DEFAULT_PRINT_SINNUMBER_ENABLED) === '1';
	},

	printItemNameInInvoicePrint: function printItemNameInInvoicePrint() {
		return true;
	},

	printItemCodeInInvoicePrint: function printItemCodeInInvoicePrint() {
		return settingCache.getValue(Queries.SETTING_PRINT_ITEMCODE_ENABLED, Defaults.DEFAULT_PRINT_ITEMCODE_ENABLED) === '1';
	},

	printHSNCodeInInvoicePrint: function printHSNCodeInInvoicePrint() {
		return settingCache.getValue(Queries.SETTING_PRINT_HSNCODE_ENABLED, Defaults.DEFAULT_PRINT_HSNCODE_ENABLED) === '1';
	},

	printItemCountInInvoicePrint: function printItemCountInInvoicePrint() {
		return settingCache.getValue(Queries.SETTING_PRINT_ITEMCOUNT_ENABLED, Defaults.DEFAULT_PRINT_ITEMCOUNT_ENABLED) === '1';
	},

	printItemBatchInInvoicePrint: function printItemBatchInInvoicePrint() {
		return settingCache.getValue(Queries.SETTING_PRINT_BATCHNO_ENABLED, Defaults.DEFAULT_PRINT_BATCHNO_ENABLED) === '1';
	},

	printItemExpiryDateInInvoicePrint: function printItemExpiryDateInInvoicePrint() {
		return settingCache.getValue(Queries.SETTING_PRINT_ITEMEXPIRYDATE_ENABLED, Defaults.DEFAULT_PRINT_ITEMEXPIRYDATE_ENABLED) === '1';
	},

	printItemManufacturingDateInInvoicePrint: function printItemManufacturingDateInInvoicePrint() {
		return settingCache.getValue(Queries.SETTING_PRINT_ITEMMANUFACTURINGDATE_ENABLED, Defaults.DEFAULT_PRINT_ITEMMANUFACTURINGDATE_ENABLED) === '1';
	},

	printItemMRPInInvoicePrint: function printItemMRPInInvoicePrint() {
		return settingCache.getValue(Queries.SETTING_PRINT_ITEMMRP_ENABLED, Defaults.DEFAULT_PRINT_ITEMMRP_ENABLED) === '1';
	},

	printItemSizeInInvoicePrint: function printItemSizeInInvoicePrint() {
		return settingCache.getValue(Queries.SETTING_PRINT_ITEMSIZE_ENABLED, Defaults.DEFAULT_PRINT_ITEMSIZE_ENABLED) === '1';
	},

	printItemSerialNumberInInvoicePrint: function printItemSerialNumberInInvoicePrint() {
		return settingCache.getValue(Queries.SETTING_PRINT_ITEMSERIALNUMBER_ENABLED, Defaults.DEFAULT_PRINT_ITEMSERIALNUMBER_ENABLED) === '1';
	},

	printItemDescriptionInInvoicePrint: function printItemDescriptionInInvoicePrint() {
		return settingCache.getValue(Queries.SETTING_PRINT_ITEMDESCRIPTION_ENABLED, Defaults.DEFAULT_PRINT_ITEMDESCRIPTION_ENABLED) === '1';
	},

	printItemQuantityInInvoicePrint: function printItemQuantityInInvoicePrint() {
		return settingCache.getValue(Queries.SETTING_PRINT_ITEMQUANTITY_ENABLED, Defaults.DEFAULT_PRINT_ITEMQUANTITY_ENABLED) === '1';
	},

	printItemUnitInInvoicePrint: function printItemUnitInInvoicePrint() {
		return settingCache.getValue(Queries.SETTING_PRINT_ITEMUNIT_ENABLED, Defaults.DEFAULT_PRINT_ITEMUNIT_ENABLED) === '1';
	},

	printItemPriceInInvoicePrint: function printItemPriceInInvoicePrint() {
		return settingCache.getValue(Queries.SETTING_PRINT_ITEMPRICE_ENABLED, Defaults.DEFAULT_PRINT_ITEMPRICE_ENABLED) === '1';
	},

	printTaxableItemPriceInInvoicePrint: function printTaxableItemPriceInInvoicePrint() {
		return settingCache.getValue(Queries.SETTING_PRINT_TAXABLE_ITEMPRICE_ENABLED, Defaults.DEFAULT_PRINT_TAXABLE_ITEMPRICE_ENABLED) === '1';
	},

	printItemDiscountAmountInInvoicePrint: function printItemDiscountAmountInInvoicePrint() {
		return settingCache.getValue(Queries.SETTING_PRINT_ITEMDISCOUNTAMOUNT_ENABLED, Defaults.DEFAULT_PRINT_ITEMDISCOUNTAMOUNT_ENABLED) === '1';
	},

	printItemDiscountPercentageInInvoicePrint: function printItemDiscountPercentageInInvoicePrint() {
		return settingCache.getValue(Queries.SETTING_PRINT_ITEMDISCOUNTPERCENTAGE_ENABLED, Defaults.DEFAULT_PRINT_ITEMDISCOUNTPERCENTAGE_ENABLED) === '1';
	},

	printItemTaxAmountInInvoicePrint: function printItemTaxAmountInInvoicePrint() {
		return settingCache.getValue(Queries.SETTING_PRINT_ITEMTAXAMOUNT_ENABLED, Defaults.DEFAULT_PRINT_ITEMTAXAMOUNT_ENABLED) === '1';
	},

	printItemTaxPercentInInvoicePrint: function printItemTaxPercentInInvoicePrint() {
		return settingCache.getValue(Queries.SETTING_PRINT_ITEMTAXPERCENT_ENABLED, Defaults.DEFAULT_PRINT_ITEMTAXPERCENT_ENABLED) === '1';
	},

	printItemTotalAmountInInvoicePrint: function printItemTotalAmountInInvoicePrint() {
		return settingCache.getValue(Queries.SETTING_PRINT_ITEMTOTALAMOUNT_ENABLED, Defaults.DEFAULT_PRINT_ITEMTOTALAMOUNT_ENABLED) === '1';
	},

	printTaxInclusiveItemPriceInInvoicePrint: function printTaxInclusiveItemPriceInInvoicePrint() {
		return settingCache.getValue(Queries.SETTING_PRINT_FINALITEMPRICE_ENABLED, Defaults.DEFAULT_PRINT_FINALITEMPRICE_ENABLED) === '1';
	},

	getSIColumnRatio: function getSIColumnRatio() {
		return MyDouble.convertStringToDouble(settingCache.getValue(Queries.SETTING_SI_COLUMNRATIO_VALUE, Defaults.DEFAULT_SI_COLUMNRATIO_VALUE), true);
	},

	getItemNameColumnRatio: function getItemNameColumnRatio() {
		return MyDouble.convertStringToDouble(settingCache.getValue(Queries.SETTING_ITEMNAME_COLUMNRATIO_VALUE, Defaults.DEFAULT_ITEMNAME_COLUMNRATIO_VALUE), true);
	},

	getItemCodeColumnRatio: function getItemCodeColumnRatio() {
		return MyDouble.convertStringToDouble(settingCache.getValue(Queries.SETTING_ITEM_CODE_COLUMNRATIO_VALUE, Defaults.DEFAULT_ITEMCODE_COLUMNRATIO_VALUE), true);
	},

	getHsnCodeColumnRatio: function getHsnCodeColumnRatio() {
		return MyDouble.convertStringToDouble(settingCache.getValue(Queries.SETTING_HSNCODE_COLUMNRATIO_VALUE, Defaults.DEFAULT_HSNCODE_COLUMNRATIO_VALUE), true);
	},

	getItemCountColumnRatio: function getItemCountColumnRatio() {
		return MyDouble.convertStringToDouble(settingCache.getValue(Queries.SETTING_ITEMCOUNT_COLUMNRATIO_VALUE, Defaults.DEFAULT_ITEMCOUNT_COLUMNRATIO_VALUE), true);
	},

	getItemBatchColumnRatio: function getItemBatchColumnRatio() {
		return MyDouble.convertStringToDouble(settingCache.getValue(Queries.SETTING_BATCHNO_COLUMNRATIO_VALUE, Defaults.DEFAULT_BATCHNO_COLUMNRATIO_VALUE), true);
	},

	getItemExpDateColumnRatio: function getItemExpDateColumnRatio() {
		return MyDouble.convertStringToDouble(settingCache.getValue(Queries.SETTING_ITEMEXPIRYDATE_COLUMNRATIO_VALUE, Defaults.DEFAULT_ITEMEXPIRYDATE_COLUMNRATIO_VALUE), true);
	},

	getItemManufacturingDateColumnRatio: function getItemManufacturingDateColumnRatio() {
		return MyDouble.convertStringToDouble(settingCache.getValue(Queries.SETTING_ITEMMANUFACTURINGDATE_COLUMNRATIO_VALUE, Defaults.DEFAULT_ITEMMANUFACTURINGDATE_COLUMNRATIO_VALUE), true);
	},

	getItemMRPColumnRatio: function getItemMRPColumnRatio() {
		return MyDouble.convertStringToDouble(settingCache.getValue(Queries.SETTING_ITEMMRP_COLUMNRATIO_VALUE, Defaults.DEFAULT_ITEMMRP_COLUMNRATIO_VALUE), true);
	},

	getItemSizeColumnRatio: function getItemSizeColumnRatio() {
		return MyDouble.convertStringToDouble(settingCache.getValue(Queries.SETTING_ITEMSIZE_COLUMNRATIO_VALUE, Defaults.DEFAULT_ITEMSIZE_COLUMNRATIO_VALUE), true);
	},

	getQuantityColumnRatio: function getQuantityColumnRatio() {
		return MyDouble.convertStringToDouble(settingCache.getValue(Queries.SETTING_QUANTITY_COLUMNRATIO_VALUE, Defaults.DEFAULT_QUANTITY_COLUMNRATIO_VALUE), true);
	},

	getItemUnitColumnRatio: function getItemUnitColumnRatio() {
		return MyDouble.convertStringToDouble(settingCache.getValue(Queries.SETTING_ITEMUNIT_COLUMNRATIO_VALUE, Defaults.DEFAULT_ITEMUNIT_COLUMNRATIO_VALUE), true);
	},

	getPricePerUnitColumnRatio: function getPricePerUnitColumnRatio() {
		return MyDouble.convertStringToDouble(settingCache.getValue(Queries.SETTING_PRICEPERUNIT_COLUMNRATIO_VALUE, Defaults.DEFAULT_PRICEPERUNIT_COLUMNRATIO_VALUE), true);
	},

	getDiscountColumnRatio: function getDiscountColumnRatio() {
		return MyDouble.convertStringToDouble(settingCache.getValue(Queries.SETTING_DISCOUNT_COLUMNRATIO_VALUE, Defaults.DEFAULT_DISCOUNT_COLUMNRATIO_VALUE), true);
	},

	getTaxableItemPriceColumnRatio: function getTaxableItemPriceColumnRatio() {
		return MyDouble.convertStringToDouble(settingCache.getValue(Queries.SETTING_TAXABLE_PRICE_COLUMNRATIO_VALUE, Defaults.DEFAULT_TAXABLE_PRICE_COLUMNRATIO_VALUE), true);
	},

	getTaxTotalAmountColumnRatio: function getTaxTotalAmountColumnRatio() {
		return MyDouble.convertStringToDouble(settingCache.getValue(Queries.SETTING_TAXTOTALAMOUNT_COLUMNRATIO_VALUE, Defaults.DEFAULT_TAXTOTALAMOUNT_COLUMNRATIO_VALUE), true);
	},

	getTaxableAmountColumnRatio: function getTaxableAmountColumnRatio() {
		return MyDouble.convertStringToDouble(settingCache.getValue(Queries.SETTING_TAXABLEAMOUNT_COLUMNRATIO_VALUE, Defaults.DEFAULT_TAXABLEAMOUNT_COLUMNRATIO_VALUE), true);
	},

	getIgstColumnRatio: function getIgstColumnRatio() {
		return TransactionPrintSettings.getTaxTotalAmountColumnRatio();
	},

	getCgstColumnRatio: function getCgstColumnRatio() {
		return TransactionPrintSettings.getTaxTotalAmountColumnRatio();
	},

	getSgstColumnRatio: function getSgstColumnRatio() {
		return TransactionPrintSettings.getTaxTotalAmountColumnRatio();
	},

	getCessColumnRatio: function getCessColumnRatio() {
		return TransactionPrintSettings.getTaxTotalAmountColumnRatio();
	},

	getStateSpecificCESSTaxColumnRatio: function getStateSpecificCESSTaxColumnRatio() {
		return TransactionPrintSettings.getTaxTotalAmountColumnRatio();
	},

	getOtherTaxColumnRatio: function getOtherTaxColumnRatio() {
		return TransactionPrintSettings.getTaxTotalAmountColumnRatio();
	},

	getAdditionCESSColumnRatio: function getAdditionCESSColumnRatio() {
		return MyDouble.convertStringToDouble(settingCache.getValue(Queries.SETTING_ADDITIONALCESS_COLUMNRATIO_VALUE, Defaults.DEFAULT_ADDITIONALCESS_COLUMNRATIO_VALUE), true);
	},

	getFinalItemPriceColumnRatio: function getFinalItemPriceColumnRatio() {
		return MyDouble.convertStringToDouble(settingCache.getValue(Queries.SETTING_FINALITEMPRICE_COLUMNRATIO_VALUE, Defaults.DEFAULT_FINALITEMPRICE_COLUMNRATIO_VALUE), true);
	},

	getTotalAmountColumnRatio: function getTotalAmountColumnRatio() {
		return MyDouble.convertStringToDouble(settingCache.getValue(Queries.SETTING_TOTALAMOUNT_COLUMNRATIO_VALUE, Defaults.DEFAULT_TOTALAMOUNT_COLUMNRATIO_VALUE), true);
	},

	getSIColumnHeader: function getSIColumnHeader() {
		return settingCache.getValue(Queries.SETTING_SI_COLUMNHEADER_VALUE, Defaults.DEFAULT_SI_COLUMNHEADER_VALUE);
	},

	getItemNameColumnHeader: function getItemNameColumnHeader() {
		return settingCache.getValue(Queries.SETTING_ITEMNAME_COLUMNHEADER_VALUE, Defaults.DEFAULT_ITEMNAME_COLUMNHEADER_VALUE);
	},

	getItemCodeColumnHeader: function getItemCodeColumnHeader() {
		return settingCache.getValue(Queries.SETTING_ITEMCODE_COLUMNHEADER_VALUE, Defaults.DEFAULT_ITEMCODE_COLUMNHEADER_VALUE);
	},

	getHsnCodeColumnHeader: function getHsnCodeColumnHeader() {
		return settingCache.getValue(Queries.SETTING_HSNCODE_COLUMNHEADER_VALUE, Defaults.DEFAULT_HSNCODE_COLUMNHEADER_VALUE);
	},

	getQuantityColumnHeader: function getQuantityColumnHeader() {
		return settingCache.getValue(Queries.SETTING_QUNATITY_COLUMNHEADER_VALUE, Defaults.DEFAULT_QUNATITY_COLUMNHEADER_VALUE);
	},

	getItemUnitColumnHeader: function getItemUnitColumnHeader() {
		return settingCache.getValue(Queries.SETTING_ITEMUNIT_COLUMNHEADER_VALUE, Defaults.DEFAULT_ITEMUNIT_COLUMNHEADER_VALUE);
	},

	getPricePerUnitColumnHeader: function getPricePerUnitColumnHeader() {
		return settingCache.getValue(Queries.SETTING_PRICEPERUNIT_COLUMNHEADER_VALUE, Defaults.DEFAULT_PRICEPERUNIT_COLUMNHEADER_VALUE);
	},

	getDiscountColumnHeader: function getDiscountColumnHeader() {
		return settingCache.getValue(Queries.SETTING_DISCOUNT_COLUMNHEADER_VALUE, Defaults.DEFAULT_DISCOUNT_COLUMNHEADER_VALUE);
	},

	getTaxablePriceColumnHeader: function getTaxablePriceColumnHeader() {
		return settingCache.getValue(Queries.SETTING_TAXABLE_PRICE_COLUMNHEADER_VALUE, Defaults.DEFAULT_TAXABLE_PRICE_COLUMNHEADER_VALUE);
	},

	getTaxTotalAmountColumnHeader: function getTaxTotalAmountColumnHeader() {
		return settingCache.getGSTEnabled() ? 'GST' : 'Tax';
	},

	getTaxableAmountColumnHeader: function getTaxableAmountColumnHeader() {
		return settingCache.getValue(Queries.SETTING_TAXABLEAMOUNT_COLUMNHEADER_VALUE, Defaults.DEFAULT_TAXABLEAMOUNT_COLUMNHEADER_VALUE);
	},

	getIgstColumnHeader: function getIgstColumnHeader() {
		return 'IGST';
	},

	getCgstColumnHeader: function getCgstColumnHeader() {
		return 'CGST';
	},

	getSgstColumnHeader: function getSgstColumnHeader(firm) {
		if (firm) {
			if (StateCode.isUnionTerritory(firm.getFirmState())) {
				return 'UTGST';
			}
		}
		return 'SGST';
	},

	getCessColumnHeader: function getCessColumnHeader() {
		return 'CESS';
	},

	getStateSpecificCessColumnHeader: function getStateSpecificCessColumnHeader() {
		return 'Flood CESS';
	},

	getOtherTaxColumnHeader: function getOtherTaxColumnHeader() {
		return 'Other Tax';
	},

	getAdditionCESSColumnHeader: function getAdditionCESSColumnHeader() {
		return settingCache.getValue(Queries.SETTING_ADDITIONALCESS_COLUMNHEADER_VALUE, Defaults.DEFAULT_ADDITIONALCESS_COLUMNHEADER_VALUE);
	},

	getFinalItemPriceColumnHeader: function getFinalItemPriceColumnHeader() {
		return settingCache.getValue(Queries.SETTING_FINALITEMPRICE_COLUMNHEADER_VALUE, Defaults.DEFAULT_FINAL_ITEM_PRICE_COLUMNHEADER_VALUE);
	},

	getTotalAmountColumnHeader: function getTotalAmountColumnHeader() {
		return settingCache.getValue(Queries.SETTING_TOTALAMOUNT_COLUMNHEADER_VALUE, Defaults.DEFAULT_TOTAL_AMOUNT_COLUMNHEADER_VALUE);
	}
};

module.exports = TransactionPrintSettings;