var SaleAgingReportHTMLGenerator = function SaleAgingReportHTMLGenerator() {
	var ReportHTMLStyleSheet = require('./ReportHTMLGenerator');
	var TransactionHTMLGenerator = require('./../ReportHTMLGenerator/TransactionHTMLGenerator.js');
	var transactionHTMLGenerator = new TransactionHTMLGenerator();
	var SettingCache = require('./../Cache/SettingCache.js');
	var settingCache = new SettingCache();

	this.getPieChart = function () {
		var pieChartHTML = document.getElementById('chartParentDiv').innerHTML;
		return pieChartHTML;
	};

	this.getTotalHeader = function (totalBalance, totalunusedPayments, totalReceivable) {
		var balText = totalReceivable >= 0 ? 'Receivable' : 'Payable';
		var html = '<table class=\'reportTableForPrint margintop20\' width=\'100%\'>\n                    <thead><tr style=\'background-color: lightgrey;\'>\n                    <th width=\'\' class=\'tableCellTextAlignRight\'>Total Balance</th>\n                    <th width=\'\' class=\'tableCellTextAlignRight\'>Unused payments</th>\n                    <th width=\'\' class=\'tableCellTextAlignRight\'>Total ' + balText + '</th>\n                   </tr></thead>';

		html += '<tr>\n\t\t\t<td class=\'tableCellTextAlignRight\'>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalBalance) + '</td>\n\t\t\t<td class=\'tableCellTextAlignRight\'>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalunusedPayments) + '</td>\n\t\t\t<td class=\'tableCellTextAlignRight\'>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalReceivable) + '</td>\n\t\t</tr>';

		html += '</table>';
		return html;
	};

	this.getHTMLText = function (partyName, firmId, reportHeader, reportObjectList, totalBalance, totalunusedPayments, totalReceivable, printGraph, printInvoiceDetails) {
		var htmlText = "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>" + ReportHTMLStyleSheet.getReportStyle() + "</head><body><div class='pdfReportHTMLView'>" + transactionHTMLGenerator.getCompanyHeader(firmId, settingCache.getTxnPDFTheme(), settingCache.getTxnPDFThemeColor(), 0.4 + 0.1 * settingCache.getPrintTextSize()) + "<h2 align='center'><u>" + reportHeader + '</u></h2>' + this.getTotalHeader(totalBalance, totalunusedPayments, totalReceivable);
		if (reportObjectList.length > 0 && printGraph) {
			htmlText += this.getPieChart();
		}
		htmlText += getHTMLTable(reportObjectList, printInvoiceDetails);
		htmlText += '</div></body></html>';
		return htmlText;
	};

	var getChildTableData = function getChildTableData(bucketArray, bucketMap) {
		var dynamicRow = '';

		for (var i = 0; i < bucketArray.length; i++) {
			var index = bucketArray[i];
			if (bucketMap[index]) {
				var dataArray = bucketMap[index];
				var bucketText = '';
				switch (index) {
					case 'bucket4':
						bucketText = 'Over 60 Days';
						break;
					case 'bucket3':
						bucketText = '46-60 Days';
						break;
					case 'bucket2':
						bucketText = '31-45 Days';
						break;
					case 'bucket1':
						bucketText = '1-30 Days';
						break;
					case 'current':
						bucketText = 'Current';
						break;
				}

				dynamicRow += '<tr>\n                        <td style="font-weight: bold">' + bucketText + '</td>\n                        <td colspan=\'5\'></td>\n                        </tr>';

				var totalOfTotals = 0;
				var totalBalance = 0;

				for (var j = 0; j < dataArray.length; j++) {
					var data = dataArray[j];
					var invoiceNo = data.invoiceNo;
					var date = data.date;
					var dueDate = data.dueDate;
					var dueDays = data.dueDays;
					var amount = data.amount;
					var balance = data.balance;

					totalBalance += balance;
					totalOfTotals += amount;

					dynamicRow += '<tr>\n                <td>' + invoiceNo + '</td>\n                <td>' + date + '</td>\n                <td>' + dueDate + '</td>\n                <td>' + dueDays + '</td>\n                <td>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(amount) + '</td>\n                <td>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(balance) + '</td>\n            </tr>';
				}
				dynamicRow += '<tr style="border-bottom: none">\n                    <td width=\'\' colspan="3"></td>\n                    <td style=\'background-color: lightgrey; font-weight: bold\' width=\'\' class=\'tableCellTextAlignLeft\'>Totals</td>\n                    <td width=\'\'  style=\'background-color: lightgrey; font-weight: bold\' class=\'tableCellTextAlignLeft\'>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalOfTotals) + '</td>\n                    <td width=\'\' style=\'background-color: lightgrey; font-weight: bold\' class=\'tableCellTextAlignLeft\'>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalBalance) + '</td>\n                   </tr>';
			}
		}
		return dynamicRow;
	};

	var getInvoiceDetails = function getInvoiceDetails(txnArray) {
		var len = txnArray.length;
		var dynamicRow = '';
		var bucketArray = ['bucket4', 'bucket3', 'bucket2', 'bucket1', 'current'];
		var bucketMap = {};

		dynamicRow += '<table class=\'reportTableForPrint\' width=\'100%\'>\n                    <thead><tr style=\'background-color: lightgrey;\'>\n                    <th width=\'\' class=\'tableCellTextAlignLeft\'>Ref No</th>\n                    <th width=\'\' class=\'tableCellTextAlignLeft\'>Date</th>\n                    <th width=\'\' class=\'tableCellTextAlignLeft\'>Due Date</th>\n                    <th width=\'\' class=\'tableCellTextAlignLeft\'>Days Late</th>\n                    <th width=\'\' class=\'tableCellTextAlignLeft\'>Total</th>\n                    <th width=\'\' class=\'tableCellTextAlignLeft\'>Balance</th>\n                   </tr></thead>';

		for (var j = 0; j < txnArray.length; j++) {
			var data = txnArray[j];
			if (!bucketMap[data.bucketName]) {
				bucketMap[data.bucketName] = [];
			}
			bucketMap[data.bucketName].push(data);
		}
		dynamicRow += getChildTableData(bucketArray, bucketMap);

		dynamicRow += '</table>';
		return dynamicRow;
	};

	var getHTMLTable = function getHTMLTable(reportObjectList, printInvoiceDetails) {
		var len = reportObjectList.length;
		var noOfColumns = 7;
		var dynamicRow = '';

		dynamicRow += '<table class=\'reportTableForPrint\' width=\'100%\'>\n                    <thead><tr style=\'background-color: lightgrey;\'>\n                    <th width=\'\' class=\'tableCellTextAlignLeft\'>Party</th>\n                    <th width=\'\' class=\'tableCellTextAlignLeft\'>Current</th>\n                    <th width=\'\' class=\'tableCellTextAlignLeft\'>1-30 Days</th>\n                    <th width=\'\' class=\'tableCellTextAlignLeft\'>31-45 Days</th>\n                    <th width=\'\' class=\'tableCellTextAlignLeft\'>46-60 Days</th>\n                    <th width=\'\' class=\'tableCellTextAlignLeft\'>Over 60 Days</th>\n                    <th width=\'\' class=\'tableCellTextAlignLeft\'>Total</th>\n                   </tr></thead>';

		for (var i = 0; i < len; i++) {
			var data = reportObjectList[i];
			var name = data.name;
			var current = data.current;
			var bucket1 = data.bucket1;
			var bucket2 = data.bucket2;
			var bucket3 = data.bucket3;
			var bucket4 = data.bucket4;
			var total = data.total;
			var txnArray = data.txnArray;

			dynamicRow += '<tr style="border-bottom: none">\n                <td style="font-weight: bold">' + name + '</td>\n                <td>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(current) + '</td>\n                <td>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(bucket1) + '</td>\n                <td>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(bucket2) + '</td>\n                <td>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(bucket3) + '</td>\n                <td>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(bucket4) + '</td>\n                <td>' + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(total) + '</td>\n            </tr>';

			if (printInvoiceDetails) {
				dynamicRow += '<tr>\n' + '  <td colspan="1" ></td>\n' + '  <td colspan="' + (noOfColumns - 1) + '">' + getInvoiceDetails(txnArray) + '</td>\n' + '</tr>\n';
			}
		}

		dynamicRow += '</table>';
		return dynamicRow;
	};
};

module.exports = SaleAgingReportHTMLGenerator;