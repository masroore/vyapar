Object.defineProperty(exports, "__esModule", {
	value: true
});

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _BaseSettings2 = require('./BaseSettings');

var _BaseSettings3 = _interopRequireDefault(_BaseSettings2);

var _styles = require('@material-ui/core/styles');

var _SetPaymentTermModal = require('../SetPaymentTermModal');

var _SetPaymentTermModal2 = _interopRequireDefault(_SetPaymentTermModal);

var _Modal = require('../Modal');

var _Modal2 = _interopRequireDefault(_Modal);

var _SettingCache = require('../../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

var _DynamicList = require('../UIControls/DynamicList');

var _DynamicList2 = _interopRequireDefault(_DynamicList);

var _PrefixModel = require('../../../Models/PrefixModel');

var _PrefixModel2 = _interopRequireDefault(_PrefixModel);

var _SelectDropDown = require('../UIControls/SelectDropDown');

var _SelectDropDown2 = _interopRequireDefault(_SelectDropDown);

var _MenuItem = require('@material-ui/core/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _ErrorCode = require('../../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

var _ItemType = require('../../../Constants/ItemType');

var _ItemType2 = _interopRequireDefault(_ItemType);

var _Queries = require('../../../Constants/Queries');

var _Queries2 = _interopRequireDefault(_Queries);

var _PassCodeUtility = require('../../../UIControllers/PassCodeUtility.js');

var _PassCodeUtility2 = _interopRequireDefault(_PassCodeUtility);

var _TxnTypeConstant = require('../../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _SettingsMapping = require('./SettingsMapping');

var _SettingsMapping2 = _interopRequireDefault(_SettingsMapping);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var transactionStyles = function transactionStyles(theme) {
	return {
		paymentTerm: {
			marginLeft: '27px',
			lineHeight: '0',
			display: 'block',
			color: '#1789FC'
		},
		prefixBorder: {
			border: '1px rgba(0, 0, 0, 0.23) solid',
			borderRadius: '4px',
			padding: '12px',
			'& > legend': {
				fontSize: '0.8rem',
				padding: '0.1em 0.3em',
				color: 'rgba(0, 0, 0, 0.54)'
			}
		},
		'@global': {
			'.additional-charges .additional-charge-field input': {
				marginRight: '10px'
			},
			'#customFieldDialog .reportTextInputFilter': {
				marginRight: '5px'
			},
			'.prefix-list-container .col-6:nth-child(odd)': {
				paddingLeft: '12px',
				paddingRight: '5px'
			},
			'.prefix-list-container .col-6:nth-child(even)': {
				paddingLeft: '5px',
				paddingRight: '12px'
			}
		}
	};
};

var TransactionSettings = function (_BaseSettings) {
	(0, _inherits3.default)(TransactionSettings, _BaseSettings);

	function TransactionSettings(props) {
		(0, _classCallCheck3.default)(this, TransactionSettings);

		var _this = (0, _possibleConstructorReturn3.default)(this, (TransactionSettings.__proto__ || (0, _getPrototypeOf2.default)(TransactionSettings)).call(this, props));

		_this.checkAllowDelete = function (option) {
			var prefixId = option.key;
			var res = _this.dataLoader.isPrefixUsedInTxn(prefixId);
			if (res) {
				return false;
			}
			return true;
		};

		_this.removePrefix = function (prefixObj) {
			var prefixModel = new _PrefixModel2.default();
			var prefixId = prefixObj.key;
			var prefixValue = prefixObj.value;
			prefixModel.setPrefixId(prefixId);
			prefixModel.setPrefixValue(prefixValue);
			var statusCode = prefixModel.removeSelectedPrefix();
			if (statusCode !== _ErrorCode2.default.ERROR_PREFIX_DELETE_SUCCESS) {
				ToastHelper.error(statusCode);
			} else {
				var updatedPrefixList = _this.getPrefixDetailsForFirm(_this.defaultFirm.getFirmId());
				_this.setState({
					prefixDetailsObj: updatedPrefixList
				});
				ToastHelper.success(statusCode);
			}
		};

		_this.saveAdditionalCharges = function () {
			var _additionalChargeEnab;

			var additionalCharges = document.getElementsByClassName('additional-charge-value');
			var isAnyAdditionalChargeEnabled = _this.state.acEnabledCheckbox1 || _this.state.acEnabledCheckbox2 || _this.state.acEnabledCheckbox3;
			var additionalChargeEnableList = (_additionalChargeEnab = {}, (0, _defineProperty3.default)(_additionalChargeEnab, _Queries2.default.SETTING_AC1_ENABLED, _this.checkboxValueForSave(_this.state.acEnabledCheckbox1)), (0, _defineProperty3.default)(_additionalChargeEnab, _Queries2.default.SETTING_AC2_ENABLED, _this.checkboxValueForSave(_this.state.acEnabledCheckbox2)), (0, _defineProperty3.default)(_additionalChargeEnab, _Queries2.default.SETTING_AC3_ENABLED, _this.checkboxValueForSave(_this.state.acEnabledCheckbox3)), (0, _defineProperty3.default)(_additionalChargeEnab, _Queries2.default.SETTING_AC_ENABLED, _this.checkboxValueForSave(isAnyAdditionalChargeEnabled)), _additionalChargeEnab);
			_this.saveMultipleSettings(additionalChargeEnableList, true);
			for (var i = 0; i < additionalCharges.length; i++) {
				var additionalChargeItem = additionalCharges[i];var data = additionalChargeItem.dataset;
				if (_this.additionalChargeValues['additionalCharge' + data.acId] !== additionalChargeItem.value) {
					MyAnalytics.pushEvent('Settings Additional Charges Save', (0, _defineProperty3.default)({}, data.acKey, additionalChargeItem.value));
					_this.dataInserter.setCurrentExtraChargesValue(data.acId, additionalChargeItem.value);
				}
			}
		};

		_this.togglePaymentTermModal = function () {
			_this.setState({
				canShowPaymentTermModal: !_this.state.canShowPaymentTermModal
			});
		};

		_this.closeAdditionalChargePopup = function () {
			_this.setState({
				canOpenAdditionalCharge: false,
				acEnabledCheckbox1: _this.settingCache.isAC1Enabled(),
				acEnabledCheckbox2: _this.settingCache.isAC2Enabled(),
				acEnabledCheckbox3: _this.settingCache.isAC3Enabled()
			});
		};

		_this.toggleAdditionalChargePopup = function (callback) {
			_this.setState({
				canOpenAdditionalCharge: !_this.state.canOpenAdditionalCharge
			}, function () {
				if (callback && typeof callback === 'function') {
					callback();
				}
			});
		};

		_this.deletePasscodeCallback = function () {
			_this.setState({
				passCodeForTransactionDelete: _this.settingCache.getDeletePasscodeEnabled()
			});
		};

		_this.handleItemTableSettingsCallback = function (key) {
			return function () {
				if (key == _Queries2.default.SETTING_ENABLE_INCLUSIVE_EXCLUSIVE_TAX_ON_TRANSACTION) {
					var _inclusiveSettingForS;

					var inclusiveSettingForSalePurchase = (_inclusiveSettingForS = {}, (0, _defineProperty3.default)(_inclusiveSettingForS, _Queries2.default.SETTING_INCLUSIVE_TAX_ON_INWARD_TXN, _ItemType2.default.ITEM_TXN_TAX_EXCLUSIVE), (0, _defineProperty3.default)(_inclusiveSettingForS, _Queries2.default.SETTING_INCLUSIVE_TAX_ON_OUTWARD_TXN, _ItemType2.default.ITEM_TXN_TAX_EXCLUSIVE), _inclusiveSettingForS);
					_this.saveMultipleSettings(inclusiveSettingForSalePurchase, false);
				}
			};
		};

		_this.transactionHeaderSettingsHandleCallback = function (key) {
			return function () {
				if (key === _Queries2.default.SETTING_ENABLE_DEFAULT_CASH_SALE) {
					_this.toggleCheckboxAndSave(_Queries2.default.SETTING_DIRECT_BARCODE_SCANNING_MODE_ENABLED, false, true);
				}
				_this.saveSettings(key, _this.checkboxValueForSave(_this.state[key]));
			};
		};

		_this.handleTaxDiscountSettingsCallback = function (key) {
			return function () {
				if (key == _Queries2.default.SETTING_TAX_ENABLED) {
					if (_this.settingCache.getGSTEnabled() && _this.settingCache.isItemwiseTaxEnabled() && _this.settingCache.getTaxEnabled()) {
						var conf = confirm('According to Government there should not be tax on tax. You have enabled item wise tax, so you should not enable transaction level tax. Do you wish to continue?');
						if (!conf) {
							_this.toggleCheckboxAndSave(key, false, true);
						}
					}
				}
			};
		};

		_this.settingCache = new _SettingCache2.default();
		_this.generateFirmList();
		_this.dummyNone = { key: '0', value: 'None' };
		_this.state = (0, _extends3.default)({}, _this.transactionSettingState());

		_this.transactionHeaderSettings = _SettingsMapping2.default.transactionHeaderSettings;
		_this.transactionItemTableSettings = _SettingsMapping2.default.transactionItemTableSettings;
		_this.transactionTaxDiscountTotalSettings = _SettingsMapping2.default.transactionTaxDiscountTotalSettings;
		_this.moreTransactionSettings = _SettingsMapping2.default.moreTransactionSettings;

		var canShowTaxInvoiceKey = _this.isChecked(_this.listOfSettings[_Queries2.default.SETTING_TAXINVOICE_ENABLED]);
		if (!canShowTaxInvoiceKey) {
			delete _this.transactionHeaderSettings[_Queries2.default.SETTING_TAXINVOICE_ENABLED];
		}
		_this.roundOffOptions = [{ key: 1, value: 'Nearest' }, { key: 2, value: 'Down To' }, { key: 3, value: 'Up To' }];

		_this.roundOffValues = [{ key: 1, value: 1 }, { key: 10, value: 10 }, { key: 50, value: 50 }, { key: 100, value: 100 }, { key: 1000, value: 1000 }];

		_this.createPrefixReference();
		return _this;
	}

	(0, _createClass3.default)(TransactionSettings, [{
		key: 'transactionSettingState',
		value: function transactionSettingState() {
			var _ref;

			return _ref = {}, (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_TXNREFNO_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_TXNREFNO_ENABLED])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ENABLE_DEFAULT_CASH_SALE, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ENABLE_DEFAULT_CASH_SALE])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ENABLE_DISPLAY_NAME, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ENABLE_DISPLAY_NAME])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PO_DETAILS_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PO_DETAILS_ENABLED])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ENABLE_INCLUSIVE_EXCLUSIVE_TAX_ON_TRANSACTION, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ENABLE_INCLUSIVE_EXCLUSIVE_TAX_ON_TRANSACTION])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_SHOW_PURCHASE_PRICE_IN_ITEM_DROP_DOWN, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_SHOW_PURCHASE_PRICE_IN_ITEM_DROP_DOWN])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_SHOW_LAST_FIVE_SALE_PRICE, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_SHOW_LAST_FIVE_SALE_PRICE])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_FREE_QTY_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_FREE_QTY_ENABLED])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ENABLE_ITEM_COUNT, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ENABLE_ITEM_COUNT])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_TAX_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_TAX_ENABLED])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_DISCOUNT_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_DISCOUNT_ENABLED])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PAYMENT_TERM_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PAYMENT_TERM_ENABLED])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_DISCOUNT_IN_MONEY_TXN, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_DISCOUNT_IN_MONEY_TXN])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_BILL_TO_BILL_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_BILL_TO_BILL_ENABLED])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_IS_ROUND_OFF_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_IS_ROUND_OFF_ENABLED])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ITEM_COUNT_VALUE, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ITEM_COUNT_VALUE])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_DISABLE_INVOICE_PREVIEW, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_DISABLE_INVOICE_PREVIEW])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ESTIMATE_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ESTIMATE_ENABLED])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ORDER_FORM_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ORDER_FORM_ENABLED])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_DELIVERY_CHALLAN_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_DELIVERY_CHALLAN_ENABLED])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_TAXINVOICE_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_TAXINVOICE_ENABLED])), (0, _defineProperty3.default)(_ref, 'acEnabledCheckbox1', this.settingCache.isAC1Enabled()), (0, _defineProperty3.default)(_ref, 'acEnabledCheckbox2', this.settingCache.isAC2Enabled()), (0, _defineProperty3.default)(_ref, 'acEnabledCheckbox3', this.settingCache.isAC3Enabled()), (0, _defineProperty3.default)(_ref, 'canShowPaymentTermModal', false), (0, _defineProperty3.default)(_ref, 'canOpenAdditionalCharge', false), (0, _defineProperty3.default)(_ref, 'prefixDetailsObj', this.getPrefixDetailsForFirm(this.defaultFirm.getFirmId())), (0, _defineProperty3.default)(_ref, 'passCodeForTransactionDelete', this.settingCache.getDeletePasscodeEnabled()), (0, _defineProperty3.default)(_ref, 'openChangeTextModal', false), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ITEM_COUNT_VALUE, this.listOfSettings[_Queries2.default.SETTING_ITEM_COUNT_VALUE]), _ref;
		}
	}, {
		key: 'resetState',
		value: function resetState() {
			this.setState((0, _extends3.default)({}, this.transactionSettingState()));
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.getAdditionalCharges();
			this.higlightSearchedSetting();
			this.initializeToolTips();
		}
	}, {
		key: 'addNewPrefix',
		value: function addNewPrefix(txnType, newPrefixValueObj) {
			var newPrefixVal = newPrefixValueObj.value;
			var isPrefixExist = this.dataLoader.isPrefixExist(newPrefixVal, txnType, this.selectedFirmForPrefix.getFirmId());
			if (isPrefixExist) {
				ToastHelper.error('Prefix already exists');
				return;
			}
			var statusCode = _ErrorCode2.default.ERROR_SETTING_SAVE_FAILED;
			var isTransactionBeginSuccess = this.transactionManager.BeginTransaction();
			if (!isTransactionBeginSuccess) {
				ToastHelper.error('Unable to store new prefix value');
				return;
			}
			var prefixModel = new _PrefixModel2.default();
			prefixModel.setPrefixValue(newPrefixValueObj.value);
			prefixModel.setFirmId(this.selectedFirmForPrefix.getFirmId());
			prefixModel.setTxnType(txnType);
			prefixModel.setIsDefault(1);
			statusCode = prefixModel.addNewPrefix();
			if (statusCode == _ErrorCode2.default.ERROR_PREFIX_CREATED_SUCCESS) {
				if (!this.transactionManager.CommitTransaction()) {
					statusCode = _ErrorCode2.default.ERROR_SETTING_SAVE_FAILED;
				} else {
					newPrefixValueObj.key = prefixModel.getPrefixId();
				}
			}

			this.transactionManager.EndTransaction();

			if (statusCode !== _ErrorCode2.default.ERROR_PREFIX_CREATED_SUCCESS) {
				ToastHelper.error(statusCode);
			}
		}
	}, {
		key: 'createPrefixReference',
		value: function createPrefixReference() {
			this.prefixSettingList = [{ key: _TxnTypeConstant2.default.TXN_TYPE_SALE, label: 'Sale', reverseMenu: false }, { key: _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN, label: 'Credit Note', reverseMenu: false }];
			if (this.state[_Queries2.default.SETTING_ORDER_FORM_ENABLED]) {
				var saleOrderPurchaseOrderList = [{ key: _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER, label: 'Sale Order', reverseMenu: false }, { key: _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER, label: 'Purchase Order', reverseMenu: false }];
				this.prefixSettingList = [].concat((0, _toConsumableArray3.default)(this.prefixSettingList), saleOrderPurchaseOrderList);
			}

			if (this.state[_Queries2.default.SETTING_ESTIMATE_ENABLED]) {
				this.prefixSettingList.push({ key: _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE, label: 'Estimate', reverseMenu: true });
			}

			if (this.state[_Queries2.default.SETTING_DELIVERY_CHALLAN_ENABLED]) {
				this.prefixSettingList.push({ key: _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN, label: 'Delivery Challan', reverseMenu: true });
			}

			this.prefixSettingList.push({ key: _TxnTypeConstant2.default.TXN_TYPE_CASHIN, label: 'Payment In', reverseMenu: true });

			for (var i = 0; i < this.prefixSettingList.length; i++) {
				this['prefixRef' + i] = _react2.default.createRef();
			}
		}
	}, {
		key: 'updatePrefix',
		value: function updatePrefix(txnType, prefixId) {
			var prefixModel = new _PrefixModel2.default();
			prefixModel.setFirmId(this.selectedFirmForPrefix.getFirmId());
			prefixModel.setTxnType(txnType);
			prefixModel.setPrefixId(prefixId);

			var isTransactionBeginSuccess = this.transactionManager.BeginTransaction();
			if (!isTransactionBeginSuccess) {
				ToastHelper.error(_ErrorCode2.default.ERROR_PREFIX_UPDATE_FAILED);
				return;
			}
			var statusCode = prefixModel.updatePrefixValue();

			if (statusCode == _ErrorCode2.default.ERROR_PREFIX_UPDATE_SUCCESS) {
				if (!this.transactionManager.CommitTransaction()) {
					statusCode = _ErrorCode2.default.ERROR_PREFIX_UPDATE_FAILED;
				}
			}

			this.transactionManager.EndTransaction();

			if (statusCode !== _ErrorCode2.default.ERROR_PREFIX_UPDATE_SUCCESS) {
				ToastHelper.error(statusCode);
			}
		}
	}, {
		key: 'generateFirmList',
		value: function generateFirmList() {
			var _this2 = this;

			this.firmList = [];
			(0, _keys2.default)(this.listOfFirmsFromCache).map(function (key) {
				var firmItem = _this2.listOfFirmsFromCache[key];
				_this2.firmList.push({ key: firmItem.getFirmId(), value: firmItem.getFirmName() });
			});
		}
	}, {
		key: 'getPrefixDetailsForFirm',
		value: function getPrefixDetailsForFirm(firmId) {
			this.selectedFirmForPrefix = this.firmCache.getFirmById(firmId);
			var prefixDetails = this.dataLoader.getPrefixDetailsOnFirmId(firmId);
			var prefixDetailsObj = {};
			for (var i in prefixDetails) {
				if (prefixDetails.hasOwnProperty(i)) {
					var curObj = prefixDetailsObj['prefixList' + prefixDetails[i].getTxnType()];
					if (!curObj) {
						prefixDetailsObj['prefixList' + prefixDetails[i].getTxnType()] = [];
						curObj = prefixDetailsObj['prefixList' + prefixDetails[i].getTxnType()];
					}
					if (Number(prefixDetails[i].getIsDefault())) {
						prefixDetailsObj['prefixDefaultKey' + prefixDetails[i].getTxnType()] = prefixDetails[i].getPrefixId();
						prefixDetailsObj['prefixDefaultVal' + prefixDetails[i].getTxnType()] = prefixDetails[i].getPrefixValue();
					}
					curObj.push({
						key: prefixDetails[i].getPrefixId(),
						value: prefixDetails[i].getPrefixValue()
					});
				}
			}
			return prefixDetailsObj;
		}

		// Check for prefix used or not


		// remove selected prefix from db

	}, {
		key: 'resetFirmPrefixes',
		value: function resetFirmPrefixes(firmId) {
			var prefixDetails = this.getPrefixDetailsForFirm(firmId);
			for (var i = 0; i < this.prefixSettingList.length; i++) {
				var prefixSetting = this.prefixSettingList[i];
				var prefixRef = this['prefixRef' + i];
				prefixRef.current.updateComponent([this.dummyNone].concat((0, _toConsumableArray3.default)(prefixDetails['prefixList' + prefixSetting.key] || [])), prefixDetails['prefixDefaultKey' + prefixSetting.key], prefixDetails['prefixDefaultVal' + prefixSetting.key]);
			}
		}
	}, {
		key: 'getAdditionalCharges',
		value: function getAdditionalCharges() {
			this.additionalChargeValues = {
				additionalCharge1: this.dataLoader.getCurrentExtraChargesValue('1'),
				additionalCharge2: this.dataLoader.getCurrentExtraChargesValue('2'),
				additionalCharge3: this.dataLoader.getCurrentExtraChargesValue('3')
			};
			this.setState((0, _extends3.default)({
				acEnabledCheckbox1: this.settingCache.isAC1Enabled(),
				acEnabledCheckbox2: this.settingCache.isAC2Enabled(),
				acEnabledCheckbox3: this.settingCache.isAC3Enabled()
			}, this.additionalChargeValues));
		}
	}, {
		key: 'openAdditionalFields',
		value: function openAdditionalFields() {
			var UserDefinedFieldsController = require('../../../UIControllers/UserDefinedFieldsController');
			UserDefinedFieldsController({
				mountPoint: $('#extraFieldsSetupDialog')
			});
		}
	}, {
		key: 'openTransportationDetails',
		value: function openTransportationDetails() {
			var SettingsUtility = require('../../../UIControllers/SettingsUtility.js');
			SettingsUtility.setCustomFields();
		}
	}, {
		key: 'moreTransactionSettingsCallback',
		value: function moreTransactionSettingsCallback(key, value) {
			if (key === _Queries2.default.SETTING_BILL_TO_BILL_ENABLED || key === _Queries2.default.SETTING_PAYMENT_TERM_ENABLED && value) {
				var keyToSave = key === _Queries2.default.SETTING_BILL_TO_BILL_ENABLED ? _Queries2.default.SETTING_PAYMENT_TERM_ENABLED : _Queries2.default.SETTING_BILL_TO_BILL_ENABLED;
				this.toggleCheckboxAndSave(keyToSave, value);
			}

			if (key === 'passCodeForTransactionDelete') {
				_PassCodeUtility2.default.deletePasscode(value, this.deletePasscodeCallback);
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var _this3 = this;

			var dummyNone = this.dummyNone;
			var classes = this.props.classes;

			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement(_SetPaymentTermModal2.default, { key: 1, onClose: this.togglePaymentTermModal, isOpen: this.state.canShowPaymentTermModal }),
				this.renderChangeTextModal(_Queries2.default.SETTING_ITEM_COUNT_VALUE),
				_react2.default.createElement(
					'div',
					{ className: 'transaction-settings' },
					_react2.default.createElement(
						'div',
						{ className: 'row mb-20' },
						_react2.default.createElement(
							'div',
							{ className: 'col' },
							_react2.default.createElement(
								'span',
								{ className: 'heading font-family-bold font-16' },
								'Transaction Header'
							),
							_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
							(0, _keys2.default)(this.transactionHeaderSettings).map(function (key) {
								return _react2.default.createElement(
									_react2.default.Fragment,
									{ key: key },
									_react2.default.createElement(
										'div',
										{ className: 'setting-item d-flex align-items-center ' + key },
										_react2.default.createElement(
											'label',
											{ className: 'input-checkbox' },
											_react2.default.createElement('input', { type: 'checkbox', onChange: _this3.toggleCheckbox(key, false, _this3.transactionHeaderSettingsHandleCallback(key)), checked: _this3.state[key] }),
											_this3.checkboxJSX
										),
										_react2.default.createElement(
											'span',
											{ className: '' },
											_this3.transactionHeaderSettings[key].heading
										),
										_this3.renderToolTip(_this3.transactionHeaderSettings[key], key)
									)
								);
							})
						),
						_react2.default.createElement(
							'div',
							{ className: 'col' },
							_react2.default.createElement(
								'span',
								{ className: 'heading font-family-bold font-16' },
								'Items Table'
							),
							_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
							(0, _keys2.default)(this.transactionItemTableSettings).map(function (key) {
								return _react2.default.createElement(
									'div',
									{ key: key, className: 'setting-item d-flex align-items-center ' + key },
									_react2.default.createElement(
										'label',
										{ className: 'input-checkbox' },
										_react2.default.createElement('input', { type: 'checkbox', onChange: _this3.toggleCheckbox(key, true, _this3.handleItemTableSettingsCallback(key)), checked: _this3.state[key] }),
										_this3.checkboxJSX
									),
									key === _Queries2.default.SETTING_ENABLE_ITEM_COUNT && _react2.default.createElement(
										_react2.default.Fragment,
										null,
										_react2.default.createElement(
											'span',
											{ className: '' },
											_this3.state[_Queries2.default.SETTING_ITEM_COUNT_VALUE]
										),
										_react2.default.createElement(
											'a',
											{ onClick: _this3.toggleChangeTextModalPopup(_Queries2.default.SETTING_ITEM_COUNT_VALUE), className: (_this3.state[_Queries2.default.SETTING_ENABLE_ITEM_COUNT] ? '' : 'disable-input') + ' font-family-medium link' },
											'Change Text'
										)
									),
									key !== _Queries2.default.SETTING_ENABLE_ITEM_COUNT && _react2.default.createElement(
										'span',
										{ className: '' },
										_this3.transactionItemTableSettings[key].heading
									),
									_this3.renderToolTip(_this3.transactionItemTableSettings[key], key)
								);
							})
						),
						_react2.default.createElement(
							'div',
							{ className: 'col' },
							_react2.default.createElement(
								'span',
								{ className: 'heading font-family-bold font-16' },
								'Taxes, Discount & Totals'
							),
							_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
							(0, _keys2.default)(this.transactionTaxDiscountTotalSettings).map(function (key) {
								return _react2.default.createElement(
									'div',
									{ key: key, className: key },
									_react2.default.createElement(
										'div',
										{ className: 'setting-item d-flex align-items-center' },
										_react2.default.createElement(
											'label',
											{ className: 'input-checkbox' },
											_react2.default.createElement('input', { type: 'checkbox', onChange: _this3.toggleCheckbox(key, true, _this3.handleTaxDiscountSettingsCallback(key)), checked: _this3.state[key] }),
											_this3.checkboxJSX
										),
										_react2.default.createElement(
											'span',
											{ className: '' },
											_this3.transactionTaxDiscountTotalSettings[key].heading
										),
										_this3.renderToolTip(_this3.transactionTaxDiscountTotalSettings[key], key)
									),
									key === _Queries2.default.SETTING_IS_ROUND_OFF_ENABLED && _react2.default.createElement(
										'div',
										{ className: 'setting-item height-66' },
										_this3.state[key] && _react2.default.createElement(
											_react2.default.Fragment,
											null,
											_react2.default.createElement(
												'div',
												{ className: 'd-inline-block', style: { width: '120px' } },
												_react2.default.createElement(
													_SelectDropDown2.default,
													{ handleChange: _this3.saveSettings.bind(_this3, _Queries2.default.SETTING_ROUND_OFF_TYPE), value: _this3.listOfSettings[_Queries2.default.SETTING_ROUND_OFF_TYPE] },
													_this3.roundOffOptions.map(function (option, index) {
														return _react2.default.createElement(
															_MenuItem2.default,
															{ key: option.key, value: option.key },
															option.value
														);
													})
												)
											),
											_react2.default.createElement(
												'span',
												{ style: { margin: '15px' } },
												'To'
											),
											_react2.default.createElement(
												'div',
												{ className: 'd-inline-block w-100-px' },
												_react2.default.createElement(
													_SelectDropDown2.default,
													{ handleChange: _this3.saveSettings.bind(_this3, _Queries2.default.SETTING_ROUND_OFF_UPTO), value: _this3.listOfSettings[_Queries2.default.SETTING_ROUND_OFF_UPTO] },
													_this3.roundOffValues.map(function (option, index) {
														return _react2.default.createElement(
															_MenuItem2.default,
															{ key: option.key, value: option.key },
															option.value
														);
													})
												)
											)
										)
									)
								);
							})
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'row' },
						_react2.default.createElement(
							'div',
							{ className: 'col' },
							_react2.default.createElement(
								'span',
								{ className: 'heading font-family-bold font-16' },
								'More Transaction Features'
							),
							_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
							(0, _keys2.default)(this.moreTransactionSettings).map(function (key) {
								return _react2.default.createElement(
									'div',
									{ key: key, className: key },
									_react2.default.createElement(
										'div',
										{ className: 'setting-item d-flex align-items-center' },
										_react2.default.createElement(
											'label',
											{ className: 'input-checkbox' },
											_react2.default.createElement('input', { type: 'checkbox', onChange: _this3.toggleCheckbox(key, true, _this3.moreTransactionSettingsCallback.bind(_this3, key)), checked: _this3.state[key] }),
											_this3.checkboxJSX
										),
										_react2.default.createElement(
											'span',
											{ className: '' },
											_this3.moreTransactionSettings[key].heading
										),
										_this3.renderToolTip(_this3.moreTransactionSettings[key], key)
									),
									key === _Queries2.default.SETTING_PAYMENT_TERM_ENABLED && _this3.state[key] && _react2.default.createElement(
										'a',
										{ onClick: _this3.togglePaymentTermModal, className: classes.paymentTerm + ' font-family-medium font-12 mb-20' },
										'Set Payment Terms'
									)
								);
							}),
							_react2.default.createElement(
								'div',
								{ className: 'setting-item' },
								_react2.default.createElement('button', { onClick: this.openAdditionalFields, dangerouslySetInnerHTML: { __html: _SettingsMapping2.default.miscTransactionSettings.ADDITIONAL_FIELDS.heading }, className: 'settings-btn-link' })
							),
							_react2.default.createElement(
								'div',
								{ className: 'setting-item' },
								_react2.default.createElement('button', { onClick: this.openTransportationDetails, dangerouslySetInnerHTML: { __html: _SettingsMapping2.default.miscTransactionSettings.TRANSPORTATION_DETAILS.heading }, className: 'settings-btn-link' })
							),
							_react2.default.createElement(
								'div',
								{ className: 'setting-item' },
								_react2.default.createElement('button', { onClick: this.toggleAdditionalChargePopup, dangerouslySetInnerHTML: { __html: _SettingsMapping2.default.miscTransactionSettings.ADDITIONAL_CHARGES.heading }, className: 'settings-btn-link' })
							)
						),
						_react2.default.createElement(
							'div',
							{ className: 'col' },
							_react2.default.createElement(
								'span',
								{ className: 'heading font-family-bold font-16' },
								'Transaction Prefixes'
							),
							_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
							_react2.default.createElement(_DynamicList2.default, { label: 'Firm',
								value: this.defaultFirm.getFirmName(),
								selectedOption: this.defaultFirm.getFirmId(),
								optionSelectCallback: this.resetFirmPrefixes.bind(this),
								reverseMenu: false, allowAdd: false, allowDelete: false, optionList: this.firmList }),
							_react2.default.createElement(
								'div',
								{ className: 'mt-20' },
								_react2.default.createElement(
									'fieldset',
									{ className: classes.prefixBorder },
									_react2.default.createElement(
										'legend',
										null,
										'Prefixes'
									),
									_react2.default.createElement(
										'div',
										{ className: 'row prefix-list-container' },
										this.prefixSettingList.map(function (prefixSetting, index) {
											return _react2.default.createElement(
												'div',
												{ className: 'col-6', key: prefixSetting.key },
												_react2.default.createElement(_DynamicList2.default, { allowAdd: true, maxLength: 15, label: prefixSetting.label, allowDelete: true,
													ref: _this3['prefixRef' + index],
													modalConfirmCallback: _this3.removePrefix,
													optionSelectCallback: _this3.updatePrefix.bind(_this3, prefixSetting.key),
													addNewValueCallback: _this3.addNewPrefix.bind(_this3, prefixSetting.key),
													checkCanDelete: _this3.checkAllowDelete,
													selectedOption: _this3.state.prefixDetailsObj['prefixDefaultKey' + prefixSetting.key] || dummyNone.key,
													value: _this3.state.prefixDetailsObj['prefixDefaultVal' + prefixSetting.key] || dummyNone.value,
													optionList: [dummyNone].concat((0, _toConsumableArray3.default)(_this3.state.prefixDetailsObj['prefixList' + prefixSetting.key] || [])), reverseMenu: prefixSetting.reverseMenu,
													errorCode: 'This prefix is currently used in transactions & cannot be deleted.'
												})
											);
										})
									)
								)
							)
						),
						_react2.default.createElement('div', { className: 'col' })
					)
				),
				_react2.default.createElement(
					_Modal2.default,
					{ width: '250px', parentClassSelector: 'additional-charges',
						onClose: this.closeAdditionalChargePopup,
						isOpen: this.state.canOpenAdditionalCharge,
						title: 'Additional Charges' },
					_react2.default.createElement(
						'div',
						{ className: 'customizePopup', style: { padding: '0' } },
						_react2.default.createElement(
							'div',
							{ style: { padding: '16px' } },
							_react2.default.createElement(
								'div',
								null,
								_react2.default.createElement(
									'label',
									null,
									'Additional Charge 1'
								),
								_react2.default.createElement(
									'div',
									{ className: 'additional-charge-field d-flex align-items-center' },
									_react2.default.createElement('input', { type: 'text', 'data-ac-id': '1',
										'data-ac-key': _Queries2.default.SETTING_AC1_ENABLED,
										'data-ac-label': 'Additional Charge 1',
										className: 'reportTextInputFilter width90 additional-charge-value ' + (this.state.acEnabledCheckbox1 ? '' : 'disable-input'),
										value: this.state.additionalCharge1,
										onChange: this.updateInputChange('additionalCharge1')
									}),
									_react2.default.createElement(
										'label',
										{ className: 'input-checkbox' },
										_react2.default.createElement('input', { type: 'checkbox',
											onChange: this.toggleCheckbox('acEnabledCheckbox1', false),
											checked: this.state.acEnabledCheckbox1 }),
										this.checkboxJSX
									)
								)
							),
							_react2.default.createElement(
								'div',
								{ className: '' },
								_react2.default.createElement(
									'label',
									null,
									'Additional Charge 2'
								),
								_react2.default.createElement(
									'div',
									{ className: 'additional-charge-field d-flex align-items-center' },
									_react2.default.createElement('input', { type: 'text', 'data-ac-id': '2',
										'data-ac-label': 'Additional Charge 2',
										'data-ac-key': _Queries2.default.SETTING_AC2_ENABLED,
										className: 'reportTextInputFilter width90 additional-charge-value ' + (this.state.acEnabledCheckbox2 ? '' : 'disable-input'),
										value: this.state.additionalCharge2,
										onChange: this.updateInputChange('additionalCharge2') }),
									_react2.default.createElement(
										'label',
										{ className: 'input-checkbox' },
										_react2.default.createElement('input', { type: 'checkbox',
											onChange: this.toggleCheckbox('acEnabledCheckbox2', false),
											checked: this.state.acEnabledCheckbox2
										}),
										this.checkboxJSX
									)
								)
							),
							_react2.default.createElement(
								'div',
								{ className: '' },
								_react2.default.createElement(
									'label',
									null,
									'Additional Charge 3'
								),
								_react2.default.createElement(
									'div',
									{ className: 'additional-charge-field d-flex align-items-center' },
									_react2.default.createElement('input', { type: 'text', 'data-ac-id': '3',
										'data-ac-label': 'Additional Charge 3',
										'data-ac-key': _Queries2.default.SETTING_AC3_ENABLED,
										className: 'reportTextInputFilter width90 additional-charge-value ' + (this.state.acEnabledCheckbox3 ? '' : 'disable-input'),
										value: this.state.additionalCharge3, onChange: this.updateInputChange('additionalCharge3') }),
									_react2.default.createElement(
										'label',
										{ className: 'input-checkbox' },
										_react2.default.createElement('input', { type: 'checkbox',
											onChange: this.toggleCheckbox('acEnabledCheckbox3', false),
											checked: this.state.acEnabledCheckbox3 }),
										this.checkboxJSX
									)
								)
							)
						),
						_react2.default.createElement(
							'div',
							{ className: 'dialogButton floatRight' },
							_react2.default.createElement(
								'button',
								{ id: 'doneForCustomFields', onClick: this.toggleAdditionalChargePopup.bind(this, this.saveAdditionalCharges), className: 'terminalButton' },
								'Done'
							)
						)
					)
				)
			);
		}
	}]);
	return TransactionSettings;
}(_BaseSettings3.default);

exports.default = (0, _styles.withStyles)(transactionStyles)(TransactionSettings);