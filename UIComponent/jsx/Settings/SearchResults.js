Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _BaseSettings2 = require('./BaseSettings');

var _BaseSettings3 = _interopRequireDefault(_BaseSettings2);

var _SettingsMapping = require('./SettingsMapping');

var _styles = require('@material-ui/core/styles');

var _Paper = require('@material-ui/core/Paper');

var _Paper2 = _interopRequireDefault(_Paper);

var _InputBase = require('@material-ui/core/InputBase');

var _InputBase2 = _interopRequireDefault(_InputBase);

var _IconButton = require('@material-ui/core/IconButton');

var _IconButton2 = _interopRequireDefault(_IconButton);

var _throttleDebounce = require('throttle-debounce');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
	return {
		root: {
			padding: '2px 4px',
			display: 'flex',
			alignItems: 'center',
			borderRadius: '20px',
			backgroundColor: '#E5EBF1'
		},
		input: {
			marginLeft: 8,
			flex: 1
		},
		iconButton: {
			padding: '5px 10px'
		},
		searchDisplayCard: {
			width: '345px',
			height: '90px',
			margin: '10px',
			padding: '10px',
			backgroundColor: '#efecec',
			display: 'inline-flex',
			position: 'relative',
			'& p': {
				marginBottom: 5
			},
			'& p.setting-location-text': {
				position: 'absolute',
				bottom: 0,
				'& a': {
					textDecoration: 'none'
				}
			}
		}
	};
};

var SearchResults = function (_BaseSettings) {
	(0, _inherits3.default)(SearchResults, _BaseSettings);

	function SearchResults(props) {
		(0, _classCallCheck3.default)(this, SearchResults);

		var _this = (0, _possibleConstructorReturn3.default)(this, (SearchResults.__proto__ || (0, _getPrototypeOf2.default)(SearchResults)).call(this, props));

		_this.searchSettings = function () {
			var exactMatch = [];
			var fuzzyMatch = [];
			var userSearchedText = _this.state.searchText ? _this.state.searchText.trim().toLowerCase() : '';
			var userSearchTextArray = userSearchedText ? userSearchedText.split(/[ ,]+/) : [];
			if (userSearchedText) {
				for (var i = 0; i < _this.settingList.length; i++) {
					var settingItem = _this.settingList[i];

					if (settingItem.ignoreInSearch) {
						continue;
					}
					var searchText = settingItem['searchText'];
					searchText = searchText ? searchText.toLowerCase() : '';
					var isSearchResultMatched = searchText && searchText.indexOf(userSearchedText.toLowerCase()) >= 0;
					if (isSearchResultMatched) {
						exactMatch.push(settingItem);
					}

					if (!isSearchResultMatched) {
						var searchResultCount = 0;
						for (var j = 0; j < userSearchTextArray.length; j++) {
							var userSearchItem = userSearchTextArray[j];
							if (searchText.indexOf(userSearchItem.toLowerCase()) >= 0) {
								searchResultCount++;
							}
						}
						var searchMatchingPercentage = searchResultCount / userSearchTextArray.length * 100;
						if (searchMatchingPercentage >= 45) {
							settingItem.matchedPercentage = Math.round(searchMatchingPercentage);
							fuzzyMatch.push(settingItem);
						}
					}
				}
			}
			fuzzyMatch.sort(function (a, b) {
				return b.matchedPercentage - a.matchedPercentage;
			});
			_this.setState({
				filteredList: [].concat(exactMatch, fuzzyMatch)
			});
		};

		_this.handleKeyDown = function (setting) {
			return function (event) {
				if (event.keyCode === 13 && _this.props.onSettingItemClickCallback && typeof _this.props.onSettingItemClickCallback === 'function') {
					_this.props.onSettingItemClickCallback(setting);
				}
			};
		};

		_this.handleSettingCardClick = function (setting) {
			return function () {
				if (_this.props.onSettingItemClickCallback && typeof _this.props.onSettingItemClickCallback === 'function') {
					_this.props.onSettingItemClickCallback(setting);
				}
			};
		};

		_this.handleBackButton = function () {
			if (_this.props.backToInitialState && typeof _this.props.backToInitialState === 'function') {
				_this.props.backToInitialState();
			}
		};

		_this.settingList = _this.getSettingList();
		_this.state = {
			searchText: '',
			filteredList: []
		};
		_this.handleSearch = (0, _throttleDebounce.debounce)(500, _this.searchSettings);
		return _this;
	}

	(0, _createClass3.default)(SearchResults, [{
		key: 'render',
		value: function render() {
			var _this2 = this;

			var classes = this.props.classes;

			return _react2.default.createElement(
				'div',
				null,
				_react2.default.createElement(
					_Paper2.default,
					{ className: classes.root + ' mt-10 width50' },
					_react2.default.createElement(
						_IconButton2.default,
						{
							onClick: this.handleBackButton,
							className: classes.iconButton,
							'aria-label': 'back-arrow'
						},
						_react2.default.createElement('img', {
							src: './../inlineSVG/arrow_back-24px.svg',
							className: 'floatRight'
						})
					),
					_react2.default.createElement(_InputBase2.default, {
						className: classes.input,
						placeholder: 'Search',
						inputProps: { 'aria-label': 'search settings', autoFocus: true },
						onChange: this.updateInputChange('searchText', false, this.handleSearch)
					})
				),
				_react2.default.createElement(
					'div',
					{ className: 'search-result-list' },
					this.state.filteredList.length > 0 && this.state.filteredList.map(function (item, i) {
						return _react2.default.createElement(
							_Paper2.default,
							{ key: i, className: classes.searchDisplayCard },
							_react2.default.createElement(
								'div',
								{
									onKeyDown: _this2.handleKeyDown(item),
									onClick: _this2.handleSettingCardClick(item)
								},
								_react2.default.createElement('p', { dangerouslySetInnerHTML: { __html: item.heading } }),
								_react2.default.createElement(
									'div',
									{ className: 'font-11 color-707070' },
									_react2.default.createElement(
										'p',
										{ className: 'ellipsis' },
										item.what
									),
									_react2.default.createElement(
										'p',
										{ className: 'setting-location-text' },
										_react2.default.createElement(
											'a',
											{ href: '#', className: 'color-1789FC' },
											_SettingsMapping.VyaparSettings[item.vyaparSettingIndex] + ' Settings'
										)
									)
								)
							)
						);
					}),
					this.state.filteredList.length === 0 && this.state.searchText !== '' && _react2.default.createElement(
						'p',
						{ className: 'mt-10' },
						'No Results found'
					)
				)
			);
		}
	}]);
	return SearchResults;
}(_BaseSettings3.default);

exports.default = (0, _styles.withStyles)(styles)(SearchResults);