Object.defineProperty(exports, "__esModule", {
	value: true
});

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _extends3 = require('babel-runtime/helpers/extends');

var _extends4 = _interopRequireDefault(_extends3);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _BasePrintSettings2 = require('./BasePrintSettings');

var _BasePrintSettings3 = _interopRequireDefault(_BasePrintSettings2);

var _Queries = require('../../../Constants/Queries');

var _Queries2 = _interopRequireDefault(_Queries);

var _InvoiceTheme = require('../../../Constants/InvoiceTheme');

var _InvoiceTheme2 = _interopRequireDefault(_InvoiceTheme);

var _NumberTextField = require('../UIControls/NumberTextField');

var _NumberTextField2 = _interopRequireDefault(_NumberTextField);

var _styles = require('@material-ui/core/styles');

var _RadioButtonGroup = require('../UIControls/RadioButtonGroup');

var _RadioButtonGroup2 = _interopRequireDefault(_RadioButtonGroup);

var _Radio = require('@material-ui/core/Radio');

var _Radio2 = _interopRequireDefault(_Radio);

var _FormControlLabel = require('@material-ui/core/FormControlLabel');

var _FormControlLabel2 = _interopRequireDefault(_FormControlLabel);

var _SettingsMapping = require('./SettingsMapping');

var _SettingsMapping2 = _interopRequireDefault(_SettingsMapping);

var _SelectDropDown = require('../UIControls/SelectDropDown');

var _SelectDropDown2 = _interopRequireDefault(_SelectDropDown);

var _MenuItem = require('@material-ui/core/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _Tabs = require('@material-ui/core/Tabs');

var _Tabs2 = _interopRequireDefault(_Tabs);

var _Tab = require('@material-ui/core/Tab');

var _Tab2 = _interopRequireDefault(_Tab);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
	return {
		leftSeparator: {
			borderLeft: '2px solid #DDDDDD'
		},
		thermalPrinterPreviewContainer: {
			height: 'calc(100vh - 100px)',
			overflow: 'auto'
		},
		thermalPrinterPreview: {
			backgroundColor: '#ffffff',
			minHeight: '400px',
			boxShadow: '1px 1px 3px #a59b9b',
			border: '1px solid  #f5e6e6',
			width: '380px',
			padding: '15px',
			margin: 'auto',
			color: '#505050',
			'& .invoice-headings': {
				'& p': {
					marginBottom: '5px'
				}
			},
			'& .total-and-taxes': {
				'& span:first-child': {
					width: '150px',
					display: 'inline-block'
				},
				'&  span:last-child': {
					width: '50px',
					display: 'inline-block',
					textAlign: 'right'
				}
			},
			'& .items-table': {
				borderCollapse: 'collapse',
				'& tr:first-child': {
					borderBottom: '1px dotted'
				},
				'& tr:last-child': {
					borderTop: '1px dotted'
				}
			}
		},
		settingsColumn: {
			height: 'calc(100vh - 100px)',
			overflow: 'auto'
		},
		thermalPrinterPageSize: {
			'& .page-size-box.selected-box': {
				backgroundColor: '#1789FC',
				color: '#ffffff'
			},
			'& .MuiTypography-root': {
				fontSize: '14px'
			},
			'& .MuiFormControl-root': {
				margin: 0
			},
			'& .MuiFormGroup-root': {
				flexDirection: 'row'
			},
			'& .page-size-box': {
				padding: '7px',
				backgroundColor: '#ffffff',
				color: '#888888',
				border: '1px solid #dddddd',
				textAlign: 'center',
				'& .MuiFormControlLabel-root': {
					marginLeft: 0,
					marginRight: 0,
					'& .MuiButtonBase-root': {
						position: 'absolute',
						padding: '0 15px',
						color: 'transparent'
					}
				}
			},
			'& .page-size-box:first-child': {
				borderRadius: '4px 0 0 4px',
				border: '1px solid #dddddd',
				borderRight: 'none'
			},
			'& .page-size-box:last-child': {
				borderLeft: 'none',
				borderRadius: '0 4px 4px 0'
			}
		},
		miscThermalPrinterSetting: {
			'& .setting-heading': {
				width: '150px'
			}
		},
		'@global': {
			'.right-content-pane': {
				overflow: 'hidden !important'
			},
			'textarea': {
				overflowX: 'hidden'
			},
			'.custom-character-count': {
				width: '30px',
				paddingLeft: '5px',
				'& .MuiInput-formControl': {
					outline: 'none',
					border: 'none'
				},
				'& .MuiInput-formControl:before': {
					border: 'none'
				},
				'& .MuiInput-formControl:after': {
					border: 'none'
				},
				'& .MuiInput-underline:hover:not(.Mui-disabled):before': {
					border: 'none'
				},
				'& input': {
					margin: 0,
					padding: 0,
					borderBottom: 'none'
				},
				'& .MuiOutlinedInput-root': {
					height: '20px'
				},
				'& fieldset': {
					borderRadius: 0
				}
			},
			'.weight-bold': {
				fontWeight: 'bold'
			}
		}
	};
};

var ThermalPrintSettings = function (_BasePrintSettings) {
	(0, _inherits3.default)(ThermalPrintSettings, _BasePrintSettings);

	function ThermalPrintSettings(props) {
		(0, _classCallCheck3.default)(this, ThermalPrintSettings);

		var _this = (0, _possibleConstructorReturn3.default)(this, (ThermalPrintSettings.__proto__ || (0, _getPrototypeOf2.default)(ThermalPrintSettings)).call(this, props));

		_this.changePageAndTextSize = function (event) {
			var key = event.target.name;
			var value = event.target.value;
			_this.setState((0, _defineProperty3.default)({}, key, value), function () {
				_this.saveSettings(key, value);
			});
		};

		_this.openThermalPrinterChooserForPrinting = function () {
			var SettingsUtility = require('../../../UIControllers/SettingsUtility.js');
			SettingsUtility.setDefaultThermalPrinter();
		};

		_this.selectTheme = function (selectedTemplate) {
			_this.setCarouselIndex(selectedTemplate);
			_this.setState({
				selectedTemplate: selectedTemplate
			}, function () {
				_this.settingsModel.setSettingKey(_Queries2.default.SETTING_THERMAL_PRINTER_THEME);
				_this.settingsModel.setSettingValue(selectedTemplate);
				_this.settingsModel.UpdateSetting(selectedTemplate);
			});
		};

		_this.companyInfoHeaderList = [{
			key: _Queries2.default.SETTING_PRINT_COMPANY_NAME_ON_TXN_PDF,
			control: 'textbox',
			controlKey: 'companyName',
			value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_COMPANY_NAME_ON_TXN_PDF].heading
		}, {
			key: _Queries2.default.SETTING_PRINT_COMPANY_ADDRESS_ON_TXN_PDF,
			control: 'textbox',
			controlKey: 'companyAddress',
			value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_COMPANY_ADDRESS_ON_TXN_PDF].heading
		}, {
			key: _Queries2.default.SETTING_PRINT_COMPANY_EMAIL_ON_PDF,
			control: 'textbox',
			controlKey: 'companyEmail',
			value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_COMPANY_EMAIL_ON_PDF].heading
		}, {
			key: _Queries2.default.SETTING_PRINT_COMPANY_NUMBER_ON_TXN_PDF,
			control: 'textbox',
			controlKey: 'companyPhone',
			value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_COMPANY_NUMBER_ON_TXN_PDF].heading
		}, {
			key: _Queries2.default.SETTING_PRINT_TINNUMBER,
			control: 'textbox',
			controlKey: 'gstIn',
			value: (_this.settingCache.getGSTEnabled() ? 'GSTIN' : _this.settingCache.getTINText()) + ' on Sale',
			maxLength: 15
			// { key: Queries.SETTING_PRINT_PARTY_SHIPPING_ADDRESS, control: 'checkbox', value: SettingsMapping.miscBasePrinterSettings[Queries.SETTING_PRINT_PARTY_SHIPPING_ADDRESS].heading },
		}];

		_this.filterGSTINSettingForCompanyHeader(_this.companyInfoHeaderList);
		_this.thermalPrinterSettings = _SettingsMapping2.default.thermalPrinterSettings;
		_this.totalsAndTaxSettings = _SettingsMapping2.default.totalsAndTaxSettings;
		_this.themesImagesList = [{ id: _InvoiceTheme2.default.THERMAL_THEME_1, url: '../inlineSVG/theme_1.svg', label: 'Theme 1' }, { id: _InvoiceTheme2.default.THERMAL_THEME_2, url: '../inlineSVG/theme_2.svg', label: 'Theme 2' }];
		_this.footerSettings = [{ key: _Queries2.default.SETTING_PRINT_DESCRIPTION_ON_TXN_PDF, control: 'checkbox', value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_DESCRIPTION_ON_TXN_PDF].heading }, {
			key: _Queries2.default.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF,
			control: 'textbox',
			controlKey: _Queries2.default.SETTING_TERMS_AND_CONDITIONS,
			value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF].heading,
			moreAttributes: { multiline: true, rows: '3' }
		}];
		// Adding terms and conditions array for displaying data on modal
		_this.TermsAndConditionsSetting = [{
			key: _Queries2.default.SETTING_PRINT_SALE_INVOICE_TERM_AND_CONDITION_ON_TXN_PDF,
			control: 'textbox',
			controlKey: _Queries2.default.SETTING_SALE_INVOICE_TERMS_AND_CONDITIONS,
			value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_SALE_INVOICE_TERM_AND_CONDITION_ON_TXN_PDF].heading,
			moreAttributes: { multiline: true, rows: '3' }
		}, {
			key: _Queries2.default.SETTING_PRINT_SALE_ORDER_TERM_AND_CONDITION_ON_TXN_PDF,
			control: 'textbox',
			controlKey: _Queries2.default.SETTING_SALE_ORDER_TERMS_AND_CONDITIONS,
			value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_SALE_ORDER_TERM_AND_CONDITION_ON_TXN_PDF].heading,
			moreAttributes: { multiline: true, rows: '3' }
		}, {
			key: _Queries2.default.SETTING_PRINT_DELIVERY_CHALLAN_TERM_AND_CONDITION_ON_TXN_PDF,
			control: 'textbox',
			controlKey: _Queries2.default.SETTING_DELIVERY_CHALLAN_TERMS_AND_CONDITIONS,
			value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_DELIVERY_CHALLAN_TERM_AND_CONDITION_ON_TXN_PDF].heading,
			moreAttributes: { multiline: true, rows: '3' }
		}, {
			key: _Queries2.default.SETTING_PRINT_ESTIMATE_QUOTATION_TERM_AND_CONDITION_ON_TXN_PDF,
			control: 'textbox',
			controlKey: _Queries2.default.SETTING_ESTIMATE_QUOTATION_TERMS_AND_CONDITIONS,
			value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_ESTIMATE_QUOTATION_TERM_AND_CONDITION_ON_TXN_PDF].heading,
			moreAttributes: { multiline: true, rows: '3' }
		}];

		_this.state = (0, _extends4.default)({
			activeIndexLayout: 0
		}, _this.thermalPrinterSettingsState());
		return _this;
	}

	(0, _createClass3.default)(ThermalPrintSettings, [{
		key: 'resetState',
		value: function resetState() {
			this.getFirmDetails();
			this.setState((0, _extends4.default)({}, this.thermalPrinterSettingsState()));
		}
	}, {
		key: 'thermalPrinterSettingsState',
		value: function thermalPrinterSettingsState() {
			var _extends2;

			return (0, _extends4.default)({}, this.getCompanyPrintHeaderInfo(), (_extends2 = {
				selectedTemplate: this.settingCache.getTxnThermalTheme()
			}, (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_THERMAL_PRINTER_PAGE_SIZE, this.listOfSettings[_Queries2.default.SETTING_THERMAL_PRINTER_PAGE_SIZE]), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_THERMAL_PRINTER_CUSTOMIZE_CHARACTER_COUNT, this.settingCache.getThermalPrinterCustomizeCharacterCount()), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_THERMAL_PRINTER_TEXT_SIZE, this.settingCache.getThermalPrinterTextSize()), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_USE_ESC_POS_CODES_IN_THERMAL_PRINTER, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_USE_ESC_POS_CODES_IN_THERMAL_PRINTER])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_ENABLE_AUTOCUT_PAPER, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ENABLE_AUTOCUT_PAPER])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_ENABLE_OPEN_DRAWER_COMMAND, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ENABLE_OPEN_DRAWER_COMMAND])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_THERMAL_PRINTER_NATIVE_LANG, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_THERMAL_PRINTER_NATIVE_LANG])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_THERMAL_PRINTER_EXTRA_FOOTER_LINES, this.settingCache.getThermalPrinterExtraFooterLines()), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_THERMAL_PRINTER_COPY_COUNT, this.settingCache.getThermalPrintCopyCount()), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_DESCRIPTION_ON_TXN_PDF, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_DESCRIPTION_ON_TXN_PDF])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_ITEM_QUANTITY_TOTAL_ON_TXN_PDF, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_ITEM_QUANTITY_TOTAL_ON_TXN_PDF])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_AMOUNT_TILL_SPECIFIED_PLACES, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_AMOUNT_TILL_SPECIFIED_PLACES])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_TAX_DETAILS, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_TAX_DETAILS])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_SHOW_RECEIVED_AMOUNT_OF_TRANSACTION, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_SHOW_RECEIVED_AMOUNT_OF_TRANSACTION])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_SHOW_BALANCE_AMOUNT_OF_TRANSACTION, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_SHOW_BALANCE_AMOUNT_OF_TRANSACTION])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_SHOW_CURRENT_BALANCE_OF_PARTY, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_SHOW_CURRENT_BALANCE_OF_PARTY])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_TERMS_AND_CONDITIONS, this.listOfSettings[_Queries2.default.SETTING_TERMS_AND_CONDITIONS]), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_DEFAULT_PRINTER, this.listOfSettings[_Queries2.default.SETTING_DEFAULT_PRINTER]), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_SALE_INVOICE_TERMS_AND_CONDITIONS, this.listOfSettings[_Queries2.default.SETTING_SALE_INVOICE_TERMS_AND_CONDITIONS] || this.listOfSettings[_Queries2.default.SETTING_TERMS_AND_CONDITIONS]), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_SALE_ORDER_TERMS_AND_CONDITIONS, this.listOfSettings[_Queries2.default.SETTING_SALE_ORDER_TERMS_AND_CONDITIONS] || this.listOfSettings[_Queries2.default.SETTING_TERMS_AND_CONDITIONS]), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_DELIVERY_CHALLAN_TERMS_AND_CONDITIONS, this.listOfSettings[_Queries2.default.SETTING_DELIVERY_CHALLAN_TERMS_AND_CONDITIONS] || this.listOfSettings[_Queries2.default.SETTING_TERMS_AND_CONDITIONS]), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_ESTIMATE_QUOTATION_TERMS_AND_CONDITIONS, this.listOfSettings[_Queries2.default.SETTING_ESTIMATE_QUOTATION_TERMS_AND_CONDITIONS] || this.listOfSettings[_Queries2.default.SETTING_TERMS_AND_CONDITIONS]), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_SALE_INVOICE_TERM_AND_CONDITION_ON_TXN_PDF, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_SALE_INVOICE_TERM_AND_CONDITION_ON_TXN_PDF] || this.listOfSettings[_Queries2.default.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_SALE_ORDER_TERM_AND_CONDITION_ON_TXN_PDF, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_SALE_ORDER_TERM_AND_CONDITION_ON_TXN_PDF] || this.listOfSettings[_Queries2.default.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_DELIVERY_CHALLAN_TERM_AND_CONDITION_ON_TXN_PDF, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_DELIVERY_CHALLAN_TERM_AND_CONDITION_ON_TXN_PDF] || this.listOfSettings[_Queries2.default.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_ESTIMATE_QUOTATION_TERM_AND_CONDITION_ON_TXN_PDF, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_ESTIMATE_QUOTATION_TERM_AND_CONDITION_ON_TXN_PDF] || this.listOfSettings[_Queries2.default.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF])), _extends2));
		}
	}, {
		key: 'getSelectedTemplate',
		value: function getSelectedTemplate() {
			var ThemeTemplates = require('./ThemeTemplates').default;
			var themeTemplates = new ThemeTemplates(this);
			var themeTemplate = themeTemplates.getThermalPrinterThemeTemplate(this.state.selectedTemplate, this.state);
			if (!themeTemplate) {
				return 'NO PREVIEW TO SHOW';
			}
			return themeTemplate;
		}
	}, {
		key: 'renderThermalPrinterThemeCarousel',
		value: function renderThermalPrinterThemeCarousel() {
			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				this.renderThemeCarousel(this.themesImagesList, this.state.selectedTemplate, this.selectTheme)
			);
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			setTimeout(function () {
				$('.settingsToolTip').tooltipster({
					theme: 'tooltipster-noir',
					maxWidth: 300
				});
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _this2 = this;

			var classes = this.props.classes;
			var activeIndexLayout = this.state.activeIndexLayout;

			return _react2.default.createElement(
				'div',
				{ className: 'print-setting-container thermal-printer-settings' },
				_react2.default.createElement(
					'div',
					{ className: 'row' },
					_react2.default.createElement(
						'div',
						{ className: classes.settingsColumn + ' col-5' },
						_react2.default.createElement(
							_Tabs2.default,
							{ key: 1, className: 'font-12', textColor: 'primary', value: activeIndexLayout },
							_react2.default.createElement(_Tab2.default, { key: 1, label: 'Change Layout' })
						),
						activeIndexLayout === 0 && this.renderThermalPrinterThemeCarousel(),
						_react2.default.createElement(
							'div',
							{ className: 'setting-item d-flex align-items-center MAKE_THERMAL_PRINTER_DEFAULT' },
							_react2.default.createElement(
								'label',
								{ className: 'input-checkbox' },
								_react2.default.createElement('input', { type: 'checkbox', onChange: this.toggleDefaultPrinterModal, checked: this.state[_Queries2.default.SETTING_DEFAULT_PRINTER] == 2 }),
								this.checkboxJSX
							),
							_react2.default.createElement(
								'span',
								{ className: '' },
								_SettingsMapping2.default.miscBasePrinterSettings.MAKE_THERMAL_PRINTER_DEFAULT.heading
							),
							this.state[_Queries2.default.SETTING_DEFAULT_PRINTER] == 2 && _react2.default.createElement(
								'a',
								{ className: 'ml-10 font-12 color-1789FC d-block', onClick: this.openThermalPrinterChooserForPrinting },
								'Set default thermal printer'
							),
							this.renderToolTip(_SettingsMapping2.default.miscBasePrinterSettings.MAKE_THERMAL_PRINTER_DEFAULT)
						),
						_react2.default.createElement(
							'div',
							null,
							_react2.default.createElement(
								'div',
								{ className: classes.thermalPrinterPageSize + ' ' + _Queries2.default.SETTING_THERMAL_PRINTER_PAGE_SIZE },
								_react2.default.createElement(
									'div',
									{ className: 'height-66 d-flex align-items-center' },
									_react2.default.createElement(
										'span',
										{ className: 'mr-10' },
										_SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_THERMAL_PRINTER_PAGE_SIZE].heading
									),
									_react2.default.createElement(
										_RadioButtonGroup2.default,
										{ handleChange: this.changePageAndTextSize, name: _Queries2.default.SETTING_THERMAL_PRINTER_PAGE_SIZE, value: this.state[_Queries2.default.SETTING_THERMAL_PRINTER_PAGE_SIZE] },
										_react2.default.createElement(
											'div',
											{ className: (this.state[_Queries2.default.SETTING_THERMAL_PRINTER_PAGE_SIZE] == '1' ? 'selected-box' : '') + ' page-size-box font-14' },
											_react2.default.createElement(_FormControlLabel2.default, { control: _react2.default.createElement(_Radio2.default, null), value: '1', label: '2 Inch' }),
											_react2.default.createElement(
												'p',
												{ className: 'font-12' },
												'58mm'
											)
										),
										_react2.default.createElement(
											'div',
											{ className: (this.state[_Queries2.default.SETTING_THERMAL_PRINTER_PAGE_SIZE] == '2' ? 'selected-box' : '') + ' page-size-box font-14' },
											_react2.default.createElement(_FormControlLabel2.default, { control: _react2.default.createElement(_Radio2.default, null), value: '2', label: '3 Inch' }),
											_react2.default.createElement(
												'p',
												{ className: 'font-12' },
												'68mm'
											)
										),
										_react2.default.createElement(
											'div',
											{ className: (this.state[_Queries2.default.SETTING_THERMAL_PRINTER_PAGE_SIZE] == '3' ? 'selected-box' : '') + ' page-size-box font-14' },
											_react2.default.createElement(_FormControlLabel2.default, { control: _react2.default.createElement(_Radio2.default, null), value: '3', label: '4 Inch' }),
											_react2.default.createElement(
												'p',
												{ className: 'font-12' },
												'88mm'
											)
										),
										_react2.default.createElement(
											'div',
											{ className: (this.state[_Queries2.default.SETTING_THERMAL_PRINTER_PAGE_SIZE] == '4' ? 'selected-box' : '') + ' page-size-box font-14' },
											_react2.default.createElement(_FormControlLabel2.default, { control: _react2.default.createElement(_Radio2.default, null), value: '4', label: 'Custom' }),
											_react2.default.createElement(
												'div',
												null,
												_react2.default.createElement(_NumberTextField2.default, {
													disabled: this.state[_Queries2.default.SETTING_THERMAL_PRINTER_PAGE_SIZE] != '4',
													className: 'custom-character-count bg-white d-inline-block',
													margin: 'dense',
													value: this.state[_Queries2.default.SETTING_THERMAL_PRINTER_CUSTOMIZE_CHARACTER_COUNT],
													handleBlur: this.handleNumber(_Queries2.default.SETTING_THERMAL_PRINTER_CUSTOMIZE_CHARACTER_COUNT),
													maxLength: 2,
													showArrow: false
												}),
												_react2.default.createElement(
													'span',
													{ className: 'ml-5 font-10' },
													'(Chars)'
												)
											)
										)
									),
									this.renderToolTip(_SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_THERMAL_PRINTER_PAGE_SIZE])
								),
								this.state[_Queries2.default.SETTING_THERMAL_PRINTER_PAGE_SIZE] == '1' && !this.state[_Queries2.default.SETTING_THERMAL_PRINTER_NATIVE_LANG] && _react2.default.createElement(
									'div',
									{ className: 'setting-item mt-10 d-flex align-items-center ' + _Queries2.default.SETTING_THERMAL_PRINTER_TEXT_SIZE },
									_react2.default.createElement(
										'label',
										{ className: 'mr-20' },
										_SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_THERMAL_PRINTER_TEXT_SIZE].heading
									),
									_react2.default.createElement(
										_RadioButtonGroup2.default,
										{ handleChange: this.changePageAndTextSize, name: _Queries2.default.SETTING_THERMAL_PRINTER_TEXT_SIZE, value: this.state[_Queries2.default.SETTING_THERMAL_PRINTER_TEXT_SIZE] },
										_react2.default.createElement(
											'div',
											{ className: (this.state[_Queries2.default.SETTING_THERMAL_PRINTER_TEXT_SIZE] == '1' ? 'selected-box' : '') + ' page-size-box font-14' },
											_react2.default.createElement(_FormControlLabel2.default, { control: _react2.default.createElement(_Radio2.default, { className: true }), value: '1', label: 'Regular' })
										),
										_react2.default.createElement(
											'div',
											{ className: (this.state[_Queries2.default.SETTING_THERMAL_PRINTER_TEXT_SIZE] == '0' ? 'selected-box' : '') + ' page-size-box font-14' },
											_react2.default.createElement(_FormControlLabel2.default, { control: _react2.default.createElement(_Radio2.default, null), value: '0', label: 'Small' })
										)
									),
									this.renderToolTip(_SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_THERMAL_PRINTER_TEXT_SIZE])
								),
								this.state[_Queries2.default.SETTING_THERMAL_PRINTER_NATIVE_LANG] && _react2.default.createElement(
									'div',
									{ className: 'setting-item d-flex align-items-center ' + _Queries2.default.SETTING_PRINT_TEXT_SIZE },
									_react2.default.createElement(
										'span',
										{ className: 'setting-heading mr-10' },
										_SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_TEXT_SIZE].heading
									),
									_react2.default.createElement(
										_SelectDropDown2.default,
										{ outlined: true, handleChange: this.saveSettings.bind(this, _Queries2.default.SETTING_PRINT_TEXT_SIZE), value: this.listOfSettings[_Queries2.default.SETTING_PRINT_TEXT_SIZE], name: 'amountInWords' },
										this.textSizeDd.map(function (option, index) {
											return _react2.default.createElement(
												_MenuItem2.default,
												{ key: option.key, value: option.key },
												option.value
											);
										})
									),
									this.renderToolTip(_SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_TEXT_SIZE])
								)
							)
						),
						_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
						_react2.default.createElement(
							'div',
							{ className: classes.miscThermalPrinterSetting },
							(0, _keys2.default)(this.thermalPrinterSettings).map(function (key, index) {
								return _react2.default.createElement(
									'div',
									{ key: index, className: 'setting-item d-flex align-items-center ' + key },
									_react2.default.createElement(
										'label',
										{ className: 'input-checkbox' },
										_react2.default.createElement('input', { type: 'checkbox', onChange: _this2.toggleCheckbox(key), checked: _this2.state[key] }),
										_this2.checkboxJSX
									),
									_react2.default.createElement('span', { dangerouslySetInnerHTML: { __html: _this2.thermalPrinterSettings[key].heading } }),
									_this2.renderToolTip(_SettingsMapping2.default.thermalPrinterSettings[key])
								);
							}),
							_react2.default.createElement(
								'div',
								{ className: 'setting-item d-flex align-items-center ' + _Queries2.default.SETTING_THERMAL_PRINTER_EXTRA_FOOTER_LINES },
								_react2.default.createElement(
									'span',
									{ className: 'setting-heading' },
									_SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_THERMAL_PRINTER_EXTRA_FOOTER_LINES].heading
								),
								_react2.default.createElement(_NumberTextField2.default, {
									className: 'w-50-px bg-white',
									margin: 'dense',
									variant: 'outlined',
									value: this.state[_Queries2.default.SETTING_THERMAL_PRINTER_EXTRA_FOOTER_LINES],
									handleBlur: this.handleNumber(_Queries2.default.SETTING_THERMAL_PRINTER_EXTRA_FOOTER_LINES),
									maxLength: 2,
									showArrow: false
								}),
								this.renderToolTip(_SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_THERMAL_PRINTER_EXTRA_FOOTER_LINES])
							),
							_react2.default.createElement(
								'div',
								{ className: 'setting-item d-flex align-items-center ' + _Queries2.default.SETTING_THERMAL_PRINTER_COPY_COUNT },
								_react2.default.createElement(
									'span',
									{ className: 'setting-heading' },
									_SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_THERMAL_PRINTER_COPY_COUNT].heading
								),
								_react2.default.createElement(_NumberTextField2.default, {
									className: 'w-50-px bg-white',
									margin: 'dense',
									variant: 'outlined',
									value: this.state[_Queries2.default.SETTING_THERMAL_PRINTER_COPY_COUNT],
									handleBlur: this.handleNumber(_Queries2.default.SETTING_THERMAL_PRINTER_COPY_COUNT),
									maxLength: 1,
									showArrow: false
								}),
								this.renderToolTip(_SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_THERMAL_PRINTER_COPY_COUNT])
							)
						),
						_react2.default.createElement(
							'div',
							{ className: 'mb-20' },
							_react2.default.createElement(
								'span',
								{ className: 'heading font-family-bold font-16' },
								'Print Company Info / Header'
							),
							_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
							this.renderCompanyInfoHeader(this.companyInfoHeaderList),
							_react2.default.createElement(
								'a',
								{ className: 'color-1789FC d-inline-block', onClick: this.toggleChangeTransactionNameModal },
								'Change Transaction Names >'
							)
						),
						_react2.default.createElement(
							'div',
							{ className: 'mb-20' },
							_react2.default.createElement(
								'span',
								{ className: 'heading font-family-bold font-16' },
								'Totals & Taxes'
							),
							_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
							(0, _keys2.default)(this.totalsAndTaxSettings).map(function (key, index) {
								return _react2.default.createElement(
									'div',
									{ key: index, className: 'setting-item d-flex align-items-center ' + key },
									_react2.default.createElement(
										'label',
										{ className: 'input-checkbox' },
										_react2.default.createElement('input', { type: 'checkbox', onChange: _this2.toggleCheckbox(key), checked: _this2.state[key] }),
										_this2.checkboxJSX
									),
									_react2.default.createElement('span', { dangerouslySetInnerHTML: { __html: _this2.totalsAndTaxSettings[key].heading } }),
									_this2.renderToolTip(_SettingsMapping2.default.totalsAndTaxSettings[key])
								);
							})
						),
						_react2.default.createElement(
							'div',
							null,
							_react2.default.createElement(
								'span',
								{ className: 'heading font-family-bold font-16' },
								'Footer'
							),
							_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
							this.renderFooter()
						)
					),
					_react2.default.createElement(
						'div',
						{ className: classes.leftSeparator + ' ' + classes.thermalPrinterPreviewContainer + ' col-7' },
						_react2.default.createElement(
							'div',
							{ className: classes.thermalPrinterPreview + ' font-family-medium' },
							_react2.default.createElement(
								'div',
								{ style: { zoom: '75%' } },
								this.getSelectedTemplate()
							)
						)
					)
				),
				this.renderCustomTransactionNamesModal(),
				this.renderDefaultPrinterModal(),
				this.state.openTermsAndConditionsModal && this.renderCustomTermsAndConditionsModal()
			);
		}
	}]);
	return ThermalPrintSettings;
}(_BasePrintSettings3.default);

exports.default = (0, _styles.withStyles)(styles)(ThermalPrintSettings);