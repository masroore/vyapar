Object.defineProperty(exports, "__esModule", {
	value: true
});

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _BaseSettings2 = require('./BaseSettings');

var _BaseSettings3 = _interopRequireDefault(_BaseSettings2);

var _SelectDropDown = require('../UIControls/SelectDropDown');

var _SelectDropDown2 = _interopRequireDefault(_SelectDropDown);

var _MenuItem = require('@material-ui/core/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _styles = require('@material-ui/core/styles');

var _Modal = require('../Modal');

var _Modal2 = _interopRequireDefault(_Modal);

var _Queries = require('../../../Constants/Queries');

var _Queries2 = _interopRequireDefault(_Queries);

var _SettingsMapping = require('./SettingsMapping');

var _SettingsMapping2 = _interopRequireDefault(_SettingsMapping);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _TextField = require('@material-ui/core/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _TaxCodeCache = require('../../../Cache/TaxCodeCache');

var _TaxCodeCache2 = _interopRequireDefault(_TaxCodeCache);

var _TaxCodeConstants = require('../../../Constants/TaxCodeConstants');

var _TaxCodeConstants2 = _interopRequireDefault(_TaxCodeConstants);

var _TaxCode = require('../../../BizLogic/TaxCode');

var _TaxCode2 = _interopRequireDefault(_TaxCode);

var _ErrorCode = require('../../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

var _DecimalInputFilter = require('../../../Utilities/DecimalInputFilter');

var _DecimalInputFilter2 = _interopRequireDefault(_DecimalInputFilter);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
	return {
		compositeScheme: {
			'& label + .MuiInput-formControl': {
				marginTop: 0
			}
		},
		button: {
			color: '#1789FC',
			backgroundColor: '#e8e8ff',
			padding: '2px',
			textTransform: 'initial',
			fontSize: '12px'
		},
		taxRateList: {
			height: 'calc(100vh - 100px)',
			overflow: 'auto'
		},
		taxGroupList: {
			height: 'calc(100vh - 100px)',
			overflow: 'auto',
			'& .tax-codes': {
				color: '#284561',
				margin: '4px 0',
				padding: '4px 0',
				flexWrap: 'wrap',
				lineHeight: '20px',
				'& span': {
					width: '80px'
				}
			}
		},
		'@global': {
			'.tax-rate-grouping': {
				height: '300px',
				overflow: 'auto',
				'& .input-checkbox': {
					margin: '0 5px'
				}
			},
			'.separator': {
				borderBottom: '1px solid #e2e2e2',
				padding: '10px'
			},
			'.separator:first-child': {
				paddingTop: 0
			},
			'.separator:last-child': {
				border: 'none'
			}
		}
	};
};

var GSTSettings = function (_BaseSettings) {
	(0, _inherits3.default)(GSTSettings, _BaseSettings);

	function GSTSettings(props) {
		(0, _classCallCheck3.default)(this, GSTSettings);

		var _this = (0, _possibleConstructorReturn3.default)(this, (GSTSettings.__proto__ || (0, _getPrototypeOf2.default)(GSTSettings)).call(this, props));

		_this.enableGSTCallback = function (isGSTEnable) {
			if (isGSTEnable) {
				var _settings, _this$setState;

				var InvoiceTheme = require('../../../Constants/InvoiceTheme');
				var settings = (_settings = {}, (0, _defineProperty3.default)(_settings, _Queries2.default.SETTING_HSN_SAC_ENABLED, '1'), (0, _defineProperty3.default)(_settings, _Queries2.default.SETTING_ITEMWISE_TAX_ENABLED, '1'), (0, _defineProperty3.default)(_settings, _Queries2.default.SETTING_TIN_NUMBER_ENABLED, '1'), (0, _defineProperty3.default)(_settings, _Queries2.default.SETTING_ENABLE_PLACE_OF_SUPPLY, '1'), (0, _defineProperty3.default)(_settings, _Queries2.default.SETTING_TXN_PDF_THEME, InvoiceTheme.THEME_10 + ''), _settings);
				_this.setState((_this$setState = {}, (0, _defineProperty3.default)(_this$setState, _Queries2.default.SETTING_HSN_SAC_ENABLED, true), (0, _defineProperty3.default)(_this$setState, _Queries2.default.SETTING_ITEMWISE_TAX_ENABLED, true), (0, _defineProperty3.default)(_this$setState, _Queries2.default.SETTING_TIN_NUMBER_ENABLED, true), (0, _defineProperty3.default)(_this$setState, _Queries2.default.SETTING_ENABLE_PLACE_OF_SUPPLY, true), (0, _defineProperty3.default)(_this$setState, _Queries2.default.SETTING_TXN_PDF_THEME, InvoiceTheme.THEME_10 + ''), _this$setState));
				_this.saveMultipleSettings(settings);
			} else {
				var _settings3, _this$setState2;

				var _settings2 = (_settings3 = {}, (0, _defineProperty3.default)(_settings3, _Queries2.default.SETTING_HSN_SAC_ENABLED, '0'), (0, _defineProperty3.default)(_settings3, _Queries2.default.SETTING_ENABLE_PLACE_OF_SUPPLY, '0'), (0, _defineProperty3.default)(_settings3, _Queries2.default.SETTING_ENABLE_ADDITIONAL_CESS_ON_ITEM, '0'), (0, _defineProperty3.default)(_settings3, _Queries2.default.SETTING_ENABLE_REVERSE_CHARGE, '0'), (0, _defineProperty3.default)(_settings3, _Queries2.default.SETTING_ENABLE_EWAY_BILL_NUMBER, '0'), (0, _defineProperty3.default)(_settings3, _Queries2.default.SETTING_IS_COMPOSITE_SCHEME_ENABLED, '0'), _settings3);
				_this.setState((_this$setState2 = {}, (0, _defineProperty3.default)(_this$setState2, _Queries2.default.SETTING_HSN_SAC_ENABLED, false), (0, _defineProperty3.default)(_this$setState2, _Queries2.default.SETTING_ENABLE_PLACE_OF_SUPPLY, false), (0, _defineProperty3.default)(_this$setState2, _Queries2.default.SETTING_ENABLE_ADDITIONAL_CESS_ON_ITEM, false), (0, _defineProperty3.default)(_this$setState2, _Queries2.default.SETTING_ENABLE_REVERSE_CHARGE, false), (0, _defineProperty3.default)(_this$setState2, _Queries2.default.SETTING_ENABLE_EWAY_BILL_NUMBER, false), (0, _defineProperty3.default)(_this$setState2, _Queries2.default.SETTING_IS_COMPOSITE_SCHEME_ENABLED, false), _this$setState2));
				_this.saveMultipleSettings(_settings2);
			}
		};

		_this.getTaxGroupModal = function () {
			return _react2.default.createElement(
				_Modal2.default,
				{
					height: '500px',
					width: '350px',
					isOpen: _this.state.openTaxGroupModal,
					canHideHeader: 'true',
					title: _this.taxGroupTitle,
					parentClassSelector: 'tax-group-modal-container',
					onClose: _this.toggleTaxGroupModal
				},
				_react2.default.createElement(
					'div',
					{ className: 'modalBody' },
					_react2.default.createElement(_TextField2.default, {
						label: 'Enter Group Name',
						margin: 'dense',
						variant: 'outlined',
						className: 'width100',
						value: _this.state.taxGroupName,
						onChange: _this.updateInputChange('taxGroupName'),
						inputProps: {
							maxLength: 20
						}
					}),
					_react2.default.createElement(
						'p',
						{ className: 'heading font-family-bold font-14 mt-10 mb-10' },
						'Select Taxes'
					),
					_react2.default.createElement(
						'div',
						{ className: 'tax-rate-grouping' },
						_this.taxRatesForModalJSX
					),
					_react2.default.createElement(
						'div',
						{ className: 'floatRight mt-10' },
						_react2.default.createElement(
							_Button2.default,
							{ margin: 'dense', className: 'mr-10', onClick: _this.toggleTaxGroupModal, variant: 'contained', color: 'primary' },
							' Cancel'
						),
						_react2.default.createElement(
							_Button2.default,
							{ margin: 'dense', onClick: _this.saveTaxGroup, variant: 'contained', color: 'primary' },
							' Save'
						)
					)
				)
			);
		};

		_this.getTaxRateModal = function () {
			return _react2.default.createElement(
				_Modal2.default,
				{
					height: '230px',
					width: '350px',
					isOpen: _this.state.openTaxRateModal,
					onClose: _this.toggleTaxRateModal,
					canHideHeader: 'true',
					title: _this.taxRateTitle,
					parentClassSelector: 'tax-rate-modal-container'
				},
				_react2.default.createElement(
					'div',
					{ className: 'modalBody' },
					_react2.default.createElement(
						'div',
						{ className: 'tax-rate-form' },
						_react2.default.createElement(_TextField2.default, {
							label: 'Tax Name',
							margin: 'dense',
							variant: 'outlined',
							className: 'width100',
							value: _this.state.taxRateName,
							onChange: _this.updateInputChange('taxRateName'),
							inputProps: {
								maxLength: 20
							}
						}),
						_react2.default.createElement(
							'div',
							{ className: 'd-flex justify-content-between align-items-center' },
							_react2.default.createElement(_TextField2.default, {
								onChange: _this.updateInputChange('taxRate'),
								label: 'Rate',
								width: 100,
								onInput: function onInput(e) {
									return _DecimalInputFilter2.default.inputTextFilterForPercentage(e.target);
								},
								value: _this.state.taxRate,
								margin: 'dense',
								variant: 'outlined',
								className: 'd-inline-flex justify-content-center w-20-px'
							}),
							_react2.default.createElement(
								_SelectDropDown2.default,
								{ outlined: true, name: 'taxType', id: 'taxType', handleChange: _this.handleDropdownChange.bind(_this, 'taxType'), value: _this.state.taxType },
								_this.taxTypeList.map(function (tax, index) {
									return _react2.default.createElement(
										_MenuItem2.default,
										{ key: tax.key, value: tax.key },
										tax.value
									);
								})
							)
						),
						_react2.default.createElement(
							'div',
							{ className: 'floatRight mt-10' },
							_react2.default.createElement(
								_Button2.default,
								{ margin: 'dense', className: 'mr-10', onClick: _this.toggleTaxRateModal, variant: 'contained', color: 'primary' },
								' Cancel'
							),
							_react2.default.createElement(
								_Button2.default,
								{ margin: 'dense', onClick: _this.saveTaxRate, variant: 'contained', color: 'primary' },
								' Save'
							)
						)
					)
				)
			);
		};

		_this.compositeSchemeList = [{ key: '1', value: 'Trader(Goods) 1.0%' }, { key: '0', value: 'Manufacturer 1.0%' }, { key: '2', value: 'Restaurant 5.0%' }];
		_this.taxTypeList = [{ key: 4, value: 'Other' }, { key: 1, value: 'SGST' }, { key: 2, value: 'CGST' }, { key: 3, value: 'IGST' }, { key: 5, value: 'CESS' }, { key: 7, value: 'Flood CESS' }];
		_this.selectGroupTaxRates = _this.selectGroupTaxRates.bind(_this);
		_this.toggleTaxRateModal = _this.toggleTaxRateModal.bind(_this);
		_this.toggleTaxGroupModal = _this.toggleTaxGroupModal.bind(_this);
		_this.saveTaxGroup = _this.saveTaxGroup.bind(_this);
		_this.saveTaxRate = _this.saveTaxRate.bind(_this);

		_this.createTaxRateAndTaxGroupList();
		_this.generateTaxRateJSXForModal();
		_this.state = (0, _extends3.default)({}, _this.gstSettingsState());
		_this.gstSettingsList = _SettingsMapping2.default.gstSettingsList;

		_this.toggleTaxListView = _this.toggleTaxListView.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(GSTSettings, [{
		key: 'gstSettingsState',
		value: function gstSettingsState() {
			var _ref;

			return _ref = {}, (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_GST_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_GST_ENABLED])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_HSN_SAC_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_HSN_SAC_ENABLED])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ENABLE_ADDITIONAL_CESS_ON_ITEM, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ENABLE_ADDITIONAL_CESS_ON_ITEM])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ENABLE_REVERSE_CHARGE, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ENABLE_REVERSE_CHARGE])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ENABLE_PLACE_OF_SUPPLY, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ENABLE_PLACE_OF_SUPPLY])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ENABLE_EWAY_BILL_NUMBER, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ENABLE_EWAY_BILL_NUMBER])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_IS_COMPOSITE_SCHEME_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_IS_COMPOSITE_SCHEME_ENABLED])), (0, _defineProperty3.default)(_ref, 'openTaxListModal', false), (0, _defineProperty3.default)(_ref, 'openTaxRateModal', false), (0, _defineProperty3.default)(_ref, 'openTaxGroupModal', false), (0, _defineProperty3.default)(_ref, 'taxGroupJSX', this.taxGroupJSX), (0, _defineProperty3.default)(_ref, 'taxRateJSX', this.taxRateJSX), (0, _defineProperty3.default)(_ref, 'taxRatesForModalJSX', this.taxRatesForModalJSX), (0, _defineProperty3.default)(_ref, 'listOfTaxCodeId', {}), (0, _defineProperty3.default)(_ref, 'showTaxListView', !this.settingCache.isCurrentCountryIndia()), _ref;
		}
	}, {
		key: 'resetState',
		value: function resetState() {
			this.setState((0, _extends3.default)({}, this.gstSettingsState()));
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.higlightSearchedSetting();
			this.refreshRateList();
			this.initializeToolTips();
		}
	}, {
		key: 'toggleTaxListView',
		value: function toggleTaxListView() {
			this.setState({
				showTaxListView: !this.state.showTaxListView
			});
		}
	}, {
		key: 'toggleTaxRateModal',
		value: function toggleTaxRateModal(taxRateParams) {
			var taxType = 4;var taxRateName = void 0;var taxRate = '';
			this.isTaxRateEditForm = false;
			this.taxRateTitle = 'Add Tax Rate';
			if (taxRateParams && taxRateParams.isEditForm) {
				var taxItem = taxRateParams.taxItem;
				taxType = taxItem.getTaxRateType();
				taxRateName = taxItem.getTaxCodeName();
				taxRate = taxItem.getTaxRate();
				this.isTaxRateEditForm = true;
				this.taxRateTitle = 'Edit Tax Rate';
				this.taxRateId = taxItem.getTaxCodeId();
			}
			this.setState({
				openTaxRateModal: !this.state.openTaxRateModal,
				taxRateName: taxRateName,
				taxType: taxType,
				taxRate: taxRate
			});
		}
	}, {
		key: 'toggleTaxGroupModal',
		value: function toggleTaxGroupModal(taxGroupParams) {
			var _this2 = this;

			var listOfTaxCodeId = {};
			var taxGroupName = void 0;
			this.taxGroupEditForm = false;
			this.taxGroupTitle = 'Add Tax Group';
			if (taxGroupParams && taxGroupParams.isEditForm) {
				var taxItem = taxGroupParams.taxItem;
				var taxGroupIds = taxItem.getTaxCodeMap();
				taxGroupName = taxItem.getTaxCodeName();
				this.taxGroupId = taxItem.getTaxCodeId();
				this.taxGroupEditForm = true;
				this.taxGroupTitle = 'Edit Tax Group';
				taxGroupIds.map(function (key) {
					listOfTaxCodeId[key] = true;
				});
			}
			this.setState({
				openTaxGroupModal: !this.state.openTaxGroupModal,
				listOfTaxCodeId: listOfTaxCodeId,
				taxGroupName: taxGroupName
			}, function () {
				if (_this2.state.openTaxGroupModal) {
					_this2.generateTaxRateJSXForModal();
					_this2.setState({
						taxRatesForModalJSX: _this2.taxRatesForModalJSX
					});
				}
			});
		}
	}, {
		key: 'createTaxRateAndTaxGroupList',
		value: function createTaxRateAndTaxGroupList() {
			var _this3 = this;

			this.taxCodeCache = new _TaxCodeCache2.default();
			var listOfTaxCodes = this.taxCodeCache.getListOfTax();
			this.taxRatesListForModal = [];
			this.taxRateJSX = [];
			this.taxGroupJSX = [];
			(0, _keys2.default)(listOfTaxCodes).map(function (taxName) {
				var taxItem = listOfTaxCodes[taxName][0];
				var isGroupItem = taxItem.getTaxType() === _TaxCodeConstants2.default.taxGroup;
				var groupTaxCode = void 0,
				    taxCodes = void 0,
				    taxItemClickEvent = void 0,
				    taxDeleteClickEvent = void 0;
				if (isGroupItem) {
					groupTaxCode = taxItem.getTaxCodeMap();
					taxCodes = groupTaxCode.map(function (taxCode, i) {
						var code = _this3.taxCodeCache.getTaxCodeNameById(taxCode);
						return _react2.default.createElement(
							'span',
							{ key: i, className: 'ml-10' },
							code
						);
					});
					taxItemClickEvent = _this3.toggleTaxGroupModal.bind(_this3, { taxItem: taxItem, isEditForm: true });
					taxDeleteClickEvent = _this3.deleteTaxGroup.bind(_this3, taxItem);
				} else {
					_this3.taxRatesListForModal.push(taxItem);
					taxItemClickEvent = _this3.toggleTaxRateModal.bind(_this3, { taxItem: taxItem, isEditForm: true });
					taxDeleteClickEvent = _this3.deleteTaxRate.bind(_this3, taxItem);
				}
				var taxItemJSX = _react2.default.createElement(
					'div',
					{ key: taxItem.getTaxCodeId(), onDoubleClick: taxItemClickEvent, className: (isGroupItem ? 'tax-group-item' : 'tax-rate-item') + ' font-13 separator' },
					_react2.default.createElement(
						'div',
						{ className: 'd-flex justify-content-between align-items-center ' },
						_react2.default.createElement(
							'span',
							null,
							taxName
						),
						_react2.default.createElement(
							'div',
							null,
							!isGroupItem && _react2.default.createElement(
								'span',
								null,
								taxItem.getTaxRate()
							),
							_react2.default.createElement('img', { className: 'ml-10', onClick: taxItemClickEvent, src: '../inlineSVG/baseline-edit-14px.svg' }),
							_react2.default.createElement('img', { className: 'ml-10', onClick: taxDeleteClickEvent, src: '../inlineSVG/baseline-delete-24px.svg' })
						)
					),
					isGroupItem && _react2.default.createElement(
						'div',
						{ className: 'd-flex align-items-center tax-codes font-12' },
						taxCodes
					)
				);
				if (!isGroupItem) {
					_this3.taxRateJSX.push(taxItemJSX);
				} else {
					_this3.taxGroupJSX.push(taxItemJSX);
				}
			});
		}
	}, {
		key: 'refreshRateList',
		value: function refreshRateList() {
			this.createTaxRateAndTaxGroupList();
			this.setState({
				taxGroupJSX: this.taxGroupJSX,
				taxRateJSX: this.taxRateJSX
			});
		}
	}, {
		key: 'selectGroupTaxRates',
		value: function selectGroupTaxRates(e) {
			var _this4 = this;

			var key = e.target.dataset && e.target.dataset.taxCodeId ? e.target.dataset.taxCodeId : '';
			var value = !this.state.listOfTaxCodeId[key];
			var listOfTaxCodeId = (0, _extends3.default)({}, this.state.listOfTaxCodeId);
			if (value) {
				listOfTaxCodeId[key] = value;
			} else {
				delete listOfTaxCodeId[key];
			}
			this.setState({
				listOfTaxCodeId: listOfTaxCodeId
			}, function () {
				if (_this4.state.openTaxGroupModal) {
					_this4.generateTaxRateJSXForModal();
					_this4.setState({
						taxRatesForModalJSX: _this4.taxRatesForModalJSX
					});
				}
			});
		}
	}, {
		key: 'generateTaxRateJSXForModal',
		value: function generateTaxRateJSXForModal() {
			var _this5 = this;

			this.taxRatesForModalJSX = [];
			if (this.taxRatesListForModal && this.taxRatesListForModal.length > 0) {
				var listOfTaxCodeId = this.state ? this.state.listOfTaxCodeId : {};
				this.taxRatesForModalJSX = this.taxRatesListForModal.map(function (taxRateItem, index) {
					var taxCodeId = taxRateItem.getTaxCodeId();
					return _react2.default.createElement(
						'div',
						{ key: index, className: 'd-flex justify-content-between align-items-center mb-10 font-14' },
						_react2.default.createElement(
							'span',
							null,
							taxRateItem.getTaxCodeName()
						),
						_react2.default.createElement(
							'div',
							{ className: 'floatRight d-flex align-items-center' },
							_react2.default.createElement(
								'span',
								null,
								taxRateItem.getTaxRate(),
								'%'
							),
							_react2.default.createElement(
								'label',
								{ className: 'ml-10 input-checkbox' },
								_react2.default.createElement('input', { type: 'checkbox', 'data-tax-code-id': taxCodeId, onChange: _this5.selectGroupTaxRates, checked: listOfTaxCodeId[taxCodeId] }),
								_this5.checkboxJSX
							)
						)
					);
				});
			}
		}
	}, {
		key: 'saveTaxRate',
		value: function saveTaxRate() {
			var taxRateName = this.state.taxRateName ? this.state.taxRateName.trim() : '';
			var taxRate = this.state.taxRate;
			var isEditForm = this.isTaxRateEditForm;
			var statusCode = void 0;
			if (taxRate === '' || taxRateName === '') {
				ToastHelper.error('fields cannot be empty');
				return false;
			}

			taxRate = Number(taxRate);
			if (isNaN(taxRate)) {
				ToastHelper.error('Please enter rate in correct format. Eg:- 11.15');
				return false;
			}

			var taxType = this.state.taxType || _TaxCodeConstants2.default['OTHER'];
			if (!isNaN(taxRate) && taxRate >= 0) {
				var taxCodeObject = !isEditForm ? new _TaxCode2.default() : this.taxCodeCache.getTaxCodeObjectById(this.taxRateId);
				if (isEditForm && !taxCodeObject.canUpdateTaxRate()) {
					return;
				}
				taxCodeObject.setTaxCodeName(taxRateName);
				taxCodeObject.setTaxRate(taxRate);
				taxCodeObject.setTaxDateModified(new Date());
				taxCodeObject.setTaxType(_TaxCodeConstants2.default.taxRate);
				taxCodeObject.setTaxRateType(taxType);
				taxCodeObject.setTaxCodeMap([]);

				if (!isEditForm) {
					taxCodeObject.setTaxDateCreated(new Date());
					statusCode = taxCodeObject.addNewTaxRate();
					this.handleTaxListStatus(statusCode == _ErrorCode2.default.ERROR_TAX_CODE_NAME_SAVE_SUCCESS, statusCode, 'Add Tax Save');
				} else {
					statusCode = taxCodeObject.updateTaxRate();
					this.handleTaxListStatus(statusCode == _ErrorCode2.default.ERROR_TAX_CODE_NAME_EDIT_SUCCESS, statusCode, 'Edit Tax Save');
				}
			} else {
				ToastHelper.error('Please enter valid tax rate.');
			}
			this.toggleTaxRateModal();
		}
	}, {
		key: 'handleTaxListStatus',
		value: function handleTaxListStatus(isSuccess, statusCode, eventName) {
			if (isSuccess) {
				if (eventName) MyAnalytics.pushEvent(eventName);
				this.refreshRateList();
				ToastHelper.success(statusCode);
			} else {
				ToastHelper.error(statusCode);
			}
		}
	}, {
		key: 'saveTaxGroup',
		value: function saveTaxGroup() {
			var _state = this.state,
			    listOfTaxCodeId = _state.listOfTaxCodeId,
			    taxGroupName = _state.taxGroupName;

			var isEditForm = this.taxGroupEditForm;
			listOfTaxCodeId = listOfTaxCodeId ? (0, _keys2.default)(listOfTaxCodeId).map(Number) : [];
			if (taxGroupName && listOfTaxCodeId.length > 0) {
				var taxCodeObject = !isEditForm ? new _TaxCode2.default() : this.taxCodeCache.getTaxCodeObjectById(this.taxGroupId);

				if (isEditForm && !taxCodeObject.canUpdateTaxRate(false)) {
					return;
				}

				var statusCode = '';
				taxCodeObject.setTaxCodeName(taxGroupName);
				taxCodeObject.setTaxType(_TaxCodeConstants2.default.taxGroup);
				taxCodeObject.setTaxRateType(null);
				taxCodeObject.setTaxCodeMap(listOfTaxCodeId);

				if (!isEditForm) {
					statusCode = taxCodeObject.addNewTaxRate();
					this.handleTaxListStatus(statusCode == _ErrorCode2.default.ERROR_TAX_GROUP_NAME_SAVE_SUCCESS, statusCode, 'Add Tax Group Save');
				} else {
					statusCode = taxCodeObject.updateTaxRate();
					this.handleTaxListStatus(statusCode == _ErrorCode2.default.ERROR_TAX_GROUP_NAME_EDIT_SUCCESS, statusCode, 'Edit Tax Group Save');
				}
			} else {
				ToastHelper.error('fields cannot be empty');
				return;
			}
			this.toggleTaxGroupModal();
		}
	}, {
		key: 'deleteTaxGroup',
		value: function deleteTaxGroup(taxItem) {
			if (!taxItem) {
				ToastHelper.error(_ErrorCode2.default.ERROR_TAX_CODE_NAME_DELETE_FAILED);
				return;
			}
			if (taxItem && taxItem.canDeleteTaxRate()) {
				var statusCode = taxItem.deleteTaxRate();
				this.handleTaxListStatus(statusCode == _ErrorCode2.default.ERROR_TAX_GROUP_NAME_DELETE_SUCCESS, statusCode);
			}
		}
	}, {
		key: 'deleteTaxRate',
		value: function deleteTaxRate(taxItem) {
			if (!taxItem) {
				ToastHelper.error(_ErrorCode2.default.ERROR_TAX_DELETE_EDIT_FAILED);
				return;
			}
			if (taxItem.getTaxRateType() && MyDouble.convertStringToDouble(taxItem.getTaxRateType(), true) == _TaxCodeConstants2.default.Exempted) {
				ToastHelper.error('Exempted Tax rate can not be deleted.');
			} else if (taxItem.canDeleteTaxRate()) {
				var statusCode = taxItem.deleteTaxRate();
				this.handleTaxListStatus(statusCode == _ErrorCode2.default.ERROR_TAX_CODE_NAME_DELETE_SUCCESS, statusCode);
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var _this6 = this;

			var classes = this.props.classes;

			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement(
					'div',
					{ className: 'gst-settings' },
					_react2.default.createElement(
						'div',
						{ className: 'row' },
						this.settingCache.isCurrentCountryIndia() && _react2.default.createElement(
							'div',
							{ className: 'col-4' },
							_react2.default.createElement(
								'span',
								{ className: 'heading font-family-bold font-16' },
								'GST Settings'
							),
							_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
							_react2.default.createElement(
								'div',
								{ className: 'setting-item d-flex align-items-center ' + _Queries2.default.SETTING_GST_ENABLED },
								_react2.default.createElement(
									'label',
									{ className: 'input-checkbox' },
									_react2.default.createElement('input', { type: 'checkbox', onChange: this.toggleCheckbox(_Queries2.default.SETTING_GST_ENABLED, true, this.enableGSTCallback), checked: this.state[_Queries2.default.SETTING_GST_ENABLED] }),
									this.checkboxJSX
								),
								_react2.default.createElement(
									'span',
									{ className: '' },
									_SettingsMapping2.default.miscGSTSettings[_Queries2.default.SETTING_GST_ENABLED].heading
								),
								this.renderToolTip(_SettingsMapping2.default.miscGSTSettings[_Queries2.default.SETTING_GST_ENABLED], _Queries2.default.SETTING_GST_ENABLED)
							),
							this.state[_Queries2.default.SETTING_GST_ENABLED] && _react2.default.createElement(
								_react2.default.Fragment,
								null,
								(0, _keys2.default)(this.gstSettingsList).map(function (key) {
									return _react2.default.createElement(
										'div',
										{ key: key, className: 'setting-item d-flex align-items-center ' + key },
										_react2.default.createElement(
											'label',
											{ className: 'input-checkbox' },
											_react2.default.createElement('input', { type: 'checkbox', onChange: _this6.toggleCheckbox(key), checked: _this6.state[key] }),
											_this6.checkboxJSX
										),
										_react2.default.createElement(
											'span',
											{ className: '' },
											_this6.gstSettingsList[key].heading
										),
										_this6.renderToolTip(_this6.gstSettingsList[key], key)
									);
								}),
								_react2.default.createElement(
									'div',
									{ className: 'setting-item d-flex align-items-center ' + _Queries2.default.SETTING_IS_COMPOSITE_SCHEME_ENABLED },
									_react2.default.createElement(
										'label',
										{ className: 'input-checkbox' },
										_react2.default.createElement('input', { type: 'checkbox', onChange: this.toggleCheckbox(_Queries2.default.SETTING_IS_COMPOSITE_SCHEME_ENABLED), checked: this.state[_Queries2.default.SETTING_IS_COMPOSITE_SCHEME_ENABLED] }),
										this.checkboxJSX
									),
									_react2.default.createElement(
										'div',
										{ className: 'd-flex align-items-center' },
										_react2.default.createElement(
											'span',
											null,
											_SettingsMapping2.default.miscGSTSettings[_Queries2.default.SETTING_IS_COMPOSITE_SCHEME_ENABLED].heading
										),
										this.renderToolTip(_SettingsMapping2.default.miscGSTSettings[_Queries2.default.SETTING_IS_COMPOSITE_SCHEME_ENABLED], _Queries2.default.SETTING_IS_COMPOSITE_SCHEME_ENABLED)
									),
									this.state[_Queries2.default.SETTING_IS_COMPOSITE_SCHEME_ENABLED] && _react2.default.createElement(
										_SelectDropDown2.default,
										{ className: classes.compositeScheme, name: 'compositeScheme', handleChange: this.saveSettings.bind(this, _Queries2.default.SETTING_COMPOSITE_USER_TYPE), value: this.listOfSettings[_Queries2.default.SETTING_COMPOSITE_USER_TYPE] },
										this.compositeSchemeList.map(function (item, index) {
											return _react2.default.createElement(
												_MenuItem2.default,
												{ key: item.key, value: item.key },
												item.value
											);
										})
									)
								)
							),
							_react2.default.createElement(
								'div',
								{ className: 'setting-item d-flex align-items-center' },
								_react2.default.createElement(
									'a',
									{ onClick: this.toggleTaxListView, className: 'settings-btn-link' },
									'Tax List >'
								)
							)
						),
						this.state.showTaxListView && _react2.default.createElement(
							_react2.default.Fragment,
							null,
							_react2.default.createElement(
								'div',
								{ className: 'col-4' },
								_react2.default.createElement(
									'div',
									{ className: 'font-family-bold font-16 d-flex align-items-center' },
									_react2.default.createElement(
										'span',
										null,
										'Tax Rates'
									),
									_react2.default.createElement('img', { className: 'ml-5', onClick: this.toggleTaxRateModal, src: '../inlineSVG/circular_add.svg' })
								),
								_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
								_react2.default.createElement(
									'div',
									{ className: classes.taxRateList + ' tax-rate-list mt-20' },
									this.state.taxRateJSX
								)
							),
							_react2.default.createElement(
								'div',
								{ className: 'col-4' },
								_react2.default.createElement(
									'div',
									{ className: 'font-family-bold font-16 d-flex align-items-center' },
									_react2.default.createElement(
										'span',
										null,
										'Tax Group'
									),
									_react2.default.createElement('img', { className: 'ml-5', onClick: this.toggleTaxGroupModal, src: '../inlineSVG/circular_add.svg' })
								),
								_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
								_react2.default.createElement(
									'div',
									{ className: classes.taxGroupList + ' tax-group-list mt-20' },
									this.state.taxGroupJSX
								)
							)
						)
					)
				),
				_react2.default.createElement(
					'div',
					null,
					this.state.openTaxGroupModal && this.getTaxGroupModal()
				),
				_react2.default.createElement(
					'div',
					null,
					this.state.openTaxRateModal && this.getTaxRateModal()
				)
			);
		}
	}]);
	return GSTSettings;
}(_BaseSettings3.default);

exports.default = (0, _styles.withStyles)(styles)(GSTSettings);