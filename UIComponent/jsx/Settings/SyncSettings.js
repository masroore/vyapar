Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/core/styles');

var _SettingCache = require('../../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

var _SyncHandler = require('../../../Utilities/SyncHandler');

var _SyncHandler2 = _interopRequireDefault(_SyncHandler);

var _SqliteDBHelper = require('../../../DBManager/SqliteDBHelper');

var _SqliteDBHelper2 = _interopRequireDefault(_SqliteDBHelper);

var _BaseSettings2 = require('./BaseSettings');

var _BaseSettings3 = _interopRequireDefault(_BaseSettings2);

var _TokenForSync = require('../../../Utilities/TokenForSync');

var _TokenForSync2 = _interopRequireDefault(_TokenForSync);

var _SettingsMapping = require('./SettingsMapping');

var _SettingsMapping2 = _interopRequireDefault(_SettingsMapping);

var _SyncUtilityHelper = require('./SyncUtilityHelper');

var _SyncUtilityHelper2 = _interopRequireDefault(_SyncUtilityHelper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import blueTheme from '../../../themes';
var styles = function styles(theme) {
	return {
		checkBoxLabel: {
			fontFamily: 'RobotoMedium'
		},
		container: {
			paddingLeft: 25,
			paddingRight: 25,
			width: '50%',
			minHeight: '60vh'
		},
		content: {
			paddingLeft: 35
		},
		smallText: {
			fontSize: 12,
			marginBottom: 10
		},
		highlight: {
			fontSize: 12,
			fontWeight: 'bold'
		},
		divider: {
			borderRight: '1px solid #BDBDBD',
			margin: '10px auto'
		},
		hide: {
			visibility: 'hidden'
		}
	};
};

var SyncSettingContainer = function (_BaseSettings) {
	(0, _inherits3.default)(SyncSettingContainer, _BaseSettings);

	function SyncSettingContainer(props) {
		(0, _classCallCheck3.default)(this, SyncSettingContainer);

		var _this = (0, _possibleConstructorReturn3.default)(this, (SyncSettingContainer.__proto__ || (0, _getPrototypeOf2.default)(SyncSettingContainer)).call(this, props));

		_this.enableDisableSync = function () {
			_this.setState({
				syncEnabled: !_this.state.syncEnabled
			}, function () {
				_SyncUtilityHelper2.default.enableDisableSync(_this.state.syncEnabled, _this.enableDisableSyncCallback);
			});
		};

		_this.enableDisableSyncCallback = function (syncEnabledStatus) {
			_this.setState({
				syncEnabled: syncEnabledStatus
			});
		};

		window.globalErrorMessage = '';
		_this.token = _this.getToken();
		_this.token = _this.token ? _this.token.auth_token : null;
		_this.state = (0, _extends3.default)({
			isLoggedIn: _this.token ? true : false
		}, _this.syncSettingsState());
		_this.onResume = _this.onResume.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(SyncSettingContainer, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.higlightSearchedSetting();
		}
	}, {
		key: 'syncSettingsState',
		value: function syncSettingsState() {
			var sqliteDBHelper = new _SqliteDBHelper2.default();
			return {
				syncEnabled: _SyncHandler2.default.isSyncEnabled(),
				globId: sqliteDBHelper.fetchCompanyGlobalId()
			};
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {}
	}, {
		key: 'getToken',
		value: function getToken() {
			try {
				var token = _TokenForSync2.default.readToken();
				return token ? token : false;
			} catch (err) {
				return false;
			}
		}
	}, {
		key: 'onResume',
		value: function onResume() {
			var settingCache = new _SettingCache2.default();
			if (this.state.value === 1 && !settingCache.isPartyGroupEnabled()) {
				this.setState({
					value: 0
				});
			}
		}
	}, {
		key: 'manageSyncUserPermissions',
		value: function manageSyncUserPermissions() {
			_SyncUtilityHelper2.default.syncPermissionManager();
		}
	}, {
		key: 'render',
		value: function render() {
			var classes = this.props.classes;
			var syncEnabled = this.state.syncEnabled;


			return _react2.default.createElement(
				'div',
				{ className: 'd-flex flex-row' },
				_react2.default.createElement(
					'div',
					{ className: classes.container },
					_react2.default.createElement(
						'div',
						{ className: 'setting-item d-flex align-items-center syncEnabled' },
						_react2.default.createElement(
							'label',
							{ className: 'input-checkbox' },
							_react2.default.createElement('input', { type: 'checkbox', onChange: this.enableDisableSync, checked: this.state['syncEnabled'] }),
							this.checkboxJSX
						),
						_react2.default.createElement(
							'span',
							{ className: '' },
							_SettingsMapping2.default.multiDeviceSync.syncEnabled.heading
						)
					),
					_react2.default.createElement(
						'div',
						{ className: classes.content },
						_react2.default.createElement(
							'div',
							{ className: classes.smallText },
							_react2.default.createElement(
								'p',
								null,
								'Share and maintain your business data among multiple mobiles and laptops of your choice.'
							),
							_react2.default.createElement(
								'p',
								null,
								'All the data entry will be shared instantly among all devices'
							)
						),
						_react2.default.createElement(
							'div',
							{ className: classes.highlight + ' d-flex' },
							_react2.default.createElement(
								'span',
								null,
								'Note:'
							),
							_react2.default.createElement(
								'span',
								{ className: 'ml-5' },
								'You need to be logged in to Vyapar to share data between different devices'
							)
						)
					),
					syncEnabled && _react2.default.createElement(
						'div',
						{ className: 'setting-item d-flex align-items-center mt-10' },
						_react2.default.createElement(
							'a',
							{ onClick: this.manageSyncUserPermissions, className: 'settings-btn-link' },
							'Manage Permissions'
						)
					)
				)
			);
		}
	}]);
	return SyncSettingContainer;
}(_BaseSettings3.default);

SyncSettingContainer.propTypes = {
	classes: _propTypes2.default.object.isRequired
};

exports.default = (0, _styles.withStyles)(styles, { withTheme: true })(SyncSettingContainer);