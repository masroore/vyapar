Object.defineProperty(exports, "__esModule", {
	value: true
});

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _BaseSettings2 = require('./BaseSettings');

var _BaseSettings3 = _interopRequireDefault(_BaseSettings2);

var _TextField = require('@material-ui/core/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _Switch = require('@material-ui/core/Switch');

var _Switch2 = _interopRequireDefault(_Switch);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _UDFFieldsConstant = require('../../../Constants/UDFFieldsConstant');

var _UDFFieldsConstant2 = _interopRequireDefault(_UDFFieldsConstant);

var _UDFCache = require('../../../Cache/UDFCache');

var _UDFCache2 = _interopRequireDefault(_UDFCache);

var _UDFModel = require('../../../Models/UDFModel');

var _UDFModel2 = _interopRequireDefault(_UDFModel);

var _SelectDropDown = require('../UIControls/SelectDropDown');

var _SelectDropDown2 = _interopRequireDefault(_SelectDropDown);

var _MenuItem = require('@material-ui/core/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _ErrorCode = require('../../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

var _SettingsMapping = require('./SettingsMapping');

var _SettingsMapping2 = _interopRequireDefault(_SettingsMapping);

var _Queries = require('../../../Constants/Queries');

var _Queries2 = _interopRequireDefault(_Queries);

var _NumberTextField = require('../UIControls/NumberTextField');

var _NumberTextField2 = _interopRequireDefault(_NumberTextField);

var _styles = require('@material-ui/core/styles');

var _Modal = require('../Modal');

var _Modal2 = _interopRequireDefault(_Modal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
	return {
		additionalFields: {
			'& .setting-item': {
				height: '100px'
			}
		}
	};
};

var PartySettings = function (_BaseSettings) {
	(0, _inherits3.default)(PartySettings, _BaseSettings);

	function PartySettings(props) {
		(0, _classCallCheck3.default)(this, PartySettings);

		var _this = (0, _possibleConstructorReturn3.default)(this, (PartySettings.__proto__ || (0, _getPrototypeOf2.default)(PartySettings)).call(this, props));

		_this.handleUdfTextFieldFocusOut = function (fieldStatus) {
			var udfValues = _this.saveUdf();
			_this.setState({
				udfValues: udfValues
			});
			if (!event.target.value) {
				_this.lastBlurEventTriggerTimestamp = Date.now();
				_this.lastBlurEventTriggeredOn = fieldStatus;
			}
		};

		_this.toggleReminderModalPopup = function () {
			_this.setState((0, _defineProperty3.default)({
				openReminderMessageModalPopup: !_this.state.openReminderMessageModalPopup
			}, _Queries2.default.SETTING_CUSTOM_PAYMENT_REMINDER_MESSAGE, _this.settingCache.getPaymentReminderMessage()));
		};

		_this.saveReminderMessage = function () {
			var StringConstants = require('../../../Constants/StringConstants');
			var balanceAmountRegex = StringConstants.balanceAmountRegex;
			var value = _this.state[_Queries2.default.SETTING_CUSTOM_PAYMENT_REMINDER_MESSAGE];
			if (balanceAmountRegex.test(value)) {
				_this.saveSettings(_Queries2.default.SETTING_CUSTOM_PAYMENT_REMINDER_MESSAGE, value);
				_this.toggleReminderModalPopup();
			} else {
				ToastHelper.error(StringConstants.BALANCE_AMOUNT_TAG_MISSING_IN_MESSAGE);
			}
		};

		_this.resetDefaultReminderMessage = function () {
			var StringConstants = require('../../../Constants/StringConstants');
			_this.setState((0, _defineProperty3.default)({}, _Queries2.default.SETTING_CUSTOM_PAYMENT_REMINDER_MESSAGE, StringConstants.DEFAULT_REMINDER_MESSAGE));
		};

		_this.getOpenReminderMessageModalPopup = function () {
			return _react2.default.createElement(
				_Modal2.default,
				{
					height: '230px',
					width: '450px',
					isOpen: _this.state.openReminderMessageModalPopup,
					onClose: _this.toggleReminderModalPopup,
					canHideHeader: 'true',
					title: 'Add/Edit Reminder Message',
					parentClassSelector: 'remider-message-modal-container'
				},
				_react2.default.createElement(
					'div',
					{ className: 'modalBody' },
					_react2.default.createElement(
						'div',
						{ className: 'reminder-message-form' },
						_react2.default.createElement(_TextField2.default, {
							label: 'Reminder Message',
							margin: 'dense',
							variant: 'outlined',
							className: 'width100',
							value: _this.state[_Queries2.default.SETTING_CUSTOM_PAYMENT_REMINDER_MESSAGE],
							onChange: _this.updateInputChange(_Queries2.default.SETTING_CUSTOM_PAYMENT_REMINDER_MESSAGE, false),
							multiline: true,
							rows: 4
						}),
						_react2.default.createElement(
							'div',
							{ className: 'floatRight mt-10' },
							_react2.default.createElement(
								_Button2.default,
								{ margin: 'dense', className: 'mr-10',
									onClick: _this.toggleReminderModalPopup, variant: 'contained', color: 'primary' },
								' Cancel'
							),
							_react2.default.createElement(
								_Button2.default,
								{ margin: 'dense', className: 'mr-10',
									onClick: _this.resetDefaultReminderMessage, variant: 'contained', color: 'primary' },
								' Reset Default'
							),
							_react2.default.createElement(
								_Button2.default,
								{ margin: 'dense', onClick: _this.saveReminderMessage, variant: 'contained', color: 'primary' },
								' Save'
							)
						)
					)
				)
			);
		};

		_this.listOfFormat = [{ key: 1, value: 'dd/mm/yy' }, { key: 2, value: 'mm/yyyy' }];
		_this.initUDF();
		_this.state = (0, _extends3.default)({}, _this.partySettingsState());

		_this.partySettingsList = _SettingsMapping2.default.partySettingsList;
		_this.udfTextFieldRefs = _this.createUdfTextFieldRefs();
		_this.lastBlurEventTriggerTimestamp = '';
		_this.lastBlurEventTriggeredOn = '';
		return _this;
	}

	(0, _createClass3.default)(PartySettings, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.higlightSearchedSetting();
			this.initializeToolTips();
		}
	}, {
		key: 'createUdfTextFieldRefs',
		value: function createUdfTextFieldRefs() {
			var refs = {};
			for (var index = 0; index < _UDFFieldsConstant2.default.TOTAL_EXTRA_FIELDS; index++) {
				refs['fieldName' + (index + 1) + 'Ref'] = _react2.default.createRef();
			}
			return refs;
		}
	}, {
		key: 'partySettingsState',
		value: function partySettingsState() {
			var _ref;

			return _ref = {}, (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PARTY_GROUP, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PARTY_GROUP])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PARTY_SHIPPING_ADDRESS_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PARTY_SHIPPING_ADDRESS_ENABLED])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PRINT_PARTY_SHIPPING_ADDRESS, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_PARTY_SHIPPING_ADDRESS])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PAYMENTREMIDNER_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PAYMENTREMIDNER_ENABLED])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PAYMENT_REMINDER_DAYS, this.listOfSettings[_Queries2.default.SETTING_PAYMENT_REMINDER_DAYS]), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_CUSTOM_PAYMENT_REMINDER_MESSAGE, this.settingCache.getPaymentReminderMessage()), (0, _defineProperty3.default)(_ref, 'udfValues', this.UDF), (0, _defineProperty3.default)(_ref, 'openReminderMessageModalPopup', false), _ref;
		}
	}, {
		key: 'resetState',
		value: function resetState() {
			this.initUDF();
			this.setState((0, _extends3.default)({}, this.partySettingsState()));
		}
	}, {
		key: 'initUDF',
		value: function initUDF() {
			var _this2 = this;

			var udfValues = {};
			this.uDFCache = new _UDFCache2.default();
			this.udfPartyFieldsMap = new _map2.default(this.uDFCache.getPartyFieldsMap());
			this.udfPartyFieldsMap.forEach(function (value, key) {
				var udfItem = value;var i = udfItem.getUdfFieldNumber();
				udfValues['fieldStatus' + i] = _this2.isChecked(udfItem.getUdfFieldStatus());
				udfValues['fieldName' + i] = udfItem.getUdfFieldName();
				udfValues['fieldShowInPrint' + i] = _this2.isChecked(udfItem.getUdfFieldPrintOnInvoice());
				if (parseInt(udfItem.getUdfFieldDataFormat()) !== 0) {
					var dateFormatObj = _this2.listOfFormat.filter(function (item) {
						return item.key === parseInt(udfItem.getUdfFieldDataFormat());
					})[0];
					udfValues.selectedDateFormatVal = dateFormatObj.key;
				}
			});
			this.UDF = udfValues;
		}
	}, {
		key: 'toggleUdfCheckbox',
		value: function toggleUdfCheckbox() {
			var _this3 = this;

			var setting = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

			if (Date.now() - this.lastBlurEventTriggerTimestamp < 200 && setting == this.lastBlurEventTriggeredOn) {
				return;
			}
			var settingValue = !this.state.udfValues[setting];
			var udfValues = (0, _extends3.default)({}, this.state.udfValues);
			udfValues[setting] = settingValue;
			this.setState({
				udfValues: udfValues
			}, function () {
				_this3.saveUdf();
				if (setting.startsWith('fieldStatus')) {
					var fieldNum = setting.replace('fieldStatus', '');
					var fieldNameRef = _this3.udfTextFieldRefs['fieldName' + fieldNum + 'Ref'];
					if (udfValues[setting] && fieldNameRef && fieldNameRef.current) {
						fieldNameRef.current.focus();
					}
				}
			});
		}
	}, {
		key: 'handleUdfDdChange',
		value: function handleUdfDdChange(value) {
			var udfValues = (0, _extends3.default)({}, this.state.udfValues);
			udfValues['selectedDateFormatVal'] = value;
			this.setState({
				udfValues: udfValues
			});
		}
	}, {
		key: 'updateUdfInputChange',
		value: function updateUdfInputChange(key) {
			var udfValues = (0, _extends3.default)({}, this.state.udfValues);
			udfValues[key] = event.target.value;
			this.setState({
				udfValues: udfValues
			});
		}
	}, {
		key: 'renderUdf',
		value: function renderUdf() {
			var udf = [];
			for (var i = 1; i <= _UDFFieldsConstant2.default.TOTAL_EXTRA_FIELDS; i++) {
				var label = 'Additional Field ' + i;
				if (this.state.udfValues['fieldStatus' + i] === undefined) {
					var udfModel = new _UDFModel2.default();
					udfModel.setUdfFieldType(_UDFFieldsConstant2.default.FIELD_TYPE_PARTY);
					udfModel.setUdfFirmId(1);
					udfModel.setUdfFieldNuber(i);
					this.udfPartyFieldsMap.set({ key: 'Party_Field_' + i, value: udfModel });
				}
				udf.push(_react2.default.createElement(
					'div',
					{ key: i, className: 'setting-item' },
					_react2.default.createElement(
						'div',
						null,
						_react2.default.createElement(
							'div',
							{ className: 'd-flex align-items-center' },
							_react2.default.createElement(
								'label',
								{ className: 'input-checkbox' },
								_react2.default.createElement('input', { type: 'checkbox',
									onChange: this.toggleUdfCheckbox.bind(this, 'fieldStatus' + i),
									checked: this.state.udfValues['fieldStatus' + i] }),
								this.checkboxJSX
							),
							_react2.default.createElement(
								_react2.default.Fragment,
								null,
								_react2.default.createElement(_TextField2.default, {
									disabled: !this.state.udfValues['fieldStatus' + i],
									label: label,
									margin: 'dense',
									variant: 'outlined',
									className: i === _UDFFieldsConstant2.default.FIELD_FOR_DATE ? '' : 'width100',
									value: this.state.udfValues['fieldName' + i] || '',
									onChange: this.updateUdfInputChange.bind(this, 'fieldName' + i),
									inputRef: this.udfTextFieldRefs['fieldName' + i + 'Ref'],
									inputProps: {
										maxLength: 30
									},
									onBlur: this.handleUdfTextFieldFocusOut.bind(this, 'fieldStatus' + i)
								}),
								i === _UDFFieldsConstant2.default.FIELD_FOR_DATE && _react2.default.createElement(
									_SelectDropDown2.default,
									{ outlined: true, isDisabled: this.settingCache.isCurrentCountryNepal() || !this.state.udfValues['fieldStatus' + i],
										handleChange: this.handleUdfDdChange.bind(this),
										value: this.state.udfValues.selectedDateFormatVal || this.listOfFormat[0].key, name: 'dateFormat', id: 'dateFormat' },
									this.listOfFormat.map(function (format, index) {
										return _react2.default.createElement(
											_MenuItem2.default,
											{ key: format.key, value: format.key },
											format.value
										);
									})
								)
							)
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'floatRight' },
						_react2.default.createElement(_Switch2.default, {
							disabled: !this.state.udfValues['fieldStatus' + i],
							checked: this.state.udfValues['fieldShowInPrint' + i],
							value: 'checkedB',
							color: 'primary',
							onChange: this.toggleUdfCheckbox.bind(this, 'fieldShowInPrint' + i)
						}),
						_react2.default.createElement(
							'span',
							null,
							'Show In Print'
						)
					)
				));
				// }
				// else {
				// 	udf.push(<div key={i} className="setting-item">
				// 		<div className="d-flex align-items-center">
				// 			<label className='input-checkbox'>
				// 				<input type='checkbox' onChange={this.toggleUdfCheckbox.bind(this, 'fieldStatus' + i)} checked={this.state.udfValues['fieldStatus' + i]} />
				// 				{this.checkboxJSX}
				// 			</label>
				// 			<TextField
				// 				disabled={!this.state.udfValues['fieldStatus' + i]}
				// 				id="outlined-dense"
				// 				label={label}
				// 				className="width100"
				// 				margin="dense"
				// 				variant="outlined"
				// 				value={this.state.udfValues['fieldName' + i] || ''}
				// 				onChange={this.updateUdfInputChange.bind(this, 'fieldName' + i)}
				// 			/>
				// 		</div>
				// 		<div className="floatRight">
				// 			<Switch
				// 				disabled={!this.state.udfValues['fieldStatus' + i]}
				// 				checked={this.state.udfValues['fieldShowInPrint' + i]}
				// 				value="checkedB"
				// 				color="primary"
				// 				onChange={this.toggleUdfCheckbox.bind(this, 'fieldShowInPrint' + i)}
				// 			/><span>Show In Print</span>
				// 		</div>
				// 	</div>
				// 	);
				// }
			}
			return _react2.default.createElement(
				_react2.default.Fragment,
				{ key: 1 },
				udf
			);
		}
	}, {
		key: 'saveUdf',
		value: function saveUdf() {
			var udfValues = this.state.udfValues;

			udfValues = (0, _extends3.default)({}, udfValues);
			var index = 0;
			this.udfPartyFieldsMap.forEach(function (value, key) {
				var udfItem = value;
				var i = udfItem.getUdfFieldNumber();
				var udfTextValue = udfValues['fieldName' + i];
				udfValues['fieldStatus' + i] = udfValues['fieldStatus' + i] && !!udfTextValue;
				var isUdfFieldEnabled = udfValues['fieldStatus' + i];
				index = index + 1;
				udfItem.setUdfFieldStatus(isUdfFieldEnabled ? 1 : 0);
				udfItem.setUdfFieldName(udfTextValue);
				udfItem.setUdfFieldPrintOnInvoice(udfValues['fieldShowInPrint' + i] ? 1 : 0);
				udfItem.setUdfFieldType(_UDFFieldsConstant2.default.FIELD_TYPE_PARTY);
				if (index === _UDFFieldsConstant2.default.FIELD_FOR_DATE) {
					udfItem.setUdfFieldDataFormat(udfValues['selectedDateFormatVal'] || 1);
					udfItem.setUdfFieldDataType(_UDFFieldsConstant2.default.DATA_TYPE_DATE);
				}
				udfItem.setUdfFirmId(1);
				udfItem.setUdfFieldNuber(index);
			});
			var statusCode = this.dataInserter.insertUdfData(this.udfPartyFieldsMap);
			if (statusCode === _ErrorCode2.default.ERROR_SETTING_SAVE_SUCCESS) {
				ToastHelper.success(statusCode);
			} else {
				ToastHelper.error(statusCode);
			}
			return udfValues;
		}
	}, {
		key: 'render',
		value: function render() {
			var _this4 = this;

			var classes = this.props.classes;

			return _react2.default.createElement(
				_react2.default.Fragment,
				{ key: 1 },
				_react2.default.createElement(
					'div',
					{ className: 'party-settings' },
					_react2.default.createElement(
						'div',
						{ className: 'row' },
						_react2.default.createElement(
							'div',
							{ className: 'col mb-20' },
							_react2.default.createElement(
								'span',
								{ className: 'heading font-family-bold font-16' },
								'Party Settings'
							),
							_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
							(0, _keys2.default)(this.partySettingsList).map(function (key, index) {
								return _react2.default.createElement(
									_react2.default.Fragment,
									{ key: index },
									_react2.default.createElement(
										'div',
										{ className: 'setting-item d-flex align-items-center ' + key },
										_react2.default.createElement(
											'label',
											{ className: 'input-checkbox' },
											_react2.default.createElement('input', { type: 'checkbox', onChange: _this4.toggleCheckbox(key), checked: _this4.state[key] }),
											_this4.checkboxJSX
										),
										_react2.default.createElement('span', { dangerouslySetInnerHTML: { __html: _this4.partySettingsList[key].heading } }),
										_this4.renderToolTip(_this4.partySettingsList[key], key)
									),
									key === _Queries2.default.SETTING_PARTY_SHIPPING_ADDRESS_ENABLED && _react2.default.createElement(
										_react2.default.Fragment,
										null,
										_this4.state[_Queries2.default.SETTING_PARTY_SHIPPING_ADDRESS_ENABLED] && _react2.default.createElement(
											'div',
											{ className: 'setting-item d-flex align-items-center ' + _Queries2.default.SETTING_PRINT_PARTY_SHIPPING_ADDRESS },
											_react2.default.createElement(
												'label',
												{ className: 'input-checkbox' },
												_react2.default.createElement('input', { type: 'checkbox', onChange: _this4.toggleCheckbox(_Queries2.default.SETTING_PRINT_PARTY_SHIPPING_ADDRESS), checked: _this4.state[_Queries2.default.SETTING_PRINT_PARTY_SHIPPING_ADDRESS] }),
												_this4.checkboxJSX
											),
											_react2.default.createElement('span', { dangerouslySetInnerHTML: { __html: _SettingsMapping2.default.miscPartySettingsList[_Queries2.default.SETTING_PRINT_PARTY_SHIPPING_ADDRESS].heading } }),
											_this4.renderToolTip(_SettingsMapping2.default.miscPartySettingsList[_Queries2.default.SETTING_PRINT_PARTY_SHIPPING_ADDRESS])
										)
									),
									key === _Queries2.default.SETTING_PAYMENTREMIDNER_ENABLED && _react2.default.createElement(
										_react2.default.Fragment,
										null,
										_this4.state[_Queries2.default.SETTING_PAYMENTREMIDNER_ENABLED] && _react2.default.createElement(
											_react2.default.Fragment,
											null,
											_react2.default.createElement(
												'div',
												{ className: 'setting-item height-72 d-flex align-items-center ' + _Queries2.default.SETTING_PAYMENT_REMINDER_DAYS },
												_react2.default.createElement(
													'div',
													{ className: 'setting-heading d-flex align-items-center  mr-10' },
													_react2.default.createElement('span', { dangerouslySetInnerHTML: { __html: _SettingsMapping2.default.miscPartySettingsList[_Queries2.default.SETTING_PAYMENT_REMINDER_DAYS].heading } }),
													_this4.renderToolTip(_SettingsMapping2.default.miscPartySettingsList[_Queries2.default.SETTING_PAYMENT_REMINDER_DAYS])
												),
												_react2.default.createElement(_NumberTextField2.default, {
													ref: _this4.decimalFieldRef,
													maxLength: 2,
													minValue: 1,
													maxValue: 99,
													handleBlur: _this4.handleNumber(_Queries2.default.SETTING_PAYMENT_REMINDER_DAYS),
													width: 100,
													value: _this4.state[_Queries2.default.SETTING_PAYMENT_REMINDER_DAYS],
													className: 'd-inline-flex justify-content-center w-20-px'
												}),
												_react2.default.createElement(
													'span',
													{ className: 'font-12 ml-5' },
													'(days)'
												)
											),
											_react2.default.createElement(
												'div',
												{ className: 'setting-item' },
												_react2.default.createElement(
													'button',
													{ onClick: _this4.toggleReminderModalPopup, className: 'settings-btn-link' },
													'Reminder Message >'
												)
											),
											_react2.default.createElement('div', null)
										)
									)
								);
							})
						),
						_react2.default.createElement(
							'div',
							{ className: classes.additionalFields + ' col' },
							_react2.default.createElement(
								'div',
								{ className: 'd-flex align-items-center' },
								_react2.default.createElement(
									'div',
									{ className: 'd-flex align-items-center' },
									_react2.default.createElement(
										'span',
										{ className: 'font-family-bold font-16' },
										_SettingsMapping2.default.miscPartySettingsList.PARTY_ADDITIONAL_FIELDS.heading
									),
									this.renderToolTip(_SettingsMapping2.default.miscPartySettingsList.PARTY_ADDITIONAL_FIELDS)
								)
							),
							_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
							this.renderUdf()
						),
						_react2.default.createElement('div', { className: 'col' })
					),
					_react2.default.createElement(
						'div',
						{ className: 'row' },
						_react2.default.createElement('div', { className: 'col' }),
						_react2.default.createElement('div', { className: 'col' }),
						_react2.default.createElement('div', { className: 'col' })
					)
				),
				_react2.default.createElement(
					'div',
					null,
					this.state.openReminderMessageModalPopup && this.getOpenReminderMessageModalPopup()
				)
			);
		}
	}]);
	return PartySettings;
}(_BaseSettings3.default);

exports.default = (0, _styles.withStyles)(styles)(PartySettings);