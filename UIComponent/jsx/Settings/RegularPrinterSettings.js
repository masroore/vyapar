Object.defineProperty(exports, "__esModule", {
	value: true
});

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _extends3 = require('babel-runtime/helpers/extends');

var _extends4 = _interopRequireDefault(_extends3);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _BasePrintSettings2 = require('./BasePrintSettings');

var _BasePrintSettings3 = _interopRequireDefault(_BasePrintSettings2);

var _Queries = require('../../../Constants/Queries');

var _Queries2 = _interopRequireDefault(_Queries);

var _NumberTextField = require('../UIControls/NumberTextField');

var _NumberTextField2 = _interopRequireDefault(_NumberTextField);

var _styles = require('@material-ui/core/styles');

var _SelectDropDown = require('../UIControls/SelectDropDown');

var _SelectDropDown2 = _interopRequireDefault(_SelectDropDown);

var _MenuItem = require('@material-ui/core/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _BaseSettings = require('./BaseSettings');

var _Tabs = require('@material-ui/core/Tabs');

var _Tabs2 = _interopRequireDefault(_Tabs);

var _Tab = require('@material-ui/core/Tab');

var _Tab2 = _interopRequireDefault(_Tab);

var _Modal = require('../Modal');

var _Modal2 = _interopRequireDefault(_Modal);

var _ErrorCode = require('../../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

var _InvoiceTheme = require('../../../Constants/InvoiceTheme');

var _InvoiceTheme2 = _interopRequireDefault(_InvoiceTheme);

var _SettingsMapping = require('./SettingsMapping');

var _SettingsMapping2 = _interopRequireDefault(_SettingsMapping);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
	return {
		leftSeparator: {
			borderLeft: '2px solid #DDDDDD'
		},
		settingsColumn: {
			height: 'calc(100vh - 100px)',
			overflow: 'auto'
		},
		themePreviewContainer: {
			height: 'calc(100vh - 100px)',
			overflow: 'auto',
			'& .theme-preview': {
				backgroundColor: '#ffffff',
				padding: '10px',
				boxShadow: '2px 2px 2px #dddddd',
				border: '1px solid #dddddd',
				minHeight: '100%'
			}
		},
		'@global': {
			'.right-content-pane': {
				overflow: 'hidden !important'
			},
			'.border-white-box': {
				border: '1px solid #dddddd'
			},
			'.option-color-box': {
				borderRadius: '12px',
				width: '22px',
				height: '22px',
				display: 'inline-block',
				marginRight: '10px',
				marginTop: '16px',
				cursor: 'pointer'
			},
			'.selected-color-box': {
				border: '3px solid #ecec0f'
			},
			'.item-table-customization .modalContainer': {
				maxHeight: 'none',
				maxWidth: 'none'
			},
			textarea: {
				overflowX: 'hidden'
			}
		}
	};
};

var colors = ['#948FE3', '#097aa8', '#A3A3A3', '#737373', '#999965', '#5387EA', '#00B4F1', '#068708', '#8EC430', '#61290E', '#7D1378', '#993365', '#933514', '#A84818', '#A643D1', '#CA0086', '#DEAE66', '#D39F53', '#EB90D3', '#F9921C', '#C5060E', '#EB4618', '#4C1E0D', '#ffffff'];

var doubleColorCombo = [_InvoiceTheme2.default.DOUBLE_THEME_COLOR_COMBO_1, _InvoiceTheme2.default.DOUBLE_THEME_COLOR_COMBO_2, _InvoiceTheme2.default.DOUBLE_THEME_COLOR_COMBO_3, _InvoiceTheme2.default.DOUBLE_THEME_COLOR_COMBO_4, _InvoiceTheme2.default.DOUBLE_THEME_COLOR_COMBO_5];

var RegularPrintSettings = function (_BasePrintSettings) {
	(0, _inherits3.default)(RegularPrintSettings, _BasePrintSettings);

	function RegularPrintSettings(props) {
		(0, _classCallCheck3.default)(this, RegularPrintSettings);

		var _this = (0, _possibleConstructorReturn3.default)(this, (RegularPrintSettings.__proto__ || (0, _getPrototypeOf2.default)(RegularPrintSettings)).call(this, props));

		_this.getItemTableCustomizationModal = function () {
			var ItemTableCustomization = require('./ItemTableCustomization').default;
			return _react2.default.createElement(
				_Modal2.default,
				{ width: '90%', height: '90%', parentClassSelector: 'item-table-customization',
					canHideHeader: 'true',
					onClose: _this.toggleItemTablePopup,
					isOpen: _this.state.itemTableCustomization,
					title: 'Item Table Customization' },
				_react2.default.createElement(ItemTableCustomization, { key: 1, callback: _this.toggleItemTablePopup, headerBackgroundColor: _this.state.selectedColor })
			);
		};

		_this.toggleItemTablePopup = function () {
			_this.setState({
				itemTableCustomization: !_this.state.itemTableCustomization
			});
		};

		_this.getCollectPaymentModal = function () {
			var CollectPayment = require('../CollectPayment').default;
			return _react2.default.createElement(
				_Modal2.default,
				{
					height: '500px',
					width: '620px',
					isOpen: _this.state.canShowCollectPaymentModal,
					canHideHeader: 'true',
					parentClassSelector: 'react-modal-container',
					title: 'Collect Payments Online',
					onClose: _this.toggleCollectQRCodePaymentScreen
				},
				_react2.default.createElement(
					'div',
					{ className: 'modalBody collect-payment-modal' },
					_react2.default.createElement(CollectPayment, { closeFunc: _this.toggleCollectQRCodePaymentScreen })
				)
			);
		};

		_this.toggleCollectQRCodePaymentScreen = function () {
			_this.setState({
				canShowCollectPaymentModal: !_this.state.canShowCollectPaymentModal
			});
		};

		_this.changeLogoAndSignature = function (isLogo) {
			var _require = require('../../../Utilities/ImageHelper'),
			    openSaveImageDialog = _require.openSaveImageDialog;

			var title = '';
			if (isLogo) {
				title = 'Select Firm Logo';
			} else {
				title = 'Select Firm Signature';
			}
			openSaveImageDialog({ title: title }, _this.firmLogoAndSignatureCallback.bind(_this, isLogo));
		};

		_this.changePageAndTextSize = function (event) {
			var key = event.target.name;
			var value = event.target.value;
			_this.setState((0, _defineProperty3.default)({}, key, value), function () {
				_this.saveSettings(key, value);
			});
		};

		_this.handleLayoutTabChange = function (_, activeIndexLayout) {
			_this.setState({ activeIndexLayout: activeIndexLayout });
		};

		_this.scrollToSettings = function (clickedId) {
			var config = _this.scrollSettingsConfig[clickedId];
			if (config && config.length > 0) {
				_this.scrollTo(config[0], config[1]);
			}
		};

		_this.selectTheme = function (selectedTemplate) {
			_this.setCarouselIndex(selectedTemplate);
			_this.setState({
				selectedTemplate: selectedTemplate
			}, function () {
				_this.settingsModel.setSettingKey(_Queries2.default.SETTING_TXN_PDF_THEME);
				_this.settingsModel.setSettingValue(selectedTemplate);
				_this.settingsModel.UpdateSetting(selectedTemplate);
				if (selectedTemplate == _InvoiceTheme2.default.THEME_12 || selectedTemplate == _InvoiceTheme2.default.THEME_11) {
					var FTUHelper = require('../../../Utilities/FTUHelper');
					FTUHelper.setPremiumThemeClicked();
					MyAnalytics.pushEvent('User Viewed Premium Theme', { Action: 'Click' });
				}
			});
		};

		_this.selectColor = function (selectedColor) {
			_this.setState({
				selectedColor: selectedColor
			}, function () {
				_this.settingsModel.setSettingKey(_Queries2.default.SETTING_TXN_PDF_THEME_COLOR);
				_this.settingsModel.setSettingValue(selectedColor);
				_this.settingsModel.UpdateSetting(selectedColor);
			});
		};

		_this.doubleColorThemeSelector = function (doubleThemeCombo) {
			_this.setState({
				doubleThemeCombo: doubleThemeCombo
			}, function () {
				_this.settingsModel.setSettingKey(_Queries2.default.SETTING_TXN_PDF_DOUBLE_THEME_COLOR);
				_this.settingsModel.setSettingValue(doubleThemeCombo);
				_this.settingsModel.UpdateSetting(doubleThemeCombo);
			});
		};

		_this.amountInWords = [{ key: 0, value: 'Indian' }, { key: 1, value: 'English' }];
		_this.themesImagesList = [{ id: 10, url: '../inlineSVG/gst_theme_1.svg', label: 'GST Theme 1' }, { id: 7, url: '../inlineSVG/gst_theme_2.svg', label: 'GST Theme 2' }, { id: 9, url: '../inlineSVG/gst_theme_3.svg', label: 'GST Theme 3' }, { id: 5, url: '../inlineSVG/gst_theme_4.svg', label: 'GST Theme 4' }, { id: 6, url: '../inlineSVG/gst_theme_5.svg', label: 'GST Theme 5' }, { id: 8, url: '../inlineSVG/gst_theme_6.svg', label: 'GST Theme 6' }, { id: 11, url: '../inlineSVG/theme_4.svg', label: 'Double Divine' }, { id: 12, url: '../inlineSVG/theme_4.svg', label: 'French Elite' }, { id: 1, url: '../inlineSVG/theme_1.svg', label: 'Theme 1' }, { id: 2, url: '../inlineSVG/theme_2.svg', label: 'Theme 2' }, { id: 3, url: '../inlineSVG/theme_3.svg', label: 'Theme 3' }, { id: 4, url: '../inlineSVG/theme_4.svg', label: 'Theme 4' }];

		_this.scrollSettingsConfig = {
			invoiceHeaderSettings: ['regularSettingsContainer', 'scrollToHeader'],
			invoiceMiddleSettings: ['regularSettingsContainer', 'scrollToCenter'],
			invoiceFooterSettings: ['regularSettingsContainer', 'scrollToFooter']
		};
		_this.paperSizeDd = [{ key: 1, value: 'A4' }, { key: 2, value: 'A5' }];
		_this.companyInfoHeaderList = [{ key: _Queries2.default.SETTING_PRINT_COMPANY_NAME_ON_TXN_PDF, control: 'textbox', controlKey: 'companyName', value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_COMPANY_NAME_ON_TXN_PDF].heading }, { key: _Queries2.default.SETTING_PRINT_LOGO_ON_TXN_PDF, control: 'checkbox', value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_LOGO_ON_TXN_PDF].heading }, { key: _Queries2.default.SETTING_PRINT_COMPANY_ADDRESS_ON_TXN_PDF, control: 'textbox', controlKey: 'companyAddress', value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_COMPANY_ADDRESS_ON_TXN_PDF].heading }, { key: _Queries2.default.SETTING_PRINT_COMPANY_EMAIL_ON_PDF, control: 'textbox', controlKey: 'companyEmail', value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_COMPANY_EMAIL_ON_PDF].heading }, { key: _Queries2.default.SETTING_PRINT_COMPANY_NUMBER_ON_TXN_PDF, control: 'textbox', controlKey: 'companyPhone', value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_COMPANY_NUMBER_ON_TXN_PDF].heading }, { key: _Queries2.default.SETTING_PRINT_TINNUMBER, control: 'textbox', controlKey: 'gstIn', value: (_this.settingCache.getGSTEnabled() ? 'GSTIN' : _this.settingCache.getTINText()) + ' on Sale' }, { key: _Queries2.default.SETTING_PRINT_PAGE_SIZE, control: 'dropdown', value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_PAGE_SIZE].heading, list: _this.paperSizeDd, showCheckbox: false }, { key: _Queries2.default.SETTING_PRINT_COMPANY_NAME_TEXT_SIZE, control: 'dropdown', value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_COMPANY_NAME_TEXT_SIZE].heading, list: _this.textSizeDd, showCheckbox: false }, { key: _Queries2.default.SETTING_PRINT_TEXT_SIZE, control: 'dropdown', value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_TEXT_SIZE].heading, list: _this.textSizeDd, showCheckbox: false }, { key: _Queries2.default.SETTING_PRINT_COPY_NUMBER, control: 'checkbox', value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_COPY_NUMBER].heading }];
		_this.filterGSTINSettingForCompanyHeader(_this.companyInfoHeaderList);

		_this.totalsAndTaxSettings = (0, _extends4.default)({}, _SettingsMapping2.default.totalsAndTaxSettings, _SettingsMapping2.default.regularPrintTotalAndTaxSettings);
		_this.footerSettings = [{
			key: _Queries2.default.SETTING_PRINT_DESCRIPTION_ON_TXN_PDF,
			control: 'checkbox',
			value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_DESCRIPTION_ON_TXN_PDF].heading
		}, {
			key: _Queries2.default.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF,
			control: 'textbox',
			controlKey: _Queries2.default.SETTING_TERMS_AND_CONDITIONS,
			value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF].heading,
			moreAttributes: { multiline: true, rows: '3' }
		}, {
			key: _Queries2.default.SETTING_SIGNATURE_ENABLED,
			control: 'textbox',
			controlKey: _Queries2.default.SETTING_SIGNATURE_TEXT,
			value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_SIGNATURE_ENABLED].heading
		}, { key: _Queries2.default.SETTING_PRINT_PAYMENT_MODE, control: 'checkbox', value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_PAYMENT_MODE].heading }, { key: _Queries2.default.SETTING_PRINT_BANK_DETAIL, control: 'checkbox', value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_BANK_DETAIL].heading }, { key: _Queries2.default.SETTING_PRINT_ACKNOWLEDGMENT, control: 'checkbox', value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_ACKNOWLEDGMENT].heading }];

		if (_this.settingCache.isCurrentCountryIndia()) {
			_this.footerSettings.push({ key: _Queries2.default.SETTING_PRINT_ENABLE_QR_CODE, control: 'checkbox', value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_ENABLE_QR_CODE].heading });
		}
		// Adding terms and conditions array for displaying data on modal
		_this.TermsAndConditionsSetting = [{
			key: _Queries2.default.SETTING_PRINT_SALE_INVOICE_TERM_AND_CONDITION_ON_TXN_PDF,
			control: 'textbox',
			controlKey: _Queries2.default.SETTING_SALE_INVOICE_TERMS_AND_CONDITIONS,
			value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_SALE_INVOICE_TERM_AND_CONDITION_ON_TXN_PDF].heading,
			moreAttributes: { multiline: true, rows: '3' }
		}, {
			key: _Queries2.default.SETTING_PRINT_SALE_ORDER_TERM_AND_CONDITION_ON_TXN_PDF,
			control: 'textbox',
			controlKey: _Queries2.default.SETTING_SALE_ORDER_TERMS_AND_CONDITIONS,
			value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_SALE_ORDER_TERM_AND_CONDITION_ON_TXN_PDF].heading,
			moreAttributes: { multiline: true, rows: '3' }
		}, {
			key: _Queries2.default.SETTING_PRINT_DELIVERY_CHALLAN_TERM_AND_CONDITION_ON_TXN_PDF,
			control: 'textbox',
			controlKey: _Queries2.default.SETTING_DELIVERY_CHALLAN_TERMS_AND_CONDITIONS,
			value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_DELIVERY_CHALLAN_TERM_AND_CONDITION_ON_TXN_PDF].heading,
			moreAttributes: { multiline: true, rows: '3' }
		}, {
			key: _Queries2.default.SETTING_PRINT_ESTIMATE_QUOTATION_TERM_AND_CONDITION_ON_TXN_PDF,
			control: 'textbox',
			controlKey: _Queries2.default.SETTING_ESTIMATE_QUOTATION_TERMS_AND_CONDITIONS,
			value: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_ESTIMATE_QUOTATION_TERM_AND_CONDITION_ON_TXN_PDF].heading,
			moreAttributes: { multiline: true, rows: '3' }
		}];

		_this.state = (0, _extends4.default)({
			activeIndexLayout: 0,
			themeTemplate: '',
			itemTableCustomization: false,
			canShowCollectPaymentModal: false
		}, _this.regularPrinterSettingsState());
		_this.carouselRef = _react2.default.createRef();
		_this.setCarouselIndex(_this.state.selectedTemplate);
		return _this;
	}

	(0, _createClass3.default)(RegularPrintSettings, [{
		key: 'componentWillUpdate',
		value: function componentWillUpdate() {
			if (window.isSyncSettingUpdate) {
				this.setCarouselIndex(this.settingCache.getTxnPDFTheme());
			}
		}
	}, {
		key: 'resetState',
		value: function resetState() {
			this.getFirmDetails();
			this.setState((0, _extends4.default)({}, this.regularPrinterSettingsState()));
		}
	}, {
		key: 'regularPrinterSettingsState',
		value: function regularPrinterSettingsState() {
			var _extends2;

			return (0, _extends4.default)({}, this.getCompanyPrintHeaderInfo(), (_extends2 = {}, (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_PAGE_SIZE, this.listOfSettings[_Queries2.default.SETTING_PRINT_PAGE_SIZE]), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_DESCRIPTION_ON_TXN_PDF, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_DESCRIPTION_ON_TXN_PDF])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_ITEM_QUANTITY_TOTAL_ON_TXN_PDF, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_ITEM_QUANTITY_TOTAL_ON_TXN_PDF])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_AMOUNT_TILL_SPECIFIED_PLACES, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_AMOUNT_TILL_SPECIFIED_PLACES])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_TAX_DETAILS, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_TAX_DETAILS])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_SIGNATURE_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_SIGNATURE_ENABLED])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_PAYMENT_MODE, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_PAYMENT_MODE])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_BANK_DETAIL, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_BANK_DETAIL])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_ACKNOWLEDGMENT, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_ACKNOWLEDGMENT])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_ENABLE_QR_CODE, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_ENABLE_QR_CODE]) && this.settingCache.isCurrentCountryIndia()), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_TERMS_AND_CONDITIONS, this.listOfSettings[_Queries2.default.SETTING_TERMS_AND_CONDITIONS]), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_SALE_INVOICE_TERMS_AND_CONDITIONS, this.listOfSettings[_Queries2.default.SETTING_SALE_INVOICE_TERMS_AND_CONDITIONS] || this.listOfSettings[_Queries2.default.SETTING_TERMS_AND_CONDITIONS]), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_SALE_ORDER_TERMS_AND_CONDITIONS, this.listOfSettings[_Queries2.default.SETTING_SALE_ORDER_TERMS_AND_CONDITIONS] || this.listOfSettings[_Queries2.default.SETTING_TERMS_AND_CONDITIONS]), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_DELIVERY_CHALLAN_TERMS_AND_CONDITIONS, this.listOfSettings[_Queries2.default.SETTING_DELIVERY_CHALLAN_TERMS_AND_CONDITIONS] || this.listOfSettings[_Queries2.default.SETTING_TERMS_AND_CONDITIONS]), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_ESTIMATE_QUOTATION_TERMS_AND_CONDITIONS, this.listOfSettings[_Queries2.default.SETTING_ESTIMATE_QUOTATION_TERMS_AND_CONDITIONS] || this.listOfSettings[_Queries2.default.SETTING_TERMS_AND_CONDITIONS]), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_SALE_INVOICE_TERM_AND_CONDITION_ON_TXN_PDF, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_SALE_INVOICE_TERM_AND_CONDITION_ON_TXN_PDF] || this.listOfSettings[_Queries2.default.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_SALE_ORDER_TERM_AND_CONDITION_ON_TXN_PDF, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_SALE_ORDER_TERM_AND_CONDITION_ON_TXN_PDF] || this.listOfSettings[_Queries2.default.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_DELIVERY_CHALLAN_TERM_AND_CONDITION_ON_TXN_PDF, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_DELIVERY_CHALLAN_TERM_AND_CONDITION_ON_TXN_PDF] || this.listOfSettings[_Queries2.default.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_ESTIMATE_QUOTATION_TERM_AND_CONDITION_ON_TXN_PDF, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_ESTIMATE_QUOTATION_TERM_AND_CONDITION_ON_TXN_PDF] || this.listOfSettings[_Queries2.default.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_SIGNATURE_TEXT, this.listOfSettings[_Queries2.default.SETTING_SIGNATURE_TEXT]), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_SHOW_RECEIVED_AMOUNT_OF_TRANSACTION, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_SHOW_RECEIVED_AMOUNT_OF_TRANSACTION])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_SHOW_BALANCE_AMOUNT_OF_TRANSACTION, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_SHOW_BALANCE_AMOUNT_OF_TRANSACTION])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_SHOW_CURRENT_BALANCE_OF_PARTY, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_SHOW_CURRENT_BALANCE_OF_PARTY])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_AMOUNT_IN_WORD_FORMAT, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_AMOUNT_IN_WORD_FORMAT])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_AMOUNT_GROUPING, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_AMOUNT_GROUPING])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_COPY_NUMBER, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_COPY_NUMBER])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_LOGO_ON_TXN_PDF, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_LOGO_ON_TXN_PDF])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_DEFAULT_PRINTER, this.listOfSettings[_Queries2.default.SETTING_DEFAULT_PRINTER]), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF])), (0, _defineProperty3.default)(_extends2, 'logoImagePath', this.defaultFirm.getFirmImagePath()), (0, _defineProperty3.default)(_extends2, 'signImagePath', this.defaultFirm.getFirmSignImagePath()), (0, _defineProperty3.default)(_extends2, 'selectedTemplate', this.settingCache.getTxnPDFTheme()), (0, _defineProperty3.default)(_extends2, 'selectedColor', this.settingCache.getTxnPDFThemeColor()), (0, _defineProperty3.default)(_extends2, 'doubleThemeCombo', this.settingCache.getTxnPDFDoubleThemeColor()), _extends2));
		}
	}, {
		key: 'firmLogoAndSignatureCallback',
		value: function firmLogoAndSignatureCallback(isLogo, imageName) {
			if (!imageName) {
				ToastHelper.error('Some error occurred while selecting the image. Please try again!');
				return;
			}
			var imageData = 'data:image/gif;base64,';

			if (isLogo) {
				this.defaultFirm.setFirmImagePath(imageName);
				this.defaultFirm.setFirmSignImagePath(imageData + this.defaultFirm.getFirmSignImagePath());
			} else {
				this.defaultFirm.setFirmImagePath(imageData + this.defaultFirm.getFirmImagePath());
				this.defaultFirm.setFirmSignImagePath(imageName);
			}
			var statusCode = this.defaultFirm.updateFirmWithoutObject(false);
			if (statusCode == _ErrorCode2.default.ERROR_FIRM_UPDATE_SUCCESS) {
				this.defaultFirm = this.firmCache.getDefaultFirm();
				this.setState({
					logoImagePath: this.defaultFirm.getFirmImagePath(),
					signImagePath: this.defaultFirm.getFirmSignImagePath()
				});
			} else {
				ToastHelper.error(statusCode);
			}
		}
	}, {
		key: 'getSelectedTemplate',
		value: function getSelectedTemplate() {
			var ThemeTemplates = require('./ThemeTemplates').default;
			var themeTemplates = new ThemeTemplates(this);
			var themeTemplate = themeTemplates.getThemeTemplate(this.state.selectedTemplate, this.state);
			if (!themeTemplate) {
				return 'NO PREVIEW TO SHOW';
			}
			return themeTemplate;
		}
	}, {
		key: 'renderRegularPrinterThemeCarousel',
		value: function renderRegularPrinterThemeCarousel() {
			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				this.renderThemeCarousel(this.themesImagesList, this.state.selectedTemplate, this.selectTheme)
			);
		}
	}, {
		key: 'adjustWidthForPreview',
		value: function adjustWidthForPreview() {
			setTimeout(function () {
				var themeViewer = document.getElementsByClassName('theme-viewer');
				if (themeViewer.length === 0) {
					return;
				}
				var txnItemTableForPrint = themeViewer[0].getElementsByClassName('txnItemTableForPrint');
				var pdfTransactionHTMLView = themeViewer[0].getElementsByClassName('pdfTransactionHTMLView');
				if (txnItemTableForPrint.length > 0 && txnItemTableForPrint[0].offsetWidth > pdfTransactionHTMLView[0].offsetWidth) {
					txnItemTableForPrint[0].style.width = pdfTransactionHTMLView[0].offsetWidth + 'px';
				}
			});
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			setTimeout(function () {
				$('.settingsToolTip').tooltipster({
					theme: 'tooltipster-noir',
					maxWidth: 300
				});
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _this2 = this;

			var classes = this.props.classes;
			var _state = this.state,
			    activeIndexLayout = _state.activeIndexLayout,
			    selectedTemplate = _state.selectedTemplate;


			return _react2.default.createElement(
				'div',
				{ className: 'print-setting-container regular-printer-settings' },
				_react2.default.createElement(
					'div',
					{ className: 'row' },
					_react2.default.createElement(
						'div',
						{ id: 'regularSettingsContainer', className: classes.settingsColumn + ' col-5' },
						_react2.default.createElement(
							_Tabs2.default,
							{ key: 1, className: 'font-12', textColor: 'primary', value: activeIndexLayout, onChange: this.handleLayoutTabChange },
							_react2.default.createElement(_Tab2.default, { key: 1, label: 'Change Layout' }),
							_react2.default.createElement(_Tab2.default, { key: 2, label: 'Change Colors' })
						),
						activeIndexLayout === 0 && this.renderRegularPrinterThemeCarousel(),
						activeIndexLayout === 1 && _InvoiceTheme2.default.getThemeType(selectedTemplate) == _InvoiceTheme2.default.SINGLE_COLOR_THEME && _react2.default.createElement(
							_BaseSettings.TabContainer,
							{ className: 'layout-container' },
							_react2.default.createElement(
								'div',
								{ className: 'choose-color' },
								_react2.default.createElement(
									'ul',
									null,
									' ',
									colors.map(function (value, index) {
										var _React$createElement;

										return _react2.default.createElement('li', (_React$createElement = { key: index, onClick: _this2.selectColor.bind(_this2, value), className: (value === '#ffffff' ? 'option-color-box border-white-box' : 'option-color-box') + ' ' + (_this2.state.selectedColor == value ? 'selected-color-box' : '') }, (0, _defineProperty3.default)(_React$createElement, 'key', index), (0, _defineProperty3.default)(_React$createElement, 'style', { backgroundColor: value }), _React$createElement));
									})
								)
							)
						),
						activeIndexLayout === 1 && _InvoiceTheme2.default.getThemeType(selectedTemplate) == _InvoiceTheme2.default.DOUBLE_COLOR_THEME && _react2.default.createElement(
							_BaseSettings.TabContainer,
							{ className: 'layout-container' },
							_react2.default.createElement(
								'div',
								{ className: 'choose-color' },
								_react2.default.createElement(
									'ul',
									null,
									' ',
									doubleColorCombo.map(function (value, index) {
										var _React$createElement2;

										var colors = _InvoiceTheme2.default.getDoubleThemeColors(value);
										var primary = colors.primary,
										    secondary = colors.secondary;

										return _react2.default.createElement('li', (_React$createElement2 = { key: index, onClick: _this2.doubleColorThemeSelector.bind(_this2, value), className: 'option-color-box' + ' ' + (_this2.state.doubleThemeCombo == value ? 'selected-color-box' : '') }, (0, _defineProperty3.default)(_React$createElement2, 'key', index), (0, _defineProperty3.default)(_React$createElement2, 'style', { background: 'linear-gradient(' + primary + ' 0%,' + primary + ' 50%, ' + secondary + ' 50%,' + secondary + ' 100%)' }), _React$createElement2));
									})
								)
							)
						),
						_react2.default.createElement(
							'div',
							{ className: 'mb-20 mt-30' },
							_react2.default.createElement(
								'span',
								{ className: 'heading font-family-bold font-16' },
								'Print Company Info / Header'
							),
							_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
							_react2.default.createElement(
								'div',
								{ className: 'setting-item d-flex align-items-center MAKE_REGULAR_PRINTER_DEFAULT' },
								_react2.default.createElement(
									'label',
									{ className: 'input-checkbox' },
									_react2.default.createElement('input', { type: 'checkbox', onChange: this.toggleDefaultPrinterModal, checked: this.state[_Queries2.default.SETTING_DEFAULT_PRINTER] == 1 }),
									this.checkboxJSX
								),
								_react2.default.createElement(
									'span',
									{ className: '' },
									_SettingsMapping2.default.miscBasePrinterSettings.MAKE_REGULAR_PRINTER_DEFAULT.heading
								),
								this.renderToolTip(_SettingsMapping2.default.miscBasePrinterSettings.MAKE_REGULAR_PRINTER_DEFAULT)
							),
							_react2.default.createElement('div', { id: 'scrollToHeader' }),
							this.renderCompanyInfoHeader(this.companyInfoHeaderList),
							_react2.default.createElement(
								'div',
								{ className: 'setting-item height-66 d-flex align-items-center ' + _Queries2.default.SETTING_EXTRA_SPACE_ON_TXN_PDF },
								_react2.default.createElement(
									'div',
									{ className: 'mr-10 d-flex align-items-center' },
									_react2.default.createElement(
										'span',
										null,
										_SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_EXTRA_SPACE_ON_TXN_PDF].heading
									),
									this.renderToolTip(_SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_EXTRA_SPACE_ON_TXN_PDF], _Queries2.default.SETTING_EXTRA_SPACE_ON_TXN_PDF)
								),
								_react2.default.createElement(_NumberTextField2.default, {
									maxLength: 2,
									maxValue: 50,
									handleBlur: this.handleNumber(_Queries2.default.SETTING_EXTRA_SPACE_ON_TXN_PDF),
									width: 100,
									value: this.listOfSettings[_Queries2.default.SETTING_EXTRA_SPACE_ON_TXN_PDF],
									className: 'd-inline-flex justify-content-center w-20-px'
								})
							),
							_react2.default.createElement('a', { className: 'color-1789FC d-inline-block', dangerouslySetInnerHTML: { __html: _SettingsMapping2.default.miscBasePrinterSettings.CHANGE_TRANSACTION_NAMES.heading }, onClick: this.toggleChangeTransactionNameModal })
						),
						_react2.default.createElement(
							'div',
							{ className: 'mb-20' },
							_react2.default.createElement(
								'span',
								{ className: 'heading font-family-bold font-16' },
								'Item Table'
							),
							_react2.default.createElement('div', { id: 'scrollToCenter' }),
							_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
							_react2.default.createElement(
								'div',
								{ className: 'setting-item height-72 d-flex align-items-center ' + _Queries2.default.SETTING_MIN_ITEM_ROWS_ON_TXN_PDF },
								_react2.default.createElement(
									'div',
									{ className: 'mr-10 d-flex align-items-center' },
									_react2.default.createElement(
										'span',
										null,
										_SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_MIN_ITEM_ROWS_ON_TXN_PDF].heading
									),
									this.renderToolTip(_SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_MIN_ITEM_ROWS_ON_TXN_PDF], _Queries2.default.SETTING_MIN_ITEM_ROWS_ON_TXN_PDF)
								),
								_react2.default.createElement(_NumberTextField2.default, {
									maxLength: 2,
									maxValue: 50,
									handleBlur: this.handleNumber(_Queries2.default.SETTING_MIN_ITEM_ROWS_ON_TXN_PDF),
									width: 100,
									value: this.listOfSettings[_Queries2.default.SETTING_MIN_ITEM_ROWS_ON_TXN_PDF],
									className: 'd-inline-flex justify-content-center w-20-px'
								})
							),
							_react2.default.createElement('a', { onClick: this.toggleItemTablePopup, dangerouslySetInnerHTML: { __html: _SettingsMapping2.default.miscBasePrinterSettings.ITEM_TABLE_CUSTOMIZATION.heading }, className: 'color-1789FC d-inline-block' })
						),
						_react2.default.createElement(
							'div',
							{ className: 'mb-20' },
							_react2.default.createElement(
								'span',
								{ className: 'heading font-family-bold font-16' },
								'Totals & Taxes'
							),
							_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
							(0, _keys2.default)(this.totalsAndTaxSettings).map(function (key) {
								return _react2.default.createElement(
									'div',
									{ key: key, className: 'setting-item d-flex align-items-center ' + key },
									_react2.default.createElement(
										'label',
										{ className: 'input-checkbox' },
										_react2.default.createElement('input', { type: 'checkbox', onChange: _this2.toggleCheckbox(key, true, _this2.invoiceMiddleSettingsCallback), checked: _this2.state[key] }),
										_this2.checkboxJSX
									),
									_react2.default.createElement('span', { dangerouslySetInnerHTML: { __html: _this2.totalsAndTaxSettings[key].heading } }),
									_this2.renderToolTip(_this2.totalsAndTaxSettings[key])
								);
							}),
							this.state[_Queries2.default.SETTING_PRINT_AMOUNT_GROUPING] && _react2.default.createElement(
								'div',
								{ className: 'setting-item d-flex align-items-center ' + _Queries2.default.SETTING_PRINT_AMOUNT_GROUPING },
								_react2.default.createElement(
									'div',
									{ className: 'd-flex align-items-center' },
									_react2.default.createElement(
										'span',
										null,
										_SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_AMOUNT_IN_WORD_FORMAT].heading
									),
									this.renderToolTip(_SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_AMOUNT_IN_WORD_FORMAT])
								),
								_react2.default.createElement(
									_SelectDropDown2.default,
									{ outlined: true, handleChange: this.saveSettings.bind(this, _Queries2.default.SETTING_AMOUNT_IN_WORD_FORMAT), value: this.listOfSettings[_Queries2.default.SETTING_AMOUNT_IN_WORD_FORMAT], name: 'amountInWords' },
									this.amountInWords.map(function (option, index) {
										return _react2.default.createElement(
											_MenuItem2.default,
											{ key: option.key, value: option.key },
											option.value
										);
									})
								)
							)
						),
						_react2.default.createElement(
							'div',
							null,
							_react2.default.createElement(
								'span',
								{ className: 'heading font-family-bold font-16' },
								'Footer'
							),
							_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
							_react2.default.createElement('div', { id: 'scrollToFooter' }),
							this.renderFooter()
						)
					),
					_react2.default.createElement(
						'div',
						{ id: 'invoiceTemplatePreview', className: classes.leftSeparator + ' ' + classes.themePreviewContainer + ' theme-viewer col-7' },
						_react2.default.createElement(
							'div',
							{ className: 'theme-preview' },
							this.getSelectedTemplate()
						)
					)
				),
				this.state.openTransactionNameModal && this.renderCustomTransactionNamesModal(),
				this.state.openTermsAndConditionsModal && this.renderCustomTermsAndConditionsModal(),
				this.state.openDefaultPrinterModal && this.renderDefaultPrinterModal(),
				_react2.default.createElement(
					'div',
					null,
					this.state.itemTableCustomization && this.getItemTableCustomizationModal()
				),
				_react2.default.createElement(
					'div',
					null,
					this.state.canShowCollectPaymentModal && this.getCollectPaymentModal()
				)
			);
		}
	}]);
	return RegularPrintSettings;
}(_BasePrintSettings3.default);

exports.default = (0, _styles.withStyles)(styles)(RegularPrintSettings);