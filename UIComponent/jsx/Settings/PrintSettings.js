Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _BasePrintSettings2 = require('./BasePrintSettings');

var _BasePrintSettings3 = _interopRequireDefault(_BasePrintSettings2);

var _BaseSettings = require('./BaseSettings');

var _Tabs = require('@material-ui/core/Tabs');

var _Tabs2 = _interopRequireDefault(_Tabs);

var _Tab = require('@material-ui/core/Tab');

var _Tab2 = _interopRequireDefault(_Tab);

var _SettingsMapping = require('./SettingsMapping');

var _SettingsMapping2 = _interopRequireDefault(_SettingsMapping);

var _styles = require('@material-ui/core/styles');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
	return {
		selectDd: {
			width: '200px',
			backgroundColor: '#ffffff',
			'& .MuiSelect-select:focus': {
				backgroundColor: '#ffffff'
			}
		},
		leftSeparator: {
			borderLeft: '2px solid #DDDDDD'
		},
		'@global': {
			'.settings-container .right-content-pane .print-setting-container .MuiTabs-root': {
				minHeight: 0
			},
			'.settings-container .right-content-pane .print-setting-container .MuiTabs-flexContainer .MuiTab-root': {
				fontSize: '12px',
				fontFamily: 'roboto',
				height: '35px',
				minHeight: 0,
				borderRadius: '1px'
			},
			'.layout-container': {
				backgroundColor: '#FFFFFF',
				border: '1px solid #dddddd',
				boxShadow: '1px 1px 1px #dddddd'
			},
			'.selected-theme': {
				backgroundColor: '#dddddd',
				border: '1px solid #dddddd'
			},
			'.item-table-customization.settings-container': {
				display: 'flex',
				flexDirection: 'column',
				'& .item-table-settings': {
					display: 'flex',
					flexDirection: 'column'
				}
			}
		}
	};
};

var PrintSettings = function (_BasePrintSettings) {
	(0, _inherits3.default)(PrintSettings, _BasePrintSettings);

	function PrintSettings(props) {
		(0, _classCallCheck3.default)(this, PrintSettings);

		var _this = (0, _possibleConstructorReturn3.default)(this, (PrintSettings.__proto__ || (0, _getPrototypeOf2.default)(PrintSettings)).call(this, props));

		_this.getPrintSettingsModule = function (moduleIndex) {
			switch (moduleIndex) {
				case 0:
					var RegularPrinterSettings = require('./RegularPrinterSettings').default;
					return _react2.default.createElement(
						_BaseSettings.TabContainer,
						null,
						_react2.default.createElement(RegularPrinterSettings, null)
					);
				case 1:
					var ThermalPrinterSettings = require('./ThermalPrinterSettings').default;
					return _react2.default.createElement(
						_BaseSettings.TabContainer,
						null,
						_react2.default.createElement(ThermalPrinterSettings, null)
					);
			}
		};

		_this.state = {
			// [Queries.SETTING_BARCODE_SCANNING_ENABLED]: this.isChecked(this.listOfSettings[Queries.SETTING_BARCODE_SCANNING_ENABLED]),
			//[Queries.SETTING_DEFAULT_PRINTER]: this.listOfSettings[Queries.SETTING_DEFAULT_PRINTER],
			activeIndex: _this.listOfSettings[Queries.SETTING_DEFAULT_PRINTER] == 1 ? 0 : 1
		};
		return _this;
	}

	(0, _createClass3.default)(PrintSettings, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.scrollTo('regularSettingsContainer', this.higlightedSettingKey);
			this.higlightSearchedSetting();
			this.initializeToolTips();
		}

		// render() {
		// 	const { activeIndex } = this.state;
		// 	return (
		// 		<React.Fragment>

		// 			<div className="">
		// 				<div className="row">
		// 					<div className="col">

		// 						<Tabs indicatorColor="primary" textColor="primary" value={activeIndex} onChange={this.handleTabChange}>
		// 							<Tab label="Transaction SMS" />
		// 							<Tab label="SMS Settings" />
		// 						</Tabs>

		// 						{activeIndex === 0 && <TabContainer> 1 </TabContainer>}
		// 						{activeIndex === 1 && <TabContainer> 2 </TabContainer>}

		// 					</div>
		// 				</div>
		// 			</div >
		// 		</React.Fragment>

		// 	);
		// }

	}, {
		key: 'render',
		value: function render() {
			var classes = this.props.classes;
			var activeIndex = this.state.activeIndex;

			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement(
					'div',
					{ className: 'print-settings' },
					_react2.default.createElement(
						'div',
						{ className: 'row' },
						_react2.default.createElement(
							'div',
							{ className: 'col' },
							_react2.default.createElement(
								_Tabs2.default,
								{ key: 1, indicatorColor: 'primary', textColor: 'primary', value: activeIndex, onChange: this.handleTabChange },
								_react2.default.createElement(_Tab2.default, { key: 1, label: _SettingsMapping2.default.miscBasePrinterSettings['regularPrinter'].heading }),
								_react2.default.createElement(_Tab2.default, { key: 2, label: _SettingsMapping2.default.miscBasePrinterSettings['thermalPrinter'].heading })
							),
							activeIndex === 0 && this.getPrintSettingsModule(0),
							activeIndex === 1 && this.getPrintSettingsModule(1)
						),
						_react2.default.createElement('div', null)
					)
				)
			);
		}
	}]);
	return PrintSettings;
}(_BasePrintSettings3.default);

exports.default = (0, _styles.withStyles)(styles)(PrintSettings);