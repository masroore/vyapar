Object.defineProperty(exports, "__esModule", {
	value: true
});

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Dialog = require('@material-ui/core/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _styles = require('@material-ui/core/styles');

var _Tabs = require('@material-ui/core/Tabs');

var _Tabs2 = _interopRequireDefault(_Tabs);

var _Tab = require('@material-ui/core/Tab');

var _Tab2 = _interopRequireDefault(_Tab);

var _Typography = require('@material-ui/core/Typography');

var _Typography2 = _interopRequireDefault(_Typography);

var _SettingsMapping = require('./SettingsMapping');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
	return {
		appBar: {
			position: 'relative'
		},
		title: {
			marginLeft: theme.spacing(2),
			flex: 1
		},
		closeWrapper: {
			position: 'fixed',
			borderRadius: '38px',
			backgroundColor: '#999',
			top: 'var(--titlebar-height)',
			margin: 10
		}
	};
};

var VerticalTabs = (0, _styles.withStyles)(function (theme) {
	return {
		flexContainer: {
			flexDirection: 'column'
		},
		indicator: {
			display: 'none'
		}
	};
})(_Tabs2.default);

function TabContainer(props) {
	return _react2.default.createElement(
		_Typography2.default,
		{ component: 'div', style: { padding: 20 } },
		props.children
	);
}

var Settings = function (_React$Component) {
	(0, _inherits3.default)(Settings, _React$Component);

	function Settings(props) {
		(0, _classCallCheck3.default)(this, Settings);

		var _this = (0, _possibleConstructorReturn3.default)(this, (Settings.__proto__ || (0, _getPrototypeOf2.default)(Settings)).call(this, props));

		_this.toggleSearchBox = function () {
			if (_this.state.activeIndex === 0) {
				_this.resetSettingTab();
			} else {
				_this.openSearchBox();
			}
		};

		_this.openSearchBox = function () {
			_this.initialIndex = _this.state.activeIndex;
			_this.setState({
				activeIndex: 0,
				searchIconColor: _this.searchBoxColor.activeSearchColor
			});
		};

		_this.handleTabChange = function (_, activeIndex) {
			_this.higlightedSettingKey = '';
			_this.setState({
				activeIndex: activeIndex,
				searchIconColor: _this.searchBoxColor.inactiveSearchColor
			}, function () {
				$('.setting-tabs .MuiTabs-flexContainer button').attr('tabindex', "0");
			});
		};

		_this.handleEscape = function (e) {
			e.preventDefault();
			e.nativeEvent.stopImmediatePropagation();
			_this.handleSettingsOpenClose();
		};

		_this.selectSettingTab = function (settingItem) {
			_this.higlightedSettingKey = settingItem.settingKey;
			_this.setState({
				activeIndex: settingItem.vyaparSettingIndex,
				searchIconColor: _this.searchBoxColor.inactiveSearchColor
			});
		};

		_this.resetSettingTab = function () {
			_this.setState({
				activeIndex: _this.initialIndex,
				searchIconColor: _this.searchBoxColor.inactiveSearchColor
			}, function () {
				_this.initialIndex = '';
			});
		};

		_this.renderTabs = function () {
			var activeIndex = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;

			var settings = _SettingsMapping.VyaparSettings;
			var canShowSetting = function canShowSetting(index) {
				if (index == 3) {
					var SettingCache = require('../../../Cache/SettingCache');
					var settingCache = new SettingCache();
					return settingCache.isCurrentCountryIndia() || settingCache.getTransactionMessageEnabled();
				} else if (index == 8) {
					var isSyncSettingVisible = localStorage.getItem('isSyncSettingVisible');
					return isSyncSettingVisible == 'true';
				} else {
					return true;
				}
			};

			var tabsObj = (0, _keys2.default)(settings).map(function (key, index) {
				return _react2.default.createElement(_Tab2.default, { tabIndex: '0', className: canShowSetting(key) ? '' : 'd-none', key: index, label: settings[key] });
			});
			_this.setState({
				tabsObj: tabsObj,
				activeIndex: activeIndex,
				searchIconColor: _this.searchBoxColor.inactiveSearchColor
			});
		};

		_this.getSettingsModule = function (moduleIndex) {
			switch (moduleIndex) {
				case 0:
					var SearchResults = require('./SearchResults').default;
					return _react2.default.createElement(SearchResults, { backToInitialState: _this.resetSettingTab, onSettingItemClickCallback: _this.selectSettingTab });
				case 1:
					var GeneralSettings = require('./GeneralSettings').default;
					return _react2.default.createElement(
						TabContainer,
						null,
						_react2.default.createElement(GeneralSettings, { higlightedSettingKey: _this.higlightedSettingKey })
					);
				case 2:
					var TransactionSettings = require('./TransactionSettings').default;
					return _react2.default.createElement(
						TabContainer,
						null,
						_react2.default.createElement(TransactionSettings, { higlightedSettingKey: _this.higlightedSettingKey })
					);
					break;
				case 3:
					var TransactionSMSSettings = require('./TransactionSMSSettings').default;
					return _react2.default.createElement(
						TabContainer,
						null,
						_react2.default.createElement(TransactionSMSSettings, { higlightedSettingKey: _this.higlightedSettingKey })
					);
				case 4:
					var PartySettings = require('./PartySettings').default;
					return _react2.default.createElement(
						TabContainer,
						null,
						_react2.default.createElement(PartySettings, { higlightedSettingKey: _this.higlightedSettingKey })
					);

				case 5:
					var ItemSettings = require('./ItemSettings').default;
					return _react2.default.createElement(
						TabContainer,
						null,
						_react2.default.createElement(ItemSettings, { higlightedSettingKey: _this.higlightedSettingKey })
					);

				case 6:
					var PrintSettings = require('./PrintSettings').default;
					return _react2.default.createElement(
						TabContainer,
						null,
						_react2.default.createElement(PrintSettings, { higlightedSettingKey: _this.higlightedSettingKey })
					);

				case 7:
					var GSTSettings = require('./GSTSettings').default;
					return _react2.default.createElement(
						TabContainer,
						null,
						_react2.default.createElement(GSTSettings, { higlightedSettingKey: _this.higlightedSettingKey })
					);

				case 8:
					var SyncSettings = require('./SyncSettings').default;
					return _react2.default.createElement(
						TabContainer,
						null,
						_react2.default.createElement(SyncSettings, { higlightedSettingKey: _this.higlightedSettingKey })
					);
			}
		};

		_this.searchBoxColor = { activeSearchColor: '#F1F17C', inactiveSearchColor: '#FFFFFF' };
		_this.state = {
			openSettingsDialog: true,
			activeIndex: props.activeIndex || 1,
			searchIconColor: _this.searchBoxColor.inactiveSearchColor
		};
		return _this;
	}

	(0, _createClass3.default)(Settings, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			var activeIndex = this.state.activeIndex;

			this.renderTabs(activeIndex || 1);
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			$('.ui-dialog:visible .ui-dialog-content').dialog('close');
			if (window.onRes) {
				delete window.onRes;
			}
		}
	}, {
		key: 'handleSettingsOpenClose',
		value: function handleSettingsOpenClose(data) {
			var _this2 = this;

			this.setState({
				openSettingsDialog: !this.state.openSettingsDialog
			}, function () {
				if (!_this2.state.openSettingsDialog && onCloseClick && typeof onCloseClick === 'function') {
					setTimeout(function () {
						onCloseClick();
					});
				}
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var activeIndex = this.state.activeIndex;
			var classes = this.props.classes;

			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement('link', { rel: 'stylesheet', href: '../styles/settings.css' }),
				_react2.default.createElement('link', { rel: 'stylesheet', href: '../styles/generalSettings.css' }),
				_react2.default.createElement(
					_Dialog2.default,
					{ onEscapeKeyDown: this.handleEscape,
						disableEnforceFocus: true, fullScreen: true, open: this.state.openSettingsDialog },
					_react2.default.createElement(
						'div',
						{ className: 'settings-container settings-layout' },
						_react2.default.createElement(
							'div',
							{
								className: 'd-flex', style: { height: '100vh' }
							},
							_react2.default.createElement(
								'div',
								{ className: 'left-tabs-panel col-2' },
								_react2.default.createElement(
									'div',
									{ className: 'heading-section mt-20' },
									_react2.default.createElement(
										'label',
										null,
										'Settings'
									),
									_react2.default.createElement(
										'span',
										{ className: 'pointer floatRight', style: { marginRight: 2 }, onClick: this.toggleSearchBox },
										_react2.default.createElement(
											'svg',
											{ height: '22', viewBox: '0 0 24 24', width: '22' },
											_react2.default.createElement('path', { className: 'searchIcon', fill: this.state.searchIconColor, d: 'M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z' }),
											_react2.default.createElement('path', { d: 'M0 0h24v24H0z', fill: 'none' })
										)
									)
								),
								_react2.default.createElement(
									VerticalTabs,
									{ key: 1,
										value: activeIndex,
										onChange: this.handleTabChange,
										className: 'setting-tabs'
									},
									_react2.default.createElement(_Tab2.default, { key: 0, label: 'Search', style: { display: 'none' } }),
									this.state.tabsObj
								)
							),
							_react2.default.createElement(
								'div',
								{ className: 'right-content-pane col-10' },
								_react2.default.createElement(
									'div',
									{ onClick: this.handleSettingsOpenClose.bind(this), className: classes.closeWrapper + ' pointer closeWrapper' },
									_react2.default.createElement('img', { src: './../inlineSVG/close-white.png' })
								),
								activeIndex === 0 && this.getSettingsModule(0),
								activeIndex === 1 && this.getSettingsModule(1),
								activeIndex === 2 && this.getSettingsModule(2),
								activeIndex === 3 && this.getSettingsModule(3),
								activeIndex === 4 && this.getSettingsModule(4),
								activeIndex === 5 && this.getSettingsModule(5),
								activeIndex === 6 && this.getSettingsModule(6),
								activeIndex === 7 && this.getSettingsModule(7),
								activeIndex === 8 && this.getSettingsModule(8)
							),
							_react2.default.createElement('div', { id: 'settingsLazyLoadComponents' })
						)
					)
				)
			);
		}
	}]);
	return Settings;
}(_react2.default.Component);

exports.default = (0, _styles.withStyles)(styles)(Settings);