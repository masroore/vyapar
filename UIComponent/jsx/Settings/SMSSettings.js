Object.defineProperty(exports, "__esModule", {
	value: true
});

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _BaseSettings2 = require('./BaseSettings');

var _BaseSettings3 = _interopRequireDefault(_BaseSettings2);

var _styles = require('@material-ui/core/styles');

var _Queries = require('../../../Constants/Queries');

var _Queries2 = _interopRequireDefault(_Queries);

var _MenuItem = require('@material-ui/core/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _TokenForSync = require('../../../Utilities/TokenForSync');

var _TokenForSync2 = _interopRequireDefault(_TokenForSync);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _SyncHelper = require('../../../Utilities/SyncHelper');

var _SyncHelper2 = _interopRequireDefault(_SyncHelper);

var _Domain = require('../../../Constants/Domain');

var _Domain2 = _interopRequireDefault(_Domain);

var _FirmCache = require('../../../Cache/FirmCache');

var _FirmCache2 = _interopRequireDefault(_FirmCache);

var _Modal = require('../Modal');

var _Modal2 = _interopRequireDefault(_Modal);

var _TextField = require('@material-ui/core/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var BrowserWindow = require('electron').remote.BrowserWindow;

var WORKFLOW = 5;
var regSenderId = /^[A-Z]{6}$/;
var styles = function styles(theme) {
	return {
		smsSettings: {
			'& .box': {
				borderRadius: '4px',
				boxShadow: '1px 1px 3px #a59b9b',
				border: '1px solid  #f5e6e6',
				backgroundColor: '#ffffff'
			},
			'& .logged-in-box': {
				width: '300px',
				minHeight: '80px',
				padding: '20px',
				'& .heading': {
					padding: '0'
				}
			},
			'& .sms-balance-box': {
				width: '180px',
				height: '144px',
				'& .heading': {
					padding: '15px 15px 10px 15px',
					borderBottom: '1px solid #f5e6e6'
				},
				'& .sms-no-container': {
					padding: '10px 15px 15px 20px',
					'& p': {
						paddingBottom: '10px'
					}
				}
			}
		},
		selectDd: {
			width: '200px',
			marginTop: '20px',
			backgroundColor: '#ffffff',
			'& .MuiSelect-select:focus': {
				backgroundColor: '#ffffff'
			}
		}
	};
};

var SMSSettings = function (_BaseSettings) {
	(0, _inherits3.default)(SMSSettings, _BaseSettings);

	function SMSSettings(props) {
		(0, _classCallCheck3.default)(this, SMSSettings);

		var _this = (0, _possibleConstructorReturn3.default)(this, (SMSSettings.__proto__ || (0, _getPrototypeOf2.default)(SMSSettings)).call(this, props));

		_this.loadLoginInformation = function () {
			_this.tokenInformation = _this.readToken();
			if (_this.tokenInformation.isLoggedIn) {
				_this.getSenderIds();
				_this.getSMSBalance();
			} else {
				_this.setState({
					tokenInformation: _this.tokenInformation
				});
			}
		};

		_this.login = function () {
			_SyncHelper2.default.loginToGetAuthToken(_this.loadLoginInformation);
		};

		_this.logout = function () {
			_TokenForSync2.default.deleteToken();
			_this.loadLoginInformation();
		};

		_this.afterLogin = function (token) {
			_this.tokenInformation = {
				authToken: token.auth_token,
				loggedInAs: token.email,
				isLoggedIn: true
			};
			_this.getSenderIds();
			_this.getSMSBalance();
		};

		_this.handleSenderIdChange = function () {
			return function (event) {
				var value = event.target.value;
				var approvedSenderIds = _this.state.approvedSenderIds;
				if (value === '-1') {
					_this.setState({
						approvedSenderIds: approvedSenderIds,
						selectedSenderId: _this.settingCache.getTxnMsgSenderId()
					}, function () {
						_this.toggleAddSenderIdModal();
					});
					return false;
				}
				var selectedApprovedId = approvedSenderIds.filter(function (approvedId) {
					return approvedId.key.toLowerCase() === value.toLowerCase();
				})[0];
				if (!selectedApprovedId) {
					ToastHelper.error('Some error occurred! Please try again after some time.');
					return;
				}
				if (selectedApprovedId.value.indexOf('Pending') >= 0 || selectedApprovedId.value.indexOf('Rejected') >= 0) {
					_this.setState({
						approvedSenderIds: approvedSenderIds,
						selectedSenderId: _this.settingCache.getTxnMsgSenderId()
					});
					ToastHelper.error('Please select approved sender id.');
					return;
				}

				_this.saveSettings(_Queries2.default.SETTING_TXN_MSG_SENDER_ID, value);
				_this.setState({
					selectedSenderId: _this.settingCache.getTxnMsgSenderId()
				});
			};
		};

		_this.createSenderId = function () {
			var newSenderId = _this.state.senderId ? _this.state.senderId.toUpperCase() : '';
			if (!newSenderId || !regSenderId.test(newSenderId)) {
				ToastHelper.error('Invalid SenderId: Sender id should be 6 character long and between A to Z');
				return false;
			}
			var ENDPOINTS = {
				alias: _this.domain + '/api/sms/alias',
				defaultHeader: {
					'Accept': 'application/json',
					'Authorization': 'Bearer ' + _this.tokenInformation.authToken,
					'Content-Type': 'application/json',
					'Cache-Control': 'no-cache'
				}
			};
			$.post({
				url: ENDPOINTS.alias,
				data: { sender_id: newSenderId },
				headers: (0, _extends3.default)({}, ENDPOINTS.defaultHeader, {
					'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
				}),
				success: function success() {
					// this.getSenderIds();
					var approvedSenderIds = _this.state.approvedSenderIds;
					approvedSenderIds.push({ key: newSenderId, value: newSenderId + ' (Pending)' });
					_this.setState({
						approvedSenderIds: approvedSenderIds
					}, function () {
						_this.toggleAddSenderIdModal();
					});
					ToastHelper.success('Sender Id is created. Please wait for Vyapar team to approve it and you will be able to set it. We will inform you once we approve it by SMS/Email.');
				},
				error: function error(xhr) {
					console.log(xhr.status);
					if (xhr.status == 422) {
						ToastHelper.error('The sender id must be 6 characters.');
					} else if (xhr.status == 409) {
						ToastHelper.error('The sender already exists. Please choose different sender id with 6 characters.');
					} else if (xhr.status == 401) {
						ToastHelper.error('Sender id creation failed. Invalid User. Please login and try again.');
					} else if (xhr.status == 0) {
						ToastHelper.error('Sender id creation failed. Please check internet connection.');
					} else {
						ToastHelper.error('Something went wrong. Please try again.');
					}
				}
			});
		};

		_this.toggleAddSenderIdModal = function () {
			_this.setState({
				showSenderIdModal: !_this.state.showSenderIdModal
			});
		};

		_this.getShowSenderIdModal = function () {
			return _react2.default.createElement(
				_Modal2.default,
				{
					height: '180px',
					width: '350px',
					isOpen: _this.state.showSenderIdModal,
					onClose: _this.toggleAddSenderIdModal,
					canHideHeader: 'true',
					title: 'Create Sender Id',
					parentClassSelector: 'senderId-modal-container'
				},
				_react2.default.createElement(
					'div',
					{ className: 'modalBody' },
					_react2.default.createElement(
						'div',
						{ className: '' },
						_react2.default.createElement(_TextField2.default, {
							label: 'Sender Id',
							margin: 'dense',
							variant: 'outlined',
							className: 'width100',
							value: _this.state.senderId,
							onChange: _this.updateInputChange('senderId'),
							inputProps: {
								maxLength: '6'
							}
						}),
						_react2.default.createElement(
							'div',
							{ className: 'floatRight mt-10' },
							_react2.default.createElement(
								_Button2.default,
								{ margin: 'dense', className: 'mr-10', onClick: _this.toggleAddSenderIdModal, variant: 'contained', color: 'primary' },
								' Cancel'
							),
							_react2.default.createElement(
								_Button2.default,
								{ margin: 'dense', onClick: _this.createSenderId, variant: 'contained', color: 'primary' },
								' Save'
							)
						)
					)
				)
			);
		};

		_this.domain = _Domain2.default.thisDomain;
		_this.state = {
			tokenInformation: {},
			showSenderIdModal: false,
			selectedSenderId: _this.settingCache.getTxnMsgSenderId()
		};
		return _this;
	}

	(0, _createClass3.default)(SMSSettings, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.loadLoginInformation();
		}
	}, {
		key: 'resetState',
		value: function resetState() {
			var _this2 = this;

			this.setState({
				selectedSenderId: this.settingCache.getTxnMsgSenderId()
			}, function () {
				_this2.loadLoginInformation();
			});
		}
	}, {
		key: 'readToken',
		value: function readToken() {
			if (_TokenForSync2.default.ifExistToken()) {
				var token = _TokenForSync2.default.readToken();
				return {
					authToken: token.auth_token,
					loggedInAs: token.email,
					isLoggedIn: true
				};
			} else {
				return {
					isLoggedIn: false
				};
			}
		}
	}, {
		key: 'getSMSBalance',
		value: function getSMSBalance() {
			var _this3 = this;

			var ENDPOINTS = {
				credits: this.domain + '/api/sms/credits',
				defaultHeader: {
					'Accept': 'application/json',
					'Authorization': 'Bearer ' + this.tokenInformation.authToken,
					'Content-Type': 'application/json',
					'Cache-Control': 'no-cache'
				}
			};
			$.get({
				url: ENDPOINTS.credits,
				headers: (0, _extends3.default)({}, ENDPOINTS.defaultHeader),
				success: function success(data) {
					_this3.smsBalance = data.credits || 0;
					_this3.setState({
						smsBalance: _this3.smsBalance
					});
				},
				error: function error(xhr) {
					if (xhr.status == 0) {
						ToastHelper.error('Failed to get SMS Settings. Please check your internet connection.');
					} else if (xhr.status == 401) {
						ToastHelper.error('Failed to get SMS Settings. Invalid User. Please login.');
					} else {
						ToastHelper.error('Failed to get SMS Settings. Something went wrong. Please try again later.');
					}
				}
			});
		}
	}, {
		key: 'getSenderIds',
		value: function getSenderIds() {
			var _this4 = this;

			var selected = false;
			var ENDPOINTS = {
				alias: this.domain + '/api/sms/alias',
				defaultHeader: {
					'Accept': 'application/json',
					'Authorization': 'Bearer ' + this.tokenInformation.authToken,
					'Content-Type': 'application/json',
					'Cache-Control': 'no-cache'
				}
			};
			$.get({
				url: ENDPOINTS.alias,
				headers: (0, _extends3.default)({}, ENDPOINTS.defaultHeader),
				success: function success(data) {
					var approvedSenderIds = data.map(function (sender) {
						var label = '';
						if (sender.approval_status == 'Pending') {
							label = 'Pending';
						} else if (sender.approval_status == 'Approved') {
							label = 'Approved';
						} else {
							label = 'Rejected';
						}
						console.log(sender);
						var senderId = sender.sender_id;
						return {
							key: senderId,
							value: senderId + ' (' + label + ')'
						};
					});
					approvedSenderIds.unshift({ key: '-1', value: '+ Add Sender Id' });
					_this4.setState({
						tokenInformation: _this4.tokenInformation,
						approvedSenderIds: approvedSenderIds
					});
				},
				error: function error(xhr) {
					if (xhr.status == 0) {
						ToastHelper.error('Failed to get SMS Settings. Please check your internet connection.');
					} else if (xhr.status == 401) {
						ToastHelper.error('Failed to get SMS Settings. Invalid User. Please login.');
					} else {
						ToastHelper.error('Failed to get SMS Settings. Something went wrong. Please try again later.');
					}
				}
			});
		}
	}, {
		key: 'openMoreSettingsWindow',
		value: function openMoreSettingsWindow() {
			var _this5 = this;

			var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.domain + '/sms';

			var moreSettingsWindow = new BrowserWindow({
				show: true,
				minimizable: false,
				alwaysOnTop: true,
				skipTaskbar: true,
				minWidth: 1000,
				width: 1000,
				autoHideMenuBar: true,
				webPreferences: { nodeIntegration: false }
			});

			// moreSettingsWindow.maximize();

			moreSettingsWindow.on('page-title-updated', function () {
				var title = moreSettingsWindow.getTitle();
				try {
					var data = JSON.parse(title);
					if (data.auth_token !== undefined) {
						_TokenForSync2.default.writeToken((0, _stringify2.default)(data));
						_this5.afterLogin(data);
						return false;
					}
					if (data.status != 'paymentSuccess') {
						return false;
					}
					ToastHelper.success('Payment Successful.');
					moreSettingsWindow.close();
				} catch (e) {}
			});
			moreSettingsWindow.on('closed', function (e) {
				moreSettingsWindow = null;
				if (_this5.tokenInformation.authToken) {
					_this5.getSMSBalance();
				}
			});
			var headers = void 0;
			var emailforlogin = '';

			var firmCache = new _FirmCache2.default();
			if (firmCache.getDefaultFirm() == null) {
				emailforlogin = '';
			} else {
				emailforlogin = firmCache.getDefaultFirm().firmEmail;
			}

			if (emailforlogin) {
				url += '&email_id_=' + emailforlogin;
			}
			moreSettingsWindow.webContents.session.clearStorageData({ origin: this.domain, storages: ['cookies'] });
			moreSettingsWindow.loadURL(url, {
				extraHeaders: headers
			});
			moreSettingsWindow.show();
		}
	}, {
		key: 'render',
		value: function render() {
			var classes = this.props.classes;

			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement(
					'div',
					{ className: classes.smsSettings + ' sms-settings d-flex' },
					_react2.default.createElement(
						'div',
						null,
						_react2.default.createElement(
							'div',
							{ className: 'mt-20 box logged-in-box font-14' },
							this.state.tokenInformation.isLoggedIn && _react2.default.createElement(
								'div',
								null,
								_react2.default.createElement(
									'p',
									{ className: 'heading font-family-bold' },
									'Logged In as'
								),
								_react2.default.createElement(
									'div',
									null,
									_react2.default.createElement(
										'p',
										{ style: { wordWrap: 'break-word' } },
										this.tokenInformation.loggedInAs
									),
									_react2.default.createElement(
										'p',
										{ className: 'color-1789FC  textRight' },
										_react2.default.createElement(
											'a',
											{ onClick: this.logout },
											'Logout'
										)
									)
								)
							),
							!this.state.tokenInformation.isLoggedIn && _react2.default.createElement(
								'div',
								null,
								_react2.default.createElement(
									'p',
									{ className: 'heading font-family-bold' },
									'Please Log In vyapar account'
								),
								_react2.default.createElement(
									'div',
									null,
									_react2.default.createElement(
										'p',
										{ className: 'color-1789FC textRight', onClick: this.login },
										'LogIn'
									)
								)
							)
						),
						this.state.tokenInformation.isLoggedIn && _react2.default.createElement(
							_TextField2.default,
							{
								select: true,
								label: 'Sender Id',
								value: this.state.selectedSenderId,
								onChange: this.handleSenderIdChange(),
								margin: 'normal',
								variant: 'outlined',
								className: classes.selectDd
							},
							this.state.approvedSenderIds && this.state.approvedSenderIds.map(function (sender, index) {
								return _react2.default.createElement(
									_MenuItem2.default,
									{ key: sender.key, value: sender.key },
									sender.value
								);
							})
						)
					),
					this.state.tokenInformation.isLoggedIn && _react2.default.createElement(
						'div',
						{ className: 'ml-20 mt-20 box sms-balance-box' },
						_react2.default.createElement(
							'p',
							{ className: 'heading font-family-bold font-14' },
							'SMS BALANCE'
						),
						_react2.default.createElement(
							'div',
							{ className: 'sms-no-container' },
							_react2.default.createElement(
								'p',
								null,
								this.state.smsBalance
							),
							_react2.default.createElement(
								_Button2.default,
								{ onClick: this.openMoreSettingsWindow.bind(this, this.domain + '/sms?workflow=' + WORKFLOW + '&credit=true&client_type=2'), className: 'add-sms', variant: 'contained', color: 'primary' },
								'+ ADD SMS'
							)
						)
					)
				),
				_react2.default.createElement(
					'a',
					{ className: 'color-1789FC ml-10 mt-20 d-inline-block', onClick: this.openMoreSettingsWindow.bind(this, this.domain + '/sms?workflow=' + WORKFLOW + '&client_type=2') },
					'Show More Settings >'
				),
				this.state.showSenderIdModal && this.getShowSenderIdModal()
			);
		}
	}]);
	return SMSSettings;
}(_BaseSettings3.default);

exports.default = (0, _styles.withStyles)(styles)(SMSSettings);