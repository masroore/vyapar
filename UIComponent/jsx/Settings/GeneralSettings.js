Object.defineProperty(exports, "__esModule", {
	value: true
});

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _extends3 = require('babel-runtime/helpers/extends');

var _extends4 = _interopRequireDefault(_extends3);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _SelectDropDown = require('../UIControls/SelectDropDown');

var _SelectDropDown2 = _interopRequireDefault(_SelectDropDown);

var _RadioButtonGroup = require('../UIControls/RadioButtonGroup');

var _RadioButtonGroup2 = _interopRequireDefault(_RadioButtonGroup);

var _NumberTextField = require('../UIControls/NumberTextField');

var _NumberTextField2 = _interopRequireDefault(_NumberTextField);

var _MenuItem = require('@material-ui/core/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _Radio = require('@material-ui/core/Radio');

var _Radio2 = _interopRequireDefault(_Radio);

var _FormControlLabel = require('@material-ui/core/FormControlLabel');

var _FormControlLabel2 = _interopRequireDefault(_FormControlLabel);

var _BaseSettings2 = require('./BaseSettings');

var _BaseSettings3 = _interopRequireDefault(_BaseSettings2);

var _LicenseInfoConstant = require('../../../Constants/LicenseInfoConstant');

var _LicenseInfoConstant2 = _interopRequireDefault(_LicenseInfoConstant);

var _ErrorCode = require('../../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

var _Queries = require('../../../Constants/Queries');

var _Queries2 = _interopRequireDefault(_Queries);

var _LocalStorageHelper = require('../../../Utilities/LocalStorageHelper');

var _LocalStorageHelper2 = _interopRequireDefault(_LocalStorageHelper);

var _SettingsMapping = require('./SettingsMapping');

var _SettingsMapping2 = _interopRequireDefault(_SettingsMapping);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var GeneralSettings = function (_BaseSettings) {
	(0, _inherits3.default)(GeneralSettings, _BaseSettings);

	function GeneralSettings(props) {
		(0, _classCallCheck3.default)(this, GeneralSettings);

		var _this = (0, _possibleConstructorReturn3.default)(this, (GeneralSettings.__proto__ || (0, _getPrototypeOf2.default)(GeneralSettings)).call(this, props));

		_this.toggleAutoLaunch = function () {
			var SettingUtility = require('../../../UIControllers/SettingsUtility');
			SettingUtility.toggleAutoLaunch(_this.state['AUTOLAUNCH']);
		};

		_this.handleFirmChange = function (event) {
			_this.setState({
				selectedFirmId: event.target.value
			});
		};

		_this.moreTransactionCallback = function (key) {
			return function () {
				if (key == _Queries2.default.SETTING_OTHER_INCOME_ENABLED) {
					checkIncomeEnabled();
				}

				if (key == _Queries2.default.SETTING_ORDER_FORM_ENABLED) {
					checkOrderFormEnabled();
				}

				if (key == _Queries2.default.SETTING_DELIVERY_CHALLAN_ENABLED) {
					checkDeliveryChallanEnabled();
				}
				if (key == _Queries2.default.SETTING_ESTIMATE_ENABLED) {
					checkEstimateEnabled();
				}
			};
		};

		_this.handleAutobackCallback = function () {
			var SyncHandler = require('../../../Utilities/SyncHandler.js');
			if (_this.state[_Queries2.default.SETTING_AUTO_BACKUP_ENABLED]) {
				if (SyncHandler.isAdminUser()) {
					var backup = require('../../../BizLogic/BackupDB');
					var Backup = new backup();
					Backup.autoBackup();
				} else {
					ToastHelper.info(_ErrorCode2.default.ERROR_SYNC_AUTO_BACKUP_ADMIN);
					_this.setState((0, _defineProperty3.default)({}, _Queries2.default.SETTING_AUTO_BACKUP_ENABLED, _this.settingCache.isAutoBackupEnabled()));
					MyAnalytics.logSetting((0, _defineProperty3.default)({}, _Queries2.default.SETTING_AUTO_BACKUP_ENABLED, _this.state[_Queries2.default.SETTING_AUTO_BACKUP_ENABLED] ? '1' : '0'));
				}
			} else if (!_this.state[_Queries2.default.SETTING_AUTO_BACKUP_ENABLED]) {
				if (!SyncHandler.isAdminUser()) {
					ToastHelper.info(_ErrorCode2.default.ERROR_SYNC_AUTO_BACKUP_ADMIN);
					_this.setState((0, _defineProperty3.default)({}, _Queries2.default.SETTING_AUTO_BACKUP_ENABLED, _this.settingCache.isAutoBackupEnabled()));
				}
			}
		};

		_this.moreTransactionList = _SettingsMapping2.default.moreGeneralTransactionList;
		_this.deliveryChallanSubSettingsList = _SettingsMapping2.default.deliveryChallanSubSettingsList;
		_this.state = (0, _extends4.default)((0, _defineProperty3.default)({}, _Queries2.default.SETTING_PASSCODE_ENABLED, _this.isChecked(_this.listOfSettings[_Queries2.default.SETTING_PASSCODE_ENABLED])), _this.generalSettingsState());
		_this.decimalFieldRef = _react2.default.createRef();
		return _this;
	}

	(0, _createClass3.default)(GeneralSettings, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.checkPasscodeEnabled();
			this.higlightSearchedSetting();
			this.initializeToolTips();
		}
	}, {
		key: 'generalSettingsState',
		value: function generalSettingsState() {
			var _ref;

			return _ref = {
				selectedFirmId: this.defaultFirm ? this.defaultFirm.getFirmId() : 1
			}, (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_TIN_NUMBER_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_TIN_NUMBER_ENABLED])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_MULTIFIRM_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_MULTIFIRM_ENABLED])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_AUTO_BACKUP_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_AUTO_BACKUP_ENABLED])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_OTHER_INCOME_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_OTHER_INCOME_ENABLED])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_DELIVERY_CHALLAN_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_DELIVERY_CHALLAN_ENABLED])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ORDER_FORM_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ORDER_FORM_ENABLED])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ESTIMATE_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ESTIMATE_ENABLED])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_DELIVERY_CHALLAN_RETURN_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_DELIVERY_CHALLAN_RETURN_ENABLED])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PRINT_AMOUNTS_IN_DELIVERY_CHALLAN, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_AMOUNTS_IN_DELIVERY_CHALLAN])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_AMOUNT_DECIMAL, this.listOfSettings[_Queries2.default.SETTING_AMOUNT_DECIMAL]), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_LAST_BACKUP_TIME, this.convertBackupDate()), (0, _defineProperty3.default)(_ref, 'listOfFirmsFromCache', this.listOfFirmsFromCache), (0, _defineProperty3.default)(_ref, 'decimalPlaceConverted', 0.00000.toFixed(this.listOfSettings[_Queries2.default.SETTING_AMOUNT_DECIMAL])), (0, _defineProperty3.default)(_ref, 'AUTOLAUNCH', localStorage.getItem('autoLaunch') ? localStorage.getItem('autoLaunch') == 1 : true), _ref;
		}
	}, {
		key: 'resetState',
		value: function resetState() {
			this.setState((0, _extends4.default)({}, this.generalSettingsState()));
		}
	}, {
		key: 'checkPasscodeEnabled',
		value: function checkPasscodeEnabled() {
			/* eslint new-cap: ["error", { "newIsCap": false }] */
			var passCode = _LocalStorageHelper2.default.getValue(_LicenseInfoConstant2.default.passCode, true);
			if (passCode && passCode.length > 1) {
				this.setState((0, _defineProperty3.default)({}, _Queries2.default.SETTING_PASSCODE_ENABLED, true));
			} else {
				this.setState((0, _defineProperty3.default)({}, _Queries2.default.SETTING_PASSCODE_ENABLED, false));
			}
		}
	}, {
		key: 'convertBackupDate',
		value: function convertBackupDate() {
			var moment = require('moment');
			var dt = new Date(this.listOfSettings[_Queries2.default.SETTING_LAST_BACKUP_TIME]);
			return moment(dt).format('DD/MM/YYYY | hh:MM A');
		}
	}, {
		key: 'changePasscode',
		value: function changePasscode() {
			var PassCodeUtility = require('../../../UIControllers/PassCodeUtility.js');
			PassCodeUtility.changePasscode();
		}
	}, {
		key: 'setDefaultFirm',
		value: function setDefaultFirm(firmId) {
			var FirmSettingUtility = require('../../../UIControllers/FirmSettingController.js');
			FirmSettingUtility.setDefaultFirm(firmId);
		}
	}, {
		key: 'generateFirmList',
		value: function generateFirmList() {
			var _this2 = this;

			var firmList = [];
			(0, _keys2.default)(this.state.listOfFirmsFromCache).map(function (key, index) {
				var firmItem = _this2.state.listOfFirmsFromCache[key];
				var firmId = firmItem && firmItem.getFirmId() ? firmItem.getFirmId() : 1;
				var firmName = firmItem && firmItem.getFirmName() ? firmItem.getFirmName() : '';
				firmList.push(_react2.default.createElement(
					'div',
					{ key: index, className: 'firm-item d-flex align-items-center justify-content-between ' + (firmId === _this2.state.selectedFirmId ? 'default' : '') },
					_react2.default.createElement(_FormControlLabel2.default, { onClick: _this2.setDefaultFirm.bind(_this2, firmId), value: firmId, control: _react2.default.createElement(_Radio2.default, { color: 'primary' }), label: firmName }),
					_react2.default.createElement(
						'div',
						{ className: 'd-flex align-items-center' },
						firmId === _this2.state.selectedFirmId && _react2.default.createElement(
							'label',
							{ className: 'font-10 default-lbl' },
							'DEFAULT'
						),
						_react2.default.createElement('img', { src: '../inlineSVG/outline-edit-24px.svg', onClick: _this2.addUpdateFirm.bind(_this2, firmId) })
					)
				));
			});
			return _react2.default.createElement(
				_RadioButtonGroup2.default,
				{ key: 1, className: 'firm-list', name: 'firmList', value: this.defaultFirm.getFirmId(), handleChange: this.handleFirmChange },
				firmList
			);
		}
	}, {
		key: 'updateFirmChanges',
		value: function updateFirmChanges() {
			this.setState({
				listOfFirmsFromCache: this.firmCache.getFirmList()
			});
		}
	}, {
		key: 'addUpdateFirm',
		value: function addUpdateFirm(firmId) {
			var FirmSettingUtility = require('../../../UIControllers/FirmSettingController.js');
			if (!firmId) {
				FirmSettingUtility.addNewFirm(this.updateFirmChanges.bind(this));
			} else {
				FirmSettingUtility.updateFirm(firmId, this.updateFirmChanges.bind(this));
			}
		}
	}, {
		key: 'handleScriptCreate',
		value: function handleScriptCreate() {
			this.setState({ scriptLoaded: false });
		}
	}, {
		key: 'handleScriptError',
		value: function handleScriptError() {
			this.setState({ scriptError: true });
		}
	}, {
		key: 'handleScriptLoad',
		value: function handleScriptLoad() {
			this.setState({ scriptLoaded: true });
		}
	}, {
		key: 'enablePasscode',
		value: function enablePasscode() {
			var statusCode = void 0;
			if (!this.state[_Queries2.default.SETTING_PASSCODE_ENABLED]) {
				var PassCodeUtility = require('../../../UIControllers/PassCodeUtility.js');
				PassCodeUtility.enablePasscode();
			} else {
				var conf = confirm('Are you sure you want to remove your passcode?');
				if (conf) {
					var PassCodeHelperUtility = require('../../../Utilities/passCodeHelperUtitlity');
					statusCode = PassCodeHelperUtility.removePasscode();
					if (statusCode == _ErrorCode2.default.ERROR_PASSCODE_UPDATE_SUCCESS) {
						this.setState((0, _defineProperty3.default)({}, _Queries2.default.SETTING_PASSCODE_ENABLED, false));
					} else {
						ToastHelper.error(statusCode);
					}
				}
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var _this3 = this;

			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement(
					'div',
					{ className: 'general-settings' },
					_react2.default.createElement(
						'div',
						{ className: 'row' },
						_react2.default.createElement(
							'div',
							{ className: 'col application-settings' },
							_react2.default.createElement(
								'span',
								{ className: 'heading font-family-bold font-16' },
								'Application'
							),
							_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
							_react2.default.createElement(
								'div',
								{ className: 'setting-item d-flex align-items-center ' + _Queries2.default.SETTING_PASSCODE_ENABLED },
								_react2.default.createElement(
									'label',
									{ className: 'input-checkbox' },
									_react2.default.createElement('input', { type: 'checkbox', onChange: this.enablePasscode.bind(this), checked: this.state[_Queries2.default.SETTING_PASSCODE_ENABLED] }),
									this.checkboxJSX
								),
								_react2.default.createElement(
									'span',
									null,
									_SettingsMapping2.default.miscGeneralSettingsList[_Queries2.default.SETTING_PASSCODE_ENABLED].heading
								),
								this.renderToolTip(_SettingsMapping2.default.miscGeneralSettingsList[_Queries2.default.SETTING_PASSCODE_ENABLED], _Queries2.default.SETTING_PASSCODE_ENABLED),
								this.state[_Queries2.default.SETTING_PASSCODE_ENABLED] && _react2.default.createElement(
									'a',
									{ onClick: this.changePasscode, className: 'font-family-medium link' },
									_SettingsMapping2.default.miscGeneralSettingsList.CHANGE_PASSCODE_TXT.heading
								)
							),
							_react2.default.createElement(
								'div',
								{ className: 'setting-item d-flex align-items-center ' + _Queries2.default.SETTING_CURRENCY_SYMBOL },
								_react2.default.createElement(
									'div',
									{ className: 'setting-heading d-flex align-items-center ' },
									_react2.default.createElement(
										'span',
										null,
										_SettingsMapping2.default.miscGeneralSettingsList[_Queries2.default.SETTING_CURRENCY_SYMBOL].heading
									),
									this.renderToolTip(_SettingsMapping2.default.miscGeneralSettingsList[_Queries2.default.SETTING_CURRENCY_SYMBOL], _Queries2.default.SETTING_CURRENCY_SYMBOL)
								),
								_react2.default.createElement(
									_SelectDropDown2.default,
									{ className: 'remove-underline', handleChange: this.saveSettings.bind(this, _Queries2.default.SETTING_CURRENCY_SYMBOL),
										value: this.listOfSettings[_Queries2.default.SETTING_CURRENCY_SYMBOL], id: 'currenyDd', name: 'currency' },
									this.listOfCurrency.map(function (value, index) {
										return _react2.default.createElement(
											_MenuItem2.default,
											{ key: value, value: value },
											value
										);
									})
								)
							),
							_react2.default.createElement(
								'div',
								{ className: 'setting-item height-72 d-flex align-items-center ' + _Queries2.default.SETTING_AMOUNT_DECIMAL },
								_react2.default.createElement(
									'div',
									{ className: 'setting-heading d-flex align-items-center  mr-10' },
									_react2.default.createElement('span', { dangerouslySetInnerHTML: { __html: _SettingsMapping2.default.miscGeneralSettingsList[_Queries2.default.SETTING_AMOUNT_DECIMAL].heading } }),
									this.renderToolTip(_SettingsMapping2.default.miscGeneralSettingsList[_Queries2.default.SETTING_AMOUNT_DECIMAL], _Queries2.default.SETTING_AMOUNT_DECIMAL)
								),
								_react2.default.createElement(_NumberTextField2.default, {
									ref: this.decimalFieldRef,
									handleBlur: this.handleDecimalPlace(_Queries2.default.SETTING_AMOUNT_DECIMAL),
									width: 100,
									value: this.state[_Queries2.default.SETTING_AMOUNT_DECIMAL],
									className: 'd-inline-flex justify-content-center w-20-px'
								}),
								_react2.default.createElement(
									'span',
									{ className: 'font-12 color-707070' },
									'e.g. ',
									this.state.decimalPlaceConverted
								)
							),
							_react2.default.createElement(
								'div',
								{ className: 'setting-item d-flex align-items-center AUTOLAUNCH' },
								_react2.default.createElement(
									'label',
									{ className: 'input-checkbox' },
									_react2.default.createElement('input', { type: 'checkbox', onChange: this.toggleCheckbox('AUTOLAUNCH', true, this.toggleAutoLaunch), checked: this.state['AUTOLAUNCH'] }),
									this.checkboxJSX
								),
								_react2.default.createElement(
									'span',
									null,
									_SettingsMapping2.default.miscGeneralSettingsList.AUTOLAUNCH.heading
								),
								this.renderToolTip(_SettingsMapping2.default.miscGeneralSettingsList.AUTOLAUNCH)
							),
							_react2.default.createElement(
								'div',
								{ className: 'setting-item d-flex align-items-center ' + _Queries2.default.SETTING_TIN_NUMBER_ENABLED },
								_react2.default.createElement(
									'label',
									{ className: 'input-checkbox' },
									_react2.default.createElement('input', { type: 'checkbox', onChange: this.toggleCheckbox(_Queries2.default.SETTING_TIN_NUMBER_ENABLED), checked: this.state[_Queries2.default.SETTING_TIN_NUMBER_ENABLED] }),
									this.checkboxJSX
								),
								_react2.default.createElement(
									'span',
									null,
									_SettingsMapping2.default.miscGeneralSettingsList[_Queries2.default.SETTING_TIN_NUMBER_ENABLED].getTINNumberSettingObj().heading
								),
								this.renderToolTip(_SettingsMapping2.default.miscGeneralSettingsList[_Queries2.default.SETTING_TIN_NUMBER_ENABLED].getTINNumberSettingObj())
							)
						),
						_react2.default.createElement(
							'div',
							{ className: 'col' },
							_react2.default.createElement(
								'div',
								{ className: 'd-flex justify-content-between' },
								_react2.default.createElement(
									'div',
									{ className: 'd-flex align-items-center' },
									_react2.default.createElement(
										'label',
										{ className: 'input-checkbox' },
										_react2.default.createElement('input', { type: 'checkbox', onChange: this.toggleCheckbox(_Queries2.default.SETTING_MULTIFIRM_ENABLED), checked: this.state[_Queries2.default.SETTING_MULTIFIRM_ENABLED] }),
										this.checkboxJSX
									),
									_react2.default.createElement(
										'span',
										{ className: 'font-family-bold font-16' },
										'Multi Firm'
									)
								),
								this.state[_Queries2.default.SETTING_MULTIFIRM_ENABLED] && _react2.default.createElement(
									'div',
									{ className: 'd-flex align-items-center' },
									_react2.default.createElement(
										'a',
										{ className: 'font-family-medium link', onClick: this.addUpdateFirm.bind(this, null) },
										'+ ',
										_SettingsMapping2.default.miscGeneralSettingsList.ADD_FIRM.heading
									),
									this.renderToolTip(_SettingsMapping2.default.miscGeneralSettingsList.ADD_FIRM)
								)
							),
							_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
							this.generateFirmList()
						),
						_react2.default.createElement(
							'div',
							{ className: 'col' },
							_react2.default.createElement(
								'span',
								{ className: 'heading font-family-bold font-16' },
								'Backup'
							),
							_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
							_react2.default.createElement(
								'div',
								{ className: 'setting-item d-flex align-items-center AUTO_BACKUP' },
								_react2.default.createElement(
									'label',
									{ className: 'input-checkbox' },
									_react2.default.createElement('input', { type: 'checkbox', onChange: this.toggleCheckbox(_Queries2.default.SETTING_AUTO_BACKUP_ENABLED, true, this.handleAutobackCallback), checked: this.state[_Queries2.default.SETTING_AUTO_BACKUP_ENABLED] }),
									this.checkboxJSX
								),
								_react2.default.createElement(
									'span',
									{ className: '' },
									_SettingsMapping2.default.miscGeneralSettingsList[_Queries2.default.SETTING_AUTO_BACKUP_ENABLED].heading
								),
								this.renderToolTip(_SettingsMapping2.default.miscGeneralSettingsList[_Queries2.default.SETTING_AUTO_BACKUP_ENABLED], _Queries2.default.SETTING_AUTO_BACKUP_ENABLED)
							),
							_react2.default.createElement(
								'div',
								null,
								_react2.default.createElement(
									'div',
									{ className: 'ml-30 font-12 backup-sub-settings AUTO_BACKUP_EVERY SIGNED_IN_AS' },
									this.loggedInAs && _react2.default.createElement(
										'p',
										null,
										_SettingsMapping2.default.miscGeneralSettingsList.SIGNED_IN_AS.heading,
										' ',
										this.loggedInAs
									),
									_react2.default.createElement(
										'div',
										{ className: 'setting-item d-flex align-items-center' },
										this.state[_Queries2.default.SETTING_LAST_BACKUP_TIME] && _react2.default.createElement(
											_react2.default.Fragment,
											null,
											_react2.default.createElement(
												'p',
												null,
												_SettingsMapping2.default.miscGeneralSettingsList.LAST_BACKUP.heading,
												' ',
												this.state[_Queries2.default.SETTING_LAST_BACKUP_TIME]
											),
											this.renderToolTip(_SettingsMapping2.default.miscGeneralSettingsList.LAST_BACKUP)
										)
									),
									this.state[_Queries2.default.SETTING_AUTO_BACKUP_ENABLED] && _react2.default.createElement(
										'div',
										{ className: 'setting-item d-flex align-items-center' },
										_react2.default.createElement(
											'span',
											{ className: 'mr-10' },
											_SettingsMapping2.default.miscGeneralSettingsList[_Queries2.default.SETTING_AUTO_BACKUP_DURATION_DAYS].heading
										),
										_react2.default.createElement(_NumberTextField2.default, {
											minValue: 1,
											maxValue: 999,
											maxLength: 3,
											handleBlur: this.handleNumber(_Queries2.default.SETTING_AUTO_BACKUP_DURATION_DAYS),
											value: this.listOfSettings[_Queries2.default.SETTING_AUTO_BACKUP_DURATION_DAYS], className: 'd-inline-flex justify-content-center w-20-px'
										}),
										_react2.default.createElement(
											'span',
											null,
											'days'
										),
										this.renderToolTip(_SettingsMapping2.default.miscGeneralSettingsList[_Queries2.default.SETTING_AUTO_BACKUP_DURATION_DAYS], _Queries2.default.SETTING_AUTO_BACKUP_DURATION_DAYS)
									)
								)
							),
							_react2.default.createElement('div', { className: 'border-bottom-1 mt-30 mb-10' }),
							_react2.default.createElement(
								'div',
								{ className: 'setting-item height-72 d-flex align-items-center MANUAL_BACKUP_REMINDER' },
								_react2.default.createElement(
									'span',
									{ className: 'mr-10' },
									_SettingsMapping2.default.miscGeneralSettingsList[_Queries2.default.SETTING_BACKUP_REMINDER_DAYS].heading
								),
								_react2.default.createElement(_NumberTextField2.default, {
									handleBlur: this.handleNumber(_Queries2.default.SETTING_BACKUP_REMINDER_DAYS),
									maxLength: 3,
									minValue: 1,
									maxValue: 999,
									value: this.listOfSettings[_Queries2.default.SETTING_BACKUP_REMINDER_DAYS],
									className: 'd-inline-flex justify-content-center w-20-px'
								}),
								_react2.default.createElement(
									'span',
									null,
									'days'
								),
								this.renderToolTip(_SettingsMapping2.default.miscGeneralSettingsList[_Queries2.default.SETTING_BACKUP_REMINDER_DAYS], _Queries2.default.SETTING_BACKUP_REMINDER_DAYS)
							)
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'row mt-10' },
						_react2.default.createElement(
							'div',
							{ className: 'col-sm-4' },
							_react2.default.createElement(
								'span',
								{ className: 'heading font-family-bold font-16' },
								'More Transactions'
							),
							_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
							(0, _keys2.default)(this.moreTransactionList).map(function (key) {
								return _react2.default.createElement(
									'div',
									{ key: key },
									_react2.default.createElement(
										'div',
										{ className: 'setting-item d-flex align-items-center ' + key },
										_react2.default.createElement(
											'label',
											{ className: 'input-checkbox' },
											_react2.default.createElement('input', { type: 'checkbox', onChange: _this3.toggleCheckbox(key, true, _this3.moreTransactionCallback(key)), checked: _this3.state[key] }),
											_this3.checkboxJSX
										),
										_react2.default.createElement('span', { dangerouslySetInnerHTML: { __html: _this3.moreTransactionList[key].heading } }),
										_this3.renderToolTip(_this3.moreTransactionList[key], key)
									),
									key === _Queries2.default.SETTING_DELIVERY_CHALLAN_ENABLED && _this3.state[_Queries2.default.SETTING_DELIVERY_CHALLAN_ENABLED] && _react2.default.createElement(
										'div',
										{ className: 'delivery-challan-settings' },
										(0, _keys2.default)(_this3.deliveryChallanSubSettingsList).map(function (key) {
											return _react2.default.createElement(
												'div',
												{ key: key, className: 'ml-20 setting-item d-flex align-items-center ' + key },
												_react2.default.createElement(
													'label',
													{ className: 'input-checkbox' },
													_react2.default.createElement('input', { type: 'checkbox', onChange: _this3.toggleCheckbox(key), checked: _this3.state[key] }),
													_this3.checkboxJSX
												),
												_react2.default.createElement('span', { dangerouslySetInnerHTML: { __html: _this3.deliveryChallanSubSettingsList[key].heading } }),
												_this3.renderToolTip(_this3.deliveryChallanSubSettingsList[key], key)
											);
										})
									)
								);
							})
						)
					)
				)
			);
		}
	}]);
	return GeneralSettings;
}(_BaseSettings3.default);

exports.default = GeneralSettings;