Object.defineProperty(exports, "__esModule", {
	value: true
});

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _BasePrintSettings2 = require('./BasePrintSettings');

var _BasePrintSettings3 = _interopRequireDefault(_BasePrintSettings2);

var _Queries = require('../../../Constants/Queries');

var _Queries2 = _interopRequireDefault(_Queries);

var _Defaults = require('../../../Constants/Defaults');

var _Defaults2 = _interopRequireDefault(_Defaults);

var _TextField = require('@material-ui/core/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _styles = require('@material-ui/core/styles');

var _Paper = require('@material-ui/core/Paper');

var _Paper2 = _interopRequireDefault(_Paper);

require('@devexpress/dx-react-grid');

var _MyDouble = require('../../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _TransactionPrintSettings = require('../../../ReportHTMLGenerator/TransactionPrintSettings');

var _TransactionPrintSettings2 = _interopRequireDefault(_TransactionPrintSettings);

var _dxReactGridMaterialUi = require('@devexpress/dx-react-grid-material-ui');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
	return {
		'@global': {
			'.item-table-customization .settings-container th div[class*="resizeHandleLine"]': {
				opacity: 'inherit',
				height: '100%',
				top: 0
			},
			'.item-table-customization .settings-container th div[class*="resizeHandleSecondLine"]': {
				display: 'none'
			},
			'.item-table-customization .settings-container th': {
				backgroundColor: function backgroundColor(props) {
					return props.headerBackgroundColor.toLowerCase() === '#ffffff' ? '#ffffff' : props.headerBackgroundColor;
				},
				color: function color(props) {
					return props.headerBackgroundColor.toLowerCase() === '#ffffff' ? 'black' : '#ffffff';
				}
			},
			'td.MuiTableCell-body': {
				borderRight: '1px solid #DDDDDD'
			},
			'.resizeHandler:before': {
				display: 'inline-block',
				width: 16,
				height: 16,
				marginRight: 5,
				content: "",
				background: 'url(../inlineSVG/baseline-arrow_right_alt-24px.svg) no-repeat 0 0',
				backgroundSize: '100%'
			},
			'.dialogButton': {
				marginLeft: 'auto'
			}
		}
	};
};

var ItemTableCustomization = function (_BasePrintSettings) {
	(0, _inherits3.default)(ItemTableCustomization, _BasePrintSettings);

	function ItemTableCustomization(props) {
		(0, _classCallCheck3.default)(this, ItemTableCustomization);

		var _this = (0, _possibleConstructorReturn3.default)(this, (ItemTableCustomization.__proto__ || (0, _getPrototypeOf2.default)(ItemTableCustomization)).call(this, props));

		_this.itemTableInitialization = function () {
			var _this$additionalItemC;

			_this.itemRelatedColumns = [{ key: _Queries2.default.SETTING_PRINT_SINNUMBER_ENABLED, control: 'textbox', controlKey: _Queries2.default.SETTING_SI_COLUMNHEADER_VALUE, value: 'SI No.' }, { key: _Queries2.default.SETTING_ITEMNAME_COLUMNHEADER_VALUE, control: 'textbox', value: 'Item Name', showCheckbox: false }, { key: _Queries2.default.SETTING_PRINT_ITEMCODE_ENABLED, control: 'textbox', controlKey: _Queries2.default.SETTING_ITEMCODE_COLUMNHEADER_VALUE, value: 'Item Code' }, { key: _Queries2.default.SETTING_PRINT_HSNCODE_ENABLED, control: 'textbox', controlKey: _Queries2.default.SETTING_HSNCODE_COLUMNHEADER_VALUE, value: 'HSN/SAC' }];
			_this.additionalItemColumns = (_this$additionalItemC = {}, (0, _defineProperty3.default)(_this$additionalItemC, _Queries2.default.SETTING_PRINT_BATCHNO_ENABLED, 'Batch Number'), (0, _defineProperty3.default)(_this$additionalItemC, _Queries2.default.SETTING_PRINT_ITEMEXPIRYDATE_ENABLED, 'Exp Date'), (0, _defineProperty3.default)(_this$additionalItemC, _Queries2.default.SETTING_PRINT_ITEMMANUFACTURINGDATE_ENABLED, 'Mfg Date'), (0, _defineProperty3.default)(_this$additionalItemC, _Queries2.default.SETTING_PRINT_ITEMMRP_ENABLED, 'MRP'), (0, _defineProperty3.default)(_this$additionalItemC, _Queries2.default.SETTING_PRINT_ITEMSIZE_ENABLED, 'Size'), (0, _defineProperty3.default)(_this$additionalItemC, _Queries2.default.SETTING_PRINT_ITEMSERIALNUMBER_ENABLED, 'Serial Number'), (0, _defineProperty3.default)(_this$additionalItemC, _Queries2.default.SETTING_PRINT_ITEMDESCRIPTION_ENABLED, 'Description'), (0, _defineProperty3.default)(_this$additionalItemC, _Queries2.default.SETTING_PRINT_ITEMCOUNT_ENABLED, 'Count'), _this$additionalItemC);
			_this.amountTotalAndTaxes = [{ key: _Queries2.default.SETTING_PRINT_ITEMQUANTITY_ENABLED, control: 'textbox', controlKey: _Queries2.default.SETTING_QUNATITY_COLUMNHEADER_VALUE, value: 'Quantity' }, { key: _Queries2.default.SETTING_PRINT_ITEMUNIT_ENABLED, control: 'textbox', controlKey: _Queries2.default.SETTING_ITEMUNIT_COLUMNHEADER_VALUE, value: 'Unit Of Measurement' }, { key: _Queries2.default.SETTING_PRINT_ITEMPRICE_ENABLED, control: 'textbox', controlKey: _Queries2.default.SETTING_PRICEPERUNIT_COLUMNHEADER_VALUE, value: 'Price/Unit' }, { key: _Queries2.default.SETTING_PRINT_ITEMDISCOUNTAMOUNT_ENABLED, control: 'textbox', controlKey: _Queries2.default.SETTING_DISCOUNT_COLUMNHEADER_VALUE, value: 'Discount/Amount' }, { key: _Queries2.default.SETTING_PRINT_ITEMDISCOUNTPERCENTAGE_ENABLED, control: 'checkbox', value: 'Discount Percent' }, { key: _Queries2.default.SETTING_PRINT_TAXABLE_ITEMPRICE_ENABLED, control: 'textbox', controlKey: _Queries2.default.SETTING_TAXABLE_PRICE_COLUMNHEADER_VALUE, value: 'Taxable Price/Unit' }, { key: _Queries2.default.SETTING_PRINT_ITEMTAXAMOUNT_ENABLED, control: 'checkbox', value: 'Tax Amount' }, { key: _Queries2.default.SETTING_PRINT_ITEMTAXPERCENT_ENABLED, control: 'checkbox', value: 'Tax Percent' }, { key: _Queries2.default.SETTING_TAXABLEAMOUNT_COLUMNHEADER_VALUE, control: 'textbox', value: 'Taxable Amount', showCheckbox: false }, { key: _Queries2.default.SETTING_ADDITIONALCESS_COLUMNHEADER_VALUE, control: 'textbox', value: 'Additional Cess', showCheckbox: false }, { key: _Queries2.default.SETTING_PRINT_FINALITEMPRICE_ENABLED, control: 'textbox', controlKey: _Queries2.default.SETTING_FINALITEMPRICE_COLUMNHEADER_VALUE, value: 'Final Price/Unit' }, { key: _Queries2.default.SETTING_PRINT_ITEMTOTALAMOUNT_ENABLED, control: 'textbox', controlKey: _Queries2.default.SETTING_TOTALAMOUNT_COLUMNHEADER_VALUE, value: 'Total Amount' }];

			return (0, _extends3.default)({}, _this.initHeaderColumnsEnabled(), {
				defaultColumnWidths: _this.initTableColumnWidth(),
				rows: _this.sampleRowsForItemTable()
			});
		};

		_this.initColumnsForItemTable = function () {
			var columns = [];

			if (_this.state[_Queries2.default.SETTING_PRINT_SINNUMBER_ENABLED]) {
				columns.push({ name: _Queries2.default.SETTING_SI_COLUMNRATIO_VALUE, title: _this.state[_Queries2.default.SETTING_SI_COLUMNHEADER_VALUE] || ' ' });
			}

			if (_TransactionPrintSettings2.default.printItemNameInInvoicePrint()) {
				columns.push({ name: _Queries2.default.SETTING_ITEMNAME_COLUMNRATIO_VALUE, title: _this.state[_Queries2.default.SETTING_ITEMNAME_COLUMNHEADER_VALUE] || ' ' });
			}

			if (_this.state[_Queries2.default.SETTING_PRINT_ITEMCODE_ENABLED]) {
				columns.push({ name: _Queries2.default.SETTING_ITEM_CODE_COLUMNRATIO_VALUE, title: _this.state[_Queries2.default.SETTING_ITEMCODE_COLUMNHEADER_VALUE] || ' ' });
			}

			if (_this.state[_Queries2.default.SETTING_PRINT_HSNCODE_ENABLED]) {
				columns.push({ name: _Queries2.default.SETTING_HSNCODE_COLUMNRATIO_VALUE, title: _this.state[_Queries2.default.SETTING_HSNCODE_COLUMNHEADER_VALUE] || ' ' });
			}

			if (_this.state[_Queries2.default.SETTING_PRINT_ITEMCOUNT_ENABLED]) {
				columns.push({ name: _Queries2.default.SETTING_ITEMCOUNT_COLUMNRATIO_VALUE, title: _this.state[_Queries2.default.SETTING_ITEM_COUNT_VALUE] || ' ' });
			}

			if (_this.state[_Queries2.default.SETTING_PRINT_BATCHNO_ENABLED]) {
				columns.push({ name: _Queries2.default.SETTING_BATCHNO_COLUMNRATIO_VALUE, title: _this.state[_Queries2.default.SETTING_ITEM_BATCH_NUMBER_VALUE] || ' ' });
			}

			if (_this.state[_Queries2.default.SETTING_PRINT_ITEMEXPIRYDATE_ENABLED]) {
				columns.push({ name: _Queries2.default.SETTING_ITEMEXPIRYDATE_COLUMNRATIO_VALUE, title: _this.state[_Queries2.default.SETTING_ITEM_EXPIRY_DATE_VALUE] || ' ' });
			}

			if (_this.state[_Queries2.default.SETTING_PRINT_ITEMMANUFACTURINGDATE_ENABLED]) {
				columns.push({ name: _Queries2.default.SETTING_ITEMMANUFACTURINGDATE_COLUMNRATIO_VALUE, title: _this.state[_Queries2.default.SETTING_ITEM_MANUFACTURING_DATE_VALUE] || ' ' });
			}

			if (_this.state[_Queries2.default.SETTING_PRINT_ITEMMRP_ENABLED]) {
				columns.push({ name: _Queries2.default.SETTING_ITEMMRP_COLUMNRATIO_VALUE, title: _this.state[_Queries2.default.SETTING_ITEM_MRP_VALUE] || ' ' });
			}

			if (_this.state[_Queries2.default.SETTING_PRINT_ITEMSIZE_ENABLED]) {
				columns.push({ name: _Queries2.default.SETTING_ITEMSIZE_COLUMNRATIO_VALUE, title: _this.state[_Queries2.default.SETTING_ITEM_SIZE_VALUE] || ' ' });
			}

			if (_this.state[_Queries2.default.SETTING_PRINT_ITEMQUANTITY_ENABLED]) {
				columns.push({ name: _Queries2.default.SETTING_QUANTITY_COLUMNRATIO_VALUE, title: _this.state[_Queries2.default.SETTING_QUNATITY_COLUMNHEADER_VALUE] || ' ' });
			}

			if (_this.state[_Queries2.default.SETTING_PRINT_ITEMPRICE_ENABLED]) {
				columns.push({ name: _Queries2.default.SETTING_PRICEPERUNIT_COLUMNRATIO_VALUE, title: _this.state[_Queries2.default.SETTING_PRICEPERUNIT_COLUMNHEADER_VALUE] || ' ' });
			}

			if (_this.state[_Queries2.default.SETTING_PRINT_ITEMDISCOUNTAMOUNT_ENABLED] || _this.state[_Queries2.default.SETTING_PRINT_ITEMDISCOUNTPERCENTAGE_ENABLED]) {
				columns.push({ name: _Queries2.default.SETTING_DISCOUNT_COLUMNRATIO_VALUE, title: _this.state[_Queries2.default.SETTING_DISCOUNT_COLUMNHEADER_VALUE] || ' ' });
			}

			if (_this.state[_Queries2.default.SETTING_PRINT_TAXABLE_ITEMPRICE_ENABLED]) {
				columns.push({ name: _Queries2.default.SETTING_TAXABLE_PRICE_COLUMNRATIO_VALUE, title: _this.state[_Queries2.default.SETTING_TAXABLE_PRICE_COLUMNHEADER_VALUE] || ' ' });
			}

			if (_this.state[_Queries2.default.SETTING_PRINT_ITEMTAXPERCENT_ENABLED] || _this.state[_Queries2.default.SETTING_PRINT_ITEMTAXAMOUNT_ENABLED]) {
				columns.push({ name: _Queries2.default.SETTING_TAXABLEAMOUNT_COLUMNRATIO_VALUE, title: _this.state[_Queries2.default.SETTING_TAXABLEAMOUNT_COLUMNHEADER_VALUE] || ' ' });
			}

			if (_this.state[_Queries2.default.SETTING_PRINT_FINALITEMPRICE_ENABLED]) {
				columns.push({ name: _Queries2.default.SETTING_FINALITEMPRICE_COLUMNRATIO_VALUE, title: _this.state[_Queries2.default.SETTING_FINALITEMPRICE_COLUMNHEADER_VALUE] || ' ' });
			}

			if (_this.state[_Queries2.default.SETTING_PRINT_ITEMTOTALAMOUNT_ENABLED]) {
				columns.push({ name: _Queries2.default.SETTING_TOTALAMOUNT_COLUMNRATIO_VALUE, title: _this.state[_Queries2.default.SETTING_TOTALAMOUNT_COLUMNHEADER_VALUE] || ' ' });
			}

			return columns;
		};

		_this.handleCheckboxCallback = function (key) {
			return function () {
				_this.saveSettingsObj[key] = _this.checkboxValueForSave(_this.state[key]);
				_this.resetItemTableColumns();
			};
		};

		_this.saveItemTableChanges = function () {
			if (_this.itemTableList) {
				_this.itemTableList.forEach(function (setting) {
					_this.saveSettingsObj[setting.columnName] = _MyDouble2.default.roundOffToNDecimalPlaces(setting.width / _this.itemTableColumnWidthConstant, 2);
				});
			}

			_this.saveMultipleSettings(_this.saveSettingsObj, true);
			if (_this.props.callback && typeof _this.props.callback === 'function') {
				_this.props.callback();
			}
		};

		_this.onColWidthChange = function (itemTableList) {
			_this.itemTableList = itemTableList;
		};

		_this.itemTableColumnWidthConstant = 75;
		_this.saveSettingsObj = {};
		_this.state = (0, _extends3.default)({}, _this.initHeaderValues(), _this.itemTableInitialization());
		_this.state = (0, _extends3.default)({}, _this.state, {
			columns: _this.initColumnsForItemTable()
		});
		return _this;
	}
	// display: inline-block;
	// width: 16px;
	// height: 16px;
	// margin-right: 5px;
	// content: "";
	// background: url(../inlineSVG/baseline-arrow_right_alt-24px.svg) no-repeat 0 0;
	// background-size: 100%;


	(0, _createClass3.default)(ItemTableCustomization, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			// let resizingColumns = document.querySelectorAll('[class*="ResizingControl-resizeHandle"]');
			// for(let col in resizingColumns){
			// 	resizingColumns[col].classList.add('resizeHandler');
			// }
			$('th [class*="ResizingControl-resizeHandle"]').addClass('resizeHandler');
		}

		// handleTxtBox(key) {
		// 	this.saveSettingsObj[key] = this.state[key];
		// 	this.resetItemTableColumns();
		// }

	}, {
		key: 'renderItemTableCustomization',
		value: function renderItemTableCustomization(list) {
			var _this2 = this;

			var getControlKey = function getControlKey(item) {
				return item.showCheckbox === false ? item.key : item.controlKey;
			};
			var handleTxtBox = function handleTxtBox() {
				return _this2.resetItemTableColumns.bind(_this2);
			};
			var handleOnBlurTxtBox = function handleOnBlurTxtBox(item) {
				var key = getControlKey(item);
				var txtBoxValue = _this2.state[key];
				if (txtBoxValue) {
					_this2.saveSettingsObj[key] = txtBoxValue;
				} else {
					delete _this2.saveSettingsObj[key];
				}
			};
			var itemTableColJSX = list.map(function (item) {
				return _react2.default.createElement(
					'div',
					{ key: item.key, className: (item.control === 'textbox' ? 'height-66' : '') + ' setting-item d-flex align-items-center' },
					_react2.default.createElement(
						'label',
						{ className: 'input-checkbox mr-10' },
						_react2.default.createElement('input', { type: 'checkbox', disabled: item.showCheckbox === false,
							checked: item.showCheckbox === false ? true : _this2.state[item.key],
							onChange: _this2.toggleCheckbox(item.key, false, _this2.handleCheckboxCallback(item.key)) }),
						_this2.checkboxJSX
					),
					_react2.default.createElement(
						'div',
						null,
						item.control === 'checkbox' && _react2.default.createElement(
							'span',
							{ className: '' },
							item.value
						)
					),
					item.control === 'textbox' && _react2.default.createElement(_TextField2.default, {
						disabled: item.showCheckbox === false ? false : !_this2.state[item.key],
						label: item.value,
						margin: 'dense',
						variant: 'outlined',
						className: '',
						value: _this2.state[item.showCheckbox === false ? item.key : item.controlKey],
						onChange: _this2.updateInputChange(item.showCheckbox === false ? item.key : item.controlKey, false, handleTxtBox()),
						onBlur: handleOnBlurTxtBox(item)
					})
				);
			});

			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				itemTableColJSX
			);
		}
	}, {
		key: 'initHeaderColumnsEnabled',
		value: function initHeaderColumnsEnabled() {
			var _ref;

			return _ref = {}, (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PRINT_SINNUMBER_ENABLED, _TransactionPrintSettings2.default.printSINumberColumnInInvoicePrint()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PRINT_ITEMCODE_ENABLED, _TransactionPrintSettings2.default.printItemCodeInInvoicePrint()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PRINT_HSNCODE_ENABLED, _TransactionPrintSettings2.default.printHSNCodeInInvoicePrint()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PRINT_BATCHNO_ENABLED, _TransactionPrintSettings2.default.printItemBatchInInvoicePrint()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PRINT_ITEMEXPIRYDATE_ENABLED, _TransactionPrintSettings2.default.printItemExpiryDateInInvoicePrint()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PRINT_ITEMMANUFACTURINGDATE_ENABLED, _TransactionPrintSettings2.default.printItemManufacturingDateInInvoicePrint()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PRINT_ITEMMRP_ENABLED, _TransactionPrintSettings2.default.printItemMRPInInvoicePrint()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PRINT_ITEMSIZE_ENABLED, _TransactionPrintSettings2.default.printItemSizeInInvoicePrint()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PRINT_ITEMSERIALNUMBER_ENABLED, _TransactionPrintSettings2.default.printItemSerialNumberInInvoicePrint()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PRINT_ITEMDESCRIPTION_ENABLED, _TransactionPrintSettings2.default.printItemDescriptionInInvoicePrint()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PRINT_ITEMCOUNT_ENABLED, _TransactionPrintSettings2.default.printItemCountInInvoicePrint()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PRINT_ITEMQUANTITY_ENABLED, _TransactionPrintSettings2.default.printItemQuantityInInvoicePrint()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PRINT_ITEMUNIT_ENABLED, _TransactionPrintSettings2.default.printItemUnitInInvoicePrint()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PRINT_ITEMPRICE_ENABLED, _TransactionPrintSettings2.default.printItemPriceInInvoicePrint()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PRINT_ITEMDISCOUNTAMOUNT_ENABLED, _TransactionPrintSettings2.default.printItemDiscountAmountInInvoicePrint()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PRINT_ITEMDISCOUNTPERCENTAGE_ENABLED, _TransactionPrintSettings2.default.printItemDiscountPercentageInInvoicePrint()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PRINT_TAXABLE_ITEMPRICE_ENABLED, _TransactionPrintSettings2.default.printTaxableItemPriceInInvoicePrint()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PRINT_ITEMTAXAMOUNT_ENABLED, _TransactionPrintSettings2.default.printItemTaxAmountInInvoicePrint()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PRINT_ITEMTAXPERCENT_ENABLED, _TransactionPrintSettings2.default.printItemTaxPercentInInvoicePrint()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PRINT_FINALITEMPRICE_ENABLED, _TransactionPrintSettings2.default.printTaxInclusiveItemPriceInInvoicePrint()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PRINT_ITEMTOTALAMOUNT_ENABLED, _TransactionPrintSettings2.default.printItemTotalAmountInInvoicePrint()), _ref;
		}
	}, {
		key: 'initHeaderValues',
		value: function initHeaderValues() {
			var _ref2;

			return _ref2 = {}, (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_SI_COLUMNHEADER_VALUE, this.listOfSettings[_Queries2.default.SETTING_SI_COLUMNHEADER_VALUE] || _Defaults2.default.DEFAULT_SI_COLUMNHEADER_VALUE), (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_ITEMNAME_COLUMNHEADER_VALUE, this.listOfSettings[_Queries2.default.SETTING_ITEMNAME_COLUMNHEADER_VALUE] || _Defaults2.default.DEFAULT_ITEMNAME_COLUMNHEADER_VALUE), (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_HSNCODE_COLUMNHEADER_VALUE, this.listOfSettings[_Queries2.default.SETTING_HSNCODE_COLUMNHEADER_VALUE] || _Defaults2.default.DEFAULT_HSNCODE_COLUMNHEADER_VALUE), (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_ITEMCODE_COLUMNHEADER_VALUE, this.listOfSettings[_Queries2.default.SETTING_ITEMCODE_COLUMNHEADER_VALUE] || _Defaults2.default.DEFAULT_ITEMCODE_COLUMNHEADER_VALUE), (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_PRICEPERUNIT_COLUMNHEADER_VALUE, this.listOfSettings[_Queries2.default.SETTING_PRICEPERUNIT_COLUMNHEADER_VALUE] || _Defaults2.default.DEFAULT_PRICEPERUNIT_COLUMNHEADER_VALUE), (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_QUNATITY_COLUMNHEADER_VALUE, this.listOfSettings[_Queries2.default.SETTING_QUNATITY_COLUMNHEADER_VALUE] || _Defaults2.default.DEFAULT_QUNATITY_COLUMNHEADER_VALUE), (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_ITEMUNIT_COLUMNHEADER_VALUE, this.listOfSettings[_Queries2.default.SETTING_ITEMUNIT_COLUMNHEADER_VALUE] || _Defaults2.default.DEFAULT_ITEMUNIT_COLUMNHEADER_VALUE), (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_DISCOUNT_COLUMNHEADER_VALUE, this.listOfSettings[_Queries2.default.SETTING_DISCOUNT_COLUMNHEADER_VALUE] || _Defaults2.default.DEFAULT_DISCOUNT_COLUMNHEADER_VALUE), (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_TAXABLE_PRICE_COLUMNHEADER_VALUE, this.listOfSettings[_Queries2.default.SETTING_TAXABLE_PRICE_COLUMNHEADER_VALUE] || _Defaults2.default.DEFAULT_TAXABLE_PRICE_COLUMNHEADER_VALUE), (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_TAXABLEAMOUNT_COLUMNHEADER_VALUE, this.listOfSettings[_Queries2.default.SETTING_TAXABLEAMOUNT_COLUMNHEADER_VALUE] || _Defaults2.default.DEFAULT_TAXABLEAMOUNT_COLUMNHEADER_VALUE), (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_ADDITIONALCESS_COLUMNHEADER_VALUE, this.listOfSettings[_Queries2.default.SETTING_ADDITIONALCESS_COLUMNHEADER_VALUE] || _Defaults2.default.DEFAULT_ADDITIONALCESS_COLUMNHEADER_VALUE), (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_FINALITEMPRICE_COLUMNHEADER_VALUE, this.listOfSettings[_Queries2.default.SETTING_FINALITEMPRICE_COLUMNHEADER_VALUE] || _Defaults2.default.DEFAULT_FINAL_ITEM_PRICE_COLUMNHEADER_VALUE), (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_TOTALAMOUNT_COLUMNHEADER_VALUE, this.listOfSettings[_Queries2.default.SETTING_TOTALAMOUNT_COLUMNHEADER_VALUE] || _Defaults2.default.DEFAULT_TOTAL_AMOUNT_COLUMNHEADER_VALUE), (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_ITEM_COUNT_VALUE, this.settingCache.getAdditionalItemDetailsHeaderValue(_Queries2.default.SETTING_ITEM_COUNT_VALUE)), (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_ITEM_BATCH_NUMBER_VALUE, this.settingCache.getAdditionalItemDetailsHeaderValue(_Queries2.default.SETTING_ITEM_BATCH_NUMBER_VALUE)), (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_ITEM_EXPIRY_DATE_VALUE, this.settingCache.getAdditionalItemDetailsHeaderValue(_Queries2.default.SETTING_ITEM_EXPIRY_DATE_VALUE)), (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_ITEM_MANUFACTURING_DATE_VALUE, this.settingCache.getAdditionalItemDetailsHeaderValue(_Queries2.default.SETTING_ITEM_MANUFACTURING_DATE_VALUE)), (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_ITEM_MRP_VALUE, this.settingCache.getAdditionalItemDetailsHeaderValue(_Queries2.default.SETTING_ITEM_MRP_VALUE)), (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_ITEM_SIZE_VALUE, this.settingCache.getAdditionalItemDetailsHeaderValue(_Queries2.default.SETTING_ITEM_SIZE_VALUE)), (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_ITEM_SERIAL_NUMBER_VALUE, this.settingCache.getAdditionalItemDetailsHeaderValue(_Queries2.default.SETTING_ITEM_SERIAL_NUMBER_VALUE)), (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_ITEM_DESCRIPTION_VALUE, this.settingCache.getAdditionalItemDetailsHeaderValue(_Queries2.default.SETTING_ITEM_DESCRIPTION_VALUE)), _ref2;
		}
	}, {
		key: 'resetItemTableColumns',
		value: function resetItemTableColumns() {
			var _setState;

			this.setState((_setState = {}, (0, _defineProperty3.default)(_setState, _Queries2.default.SETTING_SI_COLUMNHEADER_VALUE, this.state[_Queries2.default.SETTING_SI_COLUMNHEADER_VALUE]), (0, _defineProperty3.default)(_setState, _Queries2.default.SETTING_ITEMNAME_COLUMNHEADER_VALUE, this.state[_Queries2.default.SETTING_ITEMNAME_COLUMNHEADER_VALUE]), (0, _defineProperty3.default)(_setState, _Queries2.default.SETTING_HSNCODE_COLUMNHEADER_VALUE, this.state[_Queries2.default.SETTING_HSNCODE_COLUMNHEADER_VALUE]), (0, _defineProperty3.default)(_setState, _Queries2.default.SETTING_ITEMCODE_COLUMNHEADER_VALUE, this.state[_Queries2.default.SETTING_ITEMCODE_COLUMNHEADER_VALUE]), (0, _defineProperty3.default)(_setState, _Queries2.default.SETTING_PRICEPERUNIT_COLUMNHEADER_VALUE, this.state[_Queries2.default.SETTING_PRICEPERUNIT_COLUMNHEADER_VALUE]), (0, _defineProperty3.default)(_setState, _Queries2.default.SETTING_QUNATITY_COLUMNHEADER_VALUE, this.state[_Queries2.default.SETTING_QUNATITY_COLUMNHEADER_VALUE]), (0, _defineProperty3.default)(_setState, _Queries2.default.SETTING_ITEMUNIT_COLUMNHEADER_VALUE, this.state[_Queries2.default.SETTING_ITEMUNIT_COLUMNHEADER_VALUE]), (0, _defineProperty3.default)(_setState, _Queries2.default.SETTING_DISCOUNT_COLUMNHEADER_VALUE, this.state[_Queries2.default.SETTING_DISCOUNT_COLUMNHEADER_VALUE]), (0, _defineProperty3.default)(_setState, _Queries2.default.SETTING_TAXABLE_PRICE_COLUMNHEADER_VALUE, this.state[_Queries2.default.SETTING_TAXABLE_PRICE_COLUMNHEADER_VALUE]), (0, _defineProperty3.default)(_setState, _Queries2.default.SETTING_TAXABLEAMOUNT_COLUMNHEADER_VALUE, this.state[_Queries2.default.SETTING_TAXABLEAMOUNT_COLUMNHEADER_VALUE]), (0, _defineProperty3.default)(_setState, _Queries2.default.SETTING_ADDITIONALCESS_COLUMNHEADER_VALUE, this.state[_Queries2.default.SETTING_ADDITIONALCESS_COLUMNHEADER_VALUE]), (0, _defineProperty3.default)(_setState, _Queries2.default.SETTING_FINALITEMPRICE_COLUMNHEADER_VALUE, this.state[_Queries2.default.SETTING_FINALITEMPRICE_COLUMNHEADER_VALUE]), (0, _defineProperty3.default)(_setState, _Queries2.default.SETTING_TOTALAMOUNT_COLUMNHEADER_VALUE, this.state[_Queries2.default.SETTING_TOTALAMOUNT_COLUMNHEADER_VALUE]), (0, _defineProperty3.default)(_setState, 'columns', this.initColumnsForItemTable()), _setState));
		}
	}, {
		key: 'initTableColumnWidth',
		value: function initTableColumnWidth() {
			return [{ columnName: _Queries2.default.SETTING_SI_COLUMNRATIO_VALUE, width: this.itemTableColumnWidthConstant * _TransactionPrintSettings2.default.getSIColumnRatio() }, { columnName: _Queries2.default.SETTING_ITEMNAME_COLUMNRATIO_VALUE, width: this.itemTableColumnWidthConstant * _TransactionPrintSettings2.default.getItemNameColumnRatio() }, { columnName: _Queries2.default.SETTING_ITEM_CODE_COLUMNRATIO_VALUE, width: this.itemTableColumnWidthConstant * _TransactionPrintSettings2.default.getItemCodeColumnRatio() }, { columnName: _Queries2.default.SETTING_HSNCODE_COLUMNRATIO_VALUE, width: this.itemTableColumnWidthConstant * _TransactionPrintSettings2.default.getHsnCodeColumnRatio() }, { columnName: _Queries2.default.SETTING_ITEMCOUNT_COLUMNRATIO_VALUE, width: this.itemTableColumnWidthConstant * _TransactionPrintSettings2.default.getItemCountColumnRatio() }, { columnName: _Queries2.default.SETTING_BATCHNO_COLUMNRATIO_VALUE, width: this.itemTableColumnWidthConstant * _TransactionPrintSettings2.default.getItemBatchColumnRatio() }, { columnName: _Queries2.default.SETTING_ITEMEXPIRYDATE_COLUMNRATIO_VALUE, width: this.itemTableColumnWidthConstant * _TransactionPrintSettings2.default.getItemExpDateColumnRatio() }, { columnName: _Queries2.default.SETTING_ITEMMANUFACTURINGDATE_COLUMNRATIO_VALUE, width: this.itemTableColumnWidthConstant * _TransactionPrintSettings2.default.getItemManufacturingDateColumnRatio() }, { columnName: _Queries2.default.SETTING_ITEMMRP_COLUMNRATIO_VALUE, width: this.itemTableColumnWidthConstant * _TransactionPrintSettings2.default.getItemMRPColumnRatio() }, { columnName: _Queries2.default.SETTING_ITEMSIZE_COLUMNRATIO_VALUE, width: this.itemTableColumnWidthConstant * _TransactionPrintSettings2.default.getItemSizeColumnRatio() }, { columnName: _Queries2.default.SETTING_QUANTITY_COLUMNRATIO_VALUE, width: this.itemTableColumnWidthConstant * _TransactionPrintSettings2.default.getQuantityColumnRatio() }, { columnName: _Queries2.default.SETTING_PRICEPERUNIT_COLUMNRATIO_VALUE, width: this.itemTableColumnWidthConstant * _TransactionPrintSettings2.default.getPricePerUnitColumnRatio() }, { columnName: _Queries2.default.SETTING_DISCOUNT_COLUMNRATIO_VALUE, width: this.itemTableColumnWidthConstant * _TransactionPrintSettings2.default.getDiscountColumnRatio() }, { columnName: _Queries2.default.SETTING_TAXABLE_PRICE_COLUMNRATIO_VALUE, width: this.itemTableColumnWidthConstant * _TransactionPrintSettings2.default.getTaxableItemPriceColumnRatio() }, { columnName: _Queries2.default.SETTING_TAXABLEAMOUNT_COLUMNRATIO_VALUE, width: this.itemTableColumnWidthConstant * _TransactionPrintSettings2.default.getTaxableAmountColumnRatio() }, { columnName: _Queries2.default.SETTING_FINALITEMPRICE_COLUMNRATIO_VALUE, width: this.itemTableColumnWidthConstant * _TransactionPrintSettings2.default.getFinalItemPriceColumnRatio() }, { columnName: _Queries2.default.SETTING_TOTALAMOUNT_COLUMNRATIO_VALUE, width: this.itemTableColumnWidthConstant * _TransactionPrintSettings2.default.getTotalAmountColumnRatio() }];
		}
	}, {
		key: 'sampleRowsForItemTable',
		value: function sampleRowsForItemTable() {
			var _ref3;

			return [(_ref3 = {}, (0, _defineProperty3.default)(_ref3, _Queries2.default.SETTING_SI_COLUMNRATIO_VALUE, 1), (0, _defineProperty3.default)(_ref3, _Queries2.default.SETTING_ITEMNAME_COLUMNRATIO_VALUE, 'Item 1'), (0, _defineProperty3.default)(_ref3, _Queries2.default.SETTING_ITEM_CODE_COLUMNRATIO_VALUE, 123), (0, _defineProperty3.default)(_ref3, _Queries2.default.SETTING_HSNCODE_COLUMNRATIO_VALUE, 101), (0, _defineProperty3.default)(_ref3, _Queries2.default.SETTING_ITEMCOUNT_COLUMNRATIO_VALUE, 10), (0, _defineProperty3.default)(_ref3, _Queries2.default.SETTING_BATCHNO_COLUMNRATIO_VALUE, 'AY101'), (0, _defineProperty3.default)(_ref3, _Queries2.default.SETTING_ITEMEXPIRYDATE_COLUMNRATIO_VALUE, '09/2019'), (0, _defineProperty3.default)(_ref3, _Queries2.default.SETTING_ITEMMANUFACTURINGDATE_COLUMNRATIO_VALUE, '31/12/2018'), (0, _defineProperty3.default)(_ref3, _Queries2.default.SETTING_ITEMMRP_COLUMNRATIO_VALUE, '₹ 500'), (0, _defineProperty3.default)(_ref3, _Queries2.default.SETTING_ITEMSIZE_COLUMNRATIO_VALUE, 1), (0, _defineProperty3.default)(_ref3, _Queries2.default.SETTING_QUANTITY_COLUMNRATIO_VALUE, 1), (0, _defineProperty3.default)(_ref3, _Queries2.default.SETTING_PRICEPERUNIT_COLUMNRATIO_VALUE, '₹ 500'), (0, _defineProperty3.default)(_ref3, _Queries2.default.SETTING_DISCOUNT_COLUMNRATIO_VALUE, '₹ 25'), (0, _defineProperty3.default)(_ref3, _Queries2.default.SETTING_TAXABLE_PRICE_COLUMNRATIO_VALUE, '₹ 20'), (0, _defineProperty3.default)(_ref3, _Queries2.default.SETTING_TAXABLEAMOUNT_COLUMNRATIO_VALUE, '₹ 475'), (0, _defineProperty3.default)(_ref3, _Queries2.default.SETTING_FINALITEMPRICE_COLUMNRATIO_VALUE, '₹ 560'), (0, _defineProperty3.default)(_ref3, _Queries2.default.SETTING_TOTALAMOUNT_COLUMNRATIO_VALUE, '₹ 560'), _ref3)];
		}
	}, {
		key: 'render',
		value: function render() {
			var _this3 = this;

			return _react2.default.createElement(
				'div',
				{ className: 'item-table-customization settings-container' },
				_react2.default.createElement(
					'div',
					{ className: 'mt-10', style: { height: '150px', overflow: 'auto', flexShrink: 0 } },
					_react2.default.createElement(
						_Paper2.default,
						{ style: { boxShadow: '2px 2px 2px #DDDDDD', border: '1px solid #DDDDDD' } },
						_react2.default.createElement(
							_dxReactGridMaterialUi.Grid,
							{ rows: this.state.rows, columns: this.state.columns },
							_react2.default.createElement(_dxReactGridMaterialUi.Table, null),
							_react2.default.createElement(_dxReactGridMaterialUi.TableColumnResizing, {
								className: 'custom-resize-handler',
								onColumnWidthsChange: this.onColWidthChange,
								defaultColumnWidths: this.state.defaultColumnWidths
							}),
							_react2.default.createElement(_dxReactGridMaterialUi.TableHeaderRow, null)
						)
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'item-table-settings h-100-per' },
					_react2.default.createElement(
						'div',
						{ className: 'border-bottom-1 form-wrapper mb-10', style: { height: 'calc(100vh - 340px)', overflowX: 'hidden' } },
						_react2.default.createElement(
							'div',
							{ className: 'row mb-20' },
							_react2.default.createElement(
								'div',
								{ className: 'col' },
								_react2.default.createElement(
									'span',
									{ className: 'heading font-family-bold font-16' },
									'Item related columns'
								),
								_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
								this.renderItemTableCustomization(this.itemRelatedColumns)
							),
							_react2.default.createElement(
								'div',
								{ className: 'col' },
								_react2.default.createElement(
									'span',
									{ className: 'heading font-family-bold font-16' },
									'Additional Item Columns'
								),
								_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
								(0, _keys2.default)(this.additionalItemColumns).map(function (key) {
									return _react2.default.createElement(
										'div',
										{ key: key, className: 'setting-item d-flex align-items-center' },
										_react2.default.createElement(
											'label',
											{ className: 'input-checkbox' },
											_react2.default.createElement('input', { type: 'checkbox', onChange: _this3.toggleCheckbox(key, false, _this3.handleCheckboxCallback(key)), checked: _this3.state[key] }),
											_this3.checkboxJSX
										),
										_react2.default.createElement(
											'span',
											{ className: '' },
											_this3.additionalItemColumns[key]
										)
									);
								})
							),
							_react2.default.createElement(
								'div',
								{ className: 'col' },
								_react2.default.createElement(
									'span',
									{ className: 'heading font-family-bold font-16' },
									'Amounts, Totals & Taxes'
								),
								_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
								this.renderItemTableCustomization(this.amountTotalAndTaxes)
							)
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'dialogButton' },
						_react2.default.createElement(
							'button',
							{ onClick: this.saveItemTableChanges, className: 'terminalButton' },
							'Done'
						)
					)
				)
			);
		}
	}]);
	return ItemTableCustomization;
}(_BasePrintSettings3.default);

exports.default = (0, _styles.withStyles)(styles)(ItemTableCustomization);