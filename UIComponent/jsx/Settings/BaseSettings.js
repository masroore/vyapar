Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.TabContainer = undefined;

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Country = require('../../../Constants/Country');

var _Country2 = _interopRequireDefault(_Country);

var _Queries = require('../../../Constants/Queries');

var _Queries2 = _interopRequireDefault(_Queries);

var _FirmCache = require('../../../Cache/FirmCache');

var _FirmCache2 = _interopRequireDefault(_FirmCache);

var _SettingCache = require('../../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

var _SettingsModel = require('../../../Models/SettingsModel');

var _SettingsModel2 = _interopRequireDefault(_SettingsModel);

var _ErrorCode = require('../../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

var _TransactionManager = require('../../../DBManager/TransactionManager');

var _TransactionManager2 = _interopRequireDefault(_TransactionManager);

var _DataLoader = require('../../../DBManager/DataLoader');

var _DataLoader2 = _interopRequireDefault(_DataLoader);

var _DataInserter = require('../../../DBManager/DataInserter');

var _DataInserter2 = _interopRequireDefault(_DataInserter);

var _Typography = require('@material-ui/core/Typography');

var _Typography2 = _interopRequireDefault(_Typography);

var _Modal = require('../Modal');

var _Modal2 = _interopRequireDefault(_Modal);

var _TextField = require('@material-ui/core/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _Info = require('@material-ui/icons/Info');

var _Info2 = _interopRequireDefault(_Info);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var baseControllerObj = null;
var settingList = void 0;
/* eslint react/prop-types: 0 */
var TabContainer = exports.TabContainer = function TabContainer(props) {
	return _react2.default.createElement(
		_Typography2.default,
		{ component: 'div', className: props.className, style: { padding: 10 } },
		props.children
	);
};

var BaseSettings = function (_React$Component) {
	(0, _inherits3.default)(BaseSettings, _React$Component);

	function BaseSettings(props) {
		(0, _classCallCheck3.default)(this, BaseSettings);

		var _this = (0, _possibleConstructorReturn3.default)(this, (BaseSettings.__proto__ || (0, _getPrototypeOf2.default)(BaseSettings)).call(this, props));

		_this.initializeToolTips = function () {
			setTimeout(function () {
				$('.settingsToolTip').tooltipster({
					theme: 'tooltipster-noir',
					maxWidth: 300
				});
			});
		};

		_this.higlightSearchedSetting = function () {
			if (!_this.higlightedSettingKey) return;
			var settingItem = document.getElementsByClassName(_this.higlightedSettingKey)[0];
			if (settingItem) {
				settingItem.classList.add('highlighted-setting-item');
				setTimeout(function () {
					settingItem.classList.remove('highlighted-setting-item');
				}, 4000);
			}
			_this.higlightedSettingKey = '';
		};

		_this.toggleChangeTextModalPopup = function (key) {
			return function () {
				_this.previousText = '';
				if (key) {
					_this.previousText = _this.state[key];
				}
				_this.setState({
					openChangeTextModal: !_this.state.openChangeTextModal
				});
			};
		};

		_this.saveChangeText = function (key) {
			var value = _this.state[key];
			if (!value) {
				ToastHelper.error('Please enter some change text');
				return;
			}
			_this.saveSettings(key, value, true);
			_this.previousText = '';
			_this.closeChangeTextModal(key);
		};

		_this.closeChangeTextModal = function (key) {
			var _this$setState;

			_this.setState((_this$setState = {}, (0, _defineProperty3.default)(_this$setState, key, _this.previousText ? _this.previousText : _this.state[key]), (0, _defineProperty3.default)(_this$setState, 'openChangeTextModal', false), _this$setState));
		};

		_this.renderChangeTextModal = function (key) {
			return _react2.default.createElement(
				_Modal2.default,
				{
					height: '180px',
					width: '300px',
					isOpen: _this.state.openChangeTextModal,
					onClose: _this.closeChangeTextModal.bind(_this, key),
					canHideHeader: 'true',
					title: 'Edit Text',
					parentClassSelector: 'change-text-modal-container'
				},
				_react2.default.createElement(
					'div',
					{ className: 'modalBody' },
					_react2.default.createElement(
						'div',
						{ className: 'change-text-form' },
						_react2.default.createElement(_TextField2.default, {
							margin: 'dense',
							variant: 'outlined',
							className: 'width100',
							value: _this.state[key],
							onChange: _this.updateInputChange(key, false),
							inputProps: {
								maxLength: 20
							}
						}),
						_react2.default.createElement(
							'div',
							{ className: 'floatRight mt-10' },
							_react2.default.createElement(
								_Button2.default,
								{ margin: 'dense', className: 'mr-10',
									onClick: _this.closeChangeTextModal.bind(_this, key), variant: 'contained', color: 'primary' },
								' Cancel'
							),
							_react2.default.createElement(
								_Button2.default,
								{ margin: 'dense', onClick: _this.saveChangeText.bind(_this, key), variant: 'contained', color: 'primary' },
								' Save'
							)
						)
					)
				)
			);
		};

		_this.handleTabChange = function (_, activeIndex) {
			_this.setState({ activeIndex: activeIndex });
		};

		_this.toggleCheckbox = function (setting, saveSettings, callback) {
			return function () {
				var settingValue = !_this.state[setting];
				_this.toggleCheckboxAndSave(setting, settingValue, saveSettings, callback);
			};
		};

		_this.handleDecimalPlace = function (key) {
			return function (value) {
				value = Number(value);
				if (!value || value <= 0 || value > 5) {
					ToastHelper.error('Values more than 5 or less than 1 is not allowed.');
					_this.decimalFieldRef.current.setState({
						value: _this.listOfSettings[key]
					});
					return;
				}
				_this.convertedDecimalPlaceValue(value);
				_this.saveSettings(key, value);
			};
		};

		_this.handleInputChange = function (key) {
			return function (value) {
				_this.setState((0, _defineProperty3.default)({}, key, value));
			};
		};

		_this.handleDecimalChange = function (key) {
			return function (value) {
				debugger;
				// DecimalInputFilter.inputTextFilterForPercentage(e.target)
			};
		};

		_this.handleNumber = function (key) {
			return function (value) {
				value = Number(value);
				_this.saveSettings(key, value);
			};
		};

		_this.updateInputChange = function (key) {
			var canSaveSetting = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
			var callback = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
			return function (event) {
				var value = event.target.value;
				_this.setState((0, _defineProperty3.default)({}, key, value), function () {
					if (canSaveSetting) {
						_this.saveSettings(key, value);
					}
					if (callback && typeof callback === 'function') {
						callback(value);
					}
				});
			};
		};

		baseControllerObj = _this;
		_this.higlightedSettingKey = _this.props.higlightedSettingKey;
		_this.listOfCurrency = _Country2.default.getCurrencyList();
		_this.getFirmDetails();
		_this.loadSettings();
		_this.settingsModel = new _SettingsModel2.default();
		_this.transactionManager = new _TransactionManager2.default();
		_this.dataLoader = new _DataLoader2.default();
		_this.dataInserter = new _DataInserter2.default();
		_this.checkboxJSX = _this.renderCheckbox();
		window.isSyncSettingUpdate = false;
		window.onRes = function () {
			isSyncSettingUpdate = true;
			baseControllerObj.loadSettings();
			baseControllerObj.resetState();
		};
		return _this;
	}

	(0, _createClass3.default)(BaseSettings, [{
		key: 'componentDidUpdate',
		value: function componentDidUpdate() {
			window.isSyncSettingUpdate = false;
		}
	}, {
		key: 'renderCheckbox',
		value: function renderCheckbox() {
			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement(
					'svg',
					{ width: '18', height: '18' },
					_react2.default.createElement('path', { className: 'checked', d: 'M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M7,14L2,9l1.4-1.4L7,11.2l7.6-7.6L16,5L7,14z' }),
					_react2.default.createElement('path', { className: 'indeterminate', d: 'M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M7,14L2,9l1.4-1.4L7,11.2l7.6-7.6L16,5L7,14z' }),
					_react2.default.createElement('path', { className: 'unchecked', fill: '#9e9e9e', d: 'M16,2v14H2V2H16 M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z' })
				)
			);
		}
	}, {
		key: 'renderToolTip',
		value: function renderToolTip(mappingItem) {
			var tooltipId = Math.round(Math.random() * 1000000000000);
			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				mappingItem.heading === "Terms and Conditions &gt" ? _react2.default.createElement(
					'svg',
					{ style: { marginLeft: 2, color: "#bbb" }, className: 'settingsToolTip MuiSvgIcon-root', 'data-tooltip-content': "#" + tooltipId, focusable: 'false', viewBox: '0 0 24 24', 'aria-hidden': 'true', role: 'presentation' },
					_react2.default.createElement('path', { fill: 'none', d: 'M0 0h24v24H0z' }),
					_react2.default.createElement('path', { d: 'M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-6h2v6zm0-8h-2V7h2v2z' })
				) : _react2.default.createElement('img', { style: { marginLeft: 2 }, className: 'settingsToolTip', 'data-tooltip-content': "#" + tooltipId, src: '../inlineSVG/round-info-24px.svg' }),
				_react2.default.createElement(
					'div',
					{ 'class': 'tooltip_templates' },
					_react2.default.createElement(
						'span',
						{ id: tooltipId },
						_react2.default.createElement(
							_react2.default.Fragment,
							null,
							_react2.default.createElement(
								'div',
								{ style: { padding: '10px' } },
								_react2.default.createElement('p', { className: 'font-14 font-family-bold', dangerouslySetInnerHTML: { __html: mappingItem.heading } }),
								mappingItem.what && _react2.default.createElement(
									'div',
									{ className: 'mt-10' },
									_react2.default.createElement(
										'p',
										null,
										_react2.default.createElement(
											'strong',
											null,
											'What is this?'
										)
									),
									_react2.default.createElement(
										'p',
										{ className: 'font-12' },
										mappingItem.what
									)
								),
								mappingItem.how && _react2.default.createElement(
									'div',
									{ className: 'mt-10' },
									_react2.default.createElement(
										'p',
										null,
										_react2.default.createElement(
											'strong',
											null,
											'How it is used?'
										)
									),
									_react2.default.createElement(
										'p',
										{ className: 'font-12' },
										mappingItem.how
									)
								),
								mappingItem.why && _react2.default.createElement(
									'div',
									{ className: 'mt-10' },
									_react2.default.createElement(
										'p',
										null,
										_react2.default.createElement(
											'strong',
											null,
											'Why to use?'
										)
									),
									_react2.default.createElement(
										'p',
										{ className: 'font-12' },
										mappingItem.why
									)
								)
							)
						)
					)
				)
			);
		}
	}, {
		key: 'getFirmDetails',
		value: function getFirmDetails() {
			this.firmCache = new _FirmCache2.default();
			this.defaultFirm = this.firmCache.getDefaultFirm();
			this.listOfFirmsFromCache = this.firmCache.getFirmList();
		}
	}, {
		key: 'loadSettings',
		value: function loadSettings() {
			this.settingCache = new _SettingCache2.default();
			this.listOfSettings = this.settingCache.LoadAllSettings();
		}
	}, {
		key: 'handleDropdownChange',
		value: function handleDropdownChange(key, value) {
			this.setState((0, _defineProperty3.default)({}, key, value));
		}
	}, {
		key: 'toggleCheckboxAndSave',
		value: function toggleCheckboxAndSave(setting, settingValue) {
			var _this2 = this;

			var saveSettings = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
			var callback = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

			this.setState((0, _defineProperty3.default)({}, setting, settingValue), function () {
				if (saveSettings) {
					_this2.saveSettings(setting, settingValue ? '1' : '0');
				}
				if (callback && typeof callback === 'function') {
					callback(settingValue);
				}
			});
		}
	}, {
		key: 'saveSettings',
		value: function saveSettings(key, value, showStatusMessage) {
			this.settingsModel.setSettingKey(key);
			var sync = {
				nonSyncableSetting: false
			};
			if (key == _Queries2.default.SETTING_PAYMENTREMIDNER_ENABLED || key == _Queries2.default.SETTING_CUSTOM_PAYMENT_REMINDER_MESSAGE || key == _Queries2.default.SETTING_PAYMENT_REMINDER_DAYS || key == _Queries2.default.SETTING_AUTO_BACKUP_ENABLED || key == _Queries2.default.SETTING_BARCODE_SCANNING_ENABLED || key == _Queries2.default.SETTING_DELETE_PASSCODE_ENABLED || key == _Queries2.default.SETTING_DELETE_PASSCODE || key == _Queries2.default.SETTING_DIRECT_BARCODE_SCANNING_MODE_ENABLED || key == _Queries2.default.SETTING_DEFAULT_PRINTER) {
				sync.nonSyncableSetting = true;
			}
			MyAnalytics.logSetting((0, _defineProperty3.default)({}, key, value));
			var statusCode = this.settingsModel.UpdateSetting(value, sync);
			if (key == _Queries2.default.SETTING_SALE_INVOICE_TERMS_AND_CONDITIONS || key == _Queries2.default.SETTING_SALE_ORDER_TERMS_AND_CONDITIONS || key == _Queries2.default.SETTING_DELIVERY_CHALLAN_TERMS_AND_CONDITIONS || key == _Queries2.default.SETTING_ESTIMATE_QUOTATION_TERMS_AND_CONDITIONS) {
				this.handleStatus(statusCode, showStatusMessage = false);
			} else {
				this.handleStatus(statusCode, showStatusMessage);
			}
		}
	}, {
		key: 'saveMultipleSettings',
		value: function saveMultipleSettings(settingsObj) {
			var showStatusMessage = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

			var statusCode = this.settingsModel.updateMultipleSettings(settingsObj);
			this.handleStatus(statusCode, showStatusMessage);
			return statusCode;
		}
	}, {
		key: 'convertedDecimalPlaceValue',
		value: function convertedDecimalPlaceValue(toFixedVal) {
			var convertedValue = 0.00000.toFixed(toFixedVal);
			this.setState({
				decimalPlaceConverted: convertedValue
			});
		}
	}, {
		key: 'handleStatus',
		value: function handleStatus(statusCode) {
			var showStatusMessage = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

			if (statusCode) {
				if (statusCode === _ErrorCode2.default.ERROR_SETTING_SAVE_SUCCESS && showStatusMessage) {
					ToastHelper.success(statusCode);
				} else if (statusCode !== _ErrorCode2.default.ERROR_SETTING_SAVE_SUCCESS) {
					ToastHelper.error(statusCode);
				}
			}
		}
	}, {
		key: 'getSettingList',
		value: function getSettingList() {
			if (!settingList) {
				var SettingsMapping = require('./SettingsMapping').default;
				settingList = [];
				(0, _keys2.default)(SettingsMapping).map(function (key) {
					var settingObj = SettingsMapping[key];
					var settingItemsList = [];
					(0, _keys2.default)(settingObj).map(function (settingItemKey) {
						var settingItem = settingObj[settingItemKey];
						settingItem.settingKey = settingItemKey;
						settingItemsList.push(settingItem);
					});
					settingList = [].concat((0, _toConsumableArray3.default)(settingList), settingItemsList);
				});
			}
			return settingList;
		}
	}, {
		key: 'isChecked',
		value: function isChecked(settingValue) {
			return settingValue && parseInt(settingValue) === 1 || false;
		}
	}, {
		key: 'checkboxValueForSave',
		value: function checkboxValueForSave(settingValue) {
			return settingValue ? 1 : 0;
		}
	}]);
	return BaseSettings;
}(_react2.default.Component);

exports.default = BaseSettings;