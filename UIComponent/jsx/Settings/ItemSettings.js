Object.defineProperty(exports, "__esModule", {
	value: true
});

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _extends4 = require('babel-runtime/helpers/extends');

var _extends5 = _interopRequireDefault(_extends4);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _BaseSettings2 = require('./BaseSettings');

var _BaseSettings3 = _interopRequireDefault(_BaseSettings2);

var _SelectDropDown = require('../UIControls/SelectDropDown');

var _SelectDropDown2 = _interopRequireDefault(_SelectDropDown);

var _NumberTextField = require('../UIControls/NumberTextField');

var _NumberTextField2 = _interopRequireDefault(_NumberTextField);

var _MenuItem = require('@material-ui/core/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _TextField = require('@material-ui/core/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _styles = require('@material-ui/core/styles');

var _ErrorCode = require('../../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

var _Queries = require('../../../Constants/Queries');

var _Queries2 = _interopRequireDefault(_Queries);

var _ItemCache = require('../../../Cache/ItemCache');

var _ItemCache2 = _interopRequireDefault(_ItemCache);

var _SettingsMapping = require('./SettingsMapping');

var _SettingsMapping2 = _interopRequireDefault(_SettingsMapping);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var dialog = require('electron').remote.dialog;

var styles = function styles(theme) {
	return {
		itemColumnTextField: {
			width: 100
		},
		itemAdditionalField: {
			'& .setting-item': {
				height: '56px'
			}
		}
	};
};

var ItemSettings = function (_BaseSettings) {
	(0, _inherits3.default)(ItemSettings, _BaseSettings);

	function ItemSettings(props) {
		(0, _classCallCheck3.default)(this, ItemSettings);

		var _this = (0, _possibleConstructorReturn3.default)(this, (ItemSettings.__proto__ || (0, _getPrototypeOf2.default)(ItemSettings)).call(this, props));

		_this.confirmUpdateItemType = function (value) {
			var conf = void 0;
			value = Number(value);
			if (value === 1 && _this.itemCache.isExistsServiceItem()) {
				conf = dialog.showMessageBox({
					type: 'warning',
					title: 'Update Item Type',
					buttons: ['Confirm', 'Cancel'],
					message: 'Would you like to convert all your services into products?',
					detail: 'We have found that there are some services in your items which need to be converted to products to proceed further.'
				});
			} else if (value === 2 && _this.itemCache.isExistsProductItem()) {
				conf = dialog.showMessageBox({
					type: 'warning',
					title: 'Update Item Type',
					buttons: ['Confirm', 'Cancel'],
					message: 'Would you like to convert all your products into services?',
					detail: 'We have found that there are some products in your items which need to be converted to Services to proceed further.'
				});
			}

			if (conf) {
				_this.setState((0, _defineProperty3.default)({}, _Queries2.default.SETTING_ITEM_TYPE, _this.listOfSettings[_Queries2.default.SETTING_ITEM_TYPE]));
				return false;
			} else if (value === 1 || value === 2) {
				var statusCode = _ErrorCode2.default.ERROR_ITEM_TYPE_UPDATE_FAILED;
				var ItemType = require('../../../Constants/ItemType');
				statusCode = value === 1 ? _this.dataInserter.UpdateItemType(ItemType.ITEM_TYPE_INVENTORY, ItemType.ITEM_TYPE_SERVICE) : _this.dataInserter.UpdateItemType(ItemType.ITEM_TYPE_SERVICE, ItemType.ITEM_TYPE_INVENTORY);

				if (statusCode == _ErrorCode2.default.ERROR_ITEM_TYPE_UPDATE_FAILED) {
					ToastHelper.error(statusCode);
					return false;
				}
			}
			_this.saveSettings(_Queries2.default.SETTING_ITEM_TYPE, value);
		};

		_this.itemSettingsHandleCallback = function (key) {
			return function () {
				if ((key == _Queries2.default.SETTING_IS_ITEM_UNIT_ENABLED || key == _Queries2.default.SETTING_ITEM_ENABLED) && !_this.state[key] && _this.dataLoader.isItemUnitUsedInLineItems()) {
					if (key == _Queries2.default.SETTING_IS_ITEM_UNIT_ENABLED) {
						ToastHelper.error('Item unit is already being used in transactions. It cannot be disabled.');
					} else {
						ToastHelper.error('Item cannot be disabled as Item unit being used in Transactions.');
					}
					_this.toggleCheckboxAndSave(key, !_this.state[key], false);
					return;
				}
				if (key === _Queries2.default.SETTING_BARCODE_SCANNING_ENABLED) {
					_this.toggleCheckboxAndSave(_Queries2.default.SETTING_DIRECT_BARCODE_SCANNING_MODE_ENABLED, false, true);
				}
				_this.saveSettings(key, _this.checkboxValueForSave(_this.state[key]));
				showHideAddItemButtons();
			};
		};

		_this.itemTypeList = [{ key: '1', value: 'Product' }, { key: '2', value: 'Service' }, { key: '3', value: 'Product/Service' }];
		_this.dateFormatList = [{ key: '1', value: 'dd/mm/yy' }, { key: '2', value: 'mm/yy' }];

		_this.itemCache = new _ItemCache2.default();
		_this.state = (0, _extends5.default)({}, _this.itemSettingsState());
		_this.itemSettingsList = _SettingsMapping2.default.itemSettingsList;

		_this.additionalItemsColumn = [{ key: _Queries2.default.SETTING_ENABLE_ITEM_MRP, value: 'MRP/Price', editValueKey: _Queries2.default.SETTING_ITEM_MRP_VALUE }, { key: _Queries2.default.SETTING_ENABLE_ITEM_BATCH_NUMBER, value: 'Batch No.', editValueKey: _Queries2.default.SETTING_ITEM_BATCH_NUMBER_VALUE }, {
			key: _Queries2.default.SETTING_ENABLE_ITEM_EXPIRY_DATE,
			value: 'Exp Date',
			dateFormat: true,
			editValueKey: _Queries2.default.SETTING_ITEM_EXPIRY_DATE_VALUE,
			dateFormatKey: _Queries2.default.SETTING_EXPIRY_DATE_TYPE
		}, {
			key: _Queries2.default.SETTING_ENABLE_ITEM_MANUFACTURING_DATE,
			value: 'Mfg Date',
			dateFormat: true,
			editValueKey: _Queries2.default.SETTING_ITEM_MANUFACTURING_DATE_VALUE,
			dateFormatKey: _Queries2.default.SETTING_MANUFACTURING_DATE_TYPE
		}, { key: _Queries2.default.SETTING_ENABLE_SERIAL_ITEM_NUMBER, value: 'Serial No./ IMEI', editValueKey: _Queries2.default.SETTING_ITEM_SERIAL_NUMBER_VALUE }, { key: _Queries2.default.SETTING_ENABLE_ITEM_SIZE, value: 'Size', editValueKey: _Queries2.default.SETTING_ITEM_SIZE_VALUE }];

		_this.decimalFieldRef = _react2.default.createRef();
		return _this;
	}

	(0, _createClass3.default)(ItemSettings, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.higlightSearchedSetting();
			this.initializeToolTips();
		}
	}, {
		key: 'resetState',
		value: function resetState() {
			this.setState((0, _extends5.default)({}, this.itemSettingsState()));
		}
	}, {
		key: 'itemSettingsState',
		value: function itemSettingsState() {
			var _extends2;

			var showLowStockDialog = this.listOfSettings[_Queries2.default.SETTING_SHOW_LOW_STOCK_DIALOG];
			return (0, _extends5.default)((_extends2 = {}, (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_ITEM_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ITEM_ENABLED])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_BARCODE_SCANNING_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_BARCODE_SCANNING_ENABLED])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_DIRECT_BARCODE_SCANNING_MODE_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_DIRECT_BARCODE_SCANNING_MODE_ENABLED])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_ENABLE_DEFAULT_CASH_SALE, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ENABLE_DEFAULT_CASH_SALE])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_STOCK_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_STOCK_ENABLED])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_SHOW_LOW_STOCK_DIALOG, showLowStockDialog != 0), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_IS_ITEM_UNIT_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_IS_ITEM_UNIT_ENABLED])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_ITEM_CATEGORY, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ITEM_CATEGORY])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_PARTY_ITEM_RATE, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PARTY_ITEM_RATE])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_ITEMWISE_TAX_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ITEMWISE_TAX_ENABLED])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_ITEMWISE_DISCOUNT_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ITEMWISE_DISCOUNT_ENABLED])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_ENABLE_ITEM_DESCRIPTION, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ENABLE_ITEM_DESCRIPTION])), (0, _defineProperty3.default)(_extends2, 'decimalPlaceConverted', 0.00000.toFixed(this.listOfSettings[_Queries2.default.SETTING_QUANTITY_DECIMAL])), _extends2), this.setItemAdditionalFields(), (0, _defineProperty3.default)({
				openChangeTextModal: false
			}, _Queries2.default.SETTING_ITEM_DESCRIPTION_VALUE, this.listOfSettings[_Queries2.default.SETTING_ITEM_DESCRIPTION_VALUE]));
		}

		// Need to delete unused code

	}, {
		key: 'setItemAdditionalFields',
		value: function setItemAdditionalFields() {
			var _ref;

			return _ref = {}, (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ENABLE_ITEM_MRP, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ENABLE_ITEM_MRP])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ENABLE_ITEM_BATCH_NUMBER, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ENABLE_ITEM_BATCH_NUMBER])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ENABLE_ITEM_EXPIRY_DATE, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ENABLE_ITEM_EXPIRY_DATE])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ENABLE_ITEM_MANUFACTURING_DATE, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ENABLE_ITEM_MANUFACTURING_DATE])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ENABLE_SERIAL_ITEM_NUMBER, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ENABLE_SERIAL_ITEM_NUMBER])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ENABLE_ITEM_SIZE, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_ENABLE_ITEM_SIZE])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ITEM_MRP_VALUE, this.listOfSettings[_Queries2.default.SETTING_ITEM_MRP_VALUE]), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ITEM_BATCH_NUMBER_VALUE, this.listOfSettings[_Queries2.default.SETTING_ITEM_BATCH_NUMBER_VALUE]), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ITEM_EXPIRY_DATE_VALUE, this.listOfSettings[_Queries2.default.SETTING_ITEM_EXPIRY_DATE_VALUE]), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ITEM_MANUFACTURING_DATE_VALUE, this.listOfSettings[_Queries2.default.SETTING_ITEM_MANUFACTURING_DATE_VALUE]), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ITEM_SERIAL_NUMBER_VALUE, this.listOfSettings[_Queries2.default.SETTING_ITEM_SERIAL_NUMBER_VALUE]), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_ITEM_SIZE_VALUE, this.listOfSettings[_Queries2.default.SETTING_ITEM_SIZE_VALUE]), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_EXPIRY_DATE_TYPE, this.listOfSettings[_Queries2.default.SETTING_EXPIRY_DATE_TYPE]), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_MANUFACTURING_DATE_TYPE, this.listOfSettings[_Queries2.default.SETTING_MANUFACTURING_DATE_TYPE]), _ref;
		}
	}, {
		key: 'saveItemAdditionalFieldChanges',
		value: function saveItemAdditionalFieldChanges() {
			if (!this.state['itemAdditionalField']) {
				this.saveItemsColumn();
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var _this2 = this;

			var classes = this.props.classes;

			return _react2.default.createElement(
				'div',
				{ className: 'item-settings' },
				_react2.default.createElement(
					'div',
					{ className: 'row' },
					_react2.default.createElement(
						'div',
						{ className: 'col' },
						_react2.default.createElement(
							'span',
							{ className: 'heading font-family-bold font-16' },
							'Item Settings'
						),
						_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
						_react2.default.createElement(
							'div',
							{ className: 'setting-item d-flex align-items-center ' + _Queries2.default.SETTING_ITEM_ENABLED },
							_react2.default.createElement(
								'label',
								{ className: 'input-checkbox' },
								_react2.default.createElement('input', { type: 'checkbox', onChange: this.toggleCheckbox(_Queries2.default.SETTING_ITEM_ENABLED, false, this.itemSettingsHandleCallback(_Queries2.default.SETTING_ITEM_ENABLED)), checked: this.state[_Queries2.default.SETTING_ITEM_ENABLED] }),
								this.checkboxJSX
							),
							_react2.default.createElement(
								'span',
								{ className: '' },
								_SettingsMapping2.default.miscItemSettingsList[_Queries2.default.SETTING_ITEM_ENABLED].heading
							),
							this.renderToolTip(_SettingsMapping2.default.miscItemSettingsList[_Queries2.default.SETTING_ITEM_ENABLED])
						),
						this.state[_Queries2.default.SETTING_ITEM_ENABLED] && _react2.default.createElement(
							_react2.default.Fragment,
							null,
							_react2.default.createElement(
								'div',
								{ className: 'setting-item d-flex align-items-center ' + _Queries2.default.SETTING_ITEM_TYPE },
								_react2.default.createElement(
									'span',
									null,
									_SettingsMapping2.default.miscItemSettingsList[_Queries2.default.SETTING_ITEM_TYPE].heading
								),
								this.renderToolTip(_SettingsMapping2.default.miscItemSettingsList[_Queries2.default.SETTING_ITEM_TYPE]),
								_react2.default.createElement(
									_SelectDropDown2.default,
									{ handleChange: this.confirmUpdateItemType, value: this.listOfSettings[_Queries2.default.SETTING_ITEM_TYPE] },
									this.itemTypeList.map(function (item, index) {
										return _react2.default.createElement(
											_MenuItem2.default,
											{ key: item.key, value: item.key },
											item.value
										);
									})
								)
							),
							(0, _keys2.default)(this.itemSettingsList).map(function (key) {
								return _react2.default.createElement(
									_react2.default.Fragment,
									null,
									_react2.default.createElement(
										'div',
										{ key: key, className: 'setting-item d-flex align-items-center ' + key },
										_react2.default.createElement(
											'label',
											{ className: 'input-checkbox' },
											_react2.default.createElement('input', { type: 'checkbox', onChange: _this2.toggleCheckbox(key, false, _this2.itemSettingsHandleCallback(key)), checked: _this2.state[key] }),
											_this2.checkboxJSX
										),
										key === _Queries2.default.SETTING_ENABLE_ITEM_DESCRIPTION && _react2.default.createElement(
											_react2.default.Fragment,
											null,
											_react2.default.createElement(
												'span',
												{ className: '' },
												_this2.state[_Queries2.default.SETTING_ITEM_DESCRIPTION_VALUE]
											),
											_react2.default.createElement(
												'a',
												{ onClick: _this2.toggleChangeTextModalPopup(_Queries2.default.SETTING_ITEM_DESCRIPTION_VALUE), className: (_this2.state[_Queries2.default.SETTING_ENABLE_ITEM_DESCRIPTION] ? '' : 'disable-input') + ' font-family-medium link' },
												'Change Text'
											)
										),
										key !== _Queries2.default.SETTING_ENABLE_ITEM_DESCRIPTION && _react2.default.createElement(
											'span',
											{ className: '' },
											_this2.itemSettingsList[key].heading
										),
										_this2.renderToolTip(_this2.itemSettingsList[key], key)
									),
									key === _Queries2.default.SETTING_BARCODE_SCANNING_ENABLED && _this2.state[_Queries2.default.SETTING_ENABLE_DEFAULT_CASH_SALE] && _this2.state[_Queries2.default.SETTING_BARCODE_SCANNING_ENABLED] && _react2.default.createElement(
										'div',
										{ key: _Queries2.default.SETTING_DIRECT_BARCODE_SCANNING_MODE_ENABLED, className: 'setting-item d-flex align-items-center here ' + _Queries2.default.SETTING_DIRECT_BARCODE_SCANNING_MODE_ENABLED },
										_react2.default.createElement(
											'label',
											{ className: 'input-checkbox' },
											_react2.default.createElement('input', { type: 'checkbox', onChange: _this2.toggleCheckbox(_Queries2.default.SETTING_DIRECT_BARCODE_SCANNING_MODE_ENABLED), checked: _this2.state[_Queries2.default.SETTING_DIRECT_BARCODE_SCANNING_MODE_ENABLED] }),
											_this2.checkboxJSX
										),
										_react2.default.createElement(
											'span',
											{ className: '' },
											_SettingsMapping2.default.miscItemSettingsList[_Queries2.default.SETTING_DIRECT_BARCODE_SCANNING_MODE_ENABLED].heading
										),
										_this2.renderToolTip(_SettingsMapping2.default.miscItemSettingsList[_Queries2.default.SETTING_DIRECT_BARCODE_SCANNING_MODE_ENABLED], _Queries2.default.SETTING_DIRECT_BARCODE_SCANNING_MODE_ENABLED)
									)
								);
							}),
							_react2.default.createElement(
								'div',
								{ className: 'setting-item height-72 d-flex align-items-center ' + _Queries2.default.SETTING_QUANTITY_DECIMAL },
								_react2.default.createElement(
									'div',
									{ className: 'mr-10 d-flex align-items-center' },
									_react2.default.createElement('span', { dangerouslySetInnerHTML: { __html: _SettingsMapping2.default.miscItemSettingsList[_Queries2.default.SETTING_QUANTITY_DECIMAL].heading } }),
									this.renderToolTip(_SettingsMapping2.default.miscItemSettingsList[_Queries2.default.SETTING_QUANTITY_DECIMAL])
								),
								_react2.default.createElement(_NumberTextField2.default, {
									ref: this.decimalFieldRef,
									handleBlur: this.handleDecimalPlace(_Queries2.default.SETTING_QUANTITY_DECIMAL),
									width: 100,
									value: this.listOfSettings[_Queries2.default.SETTING_QUANTITY_DECIMAL],
									className: 'd-inline-flex justify-content-center w-20-px'
								}),
								_react2.default.createElement(
									'span',
									{ className: 'font-12 color-707070' },
									'e.g. ',
									this.state.decimalPlaceConverted
								)
							)
						)
					),
					this.state[_Queries2.default.SETTING_ITEM_ENABLED] && _react2.default.createElement(
						'div',
						{ className: 'col' },
						_react2.default.createElement(
							'span',
							{ className: 'font-family-bold font-16' },
							'Additional item columns ',
							_react2.default.createElement(
								'span',
								{ className: 'font-12' },
								'(batch tracking)'
							)
						),
						_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' }),
						_react2.default.createElement(
							'div',
							{ className: classes.itemAdditionalField },
							this.additionalItemsColumn.map(function (item) {
								return _react2.default.createElement(
									'div',
									{ key: item.key, className: 'setting-item d-flex justify-content-between' },
									_react2.default.createElement(
										'div',
										{ className: 'd-flex align-items-center' },
										_react2.default.createElement(
											'label',
											{ className: 'input-checkbox' },
											_react2.default.createElement('input', { type: 'checkbox', checked: _this2.state[item.key], onChange: _this2.toggleCheckbox(item.key) }),
											_this2.checkboxJSX
										),
										_react2.default.createElement(
											'span',
											{ className: '' },
											item.value
										)
									),
									_react2.default.createElement(
										'div',
										{ className: 'd-flex align-items-center' },
										item.dateFormat && _react2.default.createElement(
											_SelectDropDown2.default,
											{ isDisabled: !_this2.state[item.key], outlined: true, value: _this2.listOfSettings[item.dateFormatKey],
												handleChange: _this2.saveSettings.bind(_this2, item.dateFormatKey), name: 'dateFormat' },
											_this2.dateFormatList.map(function (format, index) {
												return _react2.default.createElement(
													_MenuItem2.default,
													{ key: format.key, value: format.key },
													format.value
												);
											})
										),
										_react2.default.createElement(_TextField2.default, {
											disabled: !_this2.state[item.key],
											margin: 'dense',
											variant: 'outlined',
											value: _this2.state[item.editValueKey],
											className: classes.itemColumnTextField,
											onChange: _this2.updateInputChange(item.editValueKey),
											onBlur: _this2.saveSettings.bind(_this2, item.editValueKey, _this2.state[item.editValueKey])
										})
									)
								);
							})
						)
					),
					_react2.default.createElement('div', { className: 'col' })
				),
				this.renderChangeTextModal(_Queries2.default.SETTING_ITEM_DESCRIPTION_VALUE)
			);
		}
	}]);
	return ItemSettings;
}(_BaseSettings3.default);

exports.default = (0, _styles.withStyles)(styles)(ItemSettings);