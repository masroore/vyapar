var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SyncUtilityHelper = function () {
    function syncPermissionManager() {
        $('#syncmngpermission').dialog({
            closeOnEscape: true,
            height: 600,
            maxHeight: 600,
            width: 900,
            modal: true,
            maxWidth: 900,
            close: function close() {
                $('#addmail').unbind();
                $('#admin').unbind();
                $('.newsec1').unbind();
                $('ul#lines').unbind();
                $('#syncmngpermission').addClass('hide');
                $('#syncmngpermission').dialog('destroy');
                $('#mail').val('');
            },
            open: function open() {
                $('.ui-dialog .ui-dialog-content').css({ 'padding': 0 });
            }
        });

        getAllowedUsers();
        window.deleteDialog = deleteDialog;

        $('#addmail').click(function (event) {
            event.preventDefault();
            dataEntered = $('#mail').val();
            var SqliteDBHelper = require('../../../DBManager/SqliteDBHelper.js');
            var sqliteDBHelper = new SqliteDBHelper();
            var globId = sqliteDBHelper.fetchCompanyGlobalId();
            var TokenForSync = require('../../../Utilities/TokenForSync');
            var VYAPAR_AUTH_TOKEN = TokenForSync.readToken().auth_token;
            function postForPermission(dataEntered) {
                $.ajax({
                    async: false,
                    type: 'POST',
                    url: domain + '/api/sync/company/users/add',
                    context: this,
                    headers: {
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + VYAPAR_AUTH_TOKEN
                    },
                    data: {
                        company_global_id: globId,
                        phone_email: dataEntered

                    },
                    success: function success(data) {
                        if (data.code && data.code == '200') {
                            var users = data.user;
                            var userNum = users.phone;
                            var userName = users.name;

                            document.getElementById('lines').innerHTML += '<div><li style="color:#097aa8" id="' + userNum + '" class="grantList">\n\t<div id="divsvg" class="floatLeft"><svg id="usersvg" fill="#097aa8" height="30" viewBox="0 0 24 24" width="30" xmlns="http://www.w3.org/2000/svg">\n\t<path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 3c1.66 0 3 1.34 3 3s-1.34 3-3 3-3-1.34-3-3 1.34-3 3-3zm0 14.2c-2.5 0-4.71-1.28-6-3.22.03-1.99 4-3.08 6-3.08 1.99 0 5.97 1.09 6 3.08-1.29 1.94-3.5 3.22-6 3.22z"/>\n\t<path d="M0 0h24v24H0z" fill="none"/>\n\t</svg></div>\n\t<b style="color:#097aa8" id="addedusername">' + userName + '</b>\n\t<p style="color:#808080;font-size: 12px;" id="addeduseremail">' + users.email + '</p>\n\t<div id="delIcon" >\n\t<svg id=\'deletebox\' class="floatRight" onclick=\'deleteDialog(' + userNum + ')\' fill="#808080" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">\n\t<path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/>\n\t<path d="M0 0h24v24H0z" fill="none"/>\n\t</svg>\n\t</div></li></div>';

                            $('#mail').val('');

                            $('#loading').hide();
                            ToastHelper.success(data.message);
                        } else {
                            ToastHelper.error(data.message);
                            $('#mail').val('');

                            $('#loading').hide();

                            if (data.code == '204') {
                                $('.inviteDialog').dialog({
                                    title: 'Want to Invite: ' + dataEntered,
                                    closeOnEscape: true,
                                    resizable: false,
                                    height: 150,
                                    minHeight: 150,
                                    maxHeight: 150,
                                    width: 500,
                                    modal: true,
                                    maxWidth: 500,

                                    buttons: [{
                                        text: 'Invite',
                                        id: 'inviteButton',
                                        class: 'terminalButton',
                                        click: function click() {
                                            if (dataEntered.match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/)) {
                                                chkData = {
                                                    email: dataEntered
                                                };

                                                inviteUser(chkData);
                                                $(this).dialog('close');
                                                $(this).dialog('destroy');
                                            } else if (dataEntered.match(/^(?:(?:\+|0{0,2})[0-9]{1,3}(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/)) {
                                                chkData = {
                                                    phone: dataEntered
                                                };
                                                inviteUser(chkData);
                                                $(this).dialog('destroy');
                                            } else {
                                                ToastHelper.error(ErrorCode.ERROR_SYNC_VALID_MAIL_PHONE);
                                            }
                                        }
                                    }, {
                                        text: 'Cancel',
                                        id: 'cancelButton',
                                        class: 'terminalButton',
                                        click: function click() {
                                            $(this).dialog('close');
                                            $(this).dialog('destroy');
                                        }
                                    }]
                                });
                            }
                        }
                    },
                    error: function error(xhr, ajaxOptions, thrownError) {
                        // alert(JSON.parse(xhr.responseText).message);

                        if (xhr.status == 401) {
                            var SyncHelper = require('../../../Utilities/SyncHelper');
                            SyncHelper.loginToGetAuthToken();
                        } else if (xhr.status == 500) {
                            ToastHelper.error(ErrorCode.ERROR_WENT_WRONG);
                        } else if (xhr.status == 0 && xhr.statusText == 'timeout') {
                            ToastHelper.error(ErrorCode.ERROR_NO_NETWORK);
                        } else if (xhr.status != 0 && xhr.status != 401 && xhr.status != 500 && xhr.status != 200 && xhr.status != 403) {
                            ToastHelper.error(ErrorCode.ERROR_WENT_WRONG);
                        }

                        $('#loading').hide();
                        if (xhr.status == '403') {
                            $('#errmsg').dialog({
                                title: false,
                                closeOnEscape: true,
                                resizable: false,
                                height: 150,
                                minHeight: 150,
                                maxHeight: 150,
                                width: 500,
                                modal: true,
                                maxWidth: 500,

                                buttons: [{
                                    text: 'Ok',

                                    class: 'terminalButton',
                                    // icon: "ui-icon-heart",
                                    click: function click() {
                                        $(this).dialog('close');
                                        $(this).dialog('destroy');
                                    }
                                }]
                            });
                        } else {
                            $('#errmsg2').dialog({
                                title: JSON.parse(xhr.responseText).message,
                                closeOnEscape: true,
                                resizable: false,
                                height: 150,
                                minHeight: 150,
                                maxHeight: 150,
                                width: 500,
                                modal: true,
                                maxWidth: 500,

                                buttons: [{
                                    text: 'Ok',
                                    class: 'terminalButton',
                                    // icon: "ui-icon-heart",
                                    click: function click() {
                                        $(this).dialog('close');
                                        $(this).dialog('destroy');
                                    }
                                }]
                            });
                        }
                    }
                });
            }

            function inviteUser(thing) {
                $.ajax({
                    async: false,
                    type: 'POST',
                    url: domain + '/api/sync/invite',
                    context: this,
                    headers: {
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + VYAPAR_AUTH_TOKEN
                    },

                    data: thing,

                    success: function success(data) {
                        ToastHelper.success(ErrorCode.ERROR_SYNC_INVITE_SUCCESS);
                    },
                    error: function error(xhr, ajaxOptions, thrownError) {
                        ToastHelper.success(ErrorCode.ERROR_SYNC_INVITE_FAILED);
                    }
                });
            }

            if (dataEntered.match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/)) {
                $('#loading').show(function () {
                    postForPermission(dataEntered);
                });
            } else if (dataEntered.match(/^(?:(?:\+|0{0,2})[0-9]{1,3}(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/)) {
                $('#loading').show(function () {
                    postForPermission(dataEntered);
                });
            } else {
                $('#mail').val('');
                ToastHelper.error(ErrorCode.ERROR_SYNC_VALID_MAIL_PHONE);
            }
        });
    }

    function getAllowedUsers() {
        var SqliteDBHelper = require('../../../DBManager/SqliteDBHelper.js');
        var sqliteDBHelper = new SqliteDBHelper();
        var globId = sqliteDBHelper.fetchCompanyGlobalId();
        var TokenForSync = require('../../../Utilities/TokenForSync');
        var VYAPAR_AUTH_TOKEN = TokenForSync.readToken().auth_token;
        var ErrorCode = require('../../../Constants/ErrorCode');

        $.ajax({
            async: false,
            type: 'GET',
            url: domain + '/api/sync/company/users/' + globId,
            context: this,
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + VYAPAR_AUTH_TOKEN
            },
            success: function success(data) {
                try {
                    var fs = require('fs');
                    var TokenForSync = require('../../../Utilities/TokenForSync');

                    if (TokenForSync.ifExistToken()) {
                        var jsonData = TokenForSync.readToken().email;
                        var chkMyLogin = jsonData;
                    }
                } catch (e) {}
                // $.data(document,"chkMyLogin",chkMyLogin);
                try {
                    $('#loginlogout').text('Welcome ' + chkMyLogin);
                } catch (e) {}

                // $.data(document,"chkMyLogin",chkMyLogin);

                var users = data.users;

                document.getElementById('lines').innerHTML = '';

                if (users) {
                    for (var i = 0; i < users.length; i++) {
                        if (data.company.admin_user_id == users[i].id) {
                            var usernumadmin = users[i].phone;
                            var username = users[i].name;
                            document.getElementById('admin').innerHTML = '<div><li  id="' + usernumadmin + '" class="" style="list-style-type: none;height:75px;width:96%">\n\t<svg id="adminsvg" fill="#808080" height="48" viewBox="0 0 24 24" width="48" xmlns="http://www.w3.org/2000/svg">\n\t\t<path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 3c1.66 0 3 1.34 3 3s-1.34 3-3 3-3-1.34-3-3 1.34-3 3-3zm0 14.2c-2.5 0-4.71-1.28-6-3.22.03-1.99 4-3.08 6-3.08 1.99 0 5.97 1.09 6 3.08-1.29 1.94-3.5 3.22-6 3.22z"/>\n\t\t<path d="M0 0h24v24H0z" fill="none"/>\n\t</svg>&nbsp;&nbsp;&nbsp;\n\t<b style="color:#2B4C56"> ' + username + '</b></li></div><br />';
                            document.getElementById('admin').innerHTML += '<div><li  id="textAdmin" style="color:#808080;list-style-type: none;font-size: 12px;"> Administrator</li></div>';
                        } else {
                            var userNum = users[i].phone;
                            var _username = users[i].name;
                            document.getElementById('lines').innerHTML += '<div><li style="color:#097aa8" id="' + userNum + '" class="grantList">\n\t<div id="divsvg" class="floatLeft"><svg id="usersvg" fill="#097aa8" height="30" viewBox="0 0 24 24" width="30" xmlns="http://www.w3.org/2000/svg">\n\t\t<path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 3c1.66 0 3 1.34 3 3s-1.34 3-3 3-3-1.34-3-3 1.34-3 3-3zm0 14.2c-2.5 0-4.71-1.28-6-3.22.03-1.99 4-3.08 6-3.08 1.99 0 5.97 1.09 6 3.08-1.29 1.94-3.5 3.22-6 3.22z"/>\n\t\t<path d="M0 0h24v24H0z" fill="none"/>\n\t</svg></div>\n\t<b style="color:#097aa8" id="addedusername">' + _username + '</b>\n\t<p style="color:#808080;font-size: 12px;" id="addeduseremail">' + users[i].email + '</p>\n\t<div id="delIcon" >\n\t<svg id=\'deletebox\' class="floatRight" onclick=\'deleteDialog(' + userNum + ')\' fill="#808080" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">\n\t\t<path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/>\n\t\t<path d="M0 0h24v24H0z" fill="none"/>\n\t</svg>\n\t</div></li></div>';
                        }
                    }
                }
            },
            error: function error(xhr, ajaxOptions, thrownError) {
                if (xhr.status == 401) {
                    var SyncHelper = require('../../../Utilities/SyncHelper');
                    SyncHelper.loginToGetAuthToken();
                } else if (xhr.status == 500) {
                    ToastHelper.error(ErrorCode.ERROR_WENT_WRONG);
                } else if (xhr.status == 403) {
                    ToastHelper.error(ErrorCode.ERROR_NOT_AUTHORIZED_FOR_AUTO_SYNC);
                } else if (xhr.status == 0 && xhr.statusText == 'timeout') {
                    ToastHelper.error(ErrorCode.ERROR_NO_NETWORK);
                } else if (xhr.status != 0 && xhr.status != 401 && xhr.status != 500 && xhr.status != 200) {
                    ToastHelper.error(ErrorCode.ERROR_WENT_WRONG);
                }
                $('#syncmngpermission').dialog('close');
            }

        });
    }

    function deleteDialog(userNum) {
        $('.delDialog').dialog({
            title: 'Are you sure you want to delete this user?',
            closeOnEscape: true,
            resizable: false,
            height: 150,
            minHeight: 150,
            maxHeight: 150,
            width: 500,
            modal: true,
            maxWidth: 500,
            buttons: [{
                text: 'Yes',
                id: 'delButtonY',
                class: 'terminalButton',
                // icon: "ui-icon-heart",
                click: function click() {
                    deleteUser(userNum);

                    $(this).dialog('close');
                    $(this).dialog('destroy');
                }

            }, {
                text: 'No',
                id: 'delButtonN',
                class: 'terminalButton',
                click: function click() {
                    $(this).dialog('destroy');
                }
            }]
        });
    }

    function fetchCompanyGlobalId(successCallBack, failCallBack) {
        var request = require('request');
        var fs = require('fs');
        var SqliteDBHelper = require(__dirname + '/../../../DBManager/SqliteDBHelper');
        var path = fs.readFileSync(appPath + '/BusinessNames/currentDB.txt', 'utf-8');
        var formData = {
            vyapardb: fs.createReadStream(path)
        };

        var TokenForSync = require('../../../Utilities/TokenForSync');
        var tokenSync = TokenForSync.readToken().auth_token;

        var size = fs.lstatSync(path).size;
        var headers = {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + tokenSync
        };

        var SqliteDBHelper = require('../../../DBManager/SqliteDBHelper.js');
        var sqliteDBHelper = new SqliteDBHelper();
        var globId = sqliteDBHelper.fetchCompanyGlobalId();

        if (!globId) {
            // send post request only if company globalId  is null in db.
            // TODO: remove request library
            var r = request.post({
                url: domain + '/api/sync/file',
                formData: formData,
                headers: headers
            }, function optionalCallback(err, httpResponse, body) {
                httpResponsVar = httpResponse;

                if (httpResponse == undefined) {
                    ToastHelper.error('There is some problem with the connection. Please try again.');
                    if (q) {
                        clearInterval(q);
                        // $(chkthis).prop('checked', false);
                    }
                    $('.loaderWrapper2').hide();
                } else {
                    if (httpResponse.statusCode == '500') {
                        ToastHelper.error('Something Went wrong. Please Contact Vyapar Support.');
                        if (q) {
                            clearInterval(q);
                        }
                        $('.loaderWrapper2').hide();
                    } else if (httpResponse.statusCode == '401') {
                        ToastHelper.error('This user is not authenticated to change this setting. Please login again.');
                        var SyncHelper = require('../../../Utilities/SyncHelper');
                        SyncHelper.loginToGetAuthToken(function () {
                            fetchCompanyGlobalId(successCallBack, failCallBack);
                        });
                        if (q) {
                            clearInterval(q);
                        }
                        $('.loaderWrapper2').hide();
                    } else if (httpResponse.statusCode == 200) {
                        var syncResponse = JSON.parse(body);
                        var db = new SqliteDBHelper();
                        var res = db.setCompanyGlobalId(syncResponse.file_name);
                        settingCache.reloadSettingCache();
                        if (res === 0) {
                            var SyncController = require('../../../UIControllers/SyncController.js');
                            var syncController = new SyncController();
                            syncController.turnOnSocketEventsForSync();

                            if (successCallBack) {
                                successCallBack();
                            }
                        } else {
                            if (failCallBack) {
                                failCallBack();
                            }
                        }
                    } else {
                        ToastHelper.error('Something is Not Right. Please Contact Vyapar Support.');
                        if (q) {
                            clearInterval(q);
                        }
                        $('.loaderWrapper2').hide();
                    }
                    clearInterval(q);

                    if (err) {
                        error = (0, _stringify2.default)(err);
                        var chk = error.search(/EHOSTUNREACH/);

                        if (chk > -1) {
                            ToastHelper.error(ErrorCode.ERROR_AUTO_SYNC_FAILED_NO_INTERNET);
                        }

                        $('#syncDesc').closest('.currSetting')['0'].childNodes[1].childNodes['0'].children['0'].children['0'].childNodes[0].checked = false;

                        $('.loaderWrapper').hide();
                        $('#loader .loaderWrapper').hide();
                        $('.loader').hide();
                        $('#loadingText').hide();
                        $('.loaderWrapper2').hide();

                        ToastHelper.error(ErrorCode.ERROR_AUTO_SYNC_FAILED);
                        if (q) {
                            clearInterval(q);
                        }
                        $('.loaderWrapper2').hide();
                    }
                }
            });
        } else {
            if (failCallBack) {
                failCallBack();
            } else {
                ToastHelper.error('Something Went wrong. Please Contact Vyapar Support.');
            }
        }

        var q = setInterval(function () {
            try {
                var dispatched = r.req.connection._bytesDispatched;

                var percent = Math.floor(dispatched * 100 / size);

                $('.loaderWrapper2').show(function () {
                    console.dir('Uploaded: ' + percent + '%');

                    $('.loadingText2').html(percent + '%');
                    $('.loadingText3').html('Enabling Sync');

                    if (percent >= 100) {
                        clearInterval(q);
                    }
                });
            } catch (e) {
                if (q) {
                    clearInterval(q);
                }
                $('.loaderWrapper2').hide();
            }
        }, 100);
    }

    function deleteUser(userNum) {
        var TokenForSync = require('../../../Utilities/TokenForSync');
        var VYAPAR_AUTH_TOKEN = TokenForSync.readToken().auth_token;
        var SqliteDBHelper = require('../../../DBManager/SqliteDBHelper.js');
        var sqliteDBHelper = new SqliteDBHelper();
        var globId = sqliteDBHelper.fetchCompanyGlobalId();

        $('#loading').show(function () {
            $.ajax({
                async: false,
                type: 'POST',
                url: domain + '/api/sync/company/users/delete',
                context: this,
                headers: {
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + VYAPAR_AUTH_TOKEN
                },
                data: {
                    company_global_id: globId,
                    phone_email: userNum

                },
                success: function success(data) {
                    if (data.code && data.code == '200') {
                        var lineId = userNum.toString();

                        $('#' + lineId).remove();
                        ToastHelper.success(data.message);
                        $('#loading').hide();
                    }
                },
                error: function error(xhr, ajaxOptions, thrownError) {
                    var ErrorCode = require('../../../Constants/ErrorCode.js');
                    ToastHelper.error(ErrorCode.ERROR_USER_PERMISSION_DELETE);
                }
            });
        });
    }

    function disableSync() {
        var SettingsModel = require('../../../Models/SettingsModel.js');
        var settingsModel = new SettingsModel();

        $('#loading').show();

        $('#syncul').hide();
        settingsModel.setSettingKey(Queries.SETTING_SYNC_ENABLED);
        var statusCode = settingsModel.UpdateSetting('0', { nonSyncableSetting: true });

        settingCache.reloadSettingCache();

        settingsModel.setSettingKey(Queries.SETTING_COMPANY_GLOBAL_ID);
        statusCode = settingsModel.UpdateSetting(null, { nonSyncableSetting: true });
        settingsModel.setSettingKey(Queries.SETTING_CHANGE_LOG_NUMBER);
        statusCode = settingsModel.UpdateSetting('0', { nonSyncableSetting: true });

        settingCache.refreshSettingsCache();
        // $("#loading").show(function(){$("#loading").html("100%");});
        $('#loading').hide();
        $('#loader').hide();

        SyncHandler.refreshSyncEnableValue();

        if (SyncHandler.isSyncEnabled()) {
            $('#syncul').show();
            $('#mng').closest('.currSetting').removeClass('hide');
            $('#syncDesc').closest('.currSetting')['0'].childNodes[1].childNodes['0'].children['0'].children['0'].childNodes[0].checked = true;
        } else {
            $('#syncul').hide();
            $('#mng').closest('.currSetting').addClass('hide');
        }

        if (statusCode == 'Settings have been saved successfully.') {
            SyncHandler.refreshSyncEnableValue();

            try {
                var CompanyListUtility = require('../../../Utilities/CompanyListUtility.js');
                var company_name = CompanyListUtility.getCurrentCompanyKey();
                var fileNameObj = CompanyListUtility.getCompaniesFileNameObject();
                var pathVar = fileNameObj[company_name].path;
                var SqliteDBHelper = require('../../../DBManager/SqliteDBHelper.js');
                var sqliteDBHelper = new SqliteDBHelper();
                var globId = sqliteDBHelper.fetchCompanyGlobalId();
                var lastNotificationSentAt = new Date().getTime();
                fileNameObj[company_name] = {
                    'path': pathVar,
                    'company_creation_type': 0,
                    'lastNotificationSentAt': lastNotificationSentAt

                };

                CompanyListUtility.updateCompanyListFile(fileNameObj);
            } catch (e) {}
        }

        return statusCode;
    };

    function enableSync() {
        var settingsModel = new SettingsModel();
        settingsModel.setSettingKey(Queries.SETTING_SYNC_ENABLED);
        statusCode = settingsModel.UpdateSetting('1', { nonSyncableSetting: true });
        settingsModel.setSettingKey(Queries.SETTING_CHANGE_LOG_NUMBER);
        statusCode = settingsModel.UpdateSetting('0', { nonSyncableSetting: true });

        try {
            if (statusCode == ErrorCode.ERROR_SETTING_SAVE_SUCCESS) {
                SyncHandler.refreshSyncEnableValue();
                ToastHelper.success(statusCode);
            } else {
                ToastHelper.error(statusCode);
            }
        } catch (e) {}

        try {
            $('.syncinfoimg').dialog({
                Height: 500,
                minWidth: 800,
                resizable: false,
                closeOnEscape: true,
                modal: true,
                appendTo: '#frameDiv',
                draggable: true
            });
        } catch (e) {}

        try {
            var CompanyListUtility = require('../../../Utilities/CompanyListUtility.js');
            var company_name = CompanyListUtility.getCurrentCompanyKey();
            var fileNameObj = CompanyListUtility.getCompaniesFileNameObject();
            var pathVar = fileNameObj[company_name].path;
            var SqliteDBHelper = require('../../../DBManager/SqliteDBHelper.js');
            var sqliteDBHelper = new SqliteDBHelper();
            var globId = sqliteDBHelper.fetchCompanyGlobalId();
            var lastNotificationSentAt = new Date().getTime();
            fileNameObj[company_name] = {
                'path': pathVar,
                'company_creation_type': 1,
                'company_global_id': globId,
                'lastNotificationSentAt': lastNotificationSentAt
            };

            CompanyListUtility.updateCompanyListFile(fileNameObj);
        } catch (e) {}
    };

    function failResultCallbackForSync() {
        ToastHelper.error(ErrorCode.ERROR_AUTO_SYNC_FAILED);
        $('#syncDesc').closest('.currSetting')['0'].childNodes[1].childNodes['0'].children['0'].children['0'].childNodes[0].checked = false;
        $('.loaderWrapper').hide();
        $('#loader .loaderWrapper').hide();
        $('.loader').hide();
        $('#loadingText').hide();
        $('#syncul').hide();
        $('#loading').hide();
        $('.loaderWrapper2').hide();
        SyncHandler.refreshSyncEnableValue;
    };

    function successResultCallBackSync() {
        $('#syncul').show();
        fetchCompanyGlobalId(function () {
            enableSync();
            $('#loading').hide();
            $('#loader').hide();
            $('#mng').closest('.currSetting').removeClass('hide');
            $('#loginbutton2').hide();
            $('#logoutbutton2').show();
            $('.loaderWrapper2').hide();
        }, failResultCallbackForSync);
    };

    function enableDisableSync(isChecked, callback) {
        var isOnline = require('is-online');
        isOnline().then(function (online) {
            if (online) {
                if (isChecked) {
                    var statusCode;

                    var TokenForSync = require('../../../Utilities/TokenForSync');
                    if (!TokenForSync.ifExistToken()) {
                        var SyncHelper = require('../../../Utilities/SyncHelper');
                        SyncHelper.loginToGetAuthToken(successResultCallBackSync, failResultCallbackForSync);
                    } else {
                        $('#syncul').show();
                        fetchCompanyGlobalId(function () {
                            enableSync();
                            $('#loading').hide();
                            $('#loader').hide();
                            $('#mng').closest('.currSetting').removeClass('hide');
                            $('.loaderWrapper2').hide();
                        }, failResultCallbackForSync);
                        SyncHandler.refreshSyncEnableValue();
                    }
                } else {
                    if (SyncHandler.isAdminUser()) {
                        $('.loaderwrapper2').show();

                        var promiseDisableSync = new _promise2.default(function (resolve, reject) {
                            $('#disablesyncmessage').dialog({
                                title: false,
                                closeOnEscape: true,
                                resizable: false,
                                height: 170,
                                minHeight: 170,
                                maxHeight: 170,
                                width: 500,
                                appendTo: '#dynamicDiv',
                                modal: true,
                                maxWidth: 500,
                                close: function close() {
                                    callback(true);
                                    //$(chkthis).prop('checked', true);
                                    $(this).dialog('close');
                                    $(this).dialog('destroy');
                                },
                                buttons: [{
                                    text: 'Ok',
                                    class: 'terminalButton',
                                    click: function click() {
                                        resolve(true);
                                    }
                                }, {
                                    text: 'Cancel',
                                    class: 'terminalButton',
                                    click: function click() {
                                        resolve(false);
                                    }
                                }]
                            });
                        });
                        promiseDisableSync.then(function (confirmValue) {
                            var TokenForSync = require('../../../Utilities/TokenForSync');

                            if (confirmValue) {
                                var SqliteDBHelper = require('../../../DBManager/SqliteDBHelper.js');
                                var sqliteDBHelper = new SqliteDBHelper();
                                var globId = sqliteDBHelper.fetchCompanyGlobalId();
                                var tokenSync = TokenForSync.readToken().auth_token;
                                var currentSocketId = sqliteDBHelper.getCurrentSocketId();

                                $.ajax({
                                    async: false,
                                    type: 'POST',
                                    url: domain + '/api/sync/off',
                                    headers: {
                                        'Accept': 'application/json',
                                        'Authorization': 'Bearer ' + tokenSync
                                    },
                                    data: {
                                        company_global_id: globId,
                                        socketId: currentSocketId
                                    },

                                    success: function success(data) {
                                        statusCode = disableSync();
                                        $('#loading').hide();
                                        $('#loader').hide();
                                        $('#disablesyncmessage').dialog('close');
                                        callback(false);
                                        //$(chkthis).prop('checked', false);

                                        if (!SyncHandler.isSyncEnabled()) {
                                            var SyncController = require('../../../UIControllers/SyncController.js');
                                            var syncController = new SyncController();
                                            syncController.turnOffSocketEventsForSync();
                                        }
                                    },
                                    error: function error(xhr, ajaxOptions, thrownError) {
                                        if (xhr.status == 500) {
                                            ToastHelper.error(ErrorCode.ERROR_WENT_WRONG);
                                        } else if (xhr.status == 0 && xhr.statusText == 'timeout') {
                                            ToastHelper.error(ErrorCode.ERROR_NO_NETWORK);
                                        } else if (xhr.status != 0 && xhr.status != 401 && xhr.status != 500 && xhr.status != 200) {
                                            ToastHelper.error(ErrorCode.ERROR_WENT_WRONG);
                                        }

                                        $('#loading').hide();
                                        $('#loader').hide();
                                        callback(true);
                                        //$(chkthis).prop('checked', true);
                                        $('#disablesyncmessage').dialog('close');
                                    }
                                });
                            } else {
                                $('#disablesyncmessage').dialog('close');
                                callback(true);
                                //$(chkthis).prop('checked', true);
                            }
                        }).catch(function (error) {});
                    } else {
                        ToastHelper.error(ErrorCode.ERROR_ONLY_ADMIN_SYNC);
                        callback(true);
                        //$(chkthis).prop('checked', true);
                    }
                }
            } else {
                if (isChecked) {
                    callback(false);
                } else {
                    callback(true);
                }
                // if ($(chkthis).prop('checked')) {
                //     $(chkthis).prop('checked', false);
                // } else {
                //     $(chkthis).prop('checked', true);
                // }
                $('.loaderWrapper2').hide();
                ToastHelper.error(ErrorCode.ERROR_AUTO_SYNC_FAILED_NO_INTERNET);
            }
        });
    }

    return {
        enableDisableSync: enableDisableSync,
        syncPermissionManager: syncPermissionManager
    };
}();

module.exports = SyncUtilityHelper;