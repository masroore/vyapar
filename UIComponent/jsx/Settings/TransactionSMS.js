Object.defineProperty(exports, "__esModule", {
	value: true
});

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _extends3 = require('babel-runtime/helpers/extends');

var _extends4 = _interopRequireDefault(_extends3);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _BaseSettings2 = require('./BaseSettings');

var _BaseSettings3 = _interopRequireDefault(_BaseSettings2);

var _styles = require('@material-ui/core/styles');

var _ErrorCode = require('../../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

var _Queries = require('../../../Constants/Queries');

var _Queries2 = _interopRequireDefault(_Queries);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _TextField = require('@material-ui/core/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _MessageCache = require('../../../Cache/MessageCache');

var _MessageCache2 = _interopRequireDefault(_MessageCache);

var _TxnMessageConfigModel = require('../../../Models/TxnMessageConfigModel');

var _TxnMessageConfigModel2 = _interopRequireDefault(_TxnMessageConfigModel);

var _TxnMessageFormatter = require('../../../BizLogic/TxnMessageFormatter');

var _TxnMessageFormatter2 = _interopRequireDefault(_TxnMessageFormatter);

var _TxnTypeConstant = require('../../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _SettingsMapping = require('./SettingsMapping');

var _SettingsMapping2 = _interopRequireDefault(_SettingsMapping);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
	return {
		SaveButton: {
			marginLeft: '10px'
		},
		transactionSMSSettingsContainer: {
			borderTop: '1px solid #DDDDDD',
			margin: '15px 0',
			minHeight: '450px',
			'& .trans-message-fiels': {
				border: '2px solid #add3f9',
				borderRadius: '4px',
				boxShadow: '2px 2px 2px #add3f9'
			},
			'& .transaction-item': {
				padding: '10px'
			},
			'& .transaction-item.selected': {
				backgroundColor: '#add3f9',
				borderRadius: '4px 0 0 4px'
			},
			'& .heading': {
				color: '#223A52'
			},
			'& .selected-txn': {
				width: '18px',
				height: '18px'
			},
			'& .message-box': {
				backgroundColor: '#ffffff',
				minHeight: '250px',
				boxShadow: '1px 1px 3px #a59b9b',
				border: '1px solid  #f5e6e6',
				'& .form-wrapper': {
					padding: '0 15px'
				},
				'& .message-section': {
					paddingTop: '10px'
				},
				'& .heading': {
					color: '#b3b3b3'
				},
				'& .save-btn-section': {
					borderTop: '1px solid #e4dfdf',
					padding: '10px'
				}
			}
		}
	};
};

var TransactionSMS = function (_BaseSettings) {
	(0, _inherits3.default)(TransactionSMS, _BaseSettings);

	function TransactionSMS(props) {
		(0, _classCallCheck3.default)(this, TransactionSMS);

		var _this = (0, _possibleConstructorReturn3.default)(this, (TransactionSMS.__proto__ || (0, _getPrototypeOf2.default)(TransactionSMS)).call(this, props));

		_this.messageConfig = new _MessageCache2.default();
		_this.messageFormatter = new _TxnMessageFormatter2.default();
		_this.txnTypes = [{
			key: _Queries2.default.SETTING_TXN_MESSAGE_ENABLED_SALE,
			value: 'Sales',
			type: _TxnTypeConstant2.default.TXN_TYPE_SALE
		}, {
			key: _Queries2.default.SETTING_TXN_MESSAGE_ENABLED_PURCHASE,
			value: 'Purchase',
			type: _TxnTypeConstant2.default.TXN_TYPE_PURCHASE
		}, {
			key: _Queries2.default.SETTING_TXN_MESSAGE_ENABLED_SALERETURN,
			value: 'Sales Return',
			type: _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN
		}, {
			key: _Queries2.default.SETTING_TXN_MESSAGE_ENABLED_PURCHASERETURN,
			value: 'Purchase Return',
			type: _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN
		}, {
			key: _Queries2.default.SETTING_TXN_MESSAGE_ENABLED_CASHIN,
			value: 'Payment In',
			type: _TxnTypeConstant2.default.TXN_TYPE_CASHIN
		}, {
			key: _Queries2.default.SETTING_TXN_MESSAGE_ENABLED_CASHOUT,
			value: 'Payment Out',
			type: _TxnTypeConstant2.default.TXN_TYPE_CASHOUT
		}, {
			key: _Queries2.default.SETTING_TXN_MESSAGE_ENABLED_SALE_ORDER,
			value: 'Sale Order',
			type: _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER
		}, {
			key: _Queries2.default.SETTING_TXN_MESSAGE_ENABLED_PURCHASE_ORDER,
			value: 'Purchase Order',
			type: _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER
		}, {
			key: _Queries2.default.SETTING_TXN_MESSAGE_ENABLED_ESTIMATE_FORM,
			value: 'Estimate',
			type: _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE
		}, {
			key: _Queries2.default.SETTING_TXN_MESSAGE_ENABLED_DELIVERY_CHALLAN,
			value: 'Delivery Challan',
			type: _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN
		}];

		_this.state = (0, _extends4.default)({}, _this.transactionSMSState());
		_this.savePhoneNumber = _this.savePhoneNumber.bind(_this);
		_this.updateHeaderAndFooter = _this.updateHeaderAndFooter.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(TransactionSMS, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.higlightSearchedSetting();
			this.initializeToolTips();
		}
	}, {
		key: 'resetState',
		value: function resetState() {
			this.setState((0, _extends4.default)({}, this.transactionSMSState()));
		}
	}, {
		key: 'transactionSMSState',
		value: function transactionSMSState() {
			var _this2 = this,
			    _extends2;

			var txnTypeState = {};
			this.txnTypes.forEach(function (txnType) {
				txnTypeState[txnType.key] = _this2.isChecked(_this2.listOfSettings[txnType.key]);
			});
			var txnTypeId = this.txnTypes[0].type;

			return (0, _extends4.default)({}, txnTypeState, (_extends2 = {}, (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_TRANSACTION_MESSAGE_ENABLED, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_TRANSACTION_MESSAGE_ENABLED])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_TXN_MSG_TO_OWNER, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_TXN_MSG_TO_OWNER])), (0, _defineProperty3.default)(_extends2, _Queries2.default.SETTING_TXN_MSG_OWNER_NUMBER, this.settingCache.getOwnerTxnMsgPhoneNumber()), (0, _defineProperty3.default)(_extends2, 'txnTypeId', txnTypeId), (0, _defineProperty3.default)(_extends2, 'msgTxnFieldState', this.createMsgFieldState(txnTypeId)), _extends2), this.messageGenerator(txnTypeId));
		}
	}, {
		key: 'updateTxnField',
		value: function updateTxnField(fieldId) {
			var msgTxnFieldState = this.state.msgTxnFieldState;

			msgTxnFieldState[fieldId] = !msgTxnFieldState[fieldId];
			this.setState({
				msgTxnFieldState: msgTxnFieldState
			});
		}
	}, {
		key: 'updateHeaderAndFooter',
		value: function updateHeaderAndFooter() {
			this.updateMsgFields(this.headerFieldId, this.state.header, true);
			this.updateMsgFields(this.footerFieldId, this.state.footer, true);
		}
	}, {
		key: 'updateMsgFields',
		value: function updateMsgFields(fieldId, value) {
			var _this3 = this;

			var isHeaderOrFooter = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

			var field = this.messageConfig.getField(this.state.txnTypeId, fieldId);
			var msgTxnFieldState = this.state.msgTxnFieldState;

			if (!isHeaderOrFooter) {
				msgTxnFieldState[fieldId] = !msgTxnFieldState[fieldId];
			}
			if (!field) {
				// e.preventDefault();
				ToastHelper.error('Something went wrong.');
				return false;
			}
			var checked = Number(this.checkboxValueForSave(msgTxnFieldState[fieldId]));
			var model = new _TxnMessageConfigModel2.default(field.txnType, field.txnFieldId, field.txnFieldName, isHeaderOrFooter ? value : checked);

			// let updatePromise = new Promise
			model.save(false).then(function (status) {
				ToastHelper.success(status);
				if (isHeaderOrFooter) {
					field.txnFieldValue = value;
				} else {
					field.txnFieldValue = Number(checked);
					_this3.setState((0, _extends4.default)({
						msgTxnFieldState: msgTxnFieldState
					}, _this3.messageGenerator(_this3.state.txnTypeId)));
				}
			}).catch(function (error) {
				ToastHelper.error(_ErrorCode2.default.ERROR_MESSAGE_CONFIG_SAVE_FAILED);
				logger.error(error);
			});
		}
	}, {
		key: 'createMsgFieldState',
		value: function createMsgFieldState(txnType) {
			var _this4 = this;

			var msgTxnFieldState = {};
			this.messageTxnFields = this.messageConfig.getByTxnType(txnType);
			if (this.messageTxnFields) this.messageTxnFields.map(function (fieldItem) {
				msgTxnFieldState[fieldItem.txnFieldId] = _this4.isChecked(fieldItem.txnFieldValue);
			});
			return msgTxnFieldState;
		}
	}, {
		key: 'createFieldsByTxnTypeJSX',
		value: function createFieldsByTxnTypeJSX() {
			var _this5 = this;

			var headerFieldId = void 0,
			    footerFieldId = void 0;
			var txnTypeJSX = this.messageTxnFields.map(function (fieldItem, i) {
				var canRenderField = fieldItem.txnFieldName.toLowerCase() !== 'header' && fieldItem.txnFieldName.toLowerCase() !== 'footer';
				if (!canRenderField) {
					if (fieldItem.txnFieldName.toLowerCase() == 'header') {
						headerFieldId = fieldItem.txnFieldId;
					} else if (fieldItem.txnFieldName.toLowerCase() == 'footer') {
						footerFieldId = fieldItem.txnFieldId;
					}
					return;
				}
				return _react2.default.createElement(
					'div',
					{ key: i, className: 'setting-item d-flex' },
					_react2.default.createElement(
						'div',
						{ className: 'd-flex align-items-center' },
						_react2.default.createElement(
							'label',
							{ className: 'input-checkbox' },
							_react2.default.createElement('input', { type: 'checkbox', onChange: _this5.updateMsgFields.bind(_this5, fieldItem.txnFieldId), checked: _this5.state.msgTxnFieldState[fieldItem.txnFieldId] }),
							_this5.checkboxJSX
						),
						_react2.default.createElement(
							'span',
							{ className: '' },
							fieldItem.txnFieldName
						)
					)
				);
			});
			this.headerFieldId = headerFieldId;
			this.footerFieldId = footerFieldId;
			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				txnTypeJSX
			);
		}
	}, {
		key: 'savePhoneNumber',
		value: function savePhoneNumber() {
			var regExPhone = /^\+?[0-9]{10,12}$/;
			var number = this.state[_Queries2.default.SETTING_TXN_MSG_OWNER_NUMBER];
			if (!regExPhone.test(number)) {
				ToastHelper.error('Please enter valid phone number without STD Code. Number should contain only 0 to 9 digits. Minimum length must be 10 and Maximum length must be 12.');
				return false;
			}
			this.saveSettings(_Queries2.default.SETTING_TXN_MSG_OWNER_NUMBER, number, true);
		}
	}, {
		key: 'selectTxnMsg',
		value: function selectTxnMsg(txnType) {
			var msgTxnFieldState = this.createMsgFieldState(txnType);
			this.setState((0, _extends4.default)({
				txnTypeId: txnType,
				msgTxnFieldState: msgTxnFieldState
			}, this.messageGenerator(txnType)));
		}
	}, {
		key: 'messageGenerator',
		value: function messageGenerator(txnType) {
			var sms = this.messageFormatter.getDefaultSampleMessage(txnType);
			return {
				header: sms.header,
				message: sms.message,
				footer: sms.footer
			};
		}
	}, {
		key: 'render',
		value: function render() {
			var _this6 = this;

			var classes = this.props.classes;

			return _react2.default.createElement(
				'div',
				{ className: 'transaction-sms fullContainerHeight' },
				_react2.default.createElement(
					'div',
					{ className: 'setting-item d-flex' },
					_react2.default.createElement(
						'div',
						{ className: 'd-flex align-items-center' },
						_react2.default.createElement(
							'label',
							{ className: 'input-checkbox' },
							_react2.default.createElement('input', { type: 'checkbox', onChange: this.toggleCheckbox(_Queries2.default.SETTING_TRANSACTION_MESSAGE_ENABLED), checked: this.state[_Queries2.default.SETTING_TRANSACTION_MESSAGE_ENABLED] }),
							this.checkboxJSX
						),
						_react2.default.createElement(
							'span',
							{ className: '' },
							_SettingsMapping2.default.transactionSMSSettings[_Queries2.default.SETTING_TRANSACTION_MESSAGE_ENABLED].heading
						),
						this.renderToolTip(_SettingsMapping2.default.transactionSMSSettings[_Queries2.default.SETTING_TRANSACTION_MESSAGE_ENABLED])
					),
					_react2.default.createElement(
						'div',
						{ className: 'd-flex align-items-center ml-10' },
						_react2.default.createElement(
							'label',
							{ className: 'input-checkbox' },
							_react2.default.createElement('input', { type: 'checkbox', onChange: this.toggleCheckbox(_Queries2.default.SETTING_TXN_MSG_TO_OWNER), checked: this.state[_Queries2.default.SETTING_TXN_MSG_TO_OWNER] }),
							this.checkboxJSX
						),
						_react2.default.createElement(
							'span',
							{ className: '' },
							_SettingsMapping2.default.transactionSMSSettings[_Queries2.default.SETTING_TXN_MSG_TO_OWNER].heading
						),
						this.renderToolTip(_SettingsMapping2.default.transactionSMSSettings[_Queries2.default.SETTING_TXN_MSG_TO_OWNER])
					),
					this.state[_Queries2.default.SETTING_TXN_MSG_TO_OWNER] && _react2.default.createElement(
						'div',
						{ className: 'd-flex align-items-center ml-10' },
						_react2.default.createElement(_TextField2.default, {
							label: 'My Phone Number',
							variant: 'outlined',
							type: 'number',
							margin: 'dense',
							value: this.state[_Queries2.default.SETTING_TXN_MSG_OWNER_NUMBER],
							onChange: this.updateInputChange(_Queries2.default.SETTING_TXN_MSG_OWNER_NUMBER)
						}),
						_react2.default.createElement(
							_Button2.default,
							{ onClick: this.savePhoneNumber, className: classes.SaveButton, variant: 'contained', color: 'primary' },
							'Save'
						)
					)
				),
				(this.state[_Queries2.default.SETTING_TRANSACTION_MESSAGE_ENABLED] || this.state[_Queries2.default.SETTING_TXN_MSG_TO_OWNER]) && _react2.default.createElement(
					'div',
					{ className: classes.transactionSMSSettingsContainer + ' row' },
					_react2.default.createElement(
						'div',
						{ className: 'col-7' },
						_react2.default.createElement(
							'div',
							{ className: 'row mt-15' },
							_react2.default.createElement(
								'div',
								{ className: 'col-4', style: { padding: '5px 0 5px 5px' } },
								_react2.default.createElement(
									'div',
									{ className: 'heading d-flex align-items-center mt-10' },
									_react2.default.createElement(
										'span',
										{ className: 'font-12' },
										_SettingsMapping2.default.transactionSMSSettings.AUTOMATIC_SMS_FOR.heading
									),
									this.renderToolTip(_SettingsMapping2.default.transactionSMSSettings.AUTOMATIC_SMS_FOR)
								),
								this.txnTypes.map(function (item) {
									return _react2.default.createElement(
										'div',
										{ key: item.key, onClick: _this6.selectTxnMsg.bind(_this6, item.type), className: (_this6.state.txnTypeId === item.type ? 'selected' : '') + ' setting-item d-flex justify-content-between transaction-item' },
										_react2.default.createElement(
											'div',
											{ className: 'd-flex  align-items-center' },
											_react2.default.createElement(
												'label',
												{ className: 'input-checkbox' },
												_react2.default.createElement('input', { type: 'checkbox', onChange: _this6.toggleCheckbox(item.key), checked: _this6.state[item.key] }),
												_this6.checkboxJSX
											),
											_react2.default.createElement(
												'span',
												{ className: '' },
												item.value
											)
										)
									);
								})
							),
							_react2.default.createElement(
								'div',
								{ className: 'col-7 trans-message-fiels' },
								_react2.default.createElement(
									'div',
									{ className: 'heading d-flex align-items-center mt-10' },
									_react2.default.createElement(
										'span',
										{ className: 'font-12' },
										_SettingsMapping2.default.transactionSMSSettings.FIELDS_ADDED_TO_MESSAGE_BODY.heading
									),
									this.renderToolTip(_SettingsMapping2.default.transactionSMSSettings.FIELDS_ADDED_TO_MESSAGE_BODY)
								),
								this.createFieldsByTxnTypeJSX()
							)
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'col-4 mt-15' },
						_react2.default.createElement(
							'div',
							{ className: 'heading d-flex align-items-center mt-10' },
							_react2.default.createElement(
								'span',
								{ className: 'font-12' },
								_SettingsMapping2.default.transactionSMSSettings.MESSAGE_PREVIEW.heading
							),
							this.renderToolTip(_SettingsMapping2.default.transactionSMSSettings.MESSAGE_PREVIEW)
						),
						_react2.default.createElement(
							'div',
							{ className: 'mt-20 message-box d-flex flex-column' },
							_react2.default.createElement(
								'div',
								{ className: 'form-wrapper' },
								_react2.default.createElement(
									'div',
									{ className: 'message-header message-section' },
									_react2.default.createElement(
										'p',
										{ className: 'font-13 heading' },
										'<Header>'
									),
									_react2.default.createElement(_TextField2.default, {
										multiline: true,
										rows: '2',
										margin: 'dense',
										value: this.state.header,
										className: 'width100',
										onChange: this.updateInputChange('header')
									})
								),
								_react2.default.createElement(
									'div',
									{ className: 'message-body message-section' },
									_react2.default.createElement(
										'p',
										{ className: 'font-13 heading' },
										'<Body>'
									),
									_react2.default.createElement(_TextField2.default, {
										multiline: true,
										rows: '5',
										margin: 'dense',
										value: this.state.message,
										InputProps: {
											readOnly: true
										},
										className: 'width100'
									})
								),
								_react2.default.createElement(
									'div',
									{ className: 'message-footer message-section' },
									_react2.default.createElement(
										'p',
										{ className: 'font-13 heading' },
										'<Footer>'
									),
									_react2.default.createElement(_TextField2.default, {
										multiline: true,
										rows: '2',
										margin: 'dense',
										value: this.state.footer,
										className: 'width100',
										onChange: this.updateInputChange('footer')
									})
								)
							),
							_react2.default.createElement(
								'div',
								{ className: 'save-btn-section mt-10' },
								_react2.default.createElement(
									_Button2.default,
									{ onClick: this.updateHeaderAndFooter, className: 'floatRight', variant: 'contained', color: 'primary' },
									'Save'
								)
							)
						)
					)
				)
			);
		}
	}]);
	return TransactionSMS;
}(_BaseSettings3.default);

exports.default = (0, _styles.withStyles)(styles)(TransactionSMS);