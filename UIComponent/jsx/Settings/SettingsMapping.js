Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.VyaparSettings = undefined;

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _moreGeneralTransacti, _deliveryChallanSubSe, _miscGeneralSettingsL, _transactionHeaderSet, _transactionItemTable, _transactionTaxDiscou, _moreTransactionSetti, _transactionSMSSettin, _partySettingsList, _miscPartySettingsLis, _itemSettingsList, _miscItemSettingsList, _totalsAndTaxSettings, _thermalPrinterSettin, _miscBasePrinterSetti, _miscGSTSettings, _gstSettingsList;

var _Queries = require('../../../Constants/Queries');

var _Queries2 = _interopRequireDefault(_Queries);

var _SettingCache = require('../../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var settingCache = new _SettingCache2.default();

var VyaparSettings = exports.VyaparSettings = {
	1: 'General',
	2: 'Transaction',
	3: 'Transaction SMS',
	4: 'Party',
	5: 'Item',
	6: 'Print',
	7: settingCache.isCurrentCountryIndia() ? 'Taxes & GST' : 'Taxes',
	8: 'Multi Device Sync'
};

function getTINNumberSettingObj() {
	var gstInText = settingCache.getGSTEnabled() ? 'GSTIN' : settingCache.getTINText();
	return {
		vyaparSettingIndex: 1,
		searchText: gstInText + ' Number Party',
		heading: gstInText + ' Number',
		what: 'You can enter ' + gstInText + ' of the party while adding a party to Vyapar. This ' + gstInText + ' will be printed on invoices to the parties.',
		why: 'In case you want Party\'s ' + gstInText + ' no. to be printed on invoice then you can enable this.'
	};
}

exports.default = SettingsMappings = {
	moreGeneralTransactionList: (_moreGeneralTransacti = {}, (0, _defineProperty3.default)(_moreGeneralTransacti, _Queries2.default.SETTING_ESTIMATE_ENABLED, {
		vyaparSettingIndex: 1,
		searchText: 'Estimate Quotation',
		heading: 'Estimate/Quotation',
		what: 'Enables the feature for generating Estimate/Quotation. ',
		how: 'Vyapar will show Estimate/Quotation transaction on enabling this setting.',
		why: 'You can make Estimates or Quotation and share them with your parties. You can easily convert an estimate/quotation into sale invoice anytime.'
	}), (0, _defineProperty3.default)(_moreGeneralTransacti, _Queries2.default.SETTING_ORDER_FORM_ENABLED, {
		vyaparSettingIndex: 1,
		searchText: 'Sale Order Purchase Order',
		heading: 'Sale/Purchase Order',
		what: 'Allows you to make Sale/ Purchase orders and convert them to Sale Invoices/Purchase Bills.',
		how: 'Vyapar will allow you to make Sale and Purchase order transactions on enabling this setting.'
	}), (0, _defineProperty3.default)(_moreGeneralTransacti, _Queries2.default.SETTING_OTHER_INCOME_ENABLED, {
		vyaparSettingIndex: 1,
		searchText: 'Other Income Different Income Source',
		heading: 'Other Income',
		what: 'You can manintain income from other sources apart from your direct business in Vyapar. This income will be shown in your Profit and Loss report.',
		how: 'Enabling this settings will show "Other Income" type of transaction. You can use it for maintaining other incomes in Vyapar.',
		why: 'To track incomes from various sources other than your primary business at one place. E.g. Rent received etc.'
	}), (0, _defineProperty3.default)(_moreGeneralTransacti, _Queries2.default.SETTING_DELIVERY_CHALLAN_ENABLED, {
		vyaparSettingIndex: 1,
		searchText: 'Delivery Challan',
		heading: 'Delivery Challan',
		what: 'Enables the feature for generating Delivery Challans.',
		how: 'Vyapar will show Delivery Challan transaction on enabling this setting.',
		why: 'You can make Delivery Challans and share them with your parties.You can easily convert a Delivery Challan into sale invoice anytime.'
	}), _moreGeneralTransacti),
	deliveryChallanSubSettingsList: (_deliveryChallanSubSe = {}, (0, _defineProperty3.default)(_deliveryChallanSubSe, _Queries2.default.SETTING_DELIVERY_CHALLAN_RETURN_ENABLED, {
		vyaparSettingIndex: 1,
		searchText: 'Goods return Delivery Challan',
		heading: 'Goods return on <strong>Delivery Challan</strong>',
		what: 'Enables you to specify goods returned from the customer & mention only those goods that are actually sold while converting delivery challan to sale invoice.',
		how: 'While converting Delivery Challan to a Sale invoice, you can specify the quantity of items returned or not delivered to your Party.',
		why: 'If your party do not accept full delivery or returns some defective items back, you can easily enter them in Vyapar while converting a Delivery Challan to a Sale Invoice.'
	}), (0, _defineProperty3.default)(_deliveryChallanSubSe, _Queries2.default.SETTING_PRINT_AMOUNTS_IN_DELIVERY_CHALLAN, {
		vyaparSettingIndex: 1,
		searchText: 'Print amount Delivery Challan',
		heading: 'Print amount in <strong>Delivery Challan</strong>',
		what: 'Enables you to print amount, description and tax details on delivery challan.',
		why: 'This allows you to turn on total amount or pricing detail in delivery challan. This setting can be turned off if you dont want your transporter to know about the pricing of the transaction.'
	}), _deliveryChallanSubSe),
	miscGeneralSettingsList: (_miscGeneralSettingsL = {}, (0, _defineProperty3.default)(_miscGeneralSettingsL, _Queries2.default.SETTING_PASSCODE_ENABLED, {
		vyaparSettingIndex: 1,
		searchText: 'application Passcode application password secure business data protect',
		heading: 'Enable Passcode',
		what: 'Secure your data and privacy by enabling Passcode (Pin) to open Vyapar.',
		how: 'Everytime you open Vyapar, it will ask you for your passcode before you can see your data.',
		why: 'It is used to make sure only the owner or the right person can see your business data.'
	}), (0, _defineProperty3.default)(_miscGeneralSettingsL, _Queries2.default.SETTING_CURRENCY_SYMBOL, {
		vyaparSettingIndex: 1,
		searchText: 'Business Currency symbol country currency',
		heading: 'Business Currency',
		what: 'Select your Currency\'s symbol.',
		how: 'Currency symbol will be printed on all your transactions like sale, purchase, expenses etc.'
	}), (0, _defineProperty3.default)(_miscGeneralSettingsL, _Queries2.default.SETTING_AMOUNT_DECIMAL, {
		vyaparSettingIndex: 1,
		searchText: 'Amount upto decimal places',
		heading: 'Amount<p class="font-9">(upto Decimal Places)</p>',
		what: 'Specify the number of digits after decimal for amounts. Amounts will be printed with these decimal places.'
	}), (0, _defineProperty3.default)(_miscGeneralSettingsL, 'CHANGE_PASSCODE_TXT', {
		vyaparSettingIndex: 1,
		searchText: '',
		heading: 'Change Passcode',
		ignoreInSearch: true
	}), (0, _defineProperty3.default)(_miscGeneralSettingsL, 'ADD_FIRM', {
		vyaparSettingIndex: 1,
		searchText: 'Add Firm Multi firm',
		heading: 'Add Firm',
		what: 'Allows you to add multiple firms.'
	}), (0, _defineProperty3.default)(_miscGeneralSettingsL, _Queries2.default.SETTING_AUTO_BACKUP_ENABLED, {
		vyaparSettingIndex: 1,
		searchText: 'Automatic backup save data secure data google drive',
		heading: 'Auto Backup',
		what: 'Takes backup of your data automatically on your personal google drive.',
		how: 'Vyapar will automatically save your data on your Google drive if internet connection is available.',
		why: 'To keep your Data safe with you in your google drive.'
	}), (0, _defineProperty3.default)(_miscGeneralSettingsL, _Queries2.default.SETTING_AUTO_BACKUP_DURATION_DAYS, {
		vyaparSettingIndex: 1,
		searchText: 'Auto Backup days frequency',
		heading: 'Auto Backup every',
		what: 'Vyapar takes backup automatically in your google drive every time after specified number of days.',
		how: 'All the Automatic backups will be present in your Google Drive.',
		why: 'Even if you forget to take back up for your data, Vyapar will take back up of your data automatically, so that no data is lost.'
	}), (0, _defineProperty3.default)(_miscGeneralSettingsL, _Queries2.default.SETTING_BACKUP_REMINDER_DAYS, {
		vyaparSettingIndex: 1,
		searchText: 'manual backup reminder days frequency save data secure data',
		heading: 'Manual Backup Reminder',
		what: 'Get a reminder to take backup after specific no. of days',
		how: 'If you set a reminder for every 2 days. When you open Vyapar then after 2 days you will get a pop up & from there you can take the back up in your local/email/google drive.',
		why: 'It is to make sure you take a back up of your business data entered in Vyapar so that you dont loose your data & it is with you on your device.'
	}), (0, _defineProperty3.default)(_miscGeneralSettingsL, 'LAST_BACKUP', {
		vyaparSettingIndex: 1,
		searchText: 'Last Backup date time google drive',
		heading: 'Last Backup',
		what: 'The date & time at which the last back up was taken in your google drive.'
	}), (0, _defineProperty3.default)(_miscGeneralSettingsL, 'SIGNED_IN_AS', {
		vyaparSettingIndex: 1,
		searchText: '',
		heading: 'Signed in as',
		ignoreInSearch: true
	}), (0, _defineProperty3.default)(_miscGeneralSettingsL, 'AUTOLAUNCH', {
		vyaparSettingIndex: 1,
		searchText: 'Auto Launch Start Vyapar',
		heading: 'Enable Auto Launch',
		what: 'AutoLaunch Application on Window Start. If disabled you won\'t get payment reminder notifications.'
	}), (0, _defineProperty3.default)(_miscGeneralSettingsL, _Queries2.default.SETTING_TIN_NUMBER_ENABLED, {
		getTINNumberSettingObj: getTINNumberSettingObj
	}), _miscGeneralSettingsL),
	transactionHeaderSettings: (_transactionHeaderSet = {}, (0, _defineProperty3.default)(_transactionHeaderSet, _Queries2.default.SETTING_TAXINVOICE_ENABLED, {
		vyaparSettingIndex: 2,
		searchText: 'Tax Invoice',
		heading: 'Enable Tax Invoice',
		what: 'Enables you to add tax invoice for your transaction',
		ignoreInSearch: true
	}), (0, _defineProperty3.default)(_transactionHeaderSet, _Queries2.default.SETTING_TXNREFNO_ENABLED, {
		vyaparSettingIndex: 2,
		searchText: 'Automatic Invoice Bill Number No',
		heading: 'Invoice/Bill No.',
		what: 'Vyapar will assign an invoice/bill numbers automatically to your transactions like sale, expenses etc.',
		how: 'Invoice/bill numbers will be assigned to every invoice & the no. will keep on increasing as you keeping making invoices in Vyapar,  You can also change the number manually.',
		why: 'Keep track of your invoices and assign unique invoice numbers automatically.'
	}), (0, _defineProperty3.default)(_transactionHeaderSet, _Queries2.default.SETTING_ENABLE_DEFAULT_CASH_SALE, {
		vyaparSettingIndex: 2,
		searchText: 'Default Cash Sale Paid Transactions',
		heading: 'Cash Sale by default',
		what: 'All Sale Invoices will treated as Paid in Full without any pending dues. Vypar will allow you to make Sale invoices without selecting any Party too.',
		how: 'Cash Sale option will be selected by default in Sale Invoices. You can always change it to Credit invoice manually.',
		why: 'For retailers and businessmen doing over the counter sales where customer pays you the full amount upfront, this setting can be used.'
	}), (0, _defineProperty3.default)(_transactionHeaderSet, _Queries2.default.SETTING_ENABLE_DISPLAY_NAME, {
		vyaparSettingIndex: 2,
		searchText: 'Billing display party name',
		heading: 'Billing Name of Parties',
		what: 'Enables you to add Billing name in Sale Invoice and Sale Orders. This billing name will be printed in invoices in place of Party name.',
		how: 'On enabling this, You will be able to enter billing name in transactions to print on invoices.',
		why: 'Can be used when Customer names are very similar and proper names need to be printed on invoices for every customer.'
	}), (0, _defineProperty3.default)(_transactionHeaderSet, _Queries2.default.SETTING_PO_DETAILS_ENABLED, {
		vyaparSettingIndex: 2,
		searchText: 'customer P.O. details Transaction',
		heading: 'Customers P.O. Details on Transactions',
		what: 'Enables you to add PO date and PO number in Sale/Purchase Transactions',
		why: 'It is to track your PO against Sale or Purchase made.'
	}), _transactionHeaderSet),
	transactionItemTableSettings: (_transactionItemTable = {}, (0, _defineProperty3.default)(_transactionItemTable, _Queries2.default.SETTING_ENABLE_INCLUSIVE_EXCLUSIVE_TAX_ON_TRANSACTION, {
		vyaparSettingIndex: 2,
		searchText: 'Inclusive Exclusive Tax Rate Price Per Unit',
		heading: 'Inclusive/Exclusive Tax on Rate(Price/Unit)',
		what: 'You can make the Rate(price/unit) field on transactions to include or exclude tax. Now you can set inclusive and exclusive tax for sale and purchase separately.  For example, if price/unit of an item is 100 and tax is 5% then rate inclusive of tax is 105 and exclusive of tax is 100.',
		how: 'You can make the Rate(price/unit) to include or exclude the tax for items in any transaction.',
		why: 'Bills received from vendors or suppliers may have rate either inclusive or exclusive of tax. This setting helps you to enter the bills and transactions easily in Vyapar for both types of bills.'
	}), (0, _defineProperty3.default)(_transactionItemTable, _Queries2.default.SETTING_SHOW_PURCHASE_PRICE_IN_ITEM_DROP_DOWN, {
		vyaparSettingIndex: 2,
		searchText: 'Purchase Price Items',
		heading: 'Display Purchase Price of Items',
		what: 'Purchase price of items will be shown in Item list while adding any item to Sale, Purchase etc transactions.',
		why: 'This will help you in understanding at what price you bought the item & what should be the margin for selling that item.'
	}), (0, _defineProperty3.default)(_transactionItemTable, _Queries2.default.SETTING_SHOW_LAST_FIVE_SALE_PRICE, {
		vyaparSettingIndex: 2,
		searchText: 'Last 5 Five Sale Price Items Transaction Sold Item Sell Item',
		heading: 'Show last 5 Sale Price of Items',
		what: 'Vyapar will show sale prices of last 5 transactions for every item while adding it to Sale transactions.',
		why: 'You can see at what price you sold the item in last 5 transactions & gives an idea about what price should you sell at this time.'
	}), (0, _defineProperty3.default)(_transactionItemTable, _Queries2.default.SETTING_FREE_QTY_ENABLED, {
		vyaparSettingIndex: 2,
		searchText: 'Free Item Quantity Invoice Give Free Party',
		heading: 'Free Item Quantity',
		what: 'Once enabled you can add additional free quantity in invoices that you are giving to the Party for free.',
		why: 'In case you are running a sale offer of Buy 1 maggie packet & get another maggie packet free then to maintain your stock inventory correctly, you can enable this setting.'
	}), (0, _defineProperty3.default)(_transactionItemTable, _Queries2.default.SETTING_ENABLE_ITEM_COUNT, {
		vyaparSettingIndex: 2,
		searchText: 'Enter Count packet customer separately',
		heading: 'Count',
		what: 'Enables you to enter the count of the packets that you have given to the customer separately.',
		how: 'For eg: 1 sack which has 5kg of Rice & you have given that in separate packets of 1 Kg each.'
	}), _transactionItemTable),
	transactionTaxDiscountTotalSettings: (_transactionTaxDiscou = {}, (0, _defineProperty3.default)(_transactionTaxDiscou, _Queries2.default.SETTING_TAX_ENABLED, {
		vyaparSettingIndex: 2,
		searchText: 'Transaction wise Tax Allows apply single tax full entire Sale Purchase transaction bill instead applying different taxes item being sold purchased.',
		heading: 'Transaction wise Tax',
		what: 'Allows you to apply a single tax on entire Sale/Purchase transaction bill instead of applying different taxes on each item being sold or purchased.',
		how: 'You will be able to apply a single tax on all the line items of any transaction. This tax will be applied on subtotal of all the individual line items.',
		why: 'In case you have same type of stock items having same tax rates, you can enable this setting and easily apply tax just once on invoice for all sold/purchased items together.'
	}), (0, _defineProperty3.default)(_transactionTaxDiscou, _Queries2.default.SETTING_DISCOUNT_ENABLED, {
		vyaparSettingIndex: 2,
		searchText: 'Transaction wise Discount Allows apply single discount full Sale Purchase transaction bill instead applying different discounts item sold purchased.',
		heading: 'Transaction wise Discount',
		what: 'Allows you to apply a single discount on full Sale/Purchase transaction bill instead of applying different discounts on each item being sold or purchased.',
		how: 'You will be able to apply a single discount on all the line items of any transaction. This discount will be applied on subtotal of all the individual line items.',
		why: 'If you are giving discounts on entire Invoice or Bill rather then separate discounts on each stock item, then you can use this option.'
	}), (0, _defineProperty3.default)(_transactionTaxDiscou, _Queries2.default.SETTING_IS_ROUND_OFF_ENABLED, {
		vyaparSettingIndex: 2,
		searchText: 'Round Off Total gets option round off amount nearest digits selected',
		heading: 'Round Off Total',
		what: 'By enabling this you gets the option to round off amount to the nearest digits selected by you.',
		why: 'When you dont want any decimal value of the transactions, then you could round off to nearest 1, by selecting it.'
	}), _transactionTaxDiscou),
	moreTransactionSettings: (_moreTransactionSetti = {}, (0, _defineProperty3.default)(_moreTransactionSetti, _Queries2.default.SETTING_DISABLE_INVOICE_PREVIEW, {
		vyaparSettingIndex: 2,
		searchText: 'Do not Show Invoice Preview Preview Invoice PDF shown every time while making any new invoice.',
		heading: 'Do not Show Invoice Preview',
		what: 'Preview of Invoice PDF will not be shown every time while making any new invoice.'
	}), (0, _defineProperty3.default)(_moreTransactionSetti, 'passCodeForTransactionDelete', {
		vyaparSettingIndex: 2,
		searchText: 'Passcode for transaction edit delete Set Passcode password Pin deleting any transaction.',
		heading: 'Enable Passcode for transaction delete',
		what: 'Set Passcode or Pin for deleting any transaction.',
		why: 'This will save you from deleting any transaction by mistake and will not allow anyone else to delete any transaction.'
	}), (0, _defineProperty3.default)(_moreTransactionSetti, _Queries2.default.SETTING_DISCOUNT_IN_MONEY_TXN, {
		vyaparSettingIndex: 2,
		searchText: 'Discount During Payments add track discount during Payment In Payment Out transactions',
		heading: 'Discount During Payments',
		what: 'Enables you to add/ track discount during Payment-In and Payment-Out transactions.',
		why: 'For eg: A customer had to pay you 1000 Ruppees but while making the payment, She/he paid only 900 & you agreed to take 900 then 100 is the discount amount in payment in but the total of bill remains 1000. This amount can be mentioned while doing the payment in transaction.'
	}), (0, _defineProperty3.default)(_moreTransactionSetti, _Queries2.default.SETTING_BILL_TO_BILL_ENABLED, {
		vyaparSettingIndex: 2,
		searchText: 'Link Payments to Invoices link Payment In transactions money received unpaid invoices bills linking received payment invoice as Paid',
		heading: 'Link Payments to Invoices',
		what: 'You will be able to link the "Payment In" transactions or money you received to the unpaid invoices/bills of Party. On linking received payment to an invoice it will be marked as "Paid"',
		how: 'While receiving payments by "Payment In" transaction or while editing any sale/purchase transaction, Vyapar will show options to Receive and Link Payments.',
		why: 'Invoices statuses can be tracked from Unpaid, Partially Paid and Paid. Received payments can be linked to invoices for complete tracking.'
	}), (0, _defineProperty3.default)(_moreTransactionSetti, _Queries2.default.SETTING_PAYMENT_TERM_ENABLED, {
		vyaparSettingIndex: 2,
		searchText: 'Due Dates and Payment Terms enter Due Dates define Payment Terms on invoices bills invoice aging reports track Paid Unpaid Overdue invoices',
		heading: 'Due Dates and Payment Terms',
		what: 'You can enter Due Dates and define Payment Terms on invoices/bills. Vyapar will also show invoice aging reports that will let you track Paid, Unpaid and Overdue invoices.',
		how: 'You can define Payment terms and due dates while making invoices/bills. Vyapar will show them as Overdue if they are not paid by the Due date.',
		why: 'It is to track overdue payments on sale and purchase invoices. This will also give you sale ageing report, which gives you details about payments which are pending with their due dates.'
	}), _moreTransactionSetti),
	miscTransactionSettings: {
		ADDITIONAL_FIELDS: {
			vyaparSettingIndex: 2,
			searchText: 'Additional Fields extra fields transaction',
			heading: 'Additional Fields &gt;',
			what: 'Add extra fields on any transaction like Sale, Purchase etc.',
			how: 'Enable extra fields and give their names. These fields will be shown while entering transactions.',
			why: 'In case you have more information to be entered & tracked on transaction level or firm level you can enable these fields & name them based on your business requirement.'
		},
		TRANSPORTATION_DETAILS: {
			vyaparSettingIndex: 2,
			searchText: 'Transportation Details Enter print transportation details like transporter name vehicle number delivery date & delivery location',
			heading: 'Transportation Details &gt;',
			what: 'Enter and print transportation details like transporter name, vehicle number, delivery date & delivery location etc. on Sale Invoices and Delivery Challans.',
			how: 'You will be able to enter transport details while making Sale invoices or Delivery Challans and Vyapar will print them in PDFs.',
			why: 'For Businesses delivering goods outside states or those who need transport details printed on invoices/bills'
		},
		ADDITIONAL_CHARGES: {
			vyaparSettingIndex: 2,
			searchText: 'Additional Charges',
			heading: 'Additional Charges &gt;'
		}
	},
	transactionSMSSettings: (_transactionSMSSettin = {}, (0, _defineProperty3.default)(_transactionSMSSettin, _Queries2.default.SETTING_TRANSACTION_MESSAGE_ENABLED, {
		vyaparSettingIndex: 3,
		searchText: 'Send SMS to Party automatic SMS party',
		heading: 'Send SMS to Party',
		what: 'Vyapar will send an automatic SMS to your party immediately after the transaction has been recorded.',
		ignoreInSearch: !settingCache.isCurrentCountryIndia()
	}), (0, _defineProperty3.default)(_transactionSMSSettin, _Queries2.default.SETTING_TXN_MSG_TO_OWNER, {
		vyaparSettingIndex: 3,
		searchText: 'Send SMS Copy to Self automatic SMS to you',
		heading: 'Send SMS Copy to Self',
		what: 'Vyapar will send an automatic SMS to you immediately after the transaction has been recorded.',
		ignoreInSearch: !settingCache.isCurrentCountryIndia()
	}), (0, _defineProperty3.default)(_transactionSMSSettin, 'AUTOMATIC_SMS_FOR', {
		vyaparSettingIndex: 3,
		searchText: 'Automatic SMS for',
		heading: 'Automatic SMS for:',
		what: 'You can choose the types of transactions below, for which you would like to send an automatic SMS to your party or yourself.',
		ignoreInSearch: !settingCache.isCurrentCountryIndia()
	}), (0, _defineProperty3.default)(_transactionSMSSettin, 'FIELDS_ADDED_TO_MESSAGE_BODY', {
		vyaparSettingIndex: 3,
		searchText: 'Fields to be added to the Message Body',
		heading: 'Fields to be added to the Message Body',
		what: 'You can select details that you want to send in the automatic transaction SMS to your party & yourself.'
	}), (0, _defineProperty3.default)(_transactionSMSSettin, 'MESSAGE_PREVIEW', {
		vyaparSettingIndex: 3,
		searchText: 'Message Preview',
		heading: 'Message Preview',
		what: 'This is how the automatic transaction SMS will go to your party.'
	}), _transactionSMSSettin),
	partySettingsList: (_partySettingsList = {}, (0, _defineProperty3.default)(_partySettingsList, _Queries2.default.SETTING_PARTY_GROUP, {
		vyaparSettingIndex: 4,
		searchText: 'Party Grouping group similar type of parties together make groups assign parties to groups',
		heading: 'Party Grouping',
		what: 'You can group similar type of parties together. You can make groups and assign parties to the groups.',
		why: 'If you want to create groups of Customers, Vendors or Region wise Customers & see reports based on groups then you can enable this setting.'
	}), (0, _defineProperty3.default)(_partySettingsList, _Queries2.default.SETTING_PARTY_SHIPPING_ADDRESS_ENABLED, {
		vyaparSettingIndex: 4,
		searchText: 'Shipping Address add shipping address for party',
		heading: 'Shipping Address',
		what: 'Enables you to add shipping address for the party.',
		why: 'There are Parties for which Billing address is different than shipping address then it is important to mention the shipping adress so that the items are delivered at the shipping address only.'
	}), (0, _defineProperty3.default)(_partySettingsList, _Queries2.default.SETTING_PAYMENTREMIDNER_ENABLED, {
		vyaparSettingIndex: 4,
		searchText: 'Enable Payment Reminder automatic payment reminders help follow up with your parties get paid faster',
		heading: 'Enable Payment Reminder',
		what: 'Enables you to get automatic payment reminders that will help you follow up with your parties & get paid faster'
	}), _partySettingsList),
	miscPartySettingsList: (_miscPartySettingsLis = {
		PARTY_ADDITIONAL_FIELDS: {
			vyaparSettingIndex: 4,
			searchText: 'Additional fields extra fields to save more information for parties',
			heading: 'Additional fields',
			what: 'Add extra fields to save more information for parties.',
			why: 'In case you have more information to be entered & tracked on party level you can enable these fields & name them based on your business requirement.'
		}
	}, (0, _defineProperty3.default)(_miscPartySettingsLis, _Queries2.default.SETTING_PRINT_PARTY_SHIPPING_ADDRESS, {
		vyaparSettingIndex: 4,
		searchText: 'Print Shipping Address print shipping address party invoices bills',
		heading: 'Print Shipping Address',
		what: 'Enables you to print shipping address of the party on invoices/bills.',
		why: 'If there are Parties for which Billing address is different than shipping address then it is important to print the shipping adress so that the items are delivered at the shipping address only.'
	}), (0, _defineProperty3.default)(_miscPartySettingsLis, _Queries2.default.SETTING_PAYMENT_REMINDER_DAYS, {
		vyaparSettingIndex: 4,
		searchText: 'Remind me for payment due in remind due payments from inactive parties Specify number days inactivity',
		heading: 'Remind me for payment due in',
		what: 'Vyapar will remind you of due payments from inactive parties. Specify the number of days of inactivity by them.'
	}), _miscPartySettingsLis),
	itemSettingsList: (_itemSettingsList = {}, (0, _defineProperty3.default)(_itemSettingsList, _Queries2.default.SETTING_BARCODE_SCANNING_ENABLED, {
		vyaparSettingIndex: 5,
		searchText: 'Barcode Scan Item code serial numbers items time of entering transactions',
		heading: 'Barcode Scan',
		what: 'Enables you to scan item code/ serial numbers for your items at the time of entering transactions.',
		why: 'If you want to use barcode scanning for searching and adding items to the transactions, you can turn this feature on and use it.'
	}), (0, _defineProperty3.default)(_itemSettingsList, _Queries2.default.SETTING_STOCK_ENABLED, {
		vyaparSettingIndex: 5,
		searchText: 'Stock Maintenance maintain stock quantity stock value on enabling this setting able to check Stock inventory reports track quantity item inventory',
		heading: 'Stock Maintenance',
		what: 'Vyapar will maintain stock quantity and stock value on enabling this setting. You will be able to check Stock inventory reports and track the quantity of each item in your inventory.',
		why: 'It helps you to track how many units of that item you are currently having in your stock. \n Transactions like Sale or Purchase will adjust the stock quantities and inventory value automatically.'
	}), (0, _defineProperty3.default)(_itemSettingsList, _Queries2.default.SETTING_SHOW_LOW_STOCK_DIALOG, {
		vyaparSettingIndex: 5,
		searchText: 'Show Low Stock Dialog',
		heading: 'Show Low Stock Dialog',
		what: 'Vyapar will show confirm window for low stock while making transaction',
		why: 'Vyapar will show confirm window for low stock while making transaction'
	}), (0, _defineProperty3.default)(_itemSettingsList, _Queries2.default.SETTING_IS_ITEM_UNIT_ENABLED, {
		vyaparSettingIndex: 5,
		searchText: 'Items Unit units for Stock Items define units item Primary Secondary',
		heading: 'Items Unit',
		what: 'Define units for Stock Items like KG, Litre, Pieces, Units, Bags, Bottles etc.\n\t\t\tNote: You can also define 2 units for an item Primary and Secondary. \n\t\t\tE.g. If\t1 Bag contains 10 Bottles then your primary unit is Bag and Secondary unit is Bottles.'
	}), (0, _defineProperty3.default)(_itemSettingsList, _Queries2.default.SETTING_ITEM_CATEGORY, {
		vyaparSettingIndex: 5,
		searchText: 'Item Category make different categories assign items different categories easily track',
		heading: 'Item Category',
		what: 'You can make different categories and assign items to them. Your items will have different categories based on which you can easily track them.',
		why: 'You can see reports by different category of items & check their performance.'
	}), (0, _defineProperty3.default)(_itemSettingsList, _Queries2.default.SETTING_PARTY_ITEM_RATE, {
		vyaparSettingIndex: 5,
		searchText: 'Party Wise Item Rate remember different sale purchase prices same item different parties last sale purchase price any item selected party in transaction forms',
		heading: 'Party Wise Item Rate',
		what: 'Vyapar can remember different sale and purchase prices of same item for different parties. This setting will show you last sale/purchase price of any item to selected party in transaction forms. ',
		why: 'If you want to sell items at different prices to different parties, you can turn this on. Vyapar will show you the last Sale/Purchase price of item to the party selected in transaction forms.'
	}), (0, _defineProperty3.default)(_itemSettingsList, _Queries2.default.SETTING_ENABLE_ITEM_DESCRIPTION, {
		vyaparSettingIndex: 5,
		searchText: 'Description add item description',
		heading: 'Description',
		what: 'Enables you to add description for an item.'
	}), (0, _defineProperty3.default)(_itemSettingsList, _Queries2.default.SETTING_ITEMWISE_TAX_ENABLED, {
		vyaparSettingIndex: 5,
		searchText: 'Item wise Tax apply item tax separately',
		heading: 'Item wise Tax',
		what: 'Enables you to apply tax separately for each item in your sale and purchase transactions.',
		why: 'In case you are charging different tax on each item, you can enable this.'
	}), (0, _defineProperty3.default)(_itemSettingsList, _Queries2.default.SETTING_ITEMWISE_DISCOUNT_ENABLED, {
		vyaparSettingIndex: 5,
		searchText: 'Item wise Discount apply item discount separately',
		heading: 'Item wise Discount',
		what: 'Enables you to apply discount separately for each item in your sale and purchase transactions.',
		why: 'In case you are giving discount on each item separately, then you can enable this.'
	}), _itemSettingsList),
	miscItemSettingsList: (_miscItemSettingsList = {}, (0, _defineProperty3.default)(_miscItemSettingsList, _Queries2.default.SETTING_DIRECT_BARCODE_SCANNING_MODE_ENABLED, {
		vyaparSettingIndex: 5,
		searchText: 'Direct Barcode Scanning Mode default cash sale barcode setting directly start filling items opening transaction',
		heading: 'Direct Barcode Scan',
		what: 'This mode will let you scan Barcode for items directly as soon you open any transaction form',
		how: 'If default cash sale and barcode setting is ON, then by enabling this setting you can directly start filling items on opening any transactions.'
	}), (0, _defineProperty3.default)(_miscItemSettingsList, _Queries2.default.SETTING_ITEM_ENABLED, {
		vyaparSettingIndex: 5,
		searchText: 'Enable Item add Stock items sale purchase expense transactions',
		heading: 'Enable Item',
		what: 'Allows you to add Stock items in sale, purchase and expense transactions.'
	}), (0, _defineProperty3.default)(_miscItemSettingsList, _Queries2.default.SETTING_ITEM_TYPE, {
		vyaparSettingIndex: 5,
		searchText: 'What do you sell? Select type item sell product service both',
		heading: 'What do you sell?',
		what: 'Select the type of item you sell (product, service or both)'
	}), (0, _defineProperty3.default)(_miscItemSettingsList, _Queries2.default.SETTING_QUANTITY_DECIMAL, {
		vyaparSettingIndex: 5,
		searchText: 'Quantity upto decimal places Specify number digits after decimal item quantity',
		heading: 'Quantity <p class="font-9">(upto Decimal Places)</p>',
		what: 'Specify the number of digits after decimal for item quantity.'
	}), _miscItemSettingsList),
	totalsAndTaxSettings: (_totalsAndTaxSettings = {}, (0, _defineProperty3.default)(_totalsAndTaxSettings, _Queries2.default.SETTING_PRINT_ITEM_QUANTITY_TOTAL_ON_TXN_PDF, {
		vyaparSettingIndex: 6,
		searchText: 'Total Item Quantity print total item quantity invoices',
		heading: 'Total Item Quantity',
		what: 'Enables you to print total item quantity on invoices.'
	}), (0, _defineProperty3.default)(_totalsAndTaxSettings, _Queries2.default.SETTING_PRINT_AMOUNT_TILL_SPECIFIED_PLACES, {
		vyaparSettingIndex: 6,
		searchText: 'Amount with Decimal print amount upto specified decimal places always',
		heading: 'Amount with Decimal <span class="font-12 color-707070">e.g. 0.00</span>',
		what: 'Enables you to print amount upto specified decimal places always'
	}), (0, _defineProperty3.default)(_totalsAndTaxSettings, _Queries2.default.SETTING_SHOW_RECEIVED_AMOUNT_OF_TRANSACTION, {
		vyaparSettingIndex: 6,
		searchText: 'Received Amount print received amount invoices bills',
		heading: 'Received Amount',
		what: 'Enables you to print received amount on invoices/bills'
	}), (0, _defineProperty3.default)(_totalsAndTaxSettings, _Queries2.default.SETTING_SHOW_BALANCE_AMOUNT_OF_TRANSACTION, {
		vyaparSettingIndex: 6,
		searchText: 'Balance Amount print balance amount Pending Payment invoices bills',
		heading: 'Balance Amount',
		what: 'Enables you to print balance amount (Pending Payment) on invoices/bills'
	}), (0, _defineProperty3.default)(_totalsAndTaxSettings, _Queries2.default.SETTING_SHOW_CURRENT_BALANCE_OF_PARTY, {
		vyaparSettingIndex: 6,
		searchText: 'Current Balance of Party print current balance party invoice previous balance last invoice bill party',
		heading: 'Current Balance of Party',
		what: 'Enables you to print current balance of party on invoice. This will also print previous balance on the last invoice/bill of that party.',
		why: 'Enabling this setting will help the party know how much is the current balance pending to be paid to you.'
	}), (0, _defineProperty3.default)(_totalsAndTaxSettings, _Queries2.default.SETTING_PRINT_TAX_DETAILS, {
		vyaparSettingIndex: 6,
		searchText: 'Tax Details Tax distribution multiple taxes applied transaction',
		heading: 'Tax Details',
		what: 'Print Tax distribution of multiple taxes applied on transaction',
		why: 'This is used so that the Party is aware of the various taxes implied on the transaction.'
	}), _totalsAndTaxSettings),
	regularPrintTotalAndTaxSettings: (0, _defineProperty3.default)({}, _Queries2.default.SETTING_PRINT_AMOUNT_GROUPING, {
		vyaparSettingIndex: 6,
		searchText: 'Print Amount with Grouping print amount with grouping using comma as separator formats Indian English number system',
		heading: 'Print Amount with Grouping',
		what: 'Enables you to print amount with grouping using comma as separator. It support two formats - Indian & English number system.',
		how: 'Once enabled in Indian format, amount will come as 2,00,000'
	}),
	thermalPrinterSettings: (_thermalPrinterSettin = {}, (0, _defineProperty3.default)(_thermalPrinterSettin, _Queries2.default.SETTING_THERMAL_PRINTER_NATIVE_LANG, {
		vyaparSettingIndex: 6,
		searchText: 'Enable Native Language print in other languages thermal printer supports image printing',
		heading: 'Enable Native Language',
		what: 'Allows you to print in other languages, if your thermal printer supports image printing.',
		why: 'When you are entering item, party details in your native language, making an invoice & printing it using thermal printer.'
	}), (0, _defineProperty3.default)(_thermalPrinterSettin, _Queries2.default.SETTING_USE_ESC_POS_CODES_IN_THERMAL_PRINTER, {
		vyaparSettingIndex: 6,
		searchText: 'Use Text Styling print text stying like bold large text',
		heading: 'Use Text Styling(<b>Bold</b>)',
		what: 'Enables you to print text stying like bold, large text etc in thermal printer.'
	}), (0, _defineProperty3.default)(_thermalPrinterSettin, _Queries2.default.SETTING_ENABLE_AUTOCUT_PAPER, {
		vyaparSettingIndex: 6,
		searchText: 'Auto Cut Paper After Printing machine to auto cut invoice after printing done',
		heading: 'Auto Cut Paper After Printing',
		what: 'If you want the machine to auto cut your invoice after the printing is done. You can enable this setting.'
	}), (0, _defineProperty3.default)(_thermalPrinterSettin, _Queries2.default.SETTING_ENABLE_OPEN_DRAWER_COMMAND, {
		vyaparSettingIndex: 6,
		searchText: 'Open Cash Drawer After Printing cash drawer open after printing invoice',
		heading: 'Open Cash Drawer After Printing',
		what: 'If you want your cash drawer to open after printing an invoice then you can enable this setting. Please note: Your thermal printer should support the cash drawer feature, only then it will work.'
	}), _thermalPrinterSettin),
	miscBasePrinterSettings: (_miscBasePrinterSetti = {
		regularPrinter: {
			vyaparSettingIndex: 6,
			searchText: '',
			heading: 'Regular Printer',
			ignoreInSearch: true
		},
		thermalPrinter: {
			vyaparSettingIndex: 6,
			searchText: '',
			heading: 'Thermal Printer',
			ignoreInSearch: true
		}
	}, (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_AMOUNT_IN_WORD_FORMAT, {
		vyaparSettingIndex: 6,
		searchText: 'Amount in Words print amount with grouping using comma as separator formats IndianEnglish number system',
		heading: 'Amount in Words',
		what: 'Enables you to print amount with grouping using comma as separator. It support two formats - Indian & English number system',
		how: 'Once enabled in Indian format, amount will come as 2,00,000',
		why: 'It makes reading the number easier in the format which is comforble for you. For eg: in Thousands or Millions.'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_THERMAL_PRINTER_EXTRA_FOOTER_LINES, {
		vyaparSettingIndex: 6,
		searchText: 'Extra lines at the end Provide add extra spacing end invoice',
		heading: 'Extra lines at the end',
		what: 'Provides extra spacing at the end of the invoice for thermal printer'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_THERMAL_PRINTER_COPY_COUNT, {
		vyaparSettingIndex: 6,
		searchText: 'Number of copies multiple invoice copy',
		heading: 'Number of copies',
		what: 'Enables you to print 1 to 9 copies of the bill.'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_THERMAL_PRINTER_PAGE_SIZE, {
		vyaparSettingIndex: 6,
		searchText: '',
		heading: 'Page Size',
		what: 'Select the page size for printing thermal invoice.',
		ignoreInSearch: true
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_THERMAL_PRINTER_TEXT_SIZE, {
		vyaparSettingIndex: 6,
		searchText: '',
		heading: 'Text Size',
		what: 'If your printer supports small text size i.e. 42 characters in single line then only you can select small otherwise you have to select regular only.',
		ignoreInSearch: true
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_THERMAL_PRINTER_CUSTOMIZE_CHARACTER_COUNT, {
		vyaparSettingIndex: 6,
		searchText: 'Custom Character Length',
		heading: 'Custom Character Length'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_PRINT_COMPANY_NAME_ON_TXN_PDF, {
		vyaparSettingIndex: 6,
		searchText: 'Company Name',
		heading: 'Company Name',
		what: 'Enables you to print company name on the transaction PDF and party statement.'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_PRINT_COMPANY_ADDRESS_ON_TXN_PDF, {
		vyaparSettingIndex: 6,
		searchText: 'Address company address',
		heading: 'Address',
		what: 'Enables you to print your company address on invoices/bills and party statement.'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_PRINT_COMPANY_EMAIL_ON_PDF, {
		vyaparSettingIndex: 6,
		searchText: 'Email company email',
		heading: 'Email',
		what: 'Print your company email on invoices/bills.'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_PRINT_COMPANY_NUMBER_ON_TXN_PDF, {
		vyaparSettingIndex: 6,
		searchText: 'Phone Number company Contact number',
		heading: 'Phone Number',
		what: 'Enables you to print your company contact number on invoices/bills and party statement.'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_PRINT_TINNUMBER, {
		vyaparSettingIndex: 6,
		searchText: 'GSTIN on Sale',
		heading: 'GSTIN on Sale',
		what: 'Print your GSTIN on sale and Sale/Purchase orders.'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_PRINT_LOGO_ON_TXN_PDF, {
		vyaparSettingIndex: 6,
		searchText: 'Company Logo',
		heading: 'Company Logo',
		what: 'Enables you to print company logo on the transaction PDF and party statement.'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_PRINT_PAGE_SIZE, {
		vyaparSettingIndex: 6,
		searchText: '',
		heading: 'Paper Size',
		what: 'Select the size of paper for invoice print',
		ignoreInSearch: true
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_PRINT_COMPANY_NAME_TEXT_SIZE, {
		vyaparSettingIndex: 6,
		searchText: 'Company Name Text Size',
		heading: 'Company Name Text Size',
		what: 'Select the size of text of company name in header for invoice print',
		ignoreInSearch: true
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_PRINT_TEXT_SIZE, {
		vyaparSettingIndex: 6,
		searchText: 'Invoice Text Size',
		heading: 'Invoice Text Size',
		what: 'Select the size of text for invoice print'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_PRINT_BILL_OF_SUPPLY_FOR_NON_TAX_TXN, {
		vyaparSettingIndex: 6,
		searchText: 'Bill of Supply for Non Tax Transaction',
		heading: 'Bill of Supply for Non Tax Transaction',
		what: 'Enables you to print bill of supply for non tax transaction'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_PRINT_COPY_NUMBER, {
		vyaparSettingIndex: 6,
		searchText: 'Print Original/Duplicate print checkboxes original Duplicate Triplicate invoice header',
		heading: 'Print Original/Duplicate',
		what: 'Enables you to print checkboxes for original/ Duplicate/Triplicate on header of invoices/bills',
		how: 'This will just print the checkboxes "Original", "Duplicate" and "Triplicate" on header of invoices/bills. You can tick any checkbox while giving printed invoice to Parties.',
		why: 'If you want two copies of one transaction PDF, one for yourself & one to give it to transaporter, you can enable this.'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF, {
		vyaparSettingIndex: 6,
		searchText: 'Terms and Conditions',
		heading: 'Terms and Conditions &gt',
		what: 'Enables you to print terms and condition on invoice, Delivery Challan, Estimate and Sale Order.'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_PRINT_SALE_INVOICE_TERM_AND_CONDITION_ON_TXN_PDF, {
		vyaparSettingIndex: 6,
		searchText: 'sale Invoice Terms and Conditions',
		heading: 'Sale Invoice.',
		what: 'Enables you to print terms and condition on invoice, Delivery Challan, Estimate and Sale Order.'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_PRINT_SALE_ORDER_TERM_AND_CONDITION_ON_TXN_PDF, {
		vyaparSettingIndex: 6,
		searchText: 'Sale Order Terms and Conditions',
		heading: 'Sale order.',
		what: 'Enables you to print terms and condition on invoice, Delivery Challan, Estimate and Sale Order.'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_PRINT_DELIVERY_CHALLAN_TERM_AND_CONDITION_ON_TXN_PDF, {
		vyaparSettingIndex: 6,
		searchText: 'Delivery Challan Terms and Conditions',
		heading: 'Delivery challan',
		what: 'Enables you to print terms and condition on invoice, Delivery Challan, Estimate and Sale Order.'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_PRINT_ESTIMATE_QUOTATION_TERM_AND_CONDITION_ON_TXN_PDF, {
		vyaparSettingIndex: 6,
		searchText: 'Estimate Quotation Terms and Conditions',
		heading: 'Estimate Quotation.',
		what: 'Enables you to print terms and condition on invoice, Delivery Challan, Estimate and Sale Order.'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_SIGNATURE_ENABLED, {
		vyaparSettingIndex: 6,
		searchText: 'Print Signature Text Authorized Signatory signature area',
		heading: 'Print Signature Text',
		what: 'Authorized Signatory is written by default below your signature area, however you can change the text to whatever you want to.'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_PRINT_PAYMENT_MODE, {
		vyaparSettingIndex: 6,
		searchText: 'Payment Mode payment mode transaction cash cheque in bank',
		heading: 'Payment Mode',
		what: 'Enables you to print payment mode used in transaction',
		why: 'It is enabled to know the payment mode through which the payment came, like cash/ cheque/ in bank.'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_PRINT_BANK_DETAIL, {
		vyaparSettingIndex: 6,
		searchText: 'Bank Detail in PDF bank detail invoice',
		heading: 'Bank Detail in PDF',
		what: 'Enables you to print your bank detail in your invoices',
		why: 'In case you want the party to pay you in your bank then you can print your bank details on transactions.'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_PRINT_ACKNOWLEDGMENT, {
		vyaparSettingIndex: 6,
		searchText: 'Print Acknowledgement',
		heading: 'Print Acknowledgement',
		what: 'Enables you to print acknowledgment in transaction'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_PRINT_ENABLE_QR_CODE, {
		vyaparSettingIndex: 6,
		searchText: 'UPI QR Code QR Code printing invoices bills customers scan QR code UPI apps like Google Pay PhonePay BHIM pay now button PDF invoice itself receive payment directly your bank account',
		heading: 'UPI QR Code',
		what: 'Enables QR Code printing on invoices/bills from where customers can scan the QR code using UPI apps like Google Pay, PhonePay or BHIM etc or they can click on pay now button in the PDF itself. You will receive the payment directly in your bank account.',
		how: 'You need to turn this ON and give your Bank account number and IFSC code. Vyapar will safely store your information in your computer and use it to make UPI QR codes and print on your invoices.',
		why: 'Enabling this setting will help you in receiving payments through UPI directly in your Bank account.'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_PRINT_DESCRIPTION_ON_TXN_PDF, {
		vyaparSettingIndex: 6,
		searchText: 'Print Description transaction description invoices',
		heading: 'Print Description',
		what: 'Enables you to print transaction description on the invoices.'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, 'MAKE_REGULAR_PRINTER_DEFAULT', {
		vyaparSettingIndex: 6,
		searchText: 'Make Regular Printer Default',
		heading: 'Make Regular Printer Default',
		what: 'You can either make Regular or Thermal printer as your default printer. Choose the one which you will use.'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, 'MAKE_THERMAL_PRINTER_DEFAULT', {
		vyaparSettingIndex: 6,
		searchText: 'Make Thermal Printer Default',
		heading: 'Make Thermal Printer Default',
		what: 'You can either make Regular or Thermal printer as your default printer. Choose the one which you will use.'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_EXTRA_SPACE_ON_TXN_PDF, {
		vyaparSettingIndex: 6,
		searchText: 'Extra space on Top of PDF Adds Extra blank line top transaction PDF invoice',
		heading: 'Extra space on Top of PDF',
		what: 'Adds Extra blank line on top of the transaction PDF/ invoice print.',
		why: 'This setting can be enabled based on your preference.'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, _Queries2.default.SETTING_MIN_ITEM_ROWS_ON_TXN_PDF, {
		vyaparSettingIndex: 6,
		searchText: 'Min No. of Rows in Item Table minimum height item table',
		heading: 'Min No. of Rows in Item Table',
		what: 'Set the minimum height of item table based on your requirement',
		why: 'This will print the item table of the size you wish. Even if there is single item on your invoice, Item table will be printed of this size to cover the empty space on paper.'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, 'CHANGE_TRANSACTION_NAMES', {
		vyaparSettingIndex: 6,
		searchText: 'Change Transaction Names',
		heading: 'Change Transaction Names &gt;'
	}), (0, _defineProperty3.default)(_miscBasePrinterSetti, 'ITEM_TABLE_CUSTOMIZATION', {
		vyaparSettingIndex: 6,
		searchText: 'Item Table Customization customize item table change item table',
		heading: 'Item Table Customization &gt;'
	}), _miscBasePrinterSetti),
	miscGSTSettings: (_miscGSTSettings = {}, (0, _defineProperty3.default)(_miscGSTSettings, _Queries2.default.SETTING_GST_ENABLED, {
		vyaparSettingIndex: 7,
		searchText: 'Enable GST Goods and Services Tax Indian Government allows GST on Sale Purchase other types transactions',
		heading: 'Enable GST',
		what: 'GST stands for Goods and Services Tax of Indian Government. Enabling GST, allows you to apply GST on Sale, Purchase and other types of transactions.',
		how: 'Apply GST on Sale and/or purchase invoices. Generate GST reports for Tax filing.',
		why: 'Businesses which have GSTIN no. & are registered under regular  or composite scheme can enable GST. Vyapar will generate readymade GSTR Reports like GSTR - 1, GSTR - 3B for your GST filing in India.'
	}), (0, _defineProperty3.default)(_miscGSTSettings, _Queries2.default.SETTING_IS_COMPOSITE_SCHEME_ENABLED, {
		vyaparSettingIndex: 7,
		searchText: 'Composite Scheme Composite Business mode Vyapar not allowed add taxes transaction under composite Business scheme GST',
		heading: 'Composite Scheme',
		what: 'Enables Composite Business mode of Vyapar. You will not be allowed to add taxes on transaction if you are under composite Business scheme of GST.',
		why: 'If your company is registered under Composite GST Scheme. Then you can turn this on & then you have to  select the category of your business among the given options which are Manufacturing or Trader(Goods) or Restaurant.'
	}), _miscGSTSettings),
	gstSettingsList: (_gstSettingsList = {}, (0, _defineProperty3.default)(_gstSettingsList, _Queries2.default.SETTING_HSN_SAC_ENABLED, {
		vyaparSettingIndex: 7,
		searchText: 'Enable HSN/SAC Code enter track HSN SAC code for item',
		heading: 'Enable HSN/SAC Code',
		what: 'Enables you to enter & track HSN/ SAC code for item.'
	}), (0, _defineProperty3.default)(_gstSettingsList, _Queries2.default.SETTING_ENABLE_ADDITIONAL_CESS_ON_ITEM, {
		vyaparSettingIndex: 7,
		searchText: 'Additional Cess On Item',
		heading: 'Additional Cess On Item',
		what: 'Enables you to add additional cess on item.'
	}), (0, _defineProperty3.default)(_gstSettingsList, _Queries2.default.SETTING_ENABLE_REVERSE_CHARGE, {
		vyaparSettingIndex: 7,
		searchText: 'Reverse Charge purchase transaction',
		heading: 'Reverse Charge',
		what: 'Enables reverse charge for your purchase transaction'
	}), (0, _defineProperty3.default)(_gstSettingsList, _Queries2.default.SETTING_ENABLE_PLACE_OF_SUPPLY, {
		vyaparSettingIndex: 7,
		searchText: 'Enable Place of Supply add place of supply while making transaction',
		heading: 'Enable Place of Supply',
		what: 'Enables you to add place of supply while making transaction'
	}), (0, _defineProperty3.default)(_gstSettingsList, _Queries2.default.SETTING_ENABLE_EWAY_BILL_NUMBER, {
		vyaparSettingIndex: 7,
		searchText: 'Enable E-Way Bill Number add E-way bill number',
		heading: 'Enable E-Way Bill Number',
		what: 'Enables you to add E-way bill number in your transaction'
	}), _gstSettingsList),
	multiDeviceSync: {
		syncEnabled: {
			vyaparSettingIndex: 8,
			searchText: 'Multi Device Sync',
			heading: 'Multi Device Sync'
		}
	}
};