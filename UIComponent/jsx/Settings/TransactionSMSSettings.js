Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _BaseSettings2 = require('./BaseSettings');

var _BaseSettings3 = _interopRequireDefault(_BaseSettings2);

var _styles = require('@material-ui/core/styles');

var _Tabs = require('@material-ui/core/Tabs');

var _Tabs2 = _interopRequireDefault(_Tabs);

var _Tab = require('@material-ui/core/Tab');

var _Tab2 = _interopRequireDefault(_Tab);

var _TransactionSMS2 = require('./TransactionSMS');

var _TransactionSMS3 = _interopRequireDefault(_TransactionSMS2);

var _SMSSettings2 = require('./SMSSettings');

var _SMSSettings3 = _interopRequireDefault(_SMSSettings2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
	return {
		'@global': {
			'.MuiInput-underline:before': {
				borderBottom: 'none'
			},
			'.MuiInput-underline:hover:not(.Mui-disabled):before': {
				borderBottom: 'none'
			}
		}
	};
};

var TransactionSMSSettings = function (_BaseSettings) {
	(0, _inherits3.default)(TransactionSMSSettings, _BaseSettings);

	function TransactionSMSSettings(props) {
		(0, _classCallCheck3.default)(this, TransactionSMSSettings);

		var _this = (0, _possibleConstructorReturn3.default)(this, (TransactionSMSSettings.__proto__ || (0, _getPrototypeOf2.default)(TransactionSMSSettings)).call(this, props));

		_this.getTransactionSmsSettingsModule = function (moduleIndex) {
			switch (moduleIndex) {
				case 0:
					var _TransactionSMS = require('./TransactionSMS').default;
					return _react2.default.createElement(
						_BaseSettings2.TabContainer,
						null,
						_react2.default.createElement(_TransactionSMS, null)
					);
				case 1:
					var _SMSSettings = require('./SMSSettings').default;
					return _react2.default.createElement(
						_BaseSettings2.TabContainer,
						null,
						_react2.default.createElement(_SMSSettings, null)
					);
			}
		};

		_this.state = {
			// [Queries.SETTING_BARCODE_SCANNING_ENABLED]: this.isChecked(this.listOfSettings[Queries.SETTING_BARCODE_SCANNING_ENABLED]),
			activeIndex: 0
		};
		return _this;
	}

	(0, _createClass3.default)(TransactionSMSSettings, [{
		key: 'render',
		value: function render() {
			var activeIndex = this.state.activeIndex;

			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement(
					'div',
					{ className: 'transaction-sms-settings' },
					_react2.default.createElement(
						'div',
						{ className: 'row' },
						_react2.default.createElement(
							'div',
							{ className: 'col' },
							_react2.default.createElement(
								'div',
								{ className: 'row' },
								_react2.default.createElement(
									'div',
									{ className: 'col-6' },
									_react2.default.createElement(
										'span',
										{ className: 'heading font-family-bold font-16' },
										'Transaction SMS'
									),
									_react2.default.createElement('div', { className: 'border-bottom-2 mt-10 mb-10' })
								),
								_react2.default.createElement('div', { className: 'col-6' })
							),
							this.getTransactionSmsSettingsModule(0)
						)
					)
				)
			);
		}
	}]);
	return TransactionSMSSettings;
}(_BaseSettings3.default);

exports.default = (0, _styles.withStyles)(styles)(TransactionSMSSettings);