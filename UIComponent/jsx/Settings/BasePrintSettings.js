Object.defineProperty(exports, "__esModule", {
	value: true
});

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _BaseSettings2 = require('./BaseSettings');

var _BaseSettings3 = _interopRequireDefault(_BaseSettings2);

var _Queries = require('../../../Constants/Queries');

var _Queries2 = _interopRequireDefault(_Queries);

var _TextField = require('@material-ui/core/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _Modal = require('../Modal');

var _Modal2 = _interopRequireDefault(_Modal);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _SelectDropDown = require('../UIControls/SelectDropDown');

var _SelectDropDown2 = _interopRequireDefault(_SelectDropDown);

var _MenuItem = require('@material-ui/core/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _StringConstants = require('../../../Constants/StringConstants');

var _StringConstants2 = _interopRequireDefault(_StringConstants);

var _ErrorCode = require('../../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

var _SettingsMapping = require('./SettingsMapping');

var _SettingsMapping2 = _interopRequireDefault(_SettingsMapping);

var _StateCode2 = require('../../../Constants/StateCode');

var _StateCode3 = _interopRequireDefault(_StateCode2);

var _CustomCarousel = require('../UIControls/CustomCarousel');

var _CustomCarousel2 = _interopRequireDefault(_CustomCarousel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var BasePrintSettings = function (_BaseSettings) {
	(0, _inherits3.default)(BasePrintSettings, _BaseSettings);

	function BasePrintSettings(props) {
		var _this$customHeaderLab, _this$state;

		(0, _classCallCheck3.default)(this, BasePrintSettings);

		var _this = (0, _possibleConstructorReturn3.default)(this, (BasePrintSettings.__proto__ || (0, _getPrototypeOf2.default)(BasePrintSettings)).call(this, props));

		_this.changePrinterSettings = function () {
			var _this$setState;

			var printerRegular = _this.state[_Queries2.default.SETTING_DEFAULT_PRINTER] == 1;
			var value = printerRegular ? '2' : '1';
			_this.setState((_this$setState = {}, (0, _defineProperty3.default)(_this$setState, _Queries2.default.SETTING_DEFAULT_PRINTER, value), (0, _defineProperty3.default)(_this$setState, 'openDefaultPrinterModal', false), _this$setState), function () {
				_this.saveSettings(_Queries2.default.SETTING_DEFAULT_PRINTER, value);
			});
		};

		_this.toggleChangeTransactionNameModal = function () {
			_this.setState((0, _extends3.default)({
				openTransactionNameModal: !_this.state.openTransactionNameModal
			}, _this.getTransactionHeaderNameList()));
		};

		_this.toggleAddTermsAndConditionsModal = function () {
			$('.settingsToolTip').tooltipster({
				theme: 'tooltipster-noir',
				maxWidth: 300
			});
			_this.setState({
				openTermsAndConditionsModal: !_this.state.openTermsAndConditionsModal

			});
		};

		_this.closeDefaultPrinterModal = function () {
			_this.setState({
				openDefaultPrinterModal: false
			});
		};

		_this.toggleDefaultPrinterModal = function (e) {
			var defaultPrinterChangeWarning = _this.state[_Queries2.default.SETTING_DEFAULT_PRINTER] == 1 ? 'Removing Regular Printer from Default will make Thermal Printer as your preferred printer. Vyapar will print invoices from your Thermal Printer. Do you want to continue?' : 'Removing Thermal Printer from Default will make Regular Printer as your preferred printer. Vyapar will print invoices from your Regular Printer. Do you want to continue?';
			if (!e.target.checked) {
				_this.setState({
					openDefaultPrinterModal: !_this.state.openDefaultPrinterModal,
					defaultPrinterChangeWarning: defaultPrinterChangeWarning
				});
			} else {
				_this.changePrinterSettings();
			}
		};

		_this.saveCustomTransactionHeaderNames = function () {
			var customerHeadersState = {};
			customerHeadersState[_Queries2.default.SETTING_PRINT_BILL_OF_SUPPLY_FOR_NON_TAX_TXN] = _this.checkboxValueForSave(_this.state[_Queries2.default.SETTING_PRINT_BILL_OF_SUPPLY_FOR_NON_TAX_TXN]);
			(0, _keys2.default)(_this.customHeaderLabels).forEach(function (key) {
				customerHeadersState[key] = _this.state[key];
			});
			_this.saveMultipleSettings(customerHeadersState, true);
			_this.toggleChangeTransactionNameModal();
		};

		_this.saveFirmDetails = function (firmKey) {
			return function (event) {
				if (firmKey === 'companyName') {
					var companyName = _this.state['companyName'].trim();
					if (!companyName) {
						ToastHelper.error('Firm Name can not be empty. Please enter some Firm name.');
						return;
					}
					if (!_this.firmCache.isFirmNameUnique(companyName.trim()) && companyName.toLowerCase() != _this.defaultFirm.getFirmName().toLowerCase()) {
						ToastHelper.error(_ErrorCode2.default.ERROR_FIRM_ALREADYEXISTS);
						return;
					}
					_this.defaultFirm.setFirmName(companyName);
				}

				if (firmKey === 'companyAddress') {
					_this.defaultFirm.setFirmAddress(_this.state['companyAddress'].trim());
				}

				if (firmKey === 'companyPhone' || firmKey === 'companyEmail') {
					var companyPhoneOrEmail = _this.state['companyPhone'].trim() || _this.state['companyEmail'].trim();
					if (!companyPhoneOrEmail) {
						ToastHelper.error('Please provide Firm email or Firm phone.');
						return;
					}
				}

				if (firmKey === 'companyEmail') {
					_this.defaultFirm.setFirmEmail(_this.state['companyEmail'].trim());
				}

				if (firmKey === 'companyPhone') {
					_this.defaultFirm.setFirmPhone(_this.state['companyPhone'].trim());
				}

				var gstInNumberValue = _this.state['gstIn'];
				if (firmKey === 'gstIn') {
					if (_this.settingCache.getGSTEnabled()) {
						var regex = new RegExp(_StringConstants2.default.GSTINregex);
						if (!(regex.test(gstInNumberValue) || gstInNumberValue.trim() == '')) {
							ToastHelper.error('Invalid GSTIN Number.');
							_this.setState({
								gstIn: ''
							});
							return;
						} else {
							_this.defaultFirm.setFirmHSNSAC(gstInNumberValue);
							var stateNo = gstInNumberValue ? Number(gstInNumberValue.substr(0, 2)) : 0;
							var _StateCode = require('../../../Constants/StateCode.js');
							var state = _StateCode.getStateName(stateNo) || '';
							_this.defaultFirm.setFirmState(state);
						}
					} else {
						_this.defaultFirm.setFirmTin(_this.state['gstIn'].trim());
					}
				}

				var statusCode = _this.defaultFirm.updateFirmWithoutObject();
				if (statusCode !== _ErrorCode2.default.ERROR_FIRM_UPDATE_SUCCESS) {
					ToastHelper.error(statusCode);
				}

				if (firmKey === 'companyName' && statusCode === _ErrorCode2.default.ERROR_FIRM_UPDATE_SUCCESS) {
					$('#company').html(_this.state['companyName']);
				}
			};
		};

		_this.resetEditableView = function (elementId) {
			var editableDivs = document.getElementsByClassName('editable-div');
			for (var i = 0; i < editableDivs.length; i++) {
				var div = editableDivs.item(i);
				div.classList.remove('display-none');
			}
			_this.scrollTo('invoiceTemplatePreview', elementId);
		};

		_this.renderTermsAndConditions = function () {
			var multipleTermsAndConditionsJSX = _this.TermsAndConditionsSetting.map(function (item) {
				return _react2.default.createElement(
					'div',
					{ key: item.key,
						className: (item.control === 'textbox' ? 'height-72 mt-10 ' : '') + 'setting-item d-flex align-items-center justify-content-center ' + item.key
					},
					_react2.default.createElement(
						'label',
						{
							className: 'input-checkbox input-checkbox-new',
							style: { padding: "5px" } },
						_react2.default.createElement('input', { type: 'checkbox',
							checked: _this.state[item.key],
							onChange: _this.toggleCheckbox(item.key, true, _this.footerCallback.bind(_this, item.key))
						}),
						_this.checkboxJSX
					),
					item.control === 'textbox' && _react2.default.createElement(
						_react2.default.Fragment,
						null,
						_react2.default.createElement(_TextField2.default, (0, _extends3.default)({}, item.moreAttributes, {
							disabled: !_this.state[item.key],
							style: { padding: "5px" },
							label: item.value,
							margin: 'dense',
							className: 'width90',
							variant: 'outlined',
							value: _this.state[item.controlKey],
							onChange: _this.updateInputChange(item.controlKey, false, _this.footerCallback),
							onBlur: _this.saveSettings.bind(_this, item.controlKey, _this.state[item.controlKey])
						}))
					)
				);
			});

			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				multipleTermsAndConditionsJSX
			);
		};

		_this.saveAndClose = function () {
			ToastHelper.success("Settings saved successfully");
			_this.toggleAddTermsAndConditionsModal();
		};

		_this.renderCustomTermsAndConditionsModal = function () {
			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement(
					_Modal2.default,
					{
						width: '550px',
						height: '80%',
						isOpen: _this.state.openTermsAndConditionsModal,
						onClose: _this.saveAndClose,
						canHideHeader: 'true',
						title: 'Terms and conditions',
						info: _this.renderToolTip(_SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF], _Queries2.default.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF),
						parentClassSelector: 'transaction-names-modal'
					},
					_react2.default.createElement(
						'div',
						{ className: 'modalBody' },
						_this.renderTermsAndConditions(),
						_react2.default.createElement(
							'div',
							{ className: 'floatRight mt-20' },
							_react2.default.createElement(
								_Button2.default,
								{ margin: 'dense',
									onClick: _this.saveAndClose,
									variant: 'contained', color: 'primary' },
								' Save and Close.'
							)
						)
					)
				)
			);
		};

		_this.printSettings = [{ key: 1, value: 'Regular' }, { key: 2, value: 'Thermal' }];

		_this.textSizeDd = [{ key: 1, value: 'V. small' }, { key: 2, value: 'Small' }, { key: 3, value: 'Medium' }, { key: 4, value: 'Large' }, { key: 5, value: 'V. Large' }, { key: 6, value: 'E. Large' }];

		_this.customHeaderLabels = (_this$customHeaderLab = {}, (0, _defineProperty3.default)(_this$customHeaderLab, _Queries2.default.SETTING_CUSTOM_NAME_FOR_SALE, 'Sale'), (0, _defineProperty3.default)(_this$customHeaderLab, _Queries2.default.SETTING_CUSTOM_NAME_FOR_PURCHASE, 'Purchase'), (0, _defineProperty3.default)(_this$customHeaderLab, _Queries2.default.SETTING_CUSTOM_NAME_FOR_CASH_IN, 'Payment-In'), (0, _defineProperty3.default)(_this$customHeaderLab, _Queries2.default.SETTING_CUSTOM_NAME_FOR_CASH_OUT, 'Payment-Out'), (0, _defineProperty3.default)(_this$customHeaderLab, _Queries2.default.SETTING_CUSTOM_NAME_FOR_EXPENSE, 'Expense'), (0, _defineProperty3.default)(_this$customHeaderLab, _Queries2.default.SETTING_CUSTOM_NAME_FOR_INCOME, 'Other Income'), (0, _defineProperty3.default)(_this$customHeaderLab, _Queries2.default.SETTING_CUSTOM_NAME_FOR_SALE_ORDER, 'Sale Order'), (0, _defineProperty3.default)(_this$customHeaderLab, _Queries2.default.SETTING_CUSTOM_NAME_FOR_PURCHASE_ORDER, 'Purchase Order'), (0, _defineProperty3.default)(_this$customHeaderLab, _Queries2.default.SETTING_CUSTOM_NAME_FOR_ESTIMATE, 'Estimate'), (0, _defineProperty3.default)(_this$customHeaderLab, _Queries2.default.SETTING_CUSTOM_NAME_FOR_DELIVERY_CHALLAN, 'Delivery Challan'), (0, _defineProperty3.default)(_this$customHeaderLab, _Queries2.default.SETTING_CUSTOM_NAME_FOR_SALE_RETURN, 'Credit Note'), (0, _defineProperty3.default)(_this$customHeaderLab, _Queries2.default.SETTING_CUSTOM_NAME_FOR_PURCHASE_RETURN, 'Purchase Note'), _this$customHeaderLab);

		if (_this.settingCache.getTaxInvoiceEnabled()) {
			_this.customHeaderLabels[_Queries2.default.SETTING_CUSTOM_NAME_FOR_TAX_INVOICE] = 'Tax Sale Invoice';
		}

		_this.state = (_this$state = {}, (0, _defineProperty3.default)(_this$state, _Queries2.default.SETTING_DEFAULT_PRINTER, _this.listOfSettings[_Queries2.default.SETTING_DEFAULT_PRINTER]), (0, _defineProperty3.default)(_this$state, _Queries2.default.SETTING_PRINT_BILL_OF_SUPPLY_FOR_NON_TAX_TXN, _this.isChecked(_this.listOfSettings[_Queries2.default.SETTING_PRINT_BILL_OF_SUPPLY_FOR_NON_TAX_TXN])), (0, _defineProperty3.default)(_this$state, 'openTransactionNameModal', false), (0, _defineProperty3.default)(_this$state, 'openDefaultPrinterModal', false), (0, _defineProperty3.default)(_this$state, 'openTermsAndConditionsModal', false), _this$state);
		return _this;
	}
	// onclick method to open terms and conditions modal----------------------------------------------------------------------


	(0, _createClass3.default)(BasePrintSettings, [{
		key: 'getTransactionHeaderNameList',
		value: function getTransactionHeaderNameList() {
			var _ref;

			return _ref = {}, (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_PRINT_BILL_OF_SUPPLY_FOR_NON_TAX_TXN, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_BILL_OF_SUPPLY_FOR_NON_TAX_TXN])), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_CUSTOM_NAME_FOR_SALE, this.settingCache.getHeaderForSale()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_CUSTOM_NAME_FOR_TAX_INVOICE, this.settingCache.getHeaderForTaxSale()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_CUSTOM_NAME_FOR_PURCHASE, this.settingCache.getHeaderForPurchase()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_CUSTOM_NAME_FOR_CASH_IN, this.settingCache.getHeaderForCashIn()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_CUSTOM_NAME_FOR_CASH_OUT, this.settingCache.getHeaderForCashOut()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_CUSTOM_NAME_FOR_EXPENSE, this.settingCache.getHeaderForExpense()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_CUSTOM_NAME_FOR_INCOME, this.settingCache.getHeaderForIncome()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_CUSTOM_NAME_FOR_SALE_ORDER, this.settingCache.getHeaderForSaleOrder()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_CUSTOM_NAME_FOR_PURCHASE_ORDER, this.settingCache.getHeaderForPurchaseOrder()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_CUSTOM_NAME_FOR_ESTIMATE, this.settingCache.getHeaderForEstimate()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_CUSTOM_NAME_FOR_DELIVERY_CHALLAN, this.settingCache.getHeaderForDeliveryChallan()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_CUSTOM_NAME_FOR_SALE_RETURN, this.settingCache.getHeaderForSaleReturn()), (0, _defineProperty3.default)(_ref, _Queries2.default.SETTING_CUSTOM_NAME_FOR_PURCHASE_RETURN, this.settingCache.getHeaderForPurchaseReturn()), _ref;
		}
	}, {
		key: 'getCompanyPrintHeaderInfo',
		value: function getCompanyPrintHeaderInfo() {
			var _ref2;

			var firmState = this.defaultFirm.getFirmState();
			if (firmState) {
				firmState = _StateCode3.default.getStateCode(this.defaultFirm.getFirmState()) + '-' + this.defaultFirm.getFirmState();
			}
			return _ref2 = {}, (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_PRINT_COMPANY_NAME_ON_TXN_PDF, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_COMPANY_NAME_ON_TXN_PDF])), (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_PRINT_COMPANY_ADDRESS_ON_TXN_PDF, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_COMPANY_ADDRESS_ON_TXN_PDF])), (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_PRINT_COMPANY_EMAIL_ON_PDF, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_COMPANY_EMAIL_ON_PDF])), (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_PRINT_COMPANY_NUMBER_ON_TXN_PDF, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_COMPANY_NUMBER_ON_TXN_PDF])), (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_PRINT_TINNUMBER, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_TINNUMBER])), (0, _defineProperty3.default)(_ref2, _Queries2.default.SETTING_PRINT_PARTY_SHIPPING_ADDRESS, this.isChecked(this.listOfSettings[_Queries2.default.SETTING_PRINT_PARTY_SHIPPING_ADDRESS])), (0, _defineProperty3.default)(_ref2, 'companyName', this.defaultFirm.getFirmName()), (0, _defineProperty3.default)(_ref2, 'companyAddress', this.defaultFirm.getFirmAddress()), (0, _defineProperty3.default)(_ref2, 'companyEmail', this.defaultFirm.getFirmEmail()), (0, _defineProperty3.default)(_ref2, 'companyPhone', this.defaultFirm.getFirmPhone()), (0, _defineProperty3.default)(_ref2, 'gstIn', this.settingCache.getGSTEnabled() ? this.defaultFirm.getFirmHSNSAC() : this.defaultFirm.getFirmTin()), (0, _defineProperty3.default)(_ref2, 'state', firmState), _ref2;
		}
	}, {
		key: 'scrollTo',
		value: function scrollTo(containerId, elementIdOrClass) {
			if (containerId && elementIdOrClass) {
				var $element = document.getElementById(elementIdOrClass);
				if (!$element) {
					$element = document.getElementsByClassName(elementIdOrClass);
					$element = $element ? $element[0] : '';
				}
				if ($element) {
					$('#' + containerId).animate({
						scrollTop: $element.offsetTop
					}, 800);
				}
			}
			// scroller.scrollTo(elementIdOrClass, {
			// 	duration: 800,
			// 	delay: 0,
			// 	smooth: 'easeInOutQuart',
			// 	containerId: containerId
			// });
		}
	}, {
		key: 'footerCallback',
		value: function footerCallback(key, value) {
			if (key === _Queries2.default.SETTING_PRINT_ENABLE_QR_CODE && value) {
				this.setState({
					canShowCollectPaymentModal: true
				});
			}
		}
	}, {
		key: 'filterGSTINSettingForCompanyHeader',
		value: function filterGSTINSettingForCompanyHeader(companyInfoHeaderList) {
			var tinNumberItem = companyInfoHeaderList.filter(function (k) {
				return k.key === _Queries2.default.SETTING_PRINT_TINNUMBER;
			})[0];
			if (this.settingCache.isTINNumberEnabled()) {
				if (this.settingCache.getGSTEnabled()) {
					tinNumberItem.maxLength = 15;
				}
			} else {
				companyInfoHeaderList.splice(this.companyInfoHeaderList.indexOf(tinNumberItem), 1);
			}
		}
	}, {
		key: 'renderCompanyInfoHeader',
		value: function renderCompanyInfoHeader(companyInfoHeaderList) {
			var _this2 = this;

			var companyInfoHeaderJSX = companyInfoHeaderList.map(function (item) {
				return _react2.default.createElement(
					'div',
					{ key: item.key, className: (item.control === 'textbox' ? 'height-66' : '') + ' setting-item d-flex align-items-center ' + item.key },
					item.showCheckbox !== false && _react2.default.createElement(
						'label',
						{ className: 'input-checkbox' },
						_react2.default.createElement('input', { type: 'checkbox', checked: _this2.state[item.key], onChange: _this2.toggleCheckbox(item.key, true, _this2.headerCallback) }),
						_this2.checkboxJSX
					),
					item.control === 'checkbox' && _react2.default.createElement(
						'span',
						{ className: '' },
						item.value
					),
					item.key === _Queries2.default.SETTING_PRINT_LOGO_ON_TXN_PDF && _react2.default.createElement(
						'a',
						{ className: (_this2.state[item.key] ? '' : 'disable-input') + ' ml-10 font-12 color-1789FC d-block', onClick: _this2.changeLogoAndSignature.bind(_this2, true) },
						'(Change)'
					),
					item.control === 'textbox' && _react2.default.createElement(_TextField2.default, {
						disabled: !_this2.state[item.key],
						label: item.value,
						margin: 'dense',
						variant: 'outlined',
						className: item.controlKey !== 'companyName' ? 'width100' : '',
						value: _this2.state[item.controlKey],
						onChange: _this2.updateInputChange(item.controlKey, false, _this2.headerCallback),
						onBlur: _this2.saveFirmDetails(item.controlKey),
						inputProps: {
							maxLength: item.maxLength
						}
					}),
					item.control === 'dropdown' && _react2.default.createElement(
						_react2.default.Fragment,
						null,
						_react2.default.createElement(
							'span',
							{ className: 'w-180-px' },
							item.value
						),
						_react2.default.createElement(
							_SelectDropDown2.default,
							{ outlined: true, handleChange: _this2.saveSettings.bind(_this2, item.key),
								value: _this2.listOfSettings[item.key], name: 'fontSizeDd' },
							item.list.map(function (option, index) {
								return _react2.default.createElement(
									_MenuItem2.default,
									{ key: option.key, value: option.key },
									option.value
								);
							})
						)
					),
					_this2.renderToolTip(_SettingsMapping2.default.miscBasePrinterSettings[item.key], item.key)
				);
			});

			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				companyInfoHeaderJSX
			);
		}
	}, {
		key: 'renderFooter',
		value: function renderFooter() {
			var _this3 = this;

			var footerJSX = this.footerSettings.map(function (item) {
				return _react2.default.createElement(
					'div',
					{ key: item.key, className: (item.control === 'textbox' ? 'height-72' : '') + ' setting-item d-flex align-items-center ' + item.key },
					_react2.default.createElement(
						'label',
						{ className: 'input-checkbox' },
						_react2.default.createElement('input', { type: 'checkbox', checked: _this3.state[item.key], onChange: _this3.toggleCheckbox(item.key, true, _this3.footerCallback.bind(_this3, item.key)) }),
						item.controlKey !== _Queries2.default.SETTING_TERMS_AND_CONDITIONS && _this3.checkboxJSX
					),
					item.control === 'checkbox' && _react2.default.createElement(
						'span',
						{ className: '' },
						item.value
					),
					item.controlKey === _Queries2.default.SETTING_TERMS_AND_CONDITIONS && _react2.default.createElement(
						_react2.default.Fragment,
						null,
						_react2.default.createElement('a', { className: 'color-1789FC d-inline-block',
							dangerouslySetInnerHTML: { __html: _SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF].heading
							},
							onClick: function onClick() {
								return _this3.toggleAddTermsAndConditionsModal();
							}
						})
					),
					item.controlKey !== _Queries2.default.SETTING_TERMS_AND_CONDITIONS && item.control === 'textbox' && _react2.default.createElement(
						_react2.default.Fragment,
						null,
						_react2.default.createElement(_TextField2.default, (0, _extends3.default)({}, item.moreAttributes, {
							disabled: !_this3.state[item.key],
							label: item.value,
							margin: 'dense',
							variant: 'outlined',
							value: _this3.state[item.controlKey],
							onChange: _this3.updateInputChange(item.controlKey, false, _this3.footerCallback),
							onBlur: _this3.saveSettings.bind(_this3, item.controlKey, _this3.state[item.controlKey])
						}))
					),
					item.controlKey !== _Queries2.default.SETTING_TERMS_AND_CONDITIONS && _this3.renderToolTip(_SettingsMapping2.default.miscBasePrinterSettings[item.key], item.key),
					item.key === _Queries2.default.SETTING_SIGNATURE_ENABLED && _react2.default.createElement(
						'a',
						{ className: (_this3.state[item.key] ? '' : 'disable-input') + ' ml-10 font-12 color-1789FC d-block', onClick: _this3.changeLogoAndSignature.bind(_this3, false) },
						'Change Signature'
					)
				);
			});

			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				footerJSX
			);
		}
		//creating  modal component to be opened after clicking Terms and conditions on setting > print > footer.................................

	}, {
		key: 'renderCustomTransactionNamesModal',

		// ends here - --------------------------------------------------------------------------------------------
		value: function renderCustomTransactionNamesModal() {
			var _this4 = this;

			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement(
					_Modal2.default,
					{
						width: '550px',
						height: '80%',
						isOpen: this.state.openTransactionNameModal,
						onClose: this.toggleChangeTransactionNameModal,
						canHideHeader: 'true',
						title: 'Change Transaction Names',
						parentClassSelector: 'transaction-names-modal'
					},
					_react2.default.createElement(
						'div',
						{ className: 'modalBody' },
						_react2.default.createElement(
							'div',
							{ className: 'row' },
							(0, _keys2.default)(this.customHeaderLabels).map(function (key) {
								return _react2.default.createElement(
									_react2.default.Fragment,
									{ key: key },
									_react2.default.createElement(
										'div',
										{ className: 'col-6 mb-10' },
										_react2.default.createElement(_TextField2.default, {
											label: _this4.customHeaderLabels[key],
											margin: 'dense',
											variant: 'outlined',
											className: 'width100',
											value: _this4.state[key],
											onChange: _this4.updateInputChange(key)
										}),
										key === _Queries2.default.SETTING_CUSTOM_NAME_FOR_SALE && _react2.default.createElement(
											'div',
											{ className: 'setting-item d-flex align-items-center mt-10 mb-10 font-12 ' + _Queries2.default.SETTING_PRINT_BILL_OF_SUPPLY_FOR_NON_TAX_TXN },
											_react2.default.createElement(
												'label',
												{ className: 'input-checkbox' },
												_react2.default.createElement('input', { type: 'checkbox', onChange: _this4.toggleCheckbox(_Queries2.default.SETTING_PRINT_BILL_OF_SUPPLY_FOR_NON_TAX_TXN, false), checked: _this4.state[_Queries2.default.SETTING_PRINT_BILL_OF_SUPPLY_FOR_NON_TAX_TXN] }),
												_this4.checkboxJSX
											),
											_react2.default.createElement(
												'span',
												{ className: 'ml-10' },
												_SettingsMapping2.default.miscBasePrinterSettings[_Queries2.default.SETTING_PRINT_BILL_OF_SUPPLY_FOR_NON_TAX_TXN].heading
											)
										)
									)
								);
							})
						),
						_react2.default.createElement(
							'div',
							{ className: 'floatRight mt-10' },
							_react2.default.createElement(
								_Button2.default,
								{ margin: 'dense', onClick: this.toggleChangeTransactionNameModal, className: 'mr-10', variant: 'contained', color: 'primary' },
								' Cancel'
							),
							_react2.default.createElement(
								_Button2.default,
								{ margin: 'dense', onClick: this.saveCustomTransactionHeaderNames, variant: 'contained', color: 'primary' },
								' Save'
							)
						)
					)
				)
			);
		}
	}, {
		key: 'setCarouselIndex',
		value: function setCarouselIndex(selectedTemplate) {
			var _this5 = this;

			this.themesImagesList.forEach(function (value, index) {
				if (value.id === selectedTemplate) {
					_this5.activeItemIndex = index;
				}
			});
		}
	}, {
		key: 'renderThemeCarousel',
		value: function renderThemeCarousel(imageList, selectedImage, clickCallback) {
			var _this6 = this;

			var carouselItems = [];
			var FTUHelper = require('../../../Utilities/FTUHelper');
			imageList.map(function (image, i) {
				return carouselItems.push(_react2.default.createElement(
					'div',
					{ key: i, className: selectedImage === image.id ? 'selected-theme textAlignCentre' : 'textAlignCentre',
						onClick: clickCallback.bind(_this6, image.id) },
					_react2.default.createElement(
						'div',
						{ style: { height: 80, background: 'url(' + image.url + ') no-repeat center' }, className: 'selectThemeImage' },
						(image.id == 11 || image.id == 12) && !FTUHelper.isPremiumThemeClicked() && _react2.default.createElement('div', { className: 'themeRedDot' })
					),
					_react2.default.createElement(
						'label',
						{ className: 'font-11' },
						image.label
					)
				));
			});
			return _react2.default.createElement(
				_BaseSettings2.TabContainer,
				{ className: 'layout-container' },
				_react2.default.createElement(
					_CustomCarousel2.default,
					{ activeItemIndex: this.activeItemIndex },
					carouselItems
				)
			);
		}
	}, {
		key: 'renderDefaultPrinterModal',
		value: function renderDefaultPrinterModal() {
			return _react2.default.createElement(
				_Modal2.default,
				{ width: '600px',
					onClose: this.closeDefaultPrinterModal,
					isOpen: this.state.openDefaultPrinterModal,
					title: 'Change Default Printer' },
				_react2.default.createElement(
					'div',
					{ style: { padding: 15 } },
					_react2.default.createElement(
						'div',
						{ className: 'font-14' },
						this.state.defaultPrinterChangeWarning
					),
					_react2.default.createElement(
						'div',
						{ className: 'floatRight mt-10' },
						_react2.default.createElement(
							'button',
							{ onClick: this.closeDefaultPrinterModal, className: 'terminalButton' },
							'Cancel'
						),
						_react2.default.createElement(
							'button',
							{ onClick: this.changePrinterSettings, className: 'terminalButton' },
							'Done'
						)
					)
				)
			);
		}
	}]);
	return BasePrintSettings;
}(_BaseSettings3.default);

exports.default = BasePrintSettings;