Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = DateFilter;

var _MyDate = require('../../../Utilities/MyDate');

var _MyDate2 = _interopRequireDefault(_MyDate);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var EQUAL_TO = 0;
var LESS_THAN = 1;
var GREATER_THAN = 2;
var RANGE = 3;

function DateFilter() {}

DateFilter.prototype = {
	init: function init(params) {
		this.model = {
			from: '',
			to: '',
			filter: EQUAL_TO
		};
		this.valueGetter = params.valueGetter;
		this.setupGui(params);
	},
	setupGui: function setupGui(params) {
		var _this = this;

		this.gui = document.createElement('div');
		this.gui.innerHTML = '\n\t\t\t<div class="grid-external-filter">\n\t\t\t\t<div class="form-control">\n\t\t\t\t\t<select\n\t\t\t\t\t\tid="grid-filter-type"\n\t\t\t\t\t\tvalue="' + this.model.filter + '"\n\t\t\t\t\t\tclass="input-control rounded select-control"\n\t\t\t\t\t>\n\t\t\t\t\t\t<option value="' + EQUAL_TO + '">Equal to</option>\n\t\t\t\t\t\t<option value="' + GREATER_THAN + '">Greater than</option>\n\t\t\t\t\t\t<option value="' + LESS_THAN + '">Less than</option>\n\t\t\t\t\t\t<option value="' + RANGE + '">Range</option>\n\t\t\t\t\t</select>\n\t\t\t\t</div>\n\t\t\t\t<div class="form-control">\n\t\t\t\t  <label class="block-label" id="grid-from-label">From:</label>\n\t\t\t\t\t<input\n\t\t\t\t\t\tid=\'grid-from-input\'\n\t\t\t\t\t\ttype="text"\n\t\t\t\t\t\tplaceholder="dd/mm/yyyy"\n\t\t\t\t\t\tclass="input-control datepicker-input"\n\t\t\t\t\t\tvalue="' + this.model.from + '"\n\t\t\t\t\t/>\n\t\t\t\t</div>\n\t\t\t\t<div id="grid-to-container" class="form-control">\n\t\t\t\t  <label class="block-label">To:</label>\n\t\t\t\t\t<input\n\t\t\t\t\t\tid=\'grid-to-input\'\n\t\t\t\t\t\tplaceholder="dd/mm/yyyy"\n\t\t\t\t\t\ttype="text"\n\t\t\t\t\t\tclass="input-control datepicker-input"\n\t\t\t\t\t\tvalue="' + this.model.to + '"\n\t\t\t\t\t/>\n\t\t\t\t</div>\n\t\t\t\t<div style="position: relative" id=\'divToAppendPicker\'></div>\n\t\t\t\t<div class="actions">\n\t\t\t\t\t<button\n\t\t\t\t\t\tid=\'grid-clear-button\'\n\t\t\t\t\t\tvariant="contained"\n\t\t\t\t\t\tclass="mr-10 terminalButton"\n\t\t\t\t\t>\n\t\t\t\t\t\tClear\n\t\t\t\t\t</button>\n\t\t\t\t\t<button id=\'grid-save-button\' class="terminalButton"> Apply </button>\n\t\t\t\t</div>\n\t';
		var clearButton = this.gui.querySelector('#grid-clear-button');
		var saveButton = this.gui.querySelector('#grid-save-button');
		var filterType = this.gui.querySelector('#grid-filter-type');
		var fromInput = this.gui.querySelector('#grid-from-input');
		var toInput = this.gui.querySelector('#grid-to-input');
		var gridToContainer = this.gui.querySelector('#grid-to-container');
		var gridFromLabel = this.gui.querySelector('#grid-from-label');
		gridToContainer.style.display = 'none';
		gridFromLabel.style.display = 'none';

		var onFromDateChange = function onFromDateChange(date) {
			_this.model.from = date;
		};

		var onToDateChange = function onToDateChange(date) {
			_this.model.to = date;
		};

		$(fromInput).datepicker({
			dateFormat: 'dd/mm/yy',
			onSelect: onFromDateChange,
			beforeShow: function beforeShow(textbox, instance) {
				$('#divToAppendPicker').append($('#ui-datepicker-div'));
				setTimeout(function () {
					$('#ui-datepicker-div').css({ top: 0, left: 0 });
				}, 10);
			}
		});
		$(toInput).datepicker({
			dateFormat: 'dd/mm/yy',
			onSelect: onToDateChange,
			beforeShow: function beforeShow(textbox, instance) {
				$('#divToAppendPicker').append($('#ui-datepicker-div'));
				setTimeout(function () {
					$('#ui-datepicker-div').css({ top: 0, left: 0 });
				}, 10);
			}
		});

		fromInput.addEventListener('blur', function (e) {
			_this.model.from = e.target.value;
		});
		toInput.addEventListener('blur', function (e) {
			_this.model.to = e.target.value;
		});

		filterType.addEventListener('change', function (e) {
			var filter = Number(e.target.value);
			_this.model.filter = filter;
			if (filter == RANGE) {
				gridToContainer.style.display = 'block';
				gridFromLabel.style.display = 'block';
			} else {
				gridToContainer.style.display = 'none';
				gridFromLabel.style.display = 'none';
			}
		});
		clearButton.addEventListener('click', function () {
			fromInput.value = '';
			toInput.value = '';
			filterType.value = EQUAL_TO;
			_this.model = {
				from: '',
				to: '',
				filterType: EQUAL_TO
			};
			params.filterChangedCallback();
			_this.hidePopup && _this.hidePopup();
		});
		saveButton.addEventListener('click', function () {
			params.filterChangedCallback();
			_this.hidePopup && _this.hidePopup();
		});
	},
	getGui: function getGui() {
		return this.gui;
	},
	afterGuiAttached: function afterGuiAttached(params) {
		this.hidePopup = params.hidePopup;
	},
	doesFilterPass: function doesFilterPass(params) {
		var value = this.valueGetter(params.node);
		if (!(value instanceof Date)) {
			return false;
		}
		var currentCellValue = value.getTime();

		var _model = this.model,
		    from = _model.from,
		    to = _model.to,
		    filter = _model.filter;

		if (!from && !to && filter === EQUAL_TO) {
			return true;
		}

		var fromTimestamp = _MyDate2.default.getDateObj(from, 'dd/mm/yyyy', '/');
		if (fromTimestamp && fromTimestamp instanceof Date && fromTimestamp != 'Invalid Date') {
			fromTimestamp = fromTimestamp.getTime();
			switch (Number(filter)) {
				case EQUAL_TO:
					return currentCellValue === fromTimestamp;
				case GREATER_THAN:
					return currentCellValue > fromTimestamp;
				case LESS_THAN:
					return currentCellValue < fromTimestamp;
			}

			var toTimestamp = _MyDate2.default.getDateObj(to, 'dd/mm/yyyy', '/');
			if (toTimestamp && toTimestamp instanceof Date && toTimestamp != 'Invalid Date') {
				// toTimestamp.setHours(0, 0, 0, 0);
				toTimestamp = toTimestamp.getTime();
				return currentCellValue >= fromTimestamp && currentCellValue <= toTimestamp;
			}
		}
		return false;
	},
	isFilterActive: function isFilterActive() {
		var filterText = this.model.from;
		return filterText !== null && filterText !== undefined && filterText !== '';
	},
	getModel: function getModel() {
		return { value: this.model };
	},
	setModel: function setModel(model) {
		var state = model ? model.value : null;
		if (state) {
			this.model = state;
		}
	}
};