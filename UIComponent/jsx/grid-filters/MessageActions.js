Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _TransactionHelper = require('../../../Utilities/TransactionHelper');

var TransactionHelper = _interopRequireWildcard(_TransactionHelper);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _TxnTypeConstant = require('../../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _SettingCache = require('../../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

var _styles = require('@material-ui/core/styles');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _showParyNumberModal = require('../showParyNumberModal');

var _showParyNumberModal2 = _interopRequireDefault(_showParyNumberModal);

var _Print = require('@material-ui/icons/Print');

var _Print2 = _interopRequireDefault(_Print);

var _Reply = require('@material-ui/icons/Reply');

var _Reply2 = _interopRequireDefault(_Reply);

var _Sms = require('@material-ui/icons/Sms');

var _Sms2 = _interopRequireDefault(_Sms);

var _WhatsAppIcon = require('../icons/WhatsAppIcon');

var _WhatsAppIcon2 = _interopRequireDefault(_WhatsAppIcon);

var _ClickAwayListener = require('@material-ui/core/ClickAwayListener');

var _ClickAwayListener2 = _interopRequireDefault(_ClickAwayListener);

var _Paper = require('@material-ui/core/Paper');

var _Paper2 = _interopRequireDefault(_Paper);

var _Portal = require('@material-ui/core/Portal');

var _Portal2 = _interopRequireDefault(_Portal);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var actionStyles = function actionStyles() {
	return {
		iconWrapper: {
			cursor: 'pointer'
		},
		printIcon: {
			fill: '#284561',
			fontSize: 18,
			marginRight: 10,
			cursor: 'pointer'
		},
		replyIcon: {
			transform: 'rotateY(-180deg)',
			fill: '#284561',
			cursor: 'pointer',
			fontSize: 18
		},
		smsIcon: { color: '#ffb229' },
		whatsAppIcon: { fontSize: 24 },
		gmailIcon: { height: 24 },
		arrow: {
			position: 'absolute',
			top: -5,
			right: 15,
			border: 'solid #ddd',
			borderWidth: '0 1px 1px 0',
			display: 'inline-block',
			padding: 3,
			transform: 'rotate(-135deg)',
			background: '#fff'
		},
		dropdownContainer: { position: 'relative' },
		dropdown: {
			position: 'absolute',
			zIndex: 101,
			top: 20,
			right: 0,
			width: 215
		},
		textCenter: {
			textAlign: 'center'
		}
	};
};

var ActionsRendererClass = function (_React$Component) {
	(0, _inherits3.default)(ActionsRendererClass, _React$Component);

	function ActionsRendererClass(props) {
		(0, _classCallCheck3.default)(this, ActionsRendererClass);

		var _this = (0, _possibleConstructorReturn3.default)(this, (ActionsRendererClass.__proto__ || (0, _getPrototypeOf2.default)(ActionsRendererClass)).call(this, props));

		_this.toggleMenu = function (e) {
			var _this$state = _this.state,
			    open = _this$state.open,
			    top = _this$state.top,
			    left = _this$state.left;

			if (!open) {
				var position = e.target.getBoundingClientRect();
				top = position.top + position.height + 10;
				left = position.right - 200;
			}
			_this.setState({ open: !open, top: top, left: left });
		};

		_this.printTxn = function () {
			var _this$props$data = _this.props.data,
			    id = _this$props$data.id,
			    txnType = _this$props$data.txnType;

			TransactionHelper.print(id, _TxnTypeConstant2.default.getTxnType(txnType));
		};

		_this.sendEmail = function () {
			var TransactionFactory = require('../../../BizLogic/TransactionFactory');
			var MessageFormatter = require('../../../BizLogic/TxnMessageFormatter');
			var HTMLGenerator = require('../../../ReportHTMLGenerator/TransactionHTMLGenerator');
			var PDFHandler = require('../../../Utilities/PDFHandler');
			var pdfHandler = new PDFHandler();
			pdfHandler.generateHiddenBrowserWindowForPDF('');
			var _this$props$data2 = _this.props.data,
			    id = _this$props$data2.id,
			    txnType = _this$props$data2.txnType,
			    name = _this$props$data2.name,
			    email = _this$props$data2.email;

			var subject = void 0,
			    message = void 0;
			if (_this.messageConfigEnabledTxns.includes(txnType)) {
				var transactionFactory = new TransactionFactory();
				var messageFormatter = new MessageFormatter();
				var txn = transactionFactory.getTransactionObject(txnType);
				var txnObj = txn.getTransactionFromId(id);
				var htmlGenerator = new HTMLGenerator();
				var settingCache = new _SettingCache2.default();

				var themeColor = settingCache.getTxnPDFThemeColor();
				var theme = settingCache.getTxnPDFTheme();
				var html = htmlGenerator.getTransactionHTMLLayout(txnObj, theme, themeColor);
				pdfHandler.generateHiddenBrowserWindowForPDF(html);
				var txnTypeString = transactionFactory.getTransTypeString(Number(txnType));
				var msg = messageFormatter.createMessageForTxn(txnObj);
				subject = 'Your ' + txnTypeString + ' invoice details';
				message = 'Dear ' + name + ',\nThanks for doing business with us. Below are your invoice details. Also attached are the PDF of invoice for your convenience.\n' + msg.message + '\nThanks & Regards';
				setTimeout(function () {
					pdfHandler.sharePDF(email, null, subject, message, 'invoice');
				}, 1000);
			}
		};

		_this.sanitizeNumber = function (phoneNumber) {
			var number = '';
			var settingCache = new _SettingCache2.default();
			number = (phoneNumber || '').replace(/[^0-9]/, '');
			if (settingCache.isCurrentCountryIndia()) {
				number = '91' + number.slice(-10);
			}
			return number;
		};

		_this.sendSMS = function () {
			_this.setState({ showPartyNumberModal: true });
		};

		_this.onSMSDialogClose = function () {
			_this.setState({ showPartyNumberModal: false });
		};

		_this.sendWhatsAppMsg = function () {
			var TransactionFactory = require('../../../BizLogic/TransactionFactory');
			var MessageFormatter = require('../../../BizLogic/TxnMessageFormatter');
			var send = require('../../../Utilities/whatsapp').default;
			var _this$props$data3 = _this.props.data,
			    id = _this$props$data3.id,
			    txnType = _this$props$data3.txnType,
			    phoneNumber = _this$props$data3.phoneNumber;


			var transactionFactory = new TransactionFactory();
			var messageFormatter = new MessageFormatter();
			var txn = transactionFactory.getTransactionObject(txnType);
			var txnObj = txn.getTransactionFromId(id);
			send((0, _extends3.default)({
				to: _this.sanitizeNumber(phoneNumber)
			}, messageFormatter.createMessageForTxn(txnObj)));
		};

		_this.state = {
			open: false,
			left: 0,
			top: 0,
			showParyNumberModal: false
		};
		_this.messageConfigEnabledTxns = [_TxnTypeConstant2.default.TXN_TYPE_SALE, _TxnTypeConstant2.default.TXN_TYPE_PURCHASE, _TxnTypeConstant2.default.TXN_TYPE_CASHIN, _TxnTypeConstant2.default.TXN_TYPE_CASHOUT, _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN, _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN, _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER, _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE, _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER, _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN];
		return _this;
	}

	(0, _createClass3.default)(ActionsRendererClass, [{
		key: 'render',
		value: function render() {
			var _props = this.props,
			    classes = _props.classes,
			    _props$data = _props.data,
			    id = _props$data.id,
			    txnType = _props$data.txnType,
			    phoneNumber = _props$data.phoneNumber,
			    nameId = _props$data.nameId;
			var _state = this.state,
			    open = _state.open,
			    top = _state.top,
			    left = _state.left,
			    showPartyNumberModal = _state.showPartyNumberModal;

			return _react2.default.createElement(
				_react.Fragment,
				null,
				showPartyNumberModal && _react2.default.createElement(_showParyNumberModal2.default, {
					onClose: this.onSMSDialogClose,
					txnId: id,
					phoneNumber: phoneNumber,
					txnType: txnType,
					nameId: nameId
				}),
				_react2.default.createElement(
					'div',
					{ className: 'd-flex' },
					_react2.default.createElement(
						'div',
						null,
						_react2.default.createElement(_Print2.default, {
							onClick: this.printTxn,
							className: classes.printIcon
						})
					),
					_react2.default.createElement(
						'div',
						{ className: (0, _classnames2.default)('d-flex', classes.dropdownContainer) },
						_react2.default.createElement(
							'div',
							{ onClick: this.toggleMenu },
							_react2.default.createElement(_Reply2.default, { className: classes.replyIcon })
						),
						open && _react2.default.createElement(
							_Portal2.default,
							null,
							_react2.default.createElement(
								_ClickAwayListener2.default,
								{ onClickAway: this.toggleMenu },
								_react2.default.createElement(
									'div',
									{
										style: { top: top + 'px', left: left + 'px' },
										className: (0, _classnames2.default)(classes.dropdown)
									},
									_react2.default.createElement(
										_Paper2.default,
										null,
										_react2.default.createElement('div', { className: classes.arrow }),
										_react2.default.createElement(
											'div',
											{
												className: (0, _classnames2.default)('d-flex justify-content-around p5', classes.textCenter)
											},
											_react2.default.createElement(
												'div',
												{
													className: (0, _classnames2.default)(classes.textCenter, classes.iconWrapper),
													onClick: this.sendEmail
												},
												_react2.default.createElement('img', {
													src: '../inlineSVG/gmail.svg',
													className: classes.gmailIcon
												}),
												_react2.default.createElement(
													'div',
													null,
													'GMail'
												)
											),
											_react2.default.createElement(
												'div',
												{
													className: (0, _classnames2.default)(classes.textCenter, classes.iconWrapper),
													onClick: this.sendSMS
												},
												_react2.default.createElement(_Sms2.default, { className: classes.smsIcon }),
												_react2.default.createElement(
													'div',
													null,
													'SMS'
												)
											),
											_react2.default.createElement(
												'div',
												{
													onClick: this.sendWhatsAppMsg,
													className: (0, _classnames2.default)(classes.textCenter, classes.iconWrapper)
												},
												_react2.default.createElement(_WhatsAppIcon2.default, {
													className: classes.whatsAppIcon,
													onClick: this.sendWhatsAppMsg
												}),
												_react2.default.createElement(
													'div',
													null,
													'Whatsapp'
												)
											)
										)
									)
								)
							)
						)
					)
				)
			);
		}
	}]);
	return ActionsRendererClass;
}(_react2.default.Component);

ActionsRendererClass.propTypes = {
	classes: _propTypes2.default.object,
	data: _propTypes2.default.object
};

exports.default = (0, _styles.withStyles)(actionStyles)(ActionsRendererClass);