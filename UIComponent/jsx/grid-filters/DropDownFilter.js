Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = DropdownFilter;

var _MyObject = require('../../../Utilities/MyObject');

var _MyObject2 = _interopRequireDefault(_MyObject);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function DropdownFilter() {}

DropdownFilter.prototype = {
	init: function init(params) {
		var filterOptions = _MyObject2.default.deepClone(params.colDef.filterOptions || []);
		this.model = {
			filterOptions: filterOptions,
			value: ''
		};
		this.filterFunction = params.colDef.filterFunction;
		this.valueGetter = params.valueGetter;
		this.setupGui(params);
	},
	setupGui: function setupGui(params) {
		var _this = this;

		this.gui = document.createElement('div');
		this.gui.innerHTML = '\n\t\t\t<div class="grid-external-filter agGridFilterContainer multiSelectionFilter">\n\t\t\t\t<div class="form-control p5">\n\t\t\t\t\t<select\n\t\t\t\t\t\tid="grid-filter-type"\n\t\t\t\t\t\tvalue="' + this.model.value + '"\n\t\t\t\t\t\tclass="input-control rounded"\n\t\t\t\t\t>\n\t\t\t\t\t<option value="">NONE</option>\n\t\t\t\t\t\t' + this.model.filterOptions.reduce(function (all, _ref) {
			var value = _ref.value,
			    label = _ref.label;
			return all + ('<option value="' + value + '">' + label + '</option>');
		}, '') + '\n\t\t\t\t\t</select>\n\t\t\t\t</div>\n\t\t\t\t<div class="actions">\n\t\t\t\t\t<button\n\t\t\t\t\t\tid=\'grid-clear-button\'\n\t\t\t\t\t\tvariant="contained"\n\t\t\t\t\t\tclass="mr-10 terminalButton"\n\t\t\t\t\t>\n\t\t\t\t\t\tClear\n\t\t\t\t\t</button>\n\t\t\t\t\t<button class="terminalButton" id=\'grid-save-button\'> Apply </button>\n\t\t\t\t</div>\n\t';
		var clearButton = this.gui.querySelector('#grid-clear-button');
		var saveButton = this.gui.querySelector('#grid-save-button');
		var filterType = this.gui.querySelector('#grid-filter-type');
		filterType.addEventListener('change', function (e) {
			_this.model.value = Number(e.target.value);
		});
		clearButton.addEventListener('click', function () {
			_this.model.value = '';
			params.filterChangedCallback();
			_this.hidePopup && _this.hidePopup();
		});
		saveButton.addEventListener('click', function () {
			params.filterChangedCallback();
			_this.hidePopup && _this.hidePopup();
		});
	},
	getGui: function getGui() {
		return this.gui;
	},
	afterGuiAttached: function afterGuiAttached(params) {
		this.hidePopup = params.hidePopup;
	},
	doesFilterPass: function doesFilterPass(params) {
		var value = this.model.value;

		var filterFunction = this.filterFunction;
		if (typeof filterFunction === 'function') {
			return filterFunction(value, params.node.data);
		} else {
			var currentCellValue = this.valueGetter(params.node);
			if (!value) {
				return true;
			}
			return value === currentCellValue;
		}
	},
	isFilterActive: function isFilterActive() {
		var filterText = this.model.value;
		return filterText !== null && filterText !== undefined && filterText !== '';
	},
	getModel: function getModel() {
		return { value: this.model };
	},
	setModel: function setModel(model) {
		var state = model ? model.value : null;
		if (state) {
			this.model = state;
		}
	}
};