Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = NumberFilter;
var EQUAL_TO = 0;
var LESS_THAN = 1;
var GREATER_THAN = 2;

function NumberFilter() {}

NumberFilter.prototype = {
	init: function init(params) {
		this.model = {
			value: '',
			filterType: EQUAL_TO
		};
		this.valueGetter = params.valueGetter;
		this.setupGui(params);
	},
	setupGui: function setupGui(params) {
		var _this = this;

		this.gui = document.createElement('div');
		this.gui.innerHTML = '\n\t\t\t<div class="grid-external-filter">\n\t\t\t\t<div class="form-control">\n\t\t\t\t\t<select\n\t\t\t\t\t\tid="grid-filter-type"\n\t\t\t\t\t\tvalue="' + this.model.filterType + '"\n\t\t\t\t\t\tclass="input-control rounded select-control"\n\t\t\t\t\t>\n\t\t\t\t\t\t<option value="' + EQUAL_TO + '">Equal to</option>\n\t\t\t\t\t\t<option value="' + GREATER_THAN + '">Greater than</option>\n\t\t\t\t\t\t<option value="' + LESS_THAN + '">Less than</option>\n\t\t\t\t\t</select>\n\t\t\t\t</div>\n\t\t\t\t<div class="form-control block-label">\n\t\t\t\t\t<input\n\t\t\t\t\t\tid=\'grid-input-filter\'\n\t\t\t\t\t\ttype="number"\n\t\t\t\t\t\tclass="input-control"\n\t\t\t\t\t\tvalue="' + this.model.value + '"\n\t\t\t\t\t/>\n\t\t\t\t</div>\n\t\t\t\t<div class="actions">\n\t\t\t\t\t<button\n\t\t\t\t\t\tid=\'grid-clear-button\'\n\t\t\t\t\t\tvariant="contained"\n\t\t\t\t\t\tclass="mr-10 terminalButton"\n\t\t\t\t\t>\n\t\t\t\t\t\tClear\n\t\t\t\t\t</button>\n\t\t\t\t\t<button \n\t\t\t\t\t\tclass="terminalButton"\n\t\t\t\t\tid=\'grid-save-button\'> Apply </button>\n\t\t\t\t</div>\n\t';
		var clearButton = this.gui.querySelector('#grid-clear-button');
		var saveButton = this.gui.querySelector('#grid-save-button');
		var filterType = this.gui.querySelector('#grid-filter-type');
		var input = this.gui.querySelector('#grid-input-filter');
		input.addEventListener('change', function (e) {
			_this.model.value = e.target.value;
		});
		filterType.addEventListener('change', function (e) {
			_this.model.filterType = Number(e.target.value);
		});
		clearButton.addEventListener('click', function () {
			input.value = '';
			filterType.value = EQUAL_TO;
			_this.model = {
				value: '',
				filterType: EQUAL_TO
			};
			params.filterChangedCallback();
			_this.hidePopup && _this.hidePopup();
		});
		saveButton.addEventListener('click', function () {
			params.filterChangedCallback();
			_this.hidePopup && _this.hidePopup();
		});
	},
	getGui: function getGui() {
		return this.gui;
	},
	afterGuiAttached: function afterGuiAttached(params) {
		this.hidePopup = params.hidePopup;
	},
	doesFilterPass: function doesFilterPass(params) {
		var currentCellValue = Number(this.valueGetter(params.node));
		var _model = this.model,
		    value = _model.value,
		    filterType = _model.filterType;

		switch (filterType) {
			case EQUAL_TO:
				return currentCellValue == value;
			case GREATER_THAN:
				return currentCellValue > value;
			case LESS_THAN:
				return currentCellValue < value;
		}
	},
	isFilterActive: function isFilterActive() {
		var filterText = this.model.value;
		return filterText !== null && filterText !== undefined && filterText !== '';
	},
	getModel: function getModel() {
		return { value: this.model };
	},
	setModel: function setModel(model) {
		var state = model ? model.value : null;
		if (state) {
			this.model = state;
		}
	}
};