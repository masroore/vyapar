Object.defineProperty(exports, "__esModule", {
	value: true
});
var CONTAINS = 0;
var EXACT_MATCH = 1;

var filterOptions = [{
	value: CONTAINS,
	label: 'Contains'
}, {
	value: EXACT_MATCH,
	label: 'Exact match'
}];

var getFilterOptions = function getFilterOptions(filterOptions) {
	var options = filterOptions.map(function (option) {
		return ' <option value="' + option.value + '"> ' + option.label + ' </option> ';
	});
	return options.join('');
};

function StringFilter() {}

StringFilter.prototype.init = function (params) {
	this.filterText = '';
	this.filterType = CONTAINS;
	this.valueGetter = params.valueGetter;
	this.params = params;
	this.setupGui(params);
};

// not called by ag-Grid, just for us to help setup
StringFilter.prototype.setupGui = function (params) {
	var _this = this;

	this.gui = document.createElement('div');
	this.gui.innerHTML = '\n\t\t\t<div class="grid-external-filter">\n\t\t\t\t<div class="form-control">\n\t\t\t\t\t<select\n\t\t\t\t\t\tid="grid-filter-type"\n\t\t\t\t\t\tvalue="' + this.filterType + '"\n\t\t\t\t\t\tclass="input-control rounded select-control"\n\t\t\t\t\t>\n\t\t\t\t\t\t' + getFilterOptions(filterOptions) + '\n\t\t\t\t\t</select>\n\t\t\t\t</div>\n\t\t\t\t<div class="form-control block-label">\n\t\t\t\t\t<input\n\t\t\t\t\t\tid=\'grid-input-filter\'\n\t\t\t\t\t\ttype="text"\n\t\t\t\t\t\tclass="input-control"\n\t\t\t\t\t\tvalue="' + this.filterText + '"\n\t\t\t\t\t/>\n\t\t\t\t</div>\n\t\t\t\t<div class="actions">\n\t\t\t\t\t<button\n\t\t\t\t\t\tid=\'grid-clear-button\'\n\t\t\t\t\t\tvariant="contained"\n\t\t\t\t\t\tclass="mr-10 terminalButton"\n\t\t\t\t\t>\n\t\t\t\t\t\tClear\n\t\t\t\t\t</button>\n\t\t\t\t\t<button \n\t\t\t\t\t\tclass="terminalButton"\n\t\t\t\t\tid=\'grid-save-button\'> Apply </button>\n\t\t\t\t</div>\n\t';
	var clearButton = this.gui.querySelector('#grid-clear-button');
	var saveButton = this.gui.querySelector('#grid-save-button');
	this.filterTypeEl = this.gui.querySelector('#grid-filter-type');
	this.input = this.gui.querySelector('#grid-input-filter');
	this.input.addEventListener('change', function (e) {
		_this.filterText = e.target.value.toLowerCase();
	});
	this.filterTypeEl.addEventListener('change', function (e) {
		_this.filterType = Number(e.target.value);
	});
	clearButton.addEventListener('click', function () {
		_this.input.value = '';
		_this.filterTypeEl.value = CONTAINS;
		params.filterChangedCallback();
		_this.hidePopup && _this.hidePopup();
	});
	saveButton.addEventListener('click', function () {
		params.filterChangedCallback();
		_this.hidePopup && _this.hidePopup();
	});
};

StringFilter.prototype.getGui = function () {
	return this.gui;
};

StringFilter.prototype.doesFilterPass = function (params) {
	var valueGetter = this.valueGetter;
	var currentCellValue = valueGetter(params.node);

	if (typeof currentCellValue === 'number') {
		currentCellValue = currentCellValue.toString();
	}

	currentCellValue = currentCellValue.toLowerCase();

	switch (this.filterType) {
		case CONTAINS:
			return currentCellValue.includes(this.filterText);
		case EXACT_MATCH:
			return currentCellValue == this.filterText;
	}
};

StringFilter.prototype.afterGuiAttached = function (params) {
	this.hidePopup = params.hidePopup;
};

StringFilter.prototype.isFilterActive = function () {
	var filterText = this.gui.querySelector('#grid-input-filter').value;
	return filterText !== null && filterText !== undefined && filterText !== '';
};

StringFilter.prototype.getModel = function () {
	return { value: this.filterText, type: this.filterType };
};

StringFilter.prototype.setModel = function (model) {
	if (model && model.value) {
		this.filterText = model.value;
		this.filterType = model.type;
		this.input.value = model.value;
		this.filterTypeEl.value = model.type;
		this.params.filterChangedCallback();
	}
};

exports.default = StringFilter;