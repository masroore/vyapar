Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = MessageActionsRenderer;

var _MessageActions = require('./MessageActions');

var _MessageActions2 = _interopRequireDefault(_MessageActions);

var _MountComponent = require('../MountComponent');

var _MountComponent2 = _interopRequireDefault(_MountComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function MessageActionsRenderer() {}

// init method gets the details of the cell to be rendere
MessageActionsRenderer.prototype.init = function (params) {
	this.eGui = document.createElement('div');
	(0, _MountComponent2.default)(_MessageActions2.default, this.eGui, params);
};

MessageActionsRenderer.prototype.getGui = function () {
	return this.eGui;
};