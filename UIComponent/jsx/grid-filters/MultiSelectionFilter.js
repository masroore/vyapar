Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = MultiSelectionFilter;

var _MyObject = require('../../../Utilities/MyObject');

var _MyObject2 = _interopRequireDefault(_MyObject);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function MultiSelectionFilter() {}

var getCheckboxesHTML = function getCheckboxesHTML(options) {
	return options.reduce(function (all, current, index) {
		return all + ('\n\t\t\t\t\t\t<label class="block-label">\n\t\t\t\t\t\t\t<input class="grid-list-of-checkboxes" ' + (current.selected ? 'checked' : '') + ' data-index="' + index + '" type="checkbox" />\n\t\t\t\t\t\t\t<span>' + current.label + '</span>\n\t\t\t\t\t\t</label>\n\t\t');
	}, '');
};

MultiSelectionFilter.prototype = {
	init: function init(params) {
		var filterOptions = _MyObject2.default.deepClone(params.colDef.filterOptions || []);
		this.model = {
			filterOptions: filterOptions,
			checkedFilterOptions: filterOptions.filter(function (option) {
				return option.selected;
			})
		};
		this.valueGetter = params.valueGetter;
		this.setupGui(params);
		this.params = params;
	},
	setupGui: function setupGui(params) {
		var _this = this;

		this.gui = document.createElement('div');
		this.gui.innerHTML = '\n\t\t\t<div class="grid-external-filter agGridFilterContainer">\n\t\t\t\t<div id="grid-checkbox-container" class="form-control">\n\t\t\t\t\t' + getCheckboxesHTML(this.model.filterOptions) + '\n\t\t\t\t</div>\n\t\t\t\t<div class="actions">\n\t\t\t\t\t<button\n\t\t\t\t\t\tid=\'grid-clear-button\'\n\t\t\t\t\t\tvariant="contained"\n\t\t\t\t\t\tclass="mr-10 terminalButton"\n\t\t\t\t\t>\n\t\t\t\t\t\tClear\n\t\t\t\t\t</button>\n\t\t\t\t\t<button id=\'grid-save-button\' class="terminalButton"> Apply </button>\n\t\t\t\t</div>\n\t';
		var clearButton = this.gui.querySelector('#grid-clear-button');
		var saveButton = this.gui.querySelector('#grid-save-button');
		var checkboxContainer = this.gui.querySelector('#grid-checkbox-container');

		checkboxContainer.addEventListener('click', function (e) {
			var target = e.target;

			if (!target.classList.contains('grid-list-of-checkboxes')) {
				return false;
			}
			var index = target.getAttribute('data-index');
			var selectedOption = _this.model.filterOptions[index];
			selectedOption.selected = !selectedOption.selected;

			_this.model.checkedFilterOptions = _this.model.filterOptions.filter(function (option) {
				return option.selected;
			});
		});

		clearButton.addEventListener('click', function () {
			var filterOptions = _MyObject2.default.deepClone(params.colDef.filterOptions);
			var checkedFilterOptions = filterOptions.filter(function (option) {
				return option.selected;
			});
			_this.model.filterOptions = filterOptions;
			_this.model.checkedFilterOptions = checkedFilterOptions;
			var allCheckboxes = checkboxContainer.querySelectorAll('input');
			allCheckboxes.forEach(function (el) {
				var index = el.getAttribute('data-index');
				el.checked = filterOptions[index].selected;
			});

			params.filterChangedCallback();
			_this.hidePopup && _this.hidePopup();
		});

		saveButton.addEventListener('click', function () {
			params.filterChangedCallback();
			_this.hidePopup && _this.hidePopup();
		});
	},
	getGui: function getGui() {
		return this.gui;
	},
	afterGuiAttached: function afterGuiAttached(params) {
		this.hidePopup = params.hidePopup;
	},
	doesFilterPass: function doesFilterPass(params) {
		var checkedFilterOptions = this.model.checkedFilterOptions;

		if (!checkedFilterOptions.length) {
			return true;
		}
		var filterFunction = this.params.colDef.filterFunction;
		if (filterFunction) {
			return filterFunction(params, this.model.checkedFilterOptions);
		}
		var currentCellValue = this.valueGetter(params.node);
		var filtered = checkedFilterOptions.find(function (option) {
			return option.selected && option.value === currentCellValue;
		});
		return !!filtered;
	},
	isFilterActive: function isFilterActive() {
		return !!this.model.checkedFilterOptions.length;
	},
	getModel: function getModel() {
		return { value: this.model };
	},
	setModel: function setModel(model) {
		var state = model ? model.value : null;
		if (state) {
			this.model = state;
		}
		this.updateGui();
	},
	applyFilter: function applyFilter(params) {
		this.params.filterChangedCallback();
		this.hidePopup && this.hidePopup();
	},
	updateGui: function updateGui() {
		var filterOptions = this.model.filterOptions;
		var checkboxContainer = this.gui.querySelector('#grid-checkbox-container');
		var allCheckboxes = checkboxContainer.querySelectorAll('input');
		allCheckboxes.forEach(function (el) {
			var index = el.getAttribute('data-index');
			el.checked = filterOptions[index].selected;
		});
	}
};