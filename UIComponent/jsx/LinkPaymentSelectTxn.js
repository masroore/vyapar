Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _TxnTypeConstant = require('../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _MyDate = require('../../Utilities/MyDate');

var _MyDate2 = _interopRequireDefault(_MyDate);

var _styles = require('@material-ui/core/styles');

var _LinkPaymnetTransactionType = require('./LinkPaymnetTransactionType');

var _LinkPaymnetTransactionType2 = _interopRequireDefault(_LinkPaymnetTransactionType);

var _numericEditor = require('./numericEditor');

var _numericEditor2 = _interopRequireDefault(_numericEditor);

var _SettingCache = require('./../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

var _ToastHelper = require('../../UIControllers/ToastHelper');

var ToastHelper = _interopRequireWildcard(_ToastHelper);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
	linkPaymentBody: {
		height: '339px',
		width: '100%',
		backgroundColor: 'white',
		padding: '15px 20px'
	}
};

var LinkPaymentSelectTxn = function (_React$Component) {
	(0, _inherits3.default)(LinkPaymentSelectTxn, _React$Component);

	function LinkPaymentSelectTxn(props) {
		(0, _classCallCheck3.default)(this, LinkPaymentSelectTxn);

		var _this = (0, _possibleConstructorReturn3.default)(this, (LinkPaymentSelectTxn.__proto__ || (0, _getPrototypeOf2.default)(LinkPaymentSelectTxn)).call(this, props));

		_this.getData = _this.getData.bind(_this);
		_this.getApi = _this.getApi.bind(_this);
		_this.onTransactionSelected = _this.onTransactionSelected.bind(_this);
		_this.api = null;
		_this.txnType = 0;
		var txnType = props.txnType;

		var isPaymentTransaction = txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT || txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHIN;
		_this.state = {
			txnListB2B: _this.getData(),
			columnDefs: [{
				field: 'txnDate',
				checkboxSelection: true,
				headerName: 'Date',
				headerClass: 'paymentLinkHeaderFont',
				width: 15,
				cellRenderer: function cellRenderer(params) {
					return _MyDate2.default.getDate('d/m/y', params.value);
				}
			}, {
				field: 'txnType',
				headerName: 'Type',
				headerClass: 'paymentLinkHeaderFont',
				width: 20
			}]
		};
		var settingCache = new _SettingCache2.default();
		if (settingCache.getTxnRefNoEnabled()) {
			_this.state.columnDefs.push({
				field: 'refNo',
				headerName: 'Ref/Inv No.',
				headerClass: 'paymentLinkHeaderFont',
				width: 11
			});
		}
		_this.state.columnDefs.push({
			field: 'amt',
			headerName: isPaymentTransaction ? 'Total' : 'Amount',
			headerClass: 'paymentLinkHeaderFont',
			width: 13,
			cellClass: 'alignRight',
			cellRenderer: function cellRenderer(params) {
				return _MyDouble2.default.getAmountStringInPrintingFormat(params.value);
			}
		});
		_this.state.columnDefs.push({
			field: 'currBal',
			headerName: isPaymentTransaction ? 'Balance' : 'Available',
			headerClass: 'paymentLinkHeaderFont',
			width: 13,
			cellClass: 'alignRight'
		});
		_this.state.columnDefs.push({
			field: 'linkAmount',
			headerName: 'Linked Amount',
			width: 14,
			cellEditor: _numericEditor2.default,
			type: 'inputLinkAmount',
			cellClass: 'linkPaymentAmtToLink',
			headerClass: 'paymentLinkHeaderFont',
			resizable: false
		});
		_this.cellValueChanged = _this.cellValueChanged.bind(_this);
		_this.changeTxnType = _this.changeTxnType.bind(_this);
		_this.setSelectedTransactions = _this.setSelectedTransactions.bind(_this);
		_this.componentStateChanged = _this.componentStateChanged.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(LinkPaymentSelectTxn, [{
		key: 'setSelectedTransactions',
		value: function setSelectedTransactions() {
			var selectedTransactionMap = this.props.selectedTransactionMap;

			this.api.forEachNode(function (rowNode, index) {
				if (selectedTransactionMap.has(rowNode.data.id)) {
					rowNode.data.autoLink = true;
					rowNode.setSelected(true);
				}
			});
		}
	}, {
		key: 'cellValueChanged',
		value: function cellValueChanged(params) {
			var _props = this.props,
			    selectedTransactionMap = _props.selectedTransactionMap,
			    txnListB2B = _props.txnListB2B;

			if (params.oldValue == params.newValue) {
				return;
			}
			if (params.column.colId == 'linkAmount') {
				if (!params.node.selected) {
					params.node.setDataValue('linkAmount', 0);
					selectedTransactionMap.delete(_MyDouble2.default.convertStringToDouble(params.data.id));
					this.props.calculateSelectedTransactionsTotal();
					return;
				}
				if (params.data.linkAmount > params.data.totalAmountIncludingselectedAndCurrBal) {
					params.data.linkAmount = params.data.totalAmountIncludingselectedAndCurrBal;
				}
				var txnAmount = params.data.linkAmount;

				var txnId = params.data.id;
				var txnBalAmount = params.data.totalAmountIncludingselectedAndCurrBal;
				var newTxnBalAmount = txnBalAmount - txnAmount;

				var mapVal = selectedTransactionMap.get(_MyDouble2.default.convertStringToDouble(txnId));
				var txnObj = mapVal[0];
				var txnLink = mapVal[2];

				if (newTxnBalAmount <= 0) {
					// params.node.setDataValue('linkAmount', txnBalAmount);
					params.node.setDataValue('currBal', 0);

					txnAmount = txnBalAmount;
					if (txnAmount > 0) {
						txnListB2B.set(_MyDouble2.default.convertStringToDouble(txnId), [txnObj, 0]);
					}
				} else {
					params.node.setDataValue('currBal', _MyDouble2.default.getAmountWithDecimal(newTxnBalAmount));
					if (txnAmount > 0) {
						txnListB2B.set(_MyDouble2.default.convertStringToDouble(txnId), [txnObj, newTxnBalAmount]);
					}
				}
				if (txnAmount > 0) {
					selectedTransactionMap.set(_MyDouble2.default.convertStringToDouble(txnId), [txnObj, txnAmount, txnLink]);
				} else {
					params.node.setSelected(false);
					if (selectedTransactionMap.has(_MyDouble2.default.convertStringToDouble(params.data.id))) {
						txnListB2B.set(_MyDouble2.default.convertStringToDouble(txnId), [txnObj, params.data.totalAmountIncludingselectedAndCurrBal]);
						selectedTransactionMap.delete(_MyDouble2.default.convertStringToDouble(params.data.id));
					}
					ToastHelper.info('Selected amount has to be greater than 0.');
				}
				this.props.calculateSelectedTransactionsTotal();
			}
		}
	}, {
		key: 'getData',
		value: function getData() {
			var _props2 = this.props,
			    txnListB2B = _props2.txnListB2B,
			    selectedTransactionMap = _props2.selectedTransactionMap;

			var data = [];
			var that = this;
			if (txnListB2B && txnListB2B.size > 0) {
				txnListB2B.forEach(function (value, key) {
					var txn = value[0];
					var txnCurrBal = _MyDouble2.default.convertStringToDouble(value[1]);
					var txnTypeFilter = _MyDouble2.default.convertStringToDouble(that.txnType);
					var txnTypeFilterVal = txnTypeFilter ? txn.getTxnType() == txnTypeFilter : true;

					var tableFilter = txnTypeFilterVal;

					var txnId = _MyDouble2.default.convertStringToDouble(key);
					if (!selectedTransactionMap.has(_MyDouble2.default.convertStringToDouble(txnId)) && tableFilter) {
						// ) {
						var txnDateStr = txn.getTxnDate();
						var invoiceNo = txn.getFullTxnRefNumber();
						var transactionTypeString = _TxnTypeConstant2.default.getTxnTypeForUI(txn.getTxnType());
						var txnTotalAmount = _MyDouble2.default.convertStringToDouble(txn.getCashAmount()) + _MyDouble2.default.convertStringToDouble(txn.getBalanceAmount());
						if (txn.getTxnType() == _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txn.getTxnType() == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT) {
							txnTotalAmount += _MyDouble2.default.convertStringToDouble(txn.getDiscountAmount(), true);
						}
						var txnCurrentBalanceAmount = txnCurrBal;
						var dataObj = {
							txnDate: txnDateStr,
							txnType: transactionTypeString,
							refNo: invoiceNo,
							amt: _MyDouble2.default.getAmountWithDecimal(txnTotalAmount),
							currBal: _MyDouble2.default.getAmountWithDecimal(txnCurrentBalanceAmount),
							totalAmountIncludingselectedAndCurrBal: _MyDouble2.default.getAmountWithDecimal(txnCurrentBalanceAmount),
							linkAmount: 0,
							id: txnId,
							selected: false,
							autoLink: false
						};
						data.push(dataObj);
					}
				});
			}
			if (selectedTransactionMap && selectedTransactionMap.size > 0) {
				selectedTransactionMap.forEach(function (value, key) {
					var selectedTxn = value[0];
					var txnTypeFilter = _MyDouble2.default.convertStringToDouble(that.txnType);
					var txnTypeFilterVal = txnTypeFilter ? selectedTxn.getTxnType() == txnTypeFilter : true;
					var tableFilter = txnTypeFilterVal;
					if (tableFilter) {
						var selectedTxnId = _MyDouble2.default.convertStringToDouble(key);
						var txnselectedAmt = _MyDouble2.default.convertStringToDouble(value[1]);
						var txnDateStr = selectedTxn.getTxnDate();
						var invoiceNo = selectedTxn.getFullTxnRefNumber();
						var transactionTypeString = _TxnTypeConstant2.default.getTxnTypeForUI(selectedTxn.getTxnType());
						var txnTotalAmount = _MyDouble2.default.convertStringToDouble(selectedTxn.getCashAmount()) + _MyDouble2.default.convertStringToDouble(selectedTxn.getBalanceAmount());
						if (selectedTxn.getTxnType() == _TxnTypeConstant2.default.TXN_TYPE_CASHIN || selectedTxn.getTxnType() == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT) {
							txnTotalAmount += _MyDouble2.default.convertStringToDouble(selectedTxn.getDiscountAmount(), true);
						}
						var txnCurrentBalanceAmount = _MyDouble2.default.getAmountWithDecimal(txnListB2B.has(selectedTxnId) ? _MyDouble2.default.convertStringToDouble(txnListB2B.get(selectedTxnId)[1]) : 0);

						var totalAmountIncludingselectedAndCurrBal = txnCurrentBalanceAmount + txnselectedAmt;

						var dataObj = {
							txnDate: txnDateStr,
							txnType: transactionTypeString,
							refNo: invoiceNo,
							amt: _MyDouble2.default.getAmountWithDecimal(txnTotalAmount),
							currBal: _MyDouble2.default.getAmountWithDecimal(txnCurrentBalanceAmount),
							totalAmountIncludingselectedAndCurrBal: totalAmountIncludingselectedAndCurrBal,
							linkAmount: _MyDouble2.default.getAmountWithDecimal(txnselectedAmt),
							id: selectedTxnId,
							selected: true,
							autoLink: false
						};
						data.push(dataObj);
					}
				});
			}
			return data;
		}
	}, {
		key: 'onTransactionSelected',
		value: function onTransactionSelected(row) {
			var _props3 = this.props,
			    txnListB2B = _props3.txnListB2B,
			    selectedTransactionMap = _props3.selectedTransactionMap;

			if (row.node.selected) {
				// extra check when row is being selected through autolink then this fucntion not to be executed
				if (txnListB2B.has(row.node.data.id)) {
					var mapVal = txnListB2B.get(_MyDouble2.default.convertStringToDouble(row.node.data.id));

					var newObj = mapVal[0];
					var linkTxn = null;

					var balAmount = _MyDouble2.default.convertStringToDouble(mapVal[1]);
					if (!row.data.autoLink) {
						var amount = _MyDouble2.default.convertStringToDouble(document.getElementById('selectedAmountTotal').textContent);
						if (amount > 0) {
							if (amount < balAmount) {
								balAmount = amount;
							}
							if (balAmount > 0) {
								row.node.setDataValue('linkAmount', balAmount);
								selectedTransactionMap.set(_MyDouble2.default.convertStringToDouble(newObj.getTxnId()), [newObj, balAmount, linkTxn]);
								txnListB2B.set(_MyDouble2.default.convertStringToDouble(newObj.getTxnId()), [newObj, 0]);
							}
							if (!row.data.autoLink) {
								this.api.startEditingCell({
									rowIndex: row.rowIndex,
									colKey: 'linkAmount'
								});
							}
						} else {
							row.node.setSelected(false);
							ToastHelper.info('No balance to link');
						}
					}
				}
			} else if (!row.node.selected) {
				row.data.autoLink = false;
				this.api.stopEditing(false);
				var newObjId = row.node.data.id;
				if (selectedTransactionMap.has(newObjId)) {
					var newVal = selectedTransactionMap.get(_MyDouble2.default.convertStringToDouble(newObjId));
					var _newObj = newVal[0];
					var newCurrBal = _MyDouble2.default.convertStringToDouble(row.node.data.totalAmountIncludingselectedAndCurrBal);
					if (newVal[1] == 0) {
						selectedTransactionMap.delete(_MyDouble2.default.convertStringToDouble(row.node.data.id));
						this.props.calculateSelectedTransactionsTotal();
					} else {
						row.node.setDataValue('currBal', newCurrBal);
						row.node.setDataValue('linkAmount', 0);
					}
					txnListB2B.set(_MyDouble2.default.convertStringToDouble(newObjId), [_newObj, newCurrBal]);
				}
			}
		}
	}, {
		key: 'changeTxnType',
		value: function changeTxnType(value) {
			this.txnType = value;
			this.setState({
				txnListB2B: this.getData()
			});
		}
	}, {
		key: 'componentStateChanged',
		value: function componentStateChanged() {
			this.setSelectedTransactions();
		}
	}, {
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(nextProps) {
			return false;
		}
	}, {
		key: 'getApi',
		value: function getApi(params) {
			this.api = params.api;
			this.setSelectedTransactions();
			this.props.getApi && this.props.getApi(params.api);
		}
	}, {
		key: 'render',
		value: function render() {
			var gridOptions = {
				columnTypes: {
					inputLinkAmount: {
						editable: function editable(params) {
							return params.node.selected == true;
						}
					}
				},
				enableFilter: false,
				enableSorting: true,
				rowSelection: 'multiple',
				columnDefs: this.state.columnDefs,
				rowData: this.state.txnListB2B,
				onRowSelected: this.onTransactionSelected,
				suppressRowClickSelection: true,
				singleClickEdit: true,
				rowClass: 'customVerticalBorder',
				onCellValueChanged: this.cellValueChanged,
				stopEditingWhenGridLosesFocus: true,
				onComponentStateChanged: this.componentStateChanged
			};
			var _props4 = this.props,
			    classes = _props4.classes,
			    txnType = _props4.txnType;


			return _react2.default.createElement(
				'div',
				{ className: classes.linkPaymentBody + ' d-flex flex-column' },
				_react2.default.createElement(_LinkPaymnetTransactionType2.default, {
					txnType: txnType,
					changeTxnType: this.changeTxnType
				}),
				_react2.default.createElement(_Grid2.default, {
					classes: 'alternateRowColor customVerticalBorder',
					notSelectFirstRow: true,
					getApi: this.getApi,
					selectTxnType: true,
					toggleSelect: true,
					quickFilter: true,
					height: '75%',
					gridOptions: gridOptions
				})
			);
		}
	}]);
	return LinkPaymentSelectTxn;
}(_react2.default.Component);

exports.default = (0, _styles.withStyles)(styles)(LinkPaymentSelectTxn);