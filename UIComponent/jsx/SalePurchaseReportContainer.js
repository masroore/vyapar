Object.defineProperty(exports, "__esModule", {
	value: true
});

var _typeof2 = require('babel-runtime/helpers/typeof');

var _typeof3 = _interopRequireDefault(_typeof2);

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _JqueryDatePicker = require('./UIControls/JqueryDatePicker');

var _JqueryDatePicker2 = _interopRequireDefault(_JqueryDatePicker);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _IPCActions = require('../../Constants/IPCActions');

var _MyDate = require('../../Utilities/MyDate');

var _MyDate2 = _interopRequireDefault(_MyDate);

var _TransactionHelper = require('../../Utilities/TransactionHelper');

var TransactionHelper = _interopRequireWildcard(_TransactionHelper);

var _DBWindow = require('../../Utilities/DBWindow');

var _DBWindow2 = _interopRequireDefault(_DBWindow);

var _TxnTypeConstant = require('../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _TxnPaymentStatusConstants = require('../../Constants/TxnPaymentStatusConstants');

var _TxnPaymentStatusConstants2 = _interopRequireDefault(_TxnPaymentStatusConstants);

var _DateFilter = require('./grid-filters/DateFilter');

var _DateFilter2 = _interopRequireDefault(_DateFilter);

var _NumberFilter = require('./grid-filters/NumberFilter');

var _NumberFilter2 = _interopRequireDefault(_NumberFilter);

var _StringFilter = require('./grid-filters/StringFilter');

var _StringFilter2 = _interopRequireDefault(_StringFilter);

var _MultiSelectionFilter = require('./grid-filters/MultiSelectionFilter');

var _MultiSelectionFilter2 = _interopRequireDefault(_MultiSelectionFilter);

var _DropDownFilter = require('./grid-filters/DropDownFilter');

var _DropDownFilter2 = _interopRequireDefault(_DropDownFilter);

var _MessageActionRenderer = require('./grid-filters/MessageActionRenderer');

var _MessageActionRenderer2 = _interopRequireDefault(_MessageActionRenderer);

var _FirmCache = require('../../Cache/FirmCache');

var _FirmCache2 = _interopRequireDefault(_FirmCache);

var _FirstScreen = require('./UIControls/FirstScreen');

var _FirstScreen2 = _interopRequireDefault(_FirstScreen);

var _SettingCache = require('../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

var _PrintOutlined = require('@material-ui/icons/PrintOutlined');

var _PrintOutlined2 = _interopRequireDefault(_PrintOutlined);

var _BarChart = require('@material-ui/icons/BarChart');

var _BarChart2 = _interopRequireDefault(_BarChart);

var _ExcellIcon = require('./icons/ExcellIcon');

var _ExcellIcon2 = _interopRequireDefault(_ExcellIcon);

var _ExpandMore = require('@material-ui/icons/ExpandMore');

var _ExpandMore2 = _interopRequireDefault(_ExpandMore);

var _withStyles = require('@material-ui/core/styles/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _trashableReact = require('trashable-react');

var _trashableReact2 = _interopRequireDefault(_trashableReact);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _Menu = require('@material-ui/core/Menu');

var _Menu2 = _interopRequireDefault(_Menu);

var _MenuItem = require('@material-ui/core/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _ListOptionModal = require('./UIControls/ListOptionModal');

var _ListOptionModal2 = _interopRequireDefault(_ListOptionModal);

var _LoaderButton = require('./UIControls/LoaderButton');

var _LoaderButton2 = _interopRequireDefault(_LoaderButton);

var _PaymentInfoCache = require('../../Cache/PaymentInfoCache');

var _PaymentInfoCache2 = _interopRequireDefault(_PaymentInfoCache);

var _NewTransactionButton = require('./NewTransactionButton');

var _NewTransactionButton2 = _interopRequireDefault(_NewTransactionButton);

var _PDFHandler = require('../../Utilities/PDFHandler');

var _PDFHandler2 = _interopRequireDefault(_PDFHandler);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function PaymentStatusContainer(_ref) {
	var data = _ref.data,
	    settingCache = _ref.settingCache;
	var paymentStatus = data.paymentStatus,
	    txnType = data.txnType,
	    txnDueDays = data.txnDueDays;

	var returnVal = '';
	if (settingCache.isBillToBillEnabled()) {
		if (settingCache.isPaymentTermEnabled()) {
			if (txnDueDays > 0 && paymentStatus != _TxnPaymentStatusConstants2.default.PAID) {
				returnVal = '<div style="color: #F36035">Overdue (' + txnDueDays + ' days)</div>';
			}
		}
	}
	if (!returnVal) {
		var statusString = _TxnPaymentStatusConstants2.default.getPaymentStatusForUI(txnType, paymentStatus);
		var color = '#03CEA4';
		if (paymentStatus == _TxnPaymentStatusConstants2.default.PAID) {
			color = '#03CEA4';
		} else if (paymentStatus == _TxnPaymentStatusConstants2.default.UNPAID) {
			color = '#1789FC';
		} else if (paymentStatus == _TxnPaymentStatusConstants2.default.PARTIAL) {
			color = '#1789FC';
		}
		returnVal = '<div style="color: ' + color + '">' + statusString + '</div>';
	}
	return returnVal;
}

var styles = function styles() {
	return {
		reportContainer: {
			height: '100%',
			width: '100%'
		},
		positionRelative: {
			position: 'relative'
		},
		height100: {
			height: '100%'
		},
		width100: {
			width: '100%'
		},
		width150px: {
			width: '150px'
		},
		boxContainer: {
			marginTop: 15,
			marginLeft: 15
		},
		box: {
			color: '#284561',
			padding: '14px 0 8px 15px',
			width: 200,
			borderRadius: 8
		},
		label: {
			fontFamily: 'roboto',
			fontSize: 14,
			paddingBottom: 5
		},
		amount: {
			fontSize: 18,
			fontFamily: 'robotoMedium'
		},
		symbol: {
			fontFamily: 'robotoMedium',
			fontSize: 24,
			margin: 'auto 12px'
		},
		paid: {
			background: '#B9F3E7'
		},
		unpaid: {
			background: '#CFE6FE'
		},
		overdue: {
			background: '#F1D5CC'
		},
		total: {
			background: 'rgba(243, 163, 58, 0.6)'
		},
		menuButton: {
			fontFamily: 'robotoMedium',
			color: '#284561',
			fontSize: 20,
			whiteSpace: 'nowrap',
			marginRight: 20,
			textTransform: 'capitalize'
		},
		toLabel: {
			color: '#284561',
			fontFamily: 'roboto'
		},
		input: {
			width: 85,
			color: '#284561',
			fontFamily: 'RobotoMedium',
			border: 0
		},
		datepickerContainer: {
			border: '1px solid #ddd',
			marginRight: 18,
			height: 32,
			borderRadius: 4
		},
		betweenLabel: {
			fontSize: 13,
			fontFamily: 'RobotoMedium',
			color: '#fff',
			background: '#aaa',
			width: 65,
			padding: 8
		},
		reportGraphContainer: {
			position: 'absolute',
			height: '100%',
			width: '100%',
			background: '#fff'
		}
	};
};

var SalePurchaseReportContainer = function (_React$Component) {
	(0, _inherits3.default)(SalePurchaseReportContainer, _React$Component);

	function SalePurchaseReportContainer(props) {
		(0, _classCallCheck3.default)(this, SalePurchaseReportContainer);

		var _this = (0, _possibleConstructorReturn3.default)(this, (SalePurchaseReportContainer.__proto__ || (0, _getPrototypeOf2.default)(SalePurchaseReportContainer)).call(this, props));

		_initialiseProps.call(_this);

		var firmCache = new _FirmCache2.default();
		_this.pdfHandler = new _PDFHandler2.default();
		_this.filterOptions = _TxnTypeConstant2.default.getFilterOptions();
		var settingCache = new _SettingCache2.default();
		_this.html = '';
		_this.allOrders = [];
		_this.contextMenu = [{
			title: 'View/Edit',
			cmd: SalePurchaseReportContainer.viewTransaction,
			visible: true,
			id: 'edit'
		}];

		if (settingCache.isBillToBillEnabled()) {
			_this.contextMenu = [].concat((0, _toConsumableArray3.default)(_this.contextMenu), [{
				title: 'Receive Payment',
				cmd: function cmd(row) {
					return TransactionHelper.receiveOrMakePayment(row.id, _TxnTypeConstant2.default.getTxnType(row.txnType));
				},
				visible: true,
				id: 'receivePayment'
			}]);
		}
		_this.contextMenu = [].concat((0, _toConsumableArray3.default)(_this.contextMenu), [{
			title: 'Return',
			cmd: function cmd(row) {
				return TransactionHelper.returnTransaction(row.id, _TxnTypeConstant2.default.getTxnType(row.txnType));
			},
			visible: true,
			id: 'rtn'
		}, {
			title: 'Preview As Delivery Challan',
			cmd: function cmd(row) {
				return TransactionHelper.preview(row.id, _TxnTypeConstant2.default.getTxnType(row.txnType), true);
			},
			visible: true,
			id: 'printDeliveryChallanpreview'
		}]);

		if (settingCache.isBillToBillEnabled()) {
			_this.contextMenu = [].concat((0, _toConsumableArray3.default)(_this.contextMenu), [{
				title: 'Payment History',
				cmd: function cmd(row) {
					return window.showPaymentHistory(row.id + ':' + _TxnTypeConstant2.default.getTxnType(row.txnType));
				},
				visible: true,
				id: 'paymentHistory'
			}]);
		}

		_this.contextMenu = [].concat((0, _toConsumableArray3.default)(_this.contextMenu), [{
			title: 'Delete',
			cmd: function cmd(row) {
				return TransactionHelper.deleteTransaction(row.id, _TxnTypeConstant2.default.getTxnType(row.txnType));
			},
			visible: true,
			id: 'delete'
		}, {
			title: 'Open PDF',
			cmd: function cmd(row) {
				return TransactionHelper.openPDF(row.id, _TxnTypeConstant2.default.getTxnType(row.txnType));
			},
			visible: true,
			id: 'openPDF'
		}, {
			title: 'Preview',
			cmd: function cmd(row) {
				return TransactionHelper.preview(row.id, _TxnTypeConstant2.default.getTxnType(row.txnType));
			},
			visible: true,
			id: 'preview'
		}, {
			title: 'Print',
			cmd: function cmd(row) {
				return TransactionHelper.print(row.id, _TxnTypeConstant2.default.getTxnType(row.txnType));
			},
			visible: true,
			id: 'print'
		}]);

		_this.transactionLabel = props.txnType === TxnTypeConstant.TXN_TYPE_SALE ? 'Sale' : 'Purchase';

		_this.DATE_RANGE = {
			all: 0,
			thisMonth: 1,
			lastMonth: 2,
			thisQuarter: 3,
			thisYear: 4,
			custom: 5,
			labels: ['All ' + _this.transactionLabel + ' Invoices', 'This Month', 'Last Month', 'This Quarter', 'This Year', 'Custom']
		};

		var _MyDate$getThisMonthF = _MyDate2.default.getThisMonthFromToDate(),
		    _MyDate$getThisMonthF2 = (0, _slicedToArray3.default)(_MyDate$getThisMonthF, 2),
		    fromDate = _MyDate$getThisMonthF2[0],
		    toDate = _MyDate$getThisMonthF2[1];

		var txnTypeList = void 0;
		if (props.txnType === TxnTypeConstant.TXN_TYPE_SALE) {
			txnTypeList = props.includeReturnTransaction ? [TxnTypeConstant.TXN_TYPE_SALE, TxnTypeConstant.TXN_TYPE_SALE_RETURN] : [TxnTypeConstant.TXN_TYPE_SALE];
		} else if (props.txnType === TxnTypeConstant.TXN_TYPE_PURCHASE) {
			txnTypeList = props.includeReturnTransaction ? [TxnTypeConstant.TXN_TYPE_PURCHASE, TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN] : [TxnTypeConstant.TXN_TYPE_PURCHASE];
		}

		var printOptions = [{
			key: 'printItemDetails',
			label: 'Print Item Details',
			value: false
		}, {
			key: 'printDescription',
			label: 'Print Description',
			value: false
		}, {
			key: 'printPaymentStatus',
			label: 'Print Payment Status',
			value: false
		}];

		_this.state = {
			records: [],
			firmList: firmCache.getFirmList(),
			firmId: '',
			fromDate: props.includeReturnTransaction ? _MyDate2.default.getDate('d/m/y', fromDate) : '',
			toDate: props.includeReturnTransaction ? _MyDate2.default.getDate('d/m/y', toDate) : '',
			dateRange: props.includeReturnTransaction ? _this.DATE_RANGE.thisMonth : _this.DATE_RANGE.all,
			txnTypes: txnTypeList,
			contextMenu: _this.contextMenu,
			loadingForPrint: false,
			loadingForExcel: false,
			printOptions: printOptions,
			excelOptions: [].concat(printOptions),
			paid: 0,
			unpaid: 0,
			overdue: 0,
			total: 0,
			showGraph: false,
			paymentStatus: 0,
			showPrintOptions: false,
			showExcelOptions: false
		};

		_this.getData = _this.getData.bind(_this);
		_this.changeFilter = _this.changeFilter.bind(_this);
		_this.getApi = _this.getApi.bind(_this);
		_this.printTransactions = _this.printTransactions.bind(_this);
		_this.init = _this.init.bind(_this);
		_this._loaded = [];
		_this._perPage = 100;
		_this._id = null;
		_this.getColDef();
		return _this;
	}

	(0, _createClass3.default)(SalePurchaseReportContainer, [{
		key: 'init',
		value: function init() {
			var _this2 = this;

			var txnTypes = this.state.txnTypes;

			this.props.registerPromise(_DBWindow2.default.send(_IPCActions.GET_TRANSACTIONS_COUNT, {
				txnTypes: txnTypes
			})).then(function (response) {
				_this2.totalTransactions = response.count;

				if (response.count) {
					_this2.filterTransactions();
				} else {
					_this2.forceUpdate();
				}
			});
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.init();
			MyAnalytics.pushScreen('View Print Center');
			window.onResume = this.onResume.bind(this);
			window.HIDE_GRAPH_PAGE = this.hideGraph.bind(this);
			TransactionHelper.bindEventsForPreviewDialog();
			window.savePDF = this.savePDF.bind(this);
		}
	}, {
		key: 'savePDF',
		value: function savePDF() {
			var PDFReportConstant = require('../../Constants/PDFReportConstant');
			var _state = this.state,
			    fromDate = _state.fromDate,
			    toDate = _state.toDate;
			var txnType = this.props.txnType;

			this.pdfHandler.savePDF({
				type: txnType == _TxnTypeConstant2.default.TXN_TYPE_SALE ? PDFReportConstant.SALE_REPORT : PDFReportConstant.PURCHASE_REPORT,
				fromDate: fromDate,
				toDate: toDate
			});
		}
	}, {
		key: 'filterTransactions',
		value: function filterTransactions() {
			var key = Math.random() * 1000000000000000000;
			var includeReturnTransaction = this.props.includeReturnTransaction;
			var _state2 = this.state,
			    fromDate = _state2.fromDate,
			    toDate = _state2.toDate,
			    txnTypes = _state2.txnTypes,
			    firmId = _state2.firmId;

			if (fromDate) {
				try {
					fromDate = _MyDate2.default.getDateObj(fromDate, 'dd/mm/yyyy', '/');
					if (!(fromDate && fromDate instanceof Date && fromDate != 'Invalid Date')) {
						throw new Error('invalid date');
					}
				} catch (e) {
					ToastHelper.error('Please enter valid from date');
					return false;
				}
			}
			if (toDate) {
				try {
					toDate = _MyDate2.default.getDateObj(toDate, 'dd/mm/yyyy', '/');
					if (!(toDate && toDate instanceof Date && toDate != 'Invalid Date')) {
						throw new Error('invalid date');
					}
				} catch (e) {
					ToastHelper.error('Please enter valid to date');
					return false;
				}
			}
			var queryFields = {
				fromDate: fromDate,
				toDate: toDate,
				txnTypes: txnTypes,
				firmId: firmId
			};

			if (includeReturnTransaction) {
				queryFields.order = [['txnDate', 'ASC'], ['txnId', 'ASC']];
			};
			this.loadTransactions(queryFields, key, 0, this._perPage, {
				queryFields: queryFields,
				key: key,
				start: this._perPage,
				noOfRecords: -1
			});
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			TransactionHelper.unBindEventsForPreviewDialog();
			delete window.onResume;
			delete window.HIDE_GRAPH_PAGE;
		}
	}, {
		key: 'onResume',
		value: function onResume() {
			var _this3 = this;

			this.getColDef();
			var firmCache = new _FirmCache2.default();
			this.setState({ firmList: firmCache.getFirmList() }, function () {
				_this3.init();
				TransactionHelper.bindEventsForPreviewDialog();
				if (_this3.api && typeof _this3.api.sizeColumnsToFit === 'function') {
					_this3.api.setColumnDefs(_this3.columnDefs);
					_this3.api.sizeColumnsToFit();
				}
			});
		}
	}, {
		key: 'getData',
		value: function getData(response) {
			var totalAmounts = {
				paid: 0,
				unpaid: 0,
				overdue: 0,
				total: 0
			};

			var isAllTxnLoaded = false;
			if (response.meta.next && response.data.length == this._perPage) {
				var next = response.meta.next;
				this.loadTransactions(next.queryFields, next.key, next.start, next.noOfRecords);
			} else {
				isAllTxnLoaded = true;
			}

			var data = response.data.map(function (row) {
				row.date = new Date(row.date);
				row.dueDate = new Date(row.dueDate);
				return row;
			});
			if (response.meta.next) {
				this.allOrders = data;
			} else {
				this.allOrders = [].concat((0, _toConsumableArray3.default)(this.allOrders), (0, _toConsumableArray3.default)(data));
			}
			if (isAllTxnLoaded) {
				var _settingCache = new _SettingCache2.default();
				var checkForOverdue = false;
				if (_settingCache.isBillToBillEnabled() && _settingCache.isPaymentTermEnabled()) {
					checkForOverdue = true;
				}

				totalAmounts = this.allOrders.reduce(function (all, current) {
					var paidAmount = current.amount - current.currentBalance;
					var unpaidAmount = current.amount - paidAmount;
					if (current.txnType == TxnTypeConstant.TXN_TYPE_SALE || current.txnType == TxnTypeConstant.TXN_TYPE_PURCHASE) {
						all.paid += paidAmount;
						if (checkForOverdue && current.txnDueDays > 0) {
							all.overdue += unpaidAmount;
						} else {
							all.unpaid += unpaidAmount;
						}
					} else if (current.txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || current.txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
						all.paid -= paidAmount;
						if (checkForOverdue && current.txnDueDays > 0) {
							all.overdue -= unpaidAmount;
						} else {
							all.unpaid -= unpaidAmount;
						}
					}
					return all;
				}, totalAmounts);

				totalAmounts.total = _MyDouble2.default.getAmountWithDecimalAndCurrencyWithSign(totalAmounts.paid + totalAmounts.unpaid + totalAmounts.overdue);
				totalAmounts.paid = _MyDouble2.default.getAmountWithDecimalAndCurrencyWithSign(totalAmounts.paid);
				totalAmounts.unpaid = _MyDouble2.default.getAmountWithDecimalAndCurrencyWithSign(totalAmounts.unpaid);
				totalAmounts.overdue = _MyDouble2.default.getAmountWithDecimalAndCurrencyWithSign(totalAmounts.overdue);
			}
			this.setState((0, _extends3.default)({ records: this.allOrders }, totalAmounts));

			if (this.api) {
				this.api.setRowData(this.allOrders);
			}
		}
	}, {
		key: 'loadTransactions',
		value: function loadTransactions(queryFields, key, start, noOfRecords, next) {
			this.props.registerPromise(_DBWindow2.default.send(_IPCActions.GET_SALE_REPORT_TRANSACTIONS, {
				queryFields: queryFields,
				key: key,
				start: start,
				noOfRecords: noOfRecords,
				next: next
			})).then(this.getData);
		}
	}, {
		key: 'changeFilter',
		value: function changeFilter(field) {
			var _this4 = this;

			return function (e) {
				var value = void 0;
				if (e && e.target) {
					value = e.target.value;
				} else {
					value = e;
				}
				if (field === 'fromDate' || field === 'toDate') {
					_this4.setState({ dateRange: _this4.DATE_RANGE.custom });
					var date = '';
					date = _MyDate2.default.getDateObj(value, 'dd/mm/yyyy', '/');
					if (!(date && date instanceof Date && date != 'Invalid Date')) {
						_this4.setState((0, _defineProperty3.default)({}, field, value));
						return false;
					}
					_this4.setState((0, _defineProperty3.default)({}, field, value), _this4.filterTransactions);
				} else {
					_this4.setState((0, _defineProperty3.default)({}, field, value), _this4.filterTransactions);
				}
			};
		}
	}, {
		key: 'printTransactions',
		value: function printTransactions() {
			var _this5 = this;

			this.setState({ loadingForPrint: true }, function () {
				var selected = [];
				var TransactionFactory = require('../../BizLogic/TransactionFactory');
				var transactionFactory = new TransactionFactory();
				_this5.api.forEachNodeAfterFilterAndSort(function (node) {
					var txn = transactionFactory.getTransactionObject(node.data.txnType);
					selected.push(txn.getTransactionFromId(node.data.id));
				});
				if (!selected.length) {
					ToastHelper.error('No transacton to print. Please change filters.');
					_this5.setState({ loadingForPrint: false });
					return false;
				}
				var paymentStatusFilterModel = _this5.api.getFilterInstance('paymentStatus');
				setTimeout(function () {
					var _state3 = _this5.state,
					    fromDate = _state3.fromDate,
					    toDate = _state3.toDate,
					    firmId = _state3.firmId,
					    printOptions = _state3.printOptions;


					var paymentStatus = paymentStatusFilterModel && paymentStatusFilterModel.getModel().value.currentValue || '';
					var txnType = _this5.props.txnType;


					var SaleReportHTMLGenerator = require('../../ReportHTMLGenerator/SaleReportHTMLGenerator');
					var saleReportHTMLGenerator = new SaleReportHTMLGenerator();
					var html = saleReportHTMLGenerator.getHTMLText(selected, fromDate || null, toDate || null, firmId, printOptions[0].value, printOptions[1].value, printOptions[2].value, paymentStatus, txnType, _this5.props.includeReturnTransaction);
					html && TransactionHelper.previewByHTML(html);
					_this5.setState({ loadingForPrint: false });
				}, 300);
			});
		}
	}, {
		key: 'getApi',
		value: function getApi(params) {
			console.log('get api called');
			this.api = params.api;
			this.api.setRowData(this.state.records);
		}
	}, {
		key: 'render',
		value: function render() {
			var _this6 = this,
			    _React$createElement;

			var _state4 = this.state,
			    firmList = _state4.firmList,
			    paid = _state4.paid,
			    unpaid = _state4.unpaid,
			    overdue = _state4.overdue,
			    total = _state4.total,
			    anchorEl = _state4.anchorEl,
			    dateRange = _state4.dateRange,
			    showPrintOptions = _state4.showPrintOptions,
			    showExcelOptions = _state4.showExcelOptions,
			    loadingForExcel = _state4.loadingForExcel,
			    loadingForPrint = _state4.loadingForPrint,
			    printOptions = _state4.printOptions,
			    excelOptions = _state4.excelOptions,
			    showGraph = _state4.showGraph;
			var classes = this.props.classes;

			var gridOptions = {
				enableFilter: true,
				enableSorting: true,
				rowSelection: 'single',
				columnDefs: this.columnDefs,
				rowData: [],
				rowHeight: 40,
				headerHeight: 40,
				overlayNoRowsTemplate: 'No transactions to show',
				onRowDoubleClicked: SalePurchaseReportContainer.viewTransaction.bind(this),
				rowClass: 'customVerticalBorder'
			};
			var txnType = this.props.txnType;

			var firmOptions = (0, _keys2.default)(firmList).map(function (key, index) {
				var firm = firmList[key];
				return _react2.default.createElement(
					'option',
					{ key: index, value: firm.getFirmId() },
					firm.getFirmName()
				);
			});
			var firstScreenData = {
				image: txnType === TxnTypeConstant.TXN_TYPE_SALE ? '../inlineSVG/first-screens/sale.svg' : '../inlineSVG/first-screens/purchase.svg',
				text: txnType === TxnTypeConstant.TXN_TYPE_SALE ? 'Make Sale invoices & Print or share with your customers directly via WhatsApp or Email.' : 'Make Purchase invoices & Print or share with your customers directly via WhatsApp or Email.',
				buttonLabel: txnType === TxnTypeConstant.TXN_TYPE_SALE ? 'Add Your First Sale Invoice' : 'Add Your First Purchase Invoice'
			};
			return _react2.default.createElement(
				'div',
				{
					className: (0, _classnames2.default)('d-flex flex-column', classes.reportContainer, showGraph ? classes.positionRelative : '')
				},
				this.totalTransactions !== undefined && this.totalTransactions === 0 && _react2.default.createElement(_FirstScreen2.default, {
					image: firstScreenData.image,
					text: firstScreenData.text,
					onClick: function onClick() {
						TransactionHelper.viewTransaction('', TxnTypeConstant.getTxnType(txnType));
					},
					buttonLabel: firstScreenData.buttonLabel
				}),
				this.totalTransactions !== undefined && this.totalTransactions > 0 && _react2.default.createElement(
					_react.Fragment,
					null,
					showPrintOptions && _react2.default.createElement(_ListOptionModal2.default, {
						options: printOptions,
						onChange: this.onPrintOptionChange,
						onClose: this.onPrintOptionClose
					}),
					showExcelOptions && _react2.default.createElement(_ListOptionModal2.default, {
						options: excelOptions,
						onChange: this.onExcelOptionChange,
						onClose: this.onExcelOptionClose
					}),
					_react2.default.createElement(
						'div',
						{ className: 'grid-external-filter' },
						_react2.default.createElement(
							'div',
							{ className: 'd-flex align-items-center' },
							_react2.default.createElement(
								'div',
								null,
								_react2.default.createElement(
									_Button2.default,
									{
										className: classes.menuButton,
										'aria-controls': 'simple-menu',
										'aria-haspopup': 'true',
										onClick: this.handleClick
									},
									this.DATE_RANGE.labels[dateRange],
									' ',
									_react2.default.createElement(_ExpandMore2.default, null)
								),
								_react2.default.createElement(
									_Menu2.default,
									{
										id: 'simple-menu',
										anchorEl: anchorEl,
										keepMounted: true,
										open: Boolean(anchorEl),
										onClose: function onClose() {
											return _this6.handleClose();
										}
									},
									_react2.default.createElement(
										_MenuItem2.default,
										{
											onClick: function onClick() {
												return _this6.handleClose(_this6.DATE_RANGE.all);
											}
										},
										this.DATE_RANGE.labels[0]
									),
									_react2.default.createElement(
										_MenuItem2.default,
										{
											onClick: function onClick() {
												return _this6.handleClose(_this6.DATE_RANGE.thisMonth);
											}
										},
										'This Month'
									),
									_react2.default.createElement(
										_MenuItem2.default,
										{
											onClick: function onClick() {
												return _this6.handleClose(_this6.DATE_RANGE.lastMonth);
											}
										},
										'Last Month'
									),
									_react2.default.createElement(
										_MenuItem2.default,
										{
											onClick: function onClick() {
												return _this6.handleClose(_this6.DATE_RANGE.thisQuarter);
											}
										},
										'This Quarter'
									),
									_react2.default.createElement(
										_MenuItem2.default,
										{
											onClick: function onClick() {
												return _this6.handleClose(_this6.DATE_RANGE.thisYear);
											}
										},
										'This Year'
									),
									_react2.default.createElement(
										_MenuItem2.default,
										{
											onClick: function onClick() {
												return _this6.handleClose(_this6.DATE_RANGE.custom);
											}
										},
										'Custom'
									)
								)
							),
							_react2.default.createElement(
								'div',
								{
									className: (0, _classnames2.default)(classes.datepickerContainer, 'd-flex align-items-center')
								},
								_react2.default.createElement(
									'div',
									{ className: classes.betweenLabel },
									'Between'
								),
								_react2.default.createElement(
									'div',
									{ className: 'ml-15' },
									_react2.default.createElement(_JqueryDatePicker2.default, {
										id: 'from-datepicker',
										className: classes.input,
										placeholder: 'dd/mm/yyyy',
										validate: true,
										value: this.state.fromDate,
										onChange: this.changeFilter('fromDate')
									})
								),
								_react2.default.createElement(
									'div',
									{ className: classes.toLabel },
									' To '
								),
								_react2.default.createElement(
									'div',
									{ className: 'ml-15' },
									_react2.default.createElement(_JqueryDatePicker2.default, {
										id: 'to-datepicker',
										className: classes.input,
										placeholder: 'dd/mm/yyyy',
										validate: true,
										value: this.state.toDate,
										onChange: this.changeFilter('toDate')
									})
								)
							),
							_react2.default.createElement(
								'div',
								{ className: 'form-control inline ml-15' },
								_react2.default.createElement(
									'select',
									{
										value: this.state.firm,
										className: 'input-control rounded',
										onChange: this.changeFilter('firmId')
									},
									_react2.default.createElement(
										'option',
										{ value: '' },
										'ALL FIRMS'
									),
									firmOptions
								)
							),
							_react2.default.createElement(
								'div',
								{
									className: (0, _classnames2.default)('d-flex justify-content-end', classes.width100)
								},
								txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE && _react2.default.createElement(
									'div',
									{ className: 'ml-15' },
									_react2.default.createElement(_LoaderButton2.default, {
										onClick: this.loadGraph,
										Icon: _react2.default.createElement(_BarChart2.default, null),
										label: 'Graph'
									})
								),
								_react2.default.createElement(
									'div',
									{ className: 'ml-15' },
									_react2.default.createElement(_LoaderButton2.default, {
										onClick: this.showExcelOptions,
										Icon: _react2.default.createElement(_ExcellIcon2.default, null),
										loading: loadingForExcel,
										label: 'Excel Report'
									})
								),
								_react2.default.createElement(
									'div',
									{ className: 'ml-15' },
									_react2.default.createElement(_LoaderButton2.default, {
										onClick: function onClick() {
											return _this6.setState({ showPrintOptions: true });
										},
										Icon: _react2.default.createElement(_PrintOutlined2.default, null),
										loading: loadingForPrint,
										label: 'Print'
									})
								)
							)
						),
						_react2.default.createElement(
							'div',
							{ className: (0, _classnames2.default)(classes.boxContainer, 'd-flex') },
							_react2.default.createElement(
								'div',
								{ className: (0, _classnames2.default)(classes.box, classes.paid) },
								_react2.default.createElement(
									'div',
									{ className: classes.label },
									' Paid '
								),
								_react2.default.createElement(
									'div',
									{ className: classes.amount },
									paid
								)
							),
							_react2.default.createElement(
								'div',
								{ className: classes.symbol },
								' + '
							),
							_react2.default.createElement(
								'div',
								{ className: (0, _classnames2.default)(classes.box, classes.unpaid) },
								_react2.default.createElement(
									'div',
									{ className: classes.label },
									' Unpaid '
								),
								_react2.default.createElement(
									'div',
									{ className: classes.amount },
									unpaid
								)
							),
							settingCache.isPaymentTermEnabled() && _react2.default.createElement(
								_react.Fragment,
								null,
								_react2.default.createElement(
									'div',
									{ className: classes.symbol },
									' + '
								),
								_react2.default.createElement(
									'div',
									{ className: (0, _classnames2.default)(classes.box, classes.overdue) },
									_react2.default.createElement(
										'div',
										{ className: classes.label },
										' Overdue '
									),
									_react2.default.createElement(
										'div',
										{ className: classes.amount },
										overdue
									)
								)
							),
							_react2.default.createElement(
								'div',
								{ className: classes.symbol },
								' = '
							),
							_react2.default.createElement(
								'div',
								{ className: (0, _classnames2.default)(classes.box, classes.total) },
								_react2.default.createElement(
									'div',
									{ className: classes.label },
									' Total '
								),
								_react2.default.createElement(
									'div',
									{ className: classes.amount },
									total
								)
							)
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'd-flex flex-column flex-container flex-grow-1' },
						_react2.default.createElement(_Grid2.default, (_React$createElement = {
							title: 'Transactions',
							quickFilter: true,
							CTAButton: _react2.default.createElement(_NewTransactionButton2.default, {
								txnType: _TxnTypeConstant2.default.getTxnType(txnType),
								label: '+ Add ' + _TxnTypeConstant2.default.getTxnTypeForUI(txnType)
							}),
							getApi: this.getApi,
							classes: 'alternateRowColor customVerticalBorder gridRowHeight40',
							filterContextMenu: this.filterContextMenu,
							contextMenu: this.state.contextMenu
						}, (0, _defineProperty3.default)(_React$createElement, 'filterContextMenu', this.filterContextMenu), (0, _defineProperty3.default)(_React$createElement, 'gridOptions', gridOptions), _React$createElement))
					),
					showGraph && _react2.default.createElement('div', {
						className: classes.reportGraphContainer,
						id: 'reportGraphContainer'
					})
				)
			);
		}
	}], [{
		key: 'viewTransaction',
		value: function viewTransaction(row) {
			if (!row.id) {
				row = row.data;
			}
			TransactionHelper.viewTransaction(row.id, _TxnTypeConstant2.default.getTxnType(row.txnType));
		}
	}]);
	return SalePurchaseReportContainer;
}(_react2.default.Component);

var _initialiseProps = function _initialiseProps() {
	var _this7 = this;

	this.filterContextMenu = function (row) {
		var menuItems = TransactionHelper.filterContextMenu([].concat((0, _toConsumableArray3.default)(_this7.contextMenu)), {
			data: {
				txnTypeConstant: TxnTypeConstant.getTxnType(row.data.txnType),
				paymentStatus: row.data.paymentStatus
			}
		});
		_this7.setState({ contextMenu: menuItems });
	};

	this.getColDef = function () {
		var settingCache = new _SettingCache2.default();
		var paymentInfoCache = new _PaymentInfoCache2.default();
		_this7.columnDefs = [{
			field: 'date',
			colId: 'date',
			headerName: 'DATE',
			width: 100,
			sortingOrder: ['desc', 'asc', null],
			filter: _DateFilter2.default,
			enableFilter: true,
			getQuickFilterText: function getQuickFilterText(params) {
				return _MyDate2.default.getDate('d/m/y', params.value);
			},
			cellRenderer: function cellRenderer(params) {
				return _MyDate2.default.getDate('d/m/y', params.value);
			}
		}];

		if (settingCache.getTxnRefNoEnabled()) {
			_this7.columnDefs = [].concat((0, _toConsumableArray3.default)(_this7.columnDefs), [{
				field: 'txnRefNumber',
				colId: 'txnRefNumber',
				headerName: 'INVOICE NO.',
				width: 100,
				cellClass: 'alignRight',
				comparator: function comparator(a, b) {
					var typeA = typeof a === 'undefined' ? 'undefined' : (0, _typeof3.default)(a);
					var typeB = typeof b === 'undefined' ? 'undefined' : (0, _typeof3.default)(b);
					if (typeA === 'number' && typeB === 'number') {
						return a - b;
					} else if (typeA === 'string' && typeB === 'string') {
						return a.localeCompare(b);
					} else {
						if (typeA === 'number') {
							return -1;
						} else {
							return 1;
						}
						// return a > b ? 1 : a < b ? -1 : 0;
					}
				},
				valueGetter: function valueGetter(params) {
					var value = Number(params.data.refNumber);
					if (isNaN(value)) {
						return params.data.refNumber;
					}
					return value;
				},
				getQuickFilterText: function getQuickFilterText(params) {
					return params.data.txnRefNumber;
				},
				cellRenderer: function cellRenderer(params) {
					return params.data.txnRefNumber;
				},
				filter: _StringFilter2.default
			}]);
		}

		_this7.columnDefs = [].concat((0, _toConsumableArray3.default)(_this7.columnDefs), [{
			field: 'name',
			colId: 'name',
			filter: _StringFilter2.default,
			headerName: 'PARTY NAME',
			width: 180
		}]);

		if (_this7.props.includeReturnTransaction) {
			var filterArray = [{
				label: 'Sale',
				value: TxnTypeConstant.TXN_TYPE_SALE,
				selected: false
			}, {
				label: 'Credit Note',
				value: TxnTypeConstant.TXN_TYPE_SALE_RETURN,
				selected: false
			}];

			if (_this7.props.txnType === TxnTypeConstant.TXN_TYPE_PURCHASE) {
				filterArray = [{
					label: 'Purchase',
					value: TxnTypeConstant.TXN_TYPE_PURCHASE,
					selected: false
				}, {
					label: 'Debit Note',
					value: TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN,
					selected: false
				}];
			}

			_this7.columnDefs = [].concat((0, _toConsumableArray3.default)(_this7.columnDefs), [{
				field: 'txnType',
				colId: 'txnType',
				filter: _MultiSelectionFilter2.default,
				headerName: 'TRANSACTION TYPE',
				filterOptions: filterArray,
				width: 100,
				cellRenderer: function cellRenderer(params) {
					return TxnTypeConstant.getTxnTypeForUI(params.data.txnType);
				}
			}]);
		}

		_this7.columnDefs = [].concat((0, _toConsumableArray3.default)(_this7.columnDefs), [{
			field: 'paymentTypeId',
			colId: 'paymentTypeId',
			filter: _MultiSelectionFilter2.default,
			filterOptions: paymentInfoCache.getFilterOptionsForGrid(),
			getQuickFilterText: function getQuickFilterText(params) {
				return params.data.paymentType;
			},
			cellRenderer: function cellRenderer(params) {
				return params.data.paymentType;
			},
			headerName: 'PAYMENT TYPE',
			width: 110
		}, {
			field: 'amount',
			colId: 'amount',
			headerName: 'AMOUNT',
			getQuickFilterText: function getQuickFilterText(params) {
				return params.data.amount;
			},
			cellRenderer: function cellRenderer(params) {
				return _MyDouble2.default.getAmountWithDecimal(params.value);
			},
			width: 100,
			cellClass: 'alignRight',
			filter: _NumberFilter2.default
		}, {
			field: 'currentBalance',
			colId: 'currentBalance',
			headerName: 'BALANCE DUE',
			filter: _NumberFilter2.default,
			cellRenderer: function cellRenderer(params) {
				return _MyDouble2.default.getAmountWithDecimal(params.value);
			},
			cellClass: 'alignRight',
			width: 100
		}]);
		if (settingCache.isBillToBillEnabled()) {
			var paymentStatusFilters = [{
				value: '',
				label: 'All'
			}].concat((0, _toConsumableArray3.default)(_TxnPaymentStatusConstants2.default.getFilterOptions()));
			if (settingCache.isPaymentTermEnabled()) {
				_this7.columnDefs = [].concat((0, _toConsumableArray3.default)(_this7.columnDefs), [{
					field: 'dueDate',
					colId: 'dueDate',
					headerName: 'DUE DATE',
					filter: _DateFilter2.default,
					width: 100,
					sortingOrder: ['desc', 'asc', null],
					enableFilter: true,
					getQuickFilterText: function getQuickFilterText(params) {
						return _MyDate2.default.getDate('d/m/y', params.value);
					},
					cellRenderer: function cellRenderer(params) {
						return _MyDate2.default.getDate('d/m/y', params.value);
					}
				}]);
				paymentStatusFilters.push({
					label: StringConstant.paymentStatusOverDue,
					value: -1
				});
			}
			_this7.columnDefs = [].concat((0, _toConsumableArray3.default)(_this7.columnDefs), [{
				colId: 'paymentStatus',
				filter: _DropDownFilter2.default,
				filterOptions: paymentStatusFilters,
				filterFunction: function filterFunction(currentValue, data) {
					if (currentValue == '') {
						return true;
					}
					var currentCellValue = data.paymentStatus;
					var paymentStatFilter = false;
					if (settingCache.isPaymentTermEnabled() && currentValue == _TxnPaymentStatusConstants2.default.OVERDUE) {
						paymentStatFilter = data.txnDueDays > 0 && currentCellValue != _TxnPaymentStatusConstants2.default.PAID;
					} else {
						paymentStatFilter = currentValue ? currentValue == currentCellValue : true;
					}

					return settingCache.isBillToBillEnabled() ? paymentStatFilter : true;
				},
				headerName: 'STATUS',
				getQuickFilterText: function getQuickFilterText(params) {
					var returnVal = '';
					var _params$data = params.data,
					    paymentStatus = _params$data.paymentStatus,
					    txnType = _params$data.txnType,
					    txnDueDays = _params$data.txnDueDays;

					if (settingCache.isBillToBillEnabled()) {
						if (settingCache.isPaymentTermEnabled()) {
							if (txnDueDays > 0 && paymentStatus != _TxnPaymentStatusConstants2.default.PAID) {
								returnVal = 'Overdue (' + txnDueDays + ' days)';
							}
						}
					}
					if (!returnVal) {
						returnVal = _TxnPaymentStatusConstants2.default.getPaymentStatusForUI(txnType, paymentStatus);
					}
					return returnVal;
				},
				cellRenderer: PaymentStatusContainer,
				cellRendererParams: {
					settingCache: settingCache
				},
				width: 100
			}]);
		}

		_this7.columnDefs = [].concat((0, _toConsumableArray3.default)(_this7.columnDefs), [{
			field: 'id',
			colId: 'id1',
			headerName: '',
			width: 80,
			getQuickFilterText: function getQuickFilterText() {
				return '';
			},
			filter: false,
			sortable: false,
			cellRenderer: _MessageActionRenderer2.default
		}, {
			field: 'id',
			colId: 'id2',
			headerName: '',
			width: 30,
			getQuickFilterText: function getQuickFilterText() {
				return '';
			},
			filter: false,
			sortable: false,
			cellRenderer: function cellRenderer() {
				return '<div class="contextMenuDots"></div>';
			}
		}]);
	};

	this.printExcelTransactions = function () {
		_this7.setState({ loadingForExcel: true }, function () {
			var selected = [];
			var showTransactionType = _this7.props.includeReturnTransaction;
			var TransactionFactory = require('../../BizLogic/TransactionFactory');
			var transactionFactory = new TransactionFactory();
			_this7.api.forEachNodeAfterFilterAndSort(function (node) {
				var txn = transactionFactory.getTransactionObject(node.data.txnType);
				selected.push(txn.getTransactionFromId(node.data.id));
			});
			if (!selected.length) {
				ToastHelper.error('No transacton to print. Please change filters.');
				_this7.setState({ loadingForExcel: false });
				return false;
			}
			setTimeout(function () {
				var _state5 = _this7.state,
				    fromDate = _state5.fromDate,
				    toDate = _state5.toDate,
				    excelOptions = _state5.excelOptions;
				var txnType = _this7.props.txnType;


				var SaleReportHTMLGenerator = require('../../ReportHTMLGenerator/SaleReportHTMLGenerator');
				var saleReportHTMLGenerator = new SaleReportHTMLGenerator();
				var paymentStatusFilterModel = _this7.api.getFilterInstance('paymentStatus');
				var dataArray = saleReportHTMLGenerator.prepareObjectForExcel({
					listOfTransactions: selected,
					showDescription: excelOptions[1].value,
					showPaymentStatus: excelOptions[2].value,
					paymentStatVal: paymentStatusFilterModel && paymentStatusFilterModel.getModel().value.currentValue || '',
					showTransactionType: showTransactionType,
					txnType: txnType,
					showAdditionalFieds: excelOptions[3] && excelOptions[3].value || false
				});
				if (dataArray.length) {
					var XLSX = require('xlsx');
					var worksheet = XLSX.utils.aoa_to_sheet(dataArray);
					var ExcelItemDetails = excelOptions[0].value;
					var ExcelExportHelper = require('../../UIControllers/ExcelExportHelper');
					var excelExportHelper = new ExcelExportHelper();

					var workbook = {
						SheetNames: [],
						Sheets: {}
					};

					var wsName = _this7.transactionLabel + ' Report';
					workbook.SheetNames[0] = wsName;
					workbook.Sheets[wsName] = worksheet;
					var wscolWidth = [{ wch: 15 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
					worksheet['!cols'] = wscolWidth;

					if (ExcelItemDetails) {
						var wsNameItem = 'Item Details';
						workbook.SheetNames[1] = wsNameItem;
						var itemArray = excelExportHelper.prepareItemObjectForExcel(selected);
						var itemWorksheet = XLSX.utils.aoa_to_sheet(itemArray);
						workbook.Sheets[wsNameItem] = itemWorksheet;
						var wsItemcolWidth = [{ wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }];
						itemWorksheet['!cols'] = wsItemcolWidth;
					}
					XLSX.writeFile(workbook, appPath + '/report.xlsx');
					var fileUtil = require('../../Utilities/FileUtil');
					var PDFReportConstant = require('../../Constants/PDFReportConstant');
					var fileName = fileUtil.getFileName({
						type: txnType == _TxnTypeConstant2.default.TXN_TYPE_SALE ? PDFReportConstant.SALE_REPORT : PDFReportConstant.PURCHASE_REPORT,
						fromDate: fromDate,
						toDate: toDate
					});
					var ExcelHelper = require('../../Utilities/ExcelHelper');
					var excelHelper = new ExcelHelper();
					excelHelper.saveExcel(fileName);
				}
				_this7.setState({ loadingForExcel: false });
			}, 300);
		});
	};

	this.handleClick = function (event) {
		_this7.setState({ anchorEl: event.currentTarget });
	};

	this.handleClose = function (dateRange) {
		if (dateRange === undefined) {
			_this7.setState({ anchorEl: null });
			return false;
		}
		var _state6 = _this7.state,
		    fromDate = _state6.fromDate,
		    toDate = _state6.toDate;

		switch (dateRange) {
			case _this7.DATE_RANGE.all:
				// all
				fromDate = '';
				toDate = '';
				break;
			case _this7.DATE_RANGE.thisMonth:
				var _MyDate$getThisMonthF3 = _MyDate2.default.getThisMonthFromToDate();

				var _MyDate$getThisMonthF4 = (0, _slicedToArray3.default)(_MyDate$getThisMonthF3, 2);

				fromDate = _MyDate$getThisMonthF4[0];
				toDate = _MyDate$getThisMonthF4[1];

				break;
			case _this7.DATE_RANGE.lastMonth:
				var _MyDate$getLastMonthF = _MyDate2.default.getLastMonthFromToDate();

				var _MyDate$getLastMonthF2 = (0, _slicedToArray3.default)(_MyDate$getLastMonthF, 2);

				fromDate = _MyDate$getLastMonthF2[0];
				toDate = _MyDate$getLastMonthF2[1];

				break;
			case _this7.DATE_RANGE.thisYear:
				var _MyDate$getThisYearFr = _MyDate2.default.getThisYearFromToDate();

				var _MyDate$getThisYearFr2 = (0, _slicedToArray3.default)(_MyDate$getThisYearFr, 2);

				fromDate = _MyDate$getThisYearFr2[0];
				toDate = _MyDate$getThisYearFr2[1];

				break;
			case _this7.DATE_RANGE.thisQuarter:
				var _MyDate$getThisQuarte = _MyDate2.default.getThisQuarterFromToDate();

				var _MyDate$getThisQuarte2 = (0, _slicedToArray3.default)(_MyDate$getThisQuarte, 2);

				fromDate = _MyDate$getThisQuarte2[0];
				toDate = _MyDate$getThisQuarte2[1];

				break;
			case _this7.DATE_RANGE.custom:
				document.querySelector('#from-datepicker').focus();
				break;
		}
		if (dateRange != _this7.DATE_RANGE.all) {
			fromDate = _MyDate2.default.getDate('d/m/y', fromDate);
			toDate = _MyDate2.default.getDate('d/m/y', toDate);
		}
		_this7.setState({ anchorEl: null, fromDate: fromDate, toDate: toDate, dateRange: dateRange }, _this7.filterTransactions);
	};

	this.onPrintOptionClose = function (status) {
		_this7.setState({ showPrintOptions: false });
		if (status) {
			_this7.printTransactions();
		}
	};

	this.showExcelOptions = function () {
		var printOptions = _this7.state.printOptions;
		var txnType = _this7.props.txnType;

		var excelOptions = [].concat((0, _toConsumableArray3.default)(printOptions));

		var UDFCache = require('../../Cache/UDFCache.js');
		var uDFCache = new UDFCache();
		var settingCache = new _SettingCache2.default();

		var firmId = Number(settingCache.getDefaultFirmId());
		var hasAdditionalTxnFieldsEnabled = uDFCache.hasAdditionalFieldsEnabledForTxnType(firmId, txnType);
		if (hasAdditionalTxnFieldsEnabled) {
			excelOptions.push({
				key: 'additionalFieldsExcel',
				label: 'Print additional fields',
				value: false
			});
		}

		_this7.setState({ showExcelOptions: true, excelOptions: excelOptions });
	};

	this.onExcelOptionClose = function (status) {
		_this7.setState({ showExcelOptions: false });
		if (status) {
			_this7.printExcelTransactions();
		}
	};

	this.onPrintOptionChange = function (event, index) {
		var printOptions = _this7.state.printOptions;

		var current = printOptions[index];
		current.value = event.target.checked;
		_this7.setState({ printOptions: printOptions });
	};

	this.onExcelOptionChange = function (event, index) {
		var excelOptions = _this7.state.excelOptions;

		var current = excelOptions[index];
		current.value = event.target.checked;
		_this7.setState({ excelOptions: excelOptions });
	};

	this.loadGraph = function () {
		_this7.setState({ showGraph: true }, function () {
			$('#reportGraphContainer').load('saleGraphReport.html');
		});
	};

	this.hideGraph = function () {
		_this7.setState({ showGraph: false });
	};
};

SalePurchaseReportContainer.propTypes = {
	classes: _propTypes2.default.func,
	txnType: _propTypes2.default.number,
	registerPromise: _propTypes2.default.func
};

exports.default = (0, _withStyles2.default)(styles)((0, _trashableReact2.default)(SalePurchaseReportContainer));