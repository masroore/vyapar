Object.defineProperty(exports, "__esModule", {
	value: true
});
function SaleAgeingRightArrowComponent() {};
SaleAgeingRightArrowComponent.prototype.init = function (params) {
	var options = params.colDef.options;
	this.eGui = document.createElement('div');
	if (params.data.nameId != 'pinnedRowForSaleAgingReport') {
		this.eGui.innerHTML = '<div id="renderer-arrow-image" class="padtop8">\n\t\t\t\t<img src=\'./../inlineSVG/outline-keyboard_arrow_right-24px.svg\' />\n\t\t\t</div>';
		var arrow = this.eGui.querySelector('#renderer-arrow-image');
		arrow.addEventListener('click', function () {
			options.openDetailAgeingView(params);
		});
	}
};

SaleAgeingRightArrowComponent.prototype.getGui = function () {
	return this.eGui;
};

exports.default = SaleAgeingRightArrowComponent;