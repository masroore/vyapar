Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _Dialog = require('@material-ui/core/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _SearchBox = require('./SearchBox');

var _SearchBox2 = _interopRequireDefault(_SearchBox);

var _Close = require('@material-ui/icons/Close');

var _Close2 = _interopRequireDefault(_Close);

var _Queries = require('../../Constants/Queries');

var Queries = _interopRequireWildcard(_Queries);

var _StringConstants = require('../../Constants/StringConstants');

var _StringConstants2 = _interopRequireDefault(_StringConstants);

var _SettingCache = require('../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

var _MyDate = require('../../Utilities/MyDate');

var _MyDate2 = _interopRequireDefault(_MyDate);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _DecimalInputFilter = require('../../Utilities/DecimalInputFilter');

var _DecimalInputFilter2 = _interopRequireDefault(_DecimalInputFilter);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _themes = require('../../themes');

var _themes2 = _interopRequireDefault(_themes);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var settingCache = new _SettingCache2.default();

var theme = (0, _themes2.default)();

var styles = function styles(theme) {
	return {
		dialogContainer: {
			width: '100%',
			maxWidth: '100%'
		},
		borderRadius: {
			borderRadius: 8
		},
		container: {},
		header: {
			marginTop: 15
		},
		footer: {
			borderTop: '1px solid #DDDDDD',
			textAlign: 'right',
			marginRight: 5,
			paddingTop: 7,
			height: 44
		},
		title: {
			color: '#222A3F',
			fontSize: 14,
			fontFamily: 'robotomedium',
			paddingLeft: 15
		},
		searchBox: {
			background: '#E9ECF0',
			marginLeft: 15
		},
		trackingList: {
			marginTop: 15,
			maxHeight: 500,
			overflow: 'auto'
		},
		column: {
			borderRight: '1px solid #DDDDDD',
			display: 'flex',
			alignItems: 'center',
			padding: '0 10px',
			width: 98,
			overflow: 'hidden',
			textOverflow: 'ellipsis'
		},
		currentQtyColumn: {
			width: 110
		},
		stockTrackingHeader: {
			fontSize: 12,
			color: 'rgba(34, 42, 63, 0.5)',
			height: 46,
			borderTop: '1px solid #ddd',
			boxShadow: '0px 0px 5px 0px #000',
			marginBottom: 1,
			fontFamily: 'robotomedium',
			'& div:last-child': {
				border: 'none'
			}
		},
		stockTrackingItem: {
			height: 42,
			fontSize: 12,
			borderTop: '1px solid #ddd',
			color: '#222A3F',
			fontFamily: 'robotomedium',
			'&:nth-child(even)': {
				background: '#E9ECF0'
			},
			'& div:last-child': {
				border: 'none'
			}
		},
		qtyInput: {
			border: '1px solid #1789FC',
			width: 80,
			borderRadius: 4,
			height: 32,
			padding: 5,
			textAlign: 'right'
		},
		closeIconWrapper: {
			display: 'flex',
			alignItems: 'center',
			height: 24,
			width: 24,
			justifyContent: 'center',
			borderRadius: 12,
			marginRight: 10,
			cursor: 'pointer'
		},
		closeIcon: {
			color: 'rgba(34,58,82,0.54)'
		},
		ellipsis: {
			overflow: 'hidden',
			textOverflow: 'ellipsis',
			whiteSpace: 'nowrap'
		},
		serialNo: {
			width: 150
		}
	};
};

var SelectBatchesModal = function (_React$Component) {
	(0, _inherits3.default)(SelectBatchesModal, _React$Component);

	function SelectBatchesModal(props) {
		(0, _classCallCheck3.default)(this, SelectBatchesModal);

		var _this = (0, _possibleConstructorReturn3.default)(this, (SelectBatchesModal.__proto__ || (0, _getPrototypeOf2.default)(SelectBatchesModal)).call(this, props));

		_this.state = {
			itemStockTrackingList: [],
			filterText: ''
		};
		_this.onClose = _this.onClose.bind(_this);
		_this.onSave = _this.onSave.bind(_this);
		_this.quickFilter = _this.quickFilter.bind(_this);

		_this.mrpColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_MRP);
		_this.batchNumberColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_BATCH_NUMBER);
		_this.serialNumberColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_SERIAL_ITEM_NUMBER);
		_this.mfgDateColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_MANUFACTURING_DATE);
		_this.expDateColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_EXPIRY_DATE);
		_this.sizeColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_SIZE);

		_this.mrpHeader = settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_MRP_VALUE);
		_this.batchNoHeader = settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_BATCH_NUMBER_VALUE);
		_this.srNoHeader = settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_SERIAL_NUMBER_VALUE);
		_this.mfgDateHeader = settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_MANUFACTURING_DATE_VALUE);
		_this.expDateHeader = settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_EXPIRY_DATE_VALUE);
		_this.sizeHeader = settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_SIZE_VALUE);
		return _this;
	}

	(0, _createClass3.default)(SelectBatchesModal, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			var itemStockTrackingList = this.props.itemStockTrackingList.map(function (item) {
				var txn = item;
				var istId = txn.getIstId();
				var istExpiryDateObj = void 0;
				var istManufacturingDateObj = void 0;

				var istBatchNumber = txn.getIstBatchNumber() ? String(txn.getIstBatchNumber()) : '';
				var istManufacturingDate = txn.getIstManufacturingDate() ? txn.getIstManufacturingDate() : '';
				var istExpiryDate = txn.getIstExpiryDate() ? txn.getIstExpiryDate() : '';
				var istSerialNumber = txn.getIstSerialNumber() ? String(txn.getIstSerialNumber()) : '';
				var istSize = txn.getIstSize() ? String(txn.getIstSize()) : '';
				var istMrp = txn.getIstMrp() ? String(txn.getIstMrp()) : '';
				var currentQuantity = String(_MyDouble2.default.getQuantityWithDecimalWithoutColor(txn.getIstCurrentQuantity()));
				istExpiryDateObj = istExpiryDate;
				istManufacturingDateObj = istManufacturingDate;
				var expiryDateFormat = void 0;
				var mfgDateFormat = void 0;

				if (istManufacturingDate && settingCache.getManufacturingDateFormat() == _StringConstants2.default.monthYear) {
					istManufacturingDate = _MyDate2.default.getDate('m/y', istManufacturingDate);
					mfgDateFormat = 'MM/yyyy';
				} else if (istManufacturingDate && settingCache.getManufacturingDateFormat() == _StringConstants2.default.dateMonthYear) {
					istManufacturingDate = _MyDate2.default.getDate('d/m/y', istManufacturingDate);
					mfgDateFormat = 'dd/MM/yyyy';
				}

				if (istExpiryDate && settingCache.getExpiryDateFormat() == _StringConstants2.default.monthYear) {
					istExpiryDate = _MyDate2.default.getDate('m/y', istExpiryDate);
					expiryDateFormat = 'MM/yyyy';
				} else if (istExpiryDate && settingCache.getExpiryDateFormat() == _StringConstants2.default.dateMonthYear) {
					istExpiryDate = _MyDate2.default.getDate('d/m/y', istExpiryDate);
					expiryDateFormat = 'dd/MM/yyyy';
				}

				return {
					expiryDateFormat: expiryDateFormat,
					mfgDateFormat: mfgDateFormat,
					istBatchNumber: istBatchNumber,
					istExpiryDate: istExpiryDate,
					istExpiryDateObj: istExpiryDateObj,
					istManufacturingDate: istManufacturingDate,
					istManufacturingDateObj: istManufacturingDateObj,
					istSerialNumber: istSerialNumber,
					istSize: istSize,
					istMrp: istMrp,
					currentQuantity: currentQuantity,
					qty: 0,
					istId: istId,
					logic: item
				};
			});

			this.itemStockTrackingList = itemStockTrackingList;
			this.setState({ itemStockTrackingList: itemStockTrackingList });
		}
	}, {
		key: 'onClose',
		value: function onClose(e) {
			e.stopPropagation();
			this.props.onClose && this.props.onClose();
		}
	}, {
		key: 'quickFilter',
		value: function quickFilter(text) {
			var _this2 = this;

			var searchText = (text || '').toLowerCase();
			var itemStockTrackingList = this.itemStockTrackingList.filter(function (item) {
				if (_this2.batchNumberColumnVisibility) {
					if (('' + item.istBatchNumber).toLowerCase().includes(searchText)) {
						return true;
					}
				}
				if (_this2.mfgDateColumnVisibility) {
					if (('' + item.istManufacturingDate).toLowerCase().includes(searchText)) {
						return true;
					}
				}
				if (_this2.expDateColumnVisibility) {
					if (('' + item.istExpiryDate).toLowerCase().includes(searchText)) {
						return true;
					}
				}
				if (_this2.serialNumberColumnVisibility) {
					if (('' + item.istSerialNumber).toLowerCase().includes(searchText)) {
						return true;
					}
				}
				if (_this2.sizeColumnVisibility) {
					if (('' + item.istSize).toLowerCase().includes(searchText)) {
						return true;
					}
				}
				if (_this2.mrpColumnVisibility) {
					if (('' + item.istMrp).toLowerCase().includes(searchText)) {
						return true;
					}
				}
				if (('' + item.currentQuantity).toLowerCase().includes(searchText)) {
					return true;
				}
				return false;
			});
			this.setState({ filterText: text, itemStockTrackingList: itemStockTrackingList });
		}
	}, {
		key: 'changeQty',
		value: function changeQty(e, currentItem) {
			var value = _DecimalInputFilter2.default.inputTextFilterForQuantity(e.target);
			currentItem.qty = value;
			this.forceUpdate();
		}
	}, {
		key: 'onSave',
		value: function onSave(e) {
			e.preventDefault();
			var selected = this.itemStockTrackingList.filter(function (item) {
				return item.qty && item.qty > 0;
			});
			this.props.onClose && this.props.onClose(selected);
		}
	}, {
		key: 'render',
		value: function render() {
			var _this3 = this;

			var classes = this.props.classes;
			var itemStockTrackingList = this.state.itemStockTrackingList;

			return _react2.default.createElement(
				_styles.MuiThemeProvider,
				{ theme: theme },
				_react2.default.createElement(
					_Dialog2.default,
					{
						disableEnforceFocus: true,
						maxWidth: false,
						classes: {
							paperWidthSm: classes.dialogContainer,
							paper: classes.borderRadius
						},
						open: true,
						onClose: this.onClose
					},
					_react2.default.createElement(
						'form',
						{ onSubmit: this.onSave },
						_react2.default.createElement(
							'div',
							{ className: classes.container },
							_react2.default.createElement(
								'div',
								{
									className: (0, _classnames2.default)(classes.header, 'd-flex justify-content-between')
								},
								_react2.default.createElement(
									'div',
									{ className: 'd-flex align-items-center' },
									_react2.default.createElement(
										'div',
										{ className: classes.title },
										'Select Batches'
									),
									_react2.default.createElement(_SearchBox2.default, {
										placeholder: 'Search',
										throttle: 200,
										filter: this.quickFilter,
										className: classes.searchBox
									})
								),
								_react2.default.createElement(
									'div',
									{
										onClick: this.onClose,
										className: classes.closeIconWrapper
									},
									_react2.default.createElement(_Close2.default, { className: classes.closeIcon })
								)
							),
							_react2.default.createElement(
								'div',
								{ className: classes.trackingList },
								_react2.default.createElement(
									'div',
									{
										className: (0, _classnames2.default)(classes.stockTrackingHeader, 'd-flex')
									},
									this.mrpColumnVisibility && _react2.default.createElement(
										'div',
										{ className: (0, _classnames2.default)(classes.column) },
										this.mrpHeader
									),
									this.batchNumberColumnVisibility && _react2.default.createElement(
										'div',
										{ className: (0, _classnames2.default)(classes.column) },
										this.batchNoHeader
									),
									this.serialNumberColumnVisibility && _react2.default.createElement(
										'div',
										{ className: (0, _classnames2.default)(classes.column, classes.serialNo) },
										this.srNoHeader
									),
									this.mfgDateColumnVisibility && _react2.default.createElement(
										'div',
										{ className: (0, _classnames2.default)(classes.column) },
										this.mfgDateHeader
									),
									this.expDateColumnVisibility && _react2.default.createElement(
										'div',
										{ className: (0, _classnames2.default)(classes.column) },
										this.expDateHeader
									),
									this.sizeColumnVisibility && _react2.default.createElement(
										'div',
										{ className: (0, _classnames2.default)(classes.column) },
										this.sizeHeader
									),
									_react2.default.createElement(
										'div',
										{
											className: (0, _classnames2.default)(classes.column, classes.currentQtyColumn)
										},
										'CURRENT QTY'
									),
									_react2.default.createElement(
										'div',
										{ className: (0, _classnames2.default)(classes.column) },
										'QTY'
									)
								),
								itemStockTrackingList.map(function (item, index) {
									return _react2.default.createElement(
										'div',
										{
											key: item.logic.getIstId(),
											className: (0, _classnames2.default)(classes.stockTrackingItem, 'd-flex')
										},
										_this3.mrpColumnVisibility && _react2.default.createElement(
											'div',
											{
												className: (0, _classnames2.default)(classes.column),
												title: item.istMRP
											},
											_react2.default.createElement(
												'div',
												{ className: classes.ellipsis },
												item.istMrp
											)
										),
										_this3.batchNumberColumnVisibility && _react2.default.createElement(
											'div',
											{
												className: (0, _classnames2.default)(classes.column),
												title: item.istBatchNumber
											},
											_react2.default.createElement(
												'div',
												null,
												item.istBatchNumber
											)
										),
										_this3.serialNumberColumnVisibility && _react2.default.createElement(
											'div',
											{
												className: (0, _classnames2.default)(classes.column, classes.serialNo),
												title: item.istSerialNumber
											},
											_react2.default.createElement(
												'div',
												null,
												item.istSerialNumber
											)
										),
										_this3.mfgDateColumnVisibility && _react2.default.createElement(
											'div',
											{
												className: (0, _classnames2.default)(classes.column),
												title: item.istManufacturingDate
											},
											_react2.default.createElement(
												'div',
												{ className: classes.ellipsis },
												item.istManufacturingDate
											)
										),
										_this3.expDateColumnVisibility && _react2.default.createElement(
											'div',
											{
												className: (0, _classnames2.default)(classes.column),
												title: item.istExpiryDate
											},
											_react2.default.createElement(
												'div',
												{ className: classes.ellipsis },
												item.istExpiryDate
											)
										),
										_this3.sizeColumnVisibility && _react2.default.createElement(
											'div',
											{
												className: (0, _classnames2.default)(classes.column),
												title: item.istSize
											},
											_react2.default.createElement(
												'div',
												{ className: classes.ellipsis },
												item.istSize
											)
										),
										_react2.default.createElement(
											'div',
											{
												className: (0, _classnames2.default)(classes.column, classes.currentQtyColumn),
												title: item.currentQuantity
											},
											item.currentQuantity
										),
										_react2.default.createElement(
											'div',
											{ className: (0, _classnames2.default)(classes.column) },
											_react2.default.createElement('input', {
												autoFocus: index === 0,
												onChange: function onChange(e) {
													return _this3.changeQty(e, item);
												},
												value: item.qty == 0 ? '' : item.qty,
												type: 'text',
												className: classes.qtyInput
											})
										)
									);
								})
							),
							_react2.default.createElement(
								'div',
								{ className: classes.footer },
								_react2.default.createElement(
									_Button2.default,
									{
										type: 'submit',
										size: 'small',
										variant: 'contained',
										color: 'primary'
									},
									'DONE'
								)
							)
						)
					)
				)
			);
		}
	}]);
	return SelectBatchesModal;
}(_react2.default.Component);

SelectBatchesModal.propTypes = {
	classes: _propTypes2.default.object,
	itemStockTrackingList: _propTypes2.default.array,
	onClose: _propTypes2.default.func
};
exports.default = (0, _styles.withStyles)(styles)(SelectBatchesModal);