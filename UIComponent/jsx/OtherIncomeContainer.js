Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _SearchBox = require('./SearchBox');

var _SearchBox2 = _interopRequireDefault(_SearchBox);

var _throttleDebounce = require('throttle-debounce');

var _AddOtherIncomeButton = require('./AddOtherIncomeButton');

var _AddOtherIncomeButton2 = _interopRequireDefault(_AddOtherIncomeButton);

var _OtherIncomeTransactions = require('./OtherIncomeTransactions');

var _OtherIncomeTransactions2 = _interopRequireDefault(_OtherIncomeTransactions);

var _OtherIncomeDetail = require('./OtherIncomeDetail');

var _OtherIncomeDetail2 = _interopRequireDefault(_OtherIncomeDetail);

var _OtherIncomeList = require('./OtherIncomeList');

var _OtherIncomeList2 = _interopRequireDefault(_OtherIncomeList);

var _FirstScreen = require('./UIControls/FirstScreen');

var _FirstScreen2 = _interopRequireDefault(_FirstScreen);

var _TransactionHelper = require('../../Utilities/TransactionHelper');

var _TxnTypeConstant2 = require('../../Constants/TxnTypeConstant');

var _TxnTypeConstant3 = _interopRequireDefault(_TxnTypeConstant2);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var OtherIncomeContainer = function (_React$Component) {
	(0, _inherits3.default)(OtherIncomeContainer, _React$Component);

	function OtherIncomeContainer(props) {
		(0, _classCallCheck3.default)(this, OtherIncomeContainer);

		var _this = (0, _possibleConstructorReturn3.default)(this, (OtherIncomeContainer.__proto__ || (0, _getPrototypeOf2.default)(OtherIncomeContainer)).call(this, props));

		_this.filterItems = _this.filterItems.bind(_this);
		_this.onItemSelected = _this.onItemSelected.bind(_this);
		var otherIncomes = _this.getOtherIncomes();
		var currentItem = null;
		_this.allOtherIncomes = otherIncomes;
		_this.state = {
			records: [],
			otherIncomes: otherIncomes,
			currentItem: currentItem,
			contextMenu: _this.contextMenu,
			showSearch: false
		};
		window.onResume = _this.onResume.bind(_this);
		_this._id = null;
		return _this;
	}

	(0, _createClass3.default)(OtherIncomeContainer, [{
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			delete window.onResume;
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			window.onResume = this.onResume.bind(this);
		}
	}, {
		key: 'onResume',
		value: function onResume() {
			var _this2 = this;

			this.props.onResume && this.props.onResume();
			var otherIncomes = this.getOtherIncomes();
			this.allOtherIncomes = otherIncomes;
			if (this.state.currentItem) {
				var currentItem = this.allOtherIncomes.find(function (income) {
					return _this2.state.currentItem.incomeId === income.incomeId;
				});
				this.setState({ currentItem: currentItem });
			}
			this.filterItems(this.state.searchText);
			this.onItemSelected(null, true, this.state.currentItem);
		}
	}, {
		key: 'getOtherIncomes',
		value: function getOtherIncomes() {
			var DataLoader = require('../../DBManager/DataLoader');
			var dataLoader = new DataLoader();
			var list = dataLoader.loadIncomeList();
			var otherIncomes = list.map(function (income) {
				return {
					incomeId: income.getCategoryId(),
					incomeName: income.getCategoryName(),
					incomeAmount: income.getAmount()
				};
			});
			return otherIncomes;
		}
	}, {
		key: 'getIncomeTransactions',
		value: function getIncomeTransactions(income) {
			var records = [];
			if (income && income.incomeId) {
				var _TxnTypeConstant = require('../../Constants/TxnTypeConstant');
				var DataLoader = require('../../DBManager/DataLoader');
				var dataLoader = new DataLoader();
				var incomeTxns = dataLoader.LoadAllTransactions(income.incomeId, true);
				records = incomeTxns.map(function (txn) {
					var txnId = txn.getTxnId();
					var typeTxn = _TxnTypeConstant.getTxnType(txn.getTxnType());
					var date = txn.getTxnDate();
					var amount = Number(txn.getCashAmount()) + Number(txn.getBalanceAmount()) || 0;
					return {
						txnId: txnId,
						typeTxn: typeTxn,
						amount: amount,
						date: date
					};
				});
			}
			return records;
		}
	}, {
		key: 'onItemSelected',
		value: function onItemSelected(row) {
			var fromSync = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
			var currentItem = arguments[2];

			if (!fromSync) {
				if (!row.node.selected) {
					return false;
				}
				this.setState({ currentItem: row.data });
			}
			if (row && row.data) {
				currentItem = row.data;
			}
			if (currentItem) {
				var incomeTransactions = this.getIncomeTransactions(currentItem);
				this.setState({
					records: incomeTransactions
				});
			}
		}
	}, {
		key: 'filterItems',
		value: function filterItems(searchText) {
			if (!searchText) {
				this.setState({ otherIncomes: this.allOtherIncomes, searchText: '' });
				return false;
			}
			var query = searchText.toLowerCase();
			var filteredItems = this.allOtherIncomes.filter(function (income) {
				return income.incomeName.toLowerCase().includes(query);
			});
			var records = this.state.records;
			var currentItem = this.state.currentItem;
			if (!filteredItems.length) {
				records = [];
				currentItem = null;
			}
			this.setState({
				records: records,
				currentItem: currentItem,
				otherIncomes: filteredItems,
				searchText: searchText
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _this3 = this;

			var _state = this.state,
			    otherIncomes = _state.otherIncomes,
			    records = _state.records,
			    currentItem = _state.currentItem;

			return _react2.default.createElement(
				'div',
				{ className: 'd-flex flex-column', style: { height: '100%' } },
				_react2.default.createElement(
					'ul',
					{ className: 'tab-container d-flex' },
					_react2.default.createElement(
						'li',
						{ className: 'tab-items col' },
						'OTHER INCOMES'
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'd-flex align-items-stretch flex-container' },
					this.allOtherIncomes.length === 0 && _react2.default.createElement(_FirstScreen2.default, {
						image: '../inlineSVG/first-screens/income.svg',
						text: 'Add any additional income from other sources outside business & know your real profits.',
						onClick: function onClick() {
							(0, _TransactionHelper.viewTransaction)('', _TxnTypeConstant3.default.getTxnType(_TxnTypeConstant3.default.TXN_TYPE_OTHER_INCOME));
						},
						buttonLabel: 'Add Your First Other Income'
					}),
					this.allOtherIncomes.length > 0 && _react2.default.createElement(
						_react.Fragment,
						null,
						_react2.default.createElement(
							'div',
							{ className: 'flex-column d-flex left-column' },
							_react2.default.createElement(
								'div',
								{ className: 'listheaderDiv d-flex mt-20 mb-10' },
								_react2.default.createElement(
									'div',
									{
										className: (0, _classnames2.default)('searchDiv', this.state.showSearch ? 'width100 d-flex' : '')
									},
									_react2.default.createElement(_SearchBox2.default, {
										throttle: 500,
										filter: this.filterItems,
										collapsed: !this.state.showSearch,
										collapsedTextBoxClass: 'width100',
										onCollapsedClick: function onCollapsedClick() {
											_this3.setState({ showSearch: true });
										},
										onInputBlur: function onInputBlur() {
											_this3.setState({ showSearch: false });
										}
									})
								),
								_react2.default.createElement(
									'div',
									{ className: 'buttonDiv flex-1 d-flex justify-content-end' },
									_react2.default.createElement(_AddOtherIncomeButton2.default, { collapsed: this.state.showSearch })
								)
							),
							_react2.default.createElement(_OtherIncomeList2.default, {
								onItemSelected: this.onItemSelected,
								items: otherIncomes
							})
						),
						_react2.default.createElement(
							'div',
							{ className: 'd-flex flex-column flex-grow-1 right-column' },
							_react2.default.createElement(_OtherIncomeDetail2.default, { currentItem: currentItem }),
							_react2.default.createElement(_OtherIncomeTransactions2.default, { items: records, addExtraColumns: true })
						)
					)
				)
			);
		}
	}]);
	return OtherIncomeContainer;
}(_react2.default.Component);

OtherIncomeContainer.propTypes = {
	onResume: _propTypes2.default.func
};

exports.default = OtherIncomeContainer;