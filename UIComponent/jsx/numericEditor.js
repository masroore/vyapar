Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = NumericCellEditor;

var _DecimalInputFilter = require('./../../Utilities/DecimalInputFilter');

var _DecimalInputFilter2 = _interopRequireDefault(_DecimalInputFilter);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var KEY_BACKSPACE = 8;
var KEY_DELETE = 46;
var KEY_F2 = 113;

function NumericCellEditor() {};

NumericCellEditor.prototype.init = function (params) {
	var _this = this;

	this.eInput = document.createElement('input');

	var charPressIsNotANumber = params.charPress && '1234567890'.indexOf(params.charPress) < 0;
	this.cancelBeforeStart = charPressIsNotANumber;

	var startValue = void 0;
	var highlightAllOnFocus = true;
	if (params.keyPress === KEY_BACKSPACE || params.keyPress === KEY_DELETE) {
		// if backspace or delete pressed, we clear the cell
		startValue = '';
	} else if (params.charPress) {
		// if a letter was pressed, we start with the letter
		startValue = params.charPress;
		highlightAllOnFocus = false;
	} else {
		// otherwise we start with the current value
		startValue = params.value;
		if (params.keyPress === KEY_F2) {
			highlightAllOnFocus = false;
		}
	}

	this.state = {
		value: startValue,
		highlightAllOnFocus: highlightAllOnFocus
	};

	var onInput = function onInput(event) {
		_DecimalInputFilter2.default.inputTextFilterForAmount(event.target);
	};

	var onKeyDown = function onKeyDown(event) {
		if (_this.isLeftOrRight(event) || _this.deleteOrBackspace(event)) {
			event.stopPropagation();
			return;
		}
		if (!_this.isKeyPressedNumeric(event)) {
			if (event.preventDefault) event.preventDefault();
		}
	};

	var handleChange = function handleChange(event) {
		_this.state.value = event.target.value;
	};

	this.eInput.addEventListener('input', onInput);
	this.eInput.addEventListener('change', handleChange);
	this.eInput.addEventListener('keydown', onKeyDown);
	this.eInput.value = this.state.value;
};

// gets called once when grid ready to insert the element
NumericCellEditor.prototype.getGui = function () {
	return this.eInput;
};

// focus and select can be done after the gui is attached
NumericCellEditor.prototype.afterGuiAttached = function () {
	var eInput = this.eInput;
	eInput.focus();
	if (this.state.highlightAllOnFocus) {
		eInput.select();
		this.state.highlightAllOnFocus = false;
	} else {
		var length = eInput.value ? eInput.value.length : 0;
		if (length > 0) {
			eInput.setSelectionRange(length, length);
		}
	}
};

// returns the new value after editing
NumericCellEditor.prototype.isCancelBeforeStart = function () {
	return this.cancelBeforeStart;
};

// example - will reject the number if it contains the value 007
// - not very practical, but demonstrates the method.
NumericCellEditor.prototype.isCancelAfterEnd = function () {
	var value = this.getValue();
	return value > 1000000;
};

// returns the new value after editing
NumericCellEditor.prototype.getValue = function () {
	return this.eInput.value;
};

// if true, then this editor will appear in a popup
NumericCellEditor.prototype.isPopup = function () {
	// and we could leave this method out also, false is the default
	return false;
};

NumericCellEditor.prototype.isLeftOrRight = function (event) {
	return [37, 39].indexOf(event.keyCode) > -1;
};

NumericCellEditor.prototype.isCharNumeric = function (charStr) {
	return !!/^[0-9]|[.]$/.test(charStr);
};

NumericCellEditor.prototype.isKeyPressedNumeric = function (event) {
	var charCode = this.getCharCodeFromEvent(event);
	var charStr = event.key ? event.key : String.fromCharCode(charCode);
	return this.isCharNumeric(charStr);
};

NumericCellEditor.prototype.getCharCodeFromEvent = function (event) {
	event = event || window.event;
	return typeof event.which === 'undefined' ? event.keyCode : event.which;
};

NumericCellEditor.prototype.deleteOrBackspace = function (event) {
	return [KEY_DELETE, KEY_BACKSPACE].indexOf(event.keyCode) > -1;
};