Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.ActivateInactivateUnitItems = undefined;

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _IconButton = require('@material-ui/core/IconButton');

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Menu = require('@material-ui/core/Menu');

var _Menu2 = _interopRequireDefault(_Menu);

var _MenuItem = require('@material-ui/core/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _MoreVert = require('@material-ui/icons/MoreVert');

var _MoreVert2 = _interopRequireDefault(_MoreVert);

var _SelectItemsModal = require('./SelectItemsModal');

var _SelectItemsModal2 = _interopRequireDefault(_SelectItemsModal);

var _DataInserter = require('../../DBManager/DataInserter.js');

var _DataInserter2 = _interopRequireDefault(_DataInserter);

var _ErrorCode = require('../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _AssignUnit = require('./AssignUnit');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var options = ['Bulk Inactive', 'Bulk Active'];

var ITEM_HEIGHT = 48;

var ActivateInactivateUnitItems = exports.ActivateInactivateUnitItems = function (_React$Component) {
	(0, _inherits3.default)(ActivateInactivateUnitItems, _React$Component);

	function ActivateInactivateUnitItems(props) {
		(0, _classCallCheck3.default)(this, ActivateInactivateUnitItems);

		var _this = (0, _possibleConstructorReturn3.default)(this, (ActivateInactivateUnitItems.__proto__ || (0, _getPrototypeOf2.default)(ActivateInactivateUnitItems)).call(this, props));

		_this.openActiveInactiveDialog = _this.openActiveInactiveDialog.bind(_this);
		_this.onCloseItemDialog = _this.onCloseItemDialog.bind(_this);
		_this.onSave = _this.onSave.bind(_this);
		_this.handleClick = _this.handleClick.bind(_this);
		_this.handleClose = _this.handleClose.bind(_this);
		_this.buttonName = '';
		_this.activeInactiveMessage = '';
		_this.activeInactiveTitle = '';
		_this.state = {
			anchorEl: null,
			itemList: [],
			itemDialogOpen: false,
			done: false
		};
		return _this;
	}

	(0, _createClass3.default)(ActivateInactivateUnitItems, [{
		key: 'onSave',
		value: function onSave(selectedUnits) {
			var activeOrInactive = void 0;
			if (!selectedUnits.length) {
				ToastHelper.info('Please select at least one item.');
				return false;
			}
			var dataInserter = new _DataInserter2.default();
			if (this.buttonName == 'Mark as Inactive') {
				var statusCode = dataInserter.updateActiveInactiveItems(selectedUnits, 0);
				activeOrInactive = 'inactive';
			} else if (this.buttonName == 'Mark as Active') {
				statusCode = dataInserter.updateActiveInactiveItems(selectedUnits, 1);
				activeOrInactive = 'active';
			}
			if (statusCode == _ErrorCode2.default.ERROR_ITEM_UPDATE_SUCCESS) {
				ToastHelper.success(statusCode);
				this.setState({
					itemDialogOpen: false
				});
				if (window.onResume) {
					window.onResume();
				}
				this.props.refreshItemCells(selectedUnits, activeOrInactive);
			} else {
				ToastHelper.error(statusCode);
			}
		}
	}, {
		key: 'onCloseItemDialog',
		value: function onCloseItemDialog() {
			this.setState({
				itemDialogOpen: false
			});
		}
	}, {
		key: 'handleClick',
		value: function handleClick(event) {
			this.setState({ anchorEl: event.currentTarget });
		}
	}, {
		key: 'handleClose',
		value: function handleClose() {
			this.setState({ anchorEl: null });
		}
	}, {
		key: 'openActiveInactiveDialog',
		value: function openActiveInactiveDialog(option) {
			var items = [];
			if (option == 'Bulk Inactive') {
				items = this.props.items.filter(function (item) {
					return item.getIsItemActive() == '1';
				});
				this.buttonName = 'Mark as Inactive';
				this.activeInactiveMessage = 'Showing only active items.';
				this.activeInactiveTitle = 'Bulk Inactive';
			} else if (option == 'Bulk Active') {
				items = this.props.items.filter(function (item) {
					return item.getIsItemActive() == '0';
				});
				this.buttonName = 'Mark as Active';
				this.activeInactiveMessage = 'Showing only inactive items.';
				this.activeInactiveTitle = 'Bulk Active';
			}
			this.setState({
				anchorEl: null,
				itemList: items,
				itemDialogOpen: true
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _this2 = this;

			var anchorEl = this.state.anchorEl;

			var open = Boolean(anchorEl);

			return _react2.default.createElement(
				'div',
				{ id: 'bulkActions' },
				_react2.default.createElement(
					_SelectItemsModal2.default,
					{
						activeInactiveMessage: this.activeInactiveMessage,
						activeInactiveTitle: this.activeInactiveTitle,
						isOpen: this.state.itemDialogOpen,
						onSave: this.onSave,
						onClose: this.onCloseItemDialog,
						items: this.state.itemList,
						notSelectFirstRow: true
					},
					' ',
					this.buttonName,
					' '
				),
				_react2.default.createElement(
					_IconButton2.default,
					{
						'aria-label': 'More',
						'aria-owns': open ? 'long-menu' : undefined,
						'aria-haspopup': 'true',
						onClick: this.handleClick
					},
					_react2.default.createElement(_MoreVert2.default, null)
				),
				_react2.default.createElement(
					_Menu2.default,
					{
						id: 'long-menu',
						disableEnforceFocus: true,
						anchorEl: anchorEl,
						open: open,
						onClose: this.handleClose,
						PaperProps: {
							style: {
								maxHeight: ITEM_HEIGHT * 4.5,
								width: 200
							}
						}
					},
					options.map(function (option) {
						return _react2.default.createElement(
							_MenuItem2.default,
							{ key: option, selected: option === 'Pyxis', onClick: function onClick() {
									return _this2.openActiveInactiveDialog(option);
								} },
							option
						);
					}),
					_react2.default.createElement(
						_MenuItem2.default,
						null,
						_react2.default.createElement(_AssignUnit.SelectItemsForUnitDialog, {
							isService: !!this.props.isService
						})
					)
				)
			);
		}
	}]);
	return ActivateInactivateUnitItems;
}(_react2.default.Component);

ActivateInactivateUnitItems.propTypes = {
	refreshItemCells: _propTypes2.default.func,
	isService: _propTypes2.default.bool,
	items: _propTypes2.default.array
};