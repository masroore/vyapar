Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _ButtonDropdown = require('./ButtonDropdown');

var _ButtonDropdown2 = _interopRequireDefault(_ButtonDropdown);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AddUnitButton = function (_React$Component) {
	(0, _inherits3.default)(AddUnitButton, _React$Component);

	function AddUnitButton() {
		(0, _classCallCheck3.default)(this, AddUnitButton);
		return (0, _possibleConstructorReturn3.default)(this, (AddUnitButton.__proto__ || (0, _getPrototypeOf2.default)(AddUnitButton)).apply(this, arguments));
	}

	(0, _createClass3.default)(AddUnitButton, [{
		key: 'addNewUnit',
		value: function addNewUnit() {
			var CommonUtility = require('../../Utilities/CommonUtility');
			window.unitNameGlobal = '';
			CommonUtility.loadFrameDiv('NewItemUnit.html');
		}
	}, {
		key: 'getMenuOptions',
		value: function getMenuOptions() {
			var menuOptions = [];
			menuOptions.push({
				buttonText: 'Add Units',
				buttonIcon: _react2.default.createElement(
					'span',
					null,
					'+'
				),
				onClick: this.addNewUnit
			});
			return menuOptions;
		}
	}, {
		key: 'render',
		value: function render() {
			return _react2.default.createElement(_ButtonDropdown2.default, {
				collapsed: this.props.collapsed,
				addPadding: true,
				menuOptions: this.getMenuOptions()
			});
		}
	}]);
	return AddUnitButton;
}(_react2.default.Component);

exports.default = AddUnitButton;

AddUnitButton.propTypes = {
	collapsed: _propTypes2.default.bool
};