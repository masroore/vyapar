var gridApi = void 0;
var getSelectedParties = function getSelectedParties(selectedPartiesRequiredFor) {
    var selectedPartiesData = selectedPartiesRequiredFor === 'email' ? '' : [];
    var selectedNodes = gridApi.getSelectedNodes();

    selectedNodes.forEach(function (node) {
        var name = node.data;
        if (selectedPartiesRequiredFor === 'email') {
            var email = name.getEmail();
            if (email) {
                selectedPartiesData += email + ',';
            }
        } else if (name.getPhoneNumber()) {
            selectedPartiesData.push(name);
        }
    });

    if (!selectedNodes || selectedNodes.length === 0 || !selectedPartiesData || !selectedPartiesData.length) {
        ToastHelper.error('Please select at least one party with contact information to share catalogue details.');
    }
    return selectedPartiesData;
};
var processRequestIfUserIsOnline = function processRequestIfUserIsOnline(callback) {
    var isOnline = require('is-online');
    isOnline().then(function (online) {
        if (online) {
            callback();
        } else {
            ToastHelper.error('No internet connection');
        }
    });
};
var CatalogueSharingOption = {
    sendEmail: function sendEmail(catalogueLink, callback) {
        return function (api) {
            gridApi = api;
            var emailRequestCallback = function emailRequestCallback() {
                var selectedPartiesEmail = getSelectedParties('email');
                if (selectedPartiesEmail) {
                    var GoogleMail = require('../../../BizLogic/GoogleMail');
                    var FirmCache = require('../../../Cache/FirmCache');
                    var firmCache = new FirmCache();
                    var firm = firmCache.getDefaultFirm();
                    var firmName = firm.getFirmName();
                    var firmPhone = firm.getFirmPhone();
                    var googleMail = new GoogleMail();
                    var header = 'Dear Sir/Madam,\n\n';
                    var subjectToSend = firmName + ' Item Catalogue';
                    var footer = '\n\nThank You,\n' + firmName + ',\n' + firmPhone;
                    var defaultText = '1. Click on the link. ' + catalogueLink + '\n2. Check all items.\n3. Send me the item code/Name to place an order.\n\nCall me if you have any queries.';
                    var messageToSend = header + defaultText + footer;
                    var receiverAddresses = selectedPartiesEmail;
                    googleMail.sendMail({
                        fileNames: null,
                        receiverId: receiverAddresses,
                        alertUser: false,
                        subject: subjectToSend,
                        message: messageToSend,
                        attachFileNames: null,
                        successCallBack: function successCallBack() {
                            callback && typeof callback === 'function' && callback();ToastHelper.success('Email sent successfully');
                        },
                        errorCallBack: null,
                        base64Attachments: []
                    });
                    ;
                }
            };
            processRequestIfUserIsOnline(emailRequestCallback);
        };
    },
    sendSms: function sendSms(catalogueLink, callback) {
        return function (api) {
            gridApi = api;
            var smsRequestCallback = function smsRequestCallback() {
                var MessageDraftLogic = require('../../../BizLogic/MessageDraftLogic');
                var messageDraftLogic = new MessageDraftLogic();
                var partyObjects = getSelectedParties('sms');
                if (partyObjects.length) {
                    messageDraftLogic.sendBulkSMS(partyObjects, { catalogueURL: catalogueLink, bulkMessageType: 'catalogue' });
                    callback && typeof callback === 'function' && callback();
                }
            };
            processRequestIfUserIsOnline(smsRequestCallback);
        };
    },
    sendWhatsapp: function sendWhatsapp(catalogueLink, callback) {
        var whatsAppMsgCall = function whatsAppMsgCall() {
            var send = require('../../../Utilities/whatsapp').default;
            var CommonUtility = require('../../../Utilities/CommonUtility');
            var MessageFormatter = require('../../../BizLogic/TxnMessageFormatter');
            var messageFormatter = new MessageFormatter();
            var catalogueMsg = messageFormatter.createMessageForCatalogue(catalogueLink);
            var whatsAppMessage = {
                message: catalogueMsg.message,
                footer: catalogueMsg.footer
            };
            send(whatsAppMessage, callback);
        };
        processRequestIfUserIsOnline(whatsAppMsgCall);
    }
};

module.exports = CatalogueSharingOption;