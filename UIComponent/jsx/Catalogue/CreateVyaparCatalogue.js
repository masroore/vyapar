Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _PrimaryButton = require('../UIControls/PrimaryButton');

var _PrimaryButton2 = _interopRequireDefault(_PrimaryButton);

var _trashableReact = require('trashable-react');

var _trashableReact2 = _interopRequireDefault(_trashableReact);

var _Grid = require('../Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _FirmCache = require('../../../Cache/FirmCache');

var _FirmCache2 = _interopRequireDefault(_FirmCache);

var _ItemCache = require('../../../Cache/ItemCache.js');

var _ItemCache2 = _interopRequireDefault(_ItemCache);

var _SettingCache = require('../../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

var _DataLoader = require('../../../DBManager/DataLoader.js');

var _DataLoader2 = _interopRequireDefault(_DataLoader);

var _Queries = require('../../../Constants/Queries');

var _Queries2 = _interopRequireDefault(_Queries);

var _ItemCategoryCache = require('../../../Cache/ItemCategoryCache');

var _ItemCategoryCache2 = _interopRequireDefault(_ItemCategoryCache);

var _analyticsHelper = require('../../../Utilities/analyticsHelper');

var _analyticsHelper2 = _interopRequireDefault(_analyticsHelper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $loadingWrapper = void 0;
var styles = function styles(theme) {
    return {
        addNewItemDialog: {
            '& .form-container': {
                maxHeight: '600px !important'
            }
        },
        '@global': {
            '.catalogue-container .ag-theme-material': {
                border: '1px solid #e0e0e0',
                marginBottom: 10
            },
            '.catalogue-container .gridContainer  .quickFilter': {
                justifyContent: 'flex-start !important',
                paddingBottom: 10
            },
            '.catalogue-container .gridContainer .ag-row .ag-cell': {
                fontSize: 14
            },
            '.catalogue-container .gridContainer .quickFilter .gridTitle': {
                padding: 0
            },
            '.catalogue-container .gridContainer .quickFilter .gridQuickFilter div': {
                marginLeft: 0
            },
            '.catalogue-container  .ag-header-container .ag-header-cell': {
                fontSize: 14,
                fontFamily: 'robotoMedium'
            },
            '#loadingText': {
                position: 'absolute',
                top: '55%',
                left: '50%',
                color: 'white',
                transform: 'translate(-50%,-50%)'
            },
            '.blink_me': {
                animation: 'blinker 1.5s linear infinite',
                color: '#0b8fc5'
            },
            '.selected-items-count': {
                backgroundColor: '#F3A33A',
                padding: 10,
                borderRadius: 5,
                color: '#FFFFFF',
                fontSize: 12
            }
        }
    };
};

var CreateVyaparCatalogue = function (_React$Component) {
    (0, _inherits3.default)(CreateVyaparCatalogue, _React$Component);

    function CreateVyaparCatalogue(props) {
        var _this2 = this;

        (0, _classCallCheck3.default)(this, CreateVyaparCatalogue);

        var _this = (0, _possibleConstructorReturn3.default)(this, (CreateVyaparCatalogue.__proto__ || (0, _getPrototypeOf2.default)(CreateVyaparCatalogue)).call(this, props));

        _this.state = {
            allItems: [],
            openNamesModal: false
        };

        _this.updateItems = function () {
            _this.setState({
                allItems: _this.itemCache.getListOfItemsAndServiceObject(false)
            }, function () {
                _this.createDeletedItemIdsList();
                _this.getDeletedItemsFromCatalogue();
            });
        };

        _this.createDeletedItemIdsList = function () {
            _this.deletedItemIdsList = new _map2.default(_this.props.existingCatalogueItemList);
            _this.state.allItems.forEach(function (existingItems) {
                var itemId = Number(existingItems.getItemId());
                if (_this.deletedItemIdsList.has(itemId)) {
                    _this.deletedItemIdsList.delete(itemId);
                }
            });
        };

        _this.getDeletedItemsFromCatalogue = function () {
            if (_this.deletedItemIdsList && _this.deletedItemIdsList.size > 0) {
                var _CatalogueItemLogic = require('../../../BizLogic/CatalogueItemLogic');
                var catalogueItemLogic = new _CatalogueItemLogic();
                var deletedItems = catalogueItemLogic.getCatalogueItemsList({ itemIds: [].concat((0, _toConsumableArray3.default)(_this.deletedItemIdsList.keys())).join(',') }, false);
                var ItemLogic = require('../../../BizLogic/ItemLogic.js');
                _this.deletedItemsList = [];
                deletedItems.forEach(function (item) {
                    var deletedItem = new ItemLogic();
                    deletedItem.setItemName(item.getCatalogueItemName());
                    deletedItem.setItemId(item.getItemId());
                    deletedItem.setItemDescription(item.getCatalogueItemDescription());
                    deletedItem.setItemCode(item.getCatalogueItemCode());
                    _this.deletedItemsList.push(deletedItem);
                });
            }
        };

        _this.isItemExistsInCatalogue = function (itemId) {
            return _this.props.existingCatalogueItemList && _this.props.existingCatalogueItemList.has(Number(itemId));
        };

        _this.getColumnDefs = function () {
            var columnDefs = [{
                headerName: '',
                field: '',
                headerCheckboxSelection: true,
                headerCheckboxSelectionFilteredOnly: false,
                checkboxSelection: true,
                cellRenderer: function cellRenderer(params) {
                    if (_this.isItemExistsInCatalogue(params.data.getItemId())) {
                        params.node.setSelected(true);
                    }
                },
                width: 40
            }, {
                field: 'itemName',
                headerName: 'ITEM',
                width: 150,
                cellRenderer: function cellRenderer(params) {
                    return params.data.getItemName();
                },
                checkboxSelection: false
            }, {
                field: 'itemDescription',
                headerName: 'Description',
                width: 150,
                cellRenderer: function cellRenderer(params) {
                    return params.data.getItemDescription();
                },
                checkboxSelection: false
            }, {
                field: 'itemCode',
                headerName: 'Item Code',
                width: 150,
                cellRenderer: function cellRenderer(params) {
                    return params.data.getItemCode();
                },
                checkboxSelection: false
            }, {
                field: 'category',
                headerName: 'Category',
                width: 150,
                cellRenderer: function cellRenderer(params) {
                    var category = _this.itemCategoryCache.getItemCategoryObjectById(params.data.getItemCategoryId());
                    return category ? category.getCategoryName() : '';
                },
                checkboxSelection: false
            }, {
                field: 'itemSaleUnitPrice',
                headerName: 'Sale Price',
                width: 150,
                cellRenderer: function cellRenderer(params) {
                    return params.data.getItemSaleUnitPrice();
                },
                checkboxSelection: false
            }];
            return columnDefs;
        };

        _this.getApi = function (params) {
            _this.api = params.api;
            clearTimeout(_this.loadDeletedItems);
            _this.loadDeletedItems = setTimeout(function () {
                if (!_this.isDeletedItemsAlreadAdded && _this.api && _this.deletedItemsList && _this.deletedItemsList.length > 0) {
                    _this.api.addItems(_this.deletedItemsList);
                    _this.isDeletedItemsAlreadAdded = true;
                }
            });
        };

        _this.onRowSelected = function (data) {
            var isSelected = data.node.isSelected();
            var itemData = data.node.data;
            var itemId = itemData.getItemId();
            var isItemInCatalogue = _this.isItemExistsInCatalogue(Number(itemId));
            if (isSelected && !isItemInCatalogue) {
                _this.newCatalogueItems.push(itemData);
            } else if (isSelected && isItemInCatalogue) {
                _this.deleteCatalogueItemsList.splice(_this.deleteCatalogueItemsList.indexOf(itemId), 1);
            } else if (!isSelected && isItemInCatalogue) {
                _this.deleteCatalogueItemsList.push(itemId);
            } else if (!isSelected && !isItemInCatalogue) {
                _this.newCatalogueItems.splice(_this.newCatalogueItems.indexOf(itemData), 1);
            }
        };

        _this.renderItemsGrid = function () {
            var columnDefs = _this.getColumnDefs();
            var gridOptions = {
                enableFilter: false,
                enableSorting: false,
                columnDefs: columnDefs,
                rowData: _this.state.allItems,
                cellClass: '',
                rowHeight: 40,
                headerHeight: 40,
                rowClass: 'customVerticalBorder',
                overlayNoRowsTemplate: 'No Items to show',
                suppressAutoSize: true,
                suppressMovableColumns: true,
                suppressContextMenu: true,
                getRowNodeId: function getRowNodeId(data) {
                    return data.getItemId();
                },
                suppressRowClickSelection: true,
                rowSelection: 'multiple',
                onRowSelected: _this.onRowSelected,
                suppressCellSelection: true
            };
            return _react2.default.createElement(_Grid2.default, {
                height: '100%',
                width: '100%',
                gridOptions: gridOptions,
                getApi: _this.getApi,
                quickFilter: true,
                classes: 'alternateRowColor customVerticalBorder gridRowHeight40',
                notSelectFirstRow: true,
                suppressContextMenuRowSelection: true,
                toggleSelect: function toggleSelect(data) {}
            });
        };

        _this.stopProcessing = function () {
            $loadingWrapper.find('.loadingText').html('');
            $loadingWrapper.hide();
        };

        _this.prepareCatalogueCompanyDetails = function () {
            var FirmCache = require('../../../Cache/FirmCache.js');
            var firmCache = new FirmCache();
            var firm = firmCache.getFirmById(1);
            return {
                firmName: firm.getFirmName(),
                firmPhone: firm.getFirmPhone(),
                firmEmail: firm.getFirmEmail(),
                firmGstinNumber: firm.getFirmHSNSAC(),
                firmAddress: firm.getFirmAddress(),
                firmLogo: firm.getFirmImagePath(),
                currencySymbol: _this.settingCache.getCurrencySymbol()
            };
        };

        _this.prepareCatalogueAddItemList = function () {
            var selectedItemList = _this.newCatalogueItems;
            var itemList = [];
            if (selectedItemList.length > 0) {
                var ItemImages = require('../../../BizLogic/ItemImages');
                var itemImages = new ItemImages();
                selectedItemList.forEach(function (itemData) {
                    var itemId = itemData.getItemId();
                    var itemImageList = itemImages.getItemImageListByItemId(itemId, false, true);
                    var category = _this.itemCategoryCache.getItemCategoryObjectById(itemData.getItemCategoryId());
                    itemList.push({
                        itemId: itemId,
                        itemName: itemData.getItemName(),
                        itemCode: itemData.getItemCode(),
                        itemDescription: itemData.getItemDescription(),
                        itemSaleUnitPrice: itemData.getItemSaleUnitPrice(),
                        itemCategoryName: category.getCategoryName(),
                        itemImages: itemImageList || [],
                        itemType: itemData.getItemType()
                    });
                });
            }
            return itemList;
        };

        _this.saveDataToCatalogueTable = function (items) {
            if (items.length > 0) {
                var status = ErrorCode.SUCCESS;
                try {
                    var _CatalogueItemLogic2 = require('../../../BizLogic/CatalogueItemLogic');
                    var catalogueItemLogic = new _CatalogueItemLogic2();
                    status = catalogueItemLogic.saveCatalogueItems(items);
                } catch (ex) {
                    status = ErrorCode.ERROR;
                }
                if (status !== ErrorCode.SUCCESS) {
                    ToastHelper.error('Some error occurred while saving your catalogue. Please contact to Vyapar team for help.');
                }
            }
        };

        _this.deleteDataFromCatalogueTable = function (items) {
            if (items.length > 0) {
                var status = ErrorCode.SUCCESS;
                try {
                    var _CatalogueItemLogic3 = require('../../../BizLogic/CatalogueItemLogic');
                    var catalogueItemLogic = new _CatalogueItemLogic3();
                    status = catalogueItemLogic.deleteCatalogueItems(items);
                } catch (ex) {
                    status = ErrorCode.ERROR;
                }
                if (status !== ErrorCode.SUCCESS) {
                    ToastHelper.error('Some error occurred while saving your catalogue. Please contact to Vyapar team for help.');
                }
            }
        };

        _this.loggedInUserIfTokenIsNotAvailable = function (callback) {
            var AuthHelper = require('../../../Utilities/TokenForSync');
            if (!AuthHelper.ifExistToken()) {
                ToastHelper.info('Please login vyapar before creating the catalogue.');
                var SyncHelper = require('../../../Utilities/SyncHelper');
                SyncHelper.loginToGetAuthToken(callback);
                return false;
            }
            return true;
        };

        _this.saveCatalogue = function () {
            var saveUserCatalogueCatalogue = function saveUserCatalogueCatalogue() {
                try {
                    $loadingWrapper.find('.loadingText').html('Please wait while we are creating your catalogue...');
                    $loadingWrapper.show((0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
                        var companyDetails, items, dt, VyaparAPIHelper, response, catalogueId, catalogueUID;
                        return _regenerator2.default.wrap(function _callee$(_context) {
                            while (1) {
                                switch (_context.prev = _context.next) {
                                    case 0:
                                        companyDetails = _this.prepareCatalogueCompanyDetails();
                                        items = _this.prepareCatalogueAddItemList();

                                        if (!(items.length > 0)) {
                                            _context.next = 19;
                                            break;
                                        }

                                        dt = void 0;
                                        _context.prev = 4;

                                        dt = (0, _stringify2.default)({
                                            companyDetails: companyDetails,
                                            items: items
                                        });
                                        _context.next = 12;
                                        break;

                                    case 8:
                                        _context.prev = 8;
                                        _context.t0 = _context['catch'](4);

                                        ToastHelper.error('All items can not be sent together. Please remove some items and then try again.');
                                        return _context.abrupt('return');

                                    case 12:
                                        VyaparAPIHelper = require('../../../Utilities/VyaparAPIHelper');
                                        _context.next = 15;
                                        return VyaparAPIHelper.fetch({
                                            url: '/api/catalogue/create', data: dt, method: 'POST',
                                            headers: {
                                                'Content-Type': 'application/json'
                                            }
                                        });

                                    case 15:
                                        response = _context.sent;

                                        if (response && (response.code === 200 || response.code === 100)) {
                                            catalogueId = response.catalogueId, catalogueUID = response.catalogueUID;

                                            _analyticsHelper2.default.pushEvent('Catalogue Created', {
                                                Count: items.length
                                            });
                                            _this.saveCatalogueSettingDetails(catalogueId, catalogueUID);
                                            if (response.failedItems.length > 0) {
                                                $loadingWrapper.find('.loadingText').html('Its almost done. Some of the items get failed. Please try again saving failed items.');
                                                items = items.filter(function (item) {
                                                    return !response.failedItems.includes(item.itemId);
                                                });
                                            }
                                            _this.saveDataToCatalogueTable(items);
                                        } else {
                                            ToastHelper.error('Some error occurred. Please try after some time.');
                                        }
                                        _context.next = 20;
                                        break;

                                    case 19:
                                        ToastHelper.error('Please select some items to create your catalogue.');

                                    case 20:
                                        _this.props.onCatalogueGenerationSuccess && _this.props.onCatalogueGenerationSuccess();
                                        _this.stopProcessing();

                                    case 22:
                                    case 'end':
                                        return _context.stop();
                                }
                            }
                        }, _callee, _this2, [[4, 8]]);
                    })));
                } catch (ex) {
                    _this.stopProcessing();
                }
            };
            var isOnline = require('is-online');
            isOnline().then(function (online) {
                if (!online) {
                    ToastHelper.error('Please check internet connection.');
                    return false;
                } else if (_this.loggedInUserIfTokenIsNotAvailable(saveUserCatalogueCatalogue)) {
                    saveUserCatalogueCatalogue();
                }
            });
        };

        _this.editCatalogue = function () {
            var saveUserCatalogueCatalogue = function saveUserCatalogueCatalogue() {
                try {
                    $loadingWrapper.find('.loadingText').html('Please wait while we are creating/editing your catalogue...');
                    $loadingWrapper.show((0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2() {
                        var items, deletedItemIds, dt, VyaparAPIHelper, editResponse;
                        return _regenerator2.default.wrap(function _callee2$(_context2) {
                            while (1) {
                                switch (_context2.prev = _context2.next) {
                                    case 0:
                                        items = _this.prepareCatalogueAddItemList();
                                        deletedItemIds = _this.deleteCatalogueItemsList;

                                        if (!(items.length > 0 || deletedItemIds.length > 0)) {
                                            _context2.next = 18;
                                            break;
                                        }

                                        dt = void 0;
                                        _context2.prev = 4;

                                        dt = (0, _stringify2.default)({
                                            catalogueId: _this.props.catalogueId,
                                            items: items,
                                            deletedItemIds: deletedItemIds
                                        });
                                        //console.log(dt);
                                        _context2.next = 13;
                                        break;

                                    case 8:
                                        _context2.prev = 8;
                                        _context2.t0 = _context2['catch'](4);

                                        ToastHelper.error('All items can not be sent together. Please remove some items and then try again.');
                                        _this.stopProcessing();
                                        return _context2.abrupt('return');

                                    case 13:
                                        VyaparAPIHelper = require('../../../Utilities/VyaparAPIHelper');
                                        _context2.next = 16;
                                        return VyaparAPIHelper.fetch({
                                            url: '/api/catalogue/update', data: dt, method: 'POST',
                                            headers: {
                                                'Content-Type': 'application/json'
                                            }
                                        });

                                    case 16:
                                        editResponse = _context2.sent;

                                        if (editResponse && (editResponse.code === 200 || editResponse.code === 100)) {
                                            $loadingWrapper.find('.loadingText').html('Its almost done. We are saving your changes.');
                                            _analyticsHelper2.default.pushEvent('Catalogue Edited', {
                                                count: items.length
                                            });
                                            if (editResponse.failedItems.length > 0) {
                                                $loadingWrapper.find('.loadingText').html('Its almost done. Some of the items get failed. Please try again saving failed items.');
                                                items = items.filter(function (item) {
                                                    return !editResponse.failedItems.includes(item.itemId);
                                                });
                                                deletedItemIds = deletedItemIds.filter(function (id) {
                                                    return !editResponse.failedItems.includes(id);
                                                });
                                            }
                                            _this.saveDataToCatalogueTable(items);
                                            _this.deleteDataFromCatalogueTable(deletedItemIds);
                                        } else {
                                            ToastHelper.error('Some error occurred. Please try after some time.');
                                        }

                                    case 18:
                                        _this.props.onCatalogueGenerationSuccess && _this.props.onCatalogueGenerationSuccess();
                                        _this.stopProcessing();

                                    case 20:
                                    case 'end':
                                        return _context2.stop();
                                }
                            }
                        }, _callee2, _this2, [[4, 8]]);
                    })));
                } catch (ex) {
                    _this.stopProcessing();
                }
            };
            var isOnline = require('is-online');
            isOnline().then(function (online) {
                if (!online) {
                    ToastHelper.error('Please check internet connection.');
                    return false;
                } else if (_this.loggedInUserIfTokenIsNotAvailable(saveUserCatalogueCatalogue)) {
                    saveUserCatalogueCatalogue();
                }
            });
        };

        _this.saveCatalogueSettingDetails = function (catalogueId, catalogueUID) {
            var SettingsModel = require('../../../Models/SettingsModel');
            var settingModel = new SettingsModel();
            settingModel.setSettingKey(_Queries2.default.SETTING_CATALOGUE_ID);
            settingModel.UpdateSetting(catalogueId);

            settingModel.setSettingKey(_Queries2.default.SETTING_CATALOGUE_UID);
            settingModel.UpdateSetting(catalogueUID);
        };

        _this.deleteCatalogueItemRecords = function () {
            var itemIds = [];
            itemIds.forEach(function (itemId) {
                var catalogueLogic = new CatalogueItemLogic();
                catalogueLogic.setItemId(itemId);
                catalogueLogic.deleteCatalogueItem();
            });
        };

        _this.returnToView = function () {
            _this.props.returnToView && _this.props.returnToView();
        };

        _this.itemCache = new _ItemCache2.default();
        _this.firmCache = new _FirmCache2.default();
        _this.dataLoader = new _DataLoader2.default();
        _this.settingCache = new _SettingCache2.default();
        _this.newCatalogueItems = [];
        _this.deleteCatalogueItemsList = [];
        _this.itemCategoryCache = new _ItemCategoryCache2.default();
        return _this;
    }

    (0, _createClass3.default)(CreateVyaparCatalogue, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            window.onResume = this.onResume.bind(this);
            $loadingWrapper = $('.loaderWrapper');
            this.updateItems();
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            delete window.onResume;
        }
    }, {
        key: 'onResume',
        value: function onResume() {
            this.updateItems();
        }
    }, {
        key: 'render',
        value: function render() {
            var classes = this.props.classes;

            return _react2.default.createElement(
                _react2.default.Fragment,
                null,
                _react2.default.createElement(
                    'div',
                    { className: 'catalogue-container d-flex flex-column flex-container gridContainer', style: { padding: 10 } },
                    _react2.default.createElement(
                        'div',
                        { className: 'bottomBorderLight d-flex align-items-center justify-content-between', style: { height: 80 } },
                        _react2.default.createElement(
                            'p',
                            null,
                            'Select the Items you want to show in your Catalogue. Only the selected items will be present in Online Catalogue.'
                        )
                    ),
                    this.state.enableAddNewItemDialog && _react2.default.createElement('div', { className: classes.addNewItemDialog, id: 'addNewItemDialog' }),
                    this.renderItemsGrid(),
                    _react2.default.createElement(
                        'div',
                        { className: 'd-flex justify-content-end' },
                        _react2.default.createElement(
                            _PrimaryButton2.default,
                            { onClick: this.returnToView, style: { backgroundColor: 'white', color: '#1789FC', marginRight: 4 } },
                            'Back'
                        ),
                        _react2.default.createElement(
                            _PrimaryButton2.default,
                            { onClick: !this.props.catalogueId ? this.saveCatalogue : this.editCatalogue },
                            'Save Catalogue'
                        )
                    )
                )
            );
        }
    }]);
    return CreateVyaparCatalogue;
}(_react2.default.Component);

exports.default = (0, _styles.withStyles)(styles)((0, _trashableReact2.default)(CreateVyaparCatalogue));