Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.TabContainer = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _assign = require('babel-runtime/core-js/object/assign');

var _assign2 = _interopRequireDefault(_assign);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _trashableReact = require('trashable-react');

var _trashableReact2 = _interopRequireDefault(_trashableReact);

var _ItemImages = require('../../../BizLogic/ItemImages');

var _ItemImages2 = _interopRequireDefault(_ItemImages);

var _CustomCarousel = require('../UIControls/CustomCarousel');

var _CustomCarousel2 = _interopRequireDefault(_CustomCarousel);

var _Typography = require('@material-ui/core/Typography');

var _Typography2 = _interopRequireDefault(_Typography);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _TextField = require('@material-ui/core/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _ItemCategoryCache = require('../../../Cache/ItemCategoryCache');

var _ItemCategoryCache2 = _interopRequireDefault(_ItemCategoryCache);

var _MenuItem = require('@material-ui/core/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _SettingCache = require('../../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TabContainer = exports.TabContainer = function TabContainer(props) {
    return _react2.default.createElement(
        _Typography2.default,
        { component: 'div', className: props.className, style: { padding: 10 } },
        props.children
    );
};

var styles = function styles(theme) {
    return {
        carouselItem: { padding: 8, width: '100%', height: '100%', border: '1px solid #dddddd' },
        selectedImage: { border: '1px solid grey' },
        '@global': {
            '.ReactModalPortal .catalogueDetailView.ReactModal__Content .modalContainer': {
                height: '100%'
            },
            '.item-details-container': {
                width: '700px',
                height: '600px',
                padding: 20
            },
            '.item-details-container .padding-15': {
                padding: '15px 0'
            },
            '.layout-container': {
                backgroundColor: '#FFFFFF',
                border: '1px solid #dddddd',
                boxShadow: '1px 1px 1px #dddddd'
            },
            '#categoryDd.MuiSelect-select': {
                paddingTop: 10,
                paddingBottom: 10
            },
            '.itemCodeTxtBox': {
                marginBottom: '15px !important'
            }, '.item-name-text': {
                textOverflow: 'ellipsis',
                overflow: 'hidden',
                whiteSpace: 'nowrap'
            }
        }
    };
};

var CatalogueItemDetails = function (_React$Component) {
    (0, _inherits3.default)(CatalogueItemDetails, _React$Component);

    function CatalogueItemDetails(props) {
        (0, _classCallCheck3.default)(this, CatalogueItemDetails);

        var _this = (0, _possibleConstructorReturn3.default)(this, (CatalogueItemDetails.__proto__ || (0, _getPrototypeOf2.default)(CatalogueItemDetails)).call(this, props));

        _initialiseProps.call(_this);

        var itemCategoryCache = new _ItemCategoryCache2.default();
        _this.listOfCategories = itemCategoryCache.getItemCategoriesArray();
        _this.settingCache = new _SettingCache2.default();
        var itemDetails = _this.getItemDetails();
        _this.imageList = [];
        _this.state = {
            isItemEditable: false,
            selectedImage: '',
            itemDetails: itemDetails
        };
        return _this;
    }

    (0, _createClass3.default)(CatalogueItemDetails, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var itemImages = new _ItemImages2.default();
            this.imageList = itemImages.getItemImagesObjectListByCatalogueItemId(this.props.item.getCatalogueItemId());
            this.setState({
                selectedImage: this.imageList.length > 0 ? this.imageList[0] : ''
            });
        }
    }, {
        key: 'renderThemeCarousel',
        value: function renderThemeCarousel(imageList, clickCallback) {
            var _this2 = this;

            var selectedImage = this.state.selectedImage;
            var classes = this.props.classes;

            var carouselItems = [];
            imageList.map(function (image, i) {
                return carouselItems.push(_react2.default.createElement(
                    'div',
                    { key: i,
                        onClick: clickCallback.bind(_this2, image) },
                    _react2.default.createElement('img', { className: (0, _classnames2.default)(classes.carouselItem, selectedImage.id === image.id ? classes.selectedImage : ''), key: i, src: image.image })
                ));
            });
            return _react2.default.createElement(
                TabContainer,
                { className: 'layout-container' },
                _react2.default.createElement(
                    _CustomCarousel2.default,
                    { activeItemIndex: this.activeItemIndex },
                    carouselItems
                )
            );
        }
    }, {
        key: 'render',
        value: function render() {
            var _this3 = this;

            var _state = this.state,
                selectedImage = _state.selectedImage,
                isItemEditable = _state.isItemEditable,
                itemDetails = _state.itemDetails;

            var categoryObject = this.listOfCategories.filter(function (category) {
                return category.categoryName.toLowerCase().trim() === itemDetails.itemCategoryName.toLowerCase().trim();
            })[0];
            if (!categoryObject) {
                categoryObject = { categoryName: itemDetails.itemCategoryName, categoryId: 0 };
                this.listOfCategories.push(categoryObject);
            }
            return _react2.default.createElement(
                'div',
                { className: 'd-flex item-details-container h-100-per flex-column' },
                _react2.default.createElement(
                    'h3',
                    { className: 'mb-10' },
                    'View/Edit Catalogue Item'
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'd-flex form-wrapper padBottom10' },
                    _react2.default.createElement(
                        'div',
                        { className: 'left-col width50 padright20' },
                        _react2.default.createElement(
                            'div',
                            { className: 'd-flex justify-content-center layout-container', style: { padding: 10, marginBottom: 10 } },
                            selectedImage && _react2.default.createElement('img', { style: { width: 200, height: 200 }, src: selectedImage.image }),
                            !selectedImage && _react2.default.createElement('img', { style: { width: 200, height: 200 }, src: './../inlineSVG/add_photo.svg' })
                        ),
                        this.imageList.length > 1 && this.renderThemeCarousel(this.imageList, function (image) {
                            _this3.setState({ selectedImage: image });
                        })
                    ),
                    _react2.default.createElement(
                        'div',
                        { className: 'right-col width50 font-family-medium', style: { overflowX: 'auto' } },
                        _react2.default.createElement(
                            'div',
                            { className: 'font-20 bottomBorderLight padding-15' },
                            _react2.default.createElement(
                                'div',
                                { className: 'd-flex align-items-stretch justify-content-between' },
                                _react2.default.createElement(
                                    'div',
                                    { className: 'padBottom10', style: { width: !isItemEditable ? '65%' : '100%' } },
                                    !isItemEditable && _react2.default.createElement(
                                        'p',
                                        { className: 'item-name-text' },
                                        itemDetails.itemName
                                    ),
                                    isItemEditable && _react2.default.createElement(_TextField2.default, {
                                        label: 'Item Name',
                                        margin: 'dense',
                                        variant: 'outlined',
                                        className: 'width100',
                                        value: itemDetails.itemName,
                                        inputProps: { maxLength: 256 },
                                        onChange: this.updateInputChange('itemName')
                                    })
                                ),
                                !isItemEditable && _react2.default.createElement(
                                    'div',
                                    null,
                                    ' ',
                                    _react2.default.createElement(
                                        _Button2.default,
                                        {
                                            variant: 'contained',
                                            color: 'primary',
                                            onClick: this.toggleEditItem },
                                        'Edit Item'
                                    )
                                )
                            ),
                            _react2.default.createElement(
                                'div',
                                { className: 'padBottom10' },
                                !isItemEditable && _react2.default.createElement(
                                    'p',
                                    null,
                                    itemDetails.itemSaleUnitPrice
                                ),
                                isItemEditable && _react2.default.createElement(_TextField2.default, {
                                    label: 'Price',
                                    margin: 'dense',
                                    variant: 'outlined',
                                    className: 'width100',
                                    value: itemDetails.itemSaleUnitPrice,
                                    onChange: this.updateInputChange('itemSaleUnitPrice'),
                                    onInput: function onInput(e) {
                                        return DecimalInputFilter.inputTextFilterForAmount(e.target, false);
                                    }
                                })
                            )
                        ),
                        _react2.default.createElement(
                            'div',
                            { className: 'font-14 bottomBorderLight padding-15' },
                            !isItemEditable && _react2.default.createElement(
                                'p',
                                { className: 'padBottom10' },
                                _react2.default.createElement(
                                    'span',
                                    null,
                                    'ITEM CODE :'
                                ),
                                _react2.default.createElement(
                                    'span',
                                    { className: 'font-family-regular color-80808B' },
                                    itemDetails.itemCode && _react2.default.createElement(
                                        'span',
                                        null,
                                        itemDetails.itemCode
                                    )
                                )
                            ),
                            isItemEditable && _react2.default.createElement(_TextField2.default, {
                                label: 'Item Code',
                                margin: 'dense',
                                variant: 'outlined',
                                className: (0, _classnames2.default)('width100', 'itemCodeTxtBox'),
                                value: itemDetails.itemCode,
                                inputProps: { maxLength: 32 },
                                onChange: this.updateInputChange('itemCode')
                            }),
                            !isItemEditable && _react2.default.createElement(
                                'p',
                                null,
                                _react2.default.createElement(
                                    'span',
                                    null,
                                    'CATEGORY :'
                                ),
                                _react2.default.createElement(
                                    'span',
                                    { className: 'font-family-regular color-80808B' },
                                    _react2.default.createElement(
                                        'span',
                                        null,
                                        categoryObject.categoryName
                                    )
                                )
                            ),
                            isItemEditable && this.listOfCategories.length > 1 && this.settingCache.isItemCategoryEnabled() && _react2.default.createElement(
                                _react2.default.Fragment,
                                null,
                                _react2.default.createElement(
                                    _TextField2.default,
                                    {
                                        id: 'categoryDd',
                                        select: true,
                                        label: 'Category',
                                        value: categoryObject.categoryId,
                                        onChange: this.updateInputChange('categoryId'),
                                        variant: 'outlined'
                                    },
                                    this.listOfCategories.map(function (value, index) {
                                        return _react2.default.createElement(
                                            _MenuItem2.default,
                                            { key: value.categoryId, value: value.categoryId },
                                            value.categoryName
                                        );
                                    })
                                )
                            )
                        ),
                        _react2.default.createElement(
                            'div',
                            { className: 'font-14 padding-15' },
                            !isItemEditable && _react2.default.createElement(
                                _react2.default.Fragment,
                                null,
                                ' ',
                                _react2.default.createElement(
                                    'p',
                                    { className: 'padBottom10' },
                                    'DESCRIPTION'
                                ),
                                _react2.default.createElement(
                                    'p',
                                    { className: 'font-family-regular color-80808B' },
                                    _react2.default.createElement(
                                        'span',
                                        null,
                                        itemDetails.itemDescription
                                    )
                                )
                            ),
                            isItemEditable && _react2.default.createElement(_TextField2.default, {
                                label: 'Description',
                                margin: 'dense',
                                variant: 'outlined',
                                className: 'width100',
                                value: itemDetails.itemDescription,
                                inputProps: { maxLength: 1024 },
                                onChange: this.updateInputChange('itemDescription')
                            })
                        )
                    )
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'utility-btn-grp justify-content-end d-flex' },
                    isItemEditable && _react2.default.createElement(
                        _react2.default.Fragment,
                        null,
                        _react2.default.createElement(
                            'div',
                            { className: 'margin-r10' },
                            _react2.default.createElement(
                                _Button2.default,
                                {
                                    variant: 'contained'
                                    //classes={{ root: classes.imageControlBtns }}
                                    , onClick: this.toggleEditItem
                                },
                                'Cancel'
                            )
                        ),
                        _react2.default.createElement(
                            _Button2.default,
                            {
                                variant: 'contained',
                                color: 'primary',
                                onClick: this.updateServerCatalogueItem
                            },
                            'Update Catalogue Item'
                        )
                    ),
                    !isItemEditable && _react2.default.createElement(
                        _Button2.default,
                        {
                            variant: 'contained',
                            color: 'primary',
                            onClick: this.close
                        },
                        'Close'
                    )
                )
            );
        }
    }]);
    return CatalogueItemDetails;
}(_react2.default.Component);

var _initialiseProps = function _initialiseProps() {
    var _this4 = this;

    this.getItemDetails = function () {
        if (_this4.itemNewState) {
            return _this4.itemNewState;
        } else {
            var item = _this4.props.item;

            return {
                itemName: item.getCatalogueItemName(),
                itemDescription: item.getCatalogueItemDescription(),
                itemCode: item.getCatalogueItemCode(),
                itemSaleUnitPrice: item.getCatalogueItemSaleUnitPrice(),
                itemCategoryName: item.getCatalogueItemCategoryName(),
                itemId: item.getItemId(),
                catalogueItemId: item.getCatalogueItemId()
            };
        }
    };

    this.toggleEditItem = function () {
        var stateObj = {
            isItemEditable: !_this4.state.isItemEditable
        };
        if (_this4.state.isItemEditable) {
            stateObj.itemDetails = (0, _assign2.default)({}, _this4.getItemDetails());
        }
        _this4.setState((0, _extends3.default)({}, stateObj));
    };

    this.updateInputChange = function (key) {
        return function (event) {
            var itemDetails = _this4.state.itemDetails;

            var value = event.target.value;
            if (key.toLowerCase() === 'categoryid') {
                var selectedCategory = _this4.listOfCategories.find(function (category) {
                    if (Number(category.categoryId) === Number(value)) return category;
                });
                itemDetails['itemCategoryName'] = selectedCategory.categoryName;
            } else {
                itemDetails[key] = event.target.value;
            }

            _this4.setState({
                itemDetails: itemDetails
            });
        };
    };

    this.updateCatalogueItemRecord = function () {
        var itemDetails = _this4.state.itemDetails;

        var status = ErrorCode.SUCCESS;
        try {
            var CatalogueItemLogic = require('../../../BizLogic/CatalogueItemLogic');
            var catalogueItemLogic = new CatalogueItemLogic();
            status = catalogueItemLogic.saveCatalogueItems([itemDetails]);
        } catch (ex) {
            status = ErrorCode.ERROR;
        }
        if (status !== ErrorCode.SUCCESS) {
            ToastHelper.error('Some error occurred while saving your catalogue. Please contact to Vyapar team for help.');
        }
    };

    this.updateServerCatalogueItem = function () {
        var updateServerCatalogueCallback = function () {
            var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
                var VyaparAPIHelper, existingItemCode, existingItemName, CatalogueItemLogic, catalogueItemLogic, isCurrentItemName, isCurrentItemCode, response;
                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                VyaparAPIHelper = require('../../../Utilities/VyaparAPIHelper');

                                _this4.itemNewState = _this4.state.itemDetails;
                                existingItemCode = _this4.props.item.getCatalogueItemCode();
                                existingItemName = _this4.props.item.getCatalogueItemName();
                                CatalogueItemLogic = require('../../../BizLogic/CatalogueItemLogic');
                                catalogueItemLogic = new CatalogueItemLogic();
                                isCurrentItemName = _this4.itemNewState.itemName && existingItemName ? _this4.itemNewState.itemName.toLowerCase().trim() === existingItemName.toLowerCase().trim() : false;
                                isCurrentItemCode = _this4.itemNewState.itemCode && existingItemCode ? _this4.itemNewState.itemCode.toLowerCase().trim() === existingItemCode.toLowerCase().trim() : false;

                                if (_this4.itemNewState.itemName) {
                                    _context.next = 11;
                                    break;
                                }

                                ToastHelper.error('Item Name can not be empty.');
                                return _context.abrupt('return');

                            case 11:
                                if (!(_this4.itemNewState.itemName && !isCurrentItemName && catalogueItemLogic.isCatalogueItemExists(_this4.itemNewState.itemName, 'name'))) {
                                    _context.next = 14;
                                    break;
                                }

                                ToastHelper.error('Item Name already exists and can not be same.');
                                return _context.abrupt('return');

                            case 14:
                                if (!(_this4.itemNewState.itemCode && !isCurrentItemCode && catalogueItemLogic.isCatalogueItemExists(_this4.itemNewState.itemCode, 'code'))) {
                                    _context.next = 17;
                                    break;
                                }

                                ToastHelper.error('Item Code already exists and can not be same.');
                                return _context.abrupt('return');

                            case 17:
                                _context.next = 19;
                                return VyaparAPIHelper.fetch({
                                    url: '/api/catalogue/update', data: (0, _stringify2.default)({
                                        catalogueId: _this4.props.catalogueId,
                                        items: [(0, _extends3.default)({}, _this4.itemNewState)]
                                    }), method: 'POST',
                                    headers: {
                                        'Content-Type': 'application/json'
                                    }
                                });

                            case 19:
                                response = _context.sent;

                                if (response && response.code === 200) {
                                    _this4.updateCatalogueItemRecord();
                                    ToastHelper.success('Catalogue record updated successfully.');
                                } else {
                                    ToastHelper.error('Some error occurred. Please try after some time.');
                                }
                                _this4.close();

                            case 22:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, _this4);
            }));

            return function updateServerCatalogueCallback() {
                return _ref.apply(this, arguments);
            };
        }();

        var isOnline = require('is-online');
        isOnline().then(function (online) {
            if (!online) {
                ToastHelper.error('Please check internet connection.');
                return false;
            } else {
                updateServerCatalogueCallback();
            }
        });
    };

    this.close = function () {
        _this4.props.close && _this4.props.close(_this4.itemNewState);
    };
};

exports.default = (0, _styles.withStyles)(styles)((0, _trashableReact2.default)(CatalogueItemDetails));