Object.defineProperty(exports, "__esModule", {
    value: true
});

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _trashableReact = require('trashable-react');

var _trashableReact2 = _interopRequireDefault(_trashableReact);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _CreateVyaparCatalogue = require('./CreateVyaparCatalogue');

var _CreateVyaparCatalogue2 = _interopRequireDefault(_CreateVyaparCatalogue);

var _SettingCache = require('../../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

var _CatalogueItemDetails = require('./CatalogueItemDetails');

var _CatalogueItemDetails2 = _interopRequireDefault(_CatalogueItemDetails);

var _Dialog = require('@material-ui/core/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _CatalogueItemLogic = require('../../../BizLogic/CatalogueItemLogic');

var _CatalogueItemLogic2 = _interopRequireDefault(_CatalogueItemLogic);

var _TextField = require('@material-ui/core/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _MenuItem = require('@material-ui/core/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _analyticsHelper = require('../../../Utilities/analyticsHelper');

var _analyticsHelper2 = _interopRequireDefault(_analyticsHelper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
    return {
        '@global': {
            '.catalogue-header': {
                minHeight: 80, maxHeight: 100, marginBottom: 5, padding: '10px 0px 10px 12px'
            },
            '.catalogue-header p': {
                paddingBottom: 3
            },
            '.catalogue-header p span': {
                paddingRight: 5
            },
            '.catalogue-header p img': {
                marginRight: 5
            },
            '.catalogue-header p img.gmail': {
                width: 14,
                height: 14
            },
            '.catalogue-header p img.phone-call': {
                width: 12,
                height: 12
            },
            '.catalogue-cards': {
                display: 'flex',
                flexWrap: 'wrap',
                listStyle: 'none',
                margin: 0,
                padding: 0,
                overflow: 'auto '
            },
            '.catalogue-item': {
                display: 'flex',
                padding: '0.5rem',
                height: 300
            },
            '.card': {
                backgroundColor: 'white',
                borderRadius: '0.25rem',
                boxShadow: '0 20px 40px -14px rgba(0,0,0,0.25)',
                display: 'flex',
                flexDirection: 'column',
                overflow: 'hidden'
            },
            '.catalogue-content': {
                display: 'flex',
                flex: '1 1 auto',
                flexDirection: 'column',
                padding: '1rem'
            },
            '.catalogue-item-text': {
                flex: '1 1 auto',
                lineHeight: 1.5,
                marginBottom: '0.5rem',
                width: 164,
                textOverflow: 'ellipsis',
                overflow: 'hidden',
                whiteSpace: 'nowrap'
            },
            '.itemMenuSharing.catalogue-item-sharing': {
                top: '-120px'
            },
            '.catalogue-company-logo': {
                width: 60,
                height: 60,
                borderRadius: '50%'
            },
            '.sample-catalogue .catalogue-company-logo': {
                border: '1px solid #d8d8d8'
            },
            '.catalogue-links a': {
                color: '#86c1fd',
                cursor: 'pointer',
                display: 'flex',
                alignItems: 'center'
            },
            '.catalogue-links a img': {
                paddingRight: 5,
                width: 18,
                height: 18
            },
            '.filter-options #categoryDd.MuiSelect-select': {
                //padding: 14,
            },
            '.filter-options .MuiInputLabel-outlined': {
                transform: 'translate(14px, 10px) scale(1)'
            },
            '.filter-options .MuiFormControl-root.MuiTextField-root': {
                width: 130,
                height: 31
            },
            '.filter-options .MuiInputLabel-outlined.MuiInputLabel-shrink': {
                transform: 'translate(14px, -6px) scale(0.75)'
            }
        }
    };
};

var VyaparCatalogueView = function (_React$Component) {
    (0, _inherits3.default)(VyaparCatalogueView, _React$Component);

    function VyaparCatalogueView(props) {
        (0, _classCallCheck3.default)(this, VyaparCatalogueView);

        var _this = (0, _possibleConstructorReturn3.default)(this, (VyaparCatalogueView.__proto__ || (0, _getPrototypeOf2.default)(VyaparCatalogueView)).call(this, props));

        _this.state = {
            showCreateCatalogueView: false,
            isCatalogueItemModalOpen: false,
            showSharingOptions: false
        };

        _this.getItemIdListOfCatalogue = function () {
            var catalogueItemLogic = new _CatalogueItemLogic2.default();
            return catalogueItemLogic.getItemIdListOfCatalogue();
        };

        _this.getCatalogueDataForView = function (lastCatalogueId, limit) {
            var catalogueItemLogic = new _CatalogueItemLogic2.default();
            return catalogueItemLogic.getCatalogueItemsList({ lastCatalogueId: lastCatalogueId, limit: limit, catalogueItemNameText: _this.selectedSearchText, catalogueItemCategoryName: _this.selectedCategoryName }, isImageRequired = true);
        };

        _this.refreshCatalogueRendering = function () {
            _this.setState({
                catalogueItemList: [],
                showCreateCatalogueView: false
            }, function () {
                _this.itemIdsOfCatalogueMap = _this.getItemIdListOfCatalogue();
                _this.catalogueId = _this.settingCache.getCatalogueId();
                _this.setState({
                    showSampleCatalogueScreen: _this.itemIdsOfCatalogueMap.size === 0
                });
                _this.lastCatalogueId = 0;
                _this.loadingCatalogueItems();
            });
        };

        _this.loadingCatalogueItems = function () {
            if (_this.catalogueId && _this.itemIdsOfCatalogueMap.size > 0) {
                var $loading = $('#loading');
                $loading.show(function () {
                    var catalogueItemList = _this.state.catalogueItemList;

                    _this.isCatalogueDataLoadingInProgress = true;
                    catalogueItemList.push.apply(catalogueItemList, (0, _toConsumableArray3.default)(_this.getCatalogueDataForView(_this.lastCatalogueId, 50)));
                    _this.setState({
                        catalogueItemList: catalogueItemList
                    }, function () {
                        _this.catalogueItemList = _this.state.catalogueItemList;
                        $loading.hide();
                        _this.isCatalogueDataLoadingInProgress = false;
                        if (catalogueItemList.length > 0) {
                            var lastCatalogueItem = catalogueItemList[catalogueItemList.length - 1];
                            _this.lastCatalogueId = lastCatalogueItem.getCatalogueItemId();
                        }
                        window.isCatalogueUpdating = false;
                    });
                });
            }
        };

        _this.showAddEditCatalogueView = function () {
            _this.setState({
                showCreateCatalogueView: !_this.state.showCreateCatalogueView
            }, function () {
                _analyticsHelper2.default.pushEvent('Make Catalogue Button Clicked');
            });
        };

        _this.renderSampleCatalogueView = function () {
            return _react2.default.createElement(
                _react2.default.Fragment,
                null,
                _react2.default.createElement(
                    'div',
                    { className: 'sample-catalogue bg-white bg-white d-flex justify-content-between catalogue-header align-items-center' },
                    _react2.default.createElement(
                        'div',
                        { className: 'catalogue-company-info' },
                        _react2.default.createElement(
                            'p',
                            { className: 'font-family-bold mb-5' },
                            'Sample Catalogue'
                        ),
                        _react2.default.createElement(
                            'p',
                            { className: 'font-12 color-80808B' },
                            'Make and Share Online Product Catalogues.'
                        )
                    )
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'd-flex h-100-per flex-column', style: { padding: 10 } },
                    _react2.default.createElement(
                        'div',
                        { className: 'bg-white d-flex form-wrapper padBottom10 justify-content-center' },
                        _react2.default.createElement('img', { src: './../inlineSVG/catalogue_sample.png' })
                    ),
                    _react2.default.createElement(
                        'div',
                        { className: 'catalogue-footer d-flex justify-content-center mt-10' },
                        _react2.default.createElement(
                            _Button2.default,
                            {
                                style: { width: 300, background: '#FF805D' },
                                variant: 'contained',
                                color: 'primary',
                                onClick: _this.showAddEditCatalogueView
                            },
                            'Make Your Catalogue'
                        )
                    )
                )
            );
        };

        _this.closeCatalogueItemModal = function (updatedItemDetails) {
            _this.updateCatalogueItemDetails(updatedItemDetails);
            _this.setState({
                isCatalogueItemModalOpen: false
            });
        };

        _this.updateCatalogueItemDetails = function (item) {
            if (item) {
                var catalogueItemList = _this.state.catalogueItemList;

                var catalogueItemLogic = catalogueItemList[catalogueItemList.indexOf(_this.selectedCatalogueItem)];
                catalogueItemLogic.setCatalogueItemCode(item.itemCode);
                catalogueItemLogic.setCatalogueItemDescription(item.itemDescription);
                catalogueItemLogic.setCatalogueItemName(item.itemName);
                catalogueItemLogic.setCatalogueItemSaleUnitPrice(item.itemSaleUnitPrice);
                catalogueItemLogic.setCatalogueItemCategoryName(item.itemCategoryName);
                _this.setState({
                    catalogueItemList: catalogueItemList
                });
            }
        };

        _this.isCatalogueItemExists = function (text, type) {
            var catalogueItemList = _this.state.catalogueItemList;

            var catItem = catalogueItemList.find(function (item) {
                if (type === 'name' && item.getCatalogueItemName().toLowerCase().trim() === text.toLowerCase().trim()) {
                    return true;
                } else if (type === 'code' && item.getCatalogueItemCode().toLowerCase().trim() === text.toLowerCase().trim()) {
                    return true;
                }
            });
            return catItem ? true : false;
        };

        _this.openCatalogueItemModal = function (selectedCatalogueItem) {
            _this.selectedCatalogueItem = selectedCatalogueItem;
            _this.setState({
                itemDetails: selectedCatalogueItem,
                isCatalogueItemModalOpen: true
            });
        };

        _this.createCatalogueURL = function () {
            var Domain = require('../../../Constants/Domain');
            var domain = Domain.thisDomain;
            var catalogueUID = _this.settingCache.getCatalogueUID();
            return domain + '/catalogue/' + catalogueUID;
        };

        _this.getCatalogueURL = function () {
            if (!_this.catalogueURL) {
                _this.catalogueURL = _this.createCatalogueURL();
            }
            return _this.catalogueURL;
        };

        _this.copyCatalogueURLForSharing = function () {
            var copyToClipboard = document.getElementById('copyToClipboard');
            copyToClipboard.addEventListener('copy', function (e) {
                e.preventDefault();
                if (e.clipboardData) {
                    e.clipboardData.setData('text/plain', _this.getCatalogueURL());
                    ToastHelper.success('Catalogue URL copied.');
                }
            });
            _analyticsHelper2.default.pushEvent('Catalogue Shared', {
                type: 'copy'
            });
            document.execCommand('copy');
        };

        _this.toggleSharingOptions = function () {
            _this.setState({
                showSharingOptions: !_this.state.showSharingOptions
            });
        };

        _this.renderUserCatalogueView = function () {
            var FirmCache = require('../../../Cache/FirmCache.js');
            var SearchBox = require('../SearchBox').default;
            var firmCache = new FirmCache();
            var catalogueItemLogic = new _CatalogueItemLogic2.default();
            var firm = firmCache.getFirmById(1);
            var catalogueURLText = _this.getCatalogueURL();
            var CatalogueSharingOptions = require('./CatalogueSharingOptions');
            var gstInNumber = _this.settingCache.getGSTEnabled() ? firm.getFirmHSNSAC() : firm.getFirmTin();
            _this.listOfCategories = [];
            _this.listOfCategories.push({ categoryId: 0, categoryName: 'All' });
            _this.listOfCategories = [].concat((0, _toConsumableArray3.default)(_this.listOfCategories), (0, _toConsumableArray3.default)(catalogueItemLogic.getCatagoryListOfCatalogue()));
            var firmDetails = {
                name: firm.getFirmName(),
                phone: firm.getFirmPhone(),
                email: firm.getFirmEmail(),
                gstin: gstInNumber,
                address: firm.getFirmAddress(),
                logo: firm.getFirmImagePath()
            };
            var closeNameModal = function closeNameModal() {
                _this.setState({
                    openNamesModal: false
                });
            };
            var handleCatalogueSharingOption = function handleCatalogueSharingOption(namesModalFor) {
                return function () {
                    var handleShare = void 0,
                        columnsVisibility = {};
                    var openNamesModal = true;
                    _analyticsHelper2.default.pushEvent('Catalogue Shared', {
                        type: namesModalFor
                    });
                    switch (namesModalFor) {
                        case 'Email':
                            columnsVisibility = { email: true };
                            handleShare = CatalogueSharingOptions.sendEmail(catalogueURLText, closeNameModal);
                            break;
                        case 'SMS':
                            columnsVisibility = { phone: true };
                            handleShare = CatalogueSharingOptions.sendSms(catalogueURLText, closeNameModal);
                            break;
                        case 'Whatsapp':
                            openNamesModal = false;
                            CatalogueSharingOptions.sendWhatsapp(catalogueURLText, closeNameModal);
                            break;
                    };
                    _this.setState({
                        openNamesModal: openNamesModal,
                        showSharingOptions: false,
                        namesModalFor: namesModalFor,
                        handleShare: handleShare,
                        columnsVisibility: columnsVisibility
                    });
                };
            };

            var renderNamesModal = function renderNamesModal() {
                var namesModalFor = _this.state.namesModalFor;

                var NamesModalPopupForSharing = require('../UIControls/NamesModalPopupForSharing').default;
                return _react2.default.createElement(NamesModalPopupForSharing, {
                    namesModalTitle: 'Share Catalogue With Parties',
                    namesModalSubTitle: 'Select the parties and share Catalogue link with them by ' + namesModalFor + '.',
                    namesModalNote: 'Note: Catalogue will be shared with only the parties whose correct information is present.',
                    shareBtnName: 'Share Catalogue',
                    columnsVisibility: _this.state.columnsVisibility,
                    namesModalFor: _this.state.namesModalFor,
                    openNamesModal: _this.state.openNamesModal,
                    closeNamesModal: function closeNamesModal() {
                        _this.setState({ openNamesModal: false });
                    },
                    handleShare: _this.state.handleShare,
                    rowSelection: _this.state.rowSelection
                });
            };

            var renderSharingOptions = function renderSharingOptions() {
                var SharingMenuOptions = require('../UIControls/sharingMenuOptions').default;
                return _react2.default.createElement(SharingMenuOptions, {
                    handleEmail: handleCatalogueSharingOption('Email'),
                    handleSms: handleCatalogueSharingOption('SMS'),
                    handleWhatsapp: handleCatalogueSharingOption('Whatsapp'),
                    isVisible: _this.state.showSharingOptions,
                    customClasses: 'itemMenuSharing catalogue-item-sharing'
                });
            };

            var listenScrollEvent = function listenScrollEvent() {
                var $catalogueCard = $('.catalogue-cards');
                var isFiltersOn = _this.selectedCategoryName || _this.selectedSearchText;
                var moreItemAvailable = !isFiltersOn ? _this.state.catalogueItemList.length < _this.itemIdsOfCatalogueMap.size : _this.state.catalogueItemList.length % 50 === 0;
                if (!_this.isCatalogueDataLoadingInProgress && moreItemAvailable && $catalogueCard.scrollTop() + $catalogueCard.innerHeight() >= $catalogueCard[0].scrollHeight - 200) {
                    _this.isCatalogueDataLoadingInProgress = true;
                    _this.loadingCatalogueItems();
                }
            };

            var filterItems = function filterItems() {
                _this.refreshCatalogueRendering();
            };

            var filterSearchBox = function filterSearchBox(searchText) {
                _this.selectedSearchText = searchText;
                filterItems();
            };

            var filterCatgories = function filterCatgories(event) {
                if (event.target.value > 0) {
                    var filteredCategory = _this.listOfCategories.find(function (category) {
                        if (Number(category.categoryId) === Number(event.target.value)) return category;
                    });
                    _this.selectedCategoryName = filteredCategory.categoryName;
                } else {
                    _this.selectedCategoryName = '';
                }
                filterItems();
            };

            return _react2.default.createElement(
                _react2.default.Fragment,
                null,
                _react2.default.createElement(
                    'div',
                    { className: 'bg-white bg-white d-flex justify-content-between catalogue-header align-items-center' },
                    _react2.default.createElement(
                        'div',
                        { className: 'catalogue-company-info' },
                        _react2.default.createElement(
                            'p',
                            { className: 'font-family-bold font-20' },
                            firmDetails.name
                        ),
                        firmDetails.address && _react2.default.createElement(
                            'p',
                            { className: 'font-12 color-80808B' },
                            firmDetails.address
                        ),
                        (firmDetails.phone || firmDetails.email || firm.gstin) && _react2.default.createElement(
                            'div',
                            { className: 'font-12 color-80808B' },
                            (firmDetails.phone || firmDetails.email) && _react2.default.createElement(
                                'p',
                                { className: 'd-flex' },
                                _react2.default.createElement(
                                    'span',
                                    { className: 'd-flex' },
                                    _react2.default.createElement('img', { className: 'phone-call', src: '../inlineSVG/phone-call.svg' }),
                                    _react2.default.createElement(
                                        'span',
                                        null,
                                        firmDetails.phone
                                    )
                                ),
                                firmDetails.email && _react2.default.createElement(
                                    _react2.default.Fragment,
                                    null,
                                    _react2.default.createElement(
                                        'span',
                                        null,
                                        '|'
                                    ),
                                    _react2.default.createElement(
                                        'span',
                                        { className: 'd-flex' },
                                        _react2.default.createElement('img', { className: 'gmail', src: '../inlineSVG/gmail.svg' }),
                                        _react2.default.createElement(
                                            'span',
                                            null,
                                            firmDetails.email
                                        )
                                    )
                                )
                            ),
                            firmDetails.gstin && _react2.default.createElement(
                                'p',
                                null,
                                _react2.default.createElement(
                                    'span',
                                    null,
                                    _this.settingCache.getGSTEnabled() ? 'GSTIN' : _this.settingCache.getTINText(),
                                    ':'
                                ),
                                _react2.default.createElement(
                                    'span',
                                    { className: 'ml-5' },
                                    firmDetails.gstin
                                )
                            )
                        )
                    ),
                    _react2.default.createElement(
                        'div',
                        { className: 'mr-10 d-flex justify-content-between align-items-center' },
                        _react2.default.createElement(
                            'div',
                            { className: 'catalogue-links font-family-medium  font-12 d-flex flex-row' },
                            _react2.default.createElement(
                                'a',
                                { className: 'mr-10', onClick: _this.showAddEditCatalogueView },
                                _react2.default.createElement('img', { src: '../inlineSVG/baseline-edit.svg' }),
                                _react2.default.createElement(
                                    'span',
                                    null,
                                    'Edit Catalogue'
                                )
                            ),
                            _react2.default.createElement(
                                'a',
                                { id: 'copyToClipboard', onClick: _this.copyCatalogueURLForSharing },
                                _react2.default.createElement('img', { src: '../inlineSVG/copy-content.svg' }),
                                _react2.default.createElement(
                                    'span',
                                    null,
                                    'Copy Catalogue URL'
                                )
                            )
                        ),
                        firmDetails.logo && _react2.default.createElement('img', { className: 'catalogue-company-logo ml-20', src: 'data:image/jpeg;base64,' + firmDetails.logo })
                    )
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'bg-white d-flex h-100-per flex-column justify-content-between', style: { padding: 10 } },
                    _react2.default.createElement(
                        'div',
                        { className: 'h-100-per mb-10 d-flex flex-column' },
                        _react2.default.createElement(
                            'div',
                            { className: 'd-flex flex-row filter-options',
                                style: { paddingBottom: '2rem' }
                            },
                            _react2.default.createElement(SearchBox, {
                                throttle: 500,
                                filter: filterSearchBox,
                                collapsed: false,
                                collapsedTextBoxClass: '',
                                onCollapsedClick: function onCollapsedClick() {
                                    _this.setState({ showSearch: true });
                                },
                                onInputBlur: function onInputBlur() {
                                    _this.setState({ showSearch: false });
                                }
                            }),
                            _this.listOfCategories.length > 2 && _react2.default.createElement(
                                _TextField2.default,
                                {
                                    id: 'categoryDd',
                                    select: true,
                                    label: 'Category'
                                    //value={categoryObject.categoryId}
                                    , onChange: filterCatgories,
                                    variant: 'outlined'
                                },
                                _this.listOfCategories.map(function (value, index) {
                                    return _react2.default.createElement(
                                        _MenuItem2.default,
                                        { key: value.categoryId, value: value.categoryId },
                                        value.categoryName
                                    );
                                })
                            )
                        ),
                        _react2.default.createElement(
                            'ul',
                            { 'class': 'catalogue-cards', onScroll: listenScrollEvent },
                            _this.state.catalogueItemList.map(function (item) {
                                var itemImages = item.getCatalogueItemImagesList();
                                return _react2.default.createElement(
                                    'li',
                                    { 'class': 'catalogue-item' },
                                    _react2.default.createElement(
                                        'div',
                                        { onClick: _this.openCatalogueItemModal.bind(_this, item), 'class': 'card' },
                                        _react2.default.createElement('img', { className: 'catalogue-image', style: { width: 200, height: 200 }, src: itemImages.length > 0 ? itemImages[0] : '../inlineSVG/no_image.svg' }),
                                        _react2.default.createElement(
                                            'div',
                                            { 'class': 'catalogue-content' },
                                            _react2.default.createElement(
                                                'p',
                                                { 'class': 'catalogue-item-text' },
                                                item.getCatalogueItemName()
                                            ),
                                            _react2.default.createElement(
                                                'p',
                                                { 'class': 'catalogue-item-price' },
                                                _this.settingCache.getCurrencySymbol(),
                                                ' ',
                                                item.getCatalogueItemSaleUnitPrice()
                                            )
                                        )
                                    )
                                );
                            })
                        )
                    ),
                    _react2.default.createElement(
                        'div',
                        { className: 'catalogue-footer d-flex justify-content-center' },
                        _react2.default.createElement(
                            'div',
                            { className: 'position-relative item-sharing-container' },
                            _react2.default.createElement(
                                _Button2.default,
                                {
                                    ref: function ref(node) {
                                        _this.sharingBtn = node;
                                    },
                                    style: { width: 300, background: '#FF805D' },
                                    variant: 'contained',
                                    color: 'primary',
                                    onClick: _this.toggleSharingOptions
                                },
                                _react2.default.createElement(
                                    'div',
                                    { className: 'd-flex align-items-center' },
                                    _react2.default.createElement('img', {
                                        src: './../inlineSVG/share.svg'
                                    }),
                                    'Share Catalogue'
                                )
                            ),
                            _this.state.showSharingOptions && _react2.default.createElement(
                                'div',
                                { className: 'd-flex justify-content-center', ref: function ref(node) {
                                        _this.sharingOptionsNode = node;
                                    } },
                                ' ',
                                renderSharingOptions(),
                                ' '
                            ),
                            _this.state.openNamesModal && renderNamesModal()
                        )
                    )
                )
            );
        };

        _this.handleEscape = function (e) {
            e.preventDefault();
            e.nativeEvent.stopImmediatePropagation();
            _this.closeCatalogueItemModal();
        };

        _this.handleClickOutside = function (event) {
            if (_this.sharingBtn && !_this.sharingBtn.contains(event.target) && _this.sharingOptionsNode && !_this.sharingOptionsNode.contains(event.target)) {
                _this.toggleSharingOptions();
            }
        };

        _this.settingCache = new _SettingCache2.default();
        _this.catalogueId = _this.settingCache.getCatalogueId();
        _this.state.catalogueItemList = [];
        _this.itemIdsOfCatalogueMap = _this.getItemIdListOfCatalogue();
        if (!_this.catalogueId || _this.itemIdsOfCatalogueMap.size === 0) {
            _this.state = {
                showSampleCatalogueScreen: true
            };
        }
        _this.selectedSearchText = '';
        _this.selectedCategoryName = '';
        window.isCatalogueUpdating = false;
        return _this;
    }

    (0, _createClass3.default)(VyaparCatalogueView, [{
        key: 'onResume',
        value: function onResume() {
            if (!window.isCatalogueUpdating) {
                window.isCatalogueUpdating = true;
                this.refreshCatalogueRendering();
            }
        }
    }, {
        key: 'componentDidMount',
        value: function componentDidMount() {
            var _this2 = this;

            setTimeout(function () {
                _this2.lastCatalogueId = 0;
                _this2.loadingCatalogueItems();
            });
            window.onResume = this.onResume.bind(this);
            document.addEventListener('mousedown', this.handleClickOutside);
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            if (window.onResume) {
                delete window.onResume;
            }
            document.removeEventListener('mousedown', this.handleClickOutside);
        }
    }, {
        key: 'render',
        value: function render() {
            var _this3 = this;

            var classes = this.props.classes;
            var _state = this.state,
                showCreateCatalogueView = _state.showCreateCatalogueView,
                showSampleCatalogueScreen = _state.showSampleCatalogueScreen,
                itemDetails = _state.itemDetails;

            return _react2.default.createElement(
                _react2.default.Fragment,
                null,
                !showCreateCatalogueView && _react2.default.createElement(
                    'div',
                    { className: 'd-flex h-100-per flex-column catalogue-container-view',
                        ref: function ref(node) {
                            _this3.catalogueNode = node;
                        } },
                    showSampleCatalogueScreen && this.renderSampleCatalogueView(),
                    !showSampleCatalogueScreen && this.renderUserCatalogueView()
                ),
                showCreateCatalogueView && _react2.default.createElement(_CreateVyaparCatalogue2.default, { catalogueId: this.catalogueId, existingCatalogueItemList: this.itemIdsOfCatalogueMap, returnToView: this.showAddEditCatalogueView, onCatalogueGenerationSuccess: this.refreshCatalogueRendering }),
                _react2.default.createElement(
                    _Dialog2.default,
                    { onEscapeKeyDown: this.handleEscape, maxWidth: 'lg',
                        disableEnforceFocus: true, open: this.state.isCatalogueItemModalOpen },
                    _react2.default.createElement(_CatalogueItemDetails2.default, { close: this.closeCatalogueItemModal, isCatalogueItemExists: this.isCatalogueItemExists, item: itemDetails, catalogueId: this.catalogueId })
                )
            );
        }
    }]);
    return VyaparCatalogueView;
}(_react2.default.Component);

exports.default = (0, _styles.withStyles)(styles)((0, _trashableReact2.default)(VyaparCatalogueView));