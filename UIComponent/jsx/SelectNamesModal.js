Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _Modal = require('./Modal');

var _Modal2 = _interopRequireDefault(_Modal);

var _SearchBox = require('./SearchBox');

var _SearchBox2 = _interopRequireDefault(_SearchBox);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SelectNamesModal = function (_React$Component) {
	(0, _inherits3.default)(SelectNamesModal, _React$Component);

	function SelectNamesModal(props) {
		(0, _classCallCheck3.default)(this, SelectNamesModal);

		var _this = (0, _possibleConstructorReturn3.default)(this, (SelectNamesModal.__proto__ || (0, _getPrototypeOf2.default)(SelectNamesModal)).call(this, props));

		_this.onClose = _this.onClose.bind(_this);
		_this.onSave = _this.onSave.bind(_this);
		_this.getApi = _this.getApi.bind(_this);
		_this.api = null;
		_this.quickFilter = _this.quickFilter.bind(_this);
		_this.title = 'Select Parties';
		return _this;
	}

	(0, _createClass3.default)(SelectNamesModal, [{
		key: 'onClose',
		value: function onClose() {
			this.props.onClose && this.props.onClose();
		}
	}, {
		key: 'quickFilter',
		value: function quickFilter(text) {
			this.api.setQuickFilter(text);
		}
	}, {
		key: 'onSave',
		value: function onSave() {
			var selected = this.api.getSelectedRows().map(function (item) {
				return item.nameId;
			});
			this.props.onSave && this.props.onSave(selected);
		}
	}, {
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(nextProps) {
			if (this.api) {
				this.api.setRowData(nextProps.items);
			}
			return true;
		}
	}, {
		key: 'getApi',
		value: function getApi(params) {
			this.api = params.api;
			this.api.setRowData(this.props.items);
		}
	}, {
		key: 'render',
		value: function render() {
			var gridOptions = {
				enableFilter: false,
				enableSorting: true,
				rowSelection: 'multiple',
				rowData: [],
				columnDefs: [{
					field: 'fullName',
					headerName: 'NAME',
					headerCheckboxSelection: true,
					headerCheckboxSelectionFilteredOnly: true,
					checkboxSelection: true
				}]
			};
			return _react2.default.createElement(
				_Modal2.default,
				{
					height: '610px',
					width: '600px',
					onClose: this.onClose,
					isOpen: this.props.isOpen,
					title: this.title
				},
				_react2.default.createElement(
					'div',
					{ className: 'modalBody d-flex flex-column flex-1' },
					_react2.default.createElement(
						'div',
						{ className: 'mb-10 mt-10' },
						_react2.default.createElement(_SearchBox2.default, { throttle: 300, filter: this.quickFilter })
					),
					_react2.default.createElement(
						'div',
						{
							className: 'd-flex flex-column flex-1',
							style: { width: '100%' }
						},
						_react2.default.createElement(_Grid2.default, {
							notSelectFirstRow: this.props.notSelectFirstRow,
							getApi: this.getApi,
							classes: 'noBorder',
							gridOptions: gridOptions
						})
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'modalFooter align-items-center justify-content-end d-flex' },
					_react2.default.createElement(
						'button',
						{
							onClick: this.onSave,
							value: 'Select Items',
							className: 'modal-footer-button terminalButton'
						},
						this.props.children
					)
				)
			);
		}
	}]);
	return SelectNamesModal;
}(_react2.default.Component);

SelectNamesModal.propTypes = {
	onClose: _propTypes2.default.func,
	onSave: _propTypes2.default.func,
	isOpen: _propTypes2.default.bool,
	title: _propTypes2.default.string,
	notSelectFirstRow: _propTypes2.default.bool,
	items: _propTypes2.default.array,
	children: _propTypes2.default.oneOfType([_propTypes2.default.arrayOf(_propTypes2.default.node), _propTypes2.default.node])
};

exports.default = SelectNamesModal;