Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Dialog = require('@material-ui/core/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _styles = require('@material-ui/core/styles');

var _LicenseUtility = require('../../Utilities/LicenseUtility');

var _LicenseUtility2 = _interopRequireDefault(_LicenseUtility);

var _UsageTypeConstants = require('../../Constants/UsageTypeConstants');

var _UsageTypeConstants2 = _interopRequireDefault(_UsageTypeConstants);

var _LocalStorageHelper = require('../../Utilities/LocalStorageHelper');

var _LocalStorageHelper2 = _interopRequireDefault(_LocalStorageHelper);

var _SettingsModel = require('../../Models/SettingsModel');

var _SettingsModel2 = _interopRequireDefault(_SettingsModel);

var _SettingCache = require('../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
  return {
    closeWrapper: {
      position: 'absolute',
      borderRadius: '38px',
      backgroundColor: '#999',
      width: 20,
      height: 20
    },
    likeOurPage: {
      color: '#010179',
      '& *': {
        margin: 2
      },
      '& img': {
        width: 20,
        height: 20
      }
    },
    licenseBgImage: {
      backgroundImage: 'url(../inlineSVG/license-bg-image.svg)',
      width: 423,
      height: 120,
      backgroundRepeat: 'no-repeat',
      position: 'relative',
      '& .license-percentage-span': {
        backgroundColor: '#EBF9B1',
        width: 120,
        height: 120,
        borderRadius: '50%',
        position: 'absolute',
        left: '38%',
        bottom: '10%',
        color: 'red',
        display: 'inline-flex',
        alignItems: 'center',
        textAlign: 'center',
        fontSize: 33,
        fontWeight: 'bold',
        wordSpacing: 120
      }
    },
    licenseTrialExpiry: {
      padding: 50,
      width: 600,
      '& .cross-icon': {
        position: 'absolute',
        top: 20,
        right: 20
      }
    },
    licenseExtBtn: {
      padding: '10px 20px',
      borderRadius: 6,
      color: '#FFFFFF'
    },
    buyLicenseBtn: {
      backgroundColor: 'orange'
    },
    extendTrialBtn: {
      backgroundColor: '#04e004'
    },
    fullAccessText: {
      color: '#a7a7a7',
      fontWeight: 'bold',
      fontSize: 13
    },
    color284561: {
      color: '#284561'
    }
  };
};

var ExtendLicenseModal = function (_React$Component) {
  (0, _inherits3.default)(ExtendLicenseModal, _React$Component);

  function ExtendLicenseModal() {
    (0, _classCallCheck3.default)(this, ExtendLicenseModal);

    var _this = (0, _possibleConstructorReturn3.default)(this, (ExtendLicenseModal.__proto__ || (0, _getPrototypeOf2.default)(ExtendLicenseModal)).call(this));

    _this.handleExtendLicenseClose = function () {
      _this.setState({
        openExtendLicense: false
      });
    };

    _this.getUserSubscriptions = function () {
      var settingCache = new _SettingCache2.default();
      return {
        youtubeSubscriptionTrialStartDate: settingCache.getYoutubeSubscriptionTrialStartDate(),
        fbSubscriptionTrialStartDate: settingCache.getFacebookSubscriptionTrialStartDate()
      };
    };

    _this.extendTrial = function () {
      var isOnline = require('is-online');
      isOnline().then(function (online) {
        if (online) {
          var BrowserWindow = require('electron').remote.BrowserWindow;

          var win = new BrowserWindow({
            center: true,
            alwaysOnTop: true,
            show: true,
            frame: true,
            autoHideMenuBar: true,
            webPreferences: {
              webSecurity: false,
              javascript: true,
              nodeIntegration: false
            }
          });

          var callback = function callback(result) {
            if (result.subscribed) {
              var Queries = require('../../Constants/Queries');
              var MyDate = require('../../Utilities/MyDate.js');
              var settingValue = MyDate.getDate('y-m-d H:M:S');
              if (result.subType === 'youtube') {
                ToastHelper.info('Subscribe to Vyapar YouTube Channel and we are extending your license by 15 days.');
                _this.settingsModel.setSettingKey(Queries.SETTING_YOUTUBE_SUBSCRIPTION_TRIAL_START_DATE);
                _this.settingsModel.setSettingValue(settingValue);
                _this.settingsModel.UpdateSetting(settingValue, { nonSyncableSetting: false });
                // win.close();
              } else if (result.subType === 'fb') {
                ToastHelper.info('Subscribe to Vyapar Facebook Page and we are extending your license by 15 days.');
                _this.settingsModel.setSettingKey(Queries.SETTING_FACEBOOK_SUBSCRIPTION_TRIAL_START_DATE);
                _this.settingsModel.setSettingValue(settingValue);
                _this.settingsModel.UpdateSetting(settingValue, { nonSyncableSetting: false });
                // win.close();
              }
              _LicenseUtility2.default.updateLicenseInfo();
            }
          };

          var userSubscriptions = _this.getUserSubscriptions();
          if (!userSubscriptions.youtubeSubscriptionTrialStartDate) {
            win.loadURL('https://www.youtube.com/channel/UCPyg1bziA6iTfjjX-IwGlhQ');
            callback({ subscribed: true, subType: 'youtube' });
          } else if (!userSubscriptions.fbSubscriptionTrialStartDate) {
            win.loadURL('https://www.facebook.com/vyaparApp/');
            callback({ subscribed: true, subType: 'fb' });
          }
          win.on('closed', function () {
            win = null;
          });
          // win.openDevTools();
          _this.handleExtendLicenseClose();
        } else {
          ToastHelper.error('Please connect to internet to extend trial of Vyapar.');
        }
      });
    };

    _this.buyLicense = function () {
      var CommonUtility = require('../../Utilities/CommonUtility');
      _this.handleExtendLicenseClose();
      CommonUtility.loadDefaultPage('plans.html');
    };

    _this.handleEscape = function (e) {
      e.preventDefault();
      e.nativeEvent.stopImmediatePropagation();
      _this.handleExtendLicenseClose();
    };

    _this.licenseInfoDetails = _LicenseUtility2.default.getLicenseInfo();
    _this.settingsModel = new _SettingsModel2.default();
    _this.plansData = [];
    try {
      var localStorageValueForPlans = _LocalStorageHelper2.default.getValue('lastPlansData');
      if (localStorageValueForPlans) {
        _this.plansData = JSON.parse(localStorageValueForPlans);
      }
    } catch (e) {}
    _this.state = {
      openExtendLicense: true
    };
    return _this;
  }

  (0, _createClass3.default)(ExtendLicenseModal, [{
    key: 'getDialogTemplate',
    value: function getDialogTemplate() {
      var template = this.licenseInfoDetails.licenseUsageType;
      switch (template) {
        case _UsageTypeConstants2.default.BLOCKED:
        case _UsageTypeConstants2.default.EXPIRED_LICENSE:
          return this.getLicenseExpireTemplate();
        case _UsageTypeConstants2.default.VALID_LICENSE:
        case _UsageTypeConstants2.default.TRIAL_PERIOD:
          return this.getLicenseExpiryDiscoutTemplate();
      }
    }
  }, {
    key: 'getLicenseExpiryDiscoutTemplate',
    value: function getLicenseExpiryDiscoutTemplate() {
      var isTrialActive = this.licenseInfoDetails.licenseUsageType === _UsageTypeConstants2.default.TRIAL_PERIOD;
      var classes = this.props.classes;

      return _react2.default.createElement(
        'div',
        { className: 'd-flex justify-content-center align-items-center fullContainerHeight bg-white ' + classes.licenseTrialExpiry },
        _react2.default.createElement(
          'div',
          { className: 'd-flex flex-column justify-content-center align-items-center' },
          _react2.default.createElement('img', { className: classes.closeWrapper + ' pointer closeWrapper', onClick: this.handleExtendLicenseClose.bind(this), src: '../inlineSVG/close-white.png' }),
          _react2.default.createElement(
            'div',
            { className: classes.licenseBgImage },
            _react2.default.createElement(
              'span',
              { className: 'license-percentage-span' },
              this.plansData.userLicenseDiscountPercentage ? this.plansData.userLicenseDiscountPercentage : '10% OFF'
            )
          ),
          _react2.default.createElement(
            'p',
            { className: 'mt-10 font-20' },
            _react2.default.createElement(
              'strong',
              null,
              'on all Vyapar Licenses'
            )
          ),
          _react2.default.createElement(
            'p',
            { className: 'mt-5' },
            _react2.default.createElement(
              'strong',
              null,
              'Valid only for today'
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'mt-20' },
            _react2.default.createElement(
              'button',
              { onClick: this.buyLicense, className: classes.buyLicenseBtn + '  ' + classes.licenseExtBtn + ' ' + classes.width230 },
              isTrialActive ? 'Buy License Now' : 'Renew License Now'
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'd-flex mt-20 font-13' },
            _react2.default.createElement(
              'p',
              { className: classes.fullAccessText },
              isTrialActive ? 'Trial' : 'License',
              ' expires in '
            ),
            _react2.default.createElement(
              'span',
              { className: 'ml-5 ' + classes.color284561 },
              _react2.default.createElement(
                'strong',
                null,
                this.licenseInfoDetails.licenseValidityDays > 1 ? this.licenseInfoDetails.licenseValidityDays + ' days' : 'today'
              )
            )
          )
        )
      );
    }
  }, {
    key: 'getLicenseExpireTemplate',
    value: function getLicenseExpireTemplate() {
      var isTrialExpired = this.licenseInfoDetails.licenseUsageType === _UsageTypeConstants2.default.BLOCKED;
      var isLicenseExpired = this.licenseInfoDetails.licenseUsageType === _UsageTypeConstants2.default.EXPIRED_LICENSE;
      // let userSubscriptions = this.getUserSubscriptions();
      // let pageToShow = !userSubscriptions.youtubeSubscriptionTrialStartDate ? 'youtube' : !userSubscriptions.fbSubscriptionTrialStartDate ? 'fb' : '';
      var pageToShow = '';
      var classes = this.props.classes;

      return _react2.default.createElement(
        'div',
        { className: 'd-flex justify-content-center align-items-center fullContainerHeight bg-white ' + classes.licenseTrialExpiry },
        _react2.default.createElement(
          'div',
          { className: 'd-flex flex-column justify-content-center align-items-center' },
          _react2.default.createElement('img', { className: classes.closeWrapper + ' pointer closeWrapper', onClick: this.handleExtendLicenseClose.bind(this), src: '../inlineSVG/close-white.png' }),
          _react2.default.createElement('img', { src: '../inlineSVG/license_expired.svg' }),
          isLicenseExpired && _react2.default.createElement(
            'p',
            { className: 'mt-20' },
            _react2.default.createElement(
              'strong',
              null,
              'Your License has Expired'
            )
          ),
          isTrialExpired && _react2.default.createElement(
            _react2.default.Fragment,
            null,
            _react2.default.createElement(
              'p',
              { className: 'mt-20' },
              _react2.default.createElement(
                'strong',
                null,
                'Your Trial Period has Expired'
              )
            ),
            pageToShow && _react2.default.createElement(
              _react2.default.Fragment,
              null,
              _react2.default.createElement(
                'p',
                { className: 'mt-20 font-13' },
                'Buy License'
              ),
              _react2.default.createElement(
                'p',
                { className: 'mt-10 font-13' },
                'or'
              ),
              pageToShow === 'youtube' && _react2.default.createElement(
                'p',
                { className: 'd-flex justify-content-center align-items-center mt-10 font-13 ' + classes.likeOurPage },
                'Like our ',
                _react2.default.createElement('img', { src: '../inlineSVG/youtube.svg' }),
                _react2.default.createElement(
                  'strong',
                  null,
                  'youtube channel'
                ),
                ' to extend trial by ',
                _react2.default.createElement(
                  'strong',
                  null,
                  '15 days'
                )
              ),
              pageToShow === 'fb' && _react2.default.createElement(
                'p',
                { className: 'd-flex justify-content-center align-items-center mt-10 font-13 ' + classes.likeOurPage },
                'Like our ',
                _react2.default.createElement('img', { src: '../inlineSVG/facebook.svg' }),
                _react2.default.createElement(
                  'strong',
                  null,
                  'facebook page'
                ),
                ' to extend trial by ',
                _react2.default.createElement(
                  'strong',
                  null,
                  '15 days'
                )
              )
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'mt-20' },
            isTrialExpired && pageToShow && _react2.default.createElement(
              'button',
              { onClick: this.extendTrial, className: 'mr-10 ' + classes.extendTrialBtn + '  ' + classes.licenseExtBtn },
              'Extend Trial'
            ),
            _react2.default.createElement(
              'button',
              { onClick: this.buyLicense, className: classes.buyLicenseBtn + '  ' + classes.licenseExtBtn },
              isTrialExpired ? 'Buy License' : 'Renew License'
            )
          ),
          _react2.default.createElement(
            'p',
            { className: classes.fullAccessText + ' mt-20' },
            'To get full access to Vyapar'
          )
        )
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var classes = this.props.classes;

      return _react2.default.createElement(
        _react2.default.Fragment,
        null,
        _react2.default.createElement(
          _Dialog2.default,
          { onEscapeKeyDown: this.handleEscape, onClose: this.handleExtendLicenseClose,
            disableEnforceFocus: true, open: this.state.openExtendLicense
          },
          this.getDialogTemplate()
        )
      );
    }
  }]);
  return ExtendLicenseModal;
}(_react2.default.Component);

exports.default = (0, _styles.withStyles)(styles)(ExtendLicenseModal);