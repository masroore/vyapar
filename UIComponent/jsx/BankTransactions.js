Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _DateFilter = require('./grid-filters/DateFilter');

var _DateFilter2 = _interopRequireDefault(_DateFilter);

var _NumberFilter = require('./grid-filters/NumberFilter');

var _NumberFilter2 = _interopRequireDefault(_NumberFilter);

var _StringFilter = require('./grid-filters/StringFilter');

var _StringFilter2 = _interopRequireDefault(_StringFilter);

var _MultiSelectionFilter = require('./grid-filters/MultiSelectionFilter');

var _MultiSelectionFilter2 = _interopRequireDefault(_MultiSelectionFilter);

var _TransactionHelper = require('../../Utilities/TransactionHelper');

var TransactionHelper = _interopRequireWildcard(_TransactionHelper);

var _MyDate = require('../../Utilities/MyDate');

var _MyDate2 = _interopRequireDefault(_MyDate);

var _ColorConstants = require('../../Constants/ColorConstants');

var _ColorConstants2 = _interopRequireDefault(_ColorConstants);

var _TxnTypeConstant = require('../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var BankTransactions = function (_React$Component) {
	(0, _inherits3.default)(BankTransactions, _React$Component);

	function BankTransactions(props) {
		(0, _classCallCheck3.default)(this, BankTransactions);

		var _this = (0, _possibleConstructorReturn3.default)(this, (BankTransactions.__proto__ || (0, _getPrototypeOf2.default)(BankTransactions)).call(this, props));

		_this.getApi = function (params) {
			_this.api = params.api;
			_this.api.setRowData(_this.props.items);
		};

		_this.columnDefs = [{
			field: 'color',
			headerName: '',
			width: 30,
			suppressMenu: true,
			getQuickFilterText: function getQuickFilterText() {
				return '';
			},
			suppressFilter: true,
			cellRenderer: function cellRenderer(params) {
				var color = _ColorConstants2.default[params.data.txnType];
				return '<div style=\'background-color: ' + color + '; border-radius:50%;width:8px;height:8px; top: 19px; position:relative;\'></div>';
			}
		}, {
			field: 'txnType',
			filter: _MultiSelectionFilter2.default,
			filterOptions: _TxnTypeConstant2.default.getFilterOptionsForBankAccounts(),
			getQuickFilterText: function getQuickFilterText(params) {
				return params.data.txnType ? _TxnTypeConstant2.default.getTxnTypeForUI(params.data.txnType, params.data.subTxnType) : 'Opening Balance';
			},
			cellRenderer: function cellRenderer(params) {
				return params.value ? _TxnTypeConstant2.default.getTxnTypeForUI(params.value, params.data.subTxnType) : 'Opening Balance';
			},
			headerName: 'TYPE',
			width: 110
		}, {
			field: 'name',
			filter: _StringFilter2.default,
			headerName: 'NAME',
			width: 180
		}, {
			field: 'date',
			headerName: 'DATE',
			sort: 'desc',
			sortingOrder: ['desc', 'asc', null],
			filter: _DateFilter2.default,
			getQuickFilterText: function getQuickFilterText(params) {
				if (params.value) {
					return _MyDate2.default.getDate('d/m/y', new Date(params.value));
				}
			},
			cellRenderer: function cellRenderer(params) {
				if (params.value) {
					return _MyDate2.default.getDate('d/m/y', new Date(params.value));
				}
			}
		}, {
			field: 'amount',
			filter: _NumberFilter2.default,
			headerName: 'AMOUNT',
			cellClass: 'alignLeft',
			minWidth: 100,
			headerClass: 'alignLeft',
			cellRenderer: function cellRenderer(params) {
				return '<div style="color:' + (params.data['cashColor'] || '') + '">' + _MyDouble2.default.getAmountWithDecimalAndCurrencyWithSign(params.value) + '</div>';
			}
		}, {
			field: 'txnId',
			headerName: '',
			width: 30,
			getQuickFilterText: function getQuickFilterText() {
				return '';
			},
			suppressSorting: true,
			suppressFilter: true,
			cellRenderer: function cellRenderer() {
				return '<div class="contextMenuDots"></div>';
			}
		}];
		_this.contextMenu = [{
			title: 'View/Edit',
			cmd: BankTransactions.viewTransaction,
			visible: true,
			id: 'edit'
		}, {
			title: 'Delete',
			cmd: function cmd(row) {
				return TransactionHelper.deleteTransaction(row.txnId, row.typeTxn);
			},
			visible: true,
			id: 'delete'
		}];
		_this.state = {
			contextMenu: _this.contextMenu
		};
		_this.firstTime = true;
		return _this;
	}

	(0, _createClass3.default)(BankTransactions, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			setTimeout(function () {
				var firstColumn = document.querySelector('.left-column .ag-row-selected .ag-cell[col-id="fullName"]');
				firstColumn && firstColumn.focus();
			}, 500);
		}
	}, {
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(nextProps) {
			if (this.api) {
				this.api.setRowData(nextProps.items);
			}
			return false;
		}
	}, {
		key: 'render',
		value: function render() {
			var addExtraColumns = this.props.addExtraColumns;

			var gridOptions = {
				enableFilter: true,
				enableSorting: true,
				rowSelection: 'single',
				columnDefs: this.columnDefs,
				rowData: [],
				onRowDoubleClicked: BankTransactions.viewTransaction,
				suppressColumnMoveAnimation: true,
				overlayNoRowsTemplate: 'No transactions to show',
				headerHeight: 40,
				rowClass: addExtraColumns ? 'customVerticalBorder' : '',
				rowHeight: 40
			};
			var gridClasses = 'noBorder gridRowHeight40';
			var selectFirstRow = true;
			var quickFilter = false;
			var title = '';
			if (addExtraColumns) {
				gridClasses = 'alternateRowColor customVerticalBorder gridRowHeight40';
				selectFirstRow = false;
				quickFilter = true;
				title = 'Transactions';
			}
			return _react2.default.createElement(
				'div',
				{ className: 'gridContainer d-flex' },
				_react2.default.createElement(_Grid2.default, {
					contextMenu: this.state.contextMenu,
					classes: gridClasses,
					selectFirstRow: selectFirstRow,
					title: title,
					quickFilter: quickFilter,
					height: '100%',
					width: '100%',
					getApi: this.getApi,
					gridOptions: gridOptions
				})
			);
		}
	}], [{
		key: 'viewTransaction',
		value: function viewTransaction(row) {
			if (!row.txnId) {
				row = row.data;
			}
			if (row.txnType == _TxnTypeConstant2.default.TXN_TYPE_BANK_TO_BANK) {
				var BankAdjustmentUtility = require('../../Utilities/BankAdjustmentUtility');
				BankAdjustmentUtility.editBankToBankTransfer(row.txnId);
			} else {
				TransactionHelper.viewTransaction(row.txnId, row.typeTxn);
			}
		}
	}]);
	return BankTransactions;
}(_react2.default.Component);

BankTransactions.propTypes = {
	items: _propTypes2.default.array,
	addExtraColumns: _propTypes2.default.bool
};

exports.default = BankTransactions;