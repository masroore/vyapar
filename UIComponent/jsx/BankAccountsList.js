Object.defineProperty(exports, "__esModule", {
	value: true
});

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _ErrorCode = require('../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

var _ToastHelper = require('../../UIControllers/ToastHelper');

var ToastHelper = _interopRequireWildcard(_ToastHelper);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var BankAccountsList = function (_React$Component) {
	(0, _inherits3.default)(BankAccountsList, _React$Component);

	function BankAccountsList(props) {
		(0, _classCallCheck3.default)(this, BankAccountsList);

		var _this = (0, _possibleConstructorReturn3.default)(this, (BankAccountsList.__proto__ || (0, _getPrototypeOf2.default)(BankAccountsList)).call(this, props));

		_this.selectFirstRow = function () {
			setTimeout(function () {
				var firstColumn = document.querySelector('.left-column .ag-row-selected .ag-cell[col-id="accountName"]');
				firstColumn && firstColumn.focus();
			}, 100);
		};

		_this.getApi = function (params) {
			_this.api = params.api;
			_this.api.setRowData(_this.props.items);
		};

		_this.columnDefs = [{
			field: 'accountName',
			headerName: 'ACCOUNT',
			width: 150,
			sort: 'asc'
		}, {
			field: 'currentBalance',
			suppressMenu: true,
			comparator: function comparator(a, b) {
				return a - b;
			},
			headerName: 'AMOUNT',
			width: 90,
			cellClass: 'alignRight',
			headerClass: 'alignRight',
			cellRenderer: function cellRenderer(params) {
				var value = MyDouble.getBalanceAmountWithDecimalAndCurrency(params.value, true);
				return '<div>' + value + '</div>';
			}
		}, {
			field: 'accountName',
			headerName: '',
			width: 30,
			getQuickFilterText: function getQuickFilterText() {
				return '';
			},
			suppressSorting: true,
			suppressFilter: true,
			cellRenderer: function cellRenderer() {
				return '<div class="contextMenuDots"></div>';
			}
		}];
		_this.state = {
			isOpen: false,
			accountName: ''
		};
		_this.deleteAccount = _this.deleteAccount.bind(_this);
		_this.editBankAccount = _this.editBankAccount.bind(_this);
		_this.contextMenu = [{
			title: 'View/Edit',
			cmd: _this.editBankAccount,
			visible: true,
			id: 'edit'
		}, {
			title: 'Delete',
			cmd: _this.deleteAccount,
			visible: true,
			id: 'delete'
		}];
		return _this;
	}

	(0, _createClass3.default)(BankAccountsList, [{
		key: 'editBankAccount',
		value: function editBankAccount(row) {
			if (!row.accountName) {
				row = row.data;
			}
			window.accountNameGlobal = row.accountName;
			var CommonUtility = require('../../Utilities/CommonUtility');
			CommonUtility.loadFrameDiv('NewBankAccount.html');
		}
	}, {
		key: 'deleteAccount',
		value: function deleteAccount(row) {
			var accountName = row.accountName;
			var PaymentInfoCache = require('../../Cache/PaymentInfoCache.js');
			var paymentInfoCache = new PaymentInfoCache();
			var paymentObj = paymentInfoCache.getPaymentInfoObjOnName(accountName);
			if (paymentObj && paymentObj.canDeletePaymentInfo()) {
				var conf = confirm('Do you want to delete account?');
				if (conf) {
					var statusCode = paymentObj.deletePaymentInfo();
					if (statusCode == _ErrorCode2.default.ERROR_BANK_DELETE_SUCCESS) {
						ToastHelper.success(statusCode);
					} else {
						ToastHelper.error(statusCode);
					}
					window.onResume && window.onResume();
				}
			} else {
				ToastHelper.error(_ErrorCode2.default.ERROR_BANK_USED_IN_TRANSACTION);
			}
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			try {
				var $delDialog2 = $('#delDialog2');
				if ($delDialog2.is(':ui-dialog')) {
					$delDialog2.dialog('close').dialog('destroy');
				}
			} catch (error) {
				var logger = require('../../Utilities/logger');
				logger.error(error);
			}
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.selectFirstRow();
		}
	}, {
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(nextProps) {
			var _this2 = this;

			if (this.api) {
				this.api.setRowData(nextProps.items);
				setTimeout(function () {
					if (_this2.api.getSelectedRows().length === 0) {
						var selected = _this2.api.getDisplayedRowAtIndex(0);
						if (selected) {
							selected.setSelected(true);
						}
					}
				});
			}
			return false;
		}
	}, {
		key: 'render',
		value: function render() {
			var _gridOptions;

			var gridOptions = (_gridOptions = {
				enableSorting: true,
				rowSelection: 'single',
				columnDefs: this.columnDefs,
				rowData: [],
				onRowDoubleClicked: this.editBankAccount,
				suppressColumnMoveAnimation: true,
				onRowSelected: this.props.onItemSelected,
				overlayNoRowsTemplate: 'No bank accounts to show',
				deltaRowDataMode: true,
				headerHeight: 40
			}, (0, _defineProperty3.default)(_gridOptions, 'deltaRowDataMode', true), (0, _defineProperty3.default)(_gridOptions, 'rowHeight', 40), (0, _defineProperty3.default)(_gridOptions, 'getRowNodeId', function getRowNodeId(data) {
				return data.accountId;
			}), _gridOptions);
			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement(
					'div',
					{ className: 'gridContainer d-flex' },
					_react2.default.createElement(_Grid2.default, {
						contextMenu: this.contextMenu,
						classes: 'noBorder gridRowHeight40',
						selectFirstRow: true,
						height: '100%',
						getApi: this.getApi,
						gridOptions: gridOptions
					})
				)
			);
		}
	}]);
	return BankAccountsList;
}(_react2.default.Component);

BankAccountsList.propTypes = {
	onItemSelected: _propTypes2.default.func,
	items: _propTypes2.default.array
};

exports.default = BankAccountsList;