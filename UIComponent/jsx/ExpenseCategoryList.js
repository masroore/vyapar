Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _AddEditCategoryModal = require('./AddEditCategoryModal');

var _AddEditCategoryModal2 = _interopRequireDefault(_AddEditCategoryModal);

var _ToastHelper = require('../../UIControllers/ToastHelper');

var ToastHelper = _interopRequireWildcard(_ToastHelper);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ExpenseCategoryList = function (_React$Component) {
	(0, _inherits3.default)(ExpenseCategoryList, _React$Component);

	function ExpenseCategoryList(props) {
		(0, _classCallCheck3.default)(this, ExpenseCategoryList);

		var _this = (0, _possibleConstructorReturn3.default)(this, (ExpenseCategoryList.__proto__ || (0, _getPrototypeOf2.default)(ExpenseCategoryList)).call(this, props));

		_this.selectFirstRow = function () {
			setTimeout(function () {
				var firstColumn = document.querySelector('.left-column .ag-row-selected .ag-cell[col-id="categoryName"]');
				firstColumn && firstColumn.focus();
			}, 100);
		};

		_this.getApi = function (params) {
			_this.api = params.api;
			_this.api.setRowData(_this.props.items);
		};

		_this.columnDefs = [{
			field: 'categoryName',
			headerName: 'CATEGORY',
			width: 150,
			sort: 'asc'
		}, {
			field: 'categoryAmount',
			suppressMenu: true,
			comparator: function comparator(a, b) {
				return a - b;
			},
			headerName: 'AMOUNT',
			width: 90,
			cellClass: 'alignRight',
			headerClass: 'alignRight'
		}, {
			field: 'categoryName',
			headerName: '',
			width: 30,
			getQuickFilterText: function getQuickFilterText() {
				return '';
			},
			suppressSorting: true,
			suppressFilter: true,
			cellRenderer: function cellRenderer() {
				return '<div class="contextMenuDots"></div>';
			}
		}];
		_this.state = {
			isOpen: false,
			categoryName: ''
		};
		_this.deleteItem = _this.deleteItem.bind(_this);
		_this.editExpenseCategory = _this.editExpenseCategory.bind(_this);
		_this.onSave = _this.onSave.bind(_this);
		_this.onClose = _this.onClose.bind(_this);
		_this.contextMenu = [{
			title: 'View/Edit',
			cmd: _this.editExpenseCategory,
			visible: true,
			id: 'edit'
		}, {
			title: 'Delete',
			cmd: _this.deleteItem,
			visible: true,
			id: 'delete'
		}];
		return _this;
	}

	(0, _createClass3.default)(ExpenseCategoryList, [{
		key: 'editExpenseCategory',
		value: function editExpenseCategory(row) {
			if (!row.categoryName) {
				row = row.data;
			}
			if (row.isCategoryLoanType) {
				ToastHelper.info('Cannot edit this category');
				return;
			}
			var MyAnalytics = require('../../Utilities/analyticsHelper');
			MyAnalytics.pushEvent('Edit Expense Category Open');
			this.setState({ isOpen: true, categoryName: row.categoryName });
		}
	}, {
		key: 'onSave',
		value: function onSave(name) {
			var oldName = this.state.categoryName;

			var ErrorCode = require('../../Constants/ErrorCode');
			var NameCache = require('../../Cache/NameCache.js');
			var statusCode = ErrorCode.ERROR_NAME_SAVE_FAILED;
			if (name) {
				var namecache = new NameCache();
				var categoryObj = namecache.findExpenseModelByName(oldName);
				if (categoryObj) {
					statusCode = categoryObj.updateExpenseName(name);
				}
				if (statusCode == ErrorCode.ERROR_NAME_SAVE_SUCCESS) {
					ToastHelper.success(statusCode);
				} else {
					ToastHelper.error(statusCode);
				}
				this.setState({
					isOpen: false,
					categoryName: name
				});
				window.onResume && window.onResume();
			} else {
				ToastHelper.error('Expense category name cannot be empty');
			}
		}
	}, {
		key: 'onClose',
		value: function onClose() {
			this.setState({ isOpen: false });
		}
	}, {
		key: 'deleteItem',
		value: function deleteItem(row) {
			if (row.isCategoryLoanType) {
				ToastHelper.info('Cannot delete this category');
				return;
			}
			var NameCache = require('../../Cache/NameCache.js');
			var nameCache = new NameCache();
			var categoryName = row.categoryName;
			var categoryObj = nameCache.findExpenseModelByName(categoryName);
			if (categoryObj && categoryObj.canDeleteParty()) {
				var conf = confirm('Do you want to delete the expense category ?');
				if (conf) {
					var statusCode = categoryObj.deleteName();
					if (statusCode == ErrorCode.ERROR_NAME_DELETE_SUCCESS) {
						statusCode = ErrorCode.ERROR_EXPENSE_DELETE_SUCCESS;
					}
					if (statusCode == ErrorCode.ERROR_EXPENSE_DELETE_SUCCESS) {
						window.onResume && window.onResume();
						ToastHelper.success(statusCode);
					} else {
						ToastHelper.error(statusCode);
					}
				}
			} else {
				ToastHelper.error(ErrorCode.ERROR_EXPENSE_DELETE_FAILED);
			}
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			try {
				var $delDialog2 = $('#delDialog2');
				if ($delDialog2.is(':ui-dialog')) {
					$delDialog2.dialog('close').dialog('destroy');
				}
			} catch (error) {
				var logger = require('../../Utilities/logger');
				logger.error(error);
			}
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.selectFirstRow();
		}
	}, {
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(nextProps, nextState) {
			var _this2 = this;

			var isOpen = this.state.isOpen;

			if (this.api) {
				this.api.setRowData(nextProps.items);
				setTimeout(function () {
					if (_this2.api.getSelectedRows().length === 0) {
						var selected = _this2.api.getDisplayedRowAtIndex(0);
						if (selected) {
							selected.setSelected(true);
						}
					}
				});
			}
			return isOpen != nextState.isOpen;
		}
	}, {
		key: 'render',
		value: function render() {
			var _state = this.state,
			    isOpen = _state.isOpen,
			    categoryName = _state.categoryName;

			var gridOptions = {
				enableSorting: true,
				rowSelection: 'single',
				columnDefs: this.columnDefs,
				rowData: [],
				onRowDoubleClicked: this.editExpenseCategory,
				suppressColumnMoveAnimation: true,
				onRowSelected: this.props.onItemSelected,
				overlayNoRowsTemplate: 'No expense categories to show',
				headerHeight: 40,
				rowHeight: 40,
				deltaRowDataMode: true,
				getRowNodeId: function getRowNodeId(data) {
					if (data.categoryId == 'ProcessingFeeCategory') {
						return 0;
					} else if (data.isCategoryLoanType) {
						return -data.categoryId;
					} else {
						return data.categoryId;
					}
				}
			};
			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				isOpen && _react2.default.createElement(_AddEditCategoryModal2.default, {
					categoryName: categoryName,
					onClose: this.onClose,
					onSave: this.onSave,
					isOpen: isOpen,
					title: 'Edit Expense Category',
					label: 'Change Expense Category Name'
				}),
				_react2.default.createElement(
					'div',
					{ className: 'gridContainer d-flex' },
					_react2.default.createElement(_Grid2.default, {
						contextMenu: this.contextMenu,
						classes: 'noBorder gridRowHeight40',
						selectFirstRow: true,
						height: '100%',
						gridOptions: gridOptions,
						getApi: this.getApi
					})
				)
			);
		}
	}]);
	return ExpenseCategoryList;
}(_react2.default.Component);

ExpenseCategoryList.propTypes = {
	onItemSelected: _propTypes2.default.func,
	items: _propTypes2.default.array
};

exports.default = ExpenseCategoryList;