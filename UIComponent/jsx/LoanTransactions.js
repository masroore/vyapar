Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _DateFilter = require('./grid-filters/DateFilter');

var _DateFilter2 = _interopRequireDefault(_DateFilter);

var _NumberFilter = require('./grid-filters/NumberFilter');

var _NumberFilter2 = _interopRequireDefault(_NumberFilter);

var _MultiSelectionFilter = require('./grid-filters/MultiSelectionFilter');

var _MultiSelectionFilter2 = _interopRequireDefault(_MultiSelectionFilter);

var _MyDate = require('../../Utilities/MyDate');

var _MyDate2 = _interopRequireDefault(_MyDate);

var _TxnTypeConstant = require('../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LoanTransactions = function (_React$Component) {
	(0, _inherits3.default)(LoanTransactions, _React$Component);

	function LoanTransactions(props) {
		(0, _classCallCheck3.default)(this, LoanTransactions);

		var _this = (0, _possibleConstructorReturn3.default)(this, (LoanTransactions.__proto__ || (0, _getPrototypeOf2.default)(LoanTransactions)).call(this, props));

		_this.viewTransaction = function (row) {
			if (!row.loanTxnId) {
				row = row.data;
			}
			_this.props.openTransaction && _this.props.openTransaction(row.loanTxnId, row.loanTxnType);
		};

		_this.getApi = function (params) {
			_this.api = params.api;
			_this.api.setRowData(_this.props.items);
		};

		_this.columnDefs = [
		// {
		// 	field: 'color',
		// 	headerName: '',
		// 	width: 30,
		// 	suppressMenu: true,
		// 	getQuickFilterText: function () {
		// 		return '';
		// 	},
		// 	suppressFilter: true,
		// 	cellRenderer: function (params) {
		// 		const color = ColorConstants[params.data.txnType];
		// 		return `<div style='background-color: ${color}; border-radius:50%;width:8px;height:8px; top: 19px; position:relative;'></div>`;
		// 	}
		// },
		{
			field: 'loanTxnType',
			filter: _MultiSelectionFilter2.default,
			filterOptions: _TxnTypeConstant2.default.getFilterOptionsForLoanAccounts(),
			getQuickFilterText: function getQuickFilterText(params) {
				return params.data.loanTxnType ? _TxnTypeConstant2.default.getTxnTypeForUI(params.data.loanTxnType) : '';
			},
			cellRenderer: function cellRenderer(params) {
				return params.value ? _TxnTypeConstant2.default.getTxnTypeForUI(params.value) : '';
			},
			headerName: 'TYPE',
			width: 110
		}, {
			field: 'date',
			headerName: 'DATE',
			sort: 'desc',
			sortingOrder: ['desc', 'asc', null],
			filter: _DateFilter2.default,
			valueGetter: function valueGetter(params) {
				var date = params.data.date;

				if (date) {
					var val = new Date(date);
					val.setHours(0, 0, 0, 0);
					return val;
				}
			},
			getQuickFilterText: function getQuickFilterText(params) {
				if (params.value) {
					return _MyDate2.default.getDate('d/m/y', new Date(params.value));
				}
			},
			cellRenderer: function cellRenderer(params) {
				if (params.value) {
					return _MyDate2.default.getDate('d/m/y', new Date(params.value));
				}
			}
		}, {
			field: 'principalAmount',
			filter: _NumberFilter2.default,
			headerName: 'PRINCIPAL',
			cellClass: 'alignLeft',
			minWidth: 100,
			headerClass: 'alignLeft',
			cellRenderer: function cellRenderer(params) {
				return '<div style="color:' + (params.data['cashColor'] || '') + '">' + _MyDouble2.default.getAmountWithDecimalAndCurrencyWithSign(params.value) + '</div>';
			}
		}, {
			field: 'interestAmount',
			filter: _NumberFilter2.default,
			headerName: 'INTEREST',
			cellClass: 'alignLeft',
			minWidth: 100,
			headerClass: 'alignLeft',
			cellRenderer: function cellRenderer(params) {
				return '<div style="color:' + (params.data['cashColor'] || '') + '">' + _MyDouble2.default.getAmountWithDecimalAndCurrencyWithSign(params.value) + '</div>';
			}
		}, {
			field: 'totalAmount',
			filter: _NumberFilter2.default,
			headerName: 'TOTAL AMOUNT',
			cellClass: 'alignLeft',
			minWidth: 100,
			headerClass: 'alignLeft',
			valueGetter: function valueGetter(params) {
				var _params$data = params.data,
				    _params$data$principa = _params$data.principalAmount,
				    principalAmount = _params$data$principa === undefined ? 0 : _params$data$principa,
				    _params$data$interest = _params$data.interestAmount,
				    interestAmount = _params$data$interest === undefined ? 0 : _params$data$interest;

				return Number(principalAmount) + Number(interestAmount);
			},
			cellRenderer: function cellRenderer(params) {
				var totalAmount = Number(params.data['principalAmount']) + Number(params.data['interestAmount']);
				return '<div style="color:' + (params.data['cashColor'] || '') + '">' + _MyDouble2.default.getAmountWithDecimalAndCurrencyWithSign(totalAmount) + '</div>';
			}
		}, {
			field: 'loanTxnId',
			headerName: '',
			width: 30,
			getQuickFilterText: function getQuickFilterText() {
				return '';
			},
			suppressSorting: true,
			suppressFilter: true,
			cellRenderer: function cellRenderer() {
				return '<div class="contextMenuDots"></div>';
			}
		}];
		_this.contextMenu = [{
			title: 'View/Edit',
			cmd: _this.viewTransaction,
			visible: true,
			id: 'edit'
		}, {
			title: 'Delete',
			cmd: _this.deleteTransaction,
			visible: true,
			id: 'delete'
		}];
		_this.state = {
			contextMenu: _this.contextMenu
		};
		return _this;
	}

	(0, _createClass3.default)(LoanTransactions, [{
		key: 'deleteTransaction',
		value: function deleteTransaction(row) {
			if (!row.loanTxnId) {
				row = row.data;
			}
			var loanTxnType = row.loanTxnType;
			if (loanTxnType == _TxnTypeConstant2.default.TXN_TYPE_LOAN_EMI_PAYMENT || loanTxnType == _TxnTypeConstant2.default.TXN_TYPE_LOAN_ADJUSTMENT) {
				if (loanTxnType == _TxnTypeConstant2.default.TXN_TYPE_LOAN_ADJUSTMENT) {
					var LoanAccountCache = require('../../Cache/LoanAccountCache');
					var loanAccountCache = new LoanAccountCache();
					var loanAccountId = row.loanAccountId;
					var loanAccount = loanAccountCache.getLoanAccountById(loanAccountId);
					if (Number(loanAccount.getCurrentBalance()) < Number(row.principalAmount)) {
						ToastHelper.error(ErrorCode.ERROR_LOAN_TRANSACTION_DELETE_FAILED);
						return;
					}
				}
				var conf = confirm('Do you want to delete transaction?');
				if (conf) {
					var LoanTransaction = require('../../Models/LoanTransactionModel');
					var loanTransaction = new LoanTransaction({
						loanTxnId: row.loanTxnId
					});
					var statusCode = loanTransaction.deleteLoanTxn();
					if (statusCode == ErrorCode.ERROR_LOAN_TRANSACTION_DELETE_SUCCESS) {
						ToastHelper.success(statusCode);
					} else {
						ToastHelper.error(statusCode);
					}
					window.onResume && window.onResume();
				}
			} else {
				ToastHelper.error(ErrorCode.ERROR_LOAN_TRANSACTION_DELETE_FAILED);
			}
		}
	}, {
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(nextProps) {
			if (this.api) {
				this.api.setRowData(nextProps.items);
			}
			return false;
		}
	}, {
		key: 'render',
		value: function render() {
			var addExtraColumns = this.props.addExtraColumns;

			var gridOptions = {
				enableFilter: true,
				enableSorting: true,
				rowSelection: 'single',
				columnDefs: this.columnDefs,
				rowData: [],
				onRowDoubleClicked: this.viewTransaction,
				suppressColumnMoveAnimation: true,
				overlayNoRowsTemplate: 'No transactions to show',
				headerHeight: 40,
				rowClass: addExtraColumns ? 'customVerticalBorder' : '',
				rowHeight: 40,
				getRowNodeId: function getRowNodeId(data) {
					return data.loanTxnId;
				}
			};
			var gridClasses = 'noBorder gridRowHeight40';
			var selectFirstRow = true;
			var quickFilter = false;
			var title = '';
			if (addExtraColumns) {
				gridClasses = 'alternateRowColor customVerticalBorder gridRowHeight40';
				selectFirstRow = false;
				quickFilter = true;
				title = 'Transactions';
			}
			return _react2.default.createElement(
				'div',
				{ className: 'gridContainer d-flex' },
				_react2.default.createElement(_Grid2.default, {
					contextMenu: this.state.contextMenu,
					classes: gridClasses,
					selectFirstRow: selectFirstRow,
					title: title,
					quickFilter: quickFilter,
					height: '100%',
					width: '100%',
					gridOptions: gridOptions,
					getApi: this.getApi
				})
			);
		}
	}]);
	return LoanTransactions;
}(_react2.default.Component);

LoanTransactions.propTypes = {
	items: _propTypes2.default.array,
	addExtraColumns: _propTypes2.default.bool,
	openTransaction: _propTypes2.default.func
};

exports.default = LoanTransactions;