Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function RightNavItemsTab(props) {
	return _react2.default.createElement(
		'li',
		{
			className: props.class ? props.class : 'changeDefaultPageSP ripple',
			id: props.id,
			'data-eventname': props.navItemName
		},
		_react2.default.createElement(
			'span',
			{ className: 'textInsideShortcutMenu' },
			props.navItemName
		),
		_react2.default.createElement(
			'span',
			{ className: 'shortcutTextKeys' },
			_react2.default.createElement(
				'span',
				null,
				'(ALT + ' + props.keys + ')'
			)
		)
	);
} // 'use babel';

var RightSideNav = function (_React$Component) {
	(0, _inherits3.default)(RightSideNav, _React$Component);

	function RightSideNav() {
		(0, _classCallCheck3.default)(this, RightSideNav);
		return (0, _possibleConstructorReturn3.default)(this, (RightSideNav.__proto__ || (0, _getPrototypeOf2.default)(RightSideNav)).apply(this, arguments));
	}

	(0, _createClass3.default)(RightSideNav, [{
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				'div',
				{ className: 'shortcuts', id: 'shortcuts' },
				_react2.default.createElement(
					'div',
					{ className: 'shortcutHeader' },
					_react2.default.createElement(
						'div',
						{
							className: 'shortcutToggler' + (!this.props.isFirstSaleTxnCreated ? ' reverseDirection' : '')
						},
						_react2.default.createElement(
							'svg',
							{
								fill: '#e35959',
								height: '24',
								viewBox: '0 0 24 24',
								width: '24',
								xmlns: 'http://www.w3.org/2000/svg'
							},
							_react2.default.createElement('path', { d: 'M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z' }),
							_react2.default.createElement('path', { d: 'M0-.25h24v24H0z', fill: 'none' })
						)
					),
					_react2.default.createElement(
						'span',
						{
							className: 'shortcutTitle' + (!this.props.isFirstSaleTxnCreated ? ' hide' : '')
						},
						'Shortcuts'
					)
				),
				_react2.default.createElement(
					'ul',
					{
						className: 'shortcutsList' + (!this.props.isFirstSaleTxnCreated ? ' hide' : ''),
						id: 'shortcutsList'
					},
					_react2.default.createElement(RightNavItemsTab, { id: 'TXN_TYPE_SALE', keys: 'S', navItemName: 'Sale' }),
					_react2.default.createElement(RightNavItemsTab, {
						id: 'TXN_TYPE_PURCHASE',
						keys: 'P',
						navItemName: 'Purchase'
					}),
					_react2.default.createElement(RightNavItemsTab, {
						id: 'TXN_TYPE_CASHIN',
						navItemName: 'Payment-In',
						keys: 'I'
					}),
					_react2.default.createElement(RightNavItemsTab, {
						id: 'TXN_TYPE_CASHOUT',
						navItemName: 'Payment-Out',
						keys: 'O'
					}),
					_react2.default.createElement(RightNavItemsTab, {
						id: 'TXN_TYPE_EXPENSE',
						navItemName: 'Expense',
						keys: 'E'
					}),
					_react2.default.createElement(RightNavItemsTab, {
						id: 'NewContact.html',
						'class': 'changeDefaultPageOther ripple',
						navItemName: 'Add Party',
						keys: 'N'
					}),
					_react2.default.createElement(RightNavItemsTab, {
						id: 'NewItems.html',
						'class': 'addItemHiddenForItemSetting changeDefaultPageOther ripple hide',
						navItemName: 'Add Item',
						keys: 'A'
					}),
					_react2.default.createElement(RightNavItemsTab, {
						id: 'TXN_TYPE_SALE_ORDER',
						navItemName: 'Sale Order',
						keys: 'F'
					}),
					_react2.default.createElement(RightNavItemsTab, {
						id: 'TXN_TYPE_PURCHASE_ORDER',
						navItemName: 'Purchase Order',
						keys: 'G'
					}),
					_react2.default.createElement(RightNavItemsTab, {
						id: 'TXN_TYPE_DELIVERY_CHALLAN',
						keys: 'D',
						navItemName: 'Delivery Challan'
					}),
					_react2.default.createElement(RightNavItemsTab, {
						id: 'TXN_TYPE_ESTIMATE',
						navItemName: 'Estimate',
						keys: 'M'
					}),
					_react2.default.createElement(RightNavItemsTab, {
						id: 'TXN_TYPE_SALE_RETURN',
						navItemName: 'Cr. Note/ Sale Return',
						keys: 'R'
					}),
					_react2.default.createElement(RightNavItemsTab, {
						id: 'TXN_TYPE_PURCHASE_RETURN',
						navItemName: 'Dr. Note/ Purchase Return',
						keys: 'L'
					}),
					_react2.default.createElement(RightNavItemsTab, {
						id: 'NewBankAccount.html',
						navItemName: 'Add Bank Account',
						keys: 'B'
					}),
					_react2.default.createElement(RightNavItemsTab, {
						id: 'TXN_TYPE_OTHER_INCOME',
						navItemName: 'Other Income',
						keys: 'Q'
					})
				)
			);
		}
	}]);
	return RightSideNav;
}(_react2.default.Component);

exports.default = RightSideNav;