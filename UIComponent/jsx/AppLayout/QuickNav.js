Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles() {
	return {
		quickNav: {
			height: '100%',
			minWidth: '100%',
			fontFamily: 'roboto',
			background: 'white',
			borderRadius: '8px',
			'& .shortcutsBody': {
				flex: '1',
				display: 'flex',
				flexWrap: 'wrap',
				padding: '0 10px 10px 10px',
				marginBottom: '20px',
				'& .shortcutDiv': {
					flex: 1,
					marginLeft: '2%'
				},
				'& .shortcutHeader': {
					color: '#2E2F32',
					fontSize: '16px',
					fontWeight: 'bold',
					fontStyle: 'normal',
					fontFamily: 'robotoBold',
					marginLeft: '9px',
					marginTop: '15px',
					marginBottom: '10px'
				},
				'& .shortcutsList': {
					display: 'flex',
					flexDirection: 'column',
					minWidth: '210px',
					'& .quickNavItem': {
						display: 'flex',
						justifyContent: 'space-between',
						margin: '8px',
						color: '#292526',
						cursor: 'pointer',
						padding: '5px 0',
						outline: 'none',
						'& .quickNavItem-title': {
							fontSize: '14px',
							fontFamily: 'roboto',
							lineHeight: '17px',
							display: 'flex',
							alignItems: 'center'
						},
						'& .quickNavItem-shortcut': {
							fontSize: '12px',
							fontFamily: 'roboto',
							lineHeight: '14px',
							opacity: '.68'
						},
						'& .quickNavItem-subtext': {
							fontSize: '10px',
							color: '#AAAAAA',
							textAlign: 'left',
							paddingTop: '5px'
						},
						'&:hover, &:focus': {
							borderBottom: '2px solid #1492E6',
							marginBottom: '6px', // Adjusting margin to accommodate borderBottom (to stop divs from moving on hover)
							'& .quickNavItem-title': {
								color: '#0075E8'
							}
						}
					}
				}
			},
			'& .quickNav-footer': {
				height: '40px',
				background: '#FFE494',
				color: '#785F16',
				fontSize: '14px',
				lineHeight: '19px',
				fontFamily: 'Roboto',
				padding: '8px',
				textAlign: 'left',
				paddingLeft: '20px',
				position: 'sticky',
				borderBottomLeftRadius: '5px',
				borderBottomRightRadius: '5px'
			}
		}
	};
};

var NavItem = function NavItem(_ref) {
	var navItemName = _ref.navItemName,
	    subText = _ref.subText,
	    keys = _ref.keys,
	    id = _ref.id,
	    handleClick = _ref.handleClick,
	    classes = _ref.classes;

	return _react2.default.createElement(
		'li',
		{
			className: classes,
			id: id,
			tabIndex: '0',
			'data-eventname': navItemName,
			onClick: function onClick() {
				handleClick && handleClick();
			},
			onKeyDown: function onKeyDown(event) {
				if (!event.ctrlKey && event.keyCode == 13) {
					$(event.target).click();
				}
			}
		},
		_react2.default.createElement(
			'div',
			{ className: 'quickNavItem-title' },
			_react2.default.createElement(
				'div',
				{ className: 'ht-100' },
				_react2.default.createElement(
					'span',
					{ className: 'triangle-right' },
					_react2.default.createElement(
						'svg',
						{ width: '7', height: '9', viewBox: '0 0 7 9', fill: 'none', xmlns: 'http://www.w3.org/2000/svg' },
						_react2.default.createElement('path', { d: 'M6.5 3.13397C7.16667 3.51887 7.16667 4.48113 6.5 4.86603L2 7.4641C1.33333 7.849 0.5 7.36788 0.5 6.59808L0.5 1.40192C0.5 0.632124 1.33333 0.150998 2 0.535898L6.5 3.13397Z', fill: '#0075E8' })
					)
				)
			),
			_react2.default.createElement(
				'div',
				{ className: 'subTextMenu' },
				_react2.default.createElement(
					'div',
					null,
					navItemName
				),
				!!subText && _react2.default.createElement(
					'div',
					{ className: 'quickNavItem-subtext' },
					subText
				)
			)
		),
		_react2.default.createElement(
			'div',
			{ className: 'quickNavItem-shortcut' },
			_react2.default.createElement(
				'div',
				{ className: 'shortcutText' },
				'ALT + ' + keys
			)
		)
	);
};

var QuickNav = function QuickNav(_ref2) {
	var classes = _ref2.classes,
	    handleClick = _ref2.handleClick;

	return _react2.default.createElement(
		'div',
		{ className: (0, _classnames2.default)(classes.quickNav, 'shortcuts'), id: 'shortcuts' },
		_react2.default.createElement('span', { id: 'addMoreSpan' }),
		_react2.default.createElement(
			'div',
			{ className: 'shortcutsBody' },
			_react2.default.createElement(
				'div',
				{ className: 'saleShortcuts shortcutDiv shortcutDiv' },
				_react2.default.createElement(
					'div',
					{ className: 'shortcutHeader' },
					'SALE'
				),
				_react2.default.createElement(
					'ul',
					{ className: 'shortcutsList' },
					_react2.default.createElement(NavItem, {
						classes: 'quickNavItem changeDefaultPageSP',
						navItemName: 'Sale Invoice',
						id: 'TXN_TYPE_SALE',
						keys: 'S',
						handleClick: handleClick
					}),
					_react2.default.createElement(NavItem, {
						classes: 'quickNavItem changeDefaultPageSP',
						navItemName: 'Payment-In',
						id: 'TXN_TYPE_CASHIN',
						keys: 'I',
						handleClick: handleClick
					}),
					_react2.default.createElement(NavItem, {
						classes: 'quickNavItem changeDefaultPageSP',
						navItemName: 'Sale Return',
						subText: 'Cr Note',
						id: 'TXN_TYPE_SALE_RETURN',
						handleClick: handleClick,
						keys: 'R'
					}),
					_react2.default.createElement(NavItem, {
						classes: 'quickNavItem changeDefaultPageSP',
						navItemName: 'Sale Order',
						id: 'TXN_TYPE_SALE_ORDER',
						keys: 'F',
						handleClick: handleClick
					}),
					_react2.default.createElement(NavItem, {
						classes: 'quickNavItem changeDefaultPageSP',
						navItemName: 'Estimate/Quotation',
						id: 'TXN_TYPE_ESTIMATE',
						keys: 'M',
						handleClick: handleClick
					}),
					_react2.default.createElement(NavItem, {
						classes: 'quickNavItem changeDefaultPageSP',
						navItemName: 'Delivery Challan',
						id: 'TXN_TYPE_DELIVERY_CHALLAN',
						keys: 'D',
						handleClick: handleClick
					})
				)
			),
			_react2.default.createElement(
				'div',
				{ className: 'purchaseShortcuts shortcutDiv' },
				_react2.default.createElement(
					'div',
					{ className: 'shortcutHeader' },
					'PURCHASE'
				),
				_react2.default.createElement(
					'ul',
					{ className: 'shortcutsList' },
					_react2.default.createElement(NavItem, {
						classes: 'quickNavItem changeDefaultPageSP',
						navItemName: 'Purchase Bill',
						id: 'TXN_TYPE_PURCHASE',
						keys: 'P',
						handleClick: handleClick
					}),
					_react2.default.createElement(NavItem, {
						classes: 'quickNavItem changeDefaultPageSP',
						navItemName: 'Payment-Out',
						id: 'TXN_TYPE_CASHOUT',
						keys: 'O',
						handleClick: handleClick
					}),
					_react2.default.createElement(NavItem, {
						classes: 'quickNavItem changeDefaultPageSP',
						navItemName: 'Purchase Return',
						id: 'TXN_TYPE_PURCHASE_RETURN',
						subText: 'Dr Note',
						handleClick: handleClick,
						keys: 'L'
					}),
					_react2.default.createElement(NavItem, {
						classes: 'quickNavItem changeDefaultPageSP',
						navItemName: 'Purchase Order',
						id: 'TXN_TYPE_PURCHASE_ORDER',
						keys: 'G',
						handleClick: handleClick
					})
				)
			),
			_react2.default.createElement(
				'div',
				{ className: 'otherShortcuts shortcutDiv' },
				_react2.default.createElement(
					'div',
					{ className: 'shortcutHeader' },
					'OTHERS'
				),
				_react2.default.createElement(
					'ul',
					{ className: 'shortcutsList' },
					_react2.default.createElement(NavItem, {
						classes: 'quickNavItem changeDefaultPageSP',
						navItemName: 'Expenses',
						id: 'TXN_TYPE_EXPENSE',
						keys: 'E',
						handleClick: handleClick
					}),
					_react2.default.createElement(NavItem, {
						classes: 'quickNavItem changeDefaultPageSP',
						navItemName: 'Other Income',
						id: 'TXN_TYPE_OTHER_INCOME',
						keys: 'Q',
						handleClick: handleClick
					})
				)
			)
		),
		_react2.default.createElement(
			'div',
			{ className: 'quickNav-footer' },
			'Shortcut to open this menu : ',
			_react2.default.createElement(
				'button',
				{ className: 'shortCutKeyQuickNav' },
				'Ctrl'
			),
			' + ',
			_react2.default.createElement(
				'button',
				{ className: 'shortCutKeyQuickNav' },
				' Enter'
			)
		)
	);
};

QuickNav.propTypes = {
	classes: _propTypes2.default.object,
	handleClick: _propTypes2.default.func
};
NavItem.propTypes = {
	classes: _propTypes2.default.string,
	handleClick: _propTypes2.default.func,
	navItemName: _propTypes2.default.string,
	subText: _propTypes2.default.string,
	keys: _propTypes2.default.string,
	id: _propTypes2.default.string
};
exports.default = (0, _styles.withStyles)(styles)(QuickNav);