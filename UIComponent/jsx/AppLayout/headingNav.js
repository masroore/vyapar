Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _headingNavButtons = require('./headingNavButtons');

var _headingNavButtons2 = _interopRequireDefault(_headingNavButtons);

var _CommonUtility = require('../../../Utilities/CommonUtility');

var _CommonUtility2 = _interopRequireDefault(_CommonUtility);

var _throttleDebounce = require('throttle-debounce');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var HeadingNav = function (_React$Component) {
	(0, _inherits3.default)(HeadingNav, _React$Component);

	function HeadingNav(props) {
		(0, _classCallCheck3.default)(this, HeadingNav);

		var _this = (0, _possibleConstructorReturn3.default)(this, (HeadingNav.__proto__ || (0, _getPrototypeOf2.default)(HeadingNav)).call(this, props));

		_this.loadDefaultPage = function (url) {
			return function () {
				_CommonUtility2.default.loadDefaultPage(url);
			};
		};

		_this.handleSearchKeyPress = (0, _throttleDebounce.debounce)(500, _this.handleSearchKeyPress);
		return _this;
	}

	(0, _createClass3.default)(HeadingNav, [{
		key: 'handleSearchKeyPress',
		value: function handleSearchKeyPress(event) {
			if (event.keyCode !== 9 && event.keyCode !== 13) {
				var defaultPage = $('#defaultPage');
				try {
					if (!canCloseDialogue || canCloseDialogue()) {
						var modelContainer = $('#modelContainer');
						defaultPage.load('SearchResults.html');
						$('#titleText').innerHTML = $('#search-anything').val();
						modelContainer.hide();
						defaultPage.show();
					}
				} catch (err) {
					$('#defaultPage').load('SearchResults.html');
				}
			}
		}
	}, {
		key: 'openCloseSettings',
		value: function openCloseSettings() {
			var Settings = require('../Settings/Settings').default;
			var mountComponent = require('../MountComponent').default;
			mountComponent(Settings, document.querySelector('#frameDiv'));
		}
	}, {
		key: 'componentDidUpdate',
		value: function componentDidUpdate(prevProps) {
			if (!prevProps.isFirstSaleTxnCreated && this.props.isFirstSaleTxnCreated) {
				updateCompanyInfo();
			}
		}
	}, {
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement(
					'div',
					{ className: 'headingNav', id: 'headingNav' },
					!this.props.isFirstSaleTxnCreated && _react2.default.createElement(
						'div',
						{ className: 'welcomeDiv' },
						_react2.default.createElement(
							'div',
							{ className: 'welcomeText' },
							'Welcome to'
						),
						_react2.default.createElement('img', { className: 'welcomeLogo', src: './../inlineSVG/vyapar_logo.svg', alt: 'vyapar' })
					),
					_react2.default.createElement(
						'div',
						{ className: 'businessNameDiv hide' },
						_react2.default.createElement('input', { type: 'text',
							className: 'enterBusinessName noBorderBottom',
							placeholder: 'Enter Business Name'
						}),
						_react2.default.createElement('input', { type: 'button', className: 'saveBusinessName hide', value: 'Save' }),
						_react2.default.createElement('div', { className: 'dot' })
					),
					this.props.isFirstSaleTxnCreated && _react2.default.createElement(
						'div',
						{ className: 'searchField' },
						_react2.default.createElement(
							'div',
							{ className: 'searchBar', id: 'search-anything-div' },
							_react2.default.createElement(
								'div',
								{ className: 'svgIconLeft' },
								_react2.default.createElement(
									'svg',
									{
										fill: '#000000',
										height: '24',
										viewBox: '0 0 24 24',
										width: '24',
										xmlns: 'http://www.w3.org/2000/svg'
									},
									_react2.default.createElement('path', {
										className: 'searchIcon',
										fill: '#9e9e9e',
										d: 'M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z'
									}),
									_react2.default.createElement('path', { d: 'M0 0h24v24H0z', fill: 'none' })
								)
							),
							_react2.default.createElement('input', {
								type: 'search',
								id: 'search-anything',
								placeholder: 'Search Transactions',
								onKeyUp: this.handleSearchKeyPress,
								tabIndex: '-1'
							})
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'right-menu-container' },
						_react2.default.createElement(
							'div',
							{
								className: 'menuItems col outboxContainer hide',
								id: 'outboxContainer'
							},
							_react2.default.createElement(
								'li',
								{ className: 'pointer', id: 'outboxIcon', accessKey: '/' },
								_react2.default.createElement(
									'div',
									{ className: 'messageCounter hide' },
									_react2.default.createElement(
										'div',
										{ className: 'messageCount' },
										'0'
									)
								),
								_react2.default.createElement('img', { src: './../inlineSVG/outbox.svg', alt: 'Outbox' })
							),
							_react2.default.createElement('div', {
								id: 'outboxDialog',
								className: 'hide',
								title: 'Unsent SMS'
							})
						),
						this.props.isFirstSaleTxnCreated && _react2.default.createElement(_headingNavButtons2.default, null),
						this.props.isFirstSaleTxnCreated && _react2.default.createElement('span', { className: 'divider' }),
						this.props.canShowPaymentReminderInHeader && _react2.default.createElement(
							'div',
							{ className: 'menuItems' },
							_react2.default.createElement(
								'li',
								{
									className: 'changeDefaultPageOther pointer',
									id: 'PaymentReminder.html'
								},
								_react2.default.createElement('img', {
									src: './../inlineSVG/payment-reminder.svg',
									className: 'bellIcon',
									alt: 'reminder for svg'
								})
							)
						),
						this.props.isFirstSaleTxnCreated && _react2.default.createElement(
							'div',
							{ className: 'menuItems settingsIcon' },
							_react2.default.createElement(
								'li',
								{
									className: 'pointer',
									onClick: this.openCloseSettings.bind(this)
								},
								_react2.default.createElement('img', {
									src: './../inlineSVG/settings-icon.svg',
									alt: 'settings-icon',
									id: this.props.canShowPrintCentreInHeader ? "" : 'settingCustomMargin'
								})
							)
						),
						this.props.canShowPrintCentreInHeader && _react2.default.createElement(
							'div',
							{ className: 'menuItems' },
							_react2.default.createElement(
								'li',
								{ onClick: this.loadDefaultPage('ViewPrintCenter.html'),
									className: 'pointer'

								},
								_react2.default.createElement('img', {
									src: './../inlineSVG/print-center.svg',
									alt: 'menu for svg',
									className: 'printIcon',
									id: 'printIcon'
								})
							)
						)
					)
				)
			);
		}
	}]);
	return HeadingNav;
}(_react2.default.Component); // 'use babel';


exports.default = HeadingNav;


HeadingNav.propTypes = {
	canShowPaymentReminderInHeader: _propTypes2.default.bool,
	canShowPrintCentreInHeader: _propTypes2.default.bool,
	isFirstSaleTxnCreated: _propTypes2.default.bool
};