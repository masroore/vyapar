Object.defineProperty(exports, "__esModule", {
	value: true
});

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _TxnTypeConstant = require('../../../Constants/TxnTypeConstant.js');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _PassCodeUtility = require('../../../UIControllers/PassCodeUtility');

var _PassCodeUtility2 = _interopRequireDefault(_PassCodeUtility);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Dialog = function (_React$Component) {
	(0, _inherits3.default)(Dialog, _React$Component);

	function Dialog() {
		(0, _classCallCheck3.default)(this, Dialog);
		return (0, _possibleConstructorReturn3.default)(this, (Dialog.__proto__ || (0, _getPrototypeOf2.default)(Dialog)).apply(this, arguments));
	}

	(0, _createClass3.default)(Dialog, [{
		key: 'submitPassCodeFunction',
		value: function submitPassCodeFunction() {
			_PassCodeUtility2.default.submitPassCodeFunction();
		}
	}, {
		key: 'deletePasscodeFunction',
		value: function deletePasscodeFunction() {
			_PassCodeUtility2.default.deletePasscodeFunction();
		}
	}, {
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				'div',
				null,
				_react2.default.createElement('div', { id: 'addNewNameDialog' }),
				_react2.default.createElement('div', { id: 'addEditFirmDialog' }),
				_react2.default.createElement(
					'div',
					{
						id: 'dialogForConfirmationImport',
						title: 'Confirm the list to be imported',
						hidden: true
					},
					_react2.default.createElement(
						'div',
						{
							style: {
								maxHeight: '300px',
								overflowY: 'auto',
								overflowX: 'hidden'
							}
						},
						_react2.default.createElement('table', { width: '100%', className: 'verifyList' })
					),
					_react2.default.createElement('div', {
						className: 'invalidItemList width100',
						id: 'invlidItemList',
						style: {
							maxHeight: '250px',
							overflowY: 'auto',
							marginTop: '20px'
						},
						hidden: true
					}),
					_react2.default.createElement(
						'button',
						{
							type: 'btn',
							id: 'confirmImport',
							className: 'hide terminalButton floatRight'
						},
						'Confirm Import'
					)
				),
				_react2.default.createElement(
					'div',
					{
						id: 'dialogForLicenseExpire',
						title: 'License or Trial Period Expired'
					},
					_react2.default.createElement(
						'span',
						{ className: 'floatLeft padtop' },
						_react2.default.createElement(
							'svg',
							{
								fill: '#eda51d',
								height: '30',
								viewBox: '0 0 24 24',
								width: '30',
								xmlns: 'http://www.w3.org/2000/svg'
							},
							_react2.default.createElement('path', { d: 'M0 0h24v24H0z', fill: 'none' }),
							_react2.default.createElement('path', { d: 'M1 21h22L12 2 1 21zm12-3h-2v-2h2v2zm0-4h-2v-4h2v4z' })
						)
					),
					_react2.default.createElement(
						'span',
						{ id: 'textForLicenseExpire', className: 'floatLeft padtop2x padleft' },
						'Your license or trial period has expired. Please purchase license before you continue.'
					)
				),
				_react2.default.createElement('div', { id: 'extraFieldsSetupDialog', title: 'Additional Fields Setup', className: ' customizePopup hide' }),
				_react2.default.createElement(
					'div',
					{ id: 'extraPartyFieldsSetupDialog', title: 'Party Additional Fields', className: 'customizePopup hide' },
					' '
				),
				_react2.default.createElement('div', {
					id: 'smsSettingDialog',
					className: 'smsSettingDialog hide',
					title: 'Transaction SMS Settings'
				}),
				_react2.default.createElement('div', {
					id: 'supportAppDialog',
					className: 'smsSettingDialog hide',
					title: 'Share screen with Vyapar'
				}),
				_react2.default.createElement(
					'div',
					{
						id: 'exportItemDialog',
						className: 'hide',
						title: 'Excel Sheet Download'
					},
					_react2.default.createElement(
						'div',
						{ className: 'exportItemContainer' },
						_react2.default.createElement(
							'div',
							{
								id: '',
								className: 'exportItemTable'
							},
							_react2.default.createElement('table', {
								id: 'tableHeadexportItemList',
								width: '100%',
								className: 'tablesorter tabTxnTable repor'
							}),
							_react2.default.createElement(
								'div',
								{
									id: 'scrollAreaExportItem',
									className: 'clusterize-scroll eIT'
								},
								_react2.default.createElement(
									'table',
									{
										width: '100%',
										className: 'tablesorter tabTxnTable'
									},
									_react2.default.createElement('tbody', {
										id: 'contentAreaExportItem',
										className: 'clusterize-content'
									})
								)
							)
						),
						_react2.default.createElement(
							'div',
							null,
							' ',
							_react2.default.createElement(
								'button',
								{
									id: 'exportItemButton',
									className: 'terminalButton'
								},
								' ',
								'Export',
								' '
							),
							' '
						)
					)
				),
				_react2.default.createElement('div', {
					className: 'returnGoodsDialog hide',
					id: 'returnGoodsDialog',
					title: 'Return Goods'
				}),
				_react2.default.createElement(
					'div',
					{
						id: 'pdfViewDialog',
						title: 'Preview',
						className: 'hide',
						style: { padding: '0' }
					},
					_react2.default.createElement(
						'div',
						{ className: 'list-wrapper' },
						_react2.default.createElement('div', { id: 'htmlView' })
					),
					_react2.default.createElement(
						'div',
						{ className: 'dialogButton floatRight pad8' },
						_react2.default.createElement(
							'button',
							{
								id: 'openPDF',
								className: 'terminalButton',
								onClick: function onClick() {
									return typeof openPDF === 'function' ? window.openPDF() : '';
								}
							},
							'Open PDF'
						),
						_react2.default.createElement(
							'button',
							{
								id: 'printPDF',
								className: 'terminalButton',
								onClick: function onClick() {
									return typeof printPDF === 'function' ? window.printPDF() : '';
								}
							},
							'Print'
						),
						_react2.default.createElement(
							'button',
							{
								id: 'savePDF',
								className: 'terminalButton',
								'data-filedate': '',
								'data-customheader': '',
								onClick: function onClick() {
									return typeof savePDF === 'function' ? window.savePDF() : '';
								}
							},
							'Save PDF'
						),
						_react2.default.createElement(
							'button',
							{
								id: 'sharePDF',
								className: 'terminalButton',
								'data-receiverid': '',
								onClick: function onClick() {
									return typeof sharePDF === 'function' ? window.sharePDF() : '';
								}
							},
							'Email PDF'
						),
						_react2.default.createElement(
							'button',
							{
								id: 'doneForHtml',
								className: 'terminalButton',
								onClick: function onClick() {
									return typeof closePreview === 'function' ? window.closePreview() : '';
								}
							},
							'Close'
						)
					)
				),
				_react2.default.createElement('div', (0, _defineProperty3.default)({ id: 'setPaymentReminderDialog', className: 'setPaymentReminderDialog', title: 'Set Reminder' }, 'className', 'hide')),
				_react2.default.createElement(
					'div',
					{ id: 'userEmailForPDF', title: 'Send Email', className: 'hide' },
					_react2.default.createElement(
						'div',
						{ className: 'input-field' },
						_react2.default.createElement(
							'div',
							null,
							_react2.default.createElement(
								'label',
								null,
								'To :'
							),
							' '
						),
						_react2.default.createElement('input', {
							type: 'text',
							id: 'receiverEmailForPDF',
							className: 'reportTextInputFilter width100',
							style: { borderRadius: '20px' }
						}),
						_react2.default.createElement(
							'p',
							{
								style: { color: 'red', display: 'none' },
								id: 'userEmailErrorForPDF'
							},
							'Please enter valid mail id!!'
						),
						_react2.default.createElement('br', null)
					),
					_react2.default.createElement(
						'div',
						{ className: 'input-field' },
						_react2.default.createElement(
							'div',
							null,
							_react2.default.createElement(
								'label',
								{ htmlFor: 'emailSubjectForPDF' },
								'Subject:'
							)
						),
						_react2.default.createElement('textarea', {
							type: 'text',
							id: 'emailSubjectForPDF',
							className: 'reportTextInputFilter width100'
						})
					),
					_react2.default.createElement(
						'div',
						{ className: 'input-field' },
						_react2.default.createElement(
							'div',
							null,
							_react2.default.createElement(
								'label',
								{ htmlFor: 'emailBodyForPDF' },
								'Message:'
							)
						),
						_react2.default.createElement('textarea', {
							type: 'text',
							id: 'emailBodyForPDF',
							className: 'reportTextInputFilter width100',
							style: { minHeight: '250px' }
						})
					),
					_react2.default.createElement(
						'div',
						{ className: 'floatRight marginTop' },
						_react2.default.createElement(
							'button',
							{
								type: 'button',
								id: 'viewPdfBeforeMailForReport',
								className: 'terminalButton ripple'
							},
							'View PDF'
						),
						_react2.default.createElement(
							'button',
							{
								type: 'button',
								id: 'receiverIdCancelForPDF',
								className: 'terminalButton ripple'
							},
							'Cancel'
						),
						_react2.default.createElement(
							'button',
							{
								type: 'button',
								id: 'receiverIdSubmitForPDF',
								className: 'terminalButton ripple'
							},
							'Send'
						)
					)
				),
				_react2.default.createElement(
					'div',
					{
						id: 'referralCodeDialog',
						className: 'referralCodeDialog hide',
						title: 'Enter Referral Code'
					},
					_react2.default.createElement(
						'h3',
						null,
						'Please enter your referral code'
					),
					_react2.default.createElement('input', { type: 'text', id: 'referralCodeValue' }),
					_react2.default.createElement(
						'button',
						{
							type: 'btn',
							id: 'terminalButtonReferralCode',
							className: 'terminalButton ripple margintop floatRight',
							onClick: function onClick() {
								return window.submitReferralCodeFunction();
							}
						},
						'Done'
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'selectCountry hide', id: 'selectCountry' },
					_react2.default.createElement(
						'h3',
						null,
						'Please select your country for automatic tax setup'
					),
					_react2.default.createElement(
						'h4',
						{ style: { color: 'red' } },
						'* Please note that once you select a country you can\'t\n\t\t\t\t\t\tchange it later.'
					),
					_react2.default.createElement('select', { id: 'userCountry', className: 'margintop' }),
					_react2.default.createElement('br', null),
					_react2.default.createElement(
						'button',
						{
							type: 'btn',
							id: 'terminalButtonSelectCountry',
							className: 'terminalButton ripple margintop floatRight'
						},
						'Select'
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'customAlertDialog hide', id: 'customAlertDialog' },
					_react2.default.createElement(
						'div',
						{
							id: 'customAlertDialogBody',
							style: {
								fontSize: '14px',
								lineHeight: '1.5',
								padding: '16px'
							}
						},
						'Dummy Message'
					),
					_react2.default.createElement(
						'div',
						{
							className: 'buttonGroup clearfix',
							style: { padding: '8px 0' }
						},
						_react2.default.createElement(
							'button',
							{
								id: 'confirm',
								className: 'terminalButton floatRight marginLeft20'
							},
							'Confirm'
						),
						_react2.default.createElement(
							'button',
							{
								id: 'cancel',
								className: 'terminalButton floatRight'
							},
							'Cancel'
						)
					)
				),
				_react2.default.createElement(
					'div',
					{
						id: 'paymentHistoryDialog',
						className: 'hide',
						style: { maxHeight: '250px!important', zIndex: '10000' }
					},
					_react2.default.createElement(
						'div',
						{
							className: 'transactionStyle',
							style: {
								background: 'white',
								maxHeight: '250px!important',
								overflowY: 'scroll',
								overflowX: 'hidden'
							}
						},
						_react2.default.createElement('table', {
							id: 'paymentHistoryTable',
							className: 'tablesorter tabTxnTable width100 linkTxnTable striped'
						})
					)
				),
				_react2.default.createElement(
					'div',
					{
						id: 'changeCompanyDiaplayNameDialog',
						title: 'Custom Header',
						className: 'customizePopup hide',
						style: { padding: '0' }
					},
					_react2.default.createElement(
						'div',
						{ className: 'list-wrapper' },
						_react2.default.createElement(
							'div',
							{ className: '' },
							_react2.default.createElement('input', {
								type: 'text',
								id: 'compName',
								className: 'reportTextInputFilter width100'
							})
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'dialogButton floatRight' },
						_react2.default.createElement(
							'button',
							{
								'data-shortcut': 'CTRL_S',
								id: 'saveCompanyDisplayName',
								className: 'terminalButton'
							},
							'Save'
						)
					)
				),
				_react2.default.createElement(
					'div',
					{ id: 'dialogForUserOffline', title: 'User Offline' },
					_react2.default.createElement(
						'div',
						{
							className: 'floatLeft padtop margin-r20',
							id: 'userOfflineSvg'
						},
						_react2.default.createElement(
							'svg',
							{
								fill: '#eda51d',
								height: '30',
								viewBox: '0 0 24 24',
								width: '30',
								xmlns: 'http://www.w3.org/2000/svg'
							},
							_react2.default.createElement('path', { d: 'M0 0h24v24H0z', fill: 'none' }),
							_react2.default.createElement('path', { d: 'M1 21h22L12 2 1 21zm12-3h-2v-2h2v2zm0-4h-2v-4h2v4z' })
						)
					),
					_react2.default.createElement(
						'span',
						{ id: 'userOfflineSync' },
						'You need to be online to perform any transaction since you have enabled sync. Please check your Internet and try again.'
					)
				),
				_react2.default.createElement(
					'div',
					{ id: 'dialogForLockedEntity', title: 'Entity locked' },
					_react2.default.createElement(
						'div',
						{
							className: 'floatLeft padtop margin-r20',
							id: 'userOfflineSvg'
						},
						_react2.default.createElement(
							'svg',
							{
								fill: '#eda51d',
								height: '30',
								viewBox: '0 0 24 24',
								width: '30',
								xmlns: 'http://www.w3.org/2000/svg'
							},
							_react2.default.createElement('path', { d: 'M0 0h24v24H0z', fill: 'none' }),
							_react2.default.createElement('path', { d: 'M1 21h22L12 2 1 21zm12-3h-2v-2h2v2zm0-4h-2v-4h2v4z' })
						)
					),
					_react2.default.createElement(
						'span',
						{ id: 'entityLockedMessage' },
						'This entity is being edited by someone else right now. Please try again after sometime.'
					)
				),
				_react2.default.createElement(
					'div',
					{ id: 'setPaymentReminderMessage', title: 'Type your message', className: 'hide' },
					_react2.default.createElement(
						'div',
						null,
						_react2.default.createElement('textarea', { id: 'paymentReminderMessageBody', className: 'paymentReminderMessageBody', type: 'textarea' })
					),
					_react2.default.createElement(
						'div',
						{ className: 'paymentReminderMessageFooter' },
						_react2.default.createElement(
							'div',
							null,
							_react2.default.createElement(
								'button',
								{ id: 'paymentReminderMessageDefault', className: 'paymentReminderButton floatLeft' },
								'Set Default'
							)
						),
						_react2.default.createElement(
							'div',
							null,
							_react2.default.createElement(
								'button',
								{ 'data-shortcut': 'CTRL_S', id: 'paymentReminderMessageDone', className: 'paymentReminderButton floatRight' },
								'DONE'
							)
						)
					)
				),
				_react2.default.createElement(
					'div',
					{ id: 'revokedialog', className: 'hide' },
					'Your access is revoked. You will be redirected to Company List.'
				),
				_react2.default.createElement(
					'div',
					{ id: 'addPrefixDialog', className: 'hide', title: 'Add Prefix' },
					_react2.default.createElement(
						'div',
						{ className: 'modalBody' },
						_react2.default.createElement(
							'div',
							{ className: 'modal-content-wrapper' },
							_react2.default.createElement(
								'div',
								{ className: 'form-control p-20' },
								_react2.default.createElement(
									'label',
									{ className: 'block-label', htmlFor: '' },
									'Transaction Type'
								),
								_react2.default.createElement(
									'select',
									{ id: 'txnTypePrefixVal', defaultValue: _TxnTypeConstant2.default.TXN_TYPE_SALE },
									_react2.default.createElement(
										'option',
										{ value: _TxnTypeConstant2.default.TXN_TYPE_SALE },
										'Sale'
									),
									_react2.default.createElement(
										'option',
										{ value: _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE, id: 'estimateSelectFirm' },
										'Estimate'
									),
									_react2.default.createElement(
										'option',
										{ value: _TxnTypeConstant2.default.TXN_TYPE_CASHIN },
										'Payment-In'
									),
									_react2.default.createElement(
										'option',
										{ value: _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN, id: 'deliveryChallanSelectFirm' },
										'Delivery Challan'
									),
									_react2.default.createElement(
										'option',
										{ value: _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER, id: 'saleOrderSelectFirm' },
										'Sale Order'
									),
									_react2.default.createElement(
										'option',
										{ value: _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER, id: 'purchaseOrderSelectFirm' },
										'Purchase Order'
									),
									_react2.default.createElement(
										'option',
										{ value: _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN, id: 'saleReturnSelectFirm' },
										'Credit Note'
									)
								)
							),
							_react2.default.createElement(
								'div',
								{ className: 'form-control p-20' },
								_react2.default.createElement(
									'label',
									{ className: 'block-label', htmlFor: '' },
									'Prefix Name'
								),
								_react2.default.createElement('input', {
									type: 'text',
									className: 'input-control',
									autoFocus: true,
									placeholder: '',
									id: 'newPrefixVal',
									maxLength: 15
								})
							)
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'modalFooter align-items-center justify-content-end d-flex' },
						_react2.default.createElement(
							'button',
							{
								type: 'submit',
								'data-shortcut': 'CTRL_S',
								className: 'modal-footer-button terminalButton',
								id: 'saveNewPrefixVal'
							},
							'Save'
						)
					)
				),
				_react2.default.createElement(
					'div',
					{ id: 'passCodeDialog', className: 'passCodeDialog hide', title: 'Setup Pass Code' },
					_react2.default.createElement(
						'div',
						null,
						_react2.default.createElement(
							'table',
							{ className: 'tableFormat', width: '100%' },
							_react2.default.createElement(
								'tbody',
								null,
								_react2.default.createElement(
									'tr',
									null,
									_react2.default.createElement(
										'td',
										null,
										_react2.default.createElement(
											'span',
											null,
											'Old Passcode'
										),
										_react2.default.createElement(
											'div',
											{ className: 'row mt-20 input-wrapper', id: 'oldPasscode' },
											_react2.default.createElement('input', { type: 'password', maxLength: '1', id: 'o1' }),
											_react2.default.createElement('input', { type: 'password', maxLength: '1', id: 'o2' }),
											_react2.default.createElement('input', { type: 'password', maxLength: '1', id: 'o3' }),
											_react2.default.createElement('input', { type: 'password', maxLength: '1', id: 'o4' })
										),
										_react2.default.createElement('p', { style: { color: 'red', paddingTop: '8px' }, id: 'oldPasscodeError' })
									)
								),
								_react2.default.createElement(
									'tr',
									null,
									_react2.default.createElement(
										'td',
										null,
										_react2.default.createElement(
											'span',
											null,
											'New Passcode'
										),
										_react2.default.createElement(
											'div',
											{ className: 'row mt-20 input-wrapper', id: 'newPasscode' },
											_react2.default.createElement('input', { type: 'password', maxLength: '1', id: 'p1' }),
											_react2.default.createElement('input', { type: 'password', maxLength: '1', id: 'p2' }),
											_react2.default.createElement('input', { type: 'password', maxLength: '1', id: 'p3' }),
											_react2.default.createElement('input', { type: 'password', maxLength: '1', id: 'p4' })
										),
										_react2.default.createElement('p', { style: { color: 'red', paddingTop: '8px' }, id: 'passcodeError' })
									)
								),
								_react2.default.createElement(
									'tr',
									null,
									_react2.default.createElement(
										'td',
										null,
										_react2.default.createElement(
											'span',
											null,
											'Confirm Passcode'
										),
										_react2.default.createElement(
											'div',
											{ className: 'row mt20 input-wrapper', id: 'confirmPasscode' },
											_react2.default.createElement('input', { type: 'password', maxLength: '1', id: 'c1' }),
											_react2.default.createElement('input', { type: 'password', maxLength: '1', id: 'c2' }),
											_react2.default.createElement('input', { type: 'password', maxLength: '1', id: 'c3' }),
											_react2.default.createElement('input', { type: 'password', maxLength: '1', id: 'c4' })
										),
										_react2.default.createElement('p', { style: { color: 'red', paddingTop: '8px' }, id: 'confirmPasscodeError' })
									)
								),
								_react2.default.createElement(
									'tr',
									null,
									_react2.default.createElement(
										'td',
										{ style: { paddingTop: '0' } },
										_react2.default.createElement(
											'button',
											{ type: 'btn', id: 'terminalButtonPassCode', className: 'terminalButton ripple margintop floatRight hide', 'data-shortcut': 'CTRL_S', onClick: this.submitPassCodeFunction },
											'Save'
										)
									)
								)
							)
						)
					)
				),
				_react2.default.createElement(
					'div',
					{ id: 'passDeleteDialog', className: 'passCodeDialog hide', title: 'Enter Pass Code' },
					_react2.default.createElement(
						'table',
						{ className: 'tableFormat', width: '100%', style: { padding: 0 } },
						_react2.default.createElement(
							'tbody',
							null,
							_react2.default.createElement(
								'tr',
								null,
								_react2.default.createElement(
									'td',
									null,
									_react2.default.createElement(
										'span',
										{ htmlFor: '' },
										'Enter Passcode'
									),
									_react2.default.createElement(
										'div',
										{ className: 'mt20 input-wrapper', id: 'deletePasscode' },
										_react2.default.createElement('input', { type: 'password', maxLength: '1', id: 'd1' }),
										_react2.default.createElement('input', { type: 'password', maxLength: '1', id: 'd2' }),
										_react2.default.createElement('input', { type: 'password', maxLength: '1', id: 'd3' }),
										_react2.default.createElement('input', { type: 'password', maxLength: '1', id: 'd4' })
									),
									_react2.default.createElement('p', { style: { color: 'red', paddingTop: '8px' }, id: 'deletePasscodeError' })
								)
							),
							_react2.default.createElement(
								'tr',
								null,
								_react2.default.createElement(
									'td',
									{ style: { paddingTop: '0' } },
									_react2.default.createElement(
										'button',
										{ type: 'btn', id: 'terminalButtonDelPassCode', className: 'terminalButton ripple margintop floatRight hide', onClick: this.deletePasscodeFunction },
										'Remove'
									)
								)
							)
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'forgotPasscode font-14 floatRight' },
						_react2.default.createElement(
							'h3',
							{ id: 'forgotDeletePasscode', style: { fontFamily: 'robotoRegular' } },
							'Forgot Passcode?'
						)
					)
				),
				_react2.default.createElement(
					'div',
					{ id: 'customFieldDialog', title: 'Transportation Details', className: ' customizePopup hide', style: { padding: '0' } },
					_react2.default.createElement(
						'div',
						{ className: 'list-wrapper' },
						_react2.default.createElement(
							'div',
							{ className: '' },
							_react2.default.createElement(
								'label',
								{ htmlFor: 'transportCustomField1' },
								'Field 1'
							),
							_react2.default.createElement('input', { type: 'text', id: 'transportCustomField1', 'data-fieldid': '1', className: 'reportTextInputFilter width90' }),
							_react2.default.createElement(
								'label',
								{ htmlFor: 'checkTransportCustomField1', className: 'input-checkbox' },
								_react2.default.createElement('input', { type: 'checkbox', 'data-field-id': 'transportCustomField1', id: 'checkTransportCustomField1', className: 'checkTransport' }),
								_react2.default.createElement(
									'svg',
									{ width: '18', height: '18' },
									_react2.default.createElement('path', { className: 'checked', d: 'M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M7,14L2,9l1.4-1.4L7,11.2l7.6-7.6L16,5L7,14z' }),
									_react2.default.createElement('path', { className: 'indeterminate', d: 'M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M14,10H4V8h10V10z' }),
									_react2.default.createElement('path', { className: 'unchecked', fill: '#9e9e9e', d: 'M16,2v14H2V2H16 M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z' })
								)
							)
						),
						_react2.default.createElement(
							'div',
							null,
							_react2.default.createElement(
								'label',
								{ htmlFor: 'transportCustomField2' },
								'Field 2'
							),
							_react2.default.createElement('input', { type: 'text', id: 'transportCustomField2', 'data-fieldid': '2', className: 'reportTextInputFilter width90' }),
							_react2.default.createElement(
								'label',
								{ htmlFor: 'checkTransportCustomField2', className: 'input-checkbox' },
								_react2.default.createElement('input', { type: 'checkbox', 'data-field-id': 'transportCustomField2', id: 'checkTransportCustomField2', className: 'checkTransport' }),
								_react2.default.createElement(
									'svg',
									{ width: '18', height: '18' },
									_react2.default.createElement('path', { className: 'checked', d: 'M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M7,14L2,9l1.4-1.4L7,11.2l7.6-7.6L16,5L7,14z' }),
									_react2.default.createElement('path', { className: 'indeterminate', d: 'M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M14,10H4V8h10V10z' }),
									_react2.default.createElement('path', { className: 'unchecked', fill: '#9e9e9e', d: 'M16,2v14H2V2H16 M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z' })
								)
							)
						),
						_react2.default.createElement(
							'div',
							null,
							_react2.default.createElement(
								'label',
								{ htmlFor: 'transportCustomField3' },
								'Field 3'
							),
							_react2.default.createElement('input', { type: 'text', id: 'transportCustomField3', 'data-fieldid': '3', className: 'reportTextInputFilter width90' }),
							_react2.default.createElement(
								'label',
								{ htmlFor: 'checkTransportCustomField3', className: 'input-checkbox' },
								_react2.default.createElement('input', { type: 'checkbox', 'data-field-id': 'transportCustomField3', id: 'checkTransportCustomField3', className: 'checkTransport' }),
								_react2.default.createElement(
									'svg',
									{ width: '18', height: '18' },
									_react2.default.createElement('path', { className: 'checked', d: 'M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M7,14L2,9l1.4-1.4L7,11.2l7.6-7.6L16,5L7,14z' }),
									_react2.default.createElement('path', { className: 'indeterminate', d: 'M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M14,10H4V8h10V10z' }),
									_react2.default.createElement('path', { className: 'unchecked', fill: '#9e9e9e', d: 'M16,2v14H2V2H16 M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z' })
								)
							)
						),
						_react2.default.createElement(
							'div',
							null,
							_react2.default.createElement(
								'label',
								{ htmlFor: 'transportCustomField4' },
								'Field 4'
							),
							_react2.default.createElement('input', { type: 'text', id: 'transportCustomField4', 'data-fieldid': '4', className: 'reportTextInputFilter width90' }),
							_react2.default.createElement(
								'label',
								{ htmlFor: 'checkTransportCustomField4', className: 'input-checkbox' },
								_react2.default.createElement('input', { type: 'checkbox', 'data-field-id': 'transportCustomField4', id: 'checkTransportCustomField4', className: 'checkTransport' }),
								_react2.default.createElement(
									'svg',
									{ width: '18', height: '18' },
									_react2.default.createElement('path', { className: 'checked', d: 'M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M7,14L2,9l1.4-1.4L7,11.2l7.6-7.6L16,5L7,14z' }),
									_react2.default.createElement('path', { className: 'indeterminate', d: 'M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M14,10H4V8h10V10z' }),
									_react2.default.createElement('path', { className: 'unchecked', fill: '#9e9e9e', d: 'M16,2v14H2V2H16 M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z' })
								)
							)
						),
						_react2.default.createElement(
							'div',
							null,
							_react2.default.createElement(
								'label',
								{ htmlFor: 'transportCustomField5' },
								'Field 5'
							),
							_react2.default.createElement('input', { type: 'text', id: 'transportCustomField5', 'data-fieldid': '5', className: 'reportTextInputFilter width90' }),
							_react2.default.createElement(
								'label',
								{ htmlFor: 'checkTransportCustomField5', className: 'input-checkbox' },
								_react2.default.createElement('input', { type: 'checkbox', 'data-field-id': 'transportCustomField5', id: 'checkTransportCustomField5', className: 'checkTransport' }),
								_react2.default.createElement(
									'svg',
									{ width: '18', height: '18' },
									_react2.default.createElement('path', { className: 'checked', d: 'M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M7,14L2,9l1.4-1.4L7,11.2l7.6-7.6L16,5L7,14z' }),
									_react2.default.createElement('path', { className: 'indeterminate', d: 'M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M14,10H4V8h10V10z' }),
									_react2.default.createElement('path', { className: 'unchecked', fill: '#9e9e9e', d: 'M16,2v14H2V2H16 M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z' })
								)
							)
						),
						_react2.default.createElement(
							'div',
							null,
							_react2.default.createElement(
								'label',
								{ htmlFor: 'transportCustomField6' },
								'Field 6'
							),
							_react2.default.createElement('input', { type: 'text', id: 'transportCustomField6', 'data-fieldid': '6', className: 'reportTextInputFilter width90' }),
							_react2.default.createElement(
								'label',
								{ htmlFor: 'checkTransportCustomField6', className: 'input-checkbox' },
								_react2.default.createElement('input', { type: 'checkbox', 'data-field-id': 'transportCustomField6', id: 'checkTransportCustomField6', className: 'checkTransport' }),
								_react2.default.createElement(
									'svg',
									{ width: '18', height: '18' },
									_react2.default.createElement('path', { className: 'checked', d: 'M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M7,14L2,9l1.4-1.4L7,11.2l7.6-7.6L16,5L7,14z' }),
									_react2.default.createElement('path', { className: 'indeterminate', d: 'M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M14,10H4V8h10V10z' }),
									_react2.default.createElement('path', { className: 'unchecked', fill: '#9e9e9e', d: 'M16,2v14H2V2H16 M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z' })
								)
							)
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'dialogButton floatRight' },
						_react2.default.createElement(
							'button',
							{ id: 'doneForCustomFields', className: 'terminalButton' },
							'Done'
						)
					)
				),
				_react2.default.createElement(
					'div',
					{ id: 'disablesyncmessage', className: ' hide' },
					'This will disable sync and access for the granted users will be revoked.'
				),
				_react2.default.createElement(
					'div',
					{ className: 'delDialog hide' },
					_react2.default.createElement(
						'b',
						null,
						'The permission will be revoked for this User.'
					)
				),
				_react2.default.createElement(
					'div',
					{ id: 'syncmngpermission', title: 'Manage Users', className: 'hide floatleft' },
					_react2.default.createElement(
						'div',
						{ title: 'Section 1', className: 'newsec1' },
						_react2.default.createElement('div', { id: 'admin' }),
						_react2.default.createElement(
							'form',
							{ id: 'mailer' },
							_react2.default.createElement('hr', { style: { width: '100%' } }),
							_react2.default.createElement('br', null),
							_react2.default.createElement('br', null),
							_react2.default.createElement(
								'b',
								{ style: { color: '#2B4C56' } },
								'Enter Phone Number Or Email'
							),
							_react2.default.createElement('br', null),
							_react2.default.createElement('input', { id: 'mail', name: 'field1', className: 'searchField searchBox2 searchBox-wrapper  ', type: 'tel email', placeholder: 'Enter Phone No. or Email', required: true }),
							_react2.default.createElement('br', null),
							_react2.default.createElement('br', null),
							_react2.default.createElement(
								'button',
								{ id: 'addmail', className: 'terminalButton ', style: { width: '100%' } },
								'Grant Access'
							)
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'newsec2' },
						_react2.default.createElement(
							'header',
							{ id: 'sect2' },
							' ',
							_react2.default.createElement(
								'b',
								{ style: { color: '#2B4C56' } },
								'Granted Users'
							),
							_react2.default.createElement('br', null),
							_react2.default.createElement('br', null),
							_react2.default.createElement('br', null),
							' '
						),
						_react2.default.createElement(
							'div',
							{ id: 'mailresult', title: 'Section 2', className: '  ' },
							_react2.default.createElement('ul', { id: 'lines' })
						)
					)
				),
				_react2.default.createElement(
					'div',
					{
						id: 'passCodeDeleteDialog',
						className: 'passCodeDialog hide',
						title: 'Enter Pass Code'
					},
					_react2.default.createElement(
						'div',
						null,
						_react2.default.createElement(
							'table',
							{ className: 'tableFormat', width: '100%' },
							_react2.default.createElement('thead', null),
							_react2.default.createElement(
								'tbody',
								null,
								_react2.default.createElement(
									'tr',
									null,
									_react2.default.createElement(
										'td',
										null,
										_react2.default.createElement(
											'span',
											{ htmlFor: '' },
											'Enter Passcode'
										),
										_react2.default.createElement(
											'div',
											{
												id: 'confirmPasscode',
												className: 'row mtt20 input-wrapper-delete'
											},
											_react2.default.createElement('input', {
												className: 'input-delete',
												id: 'e1',
												type: 'password',
												maxLength: '1'
											}),
											_react2.default.createElement('input', {
												className: 'input-delete',
												id: 'e2',
												type: 'password',
												maxLength: '1'
											}),
											_react2.default.createElement('input', {
												className: 'input-delete',
												id: 'e3',
												type: 'password',
												maxLength: '1'
											}),
											_react2.default.createElement('input', {
												className: 'input-delete',
												id: 'e4',
												type: 'password',
												maxLength: '1'
											})
										),
										_react2.default.createElement('p', {
											id: 'passcodeDeleteError',
											style: {
												color: 'red',
												paddingTop: 8 + 'px'
											}
										})
									)
								),
								_react2.default.createElement(
									'tr',
									null,
									_react2.default.createElement(
										'td',
										{ style: { paddingTop: 0 } },
										_react2.default.createElement(
											'button',
											{
												id: 'terminalButtonDeletePassCode',
												type: 'btn',
												className: 'terminalButton ripple margintop floatRight hide'
											},
											'Delete'
										)
									)
								)
							)
						),
						_react2.default.createElement(
							'div',
							{ className: 'row forgotPasscode' },
							_react2.default.createElement(
								'h3',
								{ id: 'forgotDeletePasscode', style: { fontFamily: 'robotoRegular' } },
								'forgot passcode?'
							)
						)
					)
				)
			);
		}
	}]);
	return Dialog;
}(_react2.default.Component); // 'use babel';


exports.default = Dialog;