Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _MountComponent = require('../MountComponent');

var _MountComponent2 = _interopRequireDefault(_MountComponent);

var _FestiveOffer = require('../FestiveOffer');

var _FestiveOffer2 = _interopRequireDefault(_FestiveOffer);

var _logger = require('../../../Utilities/logger');

var _logger2 = _interopRequireDefault(_logger);

var _TxnTypeConstant3 = require('../../../Constants/TxnTypeConstant');

var _TxnTypeConstant4 = _interopRequireDefault(_TxnTypeConstant3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MyAnalytics = require('../../../Utilities/analyticsHelper'); // 'use babel';
/* eslint-disable react/prop-types */

var onFocusHandler = function onFocusHandler(id) {
	return function () {
		if (!id) return;
		var subMenuItem = document.getElementById(id);
		var subMenuItemAddForm = subMenuItem.getElementsByClassName('add-form')[0];
		if (subMenuItemAddForm) {
			subMenuItemAddForm.style.display = 'block';
		}
	};
};
var onBlurHandler = function onBlurHandler(id) {
	return function () {
		if (!id) return;
		var subMenuItem = document.getElementById(id);
		var subMenuItemAddForm = subMenuItem.getElementsByClassName('add-form')[0];
		if (subMenuItemAddForm) {
			subMenuItemAddForm.style.display = 'none';
		}
	};
};

function LeftNavGeneralItem(props) {
	var handleClick = function handleClick() {
		if (props.canLogAnalytics) {
			MyAnalytics.pushEvent('leftnav_Buy Now');
		}
		props.onClick && props.onClick(props);
	};
	var handleKeyDown = function handleKeyDown(e) {
		if (props.canLogAnalytics) {
			MyAnalytics.pushEvent('leftnav_Buy Now');
		}
		props.onKeyDown && props.onKeyDown(props, e);
	};
	return _react2.default.createElement(
		'div',
		{
			onClick: handleClick,
			onKeyDown: handleKeyDown,
			tabIndex: props.tabIndex
		},
		props.children
	);
}

function LeftNavItemsTabs(props) {
	var addFormEvent = function addFormEvent() {
		return function (event) {
			event.stopPropagation();
		};
	};
	return _react2.default.createElement(
		'li',
		{
			id: props.id,
			onMouseOver: onFocusHandler(props.id),
			onMouseOut: onBlurHandler(props.id),
			onClick: function onClick() {
				return props.onClick && props.onClick(props);
			},
			onKeyDown: function onKeyDown(e) {
				return props.onKeyDown(props, e);
			},
			tabIndex: props.tabIndex,
			className: props.className || 'ripple add-menu-enabled add-menu-enabled-list-item'
		},
		_react2.default.createElement(
			'div',
			{ className: 'divInsideMenu clearfix width100' },
			props.img && _react2.default.createElement(
				'div',
				{ className: 'navMenuIcon leftIconDivForMainMenu' },
				_react2.default.createElement('img', { src: props.img })
			),
			_react2.default.createElement(
				'div',
				{
					className: props.img ? 'textInsideMenu rightTextDivForMainMenu' : 'textInsideMenu'
				},
				props.navItemName,
				props.isAddFormAvailable && _react2.default.createElement(
					'span',
					{
						id: props.addFormFileName,
						onClick: addFormEvent(),
						className: 'changeDefaultPageOther ripple add-form floatRight'
					},
					'+'
				),
				props.isAddTxnFormAvailable && _react2.default.createElement(
					'span',
					{
						id: props.id,
						onClick: addFormEvent(),
						className: 'changeDefaultPageSP ripple add-form floatRight'
					},
					'+'
				)
			),
			props.subText && _react2.default.createElement(
				'p',
				{ className: 'subText font-12' },
				props.subText
			)
		)
	);
}
LeftNavItemsTabs.propTypes = {
	className: _propTypes2.default.string,
	img: _propTypes2.default.string,
	id: _propTypes2.default.string,
	onClick: _propTypes2.default.func,
	onKeyDown: _propTypes2.default.func,
	tabIndex: _propTypes2.default.string,
	navItemName: _propTypes2.default.string
};

function LeftNavItemsAccordion(props) {
	var subItems = props.subItems.map(function (item, step) {
		var isAddFormClicked = false;
		var onClick = function onClick() {
			if (isAddFormClicked) {
				isAddFormClicked = false;
				return;
			}
			if (typeof item.ngclick == 'function') {
				item.ngclick(item.props);
			} else if (item.click) {
				window[item.click]();
			}
		};
		var addFormClickHandler = function addFormClickHandler(event) {
			isAddFormClicked = true;
		};
		var addFormEvent = function addFormEvent() {
			return function (event) {
				event.stopPropagation();
			};
		};
		var addFormId = item.props && item.props.id ? item.props.id : '';
		// if addFormId specifically provided.
		addFormId = item.props && item.props.addFormId ? item.props.addFormId : addFormId;
		var id = 'subMenuItem_' + (item.props && item.props.id ? item.props.id : Math.round(Math.random() * 1000000000000));
		return _react2.default.createElement(
			'li',
			{
				id: id,
				key: item.name,
				onClick: onClick,
				className: item.class + ' sub-menu-item ' + (item.isAddFormAvailable ? 'add-menu-enabled' : '')
			},
			_react2.default.createElement(
				'div',
				{ className: 'menu-content d-inline-flex align-items-center' },
				!!item.img && _react2.default.createElement('img', { src: item.img }),
				_react2.default.createElement(
					'span',
					null,
					item.name
				)
			),
			item.isAddFormAvailable && _react2.default.createElement(
				_react2.default.Fragment,
				null,
				addFormId && _react2.default.createElement(
					'span',
					{
						id: addFormId,
						onClick: addFormClickHandler,
						className: 'changeDefaultPageSP add-form floatRight'
					},
					'+'
				),
				item.addFormFileName && _react2.default.createElement(
					'span',
					{
						id: item.addFormFileName,
						onClick: addFormEvent(),
						className: 'changeDefaultPageOther ripple add-form floatRight'
					},
					'+'
				)
			)
		);
	});
	return _react2.default.createElement(
		_react2.default.Fragment,
		null,
		_react2.default.createElement(
			'li',
			{
				tabIndex: props.tabIndex,
				className: 'expandable-menu',
				style: { height: 'auto', width: '100%' }
			},
			_react2.default.createElement(
				'div',
				{
					className: 'divInsideMenu expandable width100'
				},
				_react2.default.createElement(
					'div',
					{ className: 'expandHeader width100' },
					_react2.default.createElement(
						'div',
						{ className: 'navMenuIcon leftIconDivForMainMenu' },
						_react2.default.createElement('img', { src: props.img })
					),
					_react2.default.createElement(
						'div',
						{
							className: 'textInsideMenu rightTextDivForMainMenu'
						},
						props.navTabName
					),
					_react2.default.createElement(
						'div',
						{ className: 'expand-icon' },
						_react2.default.createElement('img', { src: '../inlineSVG/expand-menu.svg', alt: '^' })
					)
				)
			)
		),
		_react2.default.createElement(
			'ul',
			{ className: 'expandable-open' },
			subItems
		)
	);
}
LeftNavItemsAccordion.propTypes = {
	subItems: _propTypes2.default.array,
	navTabName: _propTypes2.default.string,
	img: _propTypes2.default.string,
	navSectionName: _propTypes2.default.string
};

function LeftNavItemsAccordionWithText(props) {
	return _react2.default.createElement(
		_react2.default.Fragment,
		null,
		_react2.default.createElement(
			'div',
			{ id: 'syncul' },
			_react2.default.createElement(
				'li',
				{
					tabIndex: props.tabIndex,
					className: 'dynamic expandable-menu',
					style: { height: 'auto', width: '100%' }
				},
				_react2.default.createElement(
					'div',
					{
						className: 'divInsideMenu expandable width100'
					},
					_react2.default.createElement(
						'div',
						{
							className: 'expandHeader width100',
							onClick: function onClick() {
								return window.chkStatus();
							}
						},
						_react2.default.createElement(
							'div',
							{ className: 'navMenuIcon leftIconDivForMainMenu' },
							_react2.default.createElement('img', { src: props.img })
						),
						_react2.default.createElement(
							'div',
							{
								className: 'textInsideMenu rightTextDivForMainMenu'
							},
							props.navTabName
						),
						_react2.default.createElement(
							'div',
							{ className: 'expand-icon' },
							_react2.default.createElement('img', { src: '../inlineSVG/expand-menu.svg', alt: '^' })
						)
					)
				)
			),
			_react2.default.createElement(
				'ul',
				{ className: 'expandable-open sync-menu-btn' },
				_react2.default.createElement(
					'li',
					{
						id: 'stat',
						className: 'list-item',
						style: { color: 'green', textAlign: 'left' }
					},
					'Sync Enabled'
				),
				_react2.default.createElement('br', null),
				_react2.default.createElement('li', {
					id: 'syncmail',
					className: 'list-item',
					style: { textAlign: 'left' }
				}),
				_react2.default.createElement('br', null),
				_react2.default.createElement(
					'li',
					{ className: 'list-item' },
					_react2.default.createElement(
						'button',
						{
							id: 'loginbutton2',
							style: { textAlign: 'left' },
							onClick: function onClick() {
								return window.onLogin();
							},
							className: 'terminalButton list-item'
						},
						'Login'
					),
					_react2.default.createElement(
						'button',
						{
							id: 'logoutbutton2',
							style: { textAlign: 'left' },
							onClick: function onClick() {
								return window.onLogout();
							},
							className: 'logoutButton list-item'
						},
						'Logout'
					)
				)
			)
		)
	);
}
LeftNavItemsAccordionWithText.propTypes = {
	subItems: _propTypes2.default.array,
	navTabName: _propTypes2.default.string,
	img: _propTypes2.default.string,
	navSectionName: _propTypes2.default.string
};

var LeftSideNav = function (_React$Component) {
	(0, _inherits3.default)(LeftSideNav, _React$Component);

	function LeftSideNav(props) {
		(0, _classCallCheck3.default)(this, LeftSideNav);

		var _this = (0, _possibleConstructorReturn3.default)(this, (LeftSideNav.__proto__ || (0, _getPrototypeOf2.default)(LeftSideNav)).call(this, props));

		_initialiseProps.call(_this);

		_this.handleClick = _this.handleClick.bind(_this);
		_this.checkSyncForCloseBook = _this.checkSyncForCloseBook.bind(_this);
		_this.handleKeyDown = _this.handleKeyDown.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(LeftSideNav, [{
		key: 'handleClick',
		value: function handleClick(props) {
			try {
				if (typeof canCloseDialogue == 'undefined' || !canCloseDialogue || canCloseDialogue()) {
					var frameDiv = $('#frameDiv');
					try {
						unmountReactComponent(frameDiv[0]);
						unmountReactComponent(document.querySelector('#salePurchaseContainer'));
					} catch (ex) {}
					$('#defaultPage').show();
					frameDiv.empty();
					var eventName = props.fileName || props.subItem || '';
					eventName = eventName.split('.');
					MyAnalytics.pushEvent(eventName[0] + ' View');
					// don't unbind eventListeners if defaultPage is not going to be loaded.
					props.subItem || $('#defaultPage, #defaultPage *').unbind();

					if (props.id == 'deliveryChallanTab') {
						var Component = require('../DeliveryChallanContainer').default;
						(0, _MountComponent2.default)(Component, document.querySelector('#defaultPage'));
					} else if (props.txnTypeId == _TxnTypeConstant4.default.TXN_TYPE_SALE || props.txnTypeId == _TxnTypeConstant4.default.TXN_TYPE_PURCHASE) {
						var _Component = require('../SalePurchaseReportContainer').default;
						(0, _MountComponent2.default)(_Component, document.querySelector('#defaultPage'), {
							txnType: props.txnTypeId,
							includeReturnTransaction: false
						});
					} else if (props.fileName === 'ViewParties.html') {
						var PartiesContainer = require('../PartiesContainer').default;
						(0, _MountComponent2.default)(PartiesContainer, document.querySelector('#defaultPage'));
					} else if (props.fileName === 'ViewItems.html') {
						var ItemsContainer = require('../ItemsContainer').default;
						(0, _MountComponent2.default)(ItemsContainer, document.querySelector('#defaultPage'));
					} else if (props.fileName === 'ViewOrderForm.html') {
						var OrdersContainer = require('../OrdersContainer').default;
						var _TxnTypeConstant = require('../../../Constants/TxnTypeConstant.js');
						(0, _MountComponent2.default)(OrdersContainer, document.querySelector('#defaultPage'), {
							selectedTxnType: _TxnTypeConstant[props.id]
						});
					} else if (props.fileName === 'BusinessStatus.html') {
						if (!this.props.isFirstSaleTxnCreated) {
							var FTU = require('../ftu').default;
							_reactDom2.default.render(_react2.default.createElement(FTU, null), document.querySelector('#defaultPage'));
						} else {
							var Dashboard = require('../Dashboard/Dashboard').default;
							_reactDom2.default.render(_react2.default.createElement(Dashboard, null), document.querySelector('#defaultPage'));
						}
					} else if (props.subItem == 'ImportItems') {
						var ImportItems = require('../ExcelImport/ImportItems').default;
						(0, _MountComponent2.default)(ImportItems, document.querySelector('#frameDiv'), {
							onClose: this.handleClose
						});
					} else if (props.subItem == 'ImportParties') {
						var ImportParties = require('../ExcelImport/ImportParties').default;
						(0, _MountComponent2.default)(ImportParties, document.querySelector('#frameDiv'), {
							onClose: this.handleClose
						});
					} else if (props.fileName == 'ViewBankAccount.html') {
						var BankAccountsContainer = require('../BankAccountsContainer').default;
						(0, _MountComponent2.default)(BankAccountsContainer, document.querySelector('#defaultPage'));
					} else if (props.fileName == 'ViewExpenseList.html') {
						var ExpenseContainer = require('../ExpenseContainer').default;
						(0, _MountComponent2.default)(ExpenseContainer, document.querySelector('#defaultPage'));
					} else if (props.fileName == 'ViewIncomeList.html') {
						var OtherIncomeContainer = require('../OtherIncomeContainer').default;
						(0, _MountComponent2.default)(OtherIncomeContainer, document.querySelector('#defaultPage'));
					} else if (props.fileName === 'ViewOpenCheques.html') {
						var ChequesContainer = require('../ChequesContainer').default;
						(0, _MountComponent2.default)(ChequesContainer, document.querySelector('#defaultPage'));
					} else if (props.fileName === 'ViewCashInHand.html') {
						var CashInHandContainer = require('../CashInHandContainer').default;
						(0, _MountComponent2.default)(CashInHandContainer, document.querySelector('#defaultPage'));
					} else if (props.fileName === 'Settings.html') {
						var Settings = require('../Settings/Settings').default;
						(0, _MountComponent2.default)(Settings, document.querySelector('#frameDiv'));
					} else if (props.fileName === 'ViewLoanAccount.html') {
						var LoanAccountsContainer = require('../LoanAccountsContainer').default;
						(0, _MountComponent2.default)(LoanAccountsContainer, document.querySelector('#defaultPage'));
					} else {
						var _TxnTypeConstant2 = require('../../../Constants/TxnTypeConstant.js');
						if (props.isAReportView && props.fileName === 'CustomReport.html') {
							window.filterTxnTypeFromLeftNav = _TxnTypeConstant2[props.id];
						}
						$('#defaultPage').load(props.fileName);
					}

					$('#modelContainer').css({ display: 'none' });
				}
			} catch (err) {
				_logger2.default.error(err);
				$('#defaultPage').show();
				$('#frameDiv').empty();
				$('#modelContainer').css({ display: 'none' });
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var _this2 = this;

			var LicenseUtility = require('../../../Utilities/LicenseUtility');
			var UsageTypeConstants = require('../../../Constants/UsageTypeConstants');
			var licenseInfoDetails = LicenseUtility.getLicenseInfo();
			var isLicenseInvalid = licenseInfoDetails.licenseUsageType !== UsageTypeConstants.VALID_LICENSE;
			return _react2.default.createElement(
				'div',
				{ className: 'sideNav', id: 'sideNav' },
				_react2.default.createElement(
					'ul',
					{ className: 'navMenu' },
					_react2.default.createElement(
						'li',
						{ id: 'companyName',
							className: 'pointer updateCurrentFirm'
						},
						_react2.default.createElement(
							'div',
							{ className: 'divInsideMenu width100' },
							_react2.default.createElement(
								'div',
								{ className: 'companyImageContainer addLogo' },
								_react2.default.createElement('img', { className: 'companyImage' })
							),
							_react2.default.createElement(
								'div',
								{ className: 'company' },
								_react2.default.createElement('div', { className: 'inlineLoader' })
							),
							_react2.default.createElement(
								'span',
								{ className: 'rightIcon' },
								_react2.default.createElement(
									'svg',
									{ width: '6', height: '12', viewBox: '0 0 5 9', fill: 'none', xmlns: 'http://www.w3.org/2000/svg' },
									_react2.default.createElement('path', { d: 'M5.00002 4.52407C5.00002 4.71659 4.95372 4.9091 4.81483 5.05348L1.20372 8.80749C0.925942 9.04814 0.462979 9.04814 0.185201 8.80749C-0.0925768 8.56685 -0.0925768 8.08557 0.185201 7.7968L3.33335 4.52407L0.185201 1.25135C-0.0925771 0.962576 -0.0925771 0.52942 0.185201 0.24065C0.462978 -0.0481196 0.879645 -0.0481196 1.15742 0.24065L4.76853 3.99466C4.95372 4.13905 5.00002 4.33156 5.00002 4.52407Z', fill: 'white' })
								)
							)
						)
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'separatorDiv' },
					_react2.default.createElement(
						'ul',
						null,
						_react2.default.createElement(
							'li',
							null,
							_react2.default.createElement('hr', { className: 'topSeparator' })
						)
					)
				),
				_react2.default.createElement(
					'div',
					{ id: 'scrollableMenu', className: 'd-flex flex-column' },
					_react2.default.createElement(
						'ul',
						{ className: 'navMenu' },
						_react2.default.createElement(LeftNavItemsTabs, {
							onClick: this.handleClick,
							onKeyDown: this.handleKeyDown,
							tabIndex: '2',
							img: './../inlineSVG/home.svg',
							navItemName: 'Home',
							fileName: 'BusinessStatus.html',
							id: 'navHome'
						}),
						_react2.default.createElement(LeftNavItemsTabs, {
							onClick: this.handleClick,
							onKeyDown: this.handleKeyDown,
							tabIndex: '3',
							img: './../inlineSVG/Parties.svg',
							navItemName: 'Parties',
							fileName: 'ViewParties.html',
							id: 'viewParties',
							isAddFormAvailable: true,
							addFormFileName: 'NewContact.html'
						}),
						_react2.default.createElement(LeftNavItemsTabs, {
							onClick: this.handleClick,
							onKeyDown: this.handleKeyDown,
							tabIndex: '4',
							img: './../inlineSVG/Items.svg',
							navItemName: 'Items',
							id: 'viewItems',
							fileName: 'ViewItems.html',
							isAddFormAvailable: true,
							addFormFileName: 'NewItems.html'
						}),
						_react2.default.createElement(LeftNavItemsAccordion, {
							navSectionName: 'Sale',
							img: './../inlineSVG/Sale.svg',
							navTabName: 'Sale',
							tabIndex: '5',
							subItems: [{
								name: 'Sale Invoices',
								isAddFormAvailable: true,
								class: '',
								ngclick: this.handleClick,
								props: {
									id: 'TXN_TYPE_SALE',
									txnTypeId: _TxnTypeConstant4.default.TXN_TYPE_SALE,
									fileName: 'SaleReport.html'
								}
							}, {
								name: 'Estimate/ Quotation',
								isAddFormAvailable: true,
								class: '',
								ngclick: this.handleClick,
								props: {
									id: 'TXN_TYPE_ESTIMATE',
									fileName: 'ViewEstimate.html'
								}
							}, {
								name: 'Payment In',
								isAddFormAvailable: true,
								ngclick: this.handleClick,
								props: {
									id: 'TXN_TYPE_CASHIN',
									isAReportView: true,
									fileName: 'CustomReport.html'
								}
							}, {
								name: 'Sale Order',
								isAddFormAvailable: true,
								ngclick: this.handleClick,
								props: {
									id: 'TXN_TYPE_SALE_ORDER',
									fileName: 'ViewOrderForm.html'
								}
							}, {
								name: 'Delivery Challan',
								isAddFormAvailable: true,
								class: '',
								ngclick: this.handleClick,
								props: {
									id: 'deliveryChallanTab',
									addFormId: 'TXN_TYPE_DELIVERY_CHALLAN',
									fileName: 'ViewDeliveryChallan.html'
								}
							}, {
								name: 'Sale Return/ Cr. Note',
								isAddFormAvailable: true,
								ngclick: this.handleClick,
								props: {
									id: 'TXN_TYPE_SALE_RETURN',
									isAReportView: true,
									fileName: 'CustomReport.html'
								}
							}]
						}),
						_react2.default.createElement(LeftNavItemsAccordion, {
							navSectionName: 'Purchase',
							img: './../inlineSVG/Purchase.svg',
							navTabName: 'Purchase',
							tabIndex: '6',
							subItems: [{
								name: 'Purchase bills',
								isAddFormAvailable: true,
								ngclick: this.handleClick,
								props: {
									id: 'TXN_TYPE_PURCHASE',
									txnTypeId: _TxnTypeConstant4.default.TXN_TYPE_PURCHASE,
									fileName: 'PurchaseReport.html'
								}
							}, {
								name: 'Payment Out',
								isAddFormAvailable: true,
								ngclick: this.handleClick,
								props: {
									id: 'TXN_TYPE_CASHOUT',
									isAReportView: true,
									fileName: 'CustomReport.html'
								}
							}, {
								name: 'Purchase Order',
								isAddFormAvailable: true,
								ngclick: this.handleClick,
								props: {
									id: 'TXN_TYPE_PURCHASE_ORDER',
									fileName: 'ViewOrderForm.html'
								}
							}, {
								name: 'Purchase Return/ Dr. Note',
								isAddFormAvailable: true,
								ngclick: this.handleClick,
								props: {
									id: 'TXN_TYPE_PURCHASE_RETURN',
									isAReportView: true,
									fileName: 'CustomReport.html'
								}
							}]
						}),
						_react2.default.createElement(LeftNavItemsTabs, {
							onClick: this.handleClick,
							onKeyDown: this.handleKeyDown,
							tabIndex: '7',
							img: './../inlineSVG/Expenses.svg',
							navItemName: 'Expenses',
							fileName: 'ViewExpenseList.html',
							isAddTxnFormAvailable: true,
							id: 'TXN_TYPE_EXPENSE'
						}),
						_react2.default.createElement(LeftNavItemsTabs, {
							onClick: this.handleClick,
							onKeyDown: this.handleKeyDown,
							tabIndex: '8',
							img: './../inlineSVG/income.svg',
							navItemName: 'Other Income',
							fileName: 'ViewIncomeList.html',
							id: 'extraIncomeLink',
							isAddTxnFormAvailable: true
						}),
						_react2.default.createElement(LeftNavItemsAccordion, {
							navSectionName: 'Cash & Bank',
							img: './../inlineSVG/Cash & Bank.svg',
							navTabName: 'Cash & Bank',
							tabIndex: '9',
							id: 'cashAndBank',
							subItems: [{
								name: 'Bank Accounts',
								class: '',
								ngclick: this.handleClick,
								isAddFormAvailable: true,
								addFormFileName: 'NewBankAccount.html',
								props: { fileName: 'ViewBankAccount.html' }
							}, {
								name: 'Cash In Hand',
								class: '',
								ngclick: this.handleClick,
								isAddFormAvailable: true,
								props: {
									id: 'cashAdjustmentAddForm',
									fileName: 'ViewCashInHand.html'
								}
							}, {
								name: 'Cheques',
								class: '',
								ngclick: this.handleClick,
								props: { fileName: 'ViewOpenCheques.html' }
							}, {
								name: 'Loan Accounts',
								class: '',
								ngclick: this.handleClick,
								isAddFormAvailable: true,
								props: {
									id: 'newLoanAccountForm',
									fileName: 'ViewLoanAccount.html'
								}
							}]
						}),
						_react2.default.createElement(
							'ul',
							null,
							_react2.default.createElement(
								'li',
								{ style: { cursor: 'auto' } },
								_react2.default.createElement('div', { className: 'sideNavSeparator' }),
								_react2.default.createElement('hr', { className: 'separatorLine' })
							)
						),
						_react2.default.createElement(LeftNavItemsTabs, {
							onClick: this.handleClick,
							onKeyDown: this.handleKeyDown,
							tabIndex: '10',
							img: './../inlineSVG/Reports.svg',
							navItemName: 'Reports',
							fileName: 'ViewReports.html',
							id: 'reportsTab'
						}),
						_react2.default.createElement(LeftNavItemsAccordionWithText, {
							navSectionName: 'Auto Sync',
							img: './../inlineSVG/BackupRestore.svg',
							navTabName: 'Status',
							tabIndex: '11',
							subItems: [{ name: 'Login', click: 'onLogin', class: 'terminalButton' }, { name: 'Logout', click: 'onLogout', class: 'logoutButton' }]
						}),
						_react2.default.createElement(LeftNavItemsAccordion, {
							navSectionName: 'Backup/Restore',
							img: './../inlineSVG/BackupRestore.svg',
							navTabName: 'Backup/Restore',
							tabIndex: '12',
							subItems: [{
								name: 'Backup to Computer',
								click: 'localbackup',
								class: 'list-item'
							}, {
								name: 'Backup to Drive',
								click: 'driveBackup',
								class: 'list-item'
							}, {
								name: 'Restore Backup',
								click: 'restoreBackup',
								class: 'list-item'
							}]
						}),
						_react2.default.createElement(LeftNavItemsAccordion, {
							navSectionName: 'Extra',
							img: './../inlineSVG/Utilities.svg',
							navTabName: 'Utilities',
							tabIndex: '13',
							subItems: [{
								name: 'Import Items',
								class: 'list-item importItems',
								ngclick: this.handleClick,
								props: { subItem: 'ImportItems' }
							}, {
								name: 'Import Parties',
								class: 'list-item importParties',
								ngclick: this.handleClick,
								props: { subItem: 'ImportParties' }
							}, { name: 'Export Items', class: 'list-item exportItem' }, { name: 'Referral Code', class: 'list-item referralCode' }, { name: 'Verify My Data', class: 'list-item verifyData' }, {
								name: 'Close Financial Year',
								ngclick: this.checkSyncForCloseBook,
								class: 'list-item'
							}]
						}),
						_react2.default.createElement(LeftNavItemsTabs, {
							onClick: this.handleClick,
							onKeyDown: this.handleKeyDown,
							tabIndex: '14',
							img: './../inlineSVG/Settings.svg',
							navItemName: 'Settings',
							fileName: 'Settings.html',
							id: 'settingsTab'
						}),
						_react2.default.createElement(
							'ul',
							null,
							_react2.default.createElement(
								'li',
								{ style: { cursor: 'auto' } },
								_react2.default.createElement('div', { className: 'sideNavSeparator' }),
								_react2.default.createElement('hr', { className: 'separatorLine' })
							)
						),
						_react2.default.createElement(LeftNavItemsTabs, {
							onClick: this.handleClick,
							onKeyDown: this.handleKeyDown,
							tabIndex: '16',
							img: './../inlineSVG/Refer & Earn.svg',
							navItemName: 'Refer And Earn',
							fileName: 'ReferAndEarn.html',
							id: 'referAndEarnTab'
						}),
						_react2.default.createElement(
							'ul',
							{ id: 'licenseInfo', className: 'dynamic clearfix floatLeft' },
							_react2.default.createElement(LeftNavItemsTabs, {
								onClick: this.handleClick,
								onKeyDown: this.handleKeyDown,
								tabIndex: '17',
								navItemName: 'License Info',
								fileName: 'plans.html',
								img: './../inlineSVG/Trial Info.svg'
							})
						),
						_react2.default.createElement(LeftNavItemsTabs, {
							onClick: this.handleClick,
							onKeyDown: this.handleKeyDown,
							tabIndex: '18',
							navItemName: 'Share Feedback',
							id: 'rateUsTab',
							fileName: 'RatingPage.html',
							img: './../inlineSVG/Rate Us.svg'
						})
					)
				),
				_react2.default.createElement(
					'ul',
					{ className: 'salePopUp' },
					_react2.default.createElement(
						'li',
						{ id: 'trialEndsCustom' },
						_react2.default.createElement(
							LeftNavGeneralItem,
							{
								fileName: 'plans.html',
								onClick: this.handleClick,
								onKeyDown: this.handleKeyDown,
								canLogAnalytics: true
							},
							_react2.default.createElement(
								'div',
								{ tabIndex: '19',
									id: 'licenseTrialPeriod',
									className: 'hide',
									style: { margin: '7px' }
								},
								_react2.default.createElement(
									'div',
									{ id: 'trialText' },
									_react2.default.createElement(
										'div',
										{ className: 'divInsideMenu' },
										_react2.default.createElement(
											'div',
											{ style: { display: 'flex', flexDirection: 'column' } },
											_react2.default.createElement(
												'svg',
												{ width: '31', height: '19', viewBox: '0 0 31 19', fill: 'none', xmlns: 'http://www.w3.org/2000/svg' },
												_react2.default.createElement('path', { d: 'M29.2851 2.19022L22.6066 9.71467L16.2851 0.423913C15.8923 -0.141304 15.0708 -0.141304 14.678 0.423913L8.35656 9.71467L1.7137 2.19022C1.07084 1.4837 -0.0720165 1.97826 -0.000587885 2.93207L1.4637 17.2745C1.4637 17.769 1.89227 18.1929 2.39227 18.1929H28.6066C29.1066 18.1929 29.5351 17.769 29.5351 17.2745L30.9994 2.93207C31.0708 1.97826 29.8923 1.4837 29.2851 2.19022Z', fill: '#FFCB48' })
											),
											_react2.default.createElement(
												'svg',
												{ width: '31', height: '5', viewBox: '0 0 29 5', fill: 'none', xmlns: 'http://www.w3.org/2000/svg' },
												_react2.default.createElement('path', { d: 'M27.3929 4.99998H1.6072C1.00006 4.99998 0.500061 4.50542 0.500061 3.90488V1.25542C0.500061 0.654874 1.00006 0.160309 1.6072 0.160309H27.3929C28.0001 0.160309 28.5001 0.654874 28.5001 1.25542V3.90488C28.5001 4.50542 28.0001 4.99998 27.3929 4.99998Z', fill: '#FFC312' })
											)
										),
										_react2.default.createElement('span', { className: 'textInsideMenu' }),
										_react2.default.createElement(
											'svg',
											{ width: '9', height: '10', viewBox: '0 0 9 10', fill: 'none', xmlns: 'http://www.w3.org/2000/svg' },
											_react2.default.createElement('path', { d: 'M8.86083 4.59279C8.58247 4.54639 8.30412 4.45361 8.02577 4.36083C6.12371 3.71134 5.01031 2.13402 4.63917 0.139175C4.59278 -0.0463918 4.31443 -0.0463918 4.31443 0.139175C3.89691 2.50516 2.50515 4.17526 0.139175 4.59279C-0.0463918 4.63918 -0.0463918 4.91753 0.139175 4.91753C2.50515 5.33505 3.89691 7.00516 4.31443 9.37114C4.36083 9.51031 4.5 9.51031 4.5 9.51031C4.59278 9.51031 4.63918 9.46392 4.68557 9.37114C4.91753 8.16495 5.47423 6.54124 7.09794 5.56701C7.60825 5.24227 8.21134 5.0567 8.86083 4.91753C8.95361 4.91753 9 4.82474 9 4.73196C9 4.68557 8.95361 4.63918 8.86083 4.59279Z', fill: 'white', fillOpacity: '0.75' })
										),
										_react2.default.createElement(
											'div',
											{ style: { marginTop: '-15px' } },
											_react2.default.createElement(
												'svg',
												{ width: '19', height: '20', viewBox: '0 0 19 20', fill: 'none', xmlns: 'http://www.w3.org/2000/svg' },
												_react2.default.createElement('path', { d: 'M18.7141 9.90243C18.1425 9.80717 17.571 9.61665 16.9994 9.42613C13.0937 8.09249 10.8075 4.85364 10.0454 0.75746C9.95015 0.37642 9.3786 0.37642 9.3786 0.75746C8.52126 5.61573 5.66345 9.04509 0.805189 9.90243C0.424149 9.99769 0.424149 10.5693 0.805189 10.5693C5.66345 11.4266 8.52126 14.856 9.3786 19.7142C9.47386 20 9.75963 20 9.75963 20C9.95015 20 10.0454 19.9047 10.1407 19.7142C10.617 17.2375 11.7601 13.9034 15.0942 11.9029C16.1421 11.2361 17.3804 10.855 18.7141 10.5693C18.9046 10.5693 18.9999 10.3787 18.9999 10.1882C18.9999 10.0929 18.9046 9.99769 18.7141 9.90243Z', fill: 'white', fillOpacity: '0.15' })
											)
										)
									),
									_react2.default.createElement(
										'div',
										{ className: 'width100 see-plans' },
										_react2.default.createElement('span', { className: 'subText fontCustom12' }),
										_react2.default.createElement(
											'div',
											{ className: 'right-arrow' },
											_react2.default.createElement(
												'svg',
												{ width: '15', height: '12', viewBox: '0 0 15 12', fill: 'none', xmlns: 'http://www.w3.org/2000/svg' },
												_react2.default.createElement('path', { d: 'M0.75 6.75002L12.6 6.75002L9.15 10.725C8.85 11.025 8.925 11.475 9.225 11.775C9.525 12.075 9.975 12 10.275 11.7L14.775 6.45002C14.775 6.45002 14.85 6.37502 14.85 6.30002C14.85 6.30002 14.85 6.30002 14.85 6.22502C15 6.22502 15 6.15002 15 6.07502L15 6.00002C15 5.92502 15 5.85002 15 5.77502L15 5.70002C15 5.70002 15 5.70002 15 5.62502C15 5.55002 14.925 5.47502 14.925 5.47502L10.425 0.225024C10.2 0.0750231 9.975 2.33275e-05 9.75 2.33472e-05C9.6 2.33603e-05 9.375 0.0750232 9.225 0.150023C8.925 0.450023 8.85 0.900024 9.15 1.20002L12.6 5.25002L0.75 5.25002C0.300001 5.25002 3.92772e-07 5.55002 4.32112e-07 6.00002C4.71452e-07 6.45002 0.375 6.75002 0.75 6.75002Z', fill: '#FFC312' })
											)
										)
									)
								)
							)
						)
					)
				),
				_react2.default.createElement(
					'ul',
					{ className: 'salePopUp' },
					_react2.default.createElement(
						'li',
						null,
						isLicenseInvalid && _react2.default.createElement(_FestiveOffer2.default, null)
					)
				),
				_react2.default.createElement('div', {
					onClick: function onClick() {
						return _this2.handleClick({ fileName: 'ForgotPassword.html' });
					},
					accessKey: '!'
				}),
				_react2.default.createElement('div', {
					onClick: function onClick() {
						return _this2.handleClick({ fileName: 'EnableSync.html' });
					},
					accessKey: '%'
				})
			);
		}
	}]);
	return LeftSideNav;
}(_react2.default.Component);

var _initialiseProps = function _initialiseProps() {
	var _this3 = this;

	this.checkSyncForCloseBook = function () {
		// var SyncHandler=require('./../../../Utilities/SyncHandler.js');
		// if(!SyncHandler.isSyncEnabled()) {
		// 	MyAnalytics.pushEvent("Close financial year open");
		//
		// }else{
		// 	ToastHelper.error("You can't close Financial Year as Sync is Enabled." );
		// }
		_this3.handleClick({ fileName: 'CloseBook.html' });
	};

	this.handleKeyDown = function (props, e) {
		if (e.keyCode === 13) {
			_this3.handleClick(props);
		}
	};

	this.handleKeyDownForSalePurchase = function (props, e) {
		if (e.keyCode === 13) {
			e.target.click();
		}
	};

	this.handleClose = function (saveChanges) {
		try {
			var frameDiv = document.getElementById('frameDiv');
			_reactDom2.default.unmountComponentAtNode(frameDiv);
			saveChanges && window.onResume && window.onResume();
		} catch (err) {}
		/* window.canCloseDialogue = function () {
  	return true;
  }; */
	};
};

exports.default = LeftSideNav;

LeftSideNav.propTypes = {
	isFirstSaleTxnCreated: _propTypes2.default.bool
};