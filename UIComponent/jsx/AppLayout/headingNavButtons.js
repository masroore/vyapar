Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _QuickNav = require('./QuickNav');

var _QuickNav2 = _interopRequireDefault(_QuickNav);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles() {
	return {
		shortcutButton: {
			height: '42px',
			boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.16)',
			textTransform: 'capitalize',
			background: '#0075E8', // '#1492E6',
			color: 'white',
			marginRight: '1rem',
			borderRadius: '30px',
			display: 'flex',
			alignItems: 'center',
			justifyContent: 'center',
			position: 'relative',
			'&:hover, &:focus': {
				transform: 'scale(1.05)',
				boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.16)'
			},
			'& span': {
				fontSize: '14px',
				fontFamily: 'robotoMedium',
				lineHeight: '17px'
			}
		},
		buttonAddSale: {
			width: '125px',
			background: '#ED1A3B',
			'&:hover': {
				background: '#ED1A3B'
			}
		},
		buttonAddPurchase: {
			width: '142px'
		},
		AddMore: {
			width: '133px',
			background: '#FFFFFF',
			'&:hover': {
				background: '#FFFFFF'
			},
			color: '#0075E8',
			border: 'solid 1px #0075E8'
		},
		buttonRounded: {
			borderRadius: '30px',
			paddingRight: '12px'
		},
		headingNavButtonSpan: {
			marginLeft: '8px',
			marginTop: '2px'
		}
	};
};

var HeadingNavButtons = function (_React$Component) {
	(0, _inherits3.default)(HeadingNavButtons, _React$Component);

	function HeadingNavButtons() {
		(0, _classCallCheck3.default)(this, HeadingNavButtons);
		return (0, _possibleConstructorReturn3.default)(this, (HeadingNavButtons.__proto__ || (0, _getPrototypeOf2.default)(HeadingNavButtons)).apply(this, arguments));
	}

	(0, _createClass3.default)(HeadingNavButtons, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			setTimeout(function () {
				$('.quickNavButton').tooltipster({
					theme: 'tooltipster-quicknav',
					maxWidth: '70vw',
					minWidth: 750,
					trigger: 'click',
					side: 'bottom',
					interactive: true,
					distance: 20,
					zIndex: 91,
					delay: 0,
					functionReady: function functionReady() {
						checkOrderFormEnabled();
						checkIncomeEnabled();
						checkEstimateEnabled();
						checkDeliveryChallanEnabled();
						$('.shortcutsList #TXN_TYPE_SALE').focus();
						$('.shortcutsList .quickNavItem:visible').last().off('focusout').focusout(function () {
							$('.quickNavButton').tooltipster('close');
						});
					},
					functionBefore: function functionBefore() {
						$('.dynamicDiv').addClass('addBlurOverlay');
					},
					functionAfter: function functionAfter() {
						$('.dynamicDiv').removeClass('addBlurOverlay');
					},
					arrow: true
				});
			}, 1000);
			setTimeout(function () {
				$('#addMoreSpan').css('margin-left', $('#addMoreButton').offset().left - 567 + 'px');
			}, 1005);
			// after some calculations using offset left of addmore popup and parent width of addmore button 567 was substracted.
		}
	}, {
		key: 'render',
		value: function render() {
			var classes = this.props.classes;


			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement(
					'button',
					{
						id: 'TXN_TYPE_SALE',
						'data-eventname': 'Sale',
						className: (0, _classnames2.default)(classes.shortcutButton, classes.buttonAddSale, 'changeDefaultPageSP ripple')
					},
					_react2.default.createElement(
						'svg',
						{ xmlns: 'http://www.w3.org/2000/svg', width: '20', height: '20', viewBox: '0 0 20 20' },
						_react2.default.createElement('path', { fill: '#FFFFFF', d: 'M66,20A10,10,0,1,0,76,30,10,10,0,0,0,66,20Zm5,11H67v4a1,1,0,0,1-2,0V31H61a1,1,0,0,1,0-2h4V25a1,1,0,0,1,2,0v4h4a1,1,0,0,1,0,2Z', transform: 'translate(-56 -20)' })
					),
					_react2.default.createElement(
						'span',
						{ className: (0, _classnames2.default)(classes.headingNavButtonSpan) },
						'Add Sale'
					)
				),
				_react2.default.createElement(
					'button',
					{
						id: 'TXN_TYPE_PURCHASE',
						'data-eventname': 'Purchase',
						className: (0, _classnames2.default)(classes.shortcutButton, classes.buttonAddPurchase, 'changeDefaultPageSP ripple')
					},
					_react2.default.createElement(
						'svg',
						{ xmlns: 'http://www.w3.org/2000/svg', width: '20', height: '20', viewBox: '0 0 20 20' },
						_react2.default.createElement('path', { fill: '#FFFFFF', d: 'M66,20A10,10,0,1,0,76,30,10,10,0,0,0,66,20Zm5,11H67v4a1,1,0,0,1-2,0V31H61a1,1,0,0,1,0-2h4V25a1,1,0,0,1,2,0v4h4a1,1,0,0,1,0,2Z', transform: 'translate(-56 -20)' })
					),
					_react2.default.createElement(
						'span',
						{ className: (0, _classnames2.default)(classes.headingNavButtonSpan) },
						'Add Purchase'
					)
				),
				_react2.default.createElement(
					'button',
					{ 'data-tooltip-content': '#shortcuts', id: 'addMoreButton',
						className: (0, _classnames2.default)(classes.shortcutButton, classes.AddMore, 'ripple', 'quickNavButton'),
						'data-shortcut': 'CTRL_ENTER'
					},
					_react2.default.createElement(
						'svg',
						{ xmlns: 'http://www.w3.org/2000/svg', width: '20', height: '20', viewBox: '0 0 20 20' },
						_react2.default.createElement('path', { fill: '#0075e8', d: 'M66,20A10,10,0,1,0,76,30,10,10,0,0,0,66,20Zm5,11H67v4a1,1,0,0,1-2,0V31H61a1,1,0,0,1,0-2h4V25a1,1,0,0,1,2,0v4h4a1,1,0,0,1,0,2Z', transform: 'translate(-56 -20)' })
					),
					_react2.default.createElement(
						'span',
						{ className: (0, _classnames2.default)(classes.headingNavButtonSpan) },
						'Add More'
					)
				),
				_react2.default.createElement(
					'div',
					{ id: 'testing', className: 'hide' },
					_react2.default.createElement(_QuickNav2.default, { handleClick: function handleClick() {
							$('.quickNavButton').tooltipster('close');
						} })
				)
			);
		}
	}]);
	return HeadingNavButtons;
}(_react2.default.Component);

HeadingNavButtons.propTypes = {
	classes: _propTypes2.default.object
};

exports.default = (0, _styles.withStyles)(styles)(HeadingNavButtons);