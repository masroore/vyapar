Object.defineProperty(exports, "__esModule", {
	value: true
});

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _MultiSelectionFilter = require('./grid-filters/MultiSelectionFilter');

var _MultiSelectionFilter2 = _interopRequireDefault(_MultiSelectionFilter);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Items = function (_React$Component) {
	(0, _inherits3.default)(Items, _React$Component);

	function Items(props) {
		(0, _classCallCheck3.default)(this, Items);

		var _this = (0, _possibleConstructorReturn3.default)(this, (Items.__proto__ || (0, _getPrototypeOf2.default)(Items)).call(this, props));

		_this.selectFirstRow = function () {
			setTimeout(function () {
				var firstColumn = document.querySelector('.left-column .ag-row-selected .ag-cell[col-id="itemName"]');
				firstColumn && firstColumn.focus();
			}, 100);
		};

		_this.columnDefs = [{
			field: 'itemName',
			headerName: 'ITEM',
			width: 150,
			sort: 'asc',
			filter: _MultiSelectionFilter2.default,
			filterOptions: [{
				value: 1,
				label: 'Active',
				selected: true
			}, {
				value: 0,
				label: 'Inactive',
				selected: false
			}],
			filterFunction: function filterFunction(params, checkedFilterOptions) {
				var isActive = params.data.getIsItemActive();
				var filtered = checkedFilterOptions.find(function (option) {
					return option.selected && option.value == isActive;
				});
				return !!filtered;
			},
			cellClass: function cellClass(params) {
				return params.data.getIsItemActive() == '1' ? 'fontColorBlack' : 'fontColorGrey';
			}
		}];
		if (!_this.props.isService) {
			_this.columnDefs.push({ field: 'itemStockQuantity',
				suppressMenu: true,
				comparator: function comparator(a, b) {
					return a - b;
				},
				headerName: 'QUANTITY',
				width: 90,
				cellClass: 'alignRight',
				headerClass: 'alignRight',
				cellRenderer: function cellRenderer(params) {
					return params.data.getIsItemActive() == '1' ? _MyDouble2.default.getQuantityWithDecimal(params.value) : "<span style='color:#bdbdbd'>" + _MyDouble2.default.getQuantityWithDecimalWithoutColor(params.value) + '</span>';
				} });
		}
		_this.columnDefs.push({
			field: 'itemId',
			headerName: '',
			width: 30,
			getQuickFilterText: function getQuickFilterText() {
				return '';
			},
			suppressSorting: true,
			suppressFilter: true,
			cellRenderer: function cellRenderer() {
				return '<div class="contextMenuDots"></div>';
			}
		});
		_this.openItem = _this.openItem.bind(_this);
		_this.deleteItem = _this.deleteItem.bind(_this);
		_this.refreshItemCells = _this.refreshItemCells.bind(_this);
		_this.getApi = _this.getApi.bind(_this);
		_this.contextMenu = [{
			title: 'View/Edit',
			cmd: _this.openItem,
			visible: true,
			id: 'edit'
		}, {
			title: 'Delete',
			cmd: _this.deleteItem,
			visible: true,
			id: 'delete'
		}];
		return _this;
	}

	(0, _createClass3.default)(Items, [{
		key: 'getApi',
		value: function getApi(params) {
			this.api = params.api;
			params.api.getFilterInstance('itemName').applyFilter();
			if (this.api) {
				this.api.setRowData(this.props.items);
			}
		}
	}, {
		key: 'openItem',
		value: function openItem(itemLogic) {
			if (itemLogic.data && itemLogic.data.getItemName) {
				itemLogic = itemLogic.data;
			}
			window.itemNameGlobal = itemLogic.getItemName();
			$('#modelContainer').css('display', 'block');
			$('.viewItems').css('display', 'none');
			$('#frameDiv').load('NewItems.html');
			$('#defaultPage').hide();
		}
	}, {
		key: 'deleteItem',
		value: function deleteItem(itemLogic) {
			var ItemCache = require('../../Cache/ItemCache');
			var ErrorCode = require('../../Constants/ErrorCode');
			var item = itemLogic.getItemName();
			var itemCache = new ItemCache();
			var itemObj = itemCache.findItemByName(item, null, itemLogic.getItemType());

			if (itemObj && itemObj.canDeleteItem()) {
				$('#delDialog2').dialog({
					title: 'Are you sure you want to delete this Item?',
					closeOnEscape: true,
					resizable: false,
					height: 150,
					minHeight: 150,
					maxHeight: 150,
					width: 500,
					appendTo: '#dynamicDiv',
					modal: true,
					maxWidth: 500,

					buttons: [{
						text: 'Yes',
						// id: "delButtonY",
						class: 'terminalButton',
						// icon: "ui-icon-heart",
						click: function click() {
							var statusCode = itemObj.deleteItem();
							if (statusCode == ErrorCode.ERROR_ITEM_DELETE_SUCCESS) {
								ToastHelper.success(statusCode);
								onResume();
								try {
									$(this).dialog('close');
								} catch (ex) {}
							} else {
								ToastHelper.error(statusCode);
							}
						}
					}, {
						text: 'No',
						// id: "delButtonN",
						class: 'terminalButton',
						click: function click() {
							try {
								$(this).dialog('close');
							} catch (ex) {}
						}
					}]
				});
			} else {
				ToastHelper.error(ErrorCode.ERROR_ITEM_DELETE_FAILED);
			}
		}
	}, {
		key: 'refreshItemCells',
		value: function refreshItemCells(selectedUnits, activeOrInactive) {
			var rows = [];
			this.api.refreshCells();
			for (var i = 0; i < selectedUnits.length; i++) {
				var row = this.api.getRowNode(selectedUnits[i]);
				if (activeOrInactive == 'inactive') {
					row.data.setIsItemActive('0');
				} else {
					row.data.setIsItemActive('1');
				}
				rows.push(row);
			}
			this.api.redrawRows({ rowNodes: rows });
		}
	}, {
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(nextProps) {
			var _this2 = this;

			if (this.api) {
				this.api.setRowData(nextProps.items);
				setTimeout(function () {
					if (_this2.api.getSelectedRows().length === 0) {
						var selected = _this2.api.getDisplayedRowAtIndex(0);
						if (selected) {
							selected.setSelected(true);
						}
					}
				});
			}
			return false;
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.selectFirstRow();
			if (this.props.onRef) {
				this.props.onRef(this);
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var gridOptions = (0, _defineProperty3.default)({
				enableFilter: true,
				enableSorting: true,
				rowSelection: 'single',
				columnDefs: this.columnDefs,
				rowData: [],
				onRowDoubleClicked: this.openItem,
				suppressColumnMoveAnimation: true,
				onRowSelected: this.props.onItemSelected,
				overlayNoRowsTemplate: 'No items to show',
				headerHeight: 40,
				rowHeight: 40,
				deltaRowDataMode: true,
				getRowNodeId: function getRowNodeId(data) {
					return data.getItemId();
				}
			}, 'onRowDoubleClicked', this.openItem);
			return _react2.default.createElement(
				'div',
				{ className: 'gridContainer d-flex' },
				_react2.default.createElement(
					'div',
					{ id: 'delDialog2', className: 'hide' },
					_react2.default.createElement(
						'b',
						null,
						'This Item will be Deleted.'
					)
				),
				_react2.default.createElement(_Grid2.default, {
					getApi: this.getApi,
					gridKey: this.props.isService ? 'itemsListForServicesGridKey' : 'itemsListForProductsGridKey',
					contextMenu: this.contextMenu,
					classes: 'noBorder gridRowHeight40',
					selectFirstRow: true,
					height: '100%',
					gridOptions: gridOptions
				})
			);
		}
	}]);
	return Items;
}(_react2.default.Component);

exports.default = Items;