Object.defineProperty(exports, "__esModule", {
	value: true
});

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _typeof2 = require('babel-runtime/helpers/typeof');

var _typeof3 = _interopRequireDefault(_typeof2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _trashableReact = require('trashable-react');

var _trashableReact2 = _interopRequireDefault(_trashableReact);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _DataLoader = require('../../DBManager/DataLoader');

var _DataLoader2 = _interopRequireDefault(_DataLoader);

var _IPCActions = require('../../Constants/IPCActions');

var _MyDate = require('../../Utilities/MyDate');

var _MyDate2 = _interopRequireDefault(_MyDate);

var _TransactionHelper = require('../../Utilities/TransactionHelper');

var TransactionHelper = _interopRequireWildcard(_TransactionHelper);

var _DBWindow = require('../../Utilities/DBWindow');

var _DBWindow2 = _interopRequireDefault(_DBWindow);

var _TxnTypeConstant = require('../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _StringConstants = require('../../Constants/StringConstants');

var _StringConstants2 = _interopRequireDefault(_StringConstants);

var _DateFilter = require('./grid-filters/DateFilter');

var _DateFilter2 = _interopRequireDefault(_DateFilter);

var _NumberFilter = require('./grid-filters/NumberFilter');

var _NumberFilter2 = _interopRequireDefault(_NumberFilter);

var _StringFilter = require('./grid-filters/StringFilter');

var _StringFilter2 = _interopRequireDefault(_StringFilter);

var _MultiSelectionFilter = require('./grid-filters/MultiSelectionFilter');

var _MultiSelectionFilter2 = _interopRequireDefault(_MultiSelectionFilter);

var _NewTransactionButton = require('./NewTransactionButton');

var _NewTransactionButton2 = _interopRequireDefault(_NewTransactionButton);

var _FirstScreen = require('./UIControls/FirstScreen');

var _FirstScreen2 = _interopRequireDefault(_FirstScreen);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var OrderContainer = function (_React$Component) {
	(0, _inherits3.default)(OrderContainer, _React$Component);

	function OrderContainer(props) {
		(0, _classCallCheck3.default)(this, OrderContainer);

		var _this = (0, _possibleConstructorReturn3.default)(this, (OrderContainer.__proto__ || (0, _getPrototypeOf2.default)(OrderContainer)).call(this, props));

		_this.getApi = function (params) {
			_this.api = params.api;
			_this.api.setRowData(_this.allOrders);
		};

		_this.allOrders = [];
		_this.contextMenu = [{
			title: 'View/Edit',
			cmd: _this.viewTransaction,
			visible: true,
			id: 'edit'
		}, {
			title: 'Delete',
			cmd: function cmd(row) {
				return TransactionHelper.deleteTransaction(row.id, row.txnTypeConstant);
			},
			visible: true,
			id: 'delete'
		}, {
			title: 'Open PDF',
			cmd: function cmd(row) {
				return TransactionHelper.openPDF(row.id, row.txnTypeConstant);
			},
			visible: true,
			id: 'openPDF'
		}, {
			title: 'Preview',
			cmd: function cmd(row) {
				return TransactionHelper.preview(row.id, row.txnTypeConstant);
			},
			visible: true,
			id: 'preview'
		}, {
			title: 'Print',
			cmd: function cmd(row) {
				return TransactionHelper.print(row.id, row.txnTypeConstant);
			},
			visible: true,
			id: 'print'
		},
		// {title: 'Return', cmd: row => TransactionHelper.returnTransaction(row.id, row.txnTypeConstant), visible: true, id: 'rtn'},
		{
			title: 'Receive Payment',
			cmd: function cmd(row) {
				return TransactionHelper.receiveOrMakePayment(row.id, row.txnTypeConstant);
			},
			visible: true,
			id: 'receivePayment'
		}, {
			title: 'Payment History',
			cmd: function cmd(row) {
				return window.showPaymentHistory(row.id + ':' + row.txnTypeConstant);
			},
			visible: true,
			id: 'paymentHistory'
		}];
		_this.state = {
			totalTransactions: 0,
			records: [],
			columnDefs: [
			// {
			// 	field: 'color',
			// 	headerName: '',
			// 	width: 20,
			// 	suppressMenu: false,
			// 	getQuickFilterText: function () {
			// 		return '';
			// 	},
			// 	suppressFilter: true,
			// 	cellRenderer: function (params) {
			// 		return params.value;
			// 	}
			// },
			{
				field: 'name',
				filter: _StringFilter2.default,
				headerName: 'PARTY',
				width: 180
			}, {
				field: 'orderNumber',
				colId: 'orderNumber',
				filter: _StringFilter2.default,
				headerName: 'NO.',
				width: 80,
				comparator: function comparator(a, b) {
					var typeA = typeof a === 'undefined' ? 'undefined' : (0, _typeof3.default)(a);
					var typeB = typeof b === 'undefined' ? 'undefined' : (0, _typeof3.default)(b);
					if (typeA === 'number' && typeB === 'number') {
						return a - b;
					} else if (typeA === 'string' && typeB === 'string') {
						return a.localeCompare(b);
					} else {
						if (typeA === 'number') {
							return -1;
						} else {
							return 1;
						}
						// return a > b ? 1 : a < b ? -1 : 0;
					}
				},
				valueGetter: function valueGetter(params) {
					var value = Number(params.data.orderNumber);
					if (isNaN(value)) {
						return params.data.orderNumber;
					}
					return value;
				},
				getQuickFilterText: function getQuickFilterText(params) {
					return params.data.orderNumberWithPrefix;
				},
				cellRenderer: function cellRenderer(params) {
					return params.data.orderNumberWithPrefix;
				}
			}, {
				field: 'date',
				headerName: 'DATE',
				width: 100,
				sort: 'desc',
				filter: _DateFilter2.default,
				sortingOrder: ['desc', 'asc', null],
				getQuickFilterText: function getQuickFilterText(params) {
					return _MyDate2.default.getDate('d/m/y', params.value);
				},
				cellRenderer: function cellRenderer(params) {
					return _MyDate2.default.getDate('d/m/y', params.value);
				}
			}, {
				field: 'dueDate',
				headerName: 'DUE DATE',
				width: 100,
				filter: _DateFilter2.default,
				sortingOrder: ['desc', 'asc', null],
				getQuickFilterText: function getQuickFilterText(params) {
					return _MyDate2.default.getDate('d/m/y', params.value);
				},
				cellRenderer: function cellRenderer(params) {
					return _MyDate2.default.getDate('d/m/y', params.value);
				}
			}, {
				field: 'amount',
				headerName: 'TOTAL AMOUNT',
				width: 150,
				filter: _NumberFilter2.default,
				cellClass: 'alignRight',
				getQuickFilterText: function getQuickFilterText(params) {
					return params.value < 0 ? '-' : params.value;
				},
				cellRenderer: function cellRenderer(params) {
					if (params.value < 0) {
						return '-';
					} else {
						return _MyDouble2.default.getAmountWithDecimalAndCurrencyWithoutSign(params.value);
					}
				}
			}, {
				field: 'balance',
				headerName: 'BALANCE',
				width: 150,
				cellClass: 'alignRight',
				filter: _NumberFilter2.default,
				getQuickFilterText: function getQuickFilterText(params) {
					return params.value < 0 ? '-' : params.value;
				},
				cellRenderer: function cellRenderer(params) {
					if (params.value < 0) {
						return '-';
					} else {
						return _MyDouble2.default.getAmountWithDecimalAndCurrencyWithoutSign(params.value);
					}
				}
			}, {
				field: 'typeId',
				suppressMenu: true,
				filter: _MultiSelectionFilter2.default,
				filterOptions: _TxnTypeConstant2.default.getFilterOptionsForOrders(props.selectedTxnType),
				getQuickFilterText: function getQuickFilterText(params) {
					return params.data.type;
				},
				cellRenderer: function cellRenderer(params) {
					return params.data.type;
				},
				headerName: 'TYPE',
				width: 110
			}, {
				field: 'status',
				headerName: 'STATUS',
				width: 120,
				filter: _MultiSelectionFilter2.default,
				filterOptions: [{
					value: 0,
					label: 'Open',
					selected: false
				}, {
					value: 1,
					label: 'Overdue',
					selected: false
				}, {
					value: 2,
					label: 'Completed',
					selected: false
				}],
				filterFunction: function filterFunction(params, checkedFilterOptions) {
					var status = params.data.status;
					var statusArr = [_StringConstants2.default.orderOpen, _StringConstants2.default.orderOverdueString, _StringConstants2.default.orderCompletedString];
					var selected = checkedFilterOptions.find(function (option) {
						return option.value == statusArr.indexOf(status);
					});
					return selected;
				},
				getQuickFilterText: function getQuickFilterText(params) {
					return params.value;
				},
				cellRenderer: function cellRenderer(params) {
					var statusColor = params.data.statusColor;

					return '<div style="color: ' + statusColor + '">' + params.value + '</div>';
				}
			}, {
				field: 'action',
				onCellClicked: _this.convertTo,
				headerName: 'ACTION',
				width: 180,
				suppressFilter: true,
				getQuickFilterText: function getQuickFilterText(params) {
					return params.value;
				},
				cellRenderer: function cellRenderer(params) {
					return '<div class="convertTo">' + params.value + '</div>';
				}
			}, {
				field: 'id',
				headerName: '',
				width: 50,
				getQuickFilterText: function getQuickFilterText() {
					return '';
				},
				suppressFilter: true,
				cellRenderer: function cellRenderer() {
					return '<div class="contextMenuDots"></div>';
				}
			}],
			contextMenu: _this.contextMenu
		};

		_this._loaded = [];
		_this._perPage = 100;
		_this._id = null;
		_this.getData = _this.getData.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(OrderContainer, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.loadTransactions(0, this._perPage, {
				start: this._perPage,
				noOfRecords: -1
			});
			TransactionHelper.bindEventsForPreviewDialog();
			MyAnalytics.pushScreen('View Order Form');
			window.onResume = this.onResume.bind(this);
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			TransactionHelper.unBindEventsForPreviewDialog();
			delete window.onResume;
		}
	}, {
		key: 'onResume',
		value: function onResume() {
			this.loadTransactions(0, this._perPage, {
				start: this._perPage,
				noOfRecords: -1
			});
			TransactionHelper.bindEventsForPreviewDialog();
		}
	}, {
		key: 'viewTransaction',
		value: function viewTransaction(row) {
			if (!row.id) {
				row = row.data;
			}
			TransactionHelper.viewTransaction(row.id, row.txnTypeConstant);
		}
	}, {
		key: 'getData',
		value: function getData(event, response) {
			if (response.meta.next && response.data.length == this._perPage) {
				var next = response.meta.next;
				this.loadTransactions(next.start, next.noOfRecords);
			}

			var data = response.data.map(function (row) {
				row.date = new Date(row.date);
				row.dueDate = new Date(row.dueDate);
				return row;
			});
			if (response.meta.next) {
				this.allOrders = data;
			} else {
				this.allOrders = [].concat((0, _toConsumableArray3.default)(this.allOrders), (0, _toConsumableArray3.default)(data));
			}
			if (this.api) {
				this.api.setRowData(this.allOrders);
			}
		}
	}, {
		key: 'loadTransactions',
		value: function loadTransactions(start, noOfRecords, next) {
			var _this2 = this;

			var dataLoader = new _DataLoader2.default();
			var selectedTxnType = this.props.selectedTxnType;

			if (selectedTxnType) {
				this.setState({ totalTransactions: dataLoader.getTransactionCountByTxnType(selectedTxnType) });
			} else {
				this.setState({ totalTransactions: dataLoader.getTransactionCountByTxnType([_TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER, _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER]) });
			}
			// this.totalTransactions = 0;
			this.props.registerPromise(_DBWindow2.default.send(_IPCActions.GET_ORDER_TRANSACTIONS, {
				start: start,
				noOfRecords: noOfRecords,
				next: next
			})).then(function (data) {
				_this2.getData(null, data);
			});
		}
	}, {
		key: 'filterContextMenu',
		value: function filterContextMenu(row) {
			var menuItems = TransactionHelper.filterContextMenu([].concat((0, _toConsumableArray3.default)(this.contextMenu)), row);
			this.setState({ contextMenu: menuItems });
		}
	}, {
		key: 'convertTo',
		value: function convertTo(params) {
			var _params$data = params.data,
			    id = _params$data.id,
			    txnTypeConstant = _params$data.txnTypeConstant;

			TransactionHelper.returnTransaction(id, txnTypeConstant);
		}
	}, {
		key: 'onTransactionSelected',
		value: function onTransactionSelected(row) {
			this.currentTransaction = row;
		}
	}, {
		key: 'initializeFilter',
		value: function initializeFilter(params) {
			var _props$selectedStatus = this.props.selectedStatusFilter,
			    selectedStatusFilter = _props$selectedStatus === undefined ? [] : _props$selectedStatus;

			var filterOptions = [{
				value: 0,
				label: 'Open',
				selected: selectedStatusFilter.includes(_StringConstants2.default.orderOpen)
			}, {
				value: 1,
				label: 'Overdue',
				selected: selectedStatusFilter.includes(_StringConstants2.default.orderOverdueString)
			}, {
				value: 2,
				label: 'Completed',
				selected: selectedStatusFilter.includes(_StringConstants2.default.orderCompletedString)
			}];
			var statusInstance = params.api.getFilterInstance('status');
			if (statusInstance) {
				statusInstance.setModel({
					value: {
						filterOptions: filterOptions,
						checkedFilterOptions: filterOptions.filter(function (option) {
							return option.selected;
						})
					}
				});
				statusInstance.applyFilter();
			}
			var typeIdInstance = params.api.getFilterInstance('typeId');
			if (typeIdInstance) {
				typeIdInstance.applyFilter();
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var totalTransactions = this.state.totalTransactions;

			var gridOptions = {
				enableFilter: true,
				enableSorting: true,
				rowSelection: 'single',
				columnDefs: this.state.columnDefs,
				rowData: [],
				rowHeight: 40,
				headerHeight: 40,
				rowClass: 'customVerticalBorder',
				onRowSelected: this.onTransactionSelected.bind(this),
				onRowDoubleClicked: this.viewTransaction.bind(this),
				onGridReadyCallback: this.initializeFilter.bind(this),
				getRowNodeId: function getRowNodeId(data) {
					return data.id;
				}
			};
			var selectedTxnType = this.props.selectedTxnType;

			var image = '';
			var text = '';
			var buttonLabel = '';

			if (!selectedTxnType || selectedTxnType === _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER) {
				image = '../inlineSVG/first-screens/order.svg';
				text = 'Make & share sale orders & convert them to sale invoice instantly.';
				buttonLabel = 'Add Your First Sale Order';
			} else {
				image = '../inlineSVG/first-screens/purchase.svg';
				text = 'Make & share purchase orders with your parties & convert them to purchase bill instantly.';
				buttonLabel = 'Add Your First Purchase Order';
			}
			return _react2.default.createElement(
				'div',
				{ className: 'd-flex flex-column', style: { height: '100%' } },
				_react2.default.createElement(
					'ul',
					{ className: 'tab-container d-flex' },
					_react2.default.createElement(
						'li',
						{ className: 'tab-items col' },
						'ORDERS'
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'd-flex flex-column flex-container flex-grow-1' },
					totalTransactions === 0 && _react2.default.createElement(_FirstScreen2.default, {
						image: image,
						text: text,
						onClick: function onClick() {
							TransactionHelper.viewTransaction('', _TxnTypeConstant2.default.getTxnType(selectedTxnType || _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER));
						},
						buttonLabel: buttonLabel
					}),
					totalTransactions > 0 && _react2.default.createElement(_Grid2.default, {
						gridKey: 'ordersTransactionsGridKey',
						title: 'Transactions',
						quickFilter: true,
						CTAButton: selectedTxnType ? _react2.default.createElement(_NewTransactionButton2.default, {
							txnType: _TxnTypeConstant2.default.getTxnType(selectedTxnType),
							label: '+ Add ' + _TxnTypeConstant2.default.getTxnTypeForUI(selectedTxnType)
						}) : false,
						classes: 'alternateRowColor customVerticalBorder gridRowHeight40',
						filterContextMenu: this.filterContextMenu.bind(this),
						contextMenu: this.state.contextMenu,
						getApi: this.getApi,
						gridOptions: gridOptions
					})
				)
			);
		}
	}]);
	return OrderContainer;
}(_react2.default.Component);

OrderContainer.propTypes = {
	registerPromise: _propTypes2.default.func,
	selectedStatusFilter: _propTypes2.default.array,
	selectedTxnType: _propTypes2.default.oneOfType([_propTypes2.default.number, _propTypes2.default.string])
};
exports.default = (0, _trashableReact2.default)(OrderContainer);