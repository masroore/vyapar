Object.defineProperty(exports, "__esModule", {
	value: true
});

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Checkbox = require('@material-ui/core/Checkbox');

var _Checkbox2 = _interopRequireDefault(_Checkbox);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _DecimalInputFilter = require('../../Utilities/DecimalInputFilter');

var _DecimalInputFilter2 = _interopRequireDefault(_DecimalInputFilter);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PaymentTermList = function (_React$Component) {
	(0, _inherits3.default)(PaymentTermList, _React$Component);

	function PaymentTermList() {
		(0, _classCallCheck3.default)(this, PaymentTermList);
		return (0, _possibleConstructorReturn3.default)(this, (PaymentTermList.__proto__ || (0, _getPrototypeOf2.default)(PaymentTermList)).apply(this, arguments));
	}

	(0, _createClass3.default)(PaymentTermList, [{
		key: 'render',
		value: function render() {
			var dataColumns = this.props.columns;
			var dataRows = this.props.data;
			var options = this.props.options;
			var disableEdit = this.props.disableEdit;

			var tableHeaders = _react2.default.createElement(
				'thead',
				null,
				_react2.default.createElement(
					'tr',
					null,
					dataColumns.map(function (column) {
						return _react2.default.createElement(
							'th',
							{ className: 'paymentTermHead' },
							column['headerName']
						);
					})
				)
			);
			var tableBody = _react2.default.createElement(
				'tbody',
				null,
				(0, _keys2.default)(dataRows).map(function (key) {
					var row = dataRows[key];
					return _react2.default.createElement(
						'tr',
						{ key: key },
						_react2.default.createElement(
							'td',
							{ className: 'paymentTermTd' },
							_react2.default.createElement('input', { type: 'text', disabled: row.disabled, className: 'paymentTermInputTerm', onChange: function onChange(e) {
									return options.onChange('term', key, e);
								}, onInput: function onInput(e) {
									return _DecimalInputFilter2.default.inputTextFilterForLength(e.target, 15, false);
								}, value: row.term })
						),
						_react2.default.createElement(
							'td',
							{ className: 'paymentTermTd' },
							_react2.default.createElement('input', { type: 'number', disabled: row.disabled || !row.is_editable, className: 'paymentTermInputDays', onInput: function onInput(e) {
									return _DecimalInputFilter2.default.inputTextFilterForNumber(e.target, 3);
								}, onChange: function onChange(e) {
									return options.onChange('days', key, e);
								}, value: row.days })
						),
						_react2.default.createElement(
							'td',
							{ className: 'paymentTermTd' },
							_react2.default.createElement(_Checkbox2.default, {
								checked: !!row.is_default,
								onChange: function onChange(e) {
									return options.modifyIsDefault(key, e);
								},
								color: 'primary',
								className: 'checkboxPrimary'
							})
						),
						_react2.default.createElement(
							'td',
							{ className: 'paymentTermTd' },
							row.show_edit && _react2.default.createElement('input', { type: 'image', src: './../inlineSVG/pencil_grey.svg', disabled: disableEdit, onClick: function onClick() {
									return options.editPaymentTermObj(key);
								} }),
							row.show_update && _react2.default.createElement('img', { src: './../inlineSVG/outline-check_circle_outline-24px.svg', onClick: function onClick() {
									return options.updatePaymentTermObj(key);
								} })
						),
						_react2.default.createElement(
							'td',
							{ className: 'paymentTermTd' },
							row.show_revert && _react2.default.createElement('img', { src: './../inlineSVG/outline-cancel-24px.svg', onClick: function onClick() {
									return options.revertPaymentTermObj(key);
								} }),
							row.show_delete && _react2.default.createElement('img', { src: './../inlineSVG/baseline-delete-24px.svg', disabled: row.disabled, onClick: function onClick() {
									return options.deletePaymentTermObj(key);
								} })
						)
					);
				})
			);

			return _react2.default.createElement(
				'div',
				{ id: 'paymentTermTableDiv', className: 'd-flex paymentTermParentDiv' },
				_react2.default.createElement(
					'table',
					{ className: 'table table-bordered table-hover', width: '100%' },
					tableHeaders,
					tableBody
				)
			);
		}
	}]);
	return PaymentTermList;
}(_react2.default.Component);

PaymentTermList.propTypes = {
	columns: _propTypes2.default.array.isRequired,
	data: _propTypes2.default.object.isRequired,
	options: _propTypes2.default.object.isRequired,
	disableEdit: _propTypes2.default.bool.isRequired
};

exports.default = PaymentTermList;