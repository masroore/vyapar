Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.SelectItemsForUnitDialog = undefined;

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _DataLoader = require('../../DBManager/DataLoader.js');

var _DataLoader2 = _interopRequireDefault(_DataLoader);

var _SelectUnitModal = require('./SelectUnitModal');

var _SelectUnitModal2 = _interopRequireDefault(_SelectUnitModal);

var _SelectItemsModal = require('./SelectItemsModal');

var _SelectItemsModal2 = _interopRequireDefault(_SelectItemsModal);

var _DataInserter = require('../../DBManager/DataInserter.js');

var _DataInserter2 = _interopRequireDefault(_DataInserter);

var _ErrorCode = require('../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SelectItemsForUnitDialog = exports.SelectItemsForUnitDialog = function (_React$Component) {
	(0, _inherits3.default)(SelectItemsForUnitDialog, _React$Component);

	function SelectItemsForUnitDialog(props) {
		(0, _classCallCheck3.default)(this, SelectItemsForUnitDialog);

		var _this = (0, _possibleConstructorReturn3.default)(this, (SelectItemsForUnitDialog.__proto__ || (0, _getPrototypeOf2.default)(SelectItemsForUnitDialog)).call(this, props));

		_this.addUnitToItem = _this.addUnitToItem.bind(_this);
		_this.next = _this.next.bind(_this);
		_this.onCloseItemDialog = _this.onCloseItemDialog.bind(_this);
		_this.onCloseUnitDialog = _this.onCloseUnitDialog.bind(_this);
		_this.onItemSelected = _this.onItemSelected.bind(_this);
		_this.onSave = _this.onSave.bind(_this);
		_this.state = {
			itemList: [],
			itemDialogOpen: false,
			unitDialogOpen: false,
			done: false
		};
		_this.selectedItems = [];
		return _this;
	}

	(0, _createClass3.default)(SelectItemsForUnitDialog, [{
		key: 'next',
		value: function next() {
			if (!this.selected.length) {
				ToastHelper.info('Please select item to assign unit.');
				return false;
			}
			$('#addUnitToItemDialog').dialog('close').dialog('destroy');
			this.setState({
				next: true
			});
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			if ($('#addUnitToItemDialog').is(':ui-dialog')) {
				$('#addUnitToItemDialog').dialog('close').dialog('destroy');
			}
		}
	}, {
		key: 'addUnitToItem',
		value: function addUnitToItem() {
			var dataLoader = new _DataLoader2.default();
			var items = [];
			if (this.props.isService) {
				items = dataLoader.loadAllServicesWithoutUnits();
			} else {
				items = dataLoader.loadAllItemsWithoutUnits();
				MyAnalytics.pushEvent('Set Units to Multiple Items Open');
			}
			this.setState({
				itemList: items,
				itemDialogOpen: true
			});
		}
	}, {
		key: 'onCloseItemDialog',
		value: function onCloseItemDialog() {
			this.setState({
				itemDialogOpen: false
			});
		}
	}, {
		key: 'onCloseUnitDialog',
		value: function onCloseUnitDialog() {
			this.selected = [];
			this.setState({
				unitDialogOpen: false
			});
		}
	}, {
		key: 'onItemSelected',
		value: function onItemSelected(selectedItems) {
			this.selectedItems = selectedItems;
			if (!this.selectedItems.length) {
				ToastHelper.info('Please select at least one item.');
				return false;
			}
			this.setState({
				itemDialogOpen: false,
				unitDialogOpen: true
			});
		}
	}, {
		key: 'onSave',
		value: function onSave(_ref) {
			var baseUnit = _ref.baseUnit,
			    selectedMappingId = _ref.selectedMappingId;

			var dataInserter = new _DataInserter2.default();
			var statusCode = dataInserter.addMappingToItems(this.selectedItems, selectedMappingId, baseUnit);
			if (statusCode == _ErrorCode2.default.ERROR_UNIT_MAPPING_UPDATE_SUCCESS) {
				ToastHelper.success(statusCode);
				this.setState({
					unitDialogOpen: false
				});
			} else {
				ToastHelper.error(statusCode);
			}
		}
	}, {
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				'div',
				null,
				_react2.default.createElement(
					_SelectItemsModal2.default,
					{
						isOpen: this.state.itemDialogOpen,
						onSave: this.onItemSelected,
						isService: this.props.isService,
						onClose: this.onCloseItemDialog,
						items: this.state.itemList,
						notSelectFirstRow: true
					},
					'Next'
				),
				this.state.unitDialogOpen && _react2.default.createElement(_SelectUnitModal2.default, {
					isOpen: this.state.unitDialogOpen,
					onClose: this.onCloseUnitDialog,
					onSave: this.onSave
				}),
				_react2.default.createElement(
					'div',
					{
						id: 'addUnitToItem',
						onClick: this.addUnitToItem,
						className: this.props.classNames ? this.props.classNames : ''
					},
					'Assign Units'
				)
			);
		}
	}]);
	return SelectItemsForUnitDialog;
}(_react2.default.Component);

SelectItemsForUnitDialog.propTypes = {
	isService: _propTypes2.default.bool,
	classNames: _propTypes2.default.string
};