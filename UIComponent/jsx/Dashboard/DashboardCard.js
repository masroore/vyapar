Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _trashableReact = require('trashable-react');

var _trashableReact2 = _interopRequireDefault(_trashableReact);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _DashboardDataHelper = require('../../../UIControllers/DashboardDataHelper');

var _DashboardDataHelper2 = _interopRequireDefault(_DashboardDataHelper);

var _GraphHelper = require('../../../Utilities/GraphHelper');

var _GraphHelper2 = _interopRequireDefault(_GraphHelper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles() {
	return {
		dashboardCard: {
			background: '#FFFFFF',
			borderRadius: '8px',
			minWidth: '240px',
			// maxWidth: '726px',
			height: '150px',
			cursor: 'pointer',
			position: 'relative',
			padding: '.75rem .5rem',
			display: 'flex',
			flexDirection: 'column',
			boxShadow: '0px 1px 3px rgba(0, 0, 0, 0.1)',
			'& .graphDiv': {
				height: '102px',
				margin: '0 auto',
				width: '90%'
			},
			'& .typeHeader': {
				position: 'absolute',
				top: '-30px',
				fontFamily: 'robotoMedium'
			},
			'& select': {
				color: '#80808B',
				border: 'none',
				fontSize: '10px',
				fontFamily: 'roboto',
				outline: 'none'
			},
			'& .dashboardCardHeader': {
				display: 'flex',
				marginBottom: '8px',
				'& .title': {
					color: '#1A1A1A',
					fontSize: '12px',
					fontFamily: 'robotoMedium',
					marginRight: 'auto'
				},
				'& .amountOnCard': {
					fontSize: '16px',
					fontFamily: 'robotoMedium'
				}
			},
			'& .dashboardCardBody': {
				flex: 1,
				overflowY: 'hidden',
				'& .clusterize-scroll': {
					maxHeight: '100%'
				},
				'& .listDiv': {
					width: '100%',
					color: '#80808B',
					fontSize: '12px',
					'& td': {
						padding: '7px 0'
					}
				},
				'& .amountOnCard': {
					position: 'absolute',
					fontSize: '20px',
					top: '50%',
					left: '50%',
					transform: 'translate(-50%, -50%)',
					whiteSpace: 'nowrap',
					fontFamily: 'robotoBold'
				}
			},
			'& .saleOrdersCardBody, & .deliveryChallansCardBody, & .purchaseOrdersCardBody, & .openChequesCardBody': {
				'& .listDiv': {
					marginTop: '10px'
				},
				'& tr td': {
					padding: '10px 0'
				}
			}
		}
	};
};

var DashboardCard = function (_React$Component) {
	(0, _inherits3.default)(DashboardCard, _React$Component);

	function DashboardCard(props) {
		var _this2 = this;

		(0, _classCallCheck3.default)(this, DashboardCard);

		var _this = (0, _possibleConstructorReturn3.default)(this, (DashboardCard.__proto__ || (0, _getPrototypeOf2.default)(DashboardCard)).call(this, props));

		_this.loadData = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
			var _this$props$card, card, selectedDateFilter, cardDetails;

			return _regenerator2.default.wrap(function _callee$(_context) {
				while (1) {
					switch (_context.prev = _context.next) {
						case 0:
							_this.setState({ isLoading: true });
							_this$props$card = _this.props.card, card = _this$props$card === undefined ? {} : _this$props$card;
							selectedDateFilter = _this.state.selectedDateFilter;
							_context.prev = 3;
							_context.next = 6;
							return _DashboardDataHelper2.default.getCardDetails(card.key, {
								dateFilter: selectedDateFilter
							});

						case 6:
							cardDetails = _context.sent;

							if (!card.displayCard) {
								if (cardDetails.hasData) {
									_this.props.setDisplayCard(card);
								}
							}
							_this.setState({
								isLoading: false,
								cardDetails: cardDetails
							}, function () {
								var _this$state$cardDetai = _this.state.cardDetails,
								    cardDetails = _this$state$cardDetai === undefined ? {} : _this$state$cardDetai;

								var cardKey = _this.props.card.key;
								var list = cardDetails.list;

								if (_this.listClusterize && _this.listClusterize.destroy) {
									_this.listClusterize.clear();
									_this.listClusterize.destroy();
								}
								var hasListItemsToDisplay = !!(list && list.length);
								var scrollIDForElementId = cardKey + '_scrollArea';
								var contentAreaElementId = cardKey + '_contentArea';
								var scrollElement = $('#' + scrollIDForElementId);
								if (hasListItemsToDisplay && scrollElement && scrollElement.length) {
									_this.listClusterize = new Clusterize({
										rows: list,
										scrollId: scrollIDForElementId,
										contentId: contentAreaElementId,
										rows_in_block: StringConstant.clusterizeRow,
										blocks_in_cluster: StringConstant.clusterizeBlock
									});
								}
							});
							_context.next = 14;
							break;

						case 11:
							_context.prev = 11;
							_context.t0 = _context['catch'](3);

							console.error(card.key, _context.t0);

						case 14:
						case 'end':
							return _context.stop();
					}
				}
			}, _callee, _this2, [[3, 11]]);
		}));

		_this.handleDropdownSelect = function (event) {
			var _event$target = event.target,
			    _event$target$name = _event$target.name,
			    name = _event$target$name === undefined ? '' : _event$target$name,
			    value = _event$target.value;

			var _name$split = name.split('_'),
			    _name$split2 = (0, _slicedToArray3.default)(_name$split, 2),
			    filterType = _name$split2[1];

			if (filterType == 'graphFilter') {
				_this.setState({
					selectedGraphFilter: value
				});
			} else if (filterType == 'dateFilter') {
				_this.setState({
					selectedDateFilter: value
				}, _this.loadData);
			}
		};

		_this.handleClick = function () {
			var _this$state = _this.state,
			    showGraph = _this$state.showGraph,
			    selectedDateFilter = _this$state.selectedDateFilter;

			var options = {
				isGraph: showGraph,
				selectedDateFilter: selectedDateFilter
			};
			_DashboardDataHelper2.default.handleCardClick(_this.props.card.key, options);
		};

		_this.handleGraphIconClick = function (event) {
			var showGraph = _this.state.showGraph;

			_this.setState({ showGraph: !showGraph });
		};

		_this.state = {
			isLoading: true,
			showGraph: false,
			selectedDateFilter: '1',
			selectedGraphFilter: 'daily',
			cardDetails: {}
		};
		_this.listClusterize = null;
		return _this;
	}

	(0, _createClass3.default)(DashboardCard, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.props.registerPromise(this.loadData());
		}
	}, {
		key: 'componentDidUpdate',
		value: function componentDidUpdate(prevProps, prevState) {
			var cardKey = this.props.card.key;
			if (prevProps.card != this.props.card) {
				this.loadData();
			} else if (this.state.showGraph) {
				var options = _GraphHelper2.default.getGraphFormatOption(true);
				var _state = this.state,
				    _state$cardDetails = _state.cardDetails,
				    cardDetails = _state$cardDetails === undefined ? {} : _state$cardDetails,
				    selectedGraphFilter = _state.selectedGraphFilter;
				var graphData = cardDetails.graphData;

				if (graphData) {
					var xAxis = graphData[selectedGraphFilter].map(function (x) {
						return x[0];
					});
					xAxis = xAxis.map(function (label) {
						return label.split('--');
					});
					var yAxis = graphData[selectedGraphFilter].map(function (x) {
						return x[1];
					});
					var data = _GraphHelper2.default.getGraphData(xAxis, yAxis, true);
					var canvasGraph = $('#' + cardKey + '_graph');
					var graphDiv = canvasGraph.parent();
					canvasGraph.remove();
					graphDiv.append('<canvas id="' + cardKey + '_graph"><canvas>');
					canvasGraph = $('#' + cardKey + '_graph');
					if (canvasGraph) {
						var Chart = require('chart.js');
						var chart = new Chart(canvasGraph, {
							type: 'line',
							data: data,
							options: (0, _extends3.default)({}, options, {
								responsive: true,
								maintainAspectRatio: false
							})
						});
					}
				} else {
					$('#' + cardKey + '_graph').hide();
				}
			}
		}
	}, {
		key: 'getFilterDiv',
		value: function getFilterDiv() {
			var _state2 = this.state,
			    selectedDateFilter = _state2.selectedDateFilter,
			    showGraph = _state2.showGraph;
			var _props$card = this.props.card,
			    key = _props$card.key,
			    filterEnabled = _props$card.filterEnabled;

			var filterOptions = _DashboardDataHelper2.default.getFilterOptions(key);
			return filterEnabled && !showGraph ? _react2.default.createElement(
				'select',
				{
					name: key + '_dateFilter',
					onChange: this.handleDropdownSelect,
					value: selectedDateFilter,
					tabIndex: '-1'
				},
				filterOptions.map(function (option) {
					return _react2.default.createElement(
						'option',
						{ key: option.value, value: option.value },
						option.text
					);
				})
			) : null;
		}
	}, {
		key: 'getGraphFilterOptions',
		value: function getGraphFilterOptions() {
			var _props$card2 = this.props.card,
			    key = _props$card2.key,
			    graphEnabled = _props$card2.graphEnabled;
			var _state3 = this.state,
			    showGraph = _state3.showGraph,
			    selectedGraphFilter = _state3.selectedGraphFilter;

			var filterOptions = _DashboardDataHelper2.default.getFilterOptionsForGraph(key);
			if (!graphEnabled) {
				return null;
			}
			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				!!showGraph && _react2.default.createElement(
					'select',
					{
						name: key + '_graphFilter',
						onChange: this.handleDropdownSelect,
						value: selectedGraphFilter,
						tabIndex: '-1'
					},
					filterOptions.map(function (option) {
						return _react2.default.createElement(
							'option',
							{ key: option.value, value: option.value },
							option.text
						);
					})
				),
				_react2.default.createElement('img', {
					src: '../inlineSVG/graphIcon' + (showGraph ? 'Green' : '') + '.svg',
					alt: 'G',
					onClick: this.handleGraphIconClick
				})
			);
		}
	}, {
		key: 'getListDiv',
		value: function getListDiv() {
			var cardDetails = this.state.cardDetails;

			var cardKey = this.props.card.key;
			var listDiv = '';
			if (cardDetails.list && cardDetails.list.length) {
				listDiv = _react2.default.createElement(
					'div',
					{ id: cardKey + '_scrollArea', className: 'clusterize-scroll' },
					_react2.default.createElement(
						'table',
						{ width: '100%', className: 'listDiv' },
						_react2.default.createElement('tbody', { id: cardKey + '_contentArea', className: 'clusterize-content' })
					)
				);
			}
			return listDiv;
		}
	}, {
		key: 'getGraphDiv',
		value: function getGraphDiv() {
			var _props$card3 = this.props.card,
			    key = _props$card3.key,
			    graphEnabled = _props$card3.graphEnabled;

			if (!graphEnabled) return null;
			var showGraph = this.state.showGraph;

			return _react2.default.createElement(
				'div',
				{ className: 'graphDiv ' + (showGraph ? '' : 'hide') },
				_react2.default.createElement('canvas', { id: key + '_graph' })
			);
		}
	}, {
		key: 'render',
		value: function render() {
			var _props = this.props,
			    classes = _props.classes,
			    _props$card4 = _props.card,
			    card = _props$card4 === undefined ? {} : _props$card4;
			var title = card.title;
			var _state4 = this.state,
			    isLoading = _state4.isLoading,
			    _state4$cardDetails = _state4.cardDetails,
			    cardDetails = _state4$cardDetails === undefined ? {} : _state4$cardDetails,
			    showGraph = _state4.showGraph;
			var list = cardDetails.list;

			var hasListItemsToDisplay = !!(list && list.length);
			return card.displayCard ? _react2.default.createElement(
				'div',
				{ className: (0, _classnames2.default)(classes.dashboardCard), style: { gridRowStart: this.props.rowStart } },
				this.props.typeHeader ? _react2.default.createElement(
					'span',
					{ className: 'typeHeader' },
					this.props.typeHeader
				) : null,
				_react2.default.createElement(
					'div',
					{ className: 'dashboardCardHeader' },
					_react2.default.createElement(
						'span',
						{ className: 'title' },
						title
					),
					hasListItemsToDisplay && _react2.default.createElement('span', { className: 'amountOnCard', dangerouslySetInnerHTML: { __html: cardDetails.amount } }),
					this.getFilterDiv(),
					this.getGraphFilterOptions()
				),
				isLoading ? _react2.default.createElement('div', { className: 'inlineLoader' }) : _react2.default.createElement(
					'div',
					{ className: 'dashboardCardBody ' + card.key + 'CardBody', onClick: this.handleClick },
					!showGraph && (hasListItemsToDisplay ? this.getListDiv() : _react2.default.createElement('span', { className: 'amountOnCard', dangerouslySetInnerHTML: { __html: cardDetails.amount } })),
					this.getGraphDiv()
				)
			) : null;
		}
	}]);
	return DashboardCard;
}(_react2.default.Component);

DashboardCard.propTypes = {
	registerPromise: _propTypes2.default.func,
	setDisplayCard: _propTypes2.default.func,
	typeHeader: _propTypes2.default.string,
	card: _propTypes2.default.object,
	classes: _propTypes2.default.object,
	rowStart: _propTypes2.default.number
};

exports.default = (0, _trashableReact2.default)((0, _styles.withStyles)(styles)(DashboardCard));