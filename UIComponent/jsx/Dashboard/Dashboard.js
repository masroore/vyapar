Object.defineProperty(exports, "__esModule", {
	value: true
});

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _create = require('babel-runtime/core-js/object/create');

var _create2 = _interopRequireDefault(_create);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _throttleDebounce = require('throttle-debounce');

var _trashableReact = require('trashable-react');

var _trashableReact2 = _interopRequireDefault(_trashableReact);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _DashboardCard = require('./DashboardCard');

var _DashboardCard2 = _interopRequireDefault(_DashboardCard);

var _DashboardDataHelper = require('../../../UIControllers/DashboardDataHelper');

var _DashboardDataHelper2 = _interopRequireDefault(_DashboardDataHelper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles() {
	return {
		dashboard: {
			height: '100%',
			background: '#F7F8FB',
			overflowY: 'auto',
			display: 'grid',
			gridTemplateColumns: 'repeat(auto-fit, minmax(260px, 1fr))',
			gridAutoColumns: 'minmax(260px, 1fr)',
			gridGap: '50px 15px',
			justifyContent: 'space-between',
			padding: '20px',
			paddingTop: '60px',
			alignContent: 'start'
		},
		dashboardParent: {
			width: '100%',
			height: '100%',
			position: 'relative',
			'& .blurDiv': {
				filter: 'blur(8px)'
			},
			'& .blurForPrivacy': {
				position: 'absolute',
				top: '45%',
				left: '50%',
				transform: 'translate(-50%, -50%)',
				background: 'none',
				textAlign: 'center'
			},

			'& .blurForPrivacy span': {
				marginTop: '1.5rem',
				backgroundColor: '#707070',
				opacity: '25%',
				borderRadius: '4px',
				color: 'white',
				display: 'block',
				padding: '4px'
			},

			'& .blurOverlay': {
				position: 'absolute',
				background: '#727D90',
				top: '0',
				left: '0',
				width: '100%',
				height: '100%',
				opacity: '.3'
			},

			'& .blurContainer': {
				position: 'absolute',
				height: '100%',
				width: '100%',
				top: '0'
			},

			'& .titleBarPrivacy': {
				background: 'transparent',
				position: 'absolute',
				top: '20px',
				right: '10px',
				zIndex: '1'
			},

			'& .switchDiv': {
				display: 'inline',
				padding: '5px'
			},

			'& #privacySwitch': {
				position: 'relative',
				width: '32px',
				height: '12px',
				WebkitAppearance: 'none',
				background: 'rgba(210, 210, 210, 0.26)',
				outline: 'none',
				borderRadius: '16px',
				boxShadow: 'inset 0 0 1px rgba(0, 0, 0, .2)',
				cursor: 'pointer'
			},

			'& #privacySwitch:checked': {
				backgroundColor: 'rgba(1, 111, 185, 0.26)'
			},

			'& #privacySwitch:before': {
				content: "''",
				position: 'absolute',
				width: '12px',
				height: '12px',
				borderRadius: '50%',
				top: '0',
				left: '0',
				backgroundColor: 'white',
				transform: 'scale(1.5)',
				boxShadow: '0 .5px .5px',
				transition: '.5s'
			},

			'& #privacySwitch:checked:before': {
				left: '20px',
				background: '#016FB9'
			}
		}
	};
};
var minCardWidth = 280;

var Dashboard = function (_React$Component) {
	(0, _inherits3.default)(Dashboard, _React$Component);

	function Dashboard() {
		(0, _classCallCheck3.default)(this, Dashboard);

		var _this = (0, _possibleConstructorReturn3.default)(this, (Dashboard.__proto__ || (0, _getPrototypeOf2.default)(Dashboard)).call(this));

		_this.setDisplayCard = function (cardToDisplay) {
			_DashboardDataHelper2.default.addToDisplayedCards(cardToDisplay.key);
			var enabledCardsMap = _this.state.enabledCardsMap;

			var cards = enabledCardsMap[cardToDisplay.type];
			if (cards) {
				var index = cards.findIndex(function (card) {
					return card.key == cardToDisplay.key;
				});
				cardToDisplay.displayCard = true;
				cards[index] = (0, _extends3.default)({}, cardToDisplay);
			}
			_this.setState({
				enabledCardsMap: enabledCardsMap
			});
		};

		_this.onResume = function () {
			_this.props.registerPromise(_DashboardDataHelper2.default.getEnabledDashboardCards().then(function (enabledCardsMap) {
				_this.setState({
					enabledCardsMap: enabledCardsMap
				});
			}));
			_this.props.registerPromise(_DashboardDataHelper2.default.getIsPrivacyEnabled().then(function (isPrivacyEnabled) {
				_this.setState({
					privacyEnabled: isPrivacyEnabled
				});
			}));
		};

		_this.togglePrivacy = function () {
			var Queries = require('../../../Constants/Queries');
			var SettingsModel = require('../../../Models/SettingsModel.js');
			var settingsModel = new SettingsModel();
			settingsModel.setSettingKey(Queries.SETTING_PRIVACY_MODE);
			var privacyEnabled = _this.state.privacyEnabled;

			if (privacyEnabled) {
				settingsModel.UpdateSetting('0', { nonSyncableSetting: true });
			} else {
				settingsModel.UpdateSetting('1', { nonSyncableSetting: true });
			}
			_this.setState({
				privacyEnabled: !privacyEnabled
			});
		};

		var divWidth = window.innerWidth - $('.sideNav').width();
		var cardsPerRow = Math.floor(divWidth / minCardWidth);
		cardsPerRow = Math.min(4, cardsPerRow);
		_this.state = {
			enabledCardsMap: [],
			cardsPerRow: cardsPerRow,
			privacyEnabled: false
		};
		_this.onResize = (0, _throttleDebounce.debounce)(300, _this.onResize.bind(_this));
		return _this;
	}

	(0, _createClass3.default)(Dashboard, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			window.onResume = this.onResume;
			this.onResume();
			window.addEventListener('resize', this.onResize);
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			window.removeEventListener('resize', this.onResize);
			window.onResume = null;
		}
	}, {
		key: 'onResize',
		value: function onResize() {
			var divWidth = window.innerWidth - $('.sideNav').width();
			var cardsPerRow = Math.floor(divWidth / minCardWidth);
			cardsPerRow = Math.min(4, cardsPerRow);
			this.setState({
				cardsPerRow: cardsPerRow
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _this2 = this;

			var classes = this.props.classes;
			var _state = this.state,
			    enabledCardsMap = _state.enabledCardsMap,
			    cardsPerRow = _state.cardsPerRow,
			    privacyEnabled = _state.privacyEnabled;

			var saleCards = enabledCardsMap.sale || [];
			var purchaseCards = enabledCardsMap.purchase || [];
			var inventoryCards = enabledCardsMap.stockInventory || [];
			var cashAndBankCards = enabledCardsMap.cashAndBank || [];
			var typeHeaders = {
				saleInvoices: 'Sale',
				purchaseBills: 'Purchase',
				stockValue: 'Stock Inventory',
				cashInHand: 'Cash & Bank'
			};
			var rowStartMap = (0, _create2.default)(null);
			var rowStart = 1;
			var cardsInCurrentRow = 0;
			saleCards.filter(function (card) {
				return card.displayCard;
			}).forEach(function (card) {
				rowStartMap[card.key] = rowStart;
				cardsInCurrentRow++;
				if (cardsInCurrentRow == cardsPerRow) {
					cardsInCurrentRow = 0;
					rowStart++;
				}
			});
			if (cardsPerRow - cardsInCurrentRow < purchaseCards.filter(function (card) {
				return card.displayCard;
			}).length) {
				rowStart++;
				cardsInCurrentRow = 0;
			}
			purchaseCards.filter(function (card) {
				return card.displayCard;
			}).forEach(function (card) {
				rowStartMap[card.key] = rowStart;
				cardsInCurrentRow++;
				if (cardsInCurrentRow == cardsPerRow) {
					cardsInCurrentRow = 0;
					rowStart++;
				}
			});
			if (cardsPerRow - cardsInCurrentRow < inventoryCards.filter(function (card) {
				return card.displayCard;
			}).length) {
				rowStart++;
				cardsInCurrentRow = 0;
			}
			inventoryCards.filter(function (card) {
				return card.displayCard;
			}).forEach(function (card) {
				rowStartMap[card.key] = rowStart;
				cardsInCurrentRow++;
				if (cardsInCurrentRow == cardsPerRow) {
					cardsInCurrentRow = 0;
					rowStart++;
				}
			});
			if (cardsPerRow - cardsInCurrentRow < cashAndBankCards.filter(function (card) {
				return card.displayCard;
			}).length) {
				rowStart++;
				cardsInCurrentRow = 0;
			}
			cashAndBankCards.filter(function (card) {
				return card.displayCard;
			}).forEach(function (card) {
				rowStartMap[card.key] = rowStart;
				cardsInCurrentRow++;
				if (cardsInCurrentRow == cardsPerRow) {
					cardsInCurrentRow = 0;
					rowStart++;
				}
			});
			return _react2.default.createElement(
				'div',
				{ className: classes.dashboardParent },
				_react2.default.createElement(
					'div',
					{ className: 'titleBarPrivacy' },
					_react2.default.createElement(
						'div',
						{ className: 'privacyDiv' },
						_react2.default.createElement(
							'span',
							null,
							'Privacy'
						),
						_react2.default.createElement(
							'div',
							{ className: 'switchDiv' },
							_react2.default.createElement('input', {
								type: 'checkbox',
								checked: privacyEnabled,
								id: 'privacySwitch',
								className: 'privacySwitch',
								onChange: this.togglePrivacy,
								tabIndex: '-1'
							})
						)
					)
				),
				_react2.default.createElement(
					'div',
					{ className: (0, _classnames2.default)(classes.dashboard, '' + (privacyEnabled ? 'blurDiv' : '')) },
					[].concat((0, _toConsumableArray3.default)(saleCards), (0, _toConsumableArray3.default)(purchaseCards), (0, _toConsumableArray3.default)(inventoryCards), (0, _toConsumableArray3.default)(cashAndBankCards)).map(function (card) {
						return _react2.default.createElement(_DashboardCard2.default, {
							key: card.title,
							card: card,
							rowStart: rowStartMap[card.key],
							setDisplayCard: _this2.setDisplayCard,
							typeHeader: typeHeaders[card.key]
						});
					})
				),
				_react2.default.createElement(
					'div',
					{ className: 'blurContainer ' + (privacyEnabled ? '' : 'hide') },
					_react2.default.createElement('div', { className: 'blurOverlay' }),
					_react2.default.createElement(
						'div',
						{ className: 'blurForPrivacy' },
						_react2.default.createElement('img', { src: '../inlineSVG/privacy.svg', alt: 'in privacy mode' }),
						_react2.default.createElement(
							'span',
							{ className: 'privacyText' },
							'Privacy Mode enabled. ',
							_react2.default.createElement('br', null),
							'Please toggle the Privacy button to view your Dashboard.'
						)
					)
				)
			);
		}
	}]);
	return Dashboard;
}(_react2.default.Component);

Dashboard.propTypes = {
	registerPromise: _propTypes2.default.func,
	classes: _propTypes2.default.object
};

exports.default = (0, _trashableReact2.default)((0, _styles.withStyles)(styles)(Dashboard));