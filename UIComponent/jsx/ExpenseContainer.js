Object.defineProperty(exports, "__esModule", {
	value: true
});

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _SearchBox = require('./SearchBox');

var _SearchBox2 = _interopRequireDefault(_SearchBox);

var _ExpenseCategoryDetail = require('./ExpenseCategoryDetail');

var _ExpenseCategoryDetail2 = _interopRequireDefault(_ExpenseCategoryDetail);

var _AddExpenseCategoryButton = require('./AddExpenseCategoryButton');

var _AddExpenseCategoryButton2 = _interopRequireDefault(_AddExpenseCategoryButton);

var _ExpenseList = require('./ExpenseList');

var _ExpenseList2 = _interopRequireDefault(_ExpenseList);

var _ExpenseCategoryList = require('./ExpenseCategoryList');

var _ExpenseCategoryList2 = _interopRequireDefault(_ExpenseCategoryList);

var _FirstScreen = require('./UIControls/FirstScreen');

var _FirstScreen2 = _interopRequireDefault(_FirstScreen);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _TransactionHelper = require('../../Utilities/TransactionHelper');

var _TxnTypeConstant2 = require('../../Constants/TxnTypeConstant');

var _TxnTypeConstant3 = _interopRequireDefault(_TxnTypeConstant2);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ExpenseContainer = function (_React$Component) {
	(0, _inherits3.default)(ExpenseContainer, _React$Component);

	function ExpenseContainer(props) {
		(0, _classCallCheck3.default)(this, ExpenseContainer);

		var _this = (0, _possibleConstructorReturn3.default)(this, (ExpenseContainer.__proto__ || (0, _getPrototypeOf2.default)(ExpenseContainer)).call(this, props));

		_this.filterItems = _this.filterItems.bind(_this);
		_this.onItemSelected = _this.onItemSelected.bind(_this);
		var expenseCategories = _this.getExpenseCategories();
		var currentItem = null;
		_this.allExpenseCategories = expenseCategories;
		_this.state = {
			records: [],
			expenseCategories: expenseCategories,
			currentItem: currentItem,
			contextMenu: _this.contextMenu,
			showSearch: false
		};
		window.onResume = _this.onResume.bind(_this);
		_this._id = null;
		return _this;
	}

	(0, _createClass3.default)(ExpenseContainer, [{
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			delete window.onResume;
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			window.onResume = this.onResume.bind(this);
		}
	}, {
		key: 'onResume',
		value: function onResume() {
			var _this2 = this;

			this.props.onResume && this.props.onResume();
			var expenseCategories = this.getExpenseCategories();
			this.allExpenseCategories = expenseCategories;
			if (this.state.currentItem) {
				var currentItem = this.allExpenseCategories.find(function (category) {
					return _this2.state.currentItem.categoryId === category.categoryId;
				});
				this.setState({ currentItem: currentItem });
			}
			this.filterItems(this.state.searchText);
			this.onItemSelected(null, true, this.state.currentItem);
		}
	}, {
		key: 'getExpenseCategories',
		value: function getExpenseCategories() {
			var DataLoader = require('../../DBManager/DataLoader');
			var dataLoader = new DataLoader();
			var list = dataLoader.loadExpenseList();
			var expenseCategories = list.map(function (category) {
				return {
					categoryId: category.getCategoryId(),
					categoryName: category.getCategoryName(),
					categoryAmount: category.getAmount(),
					isCategoryLoanType: typeof category.getIsCategoryLoanType == 'function' && category.getIsCategoryLoanType()
				};
			});
			return expenseCategories;
		}
	}, {
		key: 'getExpenseTransactions',
		value: function getExpenseTransactions(category) {
			var expenses = [];
			if (category && category.categoryId) {
				var _TxnTypeConstant = require('../../Constants/TxnTypeConstant');
				var DataLoader = require('../../DBManager/DataLoader');
				var dataLoader = new DataLoader();
				if (category.isCategoryLoanType) {
					var categoryId = category.categoryId;
					var loanAccountId = categoryId == 'ProcessingFeeCategory' ? '' : categoryId;
					var loanTxnType = categoryId == 'ProcessingFeeCategory' ? _TxnTypeConstant.TXN_TYPE_LOAN_PROCESSING_FEE : _TxnTypeConstant.TXN_TYPE_LOAN_EMI_PAYMENT;
					var loanTxnTypes = [loanTxnType];
					var loanTxns = dataLoader.getLoanTransactions({
						loanAccountId: loanAccountId,
						loanTxnTypes: loanTxnTypes
					});
					loanTxns.forEach(function (loanTxn) {
						var amount = categoryId == 'ProcessingFeeCategory' ? Number(loanTxn.getPrincipalAmount()) : Number(loanTxn.getInterestAmount());
						if (amount) {
							var txnId = loanTxn.getLoanTxnId();
							var typeTxn = loanTxn.getLoanType();
							var date = loanTxn.getTransactionDate();
							expenses.push({
								txnId: txnId,
								typeTxn: typeTxn,
								amount: amount,
								date: date
							});
						}
					});
				} else {
					var expenseTxns = dataLoader.LoadAllTransactions(category.categoryId, true);
					expenses = expenseTxns.map(function (txn) {
						var txnId = txn.getTxnId();
						var typeTxn = _TxnTypeConstant.getTxnType(txn.getTxnType());
						var date = txn.getTxnDate();
						var amount = Number(txn.getCashAmount()) || 0;
						return {
							txnId: txnId,
							typeTxn: typeTxn,
							amount: amount,
							date: date
						};
					});
				}
			}
			return expenses;
		}
	}, {
		key: 'onItemSelected',
		value: function onItemSelected(row) {
			var fromSync = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
			var currentItem = arguments[2];

			if (!fromSync) {
				if (!row.node.selected) {
					return false;
				}
				this.setState({ currentItem: row.data });
			}
			if (row && row.data) {
				currentItem = row.data;
			}
			if (currentItem) {
				var expenseTransactions = this.getExpenseTransactions(currentItem);
				this.setState({
					records: expenseTransactions
				});
			}
		}
	}, {
		key: 'filterItems',
		value: function filterItems(searchText) {
			if (!searchText) {
				this.setState({
					expenseCategories: this.allExpenseCategories,
					searchText: ''
				});
				return false;
			}
			var query = searchText.toLowerCase();
			var filteredItems = this.allExpenseCategories.filter(function (category) {
				return category.categoryName.toLowerCase().includes(query);
			});
			var records = this.state.records;
			var currentItem = this.state.currentItem;
			if (!filteredItems.length) {
				records = [];
				currentItem = null;
			}
			this.setState({
				records: records,
				currentItem: currentItem,
				expenseCategories: filteredItems,
				searchText: searchText
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _this3 = this;

			var _state = this.state,
			    expenseCategories = _state.expenseCategories,
			    records = _state.records,
			    currentItem = _state.currentItem;

			return _react2.default.createElement(
				'div',
				{ className: 'd-flex flex-column', style: { height: '100%' } },
				_react2.default.createElement(
					'ul',
					{ className: 'tab-container d-flex' },
					_react2.default.createElement(
						'li',
						{ className: 'tab-items col' },
						'EXPENSES'
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'd-flex align-items-stretch flex-container' },
					this.allExpenseCategories.length === 0 && _react2.default.createElement(_FirstScreen2.default, {
						image: '../inlineSVG/first-screens/expense.svg',
						text: 'Record your business expenses & know your real profits.',
						onClick: function onClick() {
							(0, _TransactionHelper.viewTransaction)('', _TxnTypeConstant3.default.getTxnType(_TxnTypeConstant3.default.TXN_TYPE_EXPENSE));
						},
						buttonLabel: 'Add Your First Expense'
					}),
					this.allExpenseCategories.length > 0 && _react2.default.createElement(
						_react.Fragment,
						null,
						_react2.default.createElement(
							'div',
							{ className: 'flex-column d-flex left-column' },
							_react2.default.createElement(
								'div',
								{ className: 'listheaderDiv d-flex mt-20 mb-10' },
								_react2.default.createElement(
									'div',
									{
										className: (0, _classnames2.default)('searchDiv', this.state.showSearch ? 'width100 d-flex' : '')
									},
									_react2.default.createElement(_SearchBox2.default, {
										throttle: 500,
										filter: this.filterItems,
										collapsed: !this.state.showSearch,
										collapsedTextBoxClass: 'width100',
										onCollapsedClick: function onCollapsedClick() {
											_this3.setState({ showSearch: true });
										},
										onInputBlur: function onInputBlur() {
											_this3.setState({ showSearch: false });
										}
									})
								),
								_react2.default.createElement(
									'div',
									{ className: 'buttonDiv flex-1 d-flex justify-content-end' },
									_react2.default.createElement(_AddExpenseCategoryButton2.default, {
										collapsed: this.state.showSearch,
										addPadding: true
									})
								)
							),
							_react2.default.createElement(_ExpenseCategoryList2.default, {
								onItemSelected: this.onItemSelected,
								items: [].concat((0, _toConsumableArray3.default)(expenseCategories))
							})
						),
						_react2.default.createElement(
							'div',
							{ className: 'd-flex flex-column flex-grow-1 right-column' },
							_react2.default.createElement(_ExpenseCategoryDetail2.default, { currentItem: currentItem }),
							_react2.default.createElement(_ExpenseList2.default, { items: records, addExtraColumns: true })
						)
					)
				)
			);
		}
	}]);
	return ExpenseContainer;
}(_react2.default.Component);

ExpenseContainer.propTypes = {
	onResume: _propTypes2.default.func
};

exports.default = ExpenseContainer;