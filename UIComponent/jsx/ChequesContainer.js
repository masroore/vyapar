Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _trashableReact = require('trashable-react');

var _trashableReact2 = _interopRequireDefault(_trashableReact);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _ToastHelper = require('../../UIControllers/ToastHelper');

var _ToastHelper2 = _interopRequireDefault(_ToastHelper);

var _MyDate = require('../../Utilities/MyDate');

var _MyDate2 = _interopRequireDefault(_MyDate);

var _TransactionHelper = require('../../Utilities/TransactionHelper');

var TransactionHelper = _interopRequireWildcard(_TransactionHelper);

var _DateFilter = require('./grid-filters/DateFilter');

var _DateFilter2 = _interopRequireDefault(_DateFilter);

var _NumberFilter = require('./grid-filters/NumberFilter');

var _NumberFilter2 = _interopRequireDefault(_NumberFilter);

var _StringFilter = require('./grid-filters/StringFilter');

var _StringFilter2 = _interopRequireDefault(_StringFilter);

var _MultiSelectionFilter = require('./grid-filters/MultiSelectionFilter');

var _MultiSelectionFilter2 = _interopRequireDefault(_MultiSelectionFilter);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _ChequeStatus = require('../../Constants/ChequeStatus');

var _ChequeStatus2 = _interopRequireDefault(_ChequeStatus);

var _FirstScreen = require('./UIControls/FirstScreen');

var _FirstScreen2 = _interopRequireDefault(_FirstScreen);

var _TxnTypeConstant = require('../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ChequesContainer = function (_React$Component) {
	(0, _inherits3.default)(ChequesContainer, _React$Component);

	function ChequesContainer(props) {
		(0, _classCallCheck3.default)(this, ChequesContainer);

		var _this = (0, _possibleConstructorReturn3.default)(this, (ChequesContainer.__proto__ || (0, _getPrototypeOf2.default)(ChequesContainer)).call(this, props));

		_this.takeAction = function (params) {
			var _params$data = params.data,
			    action = _params$data.action,
			    chequeId = _params$data.chequeId,
			    txnType = _params$data.txnType;

			switch (action) {
				case 'Re-open':
					_this.reOpenCheque(chequeId);
					break;
				case 'DEPOSIT':
				case 'WITHDRAW':
					_this.depositOrWithdrawCheque(chequeId, txnType);
					break;
				default:
					break;
			}
		};

		_this.getApi = function (params) {
			_this.api = params.api;
		};

		_this.state = {
			records: _this.getCheques(),
			columnDefs: [{
				field: 'color',
				headerName: '',
				width: 25,
				suppressMenu: false,
				getQuickFilterText: function getQuickFilterText() {
					return '';
				},
				suppressFilter: true,
				suppressSorting: true,
				cellRenderer: function cellRenderer(params) {
					return '<div style=\'background-color: ' + params.value + '; border-radius:50%;width:8px;height:8px; top: 19px; position:relative;\'></div>';
				}
			}, {
				field: 'txnType',
				filter: _MultiSelectionFilter2.default,
				filterOptions: _TxnTypeConstant2.default.getFilterOptionsForCheques(),
				getQuickFilterText: function getQuickFilterText(params) {
					return _TxnTypeConstant2.default.getTxnTypeForUI(params.data.txnType);
				},
				cellRenderer: function cellRenderer(params) {
					return _TxnTypeConstant2.default.getTxnTypeForUI(params.value);
				},
				headerName: 'TYPE',
				width: 110
			}, {
				field: 'name',
				filter: _StringFilter2.default,
				headerName: 'NAME',
				width: 180
			}, {
				field: 'txnRefNo',
				filter: _StringFilter2.default,
				headerName: 'Ref No.',
				width: 180
			}, {
				field: 'date',
				headerName: 'DATE',
				width: 100,
				sort: 'desc',
				filter: _DateFilter2.default,
				sortingOrder: ['desc', 'asc', null],
				getQuickFilterText: function getQuickFilterText(params) {
					return _MyDate2.default.getDate('d/m/y', params.value);
				},
				cellRenderer: function cellRenderer(params) {
					return _MyDate2.default.getDate('d/m/y', params.value);
				}
			}, {
				field: 'amount',
				headerName: 'AMOUNT',
				width: 150,
				filter: _NumberFilter2.default,
				cellClass: 'alignRight',
				getQuickFilterText: function getQuickFilterText(params) {
					return params.value < 0 ? '-' : params.value;
				},
				cellRenderer: function cellRenderer(params) {
					return _MyDouble2.default.getBalanceAmountWithDecimalAndCurrencyWithoutColor(params.value);
				}
			}, {
				field: 'status',
				headerName: 'Status',
				width: 120,
				filter: _MultiSelectionFilter2.default,
				filterOptions: _ChequeStatus2.default.getFilterOptionsForCheques(),
				filterValueGetter: function filterValueGetter(params) {
					return params.data.status == 'Open' ? _ChequeStatus2.default.OPEN : _ChequeStatus2.default.CLOSE;
				},
				getQuickFilterText: function getQuickFilterText(params) {
					return params.value;
				}
			}, {
				field: 'action',
				onCellClicked: _this.takeAction,
				headerName: 'ACTION',
				width: 180,
				suppressFilter: true,
				suppressSorting: true,
				getQuickFilterText: function getQuickFilterText(params) {
					return params.value;
				},
				cellRenderer: function cellRenderer(params) {
					return '<div style="cursor:pointer;color:#0b8fc5;">' + params.value + '</div>';
				}
			}]
		};
		return _this;
	}

	(0, _createClass3.default)(ChequesContainer, [{
		key: 'getCheques',
		value: function getCheques() {
			var Dataloader = require('../../DBManager/DataLoader');
			var TxnTypeConstant = require('../../Constants/TxnTypeConstant');
			var ColorConstants = require('../../Constants/ColorConstants');
			var NameCache = require('../../Cache/NameCache');
			var nameCache = new NameCache();
			var dataloader = new Dataloader();
			var cheques = dataloader.getListOfCheques();
			var getChequeStatus = function getChequeStatus(cheque, txnType) {
				var PaymentInfoCache = require('../../Cache/PaymentInfoCache.js');
				var status = '';
				if (cheque.getChequeCurrentStatus() == _ChequeStatus2.default.OPEN) {
					status = 'Open';
				} else {
					if (cheque.getTransferredToAccount() == 1) {
						switch (txnType) {
							case TxnTypeConstant.TXN_TYPE_CASHOUT:
							case TxnTypeConstant.TXN_TYPE_EXPENSE:
							case TxnTypeConstant.TXN_TYPE_PURCHASE:
							case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
								status = 'Removed from Cash-in-hand';
								break;
							default:
								status = 'Added in Cash-in-hand';
						}
					} else {
						var paymentInfoCache = new PaymentInfoCache();
						var bankName = paymentInfoCache.getPaymentBankName(cheque.getTransferredToAccount());
						bankName = bankName || 'a deleted bank account';
						switch (txnType) {
							case TxnTypeConstant.TXN_TYPE_CASHOUT:
							case TxnTypeConstant.TXN_TYPE_EXPENSE:
							case TxnTypeConstant.TXN_TYPE_PURCHASE:
							case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
							case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
								status = 'Withdrawn from ' + bankName;
								break;
							default:
								status = 'Deposited to ' + bankName;
						}
					}
				}
				return status;
			};
			var records = cheques.map(function (cheque) {
				var txnId = Number(cheque.getChequeTxnId());
				var chequeId = cheque.getChequeId();
				var txnRefNo = cheque.getChequeTxnRefNo();
				var closedTxnRefId = cheque.getChequeClosedTxnRefId();
				var type = void 0,
				    name = void 0,
				    date = void 0,
				    amount = void 0,
				    status = void 0,
				    txnType = void 0,
				    typeId = void 0,
				    action = void 0;
				if (closedTxnRefId) {
					// closed transaction
					typeId = cheque.getChequeTxnType();
					type = TxnTypeConstant.getTxnTypeForUI(typeId);
					name = nameCache.findNameByNameId(cheque.getChequeNameId());
					date = cheque.getTxnDate();
					txnType = Number(cheque.getChequeTxnType());
					status = getChequeStatus(cheque, txnType);
					amount = Number(cheque.getChequeAmount());
				} else {
					var txnDetails = dataloader.LoadTransactionFromId(txnId);
					typeId = txnDetails['txnType'];
					var nameRef = txnDetails.getNameRef();
					type = TxnTypeConstant.getTxnTypeForUI(typeId);
					name = nameRef && nameRef.getFullName() ? nameRef.getFullName() : '';
					date = txnDetails['txnDate'];
					txnType = Number(txnDetails.getTxnType());
					status = getChequeStatus(cheque, txnDetails.getTxnType());
					amount = Number(txnDetails['cashAmount']);
				}
				switch (txnType) {
					case TxnTypeConstant.TXN_TYPE_CASHOUT:
					case TxnTypeConstant.TXN_TYPE_EXPENSE:
					case TxnTypeConstant.TXN_TYPE_PURCHASE:
					case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
					case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
						action = 'WITHDRAW';
						break;
					default:
						action = 'DEPOSIT';
				}
				action = cheque.getChequeCurrentStatus() ? 'Re-open' : action;
				var color = ColorConstants[typeId];
				return {
					txnId: txnId,
					chequeId: chequeId,
					color: color,
					type: type,
					name: name,
					txnRefNo: txnRefNo,
					date: date,
					amount: amount,
					status: status,
					action: action,
					txnType: txnType
				};
			});
			if (this.api) {
				this.api.setRowData(records);
			}
			return records;
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			var MyAnalytics = require('../../Utilities/analyticsHelper');
			MyAnalytics.pushScreen('View Cheque List');
			window.onResume = this.onResume.bind(this);
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			delete window.onResume;
		}
	}, {
		key: 'onResume',
		value: function onResume() {
			var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

			var records = this.getCheques();
			this.setState({ records: records });
		}
	}, {
		key: 'viewTransaction',
		value: function viewTransaction(row) {
			if (!row.txnId) {
				row = row.data;
			}
			if (!row.txnId) {
				_ToastHelper2.default.info('This transaction is closed and cannot be edited');
				return;
			}
			var txnTypeConstant = _TxnTypeConstant2.default.getTxnType(row.txnType);
			TransactionHelper.viewTransaction(row.txnId, txnTypeConstant);
		}
	}, {
		key: 'reOpenCheque',
		value: function reOpenCheque(chequeId) {
			var conf = confirm('Do you really want to re-open this cheque');
			if (conf) {
				var FilterCheque = require('../../BizLogic/ChequeLogic');
				var cheque = new FilterCheque();
				cheque.setChequeId(chequeId);
				var statusCode = cheque.reOpenCheque();
				if (statusCode == ErrorCode.ERROR_CHEQUE_STATUS_UPDATE_SUCCESS) {
					this.setState({
						records: this.getCheques()
					});
					_ToastHelper2.default.success(statusCode);
				} else {
					_ToastHelper2.default.error(statusCode);
				}
			}
		}
	}, {
		key: 'depositOrWithdrawCheque',
		value: function depositOrWithdrawCheque(chequeId, txnType) {
			if (txnType) {
				if (txnType == _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER) {
					_ToastHelper2.default.info('Sale order cheques can\'t be deposited before the order completion!!');
					return;
				} else if (txnType == _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN) {
					_ToastHelper2.default.info('Delivery Challan form cheques can\'t be deposited before the order completion!!');
					return;
				} else if (txnType == _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE) {
					_ToastHelper2.default.info('Estimate form cheques can\'t be deposited before the order completion!!');
					return;
				} else if (txnType == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER) {
					_ToastHelper2.default.info('Purchase order cheques can\'t be withdrawn before the order completion!!');
					return;
				}
			}
			window.TransferChequeIdGlobal = chequeId;
			var CommonUtility = require('../../Utilities/CommonUtility');
			CommonUtility.loadFrameDiv('TransferCheque.html');
		}
	}, {
		key: 'onTransactionSelected',
		value: function onTransactionSelected(row) {
			this.currentTransaction = row;
		}
	}, {
		key: 'initializeFilter',
		value: function initializeFilter(params) {
			var selectedStatusFilter = this.props.selectedStatusFilter;

			if (selectedStatusFilter !== undefined) {
				var filterOptions = _ChequeStatus2.default.getFilterOptionsForCheques(selectedStatusFilter);
				var statusInstance = params.api.getFilterInstance('status');
				if (statusInstance) {
					statusInstance.setModel({
						value: {
							filterOptions: filterOptions,
							checkedFilterOptions: filterOptions.filter(function (option) {
								return option.selected;
							})
						}
					});
					statusInstance.applyFilter();
				}
			}
			var typeIdInstance = params.api.getFilterInstance('typeId');
			if (typeIdInstance) {
				typeIdInstance.componentInstance.applyFilter();
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var _state = this.state,
			    records = _state.records,
			    columnDefs = _state.columnDefs;

			var gridOptions = {
				enableFilter: true,
				enableSorting: true,
				rowSelection: 'single',
				columnDefs: columnDefs,
				rowData: records,
				rowHeight: 40,
				headerHeight: 40,
				rowClass: 'customVerticalBorder',
				onRowSelected: this.onTransactionSelected.bind(this),
				onRowDoubleClicked: this.viewTransaction.bind(this),
				onGridReadyCallback: this.initializeFilter.bind(this)
			};
			return _react2.default.createElement(
				'div',
				{ className: 'd-flex flex-column', style: { height: '100%' } },
				_react2.default.createElement(
					'ul',
					{ className: 'tab-container d-flex' },
					_react2.default.createElement(
						'li',
						{ className: 'tab-items col' },
						'CHEQUE DETAILS'
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'd-flex flex-column flex-container flex-grow-1' },
					records.length === 0 && _react2.default.createElement(_FirstScreen2.default, {
						image: '../inlineSVG/first-screens/cheque.svg',
						text: 'Payment received through cheque for your invoices will be shown here.'
					}),
					records.length > 0 && _react2.default.createElement(_Grid2.default, {
						title: 'Transactions',
						quickFilter: true,
						classes: 'alternateRowColor customVerticalBorder gridRowHeight40',
						getApi: this.getApi,
						gridOptions: gridOptions
					})
				)
			);
		}
	}]);
	return ChequesContainer;
}(_react2.default.Component);

ChequesContainer.propTypes = {
	registerPromise: _propTypes2.default.func,
	selectedStatusFilter: _propTypes2.default.string
};
exports.default = (0, _trashableReact2.default)(ChequesContainer);