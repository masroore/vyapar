Object.defineProperty(exports, "__esModule", {
	value: true
});

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _typeof2 = require('babel-runtime/helpers/typeof');

var _typeof3 = _interopRequireDefault(_typeof2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _trashableReact = require('trashable-react');

var _trashableReact2 = _interopRequireDefault(_trashableReact);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _DataLoader = require('../../DBManager/DataLoader');

var _DataLoader2 = _interopRequireDefault(_DataLoader);

var _IPCActions = require('../../Constants/IPCActions');

var _MyDate = require('../../Utilities/MyDate');

var _MyDate2 = _interopRequireDefault(_MyDate);

var _TransactionHelper = require('../../Utilities/TransactionHelper');

var TransactionHelper = _interopRequireWildcard(_TransactionHelper);

var _DBWindow = require('../../Utilities/DBWindow');

var _DBWindow2 = _interopRequireDefault(_DBWindow);

var _TxnTypeConstant = require('../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _StringConstants = require('../../Constants/StringConstants');

var _StringConstants2 = _interopRequireDefault(_StringConstants);

var _TransactionStatus = require('../../Constants/TransactionStatus');

var _TransactionStatus2 = _interopRequireDefault(_TransactionStatus);

var _DateFilter = require('./grid-filters/DateFilter');

var _DateFilter2 = _interopRequireDefault(_DateFilter);

var _NumberFilter = require('./grid-filters/NumberFilter');

var _NumberFilter2 = _interopRequireDefault(_NumberFilter);

var _StringFilter = require('./grid-filters/StringFilter');

var _StringFilter2 = _interopRequireDefault(_StringFilter);

var _MultiSelectionFilter = require('./grid-filters/MultiSelectionFilter');

var _MultiSelectionFilter2 = _interopRequireDefault(_MultiSelectionFilter);

var _NewTransactionButton = require('./NewTransactionButton');

var _NewTransactionButton2 = _interopRequireDefault(_NewTransactionButton);

var _FirstScreen = require('./UIControls/FirstScreen');

var _FirstScreen2 = _interopRequireDefault(_FirstScreen);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DueDateRenderer = function DueDateRenderer(params) {
	var _params$data = params.data,
	    txnDueDays = _params$data.txnDueDays,
	    dueDate = _params$data.dueDate,
	    txnStatus = _params$data.txnStatus;

	var dueDateString = _MyDate2.default.getDate('d/m/y', dueDate);
	var smallText = "color: '#2B4C56'; opacity: 0.54; margin-left: 2px; position: 'relative'; top: -1px; background: #E6E6EA; font-size: 10px; height: 14px; border-radius: 2px; padding: 2px 5px";

	if (txnStatus == _TransactionStatus2.default.TXN_ORDER_OPEN) {
		if (txnDueDays < 0) {
			txnDueDays = '<div>\n\t\t\t\t\t' + dueDateString + '\n\t\t\t\t\t<span style="' + smallText + '">\n\t\t\t\t\t\tDue: ' + Math.abs(txnDueDays) + ' days\n\t\t\t\t\t</span>\n\t\t\t\t</div>\n\t\t\t';
		} else if (txnDueDays > 0) {
			txnDueDays = '\n\t\t\t\t<div>\n\t\t\t\t\t<span style="color: #DD722A">' + dueDateString + '</span>\n\t\t\t\t\t<span style="' + smallText + '">Overdue: ' + txnDueDays + ' days</span>\n\t\t\t\t</div>\n\t\t\t';
		} else {
			txnDueDays = '\n\t\t\t\t<div>\n\t\t\t\t\t' + dueDateString + '\n\t\t\t\t\t<span style=' + smallText + '>Due: Today</span>\n\t\t\t\t</div>\n\t\t\t';
		}
	} else {
		txnDueDays = '<div>' + dueDateString + '</div>';
	}
	return txnDueDays;
};

function ActionRenderer() {};
ActionRenderer.prototype = {
	init: function init(params) {
		var _params$data2 = params.data,
		    txnStatus = _params$data2.txnStatus,
		    linkedTxn = _params$data2.linkedTxn;

		var _ref = linkedTxn || {},
		    txnId = _ref.txnId,
		    prefixValue = _ref.prefixValue,
		    txnRefNumber = _ref.txnRefNumber;

		var actionTransaction = ' height: 24px; box-shadow: 1px 1px 2px 0 #bdbdbd; padding: 5px 10px; text-align: left; color: #8186D3 !important; font-size: 12px !important; cursor: pointer;';
		var convertedText = ' color: #8186D3; font-size: 12px; text-decoration: underline; cursor: pointer;';
		var convertedDeletedText = ' color: #8186D3; font-size: 12px; text-decoration: none; cursor: default;';

		this.eGui = document.createElement('div');
		var html = '\t\n\t\t\t<button id="renderer-convert-to-sale" style="' + actionTransaction + '">\n\t\t\t\tCONVERT TO SALE\n\t\t\t\t</button>\n\t\t';

		if (txnStatus != _TransactionStatus2.default.TXN_ORDER_OPEN) {
			if (linkedTxn) {
				if (txnRefNumber) {
					html = '\n\t\t\t\t\t\t<div id="renderer-view-transaction" style="' + convertedText + '">\n\t\t\t\t\t\t\tConverted To Invoice No.' + (prefixValue + txnRefNumber) + '\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t';
				} else {
					html = '\n\t\t\t\t\t\t<div id="renderer-view-transaction" style="' + convertedText + '">\n\t\t\t\t\t\t\tConverted To Invoice\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t';
				}
			} else {
				html = '\n\t\t\t\t\t<div style="' + convertedDeletedText + '">\n\t\t\t\t\t\tConverted To deleted Sale\n\t\t\t\t\t\t</div>\n\t\t\t\t';
			}
		}
		var convertToSale = function convertToSale(e) {
			e.stopPropagation();
			var _params$data3 = params.data,
			    txnId = _params$data3.txnId,
			    txnTypeConstant = _params$data3.txnTypeConstant;

			TransactionHelper.returnTransaction(txnId, txnTypeConstant);
		};

		var viewTransaction = function viewTransaction(e) {
			e.stopPropagation();
			TransactionHelper.viewTransaction(txnId, 'TXN_TYPE_SALE');
		};
		this.eGui.innerHTML = html;
		var idConvertToSale = this.eGui.querySelector('#renderer-convert-to-sale');
		var idViewTransaction = this.eGui.querySelector('#renderer-view-transaction');
		if (idViewTransaction) {
			idViewTransaction.addEventListener('click', viewTransaction);
		}
		if (idConvertToSale) {
			idConvertToSale.addEventListener('click', convertToSale);
		}
	},
	getGui: function getGui() {
		return this.eGui;
	}
};

var StatusRenderer = function StatusRenderer(params) {
	var _params$data4 = params.data,
	    txnStatus = _params$data4.txnStatus,
	    txnStatusString = _params$data4.txnStatusString,
	    linkedTxn = _params$data4.linkedTxn;

	var _ref2 = linkedTxn || {},
	    txnDate = _ref2.txnDate;

	var actionTransaction = 'text-align: left; color: #8186D3 !important; font-size: 12px !important; cursor: pointer;';
	var smallText = 'color: #2B4C56; opacity: 0.54; margin-left: 2px; position: relative; top: -1px; background: #E6E6EA; font-size: 10px; height: 14px; border-radius: 2px; padding: 2px 5px';
	var html = '<div style="' + actionTransaction + '">' + txnStatusString;
	if (txnStatus != _TransactionStatus2.default.TXN_ORDER_OPEN && linkedTxn) {
		html += '<span style="' + smallText + '">\n\t\t\t\t\t' + _MyDate2.default.getDate('d/m/y', new Date(txnDate)) + '\n\t\t\t\t</span>';
	}
	html += '</div>';
	return html;
};

var DeliveryChallanContainer = function (_React$Component) {
	(0, _inherits3.default)(DeliveryChallanContainer, _React$Component);

	function DeliveryChallanContainer(props) {
		(0, _classCallCheck3.default)(this, DeliveryChallanContainer);

		var _this = (0, _possibleConstructorReturn3.default)(this, (DeliveryChallanContainer.__proto__ || (0, _getPrototypeOf2.default)(DeliveryChallanContainer)).call(this, props));

		_this.getApi = function (params) {
			_this.api = params.api;
			var selectedStatusFilter = _this.props.selectedStatusFilter;

			if (selectedStatusFilter !== undefined) {
				var filterOptions = [{
					label: _StringConstants2.default.deliveryChallanOpen,
					value: _StringConstants2.default.deliveryChallanOpen,
					selected: _StringConstants2.default.deliveryChallanOpen == selectedStatusFilter
				}, {
					label: _StringConstants2.default.deliveryChallanCompletedString,
					value: _StringConstants2.default.deliveryChallanCompletedString,
					selected: _StringConstants2.default.deliveryChallanCompletedString == selectedStatusFilter
				}];
				var filterInstance = params.api.getFilterInstance('txnStatusString');
				filterInstance.setModel({
					value: {
						filterOptions: filterOptions,
						checkedFilterOptions: filterOptions.filter(function (option) {
							return option.selected;
						})
					}
				});
				filterInstance.applyFilter();
			}
			_this.api.setRowData(_this.allOrders);
		};

		_this.allOrders = [];
		_this.contextMenu = [{
			title: 'View/Edit',
			cmd: _this.viewTransaction,
			visible: true,
			id: 'edit'
		}, {
			title: 'Delete',
			cmd: function cmd(row) {
				return TransactionHelper.deleteTransaction(row.txnId, row.txnTypeConstant);
			},
			visible: true,
			id: 'delete'
		}, {
			title: 'Open PDF',
			cmd: function cmd(row) {
				return TransactionHelper.openPDF(row.txnId, row.txnTypeConstant);
			},
			visible: true,
			id: 'openPDF'
		}, {
			title: 'Preview',
			cmd: function cmd(row) {
				return TransactionHelper.preview(row.txnId, row.txnTypeConstant);
			},
			visible: true,
			id: 'preview'
		}, {
			title: 'Print',
			cmd: function cmd(row) {
				return TransactionHelper.print(row.txnId, row.txnTypeConstant);
			},
			visible: true,
			id: 'print'
		}];
		_this.state = {
			records: [],
			totalTransactions: 0,
			columnDefs: [{
				field: 'date',
				headerName: 'DATE',
				width: 100,
				sort: 'desc',
				filter: _DateFilter2.default,
				sortingOrder: ['desc', 'asc', null],
				getQuickFilterText: function getQuickFilterText(params) {
					return _MyDate2.default.getDate('d/m/y', params.value);
				},
				cellRenderer: function cellRenderer(params) {
					return _MyDate2.default.getDate('d/m/y', params.value);
				}
			}, {
				field: 'name',
				filter: _StringFilter2.default,
				headerName: 'PARTY',
				width: 180
			}, {
				field: 'challanNo',
				filter: _StringFilter2.default,
				headerName: 'CHALLAN NO.',
				width: 80,
				comparator: function comparator(a, b) {
					var typeA = typeof a === 'undefined' ? 'undefined' : (0, _typeof3.default)(a);
					var typeB = typeof b === 'undefined' ? 'undefined' : (0, _typeof3.default)(b);
					if (typeA === 'number' && typeB === 'number') {
						return a - b;
					} else if (typeA === 'string' && typeB === 'string') {
						return a.localeCompare(b);
					} else {
						if (typeA === 'number') {
							return -1;
						} else {
							return 1;
						}
						// return a > b ? 1 : a < b ? -1 : 0;
					}
				},
				valueGetter: function valueGetter(params) {
					var value = Number(params.data.challanNo);
					if (isNaN(value)) {
						return params.data.challanNo;
					}
					return value;
				},
				getQuickFilterText: function getQuickFilterText(params) {
					return params.data.challanNoWithPrefix;
				},
				cellRenderer: function cellRenderer(params) {
					return params.data.challanNoWithPrefix;
				}
			}, {
				field: 'dueDate',
				headerName: 'DUE DATE',
				width: 175,
				filter: _DateFilter2.default,
				sortingOrder: ['desc', 'asc', null],
				getQuickFilterText: function getQuickFilterText(params) {
					return _MyDate2.default.getDate('d/m/y', params.value);
				},
				cellRenderer: DueDateRenderer
			}, {
				field: 'amount',
				headerName: 'TOTAL AMOUNT',
				width: 150,
				filter: _NumberFilter2.default,
				cellClass: 'alignRight',
				getQuickFilterText: function getQuickFilterText(params) {
					return params.value < 0 ? '-' : params.value;
				},
				cellRenderer: function cellRenderer(params) {
					if (params.value < 0) {
						return '-';
					} else {
						return _MyDouble2.default.getAmountWithDecimalAndCurrencyWithoutSign(params.value);
					}
				}
			}, {
				field: 'txnStatusString',
				headerName: 'STATUS',
				filter: _MultiSelectionFilter2.default,
				filterOptions: [{
					label: _StringConstants2.default.deliveryChallanOpen,
					value: _StringConstants2.default.deliveryChallanOpen,
					selected: false
				}, {
					label: _StringConstants2.default.deliveryChallanCompletedString,
					value: _StringConstants2.default.deliveryChallanCompletedString,
					selected: false
				}],
				width: 120,
				cellRenderer: StatusRenderer
			}, {
				field: 'action',
				headerName: 'ACTION',
				width: 180,
				suppressFilter: true,
				getQuickFilterText: function getQuickFilterText(params) {
					return params.value;
				},
				cellRenderer: ActionRenderer
			}, {
				field: 'txnId',
				headerName: '',
				width: 50,
				getQuickFilterText: function getQuickFilterText() {
					return '';
				},
				suppressFilter: true,
				cellRenderer: function cellRenderer() {
					return '<div class="contextMenuDots"></div>';
				}
			}],
			contextMenu: _this.contextMenu
		};

		_this._loaded = [];
		_this._perPage = 100;
		_this._id = null;
		_this.getData = _this.getData.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(DeliveryChallanContainer, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			var queryFields = {
				txnTypes: [_TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN]
			};
			this.loadTransactions(queryFields, 0, this._perPage, {
				queryFields: queryFields,
				start: this._perPage,
				noOfRecords: -1
			});
			TransactionHelper.bindEventsForPreviewDialog();
			MyAnalytics.pushScreen('View Delivery Challan');
			window.onResume = this.onResume.bind(this);
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			TransactionHelper.unBindEventsForPreviewDialog();
			delete window.onResume;
		}
	}, {
		key: 'onResume',
		value: function onResume() {
			var queryFields = {
				txnTypes: [_TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN]
			};
			this.loadTransactions(queryFields, 0, this._perPage, {
				queryFields: queryFields,
				start: this._perPage,
				noOfRecords: -1
			});
			TransactionHelper.bindEventsForPreviewDialog();
		}
	}, {
		key: 'viewTransaction',
		value: function viewTransaction(row) {
			if (!row.txnId) {
				row = row.data;
			}
			TransactionHelper.viewTransaction(row.txnId, _TxnTypeConstant2.default.getTxnType(row.txnType));
		}
	}, {
		key: 'getData',
		value: function getData(response) {
			if (response.meta.next && response.data.length == this._perPage) {
				var next = response.meta.next;
				this.loadTransactions(next.queryFields, next.start, next.noOfRecords);
			}

			var data = response.data.map(function (row) {
				row.date = new Date(row.date);
				row.dueDate = new Date(row.dueDate);
				return row;
			});

			if (this.api) {
				if (response.meta.next) {
					this.api.setRowData(data);
				} else {
					this.api.updateRowData({ add: data });
				}
			} else {
				if (response.meta.next) {
					this.allOrders = data;
				} else {
					this.allOrders = [].concat((0, _toConsumableArray3.default)(this.allOrders), (0, _toConsumableArray3.default)(data));
				}
			}
		}
	}, {
		key: 'loadTransactions',
		value: function loadTransactions(queryFields, start, noOfRecords, next) {
			var dataLoader = new _DataLoader2.default();
			this.setState({
				totalTransactions: dataLoader.getTransactionCountByTxnType(TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN)
			});
			// this.totalTransactions = 0;
			this.props.registerPromise(_DBWindow2.default.send(_IPCActions.GET_DELIVERY_CHALLAN_TRANSACTIONS, {
				queryFields: queryFields,
				start: start,
				noOfRecords: noOfRecords,
				next: next
			})).then(this.getData);
		}
	}, {
		key: 'filterContextMenu',
		value: function filterContextMenu(row) {
			row.data.txnTypeConstant = _TxnTypeConstant2.default.getTxnType(row.data.txnType);
			var menuItems = TransactionHelper.filterContextMenu([].concat((0, _toConsumableArray3.default)(this.contextMenu)), row);
			this.setState({ contextMenu: menuItems });
		}
	}, {
		key: 'convertTo',
		value: function convertTo(params) {
			var _params$data5 = params.data,
			    txnId = _params$data5.txnId,
			    txnType = _params$data5.txnType;

			TransactionHelper.returnTransaction(txnId, _TxnTypeConstant2.default.getTxnType(txnType));
		}
	}, {
		key: 'onTransactionSelected',
		value: function onTransactionSelected(row) {
			this.currentTransaction = row;
		}
	}, {
		key: 'render',
		value: function render() {
			var totalTransactions = this.state.totalTransactions;

			var gridOptions = {
				enableFilter: true,
				enableSorting: true,
				rowSelection: 'single',
				columnDefs: this.state.columnDefs,
				rowData: [],
				rowHeight: 40,
				headerHeight: 40,
				rowClass: 'customVerticalBorder',
				onRowSelected: this.onTransactionSelected.bind(this),
				onRowDoubleClicked: this.viewTransaction.bind(this)
			};
			return _react2.default.createElement(
				'div',
				{ className: 'd-flex flex-column', style: { height: '100%' } },
				_react2.default.createElement(
					'ul',
					{ className: 'tab-container d-flex' },
					_react2.default.createElement(
						'li',
						{ className: 'tab-items col' },
						'DELIVERY CHALLAN'
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'd-flex flex-column flex-container flex-grow-1' },
					totalTransactions === 0 && _react2.default.createElement(_FirstScreen2.default, {
						image: '../inlineSVG/first-screens/delivery-challan.svg',
						text: 'Make & share delivery challan with your customers & convert it to sale whenever you want.',
						onClick: function onClick() {
							TransactionHelper.viewTransaction('', TxnTypeConstant.getTxnType(TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN));
						},
						buttonLabel: 'Add Your First Delivery Challan'
					}),
					totalTransactions > 0 && _react2.default.createElement(_Grid2.default, {
						title: 'Transactions',
						quickFilter: true,
						CTAButton: _react2.default.createElement(_NewTransactionButton2.default, {
							txnType: _TxnTypeConstant2.default.getTxnType(_TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN),
							label: '+ Add ' + _TxnTypeConstant2.default.getTxnTypeForUI(_TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN)
						}),
						classes: 'alternateRowColor customVerticalBorder gridRowHeight40',
						filterContextMenu: this.filterContextMenu.bind(this),
						contextMenu: this.state.contextMenu,
						gridOptions: gridOptions,
						getApi: this.getApi
					})
				)
			);
		}
	}]);
	return DeliveryChallanContainer;
}(_react2.default.Component);

DeliveryChallanContainer.propTypes = {
	registerPromise: _propTypes2.default.func,
	selectedStatusFilter: _propTypes2.default.string
};
exports.default = (0, _trashableReact2.default)(DeliveryChallanContainer);