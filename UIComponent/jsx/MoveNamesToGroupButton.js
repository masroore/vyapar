Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _DataInserter = require('../../DBManager/DataInserter');

var _DataInserter2 = _interopRequireDefault(_DataInserter);

var _SelectNamesModal = require('./SelectNamesModal');

var _SelectNamesModal2 = _interopRequireDefault(_SelectNamesModal);

var _DBWindow = require('../../Utilities/DBWindow');

var _DBWindow2 = _interopRequireDefault(_DBWindow);

var _IPCActions = require('../../Constants/IPCActions');

var _NameType = require('../../Constants/NameType');

var NameType = _interopRequireWildcard(_NameType);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _index = require('trashable-react/dist/index');

var _index2 = _interopRequireDefault(_index);

var _ButtonDropdown = require('./ButtonDropdown');

var _ButtonDropdown2 = _interopRequireDefault(_ButtonDropdown);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MoveNamesToGroupButton = function (_React$Component) {
	(0, _inherits3.default)(MoveNamesToGroupButton, _React$Component);

	function MoveNamesToGroupButton(props) {
		(0, _classCallCheck3.default)(this, MoveNamesToGroupButton);

		var _this = (0, _possibleConstructorReturn3.default)(this, (MoveNamesToGroupButton.__proto__ || (0, _getPrototypeOf2.default)(MoveNamesToGroupButton)).call(this, props));

		_this.state = {
			open: false,
			records: []
		};
		_this.openDialog = _this.openDialog.bind(_this);
		_this.add = _this.add.bind(_this);
		_this.onClose = _this.onClose.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(MoveNamesToGroupButton, [{
		key: 'onClose',
		value: function onClose() {
			this.setState({
				open: false
			});
		}
	}, {
		key: 'add',
		value: function add(selected) {
			if (!selected.length) {
				ToastHelper.error('select at least one party.');
				return;
			}
			var dataInserter = new _DataInserter2.default();
			var statusCode = dataInserter.updatePartyGroupId(this.props.partyGroupId, selected);
			if (statusCode) {
				ToastHelper.success('Parties moved successfully');
				window.onResume && window.onResume();
				this.setState({
					open: false
				});
			} else {
				ToastHelper.error('Unable to move parties');
			}
		}
	}, {
		key: 'openDialog',
		value: function openDialog() {
			var _this2 = this;

			this.props.registerPromise(_DBWindow2.default.send(_IPCActions.GET_NAMES_LIST_NOT_IN_GROUP, {
				id: this.props.partyGroupId,
				nameType: NameType.NAME_TYPE_PARTY
			})).then(function (data) {
				if (data.length < 1) {
					ToastHelper.info('This group contains all parties');
					return false;
				}
				_this2.setState({
					open: true,
					records: data
				});
			});
		}
	}, {
		key: 'getMenuOptions',
		value: function getMenuOptions() {
			var menuOptions = [];
			menuOptions.push({
				buttonText: 'Move to this group',
				onClick: this.openDialog
			});
			return menuOptions;
		}
	}, {
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				'div',
				null,
				_react2.default.createElement(
					_SelectNamesModal2.default,
					{
						isOpen: this.state.open,
						onSave: this.add,
						onClose: this.onClose,
						items: this.state.records,
						notSelectFirstRow: true
					},
					'Move to this group'
				),
				_react2.default.createElement(_ButtonDropdown2.default, {
					menuOptions: this.getMenuOptions(),
					addPadding: false,
					color: 'secondary'
				})
			);
		}
	}]);
	return MoveNamesToGroupButton;
}(_react2.default.Component);

MoveNamesToGroupButton.propTypes = {
	registerPromise: _propTypes2.default.func,
	partyGroupId: _propTypes2.default.number
};

exports.default = (0, _index2.default)(MoveNamesToGroupButton);