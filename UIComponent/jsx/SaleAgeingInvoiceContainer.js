Object.defineProperty(exports, "__esModule", {
	value: true
});

var _typeof2 = require('babel-runtime/helpers/typeof');

var _typeof3 = _interopRequireDefault(_typeof2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _NumberFilter = require('./grid-filters/NumberFilter');

var _NumberFilter2 = _interopRequireDefault(_NumberFilter);

var _SaleAgeingDropdownMenu = require('./SaleAgeingDropdownMenu');

var _SaleAgeingDropdownMenu2 = _interopRequireDefault(_SaleAgeingDropdownMenu);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SaleAgeingInvoiceContainer = function (_React$Component) {
	(0, _inherits3.default)(SaleAgeingInvoiceContainer, _React$Component);

	function SaleAgeingInvoiceContainer(props) {
		(0, _classCallCheck3.default)(this, SaleAgeingInvoiceContainer);

		var _this = (0, _possibleConstructorReturn3.default)(this, (SaleAgeingInvoiceContainer.__proto__ || (0, _getPrototypeOf2.default)(SaleAgeingInvoiceContainer)).call(this, props));

		_this.getApi = function (params) {
			var _this$props = _this.props,
			    getApi = _this$props.getApi,
			    dataInvoice = _this$props.dataInvoice;

			getApi && getApi(params);
			_this.api = params.api;
			_this.api.setRowData(dataInvoice);
		};

		var options = _this.props.options;
		_this.state = {
			columnDefs: [{
				field: 'invoiceNo',
				headerName: 'Invoice No.',
				width: 30,
				checkboxSelection: true,
				headerCheckboxSelection: true,
				headerCheckboxSelectionFilteredOnly: true,
				comparator: function comparator(a, b) {
					var typeA = typeof a === 'undefined' ? 'undefined' : (0, _typeof3.default)(a);
					var typeB = typeof b === 'undefined' ? 'undefined' : (0, _typeof3.default)(b);
					if (typeA === 'number' && typeB === 'number') {
						return a - b;
					} else if (typeA === 'string' && typeB === 'string') {
						return a.localeCompare(b);
					} else {
						if (typeA === 'number') {
							return -1;
						} else {
							return 1;
						}
						// return a > b ? 1 : a < b ? -1 : 0;
					}
				},
				valueGetter: function valueGetter(params) {
					var value = Number(params.data.refNo);
					if (isNaN(value)) {
						return params.data.refNo;
					}
					return value;
				},
				getQuickFilterText: function getQuickFilterText(params) {
					return params.data.invoiceNo;
				},
				cellRenderer: function cellRenderer(params) {
					return params.data.invoiceNo;
				},
				suppressFilter: true
			}, {
				field: 'date',
				headerName: 'Date',
				width: 30,
				suppressFilter: true
			}, {
				field: 'dueDate',
				headerName: 'Due Date',
				width: 30,
				suppressFilter: true
			}, {
				field: 'dueDays',
				headerName: 'Days Late',
				width: 30,
				filter: _NumberFilter2.default,
				cellClass: 'alignRight'
			}, {
				field: 'amount',
				headerName: 'Amount',
				width: 30,
				filter: _NumberFilter2.default,
				cellClass: 'alignRight',
				cellRenderer: function cellRenderer(params) {
					if (params.value < 0) {
						return '-';
					} else {
						return _MyDouble2.default.getAmountWithDecimalAndCurrencyWithoutSign(params.value);
					}
				}
			}, {
				field: 'balance',
				headerName: 'Balance',
				width: 30,
				filter: _NumberFilter2.default,
				cellClass: 'alignRight',
				cellRenderer: function cellRenderer(params) {
					if (params.value < 0) {
						return '-';
					} else {
						return _MyDouble2.default.getAmountWithDecimalAndCurrencyWithoutSign(params.value);
					}
				}
			}, {
				fieldId: 'txnId',
				width: 10,
				suppressFilter: true,
				cellClass: 'alignRight',
				cellRenderer: _SaleAgeingDropdownMenu2.default,
				options: {
					sendReminderOnWhatsApp: options.sendReminderOnWhatsApp,
					sendTxnOnSMS: options.sendTxnOnSMS,
					sendTxnOnMail: options.sendTxnOnMail,
					sendTxnOnWhatsApp: options.sendTxnOnWhatsApp
				}
			}]
		};
		return _this;
	}

	(0, _createClass3.default)(SaleAgeingInvoiceContainer, [{
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(nextProps) {
			if (this.api) {
				this.api.setRowData(this.props.dataInvoice);
			}
			return false;
		}
	}, {
		key: 'render',
		value: function render() {
			var rowData = this.props.dataInvoice;
			var options = this.props.options;

			var gridOptions = {
				enableFilter: true,
				enableSorting: true,
				rowSelection: 'multiple',
				columnDefs: this.state.columnDefs,
				rowData: rowData,
				rowHeight: 40,
				headerHeight: 40,
				rowClass: 'customVerticalBorder',
				overlayNoRowsTemplate: 'No transactions to show',
				onRowDoubleClicked: options.viewTransaction
			};

			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement(_Grid2.default, {
					gridKey: 'saleAgeingReportInvoiceGridKey',
					classes: 'alternateRowColor customVerticalBorder gridRowHeight40',
					getApi: this.getApi,
					gridOptions: gridOptions,
					notSelectFirstRow: true
				})
			);
		}
	}]);
	return SaleAgeingInvoiceContainer;
}(_react2.default.Component);

SaleAgeingInvoiceContainer.propTypes = {
	dataInvoice: _propTypes2.default.array.isRequired,
	getApi: _propTypes2.default.func.isRequired,
	options: _propTypes2.default.object.isRequired
};

exports.default = SaleAgeingInvoiceContainer;