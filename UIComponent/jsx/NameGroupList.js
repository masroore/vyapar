Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _AddEditCategoryModal = require('./AddEditCategoryModal');

var _AddEditCategoryModal2 = _interopRequireDefault(_AddEditCategoryModal);

var _ToastHelper = require('../../UIControllers/ToastHelper');

var ToastHelper = _interopRequireWildcard(_ToastHelper);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var NameGroupList = function (_React$Component) {
	(0, _inherits3.default)(NameGroupList, _React$Component);

	function NameGroupList(props) {
		(0, _classCallCheck3.default)(this, NameGroupList);

		var _this = (0, _possibleConstructorReturn3.default)(this, (NameGroupList.__proto__ || (0, _getPrototypeOf2.default)(NameGroupList)).call(this, props));

		_this.selectFirstRow = function () {
			setTimeout(function () {
				var firstColumn = document.querySelector('.left-column .ag-row-selected .ag-cell[col-id="partyGroupName"]');
				firstColumn && firstColumn.focus();
			}, 100);
		};

		_this.columnDefs = [{
			field: 'partyGroupName',
			headerName: 'GROUP',
			width: 150,
			sort: 'asc'
		}, {
			field: 'partyCount',
			suppressMenu: true,
			comparator: function comparator(a, b) {
				return a - b;
			},
			headerName: 'PARTY',
			width: 90,
			cellClass: 'alignRight',
			headerClass: 'alignRight'
		}, {
			field: 'groupName',
			headerName: '',
			width: 30,
			getQuickFilterText: function getQuickFilterText() {
				return '';
			},
			suppressSorting: true,
			suppressFilter: true,
			cellRenderer: function cellRenderer() {
				return '<div class="contextMenuDots"></div>';
			}
		}];
		_this.state = {
			isOpen: false,
			groupName: ''
		};
		_this.deleteItem = _this.deleteItem.bind(_this);
		_this.getApi = _this.getApi.bind(_this);
		_this.editNameGroup = _this.editNameGroup.bind(_this);
		_this.onSave = _this.onSave.bind(_this);
		_this.onClose = _this.onClose.bind(_this);
		_this.contextMenu = [{
			title: 'View/Edit',
			cmd: _this.editNameGroup,
			visible: true,
			id: 'edit'
		}, {
			title: 'Delete',
			cmd: _this.deleteItem,
			visible: true,
			id: 'delete'
		}];
		return _this;
	}

	(0, _createClass3.default)(NameGroupList, [{
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(nextProps) {
			var _this2 = this;

			if (this.api) {
				this.api.setRowData(nextProps.items);
				setTimeout(function () {
					if (_this2.api.getSelectedRows().length === 0) {
						var selected = _this2.api.getDisplayedRowAtIndex(0);
						if (selected) {
							selected.setSelected(true);
						}
					}
				});
			}
			return true;
		}
	}, {
		key: 'getApi',
		value: function getApi(params) {
			this.api = params.api;
			this.api.setRowData(this.props.items);
		}
	}, {
		key: 'editNameGroup',
		value: function editNameGroup(row) {
			if (row.data) {
				row = row.data;
			}
			if (row.partyGroupName === 'General') {
				ToastHelper.info('Genaral party group cannot be deleted or edited.');
				return false;
			}
			MyAnalytics.pushEvent('Edit Party Group Open');
			this.setState({ isOpen: true, groupName: row.partyGroupName });
		}
	}, {
		key: 'onSave',
		value: function onSave(name) {
			var oldName = this.state.groupName;

			var PartyGroupLogic = require('../../BizLogic/PartyGroupLogic');
			var ErrorCode = require('../../Constants/ErrorCode');
			var partyGroupLogic = new PartyGroupLogic();

			if (name) {
				if (oldName.toLowerCase() === name.toLowerCase()) {
					this.setState({ isOpen: false });
				} else {
					var statusCode = partyGroupLogic.editPartyGroup(oldName, name);
					if (statusCode === ErrorCode.ERROR_PARTYGROUP_UPDATE_SUCCESS) {
						MyAnalytics.pushEvent('Edit Party Group Save');
						ToastHelper.success(statusCode);
						this.setState({ isOpen: false });
						window.onResume && window.onResume();
					} else {
						ToastHelper.error(statusCode);
					}
				}
			} else {
				ToastHelper.error('group name cannot be empty');
			}
		}
	}, {
		key: 'onClose',
		value: function onClose() {
			this.setState({ isOpen: false });
		}
	}, {
		key: 'deleteItem',
		value: function deleteItem(row) {
			// let catId = that.id;
			if (row.partyGroupId === 1) {
				ToastHelper.info('General party group cannot be deleted or edited.');
				return false;
			}
			var NameGroupCache = require('../../Cache/PartyGroupCache');
			var ErrorCode = require('../../Constants/ErrorCode');
			var nameGroupCache = new NameGroupCache();
			var partyGroup = nameGroupCache.getPartyGroupObjectById(row.partyGroupId);
			if (partyGroup) {
				var statusCode = partyGroup.deletePartyGroup();
				if (statusCode === ErrorCode.ERROR_PARTYGROUP_DELETE_SUCCESS) {
					nameGroupCache.reloadPartyGroup();
					ToastHelper.success(statusCode);
					this.setState({ isOpen: false });
					window.onResume && window.onResume();
				}
			}
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			try {
				var $delDialog2 = $('#delDialog2');
				if ($delDialog2.is(':ui-dialog')) {
					$delDialog2.dialog('close').dialog('destroy');
				}
			} catch (error) {
				logger.error(error);
			}
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.selectFirstRow();
		}
	}, {
		key: 'render',
		value: function render() {
			var _state = this.state,
			    isOpen = _state.isOpen,
			    groupName = _state.groupName;

			var gridOptions = {
				enableSorting: true,
				rowSelection: 'single',
				columnDefs: this.columnDefs,
				rowData: [],
				onRowDoubleClicked: this.editNameGroup,
				suppressColumnMoveAnimation: true,
				onRowSelected: this.props.onItemSelected,
				overlayNoRowsTemplate: 'No party groups to show',
				headerHeight: 40,
				rowHeight: 40,
				deltaRowDataMode: true,
				getRowNodeId: function getRowNodeId(data) {
					return data.partyGroupId;
				}
			};
			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				isOpen && _react2.default.createElement(_AddEditCategoryModal2.default, {
					categoryName: groupName,
					onClose: this.onClose,
					onSave: this.onSave,
					isOpen: isOpen,
					title: 'Edit Party Group',
					label: 'Change Party Group Name'
				}),
				_react2.default.createElement(
					'div',
					{ className: 'gridContainer d-flex' },
					_react2.default.createElement(_Grid2.default, {
						getApi: this.getApi,
						gridKey: 'nameGroupsListGridKey',
						contextMenu: this.contextMenu,
						classes: 'noBorder gridRowHeight40',
						selectFirstRow: true,
						height: '100%',
						gridOptions: gridOptions
					})
				)
			);
		}
	}]);
	return NameGroupList;
}(_react2.default.Component);

NameGroupList.propTypes = {
	onItemSelected: _propTypes2.default.func,
	items: _propTypes2.default.array
};

exports.default = NameGroupList;