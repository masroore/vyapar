Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _MoveNamesToGroupButton = require('./MoveNamesToGroupButton');

var _MoveNamesToGroupButton2 = _interopRequireDefault(_MoveNamesToGroupButton);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles() {
	return {
		title: {
			color: '#333',
			fontFamily: 'robotoMedium',
			fontSize: '0.875rem',
			textTransform: 'uppercase',
			maxWidth: 400,
			overflow: 'hidden',
			whiteSpace: 'nowrap',
			textOverflow: 'ellipsis'
		},
		containerForItemList: {
			maxHeight: 80
		},
		partyCount: {
			marginRight: 10
		}
	};
};

var NameGroupDetail = function (_React$Component) {
	(0, _inherits3.default)(NameGroupDetail, _React$Component);

	function NameGroupDetail() {
		(0, _classCallCheck3.default)(this, NameGroupDetail);
		return (0, _possibleConstructorReturn3.default)(this, (NameGroupDetail.__proto__ || (0, _getPrototypeOf2.default)(NameGroupDetail)).apply(this, arguments));
	}

	(0, _createClass3.default)(NameGroupDetail, [{
		key: 'render',
		value: function render() {
			var classes = this.props.classes;
			var item = this.props.currentItem;

			if (!item || !item.partyGroupId) {
				return false;
			}

			return _react2.default.createElement(
				'div',
				{ className: 'containerForItemList ' + classes.containerForItemList },
				_react2.default.createElement(
					'div',
					{ className: 'mb-10 d-flex justify-content-between' },
					_react2.default.createElement(
						'div',
						{ id: 'currentItemName', className: classes.title },
						item.partyGroupName
					),
					_react2.default.createElement(
						'div',
						null,
						_react2.default.createElement(_MoveNamesToGroupButton2.default, { partyGroupId: item.partyGroupId })
					)
				),
				_react2.default.createElement(
					'div',
					null,
					_react2.default.createElement(
						'div',
						null,
						_react2.default.createElement(
							'div',
							{ className: 'row' },
							_react2.default.createElement(
								'div',
								{ className: 'col' },
								_react2.default.createElement(
									'span',
									{ className: classes.partyCount },
									'Parties(',
									item.partyCount,
									')'
								),
								_react2.default.createElement('span', { dangerouslySetInnerHTML: _MyDouble2.default.getBalanceAmountWithDecimalAndCurrency(item.partyTotal, null, true) })
							)
						)
					)
				)
			);
		}
	}]);
	return NameGroupDetail;
}(_react2.default.Component);

NameGroupDetail.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	currentItem: _propTypes2.default.object
};

exports.default = (0, _styles.withStyles)(styles)(NameGroupDetail);