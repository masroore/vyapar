Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _DecimalInputFilter = require('../../Utilities/DecimalInputFilter');

var _DecimalInputFilter2 = _interopRequireDefault(_DecimalInputFilter);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _Dialog = require('@material-ui/core/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _DialogActions = require('@material-ui/core/DialogActions');

var _DialogActions2 = _interopRequireDefault(_DialogActions);

var _DialogContent = require('@material-ui/core/DialogContent');

var _DialogContent2 = _interopRequireDefault(_DialogContent);

var _DialogContentText = require('@material-ui/core/DialogContentText');

var _DialogContentText2 = _interopRequireDefault(_DialogContentText);

var _TextField = require('@material-ui/core/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _DialogTitle = require('@material-ui/core/DialogTitle');

var _DialogTitle2 = _interopRequireDefault(_DialogTitle);

var _ErrorCode = require('../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

var _NameCache = require('../../Cache/NameCache');

var _NameCache2 = _interopRequireDefault(_NameCache);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _MessageDraftLogic = require('../../BizLogic/MessageDraftLogic');

var _MessageDraftLogic2 = _interopRequireDefault(_MessageDraftLogic);

var _StringConstants = require('../../Constants/StringConstants');

var _StringConstants2 = _interopRequireDefault(_StringConstants);

var _TransactionFactory = require('../../BizLogic/TransactionFactory');

var _TransactionFactory2 = _interopRequireDefault(_TransactionFactory);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ShowPartyNumberModal(_ref) {
	var OKButton = _ref.OKButton,
	    CancelButton = _ref.CancelButton,
	    title = _ref.title,
	    onChange = _ref.onChange,
	    _onClose = _ref.onClose,
	    _ref$dialogOptions = _ref.dialogOptions,
	    dialogOptions = _ref$dialogOptions === undefined ? {} : _ref$dialogOptions,
	    phoneNumber = _ref.phoneNumber,
	    nameId = _ref.nameId,
	    txnType = _ref.txnType,
	    txnId = _ref.txnId;

	var _useState = (0, _react.useState)(phoneNumber),
	    _useState2 = (0, _slicedToArray3.default)(_useState, 2),
	    number = _useState2[0],
	    setNumber = _useState2[1];

	var onSave = function onSave() {
		if (!number || number < 10) {
			ToastHelper.error(_ErrorCode2.default.ERROR_ENTER_CORRECT_PHONE_NUMBER);
			return;
		} else if (number.substr(0, 1) <= 5) {
			ToastHelper.error(_ErrorCode2.default.ERROR_CORRECT_PHONE_NUMBER_DIGITS);
			return;
		}
		var nameCache = new _NameCache2.default();
		var nameObj = nameCache.findNameObjectByNameId(nameId);
		if (nameObj.getFullName().toLowerCase() !== _StringConstants2.default.cashSale.toLowerCase()) {
			nameObj.setPhoneNumber(number);
			var statusCode = nameObj.updateNameToDBTransaction();
			if (statusCode !== _ErrorCode2.default.ERROR_NAME_SAVE_SUCCESS) {
				ToastHelper.error(_ErrorCode2.default.ERROR_NAME_UPDATE_FAILED);
				return false;
			}
			nameObj = nameCache.findNameObjectByNameId(nameId);
			var messageDraftLogic = new _MessageDraftLogic2.default();
			var transactionFactory = new _TransactionFactory2.default();
			var txn = transactionFactory.getTransactionObject(txnType);
			var txnObj = txn.getTransactionFromId(txnId);
			messageDraftLogic.sendTransactionSMSToParty(txnObj, true, true, number);
			_onClose(true);
		}
	};
	return _react2.default.createElement(
		_Dialog2.default,
		(0, _extends3.default)({ open: true, onClose: function onClose() {
				return _onClose();
			} }, dialogOptions),
		_react2.default.createElement(
			_DialogTitle2.default,
			null,
			title || 'Send SMS'
		),
		_react2.default.createElement(
			_DialogContent2.default,
			null,
			_react2.default.createElement(
				_DialogContentText2.default,
				{ id: 'alert-dialog-description' },
				_react2.default.createElement(_TextField2.default, {
					value: number,
					onInput: function onInput(e) {
						return _DecimalInputFilter2.default.inputTextFilterForNumber(e.target, 10);
					},
					onChange: function onChange(e) {
						return setNumber(e.target.value);
					}
				})
			)
		),
		_react2.default.createElement(
			_DialogActions2.default,
			null,
			_react2.default.createElement(
				_Button2.default,
				{ onClick: function onClick() {
						return _onClose();
					}, color: 'primary' },
				CancelButton || 'Cancel'
			),
			_react2.default.createElement(
				_Button2.default,
				{ onClick: onSave, color: 'primary', autoFocus: true },
				OKButton || 'OK'
			)
		)
	);
}

ShowPartyNumberModal.propTypes = {
	onChange: _propTypes2.default.func,
	onClose: _propTypes2.default.func,
	title: _propTypes2.default.string,
	OKButton: _propTypes2.default.string,
	CancelButton: _propTypes2.default.string,
	dialogOptions: _propTypes2.default.object,
	nameId: _propTypes2.default.number
};

exports.default = ShowPartyNumberModal;