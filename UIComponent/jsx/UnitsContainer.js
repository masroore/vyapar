Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _SearchBox = require('./SearchBox');

var _SearchBox2 = _interopRequireDefault(_SearchBox);

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _AddUnitButton = require('./AddUnitButton');

var _AddUnitButton2 = _interopRequireDefault(_AddUnitButton);

var _AddConversionButton = require('./AddConversionButton');

var _AddConversionButton2 = _interopRequireDefault(_AddConversionButton);

var _UnitList = require('./UnitList');

var _UnitList2 = _interopRequireDefault(_UnitList);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _ItemUnitCache = require('../../Cache/ItemUnitCache');

var _ItemUnitCache2 = _interopRequireDefault(_ItemUnitCache);

var _ItemUnitMappingCache = require('../../Cache/ItemUnitMappingCache');

var _ItemUnitMappingCache2 = _interopRequireDefault(_ItemUnitMappingCache);

var _throttleDebounce = require('throttle-debounce');

var _TransactionHelper = require('../../Utilities/TransactionHelper');

var TransactionHelper = _interopRequireWildcard(_TransactionHelper);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var itemUnitCache = new _ItemUnitCache2.default();

var UnitsContainer = function (_React$Component) {
	(0, _inherits3.default)(UnitsContainer, _React$Component);

	function UnitsContainer(props) {
		(0, _classCallCheck3.default)(this, UnitsContainer);

		var _this = (0, _possibleConstructorReturn3.default)(this, (UnitsContainer.__proto__ || (0, _getPrototypeOf2.default)(UnitsContainer)).call(this, props));

		_this.getApi = function (params) {
			_this.api = params.api;
			_this.api.setRowData(_this.state.records);
		};

		_this.allItems = itemUnitCache.getAllItemUnits();
		_this.state = {
			records: [],
			items: _this.allItems,
			currentItem: null,
			columnDefs: [{
				field: 'baseUnitId',
				width: 50,
				headerName: '',
				cellRenderer: function cellRenderer(params) {
					return params.rowIndex + 1;
				}
			}, {
				field: '',
				headerName: 'CONVERSION',
				getQuickFilterText: function getQuickFilterText(params) {
					var unitMapping = params.data;
					var baseUnit = unitMapping.getBaseUnitId();
					baseUnit = itemUnitCache.getItemUnitNameById(baseUnit);
					var secondaryUnit = unitMapping.getSecondaryUnitId();
					secondaryUnit = itemUnitCache.getItemUnitNameById(secondaryUnit);
					var rate = unitMapping.getConversionRate();
					return ' 1 ' + baseUnit + ' = ' + rate + ' ' + secondaryUnit + ' ';
				},
				cellRenderer: function cellRenderer(params) {
					var unitMapping = params.data;
					var baseUnit = unitMapping.getBaseUnitId();
					baseUnit = itemUnitCache.getItemUnitNameById(baseUnit);
					var secondaryUnit = unitMapping.getSecondaryUnitId();
					secondaryUnit = itemUnitCache.getItemUnitNameById(secondaryUnit);
					var rate = unitMapping.getConversionRate();
					return ' 1 ' + baseUnit + ' = ' + rate + ' ' + secondaryUnit + ' ';
				}
			}]
		};

		_this._id = null;
		_this.filterItems = _this.filterItems.bind(_this);
		_this.props.onResume && _this.props.onResume();
		_this.onItemSelected = (0, _throttleDebounce.debounce)(50, _this.onItemSelected.bind(_this));
		return _this;
	}

	(0, _createClass3.default)(UnitsContainer, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			window.onResume = this.onResume.bind(this);
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			delete window.onResume;
		}
	}, {
		key: 'onResume',
		value: function onResume() {
			var _this2 = this;

			this.props.onResume && this.props.onResume();
			this.allItems = itemUnitCache.getAllItemUnits();
			if (this.state.currentItem) {
				var currentItem = this.allItems.find(function (item) {
					return _this2.state.currentItem.getUnitId() == item.getUnitId();
				});
				this.setState({ currentItem: currentItem });
			}
			this.filterItems(this.state.searchText);
			this.onItemSelected(null, true);
		}
	}, {
		key: 'editCategory',
		value: function editCategory(row) {
			if (!row.id) {
				row = row.data;
			}
			TransactionHelper.viewTransaction(row.id, row.txnTypeConstant);
		}
	}, {
		key: 'onItemSelected',
		value: function onItemSelected(row) {
			var fromSync = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

			if (!fromSync) {
				if (!row.node.selected) {
					return false;
				}
				var id = row.data.getUnitId();
				/* do not reload current item */
				// if (this._id == id) {
				// 	return false;
				// }

				this._id = id;
			}

			var itemUnitMappingCache = new _ItemUnitMappingCache2.default();
			var records = itemUnitMappingCache.getItemUnitWhereBaseUnit(this._id);
			this.setState({
				currentItem: row ? row.data : this.state.currentItem,
				records: records
			});
			this.api.setRowData(records);
		}
	}, {
		key: 'filterItems',
		value: function filterItems(searchText) {
			if (!searchText) {
				this.setState({ items: this.allItems, searchText: '' });
				return false;
			}
			var query = searchText.toLowerCase();
			var filteredItems = this.allItems.filter(function (item) {
				return item.getUnitName().toLowerCase().includes(query) || item.getUnitShortName().toLowerCase().includes(query);
			});
			var records = this.state.records;
			var currentItem = this.state.currentItem;
			if (!filteredItems.length) {
				records = [];
				this.api.setRowData(records);
				currentItem = null;
			}
			this.setState({
				records: records,
				currentItem: currentItem,
				items: filteredItems,
				searchText: searchText
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _this3 = this;

			var gridOptions = {
				enableFilter: false,
				enableSorting: true,
				rowSelection: 'single',
				columnDefs: this.state.columnDefs,
				headerHeight: 40,
				rowHeight: 40,
				rowClass: 'customVerticalBorder',
				rowData: [],
				getRowNodeId: function getRowNodeId(data) {
					return data.getMappingId();
				}
			};
			return _react2.default.createElement(
				'div',
				{ className: 'd-flex align-items-stretch flex-container' },
				_react2.default.createElement(
					'div',
					{ className: 'd-flex flex-column left-column' },
					_react2.default.createElement(
						'div',
						{ className: 'listheaderDiv d-flex mt-20 mb-10' },
						_react2.default.createElement(
							'div',
							{ className: (0, _classnames2.default)('searchDiv', this.state.showSearch ? 'width100 d-flex' : '') },
							_react2.default.createElement(_SearchBox2.default, { throttle: 500,
								filter: this.filterItems,
								collapsed: !this.state.showSearch,
								collapsedTextBoxClass: 'width100',
								onCollapsedClick: function onCollapsedClick() {
									_this3.setState({ showSearch: true });
								},
								onInputBlur: function onInputBlur() {
									_this3.setState({ showSearch: false });
								}
							})
						),
						_react2.default.createElement(
							'div',
							{ className: 'buttonDiv flex-1 d-flex justify-content-end' },
							_react2.default.createElement(_AddUnitButton2.default, {
								collapsed: this.state.showSearch
							})
						)
					),
					_react2.default.createElement(_UnitList2.default, {
						onItemSelected: this.onItemSelected,
						items: this.state.items
					})
				),
				_react2.default.createElement(
					'div',
					{ className: 'd-flex flex-column right-column flex-grow-1' },
					this.state.currentItem && _react2.default.createElement(
						'div',
						{ className: 'categoryDetailContainer' },
						_react2.default.createElement(
							'div',
							{ className: 'clearfix' },
							_react2.default.createElement(
								'div',
								{
									id: 'currentItemName',
									className: 'titleColor halfdiv floatLeft width50 ellipseText'
								},
								this.state.currentItem && this.state.currentItem.getUnitName()
							),
							_react2.default.createElement(_AddConversionButton2.default, { baseUnit: this._id })
						)
					),
					_react2.default.createElement(_Grid2.default, {
						gridKey: 'unitMappingListGridKey',
						title: 'Units',
						quickFilter: true,
						classes: 'alternateRowColor customVerticalBorder gridRowHeight40',
						getApi: this.getApi,
						gridOptions: gridOptions
					})
				)
			);
		}
	}]);
	return UnitsContainer;
}(_react2.default.Component);

exports.default = UnitsContainer;

UnitsContainer.propTypes = {
	onResume: _propTypes2.default.func
};