Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _TxnTypeConstant = require('../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _Dialog = require('@material-ui/core/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _DialogActions = require('@material-ui/core/DialogActions');

var _DialogActions2 = _interopRequireDefault(_DialogActions);

var _DialogContent = require('@material-ui/core/DialogContent');

var _DialogContent2 = _interopRequireDefault(_DialogContent);

var _Table = require('@material-ui/core/Table');

var _Table2 = _interopRequireDefault(_Table);

var _TableBody = require('@material-ui/core/TableBody');

var _TableBody2 = _interopRequireDefault(_TableBody);

var _TableCell = require('@material-ui/core/TableCell');

var _TableCell2 = _interopRequireDefault(_TableCell);

var _TableHead = require('@material-ui/core/TableHead');

var _TableHead2 = _interopRequireDefault(_TableHead);

var _TableRow = require('@material-ui/core/TableRow');

var _TableRow2 = _interopRequireDefault(_TableRow);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _MyDate = require('../../Utilities/MyDate');

var MyDate = _interopRequireWildcard(_MyDate);

var _MyDouble = require('../../Utilities/MyDouble');

var MyDouble = _interopRequireWildcard(_MyDouble);

var _IconButton = require('@material-ui/core/IconButton');

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Close = require('@material-ui/icons/Close');

var _Close2 = _interopRequireDefault(_Close);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LinkPaymentModal = function (_React$Component) {
	(0, _inherits3.default)(LinkPaymentModal, _React$Component);

	function LinkPaymentModal() {
		(0, _classCallCheck3.default)(this, LinkPaymentModal);
		return (0, _possibleConstructorReturn3.default)(this, (LinkPaymentModal.__proto__ || (0, _getPrototypeOf2.default)(LinkPaymentModal)).apply(this, arguments));
	}

	(0, _createClass3.default)(LinkPaymentModal, [{
		key: 'render',
		value: function render() {
			var _props = this.props,
			    txnType = _props.txnType,
			    totalReceivedAmount = _props.totalReceivedAmount,
			    receivedLater = _props.receivedLater,
			    selectedTransactionMap = _props.selectedTransactionMap,
			    _props$discountAmount = _props.discountAmountInHistory,
			    discountAmountInHistory = _props$discountAmount === undefined ? 0 : _props$discountAmount,
			    isOpen = _props.isOpen,
			    onClose = _props.onClose;


			var cashReceived = totalReceivedAmount - receivedLater;
			var classes = {};

			var amountString = '';
			var title = '';
			var totalLinkedAmount = 0;
			var tableBody = [];

			switch (txnType) {
				case _TxnTypeConstant2.default.TXN_TYPE_CASHIN:
					amountString = 'Payment Amount';
					cashReceived = 0;
					break;
				case _TxnTypeConstant2.default.TXN_TYPE_CASHOUT:
					amountString = 'Payment Amount';
					cashReceived = 0;
					break;
				case _TxnTypeConstant2.default.TXN_TYPE_SALE:
					amountString = 'Received during Sale';
					break;
				case _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN:
					amountString = 'Paid during Note';
					break;
				case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE:
					amountString = 'Paid during Purchase';
					break;
				case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN:
					amountString = 'Received during Note';
					break;
				case _TxnTypeConstant2.default.TXN_TYPE_POPENBALANCE:
					break;
				case _TxnTypeConstant2.default.TXN_TYPE_ROPENBALANCE:
					break;
			}

			if (txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT) {
				if (cashReceived + discountAmountInHistory) {
					title += amountString + ' : ' + MyDouble.getAmountWithDecimal(cashReceived + discountAmountInHistory);
				}
			} else {
				if (cashReceived) {
					title += amountString + ' : ' + MyDouble.getAmountWithDecimal(cashReceived);
				}
			}

			if (selectedTransactionMap && selectedTransactionMap.size > 0) {
				selectedTransactionMap.forEach(function (value, key) {
					var selectedTxnId = MyDouble.convertStringToDouble(key);
					var txn = void 0;
					var linkedAmt = void 0;
					var txnDate = void 0;
					var txnType = void 0;
					var txnRefNo = void 0;
					var transactionTypeString = void 0;

					if (selectedTxnId > 0) {
						txn = value[0];
						linkedAmt = MyDouble.convertStringToDouble(value[1]);
						txnDate = MyDate.getDate('d/m/y', txn.getTxnDate());
						txnType = txn.getTxnType();
						txnRefNo = txn.getFullTxnRefNumber();
						transactionTypeString = _TxnTypeConstant2.default.getTxnTypeForUI(txnType);
					} else {
						var closedTxn = value[0];
						linkedAmt = MyDouble.convertStringToDouble(value[1]);
						txnDate = MyDate.getDate('d/m/y', closedTxn.getClosedLinkTxnDate());
						txnType = closedTxn.getClosedLinkTxnType();
						txnRefNo = closedTxn.getClosedLinkTxnRefNumber();
						transactionTypeString = _TxnTypeConstant2.default.getTxnTypeForUI(txnType);
					}

					totalLinkedAmount += linkedAmt;

					tableBody.push(_react2.default.createElement(
						_TableRow2.default,
						{ key: selectedTxnId },
						_react2.default.createElement(
							_TableCell2.default,
							{ component: 'th', scope: 'row' },
							txnDate
						),
						_react2.default.createElement(
							_TableCell2.default,
							{ align: 'right' },
							txnRefNo
						),
						_react2.default.createElement(
							_TableCell2.default,
							{ align: 'right' },
							transactionTypeString
						),
						_react2.default.createElement(
							_TableCell2.default,
							{ align: 'right' },
							MyDouble.getAmountWithDecimal(linkedAmt)
						)
					));
				});
			}

			return _react2.default.createElement(
				_Dialog2.default,
				{
					maxWidth: 'md',
					open: isOpen,
					onClose: onClose
				},
				_react2.default.createElement(
					'div',
					{ className: 'd-flex justify-content-between align-items-center' },
					_react2.default.createElement(
						'h2',
						{ className: 'link-payment-modal-title pl-20' },
						'Payment History'
					),
					_react2.default.createElement(
						_IconButton2.default,
						null,
						_react2.default.createElement(_Close2.default, { onClick: onClose })
					)
				),
				_react2.default.createElement(
					_DialogContent2.default,
					null,
					!title && selectedTransactionMap && selectedTransactionMap.size == 0 && _react2.default.createElement(
						'div',
						{ className: 'mt-10 mb-10' },
						' No Payment History Found.'
					),
					_react2.default.createElement(
						'div',
						{ className: 'mt-10 mb-10' },
						title
					),
					selectedTransactionMap && selectedTransactionMap.size > 0 && _react2.default.createElement(
						_Table2.default,
						{ className: classes.table },
						_react2.default.createElement(
							_TableHead2.default,
							null,
							_react2.default.createElement(
								_TableRow2.default,
								null,
								_react2.default.createElement(
									_TableCell2.default,
									null,
									'Transaction Date'
								),
								_react2.default.createElement(
									_TableCell2.default,
									{ align: 'right' },
									'Ref No'
								),
								_react2.default.createElement(
									_TableCell2.default,
									{ align: 'right' },
									'Transaction Type'
								),
								_react2.default.createElement(
									_TableCell2.default,
									{ align: 'right' },
									'Linked Amount'
								)
							)
						),
						_react2.default.createElement(
							_TableBody2.default,
							null,
							tableBody,
							_react2.default.createElement(
								_TableRow2.default,
								null,
								_react2.default.createElement(
									_TableCell2.default,
									{ align: 'right', colSpan: 4 },
									'Total: ',
									totalLinkedAmount
								)
							)
						)
					)
				),
				_react2.default.createElement(
					_DialogActions2.default,
					null,
					_react2.default.createElement(
						_Button2.default,
						{ onClick: onClose, color: 'primary' },
						'Close'
					)
				)
			);
		}
	}]);
	return LinkPaymentModal;
}(_react2.default.Component);

exports.default = LinkPaymentModal;