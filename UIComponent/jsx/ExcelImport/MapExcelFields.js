Object.defineProperty(exports, "__esModule", {
    value: true
});

var _create = require('babel-runtime/core-js/object/create');

var _create2 = _interopRequireDefault(_create);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles/');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
    '@global': {
        '.mapFieldsHeader': {
            borderBottom: '1px solid #DDDDDD'
        },
        '.mapFieldsTable': {
            color: '#2B4C56',
            fontSize: '13px',
            verticalAlign: 'middle',
            width: '36rem',
            padding: 0,
            textAlign: 'left'
        },
        '.headerRow span': {
            fontWeight: 'bold'
        },
        '.headerRow>:last-child': {
            border: 0
        },
        '.mapFieldsTableRow span': {
            padding: '.75rem',
            paddingLeft: '2rem',
            borderRight: '1px solid #DDD',
            display: 'inline-block',
            width: '50%'
        },
        '.mapFieldsTableRow select': {
            padding: '.5rem',
            paddingLeft: '2rem',
            display: 'inline-block',
            width: '50%',
            border: 0,
            backgroundColor: 'inherit'
        },
        '.mapFieldsTableRow:nth-child(even)': {
            backgroundColor: '#F2F4F8'
        },
        'option': {
            backgroundColor: 'white'
        }
    }
};

var MapFields = function (_Component) {
    (0, _inherits3.default)(MapFields, _Component);

    function MapFields(props) {
        (0, _classCallCheck3.default)(this, MapFields);

        var _this = (0, _possibleConstructorReturn3.default)(this, (MapFields.__proto__ || (0, _getPrototypeOf2.default)(MapFields)).call(this, props));

        _this.handleProceed = function () {
            var excelMap = _this.props.enabledFields.reduce(function (acc, field) {
                acc[field] = _this.fieldRefs[field].value;
                return acc;
            }, (0, _create2.default)(null));
            _this.props.handleProceed(excelMap);
        };

        _this.fieldRefs = {};
        return _this;
    }

    (0, _createClass3.default)(MapFields, [{
        key: 'getMapFieldsTableRow',
        value: function getMapFieldsTableRow(element) {
            var _this2 = this;

            var findMatch = this.props.findMatch;
            if (typeof findMatch !== 'function') {
                findMatch = function findMatch() {
                    return -1;
                };
            }
            return _react2.default.createElement(
                'div',
                { key: element, className: 'mapFieldsTableRow' },
                _react2.default.createElement(
                    'span',
                    null,
                    element
                ),
                _react2.default.createElement(
                    'select',
                    { ref: function ref(input) {
                            return _this2.fieldRefs[element] = input;
                        }, defaultValue: findMatch(element) },
                    _react2.default.createElement('option', { value: '-1' }),
                    this.props.excelFields.map(function (element, index) {
                        return _react2.default.createElement(
                            'option',
                            { key: index, value: index },
                            element
                        );
                    })
                )
            );
        }
    }, {
        key: 'render',
        value: function render() {
            var _this3 = this;

            return _react2.default.createElement(
                'div',
                { className: 'whitediv' },
                _react2.default.createElement(
                    'div',
                    { className: 'mapFieldsHeader whitediv-header' },
                    _react2.default.createElement(
                        'h3',
                        null,
                        'Map your fields to Vyapar\'s fields'
                    ),
                    _react2.default.createElement('img', { src: '../inlineSVG/round_close.svg', onClick: this.props.onClose })
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'whitediv-body' },
                    _react2.default.createElement(
                        'div',
                        { className: 'mapFieldsTable' },
                        _react2.default.createElement(
                            'div',
                            { className: 'mapFieldsTableRow headerRow' },
                            _react2.default.createElement(
                                'span',
                                null,
                                'Fields available in Vyapar'
                            ),
                            _react2.default.createElement(
                                'span',
                                null,
                                'Select your field'
                            )
                        ),
                        this.props.enabledFields.map(function (element) {
                            return _this3.getMapFieldsTableRow(element);
                        })
                    )
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'mapFieldsFooter whitediv-footer' },
                    _react2.default.createElement(
                        'button',
                        {
                            onClick: this.handleProceed,
                            className: 'blue-button floatRight mr-20' },
                        'Proceed'
                    )
                )
            );
        }
    }]);
    return MapFields;
}(_react.Component);

;

exports.default = (0, _styles.withStyles)(styles)(MapFields);