Object.defineProperty(exports, "__esModule", {
  value: true
});

var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _PartiesGrid = require('./PartiesGrid');

var _PartiesGrid2 = _interopRequireDefault(_PartiesGrid);

var _ImportPartiesXL = require('../../../../BizLogic/ImportPartiesXL');

var _ImportPartiesXL2 = _interopRequireDefault(_ImportPartiesXL);

var _styles = require('@material-ui/core/styles');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint-disable no-mixed-spaces-and-tabs */

var StringConstants = require('../../../../Constants/StringConstants');
var styles = {
  '@global': {
    '.importparty-div': {
      fontSize: '14px'
    },
    '.importparty-div-header': {
      borderBottom: '1px solid #DDDDDD',
      width: '100%',
      paddingLeft: '1.5rem'
    },
    '.importparty-tab': {
      display: 'inline-block',
      paddingRight: '1.5rem',
      backgroundColor: 'white',
      height: '100%',
      cursor: 'pointer',
      lineHeight: '2rem',
      marginRight: '2rem'
    },
    '.importparty-tab>span': {
      fontWeight: '500'
    },
    '.importparty-tab.active': {
      color: '#1789FC',
      borderBottom: '2px solid #1789FC'
    },
    '.import-party-icon': {
      display: 'inline-block',
      width: '1.5rem',
      height: '1.5rem',
      verticalAlign: 'middle',
      backgroundRepeat: 'no-repeat',
      marginRight: '.25rem'
    },
    '.import-party-icon.error': {
      backgroundImage: 'url(\'../inlineSVG/warning-icon.svg\')'
    },
    '.import-party-icon.valid': {
      backgroundImage: 'url(\'../inlineSVG/green-tick-circle.svg\')'
    },
    '.importparty-div-body': {
      height: 'calc(100% - 3rem)'
    },
    '.importparty-div-body-label': {
      margin: '.75rem',
      marginLeft: '2rem',
      fontWeight: 'bold'
    },
    '.importPartyGrid': {
      height: 'calc(100% - 3rem)'
    },
    '.importPartyGrid .gridContainer': {
      height: '100%',
      color: '#2B4C56',
      lineHeight: '14px',
      fontSize: '12px'
    },
    '.importparties-grid-row.ag-row-even': {
      backgroundColor: '#F2F4F8'
    },
    '.partyGridCell': {
      height: '100%',
      width: '100%',
      border: 0,
      borderRight: '1px solid #E9E9E9',
      backgroundColor: 'inherit',
      '-webkit-appearance': 'none',
      paddingLeft: '.5rem'
    },

    '.partyGridCell.cell-error': {
      border: '1px solid white',
      borderRadius: '4px',
      backgroundColor: '#F6B0B0'
    },

    '.partyGridCell.cell-corrected': {
      border: '1px dashed #14C059',
      borderRadius: '4px'
    },
    '.importparties-grid-row .ag-react-container': {
      height: '100%',
      width: '100%'
    },
    '.partyGridCellIcon>.import-party-icon': {
      position: 'absolute',
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)'
    },
    '.importPartyGrid.makeRowsReadOnly .importparties-grid-row': {
      pointerEvents: 'none'
    },
    '.importPartyGrid .ag-header-cell:not(:first-child)': {
      lineHeight: '36px',
      padding: '0 1rem',
      border: '1px solid #E9E9E9'
    }
  }
};

var DisplayImportParties = function (_Component) {
  (0, _inherits3.default)(DisplayImportParties, _Component);

  function DisplayImportParties(props) {
    (0, _classCallCheck3.default)(this, DisplayImportParties);

    var _this = (0, _possibleConstructorReturn3.default)(this, (DisplayImportParties.__proto__ || (0, _getPrototypeOf2.default)(DisplayImportParties)).call(this, props));

    _initialiseProps.call(_this);

    var _this$props = _this.props,
        validParties = _this$props.validParties,
        invalidParties = _this$props.invalidParties,
        partyNamesMap = _this$props.partyNamesMap;

    _this.state = {
      isErrorTabActive: invalidParties.length > 0,
      validPartyCount: validParties.length,
      invalidPartyCount: invalidParties.length,
      currentList: invalidParties.length ? invalidParties : validParties,
      partyNamesMap: partyNamesMap
    };
    _this.importPartiesBtn = _react2.default.createRef();
    _this.refreshGrid = true;
    return _this;
  }

  (0, _createClass3.default)(DisplayImportParties, [{
    key: 'render',
    value: function render() {
      var refreshGrid = this.refreshGrid;
      this.refreshGrid = false;
      return _react2.default.createElement(
        'div',
        { className: 'whitediv' },
        _react2.default.createElement(
          'div',
          { className: 'whitediv-header' },
          _react2.default.createElement(
            'h3',
            null,
            'Import Parties'
          ),
          _react2.default.createElement('img', { src: '../inlineSVG/round_close.svg', onClick: this.props.onClose })
        ),
        _react2.default.createElement(
          'div',
          { className: 'whitediv-body importparty-div' },
          _react2.default.createElement(
            'div',
            { className: 'importparty-div-header' },
            _react2.default.createElement(
              'div',
              {
                className: 'importparty-tab ' + (this.state.isErrorTabActive ? '' : ' active'),
                onClick: this.clickValidPartiesTab },
              _react2.default.createElement('div', { className: 'import-party-icon valid' }),
              _react2.default.createElement(
                'span',
                null,
                'Valid Parties : ' + this.state.validPartyCount
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'importparty-tab ' + (this.state.isErrorTabActive ? ' active' : ''),
                onClick: this.clickInvalidPartiesTab },
              _react2.default.createElement('div', { className: 'import-party-icon error' }),
              _react2.default.createElement(
                'span',
                null,
                'Parties with Errors : ' + this.state.invalidPartyCount
              )
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'importparty-div-body' },
            _react2.default.createElement(
              'div',
              { className: 'importparty-div-body-label' },
              this.state.isErrorTabActive ? 'Parties with Errors' : 'Valid Parties'
            ),
            _react2.default.createElement(
              'div',
              { className: 'importPartyGrid ' + (this.state.isErrorTabActive ? '' : 'makeRowsReadOnly') },
              _react2.default.createElement(_PartiesGrid2.default, {
                partyList: this.state.currentList,
                refreshGrid: refreshGrid,
                context: {
                  onPartyUpdate: this.onPartyUpdate,
                  enabledFields: this.props.partyFields,
                  onBlurAfterUpdate: this.onBlurAfterUpdate,
                  allowEdit: this.state.isErrorTabActive
                }
              })
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'whitediv-footer' },
          _react2.default.createElement(
            'button',
            { disabled: !this.state.validPartyCount,
              ref: this.importPartiesBtn,
              onClick: this.clickImportPartiesBtn,
              className: 'blue-button floatRight mr-20' },
            'Import ' + this.state.validPartyCount + ' Valid Parties'
          )
        )
      );
    }
  }]);
  return DisplayImportParties;
}(_react.Component);

var _initialiseProps = function _initialiseProps() {
  var _this2 = this;

  this.clickValidPartiesTab = function () {
    _this2.refreshGrid = true;
    _this2.setState({
      isErrorTabActive: false,
      currentList: _this2.props.validParties
    });
  };

  this.clickInvalidPartiesTab = function () {
    _this2.refreshGrid = true;
    _this2.setState({
      isErrorTabActive: true,
      currentList: _this2.props.invalidParties
    });
  };

  this.clickImportPartiesBtn = function () {
    _this2.importPartiesBtn.current.disabled = true;
    _this2.props.importParties();
  };

  this.onPartyUpdate = function (obj) {
    var index = obj.index,
        field = obj.field,
        newVal = obj.newVal;
    var _state = _this2.state,
        currentList = _state.currentList,
        validPartyCount = _state.validPartyCount,
        invalidPartyCount = _state.invalidPartyCount;

    var partyRow = currentList[index];
    var currFieldValidState = partyRow[field].isValid;
    var validatedRes = _ImportPartiesXL2.default.PartyDefinations[field].validateAndSetValue(newVal);
    var tempFieldValidState = !(validatedRes instanceof Error);
    var fieldsToAcceptInValidValues = [_ImportPartiesXL2.default.PartyFieldHeaders.EMAIL_ID, _ImportPartiesXL2.default.PartyFieldHeaders.GSTIN_NO, _ImportPartiesXL2.default.PartyFieldHeaders.OPENING_DATE];
    if (!fieldsToAcceptInValidValues.includes(field) && currFieldValidState && !tempFieldValidState) {
      return; // Prevent entering invalid characters in already valid fields;
    }
    var prevRowValidState = partyRow.isRowValid;
    partyRow[field].value = newVal;
    _this2.validatePartyRow(partyRow, index);
    if (prevRowValidState !== partyRow.isRowValid) {
      if (partyRow.isRowValid) {
        validPartyCount++;
        invalidPartyCount--;
      } else {
        validPartyCount--;
        invalidPartyCount++;
      }
    }
    _this2.setState({ currentList: currentList, validPartyCount: validPartyCount, invalidPartyCount: invalidPartyCount });
  };

  this.onBlurAfterUpdate = function (obj) {
    var shouldRefresh = obj.shouldRefresh,
        field = obj.field;

    if (shouldRefresh || field == _ImportPartiesXL2.default.PartyFieldHeaders.PARTY_NAME) {
      _this2.refreshPartyList();
    }
  };

  this.validatePartyRow = function (partyRow, index) {
    var partyFields = _this2.props.partyFields;
    var partyNamesMap = _this2.state.partyNamesMap;

    partyRow.isRowValid = true; // resetting.
    partyFields.forEach(function (partyField) {
      switch (partyField) {
        case _ImportPartiesXL2.default.PartyFieldHeaders.PARTY_NAME:
          var partyName = partyRow[partyField].value ? partyRow[partyField].value.trim() : '';
          if (!partyName) {
            partyRow[partyField].isValid = false;
            var error = new Error(StringConstants.IMPORT_PARTY_NAME_EMPTY);
            partyRow[partyField].error = error;
            partyRow[partyField].value = '';
          } else if (partyNamesMap.has(partyName.toLowerCase()) && index != partyNamesMap.get(partyName.toLowerCase())) {
            partyRow[partyField].isValid = false;
            var _error = new Error(StringConstants.IMPORT_PARTY_NAME_DUPLICATE);
            partyRow[partyField].error = _error;
          } else {
            partyRow[partyField].isValid = true;
            partyRow[partyField].error = '';
          }
          partyRow.isRowValid = partyRow.isRowValid && partyRow[partyField].isValid;
          break;
        default:
          var validatedRes = _ImportPartiesXL2.default.PartyDefinations[partyField].validateAndSetValue(partyRow[partyField].value);
          partyRow[partyField].isValid = !(validatedRes instanceof Error);
          partyRow[partyField].error = validatedRes instanceof Error ? validatedRes : '';
          partyRow.isRowValid = partyRow.isRowValid && partyRow[partyField].isValid;
          break;
      }
    });
    return partyRow;
  };

  this.refreshPartyList = function () {
    var partyList = _this2.state.currentList;
    var _props = _this2.props,
        partyFields = _props.partyFields,
        partyNamesMap = _props.partyNamesMap;

    partyNamesMap = new _map2.default(partyNamesMap);
    var validRowCount = 0;
    partyList.forEach(function (partyRow, index) {
      var partyName = partyRow[_ImportPartiesXL2.default.PartyFieldHeaders.PARTY_NAME].value.trim().toLowerCase();
      var partyNameExists = partyNamesMap.has(partyName) && partyNamesMap[partyName] !== index;
      partyRow[_ImportPartiesXL2.default.PartyFieldHeaders.PARTY_NAME].isValid = true; // reset
      partyRow[_ImportPartiesXL2.default.PartyFieldHeaders.PARTY_NAME].error = ''; // reset
      if (!partyRow[_ImportPartiesXL2.default.PartyFieldHeaders.PARTY_NAME].value) {
        partyRow[_ImportPartiesXL2.default.PartyFieldHeaders.PARTY_NAME].isValid = false;
        var error = new Error(StringConstants.IMPORT_PARTY_NAME_EMPTY);
        partyRow[_ImportPartiesXL2.default.PartyFieldHeaders.PARTY_NAME].error = error;
      } else if (partyNameExists) {
        partyRow[_ImportPartiesXL2.default.PartyFieldHeaders.PARTY_NAME].isValid = false;
        partyRow[_ImportPartiesXL2.default.PartyFieldHeaders.PARTY_NAME].error = new Error(StringConstants.IMPORT_PARTY_NAME_DUPLICATE);
      }
      partyRow.isRowValid = partyFields.reduce(function (acc, field) {
        return acc && partyRow[field].isValid;
      }, true);
      if (partyRow.isRowValid) {
        partyNamesMap.set(partyRow[_ImportPartiesXL2.default.PartyFieldHeaders.PARTY_NAME].value.trim().toLowerCase(), index);
      } else {
        if (partyRow[_ImportPartiesXL2.default.PartyFieldHeaders.PARTY_NAME].isValid) {
          partyNamesMap.delete(partyRow[_ImportPartiesXL2.default.PartyFieldHeaders.PARTY_NAME].value.trim().toLowerCase());
        }
      }
      validRowCount += partyRow.isRowValid;
    });
    _this2.setState({
      currentList: partyList,
      validPartyCount: _this2.props.validParties.length + validRowCount,
      invalidPartyCount: _this2.props.invalidParties.length - validRowCount,
      partyNamesMap: partyNamesMap
    });
  };
};

;
DisplayImportParties.propTypes = {
  validParties: _propTypes2.default.array,
  invalidParties: _propTypes2.default.array,
  partyNamesMap: _propTypes2.default.object,
  partyCodesMap: _propTypes2.default.object,
  excelFilePath: _propTypes2.default.string,
  uploadInProgress: _propTypes2.default.bool,
  showExcelData: _propTypes2.default.bool,
  importParties: _propTypes2.default.func,
  partyFields: _propTypes2.default.array,
  onClose: _propTypes2.default.func.isRequired
};
exports.default = (0, _styles.withStyles)(styles)(DisplayImportParties);