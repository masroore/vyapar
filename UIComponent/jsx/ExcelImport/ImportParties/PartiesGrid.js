Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _MountComponent = require('../../MountComponent');

var _MountComponent2 = _interopRequireDefault(_MountComponent);

var _ImportPartiesXL = require('../../../../BizLogic/ImportPartiesXL');

var _ImportPartiesXL2 = _interopRequireDefault(_ImportPartiesXL);

var _Tooltip = require('@material-ui/core/Tooltip');

var _Tooltip2 = _interopRequireDefault(_Tooltip);

var _Typography = require('@material-ui/core/Typography');

var _Typography2 = _interopRequireDefault(_Typography);

var _styles = require('@material-ui/core/styles/');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint-disable react/prop-types */
var styles = {
	'@global': {
		'.partyGridCellIcon': {
			height: '100%',
			width: '100%',
			border: 0,
			borderRight: '1px solid #E9E9E9',
			backgroundColor: 'white'
		}
	}
};
var CustomToolTip = (0, _styles.withStyles)(function (theme) {
	return {
		tooltip: {
			backgroundColor: '#2B3B56',
			color: 'white',
			maxWidth: 220,
			fontSize: theme.typography.pxToRem(12),
			boxShadow: '0px 8px 12px #FF0000',
			opacity: '100%'
		}
	};
})(_Tooltip2.default);

var PartiesGrid = function (_React$Component) {
	(0, _inherits3.default)(PartiesGrid, _React$Component);

	function PartiesGrid(props) {
		(0, _classCallCheck3.default)(this, PartiesGrid);

		var _this = (0, _possibleConstructorReturn3.default)(this, (PartiesGrid.__proto__ || (0, _getPrototypeOf2.default)(PartiesGrid)).call(this, props));

		_this.getApi = function (params) {
			_this.api = params.api;
			_this.api.setRowData(_this.props.partyList);
		};

		_this.state = {};
		return _this;
	}

	(0, _createClass3.default)(PartiesGrid, [{
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(nextProps) {
			if (this.api && nextProps.refreshGrid) {
				this.api.setRowData(nextProps.partyList);
			}
			return false;
		}
	}, {
		key: 'render',
		value: function render() {
			var Grid = require('../../Grid').default;
			var enabledFields = this.props.context.enabledFields;
			var columnDefs = getColumnDefs(enabledFields);
			var gridOptions = {
				columnDefs: columnDefs,
				defaultColDef: {
					suppressKeyboardEvent: suppressNavigation
				},
				rowData: [],
				rowClass: 'importparties-grid-row',
				cellClass: 'importparties-grid-cell',
				rowHeight: 36,
				headerHeight: 36,
				suppressAutoSize: true,
				suppressMovableColumns: true,
				suppressContextMenu: true,
				suppressFieldDotNotation: true,
				frameworkComponents: {
					textComponent: InputText,
					isValidComponent: IsValidComponent
				},
				context: this.props.context
			};
			return _react2.default.createElement(Grid, {
				notSelectFirstRow: true,
				height: '100%',
				width: '100%',
				avoidDefaultTheme: true,
				getApi: this.getApi,
				gridOptions: gridOptions
			});
		}
	}]);
	return PartiesGrid;
}(_react2.default.Component);

var InputText = function (_Component) {
	(0, _inherits3.default)(InputText, _Component);

	function InputText(props) {
		(0, _classCallCheck3.default)(this, InputText);

		var _this2 = (0, _possibleConstructorReturn3.default)(this, (InputText.__proto__ || (0, _getPrototypeOf2.default)(InputText)).call(this, props));

		_this2.handleChange = function (event) {
			var newVal = event.currentTarget.value;
			var _this2$props = _this2.props,
			    rowIndex = _this2$props.rowIndex,
			    column = _this2$props.column,
			    context = _this2$props.context,
			    api = _this2$props.api;

			context.onPartyUpdate({
				newVal: newVal,
				index: rowIndex,
				field: column.colId
			});
			api.refreshCells({
				rowNodes: [_this2.props.node],
				force: true
			});
		};

		_this2.handleBlur = function () {
			var _this2$props2 = _this2.props,
			    column = _this2$props2.column,
			    context = _this2$props2.context,
			    api = _this2$props2.api,
			    data = _this2$props2.data;

			context.onBlurAfterUpdate({
				shouldRefresh: _this2.state.isRowValidBeforeFocus != data.isRowValid,
				field: column.colId
			});
			api.refreshCells({
				force: true
			});
		};

		_this2.handleFocus = function () {
			_this2.setState({
				isRowValidBeforeFocus: _this2.props.data.isRowValid
			});
		};

		_this2.state = (0, _extends3.default)({}, _this2.props.value);
		return _this2;
	}

	(0, _createClass3.default)(InputText, [{
		key: 'refresh',
		value: function refresh(params) {
			this.setState((0, _extends3.default)({}, params ? params.value : this.props.value));
			return true;
		}
	}, {
		key: 'render',
		value: function render() {
			var _state = this.state,
			    error = _state.error,
			    isValid = _state.isValid,
			    value = _state.value;

			return _react2.default.createElement(
				CustomToolTip,
				{ disableHoverListener: isValid, disableFocusListener: true, disableTouchListener: true,
					title: !isValid ? _react2.default.createElement(
						_react2.default.Fragment,
						null,
						_react2.default.createElement(
							_Typography2.default,
							{ color: 'inherit' },
							error.message
						)
					) : ''
				},
				_react2.default.createElement('input', { type: 'text', className: 'partyGridCell ' + (isValid ? '' : 'cell-error'),
					onChange: this.handleChange,
					value: value,
					onBlur: this.handleBlur,
					onFocus: this.handleFocus,
					disabled: this.props.context && !this.props.context.allowEdit
				})
			);
		}
	}]);
	return InputText;
}(_react.Component);

function InputTextRenderer() {}

// init method gets the details of the cell to be rendere
InputTextRenderer.prototype.init = function (params) {
	this.eGui = document.createElement('div');
	this.eGui.className = 'height100percent';
	this.componentRef = _reactDom2.default.render(_react2.default.createElement(InputText, params), this.eGui);
};

InputTextRenderer.prototype.getGui = function () {
	return this.eGui;
};

InputTextRenderer.prototype.refresh = function (params) {
	this.componentRef.refresh(params);
	this.params = params;
	return true;
};

var IsValidComponent = function (_Component2) {
	(0, _inherits3.default)(IsValidComponent, _Component2);

	function IsValidComponent() {
		(0, _classCallCheck3.default)(this, IsValidComponent);
		return (0, _possibleConstructorReturn3.default)(this, (IsValidComponent.__proto__ || (0, _getPrototypeOf2.default)(IsValidComponent)).apply(this, arguments));
	}

	(0, _createClass3.default)(IsValidComponent, [{
		key: 'render',
		value: function render() {
			var isRowValid = this.props.value;
			var status = isRowValid ? 'valid' : 'error';
			return _react2.default.createElement(
				'div',
				{ className: 'partyGridCellIcon' },
				_react2.default.createElement('div', { className: 'import-party-icon ' + status })
			);
		}
	}]);
	return IsValidComponent;
}(_react.Component);

function IsValidComponentRenderer() {}

// init method gets the details of the cell to be rendere
IsValidComponentRenderer.prototype.init = function (params) {
	this.eGui = document.createElement('div');
	(0, _MountComponent2.default)(IsValidComponent, this.eGui, params);
};

IsValidComponentRenderer.prototype.getGui = function () {
	return this.eGui;
};

function getColumnDefs(enabledFields) {
	var PartyFieldHeaders = _ImportPartiesXL2.default.PartyFieldHeaders;
	var columnDefs = [{
		field: 'isRowValid',
		headerName: '',
		cellRenderer: IsValidComponentRenderer,
		width: 60
	}, {
		field: PartyFieldHeaders.PARTY_NAME,
		headerName: PartyFieldHeaders.PARTY_NAME,
		headerTooltip: PartyFieldHeaders.PARTY_NAME,
		cellRenderer: InputTextRenderer
	}];
	if (enabledFields.includes(PartyFieldHeaders.CONTACT_NO)) {
		columnDefs.push({
			field: PartyFieldHeaders.CONTACT_NO,
			headerName: PartyFieldHeaders.CONTACT_NO,
			headerTooltip: PartyFieldHeaders.CONTACT_NO,
			cellRenderer: InputTextRenderer
		});
	}
	if (enabledFields.includes(PartyFieldHeaders.EMAIL_ID)) {
		columnDefs.push({
			field: PartyFieldHeaders.EMAIL_ID,
			headerName: PartyFieldHeaders.EMAIL_ID,
			headerTooltip: PartyFieldHeaders.EMAIL_ID,
			cellRenderer: InputTextRenderer
		});
	}
	if (enabledFields.includes(PartyFieldHeaders.ADDRESS)) {
		columnDefs.push({
			field: PartyFieldHeaders.ADDRESS,
			headerName: PartyFieldHeaders.ADDRESS,
			headerTooltip: PartyFieldHeaders.ADDRESS,
			cellRenderer: InputTextRenderer
		});
	}
	if (enabledFields.includes(PartyFieldHeaders.OPENING_BALANCE)) {
		columnDefs.push({
			field: PartyFieldHeaders.OPENING_BALANCE,
			headerName: PartyFieldHeaders.OPENING_BALANCE,
			headerTooltip: PartyFieldHeaders.OPENING_BALANCE,
			cellRenderer: InputTextRenderer
		});
	}
	if (enabledFields.includes(PartyFieldHeaders.OPENING_DATE)) {
		columnDefs.push({
			field: PartyFieldHeaders.OPENING_DATE,
			headerName: PartyFieldHeaders.OPENING_DATE,
			headerTooltip: PartyFieldHeaders.OPENING_DATE,
			cellRenderer: InputTextRenderer
		});
	}
	if (enabledFields.includes(PartyFieldHeaders.GSTIN_NO)) {
		columnDefs.push({
			field: PartyFieldHeaders.GSTIN_NO,
			headerName: PartyFieldHeaders.GSTIN_NO,
			headerTooltip: PartyFieldHeaders.GSTIN_NO,
			cellRenderer: InputTextRenderer
		});
	}
	if (enabledFields.includes(PartyFieldHeaders.PARTY_GROUP)) {
		columnDefs.push({
			field: PartyFieldHeaders.PARTY_GROUP,
			headerName: PartyFieldHeaders.PARTY_GROUP,
			headerTooltip: PartyFieldHeaders.PARTY_GROUP,
			cellRenderer: InputTextRenderer
		});
	}
	if (enabledFields.includes(PartyFieldHeaders.SHIPPING_ADDRESS)) {
		columnDefs.push({
			field: PartyFieldHeaders.SHIPPING_ADDRESS,
			headerName: PartyFieldHeaders.SHIPPING_ADDRESS,
			headerTooltip: PartyFieldHeaders.SHIPPING_ADDRESS,
			cellRenderer: InputTextRenderer
		});
	}

	return columnDefs;
}
function suppressNavigation(params) {
	var KEY_TAB = 9;
	var KEY_LEFT = 37;
	var KEY_UP = 38;
	var KEY_RIGHT = 39;
	var KEY_DOWN = 40;

	var event = params.event;
	var key = event.which;
	var keysToSuppress = [KEY_TAB, KEY_LEFT, KEY_UP, KEY_RIGHT, KEY_DOWN];

	var suppress = keysToSuppress.indexOf(key) >= 0;
	return suppress;
}

exports.default = (0, _styles.withStyles)(styles)(PartiesGrid);