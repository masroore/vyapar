Object.defineProperty(exports, "__esModule", {
	value: true
});

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _create = require('babel-runtime/core-js/object/create');

var _create2 = _interopRequireDefault(_create);

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _ImportExcel = require('./../ImportExcel');

var _ImportExcel2 = _interopRequireDefault(_ImportExcel);

var _MapExcelFields = require('./../MapExcelFields');

var _MapExcelFields2 = _interopRequireDefault(_MapExcelFields);

var _DisplayImportParties = require('./DisplayImportParties');

var _DisplayImportParties2 = _interopRequireDefault(_DisplayImportParties);

var _ImportPartiesXL = require('../../../../BizLogic/ImportPartiesXL');

var _ImportPartiesXL2 = _interopRequireDefault(_ImportPartiesXL);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Dialog = require('@material-ui/core/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _Slide = require('@material-ui/core/Slide');

var _Slide2 = _interopRequireDefault(_Slide);

var _styles = require('@material-ui/core/styles/');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _require = require('electron'),
    ipcRenderer = _require.ipcRenderer;

var styles = {
	'@global': {
		'.importPartiesDiv': {
			height: '100%'
		},
		'.whitediv': {
			height: '100%',
			paddingTop: 'var(--titlebar-height)',
			backgroundColor: 'white',
			minHeight: 600,
			minWidth: 800,
			'&>*': {
				width: '100%'
			},
			'&-header': {
				position: 'relative',
				height: '3rem',
				'&>*': {
					position: 'absolute',
					top: '50%',
					transform: 'translateY(-50%)'
				},
				'&>:first-child': {
					left: '2rem'
				},
				'&>:last-child': {
					right: '1rem'
				},
				'&:before': {
					content: '""',
					position: 'fixed',
					top: '-10px',
					left: 0,
					width: '100%',
					height: '10px',
					boxShadow: '0px 0px 8px rgba(0,0,0,.8)'
				}
			},
			'&-body': {
				margin: 'auto 0',
				height: 'calc(100% - 6.5rem)',
				overflow: 'auto'
			},
			'&-footer': {
				height: '3.5rem'
			}
		},
		'.blue-button': {
			backgroundColor: '#1789FC',
			color: 'white',
			borderRadius: 8,
			boxShadow: '0px 3px 6px #000000',
			height: '2.5rem',
			width: '9rem',
			fontSize: 14,
			'&:disabled': {
				backgroundColor: '#9e9e9e',
				cursor: 'default',
				boxShadow: 'none'
			}
		}
	}
};
function Transition(props) {
	return _react2.default.createElement(_Slide2.default, (0, _extends3.default)({ direction: 'up' }, props));
}

var ImportParties = function (_Component) {
	(0, _inherits3.default)(ImportParties, _Component);

	function ImportParties(props) {
		(0, _classCallCheck3.default)(this, ImportParties);

		var _this = (0, _possibleConstructorReturn3.default)(this, (ImportParties.__proto__ || (0, _getPrototypeOf2.default)(ImportParties)).call(this, props));

		_this.handleEsc = function () {
			var _require2 = require('../../../../Constants/Messages'),
			    discardChanges = _require2.discardChanges;

			var retVal = true;
			retVal = !_this.state.excelFilePath || confirm(discardChanges);
			if (retVal) {
				_this.handleClose();
			}
			return retVal;
		};

		_this.handleDownloadSample = function () {
			_ImportPartiesXL2.default.downloadXLFile();
		};

		_this.readExcelFile = function (fileName) {
			if (fileName && (fileName.endsWith('.xls') || fileName.endsWith('.xlsx'))) {
				try {
					var XLSX = require('xlsx');
					return new _promise2.default(function (resolve) {
						_this.setState({
							excelFilePath: fileName,
							uploadInProgress: true
						});
						setTimeout(function () {
							var workbook = XLSX.readFileSync(fileName, {
								dateNF: 'dd/mm/yyyy'
							});
							var sheetName = workbook['SheetNames'][0];
							var sheets = workbook['Sheets'][sheetName];
							sheets['!ref'] = { s: { c: 0, r: 0 }, e: { c: 20, r: 20000 } };
							var excelData = XLSX.utils.sheet_to_json(sheets, { header: 1, blankrows: false, raw: false });
							resolve(excelData);
						}, 1000);
					}).then(function (excelData) {
						if (!excelData.length) {
							var dialog = require('electron').remote.dialog;

							dialog.showMessageBox({
								type: 'info',
								title: 'Blank excel sheet',
								message: 'Please download the template excel file and enter data to continue.'
							});
							_this.setState({
								headerMappingRequired: false,
								validParties: null,
								invalidParties: null,
								excelFilePath: '',
								excelFields: null
							});
							return;
						}
						var headerData = excelData.shift();
						var headersValid = _ImportPartiesXL2.default.validateHeaders(headerData, _this.state.enabledFields);
						if (!headersValid) {
							_this.setState({
								headerMappingRequired: true,
								excelFilePath: fileName,
								excelFields: headerData,
								uploadInProgress: false
							});
							_this.excelData = excelData;
						} else {
							var _require3 = require('../../../../Constants/StringConstants'),
							    SAMPLE_EXCEL_FOOTER_MESSAGE = _require3.SAMPLE_EXCEL_FOOTER_MESSAGE;

							var mappedData = [];
							excelData.forEach(function (partyRow) {
								var party = (0, _create2.default)(null);
								_this.state.enabledFields.forEach(function (field, index) {
									var value = partyRow[index] || '';
									value = ('' + value).trim();
									party[field] = { value: value };
								});
								var isEmptyRow = (0, _values2.default)(party).map(function (obj) {
									return obj.value;
								}).join('') == '' || party[_ImportPartiesXL2.default.PartyFieldHeaders.PARTY_NAME].value == SAMPLE_EXCEL_FOOTER_MESSAGE;
								isEmptyRow || mappedData.push(party);
							});

							var _ImportPartiesXL$pars = _ImportPartiesXL2.default.parseExcelData(mappedData, _this.state.enabledFields),
							    validParties = _ImportPartiesXL$pars.validParties,
							    invalidParties = _ImportPartiesXL$pars.invalidParties,
							    partyNamesMap = _ImportPartiesXL$pars.partyNamesMap;

							_this.setState({
								validParties: validParties,
								invalidParties: invalidParties,
								partyNamesMap: partyNamesMap,
								excelFilePath: fileName,
								uploadInProgress: false,
								showExcelData: true
							});
						}
					}).catch(function (err) {
						var logger = require('../../../../Utilities/logger');
						logger.error(err);
					});
				} catch (err) {
					var logger = require('../../../../Utilities/logger');
					logger.error(err);
				}
			}
		};

		_this.handleProceed = function (excelFieldMap) {
			var _require4 = require('../../../../Constants/StringConstants'),
			    SAMPLE_EXCEL_FOOTER_MESSAGE = _require4.SAMPLE_EXCEL_FOOTER_MESSAGE;

			var excelData = _this.excelData;
			var mappedData = [];
			excelData.map(function (partyRow) {
				var party = (0, _create2.default)(null);
				_this.state.enabledFields.forEach(function (field) {
					var value = partyRow[excelFieldMap[field]] || '';
					value = ('' + value).trim();
					party[field] = { value: value };
				});
				var isEmptyRow = (0, _values2.default)(party).map(function (obj) {
					return obj.value;
				}).join('') == '' || party[_ImportPartiesXL2.default.PartyFieldHeaders.PARTY_NAME].value == SAMPLE_EXCEL_FOOTER_MESSAGE;
				isEmptyRow || mappedData.push(party);
			});

			var _ImportPartiesXL$pars2 = _ImportPartiesXL2.default.parseExcelData(mappedData, _this.state.enabledFields),
			    validParties = _ImportPartiesXL$pars2.validParties,
			    invalidParties = _ImportPartiesXL$pars2.invalidParties,
			    partyNamesMap = _ImportPartiesXL$pars2.partyNamesMap;

			_this.setState({
				headerMappingRequired: false,
				showExcelData: true,
				validParties: validParties,
				invalidParties: invalidParties,
				partyNamesMap: partyNamesMap
			});
		};

		_this.findMatch = function (enabledField) {
			var PartyDefinations = _ImportPartiesXL2.default.PartyDefinations;

			var excelFields = _this.state.excelFields;
			var match = excelFields.findIndex(function (excelField) {
				excelField = excelField ? excelField.trim().toLowerCase() : '';
				return PartyDefinations[enabledField].mappableExcelHeaders.includes(excelField);
			});
			return match;
		};

		_this.handleClose = function () {
			var saveChanges = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

			_this.setState({ open: false });
			_this.props.onClose(saveChanges);
		};

		_this.importParties = function () {
			var partyRowList = [].concat((0, _toConsumableArray3.default)(_this.state.validParties), (0, _toConsumableArray3.default)(_this.state.invalidParties));
			var NameLogic = require('../../../../BizLogic/nameLogic');
			var SettingCache = require('../../../../Cache/SettingCache');
			var PartyGroupCache = require('../../../../Cache/PartyGroupCache');
			var GSTHelper = require('../../../../Utilities/GSTHelper');
			var CustomerType = require('../../../../Constants/NameCustomerType');
			var NameType = require('../../../../Constants/NameType');
			var MyDate = require('../../../../Utilities/MyDate');
			var settingCache = new SettingCache();
			var partyGroupCache = new PartyGroupCache();
			var isCountryIndia = settingCache.isCurrentCountryIndia();
			var isGST = settingCache.getGSTEnabled();

			var PartyFieldHeaders = _ImportPartiesXL2.default.PartyFieldHeaders;
			var partyObjs = [];
			var partyGroupMap = (0, _create2.default)(null);
			partyRowList.forEach(function (partyRow) {
				if (partyRow.isRowValid) {
					var partyName = partyRow[PartyFieldHeaders.PARTY_NAME].value || '';
					var contactNo = partyRow[PartyFieldHeaders.CONTACT_NO].value || '';
					var email = partyRow[PartyFieldHeaders.EMAIL_ID].value || '';
					var address = partyRow[PartyFieldHeaders.ADDRESS].value || '';
					var openingBalance = partyRow[PartyFieldHeaders.OPENING_BALANCE].value || '';
					var openingDate = partyRow[PartyFieldHeaders.OPENING_DATE].value || '';
					var gstINNo = partyRow[PartyFieldHeaders.GSTIN_NO] && partyRow[PartyFieldHeaders.GSTIN_NO].value;
					var groupName = partyRow[PartyFieldHeaders.PARTY_GROUP] && partyRow[PartyFieldHeaders.PARTY_GROUP].value;
					var shippingAddress = partyRow[PartyFieldHeaders.SHIPPING_ADDRESS] && partyRow[PartyFieldHeaders.SHIPPING_ADDRESS].value;
					gstINNo = gstINNo ? gstINNo.trim() : '';
					groupName = groupName ? groupName.trim() : '';
					shippingAddress = shippingAddress ? shippingAddress.trim() : '';
					openingBalance = openingBalance.trim() || 0;
					openingDate = openingDate ? MyDate.convertStringToDateObject(openingDate) : MyDate.setTimeZero(new Date());
					partyName = partyName.trim();
					contactNo = contactNo.trim();
					email = email.trim();
					address = address.trim();

					var party = new NameLogic();
					party.setFullName(partyName);
					party.setCustomerType(CustomerType.UNREGISTERED);
					party.setNameType(NameType.NAME_TYPE_PARTY);
					party.setPhoneNumber(contactNo);
					party.setEmail(email);
					party.setAddress(address);
					party.setAmount(openingBalance);
					party.setOpeningDate(openingDate);

					party.setGstinNumber('');
					party.setTinNumber('');
					party.setContactState('');
					if (isCountryIndia && isGST) {
						if (gstINNo) {
							party.setGstinNumber(gstINNo);
							var state = GSTHelper.getGSTINState(gstINNo);
							party.setContactState(state);
							party.setCustomerType(CustomerType.REGISTERED_NORMAL);
						}
					} else {
						party.setTinNumber(gstINNo);
					}
					party.setGroupId('1');
					if (groupName) {
						var groupId = partyGroupCache.getPartyGroupId(groupName);
						party.setGroupId(groupId);
						partyGroupMap[party.getFullName()] = groupName;
					}
					party.setShippingAddress(shippingAddress);
					partyObjs.push(party);
				}
			});
			_ImportPartiesXL2.default.saveImportedParties({ partyObjs: partyObjs, partyGroupMap: partyGroupMap });
			_this.handleClose(true);
		};

		_this.state = {
			headerMappingRequired: false,
			validParties: null,
			invalidParties: null,
			enabledFields: _ImportPartiesXL2.default.getEnabledPartyFields(),
			excelFilePath: '',
			excelFields: null,
			open: true
		};
		return _this;
	}

	(0, _createClass3.default)(ImportParties, [{
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				_Dialog2.default,
				{
					fullScreen: true,
					open: this.state.open,
					TransitionComponent: Transition,
					id: 'importPartiesDialog'
				},
				_react2.default.createElement(
					'div',
					{ className: 'importPartiesDiv' },
					(this.state.uploadInProgress || !this.state.excelFilePath) && _react2.default.createElement(_ImportExcel2.default, {
						onClose: this.handleClose,
						downloadSample: this.handleDownloadSample,
						parseExcelData: this.readExcelFile
					}),
					this.state.headerMappingRequired && _react2.default.createElement(_MapExcelFields2.default, {
						enabledFields: this.state.enabledFields,
						excelFields: this.state.excelFields,
						onClose: this.handleClose,
						handleProceed: this.handleProceed,
						findMatch: this.findMatch
					}),
					this.state.showExcelData && _react2.default.createElement(_DisplayImportParties2.default, {
						validParties: this.state.validParties,
						invalidParties: this.state.invalidParties,
						partyNamesMap: this.state.partyNamesMap,
						partyCodesMap: this.state.partyCodesMap,
						partyFields: this.state.enabledFields,
						onClose: this.handleClose,
						importParties: this.importParties
					})
				)
			);
		}
	}]);
	return ImportParties;
}(_react.Component);

;

ImportParties.propTypes = {
	onClose: _propTypes2.default.func.isRequired
};

exports.default = (0, _styles.withStyles)(styles)(ImportParties);