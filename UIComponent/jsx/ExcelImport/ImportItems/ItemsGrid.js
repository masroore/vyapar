Object.defineProperty(exports, "__esModule", {
	value: true
});

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _ImportItemsXL = require('../../../../BizLogic/ImportItemsXL');

var _ImportItemsXL2 = _interopRequireDefault(_ImportItemsXL);

var _MountComponent = require('../../MountComponent');

var _MountComponent2 = _interopRequireDefault(_MountComponent);

var _Tooltip = require('@material-ui/core/Tooltip');

var _Tooltip2 = _interopRequireDefault(_Tooltip);

var _Typography = require('@material-ui/core/Typography');

var _Typography2 = _interopRequireDefault(_Typography);

var _styles = require('@material-ui/core/styles/');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint-disable react/prop-types */
var styles = {
	'@global': {
		'.itemGridCellIcon': {
			height: '100%',
			width: '100%',
			border: 0,
			borderRight: '1px solid #E9E9E9',
			backgroundColor: 'white'
		}
	}
};
var CustomToolTip = (0, _styles.withStyles)(function (theme) {
	return {
		tooltip: {
			backgroundColor: '#2B3B56',
			color: 'white',
			maxWidth: 220,
			fontSize: theme.typography.pxToRem(12),
			boxShadow: '0px 8px 12px #FF0000',
			opacity: '100%'
		}
	};
})(_Tooltip2.default);

var ItemsGrid = function (_React$Component) {
	(0, _inherits3.default)(ItemsGrid, _React$Component);

	function ItemsGrid(props) {
		(0, _classCallCheck3.default)(this, ItemsGrid);

		var _this = (0, _possibleConstructorReturn3.default)(this, (ItemsGrid.__proto__ || (0, _getPrototypeOf2.default)(ItemsGrid)).call(this, props));

		_this.getApi = function (params) {
			_this.api = params.api;
			_this.api.setRowData(_this.props.itemList);
		};

		_this.state = {};
		return _this;
	}

	(0, _createClass3.default)(ItemsGrid, [{
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(nextProps) {
			if (this.api && nextProps.refreshGrid) {
				this.api.setRowData(nextProps.itemList);
			}
			return false;
		}
	}, {
		key: 'render',
		value: function render() {
			var Grid = require('../../Grid').default;
			var enabledFields = this.props.context.enabledFields;
			var columnDefs = getColumnDefs(enabledFields);
			var gridOptions = {
				columnDefs: columnDefs,
				defaultColDef: {
					suppressKeyboardEvent: suppressNavigation
				},
				rowData: [],
				rowClass: 'importitems-grid-row',
				cellClass: 'importitems-grid-cell',
				rowHeight: 36,
				headerHeight: 36,
				suppressAutoSize: true,
				suppressMovableColumns: true,
				suppressContextMenu: true,
				context: this.props.context
			};
			return _react2.default.createElement(Grid, {
				notSelectFirstRow: true,
				height: '100%',
				width: '100%',
				getApi: this.getApi,
				avoidDefaultTheme: true,
				gridOptions: gridOptions
			});
		}
	}]);
	return ItemsGrid;
}(_react2.default.Component);

var InputText = function (_Component) {
	(0, _inherits3.default)(InputText, _Component);

	function InputText(props) {
		(0, _classCallCheck3.default)(this, InputText);

		var _this2 = (0, _possibleConstructorReturn3.default)(this, (InputText.__proto__ || (0, _getPrototypeOf2.default)(InputText)).call(this, props));

		_this2.handleChange = function (event) {
			var newVal = event.currentTarget.value;
			var _this2$props = _this2.props,
			    rowIndex = _this2$props.rowIndex,
			    column = _this2$props.column,
			    context = _this2$props.context,
			    api = _this2$props.api;

			context.onItemUpdate({
				newVal: newVal,
				index: rowIndex,
				field: column.colId
			});
			api.refreshCells({
				rowNodes: [_this2.props.node],
				force: true
			});
		};

		_this2.handleBlur = function () {
			var _this2$props2 = _this2.props,
			    column = _this2$props2.column,
			    context = _this2$props2.context,
			    api = _this2$props2.api,
			    data = _this2$props2.data;

			context.onBlurAfterUpdate({
				shouldRefresh: _this2.state.isRowValidBeforeFocus != data.isRowValid,
				field: column.colId
			});
			api.refreshCells({
				force: true
			});
		};

		_this2.handleFocus = function () {
			_this2.setState({
				isRowValidBeforeFocus: _this2.props.data.isRowValid
			});
		};

		_this2.state = (0, _extends3.default)({}, _this2.props.value);
		return _this2;
	}

	(0, _createClass3.default)(InputText, [{
		key: 'refresh',
		value: function refresh(params) {
			this.setState((0, _extends3.default)({}, params ? params.value : this.props.value));
			return true;
		}
	}, {
		key: 'render',
		value: function render() {
			var _state = this.state,
			    error = _state.error,
			    isValid = _state.isValid,
			    value = _state.value;

			return _react2.default.createElement(
				CustomToolTip,
				{ disableHoverListener: isValid, disableFocusListener: true, disableTouchListener: true,
					title: !isValid ? _react2.default.createElement(
						_react2.default.Fragment,
						null,
						_react2.default.createElement(
							_Typography2.default,
							{ color: 'inherit' },
							error.message
						)
					) : ''
				},
				_react2.default.createElement('input', { type: 'text', className: 'itemGridCell ' + (isValid ? '' : 'cell-error'),
					onChange: this.handleChange,
					value: value,
					onBlur: this.handleBlur,
					onFocus: this.handleFocus,
					disabled: this.props.context && !this.props.context.allowEdit
				})
			);
		}
	}]);
	return InputText;
}(_react.Component);

function InputTextRenderer() {}

// init method gets the details of the cell to be rendere
InputTextRenderer.prototype.init = function (params) {
	this.eGui = document.createElement('div');
	this.eGui.className = 'height100percent';

	// this.params = params;
	// this.isRowValidBeforeFocus = this.params.data.isRowValid;
	// console.log(this.params);
	// const {value: column} = this.params;
	// const {value, isValid, error} = column;
	// this.eGui = document.createElement('div');
	// this.eGui.innerHTML = `
	// 			<input
	// 			type="text"
	// 			value="${value}"
	// 			autoFocus
	// 			class="itemGridCell ${isValid ? '' : 'cell-error'}"
	// 			/>
	// `;
	// const input = this.eGui.querySelector('input');
	// const handleChange = (event) => {
	// 	let newVal = event.currentTarget.value;
	// 	let { rowIndex, column, context, api, node, value: {isValid} } = this.params;
	// 	context.onItemUpdate({
	// 		newVal,
	// 		index: rowIndex,
	// 		field: column.colId
	// 	});
	// 	api.refreshCells({
	// 		rowNodes: [node],
	// 		force: true
	// 	});
	// 	setTimeout(() => {
	// 		input.setAttribute('class', `itemGridCell ${this.params.value.isValid ? '' : 'cell-error'}`);
	// 	});
	// };
	// const handleBlur = () => {
	// 	let { column, context, api, data } = this.params;
	// 	context.onBlurAfterUpdate({
	// 		shouldRefresh: this.isRowValidBeforeFocus != data.isRowValid,
	// 		field: column.colId
	// 	});
	// 	api.refreshCells({
	// 		force: true
	// 	});
	// };
	// const handleFocus = () => {
	// 	let { value: {isValid} } = this.params;
	// 	this.isRowValidBeforeFocus = this.params.data.isRowValid;
	// 	setTimeout(() => {
	// 		input.setAttribute('class', `itemGridCell ${this.params.value.isValid ? '' : 'cell-error'}`);
	// 	});
	// };
	// input.addEventListener('keyup', handleChange);
	// input.addEventListener('blur', handleBlur);
	// input.addEventListener('focus', handleFocus);

	this.componentRef = _reactDom2.default.render(_react2.default.createElement(InputText, params), this.eGui);
};

InputTextRenderer.prototype.getGui = function () {
	return this.eGui;
};

InputTextRenderer.prototype.refresh = function (params) {
	this.componentRef.refresh(params);
	this.params = params;
	return true;
};

var SelectBox = function (_Component2) {
	(0, _inherits3.default)(SelectBox, _Component2);

	function SelectBox() {
		var _ref;

		var _temp, _this3, _ret;

		(0, _classCallCheck3.default)(this, SelectBox);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this3 = (0, _possibleConstructorReturn3.default)(this, (_ref = SelectBox.__proto__ || (0, _getPrototypeOf2.default)(SelectBox)).call.apply(_ref, [this].concat(args))), _this3), _this3.handleChange = function (event) {
			var newVal = event.currentTarget.value;
			var _this3$props = _this3.props,
			    rowIndex = _this3$props.rowIndex,
			    column = _this3$props.column,
			    context = _this3$props.context,
			    api = _this3$props.api;

			context.onItemUpdate({
				newVal: newVal,
				index: rowIndex,
				field: column.colId
			});
			var itemField = _this3.props.value;
			_this3.setState({
				value: itemField.value
			});
			api.refreshCells({
				rowNodes: [_this3.props.node]
			});
		}, _temp), (0, _possibleConstructorReturn3.default)(_this3, _ret);
	}

	(0, _createClass3.default)(SelectBox, [{
		key: 'render',
		value: function render() {
			var itemField = this.props.value;
			var options = this.props.options || [''];
			var selectedItem = itemField.value || this.props.defaultValue;
			return _react2.default.createElement(
				'select',
				{ value: selectedItem, disabled: this.props.context && !this.props.context.allowEdit, onChange: this.handleChange, className: 'itemGridCell' },
				options.map(function (value, index) {
					return _react2.default.createElement(
						'option',
						{ key: index, value: value },
						value
					);
				})
			);
		}
	}]);
	return SelectBox;
}(_react.Component);

function SelectBoxRenderer() {}

// init method gets the details of the cell to be rendere
SelectBoxRenderer.prototype.init = function (params) {
	this.eGui = document.createElement('div');
	this.eGui.className = 'height100percent';
	(0, _MountComponent2.default)(SelectBox, this.eGui, params);
};

SelectBoxRenderer.prototype.getGui = function () {
	return this.eGui;
};

var IsValidComponent = function (_Component3) {
	(0, _inherits3.default)(IsValidComponent, _Component3);

	function IsValidComponent() {
		(0, _classCallCheck3.default)(this, IsValidComponent);
		return (0, _possibleConstructorReturn3.default)(this, (IsValidComponent.__proto__ || (0, _getPrototypeOf2.default)(IsValidComponent)).apply(this, arguments));
	}

	(0, _createClass3.default)(IsValidComponent, [{
		key: 'render',
		value: function render() {
			var isRowValid = this.props.value;
			var status = isRowValid ? 'valid' : 'error';
			return _react2.default.createElement(
				'div',
				{ className: 'itemGridCellIcon' },
				_react2.default.createElement('div', { className: 'import-item-icon ' + status })
			);
		}
	}]);
	return IsValidComponent;
}(_react.Component);

function IsValidComponentRenderer() {}

// init method gets the details of the cell to be rendere
IsValidComponentRenderer.prototype.init = function (params) {
	this.eGui = document.createElement('div');
	(0, _MountComponent2.default)(IsValidComponent, this.eGui, params);
};

IsValidComponentRenderer.prototype.getGui = function () {
	return this.eGui;
};

function getColumnDefs(enabledFields) {
	var ItemFieldHeaders = _ImportItemsXL2.default.ItemFieldHeaders;
	var columnDefs = [{
		field: 'isRowValid',
		headerName: '',
		cellRenderer: IsValidComponentRenderer,
		width: 60
	}, {
		field: ItemFieldHeaders.ITEM_NAME,
		headerName: ItemFieldHeaders.ITEM_NAME,
		headerTooltip: ItemFieldHeaders.ITEM_NAME,
		cellRenderer: InputTextRenderer
	}];
	if (enabledFields.includes(ItemFieldHeaders.ITEM_CODE)) {
		columnDefs.push({
			field: ItemFieldHeaders.ITEM_CODE,
			headerName: ItemFieldHeaders.ITEM_CODE,
			headerTooltip: ItemFieldHeaders.ITEM_CODE,
			cellRenderer: InputTextRenderer
		});
	}
	if (enabledFields.includes(ItemFieldHeaders.ITEM_DESCRIPTION)) {
		columnDefs.push({
			field: ItemFieldHeaders.ITEM_DESCRIPTION,
			headerName: ItemFieldHeaders.ITEM_DESCRIPTION,
			headerTooltip: ItemFieldHeaders.ITEM_DESCRIPTION,
			cellRenderer: InputTextRenderer
		});
	}
	if (enabledFields.includes(ItemFieldHeaders.ITEM_CATEGORY)) {
		columnDefs.push({
			field: ItemFieldHeaders.ITEM_CATEGORY,
			headerName: ItemFieldHeaders.ITEM_CATEGORY,
			headerTooltip: ItemFieldHeaders.ITEM_CATEGORY,
			cellRenderer: InputTextRenderer
		});
	}
	if (enabledFields.includes(ItemFieldHeaders.HSN)) {
		columnDefs.push({
			field: ItemFieldHeaders.HSN,
			headerName: ItemFieldHeaders.HSN,
			headerTooltip: ItemFieldHeaders.HSN,
			cellRenderer: InputTextRenderer
		});
	}
	if (enabledFields.includes(ItemFieldHeaders.SALE_PRICE)) {
		columnDefs.push({
			field: ItemFieldHeaders.SALE_PRICE,
			headerName: ItemFieldHeaders.SALE_PRICE,
			headerTooltip: ItemFieldHeaders.SALE_PRICE,
			cellRenderer: InputTextRenderer
		});
	}
	if (enabledFields.includes(ItemFieldHeaders.PURCHASE_PRICE)) {
		columnDefs.push({
			field: ItemFieldHeaders.PURCHASE_PRICE,
			headerName: ItemFieldHeaders.PURCHASE_PRICE,
			headerTooltip: ItemFieldHeaders.PURCHASE_PRICE,
			cellRenderer: InputTextRenderer
		});
	}
	if (enabledFields.includes(ItemFieldHeaders.OPENING_STOCK_QUANTITY)) {
		columnDefs.push({
			field: ItemFieldHeaders.OPENING_STOCK_QUANTITY,
			headerName: ItemFieldHeaders.OPENING_STOCK_QUANTITY,
			headerTooltip: ItemFieldHeaders.OPENING_STOCK_QUANTITY,
			cellRenderer: InputTextRenderer
		});
	}
	if (enabledFields.includes(ItemFieldHeaders.MINIMUM_STOCK_QUANTITY)) {
		columnDefs.push({
			field: ItemFieldHeaders.MINIMUM_STOCK_QUANTITY,
			headerName: ItemFieldHeaders.MINIMUM_STOCK_QUANTITY,
			headerTooltip: ItemFieldHeaders.MINIMUM_STOCK_QUANTITY,
			cellRenderer: InputTextRenderer
		});
	}
	if (enabledFields.includes(ItemFieldHeaders.ITEM_LOCATION)) {
		columnDefs.push({
			field: ItemFieldHeaders.ITEM_LOCATION,
			headerName: ItemFieldHeaders.ITEM_LOCATION,
			headerTooltip: ItemFieldHeaders.ITEM_LOCATION,
			cellRenderer: InputTextRenderer
		});
	}
	if (enabledFields.includes(ItemFieldHeaders.TAX_RATE)) {
		var TaxCodeCache = require('../../../../Cache/TaxCodeCache');
		var taxCodeCache = new TaxCodeCache();
		var taxRates = [''].concat((0, _toConsumableArray3.default)(taxCodeCache.getTaxListForTransaction().map(function (obj) {
			return obj.getTaxCodeName();
		})));
		columnDefs.push({
			field: ItemFieldHeaders.TAX_RATE,
			headerName: ItemFieldHeaders.TAX_RATE,
			headerTooltip: ItemFieldHeaders.TAX_RATE,
			cellRenderer: SelectBoxRenderer,
			cellRendererParams: { options: taxRates, defaultValue: '' }
		});
	}
	if (enabledFields.includes(ItemFieldHeaders.INCLUSIVE_OF_TAX)) {
		columnDefs.push({
			field: ItemFieldHeaders.INCLUSIVE_OF_TAX,
			headerName: ItemFieldHeaders.INCLUSIVE_OF_TAX,
			headerTooltip: ItemFieldHeaders.INCLUSIVE_OF_TAX,
			cellRenderer: SelectBoxRenderer,
			cellRendererParams: { options: ['Inclusive', 'Exclusive'], defaultValue: 'Exclusive' }
		});
	}
	if (enabledFields.includes(ItemFieldHeaders.BASE_UNIT)) {
		columnDefs.push({
			field: ItemFieldHeaders.BASE_UNIT,
			headerName: ItemFieldHeaders.BASE_UNIT,
			headerTooltip: ItemFieldHeaders.BASE_UNIT,
			cellRenderer: InputTextRenderer
		});
	}
	if (enabledFields.includes(ItemFieldHeaders.SECONDARY_UNIT)) {
		columnDefs.push({
			field: ItemFieldHeaders.SECONDARY_UNIT,
			headerName: ItemFieldHeaders.SECONDARY_UNIT,
			headerTooltip: ItemFieldHeaders.SECONDARY_UNIT,
			cellRenderer: InputTextRenderer
		});
	}
	if (enabledFields.includes(ItemFieldHeaders.CONVERSION_RATE)) {
		columnDefs.push({
			field: ItemFieldHeaders.CONVERSION_RATE,
			headerName: ItemFieldHeaders.CONVERSION_RATE,
			headerTooltip: ItemFieldHeaders.CONVERSION_RATE,
			cellRenderer: InputTextRenderer
		});
	}

	return columnDefs;
}

function suppressNavigation(params) {
	var KEY_TAB = 9;
	var KEY_LEFT = 37;
	var KEY_UP = 38;
	var KEY_RIGHT = 39;
	var KEY_DOWN = 40;

	var event = params.event;
	var key = event.which;
	var keysToSuppress = [KEY_TAB, KEY_LEFT, KEY_UP, KEY_RIGHT, KEY_DOWN];

	var suppress = keysToSuppress.indexOf(key) >= 0;
	return suppress;
}

exports.default = (0, _styles.withStyles)(styles)(ItemsGrid);