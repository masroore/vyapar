Object.defineProperty(exports, "__esModule", {
  value: true
});

var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

var _assign = require('babel-runtime/core-js/object/assign');

var _assign2 = _interopRequireDefault(_assign);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _ItemsGrid = require('./ItemsGrid');

var _ItemsGrid2 = _interopRequireDefault(_ItemsGrid);

var _ImportItemsXL = require('../../../../BizLogic/ImportItemsXL');

var _ImportItemsXL2 = _interopRequireDefault(_ImportItemsXL);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/core/styles');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint-disable no-mixed-spaces-and-tabs */
var StringConstants = require('../../../../Constants/StringConstants');
var styles = {
  '@global': {
    '.importitem-div': {
      fontSize: '14px'
    },
    '.importitem-div-header': {
      borderBottom: '1px solid #DDDDDD',
      width: '100%',
      paddingLeft: '1.5rem'
    },
    '.importitem-tab': {
      display: 'inline-block',
      paddingRight: '1.5rem',
      backgroundColor: 'white',
      height: '100%',
      cursor: 'pointer',
      lineHeight: '2rem',
      marginRight: '2rem'
    },
    '.importitem-tab>span': {
      fontWeight: '500'
    },
    '.importitem-tab.active': {
      color: '#1789FC',
      borderBottom: '2px solid #1789FC'
    },
    '.import-item-icon': {
      display: 'inline-block',
      width: '1.5rem',
      height: '1.5rem',
      verticalAlign: 'middle',
      backgroundRepeat: 'no-repeat',
      marginRight: '.25rem'
    },
    '.import-item-icon.error': {
      backgroundImage: 'url(\'../inlineSVG/warning-icon.svg\')'
    },
    '.import-item-icon.valid': {
      backgroundImage: 'url(\'../inlineSVG/green-tick-circle.svg\')'
    },
    '.importitem-div-body': {
      height: 'calc(100% - 3rem)'
    },
    '.importitem-div-body-label': {
      margin: '.75rem',
      marginLeft: '2rem',
      fontWeight: 'bold'
    },
    '.importItemGrid': {
      height: 'calc(100% - 3rem)'
    },
    '.importItemGrid .gridContainer': {
      height: '100%',
      color: '#2B4C56',
      lineHeight: '14px',
      fontSize: '12px'
    },
    '.importitems-grid-row.ag-row-even': {
      backgroundColor: '#F2F4F8'
    },
    '.itemGridCell': {
      height: '100%',
      width: '100%',
      border: 0,
      borderRight: '1px solid #E9E9E9',
      backgroundColor: 'inherit',
      '-webkit-appearance': 'none',
      paddingLeft: '.5rem'
    },

    '.itemGridCell.cell-error': {
      border: '1px solid white',
      borderRadius: '4px',
      backgroundColor: '#F6B0B0'
    },

    '.itemGridCell.cell-corrected': {
      border: '1px dashed #14C059',
      borderRadius: '4px'
    },
    '.importitems-grid-row .ag-react-container': {
      height: '100%',
      width: '100%'
    },
    '.itemGridCellIcon>.import-item-icon': {
      position: 'absolute',
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)'
    },
    '.importItemGrid.makeRowsReadOnly .importitems-grid-row': {
      pointerEvents: 'none'
    },
    '.importItemGrid .ag-header-cell:not(:first-child)': {
      lineHeight: '36px',
      padding: '0 1rem',
      border: '1px solid #E9E9E9'
    }
  }
};

var DisplayImportItems = function (_Component) {
  (0, _inherits3.default)(DisplayImportItems, _Component);

  function DisplayImportItems(props) {
    (0, _classCallCheck3.default)(this, DisplayImportItems);

    var _this = (0, _possibleConstructorReturn3.default)(this, (DisplayImportItems.__proto__ || (0, _getPrototypeOf2.default)(DisplayImportItems)).call(this, props));

    _initialiseProps.call(_this);

    var _this$props = _this.props,
        validItems = _this$props.validItems,
        invalidItems = _this$props.invalidItems,
        itemNamesMap = _this$props.itemNamesMap,
        itemCodesMap = _this$props.itemCodesMap;

    _this.refreshGrid = true;
    _this.state = {
      isErrorTabActive: invalidItems.length > 0,
      validItemCount: validItems.length,
      invalidItemCount: invalidItems.length,
      currentList: invalidItems.length ? invalidItems : validItems,
      itemNamesMap: itemNamesMap,
      itemCodesMap: itemCodesMap
    };
    _this.importItemsBtn = _react2.default.createRef();
    return _this;
  }

  (0, _createClass3.default)(DisplayImportItems, [{
    key: 'render',
    value: function render() {
      var refreshGrid = this.refreshGrid;
      this.refreshGrid = false;
      console.log(this.state.currentList, 'current list');
      return _react2.default.createElement(
        'div',
        { className: 'whitediv' },
        _react2.default.createElement(
          'div',
          { className: 'whitediv-header' },
          _react2.default.createElement(
            'h3',
            null,
            'Import Items'
          ),
          _react2.default.createElement('img', { src: '../inlineSVG/round_close.svg', onClick: this.props.onClose })
        ),
        _react2.default.createElement(
          'div',
          { className: 'whitediv-body importitem-div' },
          _react2.default.createElement(
            'div',
            { className: 'importitem-div-header' },
            _react2.default.createElement(
              'div',
              {
                className: 'importitem-tab ' + (this.state.isErrorTabActive ? '' : ' active'),
                onClick: this.clickValidItemsTab },
              _react2.default.createElement('div', { className: 'import-item-icon valid' }),
              _react2.default.createElement(
                'span',
                null,
                'Valid Items : ' + this.state.validItemCount
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'importitem-tab ' + (this.state.isErrorTabActive ? ' active' : ''),
                onClick: this.clickInvalidItemsTab },
              _react2.default.createElement('div', { className: 'import-item-icon error' }),
              _react2.default.createElement(
                'span',
                null,
                'Items with Errors : ' + this.state.invalidItemCount
              )
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'importitem-div-body' },
            _react2.default.createElement(
              'div',
              { className: 'importitem-div-body-label' },
              this.state.isErrorTabActive ? 'Items with Errors' : 'Valid Items'
            ),
            _react2.default.createElement(
              'div',
              { className: 'importItemGrid ' + (this.state.isErrorTabActive ? '' : 'makeRowsReadOnly') },
              _react2.default.createElement(_ItemsGrid2.default, {
                itemList: this.state.currentList,
                refreshGrid: refreshGrid,
                context: {
                  onItemUpdate: this.onItemUpdate,
                  enabledFields: this.props.itemFields,
                  onBlurAfterUpdate: this.onBlurAfterUpdate,
                  allowEdit: this.state.isErrorTabActive
                }
              })
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'whitediv-footer' },
          _react2.default.createElement(
            'button',
            { disabled: !this.state.validItemCount,
              ref: this.importItemsBtn,
              onClick: this.clickImportItemsBtn,
              className: 'blue-button floatRight mr-20' },
            'Import ' + this.state.validItemCount + ' Valid Items'
          )
        )
      );
    }
  }]);
  return DisplayImportItems;
}(_react.Component);

var _initialiseProps = function _initialiseProps() {
  var _this2 = this;

  this.clickValidItemsTab = function () {
    _this2.refreshGrid = true;
    _this2.setState({
      isErrorTabActive: false,
      currentList: _this2.props.validItems
    });
  };

  this.clickInvalidItemsTab = function () {
    _this2.refreshGrid = true;
    _this2.setState({
      isErrorTabActive: true,
      currentList: _this2.props.invalidItems
    });
  };

  this.clickImportItemsBtn = function () {
    _this2.importItemsBtn.current.disabled = true;
    _this2.props.importItems();
  };

  this.onItemUpdate = function (obj) {
    var index = obj.index,
        field = obj.field,
        newVal = obj.newVal;
    var _state = _this2.state,
        currentList = _state.currentList,
        validItemCount = _state.validItemCount,
        invalidItemCount = _state.invalidItemCount;

    var itemRow = currentList[index];
    var currFieldValidState = itemRow[field].isValid;
    var validatedRes = _ImportItemsXL2.default.ItemDefinations[field].validateAndSetValue(newVal);
    var tempFieldValidState = !(validatedRes instanceof Error);

    if (currFieldValidState && !tempFieldValidState) {
      return; // Prevent entering invalid characters in already valid fields;
    }
    var prevRowValidState = itemRow.isRowValid;
    itemRow[field].value = newVal;
    _this2.validateItemRow(itemRow, index);
    if (prevRowValidState !== itemRow.isRowValid) {
      if (itemRow.isRowValid) {
        validItemCount++;
        invalidItemCount--;
      } else {
        validItemCount--;
        invalidItemCount++;
      }
    }
    _this2.setState({ currentList: currentList, validItemCount: validItemCount, invalidItemCount: invalidItemCount });
  };

  this.onBlurAfterUpdate = function (obj) {
    var shouldRefresh = obj.shouldRefresh,
        field = obj.field;

    if (shouldRefresh || field == _ImportItemsXL2.default.ItemFieldHeaders.ITEM_NAME || field == _ImportItemsXL2.default.ItemFieldHeaders.ITEM_CODE) {
      _this2.refreshItemList();
    }
  };

  this.validateItemRow = function (itemRow, index) {
    var itemFields = _this2.props.itemFields;
    var _state2 = _this2.state,
        itemNamesMap = _state2.itemNamesMap,
        itemCodesMap = _state2.itemCodesMap;

    itemRow.isRowValid = true; // resetting.
    itemFields.forEach(function (itemField) {
      switch (itemField) {
        case _ImportItemsXL2.default.ItemFieldHeaders.ITEM_NAME:
          var itemName = itemRow[itemField].value ? itemRow[itemField].value.trim() : '';
          if (!itemName) {
            itemRow[itemField].isValid = false;
            var error = new Error(StringConstants.IMPORT_ITEM_NAME_EMPTY);
            itemRow[itemField].error = error;
            itemRow[itemField].value = '';
          } else if (itemNamesMap.has(itemName.toLowerCase()) && index != itemNamesMap.get(itemName.toLowerCase())) {
            itemRow[itemField].isValid = false;
            var _error = new Error(StringConstants.IMPORT_ITEM_NAME_DUPLICATE);
            itemRow[itemField].error = _error;
          } else {
            itemRow[itemField].isValid = true;
            itemRow[itemField].error = '';
          }
          itemRow.isRowValid = itemRow.isRowValid && itemRow[itemField].isValid;
          break;
        case _ImportItemsXL2.default.ItemFieldHeaders.ITEM_CODE:
          var itemCode = itemRow[itemField].value ? itemRow[itemField].value.trim() : '';
          if (itemCodesMap.has(itemCode.toLowerCase()) && index != itemCodesMap.get(itemCode.toLowerCase())) {
            itemRow[itemField].isValid = false;
            var _error2 = new Error(StringConstants.IMPORT_ITEM_CODE_DUPLICATE);
            itemRow[itemField].error = _error2;
          } else {
            itemRow[itemField].isValid = true;
            itemRow[itemField].error = '';
          }
          itemRow.isRowValid = itemRow.isRowValid && itemRow[itemField].isValid;
          break;
        case _ImportItemsXL2.default.ItemFieldHeaders.BASE_UNIT:
          break; // preventing default
        case _ImportItemsXL2.default.ItemFieldHeaders.SECONDARY_UNIT:
          break; // preventing default
        case _ImportItemsXL2.default.ItemFieldHeaders.CONVERSION_RATE:
          var baseUnit = itemRow[_ImportItemsXL2.default.ItemFieldHeaders.BASE_UNIT];
          var secondaryUnit = itemRow[_ImportItemsXL2.default.ItemFieldHeaders.SECONDARY_UNIT];
          var conversionRate = itemRow[_ImportItemsXL2.default.ItemFieldHeaders.CONVERSION_RATE];
          // reset values
          (0, _assign2.default)(secondaryUnit, {
            error: '',
            isValid: true
          });
          (0, _assign2.default)(conversionRate, {
            error: '',
            isValid: true
          });

          // if (conversionRate.value && !(baseUnit.value && secondaryUnit.value)) {
          //     let error = new Error('Provide Base Unit and Secondary Unit for conversion rate.');
          //     conversionRate.error = error;
          //     conversionRate.isValid = false;
          // }

          if (conversionRate.value) {
            var _validatedRes = _ImportItemsXL2.default.ItemDefinations[itemField].validateAndSetValue(conversionRate.value);
            conversionRate.isValid = !(_validatedRes instanceof Error);
            conversionRate.error = _validatedRes instanceof Error ? _validatedRes : '';
          } else {
            conversionRate.value = '';
          }

          if (secondaryUnit.value && !(baseUnit.value && conversionRate.value)) {
            var _error3 = new Error(StringConstants.IMPORT_ITEM_BASEUNIT_CONVERSIONRATE_EMPTY);
            secondaryUnit.error = _error3;
            secondaryUnit.isValid = false;
          }
          if (baseUnit.value && secondaryUnit.value && secondaryUnit.value.toLowerCase() === baseUnit.value.toLowerCase()) {
            var _error4 = new Error(StringConstants.IMPORT_ITEM_INVALID_SECONDARY_UNIT);
            secondaryUnit.error = _error4;
            secondaryUnit.isValid = false;
          }
          itemRow.isRowValid = itemRow.isRowValid && itemRow[_ImportItemsXL2.default.ItemFieldHeaders.SECONDARY_UNIT].isValid && itemRow[_ImportItemsXL2.default.ItemFieldHeaders.CONVERSION_RATE].isValid;
          break;
        default:
          var validatedRes = _ImportItemsXL2.default.ItemDefinations[itemField].validateAndSetValue(itemRow[itemField].value);
          itemRow[itemField].isValid = !(validatedRes instanceof Error);
          itemRow[itemField].error = validatedRes instanceof Error ? validatedRes : '';
          itemRow.isRowValid = itemRow.isRowValid && itemRow[itemField].isValid;
          break;
      }
    });
    return itemRow;
  };

  this.refreshItemList = function () {
    var itemList = _this2.state.currentList;
    var itemFields = _this2.props.itemFields;
    var _props = _this2.props,
        itemNamesMap = _props.itemNamesMap,
        itemCodesMap = _props.itemCodesMap;

    itemNamesMap = new _map2.default(itemNamesMap);
    itemCodesMap = new _map2.default(itemCodesMap);
    var validRowCount = 0;
    itemList.forEach(function (itemRow, index) {
      var itemNameExists = itemNamesMap.has(itemRow[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_NAME].value.trim().toLowerCase());
      var itemCodeExists = itemRow[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_CODE].value.trim().toLowerCase() && itemCodesMap.has(itemRow[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_CODE].value.trim().toLowerCase());
      if (!itemRow[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_NAME].value) {
        itemRow[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_NAME].isValid = false;
        var error = new Error(StringConstants.IMPORT_ITEM_NAME_EMPTY);
        itemRow[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_NAME].error = error;
      } else if (itemNameExists) {
        itemRow[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_NAME].isValid = false;
        itemRow[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_NAME].error = new Error(StringConstants.IMPORT_ITEM_NAME_DUPLICATE);
      } else {
        itemNamesMap.set(itemRow[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_NAME].value.trim().toLowerCase(), index);
        itemRow[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_NAME].isValid = true;
        itemRow[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_NAME].error = '';
      }
      if (itemCodeExists) {
        itemRow[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_CODE].isValid = false;
        itemRow[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_CODE].error = new Error(StringConstants.IMPORT_ITEM_CODE_DUPLICATE);
      } else {
        if (itemRow[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_CODE].value) {
          itemCodesMap.set(itemRow[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_CODE].value.trim().toLowerCase(), index);
        }
        itemRow[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_CODE].isValid = true;
        itemRow[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_CODE].error = '';
      }
      itemRow.isRowValid = itemFields.reduce(function (acc, field) {
        return acc && itemRow[field].isValid;
      }, true);
      if (itemRow.isRowValid) {
        itemNamesMap.set(itemRow[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_NAME].value.trim().toLowerCase(), index);
        itemRow[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_CODE].value.trim() && itemCodesMap.set(itemRow[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_CODE].value.trim().toLowerCase(), index);
      } else {
        if (itemRow[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_NAME].isValid) {
          itemNamesMap.delete(itemRow[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_NAME].value.trim().toLowerCase());
        }
        if (itemRow[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_CODE].isValid) {
          itemCodesMap.delete(itemRow[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_CODE].value.trim().toLowerCase());
        }
      }
      validRowCount += itemRow.isRowValid;
    });
    _this2.setState({
      currentList: itemList,
      validItemCount: _this2.props.validItems.length + validRowCount,
      invalidItemCount: _this2.props.invalidItems.length - validRowCount,
      itemNamesMap: itemNamesMap,
      itemCodesMap: itemCodesMap
    });
  };
};

;
DisplayImportItems.propTypes = {
  validItems: _propTypes2.default.array,
  invalidItems: _propTypes2.default.array,
  itemNamesMap: _propTypes2.default.object,
  itemCodesMap: _propTypes2.default.object,
  excelFilePath: _propTypes2.default.string,
  uploadInProgress: _propTypes2.default.bool,
  showExcelData: _propTypes2.default.bool,
  importItems: _propTypes2.default.func,
  itemFields: _propTypes2.default.array,
  onClose: _propTypes2.default.func.isRequired
};
exports.default = (0, _styles.withStyles)(styles)(DisplayImportItems);