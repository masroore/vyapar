Object.defineProperty(exports, "__esModule", {
	value: true
});

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _create = require('babel-runtime/core-js/object/create');

var _create2 = _interopRequireDefault(_create);

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _ImportExcel = require('./../ImportExcel');

var _ImportExcel2 = _interopRequireDefault(_ImportExcel);

var _MapExcelFields = require('./../MapExcelFields');

var _MapExcelFields2 = _interopRequireDefault(_MapExcelFields);

var _DisplayImportItems = require('./DisplayImportItems');

var _DisplayImportItems2 = _interopRequireDefault(_DisplayImportItems);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _ImportItemsXL = require('../../../../BizLogic/ImportItemsXL');

var _ImportItemsXL2 = _interopRequireDefault(_ImportItemsXL);

var _Dialog = require('@material-ui/core/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _Slide = require('@material-ui/core/Slide');

var _Slide2 = _interopRequireDefault(_Slide);

var _styles = require('@material-ui/core/styles/');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _require = require('electron'),
    ipcRenderer = _require.ipcRenderer;

var styles = {
	'@global': {
		'.importItemsDiv': {
			height: '100%'
		},
		'.whitediv': {
			height: '100%',
			paddingTop: 'var(--titlebar-height)',
			backgroundColor: 'white',
			minHeight: 600,
			minWidth: 800,
			'&>*': {
				width: '100%'
			},
			'&-header': {
				position: 'relative',
				height: '3rem',
				'&>*': {
					position: 'absolute',
					top: '50%',
					transform: 'translateY(-50%)'
				},
				'&>:first-child': {
					left: '2rem'
				},
				'&>:last-child': {
					right: '1rem'
				},
				'&:before': {
					content: '""',
					position: 'fixed',
					top: '-10px',
					left: 0,
					width: '100%',
					height: '10px',
					boxShadow: '0px 0px 8px rgba(0,0,0,.8)'
				}
			},
			'&-body': {
				margin: 'auto 0',
				height: 'calc(100% - 6.5rem)',
				overflow: 'auto'
			},
			'&-footer': {
				height: '3.5rem'
			}
		},
		'.blue-button': {
			backgroundColor: '#1789FC',
			color: 'white',
			borderRadius: 8,
			boxShadow: '0px 3px 6px #000000',
			height: '2.5rem',
			width: '9rem',
			fontSize: 14,
			'&:disabled': {
				backgroundColor: '#9e9e9e',
				cursor: 'default',
				boxShadow: 'none'
			}
		}
	}
};
function Transition(props) {
	return _react2.default.createElement(_Slide2.default, (0, _extends3.default)({ direction: 'up' }, props));
}

var ImportItems = function (_Component) {
	(0, _inherits3.default)(ImportItems, _Component);

	function ImportItems(props) {
		(0, _classCallCheck3.default)(this, ImportItems);

		var _this = (0, _possibleConstructorReturn3.default)(this, (ImportItems.__proto__ || (0, _getPrototypeOf2.default)(ImportItems)).call(this, props));

		_this.handleEsc = function () {
			var _require2 = require('../../../../Constants/Messages'),
			    discardChanges = _require2.discardChanges;

			var retVal = true;
			retVal = !_this.state.excelFilePath || confirm(discardChanges);
			if (retVal) {
				_this.handleClose();
			}
			return retVal;
		};

		_this.handleDownloadSample = function () {
			_ImportItemsXL2.default.downloadXLFile();
		};

		_this.readExcelFile = function (fileName) {
			if (fileName && (fileName.endsWith('.xls') || fileName.endsWith('.xlsx'))) {
				try {
					var XLSX = require('xlsx');
					return new _promise2.default(function (resolve) {
						_this.setState({
							excelFilePath: fileName,
							uploadInProgress: true
						});
						setTimeout(function () {
							var workbook = XLSX.readFileSync(fileName);
							var sheetName = workbook['SheetNames'][0];
							var sheets = workbook['Sheets'][sheetName];
							sheets['!ref'] = { s: { c: 0, r: 0 }, e: { c: 20, r: 20000 } };
							var excelData = XLSX.utils.sheet_to_json(sheets, { header: 1, blankrows: false });
							resolve(excelData);
						}, 1000);
					}).then(function (excelData) {
						if (!excelData.length) {
							var dialog = require('electron').remote.dialog;

							dialog.showMessageBox({
								type: 'info',
								title: 'Blank excel sheet',
								message: 'Please download the template excel file and enter data to continue.'
							});
							_this.setState({
								headerMappingRequired: false,
								validItems: null,
								invalidItems: null,
								excelFilePath: '',
								excelFields: null
							});
							return;
						}
						var headerData = excelData.shift();
						var headersValid = _ImportItemsXL2.default.validateHeaders(headerData, _this.state.enabledFields);
						if (!headersValid) {
							_this.setState({
								headerMappingRequired: true,
								excelFilePath: fileName,
								excelFields: headerData,
								uploadInProgress: false
							});
							_this.excelData = excelData;
						} else {
							var _require3 = require('../../../../Constants/StringConstants'),
							    SAMPLE_EXCEL_FOOTER_MESSAGE = _require3.SAMPLE_EXCEL_FOOTER_MESSAGE;

							var mappedData = [];
							excelData.forEach(function (itemRow) {
								var item = (0, _create2.default)(null);
								_this.state.enabledFields.forEach(function (field, index) {
									var value = itemRow[index] || '';
									value = ('' + value).trim();
									item[field] = { value: value };
								});
								var isEmptyRow = (0, _values2.default)(item).map(function (obj) {
									return obj.value;
								}).join('') == '' || item[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_NAME].value == SAMPLE_EXCEL_FOOTER_MESSAGE;
								isEmptyRow || mappedData.push(item);
							});

							var _ImportItemsXL$parseE = _ImportItemsXL2.default.parseExcelData(mappedData, _this.state.enabledFields),
							    validItems = _ImportItemsXL$parseE.validItems,
							    invalidItems = _ImportItemsXL$parseE.invalidItems,
							    itemCodesMap = _ImportItemsXL$parseE.itemCodesMap,
							    itemNamesMap = _ImportItemsXL$parseE.itemNamesMap;

							_this.setState({
								validItems: validItems,
								invalidItems: invalidItems,
								itemNamesMap: itemNamesMap,
								itemCodesMap: itemCodesMap,
								excelFilePath: fileName,
								uploadInProgress: false,
								showExcelData: true
							});
						}
					}).catch(function (err) {
						var logger = require('../../../../Utilities/logger');
						logger.error(err);
					});
				} catch (err) {
					var logger = require('../../../../Utilities/logger');
					logger.error(err);
				}
			}
		};

		_this.handleProceed = function (excelFieldMap) {
			var _require4 = require('../../../../Constants/StringConstants'),
			    SAMPLE_EXCEL_FOOTER_MESSAGE = _require4.SAMPLE_EXCEL_FOOTER_MESSAGE;

			var excelData = _this.excelData;
			var mappedData = [];
			excelData.map(function (itemRow) {
				var item = (0, _create2.default)(null);
				_this.state.enabledFields.forEach(function (field) {
					var value = itemRow[excelFieldMap[field]] || '';
					value = ('' + value).trim();
					item[field] = { value: value };
				});
				var isEmptyRow = (0, _values2.default)(item).map(function (obj) {
					return obj.value;
				}).join('') == '' || item[_ImportItemsXL2.default.ItemFieldHeaders.ITEM_NAME].value == SAMPLE_EXCEL_FOOTER_MESSAGE;
				isEmptyRow || mappedData.push(item);
			});

			var _ImportItemsXL$parseE2 = _ImportItemsXL2.default.parseExcelData(mappedData, _this.state.enabledFields),
			    validItems = _ImportItemsXL$parseE2.validItems,
			    invalidItems = _ImportItemsXL$parseE2.invalidItems,
			    itemCodesMap = _ImportItemsXL$parseE2.itemCodesMap,
			    itemNamesMap = _ImportItemsXL$parseE2.itemNamesMap;

			_this.setState({
				headerMappingRequired: false,
				showExcelData: true,
				validItems: validItems,
				invalidItems: invalidItems,
				itemCodesMap: itemCodesMap,
				itemNamesMap: itemNamesMap
			});
		};

		_this.findMatch = function (enabledField) {
			var ItemDefinations = _ImportItemsXL2.default.ItemDefinations;

			var excelFields = _this.state.excelFields;
			var match = excelFields.findIndex(function (excelField) {
				excelField = excelField ? excelField.trim().toLowerCase() : '';
				return ItemDefinations[enabledField].mappableExcelHeaders.includes(excelField);
			});
			return match;
		};

		_this.handleClose = function () {
			var saveChanges = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

			_this.setState({ open: false });
			_this.props.onClose(saveChanges);
		};

		_this.importItems = function () {
			var itemRowList = [].concat((0, _toConsumableArray3.default)(_this.state.validItems), (0, _toConsumableArray3.default)(_this.state.invalidItems));
			var ItemObject = require('../../../../BizLogic/ItemLogic');
			var TaxCodeCache = require('../../../../Cache/TaxCodeCache.js');
			var MyDouble = require('../../../../Utilities/MyDouble');
			var ItemType = require('../../../../Constants/ItemType');
			var taxCodeCache = new TaxCodeCache();
			var ItemFieldHeaders = _ImportItemsXL2.default.ItemFieldHeaders;
			var itemObjs = [];
			itemRowList.forEach(function (itemRow) {
				if (itemRow.isRowValid) {
					var item = new ItemObject();
					var itemName = itemRow[ItemFieldHeaders.ITEM_NAME].value || '';
					var itemCode = itemRow[ItemFieldHeaders.ITEM_CODE].value || '';
					var salePrice = MyDouble.convertStringToDouble(itemRow[ItemFieldHeaders.SALE_PRICE].value);
					var purchasePrice = MyDouble.convertStringToDouble(itemRow[ItemFieldHeaders.PURCHASE_PRICE].value);
					var hsn = itemRow[ItemFieldHeaders.HSN].value || '';

					var itemCategory = itemRow[ItemFieldHeaders.ITEM_CATEGORY] && itemRow[ItemFieldHeaders.ITEM_CATEGORY].value;
					var itemBaseUnit = itemRow[ItemFieldHeaders.BASE_UNIT] && itemRow[ItemFieldHeaders.BASE_UNIT].value;
					var itemSecondaryUnit = itemRow[ItemFieldHeaders.SECONDARY_UNIT] && itemRow[ItemFieldHeaders.SECONDARY_UNIT].value;
					var itemConversionRate = itemRow[ItemFieldHeaders.CONVERSION_RATE] && itemRow[ItemFieldHeaders.CONVERSION_RATE].value;
					var itemDescription = itemRow[ItemFieldHeaders.ITEM_DESCRIPTION] && itemRow[ItemFieldHeaders.ITEM_DESCRIPTION].value;

					itemName = itemName.trim();
					itemCode = itemCode.trim();
					hsn = hsn.trim();
					itemCategory = itemCategory ? itemCategory.trim() : '';
					itemBaseUnit = itemBaseUnit ? itemBaseUnit.trim() : '';
					itemSecondaryUnit = itemSecondaryUnit ? itemSecondaryUnit.trim() : '';
					itemConversionRate = itemConversionRate ? itemConversionRate.trim() : 0;
					itemDescription = itemDescription ? itemDescription.trim() : '';

					var itemLocation = itemRow[ItemFieldHeaders.ITEM_LOCATION] && itemRow[ItemFieldHeaders.ITEM_LOCATION].value;
					var itemOpeningStock = itemRow[ItemFieldHeaders.OPENING_STOCK_QUANTITY] && itemRow[ItemFieldHeaders.OPENING_STOCK_QUANTITY].value;
					var minStockQuantity = itemRow[ItemFieldHeaders.MINIMUM_STOCK_QUANTITY] && itemRow[ItemFieldHeaders.MINIMUM_STOCK_QUANTITY].value;
					itemLocation = itemLocation ? itemLocation.trim() : '';
					itemOpeningStock = MyDouble.convertStringToDouble(itemOpeningStock);
					minStockQuantity = MyDouble.convertStringToDouble(minStockQuantity);

					var taxRate = itemRow[ItemFieldHeaders.TAX_RATE].value;
					var inclusiveOfTax = itemRow[ItemFieldHeaders.INCLUSIVE_OF_TAX].value;

					item.setItemName(itemName);
					item.setItemCode(itemCode);
					item.setItemHSNCode(hsn.trim());
					item.setItemType(ItemType.ITEM_TYPE_INVENTORY);

					item.baseunit = itemBaseUnit;
					item.secondaryunit = itemSecondaryUnit;
					item.category = itemCategory;
					item.conversion = itemConversionRate;

					if (taxRate) {
						var taxId = taxCodeCache.getTaxIdByTaxName(taxRate);
						if (taxId) {
							item.setItemTaxId(taxId);
							var taxType = inclusiveOfTax;
							taxType = taxType == 'Inclusive' ? 1 : 2;
							item.setItemTaxTypeSale(taxType);
							item.setItemTaxTypePurchase(taxType);
						}
					}
					item.setItemSaleUnitPrice(salePrice);
					item.setItemPurchaseUnitPrice(purchasePrice);

					item.setItemOpeningStock(itemOpeningStock);
					item.setItemStockQuantity(itemOpeningStock);

					item.setItemMinStockQuantity(MyDouble.convertStringToDouble(minStockQuantity));
					item.setItemLocation(itemLocation || 0);
					var stockValue = itemOpeningStock * purchasePrice;
					stockValue = stockValue > 0 ? stockValue : 0;
					item.setItemStockValue(stockValue);
					item.setItemDescription(itemDescription);
					itemObjs.push(item);
				}
			});
			_ImportItemsXL2.default.saveImportedItems(itemObjs);
			_this.handleClose(true);
		};

		_this.state = {
			headerMappingRequired: false,
			validItems: null,
			invalidItems: null,
			enabledFields: _ImportItemsXL2.default.getEnabledItemFields(),
			excelFilePath: '',
			excelFields: null,
			open: true
		};
		return _this;
	}

	(0, _createClass3.default)(ImportItems, [{
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				_Dialog2.default,
				{
					fullScreen: true,
					open: this.state.open,
					TransitionComponent: Transition,
					id: 'importItemsDialog'
				},
				_react2.default.createElement(
					'div',
					{ className: 'importItemsDiv' },
					(this.state.uploadInProgress || !this.state.excelFilePath) && _react2.default.createElement(_ImportExcel2.default, {
						onClose: this.handleClose,
						downloadSample: this.handleDownloadSample,
						parseExcelData: this.readExcelFile
					}),
					this.state.headerMappingRequired && _react2.default.createElement(_MapExcelFields2.default, {
						enabledFields: this.state.enabledFields,
						excelFields: this.state.excelFields,
						onClose: this.handleClose,
						handleProceed: this.handleProceed,
						findMatch: this.findMatch
					}),
					this.state.showExcelData && _react2.default.createElement(_DisplayImportItems2.default, {
						validItems: this.state.validItems,
						invalidItems: this.state.invalidItems,
						itemNamesMap: this.state.itemNamesMap,
						itemCodesMap: this.state.itemCodesMap,
						itemFields: this.state.enabledFields,
						onClose: this.handleClose,
						importItems: this.importItems
					})
				)
			);
		}
	}]);
	return ImportItems;
}(_react.Component);

;

ImportItems.propTypes = {
	onClose: _propTypes2.default.func.isRequired
};

exports.default = (0, _styles.withStyles)(styles)(ImportItems);