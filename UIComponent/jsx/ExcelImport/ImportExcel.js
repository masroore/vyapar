Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = require("babel-runtime/core-js/object/get-prototype-of");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require("babel-runtime/helpers/createClass");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require("babel-runtime/helpers/possibleConstructorReturn");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require("babel-runtime/helpers/inherits");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _styles = require("@material-ui/core/styles/");

var _LinearProgress = require("@material-ui/core/LinearProgress");

var _LinearProgress2 = _interopRequireDefault(_LinearProgress);

var _StringConstants = require("../../../Constants/StringConstants");

var _StringConstants2 = _interopRequireDefault(_StringConstants);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var progressBarStyles = {
    barColorPrimary: {
        backgroundColor: '#1789FC'
    },
    root: {
        width: '70%',
        alignSelf: 'center'
    }
};

var styles = {
    '@global': {
        '.import-excel': {
            display: 'flex',
            textAlign: 'center',
            '&>*': {
                height: '70%',
                margin: 'auto 0',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'space-around'
            },
            '&-leftNav': {
                alignItems: 'center',
                borderRight: '1px solid #BDBDBD',
                flex: 1
            },
            '&-rightNav': {
                flex: 2,
                '& a': {
                    color: '#1789FC',
                    cursor: 'pointer'
                }
            },
            '&-dragDrop': {
                background: '#F5F6F7',
                borderRadius: '.5rem',
                margin: '0 10%',
                padding: '1rem',
                height: '70%',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'space-around',
                border: '2px dashed #BDBDBD',
                '&.active': {
                    border: '2px dashed #1789FC'
                },
                '& *': {
                    pointerEvents: 'none'
                },
                '& a': {
                    pointerEvents: 'auto'
                }
            }
        }
    }
};

var CustomLinearProgress = (0, _styles.withStyles)(progressBarStyles)(_LinearProgress2.default);

var ImportExcel = function (_Component) {
    (0, _inherits3.default)(ImportExcel, _Component);

    function ImportExcel(props) {
        (0, _classCallCheck3.default)(this, ImportExcel);

        var _this = (0, _possibleConstructorReturn3.default)(this, (ImportExcel.__proto__ || (0, _getPrototypeOf2.default)(ImportExcel)).call(this, props));

        _this.handleClickUpload = function () {
            var dialog = require('electron').remote.dialog;

            var selectedFile = dialog.showOpenDialog({
                title: 'Select Excel Sheet to Import',
                buttonLabel: 'Select',
                properties: ['openFile'],
                filters: [{
                    name: 'Excel', extensions: ['xls', 'xlsx']
                }]
            });
            if (selectedFile && selectedFile.length) {
                var path = require('path');
                var fileName = path.basename(selectedFile[0]);
                _this.setState({
                    excelFilePath: selectedFile[0],
                    uploadInProgress: true,
                    fileName: fileName
                });
            }
        };

        _this.handleDrop = function (ev) {
            ev.preventDefault();
            if (ev.dataTransfer.items && ev.dataTransfer.items.length == 1) {
                if (ev.dataTransfer.items[0].kind === 'file') {
                    var file = ev.dataTransfer.items[0].getAsFile();
                    if (file.name.endsWith('.xls') || file.name.endsWith('.xlsx')) {
                        _this.setState({
                            isActive: false,
                            excelFilePath: file.path,
                            fileName: file.name,
                            uploadInProgress: true
                        });
                    }
                }
            }
        };

        _this.handleDragEnter = function (ev) {
            var isActive = false;
            if (ev.dataTransfer.items && ev.dataTransfer.items.length == 1 && event.dataTransfer.types[0] == "Files") {
                isActive = true;
            }
            _this.setState({ isActive: isActive });
        };

        _this.handleDragOver = function (ev) {
            ev.preventDefault();
        };

        _this.handleDragLeave = function () {
            _this.setState({ isActive: false });
        };

        var excelFilePath = props.excelFilePath || 'File.xls';
        _this.state = {
            isActive: false,
            uploadInProgress: false,
            excelFilePath: excelFilePath
        };
        return _this;
    }

    (0, _createClass3.default)(ImportExcel, [{
        key: "resetState",
        value: function resetState() {
            this.setState({
                isActive: false,
                uploadInProgress: false,
                excelFilePath: 'File.xls'
            });
        }
    }, {
        key: "componentDidUpdate",
        value: function componentDidUpdate(prevProps, prevState) {
            var _this2 = this;

            if (!prevState.uploadInProgress && this.state.uploadInProgress) {
                var promise = this.props.parseExcelData(this.state.excelFilePath);
                promise && promise.then(function (res) {
                    _this2.resetState && _this2.resetState();
                });
            }
        }
    }, {
        key: "componentWillUnmount",
        value: function componentWillUnmount() {
            this.resetState = null;
        }
    }, {
        key: "render",
        value: function render() {
            return _react2.default.createElement(
                "div",
                { className: "importExcel whitediv" },
                _react2.default.createElement(
                    "div",
                    { className: "whitediv-header" },
                    _react2.default.createElement(
                        "h3",
                        null,
                        "Import Excel"
                    ),
                    _react2.default.createElement("img", { src: "../inlineSVG/round_close.svg", onClick: this.props.onClose })
                ),
                _react2.default.createElement(
                    "div",
                    { className: "whitediv-body import-excel" },
                    _react2.default.createElement(
                        "div",
                        { className: "import-excel-leftNav" },
                        _react2.default.createElement(
                            "div",
                            null,
                            _StringConstants2.default.DOWNLOAD_SAMPLE_ITEM_EXCEL_FIRST_LINE,
                            _react2.default.createElement("br", null),
                            _StringConstants2.default.DOWNLOAD_SAMPLE_ITEM_EXCEL_SECOND_LINE
                        ),
                        _react2.default.createElement(
                            "div",
                            null,
                            _react2.default.createElement("img", { src: "../inlineSVG/xls.svg" })
                        ),
                        _react2.default.createElement(
                            "button",
                            { className: "blue-button", onClick: this.props.downloadSample },
                            "Download"
                        )
                    ),
                    _react2.default.createElement(
                        "div",
                        { className: "import-excel-rightNav" },
                        this.state.uploadInProgress ? _react2.default.createElement(
                            _react.Fragment,
                            null,
                            _react2.default.createElement(
                                "div",
                                null,
                                _StringConstants2.default.UPLOAD_EXCEL_SHEET
                            ),
                            _react2.default.createElement(
                                "div",
                                { className: 'import-excel-dragDrop' },
                                _react2.default.createElement(
                                    "div",
                                    null,
                                    "Uploading " + this.state.excelFilePath
                                ),
                                _react2.default.createElement(CustomLinearProgress, null),
                                _react2.default.createElement(
                                    "div",
                                    null,
                                    "Drag and drop or",
                                    _react2.default.createElement(
                                        "a",
                                        null,
                                        " Click here to Browse",
                                        _react2.default.createElement("br", null)
                                    ),
                                    "formatted excel file to continue"
                                )
                            )
                        ) : _react2.default.createElement(
                            _react.Fragment,
                            null,
                            _react2.default.createElement(
                                "div",
                                null,
                                _StringConstants2.default.UPLOAD_EXCEL_SHEET
                            ),
                            _react2.default.createElement(
                                "div",
                                { className: this.state.isActive ? 'import-excel-dragDrop active' : 'import-excel-dragDrop',
                                    onDrop: this.handleDrop,
                                    onDragOver: this.handleDragOver,
                                    onDragEnter: this.handleDragEnter,
                                    onDragLeave: this.handleDragLeave
                                },
                                _react2.default.createElement("img", { src: "../inlineSVG/upload.svg" }),
                                _react2.default.createElement(
                                    "div",
                                    null,
                                    "Drag and drop or",
                                    _react2.default.createElement(
                                        "a",
                                        { onClick: this.handleClickUpload },
                                        ' Click here to Browse',
                                        _react2.default.createElement("br", null)
                                    ),
                                    "formatted excel file to continue"
                                )
                            )
                        )
                    )
                )
            );
        }
    }]);
    return ImportExcel;
}(_react.Component);

;

exports.default = (0, _styles.withStyles)(styles)(ImportExcel);