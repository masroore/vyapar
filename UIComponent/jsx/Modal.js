Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactModal = require('react-modal');

var _reactModal2 = _interopRequireDefault(_reactModal);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/core/styles');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import InfoIcon from '@material-ui/icons/Info';

var styles = function styles(theme) {
	return {
		modalWithoutHeader: {
			padding: '20px !important',
			backgroundColor: '#FFFFFF !important',
			borderRadius: '4px'
		}
	};
};

var Modal = function (_React$Component) {
	(0, _inherits3.default)(Modal, _React$Component);

	function Modal(props) {
		(0, _classCallCheck3.default)(this, Modal);

		var _this = (0, _possibleConstructorReturn3.default)(this, (Modal.__proto__ || (0, _getPrototypeOf2.default)(Modal)).call(this, props));

		_this.onAfterOpen = _this.onAfterOpen.bind(_this);
		_this.onClose = _this.onClose.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(Modal, [{
		key: 'onAfterOpen',
		value: function onAfterOpen() {
			this.props.onAfterOpen && this.props.onAfterOpen();
		}
	}, {
		key: 'onClose',
		value: function onClose() {
			this.props.onClose && this.props.onClose();
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			if (this.props.info) {
				setTimeout(function () {
					$('.settingsToolTip').tooltipster({
						theme: 'tooltipster-noir',
						maxWidth: 300
					});
				});
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var _this2 = this;

			this.showCloseIcon = this.props.showCloseIcon ? this.props.showCloseIcon : true;
			var classes = this.props.classes;

			return _react2.default.createElement(
				_reactModal2.default,
				(0, _extends3.default)({}, this.props, {
					isOpen: this.props.isOpen,
					onAfterOpen: this.onAfterOpen,
					ariaHideApp: false,
					onRequestClose: this.onClose,
					style: { overlay: {}, content: { width: this.props.width, height: this.props.height } },
					parentSelector: function parentSelector() {
						return _this2.props.parentSelector || document.body;
					},
					shouldCloseOnOverlayClick: false,
					role: 'dialog',
					className: this.props.parentClassSelector + ' ' + (this.props.canHideHeader ? classes.modalWithoutHeader : '')
				}),
				_react2.default.createElement(
					'div',
					{ className: 'modalContainer d-flex flex-column' },
					!this.props.canHideHeader && _react2.default.createElement(
						'div',
						{ className: 'modalHeader d-flex justify-content-between' },
						_react2.default.createElement(
							'div',
							{ className: 'modalTitle' },
							' ',
							this.props.title,
							' '
						),
						this.showCloseIcon && _react2.default.createElement(
							'div',
							{ onClick: this.onClose, className: 'closeIcon' },
							'\u2716'
						)
					),
					this.props.canHideHeader && _react2.default.createElement(
						'div',
						{ className: 'modal-title d-flex justify-content-between', style: { padding: '0.2px' } },
						_react2.default.createElement(
							'span',
							{ className: 'heading mb-10 d-inline-block font-family-bold' },
							this.props.title
						),
						this.props.info && _react2.default.createElement(
							'span',
							{ style: { marginLeft: '270px', paddingTop: '3px' } },
							this.props.info
						),
						_react2.default.createElement('img', { className: 'cross-icon', src: '../inlineSVG/cross-icon.svg', onClick: this.onClose })
					),
					this.props.children
				)
			);
		}
	}]);
	return Modal;
}(_react2.default.Component);

Modal.propTypes = {
	classes: _propTypes2.default.object,
	onAfterOpen: _propTypes2.default.func,
	onClose: _propTypes2.default.func,
	children: _propTypes2.default.oneOfType([_propTypes2.default.arrayOf(_propTypes2.default.node), _propTypes2.default.node]),
	isOpen: _propTypes2.default.bool,
	canHideHeader: _propTypes2.default.bool,
	info: _propTypes2.default.func,
	showCloseIcon: _propTypes2.default.bool,
	title: _propTypes2.default.string,
	parentClassSelector: _propTypes2.default.string,
	parentSelector: _propTypes2.default.node,
	width: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
	height: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number])
};

exports.default = (0, _styles.withStyles)(styles)(Modal);