Object.defineProperty(exports, "__esModule", {
	value: true
});

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _throttleDebounce = require('throttle-debounce');

var _SearchBox = require('./SearchBox');

var _SearchBox2 = _interopRequireDefault(_SearchBox);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var GridElContainer = function (_React$Component) {
	(0, _inherits3.default)(GridElContainer, _React$Component);

	function GridElContainer() {
		(0, _classCallCheck3.default)(this, GridElContainer);
		return (0, _possibleConstructorReturn3.default)(this, (GridElContainer.__proto__ || (0, _getPrototypeOf2.default)(GridElContainer)).apply(this, arguments));
	}

	(0, _createClass3.default)(GridElContainer, [{
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate() {
			return false;
		}
	}, {
		key: 'render',
		value: function render() {
			return _react2.default.createElement('div', {
				style: {
					height: this.props.height || '100%',
					width: this.props.width || '100%'
				},
				className: 'ag-center-cols-container',
				ref: this.props.gridRef });
		}
	}]);
	return GridElContainer;
}(_react2.default.Component);

var Grid = function (_React$Component2) {
	(0, _inherits3.default)(Grid, _React$Component2);

	function Grid(props) {
		(0, _classCallCheck3.default)(this, Grid);

		var _this2 = (0, _possibleConstructorReturn3.default)(this, (Grid.__proto__ || (0, _getPrototypeOf2.default)(Grid)).call(this, props));

		_this2.init = function () {
			var gridOptions = _this2.props.gridOptions;

			_this2.grid = new agGrid.Grid(_this2.gridRef.current, (0, _extends3.default)({
				onGridReady: _this2.onGridReady,
				animateRows: true,
				enableColResize: true,
				navigateToNextCell: _this2.keyboardNavigation,
				frameworkComponents: _this2.state.frameworkComponents,
				suppressDragLeaveHidesColumns: true,
				defaultColDef: {
					comparator: function comparator(a, b) {
						if (typeof a === 'string') {
							return a.localeCompare(b);
						} else {
							return a > b ? 1 : a < b ? -1 : 0;
						}
					},
					headerClass: 'alignLeft',
					icons: {
						menu: '<svg style="width:16px;height:16px" viewBox="0 0 24 13">	<path fill="rgba(0, 0, 0, 0.54)" d="M15,19.88C15.04,20.18 14.94,20.5 14.71,20.71C14.32,21.1 13.69,21.1 13.3,20.71L9.29,16.7C9.06,16.47 8.96,16.16 9,15.87V10.75L4.21,4.62C3.87,4.19 3.95,3.56 4.38,3.22C4.57,3.08 4.78,3 5,3V3H19V3C19.22,3 19.43,3.08 19.62,3.22C20.05,3.56 20.13,4.19 19.79,4.62L15,10.75V19.88M7.04,5L11,10.06V15.58L13,17.58V10.05L16.96,5H7.04Z" />	</svg>',
						filter: ''
					},
					headerComponentParams: {
						template: '<div class="sample-class ag-cell-label-container" role="presentation">\n\t\t\t\t\t\t\t\t\t <span ref="eMenu" class="ag-header-icon ag-header-cell-menu-button"></span>\n\t\t\t\t\t\t\t\t\t <div ref="eLabel" class="ag-header-cell-label" role="presentation">\n\t\t\t\t\t\t\t\t\t\t <span ref="eSortOrder" class="ag-header-icon ag-sort-order" ></span>\n\t\t\t\t\t\t\t\t\t\t <span ref="eSortAsc" class="ag-header-icon ag-sort-ascending-icon" ></span>\n\t\t\t\t\t\t\t\t\t\t <span ref="eSortDesc" class="ag-header-icon ag-sort-descending-icon" ></span>\n\t\t\t\t\t\t\t\t\t\t <span ref="eSortNone" class="ag-header-icon ag-sort-none-icon" ></span>\n\t\t\t\t\t\t\t\t\t\t <span ref="eText" class="ag-header-cell-text" role="columnheader"></span>\n\t\t\t\t\t\t\t\t\t </div>\n\t\t\t\t\t\t\t\t </div>'
					}
				},
				onColumnResized: _this2.saveState,
				onColumnMoved: _this2.saveState,
				onSortChanged: _this2.saveState
			}, gridOptions, {
				// colDefs: gridOptions.columnDefs,
				onRowSelected: _this2.onRowSelected
			}));
			setTimeout(function () {
				try {
					var body = _this2.containerEl.current.querySelector('.ag-center-cols-container');
					document.addEventListener('click', _this2.hideMenu);
					body.addEventListener('contextmenu', _this2.openContextMenu);
					body.addEventListener('click', _this2.openContextMenuForDots);
					body = null;
				} catch (e) {
					if (logger !== 'undefined' && logger && logger.error) {
						logger.error(e);
					}
				}
			}, 100);
		};

		_this2.containerEl = _react2.default.createRef();
		_this2.gridRef = _react2.default.createRef();
		_this2.state = {};
		_this2.onGridReady = _this2.onGridReady.bind(_this2);
		_this2.keyboardNavigation = _this2.keyboardNavigation.bind(_this2);
		_this2.onRowSelected = _this2.onRowSelected.bind(_this2);
		_this2.menuItemClicked = _this2.menuItemClicked.bind(_this2);
		_this2.hideMenu = _this2.hideMenu.bind(_this2);
		_this2.openContextMenu = _this2.openContextMenu.bind(_this2);
		_this2.openContextMenuForDots = _this2.openContextMenuForDots.bind(_this2);
		_this2.saveState = (0, _throttleDebounce.debounce)(1000, _this2.saveState.bind(_this2));
		_this2.quickFilter = _this2.quickFilter.bind(_this2);
		// this.updateData = debounce(300, this.updateData.bind(this));
		return _this2;
	}

	(0, _createClass3.default)(Grid, [{
		key: 'quickFilter',
		value: function quickFilter(text) {
			this.api.setQuickFilter(text);
		}
	}, {
		key: 'hideMenu',
		value: function hideMenu(e) {
			document.querySelectorAll('.context-menu').forEach(function (el) {
				el.style.display = 'none';
			});
		}
	}, {
		key: 'openContextMenu',
		value: function openContextMenu(e) {
			this.hideMenu(e);
			var menu = this.containerEl.current.querySelector('.context-menu');
			var agRow = e.target.closest('.ag-row');
			var clickedRowIndex = agRow ? agRow.getAttribute('row-index') : '';
			if (!clickedRowIndex) return;
			var clickedRow = this.api.getDisplayedRowAtIndex(Number(clickedRowIndex));
			if (clickedRow) {
				if (this.props.filterContextMenu) {
					this.props.filterContextMenu(clickedRow);
				}
				clickedRow.setSelected(true);
				if (this.props.contextMenuClickCallback && typeof this.props.contextMenuClickCallback === 'function') {
					this.props.contextMenuClickCallback(clickedRow);
				}
			}
			e.preventDefault();
			this.setContextMenuPosition(e, menu);
		}
	}, {
		key: 'setContextMenuPosition',
		value: function setContextMenuPosition(e, menu) {
			setTimeout(function () {
				menu.style.display = 'block';
				var _window = window,
				    windowWidth = _window.innerWidth,
				    windowHeight = _window.innerHeight;
				var menuWidth = menu.offsetWidth,
				    menuHeight = menu.offsetHeight;

				var x = e.pageX;
				var y = e.pageY;
				if (x + menuWidth > windowWidth) {
					x -= x + menuWidth - windowWidth;
				}

				if (y + menuHeight > windowHeight) {
					y -= y + (menuHeight + 40) - windowHeight;
				}
				menu.style.left = x + 'px';
				menu.style.top = y + 'px';
			}, 100);
		}
	}, {
		key: 'openContextMenuForDots',
		value: function openContextMenuForDots(e) {
			this.hideMenu(e);
			var menu = this.containerEl.current.querySelector('.context-menu');
			if (e.target.className == 'contextMenuDots') {
				var clickedRowIndex = e.target.closest('[row-index]').getAttribute('row-index');
				var clickedRow = this.api.getDisplayedRowAtIndex(Number(clickedRowIndex));
				if (clickedRow) {
					if (this.props.filterContextMenu) {
						this.props.filterContextMenu(clickedRow);
					}
					clickedRow.setSelected(true);
					if (this.props.contextMenuClickCallback && typeof this.props.contextMenuClickCallback === 'function') {
						this.props.contextMenuClickCallback(node);
					}
				}
				e.preventDefault();
				this.setContextMenuPosition(e, menu);
			}
		}
		// shouldComponentUpdate () {
		// 	return false;
		// }
		// updateData (nextProps) {
		// 	const {gridOptions} = nextProps;
		// 	if (this.api && this.api.setRowData) {
		// 		this.api.setColumnDefs(gridOptions.columnDefs);
		// 	}
		// }
		// UNSAFE_componentWillReceiveProps (nextProps) {
		// 	// this.updateData(nextProps);
		// }

	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			setTimeout(this.init.bind(this));
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			var body = this.containerEl.current.querySelector('.ag-center-cols-container');
			window.removeEventListener('resize', this.adjustGridSize);
			document.removeEventListener('resize-grid', this.adjustGridSize);
			document.removeEventListener('click', this.hideMenu);
			body.removeEventListener('contextmenu', this.openContextMenu);
			body.removeEventListener('click', this.openContextMenuForDots);
			body = null;
			try {
				var $delDialog2 = $('#delDialog2');
				if ($delDialog2.is(':ui-dialog')) {
					$delDialog2.dialog('close').dialog('destroy');
				}
			} catch (error) {
				var _logger = require('../../Utilities/logger');
				_logger.error(error);
			}
		}
	}, {
		key: 'onGridReady',
		value: function onGridReady(params) {
			var _props = this.props,
			    gridKey = _props.gridKey,
			    getApi = _props.getApi;

			var columnApi = params.columnApi;
			try {
				if (gridKey) {
					var settings = window.localStorage.getItem('__GRID_SETTINGS__');
					if (settings) {
						settings = JSON.parse(settings);
						if (settings[gridKey]) {
							settings = settings[gridKey];
							columnApi.setColumnState(settings.colState);
							// columnApi.setColumnGroupState(settings.groupState);
							params.api.setSortModel(settings.sortState);
							// params.api.setFilterModel(settings.filterState);
						}
					}
				}
			} catch (e) {
				if (logger !== 'undefined' && logger && logger.error) {
					logger.error(e);
				}
			}
			try {
				getApi && getApi(params);
			} catch (e) {
				if (e.name === 'Invariant Violation') {
					// If Grid component gets unmounted by the time ag-grid runs this function, we get this error
					// We can ignore this error as we already unmounted Grid
					return;
				}
				throw e;
			}
			this.api = params.api;

			if (!this.props.notSelectFirstRow) {
				var firstRow = this.api.getModel().getRow(0);
				if (firstRow) {
					firstRow.setSelected(true);
				}
			}

			if (params.api && typeof params.api.sizeColumnsToFit === 'function') {
				params.api.sizeColumnsToFit();
			}
			setTimeout(function () {
				if (params.api && typeof params.api.sizeColumnsToFit === 'function') {
					params.api.sizeColumnsToFit();
				}
			}, 1000);
			this.adjustGridSize = (0, _throttleDebounce.debounce)(300, function () {
				if (params.api && typeof params.api.sizeColumnsToFit === 'function') {
					params.api.sizeColumnsToFit();
				}
			});
			window.addEventListener('resize', this.adjustGridSize);
			document.addEventListener('resize-grid', this.adjustGridSize);
			if (this.props.gridOptions && this.props.gridOptions.onGridReadyCallback) {
				this.props.gridOptions.onGridReadyCallback(params);
			}
		}
	}, {
		key: 'keyboardNavigation',
		value: function keyboardNavigation(params) {
			var previousCell = params.previousCellPosition;
			var suggestedNextCell = params.nextCellPosition;

			var KEY_UP = 38;
			var KEY_DOWN = 40;
			var KEY_LEFT = 37;
			var KEY_RIGHT = 39;

			switch (params.key) {
				case KEY_DOWN:
					this.api.forEachNode(function (node) {
						if (previousCell.rowIndex + 1 === node.rowIndex) {
							node.setSelected(true);
						}
					});
					return suggestedNextCell;
				case KEY_UP:
					this.api.forEachNode(function (node) {
						if (previousCell.rowIndex - 1 === node.rowIndex) {
							node.setSelected(true);
						}
					});
					return suggestedNextCell;
				case KEY_LEFT:
				case KEY_RIGHT:
					return suggestedNextCell;
			}
		}
	}, {
		key: 'onRowSelected',
		value: function onRowSelected(param) {
			if (this.props.gridOptions && this.props.gridOptions.onRowSelected !== undefined) {
				if (!param.node.selected && !this.props.toggleSelect) {
					return false;
				}
				this.props.gridOptions.onRowSelected(param);
			}
		}
	}, {
		key: 'menuItemClicked',
		value: function menuItemClicked(callback) {
			var selectedRows = this.api.getSelectedRows();
			callback(selectedRows[0]);
		}
	}, {
		key: 'saveState',
		value: function saveState(params) {
			var _props2 = this.props,
			    gridKey = _props2.gridKey,
			    CTAButton = _props2.CTAButton;
			var columnApi = params.columnApi,
			    api = params.api;

			try {
				if (gridKey) {
					var gridSettings = {};
					var settings = window.localStorage.getItem('__GRID_SETTINGS__');
					if (settings) {
						settings = JSON.parse(settings);
					} else {
						settings = {};
					}
					gridSettings.colState = columnApi.getColumnState(settings.colState);
					// settings.groupState = columnApi.getColumnGroupState();
					gridSettings.sortState = api.getSortModel();
					settings[gridKey] = gridSettings;
					window.localStorage.setItem('__GRID_SETTINGS__', (0, _stringify2.default)(settings));
					// settings.filterState = api.getFilterModel();
				}
			} catch (e) {
				if (logger !== 'undefined' && logger && logger.error) {
					logger.error(e);
				}
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var _this3 = this;

			var CTAButton = this.props.CTAButton;

			if (this.props.selectFirstRow && this.api) {
				setTimeout(function () {
					var selectedRows = _this3.api.getSelectedRows();
					if (!selectedRows.length) {
						var firstRow = _this3.api.getModel().getRow(0);
						firstRow && firstRow.setSelected(true);
					}
				}, 1);
			}
			var menuItems = [];
			if (this.props.contextMenu) {
				menuItems = this.props.contextMenu.filter(function (menu) {
					return menu.visible;
				}).map(function (menu, index) {
					return _react2.default.createElement(
						'li',
						{
							key: index,
							onClick: function onClick() {
								return _this3.menuItemClicked(menu.cmd);
							}
						},
						menu.title
					);
				});
			}

			return _react2.default.createElement(
				'div',
				{ className: 'gridContainer d-flex flex-column' },
				this.props.quickFilter && _react2.default.createElement(
					'div',
					{ className: 'quickFilter ' + (!CTAButton ? 'd-flex justify-content-between' : '') },
					_react2.default.createElement(
						'div',
						{ className: 'gridTitle' },
						this.props.title || ''
					),
					_react2.default.createElement(
						'div',
						{ className: 'gridQuickFilter ' + (CTAButton ? 'd-flex justify-content-between pr-10 mt-15' : '') },
						_react2.default.createElement(_SearchBox2.default, {
							collapsed: false,
							collapsedTextBoxClass: '',
							throttle: 300,
							filter: this.quickFilter
						}),
						this.props.CTAButton
					)
				),
				_react2.default.createElement(
					'div',
					{
						ref: this.containerEl,
						className: this.props.avoidDefaultTheme ? '' : 'ag-theme-material flex-1 ' + (this.props.classes || ''),
						style: {
							height: this.props.height || '100%',
							width: this.props.width || '100%'
						}
					},
					_react2.default.createElement(
						'div',
						{ className: 'context-menu tempmenu' },
						_react2.default.createElement(
							'ul',
							null,
							menuItems
						)
					),
					_react2.default.createElement(GridElContainer, {
						height: this.props.height || '100%',
						width: this.props.width || '100%',
						gridRef: this.gridRef })
				)
			);
		}
	}]);
	return Grid;
}(_react2.default.Component);

Grid.propTypes = {
	filterContextMenu: _propTypes2.default.func,
	gridKey: _propTypes2.default.string,
	getApi: _propTypes2.default.func,
	notSelectFirstRow: _propTypes2.default.bool,
	toggleSelect: _propTypes2.default.bool,
	gridOptions: _propTypes2.default.object,
	selectFirstRow: _propTypes2.default.bool,
	contextMenu: _propTypes2.default.array,
	quickFilter: _propTypes2.default.bool,
	title: _propTypes2.default.string,
	classes: _propTypes2.default.string,
	height: _propTypes2.default.string,
	width: _propTypes2.default.string,
	avoidDefaultTheme: _propTypes2.default.bool
};

exports.default = Grid;