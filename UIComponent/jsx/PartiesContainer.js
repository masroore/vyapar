Object.defineProperty(exports, "__esModule", {
	value: true
});

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/core/styles');

var _AppBar = require('@material-ui/core/AppBar');

var _AppBar2 = _interopRequireDefault(_AppBar);

var _Tabs = require('@material-ui/core/Tabs');

var _Tabs2 = _interopRequireDefault(_Tabs);

var _Tab = require('@material-ui/core/Tab');

var _Tab2 = _interopRequireDefault(_Tab);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _SettingCache = require('../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
	return {
		root: {
			marginTop: 2,
			height: '100%'
		},
		paperRoot: {
			zIndex: 0
		},
		label: {
			textTransform: 'capitalize',
			fontFamily: 'robotoMedium',
			fontSize: '1rem'
		},
		selected: {
			color: 'rgba(11, 143, 197, 0.8) !important'
		},
		tabsIndicator: {
			backgroundColor: '#0b8fc5'
		}
	};
};

var PartiesContainer = function (_React$Component) {
	(0, _inherits3.default)(PartiesContainer, _React$Component);

	function PartiesContainer(props) {
		(0, _classCallCheck3.default)(this, PartiesContainer);

		var _this = (0, _possibleConstructorReturn3.default)(this, (PartiesContainer.__proto__ || (0, _getPrototypeOf2.default)(PartiesContainer)).call(this, props));

		_this.renderPage = function (index) {
			var partyId = _this.props.partyId;


			switch (index) {
				case 0:
					var NamesContainer = require('./NamesContainer').default;
					return _react2.default.createElement(NamesContainer, { partyId: partyId, onResume: _this.onResume, selectedFilter: _this.props.selectedFilter });
					break;
				case 1:
					var NamesGroupContainer = require('./NamesGroupContainer').default;
					return _react2.default.createElement(NamesGroupContainer, { onResume: _this.onResume });
					break;
			}
		};

		var settingCache = new _SettingCache2.default();
		_this.state = {
			value: 0,
			isGroupEnabled: settingCache.isPartyGroupEnabled()
		};
		_this.handleChange = _this.handleChange.bind(_this);
		_this.handleChangeIndex = _this.handleChangeIndex.bind(_this);
		_this.onResume = _this.onResume.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(PartiesContainer, [{
		key: 'handleChange',
		value: function handleChange(event, value) {
			this.setState({ value: value });
		}
	}, {
		key: 'handleChangeIndex',
		value: function handleChangeIndex(index) {
			this.setState({ value: index });
		}
	}, {
		key: 'onResume',
		value: function onResume() {
			var settingCache = new _SettingCache2.default();
			var isGroupEnabled = settingCache.isPartyGroupEnabled();

			if (this.state.value === 1 && !isGroupEnabled) {
				this.setState({
					value: 0
				});
			}
			this.setState({ isGroupEnabled: isGroupEnabled });
		}
	}, {
		key: 'render',
		value: function render() {
			var _classNames;

			var _props = this.props,
			    classes = _props.classes,
			    partyId = _props.partyId;
			var _state = this.state,
			    value = _state.value,
			    isGroupEnabled = _state.isGroupEnabled;

			var tabClasses = (0, _classnames2.default)((_classNames = {}, (0, _defineProperty3.default)(_classNames, classes.label, true), (0, _defineProperty3.default)(_classNames, classes.selected, true), _classNames));
			return _react2.default.createElement(
				'div',
				{ className: 'd-flex flex-column ' + classes.root },
				_react2.default.createElement(
					_AppBar2.default,
					{ classes: { root: classes.paperRoot }, position: 'static', color: 'default' },
					_react2.default.createElement(
						_Tabs2.default,
						{
							value: value,
							onChange: this.handleChange,
							indicatorColor: 'primary',
							textColor: 'primary',
							variant: 'fullWidth',
							classes: {
								indicator: classes.tabsIndicator
							}
						},
						_react2.default.createElement(_Tab2.default, { label: 'NAME', className: tabClasses }),
						isGroupEnabled && _react2.default.createElement(_Tab2.default, { label: 'GROUP', className: tabClasses })
					)
				),
				value === 0 && this.renderPage(value),
				value === 1 && isGroupEnabled && this.renderPage(value)
			);
		}
	}]);
	return PartiesContainer;
}(_react2.default.Component);

PartiesContainer.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	theme: _propTypes2.default.object.isRequired,
	selectedFilter: _propTypes2.default.string,
	partyId: _propTypes2.default.number
};

exports.default = (0, _styles.withStyles)(styles, { withTheme: true })(PartiesContainer);