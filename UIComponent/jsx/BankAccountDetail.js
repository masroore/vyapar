Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _ButtonDropdown = require('./ButtonDropdown');

var _ButtonDropdown2 = _interopRequireDefault(_ButtonDropdown);

var _DecimalInputFilter = require('../../Utilities/DecimalInputFilter');

var _DecimalInputFilter2 = _interopRequireDefault(_DecimalInputFilter);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles() {
	return {
		title: {
			color: '#333',
			fontFamily: 'robotoMedium',
			fontSize: '0.875rem',
			textTransform: 'uppercase',
			maxWidth: 400,
			overflow: 'hidden',
			whiteSpace: 'nowrap',
			textOverflow: 'ellipsis'
		},
		containerForItemList: {
			maxHeight: 100
		}
	};
};

var BankAccountDetail = function (_React$Component) {
	(0, _inherits3.default)(BankAccountDetail, _React$Component);

	function BankAccountDetail() {
		var _ref;

		var _temp, _this, _ret;

		(0, _classCallCheck3.default)(this, BankAccountDetail);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = BankAccountDetail.__proto__ || (0, _getPrototypeOf2.default)(BankAccountDetail)).call.apply(_ref, [this].concat(args))), _this), _this.onAdjustAccount = function () {
			var account = _this.props.currentItem;
			if (account) {
				var CommonUtility = require('../../Utilities/CommonUtility');
				var MyAnalytics = require('../../Utilities/analyticsHelper');
				var accountName = account.accountName;
				window.accountAdjustmentNameGlobal = accountName;
				window.accountAdjustmentIdGlobal = '';
				MyAnalytics.pushEvent('Bank Adjustment Open');
				CommonUtility.loadFrameDiv('NewAccountAdjustment.html');
			}
		}, _this.onB2BAdjustment = function () {
			var accountName = _this.props.currentItem.accountName;
			var BankAdjustmentUtility = require('../../Utilities/BankAdjustmentUtility');
			BankAdjustmentUtility.openBankToBankTransfer(accountName);
		}, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
	}

	(0, _createClass3.default)(BankAccountDetail, [{
		key: 'getMenuOptions',
		value: function getMenuOptions() {
			var menuOptions = [];
			menuOptions.push({
				buttonText: 'Adjust Account',
				buttonIcon: _react2.default.createElement('img', { style: { height: '12px' }, src: './../inlineSVG/item_adj.svg' }),
				onClick: this.onAdjustAccount
			});
			menuOptions.push({
				buttonText: 'Bank to Bank',
				buttonIcon: _react2.default.createElement('img', { style: { height: '12px' }, src: './../inlineSVG/b2b.jpg' }),
				onClick: this.onB2BAdjustment
			});
			return menuOptions;
		}
	}, {
		key: 'getB2BAdjustmentDiv',
		value: function getB2BAdjustmentDiv() {
			return _react2.default.createElement(
				'div',
				{ className: 'bankToBankTransfer hide', id: 'bankToBankDialog' },
				_react2.default.createElement(
					'div',
					{ className: 'transactionStyle bankTransactionStyle newfm', id: 'transactionStyle' },
					_react2.default.createElement('input', { type: 'hidden', id: 'bankAdjId' }),
					_react2.default.createElement(
						'table',
						{ className: 'width100 formDialogueTable' },
						_react2.default.createElement(
							'tbody',
							null,
							_react2.default.createElement(
								'tr',
								null,
								_react2.default.createElement(
									'td',
									{ colSpan: '2' },
									_react2.default.createElement(
										'div',
										{ className: 'inputLabel', style: { marginTop: '29px' } },
										_react2.default.createElement(
											'div',
											{ className: 'labelText' },
											'From Bank'
										),
										_react2.default.createElement('select', { id: 'fromBank', className: 'width100 padthin margintop selectiond', style: { border: 'none', outline: 'none' }, disabled: true })
									)
								)
							),
							_react2.default.createElement(
								'tr',
								null,
								_react2.default.createElement(
									'td',
									{ colSpan: '2' },
									_react2.default.createElement(
										'div',
										{ className: 'inputLabel' },
										_react2.default.createElement(
											'div',
											{ className: 'labelText' },
											'To Bank'
										),
										_react2.default.createElement(
											'select',
											{ id: 'toBank', className: 'width100 padthin margintop selection', style: { border: 'none', outline: 'none' } },
											_react2.default.createElement(
												'option',
												{ selected: true, disabled: true },
												'Select Bank'
											)
										)
									)
								)
							),
							_react2.default.createElement(
								'tr',
								null,
								_react2.default.createElement(
									'td',
									{ className: 'padright' },
									_react2.default.createElement(
										'div',
										{ className: 'inputLabel' },
										_react2.default.createElement(
											'div',
											{ className: 'labelText' },
											'Amount'
										),
										_react2.default.createElement('input', { type: 'text', onInput: function onInput(e) {
												return _DecimalInputFilter2.default.inputTextFilterForAmount(e.target);
											}, id: 'b2bAmount', className: 'width100  margintop' })
									)
								),
								_react2.default.createElement(
									'td',
									{ className: 'padleft' },
									_react2.default.createElement(
										'div',
										{ className: 'inputLabel' },
										_react2.default.createElement(
											'div',
											{ className: 'labelText' },
											'Date'
										),
										_react2.default.createElement('input', { type: 'text', id: 'b2bDate', className: 'width100 margintop padleft' })
									)
								)
							),
							_react2.default.createElement(
								'tr',
								null,
								_react2.default.createElement(
									'td',
									{ colSpan: '2' },
									_react2.default.createElement(
										'div',
										{ className: 'inputLabel' },
										_react2.default.createElement(
											'div',
											{ className: 'labelText' },
											'Description'
										),
										_react2.default.createElement('textarea', { id: 'b2bDescription', className: 'width100', style: { marginTop: '13px' } })
									)
								)
							),
							_react2.default.createElement(
								'tr',
								null,
								_react2.default.createElement(
									'td',
									{ colSpan: '2' },
									_react2.default.createElement(
										'div',
										{ className: 'floatRight terminalButtonB2BParent' },
										_react2.default.createElement(
											'button',
											{ 'data-shortcut': 'CTRL_S', type: 'submit', className: 'terminalButton terminalButtonB2B', id: 'submit' },
											'SAVE'
										)
									)
								)
							)
						)
					)
				)
			);
		}
	}, {
		key: 'render',
		value: function render() {
			var classes = this.props.classes;
			var item = this.props.currentItem;

			if (!item || !item.accountId) {
				return false;
			}

			return _react2.default.createElement(
				'div',
				{ className: 'containerForItemList ' + classes.containerForItemList },
				_react2.default.createElement(
					'div',
					{ className: 'mb-10 d-flex justify-content-between' },
					_react2.default.createElement(
						'div',
						{ className: classes.title },
						item.accountName
					),
					_react2.default.createElement(
						'div',
						null,
						_react2.default.createElement(_ButtonDropdown2.default, { color: 'secondary', menuOptions: this.getMenuOptions() })
					)
				),
				_react2.default.createElement(
					'div',
					null,
					_react2.default.createElement(
						'div',
						{ className: 'mb-10 d-flex justify-content-between' },
						_react2.default.createElement(
							'div',
							null,
							_react2.default.createElement(
								'span',
								null,
								'Bank: ',
								item.bankName
							),
							_react2.default.createElement('br', null)
						),
						_react2.default.createElement(
							'div',
							{ className: 'mr-10' },
							_react2.default.createElement(
								'span',
								null,
								'CURRENT BAL: ',
								_react2.default.createElement('span', { dangerouslySetInnerHTML: _MyDouble2.default.getBalanceAmountWithDecimalAndCurrency(item.currentBalance, true, true) })
							)
						)
					),
					_react2.default.createElement(
						'span',
						null,
						'ACCOUNT NO. : ',
						item.accountNumber
					)
				),
				this.getB2BAdjustmentDiv()
			);
		}
	}]);
	return BankAccountDetail;
}(_react2.default.Component);

BankAccountDetail.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	currentItem: _propTypes2.default.object
};

exports.default = (0, _styles.withStyles)(styles)(BankAccountDetail);