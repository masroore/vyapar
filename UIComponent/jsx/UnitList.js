Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _ItemUnitCache = require('../../Cache/ItemUnitCache');

var _ItemUnitCache2 = _interopRequireDefault(_ItemUnitCache);

var _ErrorCode = require('../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var UnitList = function (_React$Component) {
	(0, _inherits3.default)(UnitList, _React$Component);

	function UnitList(props) {
		(0, _classCallCheck3.default)(this, UnitList);

		var _this = (0, _possibleConstructorReturn3.default)(this, (UnitList.__proto__ || (0, _getPrototypeOf2.default)(UnitList)).call(this, props));

		_this.selectFirstRow = function () {
			setTimeout(function () {
				var firstColumn = document.querySelector('.left-column .ag-row-selected .ag-cell[col-id="unitName"]');
				firstColumn && firstColumn.focus();
			}, 100);
		};

		_this.getApi = function (params) {
			_this.api = params.api;
			_this.api.setRowData(_this.props.items);
		};

		_this.columnDefs = [{ field: 'unitName', headerName: 'FULLNAME', width: 165 }, {
			field: 'unitShortName',
			headerName: 'SHORTNAME',
			cellClass: 'alignRight',
			width: 125,
			headerClass: 'alignRight'
		}, {
			field: 'unitId',
			headerName: '',
			width: 30,
			getQuickFilterText: function getQuickFilterText() {
				return '';
			},
			suppressSorting: true,
			suppressFilter: true,
			cellRenderer: function cellRenderer() {
				return '<div class="contextMenuDots"></div>';
			}
		}];
		_this.openItem = _this.openItem.bind(_this);
		_this.deleteItem = _this.deleteItem.bind(_this);

		_this.contextMenu = [{
			title: 'View/Edit',
			cmd: _this.openItem,
			visible: true,
			id: 'edit'
		}, {
			title: 'Delete',
			cmd: _this.deleteItem,
			visible: true,
			id: 'delete'
		}];
		return _this;
	}

	(0, _createClass3.default)(UnitList, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.selectFirstRow();
		}
	}, {
		key: 'openItem',
		value: function openItem(itemLogic) {
			if (itemLogic.data && itemLogic.data.getUnitName) {
				itemLogic = itemLogic.data;
			}
			window.unitNameGlobal = itemLogic.getUnitName();
			$('#modelContainer').css('display', 'block');
			$('.viewItems').css('display', 'none');
			$('#frameDiv').load('NewItemUnit.html');
		}
	}, {
		key: 'deleteItem',
		value: function deleteItem(itemLogic) {
			if (itemLogic.data && itemLogic.data.getUnitName) {
				itemLogic = itemLogic.data;
			}
			var itemUnitCache = new _ItemUnitCache2.default();
			var unitName = itemLogic.getUnitName();
			var oldUnitObject = itemUnitCache.getItemUnitObjectByUnitName(unitName);
			if (oldUnitObject.isUnitDeletable()) {
				var statusCode = oldUnitObject.deleteUnit();
				if (statusCode == _ErrorCode2.default.ERROR_UNIT_DELETE_SUCCESS) {
					if (window.onResume) window.onResume();
					ToastHelper.success(statusCode);
				} else {
					ToastHelper.error(statusCode);
				}
			} else {
				ToastHelper.error('This unit cannot be deleted.');
			}
		}
	}, {
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(nextProps) {
			var _this2 = this;

			if (this.api) {
				this.api.setRowData(nextProps.items);
				setTimeout(function () {
					if (_this2.api.getSelectedRows().length === 0) {
						var selected = _this2.api.getDisplayedRowAtIndex(0);
						if (selected) {
							selected.setSelected(true);
						}
					}
				});
			}
			return false;
		}
	}, {
		key: 'render',
		value: function render() {
			var gridOptions = {
				enableFilter: false,
				enableSorting: true,
				rowSelection: 'single',
				columnDefs: this.columnDefs,
				rowData: [],
				deltaRowDataMode: true,
				headerHeight: 40,
				rowHeight: 40,
				getRowNodeId: function getRowNodeId(data) {
					return data.getUnitId();
				},
				onRowSelected: this.props.onItemSelected,
				onRowDoubleClicked: this.openItem
			};
			return _react2.default.createElement(
				'div',
				{ className: 'gridContainer d-flex' },
				_react2.default.createElement(
					'div',
					{ id: 'delDialog2', className: 'hide' },
					_react2.default.createElement(
						'b',
						null,
						'This Item will be Deleted.'
					)
				),
				_react2.default.createElement(_Grid2.default, {
					gridKey: 'unitListGridKey',
					contextMenu: this.contextMenu,
					classes: 'gridRowHeight40 noBorder',
					selectFirstRow: true,
					height: '100%',
					getApi: this.getApi,
					gridOptions: gridOptions
				})
			);
		}
	}]);
	return UnitList;
}(_react2.default.Component);

exports.default = UnitList;