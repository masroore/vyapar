Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _SetPaymentTermModal = require('./SetPaymentTermModal');

var _SetPaymentTermModal2 = _interopRequireDefault(_SetPaymentTermModal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SelectUnitButton = function (_React$Component) {
	(0, _inherits3.default)(SelectUnitButton, _React$Component);

	function SelectUnitButton(props) {
		(0, _classCallCheck3.default)(this, SelectUnitButton);

		var _this = (0, _possibleConstructorReturn3.default)(this, (SelectUnitButton.__proto__ || (0, _getPrototypeOf2.default)(SelectUnitButton)).call(this, props));

		_this.state = {
			show: true
		};
		_this.onClick = _this.onClick.bind(_this);
		_this.onClose = _this.onClose.bind(_this);
		_this.onSave = _this.onSave.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(SelectUnitButton, [{
		key: 'onClick',
		value: function onClick() {
			this.setState({
				show: true
			});
		}
	}, {
		key: 'onClose',
		value: function onClose() {
			this.setState({ show: false });
		}
	}, {
		key: 'onSave',
		value: function onSave() {
			this.setState({ show: false });
		}
	}, {
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				'div',
				{ id: 'rootElement' },
				this.state.show && _react2.default.createElement(_SetPaymentTermModal2.default, {
					isOpen: this.state.show,
					onSave: this.onSave,
					allowBlank: true,
					onClose: this.onClose })
			);
		}
	}]);
	return SelectUnitButton;
}(_react2.default.Component);

exports.default = SelectUnitButton;