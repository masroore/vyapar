Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _SearchBox = require('./SearchBox');

var _SearchBox2 = _interopRequireDefault(_SearchBox);

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _AddCategoryButton = require('./AddCategoryButton');

var _AddCategoryButton2 = _interopRequireDefault(_AddCategoryButton);

var _MoveItemsToCategoryButton = require('./MoveItemsToCategoryButton');

var _MoveItemsToCategoryButton2 = _interopRequireDefault(_MoveItemsToCategoryButton);

var _CategoryList = require('./CategoryList');

var _CategoryList2 = _interopRequireDefault(_CategoryList);

var _NumberFilter = require('./grid-filters/NumberFilter');

var _NumberFilter2 = _interopRequireDefault(_NumberFilter);

var _StringFilter = require('./grid-filters/StringFilter');

var _StringFilter2 = _interopRequireDefault(_StringFilter);

var _AddEditCategoryModal = require('./AddEditCategoryModal');

var _AddEditCategoryModal2 = _interopRequireDefault(_AddEditCategoryModal);

var _ItemCategoryCache = require('../../Cache/ItemCategoryCache');

var _ItemCategoryCache2 = _interopRequireDefault(_ItemCategoryCache);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _ItemType = require('../../Constants/ItemType');

var _ItemType2 = _interopRequireDefault(_ItemType);

var _ItemCache = require('../../Cache/ItemCache');

var _ItemCache2 = _interopRequireDefault(_ItemCache);

var _throttleDebounce = require('throttle-debounce');

var _ErrorCode = require('../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

var _ItemCategoryLogic = require('../../BizLogic/ItemCategoryLogic');

var _ItemCategoryLogic2 = _interopRequireDefault(_ItemCategoryLogic);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var itemCache = new _ItemCache2.default();

var CategoryContainer = function (_React$Component) {
	(0, _inherits3.default)(CategoryContainer, _React$Component);

	function CategoryContainer(props) {
		(0, _classCallCheck3.default)(this, CategoryContainer);

		var _this = (0, _possibleConstructorReturn3.default)(this, (CategoryContainer.__proto__ || (0, _getPrototypeOf2.default)(CategoryContainer)).call(this, props));

		_this.getApi = function (params) {
			_this.api = params.api;
			_this.api.setRowData(_this.state.records);
		};

		var itemCategoryCache = new _ItemCategoryCache2.default();
		itemCategoryCache.reloadItemCategory();
		_this.allItems = itemCategoryCache.getItemCategoriesArray();
		_this.state = {
			records: [],
			items: _this.allItems,
			currentItem: null,
			isEdit: false,
			columnDefs: [{
				field: 'itemName',
				filter: _StringFilter2.default,
				headerName: 'NAME',
				width: 270
			}, {
				field: 'itemStockQuantity',
				headerName: 'QUANTITY',
				width: 100,
				filter: _NumberFilter2.default,
				comparator: function comparator(a, b) {
					return a - b;
				},
				cellRenderer: function cellRenderer(params) {
					var itemLogic = params.data;
					return itemLogic.getItemType() != _ItemType2.default.ITEM_TYPE_SERVICE ? _MyDouble2.default.getQuantityWithDecimal(itemLogic.getItemStockQuantity()) : '';
				}
			}, {
				field: 'itemStockValue',
				headerName: 'STOCK VALUE',
				width: 100,
				cellClass: 'alignRight',
				filter: _NumberFilter2.default,
				cellRenderer: function cellRenderer(params) {
					var itemLogic = params.data;
					return itemLogic.getItemType() != _ItemType2.default.ITEM_TYPE_SERVICE ? _MyDouble2.default.getBalanceAmountWithDecimalAndCurrency(itemLogic.getItemStockValue()) : '';
				}
			}]
		};

		_this._id = null;
		_this.filterItems = _this.filterItems.bind(_this);
		_this.props.onResume && _this.props.onResume();
		_this.editCategory = _this.editCategory.bind(_this);
		_this.closeEditCategoryDialog = _this.closeEditCategoryDialog.bind(_this);
		_this.onItemSelected = (0, _throttleDebounce.debounce)(50, _this.onItemSelected.bind(_this));
		_this.onSave = _this.onSave.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(CategoryContainer, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			window.onResume = this.onResume.bind(this);
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			delete window.onResume;
		}
	}, {
		key: 'onResume',
		value: function onResume() {
			var _this2 = this;

			this.props.onResume && this.props.onResume();
			var itemCategoryCache = new _ItemCategoryCache2.default();
			itemCategoryCache.reloadItemCategory();
			this.allItems = itemCategoryCache.getItemCategoriesArray();
			if (this.state.currentItem) {
				var currentItem = this.allItems.find(function (item) {
					return _this2.state.currentItem.getCategoryId() == item.getCategoryId();
				});
				this.setState({ currentItem: currentItem });
			}
			this.filterItems(this.state.searchText);
			this.onItemSelected(null, true);
		}
	}, {
		key: 'editCategory',
		value: function editCategory(row) {
			if (!row.categoryId) {
				row = row.data;
			}
			this.setState({
				isEdit: true
			});
		}
	}, {
		key: 'onSave',
		value: function onSave(newCategoryName) {
			var newItemCategoryLogic = new _ItemCategoryLogic2.default();
			MyAnalytics.pushEvent('Edit Item Category Open');
			var oldCategoryName = this.state.currentItem.getCategoryName();
			if (newCategoryName) {
				if (oldCategoryName.toLowerCase() == newCategoryName.toLowerCase()) {
					ToastHelper.info('no changes made');
				} else {
					var statusCode = newItemCategoryLogic.editItemCategory(oldCategoryName, newCategoryName);
					if (statusCode == _ErrorCode2.default.ERROR_ITEMCATEGORY_UPDATE_SUCCESS) {
						ToastHelper.success(statusCode);
						window.onResume && window.onResume();
						this.closeEditCategoryDialog();
						MyAnalytics.pushEvent('Edit Item Category Save');
					} else {
						ToastHelper.error(statusCode);
					}
				}
			} else {
				ToastHelper.error('name cannot be empty');
			}
		}
	}, {
		key: 'closeEditCategoryDialog',
		value: function closeEditCategoryDialog() {
			this.setState({
				isEdit: false
			});
		}
	}, {
		key: 'onItemSelected',
		value: function onItemSelected(row) {
			var fromSync = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

			if (!fromSync) {
				if (!row.node.selected) {
					return false;
				}
				var id = row.data.getCategoryId();
				/* do not reload current item */
				// if (this._id == id) {
				// 	return false;
				// }

				this._id = id;
			}
			var records = itemCache.getItemListForCategoryId(this._id);

			this.setState({
				currentItem: row && row.data || this.state.currentItem,
				records: records
			});
			this.api.setRowData(records);
		}
	}, {
		key: 'filterItems',
		value: function filterItems(searchText) {
			if (!searchText) {
				this.setState({ items: this.allItems, searchText: '' });
				return false;
			}
			var query = searchText.toLowerCase();
			var filteredItems = this.allItems.filter(function (item) {
				return item.getCategoryName().toLowerCase().includes(query);
			});

			var records = this.state.records;
			var currentItem = this.state.currentItem;
			if (!filteredItems.length) {
				records = [];
				this.api.setRowData([]);
				currentItem = null;
			}
			this.setState({
				records: records,
				currentItem: currentItem,
				items: filteredItems,
				searchText: searchText
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _this3 = this;

			var gridOptions = {
				enableFilter: true,
				enableSorting: true,
				rowSelection: 'single',
				columnDefs: this.state.columnDefs,
				headerHeight: 40,
				rowHeight: 40,
				rowClass: 'customVerticalBorder',
				rowData: []
			};
			return _react2.default.createElement(
				'div',
				{ className: 'd-flex flex-container' },
				_react2.default.createElement(
					'div',
					{ className: 'd-flex flex-column left-column' },
					_react2.default.createElement(
						'div',
						{ className: 'listheaderDiv d-flex mt-20 mb-10' },
						_react2.default.createElement(
							'div',
							{ className: (0, _classnames2.default)('searchDiv', this.state.showSearch ? 'width100 d-flex' : '') },
							_react2.default.createElement(_SearchBox2.default, { throttle: 500,
								filter: this.filterItems,
								collapsedTextBoxClass: 'width100',
								collapsed: !this.state.showSearch,
								onCollapsedClick: function onCollapsedClick() {
									_this3.setState({ showSearch: true });
								},
								onInputBlur: function onInputBlur() {
									_this3.setState({ showSearch: false });
								}
							})
						),
						_react2.default.createElement(
							'div',
							{ className: 'buttonDiv flex-1 d-flex justify-content-end' },
							_react2.default.createElement(_AddCategoryButton2.default, {
								collapsed: this.state.showSearch
							})
						)
					),
					_react2.default.createElement(_CategoryList2.default, {
						editCategory: this.editCategory,
						onItemSelected: this.onItemSelected,
						items: this.state.items
					})
				),
				_react2.default.createElement(
					'div',
					{ className: 'd-flex flex-column right-column flex-grow-1' },
					this.state.currentItem && _react2.default.createElement(
						'div',
						{ className: 'categoryDetailContainer' },
						_react2.default.createElement(
							'div',
							{ className: 'clearfix' },
							_react2.default.createElement(
								'div',
								{
									id: 'currentItemName',
									className: 'titleColor halfdiv floatLeft width50 ellipseText'
								},
								this.state.currentItem && this.state.currentItem.getCategoryName()
							),
							_react2.default.createElement(_MoveItemsToCategoryButton2.default, {
								category: this.state.currentItem
							})
						),
						_react2.default.createElement(
							'div',
							null,
							this.state.currentItem && this.state.currentItem.getMemberCount()
						)
					),
					_react2.default.createElement(_Grid2.default, {
						gridKey: 'itemsCategoryGridKey',
						title: 'Items',
						quickFilter: true,
						classes: 'alternateRowColor customVerticalBorder gridRowHeight40',
						height: '100%',
						getApi: this.getApi,
						gridOptions: gridOptions
					}),
					this.state.isEdit && _react2.default.createElement(_AddEditCategoryModal2.default, {
						isOpen: this.state.isEdit,
						onClose: this.closeEditCategoryDialog,
						categoryName: this.state.currentItem.getCategoryName(),
						onSave: this.onSave,
						title: 'Edit Category'
					})
				)
			);
		}
	}]);
	return CategoryContainer;
}(_react2.default.Component);

exports.default = CategoryContainer;

CategoryContainer.propTypes = {
	onResume: _propTypes2.default.func
};