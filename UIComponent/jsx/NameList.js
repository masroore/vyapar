Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _MultiSelectionFilter = require('./grid-filters/MultiSelectionFilter');

var _MultiSelectionFilter2 = _interopRequireDefault(_MultiSelectionFilter);

var _ToastHelper = require('../../UIControllers/ToastHelper');

var ToastHelper = _interopRequireWildcard(_ToastHelper);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var NameList = function (_React$Component) {
	(0, _inherits3.default)(NameList, _React$Component);

	function NameList(props) {
		(0, _classCallCheck3.default)(this, NameList);

		var _this = (0, _possibleConstructorReturn3.default)(this, (NameList.__proto__ || (0, _getPrototypeOf2.default)(NameList)).call(this, props));

		_this.selectFirstRow = function () {
			setTimeout(function () {
				var firstColumn = document.querySelector('.left-column .ag-row-selected .ag-cell[col-id="fullName"]');
				firstColumn && firstColumn.focus();
			}, 500);
		};

		_this.columnDefs = [{
			field: 'fullName',
			headerName: 'PARTY',
			sort: 'asc',
			cellRenderer: function cellRenderer(params) {
				var value = params.value;
				return '<div title="' + value + '">' + value + '</div>';
			},
			filter: _MultiSelectionFilter2.default,
			filterOptions: [{
				value: 0,
				label: 'All',
				selected: false
			}, {
				value: 1,
				label: 'To Receive',
				selected: false
			}, {
				value: 2,
				label: 'To Pay',
				selected: false
			}],
			filterFunction: function filterFunction(params, checkedFilterOptions) {
				if (checkedFilterOptions.length == 0) {
					return true;
				}
				var amountType = 0;
				if (params.data.amount > 0.0000001) {
					amountType = 1;
				}
				if (params.data.amount < -0.0000001) {
					amountType = 2;
				}
				var selected = checkedFilterOptions.find(function (option) {
					return option.selected && option.value == amountType || option.value == 0;
				});
				return !!selected;
			}
		}, {
			field: 'amount',
			suppressMenu: true,
			comparator: function comparator(a, b) {
				return a - b;
			},
			headerName: 'AMOUNT',
			cellClass: 'alignRight',
			minWidth: 100,
			headerClass: 'alignRight',
			cellRenderer: function cellRenderer(params) {
				var value = _MyDouble2.default.getBalanceAmountDecimalWithoutCurrency(params.value);
				return '<div title="' + _MyDouble2.default.getAmountWithDecimal(params.value) + '">' + value + '</div>';
			}
		}, {
			field: 'nameId',
			headerName: '',
			width: 30,
			getQuickFilterText: function getQuickFilterText() {
				return '';
			},
			suppressSorting: true,
			suppressFilter: true,
			cellRenderer: function cellRenderer() {
				return '<div class="contextMenuDots"></div>';
			}
		}];
		NameList.openItem = NameList.openItem.bind(_this);
		_this.deleteItem = _this.deleteItem.bind(_this);
		_this.getApi = _this.getApi.bind(_this);
		_this.onItemSelected = _this.onItemSelected.bind(_this);
		_this.contextMenu = [{
			title: 'View/Edit',
			cmd: NameList.openItem,
			visible: true,
			id: 'edit'
		}, {
			title: 'Delete',
			cmd: _this.deleteItem,
			visible: true,
			id: 'delete'
		}];
		_this.firstTime = true;
		return _this;
	}

	(0, _createClass3.default)(NameList, [{
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(nextProps) {
			var _this2 = this;

			if (this.api) {
				this.api.setRowData(nextProps.items);
				setTimeout(function () {
					if (_this2.api.getSelectedRows().length === 0) {
						var selected = _this2.api.getDisplayedRowAtIndex(0);
						if (selected) {
							selected.setSelected(true);
						}
					}
				});
			}
			return false;
		}
	}, {
		key: 'getApi',
		value: function getApi(params) {
			this.api = params.api;
			var selectedFilter = this.props.selectedFilter;

			var filterOptions = [{
				value: 0,
				label: 'All',
				selected: !selectedFilter
			}, {
				value: 1,
				label: 'To Receive',
				selected: selectedFilter == 'To Receive'
			}, {
				value: 2,
				label: 'To Pay',
				selected: selectedFilter == 'To Pay'
			}];
			var filterInstance = params.api.getFilterInstance('fullName');
			filterInstance.setModel({
				value: {
					filterOptions: filterOptions,
					checkedFilterOptions: filterOptions.filter(function (option) {
						return option.selected;
					})
				}
			});
			filterInstance.applyFilter();
			this.api.setRowData(this.props.items);
		}
	}, {
		key: 'deleteItem',
		value: function deleteItem(row) {
			var NameCache = require('../../Cache/NameCache');
			var ErrorCode = require('../../Constants/ErrorCode');
			var nameCache = new NameCache();
			var nameObj = nameCache.findNameObjectByNameId(row.nameId);

			if (nameObj && nameObj.canDeleteParty()) {
				$('#delDialog2').dialog({
					title: 'Are you sure you want to delete this Party?',
					closeOnEscape: true,
					resizable: false,
					height: 150,
					minHeight: 150,
					maxHeight: 150,
					width: 500,
					appendTo: '#dynamicDiv',
					modal: true,
					maxWidth: 500,

					buttons: [{
						text: 'Yes',
						// id: "delButtonY",
						class: 'terminalButton',
						// icon: "ui-icon-heart",
						click: function click() {
							var statusCode = nameObj.deleteName();
							if (statusCode === ErrorCode.ERROR_NAME_DELETE_SUCCESS) {
								ToastHelper.success(statusCode);
								onResume();
								$(this).dialog('close');
							} else {
								ToastHelper.error(statusCode);
							}
						}
					}, {
						text: 'No',
						// id: "delButtonN",
						class: 'terminalButton',
						click: function click() {
							$(this).dialog('close');
						}
					}]
				});
			} else {
				ToastHelper.error(ErrorCode.ERROR_NAME_DELETE_FAILED);
			}
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			try {
				var $delDialog2 = $('#delDialog2');
				if ($delDialog2.is(':ui-dialog')) {
					$delDialog2.dialog('close').dialog('destroy');
				}
			} catch (error) {
				logger.error(error);
			}
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.selectFirstRow();
		}
	}, {
		key: 'onItemSelected',
		value: function onItemSelected(row) {
			var _this3 = this;

			var _props = this.props,
			    partyId = _props.partyId,
			    onItemSelected = _props.onItemSelected;

			if (this.firstTime && partyId) {
				this.firstTime = false;
				this.api.forEachNode(function (node) {
					if (node.data.nameId == partyId) {
						_this3.api.ensureIndexVisible(node.rowIndex);
						/* do not remove lines start */
						node.setSelected(false);
						node.setSelected(true);
						/* do not remove lines end */
					};
				});
			} else {
				onItemSelected && onItemSelected(row);
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var addExtraColumns = this.props.addExtraColumns;

			var gridOptions = {
				enableFilter: true,
				enableSorting: true,
				rowSelection: 'single',
				columnDefs: this.columnDefs,
				rowData: [],
				onRowDoubleClicked: NameList.openItem,
				suppressColumnMoveAnimation: true,
				onRowSelected: this.onItemSelected,
				overlayNoRowsTemplate: 'No parties to show',
				headerHeight: 40,
				rowClass: addExtraColumns ? 'customVerticalBorder' : '',
				rowHeight: 40,
				deltaRowDataMode: true,
				getRowNodeId: function getRowNodeId(data) {
					return data.nameId;
				}
			};
			var gridClasses = 'noBorder gridRowHeight40';
			var selectFirstRow = true;
			var quickFilter = false;
			var title = '';
			if (addExtraColumns) {
				gridClasses = 'alternateRowColor customVerticalBorder gridRowHeight40';
				selectFirstRow = false;
				quickFilter = true;
				title = 'Parties';
			}
			return _react2.default.createElement(
				'div',
				{ className: 'gridContainer d-flex' },
				_react2.default.createElement(
					'div',
					{ id: 'delDialog2', className: 'hide' },
					_react2.default.createElement(
						'b',
						null,
						'This Party will be Deleted.'
					)
				),
				_react2.default.createElement(_Grid2.default, {
					getApi: this.getApi,
					contextMenu: this.contextMenu,
					classes: gridClasses,
					selectFirstRow: selectFirstRow,
					title: title,
					quickFilter: quickFilter,
					height: '100%',
					width: '100%',
					gridOptions: gridOptions
				})
			);
		}
	}], [{
		key: 'openItem',
		value: function openItem(row) {
			if (!row.nameId) {
				row = row.data;
			}
			window.userNameGlobal = row.fullName;
			$('#modelContainer').css('display', 'block');
			$('.viewItems').css('display', 'none');
			$('#frameDiv').load('NewContact.html');
			$('#defaultPage').hide();
		}
	}]);
	return NameList;
}(_react2.default.Component);

NameList.propTypes = {
	onItemSelected: _propTypes2.default.func,
	partyId: _propTypes2.default.number,
	selectedFilter: _propTypes2.default.string,
	items: _propTypes2.default.array,
	addExtraColumns: _propTypes2.default.bool
};

exports.default = NameList;