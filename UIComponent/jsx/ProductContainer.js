Object.defineProperty(exports, "__esModule", {
	value: true
});

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _SearchBox = require('./SearchBox');

var _SearchBox2 = _interopRequireDefault(_SearchBox);

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _ItemDetail = require('./ItemDetail');

var _ItemDetail2 = _interopRequireDefault(_ItemDetail);

var _Items = require('./Items');

var _Items2 = _interopRequireDefault(_Items);

var _DateFilter = require('./grid-filters/DateFilter');

var _DateFilter2 = _interopRequireDefault(_DateFilter);

var _NumberFilter = require('./grid-filters/NumberFilter');

var _NumberFilter2 = _interopRequireDefault(_NumberFilter);

var _StringFilter = require('./grid-filters/StringFilter');

var _StringFilter2 = _interopRequireDefault(_StringFilter);

var _MultiSelectionFilter = require('./grid-filters/MultiSelectionFilter');

var _MultiSelectionFilter2 = _interopRequireDefault(_MultiSelectionFilter);

var _ActiveInactiveItems = require('./ActiveInactiveItems');

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _IPCActions = require('../../Constants/IPCActions');

var _ItemCache = require('../../Cache/ItemCache');

var _ItemCache2 = _interopRequireDefault(_ItemCache);

var _throttleDebounce = require('throttle-debounce');

var _MyDate = require('../../Utilities/MyDate');

var _MyDate2 = _interopRequireDefault(_MyDate);

var _TransactionHelper = require('../../Utilities/TransactionHelper');

var TransactionHelper = _interopRequireWildcard(_TransactionHelper);

var _DBWindow = require('../../Utilities/DBWindow');

var _DBWindow2 = _interopRequireDefault(_DBWindow);

var _TxnTypeConstant = require('../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _TxnPaymentStatusConstants = require('../../Constants/TxnPaymentStatusConstants');

var _TxnPaymentStatusConstants2 = _interopRequireDefault(_TxnPaymentStatusConstants);

var _trashableReact = require('trashable-react');

var _trashableReact2 = _interopRequireDefault(_trashableReact);

var _ButtonDropdown = require('./ButtonDropdown');

var _ButtonDropdown2 = _interopRequireDefault(_ButtonDropdown);

var _CommonUtility = require('../../Utilities/CommonUtility');

var _CommonUtility2 = _interopRequireDefault(_CommonUtility);

var _Description = require('@material-ui/icons/Description');

var _Description2 = _interopRequireDefault(_Description);

var _FirstScreen = require('./UIControls/FirstScreen');

var _FirstScreen2 = _interopRequireDefault(_FirstScreen);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var itemCache = new _ItemCache2.default();

var ProductContainer = function (_Component) {
	(0, _inherits3.default)(ProductContainer, _Component);

	function ProductContainer(props) {
		(0, _classCallCheck3.default)(this, ProductContainer);

		var _this = (0, _possibleConstructorReturn3.default)(this, (ProductContainer.__proto__ || (0, _getPrototypeOf2.default)(ProductContainer)).call(this, props));

		_this.addNewProduct = function () {
			window.itemNameGlobal = '';
			_CommonUtility2.default.loadFrameDiv('NewItems.html', function () {
				$('.serviceItemSwitch').hide();
			});
		};

		_this.importItems = function () {
			$('.sideNav .list-item.importItems').click();
		};

		_this.getApi = function (params) {
			_this.api = params.api;
			_this.api && _this.api.setRowData(_this.state.records);
		};

		_this.allItems = itemCache.getListOfItemsObject();
		_this.child = null;
		_this.filterItems = _this.filterItems.bind(_this);
		_this.onItemSelected = (0, _throttleDebounce.debounce)(50, _this.onItemSelected.bind(_this));
		_this.getData = _this.getData.bind(_this);
		_this.viewTransaction = _this.viewTransaction.bind(_this);
		_this.refreshItemCells = _this.refreshItemCells.bind(_this);
		_this.contextMenu = [{
			title: 'View/Edit',
			cmd: _this.viewTransaction,
			visible: true,
			id: 'edit'
		}, {
			title: 'Delete',
			cmd: function cmd(row) {
				return TransactionHelper.deleteTransaction(row.id, row.txnTypeConstant);
			},
			visible: true,
			id: 'delete'
		}, {
			title: 'Open PDF',
			cmd: function cmd(row) {
				return TransactionHelper.openPDF(row.id, row.txnTypeConstant);
			},
			visible: true,
			id: 'openPDF'
		}, {
			title: 'Preview',
			cmd: function cmd(row) {
				return TransactionHelper.preview(row.id, row.txnTypeConstant);
			},
			visible: true,
			id: 'preview'
		}, {
			title: 'Print',
			cmd: function cmd(row) {
				return TransactionHelper.print(row.id, row.txnTypeConstant);
			},
			visible: true,
			id: 'print'
		}, {
			title: 'Return',
			cmd: function cmd(row) {
				return TransactionHelper.returnTransaction(row.id, row.txnTypeConstant);
			},
			visible: true,
			id: 'rtn'
		}, {
			title: 'Receive Payment',
			cmd: function cmd(row) {
				return TransactionHelper.receiveOrMakePayment(row.id, row.txnTypeConstant);
			},
			visible: true,
			id: 'receivePayment'
		}, {
			title: 'Payment History',
			cmd: function cmd(row) {
				return window.showPaymentHistory(row.id + ':' + row.txnTypeConstant);
			},
			visible: true,
			id: 'paymentHistory'
		}];
		_this.state = {
			records: [],
			items: _this.allItems,
			currentItem: null,
			columnDefs: [{
				field: 'color',
				headerName: '',
				width: 30,
				suppressMenu: false,
				getQuickFilterText: function getQuickFilterText() {
					return '';
				},
				suppressFilter: true,
				cellRenderer: function cellRenderer(params) {
					return params.value;
				}
			}, {
				field: 'typeId',
				filter: _MultiSelectionFilter2.default,
				filterOptions: _TxnTypeConstant2.default.getFilterOptions(),
				cellRenderer: function cellRenderer(params) {
					return params.data.type;
				},
				headerName: 'TYPE',
				width: 110
			}, {
				field: 'name',
				filter: _StringFilter2.default,
				headerName: 'NAME',
				width: 180
			}, {
				field: 'date',
				headerName: 'DATE',
				width: 100,
				sort: 'desc',
				sortingOrder: ['desc', 'asc', null],
				filter: _DateFilter2.default,
				enableFilter: true,
				getQuickFilterText: function getQuickFilterText(params) {
					return _MyDate2.default.getDate('d/m/y', params.value);
				},
				cellRenderer: function cellRenderer(params) {
					return _MyDate2.default.getDate('d/m/y', params.value);
				}
			}, {
				field: 'quantity',
				headerName: 'QUANTITY',
				getQuickFilterText: function getQuickFilterText(params) {
					var _params$data = params.data,
					    quantity = _params$data.quantity,
					    intQuantity = _params$data.intQuantity,
					    freeQuantity = _params$data.freeQuantity;

					return quantity + ' ' + (intQuantity + freeQuantity);
				},
				valueGetter: function valueGetter(params) {
					var _params$data2 = params.data,
					    intQuantity = _params$data2.intQuantity,
					    freeQuantity = _params$data2.freeQuantity;

					return intQuantity + freeQuantity;
				},
				cellRenderer: function cellRenderer(params) {
					return params.data.quantity;
				},
				width: 100,
				filter: _NumberFilter2.default
			}, {
				field: 'price',
				headerName: 'PRICE/ UNIT',
				width: 100,
				filter: _NumberFilter2.default,
				cellClass: 'alignRight',
				getQuickFilterText: function getQuickFilterText(params) {
					return params.value < 0 ? '-' : params.value;
				},
				cellRenderer: function cellRenderer(params) {
					if (params.value < 0) {
						return '-';
					} else {
						return _MyDouble2.default.getAmountWithDecimalAndCurrencyWithoutSign(params.value);
					}
				}
			}, {
				field: 'paymentStatusId',
				filter: _MultiSelectionFilter2.default,
				filterOptions: _TxnPaymentStatusConstants2.default.getFilterOptions(),
				cellRenderer: function cellRenderer(params) {
					return params.data.paymentStatus;
				},
				headerName: 'STATUS',
				width: 100
			}, {
				field: 'id',
				headerName: '',
				width: 30,
				getQuickFilterText: function getQuickFilterText() {
					return '';
				},
				suppressFilter: true,
				cellRenderer: function cellRenderer() {
					return '<div class="contextMenuDots"></div>';
				}
			}],
			contextMenu: _this.contextMenu,
			showSearch: false
		};

		_this._loaded = [];
		_this._perPage = 100;
		_this._id = null;
		_this.requestQueue = {};
		return _this;
	}

	(0, _createClass3.default)(ProductContainer, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			window.onResume = this.onResume.bind(this);
			TransactionHelper.bindEventsForPreviewDialog();
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			TransactionHelper.unBindEventsForPreviewDialog();
			delete window.onResume;
		}
	}, {
		key: 'onResume',
		value: function onResume() {
			var _this2 = this;

			this.props.onResume && this.props.onResume();
			itemCache.reloadItemCache();
			this.allItems = itemCache.getListOfItemsObject();
			if (this.state.currentItem) {
				var currentItem = this.allItems.find(function (item) {
					return _this2.state.currentItem.getItemId() == item.getItemId();
				});
				this.setState({ currentItem: currentItem });
			}
			this.filterItems(this.state.searchText);
			this.onItemSelected(null, true);
			this.props.onResume && this.props.onResume();
			TransactionHelper.bindEventsForPreviewDialog();
		}
	}, {
		key: 'viewTransaction',
		value: function viewTransaction(row) {
			if (!row.id) {
				row = row.data;
			}
			TransactionHelper.viewTransaction(row.id, row.txnTypeConstant);
		}
	}, {
		key: 'getData',
		value: function getData(event, response) {
			if (response.meta.key != this.currentRequestKey) {
				return false;
			}
			if (response.meta.next && response.data.length == 100) {
				var next = response.meta.next;
				this.loadTransactions(next.id, next.start, next.noOfRecords, next.key);
			}

			var data = response.data.map(function (row) {
				row.date = new Date(row.date);
				return row;
			});
			if (response.meta.next) {
				this.api && this.api.setRowData(data);
			} else {
				this.api && this.api.updateRowData({ add: data });
			}
		}
	}, {
		key: 'loadTransactions',
		value: function loadTransactions(id, start, noOfRecords, key, next) {
			var _this3 = this;

			this.props.registerPromise(_DBWindow2.default.send(_IPCActions.GET_ITEM_TRANSACTIONS, {
				id: id,
				start: start,
				noOfRecords: noOfRecords,
				key: key,
				next: next
			})).then(function (data) {
				return _this3.getData(null, data);
			});
		}
	}, {
		key: 'onItemSelected',
		value: function onItemSelected(row) {
			var fromSync = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

			if (!fromSync) {
				if (!row.node.selected) {
					return false;
				}
				var itemId = row.data.getItemId();
				/* do not reload current item */
				// if (this._id == itemId) {
				// 	// return false;
				// }

				this._id = itemId;
				this.setState({ currentItem: row.data });
			}
			this.currentRequestKey = Math.random() * 1000000000000000000;
			this.loadTransactions(this._id, 0, this._perPage, this.currentRequestKey, {
				id: this._id,
				start: this._perPage,
				key: this.currentRequestKey,
				noOfRecords: -1
			});
		}
	}, {
		key: 'filterItems',
		value: function filterItems(searchText) {
			if (!searchText) {
				this.setState({ items: this.allItems, searchText: '' });
				return false;
			}
			var query = searchText.toLowerCase();
			var filteredItems = this.allItems.filter(function (item) {
				var itemName = item.getItemName().toLowerCase();
				var flag = itemName.includes(query);
				if (flag) {
					return true;
				}
				var itemCode = item.getItemCode();
				return itemCode && itemCode.toLowerCase().includes(query);
			});
			var records = this.state.records;
			var currentItem = this.state.currentItem;
			if (!filteredItems.length) {
				this.api.setRowData([]);
				records = [];
				currentItem = null;
			}
			this.setState({
				records: records,
				currentItem: currentItem,
				items: filteredItems,
				searchText: searchText
			});
		}
	}, {
		key: 'filterContextMenu',
		value: function filterContextMenu(row) {
			var menuItems = TransactionHelper.filterContextMenu([].concat((0, _toConsumableArray3.default)(this.contextMenu)), {
				data: {
					txnTypeConstant: TxnTypeConstant.getTxnType(row.data.typeId),
					paymentStatus: row.data.paymentStatusId
				}
			});
			this.setState({ contextMenu: menuItems });
		}
	}, {
		key: 'onTransactionSelected',
		value: function onTransactionSelected(row) {
			this.currentTransaction = row;
		}
	}, {
		key: 'refreshItemCells',
		value: function refreshItemCells(selectedUnits, activeOrInactive) {
			this.child.refreshItemCells(selectedUnits, activeOrInactive);
		}
	}, {
		key: 'getMenuOptions',
		value: function getMenuOptions() {
			var menuOptions = [];
			menuOptions.push({
				buttonText: 'Add Item',
				buttonIcon: _react2.default.createElement(
					'span',
					null,
					'+'
				),
				onClick: this.addNewProduct
			});
			menuOptions.push({
				buttonText: 'Import Items',
				buttonIcon: _react2.default.createElement(_Description2.default, { color: 'primary', fontSize: 'inherit' }),
				onClick: this.importItems
			});
			return menuOptions;
		}
	}, {
		key: 'render',
		value: function render() {
			var _this4 = this;

			var gridOptions = {
				enableFilter: true,
				enableSorting: true,
				rowSelection: 'single',
				columnDefs: this.state.columnDefs,
				rowData: [],
				rowHeight: 40,
				headerHeight: 40,
				rowClass: 'customVerticalBorder',
				overlayNoRowsTemplate: 'No transactions to show',
				onRowSelected: this.onTransactionSelected.bind(this),
				onRowDoubleClicked: this.viewTransaction.bind(this)
			};
			return _react2.default.createElement(
				'div',
				{ className: 'd-flex align-items-stretch flex-container' },
				this.allItems.length === 0 && _react2.default.createElement(_FirstScreen2.default, {
					image: '../inlineSVG/first-screens/product.svg',
					text: 'Add Products/Items you sell or purchase to manage your full Stock Inventory.',
					buttonLabel: 'Add Your First Product',
					onClick: this.addNewProduct
				}),
				this.allItems.length > 0 && _react2.default.createElement(
					_react.Fragment,
					null,
					_react2.default.createElement(
						'div',
						{ className: 'flex-column d-flex left-column' },
						_react2.default.createElement(
							'div',
							{ className: 'listheaderDiv d-flex mt-20 mb-10' },
							_react2.default.createElement(
								'div',
								{ className: 'searchDiv' },
								_react2.default.createElement(_SearchBox2.default, {
									throttle: 500,
									filter: this.filterItems,
									collapsed: !this.state.showSearch,
									onCollapsedClick: function onCollapsedClick() {
										_this4.setState({ showSearch: true });
									},
									onInputBlur: function onInputBlur() {
										_this4.setState({ showSearch: false });
									}
								})
							),
							_react2.default.createElement(
								'div',
								{ className: 'buttonDiv flex-1 d-flex justify-content-end' },
								_react2.default.createElement(_ButtonDropdown2.default, {
									classNames: 'mr-0',
									menuOptions: this.getMenuOptions(),
									addPadding: true,
									collapsed: this.state.showSearch
								})
							),
							_react2.default.createElement(_ActiveInactiveItems.ActivateInactivateUnitItems, {
								refreshItemCells: this.refreshItemCells,
								items: this.state.items
							})
						),
						_react2.default.createElement(_Items2.default, {
							onRef: function onRef(ref) {
								return _this4.child = ref;
							},
							onItemSelected: this.onItemSelected,
							tabs: this.props.tabs,
							items: this.state.items
						})
					),
					_react2.default.createElement(
						'div',
						{ className: 'd-flex flex-column flex-grow-1 right-column' },
						_react2.default.createElement(_ItemDetail2.default, { currentItem: this.state.currentItem }),
						_react2.default.createElement(_Grid2.default, {
							title: 'Transactions',
							quickFilter: true,
							classes: 'alternateRowColor customVerticalBorder gridRowHeight40',
							filterContextMenu: this.filterContextMenu.bind(this),
							contextMenu: this.state.contextMenu,
							height: '100%',
							getApi: this.getApi,
							gridOptions: gridOptions
						})
					)
				)
			);
		}
	}]);
	return ProductContainer;
}(_react.Component);

ProductContainer.propTypes = {
	registerPromise: _propTypes2.default.func,
	onResume: _propTypes2.default.func,
	isItemUnitEnable: _propTypes2.default.bool,
	tabs: _propTypes2.default.array
};
exports.default = (0, _trashableReact2.default)(ProductContainer);