Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _Modal = require('./Modal');

var _Modal2 = _interopRequireDefault(_Modal);

var _SearchBox = require('./SearchBox');

var _SearchBox2 = _interopRequireDefault(_SearchBox);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SelectUnitModal = function (_React$Component) {
	(0, _inherits3.default)(SelectUnitModal, _React$Component);

	function SelectUnitModal(props) {
		(0, _classCallCheck3.default)(this, SelectUnitModal);

		var _this = (0, _possibleConstructorReturn3.default)(this, (SelectUnitModal.__proto__ || (0, _getPrototypeOf2.default)(SelectUnitModal)).call(this, props));

		_this.onClose = _this.onClose.bind(_this);
		_this.onSave = _this.onSave.bind(_this);
		_this.getApi = _this.getApi.bind(_this);
		_this.api = null;
		_this.quickFilter = _this.quickFilter.bind(_this);
		_this.title = 'Select Items';
		return _this;
	}

	(0, _createClass3.default)(SelectUnitModal, [{
		key: 'onClose',
		value: function onClose() {
			this.props.onClose && this.props.onClose();
		}
	}, {
		key: 'quickFilter',
		value: function quickFilter(text) {
			this.api.setQuickFilter(text);
		}
	}, {
		key: 'onSave',
		value: function onSave() {
			var selected = this.api.getSelectedRows().map(function (item) {
				return item.getItemId();
			});
			this.props.onSave && this.props.onSave(selected);
		}
	}, {
		key: 'getApi',
		value: function getApi(params) {
			this.api = params.api;
		}
	}, {
		key: 'render',
		value: function render() {
			if (this.props.activeInactiveTitle) {
				this.title = this.props.activeInactiveTitle;
			}
			var gridOptions = {
				enableFilter: false,
				enableSorting: true,
				rowSelection: 'multiple',
				rowData: this.props.items,
				columnDefs: [{
					field: 'itemName',
					headerName: 'NAME',
					headerCheckboxSelection: true,
					headerCheckboxSelectionFilteredOnly: true,
					checkboxSelection: true,
					width: 400
				}],
				getRowNodeId: function getRowNodeId(data) {
					return data.getItemId();
				}
			};
			if (!this.props.isService) {
				gridOptions.columnDefs.push({
					field: 'itemStockQuantity',
					comparator: function comparator(a, b) {
						return a - b;
					},
					headerName: 'QUANTITY',
					width: 100,
					cellClass: 'alignRight pr-20',
					headerClass: 'alignRight pr-20',
					cellRenderer: function cellRenderer(params) {
						return _MyDouble2.default.getQuantityWithDecimal(params.value);
					}
				});
			}
			return _react2.default.createElement(
				_Modal2.default,
				{
					height: '610px',
					width: '600px',
					onClose: this.onClose,
					isOpen: this.props.isOpen,
					title: this.title
				},
				_react2.default.createElement(
					'div',
					{ className: 'modalBody' },
					_react2.default.createElement(
						'div',
						{ className: 'mb-10 mt-10' },
						_react2.default.createElement(_SearchBox2.default, { throttle: 300, filter: this.quickFilter })
					),
					_react2.default.createElement(
						'div',
						{
							className: 'd-flex flex-column',
							style: { height: '450px', width: '100%' }
						},
						_react2.default.createElement(_Grid2.default, {
							notSelectFirstRow: this.props.notSelectFirstRow,
							gridKey: 'selectItemsModalGridKey',
							getApi: this.getApi,
							classes: 'noBorder',
							gridOptions: gridOptions
						})
					),
					this.props.activeInactiveMessage && _react2.default.createElement(
						'div',
						{ className: 'activeInactiveMessage' },
						this.props.activeInactiveMessage
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'modalFooter align-items-center justify-content-end d-flex' },
					_react2.default.createElement(
						'button',
						{
							onClick: this.onSave,
							value: 'Select Items',
							className: 'modal-footer-button terminalButton'
						},
						this.props.children
					)
				)
			);
		}
	}]);
	return SelectUnitModal;
}(_react2.default.Component);

exports.default = SelectUnitModal;