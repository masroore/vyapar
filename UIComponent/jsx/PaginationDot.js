Object.defineProperty(exports, "__esModule", {
  value: true
});

var _assign = require('babel-runtime/core-js/object/assign');

var _assign2 = _interopRequireDefault(_assign);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {
    height: 24,
    width: 24,
    cursor: 'pointer',
    border: 0,
    background: 'none',
    padding: 0,
    outline: 'none'
  },
  dot: {
    backgroundColor: "#99bed1",
    // '#e4e6e7',
    height: 18,
    width: 18,
    borderRadius: 9,
    margin: 3
  },
  active: {
    backgroundColor: '#319fd6'
  }
};

var PaginationDot = function (_React$Component) {
  (0, _inherits3.default)(PaginationDot, _React$Component);

  function PaginationDot() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, PaginationDot);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = PaginationDot.__proto__ || (0, _getPrototypeOf2.default)(PaginationDot)).call.apply(_ref, [this].concat(args))), _this), _this.handleClick = function (event) {
      _this.props.onClick(event, _this.props.index);
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(PaginationDot, [{
    key: 'render',
    value: function render() {
      var active = this.props.active;


      var styleDot = void 0;

      if (active) {
        styleDot = (0, _assign2.default)({}, styles.dot, styles.active);
      } else {
        styleDot = styles.dot;
      }

      return _react2.default.createElement(
        'button',
        { type: 'button', style: styles.root, onClick: this.handleClick },
        _react2.default.createElement('div', { style: styleDot })
      );
    }
  }]);
  return PaginationDot;
}(_react2.default.Component);

exports.default = PaginationDot;