Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _styles = require('@material-ui/core/styles');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles() {
	return {
		button: {
			color: '#097aa8',
			fontSize: 12,
			marginLeft: 12
		}
	};
};

var AddNameButton = function (_React$Component) {
	(0, _inherits3.default)(AddNameButton, _React$Component);

	function AddNameButton() {
		(0, _classCallCheck3.default)(this, AddNameButton);
		return (0, _possibleConstructorReturn3.default)(this, (AddNameButton.__proto__ || (0, _getPrototypeOf2.default)(AddNameButton)).apply(this, arguments));
	}

	(0, _createClass3.default)(AddNameButton, [{
		key: 'render',
		value: function render() {
			var classes = this.props.classes;

			return _react2.default.createElement(
				_Button2.default,
				{
					variant: 'contained',
					color: 'default',
					onClick: AddNameButton.addNewParty,
					className: (0, _classnames2.default)(classes.button, this.props.classNames ? this.props.classNames : '')
				},
				this.props.children
			);
		}
	}], [{
		key: 'addNewParty',
		value: function addNewParty() {
			var CommonUtility = require('../../Utilities/CommonUtility');
			window.userNameGlobal = '';
			window.MyAnalytics.pushEvent('Add Party Open');
			CommonUtility.loadFrameDiv('NewContact.html');
		}
	}]);
	return AddNameButton;
}(_react2.default.Component);

AddNameButton.propTypes = {
	classes: _propTypes2.default.object,
	classNames: _propTypes2.default.string,
	children: _propTypes2.default.oneOfType([_propTypes2.default.arrayOf(_propTypes2.default.node), _propTypes2.default.node])
};

exports.default = (0, _styles.withStyles)(styles)(AddNameButton);