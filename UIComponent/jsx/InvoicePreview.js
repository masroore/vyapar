Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _create = require('babel-runtime/core-js/object/create');

var _create2 = _interopRequireDefault(_create);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _TransactionHTMLGenerator = require('../../ReportHTMLGenerator/TransactionHTMLGenerator');

var _TransactionHTMLGenerator2 = _interopRequireDefault(_TransactionHTMLGenerator);

var _InvoiceTheme = require('../../Constants/InvoiceTheme');

var _InvoiceTheme2 = _interopRequireDefault(_InvoiceTheme);

var _PDFHandler = require('../../Utilities/PDFHandler');

var _PDFHandler2 = _interopRequireDefault(_PDFHandler);

var _CollectPayment = require('./CollectPayment');

var _CollectPayment2 = _interopRequireDefault(_CollectPayment);

var _FirmCache = require('../../Cache/FirmCache');

var _FirmCache2 = _interopRequireDefault(_FirmCache);

var _NameCache = require('../../Cache/NameCache');

var _NameCache2 = _interopRequireDefault(_NameCache);

var _SettingCache = require('../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

var _TransactionFactory = require('../../BizLogic/TransactionFactory');

var _TransactionFactory2 = _interopRequireDefault(_TransactionFactory);

var _BaseTransaction = require('../../BizLogic/BaseTransaction');

var _BaseTransaction2 = _interopRequireDefault(_BaseTransaction);

var _MessageDraftLogic = require('../../BizLogic/MessageDraftLogic');

var _MessageDraftLogic2 = _interopRequireDefault(_MessageDraftLogic);

var _SettingsModel = require('../../Models/SettingsModel');

var _SettingsModel2 = _interopRequireDefault(_SettingsModel);

var _Queries = require('../../Constants/Queries');

var _Queries2 = _interopRequireDefault(_Queries);

var _isOnline = require('is-online');

var _isOnline2 = _interopRequireDefault(_isOnline);

var _TextField = require('@material-ui/core/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _PrimaryButton = require('../../UIComponent/jsx/UIControls/PrimaryButton');

var _PrimaryButton2 = _interopRequireDefault(_PrimaryButton);

var _DecimalInputFilter = require('../../Utilities/DecimalInputFilter');

var _DecimalInputFilter2 = _interopRequireDefault(_DecimalInputFilter);

var _TransactionUtil = require('../../Utilities/TransactionUtil');

var _TransactionUtil2 = _interopRequireDefault(_TransactionUtil);

var _TxnMessageFormatter = require('../../BizLogic/TxnMessageFormatter');

var _TxnMessageFormatter2 = _interopRequireDefault(_TxnMessageFormatter);

var _Modal = require('./Modal');

var _Modal2 = _interopRequireDefault(_Modal);

var _analyticsHelper = require('../../Utilities/analyticsHelper');

var _analyticsHelper2 = _interopRequireDefault(_analyticsHelper);

var _TxnTypeConstant = require('../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _styles = require('@material-ui/core/styles');

var _ImageHelper = require('../../Utilities/ImageHelper');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StringConstants = require('../../Constants/StringConstants');

var themes = [{ key: 'THEME_10', value: 'GST Theme 1' }, { key: 'THEME_7', value: 'GST Theme 2' }, { key: 'THEME_9', value: 'GST Theme 3' }, { key: 'THEME_5', value: 'GST Theme 4' }, { key: 'THEME_6', value: 'GST Theme 5' }, { key: 'THEME_8', value: 'GST Theme 6' }, { key: 'THEME_11', value: 'Double Divine' }, { key: 'THEME_12', value: 'French Elite' }, { key: 'THEME_1', value: 'Theme 1' }, { key: 'THEME_2', value: 'Theme 2' }, { key: 'THEME_3', value: 'Theme 3' }, { key: 'THEME_4', value: 'Theme 4' }];
var styles = {
	'@global': {
		'.preview-contents .invoicePreviewEditSection': {
			minWidth: '70%',
			maxWidth: '100%',
			display: 'inline-block',
			cursor: 'pointer',
			position: 'relative',
			'&:hover,&.newEditSection': {
				outline: '1px dashed #BDBDBD',
				backgroundColor: '#DEE1E6',
				'&::after': {
					content: "''",
					position: 'absolute',
					top: '0',
					right: '0',
					height: '1.5rem',
					width: '1.5rem',
					backgroundColor: 'white',
					borderRadius: '1rem',
					border: '2px solid #E1E1E1',
					transform: 'translate(50%, -50%)',
					backgroundImage: 'url(../inlineSVG/baseline-edit.svg)',
					backgroundPosition: 'center',
					zIndex: 1
				}
			}
		}
	}
};
var muiTheme = (0, _styles.createMuiTheme)({
	palette: {
		primary: {
			main: '#1789FC'
		}
	}
});
var editSectionModalHeaders = {
	firmDetails: 'Enter your Business details to be printed on your invoice header',
	payToDetails: 'Enter your Bank details to be printed on your Invoice',
	termsAndConditions: 'Enter Terms and Conditions to be printed on Invoice',
	signatureLogo: 'Upload your Signature image. This Signature will be printed on your invoices.',
	firmLogo: 'Upload your Business Logo. This logo will be printed on your invoices.'
};
var colors = ['#948FE3', '#097aa8', '#A3A3A3', '#737373', '#999965', '#5387EA', '#00B4F1', '#068708', '#8EC430', '#61290E', '#7D1378', '#993365', '#933514', '#A84818', '#A643D1', '#CA0086', '#DEAE66', '#D39F53', '#EB90D3', '#F9921C', '#C5060E', '#EB4618', '#4C1E0D', '#ffffff'];

var doubleColorCombo = [_InvoiceTheme2.default.DOUBLE_THEME_COLOR_COMBO_1, _InvoiceTheme2.default.DOUBLE_THEME_COLOR_COMBO_2, _InvoiceTheme2.default.DOUBLE_THEME_COLOR_COMBO_3, _InvoiceTheme2.default.DOUBLE_THEME_COLOR_COMBO_4, _InvoiceTheme2.default.DOUBLE_THEME_COLOR_COMBO_5];

var messageConfigEnabledTxns = [_TxnTypeConstant2.default.TXN_TYPE_SALE, _TxnTypeConstant2.default.TXN_TYPE_PURCHASE, _TxnTypeConstant2.default.TXN_TYPE_CASHIN, _TxnTypeConstant2.default.TXN_TYPE_CASHOUT, _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN, _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN, _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER, _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE, _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER, _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN];

var InvoicePreview = function (_React$Component) {
	(0, _inherits3.default)(InvoicePreview, _React$Component);

	function InvoicePreview(props) {
		(0, _classCallCheck3.default)(this, InvoicePreview);

		var _this = (0, _possibleConstructorReturn3.default)(this, (InvoicePreview.__proto__ || (0, _getPrototypeOf2.default)(InvoicePreview)).call(this, props));

		_initialiseProps.call(_this);

		_this.updateOpenPreviewCount();
		var settingCache = new _SettingCache2.default();

		var firmCache = new _FirmCache2.default();

		var baseTransaction = new _BaseTransaction2.default();

		_this.nameCache = new _NameCache2.default();
		_this.transactionId = _this.props.transactionId;
		_this.baseTxn = baseTransaction.getTransactionFromId(_this.transactionId);
		_this.firmObj = firmCache.getFirmById(_this.baseTxn.getFirmId());
		_this.settingsModel = new _SettingsModel2.default();
		_this.settingCache = settingCache;
		_this.transactionHTMLGenerator = new _TransactionHTMLGenerator2.default();

		_this.nameObj = _this.nameCache.findNameObjectByNameId(_this.baseTxn.getNameId());
		_this.pdfHandler = new _PDFHandler2.default();
		_this.selectedValue = {};

		var selectedTheme = settingCache.getTxnPDFTheme();
		var previewHtml = _this.transactionHTMLGenerator.getTransactionHTMLLayoutForPreview(_this.baseTxn, selectedTheme, settingCache.getTxnPDFThemeColor());
		var canShowCollectPaymentScreen = _TransactionUtil2.default.isUPIEnabledForTxnType(_this.baseTxn.getTxnType());
		_this.isValidCountryForSms = settingCache.isCurrentCountryIndia();
		_this.isGSTEnabled = false;
		_this.isTINEnabled = false;
		if (settingCache.isTINNumberEnabled()) {
			if (settingCache.getGSTEnabled()) {
				_this.isGSTEnabled = true;
			} else {
				_this.isTINEnabled = true;
			}
		}
		_this.getDefaultTermsAndConditions = function () {
			var defaultTermsAndConditions = '';
			switch (this.baseTxn.txnType) {
				case _TxnTypeConstant2.default.TXN_TYPE_SALE:
					defaultTermsAndConditions = this.settingCache.getTermsAndConditionSaleInvoivce();
					break;
				case _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER:
					defaultTermsAndConditions = this.settingCache.getTermsAndConditionSaleOrder();
					break;
				case _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE:
					defaultTermsAndConditions = this.settingCache.getTermsAndConditionEstimateQuotation();
					break;
				case _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN:
					defaultTermsAndConditions = this.settingCache.getTermsAndConditionDeliveryChallan();
					break;
			}
			return defaultTermsAndConditions;
		};
		_this.state = {
			previewHtml: previewHtml,
			currentSelectedPdfTheme: selectedTheme,
			currentSelectedThemeColor: settingCache.getTxnPDFThemeColor(),
			selectedDoubleColorComboId: settingCache.getTxnPDFDoubleThemeColor(),
			disableInvoicePreview: settingCache.getIsInvoicePreviewDisabled(),
			selectedTheme: 'THEME_' + selectedTheme,
			canCollectPayment: canShowCollectPaymentScreen,
			canShowExpandedView: false,
			canShowCollectPaymentModal: false,
			showPartyNumberModal: false,
			activeEditModal: ''
		};
		_this.generateHiddenBrowserWindowPdf();
		_this.invoicePreviewRef = _react2.default.createRef();
		_this.invoicePreviewContentsRef = _react2.default.createRef();
		_this.adjustWidthForPreview();
		$('.transactionStyle').css('background-color', '#E6E6EA');

		_this.editSectionModalTextValues = (0, _create2.default)(null);
		return _this;
	}

	(0, _createClass3.default)(InvoicePreview, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			document.addEventListener('mousedown', this.handleClickOutside);
			this.addEventListenerOnEditSections();
			$('#defaultPage').hide();
		}
	}, {
		key: 'updateOpenPreviewCount',
		value: function updateOpenPreviewCount() {
			var LocalStorageHelper = require('../../Utilities/LocalStorageHelper');

			var _require = require('../../Constants/LocalStorageConstant'),
			    OPEN_INVOICE_PREVIEW_COUNT = _require.OPEN_INVOICE_PREVIEW_COUNT;

			var openCount = LocalStorageHelper.getValue(OPEN_INVOICE_PREVIEW_COUNT);
			openCount = Number(openCount);
			if (openCount <= StringConstants.maxOpenCountForEditPreviewSection) {
				LocalStorageHelper.setValue(OPEN_INVOICE_PREVIEW_COUNT, openCount + 1);
			}
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			document.removeEventListener('mousedown', this.handleClickOutside);
			this.removeEventListenerOnEditSections();
		}
	}, {
		key: 'componentDidUpdate',
		value: function componentDidUpdate(prevProps, prevState) {
			if (prevState.previewHtml !== this.state.previewHtml) {
				this.generateHiddenBrowserWindowPdf();
				this.addEventListenerOnEditSections();
			}
		}
	}, {
		key: 'toggleExpandedView',
		value: function toggleExpandedView() {
			var canShowExpandedView = this.state.canShowExpandedView;

			var expandedPreviewHTML = '';
			if (!canShowExpandedView) {
				expandedPreviewHTML = this.transactionHTMLGenerator.getTransactionHTMLLayout(this.baseTxn, this.state.currentSelectedPdfTheme, this.state.currentSelectedThemeColor, null, 2, this.state.selectedDoubleColorComboId);
			}
			this.expandedPreviewHTML = expandedPreviewHTML;
			this.setState({
				canShowExpandedView: !this.state.canShowExpandedView
			});
		}
	}, {
		key: 'checkIfSystemOnline',
		value: function checkIfSystemOnline(callback) {
			(0, _isOnline2.default)().then(function (online) {
				if (online) {
					callback();
				} else {
					ToastHelper.error('No internet connection. Connect to your internet and try again.');
				}
			});
		}
	}, {
		key: 'emailPdf',
		value: function emailPdf() {
			var _buildEmailContent = this.buildEmailContent(this.baseTxn),
			    subject = _buildEmailContent.subject,
			    message = _buildEmailContent.message;

			this.updateThemeSettings();
			this.pdfHandler.sharePDF(this.nameObj.getEmail(), null, subject, message, 'invoice');
		}
	}, {
		key: 'sendSms',
		value: function sendSms() {
			this.updateThemeSettings();
			var messageDraftLogic = new _MessageDraftLogic2.default();
			messageDraftLogic.sendTransactionSMSToParty(this.baseTxn, true, true, this.selectedValue.partyNumber);
		}
	}, {
		key: 'savePdf',
		value: function savePdf() {
			this.updateThemeSettings();
			var transDetails = this.getTransactionDetails();
			this.pdfHandler.savePDF({
				type: transDetails.customHeader,
				fromDate: transDetails.txnDate
			});
		}
	}, {
		key: 'toggleInvoicePreview',
		value: function toggleInvoicePreview() {
			var previewDisabled = !this.state.disableInvoicePreview;
			this.setState({
				disableInvoicePreview: previewDisabled
			});
			previewDisabled = previewDisabled ? 1 : 0;
			this.settingsModel.setSettingKey(_Queries2.default.SETTING_DISABLE_INVOICE_PREVIEW);
			this.settingsModel.setSettingValue(previewDisabled);
			this.settingsModel.UpdateSetting(previewDisabled, {
				nonSyncableSetting: true
			});
		}
	}, {
		key: 'updateThemeSettings',
		value: function updateThemeSettings() {
			this.settingsModel.setSettingKey(_Queries2.default.SETTING_TXN_PDF_THEME);
			this.settingsModel.setSettingValue(this.state.currentSelectedPdfTheme);
			this.settingsModel.UpdateSetting(this.state.currentSelectedPdfTheme);

			if (_InvoiceTheme2.default.getThemeType(this.state.currentSelectedPdfTheme) == _InvoiceTheme2.default.DOUBLE_COLOR_THEME) {
				this.settingsModel.setSettingKey(_Queries2.default.SETTING_TXN_PDF_DOUBLE_THEME_COLOR);
				this.settingsModel.setSettingValue(this.state.selectedDoubleColorComboId);
				this.settingsModel.UpdateSetting(this.state.selectedDoubleColorComboId);
			} else {
				this.settingsModel.setSettingKey(_Queries2.default.SETTING_TXN_PDF_THEME_COLOR);
				this.settingsModel.setSettingValue(this.state.currentSelectedThemeColor);
				this.settingsModel.UpdateSetting(this.state.currentSelectedThemeColor);
			}
		}
	}, {
		key: 'closePartyNumberModal',
		value: function closePartyNumberModal() {
			this.setState({
				showPartyNumberModal: false
			});
			if (this.selectedValue.partyNumber) {
				this.selectedValue.partyNumber = this.getSanitizedPhoneNumber(this.selectedValue.partyNumber);
				this.sendMessageCallback();
			}
		}
	}, {
		key: 'getPartyNumberForShare',
		value: function getPartyNumberForShare(callback) {
			this.sendMessageCallback = callback;
			this.setState({
				showPartyNumberModal: true
			});
		}
	}, {
		key: 'toggleCollectQRCodePaymentScreen',
		value: function toggleCollectQRCodePaymentScreen() {
			!this.state.canShowCollectPaymentModal && _analyticsHelper2.default.pushEvent('Click on collect payment');
			this.setState({
				canShowCollectPaymentModal: !this.state.canShowCollectPaymentModal
			});
		}
	}, {
		key: 'getTransactionDetails',
		value: function getTransactionDetails() {
			var MyDate = require('../../Utilities/MyDate');
			var txnType = this.baseTxn.getTxnType();

			var txnSubType = this.baseTxn.getTxnSubType();

			var transactionfactory = new _TransactionFactory2.default();

			var customHeader = transactionfactory.getTransTypeStringForTransactionPDF(Number(txnType), Number(txnSubType));

			var transDetails = {};

			var txnNumber = this.baseTxn.getTxnRefNumber();
			if (txnNumber) {
				var invoicePrefix = this.baseTxn.getInvoicePrefix();
				if (invoicePrefix) {
					customHeader += '_' + invoicePrefix + txnNumber;
				} else {
					customHeader += '_' + txnNumber;
				}
			}

			var txnDate = this.baseTxn.getTxnDate();
			txnDate = MyDate.getDate('d/m/y', txnDate);
			transDetails = { customHeader: customHeader, txnDate: txnDate };
			return transDetails;
		}
	}, {
		key: 'sendWhatsAppMsg',
		value: function sendWhatsAppMsg() {
			var messageFormatter = new _TxnMessageFormatter2.default();
			var phoneNumber = this.getSanitizedPhoneNumber(this.nameObj.getPhoneNumber()) || this.selectedValue.partyNumber;
			this.messageDetails = (0, _extends3.default)({
				to: phoneNumber
			}, messageFormatter.createMessageForTxn(this.baseTxn));
			this.updateThemeSettings();
			this.whatsAppMsgCall(this.messageDetails);
		}
	}, {
		key: 'getSanitizedPhoneNumber',
		value: function getSanitizedPhoneNumber(phoneNumber) {
			if (!phoneNumber) return;
			var settingCache = new _SettingCache2.default();
			phoneNumber = (phoneNumber || '').replace(/[^0-9]/g, '');
			if (settingCache.isCurrentCountryIndia()) {
				return '91' + phoneNumber.slice(-10);
			}
			return phoneNumber;
		}
	}, {
		key: 'updatePartyNumber',
		value: function updatePartyNumber() {
			var ErrorCode = require('../../Constants/ErrorCode');
			if (!this.selectedValue.partyNumber || this.selectedValue.partyNumber.length < 10) {
				ToastHelper.error(ErrorCode.ERROR_ENTER_CORRECT_PHONE_NUMBER);
				return;
			} else if (this.selectedValue.partyNumber.substr(0, 1) <= 5) {
				ToastHelper.error(ErrorCode.ERROR_CORRECT_PHONE_NUMBER_DIGITS);
				return;
			}
			if (this.nameObj.getFullName().toLowerCase() !== StringConstants.cashSale.toLowerCase()) {
				this.nameObj.setPhoneNumber(this.selectedValue.partyNumber);
				var statusCode = this.nameObj.updateNameToDBTransaction();
				if (statusCode !== ErrorCode.ERROR_NAME_SAVE_SUCCESS) {
					ToastHelper.error(ErrorCode.ERROR_NAME_UPDATE_FAILED);
					this.nameObj = this.nameCache.findNameObjectByNameId(this.baseTxn.getNameId());
				}
			}
			this.closePartyNumberModal();
		}
	}, {
		key: 'prepareMessageAndSend',
		value: function prepareMessageAndSend(callback) {
			var contactNo = this.getSanitizedPhoneNumber(this.nameObj.getPhoneNumber()) || this.selectedValue.partyNumber;
			if (!contactNo) {
				this.getPartyNumberForShare(callback);
			} else {
				return callback();
			}
		}
	}, {
		key: 'whatsAppMsgCall',
		value: function whatsAppMsgCall(msg, callback) {
			var send = require('../../Utilities/whatsapp').default;
			this.whatsAppBrowserWindow = send(msg, callback);
		}
	}, {
		key: 'handleTextboxChange',
		value: function handleTextboxChange(e) {
			this.selectedValue[e.target.name] = e.target.value;
		}
	}, {
		key: 'adjustWidthForPreview',
		value: function adjustWidthForPreview() {
			var _this2 = this;

			setTimeout(function () {
				var width = _this2.invoicePreviewRef.current.offsetWidth * 0.55;
				var pdfViewer = document.getElementsByClassName('pdf-viewer');
				if (pdfViewer.length === 0) {
					return;
				}
				var txnItemTableForPrint = pdfViewer[0].getElementsByClassName('txnItemTableForPrint');
				var pdfTransactionHTMLView = pdfViewer[0].getElementsByClassName('pdfTransactionHTMLView');
				if (txnItemTableForPrint.length > 0 && txnItemTableForPrint[0].offsetWidth > pdfTransactionHTMLView[0].offsetWidth) {
					pdfViewer[0].style.width = width - 20 + 'px';
					pdfTransactionHTMLView[0].style.width = txnItemTableForPrint.length > 0 ? txnItemTableForPrint[0].offsetWidth + 'px' : pdfTransactionHTMLView[0].offsetWidth + 'px';
				}
				pdfViewer[0].style.height = _this2.invoicePreviewContentsRef.current.offsetHeight + 100 + 'px';
			});
		}
	}, {
		key: 'updateFirmLogoOrSignature',
		value: function updateFirmLogoOrSignature(logoType) {
			var _this3 = this;

			var ErrorCode = require('../../Constants/ErrorCode');
			var firmCache = new _FirmCache2.default();
			var updateImage = function updateImage(imageName) {
				if (logoType == 'firmLogo') {
					_this3.firmObj.setFirmImagePath(imageName);
					if (_this3.firmObj.getFirmSignId()) {
						_this3.firmObj.setFirmSignImagePath('data:image/png;base64,' + _this3.firmObj.getFirmSignImagePath());
					}
				} else {
					_this3.firmObj.setFirmSignImagePath(imageName);
					if (_this3.firmObj.getFirmLogoId()) {
						_this3.firmObj.setFirmImagePath('data:image/png;base64,' + _this3.firmObj.getFirmImagePath());
					}
				}
				var statusCode = _this3.firmObj.updateFirmWithoutObject(false);
				if (statusCode == ErrorCode.ERROR_FIRM_UPDATE_SUCCESS) {
					ToastHelper.success(statusCode);
					try {
						logoType == 'firmLogo' && window.updateCompanyInfo();
					} catch (error) {}
				} else {
					ToastHelper.error(statusCode);
				}
				_this3.firmObj = firmCache.getFirmById(_this3.baseTxn.getFirmId());
				var previewHtml = _this3.transactionHTMLGenerator.getTransactionHTMLLayoutForPreview(_this3.baseTxn, _this3.state.currentSelectedPdfTheme, _this3.state.currentSelectedThemeColor);
				_this3.setState({
					previewHtml: previewHtml,
					activeEditModal: ''
				});
			};
			if (logoType == 'firmLogo' || logoType == 'signatureLogo') {
				var title = logoType == 'firmLogo' ? 'Select Firm Logo' : 'Select Signature Image';
				(0, _ImageHelper.openSaveImageDialog)({ title: title }, updateImage);
			}
		}
	}, {
		key: 'addEventListenerOnEditSections',
		value: function addEventListenerOnEditSections() {
			var _this4 = this;

			var editSections = document.getElementsByClassName('invoicePreviewEditSection');
			Array.prototype.forEach.call(editSections, function (editSection) {
				editSection.removeEventListener('click', _this4.handleClickOnEditSection);
				editSection.addEventListener('click', _this4.handleClickOnEditSection);
			});
		}
	}, {
		key: 'removeEventListenerOnEditSections',
		value: function removeEventListenerOnEditSections() {
			var _this5 = this;

			var editSections = document.getElementsByClassName('invoicePreviewEditSection');
			Array.prototype.forEach.call(editSections, function (editSection) {
				editSection.removeEventListener('click', _this5.handleClickOnEditSection);
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _this6 = this;

			if (_InvoiceTheme2.default.getThemeType(this.state.currentSelectedPdfTheme) == _InvoiceTheme2.default.DOUBLE_COLOR_THEME) {
				var dobleThemeColors = _InvoiceTheme2.default.getDoubleThemeColors(this.state.selectedDoubleColorComboId);
				var primary = dobleThemeColors.primary,
				    secondary = dobleThemeColors.secondary;
			}
			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement(
					'div',
					{
						className: 'invoice-preview container fullContainerHeight',
						ref: this.invoicePreviewRef
					},
					_react2.default.createElement(
						'div',
						{
							className: 'row invoice-header',
							style: { backgroundColor: 'white' }
						},
						_react2.default.createElement(
							'span',
							{ className: 'heading col-sm-5 font-medium' },
							'Preview'
						),
						_react2.default.createElement(
							'div',
							{ className: 'col-sm-6 invoice-preview-checkbox ml-20' },
							_react2.default.createElement(
								'label',
								{ className: 'input-checkbox' },
								_react2.default.createElement('input', {
									type: 'checkbox',
									onChange: this.toggleInvoicePreview.bind(this),
									checked: this.state.disableInvoicePreview
								}),
								_react2.default.createElement(
									'svg',
									{ width: '18', height: '18' },
									_react2.default.createElement('path', {
										className: 'checked',
										d: 'M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M7,14L2,9l1.4-1.4L7,11.2l7.6-7.6L16,5L7,14z'
									}),
									_react2.default.createElement('path', {
										className: 'indeterminate',
										d: 'M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M14,10H4V8h10V10z'
									}),
									_react2.default.createElement('path', {
										className: 'unchecked',
										fill: '#9e9e9e',
										d: 'M16,2v14H2V2H16 M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z'
									})
								)
							),
							_react2.default.createElement(
								'label',
								{ className: 'checkbox-txt' },
								'Do not show invoice preview again'
							)
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'row d-flex justify-content-between fullContainerHeight' },
						_react2.default.createElement(
							'div',
							{
								className: 'theme-changer',
								style: { backgroundColor: 'white', maxWidth: '20%' }
							},
							_react2.default.createElement(
								'div',
								{ className: 'theme-selector' },
								_react2.default.createElement(
									'span',
									{ className: 'sub-heading ml-20' },
									'Select Theme'
								),
								_react2.default.createElement(
									'ul',
									{ className: 'themes-list bg-white' },
									themes.map(function (themeItem, index) {
										return _react2.default.createElement(
											'li',
											{
												className: _this6.state.selectedTheme === themeItem.key ? 'selected' : '',
												onClick: _this6.changePdfTheme.bind(_this6, themeItem.key),
												key: themeItem.key
											},
											themeItem.value
										);
									})
								)
							),
							_react2.default.createElement(
								'div',
								{ className: 'color-selector' },
								_react2.default.createElement(
									'span',
									{ className: 'sub-heading' },
									'Select Color'
								),
								_react2.default.createElement(
									'div',
									{ className: 'selected-color' },
									_react2.default.createElement('span', {
										className: this.state.currentSelectedThemeColor === '#ffffff' ? 'selected-color-box border-white' : 'selected-color-box',
										style: _InvoiceTheme2.default.getThemeType(this.state.currentSelectedPdfTheme) == _InvoiceTheme2.default.DOUBLE_COLOR_THEME ? { background: 'linear-gradient(' + primary + ' 0%,' + primary + ' 50%, ' + secondary + ' 50%,' + secondary + ' 100%)' } : { backgroundColor: this.state.currentSelectedThemeColor }
									}),
									_react2.default.createElement(
										'span',
										null,
										'Selected'
									)
								),
								_react2.default.createElement(
									'div',
									{ className: 'choose-color mt-10' },
									_react2.default.createElement(
										'ul',
										null,
										' ',
										_InvoiceTheme2.default.getThemeType(this.state.currentSelectedPdfTheme) == _InvoiceTheme2.default.SINGLE_COLOR_THEME && colors.map(function (value, index) {
											return _react2.default.createElement('li', {
												onClick: _this6.changePdfColor.bind(_this6, value),
												className: value === '#ffffff' ? 'option-color-box border-white' : 'option-color-box',
												key: index,
												style: { backgroundColor: value }
											});
										}),
										_InvoiceTheme2.default.getThemeType(this.state.currentSelectedPdfTheme) == _InvoiceTheme2.default.DOUBLE_COLOR_THEME && doubleColorCombo.map(function (value, index) {
											var dobleThemeColors = _InvoiceTheme2.default.getDoubleThemeColors(value);
											var primary = dobleThemeColors.primary,
											    secondary = dobleThemeColors.secondary;

											return _react2.default.createElement('li', {
												onClick: _this6.changePdfColor.bind(_this6, null, value),
												className: value === '#ffffff' ? 'option-color-box border-white' : 'option-color-box',
												key: index,
												style: { background: 'linear-gradient(' + primary + ' 0%,' + primary + ' 50%, ' + secondary + ' 50%,' + secondary + ' 100%)' }
											});
										})
									)
								)
							)
						),
						_react2.default.createElement(
							'div',
							{ className: 'pdf-viewer bg-white' },
							_react2.default.createElement(
								'div',
								{ className: 'expand-icon-wrapper' },
								_react2.default.createElement('img', {
									src: '../inlineSVG/expand_icon.svg',
									className: 'expand-preview-icon',
									onClick: this.toggleExpandedView.bind(this)
								})
							),
							_react2.default.createElement('div', {
								ref: this.invoicePreviewContentsRef,
								className: 'preview-contents',
								dangerouslySetInnerHTML: { __html: this.state.previewHtml }
							})
						),
						_react2.default.createElement(
							'div',
							{ className: 'invoice-utilities' },
							this.state.canCollectPayment && _react2.default.createElement(
								'div',
								{ className: 'utility-btn' },
								_react2.default.createElement(
									'div',
									{
										className: 'collect-payment',
										onClick: this.toggleCollectQRCodePaymentScreen.bind(this)
									},
									_react2.default.createElement(
										'span',
										{ className: 'img-container' },
										_react2.default.createElement('img', {
											className: 'padding6 bg-white',
											src: '../inlineSVG/UPI-Logo-vector.svg'
										})
									),
									_react2.default.createElement(
										'span',
										{ className: 'color-white font-bold' },
										'Collect Payment'
									)
								)
							),
							_react2.default.createElement(
								'div',
								{ className: 'sharing-menu' },
								_react2.default.createElement(
									'span',
									{ className: 'sub-heading' },
									'Share Invoice'
								),
								_react2.default.createElement(
									'div',
									{ className: 'sharing-menu-options font-family-regular' },
									_react2.default.createElement(
										'div',
										{
											onClick: this.checkIfSystemOnline.bind(this, this.emailPdf.bind(this)),
											className: 'sharing-menu-item'
										},
										_react2.default.createElement('img', { src: '../inlineSVG/gmail.svg' }),
										_react2.default.createElement(
											'div',
											{ className: 'item-label' },
											'EMAIL'
										)
									),
									this.isValidCountryForSms && messageConfigEnabledTxns.includes(this.baseTxn.getTxnType()) && _react2.default.createElement(
										'div',
										{
											onClick: this.prepareMessageAndSend.bind(this, this.sendSms.bind(this)),
											className: 'sharing-menu-item'
										},
										_react2.default.createElement('img', { src: '../inlineSVG/baseline-sms-24px.svg' }),
										_react2.default.createElement(
											'div',
											{ className: 'item-label' },
											'SMS'
										)
									),
									_react2.default.createElement(
										'div',
										{
											ref: function ref(node) {
												_this6.node = node;
											},
											onClick: this.checkIfSystemOnline.bind(this, this.prepareMessageAndSend.bind(this, this.sendWhatsAppMsg.bind(this))),
											className: 'sharing-menu-item'
										},
										_react2.default.createElement('img', { src: '../inlineSVG/whatsapp.svg' }),
										_react2.default.createElement(
											'div',
											{ className: 'item-label' },
											'WHATSAPP'
										)
									)
								)
							),
							_react2.default.createElement(
								'div',
								{ className: 'utility-btn ' },
								_react2.default.createElement(
									'div',
									{
										className: 'print-invoice',
										onClick: this.printPdf.bind(this)
									},
									_react2.default.createElement(
										'span',
										{ className: 'img-container' },
										_react2.default.createElement('img', { src: '../inlineSVG/baseline-print-24px.svg' })
									),
									_react2.default.createElement(
										'span',
										{ className: 'color-white font-family-regular' },
										'Print Invoice'
									)
								)
							),
							_react2.default.createElement(
								'div',
								{ className: 'utility-btn' },
								_react2.default.createElement(
									'div',
									{
										className: 'download-pdf bg-white',
										onClick: this.savePdf.bind(this)
									},
									_react2.default.createElement(
										'span',
										{ className: 'img-container' },
										_react2.default.createElement('img', { src: '../inlineSVG/pdf.png' })
									),
									_react2.default.createElement(
										'span',
										{ className: 'font-family-regular' },
										'Download PDF'
									)
								),
								' '
							)
						)
					)
				),
				_react2.default.createElement(
					_Modal2.default,
					{
						height: '500px',
						width: '620px',
						isOpen: this.state.canShowCollectPaymentModal,
						canHideHeader: 'true',
						parentClassSelector: 'react-modal-container',
						onClose: this.toggleCollectQRCodePaymentScreen.bind(this),
						title: 'Collect Payments Online'
					},
					_react2.default.createElement(
						'div',
						{ className: 'modalBody collect-payment-modal' },
						_react2.default.createElement(_CollectPayment2.default, {
							closeFunc: this.toggleCollectQRCodePaymentScreen.bind(this),
							firmId: this.baseTxn.getFirmId(),
							callback: this.changePdfThemeAndColor.bind(this)
						})
					)
				),
				_react2.default.createElement(
					_Modal2.default,
					{
						isOpen: this.state.canShowExpandedView,
						canHideHeader: 'true',
						parentClassSelector: 'react-modal-container',
						onClose: this.toggleExpandedView.bind(this)
					},
					_react2.default.createElement(
						'div',
						{ id: 'expandedView', className: 'modalBody expanded-view-body mt-35' },
						_react2.default.createElement('div', {
							className: 'expanded-contents',
							dangerouslySetInnerHTML: { __html: this.expandedPreviewHTML }
						})
					)
				),
				_react2.default.createElement(
					_Modal2.default,
					{
						width: '450px',
						isOpen: this.state.showPartyNumberModal,
						canHideHeader: 'true',
						parentClassSelector: 'react-modal-container',
						onClose: this.closePartyNumberModal.bind(this),
						title: 'Please enter party number to send message'
					},
					_react2.default.createElement(
						'div',
						{ className: 'party-number-modal' },
						_react2.default.createElement(
							'div',
							{ className: 'party-number-container fullContainerHeight' },
							_react2.default.createElement(
								'div',
								{ className: 'row' },
								_react2.default.createElement(
									'div',
									{ className: 'col-sm-10' },
									_react2.default.createElement(_TextField2.default, {
										id: 'partyNumber',
										name: 'partyNumber',
										label: 'Party Number',
										type: 'text',
										style: { width: '100%' },
										onInput: function onInput(e) {
											return _DecimalInputFilter2.default.inputTextFilterForNumber(e.target, 10);
										},
										onChange: this.handleTextboxChange.bind(this)
									})
								),
								_react2.default.createElement(
									'div',
									{ className: 'col-sm-10 mt-20' },
									_react2.default.createElement(
										_PrimaryButton2.default,
										{
											style: { width: '100%' },
											onClick: this.updatePartyNumber.bind(this)
										},
										'Update Party Number'
									)
								)
							)
						)
					)
				),
				_react2.default.createElement(
					_styles.MuiThemeProvider,
					{ theme: muiTheme },
					_react2.default.createElement(
						_Modal2.default,
						{
							width: '350px',
							isOpen: !!editSectionModalHeaders[this.state.activeEditModal],
							canHideHeader: true,
							onAfterOpen: this.resetEditSectionModalTextValues,
							title: editSectionModalHeaders[this.state.activeEditModal],
							onClose: this.onClose
						},
						_react2.default.createElement(
							'form',
							{ className: 'invoice-preview-form-container' },
							_react2.default.createElement(
								'div',
								{ className: 'invoice-preview-form-body' },
								this.state.activeEditModal == 'firmDetails' && _react2.default.createElement(
									_react.Fragment,
									null,
									_react2.default.createElement(_TextField2.default, {
										required: true,
										label: 'Business Name',
										defaultValue: this.firmObj.getFirmName(),
										name: 'firmName',
										onChange: this.handleInputInEditModal,
										margin: 'dense',
										variant: 'outlined',
										fullWidth: true,
										color: 'primary'
									}),
									_react2.default.createElement(_TextField2.default, {
										label: 'Phone Number',
										defaultValue: this.firmObj.getFirmPhone(),
										name: 'firmPhone',
										onChange: this.handleInputInEditModal,
										type: 'number',
										margin: 'dense',
										variant: 'outlined',
										fullWidth: true,
										color: 'primary'
									}),
									_react2.default.createElement(_TextField2.default, {
										label: 'Email ID',
										defaultValue: this.firmObj.getFirmEmail(),
										name: 'firmEmail',
										onChange: this.handleInputInEditModal,
										type: 'email',
										margin: 'dense',
										variant: 'outlined',
										fullWidth: true,
										color: 'primary'
									}),
									_react2.default.createElement(_TextField2.default, {
										label: 'Address',
										defaultValue: this.firmObj.getFirmAddress(),
										name: 'firmAddress',
										onChange: this.handleInputInEditModal,
										rows: 4,
										multiline: true,
										margin: 'dense',
										variant: 'outlined',
										fullWidth: true,
										color: 'primary'
									}),
									this.isGSTEnabled && _react2.default.createElement(_TextField2.default, {
										label: 'GSTIN No.',
										defaultValue: this.firmObj.getFirmHSNSAC(),
										name: 'firmGSTIN',
										onChange: this.handleInputInEditModal,
										margin: 'dense',
										variant: 'outlined',
										inputProps: { maxLength: 15 },
										fullWidth: true,
										color: 'primary'
									}),
									this.isTINEnabled && _react2.default.createElement(_TextField2.default, {
										label: 'TIN No.',
										defaultValue: this.firmObj.getFirmTin(),
										name: 'firmTIN',
										onChange: this.handleInputInEditModal,
										margin: 'dense',
										variant: 'outlined',
										inputProps: { maxLength: 15 },
										fullWidth: true,
										color: 'primary'
									})
								),
								this.state.activeEditModal == 'payToDetails' && _react2.default.createElement(
									_react.Fragment,
									null,
									_react2.default.createElement(_TextField2.default, {
										required: true,
										label: 'Bank Name',
										defaultValue: this.firmObj.getBankName(),
										name: 'firmBankName',
										onChange: this.handleInputInEditModal,
										margin: 'dense',
										variant: 'outlined',
										fullWidth: true,
										color: 'primary'
									}),
									_react2.default.createElement(_TextField2.default, {
										label: 'Account Number',
										defaultValue: this.firmObj.getAccountNumber(),
										name: 'firmAccountNumber',
										onChange: this.handleInputInEditModal,
										margin: 'dense',
										variant: 'outlined',
										fullWidth: true,
										color: 'primary'
									}),
									_react2.default.createElement(_TextField2.default, {
										label: 'BANK IFSC',
										defaultValue: this.firmObj.getBankIFSC(),
										name: 'firmBankIFSC',
										onChange: this.handleInputInEditModal,
										type: 'email',
										margin: 'dense',
										variant: 'outlined',
										fullWidth: true,
										color: 'primary'
									})
								),
								this.state.activeEditModal == 'termsAndConditions' && _react2.default.createElement(
									_react.Fragment,
									null,
									_react2.default.createElement(_TextField2.default, {
										label: 'Terms and conditions',
										defaultValue: this.getDefaultTermsAndConditions(),
										name: 'termsAndConditions',
										onChange: this.handleInputInEditModal,
										rows: 4,
										multiline: true,
										margin: 'dense',
										variant: 'outlined',
										fullWidth: true,
										color: 'primary'
									})
								)
							),
							_react2.default.createElement(
								'div',
								{ className: 'invoice-preview-form-footer' },
								_react2.default.createElement(
									_PrimaryButton2.default,
									{
										style: {
											backgroundColor: 'white',
											color: '#1789FC',
											marginLeft: 'auto'
										},
										onClick: this.onClose
									},
									'Cancel'
								),
								_react2.default.createElement(
									_PrimaryButton2.default,
									{
										style: {
											color: 'white',
											backgroundColor: '#1789FC',
											marginLeft: '1rem'
										},
										onClick: this.saveForm
									},
									['signatureLogo', 'firmLogo'].includes(this.state.activeEditModal) ? 'Upload' : 'Save'
								)
							)
						)
					)
				)
			);
		}
	}]);
	return InvoicePreview;
}(_react2.default.Component);

var _initialiseProps = function _initialiseProps() {
	var _this7 = this;

	this.handleClickOutside = function (event) {
		if (_this7.node && !_this7.node.contains(event.target) && _this7.whatsAppBrowserWindow) {
			_this7.whatsAppBrowserWindow.destroy();
		}
	};

	this.changePdfTheme = function (theme) {
		this.setState({
			selectedTheme: theme
		});
		if (theme == 'THEME_12' || theme == 'THEME_11') {
			_analyticsHelper2.default.pushEvent('User Viewed Premium Theme', { Action: 'Click' });
		}
		this.changePdfThemeAndColor(_InvoiceTheme2.default[theme]);
	};

	this.changePdfColor = function (color, doubleColorComboId) {
		this.changePdfThemeAndColor(null, color, doubleColorComboId);
	};

	this.printPdf = function () {
		this.updateThemeSettings();
		this.pdfHandler.print(this.originalSizePreviewHtml);
	};

	this.buildEmailContent = function () {
		var txnType = _this7.baseTxn.getTxnType();
		var subject = void 0,
		    message = void 0;
		if (messageConfigEnabledTxns.includes(txnType)) {
			var transactionfactory = new _TransactionFactory2.default();
			var messageFormatter = new _TxnMessageFormatter2.default();
			var txnTypeString = transactionfactory.getTransTypeString(Number(txnType));
			var msg = messageFormatter.createMessageForTxn(_this7.baseTxn);
			var partyName = _this7.nameObj.getFullName();
			subject = 'Your ' + txnTypeString + ' invoice details';
			message = 'Dear ' + partyName + ',\nThanks for doing business with us. Below are your invoice details. Also attached are the PDF of invoice for your convenience.\n' + msg.message + '\nThanks & Regards';
		}
		return {
			subject: subject,
			message: message
		};
	};

	this.changePdfThemeAndColor = function (newTheme, newColor) {
		var doubleColorComboId = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

		var theme = newTheme || this.state.currentSelectedPdfTheme;
		var color = newColor || this.state.currentSelectedThemeColor;
		var doubleColorId = doubleColorComboId || this.state.selectedDoubleColorComboId;
		var previewHtml = this.transactionHTMLGenerator.getTransactionHTMLLayoutForPreview(this.baseTxn, theme, color, doubleColorId);
		this.adjustWidthForPreview();

		this.setState({
			previewHtml: previewHtml,
			currentSelectedPdfTheme: theme,
			currentSelectedThemeColor: color,
			selectedDoubleColorComboId: doubleColorId
		});
	};

	this.generateHiddenBrowserWindowPdf = function () {
		_this7.originalSizePreviewHtml = _this7.transactionHTMLGenerator.getTransactionHTMLLayout(_this7.baseTxn, _this7.state.currentSelectedPdfTheme, _this7.state.currentSelectedThemeColor, null, null, _this7.state.selectedDoubleColorComboId);
		_this7.pdfHandler.generateHiddenBrowserWindowForPDF(_this7.originalSizePreviewHtml);
	};

	this.onClose = function () {
		_this7.setState({
			activeEditModal: ''
		});
	};

	this.handleClickOnEditSection = function (event) {
		var LocalStorageHelper = require('../../Utilities/LocalStorageHelper');
		var editSection = event.currentTarget.getAttribute('editsection') || '';
		editSection && LocalStorageHelper.setValue(editSection, 'true');
		var previewHtml = _this7.transactionHTMLGenerator.getTransactionHTMLLayoutForPreview(_this7.baseTxn, _this7.state.currentSelectedPdfTheme, _this7.state.currentSelectedThemeColor);
		_this7.setState({
			activeEditModal: editSection,
			previewHtml: previewHtml
		});
	};

	this.saveForm = function () {
		var ErrorCode = require('../../Constants/ErrorCode');
		var activeEditModal = _this7.state.activeEditModal;

		var firmCache = new _FirmCache2.default();
		var statusCode = void 0,
		    previewHtml = void 0;
		var firmName = '';
		var firmPhone = '';
		var firmEmail = '';
		var firmAddress = '';
		var firmGSTIN = '';
		var firmTIN = '';
		var bankName = '';
		var accountNumber = '';
		var bankIFSC = '';
		var termsAndConditions = '';
		switch (activeEditModal) {
			case 'firmDetails':
				firmName = _this7.editSectionModalTextValues.firmName || '';
				firmPhone = _this7.editSectionModalTextValues.firmPhone || '';
				firmEmail = _this7.editSectionModalTextValues.firmEmail || '';
				firmAddress = _this7.editSectionModalTextValues.firmAddress || '';
				firmGSTIN = _this7.editSectionModalTextValues.firmGSTIN || '';
				firmTIN = _this7.editSectionModalTextValues.firmTIN || '';

				firmName = firmName.trim();
				firmPhone = firmPhone.trim();
				firmEmail = firmEmail.trim();
				firmAddress = firmAddress.trim();
				firmGSTIN = firmGSTIN.trim();
				firmTIN = firmTIN.trim();

				if (!firmName) {
					ToastHelper.error(ErrorCode.ERROR_FIRM_NAME_EMPTY);
					return;
				}

				if (firmName != _this7.firmObj.getFirmName() && !firmCache.isFirmNameUnique(firmName)) {
					ToastHelper.error(ErrorCode.ERROR_FIRM_ALREADYEXISTS);
					return;
				}
				if (_this7.isGSTEnabled) {
					var gstRegex = new RegExp(StringConstants.GSTINregex);
					if (firmGSTIN && !gstRegex.test(firmGSTIN)) {
						ToastHelper.error(ErrorCode.ERROR_GST_INVALID);
						return;
					}
					if (firmGSTIN) {
						var StateCode = require('../../Constants/StateCode');
						var stateIndex = Number(firmGSTIN.substr(0, 2));
						var state = StateCode.getStateName(stateIndex) || '';
						_this7.firmObj.setFirmState(state);
					}
				}
				if (!firmEmail && !firmPhone) {
					ToastHelper.error(ErrorCode.ERROR_FIRM_EMAIL_AND_PHONE_EMPTY);
					return;
				}
				if (firmEmail && !firmEmail.includes('@')) {
					ToastHelper.error('Invalid email Address');
					return;
				}
				if (firmPhone && firmPhone.length < 5) {
					ToastHelper.error('Invalid phone number');
					return;
				}

				_this7.firmObj.setFirmName(firmName);
				_this7.firmObj.setFirmPhone(firmPhone);
				_this7.firmObj.setFirmEmail(firmEmail);
				_this7.firmObj.setFirmAddress(firmAddress);
				_this7.isGSTEnabled && _this7.firmObj.setFirmHSNSAC(firmGSTIN);
				_this7.isTINEnabled && _this7.firmObj.setFirmTin(firmTIN);
				statusCode = _this7.firmObj.updateFirmWithoutObject();
				_this7.firmObj = firmCache.getFirmById(_this7.baseTxn.getFirmId());
				if (statusCode !== ErrorCode.ERROR_FIRM_UPDATE_SUCCESS) {
					ToastHelper.error(statusCode);
					_this7.setState({
						activeEditModal: ''
					});
					return;
				}
				ToastHelper.success(statusCode);
				previewHtml = _this7.transactionHTMLGenerator.getTransactionHTMLLayoutForPreview(_this7.baseTxn, _this7.state.currentSelectedPdfTheme, _this7.state.currentSelectedThemeColor);
				_this7.setState({
					previewHtml: previewHtml,
					activeEditModal: ''
				});
				try {
					window.updateCompanyInfo();
				} catch (err) {}
				break;
			case 'payToDetails':
				bankName = _this7.editSectionModalTextValues.firmBankName || '';
				accountNumber = _this7.editSectionModalTextValues.firmAccountNumber || '';
				bankIFSC = _this7.editSectionModalTextValues.firmBankIFSC || '';
				bankName = bankName.trim();
				accountNumber = accountNumber.trim();
				bankIFSC = bankIFSC.trim();
				_this7.firmObj.setBankName(bankName);
				_this7.firmObj.setAccountNumber(accountNumber);
				_this7.firmObj.setBankIFSC(bankIFSC);
				statusCode = _this7.firmObj.updateFirmWithoutObject();
				_this7.firmObj = firmCache.getFirmById(_this7.baseTxn.getFirmId());
				if (statusCode !== ErrorCode.ERROR_FIRM_UPDATE_SUCCESS) {
					ToastHelper.error(statusCode);
					_this7.setState({
						activeEditModal: ''
					});
					return;
				}
				ToastHelper.success(statusCode);
				previewHtml = _this7.transactionHTMLGenerator.getTransactionHTMLLayoutForPreview(_this7.baseTxn, _this7.state.currentSelectedPdfTheme, _this7.state.currentSelectedThemeColor);
				_this7.setState({
					previewHtml: previewHtml,
					activeEditModal: ''
				});
				try {
					window.updateCompanyInfo();
				} catch (err) {}
				break;
			case 'termsAndConditions':
				termsAndConditions = _this7.editSectionModalTextValues.termsAndConditions || '';
				termsAndConditions = termsAndConditions.trim();
				switch (_this7.baseTxn.txnType) {
					case _TxnTypeConstant2.default.TXN_TYPE_SALE:
						_this7.settingsModel.setSettingKey(_Queries2.default.SETTING_SALE_INVOICE_TERMS_AND_CONDITIONS);
						break;
					case _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER:
						_this7.settingsModel.setSettingKey(_Queries2.default.SETTING_SALE_ORDER_TERMS_AND_CONDITIONS);
						break;
					case _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE:
						_this7.settingsModel.setSettingKey(_Queries2.default.SETTING_ESTIMATE_QUOTATION_TERMS_AND_CONDITIONS);
						break;
					case _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN:
						_this7.settingsModel.setSettingKey(_Queries2.default.SETTING_DELIVERY_CHALLAN_TERMS_AND_CONDITIONS);
						break;
				}
				_this7.settingsModel.setSettingValue(termsAndConditions);
				_this7.settingsModel.UpdateSetting(termsAndConditions, {
					nonSyncableSetting: false
				});
				previewHtml = _this7.transactionHTMLGenerator.getTransactionHTMLLayoutForPreview(_this7.baseTxn, _this7.state.currentSelectedPdfTheme, _this7.state.currentSelectedThemeColor);
				_this7.setState({
					previewHtml: previewHtml,
					activeEditModal: ''
				});
				break;
			case 'signatureLogo':
				_this7.updateFirmLogoOrSignature(activeEditModal);
				break;
			case 'firmLogo':
				_this7.updateFirmLogoOrSignature(activeEditModal);
				break;
			default:
				break;
		}
	};

	this.handleInputInEditModal = function (event) {
		var name = event.currentTarget.name || '';
		var value = event.currentTarget.value || '';
		if (name == 'firmPhone') {
			event.target.oldValue = event.target.oldValue || _this7.editSectionModalTextValues[name] || '';
			value = _DecimalInputFilter2.default.inputTextFilterForPhone(event.target);
		}
		if (name == 'firmEmail') {
			// This below statement is for DecimalInputFilter to fallback to initial value in case user enters invalid data in edit loan account scenario
			event.target.oldValue = event.target.oldValue || _this7.editSectionModalTextValues[name] || '';
			value = _DecimalInputFilter2.default.inputTextFilterForLength(event.target, 45);
		}
		if (name == 'firmName') {
			// This below statement is for DecimalInputFilter to fallback to initial value in case user enters invalid data in edit loan account scenario
			event.target.oldValue = event.target.oldValue || _this7.editSectionModalTextValues[name] || '';
			value = _DecimalInputFilter2.default.inputTextFilterForLength(event.target, 200);
		}
		_this7.editSectionModalTextValues[name] = value;
	};

	this.resetEditSectionModalTextValues = function () {
		_this7.editSectionModalTextValues.firmName = _this7.firmObj.getFirmName();
		_this7.editSectionModalTextValues.firmPhone = _this7.firmObj.getFirmPhone();
		_this7.editSectionModalTextValues.firmEmail = _this7.firmObj.getFirmEmail();
		_this7.editSectionModalTextValues.firmAddress = _this7.firmObj.getFirmAddress();
		_this7.editSectionModalTextValues.firmGSTIN = _this7.firmObj.getFirmHSNSAC();
		_this7.editSectionModalTextValues.firmTIN = _this7.firmObj.getFirmTin();
		switch (_this7.baseTxn.txnType) {
			case _TxnTypeConstant2.default.TXN_TYPE_SALE:
				_this7.editSectionModalTextValues.termsAndConditions = _this7.settingCache.getTermsAndConditionSaleInvoivce();
				break;
			case _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER:
				_this7.editSectionModalTextValues.termsAndConditions = _this7.settingCache.getTermsAndConditionSaleOrder();
				break;
			case _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE:
				_this7.editSectionModalTextValues.termsAndConditions = _this7.settingCache.getTermsAndConditionEstimateQuotation();
				break;
			case _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN:
				_this7.editSectionModalTextValues.termsAndConditions = _this7.settingCache.getTermsAndConditionDeliveryChallan();
				break;
		}
		_this7.editSectionModalTextValues.firmAccountNumber = _this7.firmObj.getAccountNumber();
		_this7.editSectionModalTextValues.firmBankName = _this7.firmObj.getBankName();
		_this7.editSectionModalTextValues.firmBankIFSC = _this7.firmObj.getBankIFSC();
	};
};

InvoicePreview.propTypes = {
	transactionId: _propTypes2.default.string
};

exports.default = (0, _styles.withStyles)(styles)(InvoicePreview);