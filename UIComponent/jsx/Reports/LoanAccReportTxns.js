Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Grid = require('../Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _MyDouble = require('../../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _MyDate = require('../../../Utilities/MyDate');

var _MyDate2 = _interopRequireDefault(_MyDate);

var _TxnTypeConstant = require('../../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LoanAccReportTxns = function (_React$Component) {
	(0, _inherits3.default)(LoanAccReportTxns, _React$Component);

	function LoanAccReportTxns(props) {
		(0, _classCallCheck3.default)(this, LoanAccReportTxns);

		var _this = (0, _possibleConstructorReturn3.default)(this, (LoanAccReportTxns.__proto__ || (0, _getPrototypeOf2.default)(LoanAccReportTxns)).call(this, props));

		_this.getApi = function (params) {
			_this.api = params.api;
			_this.api.setRowData(_this.props.records);
		};

		_this.state = {
			columnDefs: [{
				field: 'date',
				headerName: 'DATE',
				width: 30,
				cellRenderer: function cellRenderer(params) {
					if (params.value) {
						return _MyDate2.default.getDate('d/m/y', new Date(params.value));
					}
				}
			}, {
				field: 'loanTxnType',
				headerName: 'TYPE',
				width: 30,
				cellRenderer: function cellRenderer(params) {
					return _TxnTypeConstant2.default.getTxnTypeForUI(params.value);
				}
			}, {
				field: 'amount',
				headerName: 'Amount',
				width: 30,
				cellRenderer: function cellRenderer(params) {
					var amount = Number(params.data.principalAmount) + Number(params.data.interestAmount);
					return _MyDouble2.default.getAmountWithDecimalAndCurrencyWithSign(amount);
				}
			}, {
				field: 'endingBalance',
				headerName: 'Ending Balance',
				width: 30,
				cellRenderer: function cellRenderer(params) {
					return _MyDouble2.default.getAmountWithDecimalAndCurrencyWithSign(params.value);
				}
			}]
		};
		return _this;
	}

	(0, _createClass3.default)(LoanAccReportTxns, [{
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(nextProps) {
			if (this.api) {
				this.api.setRowData(nextProps.records);
			}
			return false;
		}
	}, {
		key: 'render',
		value: function render() {
			var gridOptions = {
				enableFilter: false,
				enableSorting: false,
				rowSelection: 'single',
				columnDefs: this.state.columnDefs,
				rowData: [],
				rowHeight: 40,
				headerHeight: 40,
				rowClass: 'customVerticalBorder',
				overlayNoRowsTemplate: 'No transactions to show'
			};

			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement(_Grid2.default, {
					classes: 'alternateRowColor customVerticalBorder gridRowHeight40',
					gridOptions: gridOptions,
					notSelectFirstRow: true,
					getApi: this.getApi
				})
			);
		}
	}]);
	return LoanAccReportTxns;
}(_react2.default.Component);

LoanAccReportTxns.propTypes = {
	records: _propTypes2.default.array
};

exports.default = LoanAccReportTxns;