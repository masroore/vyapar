Object.defineProperty(exports, "__esModule", {
	value: true
});

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _LoanAccountCache = require('../../../Cache/LoanAccountCache');

var _LoanAccountCache2 = _interopRequireDefault(_LoanAccountCache);

var _SettingCache = require('../../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

var _FirmCache = require('../../../Cache/FirmCache');

var _FirmCache2 = _interopRequireDefault(_FirmCache);

var _LoanAccReportTxns = require('./LoanAccReportTxns');

var _LoanAccReportTxns2 = _interopRequireDefault(_LoanAccReportTxns);

var _JqueryDatePicker = require('../UIControls/JqueryDatePicker');

var _JqueryDatePicker2 = _interopRequireDefault(_JqueryDatePicker);

var _DataLoader = require('../../../DBManager/DataLoader');

var _DataLoader2 = _interopRequireDefault(_DataLoader);

var _PDFReportConstant = require('../../../Constants/PDFReportConstant');

var _PDFReportConstant2 = _interopRequireDefault(_PDFReportConstant);

var _ExcelHelper = require('../../../Utilities/ExcelHelper');

var _ExcelHelper2 = _interopRequireDefault(_ExcelHelper);

var _TransactionHelper = require('../../../Utilities/TransactionHelper');

var TransactionHelper = _interopRequireWildcard(_TransactionHelper);

var _TxnTypeConstant = require('../../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _MyDouble = require('../../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _MyDate = require('../../../Utilities/MyDate');

var _MyDate2 = _interopRequireDefault(_MyDate);

var _trashableReact = require('trashable-react');

var _trashableReact2 = _interopRequireDefault(_trashableReact);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LoanAccountReportContainer = function (_React$Component) {
	(0, _inherits3.default)(LoanAccountReportContainer, _React$Component);

	function LoanAccountReportContainer(props) {
		(0, _classCallCheck3.default)(this, LoanAccountReportContainer);

		var _this = (0, _possibleConstructorReturn3.default)(this, (LoanAccountReportContainer.__proto__ || (0, _getPrototypeOf2.default)(LoanAccountReportContainer)).call(this, props));

		_this.printTransactions = function () {
			var LoanStatementReportHTMLGenerator = require('../../../ReportHTMLGenerator/LoanStatementReportHTMLGenerator');
			var loanStatementReportHTMLGenerator = new LoanStatementReportHTMLGenerator();
			var loanAccountCache = new _LoanAccountCache2.default();
			var _this$state = _this.state,
			    totalPrincipalPaid = _this$state.totalPrincipalPaid,
			    totalInterestPaid = _this$state.totalInterestPaid,
			    loanAccountId = _this$state.loanAccountId,
			    firmId = _this$state.firmId;

			var loanAccount = loanAccountCache.getLoanAccountById(loanAccountId) || {};
			var openingBalance = loanAccount.openingBalance,
			    currentBalance = loanAccount.currentBalance;

			var fromDateStr = _this.state.fromDate;
			var toDateStr = _this.state.toDate;
			var html = loanStatementReportHTMLGenerator.getHTMLText(_this.state.records, fromDateStr, toDateStr, openingBalance, totalPrincipalPaid, currentBalance, totalInterestPaid, loanAccount, firmId);
			TransactionHelper.previewByHTML(html);
		};

		_this.printExcel = function () {
			var XLSX = require('xlsx');
			var dataArray = _this.prepareDataForExcel();
			var worksheet = XLSX.utils.aoa_to_sheet(dataArray);
			var workbook = {
				'SheetNames': [],
				'Sheets': {}
			};
			var wsName = 'Loan Statement';
			workbook.SheetNames[0] = wsName;
			workbook.Sheets[wsName] = worksheet;
			var wscolWidth = [{ wch: 20 }, { wch: 30 }, { wch: 20 }, { wch: 20 }];
			worksheet['!cols'] = wscolWidth;
			XLSX.writeFile(workbook, appPath + '/report.xlsx');
			_this.downloadExcelFile();
		};

		_this.loanAccountNames = [];
		_this.firms = [];
		var currentDate = new Date();
		var monthStartDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1, 0, 0, 0, 0);
		_this.state = {
			isMultiFirmEnabled: false,
			firmId: '',
			loanAccountId: '',
			fromDate: _MyDate2.default.getDate('d/m/y', monthStartDate),
			toDate: _MyDate2.default.getDate('d/m/y')
		};
		return _this;
	}

	(0, _createClass3.default)(LoanAccountReportContainer, [{
		key: 'loadData',
		value: function loadData() {
			var settingCache = new _SettingCache2.default();
			var loanAccountCache = new _LoanAccountCache2.default();
			var isMultiFirmEnabled = settingCache.getMultipleFirmEnabled();
			var _state = this.state,
			    loanAccountId = _state.loanAccountId,
			    fromDate = _state.fromDate,
			    toDate = _state.toDate,
			    firmId = _state.firmId;

			if (isMultiFirmEnabled) {
				var firmCache = new _FirmCache2.default();
				this.firms = (0, _values2.default)(firmCache.getFirmList()).map(function (firm) {
					return {
						firmId: firm.getFirmId(),
						firmName: firm.getFirmName()
					};
				});
			}
			var loanAccounts = loanAccountCache.getAllLoanAccounts(firmId);
			var loanAccountNames = loanAccounts.map(function (loanAccount) {
				return loanAccount.accountName;
			});
			this.loanAccountNames = loanAccountNames;
			if (loanAccountNames && loanAccountNames.length) {
				loanAccountId = loanAccountId || loanAccountCache.getLoanAccountIdByName(loanAccountNames[0]);
				this.loadTransactions(loanAccountId, _MyDate2.default.convertStringToDateObject(fromDate), _MyDate2.default.convertStringToDateObject(toDate));
			} else {
				this.setState({
					records: [],
					loanAccountId: loanAccountId
				});
			}
			this.setState({
				isMultiFirmEnabled: isMultiFirmEnabled
			});
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.loadData();
			TransactionHelper.bindEventsForPreviewDialog();
			window.onResume = this.onResume.bind(this);
			window.savePDF = this.savePDF.bind(this);
		}
	}, {
		key: 'savePDF',
		value: function savePDF() {
			var PDFHandler = require('../../../Utilities/PDFHandler');
			var pdfHandler = new PDFHandler();
			var _state2 = this.state,
			    fromDate = _state2.fromDate,
			    toDate = _state2.toDate;

			pdfHandler.savePDF({ type: _PDFReportConstant2.default.LOAN_STATEMENT_REPORT, fromDate: fromDate, toDate: toDate });
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			TransactionHelper.unBindEventsForPreviewDialog();
			delete window.onResume;
			delete window.savePDF;
		}
	}, {
		key: 'onResume',
		value: function onResume() {
			$('#modelContainer').css({ 'display': 'none' });
			$('.viewItems').css('display', 'block');
			this.loadData();
			TransactionHelper.bindEventsForPreviewDialog();
		}
	}, {
		key: 'loadTransactions',
		value: function loadTransactions(loanAccountId, fromDate, toDate) {
			var dataLoader = new _DataLoader2.default();
			var txns = [];
			if (loanAccountId) {
				txns = dataLoader.getLoanTransactions({
					loanAccountId: loanAccountId,
					fromDate: fromDate,
					toDate: toDate,
					latestFirst: false
				});
			}
			var balanceMap = dataLoader.getLoanAccountBalances({ loanAccountIds: [loanAccountId], tillDate: new Date(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate() - 1) });
			var startingBalance = balanceMap.get(Number(loanAccountId)) || 0;
			var totalPrincipalPaid = 0;
			var totalInterestPaid = 0;
			var nextStartingBalance = startingBalance;
			var records = [];
			txns.forEach(function (loanTxn) {
				var date = void 0,
				    loanTxnType = void 0,
				    principalAmount = void 0,
				    interestAmount = void 0,
				    endingBalance = void 0,
				    loanTxnId = void 0,
				    loanAccountId = void 0;
				loanTxnId = loanTxn.getLoanTxnId();
				loanAccountId = loanTxn.getLoanAccountId();
				loanTxnType = Number(loanTxn.getLoanType());
				principalAmount = loanTxn.getPrincipalAmount();
				interestAmount = loanTxn.getInterestAmount();
				endingBalance = nextStartingBalance;

				if (Number(principalAmount) || Number(interestAmount)) {
					if (loanTxnType != _TxnTypeConstant2.default.TXN_TYPE_LOAN_PROCESSING_FEE) {
						if (loanTxnType == _TxnTypeConstant2.default.TXN_TYPE_LOAN_EMI_PAYMENT) {
							endingBalance -= Number(principalAmount);
							totalPrincipalPaid += Number(principalAmount);
							totalInterestPaid += Number(interestAmount);
						} else {
							endingBalance += Number(principalAmount);
						}
						nextStartingBalance = endingBalance;
					}
				}
				date = loanTxn.getTransactionDate();
				records.push({
					date: date,
					loanTxnId: loanTxnId,
					loanAccountId: loanAccountId,
					loanTxnType: loanTxnType,
					principalAmount: principalAmount,
					interestAmount: interestAmount,
					endingBalance: endingBalance
				});
			});
			this.setState({
				loanAccountId: loanAccountId,
				totalInterestPaid: totalInterestPaid,
				totalPrincipalPaid: totalPrincipalPaid,
				records: records
			});
		}
	}, {
		key: 'changeFilter',
		value: function changeFilter(field) {
			var _this2 = this;

			return function (e) {
				var value = void 0;
				if (e && e.target) {
					value = e.target.value;
				} else {
					value = e;
				}
				var loanAccountCache = new _LoanAccountCache2.default();
				var date = void 0;
				var _state3 = _this2.state,
				    loanAccountId = _state3.loanAccountId,
				    firmId = _state3.firmId,
				    fromDate = _state3.fromDate,
				    toDate = _state3.toDate;

				switch (field) {
					case 'firm':
						firmId = value;
						loanAccountId = '';
						break;
					case 'accountName':
						loanAccountId = loanAccountCache.getLoanAccountIdByName(value);
						break;
					case 'fromDate':
						date = _MyDate2.default.getDateObj(value, 'dd/mm/yyyy', '/');
						if (date && date instanceof Date && date != 'Invalid Date') {
							fromDate = value;
						}
						break;
					case 'toDate':
						date = _MyDate2.default.getDateObj(value, 'dd/mm/yyyy', '/');
						if (date && date instanceof Date && date != 'Invalid Date') {
							toDate = value;
						}
						break;
					default:
						break;
				}

				_this2.setState({
					loanAccountId: loanAccountId,
					firmId: firmId,
					fromDate: fromDate,
					toDate: toDate
				}, function () {
					return _this2.loadData();
				});
			};
		}
	}, {
		key: 'prepareDataForExcel',
		value: function prepareDataForExcel() {
			var _state4 = this.state,
			    totalPrincipalPaid = _state4.totalPrincipalPaid,
			    totalInterestPaid = _state4.totalInterestPaid,
			    loanAccountId = _state4.loanAccountId;

			var loanAccountCache = new _LoanAccountCache2.default();
			var loanAccount = loanAccountCache.getLoanAccountById(loanAccountId) || {};
			var openingBalance = loanAccount.openingBalance,
			    currentBalance = loanAccount.currentBalance;

			var data = this.state.records;
			var excelData = [];
			var tableHeadArray = ['Date', 'Type', 'Amount', 'Ending Balance'];
			excelData.push(tableHeadArray);
			data.forEach(function (txn) {
				var row = [];
				var amount = Number(txn.principalAmount) + Number(txn.interestAmount);
				row.push(_MyDate2.default.getDate('d/m/y', new Date(txn.date)));
				row.push(_TxnTypeConstant2.default.getTxnTypeForUI(txn.loanTxnType));
				row.push(_MyDouble2.default.getBalanceAmountWithDecimal(amount) || '');
				row.push(_MyDouble2.default.getBalanceAmountWithDecimal(txn.endingBalance) || '');
				excelData.push(row);
			});
			excelData.push([], ['Summary']);
			excelData.push(['Opening Balance:', _MyDouble2.default.getBalanceAmountWithDecimal(openingBalance || ''), 'Total Principal Paid:', _MyDouble2.default.getBalanceAmountWithDecimal(totalPrincipalPaid || '')]);
			excelData.push(['Balance Due:', _MyDouble2.default.getBalanceAmountWithDecimal(currentBalance || ''), 'Total Interest Paid:', _MyDouble2.default.getBalanceAmountWithDecimal(totalInterestPaid || '')]);
			return excelData;
		}
	}, {
		key: 'downloadExcelFile',
		value: function downloadExcelFile() {
			var fileUtil = require('../../../Utilities/FileUtil');
			var _state5 = this.state,
			    fromDate = _state5.fromDate,
			    toDate = _state5.toDate;

			var fileName = fileUtil.getFileName({ type: _PDFReportConstant2.default.LOAN_STATEMENT_REPORT, fromDate: fromDate, toDate: toDate });
			var excelHelper = new _ExcelHelper2.default();
			excelHelper.saveExcel(fileName);
		}
	}, {
		key: 'render',
		value: function render() {
			var loanAccountCache = new _LoanAccountCache2.default();
			var _state6 = this.state,
			    isMultiFirmEnabled = _state6.isMultiFirmEnabled,
			    firmId = _state6.firmId,
			    fromDate = _state6.fromDate,
			    toDate = _state6.toDate,
			    loanAccountId = _state6.loanAccountId;

			var loanAccount = loanAccountCache.getLoanAccountById(loanAccountId) || {};
			return _react2.default.createElement(
				'div',
				{ className: 'd-flex flex-column flex-grow-1 right-column' },
				_react2.default.createElement(
					'div',
					null,
					_react2.default.createElement(
						'div',
						{ className: 'grid-external-filter d-flex justify-content-between align-items-center' },
						_react2.default.createElement(
							'div',
							{ className: 'flex flex-1 flex-wrap align-items-center' },
							isMultiFirmEnabled && _react2.default.createElement(
								'div',
								{ className: 'form-control flex-1' },
								_react2.default.createElement(
									'label',
									{ className: 'filterLabelText' },
									'FIRM:'
								),
								_react2.default.createElement(
									'select',
									{
										value: firmId || '',
										className: 'input-control rounded ml-15',
										selected: loanAccount.accountName || '',
										onChange: this.changeFilter('firm')
									},
									_react2.default.createElement(
										'option',
										{ key: '', value: '' },
										'All Firms'
									),
									this.firms.map(function (_ref) {
										var firmId = _ref.firmId,
										    firmName = _ref.firmName;

										return _react2.default.createElement(
											'option',
											{ key: firmId, value: firmId },
											firmName
										);
									})
								)
							),
							_react2.default.createElement(
								'div',
								{ className: 'form-control flex-1 ml-15' },
								_react2.default.createElement(
									'label',
									{ className: 'filterLabelText' },
									'ACCOUNT:'
								),
								_react2.default.createElement(
									'select',
									{
										value: loanAccount.accountName || '',
										className: 'input-control rounded ml-15',
										selected: loanAccount.accountName || '',
										onChange: this.changeFilter('accountName')
									},
									this.loanAccountNames.map(function (loanAccountName) {
										return _react2.default.createElement(
											'option',
											{ key: loanAccountName, value: loanAccountName },
											loanAccountName
										);
									})
								)
							),
							_react2.default.createElement(
								'div',
								{ className: 'form-control ml-15 flex flex-2 align-self-end align-items-center' },
								_react2.default.createElement(
									'div',
									{ className: 'form-control inline ml-15' },
									_react2.default.createElement(_JqueryDatePicker2.default, { className: 'input-control rounded datepicker-input',
										placeholder: 'dd/MM/yyyy',
										value: fromDate,
										validate: true,
										onChange: this.changeFilter('fromDate')
									})
								),
								_react2.default.createElement(
									'label',
									{ className: 'filterLabelText', style: { marginRight: 15 } },
									'TO'
								),
								_react2.default.createElement(
									'div',
									{ className: 'form-control inline ml-15' },
									_react2.default.createElement(_JqueryDatePicker2.default, { className: 'input-control rounded datepicker-input',
										placeholder: 'dd/MM/yyyy',
										validate: true,
										value: toDate,
										onChange: this.changeFilter('toDate')
									})
								)
							)
						),
						_react2.default.createElement(
							'div',
							null,
							_react2.default.createElement(
								'div',
								{ className: 'circle-button floatLeft margin-r20', id: '', title: 'Excel Export' },
								_react2.default.createElement('img', { className: 'header-icon', src: '../inlineSVG/excel.svg', onClick: this.printExcel })
							),
							_react2.default.createElement(
								'div',
								{ className: 'circle-button floatLeft', id: '', title: 'Print' },
								_react2.default.createElement('img', { className: 'header-icon', src: '../inlineSVG/print.svg', onClick: this.printTransactions })
							)
						)
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'd-flex flex-1' },
					_react2.default.createElement(_LoanAccReportTxns2.default, { records: this.state.records })
				),
				_react2.default.createElement(
					'div',
					{ className: 'font-14 line-height-15 ml-10 mr-10' },
					_react2.default.createElement(
						'div',
						{ className: 'font-med-14' },
						'Loan Account Summary'
					),
					_react2.default.createElement(
						'div',
						{ className: 'd-flex  justify-content-between' },
						_react2.default.createElement(
							'div',
							null,
							'Opening Balance: ',
							_MyDouble2.default.getAmountWithDecimalAndCurrencyWithSign(loanAccount.openingBalance || 0)
						),
						_react2.default.createElement(
							'div',
							null,
							'Total Principal Paid: ',
							_MyDouble2.default.getAmountWithDecimalAndCurrencyWithSign(this.state.totalPrincipalPaid || 0)
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'd-flex  justify-content-between' },
						_react2.default.createElement(
							'div',
							null,
							'Balance Due: ',
							_MyDouble2.default.getAmountWithDecimalAndCurrencyWithSign(loanAccount.currentBalance || 0)
						),
						_react2.default.createElement(
							'div',
							null,
							'Total Interest Paid: ',
							_MyDouble2.default.getAmountWithDecimalAndCurrencyWithSign(this.state.totalInterestPaid || 0)
						)
					)
				)
			);
		}
	}]);
	return LoanAccountReportContainer;
}(_react2.default.Component);

LoanAccountReportContainer.propTypes = {
	registerPromise: _propTypes2.default.func
};

exports.default = (0, _trashableReact2.default)(LoanAccountReportContainer);