Object.defineProperty(exports, "__esModule", {
	value: true
});

var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/core/styles');

var _HelpOutline = require('@material-ui/icons/HelpOutline');

var _HelpOutline2 = _interopRequireDefault(_HelpOutline);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _ErrorCode = require('../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

var _MyDouble = require('./../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _ToastHelper = require('../../UIControllers/ToastHelper');

var ToastHelper = _interopRequireWildcard(_ToastHelper);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles() {
	return {
		mr18: {
			marginRight: 18
		},
		linkPaymentButton: {
			background: '#1BDA94',
			borderColor: '#1BDA94',
			color: '#fff',
			'&:hover': {
				background: '#1BDA94',
				borderColor: '#1BDA94'
			}
		}
	};
};

var LinkPaymentButton = function (_React$Component) {
	(0, _inherits3.default)(LinkPaymentButton, _React$Component);

	function LinkPaymentButton(props) {
		(0, _classCallCheck3.default)(this, LinkPaymentButton);

		var _this = (0, _possibleConstructorReturn3.default)(this, (LinkPaymentButton.__proto__ || (0, _getPrototypeOf2.default)(LinkPaymentButton)).call(this, props));

		_this.state = {
			show: false
		};
		_this.onClick = _this.onClick.bind(_this);
		_this.onClose = _this.onClose.bind(_this);
		_this.closeDialog = _this.closeDialog.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(LinkPaymentButton, [{
		key: 'onClick',
		value: function onClick() {
			var _props = this.props,
			    txnType = _props.txnType,
			    txnId = _props.txnId,
			    oldTxnObj = _props.oldTxnObj,
			    txnListB2B = _props.txnListB2B,
			    selectedTransactionMap = _props.selectedTransactionMap,
			    copyOfSelectedTransactionMap = _props.copyOfSelectedTransactionMap,
			    transaction = _props.transaction,
			    changeTransactionLinks = _props.changeTransactionLinks,
			    changeFields = _props.changeFields;
			var _props$transaction = this.props.transaction,
			    cashSale = _props$transaction.cashSale,
			    party = _props$transaction.party;

			if (cashSale) {
				ToastHelper.error(_ErrorCode2.default.ERROR_TXN_CASH_SALE_B2B);
				return;
			}

			if (!party || party && !party.value) {
				ToastHelper.error('Enter party name');
				return;
			}
			var MountComponent = require('./MountComponent').default;
			var LinkPaymentModal = require('./LinkPaymentModal').default;
			MountComponent(LinkPaymentModal, document.querySelector('#link-payment-modal-container'), {
				txnType: txnType,
				txnId: txnId,
				oldTxnObj: oldTxnObj,
				isOpen: true,
				onClose: this.onClose,
				txnListB2B: txnListB2B,
				changeFields: changeFields,
				changeTransactionLinks: changeTransactionLinks,
				selectedTransactionMap: selectedTransactionMap,
				copyOfSelectedTransactionMap: copyOfSelectedTransactionMap,
				closeDialog: this.closeDialog,
				transaction: transaction
			});
		}
	}, {
		key: 'revertCurrentBalanceIntxnListB2B',
		value: function revertCurrentBalanceIntxnListB2B(selectedTransactionMap, txnListB2B, copyOfSelectedTransactionMap) {
			selectedTransactionMap.forEach(function (value, key) {
				var txnId = _MyDouble2.default.convertStringToDouble(key);

				if (!copyOfSelectedTransactionMap.has(txnId)) {
					var mapVal = txnListB2B.has(txnId) ? txnListB2B.get(txnId) : false;

					if (mapVal) {
						var newObj = mapVal[0];
						var newCurrBal = _MyDouble2.default.convertStringToDouble(newObj.getTxnCurrentBalanceAmount());

						txnListB2B.set(txnId, [newObj, newCurrBal]);
					}
				}
			});

			copyOfSelectedTransactionMap.forEach(function (value, key) {
				var txnId = _MyDouble2.default.convertStringToDouble(key);

				var mapVal = txnListB2B.has(txnId) ? txnListB2B.get(txnId) : false;

				if (mapVal) {
					var newObj = mapVal[0];
					var amt = _MyDouble2.default.convertStringToDouble(value[1]);
					var newCurrBal = _MyDouble2.default.convertStringToDouble(amt);
					txnListB2B.set(txnId, [newObj, newCurrBal]);
				}
			});
		}
	}, {
		key: 'closeDialog',
		value: function closeDialog() {
			unmountReactComponent(document.querySelector('#link-payment-modal-container'));
		}
	}, {
		key: 'onClose',
		value: function onClose() {
			var _props2 = this.props,
			    selectedTransactionMap = _props2.selectedTransactionMap,
			    copyOfSelectedTransactionMap = _props2.copyOfSelectedTransactionMap,
			    txnListB2B = _props2.txnListB2B,
			    changeTransactionLinks = _props2.changeTransactionLinks;

			this.revertCurrentBalanceIntxnListB2B(selectedTransactionMap, txnListB2B, copyOfSelectedTransactionMap);
			changeTransactionLinks(txnListB2B, new _map2.default(copyOfSelectedTransactionMap), copyOfSelectedTransactionMap);
			unmountReactComponent(document.querySelector('#link-payment-modal-container'));
		}
	}, {
		key: 'render',
		value: function render() {
			var classes = this.props.classes;

			return _react2.default.createElement(
				'div',
				null,
				_react2.default.createElement('div', { id: 'link-payment-modal-container' }),
				_react2.default.createElement(
					_Button2.default,
					{
						variant: 'contained',
						title: 'You can link your received payments to invoices and mark them as Paid',
						color: 'primary',
						onClick: this.onClick,
						endIcon: _react2.default.createElement(_HelpOutline2.default, null),
						className: (0, _classnames2.default)(classes.linkPaymentButton, classes.mr18) },
					'Link Payment'
				)
			);
		}
	}]);
	return LinkPaymentButton;
}(_react2.default.Component);

LinkPaymentButton.propTypes = {
	classes: _propTypes2.default.object,
	transaction: _propTypes2.default.object,
	selectedTransactionMap: _propTypes2.default.object,
	copyOfSelectedTransactionMap: _propTypes2.default.object,
	txnListB2B: _propTypes2.default.object,
	changeTransactionLinks: _propTypes2.default.func,
	txnType: _propTypes2.default.number,
	txnId: _propTypes2.default.number,
	oldTxnObj: _propTypes2.default.object,
	changeFields: _propTypes2.default.func
};
exports.default = (0, _styles.withStyles)(styles)(LinkPaymentButton);