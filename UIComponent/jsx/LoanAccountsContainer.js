Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _SearchBox = require('./SearchBox');

var _SearchBox2 = _interopRequireDefault(_SearchBox);

var _LoanAccountDetail = require('./LoanAccountDetail');

var _LoanAccountDetail2 = _interopRequireDefault(_LoanAccountDetail);

var _FirstScreen = require('./UIControls/FirstScreen');

var _FirstScreen2 = _interopRequireDefault(_FirstScreen);

var _ButtonDropdown = require('./ButtonDropdown');

var _ButtonDropdown2 = _interopRequireDefault(_ButtonDropdown);

var _LoanTransactions = require('./LoanTransactions');

var _LoanTransactions2 = _interopRequireDefault(_LoanTransactions);

var _LoanAccountsList = require('./LoanAccountsList');

var _LoanAccountsList2 = _interopRequireDefault(_LoanAccountsList);

var _AddLoanAccountModal = require('./AddLoanAccountModal');

var _AddLoanAccountModal2 = _interopRequireDefault(_AddLoanAccountModal);

var _AdjustLoanAccountModal = require('./AdjustLoanAccountModal');

var _AdjustLoanAccountModal2 = _interopRequireDefault(_AdjustLoanAccountModal);

var _PayEmiModal = require('./PayEmiModal');

var _PayEmiModal2 = _interopRequireDefault(_PayEmiModal);

var _TxnTypeConstant = require('../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _PaymentInfoCache = require('../../Cache/PaymentInfoCache');

var _PaymentInfoCache2 = _interopRequireDefault(_PaymentInfoCache);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LoanAccountsContainer = function (_React$Component) {
	(0, _inherits3.default)(LoanAccountsContainer, _React$Component);

	function LoanAccountsContainer(props) {
		(0, _classCallCheck3.default)(this, LoanAccountsContainer);

		var _this = (0, _possibleConstructorReturn3.default)(this, (LoanAccountsContainer.__proto__ || (0, _getPrototypeOf2.default)(LoanAccountsContainer)).call(this, props));

		_initialiseProps.call(_this);

		_this.filterItems = _this.filterItems.bind(_this);
		_this.onItemSelected = _this.onItemSelected.bind(_this);
		var loanAccounts = _this.getLoanAccounts();
		var currentItem = null;
		var showAddLoanModal = _this.props.showAddLoanModal || false;
		_this.allLoanAccounts = loanAccounts;
		var paymentOptions = _this.getPaymentOptions();
		_this.state = {
			records: [],
			loanAccounts: loanAccounts,
			paymentOptions: paymentOptions,
			currentItem: currentItem,
			contextMenu: _this.contextMenu,
			showSearch: false,
			showAddLoanModal: showAddLoanModal,
			showAdjustLoanModal: false,
			showPayEmiModal: false
		};
		window.onResume = _this.onResume.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(LoanAccountsContainer, [{
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			delete window.onResume;
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			window.onResume = this.onResume.bind(this);
		}
	}, {
		key: 'onResume',
		value: function onResume() {
			var _this2 = this;

			var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

			this.props.onResume && this.props.onResume(notFromAutoSyncFlow);
			var loanAccounts = this.getLoanAccounts();
			this.allLoanAccounts = loanAccounts;
			if (this.state.currentItem) {
				var currentItem = this.allLoanAccounts.find(function (bank) {
					return _this2.state.currentItem.accountId === bank.accountId;
				});
				this.setState({ currentItem: currentItem });
			}
			this.filterItems(this.state.searchText);
			this.onItemSelected(null, !notFromAutoSyncFlow);
		}
	}, {
		key: 'getLoanAccounts',
		value: function getLoanAccounts() {
			var LoanAccountCache = require('../../Cache/LoanAccountCache');
			var loanAccountCache = new LoanAccountCache();
			loanAccountCache.refreshLoanAccountCache();
			var list = loanAccountCache.getAllLoanAccounts();
			var loanAccounts = list.map(function (loanAccount) {
				return {
					accountId: loanAccount.getLoanAccountId(),
					accountName: loanAccount.getLoanAccName(),
					currentBalance: loanAccount.getCurrentBalance(),
					bankName: loanAccount.getLenderBank(),
					accountNumber: loanAccount.getLoanAccountNumber()
				};
			});
			return loanAccounts;
		}
	}, {
		key: 'getMenuOptions',
		value: function getMenuOptions() {
			var menuOptions = [];
			menuOptions.push({
				buttonText: 'Add Loan A/C',
				buttonIcon: _react2.default.createElement(
					'span',
					null,
					'+'
				),
				onClick: this.addLoanAccount
			});
			return menuOptions;
		}
	}, {
		key: 'getLoanTransactions',
		value: function getLoanTransactions(account) {
			var records = [];
			if (account && account.accountId) {
				// const TxnTypeConstant = require('../../Constants/TxnTypeConstant');
				// const ColorConstants = require('../../Constants/ColorConstants');
				var DataLoader = require('../../DBManager/DataLoader');
				var dataLoader = new DataLoader();
				var loanAccountId = account.accountId;
				var txns = dataLoader.getLoanTransactions({ loanAccountId: loanAccountId });
				records = txns.map(function (txn) {
					var loanTxnId = Number(txn.getLoanTxnId());
					var loanTxnType = Number(txn.getLoanType());
					var principalAmount = Number(txn.getPrincipalAmount());
					var interestAmount = Number(txn.getInterestAmount());
					var date = txn.getTransactionDate();
					var paymentTypeId = txn.getPaymentTypeId();
					return {
						loanTxnId: loanTxnId,
						loanAccountId: loanAccountId,
						loanTxnType: loanTxnType,
						principalAmount: principalAmount,
						interestAmount: interestAmount,
						date: date,
						paymentTypeId: paymentTypeId
					};
				});
			}
			return records;
		}
	}, {
		key: 'onItemSelected',
		value: function onItemSelected(row) {
			var _this3 = this;

			var fromSync = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

			if (!fromSync) {
				if (row && row.node && !row.node.selected) {
					return false;
				}
			}
			var currentItem = null;
			if (row && row.data) {
				currentItem = row.data;
			} else if (this.state.currentItem) {
				currentItem = this.allLoanAccounts.find(function (bank) {
					return _this3.state.currentItem.accountId === bank.accountId;
				});
			}
			var loanTransactions = this.getLoanTransactions(currentItem);
			this.setState({
				records: loanTransactions,
				currentItem: currentItem
			});
		}
	}, {
		key: 'filterItems',
		value: function filterItems(searchText) {
			if (!searchText) {
				this.setState({ loanAccounts: this.allLoanAccounts, searchText: '' });
				return false;
			}
			var query = searchText.toLowerCase();
			var filteredItems = this.allLoanAccounts.filter(function (category) {
				return category.accountName.toLowerCase().includes(query);
			});
			var records = this.state.records;
			var currentItem = this.state.currentItem;
			if (!filteredItems.length) {
				records = [];
				currentItem = null;
			}
			this.setState({
				records: records,
				currentItem: currentItem,
				loanAccounts: filteredItems,
				searchText: searchText
			});
		}
	}, {
		key: 'getPaymentOptions',
		value: function getPaymentOptions() {
			var paymentInfoCache = new _PaymentInfoCache2.default();
			var paymentTypeNames = paymentInfoCache.getListOfPaymentAccountName();
			paymentTypeNames = paymentTypeNames.filter(function (paymentType) {
				return paymentType != 'Cheque';
			});
			var paymentOptions = paymentTypeNames.map(function (paymentTypeName) {
				return {
					key: paymentTypeName,
					value: paymentTypeName
				};
			});
			return paymentOptions;
		}
	}, {
		key: 'render',
		value: function render() {
			var _this4 = this;

			var _state = this.state,
			    loanAccounts = _state.loanAccounts,
			    records = _state.records,
			    currentItem = _state.currentItem;

			return _react2.default.createElement(
				'div',
				{ className: 'd-flex flex-column', style: { height: '100%' } },
				_react2.default.createElement(
					'ul',
					{ className: 'tab-container d-flex' },
					_react2.default.createElement(
						'li',
						{ className: 'tab-items col' },
						'Loan Accounts'
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'd-flex align-items-stretch flex-container' },
					this.allLoanAccounts.length === 0 && _react2.default.createElement(_FirstScreen2.default, {
						image: '../inlineSVG/first-screens/bank.svg',
						text: 'Add your loan accounts & track loan transactions all at one place.',
						onClick: this.addLoanAccount,
						buttonLabel: 'Add Your First Loan A/C'
					}),
					this.allLoanAccounts.length > 0 && _react2.default.createElement(
						_react2.default.Fragment,
						null,
						_react2.default.createElement(
							'div',
							{ className: 'flex-column d-flex left-column' },
							_react2.default.createElement(
								'div',
								{ className: 'listheaderDiv d-flex mt-20 mb-10' },
								_react2.default.createElement(
									'div',
									{ className: (0, _classnames2.default)('searchDiv', this.state.showSearch ? 'width100 d-flex' : '')
									},
									_react2.default.createElement(_SearchBox2.default, { throttle: 500,
										filter: this.filterItems,
										collapsed: !this.state.showSearch,
										collapsedTextBoxClass: 'width100',
										onCollapsedClick: function onCollapsedClick() {
											_this4.setState({ showSearch: true });
										},
										onInputBlur: function onInputBlur() {
											_this4.setState({ showSearch: false });
										}
									})
								),
								_react2.default.createElement(
									'div',
									{ className: 'buttonDiv flex-1 d-flex justify-content-end' },
									_react2.default.createElement(_ButtonDropdown2.default, {
										addPadding: true,
										menuOptions: this.getMenuOptions(),
										collapsed: this.state.showSearch
									})
								)
							),
							_react2.default.createElement(_LoanAccountsList2.default, {
								onItemSelected: this.onItemSelected,
								items: loanAccounts,
								onEditAccount: this.editLoanAccount,
								loanAccountId: currentItem ? currentItem.accountId : ''
							})
						),
						_react2.default.createElement(
							'div',
							{ className: 'd-flex flex-column flex-grow-1 right-column' },
							_react2.default.createElement(_LoanAccountDetail2.default, {
								currentItem: currentItem,
								handleAdjustLoanClick: this.handleAdjustLoanClick,
								handlePayEmiClick: this.handlePayEmiClick
							}),
							_react2.default.createElement(_LoanTransactions2.default, {
								items: records,
								openTransaction: this.openTransaction,
								addExtraColumns: true
							})
						)
					),
					this.state.showAddLoanModal && _react2.default.createElement(_AddLoanAccountModal2.default, {
						isOpen: this.state.showAddLoanModal,
						onClose: function onClose() {
							return _this4.setState({ showAddLoanModal: false });
						},
						onSave: this.onSaveLoanAccount,
						accountId: this.state.loanAccountId,
						paymentOptions: this.state.paymentOptions,
						onPaymentOptionsUpdate: this.updatePaymentOptions
					}),
					this.state.showAdjustLoanModal && _react2.default.createElement(_AdjustLoanAccountModal2.default, {
						isOpen: this.state.showAdjustLoanModal,
						onClose: function onClose() {
							return _this4.setState({ showAdjustLoanModal: false });
						},
						onSave: this.onSaveModal,
						accountId: this.state.currentItem.accountId,
						transaction: this.state.transaction
					}),
					this.state.showPayEmiModal && _react2.default.createElement(_PayEmiModal2.default, {
						isOpen: this.state.showPayEmiModal,
						onClose: function onClose() {
							_this4.setState({ showPayEmiModal: false });
						},
						onSave: this.onSaveModal,
						accountId: this.state.currentItem.accountId,
						transaction: this.state.transaction,
						paymentOptions: this.state.paymentOptions,
						onPaymentOptionsUpdate: this.updatePaymentOptions
					})
				)
			);
		}
	}]);
	return LoanAccountsContainer;
}(_react2.default.Component);

var _initialiseProps = function _initialiseProps() {
	var _this5 = this;

	this.onSaveLoanAccount = function () {
		_this5.setState({
			showAddLoanModal: false
		});
		_this5.onResume();
	};

	this.addLoanAccount = function () {
		_this5.setState({
			showAddLoanModal: true,
			loanAccountId: ''
		});
	};

	this.editLoanAccount = function (loanAccountId) {
		_this5.setState({
			showAddLoanModal: true,
			loanAccountId: loanAccountId
		});
	};

	this.handlePayEmiClick = function (loanTxnId) {
		var transaction = {};
		if (loanTxnId) {
			transaction = _this5.state.records.find(function (txn) {
				return txn.loanTxnId === loanTxnId;
			});
		}
		_this5.setState({
			showPayEmiModal: true,
			transaction: transaction
		});
	};

	this.onSaveModal = function () {
		_this5.setState({
			showPayEmiModal: false,
			showAdjustLoanModal: false
		});
		_this5.onResume();
	};

	this.handleAdjustLoanClick = function (loanTxnId) {
		var transaction = {};
		if (loanTxnId) {
			transaction = _this5.state.records.find(function (txn) {
				return txn.loanTxnId === loanTxnId;
			});
		}
		_this5.setState({
			showAdjustLoanModal: true,
			transaction: transaction
		});
	};

	this.updatePaymentOptions = function () {
		var paymentOptions = _this5.getPaymentOptions();
		_this5.setState({
			paymentOptions: paymentOptions
		});
	};

	this.openTransaction = function (loanTxnId, loanTxnType) {
		switch (loanTxnType) {
			case _TxnTypeConstant2.default.TXN_TYPE_LOAN_EMI_PAYMENT:
				_this5.handlePayEmiClick(loanTxnId);
				break;
			case _TxnTypeConstant2.default.TXN_TYPE_LOAN_ADJUSTMENT:
				_this5.handleAdjustLoanClick(loanTxnId);
				break;
			default:
				ToastHelper.info('Cannot open this transaction');
		}
	};
};

LoanAccountsContainer.propTypes = {
	onResume: _propTypes2.default.func,
	showAddLoanModal: _propTypes2.default.bool
};

exports.default = LoanAccountsContainer;