Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _leftNav = require('./AppLayout/leftNav');

var _leftNav2 = _interopRequireDefault(_leftNav);

var _headingNav = require('./AppLayout/headingNav');

var _headingNav2 = _interopRequireDefault(_headingNav);

var _dialog = require('./AppLayout/dialog');

var _dialog2 = _interopRequireDefault(_dialog);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

require('../../Utilities/DBWindow'); // 'use babel';

var Vyapar = function (_React$Component) {
	(0, _inherits3.default)(Vyapar, _React$Component);

	function Vyapar(props) {
		(0, _classCallCheck3.default)(this, Vyapar);

		var _this = (0, _possibleConstructorReturn3.default)(this, (Vyapar.__proto__ || (0, _getPrototypeOf2.default)(Vyapar)).call(this, props));

		_initialiseProps.call(_this);

		var FTUHelper = require('../../Utilities/FTUHelper');
		var isFirstSaleTxnCreated = FTUHelper.isFirstSaleTxnCreated();
		var canShowPaymentReminderInHeader = FTUHelper.canShowPaymentReminderInHeader();
		var canShowPrintCentreInHeader = FTUHelper.canShowPrintCentreInHeader();
		_this.state = {
			isFirstSaleTxnCreated: isFirstSaleTxnCreated,
			canShowPaymentReminderInHeader: canShowPaymentReminderInHeader,
			canShowPrintCentreInHeader: canShowPrintCentreInHeader
		};
		return _this;
	}

	(0, _createClass3.default)(Vyapar, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			var _state = this.state,
			    isFirstSaleTxnCreated = _state.isFirstSaleTxnCreated,
			    canShowPaymentReminderInHeader = _state.canShowPaymentReminderInHeader,
			    canShowPrintCentreInHeader = _state.canShowPrintCentreInHeader;

			if (!isFirstSaleTxnCreated || !canShowPaymentReminderInHeader || !canShowPrintCentreInHeader) {
				document.addEventListener('saveTxnEvent', this.handleSaveSaleTxn);
			}
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			document.removeEventListener('saveTxnEvent', this.handleSaveSaleTxn);
		}
	}, {
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				'div',
				null,
				_react2.default.createElement('div', { id: 'loading', style: { zIndex: 100000 }, hidden: true }),
				_react2.default.createElement(
					'div',
					{ id: 'loader', className: 'loaderWrapper', hidden: true },
					_react2.default.createElement('div', { className: 'loader' }),
					_react2.default.createElement('div', { id: 'loadingText', className: 'loadingText blink_me' })
				),
				_react2.default.createElement(
					'div',
					{ className: 'loaderWrapper2', style: { zIndex: '100000' }, hidden: true },
					_react2.default.createElement('div', { className: 'loader2' }),
					_react2.default.createElement('div', { className: 'loadingText2 ' }),
					_react2.default.createElement('div', { className: 'loadingText3 blink_me2' })
				),
				_react2.default.createElement('div', { id: 'closeBookLoader', className: 'closeBookloaderWrapper', hidden: true }),
				_react2.default.createElement(
					'div',
					{ id: 'startIntro' },
					_react2.default.createElement(_leftNav2.default, {
						isFirstSaleTxnCreated: this.state.isFirstSaleTxnCreated }),
					_react2.default.createElement(
						'div',
						{ className: 'zoomImageDialog hide textAlignCentre', id: 'zoomImageDialog' },
						_react2.default.createElement('img', { width: '500px', height: '500px' })
					),
					_react2.default.createElement(_headingNav2.default, {
						canShowPaymentReminderInHeader: this.state.canShowPaymentReminderInHeader,
						canShowPrintCentreInHeader: this.state.canShowPrintCentreInHeader,
						isFirstSaleTxnCreated: this.state.isFirstSaleTxnCreated
					}),
					_react2.default.createElement(
						'div',
						{ className: 'centreDiv', id: 'centreDiv' },
						_react2.default.createElement(
							'div',
							{ className: 'dynamicDiv', id: 'dynamicDiv' },
							_react2.default.createElement(
								'div',
								{ id: 'modelContainer', style: { display: 'none', height: '100%', zIndex: '99' }, tabIndex: '0' },
								_react2.default.createElement(
									'div',
									{ className: 'closeWrapper' },
									_react2.default.createElement('img', { src: './../inlineSVG/close.svg', id: 'close', className: 'pointer floatRight' })
								),
								_react2.default.createElement('div', { className: 'model', id: 'frameDiv' })
							),
							_react2.default.createElement('div', { id: 'defaultPage' })
						)
					)
				),
				_react2.default.createElement(_dialog2.default, null)
			);
		}
	}]);
	return Vyapar;
}(_react2.default.Component);

var _initialiseProps = function _initialiseProps() {
	var _this2 = this;

	this.handleSaveSaleTxn = function () {
		var FTUHelper = require('../../Utilities/FTUHelper');
		var isFirstSaleTxnCreated = _this2.state.isFirstSaleTxnCreated || FTUHelper.isFirstSaleTxnCreated();
		var canShowPaymentReminderInHeader = _this2.state.canShowPaymentReminderInHeader || FTUHelper.canShowPaymentReminderInHeader();
		var canShowPrintCentreInHeader = _this2.state.canShowPrintCentreInHeader || FTUHelper.canShowPrintCentreInHeader();
		_this2.setState({
			isFirstSaleTxnCreated: isFirstSaleTxnCreated,
			canShowPaymentReminderInHeader: canShowPaymentReminderInHeader,
			canShowPrintCentreInHeader: canShowPrintCentreInHeader
		});
		if (isFirstSaleTxnCreated && canShowPaymentReminderInHeader && canShowPrintCentreInHeader) {
			document.removeEventListener('saveTxnEvent', _this2.handleSaveSaleTxn);
		}
	};
};

exports.default = Vyapar;


_reactDom2.default.render(_react2.default.createElement(Vyapar, null), document.getElementById('vyapar'));