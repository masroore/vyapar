Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _ErrorCode = require('../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

var _ItemCategoryLogic = require('../../BizLogic/ItemCategoryLogic');

var _ItemCategoryLogic2 = _interopRequireDefault(_ItemCategoryLogic);

var _AddEditCategoryModal = require('./AddEditCategoryModal');

var _AddEditCategoryModal2 = _interopRequireDefault(_AddEditCategoryModal);

var _ButtonDropdown = require('./ButtonDropdown');

var _ButtonDropdown2 = _interopRequireDefault(_ButtonDropdown);

var _AddCircleOutline = require('@material-ui/icons/AddCircleOutline');

var _AddCircleOutline2 = _interopRequireDefault(_AddCircleOutline);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AddCategoryButton = function (_React$Component) {
	(0, _inherits3.default)(AddCategoryButton, _React$Component);

	function AddCategoryButton(props) {
		(0, _classCallCheck3.default)(this, AddCategoryButton);

		var _this = (0, _possibleConstructorReturn3.default)(this, (AddCategoryButton.__proto__ || (0, _getPrototypeOf2.default)(AddCategoryButton)).call(this, props));

		_this.state = {
			isOpen: false
		};
		_this.addCategory = _this.addCategory.bind(_this);
		_this.onClose = _this.onClose.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(AddCategoryButton, [{
		key: 'onClick',
		value: function onClick() {
			this.setState({ isOpen: true });
		}
	}, {
		key: 'onClose',
		value: function onClose() {
			this.setState({
				isOpen: false
			});
		}
	}, {
		key: 'addCategory',
		value: function addCategory(categoryName) {
			MyAnalytics.pushEvent('Add Item Category Open');
			var newItemCategoryLogic = new _ItemCategoryLogic2.default();
			var statusCode = newItemCategoryLogic.saveNewCategory(categoryName);

			if (statusCode == _ErrorCode2.default.ERROR_ITEMCATEGORY_SAVE_SUCCESS) {
				MyAnalytics.pushEvent('Add Item Category Save');
				try {
					this.setState({ isOpen: false });
					window.onResume && window.onResume();
				} catch (err) {
					var logger = require('../../Utilities/logger');
					logger.error(err);
				}
			}
			if (statusCode == _ErrorCode2.default.ERROR_ITEMCATEGORY_SAVE_SUCCESS) {
				ToastHelper.success(statusCode);
			} else {
				ToastHelper.error(statusCode);
			}
		}
	}, {
		key: 'getMenuOptions',
		value: function getMenuOptions() {
			var menuOptions = [];
			menuOptions.push({
				buttonText: 'Add Category',
				buttonIcon: _react2.default.createElement(
					'span',
					null,
					'+'
				),
				onClick: this.onClick.bind(this)
			});
			return menuOptions;
		}
	}, {
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				'div',
				{ id: 'rootElement' },
				_react2.default.createElement(_AddEditCategoryModal2.default, {
					onClose: this.onClose,
					isOpen: this.state.isOpen,
					onSave: this.addCategory,
					title: 'Add Category'
				}),
				_react2.default.createElement(_ButtonDropdown2.default, {
					collapsed: this.props.collapsed,
					addPadding: true,
					menuOptions: this.getMenuOptions()
				})
			);
		}
	}]);
	return AddCategoryButton;
}(_react2.default.Component);

exports.default = AddCategoryButton;

AddCategoryButton.propTypes = {
	collapsed: _propTypes2.default.bool
};