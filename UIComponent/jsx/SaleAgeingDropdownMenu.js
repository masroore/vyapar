Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

exports.default = SaleAgeingDropdownMenuRenderer;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Menu = require('@material-ui/core/Menu');

var _Menu2 = _interopRequireDefault(_Menu);

var _MenuItem = require('@material-ui/core/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _Share = require('@material-ui/icons/Share');

var _Share2 = _interopRequireDefault(_Share);

var _MountComponent = require('./MountComponent');

var _MountComponent2 = _interopRequireDefault(_MountComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SaleAgeingDropdownMenu = function (_React$Component) {
  (0, _inherits3.default)(SaleAgeingDropdownMenu, _React$Component);

  function SaleAgeingDropdownMenu(props) {
    (0, _classCallCheck3.default)(this, SaleAgeingDropdownMenu);

    var _this = (0, _possibleConstructorReturn3.default)(this, (SaleAgeingDropdownMenu.__proto__ || (0, _getPrototypeOf2.default)(SaleAgeingDropdownMenu)).call(this, props));

    _this.handleClick = function (event) {
      _this.setState({ anchorEl: event.currentTarget });
    };

    _this.handleClose = function () {
      _this.setState({ anchorEl: null });
    };

    _this.state = {
      anchorEl: null
    };
    return _this;
  }

  (0, _createClass3.default)(SaleAgeingDropdownMenu, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var options = this.props.colDef.options;
      var anchorEl = this.state.anchorEl;


      return _react2.default.createElement(
        'div',
        { className: 'padding10' },
        _react2.default.createElement(_Share2.default, { style: { color: '#097AA8', fontSize: 20 }, onClick: this.handleClick }),
        _react2.default.createElement(
          _Menu2.default,
          {
            id: 'simple-menu',
            anchorEl: anchorEl,
            open: Boolean(anchorEl),
            onClose: this.handleClose
          },
          _react2.default.createElement(
            _MenuItem2.default,
            { onClick: function onClick() {
                return options.sendTxnOnWhatsApp(_this2);
              } },
            'WhatsApp'
          ),
          _react2.default.createElement(
            _MenuItem2.default,
            { onClick: function onClick() {
                return options.sendTxnOnSMS(_this2);
              } },
            'SMS'
          ),
          _react2.default.createElement(
            _MenuItem2.default,
            { onClick: function onClick() {
                return options.sendTxnOnMail(_this2);
              } },
            'Email'
          )
        )
      );
    }
  }]);
  return SaleAgeingDropdownMenu;
}(_react2.default.Component);

function SaleAgeingDropdownMenuRenderer() {}

// init method gets the details of the cell to be rendere
SaleAgeingDropdownMenuRenderer.prototype.init = function (params) {
  this.eGui = document.createElement('div');
  (0, _MountComponent2.default)(SaleAgeingDropdownMenu, this.eGui, params);
};

SaleAgeingDropdownMenuRenderer.prototype.getGui = function () {
  return this.eGui;
};