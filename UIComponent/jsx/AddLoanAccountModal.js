Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Modal = require('./Modal');

var _Modal2 = _interopRequireDefault(_Modal);

var _TextField = require('@material-ui/core/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _InputAdornment = require('@material-ui/core/InputAdornment');

var _InputAdornment2 = _interopRequireDefault(_InputAdornment);

var _PrimaryButton = require('./UIControls/PrimaryButton');

var _PrimaryButton2 = _interopRequireDefault(_PrimaryButton);

var _MyDate = require('../../Utilities/MyDate');

var _MyDate2 = _interopRequireDefault(_MyDate);

var _DecimalInputFilter = require('../../Utilities/DecimalInputFilter');

var _DecimalInputFilter2 = _interopRequireDefault(_DecimalInputFilter);

var _PaymentInfoSelect = require('./PaymentInfoSelect');

var _PaymentInfoSelect2 = _interopRequireDefault(_PaymentInfoSelect);

var _DynamicList = require('./UIControls/DynamicList');

var _DynamicList2 = _interopRequireDefault(_DynamicList);

var _MenuItem = require('@material-ui/core/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _styles = require('@material-ui/core/styles');

var _LoanAccountCache = require('../../Cache/LoanAccountCache');

var _LoanAccountCache2 = _interopRequireDefault(_LoanAccountCache);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
	return {
		'@global': {
			'.addLoanAccountContainer': {
				padding: '.5rem',
				marginTop: '.5rem',
				'& .loanDetails, & .accountDetails': {
					display: 'grid',
					gridTemplateColumns: '1fr 1fr',
					gridColumnGap: '.5rem',
					gridRowGap: '1rem'
				},
				'& .partitionDiv': {
					margin: '12px 0',
					border: '1px solid #AAAAAA',
					opacity: 0.3
				},
				'& .MuiFormLabel-root.Mui-focused': {
					color: '#1789FC'
				},
				'& .MuiFormHelperText-root.Mui-error.Mui-required': {
					position: 'absolute',
					top: '90%'
				},
				'& .closeBookLoanDetails': {
					gridTemplateColumns: '60%'
				}
			}
		},
		smallFont: {
			fontSize: '12px'
		}
	};
};
var commonTextFieldProps = {
	margin: 'dense',
	variant: 'outlined',
	fullWidth: true,
	color: 'primary'
};

var AddLoanAccountModal = function (_React$Component) {
	(0, _inherits3.default)(AddLoanAccountModal, _React$Component);

	function AddLoanAccountModal(props) {
		(0, _classCallCheck3.default)(this, AddLoanAccountModal);

		var _this = (0, _possibleConstructorReturn3.default)(this, (AddLoanAccountModal.__proto__ || (0, _getPrototypeOf2.default)(AddLoanAccountModal)).call(this, props));

		_this.onSubmit = function () {
			var validationSuccess = _this.validateFields();
			if (!validationSuccess) {
				return;
			}
			var _this$state = _this.state,
			    accountName = _this$state.accountName,
			    lenderBank = _this$state.lenderBank,
			    accountNumber = _this$state.accountNumber,
			    description = _this$state.description,
			    openingBalance = _this$state.openingBalance,
			    openingDate = _this$state.openingDate,
			    interestRate = _this$state.interestRate,
			    termDuration = _this$state.termDuration,
			    processingFee = _this$state.processingFee,
			    processingFeePaidFrom = _this$state.processingFeePaidFrom,
			    loanReceivedIn = _this$state.loanReceivedIn,
			    firmId = _this$state.firmId;

			lenderBank = lenderBank ? lenderBank.trim() : '';
			accountNumber = accountNumber ? accountNumber.trim() : '';
			description = description ? description.trim() : '';
			interestRate = interestRate || '';
			processingFee = processingFee || 0;
			termDuration = termDuration || '';
			var accountId = _this.props.accountId || '';
			openingDate = _MyDate2.default.getDateObj(openingDate, 'dd/mm/yyyy', '/');
			openingDate.setHours(0, 0, 0, 0);

			var LoanAccountModel = require('../../Models/LoanAccountModel');
			var loanAccountModel = new LoanAccountModel({
				accountId: accountId,
				accountName: accountName,
				lenderBank: lenderBank,
				accountNumber: accountNumber,
				description: description,
				openingBalance: openingBalance,
				openingDate: openingDate,
				interestRate: interestRate,
				termDuration: termDuration,
				processingFee: processingFee,
				processingFeePaidFrom: processingFeePaidFrom,
				loanReceivedIn: loanReceivedIn,
				firmId: firmId
			});
			var statusCode = loanAccountModel.saveLoanAccount();
			if (statusCode == ErrorCode.ERROR_LOAN_ACCOUNT_SAVE_SUCCESS || statusCode == ErrorCode.ERROR_LOAN_ACCOUNT_UPDATE_SUCCESS) {
				ToastHelper.success(statusCode);
				_this.props.onSave && _this.props.onSave(loanAccountModel.getLoanAccountId());
			} else {
				ToastHelper.error(statusCode);
			}
		};

		_this.onOpen = function () {
			var SettingCache = require('../../Cache/SettingCache');
			var FirmCache = require('../../Cache/FirmCache');
			var settingCache = new SettingCache();
			var isMultiFirmEnabled = settingCache.getMultipleFirmEnabled();
			var firmId = settingCache.getDefaultFirmId();
			if (isMultiFirmEnabled) {
				var firmCache = new FirmCache();
				_this.firmListDdOptions = (0, _values2.default)(firmCache.getFirmList()).map(function (firm) {
					return { key: firm.firmId, value: firm.firmName };
				});
			}
			if (_this.props.accountId) {
				var loanAccountCache = new _LoanAccountCache2.default();
				var loanAccount = loanAccountCache.getLoanAccountById(_this.props.accountId);
				if (!loanAccount) return;
				_this.title = 'Update Loan Account';
				_this.setState({
					accountName: loanAccount.getLoanAccName(),
					lenderBank: loanAccount.getLenderBank(),
					accountNumber: loanAccount.getLoanAccountNumber(),
					description: loanAccount.getDescription(),
					openingBalance: loanAccount.getOpeningBalance(),
					openingDate: _MyDate2.default.getDate('d/m/y', loanAccount.getOpeningDate()),
					interestRate: loanAccount.getInterestRate(),
					termDuration: loanAccount.getTermDuration(),
					processingFee: loanAccount.getProcessingFee(),
					processingFeePaidFrom: loanAccount.getProcessingFeePaidFrom(),
					loanReceivedIn: loanAccount.getLoanReceivedIn(),
					isClosebookLoanAccount: loanAccount.getIsClosebookLoanAccount(),
					accountNameErrorText: '',
					currentBalErrorText: ''
				});
				firmId = loanAccount.getFirmId();
			}
			_this.setState({
				showFirmDropDown: isMultiFirmEnabled,
				firmId: firmId
			}, function () {
				var selectedOption = _this.firmListDdOptions.find(function (_ref) {
					var key = _ref.key;
					return key == _this.state.firmId;
				});
				_this.firmRef.current && _this.firmRef.current.updateComponent(_this.firmListDdOptions, selectedOption.key, selectedOption.value);
			});
		};

		_this.onClose = function () {
			_this.props.onClose && _this.props.onClose();
		};

		_this.onChange = function (event) {
			var field = event.target.name;
			var value = event.target.value;
			var numberFields = ['openingBalance', 'processingFee'];
			if (numberFields.includes(field)) {
				// This below statement is for DecimalInputFilter to fallback to initial value in case user enters invalid data in edit loan account scenario
				event.target.oldValue = event.target.oldValue || _this.state[field] || '';
				value = _DecimalInputFilter2.default.inputTextFilterForAmount(event.target);
			}
			if (field == 'termDuration') {
				// This below statement is for DecimalInputFilter to fallback to initial value in case user enters invalid data in edit loan account scenario
				event.target.oldValue = event.target.oldValue || _this.state[field] || '';
				value = _DecimalInputFilter2.default.inputTextFilterForOnlyInteger(event.target);
			}
			if (field == 'interestRate') {
				// This below statement is for DecimalInputFilter to fallback to initial value in case user enters invalid data in edit loan account scenario
				event.target.oldValue = event.target.oldValue || _this.state[field] || '';
				value = _DecimalInputFilter2.default.inputTextFilterForPercentage(event.target);
			}
			if (field == 'accountName') {
				_this.setState({
					accountNameErrorText: ''
				});
			}
			if (field == 'openingBalance') {
				_this.setState({
					currentBalErrorText: ''
				});
			}
			_this.setState((0, _defineProperty3.default)({}, field, value));
		};

		_this.onFirmUpdate = function (firmId) {
			_this.setState({ firmId: firmId });
		};

		_this.title = 'Add Loan Account';
		_this.firmListDdOptions = [];
		_this.firmRef = _react2.default.createRef();
		_this.state = {
			accountName: '',
			lenderBank: '',
			accountNumber: '',
			description: '',
			openingBalance: '',
			openingDate: _MyDate2.default.getDate('d/m/y', new Date()),
			interestRate: '',
			termDuration: '',
			processingFee: '',
			processingFeePaidFrom: '1',
			loanReceivedIn: '1',
			accountNameErrorText: '',
			currentBalErrorText: '',
			showFirmDropDown: false,
			firmId: 1
		};
		return _this;
	}

	(0, _createClass3.default)(AddLoanAccountModal, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			var _this2 = this;

			setTimeout(function () {
				$('#openingDate').datepicker({
					dateFormat: 'dd/mm/yy',
					onSelect: function onSelect(value) {
						_this2.setState({
							openingDate: value
						});
					},
					onClose: function onClose(value) {
						try {
							var dt = _MyDate2.default.getDateObj(value, 'dd/mm/yyyy', '/');
							if (dt && dt.toString() === 'Invalid Date' || !value || value.length < 10) {
								_this2.setState({
									openingDate: _MyDate2.default.getDate('d/m/y', new Date())
								});
							}
						} catch (ex) {
							_this2.setState({
								openingDate: _MyDate2.default.getDate('d/m/y', new Date())
							});
						}
					}
				});
			});
		}
	}, {
		key: 'validateFields',
		value: function validateFields() {
			var _state = this.state,
			    _state$accountName = _state.accountName,
			    accountName = _state$accountName === undefined ? '' : _state$accountName,
			    _state$openingBalance = _state.openingBalance,
			    openingBalance = _state$openingBalance === undefined ? '' : _state$openingBalance;

			var accountNameErrorText = '';
			var currentBalErrorText = '';
			var loanAccountCache = new _LoanAccountCache2.default();
			if (!accountName.trim()) {
				accountNameErrorText = 'Cannot be empty';
			} else {
				var loanAccountId = loanAccountCache.getLoanAccountIdByName(accountName);
				if (loanAccountId && loanAccountId != this.props.accountId) {
					accountNameErrorText = 'Account Name should be unique';
				}
			}
			if (openingBalance === '') {
				currentBalErrorText = 'Cannot be empty';
			}
			if (!(Number(openingBalance) >= 0)) {
				currentBalErrorText = 'Cannot be less than zero';
			}

			this.setState({
				accountNameErrorText: accountNameErrorText,
				currentBalErrorText: currentBalErrorText
			});
			return !(accountNameErrorText || currentBalErrorText);
		}
	}, {
		key: 'getLoanDetailsTemplate',
		value: function getLoanDetailsTemplate() {
			var _this3 = this;

			var isEditMode = Number(this.props.accountId);
			var isClosebookLoanAccount = this.state.isClosebookLoanAccount;
			return _react2.default.createElement(
				'div',
				{ className: 'loanDetails ' + (isClosebookLoanAccount ? 'closeBookLoanDetails' : '') },
				_react2.default.createElement(_TextField2.default, (0, _extends3.default)({}, commonTextFieldProps, {
					error: !!this.state.currentBalErrorText,
					helperText: this.state.currentBalErrorText,
					required: true,
					disabled: isClosebookLoanAccount,
					label: isEditMode ? 'Opening Balance' : 'Current Balance',
					placeholder: isEditMode ? 'Opening Balance' : 'Current Balance',
					name: 'openingBalance',
					onChange: this.onChange,
					value: this.state.openingBalance
				})),
				_react2.default.createElement(_TextField2.default, (0, _extends3.default)({}, commonTextFieldProps, {
					label: isEditMode ? 'Opening Date' : 'Balance as of',
					placeholder: isEditMode ? 'Opening Date' : 'Balance as of',
					disabled: isClosebookLoanAccount,
					name: 'openingDate',
					id: 'openingDate',
					value: this.state.openingDate,
					onChange: this.onChange
				})),
				!isClosebookLoanAccount && _react2.default.createElement(
					_react2.default.Fragment,
					null,
					_react2.default.createElement(_PaymentInfoSelect2.default, {
						label: 'Loan received In',
						optionSelectCallback: function optionSelectCallback(paymentTypeId) {
							return _this3.setState({
								loanReceivedIn: paymentTypeId
							});
						},
						defaultPaymentTypeId: this.state.loanReceivedIn,
						paymentOptions: this.props.paymentOptions,
						onPaymentOptionsUpdate: this.props.onPaymentOptionsUpdate
					}),
					_react2.default.createElement('div', null)
				),
				_react2.default.createElement(_TextField2.default, (0, _extends3.default)({}, commonTextFieldProps, {
					label: 'Interest Rate',
					placeholder: 'Interest Rate',
					name: 'interestRate',
					value: this.state.interestRate,
					onChange: this.onChange,
					InputProps: {
						endAdornment: _react2.default.createElement(
							_InputAdornment2.default,
							{ position: 'end' },
							'% per annum'
						),
						style: {
							whiteSpace: 'nowrap'
						}
					}
				})),
				_react2.default.createElement(_TextField2.default, (0, _extends3.default)({}, commonTextFieldProps, {
					label: 'Term Duration(in Months)',
					placeholder: 'Term Duration(in Months)',
					name: 'termDuration',
					onChange: this.onChange,
					inputProps: {
						maxLength: '8'
					},
					value: this.state.termDuration
				})),
				!isClosebookLoanAccount && _react2.default.createElement(
					_react2.default.Fragment,
					null,
					_react2.default.createElement(_TextField2.default, (0, _extends3.default)({}, commonTextFieldProps, {
						label: 'Processing Fee',
						placeholder: 'Processing Fee',
						name: 'processingFee',
						onChange: this.onChange,
						disabled: isClosebookLoanAccount,
						value: this.state.processingFee
					})),
					_react2.default.createElement(_PaymentInfoSelect2.default, {
						label: 'Processing Fee Paid from',
						optionSelectCallback: function optionSelectCallback(paymentTypeId) {
							return _this3.setState({
								processingFeePaidFrom: paymentTypeId
							});
						},
						defaultPaymentTypeId: this.state.processingFeePaidFrom,
						paymentOptions: this.props.paymentOptions,
						onPaymentOptionsUpdate: this.props.onPaymentOptionsUpdate,
						disabled: isClosebookLoanAccount
					})
				)
			);
		}
	}, {
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				_Modal2.default,
				{
					height: this.state.showFirmDropDown ? '600px' : '540px',
					width: '560px',
					onClose: this.onClose,
					onAfterOpen: this.onOpen,
					isOpen: this.props.isOpen,
					title: this.title,
					canHideHeader: true
				},
				_react2.default.createElement(
					'form',
					{ className: 'addLoanAccountContainer', onSubmit: this.onSubmit },
					_react2.default.createElement(
						'div',
						{ className: 'accountDetails' },
						_react2.default.createElement(_TextField2.default, {
							error: !!this.state.accountNameErrorText,
							helperText: this.state.accountNameErrorText,
							label: 'Account Name',
							placeholder: 'Account Name',
							required: true,
							name: 'accountName',
							onChange: this.onChange,
							margin: 'dense',
							variant: 'outlined',
							fullWidth: true,
							inputProps: {
								maxLength: '50'
							},
							color: 'primary',
							value: this.state.accountName
						}),
						_react2.default.createElement(_TextField2.default, {
							label: 'Lender Bank',
							placeholder: 'Lender Bank',
							name: 'lenderBank',
							onChange: this.onChange,
							inputProps: {
								maxLength: '50'
							},
							margin: 'dense',
							variant: 'outlined',
							fullWidth: true,
							color: 'primary',
							value: this.state.lenderBank
						}),
						_react2.default.createElement(_TextField2.default, {
							label: 'Account Number',
							placeholder: 'Account Number',
							name: 'accountNumber',
							onChange: this.onChange,
							inputProps: {
								maxLength: '30'
							},
							margin: 'dense',
							variant: 'outlined',
							fullWidth: true,
							color: 'primary',
							value: this.state.accountNumber
						}),
						_react2.default.createElement(_TextField2.default, {
							label: 'Description',
							placeholder: 'Description',
							name: 'description',
							onChange: this.onChange,
							margin: 'dense',
							variant: 'outlined',
							fullWidth: true,
							color: 'primary',
							value: this.state.description
						}),
						this.state.showFirmDropDown && _react2.default.createElement(
							_DynamicList2.default,
							{
								ref: this.firmRef,
								label: 'Firm',
								optionList: this.firmListDdOptions,
								selectedOption: this.state.firmId,
								optionSelectCallback: this.onFirmUpdate,
								allowAdd: false,
								allowDelete: false
							},
							this.firmListDdOptions.map(function (option) {
								return _react2.default.createElement(
									_MenuItem2.default,
									{ key: option.key, value: option.key },
									option.value
								);
							})
						)
					),
					_react2.default.createElement('div', { className: 'partitionDiv' }),
					this.getLoanDetailsTemplate(),
					_react2.default.createElement(
						_PrimaryButton2.default,
						{ style: { color: 'white', backgroundColor: '#1789FC', marginTop: '.5rem', float: 'right' }, 'data-shortcut': 'CTRL_S', onClick: this.onSubmit },
						'Save'
					)
				)
			);
		}
	}]);
	return AddLoanAccountModal;
}(_react2.default.Component);

AddLoanAccountModal.propTypes = {
	onClose: _propTypes2.default.func,
	onSave: _propTypes2.default.func,
	isOpen: _propTypes2.default.bool,
	title: _propTypes2.default.string,
	classes: _propTypes2.default.object,
	paymentOptions: _propTypes2.default.arrayOf(_propTypes2.default.shape({
		key: _propTypes2.default.string,
		value: _propTypes2.default.string
	})),
	accountId: _propTypes2.default.string,
	onPaymentOptionsUpdate: _propTypes2.default.func
};

exports.default = (0, _styles.withStyles)(styles)(AddLoanAccountModal);