Object.defineProperty(exports, "__esModule", {
  value: true
});

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _trashableReact = require('trashable-react');

var _trashableReact2 = _interopRequireDefault(_trashableReact);

var _isOnline = require('is-online');

var _isOnline2 = _interopRequireDefault(_isOnline);

var _sharingMenuOptions = require('./UIControls/sharingMenuOptions');

var _sharingMenuOptions2 = _interopRequireDefault(_sharingMenuOptions);

var _NamesModalPopupForSharing = require('./UIControls/NamesModalPopupForSharing');

var _NamesModalPopupForSharing2 = _interopRequireDefault(_NamesModalPopupForSharing);

var _ItemCache = require('../../Cache/ItemCache');

var _ItemCache2 = _interopRequireDefault(_ItemCache);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
  return {
    '@global': {
      '.sharing-options-form-body .ag-root': {
        backgroundColor: '#FFFFFF'
      },
      '.ReactModal__Content .modalContainer': {
        height: '100%'
      },
      '.sharing-options-form-body .ag-header-select-all span, .sharing-options-form-body .ag-selection-checkbox span': {
        height: 16,
        width: 16,
        backgroundSize: 16
      },
      '.sharing-options-form-body .ag-cell-value': {},
      '.sharing-options-form-body .ag-row .ag-cell': {
        display: 'flex',
        alignItems: 'center',
        outline: 'none'
      },
      '.sharing-options-form-body .ag-cell-value span': {
        width: '100%'
      },
      '.sharing-options-form-body .ag-cell-value div': {
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis'
      },
      '.sharing-options-form-body .ag-cell-value .cell-error': {
        borderBottom: '1px solid red'
      },
      '.sharing-options-form-body .ag-header-cell.alignLeft': {
        display: 'flex',
        alignItems: 'center'
      },
      '.sharing-options-form-body .gridContainer  .quickFilter': {
        justifyContent: 'flex-start !important',
        paddingBottom: 10
      },
      '.sharing-options-form-body .gridContainer .quickFilter .gridTitle': {
        padding: 0
      },
      '.sharing-options-form-body .gridContainer .quickFilter .gridQuickFilter div': {
        marginLeft: 0
      },
      '.sharing-options-form-body  .ag-header-container .ag-header-cell': {
        fontWeight: 600
      },
      '.sharing-options-form-body .ag-icon-checkbox-unchecked:empty': {
        backgroundImage: 'url(../inlineSVG/checkbox_empty.svg)'
      },
      '.sharing-options-form-body .ag-icon-checkbox-checked:empty': {
        backgroundImage: 'url(../inlineSVG/checkbox-blue.svg)'
      }
    }
  };
};

var ItemSharingOptions = function (_React$Component) {
  (0, _inherits3.default)(ItemSharingOptions, _React$Component);

  function ItemSharingOptions(props) {
    (0, _classCallCheck3.default)(this, ItemSharingOptions);

    var _this = (0, _possibleConstructorReturn3.default)(this, (ItemSharingOptions.__proto__ || (0, _getPrototypeOf2.default)(ItemSharingOptions)).call(this, props));

    _this.state = {
      openNamesModal: false
    };

    _this.handleEmail = function (api) {
      var selectedPartiesForEmail = {};
      var selectedNodes = api && api.getSelectedNodes() || [];

      selectedNodes.forEach(function (node) {
        var name = node.data;
        var email = name.getEmail();
        if (email) {
          selectedPartiesForEmail[name.getNameId()] = email;
        }
      });

      if (!selectedNodes || selectedNodes.length === 0 || !selectedPartiesForEmail || (0, _keys2.default)(selectedPartiesForEmail).length === 0) {
        ToastHelper.error('Please select at least one party with email to share item details.');
        return;
      }

      (0, _isOnline2.default)().then(function (online) {
        if (online) {
          var ItemImages = require('../../BizLogic/ItemImages');
          var SettingCache = require('../../Cache/SettingCache');
          var MyDouble = require('../../Utilities/MyDouble');

          var _require = require('../jsx/SalePurchaseContainer/SalePurchaseHelpers'),
              getTaxInclusivePrice = _require.getTaxInclusivePrice;

          var settingCache = new SettingCache();
          var itemImages = new ItemImages();
          var itemImageList = itemImages.getItemImageListByItemId(_this.props.itemId);
          var item = _this.itemCache.getItemById(_this.props.itemId);
          var isPartWiseRateEnabled = settingCache.getPartyWiseItemRateEnabled();
          var receiverAddresses = '';
          var sendEmail = _this.sendEmail();
          var saleUnitPrice = MyDouble.getAmountWithDecimal(getTaxInclusivePrice(item.getItemSaleUnitPrice(), item.getItemTaxId(), item.getItemTaxTypeSale()));
          var itemData = {
            itemName: item.getItemName(),
            itemDesc: item.getItemDescription(),
            itemCode: item.getItemCode(),
            saleUnitPrice: saleUnitPrice
          };
          var DataLoader = require('../../DBManager/DataLoader.js');
          var dataloader = new DataLoader();
          for (var nameId in selectedPartiesForEmail) {
            var email = selectedPartiesForEmail[nameId];
            if (isPartWiseRateEnabled) {
              var partyRate = dataloader.LoadPartyWiseItemRate(nameId, _this.props.itemId);
              itemData.saleUnitPrice = partyRate && partyRate.getSalePrice() && partyRate.getSalePrice() > 0 ? MyDouble.getAmountWithDecimal(partyRate.getSalePrice()) : saleUnitPrice;
              sendEmail(itemData, email, itemImageList);
            } else {
              receiverAddresses += email + ';';
            }
          }
          if (!isPartWiseRateEnabled) {
            sendEmail(itemData, receiverAddresses, itemImageList);
          }
        } else {
          ToastHelper.error('No internet connection');
        }
      });

      _this.closeNamesModal();
    };

    _this.sendEmail = function () {
      var FirmCache = require('../../Cache/FirmCache');
      var ItemType = require('../../Constants/ItemType.js');
      var firmCache = new FirmCache();
      var firm = firmCache.getDefaultFirm();
      var firmName = firm.getFirmName();
      var item = _this.itemCache.getItemById(_this.props.itemId);
      var isProduct = item.getItemType() == ItemType.ITEM_TYPE_INVENTORY;
      var header = 'Dear Sir/Madam,' + '\n\n';
      var body = '';
      var subjectToSend = isProduct ? 'Item Details' : 'Service Details';
      var footer = 'Thank You,' + '\n' + firmName;
      var GoogleMail = require('../../BizLogic/GoogleMail');
      var googleMail = new GoogleMail();
      return function (itemData, receiverAddresses, base64Attachments) {
        var defaultText = 'Please find ' + (isProduct ? 'item' : 'service') + ' details ' + (base64Attachments.length > 0 ? 'along with attached ' + (isProduct ? 'item' : 'service') + ' images for better understanding.' : '.') + '\n\n';
        var itemDetails = (itemData.itemName ? (isProduct ? 'Product' : 'Service') + ' Name: ' + itemData.itemName : '') + ' ' + (itemData.itemDesc ? '\n' + (isProduct ? 'Product' : 'Service') + ' Description: ' + itemData.itemDesc : '') + ' ' + (itemData.saleUnitPrice > 0 ? '\n Sale Price: ' + this.settingCache.getCurrencySymbol() + ' ' + itemData.saleUnitPrice : '') + ' \n\n';
        var messageToSend = header + itemDetails + defaultText + body + footer;
        googleMail.sendMail({
          fileNames: null,
          receiverId: receiverAddresses,
          alertUser: false,
          subject: subjectToSend,
          message: messageToSend,
          attachFileNames: null,
          successCallBack: function successCallBack() {
            ToastHelper.success('Email sent successfully');
          },
          errorCallBack: null,
          base64Attachments: base64Attachments
        });
      };
    };

    _this.openNamesModalPopup = function () {
      _this.setState({
        openNamesModal: true
      });
    };

    _this.closeNamesModal = function () {
      _this.setState({
        openNamesModal: false
      });
    };

    _this.itemCache = new _ItemCache2.default();
    return _this;
  }

  (0, _createClass3.default)(ItemSharingOptions, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        _react2.default.Fragment,
        null,
        _react2.default.createElement(_sharingMenuOptions2.default, {
          isVisible: this.props.isVisible,
          handleEmail: this.openNamesModalPopup,
          customClasses: 'itemMenuSharing'
        }),
        _react2.default.createElement(_NamesModalPopupForSharing2.default, {
          columnsVisibility: { email: true },
          openNamesModal: this.state.openNamesModal,
          closeNamesModal: this.closeNamesModal,
          handleShare: this.handleEmail
        })
      );
    }
  }]);
  return ItemSharingOptions;
}(_react2.default.Component);

exports.default = (0, _styles.withStyles)(styles)((0, _trashableReact2.default)(ItemSharingOptions));