Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/core/styles');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
	return {
		leftNavOfferDiv: {
			boxShadow: '0px 3px 6px #000029',
			borderRadius: '4px',
			opacity: 1,
			background: '#FFF4D4',
			margin: '10px'
		},
		discountDiv: {
			background: '#FFF4D4',
			color: '#42B62F',
			flexDirection: 'row',
			textTransform: 'uppercase',
			alignItems: 'center',
			justifyContent: 'space-around',
			marginLeft: '5%',
			paddingTop: '6px',
			borderBottomLeftRadius: '4px',
			fontFamily: 'roboto',
			fontStyle: 'normal',
			fontWeight: 'normal',
			fontSize: '18px',
			lineHeight: '22px'
		},
		discountSpan: {
			fontSize: '16px',
			fontWeight: 'bold'
		},
		offerHeaderDiv: {
			background: '#FFF4D4',
			textAlign: 'center',
			color: '#2E2F32',
			fontStyle: 'normal',
			fontWeight: 'Bold',
			fontSize: '19px',
			fontFamily: 'roboto',
			paddingTop: '10px',
			borderTopLeftRadius: '4px',
			borderTopRightRadius: '4px'
		},
		offerButtonDiv: {
			textAlign: 'center'
		},
		dateSpan: {
			background: 'rgba(255, 195, 18, 0.45)',
			color: '#4B4B4B',
			fontSize: '14px',
			textAlign: 'center',
			marginTop: '10px',
			padding: '3px'
		},
		buyLicenseBtn: {
			background: '#ED1A3B',
			color: '#FFFFFF',
			marginTop: '10px',
			borderRadius: '6px',
			width: '122px',
			height: '32px',
			fontSize: '12px',
			fontStyle: 'normal',
			fontWeight: 'bold',
			marginLeft: '30%',
			display: 'flex',
			justifyContent: 'center',
			alignItems: 'center'
		}
	};
}; // 'use babel';

var FestiveOffer = function (_React$Component) {
	(0, _inherits3.default)(FestiveOffer, _React$Component);

	function FestiveOffer(props) {
		(0, _classCallCheck3.default)(this, FestiveOffer);

		var _this = (0, _possibleConstructorReturn3.default)(this, (FestiveOffer.__proto__ || (0, _getPrototypeOf2.default)(FestiveOffer)).call(this, props));

		_this.checkForOffers = function () {
			var PlansUtility = require('../../Utilities/PlansUtility');
			PlansUtility.checkForOffers(function (planDetails) {
				var showOffer = false;
				var offerName = '';
				var discountOff = '';
				var dates = '';
				if (planDetails) {
					showOffer = planDetails.banner_status == 1;
					offerName = planDetails.banner_sale_type;
					discountOff = planDetails.banner_discount_percentage;
					dates = planDetails.banner_time;
				}
				_this.setState({
					showOffer: showOffer,
					offerName: offerName,
					discountOff: discountOff,
					dates: dates
				});
			});
		};

		_this.handleClick = function () {
			var MyAnalytics = require('../../Utilities/analyticsHelper');
			MyAnalytics.pushEvent(_this.state.offerName + ' Sale banner clicked');
			var CommonUtility = require('../../Utilities/CommonUtility');
			CommonUtility.loadDefaultPage('plans.html');
		};

		_this.state = {
			showOffer: false,
			offerName: '',
			discountOff: '',
			dates: ''
		};
		return _this;
	}

	(0, _createClass3.default)(FestiveOffer, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			var _this2 = this;

			setTimeout(function () {
				_this2.checkForOffers();
			}, 3000);
		}
	}, {
		key: 'render',
		value: function render() {
			var classes = this.props.classes;

			return this.state.showOffer && _react2.default.createElement(
				'div',
				{ className: classes.leftNavOfferDiv },
				_react2.default.createElement(
					'div',
					{ className: classes.offerHeaderDiv },
					this.state.offerName
				),
				_react2.default.createElement(
					'div',
					{ className: classes.discountDiv },
					_react2.default.createElement(
						'div',
						null,
						'UPTO ',
						_react2.default.createElement(
							'span',
							{ className: classes.discountSpan },
							this.state.discountOff
						),
						' OFF '
					)
				),
				_react2.default.createElement(
					'div',
					{ className: classes.offerButtonDiv },
					_react2.default.createElement(
						'button',
						{ onClick: this.handleClick, className: classes.buyLicenseBtn },
						'Buy License',
						_react2.default.createElement(
							'span',
							{ style: { marginLeft: '10px' } },
							_react2.default.createElement(
								'svg',
								{ width: '15', height: '12', viewBox: '0 0 15 12', fill: 'none', xmlns: 'http://www.w3.org/2000/svg' },
								_react2.default.createElement('path', { d: 'M0.75 6.75009L12.6 6.75008L9.15 10.7251C8.85 11.0251 8.925 11.4751 9.225 11.7751C9.525 12.0751 9.975 12.0001 10.275 11.7001L14.775 6.45008C14.775 6.45008 14.85 6.37508 14.85 6.30008C14.85 6.30008 14.85 6.30008 14.85 6.22508C15 6.22508 15 6.15008 15 6.07508L15 6.00008C15 5.92508 15 5.85008 15 5.77508L15 5.70008C15 5.70008 15 5.70008 15 5.62508C15 5.55008 14.925 5.47508 14.925 5.47508L10.425 0.225085C10.2 0.0750842 9.975 8.43626e-05 9.75 8.43823e-05C9.6 8.43954e-05 9.375 0.0750842 9.225 0.150084C8.925 0.450084 8.85 0.900085 9.15 1.20009L12.6 5.25008L0.75 5.25009C0.300001 5.25009 3.92772e-07 5.55009 4.32112e-07 6.00009C4.71452e-07 6.45009 0.375 6.75009 0.75 6.75009Z', fill: 'white' })
							)
						)
					)
				),
				_react2.default.createElement(
					'div',
					{ className: classes.dateSpan },
					this.state.dates
				)
			);
		}
	}]);
	return FestiveOffer;
}(_react2.default.Component);

exports.default = (0, _styles.withStyles)(styles)(FestiveOffer);


FestiveOffer.propTypes = {
	classes: _propTypes2.default.object
};