Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _DateFilter = require('./grid-filters/DateFilter');

var _DateFilter2 = _interopRequireDefault(_DateFilter);

var _NumberFilter = require('./grid-filters/NumberFilter');

var _NumberFilter2 = _interopRequireDefault(_NumberFilter);

var _StringFilter = require('./grid-filters/StringFilter');

var _StringFilter2 = _interopRequireDefault(_StringFilter);

var _MultiSelectionFilter = require('./grid-filters/MultiSelectionFilter');

var _MultiSelectionFilter2 = _interopRequireDefault(_MultiSelectionFilter);

var _SaleAgeingRightArrowComponent = require('./SaleAgeingRightArrowComponent');

var _SaleAgeingRightArrowComponent2 = _interopRequireDefault(_SaleAgeingRightArrowComponent);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SaleAgeingPartyContainer = function (_React$Component) {
	(0, _inherits3.default)(SaleAgeingPartyContainer, _React$Component);

	function SaleAgeingPartyContainer(props) {
		(0, _classCallCheck3.default)(this, SaleAgeingPartyContainer);

		var _this = (0, _possibleConstructorReturn3.default)(this, (SaleAgeingPartyContainer.__proto__ || (0, _getPrototypeOf2.default)(SaleAgeingPartyContainer)).call(this, props));

		_this.getApi = function (params) {
			_this.api = params.api;
			var _this$props = _this.props,
			    getApi = _this$props.getApi,
			    dataParty = _this$props.dataParty;

			getApi && getApi(params);
			_this.api.setRowData(dataParty);
			if (dataParty.length) {
				var bottomRow = _this.getTotalOfRows(dataParty);
				_this.api.setPinnedBottomRowData([bottomRow]);
			}
		};

		_this.options = _this.props.options;
		_this.state = {
			columnDefs: [{
				field: 'name',
				filter: _StringFilter2.default,
				headerName: 'PARTY',
				width: 30
			}, {
				field: 'current',
				headerName: 'Current',
				width: 30,
				filter: _NumberFilter2.default,
				cellRenderer: function cellRenderer(params) {
					if (params.value < 0) {
						return '-';
					} else {
						return _MyDouble2.default.getAmountWithDecimalAndCurrencyWithoutSign(params.value);
					}
				}
			}, {
				field: 'bucket1',
				headerName: '1-30 Days',
				width: 30,
				filter: _NumberFilter2.default,
				cellRenderer: function cellRenderer(params) {
					if (params.value < 0) {
						return '-';
					} else {
						return _MyDouble2.default.getAmountWithDecimalAndCurrencyWithoutSign(params.value);
					}
				}
			}, {
				field: 'bucket2',
				headerName: '31-45 Days',
				width: 30,
				filter: _NumberFilter2.default,
				cellRenderer: function cellRenderer(params) {
					if (params.value < 0) {
						return '-';
					} else {
						return _MyDouble2.default.getAmountWithDecimalAndCurrencyWithoutSign(params.value);
					}
				}
			}, {
				field: 'bucket3',
				headerName: '46-60 Days',
				width: 30,
				filter: _NumberFilter2.default,
				cellRenderer: function cellRenderer(params) {
					if (params.value < 0) {
						return '-';
					} else {
						return _MyDouble2.default.getAmountWithDecimalAndCurrencyWithoutSign(params.value);
					}
				}
			}, {
				field: 'bucket4',
				headerName: 'Over 60 Days',
				width: 30,
				filter: _NumberFilter2.default,
				cellRenderer: function cellRenderer(params) {
					if (params.value < 0) {
						return '-';
					} else {
						return _MyDouble2.default.getAmountWithDecimalAndCurrencyWithoutSign(params.value);
					}
				}
			}, {
				field: 'total',
				headerName: 'Total',
				width: 30,
				filter: _NumberFilter2.default,
				cellRenderer: function cellRenderer(params) {
					if (params.value < 0) {
						return '-';
					} else {
						return _MyDouble2.default.getAmountWithDecimalAndCurrencyWithoutSign(params.value);
					}
				}
			}, {
				field: 'nameId',
				headerName: '',
				width: 5,
				suppressFilter: true,
				cellRenderer: _SaleAgeingRightArrowComponent2.default,
				options: {
					openDetailAgeingView: _this.options.openDetailAgeingView
				}
			}]
		};
		_this.getTotalOfRows = _this.getTotalOfRows.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(SaleAgeingPartyContainer, [{
		key: 'getTotalOfRows',
		value: function getTotalOfRows(rowData) {
			var len = rowData.length;
			var totalObject = {
				name: 'Total',
				current: 0,
				bucket1: 0,
				bucket2: 0,
				bucket3: 0,
				bucket4: 0,
				total: 0,
				nameId: 'pinnedRowForSaleAgingReport'

			};
			for (var i = 0; i < len; i++) {
				var data = rowData[i];
				totalObject['current'] = _MyDouble2.default.getBalanceAmountWithDecimal(totalObject['current']) + _MyDouble2.default.getBalanceAmountWithDecimal(data.current);
				totalObject['bucket1'] = _MyDouble2.default.getBalanceAmountWithDecimal(totalObject['bucket1']) + _MyDouble2.default.getBalanceAmountWithDecimal(data.bucket1);
				totalObject['bucket2'] = _MyDouble2.default.getBalanceAmountWithDecimal(totalObject['bucket2']) + _MyDouble2.default.getBalanceAmountWithDecimal(data.bucket2);
				totalObject['bucket3'] = _MyDouble2.default.getBalanceAmountWithDecimal(totalObject['bucket3']) + _MyDouble2.default.getBalanceAmountWithDecimal(data.bucket3);
				totalObject['bucket4'] = _MyDouble2.default.getBalanceAmountWithDecimal(totalObject['bucket4']) + _MyDouble2.default.getBalanceAmountWithDecimal(data.bucket4);
				totalObject['total'] = _MyDouble2.default.getBalanceAmountWithDecimal(totalObject['total']) + _MyDouble2.default.getBalanceAmountWithDecimal(data.total);
			}
			return totalObject;
		}
	}, {
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(nextProps) {
			if (this.api) {
				var dataParty = nextProps.dataParty;

				this.api.setRowData(dataParty);
				if (dataParty.length) {
					var bottomRow = this.getTotalOfRows(dataParty);
					this.api.setPinnedBottomRowData([bottomRow]);
				}
			}
			return false;
		}
	}, {
		key: 'render',
		value: function render() {
			var rowData = this.props.dataParty;
			var totalRowData = this.getTotalOfRows(rowData);
			var getApi = this.props.getApi;

			var gridOptions = {
				enableFilter: true,
				enableSorting: true,
				rowSelection: 'single',
				columnDefs: this.state.columnDefs,
				rowData: rowData,
				rowHeight: 40,
				headerHeight: 40,
				rowClass: 'customVerticalBorder',
				overlayNoRowsTemplate: 'No transactions to show',
				onRowDoubleClicked: this.options.openDetailAgeingView,
				pinnedBottomRowData: [totalRowData],
				getRowNodeId: function getRowNodeId(data) {
					return data.nameId;
				}
			};

			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement(_Grid2.default, {
					gridKey: 'saleAgeingReportPartyGridKey',
					classes: 'alternateRowColor customVerticalBorder gridRowHeight40',
					getApi: this.getApi,
					gridOptions: gridOptions,
					notSelectFirstRow: true
				})
			);
		}
	}]);
	return SaleAgeingPartyContainer;
}(_react2.default.Component);

SaleAgeingPartyContainer.propTypes = {
	dataParty: _propTypes2.default.array.isRequired,
	getApi: _propTypes2.default.func.isRequired,
	options: _propTypes2.default.object.isRequired
};

exports.default = SaleAgeingPartyContainer;