Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _AddEditCategoryModal = require('./AddEditCategoryModal');

var _AddEditCategoryModal2 = _interopRequireDefault(_AddEditCategoryModal);

var _ButtonDropdown = require('./ButtonDropdown');

var _ButtonDropdown2 = _interopRequireDefault(_ButtonDropdown);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AddNameButton = function (_React$Component) {
	(0, _inherits3.default)(AddNameButton, _React$Component);

	function AddNameButton(props) {
		(0, _classCallCheck3.default)(this, AddNameButton);

		var _this = (0, _possibleConstructorReturn3.default)(this, (AddNameButton.__proto__ || (0, _getPrototypeOf2.default)(AddNameButton)).call(this, props));

		_this.state = {
			isOpen: false
		};
		_this.addNewGroup = _this.addNewGroup.bind(_this);
		_this.onSave = _this.onSave.bind(_this);
		_this.onClose = _this.onClose.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(AddNameButton, [{
		key: 'addNewGroup',
		value: function addNewGroup() {
			this.setState({ isOpen: true });
			MyAnalytics.pushEvent('Add New Group Open');
		}
	}, {
		key: 'getMenuOptions',
		value: function getMenuOptions() {
			var menuOptions = [];
			menuOptions.push({
				buttonText: 'Add Party Group',
				buttonIcon: _react2.default.createElement(
					'span',
					null,
					'+'
				),
				onClick: this.addNewGroup
			});
			return menuOptions;
		}
	}, {
		key: 'onSave',
		value: function onSave(name) {
			var PartyGroupLogic = require('../../BizLogic/PartyGroupLogic');
			var ErrorCode = require('../../Constants/ErrorCode');
			var partyGroupLogic = new PartyGroupLogic();
			var statusCode = partyGroupLogic.saveNewGroup(name);
			if (statusCode == ErrorCode.ERROR_PARTYGROUP_SAVE_SUCCESS) {
				MyAnalytics.pushEvent('Add Party Group Save');
				window.onResume && window.onResume();
				ToastHelper.success(ErrorCode.ERROR_PARTYGROUP_SAVE_SUCCESS);
				this.setState({ isOpen: false });
			}
			if (statusCode == ErrorCode.ERROR_PARTYGROUP_SAVE_SUCCESS) {
				ToastHelper.success(statusCode);
			} else {
				ToastHelper.error(statusCode);
			}
		}
	}, {
		key: 'onClose',
		value: function onClose() {
			this.setState({ isOpen: false });
		}
	}, {
		key: 'render',
		value: function render() {
			var isOpen = this.state.isOpen;

			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement(_AddEditCategoryModal2.default, {
					categoryName: '',
					onClose: this.onClose,
					onSave: this.onSave,
					isOpen: isOpen,
					title: 'Add Party Group',
					label: 'Enter Party Group Name'
				}),
				_react2.default.createElement(_ButtonDropdown2.default, {
					collapsed: this.props.collapsed,
					addPadding: true,
					menuOptions: this.getMenuOptions()
				})
			);
		}
	}]);
	return AddNameButton;
}(_react2.default.Component);

AddNameButton.propTypes = {
	classNames: _propTypes2.default.string,
	children: _propTypes2.default.oneOfType([_propTypes2.default.arrayOf(_propTypes2.default.node), _propTypes2.default.node]),
	collapsed: _propTypes2.default.bool
};

exports.default = AddNameButton;