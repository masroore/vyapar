Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _Dialog = require('@material-ui/core/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _DialogActions = require('@material-ui/core/DialogActions');

var _DialogActions2 = _interopRequireDefault(_DialogActions);

var _DialogContent = require('@material-ui/core/DialogContent');

var _DialogContent2 = _interopRequireDefault(_DialogContent);

var _Checkbox = require('@material-ui/core/Checkbox');

var _Checkbox2 = _interopRequireDefault(_Checkbox);

var _DialogTitle = require('@material-ui/core/DialogTitle');

var _DialogTitle2 = _interopRequireDefault(_DialogTitle);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SaleAgingReportPrintOptions = function (_React$Component) {
	(0, _inherits3.default)(SaleAgingReportPrintOptions, _React$Component);

	function SaleAgingReportPrintOptions(props) {
		(0, _classCallCheck3.default)(this, SaleAgingReportPrintOptions);

		var _this = (0, _possibleConstructorReturn3.default)(this, (SaleAgingReportPrintOptions.__proto__ || (0, _getPrototypeOf2.default)(SaleAgingReportPrintOptions)).call(this, props));

		_this.state = {
			open: false
		};
		_this.handleClickOpen = _this.handleClickOpen.bind(_this);
		_this.handleClose = _this.handleClose.bind(_this);
		_this.print = _this.print.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(SaleAgingReportPrintOptions, [{
		key: 'handleClickOpen',
		value: function handleClickOpen() {
			this.setState({ open: true });
		}
	}, {
		key: 'handleClose',
		value: function handleClose() {
			this.setState({ open: false });
		}
	}, {
		key: 'print',
		value: function print() {
			if (this.props.view) {
				this.props.options.printTransactionsFromInvoiceView();
			} else {
				this.props.options.printTransactionsFromPartyView();
			}
			this.handleClose();
		}
	}, {
		key: 'render',
		value: function render() {
			var options = this.props.options;
			var invoiceView = this.props.view;
			var includeGraphInPrint = this.props.includeGraphInPrint;
			var includeInvoiceDetailsInPrint = this.props.includeInvoiceDetailsInPrint;
			return _react2.default.createElement(
				'div',
				null,
				_react2.default.createElement('img', { className: 'header-icon', src: '../inlineSVG/print.svg', onClick: this.handleClickOpen }),
				_react2.default.createElement(
					_Dialog2.default,
					{
						open: this.state.open,
						onClose: this.handleClose,
						'aria-labelledby': 'form-dialog-title'
					},
					_react2.default.createElement(
						_DialogTitle2.default,
						{ id: 'form-dialog-title' },
						'Print Options'
					),
					_react2.default.createElement(
						_DialogContent2.default,
						null,
						_react2.default.createElement(
							'div',
							null,
							'Include graph in report',
							_react2.default.createElement(_Checkbox2.default, {
								checked: includeGraphInPrint,
								onChange: function onChange(e) {
									return options.onChangePrintOptions('includeGraphInPrint', e);
								},
								color: 'primary',
								className: 'checkboxPrimary'
							})
						),
						!invoiceView && _react2.default.createElement(
							'div',
							null,
							'Include Invoice Details',
							_react2.default.createElement(_Checkbox2.default, {
								checked: includeInvoiceDetailsInPrint,
								onChange: function onChange(e) {
									return options.onChangePrintOptions('includeInvoiceDetailsInPrint', e);
								},
								color: 'primary',
								className: 'checkboxPrimary'
							})
						)
					),
					_react2.default.createElement(
						_DialogActions2.default,
						null,
						_react2.default.createElement(
							_Button2.default,
							{ onClick: this.handleClose, color: 'primary' },
							'Cancel'
						),
						_react2.default.createElement(
							_Button2.default,
							{ onClick: this.print, color: 'primary' },
							'Ok'
						)
					)
				)
			);
		}
	}]);
	return SaleAgingReportPrintOptions;
}(_react2.default.Component);

exports.default = SaleAgingReportPrintOptions;


SaleAgingReportPrintOptions.propTypes = {
	options: _propTypes2.default.object.isRequired,
	view: _propTypes2.default.bool.isRequired,
	includeGraphInPrint: _propTypes2.default.bool.isRequired,
	includeInvoiceDetailsInPrint: _propTypes2.default.bool.isRequired
};