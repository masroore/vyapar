Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Modal = require('./Modal');

var _Modal2 = _interopRequireDefault(_Modal);

var _TextField = require('@material-ui/core/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _PrimaryButton = require('./UIControls/PrimaryButton');

var _PrimaryButton2 = _interopRequireDefault(_PrimaryButton);

var _PaymentInfoSelect = require('./PaymentInfoSelect');

var _PaymentInfoSelect2 = _interopRequireDefault(_PaymentInfoSelect);

var _DecimalInputFilter = require('../../Utilities/DecimalInputFilter');

var _DecimalInputFilter2 = _interopRequireDefault(_DecimalInputFilter);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _styles = require('@material-ui/core/styles');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
	return {
		'@global': {
			'.payEmiModalBody': {
				display: 'grid',
				gridColumnGap: '.5rem',
				gridRowGap: '1rem',
				gridTemplateAreas: '\n            "a e"\n            "b ."\n            "c ."\n            "d ."\n            ',
				alignItems: 'center',
				'& :nth-child(1)': {
					gridArea: 'a'
				},
				'& :nth-child(2)': {
					gridArea: 'b'
				},
				'& :nth-child(3)': {
					gridArea: 'c'
				},
				'& :nth-child(4)': {
					gridArea: 'd'
				},
				'& :nth-child(5)': {
					display: 'flex',
					alignItems: 'center',
					gridArea: 'e'
				}
			},
			'.payEmiModalFooter': {
				display: 'flex',
				justifyContent: 'flex-end',
				marginTop: '1rem'
			}
		}
	};
};
var commonTextFieldProps = {
	margin: 'dense',
	variant: 'outlined',
	fullWidth: true,
	color: 'primary'
};

var PayEmiModal = function (_React$Component) {
	(0, _inherits3.default)(PayEmiModal, _React$Component);

	function PayEmiModal(props) {
		(0, _classCallCheck3.default)(this, PayEmiModal);

		var _this = (0, _possibleConstructorReturn3.default)(this, (PayEmiModal.__proto__ || (0, _getPrototypeOf2.default)(PayEmiModal)).call(this, props));

		_this.handleChange = function (event) {
			var _event$target = event.target,
			    name = _event$target.name,
			    value = _event$target.value;

			var numberFields = ['principalAmount', 'interestAmount'];
			if (numberFields.includes(name)) {
				value = _DecimalInputFilter2.default.inputTextFilterForAmount(event.target);
			}
			_this.setState((0, _defineProperty3.default)({}, name, value));
		};

		_this.onOpen = function () {
			var transaction = _this.props.transaction || {};
			var _transaction$principa = transaction.principalAmount,
			    principalAmount = _transaction$principa === undefined ? '' : _transaction$principa,
			    _transaction$interest = transaction.interestAmount,
			    interestAmount = _transaction$interest === undefined ? '' : _transaction$interest,
			    _transaction$date = transaction.date,
			    date = _transaction$date === undefined ? new Date() : _transaction$date,
			    _transaction$paymentT = transaction.paymentTypeId,
			    paymentTypeId = _transaction$paymentT === undefined ? '1' : _transaction$paymentT;

			_this.setState({
				principalAmount: principalAmount,
				interestAmount: interestAmount,
				transactionDate: MyDate.getDate('d/m/y', date),
				paymentTypeId: paymentTypeId
			});
		};

		_this.onSubmit = function (e) {
			e.preventDefault();
			var validationSuccess = _this.validateFields();
			if (!validationSuccess) {
				return;
			}
			var LoanTransactionModel = require('../../Models/LoanTransactionModel');
			var TxnTypeConstant = require('../../Constants/TxnTypeConstant');
			var _this$state = _this.state,
			    principalAmount = _this$state.principalAmount,
			    interestAmount = _this$state.interestAmount,
			    transactionDate = _this$state.transactionDate,
			    paymentTypeId = _this$state.paymentTypeId;

			var loanAccountId = _this.props.accountId;
			var transaction = _this.props.transaction || {};
			var loanTxnId = transaction.loanTxnId || '';
			var loanType = TxnTypeConstant.TXN_TYPE_LOAN_EMI_PAYMENT;
			transactionDate = MyDate.getDateObj(transactionDate, 'dd/mm/yyyy', '/');
			transactionDate.setHours(0, 0, 0, 0);
			var loanTransactionModel = new LoanTransactionModel({
				loanTxnId: loanTxnId,
				principalAmount: principalAmount,
				interestAmount: interestAmount,
				transactionDate: transactionDate,
				paymentTypeId: paymentTypeId,
				loanAccountId: loanAccountId,
				loanType: loanType
			});
			var success = loanTransactionModel.saveLoanTxn();
			if (success) {
				_this.props.onSave && _this.props.onSave(loanTransactionModel.getLoanTxnId());
			}
		};

		_this.handleClose = function () {
			_this.props.onClose && _this.props.onClose();
		};

		_this.state = {
			principalAmount: '',
			interestAmount: '',
			transactionDate: MyDate.getDate('d/m/y', new Date()),
			paymentTypeId: '1'
		};
		return _this;
	}

	(0, _createClass3.default)(PayEmiModal, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			var _this2 = this;

			setTimeout(function () {
				$('.payEmiModalBody #transactionDate').datepicker({
					dateFormat: 'dd/mm/yy',
					onSelect: function onSelect(value) {
						_this2.setState({
							transactionDate: value
						});
					},
					onClose: function onClose(value) {
						try {
							var dt = MyDate.getDateObj(value, 'dd/mm/yyyy', '/');
							if (dt && dt.toString() === 'Invalid Date' || !value || value.length < 10) {
								_this2.setState({
									transactionDate: MyDate.getDate('d/m/y', new Date())
								});
							}
						} catch (ex) {
							_this2.setState({
								transactionDate: MyDate.getDate('d/m/y', new Date())
							});
						}
					}
				});
			});
		}
	}, {
		key: 'validateFields',
		value: function validateFields() {
			var LoanAccountCache = require('../../Cache/LoanAccountCache');
			var loanAccountCache = new LoanAccountCache();
			var isValidationSuccess = true;
			var transaction = this.props.transaction || {};
			var _state = this.state,
			    principalAmount = _state.principalAmount,
			    interestAmount = _state.interestAmount,
			    transactionDate = _state.transactionDate;

			var loanAccountId = this.props.accountId;
			var loanAccount = loanAccountCache.getLoanAccountById(loanAccountId);
			try {
				var currentBalance = Number(loanAccount.getCurrentBalance()) + Number(transaction.principalAmount || 0);
				var startingDate = loanAccount.getOpeningDate();
				transactionDate = MyDate.getDateObj(transactionDate, 'dd/mm/yyyy', '/');
				transactionDate.setHours(0, 0, 0, 0);
				if (!Number(principalAmount) && !Number(interestAmount)) {
					ToastHelper.error('Total Amount cannot be zero.');
					isValidationSuccess = false;
				}
				if (currentBalance < Number(principalAmount)) {
					ToastHelper.error('Principal amount cannot exceed outstanding balance.');
					isValidationSuccess = false;
				}
				if (startingDate.getTime() > transactionDate.getTime()) {
					ToastHelper.error('EMI payment date cannot be before loan starting date.');
					isValidationSuccess = false;
				}
			} catch (error) {
				isValidationSuccess = false;
				var ErrorCode = require('../../Constants/ErrorCode');
				ToastHelper.error(ErrorCode.ERROR_WENT_WRONG);
			}
			return isValidationSuccess;
		}
	}, {
		key: 'render',
		value: function render() {
			var _this3 = this;

			var _state2 = this.state,
			    principalAmount = _state2.principalAmount,
			    interestAmount = _state2.interestAmount,
			    transactionDate = _state2.transactionDate;

			var totalAmount = _MyDouble2.default.getBalanceAmountWithDecimal(Number(interestAmount) + Number(principalAmount));
			return _react2.default.createElement(
				_Modal2.default,
				{
					height: '390px',
					width: '540px',
					onClose: this.handleClose,
					isOpen: this.props.isOpen,
					onAfterOpen: this.onOpen,
					title: 'Make Payment',
					canHideHeader: true
				},
				_react2.default.createElement(
					'form',
					null,
					_react2.default.createElement(
						'div',
						{ className: 'payEmiModalBody' },
						_react2.default.createElement(_TextField2.default, (0, _extends3.default)({
							required: true
						}, commonTextFieldProps, {
							label: 'Principal Amount',
							placeholder: 'Principal Amount',
							value: principalAmount,
							name: 'principalAmount',
							onChange: this.handleChange
						})),
						_react2.default.createElement(_TextField2.default, (0, _extends3.default)({}, commonTextFieldProps, {
							label: 'Interest Amount',
							placeholder: 'Interest Amount',
							value: interestAmount,
							name: 'interestAmount',
							onChange: this.handleChange
						})),
						_react2.default.createElement(_TextField2.default, (0, _extends3.default)({}, commonTextFieldProps, {
							label: 'Total Amount',
							placeholder: 'Total Amount',
							name: 'totalAmount',
							value: totalAmount,
							disabled: true
						})),
						_react2.default.createElement(_PaymentInfoSelect2.default, {
							label: 'Paid from',
							optionSelectCallback: function optionSelectCallback(paymentTypeId) {
								return _this3.setState({
									paymentTypeId: paymentTypeId
								});
							},
							defaultPaymentTypeId: this.state.paymentTypeId,
							paymentOptions: this.props.paymentOptions,
							onPaymentOptionsUpdate: this.props.onPaymentOptionsUpdate
						}),
						_react2.default.createElement(_TextField2.default, (0, _extends3.default)({}, commonTextFieldProps, {
							label: 'Date',
							className: 'transactionDate',
							value: transactionDate,
							name: 'transactionDate',
							id: 'transactionDate',
							onChange: this.handleChange
						}))
					),
					_react2.default.createElement(
						'div',
						{ className: 'payEmiModalFooter' },
						_react2.default.createElement(
							_PrimaryButton2.default,
							{ 'data-shortcut': 'CTRL_S', style: { color: 'white', backgroundColor: '#1789FC', marginTop: '.5rem', float: 'right' }, onClick: this.onSubmit },
							'Save'
						)
					)
				)
			);
		}
	}]);
	return PayEmiModal;
}(_react2.default.Component);

PayEmiModal.propTypes = {
	accountId: _propTypes2.default.string,
	transaction: _propTypes2.default.object,
	onClose: _propTypes2.default.func,
	onSave: _propTypes2.default.func,
	isOpen: _propTypes2.default.bool,
	paymentOptions: _propTypes2.default.arrayOf(_propTypes2.default.shape({
		key: _propTypes2.default.string,
		value: _propTypes2.default.string
	})),
	onPaymentOptionsUpdate: _propTypes2.default.func
};

exports.default = (0, _styles.withStyles)(styles)(PayEmiModal);