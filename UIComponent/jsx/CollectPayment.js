Object.defineProperty(exports, "__esModule", {
	value: true
});

var _typeof2 = require('babel-runtime/helpers/typeof');

var _typeof3 = _interopRequireDefault(_typeof2);

var _set = require('babel-runtime/core-js/set');

var _set2 = _interopRequireDefault(_set);

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _AutoSuggest = require('../../UIComponent/jsx/UIControls/AutoSuggest');

var _AutoSuggest2 = _interopRequireDefault(_AutoSuggest);

var _PrimaryButton = require('../../UIComponent/jsx/UIControls/PrimaryButton');

var _PrimaryButton2 = _interopRequireDefault(_PrimaryButton);

var _FirmCache = require('../../Cache/FirmCache');

var _FirmCache2 = _interopRequireDefault(_FirmCache);

var _ErrorCode = require('../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

var _analyticsHelper = require('../../Utilities/analyticsHelper');

var _analyticsHelper2 = _interopRequireDefault(_analyticsHelper);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CollectPayment = function (_React$Component) {
	(0, _inherits3.default)(CollectPayment, _React$Component);

	function CollectPayment(props) {
		(0, _classCallCheck3.default)(this, CollectPayment);

		var _this = (0, _possibleConstructorReturn3.default)(this, (CollectPayment.__proto__ || (0, _getPrototypeOf2.default)(CollectPayment)).call(this, props));

		_this.updateUpiAccountNumberAndIFSC = function () {
			if (this.selectedValues.selectedAccountNumber && (this.selectedValues.selectedAccountNumber.length < 9 || this.selectedValues.selectedAccountNumber.length > 18)) {
				ToastHelper.error(_ErrorCode2.default.ERROR_ENTER_CORRECT_UPI_ACCOUNT_NUMBER);
				return;
			} else if (this.selectedValues.selectedIFSC && this.selectedValues.selectedIFSC.length !== 11) {
				ToastHelper.error(_ErrorCode2.default.ERROR_ENTER_CORRECT_UPI_IFSC);
				return;
			}
			this.firmObj.setUpiAccountNumber(this.selectedValues.selectedAccountNumber);
			this.firmObj.setUpiIFSC(this.selectedValues.selectedIFSC);
			this.firmObj.updateFirmWithoutObject();
			if (this.props.callback && typeof this.props.callback === 'function') {
				this.props.callback();
			}
			this.closeDialog();
			_analyticsHelper2.default.pushEvent('pop up open  & save');
		};

		_this.closeDialog = function () {
			var closeWrapper = $('#collectPaymentWrapper');
			if (closeWrapper && closeWrapper.is(':visible') && closeWrapper.dialog('isOpen')) {
				closeWrapper.dialog('close').dialog('destroy');
			}

			if (this.props.closeFunc && typeof this.props.closeFunc === 'function') {
				this.props.closeFunc();
			}
		};

		_this.enableDisableQRBtn = function () {
			var canGenerateQRCode = this.selectedValues.selectedAccountNumber.length >= 2 && this.selectedValues.selectedIFSC.length >= 2;
			this.setState({
				canGenerateQRCode: canGenerateQRCode
			});
		};

		_this.handleAutocompleteChange = function (key, value) {
			if (key) {
				this.selectedValues[key] = value;
			}
			this.enableDisableQRBtn();
		};

		_this.renderAccountNumberAutosuggest = function () {
			return _react2.default.createElement(_AutoSuggest2.default, { value: this.selectedValues.selectedAccountNumber, type: 'text', pattern: /^[0-9]+$/i, maxLength: '18', name: 'selectedAccountNumber', onValueChange: this.handleAutocompleteChange.bind(this), id: 'accountNumberList', label: 'Bank Account Number', list: this.accountNumberList });
		};

		_this.renderIFSCAutosuggest = function () {
			return _react2.default.createElement(_AutoSuggest2.default, { value: this.selectedValues.selectedIFSC, maxLength: '11', pattern: /^[a-z0-9]+$/i, name: 'selectedIFSC', onValueChange: this.handleAutocompleteChange.bind(this), id: 'ifscList', label: 'IFSC Code', list: this.ifscList });
		};

		_this.accountNumberList = [];
		_this.ifscList = [];
		_this.firmCache = new _FirmCache2.default();
		_this.state = { canGenerateQRCode: false };
		_this.firmObj = _this.firmCache.getFirmById(_this.props.firmId);
		if (!_this.firmObj) _this.firmObj = _this.firmCache.getDefaultFirm();
		_this.populateAccountNumberAndIFSCCode();
		_this.selectedValues = {
			selectedAccountNumber: _this.firmObj.getUpiAccountNumber(),
			selectedIFSC: _this.firmObj.getUpiIFSC()
		};
		return _this;
	}

	(0, _createClass3.default)(CollectPayment, [{
		key: 'componentDidMount',
		value: function componentDidMount() {}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {}
	}, {
		key: 'populateAccountNumberAndIFSCCode',
		value: function populateAccountNumberAndIFSCCode() {
			var regex = /[^\w\\s+]/gi;
			var firmList = this.firmCache.getFirmList();
			var firmNameList = (0, _keys2.default)(firmList);
			var accNum = new _set2.default();var ifscNum = new _set2.default();
			for (var i = 0; i < firmNameList.length; i++) {
				var firmItem = firmList[firmNameList[i]];
				if ((typeof firmItem === 'undefined' ? 'undefined' : (0, _typeof3.default)(firmItem)) === 'object') {
					firmItem.upiAccountNumber && accNum.add(firmItem.upiAccountNumber);
					firmItem.upiIFSC && ifscNum.add(firmItem.upiIFSC);
					firmItem.accountNumber.split(regex).filter(function (ac) {
						return ac !== '' ? accNum.add(ac) : '';
					});
					firmItem.bankIFSC.split(regex).filter(function (ifsc) {
						return ifsc !== '' ? ifscNum.add(ifsc) : '';
					});
				}
			}

			this.accountNumberList = accNum.toJSON();
			this.ifscList = ifscNum.toJSON();
		}
	}, {
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement('link', { rel: 'stylesheet', href: './../styles/collectPayment.css' }),
				_react2.default.createElement(
					'div',
					{ className: 'collect-payment fullContainerHeight' },
					_react2.default.createElement(
						'div',
						{ className: 'row' },
						_react2.default.createElement(
							'div',
							{ className: 'col-sm-8' },
							this.renderAccountNumberAutosuggest()
						),
						_react2.default.createElement(
							'div',
							{ className: 'col-sm-8' },
							this.renderIFSCAutosuggest()
						),
						_react2.default.createElement(
							'div',
							{ className: 'col-sm-8 mb-10' },
							_react2.default.createElement(
								'div',
								{ className: 'banking-safe-info' },
								_react2.default.createElement('img', { src: '../inlineSVG/shield.svg' }),
								_react2.default.createElement(
									'span',
									{ className: 'font-family-regular font-12' },
									'Your Bank details are completely safe and stays on your device'
								)
							)
						),
						_react2.default.createElement(
							'div',
							{ className: 'col-sm-8' },
							_react2.default.createElement(
								_PrimaryButton2.default,
								{ disabled: !this.state.canGenerateQRCode, onClick: this.updateUpiAccountNumberAndIFSC.bind(this), style: { width: '100%' } },
								'Get QR Code'
							)
						),
						_react2.default.createElement(
							'div',
							{ className: 'col-sm-9' },
							_react2.default.createElement(
								'div',
								{ className: 'qr-code-info-steps row font-family-regular font-14' },
								_react2.default.createElement(
									'div',
									{ className: 'step mt-30 col-sm-12 d-flex align-items-center' },
									_react2.default.createElement('img', { className: 'floatLeft', src: '../inlineSVG/baseline-check_circle-24px.svg' }),
									_react2.default.createElement(
										'span',
										null,
										'Print QR codes and Payment Links on bills.'
									)
								),
								_react2.default.createElement(
									'div',
									{ className: 'step mt-30 col-sm-12 d-flex align-items-center' },
									_react2.default.createElement('img', { className: 'floatLeft', src: '../inlineSVG/baseline-check_circle-24px.svg' }),
									_react2.default.createElement(
										'span',
										null,
										'Customer pays directly into your bank account.'
									)
								),
								_react2.default.createElement(
									'div',
									{ className: 'step mt-30 col-sm-12 d-flex align-items-center' },
									_react2.default.createElement('img', { className: 'floatLeft', src: '../inlineSVG/baseline-check_circle-24px.svg' }),
									_react2.default.createElement(
										'span',
										null,
										'No Charges, its Free!!'
									)
								)
							)
						),
						_react2.default.createElement(
							'div',
							{ className: 'col-sm-3 mt-30' },
							_react2.default.createElement(
								'div',
								{ className: 'row' },
								_react2.default.createElement('div', { className: 'col-sm-3' }),
								_react2.default.createElement(
									'div',
									{ className: 'col-sm-9 floatRight nopad' },
									_react2.default.createElement('img', { src: '../inlineSVG/upi-icon.svg' }),
									_react2.default.createElement(
										'div',
										null,
										_react2.default.createElement('img', { src: '../inlineSVG/gpay.jpg', style: { float: 'left' } }),
										_react2.default.createElement('img', { src: '../inlineSVG/phonepe-seeklogo.com.svg', style: { marginLeft: '60px' } })
									)
								)
							)
						)
					)
				)
			);
		}
	}]);
	return CollectPayment;
}(_react2.default.Component);

exports.default = CollectPayment;

CollectPayment.propTypes = {
	firmId: _propTypes2.default.string,
	callback: _propTypes2.default.func,
	closeFunc: _propTypes2.default.func
};