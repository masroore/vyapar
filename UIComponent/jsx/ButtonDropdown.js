Object.defineProperty(exports, "__esModule", {
	value: true
});

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/core/styles');

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _ButtonGroup = require('@material-ui/core/ButtonGroup');

var _ButtonGroup2 = _interopRequireDefault(_ButtonGroup);

var _ListItem = require('@material-ui/core/ListItem');

var _ListItem2 = _interopRequireDefault(_ListItem);

var _ListItemIcon = require('@material-ui/core/ListItemIcon');

var _ListItemIcon2 = _interopRequireDefault(_ListItemIcon);

var _ListItemText = require('@material-ui/core/ListItemText');

var _ListItemText2 = _interopRequireDefault(_ListItemText);

var _Menu = require('@material-ui/core/Menu');

var _Menu2 = _interopRequireDefault(_Menu);

var _ExpandMore = require('@material-ui/icons/ExpandMore');

var _ExpandMore2 = _interopRequireDefault(_ExpandMore);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles() {
	return {
		root: {
			position: 'relative',
			margin: '0 10px',
			textTransform: 'capitalize'
		},
		defaultButton: {
			borderRadius: '.5rem',
			overflow: 'hidden',
			minHeight: '2rem',
			padding: 0,
			flex: 1
		},
		colorPrimary: {
			backgroundColor: '#F3A33A',
			color: 'white',
			'&:hover': {
				backgroundColor: 'rgba(245, 179, 88, 1)'
			}
		},
		colorSecondary: {
			backgroundColor: '#1789FC',
			color: 'white',
			'&:hover': {
				backgroundColor: 'rgba(13, 137, 252, 0.69)'
			}
		},
		primaryDownButton: {
			flex: 0,
			borderRadius: '.5rem',
			backgroundColor: '#D48D2F',
			minWidth: '2rem',
			color: 'white',
			'&:hover': {
				backgroundColor: 'rgba(245, 179, 88, 1)'
			}
		},
		secondaryDownButton: {
			flex: 0,
			borderRadius: '.5rem',
			backgroundColor: '#107ce8',
			minWidth: '2rem',
			color: 'white',
			'&:hover': {
				backgroundColor: '#146cc5'
			}
		},
		buttonGroup: {
			display: 'flex',
			height: '100%'
		},
		buttonLabel: {
			display: 'inherit',
			textTransform: 'capitalize',
			alignItems: 'center'
		},
		marginRight8: {
			marginRight: 8
		},
		buttonLabelIcon: {
			display: 'inherit',
			paddingRight: '.25rem',
			marginLeft: 10,
			marginRight: 4,
			width: 16,
			fontSize: '1.25rem'
		},
		collapsedBtn: {
			height: '2rem',
			minWidth: '2rem',
			width: '2.25rem',
			fontSize: '1.25rem',
			marginRight: '.5rem'
		},
		labelPadding: {
			padding: '0 .5rem'
		}
	};
};

var ButtonDropdown = function (_React$Component) {
	(0, _inherits3.default)(ButtonDropdown, _React$Component);

	function ButtonDropdown(props) {
		(0, _classCallCheck3.default)(this, ButtonDropdown);

		var _this = (0, _possibleConstructorReturn3.default)(this, (ButtonDropdown.__proto__ || (0, _getPrototypeOf2.default)(ButtonDropdown)).call(this, props));

		_this.handleToggle = function (event) {
			event.stopPropagation();
			var _this$state = _this.state,
			    isListOpen = _this$state.isListOpen,
			    anchorEl = _this$state.anchorEl;

			anchorEl = _this.buttonGroupRef.current; // event.currentTarget;
			isListOpen = !isListOpen;
			_this.setState({
				isListOpen: isListOpen,
				anchorEl: anchorEl
			});
		};

		_this.handleClose = function (event) {
			_this.setState({
				isListOpen: false
			});
		};

		_this.buttonGroupRef = _react2.default.createRef();
		_this.state = {
			isListOpen: false,
			anchorEl: null
		};
		return _this;
	}

	(0, _createClass3.default)(ButtonDropdown, [{
		key: 'handleClick',
		value: function handleClick(onClickFunc) {
			var _this2 = this;

			return function (event) {
				return _this2.setState({
					isListOpen: false
				}, function () {
					onClickFunc && onClickFunc(event);
				});
			};
		}
	}, {
		key: 'getButtonDropDown',
		value: function getButtonDropDown() {
			var _this3 = this;

			var _props = this.props,
			    classes = _props.classes,
			    addPadding = _props.addPadding;

			var menuOptions = [].concat((0, _toConsumableArray3.default)(this.props.menuOptions));
			var _state = this.state,
			    isListOpen = _state.isListOpen,
			    anchorEl = _state.anchorEl;

			var defaultOption = menuOptions.shift();
			var colorClass = this.props.color == 'secondary' ? classes.colorSecondary : classes.colorPrimary;
			var defaultButtonRoot = (0, _classnames2.default)(classes.defaultButton, colorClass);
			var iconButtonRoot = this.props.color == 'secondary' ? classes.secondaryDownButton : classes.primaryDownButton;
			var buttonGroupRoot = (0, _classnames2.default)(_classnames2.default.buttonGroup);

			return _react2.default.createElement(
				'div',
				{
					className: (0, _classnames2.default)(classes.root) + ' ' + (this.props.classNames ? this.props.classNames : '')
				},
				_react2.default.createElement(
					_ButtonGroup2.default,
					{
						ref: this.buttonGroupRef,
						classes: { root: buttonGroupRoot }
					},
					_react2.default.createElement(
						_Button2.default,
						{
							onClick: defaultOption.onClick,
							'aria-haspopup': 'true',
							classes: {
								root: defaultButtonRoot,
								label: this.props.color == 'secondary' ? classes.labelPadding : ''
							},
							variant: 'contained'
						},
						_react2.default.createElement(
							'span',
							{
								onClick: this.handleClick(defaultOption.onClick),
								className: (0, _classnames2.default)(classes.buttonLabel)
							},
							!!defaultOption.buttonIcon && _react2.default.createElement(
								'span',
								{ className: (0, _classnames2.default)(classes.buttonLabelIcon) },
								defaultOption.buttonIcon
							),
							_react2.default.createElement(
								'span',
								{
									className: (0, _classnames2.default)(classes.buttonLabel, addPadding ? classes.marginRight8 : '')
								},
								defaultOption.buttonText
							)
						)
					),
					menuOptions.length > 0 && _react2.default.createElement(
						_Button2.default,
						{
							size: 'small',
							'aria-owns': open ? 'menu-list-grow' : undefined,
							'aria-haspopup': 'true',
							variant: 'contained',
							onClick: this.handleToggle,
							classes: {
								root: iconButtonRoot
							}
						},
						_react2.default.createElement(_ExpandMore2.default, null)
					)
				),
				_react2.default.createElement(
					_Menu2.default,
					{
						id: 'action-menu',
						anchorEl: anchorEl,
						open: isListOpen,
						onClose: this.handleClose,
						anchorOrigin: {
							vertical: 'bottom',
							horizontal: 'left'
						},
						getContentAnchorEl: null,
						classes: {
							paper: classes.menu
						}
					},
					menuOptions.map(function (option, index) {
						return _react2.default.createElement(
							_ListItem2.default,
							{
								key: index,
								dense: true,
								button: true,
								style: { minWidth: anchorEl && anchorEl.offsetWidth },
								onClick: _this3.handleClick(option.onClick)
							},
							!!option.buttonIcon && _react2.default.createElement(
								_ListItemIcon2.default,
								{ style: { minWidth: '24px' } },
								option.buttonIcon
							),
							_react2.default.createElement(_ListItemText2.default, { primary: option.buttonText })
						);
					})
				)
			);
		}
	}, {
		key: 'render',
		value: function render() {
			var btnDropDown = this.getButtonDropDown();
			// const collapsedBtn = this.getCollapsedButton();
			var collapsed = this.props.collapsed;

			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				collapsed ? _react2.default.createElement('span', null) : btnDropDown
			);
		}
	}]);
	return ButtonDropdown;
}(_react2.default.Component);

ButtonDropdown.propTypes = {
	classes: _propTypes2.default.object,
	classNames: _propTypes2.default.string,
	menuOptions: _propTypes2.default.arrayOf(_propTypes2.default.shape({
		buttonText: _propTypes2.default.string,
		buttonIcon: _propTypes2.default.node,
		onClick: _propTypes2.default.func
	})),
	collapsed: _propTypes2.default.bool,
	color: _propTypes2.default.oneOf(['primary', 'secondary'])
};

exports.default = (0, _styles.withStyles)(styles)(ButtonDropdown);