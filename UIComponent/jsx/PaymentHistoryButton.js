Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LinkPaymentButton = function (_React$Component) {
	(0, _inherits3.default)(LinkPaymentButton, _React$Component);

	function LinkPaymentButton() {
		var _ref;

		var _temp, _this, _ret;

		(0, _classCallCheck3.default)(this, LinkPaymentButton);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = LinkPaymentButton.__proto__ || (0, _getPrototypeOf2.default)(LinkPaymentButton)).call.apply(_ref, [this].concat(args))), _this), _this.onClick = function () {
			var _this$props = _this.props,
			    txnType = _this$props.txnType,
			    totalReceivedAmount = _this$props.totalReceivedAmount,
			    receivedLater = _this$props.receivedLater,
			    selectedTransactionMap = _this$props.selectedTransactionMap,
			    _this$props$discountA = _this$props.discountAmountInHistory,
			    discountAmountInHistory = _this$props$discountA === undefined ? 0 : _this$props$discountA;

			var PaymentHistoryModal = require('./PaymentHistoryModal').default;
			var MountComponent = require('./MountComponent').default;

			MountComponent(PaymentHistoryModal, document.querySelector('#link-history-modal-container'), {
				txnType: txnType,
				totalReceivedAmount: totalReceivedAmount,
				receivedLater: receivedLater,
				selectedTransactionMap: selectedTransactionMap,
				discountAmountInHistory: discountAmountInHistory,
				isOpen: true,
				onClose: _this.onClose
			});
		}, _this.onClose = function () {
			unmountReactComponent(document.querySelector('#link-history-modal-container'));
		}, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
	}

	(0, _createClass3.default)(LinkPaymentButton, [{
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement('div', { id: 'link-history-modal-container' }),
				_react2.default.createElement(
					_Button2.default,
					{
						variant: 'outlined',
						color: 'primary',
						onClick: this.onClick
					},
					'Payment History'
				)
			);
		}
	}]);
	return LinkPaymentButton;
}(_react2.default.Component);

exports.default = LinkPaymentButton;