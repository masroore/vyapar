Object.defineProperty(exports, "__esModule", {
	value: true
});

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _SaleAgeingReportPieChart = require('./SaleAgeingReportPieChart');

var _SaleAgeingReportPieChart2 = _interopRequireDefault(_SaleAgeingReportPieChart);

var _JqueryDatePicker = require('./UIControls/JqueryDatePicker');

var _JqueryDatePicker2 = _interopRequireDefault(_JqueryDatePicker);

var _IPCActions = require('../../Constants/IPCActions');

var _MyDate = require('../../Utilities/MyDate');

var _MyDate2 = _interopRequireDefault(_MyDate);

var _DBWindow = require('../../Utilities/DBWindow');

var _DBWindow2 = _interopRequireDefault(_DBWindow);

var _SaleAgeingPartyContainer = require('./SaleAgeingPartyContainer');

var _SaleAgeingPartyContainer2 = _interopRequireDefault(_SaleAgeingPartyContainer);

var _SaleAgeingInvoiceContainer = require('./SaleAgeingInvoiceContainer');

var _SaleAgeingInvoiceContainer2 = _interopRequireDefault(_SaleAgeingInvoiceContainer);

var _NameCache = require('../../Cache/NameCache');

var _NameCache2 = _interopRequireDefault(_NameCache);

var _FirmCache = require('../../Cache/FirmCache');

var _FirmCache2 = _interopRequireDefault(_FirmCache);

var _SettingCache = require('../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

var _MessageDraftLogic = require('../../BizLogic/MessageDraftLogic');

var _MessageDraftLogic2 = _interopRequireDefault(_MessageDraftLogic);

var _DataLoader = require('../../DBManager/DataLoader');

var _DataLoader2 = _interopRequireDefault(_DataLoader);

var _SaleAgeingDropdownMenuForRemind = require('./SaleAgeingDropdownMenuForRemind');

var _SaleAgeingDropdownMenuForRemind2 = _interopRequireDefault(_SaleAgeingDropdownMenuForRemind);

var _SaleAgeingDropdownMenuForSendSelected = require('./SaleAgeingDropdownMenuForSendSelected');

var _SaleAgeingDropdownMenuForSendSelected2 = _interopRequireDefault(_SaleAgeingDropdownMenuForSendSelected);

var _SaleAgingReportPrintOptions = require('./SaleAgingReportPrintOptions');

var _SaleAgingReportPrintOptions2 = _interopRequireDefault(_SaleAgingReportPrintOptions);

var _GoogleMail = require('../../BizLogic/GoogleMail');

var _GoogleMail2 = _interopRequireDefault(_GoogleMail);

var _TransactionHTMLGenerator = require('../../ReportHTMLGenerator/TransactionHTMLGenerator');

var _TransactionHTMLGenerator2 = _interopRequireDefault(_TransactionHTMLGenerator);

var _PDFHandler = require('../../Utilities/PDFHandler');

var _PDFHandler2 = _interopRequireDefault(_PDFHandler);

var _xlsx = require('xlsx');

var _xlsx2 = _interopRequireDefault(_xlsx);

var _FileUtil = require('../../Utilities/FileUtil');

var _FileUtil2 = _interopRequireDefault(_FileUtil);

var _PDFReportConstant = require('../../Constants/PDFReportConstant');

var _PDFReportConstant2 = _interopRequireDefault(_PDFReportConstant);

var _ExcelHelper = require('../../Utilities/ExcelHelper');

var _ExcelHelper2 = _interopRequireDefault(_ExcelHelper);

var _TransactionHelper = require('../../Utilities/TransactionHelper');

var TransactionHelper = _interopRequireWildcard(_TransactionHelper);

var _SaleAgingReportHTMLGenerator = require('../../ReportHTMLGenerator/SaleAgingReportHTMLGenerator');

var _SaleAgingReportHTMLGenerator2 = _interopRequireDefault(_SaleAgingReportHTMLGenerator);

var _TxnTypeConstant = require('../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _ReportFilterConstant = require('../../Constants/ReportFilterConstant');

var _ReportFilterConstant2 = _interopRequireDefault(_ReportFilterConstant);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _trashableReact = require('trashable-react');

var _trashableReact2 = _interopRequireDefault(_trashableReact);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// //
// DBWindow.show();
// DBWindow.showDevTools();

var SaleAgeingReportContainer = function (_React$Component) {
	(0, _inherits3.default)(SaleAgeingReportContainer, _React$Component);

	function SaleAgeingReportContainer(props) {
		(0, _classCallCheck3.default)(this, SaleAgeingReportContainer);

		var _this = (0, _possibleConstructorReturn3.default)(this, (SaleAgeingReportContainer.__proto__ || (0, _getPrototypeOf2.default)(SaleAgeingReportContainer)).call(this, props));

		_this.pdfHandler = new _PDFHandler2.default();
		_this.nameCache = new _NameCache2.default();
		_this.firmCache = new _FirmCache2.default();
		_this.settingCache = new _SettingCache2.default();

		_this.state = {
			date: _MyDate2.default.getDate('d/m/y'),
			dateType: _ReportFilterConstant2.default.DATE_TODAY,
			records: [],
			dataInvoice: [],
			selectedPartyId: null,
			selectedPartyName: '',
			invoiceview: false,
			showChart: true,
			pieChartData: [],
			totalOutstandingBalance: 0,
			totalUnusedPayment: 0,
			totalPartyBalance: 0,
			firmList: _this.firmCache.getFirmList(),
			firmId: '',
			CurrentComponent: _SaleAgeingPartyContainer2.default,
			includeGraphInPrint: true,
			includeInvoiceDetailsInPrint: false
		};

		_this.options = {
			openDetailAgeingView: _this.openDetailAgeingView.bind(_this),
			sendReminderOnWhatsApp: _this.sendReminderOnWhatsApp.bind(_this),
			sendTxnOnSMS: _this.sendTxnOnSMS.bind(_this),
			sendTxnOnMail: _this.sendTxnOnMail.bind(_this),
			sendTxnOnWhatsApp: _this.sendTxnOnWhatsApp.bind(_this),
			sendReminderOnSMS: _this.sendReminderOnSMS.bind(_this),
			sendReminderOnMail: _this.sendReminderOnMail.bind(_this),
			viewTransaction: _this.viewTransaction.bind(_this),
			onChangePrintOptions: _this.onChangePrintOptions.bind(_this),
			printTransactionsFromInvoiceView: _this.printTransactionsFromInvoiceView.bind(_this),
			printTransactionsFromPartyView: _this.printTransactionsFromPartyView.bind(_this)
		};

		_this.api = null;
		_this._loaded = [];
		_this._perPage = 15;
		_this.getData = _this.getData.bind(_this);
		_this.openDetailAgeingView = _this.openDetailAgeingView.bind(_this);
		_this.openPartyView = _this.openPartyView.bind(_this);
		_this.handleChart = _this.handleChart.bind(_this);
		_this.sendReminderOnWhatsApp = _this.sendReminderOnWhatsApp.bind(_this);
		_this.sendReminderOnMail = _this.sendReminderOnMail.bind(_this);
		_this.sendReminderOnSMS = _this.sendReminderOnSMS.bind(_this);
		_this.sendWhatsApp = _this.sendWhatsApp.bind(_this);
		_this.sendTxnOnWhatsApp = _this.sendTxnOnWhatsApp.bind(_this);
		_this.sendTxnOnSMS = _this.sendTxnOnSMS.bind(_this);
		_this.sendTxnOnMail = _this.sendTxnOnMail.bind(_this);
		_this.errorCallBack = _this.errorCallBack.bind(_this);
		_this.successCallBack = _this.successCallBack.bind(_this);
		_this.getApi = _this.getApi.bind(_this);
		_this.openExcelFromInvoiceView = _this.openExcelFromInvoiceView.bind(_this);
		_this.openExcelFromPartyView = _this.openExcelFromPartyView.bind(_this);
		_this.downloadExcelFile = _this.downloadExcelFile.bind(_this);
		_this.prepareObjectForExcelPartyWiseAging = _this.prepareObjectForExcelPartyWiseAging.bind(_this);
		_this.prepareObjectForExcelForOutstandingSale = _this.prepareObjectForExcelForOutstandingSale.bind(_this);
		_this.openExcelFromInvoiceView = _this.openExcelFromInvoiceView.bind(_this);
		_this.printTransactions = _this.printTransactions.bind(_this);
		_this.printTransactionsFromInvoiceView = _this.printTransactionsFromInvoiceView.bind(_this);
		_this.printTransactionsFromPartyView = _this.printTransactionsFromPartyView.bind(_this);
		_this.viewTransaction = _this.viewTransaction.bind(_this);
		_this.generateDataInvoiceWise = _this.generateDataInvoiceWise.bind(_this);
		_this.onChangePrintOptions = _this.onChangePrintOptions.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(SaleAgeingReportContainer, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.loadData();
			TransactionHelper.bindEventsForPreviewDialog();
			window.onResume = this.onResume.bind(this);
			window.savePDF = this.savePDF.bind(this);
		}
	}, {
		key: 'savePDF',
		value: function savePDF() {
			this.pdfHandler.savePDF({ type: _PDFReportConstant2.default.SALE_AGING_REPORT, fromDate: this.state.date, toDate: this.state.date });
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			TransactionHelper.unBindEventsForPreviewDialog();
			delete window.onResume;
			delete window.savePDF;
		}
	}, {
		key: 'onResume',
		value: function onResume() {
			$('#modelContainer').css({ 'display': 'none' });
			$('.viewItems').css('display', 'block');
			this.loadData();
			TransactionHelper.bindEventsForPreviewDialog();
		}
	}, {
		key: 'getData',
		value: function getData(event, response) {
			var _this2 = this;

			if (response.meta.next && response.data.length == this._perPage) {
				var next = response.meta.next;
				this.loadTransactions(next.start, next.noOfRecords);
			}
			var data = response.data;
			var dataObj = void 0;
			if (response.meta.next) {
				dataObj = data;
			} else {
				dataObj = [].concat((0, _toConsumableArray3.default)(this.state.records), (0, _toConsumableArray3.default)(data));
			}
			var invoiceDataArray = [];
			var piechartData = dataObj;
			var totalpartyBal = 0;
			var totalOutstanding = 0;
			var isPartyContainTxns = false;
			var recordObj = dataObj;
			recordObj.map(function (data) {
				if (_this2.state.selectedPartyId) {
					if (data.nameId == _this2.state.selectedPartyId) {
						invoiceDataArray = data.txnArray;
						piechartData = [data];
						totalpartyBal += data.partyBal;
						totalOutstanding += data.total;
						isPartyContainTxns = true;
					}
				} else {
					totalpartyBal += data.partyBal;
					totalOutstanding += data.total;
				}
			});

			if (!isPartyContainTxns && this.state.selectedPartyId) {
				// spacific case when in txn screen but party is not in sale aging data
				piechartData = [];
				var nameObj = this.nameCache.findNameObjectByNameId(this.state.selectedPartyId);
				totalpartyBal = nameObj ? nameObj.getAmount() : 0;
			}

			this.setState({
				records: dataObj,
				dataInvoice: invoiceDataArray,
				pieChartData: piechartData,
				totalOutstandingBalance: totalOutstanding,
				totalPartyBalance: totalpartyBal,
				totalUnusedPayment: totalOutstanding - totalpartyBal
			});
		}
	}, {
		key: 'getApi',
		value: function getApi(params) {
			this.api = params.api;
		}
	}, {
		key: 'loadTransactions',
		value: function loadTransactions(start, noOfRecords, next) {
			var _this3 = this;

			var dateObj = void 0;
			try {
				dateObj = _MyDate2.default.getDateObj(this.state.date, 'dd/mm/yyyy', '/');
				if (!(dateObj && dateObj instanceof Date && dateObj != 'Invalid Date')) {
					throw new Error('invalid date');
				}
			} catch (e) {
				ToastHelper.error('Please enter valid date');
				return false;
			}
			var date = "'" + _MyDate2.default.getDate('y-m-d', dateObj) + " 23:59:59'";
			var firmId = Number(this.state.firmId);

			this.props.registerPromise(_DBWindow2.default.send(_IPCActions.GET_SALE_AGING_REPORT_DATA, {
				date: date,
				firmId: firmId,
				start: start,
				noOfRecords: noOfRecords,
				next: next
			})).then(function (data) {
				return _this3.getData(null, data);
			});
		}
	}, {
		key: 'loadData',
		value: function loadData() {
			this.loadTransactions(0, this._perPage, {
				start: this._perPage,
				noOfRecords: -1
			});
		}
	}, {
		key: 'onChangePrintOptions',
		value: function onChangePrintOptions(field, e) {
			var value = e.target.checked;
			this.setState((0, _defineProperty3.default)({}, field, value));
		}
	}, {
		key: 'changeFilter',
		value: function changeFilter(field) {
			var _this4 = this;

			return function (e) {
				var value = void 0;
				if (e && e.target) {
					value = e.target.value;
				} else {
					value = e;
				}
				var date = '';
				if (field === 'date') {
					date = _MyDate2.default.getDateObj(value, 'dd/mm/yyyy', '/');
					if (!(date && date instanceof Date && date != 'Invalid Date')) {
						_this4.setState((0, _defineProperty3.default)({}, field, value));
						return false;
					}
				}
				if (field == 'dateType' && value == _ReportFilterConstant2.default.DATE_TODAY) {
					_this4.setState({
						dateType: _ReportFilterConstant2.default.DATE_TODAY,
						date: _MyDate2.default.getDate('d/m/y')
					}, function () {
						return _this4.loadData();
					});
				} else if (field == 'date' && !_MyDate2.default.isSameDate(date, new Date())) {
					_this4.setState({
						dateType: _ReportFilterConstant2.default.DATE_CUSTOM,
						date: value
					}, function () {
						return _this4.loadData();
					});
				} else {
					_this4.setState((0, _defineProperty3.default)({}, field, value), function () {
						return _this4.loadData();
					});
				}
			};
		}
	}, {
		key: 'openDetailAgeingView',
		value: function openDetailAgeingView(row) {
			if (row.data.nameId == 'pinnedRowForSaleAgingReport') {
				return;
			}
			var piechartData = this.state.records;
			var totalPartyBal = 0;
			var totalOutstandingBal = 0;
			this.state.records.map(function (data) {
				if (data.nameId == row.data.nameId) {
					piechartData = [data];
					totalPartyBal += data.partyBal;
					totalOutstandingBal += data.total;
				}
			});
			var transactionArray = row.data.txnArray;
			this.setState({
				showBackButton: true,
				selectedPartyId: row.data.nameId,
				selectedPartyName: row.data.name,
				invoiceview: true,
				dataInvoice: transactionArray,
				pieChartData: piechartData,
				CurrentComponent: _SaleAgeingInvoiceContainer2.default,
				totalPartyBalance: totalPartyBal,
				totalOutstandingBalance: totalOutstandingBal,
				totalUnusedPayment: totalOutstandingBal - totalPartyBal
			});
		}
	}, {
		key: 'openPartyView',
		value: function openPartyView() {
			var totalPartyBal = 0;
			var totalOutstandingBal = 0;
			this.state.records.map(function (data) {
				totalPartyBal += data.partyBal;
				totalOutstandingBal += data.total;
			});

			this.setState({
				showBackButton: false,
				selectedPartyId: null,
				invoiceview: false,
				CurrentComponent: _SaleAgeingPartyContainer2.default,
				pieChartData: this.state.records,
				totalPartyBalance: totalPartyBal,
				totalOutstandingBalance: totalOutstandingBal,
				totalUnusedPayment: totalOutstandingBal - totalPartyBal
			});
		}
	}, {
		key: 'handleChart',
		value: function handleChart() {
			this.setState({
				showChart: !this.state.showChart
			});
		}
	}, {
		key: 'printTransactions',
		value: function printTransactions(partyName, reportHeader, data) {
			var saleAgingReportHTMLGenerator = new _SaleAgingReportHTMLGenerator2.default();
			var html = saleAgingReportHTMLGenerator.getHTMLText(partyName, this.state.firmId, reportHeader, data, this.state.totalOutstandingBalance, this.state.totalUnusedPayment, this.state.totalPartyBalance, this.state.includeGraphInPrint, this.state.includeInvoiceDetailsInPrint);
			TransactionHelper.previewByHTML(html);
		}
	}, {
		key: 'printTransactionsFromPartyView',
		value: function printTransactionsFromPartyView() {
			this.printTransactions(null, 'Sale Aging Report', this.state.records);
		}
	}, {
		key: 'printTransactionsFromInvoiceView',
		value: function printTransactionsFromInvoiceView() {
			var dataObject = this.state.records;
			var printData = [];
			for (var i = 0; i < dataObject.length; i++) {
				var data = dataObject[i];
				if (this.state.selectedPartyId == data.nameId) {
					printData.push(data);
					break;
				}
			}
			this.printTransactions(this.state.selectedPartyName, 'Sale Aging Report by party', printData);
		}
	}, {
		key: 'sendReminderOnWhatsApp',
		value: function sendReminderOnWhatsApp(row) {
			var callback = row.handleClose;
			var nameObj = this.nameCache.findNameObjectByNameId(this.state.selectedPartyId);
			var contactNo = nameObj.getPhoneNumber();
			var firm = this.firmCache.getDefaultFirm();
			var firmName = firm.getFirmName();
			var balance = nameObj.getAmount();
			var msg = {
				to: contactNo,
				message: 'Hi,' + '\n' + "It's a friendly reminder to you for paying " + balance + ' to me.',
				footer: 'Thank you,' + '\n' + firmName
			};
			this.sendWhatsApp(msg);
			callback();
		}
	}, {
		key: 'sendWhatsApp',
		value: function sendWhatsApp(msg, callback) {
			var send = require('../../Utilities/whatsapp').default;
			msg.to = '91' + msg.to;
			send(msg, callback);
		}
	}, {
		key: 'sendTxnOnSMS',
		value: function sendTxnOnSMS(row) {
			var callback = row.handleClose;
			var selectedRows = this.api.getSelectedRows();
			if (selectedRows.length == 0) {
				ToastHelper.info('Please select transaction');
				callback();
				return;
			}
			if (!(this.settingCache.isTxnMessageEnabledForTxn(_TxnTypeConstant2.default.TXN_TYPE_SALE) && (this.settingCache.getTransactionMessageEnabled() || this.settingCache.isOwnerTxnMsgEnabled()))) {
				ToastHelper.info('Please enable message setting');
				return;
			}
			var nameObj = this.nameCache.findNameObjectByNameId(this.state.selectedPartyId);
			var contactNo = nameObj.getPhoneNumber();
			if (!contactNo) {
				ToastHelper.info('Please add contact number of party');
				return;
			}
			var messageDraftLogic = new _MessageDraftLogic2.default();
			var dataLoader = new _DataLoader2.default();
			selectedRows.map(function (data) {
				var txnId = data.txnId;
				var txnObject = dataLoader.LoadTransactionFromId(txnId);
				messageDraftLogic.sendTransactionSMSToParty(txnObject);
			});
			callback();
		}
	}, {
		key: 'successCallBack',
		value: function successCallBack() {
			// alert('success');
		}
	}, {
		key: 'errorCallBack',
		value: function errorCallBack() {
			// alert('failure');
		}
	}, {
		key: 'sendTxnOnMail',
		value: function sendTxnOnMail(row) {
			var callback = row.handleClose;
			var selectedRows = this.api.getSelectedRows();
			if (selectedRows.length == 0) {
				ToastHelper.info('Please select transaction');
				callback();
				return;
			}
			var nameObj = this.nameCache.findNameObjectByNameId(this.state.selectedPartyId);
			var contactMail = nameObj.getEmail();
			if (!contactMail) {
				ToastHelper.info('Please add email id of party');
				return;
			}
			var contactName = nameObj.getFullName();
			var firm = this.firmCache.getDefaultFirm();
			var firmName = firm.getFirmName();
			var subjectToSend = 'Your Invoice(s) Details';
			var header = 'Dear ' + contactName + ',' + '\n\n\n';
			var defaultText = 'Thanks for doing business with us. Below are your invoice(s) details. Also attached are the PDF of invoice(s) for your convenience.' + '\n\n\n';
			var body = '';

			var transactionHTMLGenerator = new _TransactionHTMLGenerator2.default();

			var txnIdlist = [];
			selectedRows.map(function (data) {
				var invoiceAmount = data.amount;
				var invoiceNo = data.invoiceNo;
				var date = data.date;
				var dueDate = data.dueDate;
				var txnId = data.txnId;
				txnIdlist.push(txnId);
				body += 'Invoice Amount: ' + invoiceAmount + '\n' + 'Invoice Number: ' + invoiceNo + '\n' + 'Invoice Date: ' + date + '\n' + 'Due Date: ' + dueDate + '\n\n\n';
			});

			var footer = 'Thank You,' + '\n' + firmName;
			var messageToSend = header + defaultText + body + footer;
			var receiverId = contactMail;
			var fileName = 'Sale_' + _MyDate2.default.getDate('d_m_y_H_M_S');

			var html = transactionHTMLGenerator.getTransactionListHTML(txnIdlist);
			this.pdfHandler.getPDFPath(html, 'Sale', function (pdfPath) {
				var _this5 = this;

				var isOnline = require('is-online');
				isOnline().then(function (online) {
					if (online) {
						var googleMail = new _GoogleMail2.default();
						googleMail.sendMail({
							fileNames: pdfPath,
							receiverId: receiverId,
							alertUser: true,
							subject: subjectToSend,
							message: messageToSend,
							attachFileNames: fileName,
							successCallBack: _this5.successCallBack,
							errorCallBack: _this5.errorCallBack,
							eventLog: 'saleAgeingTxnReport'
						});
					} else {
						ToastHelper.error('No internet connection');
					}
				});
			});
			callback();
		}
	}, {
		key: 'sendTxnOnWhatsApp',
		value: function sendTxnOnWhatsApp(row) {
			var callback = row.handleClose;
			var selectedRows = this.api.getSelectedRows();
			if (selectedRows.length == 0) {
				ToastHelper.info('Please select transaction');
				callback();
				return;
			}
			var firm = this.firmCache.getDefaultFirm();
			var firmName = firm.getFirmName();
			var nameObj = this.nameCache.findNameObjectByNameId(this.state.selectedPartyId);
			var contactNo = nameObj.getPhoneNumber();
			var contactName = nameObj.getFullName();
			var header = 'Dear ' + contactName + ',' + '\n\n\n';
			var defaultText = 'Thanks for doing business with us. Below are your invoice(s) details.' + '\n\n\n';
			var body = '';

			selectedRows.map(function (data) {
				var invoiceAmount = data.amount;
				var invoiceNo = data.invoiceNo;
				var date = data.date;
				var dueDate = data.dueDate;
				body += 'Invoice Amount: ' + invoiceAmount + '\n' + 'Invoice Number: ' + invoiceNo + '\n' + 'Invoice Date: ' + date + '\n' + 'Due Date: ' + dueDate + '\n\n\n';
			});

			var footer = 'Thank You,' + '\n' + firmName;
			var messageToSend = header + defaultText + body;

			var msg = {
				to: contactNo,
				message: messageToSend,
				footer: footer
			};
			this.sendWhatsApp(msg, callback);
		}
	}, {
		key: 'sendReminderOnSMS',
		value: function sendReminderOnSMS(row) {
			var callback = row.handleClose;
			if (!(this.settingCache.isTxnMessageEnabledForTxn(_TxnTypeConstant2.default.TXN_TYPE_SALE) && (this.settingCache.getTransactionMessageEnabled() || this.settingCache.isOwnerTxnMsgEnabled()))) {
				ToastHelper.info('Please enable message setting');
				return;
			}
			var messageDraftLogic = new _MessageDraftLogic2.default();
			var nameId = this.state.selectedPartyId;
			var nameObj = this.nameCache.findNameObjectByNameId(nameId);
			var contactNo = nameObj.getPhoneNumber();
			var firm = this.firmCache.getDefaultFirm();
			var firmName = firm.getFirmName();
			var balance = nameObj.getAmount();
			var msg = {
				header: this.state.selectedPartyName,
				party_name: this.state.selectedPartyName,
				to: contactNo,
				message: 'Hi,' + '\n' + "It's a friendly reminder to you for paying " + balance + ' to me.',
				footer: 'Thank you,' + '\n' + firmName
			};
			messageDraftLogic.sendSMS(nameId + '-ageing', msg);
			callback();
		}
	}, {
		key: 'sendReminderOnMail',
		value: function sendReminderOnMail(row) {
			var _this6 = this;

			var callback = row.handleClose;
			var nameObj = this.nameCache.findNameObjectByNameId(this.state.selectedPartyId);
			var contactMail = nameObj.getEmail();
			var balance = nameObj.getAmount();
			var firm = this.firmCache.getDefaultFirm();
			var firmName = firm.getFirmName();
			var subjectToSend = 'Vyapar Gentle Reminder!';
			var header = 'Hi,' + '\n\n\n';
			var defaultText = "It's a friendly reminder to you for paying " + balance + ' to me.' + '\n\n';
			var footer = 'Thank You,' + '\n' + firmName;
			var messageToSend = header + defaultText + footer;
			var receiverId = contactMail;
			var isOnline = require('is-online');
			isOnline().then(function (online) {
				if (online) {
					var googleMail = new _GoogleMail2.default();
					googleMail.sendMail({
						fileNames: null,
						receiverId: receiverId,
						alertUser: true,
						subject: subjectToSend,
						message: messageToSend,
						attachFileNames: null,
						successCallBack: _this6.successCallBack,
						errorCallBack: _this6.errorCallBack,
						eventLog: 'saleAgeingTxnReminder'
					});
				} else {
					ToastHelper.error('No internet connection');
				}
			});
			callback();
		}
	}, {
		key: 'downloadExcelFile',
		value: function downloadExcelFile() {
			var fileName = _FileUtil2.default.getFileName({ type: _PDFReportConstant2.default.SALE_AGING_REPORT, fromDate: this.state.date, toDate: this.state.date });
			var excelHelper = new _ExcelHelper2.default();
			excelHelper.saveExcel(fileName);
		}
	}, {
		key: 'viewTransaction',
		value: function viewTransaction(row) {
			var txnId = row.data.txnId;
			TransactionHelper.viewTransaction(txnId, _TxnTypeConstant2.default.getTxnType(_TxnTypeConstant2.default.TXN_TYPE_SALE));
		}
	}, {
		key: 'prepareObjectForExcelPartyWiseAging',
		value: function prepareObjectForExcelPartyWiseAging() {
			var rowObj = [];
			var tableHeadArray = [];
			var totalArray = [];

			tableHeadArray.push('Party');
			tableHeadArray.push('Current');
			tableHeadArray.push('1-30 Days');
			tableHeadArray.push('31-45 Days');
			tableHeadArray.push('46-60 Days');
			tableHeadArray.push('Over 60 Days');
			tableHeadArray.push('Total Balance');
			tableHeadArray.push('Unused Payments');
			tableHeadArray.push('Total Receivable/Total Payable');
			rowObj.push(tableHeadArray);
			rowObj.push([]);

			var dataList = this.state.records;
			var currentTotal = 0;
			var bucket1Total = 0;
			var bucket2Total = 0;
			var bucket3Total = 0;
			var bucket4Total = 0;
			var totalBalanceTotal = 0;
			var unusedpaymentTotal = 0;
			var payableTotal = 0;

			for (var i = 0; i < dataList.length; i++) {
				var tempArray = [];
				var dataObject = dataList[i];
				var nameObj = this.nameCache.findNameObjectByNameId(dataObject.nameId);
				var name = dataObject.name;
				var current = dataObject.current;
				var bucket1 = dataObject.bucket1;
				var bucket2 = dataObject.bucket2;
				var bucket3 = dataObject.bucket3;
				var bucket4 = dataObject.bucket4;
				var total = dataObject.total;
				var payable = nameObj.getAmount();
				var unusedPayment = total - payable;
				tempArray.push(name);
				tempArray.push(_MyDouble2.default.getBalanceAmountWithDecimal(current));
				tempArray.push(_MyDouble2.default.getBalanceAmountWithDecimal(bucket1));
				tempArray.push(_MyDouble2.default.getBalanceAmountWithDecimal(bucket2));
				tempArray.push(_MyDouble2.default.getBalanceAmountWithDecimal(bucket3));
				tempArray.push(_MyDouble2.default.getBalanceAmountWithDecimal(bucket4));
				tempArray.push(_MyDouble2.default.getBalanceAmountWithDecimal(total));
				tempArray.push(_MyDouble2.default.getBalanceAmountWithDecimal(unusedPayment));
				tempArray.push(_MyDouble2.default.getBalanceAmountWithDecimal(payable));

				currentTotal += current;
				bucket1Total += bucket1;
				bucket2Total += bucket2;
				bucket3Total += bucket3;
				bucket4Total += bucket4;
				totalBalanceTotal += total;
				unusedpaymentTotal += unusedPayment;
				payableTotal += payable;

				rowObj.push(tempArray);
			}

			rowObj.push([]);
			totalArray.push('Totals');
			totalArray.push(_MyDouble2.default.getBalanceAmountWithDecimal(currentTotal));
			totalArray.push(_MyDouble2.default.getBalanceAmountWithDecimal(bucket1Total));
			totalArray.push(_MyDouble2.default.getBalanceAmountWithDecimal(bucket2Total));
			totalArray.push(_MyDouble2.default.getBalanceAmountWithDecimal(bucket3Total));
			totalArray.push(_MyDouble2.default.getBalanceAmountWithDecimal(bucket4Total));
			totalArray.push(_MyDouble2.default.getBalanceAmountWithDecimal(totalBalanceTotal));
			totalArray.push(_MyDouble2.default.getBalanceAmountWithDecimal(unusedpaymentTotal));
			totalArray.push(_MyDouble2.default.getBalanceAmountWithDecimal(payableTotal));
			rowObj.push(totalArray);
			return rowObj;
		}
	}, {
		key: 'generateDataInvoiceWise',
		value: function generateDataInvoiceWise(dataList) {
			var rowObj = [];
			var tableHeadArray = [];
			tableHeadArray.push('Reference No');
			tableHeadArray.push('Date');
			tableHeadArray.push('Due Date');
			tableHeadArray.push('Days Late');
			tableHeadArray.push('Total Amount');
			tableHeadArray.push('Balance Amount');
			tableHeadArray.push('');
			rowObj.push(tableHeadArray);
			rowObj.push([]);
			rowObj.push([]);

			for (var i = 0; i < dataList.length; i++) {
				var dataObject = dataList[i];
				var name = dataObject.name;
				var txnArray = dataObject.txnArray;
				rowObj.push([name]);

				var bucketMap = {};

				for (var j = 0; j < txnArray.length; j++) {
					var data = txnArray[j];
					if (!bucketMap[data.bucketName]) {
						bucketMap[data.bucketName] = [];
					}
					bucketMap[data.bucketName].push(data);
				}

				if (bucketMap['bucket4']) {
					rowObj.push(['Over 60 Days']);
					var bucketTotal = 0;
					var bucketBalance = 0;
					for (var k in bucketMap['bucket4']) {
						var _data = bucketMap['bucket4'][k];
						var tempArray = [];
						tempArray.push(_data.invoiceNo);
						tempArray.push(_data.date);
						tempArray.push(_data.dueDate);
						tempArray.push(_data.dueDays);
						tempArray.push(_MyDouble2.default.getBalanceAmountWithDecimal(_data.amount));
						tempArray.push(_MyDouble2.default.getBalanceAmountWithDecimal(_data.balance));
						rowObj.push(tempArray);
						bucketTotal += _data.amount;
						bucketBalance += _data.balance;
					}
					var totalArray = [];
					totalArray.push('Totals');
					totalArray.push('');
					totalArray.push('');
					totalArray.push('');
					totalArray.push(_MyDouble2.default.getBalanceAmountWithDecimal(bucketTotal));
					totalArray.push(_MyDouble2.default.getBalanceAmountWithDecimal(bucketBalance));
					rowObj.push(totalArray);
					rowObj.push([]);
				}

				if (bucketMap['bucket3']) {
					rowObj.push(['45-60 Days']);
					var _bucketTotal = 0;
					var _bucketBalance = 0;
					for (var l in bucketMap['bucket3']) {
						var _data2 = bucketMap['bucket3'][l];
						var _tempArray = [];
						_tempArray.push(_data2.invoiceNo);
						_tempArray.push(_data2.date);
						_tempArray.push(_data2.dueDate);
						_tempArray.push(_data2.dueDays);
						_tempArray.push(_MyDouble2.default.getBalanceAmountWithDecimal(_data2.amount));
						_tempArray.push(_MyDouble2.default.getBalanceAmountWithDecimal(_data2.balance));
						rowObj.push(_tempArray);
						_bucketTotal += _data2.amount;
						_bucketBalance += _data2.balance;
					}
					var _totalArray = [];
					_totalArray.push('Totals');
					_totalArray.push('');
					_totalArray.push('');
					_totalArray.push('');
					_totalArray.push(_MyDouble2.default.getBalanceAmountWithDecimal(_bucketTotal));
					_totalArray.push(_MyDouble2.default.getBalanceAmountWithDecimal(_bucketBalance));
					rowObj.push(_totalArray);
					rowObj.push([]);
				}

				if (bucketMap['bucket2']) {
					rowObj.push(['31-45 Days']);
					var _bucketTotal2 = 0;
					var _bucketBalance2 = 0;
					for (var m in bucketMap['bucket2']) {
						var _data3 = bucketMap['bucket2'][m];
						var _tempArray2 = [];
						_tempArray2.push(_data3.invoiceNo);
						_tempArray2.push(_data3.date);
						_tempArray2.push(_data3.dueDate);
						_tempArray2.push(_data3.dueDays);
						_tempArray2.push(_MyDouble2.default.getBalanceAmountWithDecimal(_data3.amount));
						_tempArray2.push(_MyDouble2.default.getBalanceAmountWithDecimal(_data3.balance));
						rowObj.push(_tempArray2);
						_bucketTotal2 += _data3.amount;
						_bucketBalance2 += _data3.balance;
					}
					var _totalArray2 = [];
					_totalArray2.push('Totals');
					_totalArray2.push('');
					_totalArray2.push('');
					_totalArray2.push('');
					_totalArray2.push(_MyDouble2.default.getBalanceAmountWithDecimal(_bucketTotal2));
					_totalArray2.push(_MyDouble2.default.getBalanceAmountWithDecimal(_bucketBalance2));
					rowObj.push(_totalArray2);
					rowObj.push([]);
				}

				if (bucketMap['bucket1']) {
					rowObj.push(['1-30 Days']);
					var _bucketTotal3 = 0;
					var _bucketBalance3 = 0;
					for (var n in bucketMap['bucket1']) {
						var _data4 = bucketMap['bucket1'][n];
						var _tempArray3 = [];
						_tempArray3.push(_data4.invoiceNo);
						_tempArray3.push(_data4.date);
						_tempArray3.push(_data4.dueDate);
						_tempArray3.push(_data4.dueDays);
						_tempArray3.push(_MyDouble2.default.getBalanceAmountWithDecimal(_data4.amount));
						_tempArray3.push(_MyDouble2.default.getBalanceAmountWithDecimal(_data4.balance));
						rowObj.push(_tempArray3);
						_bucketTotal3 += _data4.amount;
						_bucketBalance3 += _data4.balance;
					}
					var _totalArray3 = [];
					_totalArray3.push('Totals');
					_totalArray3.push('');
					_totalArray3.push('');
					_totalArray3.push('');
					_totalArray3.push(_MyDouble2.default.getBalanceAmountWithDecimal(_bucketTotal3));
					_totalArray3.push(_MyDouble2.default.getBalanceAmountWithDecimal(_bucketBalance3));
					rowObj.push(_totalArray3);
					rowObj.push([]);
				}

				if (bucketMap['current']) {
					rowObj.push(['Current']);
					var _bucketTotal4 = 0;
					var _bucketBalance4 = 0;
					for (var o in bucketMap['current']) {
						var _data5 = bucketMap['current'][o];
						var _tempArray4 = [];
						_tempArray4.push(_data5.invoiceNo);
						_tempArray4.push(_data5.date);
						_tempArray4.push(_data5.dueDate);
						_tempArray4.push(_data5.dueDays);
						_tempArray4.push(_MyDouble2.default.getBalanceAmountWithDecimal(_data5.amount));
						_tempArray4.push(_MyDouble2.default.getBalanceAmountWithDecimal(_data5.balance));
						rowObj.push(_tempArray4);
						_bucketTotal4 += _data5.amount;
						_bucketBalance4 += _data5.balance;
					}
					var _totalArray4 = [];
					_totalArray4.push('Totals');
					_totalArray4.push('');
					_totalArray4.push('');
					_totalArray4.push('');
					_totalArray4.push(_MyDouble2.default.getBalanceAmountWithDecimal(_bucketTotal4));
					_totalArray4.push(_MyDouble2.default.getBalanceAmountWithDecimal(_bucketBalance4));
					rowObj.push(_totalArray4);
					rowObj.push([]);
				}
			}
			return rowObj;
		}
	}, {
		key: 'prepareObjectForExcelForOutstandingSale',
		value: function prepareObjectForExcelForOutstandingSale() {
			var dataList = this.state.records;
			return this.generateDataInvoiceWise(dataList);
		}
	}, {
		key: 'prepareObjectForExcelForInvoiceView',
		value: function prepareObjectForExcelForInvoiceView() {
			var dataList = this.state.records;
			for (var i = 0; i < dataList.length; i++) {
				var dataObject = dataList[i];
				var nameId = dataObject.nameId;

				if (this.state.selectedPartyId == nameId) {
					return this.generateDataInvoiceWise([dataObject]);
				}
			}
		}
	}, {
		key: 'openExcelFromInvoiceView',
		value: function openExcelFromInvoiceView() {
			var dataArray = this.prepareObjectForExcelForInvoiceView();
			var worksheet = _xlsx2.default.utils.aoa_to_sheet(dataArray);

			var workbook = {
				'SheetNames': [],
				'Sheets': {}
			};

			var wsName = 'Sale Ageing Invoices';
			workbook.SheetNames[0] = wsName;
			workbook.Sheets[wsName] = worksheet;
			var wscolWidth = [{ wch: 15 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];

			worksheet['!cols'] = wscolWidth;

			_xlsx2.default.writeFile(workbook, appPath + '/report.xlsx');
			this.downloadExcelFile();
		}
	}, {
		key: 'openExcelFromPartyView',
		value: function openExcelFromPartyView() {
			var dataArray = this.prepareObjectForExcelPartyWiseAging();
			var worksheet = _xlsx2.default.utils.aoa_to_sheet(dataArray);

			var workbook = {
				'SheetNames': [],
				'Sheets': {}
			};

			var wsName = 'Party Wise Aging';
			workbook.SheetNames[0] = wsName;
			workbook.Sheets[wsName] = worksheet;
			var wscolWidth = [{ wch: 15 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];

			worksheet['!cols'] = wscolWidth;
			var wsNameItem = 'Outstanding Sale Invoices';
			workbook.SheetNames[1] = wsNameItem;
			var itemArray = this.prepareObjectForExcelForOutstandingSale();
			var itemWorksheet = _xlsx2.default.utils.aoa_to_sheet(itemArray);
			workbook.Sheets[wsNameItem] = itemWorksheet;
			var wsItemcolWidth = [{ wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }];
			itemWorksheet['!cols'] = wsItemcolWidth;

			_xlsx2.default.writeFile(workbook, appPath + '/report.xlsx');
			this.downloadExcelFile();
		}
	}, {
		key: 'render',
		value: function render() {
			var _state = this.state,
			    firmList = _state.firmList,
			    CurrentComponent = _state.CurrentComponent;

			var partyBalText = this.state.totalPartyBalance >= 0 ? 'Receivable' : 'Payable';

			var firmOptions = (0, _keys2.default)(firmList).map(function (key, index) {
				var firm = firmList[key];
				return _react2.default.createElement(
					'option',
					{ key: index, value: firm.getFirmId() },
					firm.getFirmName()
				);
			});

			return _react2.default.createElement(
				'div',
				{ className: 'd-flex flex-column flex-grow-1 right-column' },
				_react2.default.createElement(
					'div',
					null,
					_react2.default.createElement(
						'div',
						{ className: 'grid-external-filter d-flex justify-content-between' },
						_react2.default.createElement(
							'div',
							{ className: 'd-flex' },
							_react2.default.createElement('img', { src: './../inlineSVG/outline-arrow_back-24px.svg', className: this.state.invoiceview ? '' : 'hide', onClick: this.openPartyView }),
							this.settingCache.getMultipleFirmEnabled() && _react2.default.createElement(
								'div',
								{ className: 'form-control inline ml-15' },
								_react2.default.createElement(
									'label',
									{ className: 'block-label filterLabelText' },
									'Firm'
								),
								_react2.default.createElement(
									'select',
									{
										value: this.state.firm,
										className: 'input-control rounded',
										onChange: this.changeFilter('firmId')
									},
									_react2.default.createElement(
										'option',
										{ value: '' },
										'ALL FIRMS'
									),
									firmOptions
								)
							),
							_react2.default.createElement(
								'div',
								{ className: 'form-control inline ml-15' },
								_react2.default.createElement(
									'label',
									{ className: 'block-label filterLabelText' },
									'REPORT BY'
								),
								_react2.default.createElement(
									'select',
									{
										value: this.state.dateType,
										className: 'input-control rounded',
										selected: this.state.dateType,
										onChange: this.changeFilter('dateType')
									},
									_react2.default.createElement(
										'option',
										{ value: _ReportFilterConstant2.default.DATE_TODAY },
										'Today'
									),
									_react2.default.createElement(
										'option',
										{ value: _ReportFilterConstant2.default.DATE_CUSTOM },
										'Custom'
									)
								)
							),
							_react2.default.createElement(
								'div',
								{ className: 'form-control inline ml-15' },
								_react2.default.createElement(
									'label',
									{ className: 'block-label filterLabelText' },
									'DATE'
								),
								_react2.default.createElement(_JqueryDatePicker2.default, {
									className: 'input-control rounded datepicker-input',
									placeholder: 'dd/mm/yyyy',
									validate: true,
									value: this.state.date,
									onChange: this.changeFilter('date')
								})
							)
						),
						_react2.default.createElement('div', null),
						_react2.default.createElement(
							'div',
							null,
							_react2.default.createElement(
								'div',
								{ className: 'circle-button floatLeft margin-r20', id: '', title: 'Excel Export' },
								_react2.default.createElement('img', { className: 'header-icon', src: '../inlineSVG/excel.svg', onClick: this.state.invoiceview ? this.openExcelFromInvoiceView : this.openExcelFromPartyView })
							),
							_react2.default.createElement(
								'div',
								{ className: 'circle-button floatLeft margin-r20', id: '', title: 'Print' },
								_react2.default.createElement(_SaleAgingReportPrintOptions2.default, { view: this.state.invoiceview, options: this.options, includeGraphInPrint: this.state.includeGraphInPrint, includeInvoiceDetailsInPrint: this.state.includeInvoiceDetailsInPrint })
							)
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'padding12' },
						_react2.default.createElement(
							'div',
							{ className: 'd-flex justify-content-between marginleft5' },
							_react2.default.createElement(
								'div',
								{ className: 'd-flex padBottom10' },
								_react2.default.createElement(
									'div',
									{ className: 'padtop5 padleft10' },
									_react2.default.createElement(
										'span',
										{ className: this.state.invoiceview ? '' : 'hide' },
										this.state.selectedPartyName
									)
								),
								_react2.default.createElement(
									'div',
									{ className: 'padleft10' },
									this.state.invoiceview && this.state.totalPartyBalance > 0 && _react2.default.createElement(_SaleAgeingDropdownMenuForRemind2.default, { options: this.options })
								)
							),
							_react2.default.createElement(
								'div',
								{ className: 'padTop2' },
								_react2.default.createElement(
									'div',
									{ className: 'pieChartNavigation padding6' },
									_react2.default.createElement('img', { src: './../inlineSVG/outline-assessment-24px.svg' }),
									_react2.default.createElement('img', { src: './../inlineSVG/baseline-expand_less-24px.svg', className: this.state.showChart ? '' : 'hide', onClick: this.handleChart }),
									_react2.default.createElement('img', { src: './../inlineSVG/baseline-expand_more-24px.svg', className: this.state.showChart ? 'hide' : '', onClick: this.handleChart })
								)
							)
						),
						_react2.default.createElement(
							'div',
							{ className: 'saleAgeingPieChartParentDiv', id: 'chartParentDiv' },
							this.state.showChart && this.state.pieChartData.length > 0 && _react2.default.createElement(_SaleAgeingReportPieChart2.default, { data: this.state.pieChartData })
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'd-flex justify-content-between' },
						this.state.invoiceview && _react2.default.createElement(_SaleAgeingDropdownMenuForSendSelected2.default, { options: this.options }),
						_react2.default.createElement(
							'div',
							{ className: 'd-flex bd-highlight padding10' },
							_react2.default.createElement(
								'div',
								{ className: 'p-2 flex-fill bd-highlight saleAgeingBal padding10' },
								_react2.default.createElement(
									'span',
									null,
									'Unused Payments & Open Cr Notes:'
								),
								_react2.default.createElement('span', { className: 'saleAgeingAmount marginleft5', dangerouslySetInnerHTML: _MyDouble2.default.getBalanceAmountWithDecimalAndCurrency(this.state.totalUnusedPayment, false, true) })
							)
						),
						_react2.default.createElement(
							'span',
							{ className: 'saleAgeingDateText' },
							'Total ',
							partyBalText,
							': ',
							_react2.default.createElement('span', { dangerouslySetInnerHTML: _MyDouble2.default.getBalanceAmountWithDecimalAndCurrency(this.state.totalPartyBalance, false, true) })
						)
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'd-flex flex-1' },
					CurrentComponent && _react2.default.createElement(CurrentComponent, { dataParty: this.state.records, dataInvoice: this.state.dataInvoice, options: this.options, onResume: this.onResume, getApi: this.getApi })
				)
			);
		}
	}]);
	return SaleAgeingReportContainer;
}(_react2.default.Component);

SaleAgeingReportContainer.propTypes = {
	registerPromise: _propTypes2.default.func
};

exports.default = (0, _trashableReact2.default)(SaleAgeingReportContainer);