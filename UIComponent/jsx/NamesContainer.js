Object.defineProperty(exports, "__esModule", {
	value: true
});

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _typeof2 = require('babel-runtime/helpers/typeof');

var _typeof3 = _interopRequireDefault(_typeof2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _trashableReact = require('trashable-react');

var _trashableReact2 = _interopRequireDefault(_trashableReact);

var _SearchBox = require('./SearchBox');

var _SearchBox2 = _interopRequireDefault(_SearchBox);

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _NameDetail = require('./NameDetail');

var _NameDetail2 = _interopRequireDefault(_NameDetail);

var _ButtonDropdown = require('./ButtonDropdown');

var _ButtonDropdown2 = _interopRequireDefault(_ButtonDropdown);

var _Description = require('@material-ui/icons/Description');

var _Description2 = _interopRequireDefault(_Description);

var _NameList = require('./NameList');

var _NameList2 = _interopRequireDefault(_NameList);

var _DateFilter = require('./grid-filters/DateFilter');

var _DateFilter2 = _interopRequireDefault(_DateFilter);

var _NumberFilter = require('./grid-filters/NumberFilter');

var _NumberFilter2 = _interopRequireDefault(_NumberFilter);

var _StringFilter = require('./grid-filters/StringFilter');

var _StringFilter2 = _interopRequireDefault(_StringFilter);

var _MultiSelectionFilter = require('./grid-filters/MultiSelectionFilter');

var _MultiSelectionFilter2 = _interopRequireDefault(_MultiSelectionFilter);

var _IPCActions = require('../../Constants/IPCActions');

var _throttleDebounce = require('throttle-debounce');

var _MyDate = require('../../Utilities/MyDate');

var _MyDate2 = _interopRequireDefault(_MyDate);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _TransactionHelper = require('../../Utilities/TransactionHelper');

var TransactionHelper = _interopRequireWildcard(_TransactionHelper);

var _DBWindow = require('../../Utilities/DBWindow');

var _DBWindow2 = _interopRequireDefault(_DBWindow);

var _TxnTypeConstant = require('../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _TxnPaymentStatusConstants = require('../../Constants/TxnPaymentStatusConstants');

var _TxnPaymentStatusConstants2 = _interopRequireDefault(_TxnPaymentStatusConstants);

var _ColorConstants = require('../../Constants/ColorConstants');

var _ColorConstants2 = _interopRequireDefault(_ColorConstants);

var _NameType = require('../../Constants/NameType');

var _NameType2 = _interopRequireDefault(_NameType);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _SettingCache = require('../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

var _FirstScreen = require('./UIControls/FirstScreen');

var _FirstScreen2 = _interopRequireDefault(_FirstScreen);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ShowPaymentStatusForTheseTransactions = [_TxnTypeConstant2.default.TXN_TYPE_SALE, _TxnTypeConstant2.default.TXN_TYPE_PURCHASE, _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN, _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN, _TxnTypeConstant2.default.TXN_TYPE_CASHIN, _TxnTypeConstant2.default.TXN_TYPE_CASHOUT, _TxnTypeConstant2.default.TXN_TYPE_ROPENBALANCE, _TxnTypeConstant2.default.TXN_TYPE_POPENBALANCE, _TxnTypeConstant2.default.TXN_TYPE_LINKED_CASHIN_OPENINGBALANCE, _TxnTypeConstant2.default.TXN_TYPE_LINKED_CASHOUT_OPENINGBALANCE];

var NamesContainer = function (_Component) {
	(0, _inherits3.default)(NamesContainer, _Component);

	function NamesContainer(props) {
		(0, _classCallCheck3.default)(this, NamesContainer);

		var _this = (0, _possibleConstructorReturn3.default)(this, (NamesContainer.__proto__ || (0, _getPrototypeOf2.default)(NamesContainer)).call(this, props));

		_this.importParties = function () {
			$('.sideNav .list-item.importParties').click();
		};

		_this.onResume = function () {
			_this.props.onResume && _this.props.onResume();
			var columnDefs = _this.getColumnDefs();
			_this.setState({ columnDefs: columnDefs });

			_this.getNames().then(function (names) {
				_this.allNames = names;
				_this.setState({ names: names });
				if (_this.state.currentItem) {
					var currentItem = _this.allNames.find(function (item) {
						return _this.state.currentItem.nameId === item.nameId;
					});
					_this.setState({ currentItem: currentItem });
				}
				_this.filterItems(_this.state.searchText);
				_this.onItemSelected(null, true);
				_this.api.setColumnDefs(columnDefs);
				_this.grid.api.sizeColumnsToFit();
			});
		};

		_this.filterItems = _this.filterItems.bind(_this);
		_this.onItemSelected = (0, _throttleDebounce.debounce)(50, _this.onItemSelected.bind(_this));
		_this.getData = _this.getData.bind(_this);
		_this.getApi = _this.getApi.bind(_this);
		NamesContainer.viewTransaction = NamesContainer.viewTransaction.bind(_this);
		_this.contextMenu = [{
			title: 'View/Edit',
			cmd: NamesContainer.viewTransaction,
			visible: true,
			id: 'edit'
		}, {
			title: 'Delete',
			cmd: function cmd(row) {
				return TransactionHelper.deleteTransaction(row.txnId, _TxnTypeConstant2.default.getTxnType(row.txnType));
			},
			visible: true,
			id: 'delete'
		}, {
			title: 'Open PDF',
			cmd: function cmd(row) {
				return TransactionHelper.openPDF(row.txnId, _TxnTypeConstant2.default.getTxnType(row.txnType));
			},
			visible: true,
			id: 'openPDF'
		}, {
			title: 'Preview',
			cmd: function cmd(row) {
				return TransactionHelper.preview(row.txnId, _TxnTypeConstant2.default.getTxnType(row.txnType));
			},
			visible: true,
			id: 'preview'
		}, {
			title: 'Print',
			cmd: function cmd(row) {
				return TransactionHelper.print(row.txnId, _TxnTypeConstant2.default.getTxnType(row.txnType));
			},
			visible: true,
			id: 'print'
		}, {
			title: 'Preview As Delivery Challan',
			cmd: function cmd(row) {
				return TransactionHelper.preview(row.txnId, _TxnTypeConstant2.default.getTxnType(row.txnType), true);
			},
			visible: true,
			id: 'printDeliveryChallanpreview'
		}, {
			title: 'Return',
			cmd: function cmd(row) {
				return TransactionHelper.returnTransaction(row.txnId, _TxnTypeConstant2.default.getTxnType(row.txnType));
			},
			visible: true,
			id: 'rtn'
		}, {
			title: 'Receive Payment',
			cmd: function cmd(row) {
				return TransactionHelper.receiveOrMakePayment(row.txnId, _TxnTypeConstant2.default.getTxnType(row.txnType));
			},
			visible: true,
			id: 'receivePayment'
		}, {
			title: 'Payment History',
			cmd: function cmd(row) {
				return window.showPaymentHistory(row.txnId + ':' + _TxnTypeConstant2.default.getTxnType(row.txnType));
			},
			visible: true,
			id: 'paymentHistory'
		}];
		_this.state = {
			records: [],
			names: [],
			currentItem: null,
			columnDefs: _this.getColumnDefs(),
			contextMenu: _this.contextMenu,
			showSearch: false,
			loaded: false
		};

		_this._perPage = 100;
		_this._id = null;
		_this.allNames = [];
		return _this;
	}

	(0, _createClass3.default)(NamesContainer, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			var _this2 = this;

			this.getNames().then(function (names) {
				_this2.allNames = names;
				_this2.setState({ names: names, loaded: true });
			});
			window.onResume = this.onResume.bind(this);
			TransactionHelper.bindEventsForPreviewDialog();
		}
	}, {
		key: 'addNewParty',
		value: function addNewParty() {
			var CommonUtility = require('../../Utilities/CommonUtility');
			window.userNameGlobal = '';
			window.MyAnalytics.pushEvent('Add Party Open');
			CommonUtility.loadFrameDiv('NewContact.html');
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			delete window.onResume;
			TransactionHelper.unBindEventsForPreviewDialog();
		}
	}, {
		key: 'getColumnDefs',
		value: function getColumnDefs() {
			var settingCache = new _SettingCache2.default();
			var columnDefs = [{
				field: 'color',
				colId: 'color',
				headerName: '',
				width: 30,
				suppressMenu: false,
				sortable: false,
				getQuickFilterText: function getQuickFilterText() {
					return '';
				},
				filter: false,
				cellRenderer: function cellRenderer(params) {
					var color = _ColorConstants2.default[params.data.txnType];
					return '<div style=\'background-color: ' + color + '; border-radius:50%;width:8px;height:8px; top: 19px; position:relative;\'></div>';
				}
			}, {
				field: 'txnType',
				colId: 'txnType',
				filter: _MultiSelectionFilter2.default,
				filterOptions: _TxnTypeConstant2.default.getFilterOptionsForParties(),
				getQuickFilterText: function getQuickFilterText(params) {
					return _TxnTypeConstant2.default.getTxnTypeForUI(params.data.txnType);
				},
				cellRenderer: function cellRenderer(params) {
					return _TxnTypeConstant2.default.getTxnTypeForUI(params.value);
				},
				headerName: 'TYPE',
				width: 110
			}, {
				field: 'txnRefNumber',
				colId: 'txnRefNumber',
				filter: _StringFilter2.default,
				headerName: 'NUMBER',
				cellClass: 'alignRight',
				width: 100,
				comparator: function comparator(a, b) {
					var typeA = typeof a === 'undefined' ? 'undefined' : (0, _typeof3.default)(a);
					var typeB = typeof b === 'undefined' ? 'undefined' : (0, _typeof3.default)(b);
					if (typeA === 'number' && typeB === 'number') {
						return a - b;
					} else if (typeA === 'string' && typeB === 'string') {
						return a.localeCompare(b);
					} else {
						if (typeA === 'number') {
							return -1;
						} else {
							return 1;
						}
						// return a > b ? 1 : a < b ? -1 : 0;
					}
				},
				valueGetter: function valueGetter(params) {
					var value = Number(params.data.txnRefNumber);
					// return value;
					if (isNaN(value)) {
						return params.data.txnRefNumber;
					}
					return value;
				},
				getQuickFilterText: function getQuickFilterText(params) {
					var _params$data = params.data,
					    txnRefNumber = _params$data.txnRefNumber,
					    prefixValue = _params$data['prefix.prefixValue'];

					var prefix = txnRefNumber && prefixValue || '';
					return '' + prefix + txnRefNumber;
				},
				cellRenderer: function cellRenderer(params) {
					var _params$data2 = params.data,
					    txnRefNumber = _params$data2.txnRefNumber,
					    prefixValue = _params$data2['prefix.prefixValue'];

					var prefix = txnRefNumber && prefixValue || '';
					return '' + prefix + txnRefNumber;
				}
			}, {
				field: 'txnDate',
				colId: 'txnDate',
				headerName: 'DATE',
				width: 100,
				sort: 'desc',
				sortingOrder: ['desc', 'asc', null],
				filter: _DateFilter2.default,
				valueGetter: function valueGetter(params) {
					var txnDate = params.data.txnDate;

					if (txnDate) {
						var date = new Date(txnDate);
						date.setHours(0, 0, 0, 0);
						return date;
					}
				},
				getQuickFilterText: function getQuickFilterText(params) {
					if (params.value) {
						return _MyDate2.default.getDate('d/m/y', new Date(params.value));
					}
				},
				cellRenderer: function cellRenderer(params) {
					if (params.value) {
						return _MyDate2.default.getDate('d/m/y', new Date(params.value));
					}
				}
			}, {
				field: 'txnCurrentBalance',
				colId: 'txnCurrentBalance',
				headerName: 'TOTAL',
				width: 100,
				filter: _NumberFilter2.default,
				cellClass: 'alignRight',
				valueGetter: function valueGetter(params) {
					var _params$data3 = params.data,
					    txnCashAmount = _params$data3.txnCashAmount,
					    txnBalanceAmount = _params$data3.txnBalanceAmount,
					    txnType = _params$data3.txnType,
					    txnDiscountAmount = _params$data3.txnDiscountAmount;

					var total = txnCashAmount + txnBalanceAmount;
					if (txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHOUT) {
						total += txnDiscountAmount;
					}
					return total;
				},
				cellRenderer: function cellRenderer(params) {
					return _MyDouble2.default.getBalanceAmountWithDecimalAndCurrencyWithoutColor(params.value);
				}
			}];
			if (settingCache.isBillToBillEnabled()) {
				columnDefs.push({
					field: 'txnCurrentBalance',
					colId: 'txnCurrentBalance',
					headerName: 'BALANCE/ UNUSED',
					cellClass: 'alignRight',
					getQuickFilterText: function getQuickFilterText(params) {
						return params.data.txnCurrentBalance;
					},
					valueGetter: function valueGetter(params) {
						var _params$data4 = params.data,
						    txnType = _params$data4.txnType,
						    txnCurrentBalance = _params$data4.txnCurrentBalance;

						if (txnType === _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE || txnType === _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN) {
							return 0;
						}
						return txnCurrentBalance;
					},
					cellRenderer: function cellRenderer(params) {
						var txnType = params.data.txnType;

						if (txnType === _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE || txnType === _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN) {
							return '';
						}
						return _MyDouble2.default.getBalanceAmountWithDecimalAndCurrencyWithoutColor(params.value);
					},
					width: 100,
					filter: _NumberFilter2.default
				});
				if (settingCache.isPaymentTermEnabled()) {
					columnDefs.push({
						field: 'txnDueDate',
						colId: 'txnDueDate',
						headerName: 'DUE DATE',
						width: 100,
						sortingOrder: ['desc', 'asc', null],
						filter: _DateFilter2.default,
						enableFilter: true,
						valueGetter: function valueGetter(params) {
							var _params$data5 = params.data,
							    txnDueDate = _params$data5.txnDueDate,
							    txnType = _params$data5.txnType,
							    txnPaymentStatus = _params$data5.txnPaymentStatus;

							if ((txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE) && txnDueDate && txnPaymentStatus !== _TxnPaymentStatusConstants2.default.PAID) {
								if (txnDueDate) {
									var date = new Date(txnDueDate);
									date.setHours(0, 0, 0, 0);
									return date;
								}
							}
						},
						getQuickFilterText: function getQuickFilterText(params) {
							var _params$data6 = params.data,
							    txnType = _params$data6.txnType,
							    txnPaymentStatus = _params$data6.txnPaymentStatus;

							if ((txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE) && params.value && txnPaymentStatus !== _TxnPaymentStatusConstants2.default.PAID) {
								return _MyDate2.default.getDate('d/m/y', new Date(params.value));
							} else {
								return '';
							}
						},
						cellRenderer: function cellRenderer(params) {
							var _params$data7 = params.data,
							    txnType = _params$data7.txnType,
							    txnPaymentStatus = _params$data7.txnPaymentStatus;

							if ((txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE) && params.value && txnPaymentStatus !== _TxnPaymentStatusConstants2.default.PAID) {
								return _MyDate2.default.getDate('d/m/y', params.value);
							}
							return '';
						}
					});
					columnDefs.push({
						field: 'txnPaymentStatus',
						colId: 'txnPaymentStatus',
						filter: _MultiSelectionFilter2.default,
						filterOptions: [].concat((0, _toConsumableArray3.default)(_TxnPaymentStatusConstants2.default.getFilterOptions()), [{
							value: _TxnPaymentStatusConstants2.default.OVERDUE,
							label: 'Overdue',
							selected: false
						}]),
						getQuickFilterText: function getQuickFilterText(params) {
							var _params$data8 = params.data,
							    txnType = _params$data8.txnType,
							    txnPaymentStatus = _params$data8.txnPaymentStatus,
							    txnDueDate = _params$data8.txnDueDate;

							var dueDays = _MyDouble2.default.getDueDays(new Date(txnDueDate));
							var paymentStatusString = _TxnPaymentStatusConstants2.default.getPaymentStatusForUI(txnType, txnPaymentStatus);
							if (!ShowPaymentStatusForTheseTransactions.includes(txnType)) {
								paymentStatusString = '';
							}

							if (txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE) {
								if (dueDays > 0 && txnPaymentStatus !== _TxnPaymentStatusConstants2.default.PAID) {
									return 'Overdue ' + dueDays + ' Days';
								}
							}
							return paymentStatusString;
						},
						valueGetter: function valueGetter(params) {
							var _params$data9 = params.data,
							    txnType = _params$data9.txnType,
							    txnPaymentStatus = _params$data9.txnPaymentStatus,
							    txnDueDate = _params$data9.txnDueDate;


							if ((txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE) && txnDueDate) {
								var dueDays = _MyDouble2.default.getDueDays(new Date(txnDueDate));
								if (dueDays > 0 && txnPaymentStatus !== _TxnPaymentStatusConstants2.default.PAID) {
									return _TxnPaymentStatusConstants2.default.OVERDUE;
								}
							}
							return txnPaymentStatus;
						},
						cellRenderer: function cellRenderer(params) {
							var _params$data10 = params.data,
							    txnType = _params$data10.txnType,
							    txnPaymentStatus = _params$data10.txnPaymentStatus,
							    txnDueDate = _params$data10.txnDueDate;

							var dueDays = _MyDouble2.default.getDueDays(new Date(txnDueDate));
							var paymentStatusString = _TxnPaymentStatusConstants2.default.getPaymentStatusForUI(txnType, txnPaymentStatus);
							if (!ShowPaymentStatusForTheseTransactions.includes(txnType)) {
								paymentStatusString = '';
							}

							if (txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE) {
								if (dueDays > 0 && txnPaymentStatus !== _TxnPaymentStatusConstants2.default.PAID) {
									return 'Overdue ' + dueDays + ' Days';
								}
							}
							return paymentStatusString;
						},
						headerName: 'STATUS',
						width: 100
					});
				} else {
					columnDefs.push({
						field: 'txnPaymentStatus',
						colId: 'txnPaymentStatus',
						filter: _MultiSelectionFilter2.default,
						filterOptions: _TxnPaymentStatusConstants2.default.getFilterOptions(),
						valueGetter: function valueGetter(params) {
							var _params$data11 = params.data,
							    txnType = _params$data11.txnType,
							    txnPaymentStatus = _params$data11.txnPaymentStatus;

							var paymentStatusString = _TxnPaymentStatusConstants2.default.getPaymentStatusForUI(txnType, txnPaymentStatus);
							if (!ShowPaymentStatusForTheseTransactions.includes(txnType)) {
								return '';
							}
							return paymentStatusString;
						},
						cellRenderer: function cellRenderer(params) {
							return params.value;
						},
						headerName: 'STATUS',
						width: 100
					});
				}
			} else {
				columnDefs.push({
					field: 'txnBalanceAmount',
					colId: 'txnBalanceAmount',
					headerName: 'BALANCE',
					cellClass: 'alignRight',
					getQuickFilterText: function getQuickFilterText(params) {
						return params.data.txnBalanceAmount;
					},
					valueGetter: function valueGetter(params) {
						var _params$data12 = params.data,
						    txnType = _params$data12.txnType,
						    txnBalanceAmount = _params$data12.txnBalanceAmount;

						if (txnType === _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE || txnType === _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN || txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHOUT) {
							return 0;
						}
						return txnBalanceAmount;
					},
					cellRenderer: function cellRenderer(params) {
						var _params$data13 = params.data,
						    txnType = _params$data13.txnType,
						    txnBalanceAmount = _params$data13.txnBalanceAmount;

						if (txnType === _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE || txnType === _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN || txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHOUT) {
							return '';
						}
						return _MyDouble2.default.getBalanceAmountWithDecimalAndCurrencyWithoutColor(txnBalanceAmount);
					},
					width: 100,
					filter: _NumberFilter2.default
				});
			}

			columnDefs.push({
				field: 'txnId',
				colId: 'txnId',
				headerName: '',
				width: 30,
				getQuickFilterText: function getQuickFilterText() {
					return '';
				},
				sortable: false,
				filter: false,
				cellRenderer: function cellRenderer() {
					return '<div class="contextMenuDots"></div>';
				}
			});
			return columnDefs;
		}
	}, {
		key: 'getNames',
		value: function getNames() {
			return this.props.registerPromise(_DBWindow2.default.send(_IPCActions.GET_NAMES_LIST, { nameType: _NameType2.default.NAME_TYPE_PARTY }));
		}
	}, {
		key: 'getData',
		value: function getData(response) {
			if (response.meta.key !== this.currentRequestKey) {
				return false;
			}
			if (response.meta.next && response.data.length === this._perPage) {
				var next = response.meta.next;
				this.loadTransactions(next.id, next.start, next.noOfRecords, next.key);
			}
			if (response.meta.next) {
				this.api.setRowData(response.data);
			} else {
				this.api.updateRowData({ add: response.data });
			}
		}
	}, {
		key: 'loadTransactions',
		value: function loadTransactions(id, start, noOfRecords, key, next) {
			var _this3 = this;

			this.props.registerPromise(_DBWindow2.default.send(_IPCActions.GET_NAME_TRANSACTIONS, {
				id: id,
				start: start,
				noOfRecords: noOfRecords,
				key: key,
				next: next
			})).then(function (data) {
				return _this3.getData(data);
			});
		}
	}, {
		key: 'onItemSelected',
		value: function onItemSelected(row) {
			var fromSync = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

			if (!fromSync) {
				if (!row.node.selected) {
					return false;
				}
				this._id = row.data.nameId;
				this.setState({ currentItem: row.data });
			}
			this.currentRequestKey = Math.random() * 1000000000000000000;
			this.loadTransactions(this._id, 0, this._perPage, this.currentRequestKey, {
				id: this._id,
				start: this._perPage,
				key: this.currentRequestKey,
				noOfRecords: -1
			});
		}
	}, {
		key: 'filterItems',
		value: function filterItems(searchText) {
			if (!searchText) {
				this.setState({ names: this.allNames, searchText: '' });
				return false;
			}
			var query = searchText.toLowerCase();
			var filteredItems = this.allNames.filter(function (party) {
				var fullName = party.fullName,
				    _party$email = party.email,
				    email = _party$email === undefined ? '' : _party$email,
				    _party$phoneNumber = party.phoneNumber,
				    phoneNumber = _party$phoneNumber === undefined ? '' : _party$phoneNumber;

				return fullName.toLowerCase().includes(query) || email && email.toLowerCase().includes(query) || phoneNumber && phoneNumber.toLowerCase().includes(query);
			});
			var records = this.state.records;
			var currentItem = this.state.currentItem;
			if (!filteredItems.length) {
				records = [];
				this.api.setRowData([]);
				currentItem = null;
			}
			this.setState({
				records: records,
				currentItem: currentItem,
				names: filteredItems,
				searchText: searchText
			});
		}
	}, {
		key: 'filterContextMenu',
		value: function filterContextMenu(row) {
			var menuItems = TransactionHelper.filterContextMenu([].concat((0, _toConsumableArray3.default)(this.contextMenu)), {
				data: {
					txnTypeConstant: TxnTypeConstant.getTxnType(row.data.txnType),
					paymentStatus: row.data.txnPaymentStatus
				}
			});
			this.setState({ contextMenu: menuItems });
		}
	}, {
		key: 'getApi',
		value: function getApi(params) {
			this.grid = params;
			this.api = params.api;
			this.api.setRowData(this.state.records);
		}
	}, {
		key: 'getMenuOptions',
		value: function getMenuOptions() {
			var menuOptions = [];
			menuOptions.push({
				buttonText: 'Add Party',
				buttonIcon: _react2.default.createElement(
					'div',
					null,
					'+'
				),
				onClick: this.addNewParty
			});
			menuOptions.push({
				buttonText: 'Import Parties',
				buttonIcon: _react2.default.createElement(_Description2.default, { color: 'primary', fontSize: 'inherit' }),
				onClick: this.importParties
			});
			return menuOptions;
		}
	}, {
		key: 'render',
		value: function render() {
			var _this4 = this;

			var partyId = this.props.partyId;
			var _state = this.state,
			    columnDefs = _state.columnDefs,
			    loaded = _state.loaded,
			    names = _state.names,
			    showSearch = _state.showSearch,
			    currentItem = _state.currentItem,
			    contextMenu = _state.contextMenu;

			var gridOptions = {
				enableFilter: true,
				enableSorting: true,
				rowSelection: 'single',
				columnDefs: columnDefs,
				rowHeight: 40,
				headerHeight: 40,
				rowClass: 'customVerticalBorder',
				rowData: [],
				overlayNoRowsTemplate: 'No transactions to show',
				onRowDoubleClicked: NamesContainer.viewTransaction.bind(this)
			};
			return _react2.default.createElement(
				'div',
				{ className: 'd-flex align-items-stretch flex-container' },
				loaded && this.allNames.length === 0 && _react2.default.createElement(_FirstScreen2.default, {
					image: '../inlineSVG/first-screens/party.svg',
					text: 'Add your customers & suppliers. Manage your business with them.',
					onClick: this.addNewParty,
					buttonLabel: 'Add Your First Party'
				}),
				loaded && this.allNames.length > 0 && _react2.default.createElement(
					_react.Fragment,
					null,
					_react2.default.createElement(
						'div',
						{ className: 'flex-column d-flex left-column' },
						_react2.default.createElement(
							'div',
							{ className: 'listheaderDiv d-flex mt-20 mb-10' },
							_react2.default.createElement(
								'div',
								{
									className: (0, _classnames2.default)('searchDiv', showSearch ? 'width100 d-flex' : '')
								},
								_react2.default.createElement(_SearchBox2.default, {
									throttle: 500,
									filter: this.filterItems,
									collapsed: !showSearch,
									collapsedTextBoxClass: 'width100',
									onCollapsedClick: function onCollapsedClick() {
										_this4.setState({ showSearch: true });
									},
									onInputBlur: function onInputBlur() {
										_this4.setState({ showSearch: false });
									}
								})
							),
							_react2.default.createElement(
								'div',
								{ className: 'buttonDiv flex-1 d-flex justify-content-end' },
								_react2.default.createElement(_ButtonDropdown2.default, {
									collapsed: showSearch,
									addPadding: true,
									menuOptions: this.getMenuOptions()
								})
							)
						),
						_react2.default.createElement(_NameList2.default, {
							onItemSelected: this.onItemSelected,
							selectedFilter: this.props.selectedFilter,
							partyId: partyId,
							items: names
						})
					),
					_react2.default.createElement(
						'div',
						{ className: 'd-flex flex-column flex-grow-1 right-column' },
						_react2.default.createElement(_NameDetail2.default, { currentItem: currentItem }),
						_react2.default.createElement(_Grid2.default, {
							title: 'Transactions',
							quickFilter: true,
							classes: 'alternateRowColor customVerticalBorder gridRowHeight40',
							filterContextMenu: this.filterContextMenu.bind(this),
							contextMenu: contextMenu,
							getApi: this.getApi,
							height: '100%',
							gridOptions: gridOptions
						})
					)
				)
			);
		}
	}], [{
		key: 'viewTransaction',
		value: function viewTransaction(row) {
			if (!row.txnId) {
				row = row.data;
			}
			TransactionHelper.viewTransaction(row.txnId, _TxnTypeConstant2.default.getTxnType(row.txnType));
		}
	}]);
	return NamesContainer;
}(_react.Component);

NamesContainer.propTypes = {
	onResume: _propTypes2.default.func,
	partyId: _propTypes2.default.number,
	selectedFilter: _propTypes2.default.string,
	registerPromise: _propTypes2.default.func
};

exports.default = (0, _trashableReact2.default)(NamesContainer);