Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _ButtonDropdown = require('./ButtonDropdown');

var _ButtonDropdown2 = _interopRequireDefault(_ButtonDropdown);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles() {
	return {
		span: {
			color: '#284561',
			fontFamily: 'robotoMedium',
			maxWidth: 400,
			overflow: 'hidden',
			whiteSpace: 'nowrap',
			textOverflow: 'ellipsis',
			display: 'block'
		},
		containerForItemList: {
			maxHeight: 100
		},
		viewReportLink: {
			cursor: 'pointer',
			marginRight: '10px',
			color: '#1789FC',
			fontSize: '14px'
		}
	};
};

var LoanAccountDetail = function (_React$Component) {
	(0, _inherits3.default)(LoanAccountDetail, _React$Component);

	function LoanAccountDetail() {
		var _ref;

		var _temp, _this, _ret;

		(0, _classCallCheck3.default)(this, LoanAccountDetail);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = LoanAccountDetail.__proto__ || (0, _getPrototypeOf2.default)(LoanAccountDetail)).call.apply(_ref, [this].concat(args))), _this), _this.onPayEmiClick = function () {
			_this.props.handlePayEmiClick && _this.props.handlePayEmiClick('');
		}, _this.onAdjustLoanClick = function () {
			_this.props.handleAdjustLoanClick && _this.props.handleAdjustLoanClick('');
		}, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
	}

	(0, _createClass3.default)(LoanAccountDetail, [{
		key: 'getMenuOptions',
		value: function getMenuOptions() {
			var menuOptions = [];
			var currentItem = this.props.currentItem;

			currentItem && Number(currentItem.currentBalance) && menuOptions.push({
				buttonText: 'Make Payment',
				buttonIcon: _react2.default.createElement('img', { style: { height: '12px' }, src: './../inlineSVG/item_adj.svg' }),
				onClick: this.onPayEmiClick
			});
			menuOptions.push({
				buttonText: 'Take more loan',
				buttonIcon: _react2.default.createElement('img', { style: { height: '12px' }, src: './../inlineSVG/item_adj.svg' }),
				onClick: this.onAdjustLoanClick
			});
			return menuOptions;
		}
	}, {
		key: 'openLoanStatement',
		value: function openLoanStatement() {
			var CommonUtility = require('../../Utilities/CommonUtility');
			CommonUtility.loadDefaultPage('ViewReports.html', function () {
				window.changeReportPage('LoanStatement.html');
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var classes = this.props.classes;
			var item = this.props.currentItem;

			if (!item || !item.accountId) {
				return false;
			}

			return _react2.default.createElement(
				'div',
				{ className: 'containerForItemList ' + classes.containerForItemList },
				_react2.default.createElement(
					'div',
					{ className: 'mb-10 d-flex justify-content-between' },
					_react2.default.createElement(
						'div',
						null,
						_react2.default.createElement(
							'span',
							{ style: { textTransform: 'uppercase', fontSize: '1rem' }, className: classes.span },
							item.accountName
						),
						_react2.default.createElement(
							'span',
							{ style: { fontSize: '.75rem' }, className: classes.span },
							'Lending Bank / Agency: ',
							item.bankName
						),
						_react2.default.createElement(
							'span',
							{ style: { fontSize: '.75rem' }, className: classes.span },
							'Account Number: ',
							item.accountNumber
						)
					),
					_react2.default.createElement(
						'div',
						null,
						_react2.default.createElement(_ButtonDropdown2.default, { classNames: true, color: 'secondary', menuOptions: this.getMenuOptions() })
					)
				),
				_react2.default.createElement(
					'div',
					null,
					_react2.default.createElement(
						'div',
						{ className: 'mb-10 d-flex justify-content-between' },
						_react2.default.createElement(
							'div',
							{ className: 'mr-10' },
							_react2.default.createElement(
								'span',
								{ className: classes.span },
								'Balance Amount: ',
								_react2.default.createElement('span', { style: { color: '#e35959' }, dangerouslySetInnerHTML: { __html: _MyDouble2.default.getAmountWithDecimalAndCurrency(item.currentBalance) } })
							)
						),
						_react2.default.createElement(
							'span',
							{ className: classes.viewReportLink, onClick: this.openLoanStatement },
							'View Loan Statement'
						)
					)
				)
			);
		}
	}]);
	return LoanAccountDetail;
}(_react2.default.Component);

LoanAccountDetail.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	currentItem: _propTypes2.default.object,
	handlePayEmiClick: _propTypes2.default.func,
	handleAdjustLoanClick: _propTypes2.default.func
};

exports.default = (0, _styles.withStyles)(styles)(LoanAccountDetail);