Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _Dialog = require('@material-ui/core/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Close = require('@material-ui/icons/Close');

var _Close2 = _interopRequireDefault(_Close);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _SettingCache = require('../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

var _ItemCache = require('../../Cache/ItemCache');

var _ItemCache2 = _interopRequireDefault(_ItemCache);

var _SalePurchaseHelpers = require('./SalePurchaseContainer/SalePurchaseHelpers');

var _SelectBatchesModal = require('./SelectBatchesModal');

var _SelectBatchesModal2 = _interopRequireDefault(_SelectBatchesModal);

var _themes = require('../../themes');

var _themes2 = _interopRequireDefault(_themes);

var _ToastHelper = require('../../UIControllers/ToastHelper');

var ToastHelper = _interopRequireWildcard(_ToastHelper);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _DecimalInputFilter = require('../../Utilities/DecimalInputFilter');

var DecimalInputFilter = _interopRequireWildcard(_DecimalInputFilter);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var itemCache = void 0;
var settingCache = void 0;

var theme = (0, _themes2.default)();

var styles = function styles() {
	return {
		dialogContainer: {
			width: '100%',
			maxWidth: '100%'
		},
		borderRadius: {
			borderRadius: 8
		},
		container: {},
		header: {
			marginTop: 15
		},
		footer: {
			borderTop: '1px solid #DDDDDD',
			textAlign: 'right',
			marginRight: 5,
			paddingTop: 7,
			height: 44
		},
		title: {
			color: '#222A3F',
			fontSize: 14,
			fontFamily: 'robotomedium',
			paddingLeft: 15
		},
		searchBox: {
			background: '#E9ECF0',
			marginLeft: 15
		},
		trackingList: {
			marginTop: 15,
			maxHeight: 500,
			overflow: 'auto'
		},
		column: {
			borderRight: '1px solid #DDDDDD',
			display: 'flex',
			alignItems: 'center',
			padding: '0 10px',
			width: 98
		},
		nameColumn: {
			width: 200
		},
		codeColumn: {
			width: 160
		},
		listHeader: {
			fontSize: 12,
			color: 'rgba(34, 42, 63, 0.5)',
			height: 46,
			borderTop: '1px solid #ddd',
			boxShadow: '0px 0px 5px 0px #000',
			marginBottom: 1,
			fontFamily: 'robotomedium'
		},
		item: {
			height: 42,
			fontSize: 12,
			borderTop: '1px solid #ddd',
			color: '#222A3F',
			fontFamily: 'robotomedium',
			'&:nth-child(even)': {
				background: '#E9ECF0'
			}
		},
		input: {
			border: '1px solid #1789FC',
			width: 80,
			borderRadius: 4,
			height: 32,
			padding: 5,
			textAlign: 'right'
		},
		nameInput: {
			width: 180
		},
		codeInput: {
			width: 140
		},
		closeIconWrapper: {
			display: 'flex',
			alignItems: 'center',
			height: 24,
			width: 24,
			justifyContent: 'center',
			borderRadius: 12,
			marginRight: 10,
			cursor: 'pointer'
		},
		closeIcon: {
			color: 'rgba(34,58,82,0.54)'
		}
	};
};

var BarcodeModal = function (_React$Component) {
	(0, _inherits3.default)(BarcodeModal, _React$Component);

	function BarcodeModal(props) {
		(0, _classCallCheck3.default)(this, BarcodeModal);

		var _this = (0, _possibleConstructorReturn3.default)(this, (BarcodeModal.__proto__ || (0, _getPrototypeOf2.default)(BarcodeModal)).call(this, props));

		_this.onQtyChange = function (e, index) {
			var selected = _this.state.selected;
			var target = e.target;

			var newQty = DecimalInputFilter.inputTextFilterForQuantity(target);
			var current = selected[index];
			current.lineItem.qty = newQty;
			_this.setState({ selected: selected });
		};

		_this.state = {
			selected: [],
			showItemStockTrackingModal: false,
			itemStockTrackingList: []
		};
		_this.itemCode = '';
		_this.onClose = _this.onClose.bind(_this);
		_this.onSave = _this.onSave.bind(_this);
		_this.onCodeChange = _this.onCodeChange.bind(_this);
		_this.onCodeBlur = _this.onCodeBlur.bind(_this);
		_this.onItemStockTrackingDialogClose = _this.onItemStockTrackingDialogClose.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(BarcodeModal, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			itemCache = new _ItemCache2.default();
			settingCache = new _SettingCache2.default();
		}
	}, {
		key: 'onClose',
		value: function onClose(e) {
			e.stopPropagation();
			this.props.onClose && this.props.onClose();
		}
	}, {
		key: 'onCodeChange',
		value: function onCodeChange(e, index) {
			var itemCode = e.target.value;
			var selected = this.state.selected;

			var item = selected[index];
			item.itemCode = itemCode;
			this.setState({ selected: selected });
		}
	}, {
		key: 'onCodeBlur',
		value: function onCodeBlur(e, index, last) {
			var _this2 = this;

			var keypress = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;

			if (keypress) {
				e.stopPropagation();
			}
			if (keypress && e.keyCode !== 13) {
				return false;
			}
			var itemCode = e.target.value.trim();
			if (this.itemCode) {
				return false;
			}
			var selected = this.state.selected;
			var _props = this.props,
			    txnType = _props.txnType,
			    party = _props.party,
			    getDefaultLineItem = _props.getDefaultLineItem;

			var itemId = 0;
			var nameId = party && party.value ? party.value : 0;
			this.itemCode = itemCode;
			if (last) {
				e.target.value = '';
			} else {
				if (!itemCode.trim()) {
					var items = selected.filter(function (item, idx) {
						return idx !== index;
					});
					this.setState({ selected: items });
					return false;
				}
			}

			if (!itemCode) {
				return false;
			}

			var itemObj = itemCache.findItemByCode(itemCode);
			if (itemObj) {
				itemId = itemObj.getItemId();
			}

			if (settingCache.isAnyAdditionalItemDetailsEnabled()) {
				var itemStockTrackingList = (0, _SalePurchaseHelpers.getItemStockTrackingList)({
					itemId: itemId,
					itemCode: itemCode,
					nameId: nameId,
					txnType: txnType
				});
				if (itemStockTrackingList.length > 1) {
					setTimeout(function () {
						_this2.setState({
							showItemStockTrackingModal: true,
							itemStockTrackingList: itemStockTrackingList
						});
					}, 100);
					return false;
				} else if (itemStockTrackingList.length == 1) {
					var txn = itemStockTrackingList[0];
					var istId = txn.getIstId();
					var istExpiryDateObj = void 0;
					var istManufacturingDateObj = void 0;

					var istBatchNumber = txn.getIstBatchNumber() ? String(txn.getIstBatchNumber()) : '';
					var istManufacturingDate = txn.getIstManufacturingDate() ? txn.getIstManufacturingDate() : '';
					var istExpiryDate = txn.getIstExpiryDate() ? txn.getIstExpiryDate() : '';
					var istSerialNumber = txn.getIstSerialNumber() ? String(txn.getIstSerialNumber()) : '';
					var istSize = txn.getIstSize() ? String(txn.getIstSize()) : '';
					var istMrp = txn.getIstMrp() ? String(txn.getIstMrp()) : '';
					var currentQuantity = String(_MyDouble2.default.getQuantityWithDecimalWithoutColor(txn.getIstCurrentQuantity()));
					istExpiryDateObj = istExpiryDate;
					istManufacturingDateObj = istManufacturingDate;

					if (istManufacturingDate && settingCache.getManufacturingDateFormat() == StringConstant.monthYear) {
						istManufacturingDate = MyDate.getDate('m/y', istManufacturingDate);
					} else if (istManufacturingDate && settingCache.getManufacturingDateFormat() == StringConstant.dateMonthYear) {
						istManufacturingDate = MyDate.getDate('d/m/y', istManufacturingDate);
					}

					if (istExpiryDate && settingCache.getExpiryDateFormat() == StringConstant.monthYear) {
						istExpiryDate = MyDate.getDate('m/y', istExpiryDate);
					} else if (istExpiryDate && settingCache.getExpiryDateFormat() == StringConstant.dateMonthYear) {
						istExpiryDate = MyDate.getDate('d/m/y', istExpiryDate);
					}

					var selectedList = [{
						istBatchNumber: istBatchNumber,
						istExpiryDate: istExpiryDate,
						istExpiryDateObj: istExpiryDateObj,
						istManufacturingDate: istManufacturingDate,
						istManufacturingDateObj: istManufacturingDateObj,
						istSerialNumber: istSerialNumber,
						istSize: istSize,
						istMrp: istMrp,
						currentQuantity: currentQuantity,
						qty: 1,
						istId: istId,
						logic: txn
					}];

					this.onItemStockTrackingDialogClose(selectedList);
					return false;
				} else if (itemId) {
					var found = selected.find(function (item) {
						return item.itemId == itemId;
					});
					if (found) {
						found.lineItem.qty = _MyDouble2.default.convertStringToDouble(found.lineItem.qty) + 1;
					} else {
						var lineItem = getDefaultLineItem();
						lineItem.qty = 1;
						lineItem.item = {
							value: _MyDouble2.default.convertStringToDouble(itemObj.getItemId()),
							label: itemObj.getItemName(),
							purchasePrice: _MyDouble2.default.getAmountWithDecimal(itemObj.getItemPurchaseUnitPrice()),
							salePrice: _MyDouble2.default.getAmountWithDecimal(itemObj.getItemSaleUnitPrice()),
							stockQty: _MyDouble2.default.getQuantityWithDecimal(itemObj.getAvailableQuantity(), itemObj.getItemMinStockQuantity(), true),
							item: itemObj
						};
						selected.push({
							istId: null,
							itemId: itemId,
							itemCode: this.itemCode,
							lineItem: lineItem
						});
					}
					this.itemCode = '';
					this.setState({ selected: selected });
					return false;
				} else {
					this.itemCode = '';
					ToastHelper.error('Item code or serial number not assigned to any item');
				}
			} else if (itemId) {
				var _found = selected.find(function (item) {
					return item.itemId == itemId;
				});
				if (_found) {
					_found.lineItem.qty = _MyDouble2.default.convertStringToDouble(_found.lineItem.qty) + 1;
				} else {
					var _lineItem = getDefaultLineItem();
					_lineItem.qty = 1;
					_lineItem.item = {
						value: _MyDouble2.default.convertStringToDouble(itemObj.getItemId()),
						label: itemObj.getItemName(),
						purchasePrice: _MyDouble2.default.getAmountWithDecimal(itemObj.getItemPurchaseUnitPrice()),
						salePrice: _MyDouble2.default.getAmountWithDecimal(itemObj.getItemSaleUnitPrice()),
						stockQty: _MyDouble2.default.getQuantityWithDecimal(itemObj.getAvailableQuantity(), itemObj.getItemMinStockQuantity(), true),
						item: itemObj
					};
					selected.push({
						istId: null,
						itemId: itemId,
						itemCode: this.itemCode,
						lineItem: _lineItem
					});
				}
				this.itemCode = '';
				this.setState({ selected: selected });
				return false;
			} else {
				if (this.itemCode) {
					ToastHelper.error('Item code or serial number not assigned to any item');
				}
				this.itemCode = '';
			}
		}
	}, {
		key: 'onItemStockTrackingDialogClose',
		value: function onItemStockTrackingDialogClose() {
			var _this3 = this;

			var selectedList = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

			if (selectedList.length) {
				var selected = this.state.selected;
				var _props2 = this.props,
				    txnType = _props2.txnType,
				    party = _props2.party,
				    firmLogic = _props2.firmLogic,
				    placeOfSupply = _props2.placeOfSupply,
				    txnId = _props2.txnId,
				    getDefaultLineItem = _props2.getDefaultLineItem;

				var partyLogic = party && party.partyLogic ? party.partyLogic : null;
				var lineItems = (0, _SalePurchaseHelpers.addItemStockTrackingInLineItem)(getDefaultLineItem(), selectedList, firmLogic, partyLogic, txnType, txnId, placeOfSupply);
				if (lineItems.length) {
					lineItems.map(function (item) {
						item.uniqueId = (0, _SalePurchaseHelpers.getUniqueKey)();
					});
					var all = lineItems.reduce(function (selected, lineItem) {
						var found = selected.find(function (item) {
							return item.istId == lineItem.istId;
						});
						if (found) {
							found.lineItem.qty = _MyDouble2.default.convertStringToDouble(found.lineItem.qty) + _MyDouble2.default.convertStringToDouble(lineItem.qty);
						} else {
							selected.push({
								istId: lineItem.istId,
								itemId: lineItem.item.value,
								itemCode: _this3.itemCode,
								lineItem: lineItem
							});
						}
						return selected;
					}, selected);
					this.itemCode = '';
					this.setState({
						selected: all
					});
				}
			}
			this.itemCode = '';
			this.setState({ showItemStockTrackingModal: false });
		}
	}, {
		key: 'onSave',
		value: function onSave(e) {
			e.preventDefault();
			var selected = this.state.selected;
			var _props3 = this.props,
			    txnType = _props3.txnType,
			    party = _props3.party,
			    firmLogic = _props3.firmLogic,
			    placeOfSupply = _props3.placeOfSupply,
			    txnId = _props3.txnId,
			    taxInclusive = _props3.taxInclusive;

			var partyLogic = party && party.partyLogic ? party.partyLogic : null;
			var lineItems = selected.map(function (row) {
				return (0, _SalePurchaseHelpers.changeLineItem)({
					defaultLineItem: row.lineItem,
					partyLogic: partyLogic,
					itemLogic: row.lineItem.item.item,
					placeOfSupply: placeOfSupply,
					txnType: txnType,
					txnId: txnId,
					firmLogic: firmLogic,
					taxInclusive: taxInclusive
				});
			});
			this.props.onClose && this.props.onClose(lineItems);
		}
	}, {
		key: 'render',
		value: function render() {
			var _this4 = this;

			var classes = this.props.classes;
			var _state = this.state,
			    selected = _state.selected,
			    showItemStockTrackingModal = _state.showItemStockTrackingModal,
			    itemStockTrackingList = _state.itemStockTrackingList;

			var allRows = selected.map(function (item, index) {
				return _react2.default.createElement(
					'div',
					{
						key: item.itemId + '_' + index,
						className: (0, _classnames2.default)(classes.item, 'd-flex')
					},
					_react2.default.createElement(
						'div',
						{ className: (0, _classnames2.default)(classes.column, classes.codeColumn) },
						_react2.default.createElement('input', {
							value: item.itemCode,
							onBlur: function onBlur(e) {
								return _this4.onCodeBlur(e, index);
							},
							onChange: function onChange(e) {
								return _this4.onCodeChange(e, index);
							},
							type: 'text',
							className: (0, _classnames2.default)(classes.input, classes.codeInput)
						})
					),
					_react2.default.createElement(
						'div',
						{ className: (0, _classnames2.default)(classes.column, classes.nameColumn) },
						_react2.default.createElement('input', {
							value: item.lineItem.item.label,
							disabled: true,
							type: 'text',
							className: (0, _classnames2.default)(classes.input, classes.nameInput)
						})
					),
					_react2.default.createElement(
						'div',
						{ className: (0, _classnames2.default)(classes.column) },
						_react2.default.createElement('input', {
							value: item.lineItem.qty || '',
							onChange: function onChange(e) {
								return _this4.onQtyChange(e, index);
							},
							type: 'text',
							className: classes.input
						})
					)
				);
			});
			return _react2.default.createElement(
				_styles.MuiThemeProvider,
				{ theme: theme },
				_react2.default.createElement(
					_Dialog2.default,
					{
						maxWidth: false,
						disableEnforceFocus: true,
						classes: {
							paperWidthSm: classes.dialogContainer,
							paper: classes.borderRadius
						},
						open: true,
						disableBackdropClick: true,
						onClose: this.onClose
					},
					_react2.default.createElement(
						'div',
						{ className: classes.container },
						showItemStockTrackingModal && _react2.default.createElement(_SelectBatchesModal2.default, {
							itemStockTrackingList: itemStockTrackingList,
							onClose: this.onItemStockTrackingDialogClose
						}),
						_react2.default.createElement(
							'div',
							{
								className: (0, _classnames2.default)(classes.header, 'd-flex justify-content-between')
							},
							_react2.default.createElement(
								'div',
								{ className: 'd-flex align-items-center' },
								_react2.default.createElement(
									'div',
									{ className: classes.title },
									'Scan Items'
								)
							),
							_react2.default.createElement(
								'div',
								{ onClick: this.onClose, className: classes.closeIconWrapper },
								_react2.default.createElement(_Close2.default, { className: classes.closeIcon })
							)
						),
						_react2.default.createElement(
							'div',
							{ className: classes.trackingList },
							_react2.default.createElement(
								'div',
								{ className: (0, _classnames2.default)(classes.listHeader, 'd-flex') },
								_react2.default.createElement(
									'div',
									{ className: (0, _classnames2.default)(classes.column, classes.codeColumn) },
									'Code'
								),
								_react2.default.createElement(
									'div',
									{ className: (0, _classnames2.default)(classes.column, classes.nameColumn) },
									'Name'
								),
								_react2.default.createElement(
									'div',
									{ className: (0, _classnames2.default)(classes.column) },
									'QTY'
								)
							),
							allRows,
							_react2.default.createElement(
								'div',
								{ className: (0, _classnames2.default)(classes.item, 'd-flex') },
								_react2.default.createElement(
									'div',
									{ className: (0, _classnames2.default)(classes.column, classes.codeColumn) },
									_react2.default.createElement('input', {
										onKeyDown: function onKeyDown(e) {
											return _this4.onCodeBlur(e, null, true, true);
										},
										onBlur: function onBlur(e) {
											return _this4.onCodeBlur(e, null, true, false);
										},
										type: 'text',
										autoFocus: true,
										className: (0, _classnames2.default)(classes.input, classes.codeInput)
									})
								),
								_react2.default.createElement(
									'div',
									{ className: (0, _classnames2.default)(classes.column, classes.nameColumn) },
									_react2.default.createElement('input', {
										disabled: true,
										type: 'text',
										className: (0, _classnames2.default)(classes.input, classes.nameInput)
									})
								),
								_react2.default.createElement(
									'div',
									{ className: (0, _classnames2.default)(classes.column) },
									_react2.default.createElement('input', {
										disabled: true,
										type: 'text',
										className: classes.input
									})
								)
							)
						),
						_react2.default.createElement(
							'div',
							{ className: classes.footer },
							_react2.default.createElement(
								_Button2.default,
								{
									onClick: this.onSave,
									size: 'small',
									variant: 'contained',
									color: 'primary'
								},
								'SAVE'
							)
						)
					)
				)
			);
		}
	}]);
	return BarcodeModal;
}(_react2.default.Component);

BarcodeModal.propTypes = {
	classes: _propTypes2.default.object,
	onClose: _propTypes2.default.func,
	txnType: _propTypes2.default.number,
	party: _propTypes2.default.object,
	getDefaultLineItem: _propTypes2.default.func,
	firmLogic: _propTypes2.default.object,
	placeOfSupply: _propTypes2.default.string,
	txnId: _propTypes2.default.number,
	taxInclusive: _propTypes2.default.number
};
exports.default = (0, _styles.withStyles)(styles)(BarcodeModal);