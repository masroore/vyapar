Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
	paymentLinkReset: {
		height: '32px',
		padding: '7px',
		backgroundColor: 'white',
		width: '33px',
		borderRadius: '43px',
		boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
		border: '#000000 blur 6px'
	},
	linkPaymentResetText: {
		paddingTop: '7px',
		paddingLeft: '2px',
		color: 'rgba(0, 0, 0, 0.54)'
	}
};

var LinkPaymentReset = function (_React$Component) {
	(0, _inherits3.default)(LinkPaymentReset, _React$Component);

	function LinkPaymentReset(props) {
		(0, _classCallCheck3.default)(this, LinkPaymentReset);
		return (0, _possibleConstructorReturn3.default)(this, (LinkPaymentReset.__proto__ || (0, _getPrototypeOf2.default)(LinkPaymentReset)).call(this, props));
	}

	(0, _createClass3.default)(LinkPaymentReset, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			setTimeout(function () {
				$('.reset-link-payment-button').tooltipster({
					theme: 'tooltipster-noir',
					maxWidth: 300
				});
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var classes = this.props.classes;

			return _react2.default.createElement(
				'div',
				null,
				_react2.default.createElement(
					'button',
					{
						title: 'Clear the payment links and start fresh.',
						type: 'button',
						id: 'paymentLinkReset',
						className: (0, _classnames2.default)(classes.paymentLinkReset, 'reset-link-payment-button'),
						onClick: this.props.reset
					},
					_react2.default.createElement('img', { src: this.props.img })
				),
				_react2.default.createElement(
					'div',
					{ className: classes.linkPaymentResetText },
					'RESET'
				)
			);
		}
	}]);
	return LinkPaymentReset;
}(_react2.default.Component);

exports.default = (0, _styles.withStyles)(styles)(LinkPaymentReset);