Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _throttleDebounce = require('throttle-debounce');

var _Fab = require('@material-ui/core/Fab');

var _Fab2 = _interopRequireDefault(_Fab);

var _Search = require('@material-ui/icons/Search');

var _Search2 = _interopRequireDefault(_Search);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SearchBox = function (_React$Component) {
	(0, _inherits3.default)(SearchBox, _React$Component);

	function SearchBox(props) {
		(0, _classCallCheck3.default)(this, SearchBox);

		var _this = (0, _possibleConstructorReturn3.default)(this, (SearchBox.__proto__ || (0, _getPrototypeOf2.default)(SearchBox)).call(this, props));

		_this.handleBlur = function () {
			if (!_this.state.searchText) {
				_this.collapseSearch();
			}
		};

		_this.handleCloseIconClick = function () {
			_this.filter('', true);
			_this.collapseSearch();
		};

		_this.state = {
			searchText: ''
		};
		_this.searchInputRef = _react2.default.createRef();
		if (_this.props.throttle) {
			_this.search = (0, _throttleDebounce.debounce)(_this.props.throttle, _this.props.filter);
		} else {
			_this.search = _this.props.filter;
		}

		if (_this.props.minLength) {
			_this.minLength = _this.props.minLength;
		} else {
			_this.minLength = 0;
		}
		return _this;
	}

	(0, _createClass3.default)(SearchBox, [{
		key: 'filter',
		value: function filter(searchText) {
			var reset = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

			this.setState({
				searchText: searchText
			});
			if (reset) {
				this.search(searchText, true);
				return false;
			}
			if (searchText.length >= this.minLength) {
				this.search(searchText, true);
			}
		}
	}, {
		key: 'collapseSearch',
		value: function collapseSearch() {
			var onInputBlur = this.props.onInputBlur;

			typeof onInputBlur == 'function' && onInputBlur();
		}
	}, {
		key: 'componentDidUpdate',
		value: function componentDidUpdate(prevProps) {
			if (!this.props.collapsed && prevProps.collapsed !== this.props.collapsed) {
				this.searchInputRef.current && this.searchInputRef.current.focus();
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var _this2 = this;

			var _props = this.props,
			    collapsed = _props.collapsed,
			    collapsedTextBoxClass = _props.collapsedTextBoxClass,
			    onCollapsedClick = _props.onCollapsedClick;

			return collapsed ? _react2.default.createElement(
				_Fab2.default,
				{ size: 'small', style: { marginLeft: '.5rem', height: '2rem', width: '2.25rem', boxShadow: 'none' }, onClick: onCollapsedClick },
				_react2.default.createElement(_Search2.default, { fontSize: 'small' })
			) : _react2.default.createElement(
				'div',
				{ className: (0, _classnames2.default)('ml-10 mr-10', !collapsed ? collapsedTextBoxClass : '') },
				_react2.default.createElement(
					'div',
					{ className: 'gridQuickFilter' },
					_react2.default.createElement('input', { value: this.state.searchText, onChange: function onChange(e) {
							return _this2.filter(e.target.value);
						}, onBlur: this.handleBlur, ref: this.searchInputRef }),
					this.state.searchText && _react2.default.createElement(
						'div',
						{ onClick: this.handleCloseIconClick, className: 'closeIcon' },
						'\u2716'
					)
				)
			);
		}
	}]);
	return SearchBox;
}(_react2.default.Component);

exports.default = SearchBox;