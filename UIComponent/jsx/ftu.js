Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FTU = function (_React$Component) {
	(0, _inherits3.default)(FTU, _React$Component);

	function FTU() {
		(0, _classCallCheck3.default)(this, FTU);
		return (0, _possibleConstructorReturn3.default)(this, (FTU.__proto__ || (0, _getPrototypeOf2.default)(FTU)).apply(this, arguments));
	}

	(0, _createClass3.default)(FTU, [{
		key: 'handleClick',
		value: function handleClick() {
			MyAnalytics.pushEvent('Add Sale Txn Open from FTU');
			var MountComponent = require('../jsx/MountComponent').default;
			var Component = require('../jsx/SalePurchaseContainer').default;
			var TxnTypeConstant = require('../../Constants/TxnTypeConstant');
			MountComponent(Component, document.querySelector('#salePurchaseContainer'), {
				txnType: TxnTypeConstant.TXN_TYPE_SALE
			});
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			window.onResume = this.onResume.bind(this);
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			delete window.onResume;
		}
	}, {
		key: 'onResume',
		value: function onResume() {
			setTimeout(function () {
				var FTUHelper = require('../../Utilities/FTUHelper');
				try {
					if ($('.ftu').length && FTUHelper.isFirstSaleTxnCreated()) {
						var MountComponent = require('../jsx/MountComponent').default;
						var Dashboard = require('./Dashboard/Dashboard').default;
						MountComponent(Dashboard, document.querySelector('#defaultPage'));
					}
				} catch (err) {}
			});
		}
	}, {
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				'div',
				{ className: 'ftu' },
				_react2.default.createElement(
					'div',
					{ className: 'ftuHeader' },
					_react2.default.createElement(
						'p',
						null,
						"Let's create your first invoice. Click on the 'Add Sale' button at the bottom of your screen."
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'ftuBody' },
					_react2.default.createElement('div', { className: 'ftuItem' }),
					_react2.default.createElement('div', { className: 'ftuItem' }),
					_react2.default.createElement('div', { className: 'ftuItem' })
				),
				_react2.default.createElement(
					'button',
					{ className: 'ftuButton', onClick: this.handleClick },
					_react2.default.createElement(
						'span',
						null,
						'+ Add Sale Invoice'
					)
				)
			);
		}
	}]);
	return FTU;
}(_react2.default.Component);

exports.default = FTU;