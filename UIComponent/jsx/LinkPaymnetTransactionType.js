Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _TxnTypeConstant = require('../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _styles = require('@material-ui/core/styles');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
	linkPaymentSelect: {
		position: 'absolute',
		width: 215,
		padding: 6,
		top: 150,
		margin: '-15px 0'
	}
};

var PaymentLinkAutoLink = function (_React$Component) {
	(0, _inherits3.default)(PaymentLinkAutoLink, _React$Component);

	function PaymentLinkAutoLink(props) {
		(0, _classCallCheck3.default)(this, PaymentLinkAutoLink);

		var _this = (0, _possibleConstructorReturn3.default)(this, (PaymentLinkAutoLink.__proto__ || (0, _getPrototypeOf2.default)(PaymentLinkAutoLink)).call(this, props));

		_this.state = {};
		var txnType = props.txnType;
		switch (txnType) {
			case _TxnTypeConstant2.default.TXN_TYPE_SALE:
			case _TxnTypeConstant2.default.TXN_TYPE_CASHOUT:
			case _TxnTypeConstant2.default.TXN_TYPE_LINKED_CASHOUT_OPENINGBALANCE:
			case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN:
				_this.transactions = [{
					name: 'All transactions',
					type: 0
				}, {
					name: 'Purchase',
					type: _TxnTypeConstant2.default.TXN_TYPE_PURCHASE
				}, {
					name: 'Payment-In',
					type: _TxnTypeConstant2.default.TXN_TYPE_CASHIN
				}, {
					name: 'Payable Opening Bal',
					type: _TxnTypeConstant2.default.TXN_TYPE_POPENBALANCE
				}, {
					name: 'Credit Note',
					type: _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN
				}];
				break;
			case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE:
			case _TxnTypeConstant2.default.TXN_TYPE_CASHIN:
			case _TxnTypeConstant2.default.TXN_TYPE_LINKED_CASHIN_OPENINGBALANCE:
			case _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN:
				_this.transactions = [{
					name: 'All transactions',
					type: 0
				}, {
					name: 'Sale',
					type: _TxnTypeConstant2.default.TXN_TYPE_SALE
				}, {
					name: 'Debit Note',
					type: _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN
				}, {
					name: 'Payment-Out',
					type: _TxnTypeConstant2.default.TXN_TYPE_CASHOUT
				}, {
					name: 'Receivable Opening Bal',
					type: _TxnTypeConstant2.default.TXN_TYPE_ROPENBALANCE
				}];
				break;
		}
		_this.changeTxnType = _this.changeTxnType.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(PaymentLinkAutoLink, [{
		key: 'changeTxnType',
		value: function changeTxnType(e) {
			this.props.changeTxnType(e.target.value);
		}
	}, {
		key: 'render',
		value: function render() {
			var classes = this.props.classes;

			return _react2.default.createElement(
				'select',
				{
					value: this.state.transaction,
					onChange: this.changeTxnType,
					className: classes.linkPaymentSelect
				},
				this.transactions.map(function (txn) {
					return _react2.default.createElement(
						'option',
						{ key: txn.type, value: txn.type },
						txn.name
					);
				})
			);
		}
	}]);
	return PaymentLinkAutoLink;
}(_react2.default.Component);

exports.default = (0, _styles.withStyles)(styles)(PaymentLinkAutoLink);