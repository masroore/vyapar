Object.defineProperty(exports, "__esModule", {
	value: true
});

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _ItemCategoryCache = require('../../Cache/ItemCategoryCache');

var _ItemCategoryCache2 = _interopRequireDefault(_ItemCategoryCache);

var _ErrorCode = require('../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Items = function (_React$Component) {
	(0, _inherits3.default)(Items, _React$Component);

	function Items(props) {
		(0, _classCallCheck3.default)(this, Items);

		var _this = (0, _possibleConstructorReturn3.default)(this, (Items.__proto__ || (0, _getPrototypeOf2.default)(Items)).call(this, props));

		_this.selectFirstRow = function () {
			setTimeout(function () {
				var firstColumn = document.querySelector('.left-column .ag-row-selected .ag-cell[col-id="categoryName"]');
				firstColumn && firstColumn.focus();
			}, 100);
		};

		_this.getApi = function (params) {
			_this.api = params.api;
			_this.api.setRowData(_this.props.items);
		};

		_this.columnDefs = [{ field: 'categoryName', headerName: 'CATEGORY', width: 200 }, {
			field: 'memberCount',
			headerName: 'ITEM',
			width: 90,
			cellClass: 'alignRight',
			headerClass: 'alignRight'
		}, {
			field: 'categoryId',
			headerName: '',
			width: 30,
			getQuickFilterText: function getQuickFilterText() {
				return '';
			},
			suppressSorting: true,
			suppressFilter: true,
			cellRenderer: function cellRenderer() {
				return '<div class="contextMenuDots"></div>';
			}
		}];
		_this.openItem = _this.openItem.bind(_this);
		_this.deleteItem = _this.deleteItem.bind(_this);

		_this.contextMenu = [{
			title: 'View/Edit',
			cmd: _this.openItem,
			visible: true,
			id: 'edit'
		}, {
			title: 'Delete',
			cmd: _this.deleteItem,
			visible: true,
			id: 'delete'
		}];
		_this.state = {
			contextMenu: _this.contextMenu
		};
		return _this;
	}

	(0, _createClass3.default)(Items, [{
		key: 'openItem',
		value: function openItem(logic) {
			var categoryId = logic.categoryId || logic.data.categoryId;
			if (categoryId == '1') {
				ToastHelper.info(_ErrorCode2.default.ERROR_GENERALCATEGORY_NOT_EDITABLE);
				return;
			}
			this.props.editCategory(logic);
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.selectFirstRow();
		}
	}, {
		key: 'deleteItem',
		value: function deleteItem(logic) {
			var cache = new _ItemCategoryCache2.default();
			var itemCategory = cache.getItemCategoryObjectById(logic.getCategoryId());
			if (logic.getCategoryId() == '1') {
				ToastHelper.info(_ErrorCode2.default.ERROR_GENERALCATEGORY_NOT_EDITABLE);
				return;
			}
			if (itemCategory) {
				var statusCode = itemCategory.deleteItemCategory();
				if (statusCode) {
					if (statusCode == _ErrorCode2.default.ERROR_ITEMCATEGORY_DELETE_SUCCESS) {
						ToastHelper.success(statusCode);
						cache.reloadItemCategory();
						if (window.onResume) window.onResume();
					} else {
						ToastHelper.error(statusCode);
					}
				} else {
					return false;
				}
			}
		}
	}, {
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(nextProps) {
			var _this2 = this;

			if (this.api) {
				this.api.setRowData(nextProps.items);
				setTimeout(function () {
					if (_this2.api.getSelectedRows().length === 0) {
						var selected = _this2.api.getDisplayedRowAtIndex(0);
						if (selected) {
							selected.setSelected(true);
						}
					}
				});
			}
			return false;
		}
	}, {
		key: 'render',
		value: function render() {
			var gridOptions = (0, _defineProperty3.default)({
				enableFilter: false,
				enableSorting: true,
				rowSelection: 'single',
				columnDefs: this.columnDefs,
				rowData: [],
				deltaRowDataMode: true,
				getRowNodeId: function getRowNodeId(data) {
					return data.getCategoryId();
				},
				headerHeight: 40,
				rowHeight: 40,
				onRowDoubleClicked: this.openItem,
				onRowSelected: this.props.onItemSelected
			}, 'onRowDoubleClicked', this.openItem);
			return _react2.default.createElement(
				'div',
				{ className: 'gridContainer d-flex' },
				_react2.default.createElement(
					'div',
					{ id: 'delDialog2', className: 'hide' },
					_react2.default.createElement(
						'b',
						null,
						'This Item will be Deleted.'
					)
				),
				_react2.default.createElement(_Grid2.default, {
					gridKey: 'categoryListGridKey',
					classes: 'alternateRowColor gridRowHeight40 noBorder',
					contextMenu: this.contextMenu,
					selectFirstRow: true,
					height: '100%',
					gridOptions: gridOptions,
					getApi: this.getApi
				})
			);
		}
	}]);
	return Items;
}(_react2.default.Component);

exports.default = Items;