Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Menu = require('@material-ui/core/Menu');

var _Menu2 = _interopRequireDefault(_Menu);

var _MenuItem = require('@material-ui/core/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _Share = require('@material-ui/icons/Share');

var _Share2 = _interopRequireDefault(_Share);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SaleAgeingDropdownMenuForRemind = function (_React$Component) {
  (0, _inherits3.default)(SaleAgeingDropdownMenuForRemind, _React$Component);

  function SaleAgeingDropdownMenuForRemind(props) {
    (0, _classCallCheck3.default)(this, SaleAgeingDropdownMenuForRemind);

    var _this = (0, _possibleConstructorReturn3.default)(this, (SaleAgeingDropdownMenuForRemind.__proto__ || (0, _getPrototypeOf2.default)(SaleAgeingDropdownMenuForRemind)).call(this, props));

    _this.handleClick = function (event) {
      _this.setState({ anchorEl: event.currentTarget });
    };

    _this.handleClose = function () {
      _this.setState({ anchorEl: null });
    };

    _this.state = {
      anchorEl: null
    };
    _this.data = null;
    return _this;
  }

  (0, _createClass3.default)(SaleAgeingDropdownMenuForRemind, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var options = this.props.options;
      var anchorEl = this.state.anchorEl;


      return _react2.default.createElement(
        'div',
        { className: '' },
        _react2.default.createElement(
          _Button2.default,
          { variant: 'outlined', size: 'small', style: { color: '#097AA8', paddingRight: 8, paddingLeft: 8, paddingBottom: 2, paddingTop: 2 }, onClick: this.handleClick },
          _react2.default.createElement(_Share2.default, { style: { color: '#097AA8', fontSize: 20 } }),
          _react2.default.createElement(
            'span',
            { className: 'marginleft5' },
            'Remind'
          )
        ),
        _react2.default.createElement(
          _Menu2.default,
          {
            id: 'simple-menu',
            anchorEl: anchorEl,
            open: Boolean(anchorEl),
            onClose: this.handleClose
          },
          _react2.default.createElement(
            _MenuItem2.default,
            { onClick: function onClick() {
                return options.sendReminderOnWhatsApp(_this2);
              } },
            'WhatsApp'
          ),
          _react2.default.createElement(
            _MenuItem2.default,
            { onClick: function onClick() {
                return options.sendReminderOnSMS(_this2);
              } },
            'SMS'
          ),
          _react2.default.createElement(
            _MenuItem2.default,
            { onClick: function onClick() {
                return options.sendReminderOnMail(_this2);
              } },
            'Email'
          )
        )
      );
    }
  }]);
  return SaleAgeingDropdownMenuForRemind;
}(_react2.default.Component);

exports.default = SaleAgeingDropdownMenuForRemind;