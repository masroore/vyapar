Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LinkPaymentAutoLink = function (_React$Component) {
	(0, _inherits3.default)(LinkPaymentAutoLink, _React$Component);

	function LinkPaymentAutoLink() {
		(0, _classCallCheck3.default)(this, LinkPaymentAutoLink);
		return (0, _possibleConstructorReturn3.default)(this, (LinkPaymentAutoLink.__proto__ || (0, _getPrototypeOf2.default)(LinkPaymentAutoLink)).apply(this, arguments));
	}

	(0, _createClass3.default)(LinkPaymentAutoLink, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			setTimeout(function () {
				$('.paymentLinkToolTip').tooltipster({
					theme: 'tooltipster-noir',
					maxWidth: 300
				});
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _props = this.props,
			    autoLink = _props.autoLink,
			    title = _props.title;

			return _react2.default.createElement(
				'div',
				null,
				_react2.default.createElement(
					'button',
					{
						type: 'button',
						id: 'autoLink',
						onClick: autoLink,
						className: 'terminalButton ripple '
					},
					'AUTO LINK'
				),
				_react2.default.createElement(
					'span',
					{ title: title, className: 'paymentLinkToolTip' },
					'?'
				)
			);
		}
	}]);
	return LinkPaymentAutoLink;
}(_react2.default.Component);

;
exports.default = LinkPaymentAutoLink;