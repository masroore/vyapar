Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = require('babel-runtime/helpers/objectWithoutProperties');

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactAutosuggest = require('react-autosuggest');

var _reactAutosuggest2 = _interopRequireDefault(_reactAutosuggest);

var _CustomTextInput = require('./UIControls/CustomTextInput');

var _CustomTextInput2 = _interopRequireDefault(_CustomTextInput);

var _BusinessCategories = require('../../Constants/BusinessCategories');

var _BusinessCategories2 = _interopRequireDefault(_BusinessCategories);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _styles = require('@material-ui/core/styles');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles() {
	return {
		suggestion: {
			fontSize: '13px',
			color: '#A6A6AE',
			padding: '4px 12px'
		},
		ddContainer: {
			position: 'relative'
		},
		suggestionHighLighted: {
			background: '#E7F3FF',
			borderRadius: '0px 0px 3px 3px'
		},
		container: {
			position: 'absolute',
			maxHeight: 120,
			width: '268px',
			overflowY: 'auto',
			overflowX: 'hidden',
			background: '#fff',
			zIndex: 9999,
			borderTop: 0,
			border: '1px solid #E0E0E3',
			borderRadius: '4px',
			fontFamily: 'roboto',
			top: 38
		}
	};
};

var businessCategoryList = _BusinessCategories2.default.getBusinessCategories();
var getSuggestions = function getSuggestions(value) {
	var inputValue = value.trim().toLowerCase();
	var inputLength = inputValue.length;
	return inputLength === 0 ? businessCategoryList : businessCategoryList.filter(function (category) {
		return category.toLowerCase().indexOf(inputValue) != -1;
	});
};
var getSuggestionValue = function getSuggestionValue(suggestion) {
	return suggestion;
};
var renderSuggestion = function renderSuggestion(suggestion) {
	return _react2.default.createElement(
		'div',
		{ className: 'suggestion' },
		suggestion
	);
};

var BusinessCategoryDd = function (_React$Component) {
	(0, _inherits3.default)(BusinessCategoryDd, _React$Component);

	function BusinessCategoryDd(props) {
		(0, _classCallCheck3.default)(this, BusinessCategoryDd);

		var _this = (0, _possibleConstructorReturn3.default)(this, (BusinessCategoryDd.__proto__ || (0, _getPrototypeOf2.default)(BusinessCategoryDd)).call(this, props));

		_this.onChange = function (event, _ref) {
			var newValue = _ref.newValue;

			_this.props.onChange(newValue);
		};

		_this.onSuggestionsFetchRequested = function (_ref2) {
			var value = _ref2.value;

			_this.setState({
				suggestions: getSuggestions(value)
			});
		};

		_this.renderInputComponent = function (inputProps) {
			var className = inputProps.className,
			    ref = inputProps.ref,
			    rest = (0, _objectWithoutProperties3.default)(inputProps, ['className', 'ref']);
			var classes = _this.props.classes;

			return _react2.default.createElement(
				'div',
				null,
				_react2.default.createElement(_CustomTextInput2.default, (0, _extends3.default)({}, rest, {
					inputRef: ref,
					inputClass: (0, _classnames2.default)(className)
				}))
			);
		};

		_this.onSuggestionsClearRequested = function () {
			_this.setState({
				suggestions: businessCategoryList
			});
		};

		_this.state = {
			suggestions: businessCategoryList
		};
		return _this;
	}

	(0, _createClass3.default)(BusinessCategoryDd, [{
		key: 'shouldRenderSuggestions',
		value: function shouldRenderSuggestions(value) {
			return value.trim().length > 2;
		}

		// Autosuggest will call this function every time you need to clear suggestions.

	}, {
		key: 'render',
		value: function render() {
			var suggestions = this.state.suggestions;
			var classes = this.props.classes;
			// Autosuggest will pass through all these props to the input.

			var inputProps = (0, _extends3.default)({}, this.props.inputProps, {
				onChange: this.onChange
			});

			// Finally, render it!
			return _react2.default.createElement(
				'div',
				{ className: classes.ddContainer },
				_react2.default.createElement(_reactAutosuggest2.default, {
					suggestions: suggestions,
					onSuggestionsFetchRequested: this.onSuggestionsFetchRequested,
					onSuggestionsClearRequested: this.onSuggestionsClearRequested,
					getSuggestionValue: getSuggestionValue,
					renderSuggestion: renderSuggestion,
					renderInputComponent: this.renderInputComponent,
					inputProps: inputProps,
					renderSuggestionsContainer: function renderSuggestionsContainer(options) {
						return _react2.default.createElement(
							'div',
							(0, _extends3.default)({}, options.containerProps, { square: true }),
							options.children
						);
					},
					shouldRenderSuggestions: this.shouldRenderSuggestions,
					theme: {
						containerOpen: 'react-autosuggest__container--open',
						// input: classes.input,
						inputOpen: 'react-autosuggest__input--open',
						inputFocused: 'react-autosuggest__input--focused',
						suggestionsContainer: 'react-autosuggest__suggestions-container',
						suggestionsContainerOpen: classes.container,
						suggestionsList: '',
						suggestion: classes.suggestion,
						suggestionFirst: 'react-autosuggest__suggestion--first',
						suggestionHighlighted: classes.suggestionHighLighted,
						sectionContainer: 'react-autosuggest__section-container',
						sectionContainerFirst: 'react-autosuggest__section-container--first',
						sectionTitle: 'react-autosuggest__section-title'
					}
				})
			);
		}
	}]);
	return BusinessCategoryDd;
}(_react2.default.Component);

BusinessCategoryDd.propTypes = {
	classes: _propTypes2.default.object,
	inputProps: _propTypes2.default.object,
	onChange: _propTypes2.default.func
};

exports.default = (0, _styles.withStyles)(styles)(BusinessCategoryDd);