Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _AddUnitConversionModal = require('./AddUnitConversionModal');

var _AddUnitConversionModal2 = _interopRequireDefault(_AddUnitConversionModal);

var _ButtonDropdown = require('./ButtonDropdown');

var _ButtonDropdown2 = _interopRequireDefault(_ButtonDropdown);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AddConversionButton = function (_React$Component) {
	(0, _inherits3.default)(AddConversionButton, _React$Component);

	function AddConversionButton() {
		var _ref;

		var _temp, _this, _ret;

		(0, _classCallCheck3.default)(this, AddConversionButton);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = AddConversionButton.__proto__ || (0, _getPrototypeOf2.default)(AddConversionButton)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
			showDialog: false
		}, _this.onClick = function () {
			MyAnalytics.pushEvent('Set New Unit Conversion Open');
			_this.setState({ showDialog: true });
		}, _this.onClose = function () {
			window.onResume && window.onResume();
			_this.setState({ showDialog: false });
		}, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
	}

	(0, _createClass3.default)(AddConversionButton, [{
		key: 'getMenuOptions',
		value: function getMenuOptions() {
			var menuOptions = [];
			menuOptions.push({
				buttonText: 'Add Conversion',
				onClick: this.onClick
			});
			return menuOptions;
		}
	}, {
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				'div',
				null,
				this.state.showDialog && _react2.default.createElement(_AddUnitConversionModal2.default, {
					isOpen: this.state.showDialog,
					onClose: this.onClose,
					selected: this.props.baseUnit,
					onSave: this.onClose }),
				_react2.default.createElement(_ButtonDropdown2.default, {
					menuOptions: this.getMenuOptions(),
					color: 'secondary',
					classNames: 'floatRight'
				})
			);
		}
	}]);
	return AddConversionButton;
}(_react2.default.Component);

exports.default = AddConversionButton;