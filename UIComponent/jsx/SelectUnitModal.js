Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Modal = require('./Modal');

var _Modal2 = _interopRequireDefault(_Modal);

var _ItemUnitCache = require('../../Cache/ItemUnitCache');

var _ItemUnitCache2 = _interopRequireDefault(_ItemUnitCache);

var _ItemUnitMappingCache = require('../../Cache/ItemUnitMappingCache.js');

var _ItemUnitMappingCache2 = _interopRequireDefault(_ItemUnitMappingCache);

var _Radio = require('@material-ui/core/Radio');

var _Radio2 = _interopRequireDefault(_Radio);

var _ItemunitMapping = require('../../BizLogic/ItemunitMapping');

var _ItemunitMapping2 = _interopRequireDefault(_ItemunitMapping);

var _ErrorCode = require('../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SelectUnitModal = function (_React$Component) {
	(0, _inherits3.default)(SelectUnitModal, _React$Component);

	function SelectUnitModal(props) {
		(0, _classCallCheck3.default)(this, SelectUnitModal);

		var _this = (0, _possibleConstructorReturn3.default)(this, (SelectUnitModal.__proto__ || (0, _getPrototypeOf2.default)(SelectUnitModal)).call(this, props));

		_this.state = {
			baseUnit: _this.props.baseUnit || 0,
			secondaryUnit: _this.props.secondaryUnit || 0,
			mappings: [],
			selectedUnit: _this.props.selectedUnit || -1,
			showMappings: false,
			conversionRate: 0
		};
		_this.onClose = _this.onClose.bind(_this);
		_this.changeBaseUnit = _this.changeBaseUnit.bind(_this);
		_this.changeSecondaryUnit = _this.changeSecondaryUnit.bind(_this);
		_this.setConversionRate = _this.setConversionRate.bind(_this);
		_this.handleMapping = _this.handleMapping.bind(_this);
		_this.onSave = _this.onSave.bind(_this);
		_this.getMappings = _this.getMappings.bind(_this);
		_this.addNewUnitMapping = _this.addNewUnitMapping.bind(_this);

		_this.itemUnitCache = new _ItemUnitCache2.default();
		_this.baseUnits = _this.itemUnitCache.getAllItemUnits();
		_this.unitMappings = [];
		return _this;
	}

	(0, _createClass3.default)(SelectUnitModal, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			if (this.state.baseUnit && this.state.secondaryUnit) {
				var mappings = this.getMappings(this.state.baseUnit, this.state.secondaryUnit);
				var showMappings = true;
				this.setState({
					mappings: mappings,
					showMappings: showMappings
				});
			}
		}
	}, {
		key: 'onClose',
		value: function onClose() {
			this.props.onClose && this.props.onClose();
		}
	}, {
		key: 'changeBaseUnit',
		value: function changeBaseUnit(e) {
			var baseUnit = e.target.value;
			var mappings = [];
			var showMappings = false;
			if (this.state.secondaryUnit !== baseUnit && baseUnit != 0 && this.state.secondaryUnit != 0) {
				mappings = this.getMappings(baseUnit, this.state.secondaryUnit);
				showMappings = true;
			}
			this.setState({
				baseUnit: baseUnit,
				mappings: mappings,
				showMappings: showMappings
			});
		}
	}, {
		key: 'changeSecondaryUnit',
		value: function changeSecondaryUnit(e) {
			var secondaryUnit = e.target.value;
			var mappings = [];
			var showMappings = false;
			if (this.state.baseUnit == secondaryUnit && secondaryUnit != 0) {
				ToastHelper.error('Base Unit and Secondary Unit can not be same.');
			}

			if (this.state.baseUnit !== secondaryUnit && secondaryUnit != 0 && this.state.baseUnit != 0) {
				mappings = this.getMappings(this.state.baseUnit, secondaryUnit);
				showMappings = true;
			}
			this.setState({
				secondaryUnit: secondaryUnit,
				showMappings: showMappings,
				mappings: mappings
			});
		}
	}, {
		key: 'getMappings',
		value: function getMappings(baseUnit, secondaryUnit) {
			var itemUnitMappingCache = new _ItemUnitMappingCache2.default();
			return itemUnitMappingCache.getMappingFromBaseAndSecondaryUnitIds(baseUnit, secondaryUnit);
		}
	}, {
		key: 'onSave',
		value: function onSave() {
			var _this2 = this;

			if (this.props.allowBlank) {
				// check if unit can be removed from item => for edit
				var _state = this.state,
				    baseUnit = _state.baseUnit,
				    secondaryUnit = _state.secondaryUnit;

				if (baseUnit == 0 && secondaryUnit == 0) {
					this.props.onSave({});
					return false;
				} else if (baseUnit != 0 && secondaryUnit == 0) {
					this.props.onSave({ baseUnit: baseUnit });
					return false;
				} else if (baseUnit == 0 && secondaryUnit != 0) {
					ToastHelper.info('Please select base Unit.');
					return false;
				}
			} else {
				// check if both None
				if (this.state.baseUnit == 0) {
					ToastHelper.info('Please select base Unit.');
					return false;
				}
				// check if both same
				if (this.state.baseUnit == this.state.secondaryUnit) {
					ToastHelper.info('Please base and secondary Unit can not be same.');
					return false;
				}
			}
			// check if first selected
			if (this.state.selectedUnit == -1 && this.state.baseUnit !== 0 && this.state.secondaryUnit !== 0) {
				// validate conversion rate. must be greater than zero if new
				if (this.state.conversionRate <= 0) {
					ToastHelper.info('Please enter conversion rate.');
					return false;
				}
				// check if mapping already exists -> if yes -> use it OR create new mapping and use it
				var available = this.state.mappings.find(function (mapping) {
					return _this2.state.conversionRate == Number(mapping.getConversionRate());
				});
				if (available) {
					this.props.onSave({
						selectedMappingId: available.getMappingId()
					});
					return false;
				}
				// if not exists, create new
				return this.addNewUnitMapping();
			}
			this.props.onSave({
				baseUnit: this.state.baseUnit,
				secondaryUnit: this.state.secondaryUnit,
				selectedMappingId: this.state.selectedUnit === -1 ? null : this.state.selectedUnit
			});
		}
	}, {
		key: 'addNewUnitMapping',
		value: function addNewUnitMapping() {
			var itemUnitMapping = new _ItemunitMapping2.default();
			var statusCode = itemUnitMapping.addNewUnitMapping(this.state.baseUnit, this.state.secondaryUnit, this.state.conversionRate);

			if (statusCode == _ErrorCode2.default.ERROR_UNIT_MAPPING_SAVE_SUCCESS) {
				ToastHelper.success(statusCode);
				var itemUnitMappingCache = new _ItemUnitMappingCache2.default();
				var mappings = itemUnitMappingCache.getMappingIdFromBaseAndSecondaryUnitIds(this.state.baseUnit, this.state.secondaryUnit, this.state.conversionRate);
				this.props.onSave({ selectedMappingId: mappings.getMappingId() });
				return true;
			} else {
				ToastHelper.error(statusCode);
				return false;
			}
		}
	}, {
		key: 'handleMapping',
		value: function handleMapping(e) {
			this.setState({ selectedUnit: event.target.value });
		}
	}, {
		key: 'setConversionRate',
		value: function setConversionRate(e) {
			this.setState({ conversionRate: e.target.value });
		}
	}, {
		key: 'render',
		value: function render() {
			var _this3 = this;

			var units = this.baseUnits.map(function (unit) {
				return _react2.default.createElement(
					'option',
					{
						key: unit.getUnitId(),
						value: unit.getUnitId()
					},
					unit.getUnitName() + ' (' + unit.getUnitShortName() + ')'
				);
			});
			this.baseUnitName = this.itemUnitCache.getItemUnitNameById(this.state.baseUnit);
			this.secondaryUnitName = this.itemUnitCache.getItemUnitNameById(this.state.secondaryUnit);

			var mappings = this.state.mappings.map(function (mapping) {
				return _react2.default.createElement(
					'div',
					{
						key: mapping.getMappingId(),
						className: 'mappedUnits d-flex flex-row'
					},
					_react2.default.createElement(
						'div',
						null,
						_react2.default.createElement(_Radio2.default, {
							color: 'primary',
							onChange: _this3.handleMapping,
							name: 'mappedUnit',
							value: mapping.getMappingId(),
							checked: _this3.state.selectedUnit == mapping.getMappingId()
						})
					),
					_react2.default.createElement(
						'div',
						{ className: 'd-flex flex-column justify-content-center' },
						_react2.default.createElement(
							'div',
							{ className: 'd-flex flex-row' },
							_react2.default.createElement(
								'div',
								{ className: 'oneUnit' },
								'1'
							),
							' ',
							_react2.default.createElement(
								'div',
								{ className: 'baseUnitName' },
								_this3.baseUnitName
							),
							' ',
							_react2.default.createElement(
								'div',
								{ className: 'equalSign' },
								'='
							),
							_react2.default.createElement(
								'div',
								{ className: 'conversionRate' },
								mapping.getConversionRate()
							),
							_react2.default.createElement(
								'div',
								{ className: 'secondaryUnitName' },
								_this3.secondaryUnitName
							)
						)
					)
				);
			});
			return _react2.default.createElement(
				_Modal2.default,
				{
					parentSelector: document.querySelector('#addNewItemDialog'),
					onClose: this.onClose,
					isOpen: this.props.isOpen,
					title: 'Select Unit'
				},
				_react2.default.createElement(
					'div',
					{ className: 'modalBody' },
					_react2.default.createElement(
						'div',
						{ className: 'modal-content-wrapper select-unit' },
						_react2.default.createElement(
							'div',
							{ className: 'form-control inline' },
							_react2.default.createElement(
								'label',
								{ className: 'block-label', htmlFor: '' },
								'base unit'
							),
							_react2.default.createElement(
								'select',
								{
									autoFocus: true,
									tabIndex: 0,
									defaultValue: this.state.baseUnit,
									style: { width: '200px' },
									onChange: this.changeBaseUnit,
									className: 'input-control'
								},
								_react2.default.createElement(
									'option',
									{ value: '0' },
									'None'
								),
								units
							)
						),
						_react2.default.createElement(
							'div',
							{ className: 'form-control inline ml-15' },
							_react2.default.createElement(
								'label',
								{ className: 'block-label', htmlFor: '' },
								'secondary unit'
							),
							_react2.default.createElement(
								'select',
								{
									defaultValue: this.state.secondaryUnit,
									style: { width: '200px' },
									onChange: this.changeSecondaryUnit,
									className: 'input-control'
								},
								_react2.default.createElement(
									'option',
									{ value: '0' },
									'None'
								),
								units
							)
						),
						this.state.showMappings && _react2.default.createElement(
							'div',
							{ className: 'itemMappingsContainer mappedUnits' },
							_react2.default.createElement(
								'h4',
								null,
								'Conversion Rates'
							),
							_react2.default.createElement(
								'div',
								{ className: 'mappedUnits d-flex flex-row' },
								_react2.default.createElement(
									'div',
									null,
									_react2.default.createElement(_Radio2.default, {
										color: 'primary',
										onChange: this.handleMapping,
										name: 'mappedUnit',
										value: -1,
										checked: this.state.selectedUnit == -1
									})
								),
								_react2.default.createElement(
									'div',
									{ className: 'd-flex flex-column justify-content-center' },
									_react2.default.createElement(
										'div',
										{ className: 'd-flex flex-row mappingFirstRow' },
										_react2.default.createElement(
											'div',
											{ className: 'oneUnit' },
											'1'
										),
										_react2.default.createElement(
											'div',
											{ className: 'baseUnitName' },
											this.baseUnitName
										),
										_react2.default.createElement(
											'div',
											{ className: 'equalSign' },
											'='
										),
										_react2.default.createElement(
											'div',
											{ className: 'conversionRate' },
											_react2.default.createElement('input', {
												value: this.state.conversionRate,
												onChange: this.setConversionRate,
												type: 'number'
											})
										),
										_react2.default.createElement(
											'div',
											{ className: 'secondaryUnitName' },
											this.secondaryUnitName
										)
									)
								)
							),
							mappings
						)
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'modalFooter align-items-center justify-content-end d-flex' },
					_react2.default.createElement(
						'button',
						{
							onClick: this.onSave,
							value: 'Select Items',
							className: 'modal-footer-button terminalButton'
						},
						'Save'
					)
				)
			);
		}
	}]);
	return SelectUnitModal;
}(_react2.default.Component);

exports.default = SelectUnitModal;