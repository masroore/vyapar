Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Dialog = require('@material-ui/core/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _LinkPaymentFilter = require('./LinkPaymentFilter');

var _LinkPaymentFilter2 = _interopRequireDefault(_LinkPaymentFilter);

var _LinkPaymentSelectTxn = require('./LinkPaymentSelectTxn');

var _LinkPaymentSelectTxn2 = _interopRequireDefault(_LinkPaymentSelectTxn);

var _LinkPaymentTerminalButton = require('./LinkPaymentTerminalButton');

var _LinkPaymentTerminalButton2 = _interopRequireDefault(_LinkPaymentTerminalButton);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _TxnTypeConstant = require('../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _IconButton = require('@material-ui/core/IconButton');

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Close = require('@material-ui/icons/Close');

var _Close2 = _interopRequireDefault(_Close);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LinkPaymentModal = function (_React$Component) {
	(0, _inherits3.default)(LinkPaymentModal, _React$Component);

	function LinkPaymentModal(props) {
		(0, _classCallCheck3.default)(this, LinkPaymentModal);

		var _this = (0, _possibleConstructorReturn3.default)(this, (LinkPaymentModal.__proto__ || (0, _getPrototypeOf2.default)(LinkPaymentModal)).call(this, props));

		_this.state = {
			selectedAmountText: '',
			selectedAmount: 0
		};
		_this.amountToLink = 0;
		_this.calculateSelectedTransactionsTotal = _this.calculateSelectedTransactionsTotal.bind(_this);
		_this.getSelectedTransactionTotal = _this.getSelectedTransactionTotal.bind(_this);
		_this.autoLink = _this.autoLink.bind(_this);
		_this.reset = _this.reset.bind(_this);
		_this.getApi = _this.getApi.bind(_this);
		_this.gridApi = null;
		return _this;
	}

	(0, _createClass3.default)(LinkPaymentModal, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.calculateSelectedTransactionsTotal(true);
		}
	}, {
		key: 'getApi',
		value: function getApi(api) {
			this.gridApi = api;
		}
	}, {
		key: 'getSelectedTransactionTotal',
		value: function getSelectedTransactionTotal() {
			var selectedTransactionMap = this.props.selectedTransactionMap;

			var totalSelectedAmount = 0;

			selectedTransactionMap.forEach(function (value, key) {
				var selectedAmount = _MyDouble2.default.convertStringToDouble(value[1]);
				totalSelectedAmount += selectedAmount;
			});

			return totalSelectedAmount;
		}
	}, {
		key: 'calculateSelectedTransactionsTotal',
		value: function calculateSelectedTransactionsTotal() {
			var setAmountToLink = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
			var _props = this.props,
			    txnType = _props.txnType,
			    _props$transaction = _props.transaction,
			    total = _props$transaction.total,
			    received = _props$transaction.received,
			    isReverseCharge = _props$transaction.isReverseCharge,
			    payable = _props$transaction.payable,
			    discountForCash = _props$transaction.discountForCash;

			var totalSelectedAmount = this.getSelectedTransactionTotal();
			var amountToLink = void 0;

			var cashReceivedInDialogInput = document.querySelector('#paymentLinkCashReceived');
			var cashReceivedInDialog = 0;
			if (cashReceivedInDialogInput) {
				cashReceivedInDialog = _MyDouble2.default.convertStringToDouble(cashReceivedInDialogInput.value);
			}
			// let totalAmount = total;
			var totalAmount = isReverseCharge ? _MyDouble2.default.convertStringToDouble(payable) : _MyDouble2.default.convertStringToDouble(total);
			if (!cashReceivedInDialogInput) {
				if (txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT) {
					cashReceivedInDialog = _MyDouble2.default.convertStringToDouble(total);
				} else {
					cashReceivedInDialog = _MyDouble2.default.convertStringToDouble(received);
				}
			}
			var discountAmountInDialog = document.querySelector('#paymentLinkDiscount');
			if (discountAmountInDialog) {
				discountAmountInDialog = _MyDouble2.default.convertStringToDouble(discountAmountInDialog.value);
			} else {
				discountAmountInDialog = _MyDouble2.default.convertStringToDouble(discountForCash);
			}

			var amountTotal = 0;

			if (txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT) {
				var amountString = 'Unused Amount :';
				amountTotal = cashReceivedInDialog + discountAmountInDialog - totalSelectedAmount;
				amountToLink = cashReceivedInDialog + discountAmountInDialog;
			} else {
				amountString = 'Remaining to Link :';

				amountTotal = totalAmount - cashReceivedInDialog - discountAmountInDialog - totalSelectedAmount;
				amountToLink = totalAmount - cashReceivedInDialog - discountAmountInDialog;
			}
			if (setAmountToLink) {
				this.amountToLink = _MyDouble2.default.getAmountWithDecimal(amountToLink);
			}
			this.setState({
				selectedAmountText: amountString,
				selectedAmount: _MyDouble2.default.getAmountWithDecimal(amountTotal)
			});
		}
	}, {
		key: 'autoLink',
		value: function autoLink() {
			var _props2 = this.props,
			    txnType = _props2.txnType,
			    txnListB2B = _props2.txnListB2B,
			    selectedTransactionMap = _props2.selectedTransactionMap;

			this.reset();
			var amount = void 0;
			if (txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT) {
				amount = Number(this.amountToLink);
			} else {
				amount = Number(document.getElementById('linkPaymentTxnTotal').textContent) - document.getElementById('paymentLinkCashReceived').value;
			}
			if (amount >= 0) {
				this.gridApi.forEachNode(function (rowNode, index) {
					var mapVal = txnListB2B.get(_MyDouble2.default.convertStringToDouble(rowNode.data.id));
					var newObj = mapVal[0];
					var linkTxn = null;
					var totalAmountIncludingselectedAndCurrBal = rowNode.data.totalAmountIncludingselectedAndCurrBal;
					if (amount != 0) {
						rowNode.data.autoLink = true;
						rowNode.setSelected(true);
						if (amount > totalAmountIncludingselectedAndCurrBal) {
							rowNode.setDataValue('linkAmount', _MyDouble2.default.getAmountWithDecimal(totalAmountIncludingselectedAndCurrBal));
							amount -= totalAmountIncludingselectedAndCurrBal;
							selectedTransactionMap.set(_MyDouble2.default.convertStringToDouble(rowNode.data.id), [newObj, totalAmountIncludingselectedAndCurrBal, linkTxn]);
							txnListB2B.set(_MyDouble2.default.convertStringToDouble(rowNode.data.id), [newObj, 0]);
						} else {
							rowNode.setDataValue('linkAmount', _MyDouble2.default.getAmountWithDecimal(amount));
							selectedTransactionMap.set(_MyDouble2.default.convertStringToDouble(rowNode.data.id), [newObj, amount, linkTxn]);
							txnListB2B.set(_MyDouble2.default.convertStringToDouble(rowNode.data.id), [newObj, 0]);

							amount = 0;
						}
					}
				});
			}
		}
	}, {
		key: 'reset',
		value: function reset() {
			this.gridApi.forEachNode(function (rowNode, index) {
				rowNode.setSelected(false);
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _props3 = this.props,
			    txnType = _props3.txnType,
			    txnId = _props3.txnId,
			    oldTxnObj = _props3.oldTxnObj,
			    txnListB2B = _props3.txnListB2B,
			    selectedTransactionMap = _props3.selectedTransactionMap,
			    isOpen = _props3.isOpen,
			    onClose = _props3.onClose,
			    closeDialog = _props3.closeDialog,
			    transaction = _props3.transaction,
			    changeFields = _props3.changeFields,
			    changeTransactionLinks = _props3.changeTransactionLinks;

			return _react2.default.createElement(
				_Dialog2.default,
				{
					maxWidth: false,
					open: isOpen,
					onClose: onClose
					// TransitionComponent={Transition}
					, title: 'Link Payment'
				},
				_react2.default.createElement(
					'div',
					{ className: 'modalBody linkPaymentModal' },
					_react2.default.createElement(
						'div',
						{ className: 'd-flex justify-content-between align-items-center link-payment-header-container' },
						_react2.default.createElement(
							'h2',
							{ className: 'link-payment-modal-title' },
							'Link Payment'
						),
						_react2.default.createElement(
							_IconButton2.default,
							{ onClick: onClose },
							_react2.default.createElement(_Close2.default, null)
						)
					),
					_react2.default.createElement(_LinkPaymentFilter2.default, {
						txnType: txnType,
						txnId: txnId,
						oldTxnObj: oldTxnObj,
						amtToLink: this.amountToLink,
						reset: this.reset,
						autoLink: this.autoLink,
						calculateSelectedTransactionsTotal: this.calculateSelectedTransactionsTotal,
						transaction: transaction
					}),
					_react2.default.createElement(_LinkPaymentSelectTxn2.default, {
						txnType: txnType,
						getApi: this.getApi,
						calculateSelectedTransactionsTotal: this.calculateSelectedTransactionsTotal,
						selectedTransactionMap: selectedTransactionMap,
						txnListB2B: txnListB2B,
						transaction: transaction
					})
				),
				_react2.default.createElement(_LinkPaymentTerminalButton2.default, {
					changeFields: changeFields,
					closeDialog: closeDialog,
					onClose: onClose,
					txnType: txnType,
					calculateSelectedTransactionsTotal: this.calculateSelectedTransactionsTotal,
					selectedAmountText: this.state.selectedAmountText,
					selectedAmount: this.state.selectedAmount,
					selectedTransactionMap: selectedTransactionMap,
					changeTransactionLinks: changeTransactionLinks,
					txnListB2B: txnListB2B,
					transaction: transaction
				})
			);
		}
	}]);
	return LinkPaymentModal;
}(_react2.default.Component);

exports.default = LinkPaymentModal;

LinkPaymentModal.propTypes = {
	selectedTransactionMap: _propTypes2.default.object,
	txnType: _propTypes2.default.number,
	transaction: _propTypes2.default.object,
	txnListB2B: _propTypes2.default.object,
	txnId: _propTypes2.default.number,
	oldTxnObj: _propTypes2.default.object,
	isOpen: _propTypes2.default.bool,
	onClose: _propTypes2.default.func,
	closeDialog: _propTypes2.default.func,
	changeFields: _propTypes2.default.func,
	changeTransactionLinks: _propTypes2.default.func
};