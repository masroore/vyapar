Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _Dialog = require('@material-ui/core/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Close = require('@material-ui/icons/Close');

var _Close2 = _interopRequireDefault(_Close);

var _themes = require('../../themes');

var _themes2 = _interopRequireDefault(_themes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var theme = (0, _themes2.default)();
var styles = function styles() {
	return {
		container: {
			maxHeight: 600,
			height: 420,
			maxWidth: 1000,
			width: 600,
			padding: 0,
			margin: 0
		},
		wrapper: {
			position: 'relative'
		},
		closeButtonWrapper: {
			position: 'absolute',
			right: 35,
			zIndex: 999,
			top: 15
		},
		actionIcons: {
			color: 'rgba(34,58,82,0.54)',
			cursor: 'pointer'
		}
	};
};

var AddNewBankAccount = function (_React$Component) {
	(0, _inherits3.default)(AddNewBankAccount, _React$Component);

	function AddNewBankAccount(props) {
		(0, _classCallCheck3.default)(this, AddNewBankAccount);

		var _this = (0, _possibleConstructorReturn3.default)(this, (AddNewBankAccount.__proto__ || (0, _getPrototypeOf2.default)(AddNewBankAccount)).call(this, props));

		_this.onClose = _this.onClose.bind(_this);
		_this.onSave = _this.onSave.bind(_this);
		window.onBankAccountCreated = _this.onSave;
		return _this;
	}

	(0, _createClass3.default)(AddNewBankAccount, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			setTimeout(function () {
				$('#addNewBankAccountContainer').load('../UIComponent/NewBankAccount.html', {}, function () {
					$('#addNewBankAccountContainer #transactionStyle').css({
						top: 0,
						background: '#fff'
					});
					$('#addNewBankAccountContainer .newBank').css('width', 'auto');
					$('#addNewBankAccountContainer .tableFormat').css('margin-top', '15px');
					$('#addNewBankAccountContainer #submitNew').hide();
					$('#addNewBankAccountContainer #submit').css('margin-right', '0px');
				});
			}, 100);
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			if (window.onBankAccountCreated) {
				delete window.onBankAccountCreated;
			}
		}
	}, {
		key: 'onClose',
		value: function onClose(e) {
			e.stopPropagation();
			this.props.onCreate && this.props.onCreate('');
		}
	}, {
		key: 'onSave',
		value: function onSave(createdBankAccount) {
			this.props.onCreate && this.props.onCreate(createdBankAccount);
		}
	}, {
		key: 'render',
		value: function render() {
			var classes = this.props.classes;

			return _react2.default.createElement(
				_styles.MuiThemeProvider,
				{ theme: theme },
				_react2.default.createElement(
					_Dialog2.default,
					{ disableEnforceFocus: true, open: true, onClose: this.onClose },
					_react2.default.createElement(
						'div',
						{ className: classes.wrapper },
						_react2.default.createElement(
							'div',
							{ onClick: this.onClose, className: classes.closeButtonWrapper },
							_react2.default.createElement(_Close2.default, { className: classes.actionIcons })
						),
						_react2.default.createElement('div', {
							className: classes.container,
							id: 'addNewBankAccountContainer'
						})
					)
				)
			);
		}
	}]);
	return AddNewBankAccount;
}(_react2.default.Component);

AddNewBankAccount.propTypes = {
	classes: _propTypes2.default.object,
	onCreate: _propTypes2.default.func
};
exports.default = (0, _styles.withStyles)(styles)(AddNewBankAccount);