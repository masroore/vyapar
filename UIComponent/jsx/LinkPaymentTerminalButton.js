Object.defineProperty(exports, "__esModule", {
	value: true
});

var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _TxnTypeConstant = require('../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _styles = require('@material-ui/core/styles');

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _ToastHelper = require('../../UIControllers/ToastHelper');

var ToastHelper = _interopRequireWildcard(_ToastHelper);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
	selectedAmountText: {
		paddingRight: '10px'
	}
};

var LinkPaymentTerminalButton = function (_React$Component) {
	(0, _inherits3.default)(LinkPaymentTerminalButton, _React$Component);

	function LinkPaymentTerminalButton(props) {
		(0, _classCallCheck3.default)(this, LinkPaymentTerminalButton);

		var _this = (0, _possibleConstructorReturn3.default)(this, (LinkPaymentTerminalButton.__proto__ || (0, _getPrototypeOf2.default)(LinkPaymentTerminalButton)).call(this, props));

		_this.state = {};
		_this.closeDialog = _this.closeDialog.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(LinkPaymentTerminalButton, [{
		key: 'fillTotalReceivedAmount',
		value: function fillTotalReceivedAmount() {
			var _props = this.props,
			    _props$transaction = _props.transaction,
			    received = _props$transaction.received,
			    receivedLater = _props$transaction.receivedLater,
			    transaction = _props.transaction;

			var totalReceivedAmount = _MyDouble2.default.convertStringToDouble(received) + _MyDouble2.default.convertStringToDouble(receivedLater);

			transaction.totalReceivedAmount = totalReceivedAmount;
		}
	}, {
		key: 'calculateTxnCurrentBalance',
		value: function calculateTxnCurrentBalance() {
			var updateForCashIn = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
			var _props2 = this.props,
			    txnType = _props2.txnType,
			    _props2$transaction = _props2.transaction,
			    total = _props2$transaction.total,
			    received = _props2$transaction.received,
			    receivedLater = _props2$transaction.receivedLater,
			    isReverseCharge = _props2$transaction.isReverseCharge,
			    payable = _props2$transaction.payable,
			    discountForCash = _props2$transaction.discountForCash,
			    transaction = _props2.transaction;
			// transaction current balance calculation

			var totalAmount = isReverseCharge ? _MyDouble2.default.convertStringToDouble(payable) : _MyDouble2.default.convertStringToDouble(total);
			var cashAmount = _MyDouble2.default.convertStringToDouble(received);
			var receivedLaterAmount = _MyDouble2.default.convertStringToDouble(receivedLater);
			var discountTotalAmount = _MyDouble2.default.convertStringToDouble(discountForCash);

			if (txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT) {
				cashAmount = 0;
			}
			var txnCurrentBalance = _MyDouble2.default.convertStringToDouble(totalAmount) - _MyDouble2.default.convertStringToDouble(cashAmount) - _MyDouble2.default.convertStringToDouble(receivedLaterAmount);

			if (txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT) {
				txnCurrentBalance = txnCurrentBalance + discountTotalAmount;

				if (_MyDouble2.default.convertStringToDouble(totalAmount) == 0) {
					//                        txnCurrentBalance = 0;
					if (!updateForCashIn) {
						transaction.total = _MyDouble2.default.getAmountWithDecimal(receivedLaterAmount - discountTotalAmount);
					}
				}
			}

			transaction.currentTxnBalance = _MyDouble2.default.getBalanceAmountWithDecimal(txnCurrentBalance);

			transaction.totalReceivedAmount = _MyDouble2.default.convertStringToDouble(received) + _MyDouble2.default.convertStringToDouble(receivedLater);
		}
	}, {
		key: 'calculateBalanceWhenReverseChargeEnabled',
		value: function calculateBalanceWhenReverseChargeEnabled() {
			var _props3 = this.props,
			    _props3$transaction = _props3.transaction,
			    reverseChargeAmount = _props3$transaction.reverseChargeAmount,
			    total = _props3$transaction.total,
			    roundOffAmount = _props3$transaction.roundOffAmount,
			    received = _props3$transaction.received,
			    transaction = _props3.transaction;

			var taxAmount = reverseChargeAmount;
			var totalPayableAmount = _MyDouble2.default.convertStringToDouble(total) - taxAmount + roundOffAmount;
			transaction.payable = _MyDouble2.default.getBalanceAmountWithDecimal(totalPayableAmount);
			if (totalPayableAmount > 0) {
				transaction.balance = _MyDouble2.default.getBalanceAmountWithDecimal(totalPayableAmount - received);
			}
		}
	}, {
		key: 'changeBal',
		value: function changeBal() {
			var _props4 = this.props,
			    txnType = _props4.txnType,
			    selectedTransactionMap = _props4.selectedTransactionMap,
			    _props4$transaction = _props4.transaction,
			    total = _props4$transaction.total,
			    isReverseCharge = _props4$transaction.isReverseCharge,
			    received = _props4$transaction.received,
			    cashSale = _props4$transaction.cashSale,
			    balance = _props4$transaction.balance,
			    transaction = _props4.transaction;

			var totalAmount = _MyDouble2.default.convertStringToDouble(total);
			var receivedAmount = received;
			var balanceAmount = balance;

			if (isReverseCharge && (txnType == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE || txnType == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN)) {
				this.calculateBalanceWhenReverseChargeEnabled();
			} else {
				var amount = received ? _MyDouble2.default.convertStringToDouble(received) : 0;

				receivedAmount = amount;
				if (cashSale) {
					receivedAmount = totalAmount;
					balanceAmount = 0;
				} else {
					balanceAmount = _MyDouble2.default.getBalanceAmountWithDecimal(totalAmount - amount);
				}
			}

			transaction.received = receivedAmount;
			transaction.balance = balanceAmount;

			if (selectedTransactionMap.size > 0) {
				this.calculateTxnCurrentBalance(true);
			}
		}
	}, {
		key: 'compareAmountForBillToBill',
		value: function compareAmountForBillToBill() {
			var _props5 = this.props,
			    txnType = _props5.txnType,
			    oldTxnObj = _props5.oldTxnObj,
			    selectedTransactionMap = _props5.selectedTransactionMap,
			    _props5$transaction = _props5.transaction,
			    total = _props5$transaction.total,
			    receivedLater = _props5$transaction.receivedLater,
			    discountForCash = _props5$transaction.discountForCash,
			    transaction = _props5.transaction;

			var totalAmount = _MyDouble2.default.convertStringToDouble(total);

			if ((txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT) && (!oldTxnObj || oldTxnObj.getTxnType() == _TxnTypeConstant2.default.TXN_TYPE_CASHIN || oldTxnObj.getTxnType() == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT)) {
				var SettingCache = require('../../Cache/SettingCache');
				var settingCache = new SettingCache();
				if (settingCache.isBillToBillEnabled() || selectedTransactionMap.size > 0) {
					var receivedLaterAmt = _MyDouble2.default.convertStringToDouble(receivedLater);
					var discountTotalAmount = _MyDouble2.default.convertStringToDouble(discountForCash);

					if (receivedLaterAmt > 0) {
						if (totalAmount + discountTotalAmount < receivedLaterAmt) {
							transaction.showErrorForTotal = true;
						} else {
							transaction.showErrorForTotal = false;
						}
					}
				}
			}
		}
	}, {
		key: 'updateReceivedLaterAmount',
		value: function updateReceivedLaterAmount(totalLinkedAmount) {
			var transaction = this.props.transaction;

			transaction.receivedLater = _MyDouble2.default.convertStringToDouble(totalLinkedAmount);
		}
	}, {
		key: 'getSelectedTransactionTotal',
		value: function getSelectedTransactionTotal() {
			var selectedTransactionMap = this.props.selectedTransactionMap;


			var totalSelectedAmount = 0;

			selectedTransactionMap.forEach(function (value, key) {
				var selectedAmount = _MyDouble2.default.convertStringToDouble(value[1]);
				totalSelectedAmount += selectedAmount;
			});

			return totalSelectedAmount;
		}
	}, {
		key: 'fillReceivedAmountAndDiscountFromSelectTxnDialog',
		value: function fillReceivedAmountAndDiscountFromSelectTxnDialog() {
			var _props6 = this.props,
			    txnType = _props6.txnType,
			    _props6$transaction = _props6.transaction,
			    total = _props6$transaction.total,
			    received = _props6$transaction.received,
			    discountForCash = _props6$transaction.discountForCash,
			    transaction = _props6.transaction;

			var cashReceivedInDialog = document.querySelector('#paymentLinkCashReceived');
			if (cashReceivedInDialog) {
				cashReceivedInDialog = _MyDouble2.default.convertStringToDouble(cashReceivedInDialog.value);
			} else {
				if (txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT) {
					cashReceivedInDialog = _MyDouble2.default.convertStringToDouble(total);
				} else {
					cashReceivedInDialog = _MyDouble2.default.convertStringToDouble(received);
				}
			}

			var discountAmountInDialog = document.querySelector('#paymentLinkDiscount');
			if (discountAmountInDialog) {
				discountAmountInDialog = _MyDouble2.default.convertStringToDouble(discountAmountInDialog.value);
			} else {
				discountAmountInDialog = _MyDouble2.default.convertStringToDouble(discountForCash);
			}
			var discountForCashAmount = discountForCash;
			var totalAmount = total;
			var receivedAmount = received;

			if (txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT) {
				discountForCashAmount = _MyDouble2.default.getAmountWithDecimal(discountAmountInDialog);
				totalAmount = _MyDouble2.default.getAmountWithDecimal(cashReceivedInDialog);
			} else {
				receivedAmount = _MyDouble2.default.getAmountWithDecimal(cashReceivedInDialog);
			}
			transaction.discountForCash = discountForCashAmount;
			transaction.total = totalAmount;
			transaction.received = receivedAmount;
		}
	}, {
		key: 'showAlertIfCurrentBalIsNegative',
		value: function showAlertIfCurrentBalIsNegative() {
			var txnType = this.props.txnType;

			var currBalInTxnDialog = _MyDouble2.default.convertStringToDouble(document.getElementById('selectedAmountTotal').textContent);

			var alertMsg = false;

			if (currBalInTxnDialog < 0) {
				switch (txnType) {
					case _TxnTypeConstant2.default.TXN_TYPE_CASHIN:
						alertMsg = 'Received amount cannot be more than total amount';
						break;
					case _TxnTypeConstant2.default.TXN_TYPE_CASHOUT:
						alertMsg = 'Paid amount cannot be more than total amount';
						break;
					case _TxnTypeConstant2.default.TXN_TYPE_SALE:
						alertMsg = 'Received amount on sale cannot be more than total sale amount';
						break;
					case _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN:
						alertMsg = 'Received amount cannot be more than total amount';
						break;
					case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE:
						alertMsg = 'Paid amount on purchase cannot be more than total purchase amount';
						break;
					case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN:
						alertMsg = 'Paid amount cannot be more than total amount';
						break;
					case _TxnTypeConstant2.default.TXN_TYPE_POPENBALANCE:
						break;
					case _TxnTypeConstant2.default.TXN_TYPE_ROPENBALANCE:
						break;
				}
			}

			return alertMsg;
		}
	}, {
		key: 'closeDialog',
		value: function closeDialog() {
			var _props7 = this.props,
			    selectedTransactionMap = _props7.selectedTransactionMap,
			    txnListB2B = _props7.txnListB2B,
			    changeTransactionLinks = _props7.changeTransactionLinks,
			    transaction = _props7.transaction,
			    changeFields = _props7.changeFields;

			var alertMsg = this.showAlertIfCurrentBalIsNegative();
			if (alertMsg) {
				ToastHelper.error(alertMsg);
				return;
			}

			this.fillReceivedAmountAndDiscountFromSelectTxnDialog();

			var totalLinkedAmount = this.getSelectedTransactionTotal();

			this.updateReceivedLaterAmount(totalLinkedAmount);

			this.compareAmountForBillToBill();

			this.changeBal();

			changeFields(transaction);
			changeTransactionLinks(txnListB2B, selectedTransactionMap, new _map2.default(selectedTransactionMap));

			try {
				this.props.closeDialog();
			} catch (err) {}
		}
	}, {
		key: 'render',
		value: function render() {
			var _props8 = this.props,
			    classes = _props8.classes,
			    onClose = _props8.onClose;

			return _react2.default.createElement(
				'div',
				{ className: 'clearfix marginv12 pr-20 font-med-14' },
				_react2.default.createElement(
					'div',
					{ className: 'd-flex justify-content-end' },
					_react2.default.createElement(
						'div',
						{ className: 'padright' },
						_react2.default.createElement(
							'span',
							{
								id: 'selectedAmountText',
								className: classes.selectedAmountText
							},
							this.props.selectedAmountText
						),
						_react2.default.createElement(
							'span',
							{ id: 'selectedAmountTotal', className: '' },
							this.props.selectedAmount
						)
					),
					_react2.default.createElement(
						_Button2.default,
						{
							onClick: onClose,
							variant: 'contained',
							color: 'default',
							className: 'mr-20'
						},
						'CANCEL'
					),
					_react2.default.createElement(
						_Button2.default,
						{
							onClick: this.closeDialog,
							variant: 'contained',
							color: 'primary'
						},
						'DONE'
					)
				)
			);
		}
	}]);
	return LinkPaymentTerminalButton;
}(_react2.default.Component);

LinkPaymentTerminalButton.propTypes = {
	transaction: _propTypes2.default.object,
	txnType: _propTypes2.default.number,
	selectedTransactionMap: _propTypes2.default.object,
	oldTxnObj: _propTypes2.default.object,
	txnListB2B: _propTypes2.default.object,
	changeTransactionLinks: _propTypes2.default.func,
	changeFields: _propTypes2.default.func,
	closeDialog: _propTypes2.default.func,
	classes: _propTypes2.default.object,
	onClose: _propTypes2.default.func,
	selectedAmountText: _propTypes2.default.string,
	selectedAmount: _propTypes2.default.number
};

exports.default = (0, _styles.withStyles)(styles)(LinkPaymentTerminalButton);