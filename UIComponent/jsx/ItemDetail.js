Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _SettingCache = require('../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _ItemType = require('../../Constants/ItemType');

var _ItemType2 = _interopRequireDefault(_ItemType);

var _styles = require('@material-ui/core/styles');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _ItemSharingOptions = require('./ItemSharingOptions');

var _ItemSharingOptions2 = _interopRequireDefault(_ItemSharingOptions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var settingCache = new _SettingCache2.default();
var styles = function styles() {
	return {
		blueBg: {
			background: '#1789FC',
			height: 28,
			borderRadius: 5,
			'&:hover': {
				boxShadow: 'none'
			}
		},
		btnIcon: {
			transfrom: 'rotate(90deg)'
		}
	};
};

var ItemDetail = function (_React$Component) {
	(0, _inherits3.default)(ItemDetail, _React$Component);

	function ItemDetail() {
		var _ref;

		var _temp, _this, _ret;

		(0, _classCallCheck3.default)(this, ItemDetail);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = ItemDetail.__proto__ || (0, _getPrototypeOf2.default)(ItemDetail)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
			showSharingOptions: false
		}, _this.shareItem = function () {
			_this.setState({
				showSharingOptions: !_this.state.showSharingOptions
			});
		}, _this.handleClickOutside = function (event) {
			if (_this.node && !_this.node.contains(event.target)) {
				_this.setState({
					showSharingOptions: false
				});
			}
		}, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
	}

	(0, _createClass3.default)(ItemDetail, [{
		key: 'adjustStock',
		value: function adjustStock(item) {
			MyAnalytics.pushEvent('Item Adjustment Open');
			var curItem = item.getItemName();
			window.itemAdjustmentNameGlobal = curItem;
			window.itemAdjustmentIdGlobal = '';
			$('#defaultPage').hide();
			$('#frameDiv').load('StockAdjustment.html');
			$('.viewItems').css('display', 'none');
			$('#modelContainer').css('display', 'block');
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			document.addEventListener('mousedown', this.handleClickOutside);
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			document.removeEventListener('mousedown', this.handleClickOutside);
		}
	}, {
		key: 'render',
		value: function render() {
			var _this2 = this;

			var _props = this.props,
			    classes = _props.classes,
			    currentItem = _props.currentItem;

			var item = currentItem;
			if (!item) {
				return false;
			}
			var reservedQuantity = item.getReservedQuantity() ? Number(item.getReservedQuantity()) : 0;
			var isStockEnabled = settingCache.getStockEnabled();
			var taxTypeSaleString = '';
			var taxTypePurchaseString = '';
			if (settingCache.isItemwiseTaxEnabled() && settingCache.isInclusiveExclusiveTaxEnabledForTransactions()) {
				taxTypeSaleString = item.getItemTaxTypeSale() == _ItemType2.default.ITEM_TXN_TAX_INCLUSIVE ? '(incl)' : '(excl)';
				taxTypePurchaseString = item.getItemTaxTypePurchase() == _ItemType2.default.ITEM_TXN_TAX_INCLUSIVE ? '(incl)' : '(excl)';
			}

			return _react2.default.createElement(
				'div',
				{ className: 'containerForItemList ' + (!isStockEnabled ? 'settingItemStockDisable' : '') },
				_react2.default.createElement(
					'div',
					{ className: 'clearfix mb-10' },
					_react2.default.createElement(
						'div',
						{ id: 'currentItemName', className: 'titleColor halfdiv floatLeft d-flex align-items-center width50' },
						_react2.default.createElement(
							'span',
							{ className: 'ellipseText' },
							item.getItemName()
						),
						_react2.default.createElement(
							'div',
							{ className: 'position-relative item-sharing-container' },
							_react2.default.createElement(
								'div',
								{ ref: function ref(node) {
										_this2.node = node;
									} },
								_react2.default.createElement('img', { onClick: this.shareItem, className: 'ml-10', src: '../inlineSVG/share_icon.svg' }),
								_react2.default.createElement(_ItemSharingOptions2.default, {
									itemId: item.getItemId(),
									isVisible: this.state.showSharingOptions
								})
							)
						)
					),
					isStockEnabled && _react2.default.createElement(
						'div',
						{
							onClick: function onClick() {
								return _this2.adjustStock(item);
							},
							className: (0, _classnames2.default)('smallIconButton floatRight d-flex align-items-center', classes.blueBg),
							id: 'itemAdjustment'
						},
						_react2.default.createElement(
							'div',
							{ className: 'svgImageSmall floatLeft d-flex align-items-center' },
							_react2.default.createElement('img', { src: './../inlineSVG/item_adj.svg', width: '20px', className: 'pointer' })
						),
						_react2.default.createElement(
							'div',
							{ className: 'smallIconButtonText floatLeft' },
							' Adjust Item '
						)
					)
				),
				_react2.default.createElement(
					'div',
					null,
					isStockEnabled && _react2.default.createElement(
						'div',
						null,
						_react2.default.createElement(
							'div',
							{ className: 'row' },
							_react2.default.createElement(
								'div',
								{ className: 'col' },
								_react2.default.createElement(
									'span',
									null,
									'SALE PRICE: ',
									_react2.default.createElement('span', { dangerouslySetInnerHTML: _MyDouble2.default.getBalanceAmountWithDecimalAndCurrency(item['itemSaleUnitPrice'], undefined, true) }),
									' ',
									_react2.default.createElement('span', { dangerouslySetInnerHTML: { __html: '' + taxTypeSaleString } })
								)
							),
							_react2.default.createElement(
								'div',
								{ className: 'col d-flex justify-content-end' },
								_react2.default.createElement(
									'span',
									null,
									'STOCK QUANTITY: ',
									_react2.default.createElement('span', { dangerouslySetInnerHTML: _MyDouble2.default.getQuantityWithDecimal(item['itemStockQuantity'], undefined, true) })
								)
							)
						),
						_react2.default.createElement(
							'div',
							{ className: 'row' },
							_react2.default.createElement(
								'div',
								{ className: 'col' },
								_react2.default.createElement(
									'span',
									null,
									'PURCHASE PRICE: ',
									_react2.default.createElement('span', { dangerouslySetInnerHTML: _MyDouble2.default.getBalanceAmountWithDecimalAndCurrency(item['itemPurchaseUnitPrice'], undefined, true) }),
									' ',
									_react2.default.createElement('span', { dangerouslySetInnerHTML: { __html: '' + taxTypePurchaseString } })
								)
							),
							_react2.default.createElement(
								'div',
								{ className: 'col d-flex justify-content-end' },
								_react2.default.createElement(
									'span',
									null,
									'STOCK VALUE: ',
									_react2.default.createElement('span', { dangerouslySetInnerHTML: _MyDouble2.default.getBalanceAmountWithDecimalAndCurrency(item['itemStockValue'], undefined, true) })
								)
							)
						),
						reservedQuantity > 0.00000 && _react2.default.createElement(
							'div',
							{ className: 'row' },
							_react2.default.createElement(
								'div',
								{ className: 'col' },
								_react2.default.createElement(
									'span',
									null,
									'Available for Sale: ',
									_react2.default.createElement('span', { dangerouslySetInnerHTML: _MyDouble2.default.getQuantityWithDecimal(item.getAvailableQuantity(), undefined, true) })
								)
							),
							_react2.default.createElement(
								'div',
								{ className: 'col d-flex justify-content-end' },
								_react2.default.createElement(
									'span',
									null,
									'RESERVED QUANTITY: ',
									_react2.default.createElement('span', { dangerouslySetInnerHTML: _MyDouble2.default.getQuantityWithDecimal(reservedQuantity, undefined, true) })
								),
								' '
							)
						)
					),
					!isStockEnabled && _react2.default.createElement(
						'div',
						{ className: 'row' },
						_react2.default.createElement(
							'div',
							{ className: 'col' },
							_react2.default.createElement(
								'span',
								null,
								'SALE PRICE: ',
								_react2.default.createElement('span', { dangerouslySetInnerHTML: _MyDouble2.default.getBalanceAmountWithDecimalAndCurrency(item['itemSaleUnitPrice'], undefined, true) }),
								' ',
								_react2.default.createElement('span', { dangerouslySetInnerHTML: { __html: '' + taxTypeSaleString } })
							)
						),
						_react2.default.createElement(
							'div',
							{ className: 'col d-flex justify-content-end' },
							_react2.default.createElement(
								'span',
								null,
								' PURCHASE PRICE: ',
								_react2.default.createElement('span', { dangerouslySetInnerHTML: _MyDouble2.default.getBalanceAmountWithDecimalAndCurrency(item['itemPurchaseUnitPrice'], undefined, true) }),
								' ',
								_react2.default.createElement('span', { dangerouslySetInnerHTML: { __html: '' + taxTypePurchaseString } })
							)
						)
					)
				)
			);
		}
	}]);
	return ItemDetail;
}(_react2.default.Component);

exports.default = (0, _styles.withStyles)(styles)(ItemDetail);