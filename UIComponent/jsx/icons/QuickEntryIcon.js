Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _SvgIcon = require('@material-ui/core/SvgIcon');

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function QuickEntryIcon(props) {
	return _react2.default.createElement(
		_SvgIcon2.default,
		props,
		_react2.default.createElement(
			'g',
			{ xmlns: 'http://www.w3.org/2000/svg', transform: 'translate(-29.386 -6.422)' },
			_react2.default.createElement('path', {
				d: 'M33.986,25.432a.666.666,0,0,0,.8-.246l7.044-10.18a.655.655,0,0,0,.049-.69.685.685,0,0,0-.591-.361l-4.368-.033,1.264-6.7a.673.673,0,0,0-.378-.739.664.664,0,0,0-.8.2l-7.471,9.409a.652.652,0,0,0-.082.706.675.675,0,0,0,.608.378l4.828.033-1.3,7.488A.645.645,0,0,0,33.986,25.432ZM31.44,15.843l4.926-6.207-.92,4.844a.684.684,0,0,0,.657.8L40,15.3l-4.581,6.634.9-5.287a.646.646,0,0,0-.148-.542.616.616,0,0,0-.493-.23l-4.236-.033Z' })
		)
	);
}
exports.default = QuickEntryIcon;