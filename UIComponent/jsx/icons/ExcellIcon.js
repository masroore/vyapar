Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _SvgIcon = require('@material-ui/core/SvgIcon');

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ExcellIcon() {
	return _react2.default.createElement(
		_SvgIcon2.default,
		{ fontSize: 'small', viewBox: '0 0 24 24' },
		_react2.default.createElement(
			'g',
			{
				xmlns: 'http://www.w3.org/2000/svg',
				transform: 'translate(-1172.271 -46.66)'
			},
			_react2.default.createElement(
				'g',
				{ transform: 'translate(1174.43 49.66)' },
				_react2.default.createElement('path', {
					fill: '#284561',
					d: 'M11.072,0H12.5V1.924c2.407,0,4.814,0,7.222-.007a2.214,2.214,0,0,1,1.207.231,2.208,2.208,0,0,1,.236,1.217q-.018,6.262,0,12.519a10.979,10.979,0,0,1-.082,2.1c-.1.5-.7.512-1.1.529-2.491.007-4.985,0-7.479,0v2.164H11.007C7.342,20.015,3.67,19.4,0,18.757V1.926C3.691,1.284,7.383.652,11.072,0Z'
				}),
				_react2.default.createElement('path', {
					fill: '#fff',
					d: 'M51.98,11h7.936V26.15H51.98V24.707H53.9V23.024H51.98v-.962H53.9V20.379H51.98v-.962H53.9V17.733H51.98v-.962H53.9V15.088H51.98v-.962H53.9V12.443H51.98Z',
					transform: 'translate(-39.48 -8.355)'
				}),
				_react2.default.createElement('path', {
					fill: '#284561',
					d: 'M63.98,17h3.367v1.683H63.98Z',
					transform: 'translate(-48.594 -12.912)'
				}),
				_react2.default.createElement('path', {
					fill: '#fff',
					d: 'M17.02,26.059q.815-.058,1.635-.1-.963,1.973-1.941,3.941c.661,1.347,1.337,2.684,2,4.03-.58-.034-1.157-.07-1.736-.111a28.385,28.385,0,0,1-1.2-3.02c-.327.976-.794,1.9-1.169,2.854-.527-.007-1.053-.029-1.58-.05.618-1.21,1.214-2.429,1.852-3.631-.541-1.238-1.135-2.453-1.693-3.684q.794-.047,1.587-.091c.358.94.75,1.868,1.046,2.833.317-1.022.791-1.984,1.2-2.972Z',
					transform: 'translate(-9.897 -19.717)'
				}),
				_react2.default.createElement('path', {
					fill: '#284561',
					d: 'M63.98,28h3.367v1.683H63.98Z',
					transform: 'translate(-48.594 -21.267)'
				}),
				_react2.default.createElement('path', {
					fill: '#284561',
					d: 'M63.98,39h3.367v1.683H63.98Z',
					transform: 'translate(-48.594 -29.621)'
				}),
				_react2.default.createElement('path', {
					fill: '#284561',
					d: 'M63.98,50h3.367v1.683H63.98Z',
					transform: 'translate(-48.594 -37.976)'
				}),
				_react2.default.createElement('path', {
					fill: '#284561',
					d: 'M63.98,61h3.367v1.683H63.98Z',
					transform: 'translate(-48.594 -46.331)'
				})
			)
		)
	);
}
exports.default = ExcellIcon;