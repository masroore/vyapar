Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _SvgIcon = require('@material-ui/core/SvgIcon');

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function BarcodeIcon(props) {
	return _react2.default.createElement(
		_SvgIcon2.default,
		props,
		_react2.default.createElement('path', { xmlns: 'http://www.w3.org/2000/svg',
			d: 'M3.333,5.333H5v10H3.333v-10m2.5,0h.833v10H5.833v-10m1.667,0H10v10H7.5v-10m3.333,0h.833v10h-.833v-10m2.5,0H15v10H13.333v-10m2.5,0h.833v10h-.833v-10M1.667,3.667V7H0V3.667A1.667,1.667,0,0,1,1.667,2H5V3.667H1.667M18.333,2A1.667,1.667,0,0,1,20,3.667V7H18.333V3.667H15V2h3.333M1.667,13.667V17H5v1.667H1.667A1.667,1.667,0,0,1,0,17V13.667H1.667M18.333,17V13.667H20V17a1.667,1.667,0,0,1-1.667,1.667H15V17Z',
			transform: 'translate(0 -2)' })
	);
}
exports.default = BarcodeIcon;