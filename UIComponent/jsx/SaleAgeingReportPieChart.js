Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _recharts = require('recharts');

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SaleAgeingReportPieChart = function (_React$Component) {
	(0, _inherits3.default)(SaleAgeingReportPieChart, _React$Component);

	function SaleAgeingReportPieChart(props) {
		(0, _classCallCheck3.default)(this, SaleAgeingReportPieChart);

		var _this = (0, _possibleConstructorReturn3.default)(this, (SaleAgeingReportPieChart.__proto__ || (0, _getPrototypeOf2.default)(SaleAgeingReportPieChart)).call(this, props));

		_this.state = {};
		return _this;
	}

	(0, _createClass3.default)(SaleAgeingReportPieChart, [{
		key: 'render',
		value: function render() {
			var rowData = this.props.data;

			var dataForPieChart = {
				currentTotal: 0,
				bucket1Total: 0,
				bucket2Total: 0,
				bucket3Total: 0,
				bucket4Total: 0
			};

			rowData.map(function (data) {
				dataForPieChart['currentTotal'] += _MyDouble2.default.getBalanceAmountWithDecimal(data.current);
				dataForPieChart['bucket1Total'] += _MyDouble2.default.getBalanceAmountWithDecimal(data.bucket1);
				dataForPieChart['bucket2Total'] += _MyDouble2.default.getBalanceAmountWithDecimal(data.bucket2);
				dataForPieChart['bucket3Total'] += _MyDouble2.default.getBalanceAmountWithDecimal(data.bucket3);
				dataForPieChart['bucket4Total'] += _MyDouble2.default.getBalanceAmountWithDecimal(data.bucket4);
			});

			var pieChartTotal = dataForPieChart['currentTotal'] + dataForPieChart['bucket1Total'] + dataForPieChart['bucket2Total'] + dataForPieChart['bucket3Total'] + dataForPieChart['bucket4Total'];

			var data = [{ name: 'Current' + ' (' + _MyDouble2.default.getBalanceAmountWithDecimal(dataForPieChart.currentTotal) + ')', value: dataForPieChart.currentTotal }, { name: '1-30 Days' + ' (' + _MyDouble2.default.getBalanceAmountWithDecimal(dataForPieChart.bucket1Total) + ')', value: dataForPieChart.bucket1Total }, { name: '31-45 Days' + ' (' + _MyDouble2.default.getBalanceAmountWithDecimal(dataForPieChart.bucket2Total) + ')', value: dataForPieChart.bucket2Total }, { name: '46-60 Days' + ' (' + _MyDouble2.default.getBalanceAmountWithDecimal(dataForPieChart.bucket3Total) + ')', value: dataForPieChart.bucket3Total }, { name: 'Over 60 Days' + ' (' + _MyDouble2.default.getBalanceAmountWithDecimal(dataForPieChart.bucket4Total) + ')', value: dataForPieChart.bucket4Total }];

			var COLORS = ['#B3E758', '#FFBC00', '#097AA8', '#DEDCEA', '#FF8A67'];

			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement(
					_recharts.PieChart,
					{ width: 800, height: 200, onMouseEnter: this.onPieEnter },
					_react2.default.createElement(_recharts.Legend, { iconType: 'square', width: 220, height: 100, verticalAlign: 'middle', iconSize: 22, align: 'center', layout: 'vertical', wrapperStyle: { fontSize: 12, color: '#2B4C56' } }),
					_react2.default.createElement(
						_recharts.Pie,
						{
							data: data,
							dataKey: 'value',
							cx: 100,
							cy: 100,
							labelLine: false,
							outerRadius: 80,
							fill: '#8884d8'
						},
						data.map(function (entry, index) {
							return _react2.default.createElement(_recharts.Cell, { key: index, fill: COLORS[index % COLORS.length] });
						})
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'pieChartTotal' },
					_react2.default.createElement(
						'span',
						null,
						'Total: '
					),
					_MyDouble2.default.getBalanceAmountWithDecimal(pieChartTotal)
				)
			);
		}
	}]);
	return SaleAgeingReportPieChart;
}(_react2.default.Component);

SaleAgeingReportPieChart.propTypes = {
	data: _propTypes2.default.array.isRequired
};

exports.default = SaleAgeingReportPieChart;