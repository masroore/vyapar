Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/styles');

var _TransactionHelper = require('../../Utilities/TransactionHelper');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles() {
	return {
		root: {
			position: 'relative',
			margin: '0 10px',
			textTransform: 'capitalize'
		},
		defaultButton: {
			borderRadius: '.5rem',
			overflow: 'hidden',
			minHeight: '2rem',
			padding: 0
		},
		btnColor: {
			backgroundColor: '#1789FC',
			color: 'white',
			'&:hover': {
				backgroundColor: 'rgba(13, 137, 252, 0.69)'
			}
		},
		label: {
			textTransform: 'capitalize',
			paddingLeft: 12,
			paddingRight: 12
		}
	};
};

var AddNewTransaction = function (_React$Component) {
	(0, _inherits3.default)(AddNewTransaction, _React$Component);

	function AddNewTransaction() {
		var _ref;

		var _temp, _this, _ret;

		(0, _classCallCheck3.default)(this, AddNewTransaction);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = AddNewTransaction.__proto__ || (0, _getPrototypeOf2.default)(AddNewTransaction)).call.apply(_ref, [this].concat(args))), _this), _this.handleClick = function () {
			var txnType = _this.props.txnType;

			(0, _TransactionHelper.viewTransaction)('', txnType);
		}, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
	}

	(0, _createClass3.default)(AddNewTransaction, [{
		key: 'render',
		value: function render() {
			var _props = this.props,
			    classes = _props.classes,
			    label = _props.label,
			    handleClick = _props.handleClick;

			return _react2.default.createElement(
				_Button2.default,
				{
					variant: 'contained',
					classes: {
						root: classes.defaultButton,
						label: classes.label
					},
					className: (0, _classnames2.default)(classes.btnColor),
					onClick: handleClick || this.handleClick
				},
				label || 'Create'
			);
		}
	}]);
	return AddNewTransaction;
}(_react2.default.Component);

AddNewTransaction.propType = {
	classes: _propTypes2.default.object,
	label: _propTypes2.default.string,
	txnType: _propTypes2.default.string,
	handleClick: _propTypes2.default.func
};

exports.default = (0, _styles.withStyles)(styles)(AddNewTransaction);