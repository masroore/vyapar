Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Modal = require('./Modal');

var _Modal2 = _interopRequireDefault(_Modal);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AddCategoryButton = function (_React$Component) {
	(0, _inherits3.default)(AddCategoryButton, _React$Component);

	function AddCategoryButton(props) {
		(0, _classCallCheck3.default)(this, AddCategoryButton);

		var _this = (0, _possibleConstructorReturn3.default)(this, (AddCategoryButton.__proto__ || (0, _getPrototypeOf2.default)(AddCategoryButton)).call(this, props));

		_this.state = {
			categoryName: _this.props.categoryName || ''
		};
		_this.changeCategory = _this.changeCategory.bind(_this);
		_this.onSubmit = _this.onSubmit.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(AddCategoryButton, [{
		key: 'changeCategory',
		value: function changeCategory(e) {
			this.setState({ categoryName: e.target.value });
		}
	}, {
		key: 'onSubmit',
		value: function onSubmit(e) {
			e.preventDefault();
			this.props.onSave(this.state.categoryName);

			this.setState({
				categoryName: ''
			});
		}
	}, {
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				_Modal2.default,
				{
					height: '200px',
					width: '455px',
					onClose: this.props.onClose,
					isOpen: this.props.isOpen,
					title: this.props.title
				},
				_react2.default.createElement(
					'form',
					{
						onSubmit: this.onSubmit
					},
					_react2.default.createElement(
						'div',
						{ className: 'modalBody' },
						_react2.default.createElement(
							'div',
							{ className: 'modal-content-wrapper' },
							_react2.default.createElement(
								'div',
								{ className: 'form-control p-20' },
								_react2.default.createElement(
									'label',
									{ className: 'block-label', htmlFor: '' },
									this.props.label || 'Category Name'
								),
								_react2.default.createElement('input', {
									onChange: this.changeCategory,
									value: this.state.categoryName,
									type: 'text',
									className: 'input-control',
									autoFocus: true,
									placeholder: ''
								})
							)
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'modalFooter align-items-center justify-content-end d-flex' },
						_react2.default.createElement(
							'button',
							{
								type: 'submit',
								'data-shortcut': 'CTRL_S',
								className: 'modal-footer-button terminalButton'
							},
							'Save'
						)
					)
				)
			);
		}
	}]);
	return AddCategoryButton;
}(_react2.default.Component);

AddCategoryButton.propTypes = {
	categoryName: _propTypes2.default.string,
	label: _propTypes2.default.string,
	onClose: _propTypes2.default.func,
	onSave: _propTypes2.default.func,
	isOpen: _propTypes2.default.bool,
	title: _propTypes2.default.string
};

exports.default = AddCategoryButton;