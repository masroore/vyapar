Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _ErrorCode = require('../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

var _ToastHelper = require('../../UIControllers/ToastHelper');

var ToastHelper = _interopRequireWildcard(_ToastHelper);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LoanAccountList = function (_React$Component) {
	(0, _inherits3.default)(LoanAccountList, _React$Component);

	function LoanAccountList(props) {
		(0, _classCallCheck3.default)(this, LoanAccountList);

		var _this = (0, _possibleConstructorReturn3.default)(this, (LoanAccountList.__proto__ || (0, _getPrototypeOf2.default)(LoanAccountList)).call(this, props));

		_this.getApi = function (params) {
			_this.api = params.api;
			_this.api.setRowData(_this.props.items);
		};

		_this.columnDefs = [{
			field: 'accountName',
			headerName: 'ACCOUNT',
			width: 150,
			sort: 'asc'
		}, {
			field: 'currentBalance',
			suppressMenu: true,
			comparator: function comparator(a, b) {
				return a - b;
			},
			headerName: 'AMOUNT',
			width: 90,
			cellClass: 'alignRight',
			headerClass: 'alignRight',
			cellRenderer: function cellRenderer(params) {
				var value = MyDouble.getAmountWithDecimalAndCurrency(params.value);
				return '<div style="color:#e35959">' + value + '</div>';
			}
		}];
		_this.state = {
			isOpen: false,
			accountName: ''
		};
		_this.deleteAccount = _this.deleteAccount.bind(_this);
		_this.editAccount = _this.editAccount.bind(_this);
		_this.contextMenu = [{
			title: 'View/Edit',
			cmd: _this.editAccount,
			visible: true,
			id: 'edit'
		}, {
			title: 'Delete',
			cmd: _this.deleteAccount,
			visible: true,
			id: 'delete'
		}];
		return _this;
	}

	(0, _createClass3.default)(LoanAccountList, [{
		key: 'editAccount',
		value: function editAccount(row) {
			if (!row.accountName) {
				row = row.data;
			}
			this.props.onEditAccount && this.props.onEditAccount(row.accountId);
		}
	}, {
		key: 'deleteAccount',
		value: function deleteAccount(row) {
			var accountId = row.accountId;
			var LoanAccountCache = require('../../Cache/LoanAccountCache.js');
			var loanAccountCache = new LoanAccountCache();
			var loanAccount = loanAccountCache.getLoanAccountById(accountId);
			if (loanAccount) {
				var conf = confirm('Do you want to delete account?');
				if (conf) {
					var statusCode = loanAccount.deleteLoanAccount();
					if (statusCode == _ErrorCode2.default.ERROR_LOAN_ACCOUNT_DELETE_SUCCESS) {
						ToastHelper.success(statusCode);
					} else {
						ToastHelper.error(statusCode);
					}
					window.onResume && window.onResume();
				}
			} else {
				ToastHelper.error(_ErrorCode2.default.ERROR_BANK_USED_IN_TRANSACTION);
			}
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			setTimeout(function () {
				var firstColumn = document.querySelector('.left-column .ag-row-selected .ag-cell[col-id="accountName"]');
				firstColumn && firstColumn.focus();
			}, 100);
		}
	}, {
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(nextProps) {
			var _this2 = this;

			if (this.api) {
				this.api.setRowData(nextProps.items);
				setTimeout(function () {
					if (_this2.api.getSelectedRows().length === 0) {
						var selected = _this2.api.getDisplayedRowAtIndex(0);
						if (selected) {
							selected.setSelected(true);
						}
					}
				});
			}
			return false;
		}
	}, {
		key: 'render',
		value: function render() {
			var gridOptions = {
				enableSorting: true,
				rowSelection: 'single',
				columnDefs: this.columnDefs,
				rowData: [],
				deltaRowDataMode: true,
				onRowDoubleClicked: this.editAccount,
				suppressColumnMoveAnimation: true,
				onRowSelected: this.props.onItemSelected,
				overlayNoRowsTemplate: 'No loan accounts to show',
				headerHeight: 40,
				rowHeight: 40,
				getRowNodeId: function getRowNodeId(data) {
					return data.accountId;
				}
			};
			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement(
					'div',
					{ className: 'gridContainer d-flex' },
					_react2.default.createElement(_Grid2.default, {
						contextMenu: this.contextMenu,
						classes: 'noBorder gridRowHeight40',
						selectFirstRow: true,
						height: '100%',
						gridOptions: gridOptions,
						getApi: this.getApi
					})
				)
			);
		}
	}]);
	return LoanAccountList;
}(_react2.default.Component);

LoanAccountList.propTypes = {
	onItemSelected: _propTypes2.default.func,
	onEditAccount: _propTypes2.default.func,
	items: _propTypes2.default.array
};

exports.default = LoanAccountList;