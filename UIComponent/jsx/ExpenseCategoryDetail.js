Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles() {
	return {
		title: {
			color: '#333',
			fontFamily: 'robotoMedium',
			fontSize: '0.875rem',
			textTransform: 'uppercase',
			maxWidth: 400,
			overflow: 'hidden',
			whiteSpace: 'nowrap',
			textOverflow: 'ellipsis'
		},
		containerForItemList: {
			maxHeight: 80
		},
		expenseAmount: {
			marginRight: 10
		}
	};
};

var ExpenseCategoryDetail = function (_React$Component) {
	(0, _inherits3.default)(ExpenseCategoryDetail, _React$Component);

	function ExpenseCategoryDetail() {
		(0, _classCallCheck3.default)(this, ExpenseCategoryDetail);
		return (0, _possibleConstructorReturn3.default)(this, (ExpenseCategoryDetail.__proto__ || (0, _getPrototypeOf2.default)(ExpenseCategoryDetail)).apply(this, arguments));
	}

	(0, _createClass3.default)(ExpenseCategoryDetail, [{
		key: 'render',
		value: function render() {
			var classes = this.props.classes;
			var item = this.props.currentItem;

			if (!item || !item.categoryId) {
				return false;
			}
			return _react2.default.createElement(
				'div',
				{ className: 'containerForItemList ' + classes.containerForItemList },
				_react2.default.createElement(
					'div',
					{ className: 'mb-10 d-flex justify-content-between' },
					_react2.default.createElement(
						'div',
						{ id: 'currrentCategory', className: classes.title },
						item.categoryName
					)
				),
				_react2.default.createElement(
					'div',
					null,
					_react2.default.createElement(
						'div',
						null,
						_react2.default.createElement(
							'div',
							{ className: 'row' },
							_react2.default.createElement(
								'div',
								{ className: 'col' },
								_react2.default.createElement('span', {
									dangerouslySetInnerHTML: _MyDouble2.default.getBalanceAmountWithDecimalAndCurrency(item.categoryAmount, null, true)
								})
							)
						)
					)
				)
			);
		}
	}]);
	return ExpenseCategoryDetail;
}(_react2.default.Component);

ExpenseCategoryDetail.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	currentItem: _propTypes2.default.object
};

exports.default = (0, _styles.withStyles)(styles)(ExpenseCategoryDetail);