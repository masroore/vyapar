Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _SearchBox = require('./SearchBox');

var _SearchBox2 = _interopRequireDefault(_SearchBox);

var _BankAccountDetail = require('./BankAccountDetail');

var _BankAccountDetail2 = _interopRequireDefault(_BankAccountDetail);

var _ButtonDropdown = require('./ButtonDropdown');

var _ButtonDropdown2 = _interopRequireDefault(_ButtonDropdown);

var _BankTransactions = require('./BankTransactions');

var _BankTransactions2 = _interopRequireDefault(_BankTransactions);

var _BankAccountsList = require('./BankAccountsList');

var _BankAccountsList2 = _interopRequireDefault(_BankAccountsList);

var _CommonUtility = require('../../Utilities/CommonUtility');

var _CommonUtility2 = _interopRequireDefault(_CommonUtility);

var _FirstScreen = require('./UIControls/FirstScreen');

var _FirstScreen2 = _interopRequireDefault(_FirstScreen);

var _TransactionHelper = require('../../Utilities/TransactionHelper');

var _TxnTypeConstant2 = require('../../Constants/TxnTypeConstant');

var _TxnTypeConstant3 = _interopRequireDefault(_TxnTypeConstant2);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var BankAccountsContainer = function (_React$Component) {
	(0, _inherits3.default)(BankAccountsContainer, _React$Component);

	function BankAccountsContainer(props) {
		(0, _classCallCheck3.default)(this, BankAccountsContainer);

		var _this = (0, _possibleConstructorReturn3.default)(this, (BankAccountsContainer.__proto__ || (0, _getPrototypeOf2.default)(BankAccountsContainer)).call(this, props));

		_this.addNewAccount = function () {
			window.accountNameGlobal = '';
			_CommonUtility2.default.loadFrameDiv('NewBankAccount.html');
		};

		_this.filterItems = _this.filterItems.bind(_this);
		_this.onItemSelected = _this.onItemSelected.bind(_this);
		var bankAccounts = _this.getBankAccounts();
		var currentItem = null;
		_this.allBankAccounts = bankAccounts;
		_this.state = {
			records: [],
			bankAccounts: bankAccounts,
			currentItem: currentItem,
			contextMenu: _this.contextMenu,
			showSearch: false
		};
		window.onResume = _this.onResume.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(BankAccountsContainer, [{
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			delete window.onResume;
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			window.onResume = this.onResume.bind(this);
		}
	}, {
		key: 'onResume',
		value: function onResume() {
			var _this2 = this;

			var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

			this.props.onResume && this.props.onResume(notFromAutoSyncFlow);
			var bankAccounts = this.getBankAccounts();
			this.allBankAccounts = bankAccounts;
			if (this.state.currentItem) {
				var currentItem = this.allBankAccounts.find(function (bank) {
					return _this2.state.currentItem.accountId === bank.accountId;
				});
				this.setState({ currentItem: currentItem });
			}
			this.filterItems(this.state.searchText);
			this.onItemSelected(null, true, this.state.currentItem);
		}
	}, {
		key: 'getBankAccounts',
		value: function getBankAccounts() {
			var PaymentInfoCache = require('../../Cache/PaymentInfoCache');
			var paymentInfoCache = new PaymentInfoCache();
			paymentInfoCache.refreshPaymentInfoCache();
			var list = paymentInfoCache.getBankListObj();
			var bankAccounts = list.map(function (bank) {
				return {
					accountId: Number(bank.getId()),
					accountName: bank.getName(),
					currentBalance: bank.getCurrentBalance(),
					bankName: bank.getBankName(),
					accountNumber: bank.getAccountNumber()
				};
			});
			return bankAccounts;
		}
	}, {
		key: 'getBankTransactions',
		value: function getBankTransactions(account) {
			var records = [];
			if (account && account.accountId) {
				var _TxnTypeConstant = require('../../Constants/TxnTypeConstant');
				var ColorConstants = require('../../Constants/ColorConstants');
				var DataLoader = require('../../DBManager/DataLoader');
				var dataLoader = new DataLoader();
				var NameCache = require('../../Cache/NameCache.js');
				var nameCache = new NameCache();
				var txns = dataLoader.getBankDetailList(account.accountId, true);
				var PaymentInfoCache = require('../../Cache/PaymentInfoCache');
				var paymentInfoCache = new PaymentInfoCache();
				records = txns.map(function (txn) {
					var txnId = Number(txn.getTxnId());
					var typeTxn = _TxnTypeConstant.getTxnType(txn.getTxnType());
					var txnType = txn.getTxnType();
					var subTxnType = txn.getSubTxnType();
					var description = txn.getDescription();
					var color = ColorConstants[txnType];
					var name = '';
					if (txnType == _TxnTypeConstant.TXN_TYPE_BANK_TO_BANK) {
						if (txn.getFromBankId() == account.accountId) {
							name += 'To: ' + paymentInfoCache.getPaymentBankName(Number(txn.getToBankId()));
						} else if (txn.getToBankId() == account.accountId) {
							name += 'From: ' + paymentInfoCache.getPaymentBankName(Number(txn.getFromBankId()));
						}
						name += description ? ' ( ' + description + ' )' : '';
					} else {
						name = txn.getUserId() ? nameCache.findNameByNameId(txn.getUserId()) : txn.getDescription() ? txn.getDescription() : '';
					}
					var date = txn.getTxnDate();
					var amount = Number(txn.getAmount());
					var cashColor = '#1faf9d';
					var appliedTxnType = txn.getTxnType() == _TxnTypeConstant.TXN_TYPE_CHEQUE_TRANSFER ? Number(txn.getSubTxnType()) : txn.getTxnType();
					switch (appliedTxnType) {
						case _TxnTypeConstant.TXN_TYPE_PURCHASE:
						case _TxnTypeConstant.TXN_TYPE_CASHOUT:
						case _TxnTypeConstant.TXN_TYPE_EXPENSE:
						case _TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE:
						case _TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE_WITHOUT_CASH:
						case _TxnTypeConstant.TXN_TYPE_SALE_RETURN:
						case _TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
							cashColor = '#e35959';
							break;
						case _TxnTypeConstant.TXN_TYPE_BANK_OPENING:
							if (amount < 0) {
								cashColor = '#e35959';
							}
							break;
						case _TxnTypeConstant.TXN_TYPE_BANK_TO_BANK:
							if (txn.getFromBankId() == account.accountId) {
								cashColor = '#e35959';
							}
							break;
						case _TxnTypeConstant.TXN_TYPE_LOAN_STARTING_BALANCE:
						case _TxnTypeConstant.TXN_TYPE_LOAN_PROCESSING_FEE:
						case _TxnTypeConstant.TXN_TYPE_LOAN_ADJUSTMENT:
						case _TxnTypeConstant.TXN_TYPE_LOAN_EMI_PAYMENT:
						case _TxnTypeConstant.TXN_TYPE_LOAN_CLOSE_BOOK_ADJ:
							if (txnType == _TxnTypeConstant.TXN_TYPE_LOAN_EMI_PAYMENT || txnType == _TxnTypeConstant.TXN_TYPE_LOAN_PROCESSING_FEE) {
								cashColor = '#e35959';
							}
							break;
					}
					return {
						txnId: txnId,
						typeTxn: typeTxn,
						txnType: txnType,
						subTxnType: subTxnType,
						color: color,
						name: name,
						amount: amount,
						date: date,
						cashColor: cashColor
					};
				});
			}
			return records;
		}
	}, {
		key: 'onItemSelected',
		value: function onItemSelected(row) {
			var fromSync = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

			if (!fromSync) {
				if (!row.node.selected) {
					return false;
				}
			}
			var currentItem = this.state.currentItem;
			if (row && row.data) {
				currentItem = row.data;
			}
			var bankTransactions = this.getBankTransactions(currentItem);
			this.setState({
				records: bankTransactions,
				currentItem: currentItem
			});
		}
	}, {
		key: 'filterItems',
		value: function filterItems(searchText) {
			if (!searchText) {
				this.setState({ bankAccounts: this.allBankAccounts, searchText: '' });
				return false;
			}
			var query = searchText.toLowerCase();
			var filteredItems = this.allBankAccounts.filter(function (category) {
				return category.accountName.toLowerCase().includes(query);
			});
			var records = this.state.records;
			var currentItem = this.state.currentItem;
			if (!filteredItems.length) {
				records = [];
				currentItem = null;
			}
			this.setState({
				records: records,
				currentItem: currentItem,
				bankAccounts: filteredItems,
				searchText: searchText
			});
		}
	}, {
		key: 'getMenuOptions',
		value: function getMenuOptions() {
			var menuOptions = [];
			menuOptions.push({
				buttonText: 'Add Bank A/C',
				buttonIcon: _react2.default.createElement(
					'span',
					null,
					'+'
				),
				onClick: this.addNewAccount
			});
			return menuOptions;
		}
	}, {
		key: 'render',
		value: function render() {
			var _this3 = this;

			var _state = this.state,
			    bankAccounts = _state.bankAccounts,
			    records = _state.records,
			    currentItem = _state.currentItem;

			return _react2.default.createElement(
				'div',
				{ className: 'd-flex flex-column', style: { height: '100%' } },
				_react2.default.createElement(
					'ul',
					{ className: 'tab-container d-flex' },
					_react2.default.createElement(
						'li',
						{ className: 'tab-items col' },
						'Banks'
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'd-flex align-items-stretch flex-container' },
					this.allBankAccounts.length === 0 && _react2.default.createElement(_FirstScreen2.default, {
						image: '../inlineSVG/first-screens/bank.svg',
						text: 'Add your bank accounts & track bank transactions all at one place.',
						onClick: this.addNewAccount,
						buttonLabel: 'Add Your First Bank A/C'
					}),
					this.allBankAccounts.length > 0 && _react2.default.createElement(
						_react.Fragment,
						null,
						_react2.default.createElement(
							'div',
							{ className: 'flex-column d-flex left-column' },
							_react2.default.createElement(
								'div',
								{ className: 'listheaderDiv d-flex mt-20 mb-10' },
								_react2.default.createElement(
									'div',
									{
										className: (0, _classnames2.default)('searchDiv', this.state.showSearch ? 'width100 d-flex' : '')
									},
									_react2.default.createElement(_SearchBox2.default, {
										throttle: 500,
										filter: this.filterItems,
										collapsed: !this.state.showSearch,
										collapsedTextBoxClass: 'width100',
										onCollapsedClick: function onCollapsedClick() {
											_this3.setState({ showSearch: true });
										},
										onInputBlur: function onInputBlur() {
											_this3.setState({ showSearch: false });
										}
									})
								),
								_react2.default.createElement(
									'div',
									{ className: 'buttonDiv flex-1 d-flex justify-content-end' },
									_react2.default.createElement(_ButtonDropdown2.default, {
										menuOptions: this.getMenuOptions(),
										addPadding: true,
										collapsed: this.state.showSearch
									})
								)
							),
							_react2.default.createElement(_BankAccountsList2.default, {
								onItemSelected: this.onItemSelected,
								items: bankAccounts
							})
						),
						_react2.default.createElement(
							'div',
							{ className: 'd-flex flex-column flex-grow-1 right-column' },
							_react2.default.createElement(_BankAccountDetail2.default, { currentItem: currentItem }),
							_react2.default.createElement(_BankTransactions2.default, { items: records, addExtraColumns: true })
						)
					)
				)
			);
		}
	}]);
	return BankAccountsContainer;
}(_react2.default.Component);

BankAccountsContainer.propTypes = {
	onResume: _propTypes2.default.func
};

exports.default = BankAccountsContainer;