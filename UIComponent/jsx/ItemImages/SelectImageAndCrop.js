Object.defineProperty(exports, "__esModule", {
    value: true
});

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _ImageDialog = require('../UIControls/ImageDialog');

var _ImageDialog2 = _interopRequireDefault(_ImageDialog);

var _ItemImagesDisplay = require('./ItemImagesDisplay');

var _ItemImagesDisplay2 = _interopRequireDefault(_ItemImagesDisplay);

var _LocalStorageConstant = require('../../../Constants/LocalStorageConstant');

var _LocalStorageConstant2 = _interopRequireDefault(_LocalStorageConstant);

var _ItemImagesUtility = require('../../../Utilities/ItemImagesUtility');

var _ItemImagesUtility2 = _interopRequireDefault(_ItemImagesUtility);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
    return {
        addNewImage: {
            border: '1px dotted',
            width: 90,
            height: 90
        }
    };
};

var SelectImageAndCrop = function (_React$Component) {
    (0, _inherits3.default)(SelectImageAndCrop, _React$Component);

    function SelectImageAndCrop(props) {
        (0, _classCallCheck3.default)(this, SelectImageAndCrop);

        var _this = (0, _possibleConstructorReturn3.default)(this, (SelectImageAndCrop.__proto__ || (0, _getPrototypeOf2.default)(SelectImageAndCrop)).call(this, props));

        _this.state = {
            itemImageList: []
        };

        _this.getItemImageList = function () {
            var ItemImage = require('../../../BizLogic/ItemImages');
            var itemImage = new ItemImage();
            var imageList = itemImage.getItemImagesObjectListByItemId(_this.props.itemId);
            return imageList;
        };

        _this.updateImageList = function (imageList) {
            _this.setState({
                itemImageList: imageList
            }, function () {
                localStorage.setItem(_LocalStorageConstant2.default.ITEM_IMAGE_LIST, (0, _stringify2.default)(imageList));
            });
        };

        _this.clearImageFromList = function (itemImageObj) {
            var imageList = _this.state.itemImageList || [];
            imageList.splice(imageList.indexOf(itemImageObj), 1);
            _this.updateImageList(imageList);
        };

        _this.deleteItemImage = function (itemImageObj) {
            try {
                var deleteConf = confirm('Are you sure you want to delete this image?');
                if (deleteConf) {
                    if (itemImageObj.isNewImage) {
                        _this.clearImageFromList(itemImageObj);
                    } else {
                        var ItemImage = require('../../../BizLogic/ItemImages');
                        var itemImage = new ItemImage();
                        itemImage.setItemImageId(itemImageObj.id);
                        var deleteResult = itemImage.deleteItemImage();
                        if (deleteResult) {
                            _this.clearImageFromList(itemImageObj);
                        } else {
                            ToastHelper.error('Some error occurred in deleting item image. Please contact to Vyapar team.');
                        }
                    }
                }
            } catch (ex) {
                if (logger) {
                    logger.error('Error occurred in deleting item image', ex);
                }
            }
        };

        _this.handleImageClose = function () {
            _this.setState({ isImageDialogOpen: false });
        };

        _this.saveItemImageList = function (croppedImg) {
            var itemImageList = _this.state.itemImageList || [];
            itemImageList.push({ name: _this.imageName, image: croppedImg, isNewImage: true });
            _this.updateImageList(itemImageList);
        };

        _this.selectNewItemImage = function (e) {
            if (_this.state.itemImageList.length < 5) {
                var callback = function callback(result) {
                    _this.setState({ imgSrc: 'data:image/jpeg;base64,' + result, isImageDialogOpen: true });
                };
                _ItemImagesUtility2.default.selectImage(callback);
            } else {
                ToastHelper.error('You can add maximum 5 images per item.');
            }
        };

        _this.selectImageFile = _react2.default.createRef();
        _this.imageName = '';
        if (_this.props.itemId) {
            _this.state = { itemImageList: _this.getItemImageList(_this.props.itemId) };
            localStorage.setItem(_LocalStorageConstant2.default.ITEM_IMAGE_LIST, (0, _stringify2.default)(_this.state.itemImageList));
        }
        return _this;
    }

    (0, _createClass3.default)(SelectImageAndCrop, [{
        key: 'render',
        value: function render() {
            var classes = this.props.classes;

            return _react2.default.createElement(
                'div',
                { style: { padding: '10px 34px' } },
                _react2.default.createElement(
                    'p',
                    { className: 'mb-10 color-grey' },
                    'Attach Images',
                    _react2.default.createElement(
                        'span',
                        { className: 'font-10' },
                        ' (Max 5)'
                    )
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'd-flex' },
                    _react2.default.createElement(_ItemImagesDisplay2.default, { deleteItemImage: this.deleteItemImage, imageList: this.state.itemImageList }),
                    _react2.default.createElement(
                        'div',
                        { className: classes.addNewImage + ' d-flex align-items-center bg-color-d3d3d3 justify-content-center pointer', onClick: this.selectNewItemImage },
                        _react2.default.createElement('img', { src: '../inlineSVG/circular_add.svg' })
                    ),
                    this.state.isImageDialogOpen && _react2.default.createElement(_ImageDialog2.default, { callback: this.saveItemImageList, open: this.state.isImageDialogOpen, close: this.handleImageClose, img: this.state.imgSrc })
                )
            );
        }
    }]);
    return SelectImageAndCrop;
}(_react2.default.Component);

exports.default = (0, _styles.withStyles)(styles)(SelectImageAndCrop);