Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
    return {
        itemImage: {
            '& .delete-image': {
                position: 'absolute',
                top: 0,
                right: 5,
                width: 15,
                height: 15,
                display: 'none'
            },
            '&:hover .delete-image': {
                display: 'block'
            }
        }
    };
};

var ItemImages = function (_React$Component) {
    (0, _inherits3.default)(ItemImages, _React$Component);

    function ItemImages(props) {
        (0, _classCallCheck3.default)(this, ItemImages);
        return (0, _possibleConstructorReturn3.default)(this, (ItemImages.__proto__ || (0, _getPrototypeOf2.default)(ItemImages)).call(this, props));
    }

    (0, _createClass3.default)(ItemImages, [{
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps() {}
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var classes = this.props.classes;

            return _react2.default.createElement(
                'div',
                { className: 'd-flex align-items-center justify-content-center' },
                this.props.imageList.map(function (value, index) {
                    return _react2.default.createElement(
                        _react2.default.Fragment,
                        { key: index },
                        _react2.default.createElement(
                            'div',
                            { className: classes.itemImage + ' position-relative' },
                            _react2.default.createElement('img', { onClick: _this2.props.deleteItemImage.bind(_this2, value), src: '../inlineSVG/round-close.svg', className: classes.deleteImage + ' delete-image' }),
                            _react2.default.createElement('img', { key: index, style: { padding: 10, width: 90, height: 90 }, src: value.image })
                        )
                    );
                })
            );
        }
    }]);
    return ItemImages;
}(_react2.default.Component);

exports.default = (0, _styles.withStyles)(styles)(ItemImages);