Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _AddEditCategoryModal = require('./AddEditCategoryModal');

var _AddEditCategoryModal2 = _interopRequireDefault(_AddEditCategoryModal);

var _ButtonDropdown = require('./ButtonDropdown');

var _ButtonDropdown2 = _interopRequireDefault(_ButtonDropdown);

var _AddCircleOutline = require('@material-ui/icons/AddCircleOutline');

var _AddCircleOutline2 = _interopRequireDefault(_AddCircleOutline);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AddOtherIncomeButton = function (_React$Component) {
	(0, _inherits3.default)(AddOtherIncomeButton, _React$Component);

	function AddOtherIncomeButton(props) {
		(0, _classCallCheck3.default)(this, AddOtherIncomeButton);

		var _this = (0, _possibleConstructorReturn3.default)(this, (AddOtherIncomeButton.__proto__ || (0, _getPrototypeOf2.default)(AddOtherIncomeButton)).call(this, props));

		_this.state = {
			isOpen: false
		};
		_this.addNewCategory = _this.addNewCategory.bind(_this);
		_this.onSave = _this.onSave.bind(_this);
		_this.onClose = _this.onClose.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(AddOtherIncomeButton, [{
		key: 'addNewCategory',
		value: function addNewCategory() {
			this.setState({ isOpen: true });
			MyAnalytics.pushEvent('Add Other Income Category Open');
		}
	}, {
		key: 'getMenuOptions',
		value: function getMenuOptions() {
			var menuOptions = [];
			menuOptions.push({
				buttonText: 'Add Category',
				buttonIcon: _react2.default.createElement(
					'span',
					null,
					'+'
				),
				onClick: this.addNewCategory
			});
			return menuOptions;
		}
	}, {
		key: 'onSave',
		value: function onSave(name) {
			var ToastHelper = require('../../UIControllers/ToastHelper');
			name = name || '';
			name = name.trim();
			if (name) {
				var NameLogic = require('../../BizLogic/nameLogic.js');
				var NameType = require('../../Constants/NameType.js');
				var namelogic = new NameLogic();
				var obj = {
					name: name,
					phone_number: '',
					addressStr: '',
					email: '',
					groupId: '',
					tinNumber: '',
					opening_balance: '',
					opening_balance_date: '',
					isReceivable: true,
					nameType: NameType.NAME_TYPE_INCOME
				};
				var statusCode = namelogic.saveNewName(obj, false, true);
				if (statusCode == ErrorCode.ERROR_NAME_SAVE_SUCCESS) {
					statusCode = ErrorCode.ERROR_INCOME_SAVE_SUCCESS;
					ToastHelper.success(statusCode);
					MyAnalytics.pushEvent('Add Other Income Category Save');
				} else if (statusCode == ErrorCode.ERROR_NAME_ALREADY_EXISTS) {
					statusCode = ErrorCode.ERROR_INCOME_NAME_EXISTS;
					ToastHelper.error(statusCode);
				} else {
					statusCode = ErrorCode.ERROR_INCOME_SAVE_FAILED;
					ToastHelper.error(statusCode);
				}
				this.setState({
					isOpen: false
				});
				window.onResume && window.onResume();
			} else {
				ToastHelper.error('Income category name can not be empty');
			}
		}
	}, {
		key: 'onClose',
		value: function onClose() {
			this.setState({ isOpen: false });
		}
	}, {
		key: 'render',
		value: function render() {
			var isOpen = this.state.isOpen;

			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement(_AddEditCategoryModal2.default, {
					categoryName: '',
					onClose: this.onClose,
					onSave: this.onSave,
					isOpen: isOpen,
					title: 'Add Other Income',
					label: 'Enter Other Income Name'
				}),
				_react2.default.createElement(_ButtonDropdown2.default, {
					collapsed: this.props.collapsed,
					addPadding: true,
					menuOptions: this.getMenuOptions()
				})
			);
		}
	}]);
	return AddOtherIncomeButton;
}(_react2.default.Component);

AddOtherIncomeButton.propTypes = {
	classNames: _propTypes2.default.string,
	collapsed: _propTypes2.default.bool
};

exports.default = AddOtherIncomeButton;