Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _trashableReact = require('trashable-react');

var _trashableReact2 = _interopRequireDefault(_trashableReact);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _MyDate = require('../../Utilities/MyDate');

var _MyDate2 = _interopRequireDefault(_MyDate);

var _TransactionHelper = require('../../Utilities/TransactionHelper');

var TransactionHelper = _interopRequireWildcard(_TransactionHelper);

var _DateFilter = require('./grid-filters/DateFilter');

var _DateFilter2 = _interopRequireDefault(_DateFilter);

var _NumberFilter = require('./grid-filters/NumberFilter');

var _NumberFilter2 = _interopRequireDefault(_NumberFilter);

var _StringFilter = require('./grid-filters/StringFilter');

var _StringFilter2 = _interopRequireDefault(_StringFilter);

var _MultiSelectionFilter = require('./grid-filters/MultiSelectionFilter');

var _MultiSelectionFilter2 = _interopRequireDefault(_MultiSelectionFilter);

var _NewTransactionButton = require('./NewTransactionButton');

var _NewTransactionButton2 = _interopRequireDefault(_NewTransactionButton);

var _styles = require('@material-ui/core/styles');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _FirstScreen = require('./UIControls/FirstScreen');

var _FirstScreen2 = _interopRequireDefault(_FirstScreen);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles() {
	return {
		btn: {
			height: 28,
			borderRadius: 5,
			whiteSpace: 'nowrap',
			'&:hover': {
				boxShadow: 'none'
			}
		},
		btnIcon: {
			height: 12,
			marginRight: 5,
			transform: 'rotate(90deg)'
		}
	};
};

var CashInHandContainer = function (_React$Component) {
	(0, _inherits3.default)(CashInHandContainer, _React$Component);

	function CashInHandContainer(props) {
		(0, _classCallCheck3.default)(this, CashInHandContainer);

		var _this = (0, _possibleConstructorReturn3.default)(this, (CashInHandContainer.__proto__ || (0, _getPrototypeOf2.default)(CashInHandContainer)).call(this, props));

		_this.showAdjustCashInHand = function (showDialog) {
			var adjustCash = require('../../Utilities/AdjustCashInHand');
			adjustCash.loadAdjustCashInHand(function () {
				return _this.refresh();
			}, !!showDialog);
		};

		_this.getApi = function (params) {
			_this.api = params.api;
		};

		var txns = _this.getCashInHandTxns();
		var currentCashInHand = _this.getCurrentCashInHand();
		_this.contextMenu = [{
			title: 'View/Edit',
			cmd: _this.viewTransaction,
			visible: true,
			id: 'edit'
		}, {
			title: 'Delete',
			cmd: function cmd(row) {
				return TransactionHelper.deleteTransaction(row.txnId, TxnTypeConstant.getTxnType(row.txnType));
			},
			visible: true,
			id: 'delete'
		}, {
			title: 'Print',
			cmd: function cmd(row) {
				return TransactionHelper.print(row.txnId, TxnTypeConstant.getTxnType(row.txnType));
			},
			visible: true,
			id: 'print'
		}];
		_this.state = {
			records: txns,
			columnDefs: [{
				field: 'color',
				headerName: '',
				width: 25,
				suppressMenu: false,
				suppressSorting: true,
				getQuickFilterText: function getQuickFilterText() {
					return '';
				},
				suppressFilter: true,
				cellRenderer: function cellRenderer(params) {
					return '<div style=\'background-color: ' + params.value + '; border-radius:50%;width:8px;height:8px; top: 19px; position:relative;\'></div>';
				}
			}, {
				field: 'txnType',
				filter: _MultiSelectionFilter2.default,
				filterOptions: TxnTypeConstant.getFilterOptionsForCashInHand(),
				getQuickFilterText: function getQuickFilterText(params) {
					return TxnTypeConstant.getTxnTypeForUI(params.data.txnType, params.data.subTxnType);
				},
				cellRenderer: function cellRenderer(params) {
					return TxnTypeConstant.getTxnTypeForUI(params.value, params.data.subTxnType);
				},
				headerName: 'TYPE',
				width: 110
			}, {
				field: 'name',
				filter: _StringFilter2.default,
				headerName: 'NAME',
				width: 180
			}, {
				field: 'dates',
				headerName: 'DATE',
				width: 100,
				comparator: function comparator(a, b) {
					var txnDateA = a['txnDate'];
					var modifiedDateA = a['lastModifiedDate'];
					var txnDateB = b['txnDate'];
					var modifiedDateB = b['lastModifiedDate'];
					if (txnDateA < txnDateB || txnDateA.getTime() === txnDateB.getTime() && modifiedDateA < modifiedDateB) {
						return -1;
					} else {
						return 1;
					}
				},
				filterValueGetter: function filterValueGetter(params) {
					return params.data.dates['txnDate'];
				},
				sort: 'desc',
				filter: _DateFilter2.default,
				sortingOrder: ['desc', 'asc', null],
				getQuickFilterText: function getQuickFilterText(params) {
					return _MyDate2.default.getDate('d/m/y', params.value['txnDate']);
				},
				cellRenderer: function cellRenderer(params) {
					return _MyDate2.default.getDate('d/m/y', params.value['txnDate']);
				}
			}, {
				field: 'amount',
				headerName: 'AMOUNT',
				width: 150,
				filter: _NumberFilter2.default,
				cellClass: 'alignRight',
				getQuickFilterText: function getQuickFilterText(params) {
					return params.value;
				},
				cellRenderer: function cellRenderer(params) {
					return _MyDouble2.default.getBalanceAmountWithDecimalAndCurrencyForTxnType(params.value, params.data['txnType'], params.data['subTxnType'], true, false);
				}
			}, {
				field: 'txnId',
				headerName: '',
				width: 50,
				getQuickFilterText: function getQuickFilterText() {
					return '';
				},
				suppressFilter: true,
				suppressSorting: true,
				cellRenderer: function cellRenderer() {
					return '<div class="contextMenuDots"></div>';
				}
			}],
			currentCashInHand: currentCashInHand,
			contextMenu: _this.contextMenu
		};
		return _this;
	}

	(0, _createClass3.default)(CashInHandContainer, [{
		key: 'getCurrentCashInHand',
		value: function getCurrentCashInHand() {
			var Dataloader = require('../../DBManager/DataLoader');
			var dataLoader = new Dataloader();
			return dataLoader.getCashInHandAmount();
		}
	}, {
		key: 'getCashInHandTxns',
		value: function getCashInHandTxns() {
			var Dataloader = require('../../DBManager/DataLoader');
			var dataLoader = new Dataloader();
			var TxnTypeConstant = require('../../Constants/TxnTypeConstant');
			var ColorConstants = require('../../Constants/ColorConstants');
			var NameCache = require('../../Cache/NameCache');
			var PaymentInfoCache = require('../../Cache/PaymentInfoCache');
			var nameCache = new NameCache();
			var paymentInfoCache = new PaymentInfoCache();
			var CIHList = dataLoader.getCashInHandTxnList();
			var records = [];
			records = CIHList.map(function (cashTxn) {
				// id, name, type, amount, date, color
				var txnId = '';
				var name = '';
				var type = '';
				var amount = '';
				var dates = {};
				var color = void 0;
				txnId = Number(cashTxn.getTxnId());
				var subTxnType = cashTxn.getSubTxnType();
				var typeId = Number(cashTxn.getTxnType());
				type = TxnTypeConstant.getTxnTypeForUI(typeId, cashTxn.getSubTxnType());
				dates['txnDate'] = cashTxn.getTxnDate();
				dates['lastModifiedDate'] = cashTxn['lastModifiedDate'];
				color = ColorConstants[typeId];

				var nameObject = nameCache.findNameObjectByNameId(cashTxn.getUserId());
				var bankName = paymentInfoCache.getPaymentBankName(cashTxn.getBankId());
				if (nameObject) {
					name = nameObject.getFullName();
				} else if (bankName) {
					name = bankName;
				} else {
					name = cashTxn.getDescription();
				}
				amount = Number(cashTxn.getAmount());
				return {
					txnId: txnId,
					name: name,
					type: type,
					amount: amount,
					dates: dates,
					color: color,
					txnType: typeId,
					subTxnType: subTxnType
				};
			});

			if (this.api) {
				this.api.setRowData(records);
			}
			return records;
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			TransactionHelper.bindEventsForPreviewDialog();
			MyAnalytics.pushScreen('Cash In Hand');
			window.onResume = this.onResume.bind(this);
			this.showAdjustCashInHand(this.props.showAdjustCashInHand);
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			TransactionHelper.unBindEventsForPreviewDialog();
			delete window.onResume;
		}
	}, {
		key: 'onResume',
		value: function onResume() {
			var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

			this.refresh();
			TransactionHelper.bindEventsForPreviewDialog();
		}
	}, {
		key: 'refresh',
		value: function refresh() {
			var txns = this.getCashInHandTxns();
			var currentCashInHand = this.getCurrentCashInHand();
			this.setState({
				records: txns,
				currentCashInHand: currentCashInHand
			});
		}
	}, {
		key: 'viewTransaction',
		value: function viewTransaction(row) {
			if (!row.txnId) {
				row = row.data;
			}
			var txnTypeConstant = TxnTypeConstant.getTxnType(row.txnType);
			TransactionHelper.viewTransaction(row.txnId, txnTypeConstant);
		}
	}, {
		key: 'onTransactionSelected',
		value: function onTransactionSelected(row) {
			this.currentTransaction = row;
		}
	}, {
		key: 'initializeFilter',
		value: function initializeFilter(params) {
			var typeIdInstance = params.api.getFilterInstance('typeId');
			if (typeIdInstance) {
				typeIdInstance.componentInstance.applyFilter();
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var _state = this.state,
			    records = _state.records,
			    columnDefs = _state.columnDefs;

			var gridOptions = {
				enableFilter: true,
				enableSorting: true,
				rowSelection: 'single',
				columnDefs: columnDefs,
				rowData: records,
				rowHeight: 40,
				headerHeight: 40,
				rowClass: 'customVerticalBorder',
				onRowSelected: this.onTransactionSelected.bind(this),
				onRowDoubleClicked: this.viewTransaction.bind(this),
				onGridReadyCallback: this.initializeFilter.bind(this)
			};
			var classes = this.props.classes;
			return _react2.default.createElement(
				'div',
				{ className: 'd-flex flex-column', style: { height: '100%' } },
				_react2.default.createElement(
					'div',
					{ className: 'd-flex txnHeaderWrapper' },
					_react2.default.createElement(
						'div',
						{ className: 'txnHeader' },
						'CASH IN HAND'
					),
					_react2.default.createElement(
						'div',
						{
							style: {
								paddingLeft: 24,
								fontFamily: 'robotoMedium'
							}
						},
						_react2.default.createElement('span', {
							dangerouslySetInnerHTML: {
								__html: _MyDouble2.default.getBalanceAmountWithDecimalAndCurrency(this.state.currentCashInHand, true)
							}
						})
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'd-flex flex-column flex-container flex-grow-1' },
					records.length === 0 && _react2.default.createElement(_FirstScreen2.default, {
						image: '../inlineSVG/first-screens/cash-in-hand.svg',
						text: 'Whenever you choose payment type as cash in your invoices, that amount will be reflected in cash in hand.',
						buttonLabel: 'Adjust Cash',
						onClick: this.showAdjustCashInHand
					}),
					records.length > 0 && _react2.default.createElement(_Grid2.default, {
						quickFilter: true,
						CTAButton: _react2.default.createElement(_NewTransactionButton2.default, {
							handleClick: this.showAdjustCashInHand,
							label: _react2.default.createElement(
								'div',
								{
									className: (0, _classnames2.default)('d-flex align-items-center', classes.btn)
								},
								_react2.default.createElement('img', {
									src: './../inlineSVG/item_adj.svg',
									className: classes.btnIcon
								}),
								'Adjust Cash'
							)
						}),
						classes: 'alternateRowColor customVerticalBorder gridRowHeight40',
						contextMenu: this.state.contextMenu,
						gridOptions: gridOptions,
						title: 'Transactions',
						getApi: this.getApi
					})
				),
				_react2.default.createElement('div', { id: 'adjustCashWrapper' })
			);
		}
	}]);
	return CashInHandContainer;
}(_react2.default.Component);

CashInHandContainer.propTypes = {
	showAdjustCashInHand: _propTypes2.default.bool
};
exports.default = (0, _styles.withStyles)(styles)((0, _trashableReact2.default)(CashInHandContainer));