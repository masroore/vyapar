Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _NewTransactionButton = require('./NewTransactionButton');

var _NewTransactionButton2 = _interopRequireDefault(_NewTransactionButton);

var _TxnTypeConstant = require('../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles() {
	return {
		title: {
			color: '#333',
			fontFamily: 'robotoMedium',
			fontSize: '0.875rem',
			textTransform: 'uppercase',
			maxWidth: 400,
			overflow: 'hidden',
			whiteSpace: 'nowrap',
			textOverflow: 'ellipsis'
		},
		containerForItemList: {
			maxHeight: 80
		},
		expenseAmount: {
			marginRight: 10
		}
	};
};

var OtherIncomeDetail = function (_React$Component) {
	(0, _inherits3.default)(OtherIncomeDetail, _React$Component);

	function OtherIncomeDetail() {
		(0, _classCallCheck3.default)(this, OtherIncomeDetail);
		return (0, _possibleConstructorReturn3.default)(this, (OtherIncomeDetail.__proto__ || (0, _getPrototypeOf2.default)(OtherIncomeDetail)).apply(this, arguments));
	}

	(0, _createClass3.default)(OtherIncomeDetail, [{
		key: 'render',
		value: function render() {
			var classes = this.props.classes;
			var item = this.props.currentItem;

			if (!item || !item.incomeId) {
				return false;
			}

			return _react2.default.createElement(
				'div',
				{ className: 'containerForItemList ' + classes.containerForItemList },
				_react2.default.createElement(
					'div',
					{ className: 'mb-10 d-flex justify-content-between' },
					_react2.default.createElement(
						'div',
						{ id: 'currrentCategory', className: classes.title },
						item.incomeName
					),
					_react2.default.createElement(
						'div',
						null,
						_react2.default.createElement(_NewTransactionButton2.default, {
							txnType: _TxnTypeConstant2.default.getTxnType(_TxnTypeConstant2.default.TXN_TYPE_OTHER_INCOME),
							label: '+ Add ' + _TxnTypeConstant2.default.getTxnTypeForUI(_TxnTypeConstant2.default.TXN_TYPE_OTHER_INCOME)
						})
					)
				),
				_react2.default.createElement(
					'div',
					null,
					_react2.default.createElement(
						'div',
						null,
						_react2.default.createElement(
							'div',
							{ className: 'row' },
							_react2.default.createElement(
								'div',
								{ className: 'col' },
								_react2.default.createElement('span', {
									dangerouslySetInnerHTML: _MyDouble2.default.getBalanceAmountWithDecimalAndCurrency(item.incomeAmount, null, true)
								})
							)
						)
					)
				)
			);
		}
	}]);
	return OtherIncomeDetail;
}(_react2.default.Component);

OtherIncomeDetail.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	currentItem: _propTypes2.default.object
};

exports.default = (0, _styles.withStyles)(styles)(OtherIncomeDetail);