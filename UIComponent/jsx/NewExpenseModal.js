Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Dialog = require('@material-ui/core/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _DialogTitle = require('@material-ui/core/DialogTitle');

var _DialogTitle2 = _interopRequireDefault(_DialogTitle);

var _DialogActions = require('@material-ui/core/DialogActions');

var _DialogActions2 = _interopRequireDefault(_DialogActions);

var _DialogContent = require('@material-ui/core/DialogContent');

var _DialogContent2 = _interopRequireDefault(_DialogContent);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _TextField = require('@material-ui/core/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _TxnTypeConstant = require('../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _SalePurchaseHelpers = require('./SalePurchaseContainer/SalePurchaseHelpers');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var NewExpenseModal = function (_React$Component) {
	(0, _inherits3.default)(NewExpenseModal, _React$Component);

	function NewExpenseModal(props) {
		(0, _classCallCheck3.default)(this, NewExpenseModal);

		var _this = (0, _possibleConstructorReturn3.default)(this, (NewExpenseModal.__proto__ || (0, _getPrototypeOf2.default)(NewExpenseModal)).call(this, props));

		_this.state = {
			partyName: props.partyName || ''
		};
		_this.onChange = _this.onChange.bind(_this);
		_this.onSave = _this.onSave.bind(_this);
		_this.onClose = _this.onClose.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(NewExpenseModal, [{
		key: 'onChange',
		value: function onChange(e) {
			this.setState({
				partyName: e.target.value
			});
		}
	}, {
		key: 'onSave',
		value: function onSave() {
			var partyName = this.state.partyName;

			var statusCode = (0, _SalePurchaseHelpers.saveNewName)(partyName, _TxnTypeConstant2.default.TXN_TYPE_EXPENSE);
			if (statusCode) {
				var NameCache = require('../../Cache/NameCache');
				var nameCache = new NameCache();
				var logic = nameCache.findExpenseModelByName(partyName);
				this.props.onClose(logic);
			}
		}
	}, {
		key: 'onClose',
		value: function onClose() {
			this.props.onClose();
		}
	}, {
		key: 'render',
		value: function render() {
			var partyName = this.state.partyName;
			var isOpen = this.props.isOpen;


			return _react2.default.createElement(
				_Dialog2.default,
				{
					maxWidth: 'md',
					open: isOpen,
					disableEnforceFocus: true,
					onClose: this.onClose
				},
				_react2.default.createElement(
					_DialogTitle2.default,
					null,
					'Add New Expense'
				),
				_react2.default.createElement(
					_DialogContent2.default,
					null,
					_react2.default.createElement(_TextField2.default, {
						autoFocus: true,
						onChange: this.onChange,
						value: partyName
					})
				),
				_react2.default.createElement(
					_DialogActions2.default,
					null,
					_react2.default.createElement(
						_Button2.default,
						{ onClick: this.onClose, color: 'default' },
						'Close'
					),
					_react2.default.createElement(
						_Button2.default,
						{ onClick: this.onSave, color: 'primary' },
						'Save'
					)
				)
			);
		}
	}]);
	return NewExpenseModal;
}(_react2.default.Component);

NewExpenseModal.propTypes = {
	partyName: _propTypes2.default.string,
	onClose: _propTypes2.default.func,
	isOpen: _propTypes2.default.bool
};

exports.default = NewExpenseModal;