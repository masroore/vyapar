Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _SettingCache = require('../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

var _styles = require('@material-ui/core/styles');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _WhatsAppIcon = require('./icons/WhatsAppIcon');

var _WhatsAppIcon2 = _interopRequireDefault(_WhatsAppIcon);

var _Sms = require('@material-ui/icons/Sms');

var _Sms2 = _interopRequireDefault(_Sms);

var _AlarmAdd = require('@material-ui/icons/AlarmAdd');

var _AlarmAdd2 = _interopRequireDefault(_AlarmAdd);

var _Tooltip = require('@material-ui/core/Tooltip');

var _Tooltip2 = _interopRequireDefault(_Tooltip);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles() {
	return {
		title: {
			color: '#333',
			fontFamily: 'robotoMedium',
			fontSize: '0.875rem',
			textTransform: 'uppercase',
			maxWidth: 400,
			overflow: 'hidden',
			whiteSpace: 'nowrap',
			textOverflow: 'ellipsis'
		},
		icon: {
			cursor: 'pointer'
		},
		whatsappIcon: {
			position: 'relative',
			top: -4,
			marginRight: 14
		},
		smsIcon: {
			color: '#F2B973',
			marginRight: 14
		},
		tooltip: {
			fontSize: 12
		}
	};
};

var ItemDetail = function (_React$Component) {
	(0, _inherits3.default)(ItemDetail, _React$Component);

	function ItemDetail(props) {
		(0, _classCallCheck3.default)(this, ItemDetail);

		var _this = (0, _possibleConstructorReturn3.default)(this, (ItemDetail.__proto__ || (0, _getPrototypeOf2.default)(ItemDetail)).call(this, props));

		_this.setPaymentReminder = _this.setPaymentReminder.bind(_this);
		_this.sendSMS = _this.sendReminder('sms');
		_this.sendWhatsApp = _this.sendReminder('whatsapp');
		return _this;
	}

	(0, _createClass3.default)(ItemDetail, [{
		key: 'setPaymentReminder',
		value: function setPaymentReminder() {
			var SetPaymentReminderController = require('../../UIControllers/SetPartyPaymentReminderController');
			SetPaymentReminderController({
				mountPoint: $('#setPaymentReminderDialog'),
				partyId: this.props.currentItem.nameId
			});
		}
	}, {
		key: 'sendReminder',
		value: function sendReminder(type) {
			var _this2 = this;

			return function () {
				var MessageDraftLogic = require('../../BizLogic/MessageDraftLogic');
				var messageDraftLogic = new MessageDraftLogic();
				var currentItem = _this2.props.currentItem;
				var nameObj = {
					getNameId: function getNameId() {
						return currentItem.nameId;
					},
					getPhoneNumber: function getPhoneNumber() {
						return currentItem.phoneNumber;
					},
					getFullName: function getFullName() {
						return currentItem.fullName;
					},
					getAmount: function getAmount() {
						return currentItem.amount;
					}
				};
				messageDraftLogic.sendPaymentReminderMessage(nameObj, type, true, true);
			};
		}
	}, {
		key: 'render',
		value: function render() {
			var settingCache = new _SettingCache2.default();
			var classes = this.props.classes;

			var item = this.props.currentItem;
			var tinEnabled = settingCache.isTINNumberEnabled();
			var isGSTEnabled = settingCache.getGSTEnabled();
			var isCurrentCountryIndia = settingCache.isCurrentCountryIndia();
			var isPartyReceiable = item && item.amount > 0;
			if (!item || !item.nameId) {
				return false;
			}

			return _react2.default.createElement(
				'div',
				{ className: 'containerForItemList' },
				_react2.default.createElement(
					'div',
					{ className: 'mb-10 d-flex justify-content-between' },
					_react2.default.createElement(
						'div',
						{ id: 'currentItemName', className: classes.title },
						item.fullName
					),
					isPartyReceiable > 0 && _react2.default.createElement(
						'div',
						null,
						isCurrentCountryIndia && _react2.default.createElement(
							_Tooltip2.default,
							{ classes: { tooltip: classes.tooltip }, title: 'Send Payment Reminder sms to party' },
							_react2.default.createElement(_Sms2.default, { onClick: this.sendSMS, className: (0, _classnames2.default)(classes.icon, classes.smsIcon), color: 'action' })
						),
						_react2.default.createElement(
							_Tooltip2.default,
							{ classes: { tooltip: classes.tooltip }, title: 'Send Payment Reminder Whatsapp to party' },
							_react2.default.createElement(
								'span',
								{ onClick: this.sendWhatsApp, className: (0, _classnames2.default)(classes.icon, classes.whatsappIcon) },
								_react2.default.createElement(_WhatsAppIcon2.default, null)
							)
						),
						_react2.default.createElement(
							_Tooltip2.default,
							{ classes: { tooltip: classes.tooltip }, title: 'Set Reminder' },
							_react2.default.createElement(_AlarmAdd2.default, { titleAccess: 'Set Reminder', onClick: this.setPaymentReminder, color: 'disabled', className: (0, _classnames2.default)(classes.icon) })
						)
					)
				),
				_react2.default.createElement(
					'div',
					null,
					_react2.default.createElement(
						'div',
						null,
						_react2.default.createElement(
							'div',
							{ className: 'row' },
							_react2.default.createElement(
								'div',
								{ className: 'col' },
								_react2.default.createElement(
									'span',
									null,
									'PHONE: ',
									item.phoneNumber
								)
							),
							_react2.default.createElement(
								'div',
								{ className: 'col d-flex justify-content-end' },
								_react2.default.createElement(
									'span',
									null,
									'ADDRESS: ',
									item.address
								)
							)
						),
						_react2.default.createElement(
							'div',
							{ className: 'row' },
							_react2.default.createElement(
								'div',
								{ className: 'col' },
								_react2.default.createElement(
									'span',
									null,
									'EMAIL: ',
									item.email
								)
							),
							_react2.default.createElement(
								'div',
								{ className: 'col d-flex justify-content-end' },
								tinEnabled && !isGSTEnabled && _react2.default.createElement(
									'span',
									null,
									settingCache.getTINText(),
									': ',
									item.tinNumber
								),
								tinEnabled && isGSTEnabled && _react2.default.createElement(
									'span',
									null,
									'GSTIN: ',
									item.gstinNumber
								)
							)
						)
					)
				)
			);
		}
	}]);
	return ItemDetail;
}(_react2.default.Component);

ItemDetail.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	currentItem: _propTypes2.default.object
};

exports.default = (0, _styles.withStyles)(styles)(ItemDetail);