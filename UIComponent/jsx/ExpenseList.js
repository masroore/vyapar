Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _DateFilter = require('./grid-filters/DateFilter');

var _DateFilter2 = _interopRequireDefault(_DateFilter);

var _NumberFilter = require('./grid-filters/NumberFilter');

var _NumberFilter2 = _interopRequireDefault(_NumberFilter);

var _TransactionHelper = require('../../Utilities/TransactionHelper');

var TransactionHelper = _interopRequireWildcard(_TransactionHelper);

var _MyDate = require('../../Utilities/MyDate');

var _MyDate2 = _interopRequireDefault(_MyDate);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _NewTransactionButton = require('./NewTransactionButton');

var _NewTransactionButton2 = _interopRequireDefault(_NewTransactionButton);

var _TxnTypeConstant = require('../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ExpenseList = function (_React$Component) {
	(0, _inherits3.default)(ExpenseList, _React$Component);

	function ExpenseList(props) {
		(0, _classCallCheck3.default)(this, ExpenseList);

		var _this = (0, _possibleConstructorReturn3.default)(this, (ExpenseList.__proto__ || (0, _getPrototypeOf2.default)(ExpenseList)).call(this, props));

		_this.getApi = function (params) {
			_this.api = params.api;
			_this.api.setRowData(_this.props.items);
		};

		_this.columnDefs = [{
			field: 'date',
			headerName: 'DATE',
			sort: 'desc',
			sortingOrder: ['desc', 'asc', null],
			filter: _DateFilter2.default,
			getQuickFilterText: function getQuickFilterText(params) {
				if (params.value) {
					return _MyDate2.default.getDate('d/m/y', new Date(params.value));
				}
			},
			cellRenderer: function cellRenderer(params) {
				if (params.value) {
					return _MyDate2.default.getDate('d/m/y', new Date(params.value));
				}
			}
		}, {
			field: 'amount',
			filter: _NumberFilter2.default,
			headerName: 'AMOUNT',
			cellClass: 'alignLeft',
			minWidth: 100,
			headerClass: 'alignLeft',
			cellRenderer: function cellRenderer(params) {
				return _MyDouble2.default.getBalanceAmountWithDecimalAndCurrencyWithoutColor(params.value);
			}
		}, {
			field: 'txnId',
			headerName: '',
			width: 30,
			getQuickFilterText: function getQuickFilterText() {
				return '';
			},
			suppressSorting: true,
			suppressFilter: true,
			cellRenderer: function cellRenderer() {
				return '<div class="contextMenuDots"></div>';
			}
		}];
		_this.contextMenu = [{
			title: 'View/Edit',
			cmd: ExpenseList.viewTransaction,
			visible: true,
			id: 'edit'
		}, {
			title: 'Delete',
			cmd: function cmd(row) {
				return TransactionHelper.deleteTransaction(row.txnId, row.typeTxn);
			},
			visible: true,
			id: 'delete'
		}];
		_this.state = {
			contextMenu: _this.contextMenu
		};
		_this.firstTime = true;
		return _this;
	}

	(0, _createClass3.default)(ExpenseList, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			setTimeout(function () {
				var firstColumn = document.querySelector('.left-column .ag-row-selected .ag-cell[col-id="fullName"]');
				firstColumn && firstColumn.focus();
			}, 500);
		}
	}, {
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(nextProps) {
			if (this.api) {
				this.api.setRowData(nextProps.items);
			}
			return false;
		}
	}, {
		key: 'render',
		value: function render() {
			var addExtraColumns = this.props.addExtraColumns;

			var gridOptions = {
				enableFilter: true,
				enableSorting: true,
				rowSelection: 'single',
				columnDefs: this.columnDefs,
				rowData: [],
				onRowDoubleClicked: ExpenseList.viewTransaction,
				suppressColumnMoveAnimation: true,
				overlayNoRowsTemplate: 'No expenses to show',
				headerHeight: 40,
				rowClass: addExtraColumns ? 'customVerticalBorder' : '',
				rowHeight: 40,
				getRowNodeId: function getRowNodeId(data) {
					return data.txnId;
				}
			};
			var gridClasses = 'noBorder gridRowHeight40';
			var selectFirstRow = true;
			var quickFilter = false;
			var title = '';
			if (addExtraColumns) {
				gridClasses = 'alternateRowColor customVerticalBorder gridRowHeight40';
				selectFirstRow = false;
				quickFilter = true;
				title = 'Expenses';
			}

			return _react2.default.createElement(
				'div',
				{ className: 'gridContainer d-flex' },
				_react2.default.createElement(_Grid2.default, {
					CTAButton: _react2.default.createElement(_NewTransactionButton2.default, {
						txnType: _TxnTypeConstant2.default.getTxnType(_TxnTypeConstant2.default.TXN_TYPE_EXPENSE),
						label: '+ Add ' + _TxnTypeConstant2.default.getTxnTypeForUI(_TxnTypeConstant2.default.TXN_TYPE_EXPENSE)
					}),
					contextMenu: this.state.contextMenu,
					classes: gridClasses,
					selectFirstRow: selectFirstRow,
					title: title,
					quickFilter: quickFilter,
					height: '100%',
					width: '100%',
					gridOptions: gridOptions,
					getApi: this.getApi
				})
			);
		}
	}], [{
		key: 'viewTransaction',
		value: function viewTransaction(row) {
			if (!row.txnId) {
				row = row.data;
			}
			TransactionHelper.viewTransaction(row.txnId, row.typeTxn);
		}
	}]);
	return ExpenseList;
}(_react2.default.Component);

ExpenseList.propTypes = {
	items: _propTypes2.default.array,
	addExtraColumns: _propTypes2.default.bool
};

exports.default = ExpenseList;