Object.defineProperty(exports, "__esModule", {
	value: true
});

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _trashableReact = require('trashable-react');

var _trashableReact2 = _interopRequireDefault(_trashableReact);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _SearchBox = require('./SearchBox');

var _SearchBox2 = _interopRequireDefault(_SearchBox);

var _NameGroupDetail = require('./NameGroupDetail');

var _NameGroupDetail2 = _interopRequireDefault(_NameGroupDetail);

var _AddNameGroupButton = require('./AddNameGroupButton');

var _AddNameGroupButton2 = _interopRequireDefault(_AddNameGroupButton);

var _NameList = require('./NameList');

var _NameList2 = _interopRequireDefault(_NameList);

var _NameGroupList = require('./NameGroupList');

var _NameGroupList2 = _interopRequireDefault(_NameGroupList);

var _IPCActions = require('../../Constants/IPCActions');

var _throttleDebounce = require('throttle-debounce');

var _DBWindow = require('../../Utilities/DBWindow');

var _DBWindow2 = _interopRequireDefault(_DBWindow);

var _NameType = require('../../Constants/NameType');

var _NameType2 = _interopRequireDefault(_NameType);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var NamesGroupContainer = function (_React$Component) {
	(0, _inherits3.default)(NamesGroupContainer, _React$Component);

	function NamesGroupContainer(props) {
		(0, _classCallCheck3.default)(this, NamesGroupContainer);

		var _this = (0, _possibleConstructorReturn3.default)(this, (NamesGroupContainer.__proto__ || (0, _getPrototypeOf2.default)(NamesGroupContainer)).call(this, props));

		_this.filterItems = _this.filterItems.bind(_this);
		_this.onItemSelected = (0, _throttleDebounce.debounce)(50, _this.onItemSelected.bind(_this));
		_this.getData = _this.getData.bind(_this);
		_this.state = {
			records: [],
			groups: [],
			currentItem: null,
			contextMenu: _this.contextMenu,
			showSearch: false
		};

		_this._perPage = 100;
		_this._id = null;
		return _this;
	}

	(0, _createClass3.default)(NamesGroupContainer, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			var _this2 = this;

			this.getNameGroups().then(function (groups) {
				_this2.allGroups = groups;
				_this2.setState({ groups: groups });
			});
			window.onResume = this.onResume.bind(this);
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			delete window.onResume;
		}
	}, {
		key: 'getNameGroups',
		value: function getNameGroups() {
			return this.props.registerPromise(_DBWindow2.default.send(_IPCActions.GET_NAMES_GROUP_LIST, { nameType: _NameType2.default.NAME_TYPE_PARTY }));
		}
	}, {
		key: 'onResume',
		value: function onResume() {
			var _this3 = this;

			this.props.onResume && this.props.onResume();
			this.getNameGroups().then(function (groups) {
				_this3.allGroups = groups;
				_this3.setState({ groups: groups });
				if (_this3.state.currentItem) {
					var currentItem = _this3.allGroups.find(function (item) {
						return _this3.state.currentItem.partyGroupId === item.partyGroupId;
					});
					_this3.setState({ currentItem: currentItem });
				}
				_this3.filterItems(_this3.state.searchText);
				_this3.onItemSelected(null, true);
			});
		}
	}, {
		key: 'getData',
		value: function getData(response) {
			if (response.meta.key !== this.currentRequestKey) {
				return false;
			}
			if (response.meta.next && response.data.length === this._perPage) {
				var next = response.meta.next;
				this.loadTransactions(next.id, next.start, next.noOfRecords, next.key);
			}
			if (response.meta.next) {
				this.setState({ records: response.data });
			} else {
				var records = [].concat((0, _toConsumableArray3.default)(this.state.records), (0, _toConsumableArray3.default)(response.data));
				this.setState({ records: records });
			}
		}
	}, {
		key: 'loadTransactions',
		value: function loadTransactions(id, start, noOfRecords, key, next) {
			var _this4 = this;

			this.props.registerPromise(_DBWindow2.default.send(_IPCActions.GET_NAMES_BY_GROUP_ID, {
				nameType: _NameType2.default.NAME_TYPE_PARTY,
				id: id,
				start: start,
				noOfRecords: noOfRecords,
				key: key,
				next: next
			})).then(function (data) {
				return _this4.getData(data);
			});
		}
	}, {
		key: 'onItemSelected',
		value: function onItemSelected(row) {
			var fromSync = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

			if (!fromSync) {
				if (!row.node.selected) {
					return false;
				}
				this._id = row.data.partyGroupId;
				this.setState({ currentItem: row.data });
			}
			this.currentRequestKey = Math.random() * 1000000000000000000;
			this.loadTransactions(this._id, 0, this._perPage, this.currentRequestKey, {
				id: this._id,
				start: this._perPage,
				key: this.currentRequestKey,
				noOfRecords: -1
			});
		}
	}, {
		key: 'filterItems',
		value: function filterItems(searchText) {
			if (!searchText) {
				this.setState({ groups: this.allGroups, searchText: '' });
				return false;
			}
			var query = searchText.toLowerCase();
			var filteredItems = this.allGroups.filter(function (party) {
				return party.partyGroupName.toLowerCase().includes(query);
			});
			var records = this.state.records;
			var currentItem = this.state.currentItem;
			if (!filteredItems.length) {
				records = [];
				currentItem = null;
			}
			this.setState({
				records: records,
				currentItem: currentItem,
				groups: filteredItems,
				searchText: searchText
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _this5 = this;

			var _state = this.state,
			    groups = _state.groups,
			    records = _state.records,
			    currentItem = _state.currentItem;

			return _react2.default.createElement(
				'div',
				{ className: 'd-flex align-items-stretch flex-container' },
				_react2.default.createElement(
					'div',
					{ className: 'flex-column d-flex left-column' },
					_react2.default.createElement(
						'div',
						{ className: 'listheaderDiv d-flex mt-20 mb-10' },
						_react2.default.createElement(
							'div',
							{ className: (0, _classnames2.default)('searchDiv', this.state.showSearch ? 'width100 d-flex' : '') },
							_react2.default.createElement(_SearchBox2.default, { throttle: 500,
								filter: this.filterItems,
								collapsed: !this.state.showSearch,
								collapsedTextBoxClass: 'width100',
								onCollapsedClick: function onCollapsedClick() {
									_this5.setState({ showSearch: true });
								},
								onInputBlur: function onInputBlur() {
									_this5.setState({ showSearch: false });
								}
							})
						),
						_react2.default.createElement(
							'div',
							{ className: 'buttonDiv flex-1 d-flex justify-content-end' },
							_react2.default.createElement(_AddNameGroupButton2.default, {
								collapsed: this.state.showSearch
							})
						)
					),
					_react2.default.createElement(_NameGroupList2.default, {
						onItemSelected: this.onItemSelected,
						items: groups
					})
				),
				_react2.default.createElement(
					'div',
					{ className: 'd-flex flex-column flex-grow-1 right-column' },
					_react2.default.createElement(_NameGroupDetail2.default, { currentItem: currentItem }),
					_react2.default.createElement(_NameList2.default, { items: records, addExtraColumns: true })
				)
			);
		}
	}]);
	return NamesGroupContainer;
}(_react2.default.Component);

NamesGroupContainer.propTypes = {
	onResume: _propTypes2.default.func,
	registerPromise: _propTypes2.default.func
};

exports.default = (0, _trashableReact2.default)(NamesGroupContainer);