Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = mountComponent;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function mountComponent(Component, element) {
	var props = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

	_reactDom2.default.unmountComponentAtNode(element);
	if (!props) {
		_reactDom2.default.render(_react2.default.createElement(Component, null), element);
	} else {
		_reactDom2.default.render(_react2.default.createElement(Component, props), element);
	}
}