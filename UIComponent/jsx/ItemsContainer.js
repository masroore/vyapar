Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Tabs = require('./Tabs');

var _Tabs2 = _interopRequireDefault(_Tabs);

var _SettingCache = require('../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ItemsContainer = function (_React$Component) {
	(0, _inherits3.default)(ItemsContainer, _React$Component);

	function ItemsContainer(props) {
		(0, _classCallCheck3.default)(this, ItemsContainer);

		var _this = (0, _possibleConstructorReturn3.default)(this, (ItemsContainer.__proto__ || (0, _getPrototypeOf2.default)(ItemsContainer)).call(this, props));

		var settingCache = new _SettingCache2.default();
		_this.state = {
			tabs: _this.visibleTabs(),
			current: 'PRODUCTS',
			isItemUnitEnable: settingCache.getItemUnitEnabled()
		};
		_this.onResume = _this.onResume.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(ItemsContainer, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			if (this.state.tabs.length) {
				this.changeTab(this.state.tabs[0].label);
			}
		}
	}, {
		key: 'changeTab',
		value: function changeTab(clickedTab) {
			var tabs = this.state.tabs.map(function (tab) {
				if (tab.label === clickedTab) {
					tab.active = true;
				} else {
					tab.active = false;
				}
				return tab;
			});
			switch (clickedTab) {
				case 'PRODUCTS':
					var ProductContainer = require('./ProductContainer').default;
					this.setState({
						CurrentComponent: ProductContainer,
						current: clickedTab,
						tabs: tabs
					});
					break;
				case 'SERVICES':
					var ServiceContainer = require('./ServiceContainer').default;
					this.setState({
						CurrentComponent: ServiceContainer,
						current: clickedTab,
						tabs: tabs
					});
					break;
				case 'CATEGORY':
					var CategoryContainer = require('./CategoryContainer').default;
					this.setState({
						CurrentComponent: CategoryContainer,
						current: clickedTab,
						tabs: tabs
					});
					break;
				case 'UNITS':
					var UnitsContainer = require('./UnitsContainer').default;
					this.setState({
						CurrentComponent: UnitsContainer,
						current: clickedTab,
						tabs: tabs
					});
					break;
				case 'CATALOGUE':
					var VyaparCatalogueView = require('./Catalogue/VyaparCatalogueView').default;
					var MyAnalytics = require('./../../Utilities/analyticsHelper');
					MyAnalytics.pushEvent('Catalogue Tab Clicked');
					this.setState({
						CurrentComponent: VyaparCatalogueView,
						current: clickedTab,
						tabs: tabs
					});
					break;
			}
		}
	}, {
		key: 'visibleTabs',
		value: function visibleTabs(active) {
			var tabs = [];
			var settingCache = new _SettingCache2.default();
			settingCache.reloadSettingCache();
			if (settingCache.isProductItemEnabled()) {
				tabs.push({
					label: 'PRODUCTS',
					active: false
				});
			}
			if (settingCache.getServiceItemEnabled()) {
				tabs.push({
					label: 'SERVICES',
					active: false
				});
			}
			if (settingCache.isItemCategoryEnabled()) {
				tabs.push({
					label: 'CATEGORY',
					active: false
				});
			}
			if (settingCache.getItemUnitEnabled()) {
				tabs.push({
					label: 'UNITS',
					active: false
				});
			}
			tabs.push({
				label: 'CATALOGUE',
				active: false
			});
			return tabs;
		}
	}, {
		key: 'onResume',
		value: function onResume() {
			var settingCache = new _SettingCache2.default();
			var tabs = this.visibleTabs();
			var current = this.state.current;
			var isCurrentTabEnabled = tabs.find(function (tab) {
				return tab.label === current;
			});
			if (!isCurrentTabEnabled) {
				current = tabs[0].label;
			}
			this.setState({
				tabs: this.visibleTabs(),
				current: current,
				isItemUnitEnable: settingCache.getItemUnitEnabled()
			});
			this.changeTab(current);
		}
	}, {
		key: 'render',
		value: function render() {
			var _state = this.state,
			    CurrentComponent = _state.CurrentComponent,
			    tabs = _state.tabs;

			return _react2.default.createElement(
				'div',
				{ className: 'd-flex flex-column', style: { height: '100%' } },
				_react2.default.createElement(_Tabs2.default, { tabs: tabs, onClick: this.changeTab.bind(this) }),
				CurrentComponent && _react2.default.createElement(CurrentComponent, { onResume: this.onResume, isItemUnitEnable: this.state.isItemUnitEnable })
			);
		}
	}]);
	return ItemsContainer;
}(_react2.default.Component);

exports.default = ItemsContainer;