Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Modal = require('./Modal');

var _Modal2 = _interopRequireDefault(_Modal);

var _TextField = require('@material-ui/core/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _DecimalInputFilter = require('../../Utilities/DecimalInputFilter');

var _DecimalInputFilter2 = _interopRequireDefault(_DecimalInputFilter);

var _styles = require('@material-ui/core/styles');

var _PrimaryButton = require('./UIControls/PrimaryButton');

var _PrimaryButton2 = _interopRequireDefault(_PrimaryButton);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
	return {
		'@global': {
			'.bankAccountModalBody': {
				display: 'grid',
				gridColumnGap: '.5rem',
				gridRowGap: '1rem',
				gridTemplateAreas: '\n            "a a"\n            "b b"\n            "c c"\n            "d e"\n            ',
				alignItems: 'center',
				'& :nth-child(1)': {
					gridArea: 'a'
				},
				'& :nth-child(2)': {
					gridArea: 'b'
				},
				'& :nth-child(3)': {
					gridArea: 'c'
				},
				'& :nth-child(4)': {
					gridArea: 'd'
				},
				'& :nth-child(5)': {
					display: 'flex',
					alignItems: 'center',
					gridArea: 'e'
				}
			},
			'.bankAccountModalFooter': {
				display: 'flex',
				justifyContent: 'flex-end',
				marginTop: '1rem'
			}
		}
	};
};
var commonTextFieldProps = {
	margin: 'dense',
	variant: 'outlined',
	fullWidth: true,
	color: 'primary'
};

var AddEditBankAccountModal = function (_React$Component) {
	(0, _inherits3.default)(AddEditBankAccountModal, _React$Component);

	function AddEditBankAccountModal(props) {
		(0, _classCallCheck3.default)(this, AddEditBankAccountModal);

		var _this = (0, _possibleConstructorReturn3.default)(this, (AddEditBankAccountModal.__proto__ || (0, _getPrototypeOf2.default)(AddEditBankAccountModal)).call(this, props));

		_this.handleChange = function (event) {
			var field = event.target.name;
			var value = event.target.value;
			if (field === 'openingBalance') {
				value = _DecimalInputFilter2.default.inputTextFilterForAmount(event.target);
			}
			_this.setState((0, _defineProperty3.default)({}, field, value));
		};

		_this.onSubmit = function (e) {
			e.preventDefault();
			var _this$state = _this.state,
			    accountId = _this$state.accountId,
			    accountName = _this$state.accountName,
			    bankName = _this$state.bankName,
			    accountNumber = _this$state.accountNumber,
			    openingBalance = _this$state.openingBalance,
			    openingDate = _this$state.openingDate;

			bankName = bankName ? bankName.trim() : '';
			accountName = accountName ? accountName.trim() : '';
			accountNumber = accountNumber ? accountNumber.trim() : '';
			openingBalance = MyDouble.convertStringToDouble(openingBalance);

			if (!accountName) {
				ToastHelper.error('Display Name Cannot Be Empty');
				return;
			}

			var PaymentInfo = require('../../BizLogic/PaymentInfo.js');
			var paymentinfo = new PaymentInfo();

			var ErrorCode = require('../../Constants/ErrorCode.js');

			var PaymentInfoCache = require('../../Cache/PaymentInfoCache.js');
			var paymentinfocache = new PaymentInfoCache();

			var statusCode = ErrorCode.SUCCESS;

			paymentinfo.setName(accountName);
			paymentinfo.setAccountNumber(accountNumber);
			if (!accountId && accountName) {
				if (paymentinfocache.paymentInfoExists(accountName) > -1) {
					ToastHelper.error(ErrorCode.ERROR_BANK_DISPLAY_NAME_EXIST);
					return;
				}
			}
			paymentinfo.setBankName(bankName);
			paymentinfo.setType('BANK');
			if (openingBalance) {
				paymentinfo.setOpeningBalance(openingBalance);
			}

			var MyDate = require('../../Utilities/MyDate.js');
			openingDate = MyDate.getDateObj(openingDate, 'dd/mm/yyyy', '/');
			var currentTime = new Date();
			openingDate.setHours(currentTime.getHours(), currentTime.getMinutes(), currentTime.getSeconds(), 0);
			paymentinfo.setOpeningDate(openingDate);
			if (accountId > 0) {
				paymentinfo.setId(accountId);
				statusCode = paymentinfo.updateInfo();
			} else {
				statusCode = paymentinfo.saveNewInfo();
			}

			if (statusCode != ErrorCode.ERROR_NEW_BANK_INFO_SUCCESS && statusCode != ErrorCode.ERROR_UPDATE_BANK_INFO_SUCCESS) {
				ToastHelper.error(statusCode);
			} else {
				ToastHelper.success(statusCode);
				_this.props.onSave && _this.props.onSave(accountName);
			}
		};

		_this.onAfterOpen = function () {
			setTimeout(function () {
				$('.bankAccountModalBody #openingDate').datepicker({
					dateFormat: 'dd/mm/yy',
					onSelect: function onSelect(value) {
						_this.setState({
							openingDate: value
						});
					},
					onClose: function onClose(value) {
						try {
							var dt = MyDate.getDateObj(value, 'dd/mm/yyyy', '/');
							if (dt && dt.toString() === 'Invalid Date' || !value || value.length < 10) {
								_this.setState({
									openingDate: MyDate.getDate('d/m/y', new Date())
								});
							}
						} catch (ex) {
							_this.setState({
								openingDate: MyDate.getDate('d/m/y', new Date())
							});
						}
					}
				});
			});
		};

		_this.handleClose = function () {
			_this.resetState();
			_this.props.onClose && _this.props.onClose();
		};

		_this.state = {};
		return _this;
	}

	(0, _createClass3.default)(AddEditBankAccountModal, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			var MyDate = require('../../Utilities/MyDate');
			var _props$accountId = this.props.accountId,
			    accountId = _props$accountId === undefined ? '' : _props$accountId;

			var accountName = '';
			var bankName = '';
			var accountNumber = '';
			var openingBalance = 0;
			var openingDate = new Date();
			if (accountId) {
				var PaymentInfoCache = require('../../Cache/PaymentInfoCache.js');
				var paymentInfoCache = new PaymentInfoCache();
				var account = paymentInfoCache.getPaymentInfoObjById(accountId);
				if (account) {
					accountName = account.getName();
					accountNumber = account.getAccountNumber();
					bankName = account.getBankName();
					openingBalance = account.getOpeningBalance();
					openingDate = account.getOpeningDate();
				}
			}
			openingDate = MyDate.getDate('d/m/y', openingDate);
			this.setState({
				accountId: accountId,
				accountName: accountName,
				bankName: bankName,
				accountNumber: accountNumber,
				openingBalance: openingBalance,
				openingDate: openingDate
			});
		}
	}, {
		key: 'resetState',
		value: function resetState() {
			this.setState({
				accountId: '',
				accountName: '',
				bankName: '',
				accountNumber: '',
				openingBalance: '',
				openingDate: MyDate.getDate('d/m/y', new Date())
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _state = this.state,
			    accountId = _state.accountId,
			    accountName = _state.accountName,
			    bankName = _state.bankName,
			    accountNumber = _state.accountNumber,
			    openingBalance = _state.openingBalance,
			    openingDate = _state.openingDate;

			return _react2.default.createElement(
				_Modal2.default,
				{
					height: '400px',
					width: '50%',
					onClose: this.handleClose,
					onAfterOpen: this.onAfterOpen,
					isOpen: this.props.isOpen,
					title: accountId ? 'Edit Bank Account' : 'Add Bank Account',
					canHideHeader: true
				},
				_react2.default.createElement(
					'form',
					null,
					_react2.default.createElement(
						'div',
						{ className: 'bankAccountModalBody' },
						_react2.default.createElement(_TextField2.default, (0, _extends3.default)({
							required: true
						}, commonTextFieldProps, {
							label: 'Account Name',
							placeholder: 'Account Name',
							value: accountName,
							name: 'accountName',
							onChange: this.handleChange
						})),
						_react2.default.createElement(_TextField2.default, (0, _extends3.default)({}, commonTextFieldProps, {
							label: 'Bank Name',
							placeholder: 'Bank Name',
							value: bankName,
							name: 'bankName',
							onChange: this.handleChange
						})),
						_react2.default.createElement(_TextField2.default, (0, _extends3.default)({}, commonTextFieldProps, {
							label: 'Account Number',
							placeholder: 'Account Number',
							value: accountNumber,
							name: 'accountNumber',
							onChange: this.handleChange
						})),
						_react2.default.createElement(_TextField2.default, (0, _extends3.default)({}, commonTextFieldProps, {
							label: 'Opening Balance',
							placeholder: 'Opening Balance',
							value: openingBalance,
							name: 'openingBalance',
							onChange: this.handleChange
						})),
						_react2.default.createElement(_TextField2.default, (0, _extends3.default)({}, commonTextFieldProps, {
							label: 'Opening Date',
							className: 'openingDate',
							value: openingDate,
							name: 'openingDate',
							id: 'openingDate',
							onChange: this.handleChange
						}))
					),
					_react2.default.createElement(
						'div',
						{ className: 'bankAccountModalFooter' },
						_react2.default.createElement(
							_PrimaryButton2.default,
							{ style: { color: 'white', backgroundColor: '#1789FC', marginTop: '.5rem', float: 'right' }, onClick: this.onSubmit },
							'Save'
						)
					)
				)
			);
		}
	}]);
	return AddEditBankAccountModal;
}(_react2.default.Component);

AddEditBankAccountModal.propTypes = {
	accountId: _propTypes2.default.string,
	label: _propTypes2.default.string,
	onClose: _propTypes2.default.func,
	onSave: _propTypes2.default.func,
	isOpen: _propTypes2.default.bool,
	title: _propTypes2.default.string
};

exports.default = (0, _styles.withStyles)(styles)(AddEditBankAccountModal);