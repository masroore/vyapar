Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _core = require('@material-ui/core');

var _Close = require('@material-ui/icons/Close');

var _Close2 = _interopRequireDefault(_Close);

var _themes = require('../../themes');

var _themes2 = _interopRequireDefault(_themes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var theme = (0, _themes2.default)();

var styles = {
	dialogPaper: {
		minWidth: '421px',
		maxHeight: '173px'
	},
	button: {
		width: '110px',
		height: '40px'
	},
	title: {
		fontSize: '18px',
		fontWeight: 'bold',
		color: '#2B4C56',
		lineHeight: '22px',
		width: '103px'
	},
	contentText: {
		fontSize: '14px',
		color: '#284561',
		lineHeight: '17px'
	}
};

var ConfirmModal = function (_React$Component) {
	(0, _inherits3.default)(ConfirmModal, _React$Component);

	function ConfirmModal(props) {
		(0, _classCallCheck3.default)(this, ConfirmModal);

		var _this = (0, _possibleConstructorReturn3.default)(this, (ConfirmModal.__proto__ || (0, _getPrototypeOf2.default)(ConfirmModal)).call(this, props));

		_this.onClose = _this.onClose.bind(_this);
		_this.onSave = _this.onSave.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(ConfirmModal, [{
		key: 'onClose',
		value: function onClose(e) {
			e.stopPropagation();
			this.props.onClose && this.props.onClose(false);
		}
	}, {
		key: 'onSave',
		value: function onSave(e) {
			e.preventDefault();
			this.props.onClose && this.props.onClose(true);
		}
	}, {
		key: 'render',
		value: function render() {
			var _props = this.props,
			    classes = _props.classes,
			    _props$title = _props.title,
			    title = _props$title === undefined ? 'Confirm' : _props$title,
			    _props$message = _props.message,
			    message = _props$message === undefined ? 'Are you sure you want to continue?' : _props$message,
			    _props$cancelText = _props.cancelText,
			    cancelText = _props$cancelText === undefined ? 'Cancel' : _props$cancelText,
			    _props$OKText = _props.OKText,
			    OKText = _props$OKText === undefined ? 'OK' : _props$OKText,
			    _props$open = _props.open,
			    open = _props$open === undefined ? true : _props$open;

			return _react2.default.createElement(
				_styles.MuiThemeProvider,
				{ theme: theme
				},
				_react2.default.createElement(
					_core.Dialog,
					{
						open: open,
						onClose: this.onClose,
						onEnter: this.save,
						classes: { paper: classes.dialogPaper }
					},
					_react2.default.createElement(
						_core.DialogTitle,
						{ id: 'alert-dialog-title' },
						_react2.default.createElement(
							_core.Grid,
							{ container: true, direction: 'row', justify: 'space-between', alignItems: 'center' },
							_react2.default.createElement(
								_core.Grid,
								{ item: true },
								_react2.default.createElement(
									_core.Typography,
									{ variant: 'h6', classes: { root: classes.title } },
									title
								)
							),
							_react2.default.createElement(
								_core.Grid,
								{ item: true },
								_react2.default.createElement(_Close2.default, { 'aria-label': 'close', onClick: this.onClose })
							)
						)
					),
					_react2.default.createElement(
						_core.DialogContent,
						null,
						_react2.default.createElement(
							_core.DialogContentText,
							{ id: 'alert-dialog-description', classes: { root: classes.contentText } },
							message
						)
					),
					_react2.default.createElement(
						_core.DialogActions,
						null,
						_react2.default.createElement(
							_core.Button,
							{ className: 'mr-15', classes: { root: classes.button }, style: { textTransform: 'none' }, tabindex: 0, onClick: this.onClose, variant: 'outlined', color: 'primary' },
							cancelText
						),
						_react2.default.createElement(
							_core.Button,
							{ className: 'mr-10', classes: { root: classes.button }, style: { textTransform: 'none' }, onClick: this.onSave, color: 'primary', variant: 'contained', autoFocus: true },
							OKText
						)
					)
				)
			);
		}
	}]);
	return ConfirmModal;
}(_react2.default.Component);

exports.default = (0, _styles.withStyles)(styles)(ConfirmModal);