Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _Dialog = require('@material-ui/core/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _DialogTitle = require('@material-ui/core/DialogTitle');

var _DialogTitle2 = _interopRequireDefault(_DialogTitle);

var _DialogContent = require('@material-ui/core/DialogContent');

var _DialogContent2 = _interopRequireDefault(_DialogContent);

var _DialogActions = require('@material-ui/core/DialogActions');

var _DialogActions2 = _interopRequireDefault(_DialogActions);

var _DialogContentText = require('@material-ui/core/DialogContentText');

var _DialogContentText2 = _interopRequireDefault(_DialogContentText);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _themes = require('../../themes');

var _themes2 = _interopRequireDefault(_themes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var theme = (0, _themes2.default)();

var ConfirmModal = function (_React$Component) {
	(0, _inherits3.default)(ConfirmModal, _React$Component);

	function ConfirmModal(props) {
		(0, _classCallCheck3.default)(this, ConfirmModal);

		var _this = (0, _possibleConstructorReturn3.default)(this, (ConfirmModal.__proto__ || (0, _getPrototypeOf2.default)(ConfirmModal)).call(this, props));

		_this.onClose = _this.onClose.bind(_this);
		_this.onSave = _this.onSave.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(ConfirmModal, [{
		key: 'onClose',
		value: function onClose(e) {
			e.stopPropagation();
			this.props.onClose && this.props.onClose();
		}
	}, {
		key: 'onSave',
		value: function onSave(e) {
			e.preventDefault();
			this.props.onClose && this.props.onClose();
			setTimeout(function () {
				document.getElementById('trialText').click();
			}, 100);
		}
	}, {
		key: 'render',
		value: function render() {
			var _props = this.props,
			    _props$cancelText = _props.cancelText,
			    cancelText = _props$cancelText === undefined ? 'Cancel' : _props$cancelText,
			    _props$OKText = _props.OKText,
			    OKText = _props$OKText === undefined ? 'Buy License' : _props$OKText,
			    _props$open = _props.open,
			    open = _props$open === undefined ? true : _props$open;

			return _react2.default.createElement(
				_styles.MuiThemeProvider,
				{ theme: theme },
				_react2.default.createElement(
					_Dialog2.default,
					{
						disableEnforceFocus: true,
						open: open,
						onClose: this.onClose
					},
					_react2.default.createElement(
						_DialogTitle2.default,
						{ id: 'alert-dialog-title' },
						'License Expired'
					),
					_react2.default.createElement(
						_DialogContent2.default,
						null,
						_react2.default.createElement(
							_DialogContentText2.default,
							{ id: 'alert-dialog-description' },
							' Your license or trial period has expired. Please purchase license before you continue. '
						)
					),
					_react2.default.createElement(
						_DialogActions2.default,
						null,
						_react2.default.createElement(
							_Button2.default,
							{ className: 'mr-20', variant: 'contained', tabindex: 0, onClick: this.onClose, color: 'default' },
							cancelText
						),
						_react2.default.createElement(
							_Button2.default,
							{ variant: 'contained', onClick: this.onSave, color: 'primary', autoFocus: true },
							OKText
						)
					)
				)
			);
		}
	}]);
	return ConfirmModal;
}(_react2.default.Component);

ConfirmModal.propTypes = {
	onClose: _propTypes2.default.func,
	cancelText: _propTypes2.default.string,
	OKText: _propTypes2.default.string,
	open: _propTypes2.default.bool
};
exports.default = ConfirmModal;