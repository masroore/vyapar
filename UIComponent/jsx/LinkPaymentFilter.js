Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _TxnTypeConstant = require('../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _MyDate = require('../../Utilities/MyDate');

var _MyDate2 = _interopRequireDefault(_MyDate);

var _LinkPaymentAutoLink = require('./LinkPaymentAutoLink');

var _LinkPaymentAutoLink2 = _interopRequireDefault(_LinkPaymentAutoLink);

var _LinkPaymentReset = require('./LinkPaymentReset');

var _LinkPaymentReset2 = _interopRequireDefault(_LinkPaymentReset);

var _styles = require('@material-ui/core/styles');

var _DecimalInputFilter = require('./../../Utilities/DecimalInputFilter');

var _DecimalInputFilter2 = _interopRequireDefault(_DecimalInputFilter);

var _SettingCache = require('./../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
	paymentReminderHeader: {
		display: 'grid',
		gridTemplateColumns: '36% 12% 12% 12% 9% 15% 5%',
		padding: '0 20px 10px 20px',
		borderBottom: '#BDBDBD solid 1px',
		font: '11px "Roboto", sans-serif',
		backgroundColor: 'white'
	},
	cashPaymentReminderHeader: {
		gridTemplateColumns: '40% 14% 14% 15% 13% 5%'
	},
	linkPaymentInput: {
		border: '#BDBDBD solid 1px',
		padding: '6px',
		width: '100%',
		marginTop: '7px',
		borderRadius: '3px'
	},
	paymentLinkAmountValue: {
		fontSize: '13px',
		paddingTop: '11px',
		textAlign: 'right',
		paddingRight: '30px'
	},
	paymentLinkHeaderValue: {
		fontSize: '13px',
		paddingTop: '11px'
	},
	paymentLinkPartyNameHeader: {
		paddingLeft: '14px',
		'&:first-child': {
			paddingLeft: 0
		}
	},
	paymentLinkFilterLabel: {
		color: '#0B8FC5'
	},
	paddingTop6: {
		paddingTop: '6px'
	},
	pr8: {
		paddingRight: '8px'
	}
};

var PaymnentLinkFilter = function (_React$Component) {
	(0, _inherits3.default)(PaymnentLinkFilter, _React$Component);

	function PaymnentLinkFilter(props) {
		(0, _classCallCheck3.default)(this, PaymnentLinkFilter);

		var _this = (0, _possibleConstructorReturn3.default)(this, (PaymnentLinkFilter.__proto__ || (0, _getPrototypeOf2.default)(PaymnentLinkFilter)).call(this, props));

		var txnType = props.txnType,
		    _props$transaction = props.transaction,
		    party = _props$transaction.party,
		    total = _props$transaction.total,
		    invoiceDate = _props$transaction.invoiceDate,
		    invoiceNo = _props$transaction.invoiceNo;

		_this.state = {
			receivedAmount: 0,
			discountAmount: 0
		};

		_this.partyName = party && party.label || '';
		_this.totalAmount = total;
		_this.showTotal = false;
		_this.showDiscount = false;
		_this.receivedPaidAmountTitle = '';
		_this.discountTitle = '';
		switch (txnType) {
			case _TxnTypeConstant2.default.TXN_TYPE_SALE:
				_this.showTotal = true;
				_this.receivedPaidAmountTitle = 'Received';
				break;
			case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE:
				_this.showTotal = true;
				_this.receivedPaidAmountTitle = 'Paid Amount';
				break;
			case _TxnTypeConstant2.default.TXN_TYPE_CASHIN:
				_this.showDiscount = true;
				_this.discountTitle = 'Discount Given';
				_this.receivedPaidAmountTitle = 'Received';
				break;
			case _TxnTypeConstant2.default.TXN_TYPE_CASHOUT:
				_this.showDiscount = true;
				_this.receivedPaidAmountTitle = 'Paid Amount';
				_this.discountTitle = 'Discount Received';
				break;
			case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN:
				_this.showTotal = true;
				_this.receivedPaidAmountTitle = 'Received';
				break;
			case _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN:
				_this.showTotal = true;
				_this.receivedPaidAmountTitle = 'Paid Amount';
				break;
		}
		_this.fillReceivedAndDiscount = _this.fillReceivedAndDiscount.bind(_this);
		_this.onReceivedInputChange = _this.onReceivedInputChange.bind(_this);
		_this.onDiscountInputChange = _this.onDiscountInputChange.bind(_this);
		_this.settingCache = new _SettingCache2.default();
		return _this;
	}

	(0, _createClass3.default)(PaymnentLinkFilter, [{
		key: 'onDiscountInputChange',
		value: function onDiscountInputChange() {
			this.setState({ discountAmount: event.target.value });
			this.props.calculateSelectedTransactionsTotal(true);
		}
	}, {
		key: 'onReceivedInputChange',
		value: function onReceivedInputChange(event) {
			this.setState({ receivedAmount: event.target.value });
			this.props.calculateSelectedTransactionsTotal(true);
		}
	}, {
		key: 'fillReceivedAndDiscount',
		value: function fillReceivedAndDiscount() {
			var _props = this.props,
			    txnType = _props.txnType,
			    _props$transaction2 = _props.transaction,
			    total = _props$transaction2.total,
			    discountForCash = _props$transaction2.discountForCash,
			    received = _props$transaction2.received;

			if (txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT) {
				var receivedAmount = _MyDouble2.default.convertStringToDouble(total || 0);
				var discountAmount = _MyDouble2.default.convertStringToDouble(discountForCash || 0);
			} else {
				discountAmount = 0;
				receivedAmount = _MyDouble2.default.convertStringToDouble(received);
			}
			this.setState({
				receivedAmount: receivedAmount,
				discountAmount: discountAmount
			});
		}
	}, {
		key: 'inputTextFilterForAmount',
		value: function inputTextFilterForAmount(event) {
			_DecimalInputFilter2.default.inputTextFilterForAmount(event.currentTarget);
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.fillReceivedAndDiscount();
		}
	}, {
		key: 'render',
		value: function render() {
			var showAmountToLink = true;
			var showBlankDiscount = false;
			var _props2 = this.props,
			    classes = _props2.classes,
			    txnId = _props2.txnId,
			    oldTxnObj = _props2.oldTxnObj,
			    txnType = _props2.txnType;

			var showAutoLink = true;
			if (txnId && oldTxnObj.getTxnType() == txnType) {
				showAutoLink = false;
			}
			var isPaymentTransaction = txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT || txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHIN;
			if (this.showDiscount) {
				if (!this.settingCache.getDiscountInMoneyTxnEnabled()) {
					showBlankDiscount = true;
					showAmountToLink = false;
				}
			}
			return _react2.default.createElement(
				'div',
				{ id: 'linkPaymentHeader', className: (0, _classnames2.default)(classes.paymentReminderHeader, isPaymentTransaction ? classes.cashPaymentReminderHeader : '') },
				_react2.default.createElement(
					'div',
					{ className: classes.paymentLinkPartyNameHeader },
					_react2.default.createElement(
						'label',
						{ htmlFor: 'linkPaymentPartyName' },
						' Party'
					),
					_react2.default.createElement(
						'div',
						{
							id: 'linkPaymentPartyName',
							className: classes.paymentLinkHeaderValue
						},
						this.partyName
					)
				),
				_react2.default.createElement(
					'div',
					{ className: (0, _classnames2.default)(classes.paymentLinkPartyNameHeader, !this.showTotal ? 'hide' : '') },
					_react2.default.createElement(
						'label',
						{ htmlFor: 'linkPaymentTxnTotal' },
						' Total Amount'
					),
					_react2.default.createElement(
						'div',
						{
							id: 'linkPaymentTxnTotal',
							className: classes.paymentLinkHeaderValue
						},
						this.totalAmount ? this.totalAmount : 0
					)
				),
				_react2.default.createElement(
					'div',
					null,
					_react2.default.createElement(
						'label',
						{
							htmlFor: 'paymentLinkCashReceived',
							className: classes.paymentLinkFilterLabel
						},
						this.receivedPaidAmountTitle
					),
					_react2.default.createElement(
						'div',
						{ className: classes.pr8 },
						_react2.default.createElement('input', {
							type: 'text',
							id: 'paymentLinkCashReceived',
							value: this.state.receivedAmount,
							onInput: this.inputTextFilterForAmount,
							className: classes.linkPaymentInput,
							onChange: this.onReceivedInputChange
						})
					)
				),
				this.showDiscount && !showBlankDiscount && _react2.default.createElement(
					'div',
					null,
					_react2.default.createElement(
						'label',
						{
							htmlFor: 'paymentLinkDiscount',
							className: classes.paymentLinkFilterLabel
						},
						this.discountTitle
					),
					_react2.default.createElement(
						'div',
						{ className: classes.pr8 },
						_react2.default.createElement('input', {
							type: 'text',
							id: 'paymentLinkDiscount',
							value: this.state.discountAmount,
							onInput: this.inputTextFilterForAmount,
							className: classes.linkPaymentInput,
							onChange: this.onDiscountInputChange
						})
					)
				),
				_react2.default.createElement(
					'div',
					{ className: !(this.showDiscount && showBlankDiscount && !isPaymentTransaction) ? 'hide' : '' },
					_react2.default.createElement(
						'label',
						{ htmlFor: 'linkPaymentAmountToLink' },
						' Amount to Link'
					),
					_react2.default.createElement(
						'div',
						{
							id: 'linkPaymentAmountToLink',
							className: classes.paymentLinkAmountValue
						},
						this.props.amtToLink
					)
				),
				this.showDiscount && showBlankDiscount && _react2.default.createElement('div', null),
				_react2.default.createElement(
					'div',
					{ className: !(showAmountToLink && !isPaymentTransaction) ? 'hide' : '' },
					_react2.default.createElement(
						'label',
						{ htmlFor: 'linkPaymentAmountToLink' },
						' Amount to Link'
					),
					_react2.default.createElement(
						'div',
						{
							id: 'linkPaymentAmountToLink',
							className: classes.paymentLinkAmountValue
						},
						this.props.amtToLink
					)
				),
				!this.settingCache.getTxnRefNoEnabled() && _react2.default.createElement('div', null),
				_react2.default.createElement('div', null),
				showAutoLink && _react2.default.createElement(
					'div',
					{ className: classes.paddingTop6 },
					_react2.default.createElement(_LinkPaymentAutoLink2.default, {
						title: isPaymentTransaction ? 'Vyapar will link Credit Transactions to Payment Received automatically. Oldest transaction will be linked first.' : 'Vyapar will automatically link payments received to your invoices and mark them as paid. Oldest Payments will be linked first.',
						autoLink: this.props.autoLink
					})
				),
				!showAutoLink && _react2.default.createElement('div', { className: classes.paddingTop6 }),
				_react2.default.createElement(
					'div',
					{ className: classes.paddingTop6 },
					_react2.default.createElement(_LinkPaymentReset2.default, {
						img: './../inlineSVG/baseline-refresh-24px.svg',
						reset: this.props.reset
					})
				)
			);
		}
	}]);
	return PaymnentLinkFilter;
}(_react2.default.Component);

exports.default = (0, _styles.withStyles)(styles)(PaymnentLinkFilter);