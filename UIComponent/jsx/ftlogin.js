Object.defineProperty(exports, "__esModule", {
	value: true
});

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _Slide = require('./Slide');

var _Slide2 = _interopRequireDefault(_Slide);

var _reactIntlTelInput = require('react-intl-tel-input');

var _reactIntlTelInput2 = _interopRequireDefault(_reactIntlTelInput);

var _Country = require('../../Constants/Country');

var _Country2 = _interopRequireDefault(_Country);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// eslint-disable-next-line new-cap
var LocalStorageHelper = require('./../../Utilities/LocalStorageHelper');
var LicenseInfoConstants = require('../../Constants/LicenseInfoConstant');

var FTLogin = function (_React$Component) {
	(0, _inherits3.default)(FTLogin, _React$Component);

	function FTLogin(props) {
		(0, _classCallCheck3.default)(this, FTLogin);

		var _this = (0, _possibleConstructorReturn3.default)(this, (FTLogin.__proto__ || (0, _getPrototypeOf2.default)(FTLogin)).call(this, props));

		_this.restoreBackup = function () {
			if (_this.restoreBackupInProgress) {
				return;
			}
			_this.restoreBackupInProgress = true;
			var handleRestoreComplete = function handleRestoreComplete() {
				$('.loaderWrapper').hide();
				_this.restoreBackupInProgress = false;
			};
			try {
				$('.loadingText.blink_me').html('Restoring backup...');
				$('.loaderWrapper').show();
				var ImportDB = require('./../../BizLogic/ImportDB.js');
				var importDB = new ImportDB();
				importDB.ImportDatabase(handleRestoreComplete);
			} catch (err) {
				handleRestoreComplete();
				alert('Restore backup failed');
			}
		};

		var existingReferralCode = LocalStorageHelper.getValue(LicenseInfoConstants.referralCode) || '';
		_this.defaultCountry = (0, _Country.getDefaultCountry)();
		_this.state = {
			phoneNumber: '',
			isReferralCodeAvailable: !!existingReferralCode,
			referralCode: existingReferralCode,
			showReferralCode: !!existingReferralCode,
			currentCountry: (0, _Country.getDefaultCountry)(),
			inputError: '',
			companyCreationInProgress: false
		};
		return _this;
	}

	(0, _createClass3.default)(FTLogin, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			var phoneInput = document.querySelector('input[type=tel]');
			if (phoneInput) {
				phoneInput.onfocus = function () {
					var phoneParentDiv = document.querySelector('.intl-tel-input.phone');
					phoneParentDiv && phoneParentDiv.click();
				};
			}
		}
	}, {
		key: 'buildLoginDiv',
		value: function buildLoginDiv() {
			var _this2 = this;

			var countriesData = _Country2.default.getCountryList().map(function (country) {
				return [country.countryName, country.countryCode.toLowerCase(), country.dialCode];
			});
			return _react2.default.createElement(
				'div',
				{ className: 'rightNav' },
				_react2.default.createElement(
					'div',
					{ className: 'loginDiv' },
					_react2.default.createElement(
						'h3',
						null,
						'Login'
					),
					_react2.default.createElement(
						'div',
						{ className: 'phoneDiv' },
						_react2.default.createElement(
							'span',
							null,
							'Enter Mobile No.'
						),
						_react2.default.createElement(_reactIntlTelInput2.default, {
							preferredCountries: ['in', 'bd', 'np', 'pk', 'us'],
							separateDialCode: true,
							containerClassName: 'intl-tel-input phone',
							defaultCountry: this.defaultCountry.dialCode.toLowerCase(),
							value: this.state.phoneNumber,
							autoPlaceholder: false,
							autoFocus: true,
							countriesData: countriesData,
							onSelectFlag: function onSelectFlag(str, countryObj) {
								_this2.setState({
									currentCountry: _Country2.default.getCountryFromCountryCode(countryObj.iso2)
								});
								var phoneRegex = _this2.state.currentCountry.dialCode == 91 ? '^[0-9]{0,10}$' : '^[0-9]{0,15}$';
								var regex = new RegExp(phoneRegex);
								if (regex.test(_this2.state.phoneNumber)) {
									_this2.setState({ inputError: '' });
								} else {
									_this2.setState({
										inputError: 'Invalid Mobile Number.'
									});
								}
							},
							telInputProps: function () {
								return {
									onInput: function onInput(event) {
										return _this2.handleInput(event);
									},
									onKeyUp: function onKeyUp(event) {
										if (event.keyCode == 13) {
											_this2.handleLogin();
										}
									}
								};
							}()
						}),
						_react2.default.createElement(
							'p',
							{ className: 'errorText' },
							this.state.inputError
						)
					),
					_react2.default.createElement(
						'div',
						null,
						_react2.default.createElement(
							'button',
							{ className: 'loginBtn', onClick: function onClick() {
									return _this2.handleLogin();
								} },
							'Complete Login'
						)
					),
					this.buildReferralCodeDiv(),
					_react2.default.createElement(
						'span',
						null,
						'By continuing, you accept the ',
						_react2.default.createElement(
							'a',
							{ href: '#', onClick: function onClick() {
									return _this2.showTerms();
								} },
							'Terms and conditions'
						)
					),
					_react2.default.createElement(
						'a',
						{ className: 'restoreButton', onClick: this.restoreBackup },
						'Restore backup'
					)
				)
			);
		}
	}, {
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				'div',
				{ className: 'container' },
				_react2.default.createElement(_Slide2.default, null),
				this.buildLoginDiv()
			);
		}
	}, {
		key: 'buildReferralCodeDiv',
		value: function buildReferralCodeDiv() {
			var _this3 = this;

			if (!this.state.isReferralCodeAvailable) {
				return _react2.default.createElement(
					'div',
					{ className: 'referralDiv' },
					_react2.default.createElement(
						'a',
						{ href: '#', className: 'hasReferralCode', onClick: function onClick() {
								return _this3.setState({ showReferralCode: true });
							} },
						'Have a Referral Code?'
					),
					_react2.default.createElement(
						'div',
						{ className: 'referralCode new', hidden: !this.state.showReferralCode },
						_react2.default.createElement('input', { type: 'text', placeholder: 'Enter Referral Code', value: this.state.referralCode, onChange: function onChange(event) {
								return _this3.setState({ referralCode: event.currentTarget.value });
							} }),
						_react2.default.createElement(
							'span',
							{ onClick: function onClick() {
									return _this3.setState({
										referralCode: '',
										showReferralCode: false
									});
								} },
							'x'
						)
					)
				);
			} else {
				return _react2.default.createElement(
					'div',
					{ className: 'referralDiv' },
					_react2.default.createElement(
						'span',
						null,
						'Referral Code: '
					),
					_react2.default.createElement('input', { type: 'text', className: 'referralCode existing', disabled: true, id: 'referralCode', value: this.state.referralCode })
				);
			}
		}
	}, {
		key: 'showTerms',
		value: function showTerms() {
			require('electron').shell.openExternal('https://vyaparapp.in/permission');
		}
	}, {
		key: 'handleLogin',
		value: function handleLogin() {
			var _this4 = this;

			if (this.state.companyCreationInProgress) {
				return;
			} else {
				this.setState({ companyCreationInProgress: true });
			}
			var phoneRegex = this.state.currentCountry.dialCode == 91 ? '^[0-9]{10}$' : '^[0-9]{8,15}$';
			var regex = new RegExp(phoneRegex);
			if (regex.test(this.state.phoneNumber)) {
				$('.loadingText.blink_me').html('Creating Company...');
				$('.loaderWrapper').show(function () {
					return _this4.updateUserDetailsToDb();
				});
			} else {
				this.setState({
					companyCreationInProgress: false,
					inputError: 'Invalid Mobile Number.'
				});
			}
		}
	}, {
		key: 'updateUserDetailsToDb',
		value: function updateUserDetailsToDb() {
			var _this5 = this;

			var ErrorCode = require('../../Constants/ErrorCode');
			try {
				var MyAnalytics = require('../../Utilities/analyticsHelper.js');
				var CompanyListUtility = require('../../Utilities/CompanyListUtility.js');
				var StringConstants = require('../../Constants/StringConstants');
				var phoneNumber = this.state.phoneNumber;
				var obj = {};
				var businessName = StringConstants.MY_COMPANY;
				var businessFileName = phoneNumber;
				obj['phone'] = phoneNumber;
				obj['businessName'] = businessName;
				obj['firstTime'] = 1;
				obj['email'] = '';
				var newDB = businessFileName + '.vyp';
				var fs = require('fs');
				var path = require('path');

				var app = require('electron').remote.app;

				var appPath = app.getPath('userData');
				var businessNamesPath = path.join(appPath, '/BusinessNames');
				fs.mkdir(businessNamesPath, function () {
					if (fs.existsSync(appPath + '/BusinessNames/' + newDB)) {
						newDB = businessFileName + '_' + new Date().getTime() + '.vyp';
					}
					var fileNameObj = CompanyListUtility.getCompaniesFileNameObject();
					try {
						LocalStorageHelper.setValue(LicenseInfoConstants.referralCode, _this5.state.referralCode);
					} catch (err1) {}
					fs.writeFileSync(appPath + '/BusinessNames/currentDB.txt', appPath + '/BusinessNames/' + newDB);
					var SqliteDBHelper = require('../../DBManager/SqliteDBHelper.js');
					var TransactionManager = require('../../DBManager/TransactionManager.js');
					var transactionManager = new TransactionManager();
					var sqlitedbhelper = new SqliteDBHelper();

					var isTransactionBeginSuccess = transactionManager.BeginTransaction();
					var statusCode = ErrorCode.ERROR_DB_CREATION_FAILED;
					if (isTransactionBeginSuccess) {
						try {
							MyAnalytics.pushProfile(obj);
							statusCode = sqlitedbhelper.setFirstTimeLogin(obj);
							if (statusCode == true) {
								_this5.setCountryConfig();
								if (transactionManager.CommitTransaction()) {
									if ((0, _keys2.default)(fileNameObj).includes(businessName)) {
										businessName = businessName + '_' + new Date().getTime();
									}
									fileNameObj[businessName] = {
										'path': appPath + '/BusinessNames/' + newDB,
										'company_creation_type': 0
									};
									CompanyListUtility.updateCompanyListFile(fileNameObj);
									statusCode = ErrorCode.ERROR_DB_CREATION_SUCCESS;
									$('.loaderWrapper').hide();
								} else {
									statusCode = ErrorCode.ERROR_DB_CREATION_FAILED;
								}
							}
						} catch (error) {
							var logger = require('./Utilities/logger');
							logger.error(error);
						}
						transactionManager.EndTransaction();
					} else {
						statusCode = ErrorCode.ERROR_DB_CREATION_FAILED;
					}
					if (statusCode == ErrorCode.ERROR_DB_CREATION_SUCCESS) {
						var LocalStorageConstant = require('./../../Constants/LocalStorageConstant');
						LocalStorageHelper.setValue(LocalStorageConstant.LOGIN_PHONE_NUMBER, _this5.state.phoneNumber);
						var countryStr = '';
						try {
							countryStr = (0, _stringify2.default)(_this5.state.currentCountry);
						} catch (err) {
							countryStr = '';
						}
						LocalStorageHelper.setValue(LocalStorageConstant.LOGIN_COUNTRY, countryStr);
						MyAnalytics.pushEvent('Create Company Save First Time');

						var _require = require('electron'),
						    ipcRenderer = _require.ipcRenderer;

						ipcRenderer.send('changeURL', 'Index1.html');
					} else {
						$('.loaderWrapper').hide(function () {
							var ToastHelper = require('../../UIControllers/ToastHelper.js');
							ToastHelper.errorWithoutCompanyAccess(statusCode);
							_this5.setState({ companyCreationInProgress: false });
						});
					}
				});
			} catch (err) {
				var ToastHelper = require('../../UIControllers/ToastHelper.js');
				this.setState({ companyCreationInProgress: false });
				ToastHelper.errorWithoutCompanyAccess(ErrorCode.ERROR_DB_CREATION_FAILED);
			}
		}
	}, {
		key: 'handleInput',
		value: function handleInput(event) {
			var currentVal = event.currentTarget.value;
			var phoneRegex = this.state.currentCountry.dialCode == 91 ? '^[0-9]{0,10}$' : '^[0-9]{0,15}$';
			var regex = new RegExp(phoneRegex);
			if (!regex.test(currentVal)) {
				currentVal = this.state.phoneNumber;
			} else {
				this.setState({
					phoneNumber: currentVal,
					inputError: ''
				});
			}
			event.currentTarget.value = currentVal;
		}
	}, {
		key: 'setCountryConfig',
		value: function setCountryConfig() {
			var selectedCountry = this.state.currentCountry;
			var SettingsModel = require('../../Models/SettingsModel');
			var Queries = require('../../Constants/Queries');
			var StringConstants = require('../../Constants/StringConstants');
			var InvoiceTheme = require('../../Constants/InvoiceTheme.js');
			var settingsModel = new SettingsModel();
			settingsModel.setSettingKey(Queries.SETTING_USER_COUNTRY);
			settingsModel.UpdateSetting(selectedCountry.countryCode);
			var currenciesForSelectedCountry = selectedCountry.currencySymbols;
			if (currenciesForSelectedCountry && currenciesForSelectedCountry.length && currenciesForSelectedCountry.length > 0) {
				var defaultCurrencySymbol = currenciesForSelectedCountry[0];
				settingsModel.setSettingKey(Queries.SETTING_CURRENCY_SYMBOL);
				settingsModel.UpdateSetting(defaultCurrencySymbol);
			}
			if (selectedCountry == _Country2.default.Countries.INDIA) {
				settingsModel.setSettingKey(Queries.SETTING_GST_ENABLED);
				settingsModel.UpdateSetting('1');
				settingsModel.setSettingKey(Queries.SETTING_HSN_SAC_ENABLED);
				settingsModel.UpdateSetting('1');
				settingsModel.setSettingKey(Queries.SETTING_ITEMWISE_TAX_ENABLED);
				settingsModel.UpdateSetting('1');
				settingsModel.setSettingKey(Queries.SETTING_ITEMWISE_DISCOUNT_ENABLED);
				settingsModel.UpdateSetting('1');
				settingsModel.setSettingKey(Queries.SETTING_TIN_NUMBER_ENABLED);
				settingsModel.UpdateSetting('1');
				settingsModel.setSettingKey(Queries.SETTING_ENABLE_PLACE_OF_SUPPLY);
				settingsModel.UpdateSetting('1');
				settingsModel.setSettingKey(Queries.SETTING_TAX_ENABLED);
				settingsModel.UpdateSetting('0');
				settingsModel.setSettingKey(Queries.SETTING_PRINT_TINNUMBER);
				settingsModel.UpdateSetting('1');
				settingsModel.setSettingKey(Queries.SETTING_CUSTOM_NAME_FOR_SALE);
				settingsModel.UpdateSetting('Tax Invoice');
			} else if (_Country2.default.isGulfCountryByCountry(selectedCountry)) {
				settingsModel.setSettingKey(Queries.SETTING_ITEMWISE_TAX_ENABLED);
				settingsModel.UpdateSetting('1');
				settingsModel.setSettingKey(Queries.SETTING_ITEMWISE_DISCOUNT_ENABLED);
				settingsModel.UpdateSetting('1');
				settingsModel.setSettingKey(Queries.SETTING_TIN_NUMBER_ENABLED);
				settingsModel.UpdateSetting('1');
				settingsModel.setSettingKey(Queries.SETTING_PRINT_TINNUMBER);
				settingsModel.UpdateSetting('1');
				settingsModel.setSettingKey(Queries.SETTING_THERMAL_PRINTER_THEME);
				settingsModel.UpdateSetting(InvoiceTheme.THERMAL_THEME_2 + '');
			} else {
				if (selectedCountry == _Country2.default.Countries.NEPAL) {
					settingsModel.setSettingKey(Queries.SETTING_EXPIRY_DATE_TYPE);
					settingsModel.UpdateSetting(StringConstants.dateMonthYear);
				}
				settingsModel.setSettingKey(Queries.SETTING_TAX_ENABLED);
				settingsModel.UpdateSetting('1');
				settingsModel.setSettingKey(Queries.SETTING_DISCOUNT_ENABLED);
				settingsModel.UpdateSetting('1');
			}
		}
	}]);
	return FTLogin;
}(_react2.default.Component);

exports.default = FTLogin;


_reactDom2.default.render(_react2.default.createElement(FTLogin, null), document.getElementById('ftLoginPage'));