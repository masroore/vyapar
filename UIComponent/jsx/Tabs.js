Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = Tabs;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Tabs(props) {
	var tabs = props.tabs.map(function (tab, index) {
		return _react2.default.createElement(
			'li',
			{
				key: index,
				onClick: function onClick() {
					return props.onClick(tab.label);
				},
				className: 'tab-items flex-fill ' + (tab.active ? 'bottomLine' : '') },
			tab.label
		);
	});
	return _react2.default.createElement(
		'ul',
		{ className: 'tab-container d-flex' },
		tabs
	);
}