Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _CustomTextInput = require('./UIControls/CustomTextInput');

var _CustomTextInput2 = _interopRequireDefault(_CustomTextInput);

var _CustomSelect = require('./UIControls/CustomSelect');

var _CustomSelect2 = _interopRequireDefault(_CustomSelect);

var _CustomTextArea = require('./UIControls/CustomTextArea');

var _CustomTextArea2 = _interopRequireDefault(_CustomTextArea);

var _DecimalInputFilter = require('../../Utilities/DecimalInputFilter');

var _DecimalInputFilter2 = _interopRequireDefault(_DecimalInputFilter);

var _BusinessCategoryDropdown = require('./BusinessCategoryDropdown');

var _BusinessCategoryDropdown2 = _interopRequireDefault(_BusinessCategoryDropdown);

var _styles = require('@material-ui/core/styles');

var _Dialog = require('@material-ui/core/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _StateCode = require('../../Constants/StateCode');

var _StateCode2 = _interopRequireDefault(_StateCode);

var _BusinessTypes = require('../../Constants/BusinessTypes');

var _BusinessTypes2 = _interopRequireDefault(_BusinessTypes);

var _BusinessCategories = require('../../Constants/BusinessCategories');

var _BusinessCategories2 = _interopRequireDefault(_BusinessCategories);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
	return {
		collapsed: {
			height: 330
		},
		normal: {
			height: 664
		},
		editFirmDialog: {
			width: 640,
			padding: '16px 32px',
			userSelect: 'none',
			'& .editFirmDialogHeader': {
				display: 'flex',
				justifyContent: 'space-between',
				alignItems: 'center',
				marginBottom: '32px',
				'& h4': {
					color: '#284561',
					fontFamily: 'robotoBold'
				},
				'& img': {
					opacity: '.6',
					cursor: 'pointer'
				}
			},
			'& .editFirmDialogBody': {
				marginBottom: 'auto',
				'& .basicDetails': {
					display: 'grid',
					gridTemplateColumns: '1fr 1fr',
					width: '100%',
					gridGap: '8px 40px',
					marginBottom: '24px'
				},
				'& .firmLogoDiv': {
					gridRowStart: 1,
					gridRowEnd: 'span 3',
					position: 'relative',
					'& .firmLogo': {
						position: 'absolute',
						top: '50%',
						left: '50%',
						transform: 'translate(-50%, -50%)',
						border: '1.5px dashed #E0E0E3',
						borderRadius: '8px',
						width: 110,
						height: 110,
						cursor: 'pointer',
						'& img': {
							width: '100%',
							height: '100%',
							zIndex: 2,
							position: 'relative',
							borderRadius: '8px'
						},
						'&:hover .updateLogo, &:hover .deleteLogo': {
							display: 'block'
						}
					},
					'& .addLogoBg': {
						position: 'absolute',
						top: '50%',
						left: '50%',
						transform: 'translate(-50%, -50%)',
						width: 40,
						color: '#BFBFC5',
						fontSize: '18px',
						fontFamily: 'roboto',
						lineHeight: '22px',
						textAlign: 'center'
					}
				},
				'& .updateLogo': {
					position: 'absolute',
					bottom: '0px',
					width: '100%',
					height: '30px',
					background: 'rgba(40, 69, 97, 0.85)',
					borderRadius: '0px 0px 8px 8px',
					font: '15px roboto',
					lineHeight: '30px',
					letterSpacing: '0.45px',
					color: 'white',
					textAlign: 'center',
					zIndex: 3,
					display: 'none',
					cursor: 'pointer'
				},
				'& .deleteLogo': {
					position: 'absolute',
					width: 24,
					height: 24,
					top: 0,
					right: 0,
					transform: 'translate(50%, -50%)',
					background: "transparent url('../inlineSVG/round_close_blue.svg')",
					backgroundSize: '32px',
					backgroundPosition: '26px 0',
					zIndex: 3,
					display: 'none',
					cursor: 'pointer'
				}
			},
			'& .editFirmDialogFooter': {
				display: 'flex',
				alignItems: 'center',
				padding: '32px',
				bottom: '0',
				position: 'absolute',
				right: '0',
				width: '100%',
				pointerEvents: 'none',
				'& .showMoreInfoDiv': {
					outlineColor: '#BFBFC5',
					height: '30px',
					padding: '5px',
					cursor: 'pointer',
					color: '#1789FC',
					letterSpacing: '0.48px',
					font: '16px robotoBold',
					display: 'inline-flex',
					alignItems: 'center',
					pointerEvents: 'auto',
					'& .showMoreInfoIcon': {
						width: '16px',
						height: '16px',
						background: 'url(../inlineSVG/round_add_blue.svg)',
						marginRight: '8px'
					}
				}
			}
		},
		bankDetails: {
			display: 'grid',
			gridTemplateColumns: '1fr 1fr',
			gridGap: '8px 40px',
			marginTop: '16px',
			gridAutoFlow: 'row dense',
			'& .uPIAccountNumber, & .uPIIfsc': {
				gridColumnStart: 2
			}
		},
		businessDetails: {
			display: 'grid',
			gridTemplateColumns: '1fr 1fr',
			gridGap: '8px 40px',
			marginTop: '16px',
			gridAutoFlow: 'row dense',
			gridTemplateRows: 'repeat(5, 50px)',
			'& .businessAddress': {
				gridRowStart: 1,
				gridRowEnd: 'span 2'
			},
			'& .businessType': {
				gridColumnStart: 2,
				gridRowStart: 1
			},
			'& .businessCategory': {
				gridColumnStart: 2
			},
			'& .gstin, & .state, & .tinNumber': {
				gridColumnStart: 1
			},
			'& .signatureLogoDiv': {
				gridRowEnd: 'span 2',
				gridColumnStart: 2,
				border: '1.5px dashed #E0E0E3',
				borderRadius: '8px',
				margin: '8px 0',
				position: 'relative',
				cursor: 'pointer',
				'& img': {
					width: '100%',
					height: '100%',
					zIndex: 2,
					position: 'relative',
					borderRadius: '8px',
					background: 'white'
				},
				'&:hover .updateLogo, &:hover .deleteLogo': {
					display: 'block'
				}
			},
			'& .addSignatureLogoBg': {
				position: 'absolute',
				top: '50%',
				left: '50%',
				transform: 'translate(-50%, -50%)',
				color: '#BFBFC5',
				fontSize: '13px',
				fontFamily: 'roboto',
				lineHeight: '15px'
			}
		},
		tabContainer: {
			position: 'relative',
			'&:after': {
				content: '""',
				position: 'absolute',
				width: '100%',
				border: '1px solid #E0E0E3',
				bottom: 0,
				left: 0
			}
		},
		tab: {
			background: 'none',
			font: '14px robotoBold',
			letterSpacing: '0.42px',
			color: '#BFBFC5',
			textAlign: 'left',
			padding: '8px 0px',
			marginRight: '32px',
			zIndex: 1
		},
		active: {
			color: '#1789FC',
			borderBottom: '2px solid #1789FC'
		},
		saveButton: {
			marginLeft: 'auto',
			background: '#1789FC',
			boxShadow: '0px 3px 6px rgba(0, 0, 0, 0.16)',
			borderRadius: '8px',
			width: '100px',
			textTransform: 'none',
			pointerEvents: 'auto'
		},
		noBorder: {
			border: 'none !important'
		}
	};
};

var AddEditFirmDialog = function (_React$Component) {
	(0, _inherits3.default)(AddEditFirmDialog, _React$Component);

	function AddEditFirmDialog(props) {
		(0, _classCallCheck3.default)(this, AddEditFirmDialog);

		var _this = (0, _possibleConstructorReturn3.default)(this, (AddEditFirmDialog.__proto__ || (0, _getPrototypeOf2.default)(AddEditFirmDialog)).call(this, props));

		_this.handleTabClick = function (event) {
			var dataset = event.target.dataset;

			_this.setState({
				activeTab: Number(dataset.index || 0)
			});
		};

		_this.handleMoreInformationClick = function (event) {
			event.preventDefault();
			_this.setState({
				showMoreInformation: true
			});
		};

		_this.handleChange = function (event) {
			var field = event.target.name;
			var value = event.target.value;
			var shortInputTexts = ['emailId', 'accountNumber', 'ifsc'];
			var mediumInputTexts = ['businessName', 'bankName'];
			if (shortInputTexts.includes(field)) {
				// This below statement is for DecimalInputFilter to fallback to initial value in case user enters invalid data in edit loan account scenario
				event.target.oldValue = event.target.oldValue || _this.state[field] || '';
				value = _DecimalInputFilter2.default.inputTextFilterForLength(event.target, 45);
			}
			if (mediumInputTexts.includes(field)) {
				// This below statement is for DecimalInputFilter to fallback to initial value in case user enters invalid data in edit loan account scenario
				event.target.oldValue = event.target.oldValue || _this.state[field] || '';
				value = _DecimalInputFilter2.default.inputTextFilterForLength(event.target, 200);
			}
			if (field == 'gstin' || field == 'tinNumber') {
				event.target.oldValue = event.target.oldValue || _this.state[field] || '';
				value = _DecimalInputFilter2.default.inputTextFilterForLength(event.target);
				if (field == 'gstin') {
					var state = '';
					if (value) {
						var stateNo = Number(value.substr(0, 2));
						if (!_this.displayTin && stateNo) {
							state = _StateCode2.default.getStateName(stateNo) || '';
						}
					}
					_this.setState({
						state: state
					});
				}
			}
			if (field == 'phone') {
				event.target.oldValue = event.target.oldValue || _this.state[field] || '';
				value = _DecimalInputFilter2.default.inputTextFilterForPhone(event.target);
			}
			if (field == 'businessName') {
				_this.setState({
					businessNameErrorText: ''
				});
			}
			if (field == 'gstin') {
				_this.setState({
					gstinErrorText: ''
				});
			}
			if (field == 'uPIAccountNumber') {
				if (value.trim().length > 18) {
					value = _this.state[field];
				}
				_this.setState({
					uPIAccountNumberErrorText: ''
				});
			}
			if (field == 'uPIIfsc') {
				if (value && (!/^[a-z0-9]+$/i.test(value) || value.trim().length > 11)) {
					value = _this.state[field];
				}
				_this.setState({
					uPIIfscErrorText: ''
				});
			}
			if (field == 'emailId') {
				_this.setState({
					phoneEmailErrorText: ''
				});
			}
			if (field == 'phone') {
				_this.setState({
					phoneEmailErrorText: '',
					phoneErrorText: ''
				});
			}
			_this.setState((0, _defineProperty3.default)({}, field, value));
		};

		_this.businessCategoryCallBack = function (newvalue) {
			_this.setState({
				businessCategory: newvalue
			});
		};

		_this.onSave = function () {
			var ErrorCode = require('../../Constants/ErrorCode');
			var UDFCache = require('../../Cache/UDFCache');
			var uDFCache = new UDFCache();
			var UDFFieldsConstant = require('../../Constants/UDFFieldsConstant');
			var UdfValueModel = require('../../Models/udfValueModel.js');
			var validationSuccess = _this.validateFields();
			if (!validationSuccess) {
				return;
			}
			var _this$state = _this.state,
			    businessName = _this$state.businessName,
			    phone = _this$state.phone,
			    emailId = _this$state.emailId,
			    firmLogo = _this$state.firmLogo,
			    businessAddress = _this$state.businessAddress,
			    gstin = _this$state.gstin,
			    tinNumber = _this$state.tinNumber,
			    state = _this$state.state,
			    businessType = _this$state.businessType,
			    businessCategory = _this$state.businessCategory,
			    udfFields = _this$state.udfFields,
			    signatureLogo = _this$state.signatureLogo,
			    accountNumber = _this$state.accountNumber,
			    bankName = _this$state.bankName,
			    ifsc = _this$state.ifsc,
			    uPIAccountNumber = _this$state.uPIAccountNumber,
			    uPIIfsc = _this$state.uPIIfsc;

			var extraFieldValueArray = [];
			for (var i = 1; i <= UDFFieldsConstant.TOTAL_FIRM_EXTRA_FIELDS; i++) {
				if (udfFields && udfFields.length != 0) {
					var udfValueObj = new UdfValueModel();
					var val = _this.state['udfField' + i];
					udfValueObj.setUdfFieldValue(val);
					udfValueObj.setUdfFieldType(UDFFieldsConstant.FIELD_TYPE_FIRM);
					udfValueObj.setUdfFieldId(uDFCache.getFieldIdByFieldNumberAndFirmIdForFirm(i, _this.props.firmId));
					extraFieldValueArray.push(udfValueObj);
				}
			}
			var obj = {
				firmName: businessName.trim(),
				firmPhone: phone.trim(),
				firmEmail: emailId.trim(),
				firmLogoPath: firmLogo.trim(),
				firmAddress: businessAddress.trim(),
				firmTin: tinNumber.trim(),
				firmGstin: gstin.trim(),
				firmSignPath: signatureLogo.trim(),
				firmState: state.trim(),
				bankName: bankName.trim(),
				bankIFSC: ifsc.trim(),
				extraFieldValueArray: extraFieldValueArray,
				accountNumber: accountNumber.trim(),
				upiAccountNumber: uPIAccountNumber.trim(),
				upiIFSC: uPIIfsc.trim(),
				businessType: businessType || 0,
				businessCategory: businessCategory.trim()
			};
			var FirmObject = require('./../../BizLogic/FirmObject.js');
			var firmObject = new FirmObject();
			var statusCode = '';
			if (_this.props.firmId) {
				var FirmCache = require('../../Cache/FirmCache');
				var firmCache = new FirmCache();
				firmObject = firmCache.getFirmById(_this.props.firmId);
				if (firmObject) {
					statusCode = firmObject.updateFirm(obj);
				}
			} else {
				statusCode = firmObject.SaveNewFirm(obj);
			}
			var phoneEmailErrorText = '';
			var businessNameErrorText = '';
			if (statusCode == ErrorCode.ERROR_FIRM_SAVE_SUCCESS || statusCode == ErrorCode.ERROR_FIRM_UPDATE_SUCCESS) {
				if (statusCode == ErrorCode.ERROR_FIRM_UPDATE_SUCCESS) {
					var SettingsModel = require('../../Models/SettingsModel.js');
					var settingsModel = new SettingsModel();
					settingsModel.setSettingKey(Queries.SETTING_LEADS_INFO_SENT);
					settingsModel.UpdateSetting('0', { nonSyncableSetting: true });
				}
				ToastHelper.success(statusCode);
				MyAnalytics.pushEvent('Add Firm Save');
				_this.handleClose();
			} else {
				switch (statusCode) {
					case ErrorCode.ERROR_FIRM_EMAIL_AND_PHONE_EMPTY:
						phoneEmailErrorText = 'Atleast one of phone or email are required';
						break;
					case ErrorCode.ERROR_FIRM_ALREADYEXISTS:
						businessNameErrorText = 'Firm Name should be unique';
						break;
					default:
						ToastHelper.error(statusCode);
						break;
				}
				_this.setState({
					phoneEmailErrorText: phoneEmailErrorText,
					businessNameErrorText: businessNameErrorText
				});
			}
		};

		_this.handleClose = function () {
			_this.setState({
				isOpen: false
			});
			typeof updateCompanyInfo == 'function' && updateCompanyInfo();
			_this.props.onClose && _this.props.onClose();
		};

		_this.updateLogo = function (event) {
			event.stopPropagation();
			var parent = event.target.parentElement;
			var name = parent.getAttribute('name') || event.target.getAttribute('name');

			var _require = require('../../Utilities/ImageHelper'),
			    openSaveImageDialog = _require.openSaveImageDialog;

			var title = name == 'firmLogo' ? 'Select Firm Logo' : 'Select Signature Image';
			openSaveImageDialog({ title: title }, function (imageName, selectedFile) {
				_this.setState((0, _defineProperty3.default)({}, name, selectedFile));
			});
		};

		_this.deleteLogo = function (event) {
			event.stopPropagation();
			var name = event.currentTarget.getAttribute('name');
			_this.setState((0, _defineProperty3.default)({}, name, ''));
		};

		_this.zoomImage = function (event) {
			event.stopPropagation();
			var parent = event.currentTarget.parentElement;
			var name = parent.getAttribute('name');
			var image = _this.state[name];
			if (image) {
				$('#zoomImageDialog').removeClass('hide').dialog({
					width: 700,
					height: 500,
					appendTo: '#dynamicDiv',
					close: function close() {
						$('#zoomImageDialog').addClass('hide').dialog('close').dialog('destroy');
					}
				});
				$('#zoomImageDialog img').attr('src', image);
			}
		};

		_this.state = {
			isOpen: true,
			saveInProgress: false,
			showMoreInformation: false,
			activeTab: 0,
			businessName: '',
			phone: '',
			emailId: '',
			bankName: '',
			accountNumber: '',
			ifsc: '',
			uPIAccountNumber: '',
			uPIIfsc: '',
			businessAddress: '',
			gstin: '',
			tinNumber: '',
			businessType: '',
			businessCategory: '',
			state: '',
			signatureLogo: '',
			firmLogo: '',
			businessNameErrorText: '',
			gstinErrorText: '',
			phoneEmailErrorText: '',
			phoneErrorText: ''
		};
		return _this;
	}

	(0, _createClass3.default)(AddEditFirmDialog, [{
		key: 'canDisplayUPIFields',
		value: function canDisplayUPIFields() {
			var SettingCache = require('../../Cache/SettingCache');
			var FirmCache = require('../../Cache/FirmCache');
			var settingCache = new SettingCache();
			var firmCache = new FirmCache();
			var canShowUpiFields = false;
			if (settingCache.getIsPrintQRCodeEnabled()) {
				var firmList = (0, _values2.default)(firmCache.getFirmList() || {});
				canShowUpiFields = firmList.find(function (firm) {
					return firm.getUpiAccountNumber() !== '' && firm.getUpiIFSC() !== '';
				});
			}
			return canShowUpiFields;
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.addEventListeners();
			this.stateList = _StateCode2.default.getStateList();
			this.businessTypeList = _BusinessTypes2.default.getBusinessTypes();
			this.businessCategoryList = _BusinessCategories2.default.getBusinessCategories();
			this.displayUPIFields = this.canDisplayUPIFields();
			this.displayTin = false;
			this.firmGSTEnabled = false;
			this.tinLabel = '';
			var SettingCache = require('../../Cache/SettingCache');
			var settingCache = new SettingCache();
			this.isCurrentCountryIndia = settingCache.isCurrentCountryIndia();
			if (settingCache.isTINNumberEnabled()) {
				this.firmGSTEnabled = true;
				if (settingCache.getGSTEnabled()) {
					this.tinLabel = 'GSTIN';
				} else {
					this.displayTin = true;
					this.tinLabel = settingCache.getTINText();
				}
			}
			if (this.props.firmId) {
				this.loadData(this.props.firmId);
			}
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			this.removeEventListeners();
		}
	}, {
		key: 'addEventListeners',
		value: function addEventListeners() {
			document.addEventListener('defaultpage-shortcut-triggered', this.handleClose);
			document.addEventListener('defaultpageother-shortcut-triggered', this.handleClose);
		}
	}, {
		key: 'removeEventListeners',
		value: function removeEventListeners() {
			document.removeEventListener('defaultpage-shortcut-triggered', this.handleClose);
			document.removeEventListener('defaultpageother-shortcut-triggered', this.handleClose);
		}
	}, {
		key: 'loadData',
		value: function loadData(firmId) {
			var FirmCache = require('../../Cache/FirmCache');
			var firmCache = new FirmCache();
			var firm = firmCache.getFirmById(firmId);
			if (firm) {
				var businessName = firm.getFirmName() || '';
				var phone = firm.getFirmPhone() || '';
				var emailId = firm.getFirmEmail() || '';
				var bankName = firm.getBankName() || '';
				var accountNumber = firm.getAccountNumber() || '';
				var ifsc = firm.getBankIFSC() || '';
				var uPIAccountNumber = firm.getUpiAccountNumber() || '';
				var uPIIfsc = firm.getUpiIFSC() || '';
				var businessAddress = firm.getFirmAddress() || '';
				var gstin = firm.getFirmHSNSAC() || '';
				var tinNumber = firm.getFirmTin() || '';
				var businessType = firm.getBusinessType() || '';
				var businessCategory = firm.getBusinessCategory() || '';
				var state = firm.getFirmState() || '';
				var firmLogo = '';
				var signatureLogo = '';
				if (Number(firm.getFirmLogoId())) {
					firmLogo = 'data:image/gif;base64,' + firm.getFirmImagePath();
				}
				if (Number(firm.getFirmSignId())) {
					signatureLogo = 'data:image/gif;base64,' + firm.getFirmSignImagePath();
				}
				var extraFieldObjectArray = firm.getUdfObjectArray() || [];
				var udfFields = [];
				var udfFieldValues = {};
				var UDFCache = require('../../Cache/UDFCache');
				var uDFCache = new UDFCache();
				extraFieldObjectArray.forEach(function (fieldModel) {
					var udfModel = uDFCache.getUdfFirmModelByFirmAndFieldId(firm.getFirmId(), fieldModel.getUdfFieldId());
					if (udfModel.getUdfFieldStatus() == 1) {
						udfFields.push(udfModel);
						udfFieldValues['udfField' + udfModel.getUdfFieldNumber()] = fieldModel.getUdfFieldValue() || '';
					}
				});
				this.setState((0, _extends3.default)({
					businessName: businessName,
					phone: phone,
					emailId: emailId,
					bankName: bankName,
					accountNumber: accountNumber,
					ifsc: ifsc,
					uPIAccountNumber: uPIAccountNumber,
					uPIIfsc: uPIIfsc,
					businessAddress: businessAddress,
					gstin: gstin,
					tinNumber: tinNumber,
					businessType: businessType,
					businessCategory: businessCategory,
					state: state,
					firmLogo: firmLogo,
					signatureLogo: signatureLogo,
					udfFields: udfFields
				}, udfFieldValues));
			}
		}
	}, {
		key: 'validateFields',
		value: function validateFields() {
			var _state = this.state,
			    businessName = _state.businessName,
			    gstin = _state.gstin,
			    uPIIfsc = _state.uPIIfsc,
			    uPIAccountNumber = _state.uPIAccountNumber,
			    phone = _state.phone,
			    emailId = _state.emailId,
			    phoneEmailErrorText = _state.phoneEmailErrorText;

			var StringConstants = require('../../Constants/StringConstants');
			var ErrorCode = require('../../Constants/ErrorCode');

			var businessNameErrorText = '';
			if (!businessName.trim()) {
				businessNameErrorText = 'Cannot be empty';
			}

			var phoneErrorText = '';
			if (phone.trim() && phone.trim().length < 5) {
				phoneErrorText = 'Invalid phone number';
			}

			if (emailId.trim() && !emailId.includes('@')) {
				phoneEmailErrorText = 'Invalid email address';
			}
			var uPIAccountNumberErrorText = '';
			uPIAccountNumber = uPIAccountNumber.trim();
			if (uPIAccountNumber && (uPIAccountNumber.length < 9 || uPIAccountNumber.length > 18)) {
				uPIAccountNumberErrorText = 'Should be between 9 to 18 digits.';
				ToastHelper.error('UPI account no. should be between 9 to 18 digits.');
			}

			var uPIIfscErrorText = '';
			if (uPIIfsc.trim() && uPIIfsc.trim().length != 11) {
				uPIIfscErrorText = 'Should be 11 characters. E.g. HDFC1234567';
				ToastHelper.error('IFSC Code Should be 11 characters. E.g. HDFC1234567');
			}

			var gstinErrorText = '';
			var regex = new RegExp(StringConstants.GSTINregex);
			if (!(regex.test(gstin) || gstin.trim() == '')) {
				gstinErrorText = ErrorCode.ERROR_GST_INVALID;
				ToastHelper.error(ErrorCode.ERROR_GST_INVALID);
			}
			this.setState({
				businessNameErrorText: businessNameErrorText,
				gstinErrorText: gstinErrorText,
				uPIAccountNumberErrorText: uPIAccountNumberErrorText,
				uPIIfscErrorText: uPIIfscErrorText,
				phoneErrorText: phoneErrorText,
				phoneEmailErrorText: phoneEmailErrorText
			});
			return !(businessNameErrorText || gstinErrorText || uPIAccountNumberErrorText || uPIIfscErrorText || phoneErrorText || phoneEmailErrorText);
		}
	}, {
		key: 'getBusinessDetailsDiv',
		value: function getBusinessDetailsDiv() {
			var classes = this.props.classes;
			var _state2 = this.state,
			    businessAddress = _state2.businessAddress,
			    gstin = _state2.gstin,
			    tinNumber = _state2.tinNumber,
			    state = _state2.state,
			    businessCategory = _state2.businessCategory,
			    businessType = _state2.businessType,
			    signatureLogo = _state2.signatureLogo,
			    gstinErrorText = _state2.gstinErrorText;

			return _react2.default.createElement(
				'div',
				{ className: classes.businessDetails },
				_react2.default.createElement(_CustomTextArea2.default, {
					rootClass: 'businessAddress',
					label: 'Business Address',
					value: businessAddress,
					name: 'businessAddress',
					rows: '4',
					onChange: this.handleChange
				}),
				!this.displayTin && this.firmGSTEnabled && _react2.default.createElement(_CustomTextInput2.default, {
					label: 'GSTIN',
					value: gstin,
					name: 'gstin',
					rootClass: 'gstin',
					error: !!gstinErrorText,
					helperText: gstinErrorText,
					onChange: this.handleChange
				}),
				this.displayTin && _react2.default.createElement(_CustomTextInput2.default, {
					label: this.tinLabel,
					value: tinNumber,
					name: 'tinNumber',
					rootClass: 'tinNumber',
					onChange: this.handleChange
				}),
				_react2.default.createElement(
					_CustomSelect2.default,
					{
						label: 'Business Type',
						value: businessType,
						name: 'businessType',
						rootClass: 'businessType',
						onChange: this.handleChange,
						shrinkLabel: true
					},
					_react2.default.createElement(
						'option',
						{ 'aria-label': 'None', value: '' },
						'None'
					),
					this.businessTypeList.map(function (type, index) {
						return _react2.default.createElement(
							'option',
							{ value: type, key: index },
							_BusinessTypes2.default.getBusinessTypeForUI(Number(type))
						);
					})
				),
				_react2.default.createElement(_BusinessCategoryDropdown2.default, {
					onChange: this.businessCategoryCallBack,
					inputProps: {
						value: businessCategory,
						inputClass: classes.businessCategory,
						label: 'Business Category',
						name: 'businessCategory'
					}
				}),
				this.isCurrentCountryIndia && _react2.default.createElement(
					_CustomSelect2.default,
					{
						label: 'State',
						value: state,
						name: 'state',
						rootClass: 'state',
						onChange: this.handleChange,
						shrinkLabel: true,
						disabled: _StateCode2.default.getStateName(Number(gstin.substr(0, 2))) && _StateCode2.default.getStateName(Number(gstin.substr(0, 2))) == state
					},
					_react2.default.createElement(
						'option',
						{ 'aria-label': 'None', value: '' },
						'None'
					),
					this.stateList.map(function (stateStr, index) {
						return _react2.default.createElement(
							'option',
							{ value: stateStr, key: index },
							stateStr
						);
					})
				),
				_react2.default.createElement(
					'div',
					{
						name: 'signatureLogo',
						className: (0, _classnames2.default)('signatureLogoDiv', signatureLogo ? classes.noBorder : ''),
						onClick: this.updateLogo
					},
					!signatureLogo && _react2.default.createElement(
						'div',
						{ className: 'addSignatureLogoBg' },
						'Add Signature'
					),
					signatureLogo ? _react2.default.createElement('img', { onClick: this.zoomImage, src: signatureLogo, alt: 'Sign Logo' }) : '',
					_react2.default.createElement(
						'span',
						{ className: 'updateLogo', onClick: this.updateLogo },
						signatureLogo ? 'Change' : 'Upload'
					),
					signatureLogo ? _react2.default.createElement('span', { onClick: this.deleteLogo, name: 'signatureLogo', className: 'deleteLogo' }) : ''
				)
			);
		}
	}, {
		key: 'getBankDetailsDiv',
		value: function getBankDetailsDiv() {
			var classes = this.props.classes;
			var _state3 = this.state,
			    bankName = _state3.bankName,
			    accountNumber = _state3.accountNumber,
			    ifsc = _state3.ifsc,
			    uPIAccountNumber = _state3.uPIAccountNumber,
			    uPIIfsc = _state3.uPIIfsc,
			    uPIAccountNumberErrorText = _state3.uPIAccountNumberErrorText,
			    uPIIfscErrorText = _state3.uPIIfscErrorText;

			return _react2.default.createElement(
				'div',
				{ className: classes.bankDetails },
				this.displayUPIFields && _react2.default.createElement(_CustomTextInput2.default, {
					label: 'UPI Account No.',
					value: uPIAccountNumber,
					type: 'number',
					name: 'uPIAccountNumber',
					rootClass: 'uPIAccountNumber',
					helperText: uPIAccountNumberErrorText,
					error: !!uPIAccountNumberErrorText,
					onChange: this.handleChange
				}),
				this.displayUPIFields && _react2.default.createElement(_CustomTextInput2.default, {
					label: 'UPI IFSC',
					value: uPIIfsc,
					name: 'uPIIfsc',
					rootClass: 'uPIIfsc',
					helperText: uPIIfscErrorText,
					error: !!uPIIfscErrorText,
					onChange: this.handleChange
				}),
				_react2.default.createElement(_CustomTextInput2.default, {
					label: 'Bank Name',
					value: bankName,
					name: 'bankName',
					onChange: this.handleChange
				}),
				_react2.default.createElement(_CustomTextInput2.default, {
					label: 'Account No.',
					value: accountNumber,
					name: 'accountNumber',
					onChange: this.handleChange
				}),
				_react2.default.createElement(_CustomTextInput2.default, {
					label: 'IFSC',
					value: ifsc,
					name: 'ifsc',
					onChange: this.handleChange
				})
			);
		}
	}, {
		key: 'getAdditionalFieldsDiv',
		value: function getAdditionalFieldsDiv() {
			var _this2 = this;

			var classes = this.props.classes;
			var udfFields = this.state.udfFields;

			return _react2.default.createElement(
				'div',
				{ className: classes.bankDetails },
				udfFields.map(function (udfField) {
					return _react2.default.createElement(_CustomTextInput2.default, {
						label: udfField.getUdfFieldName(),
						key: 'udfField' + udfField.getUdfFieldNumber(),
						name: 'udfField' + udfField.getUdfFieldNumber(),
						value: _this2.state['udfField' + udfField.getUdfFieldNumber()],
						maxlength: '30',
						onChange: _this2.handleChange
					});
				})
			);
		}
	}, {
		key: 'render',
		value: function render() {
			var _this3 = this;

			var _props = this.props,
			    firmId = _props.firmId,
			    classes = _props.classes;
			var _state4 = this.state,
			    activeTab = _state4.activeTab,
			    businessName = _state4.businessName,
			    phone = _state4.phone,
			    firmLogo = _state4.firmLogo,
			    emailId = _state4.emailId,
			    showMoreInformation = _state4.showMoreInformation,
			    udfFields = _state4.udfFields,
			    saveInProgress = _state4.saveInProgress,
			    businessNameErrorText = _state4.businessNameErrorText,
			    phoneEmailErrorText = _state4.phoneEmailErrorText,
			    phoneErrorText = _state4.phoneErrorText;

			var tabs = ['Business Details', 'Bank Details'];
			if (udfFields && udfFields.length) {
				tabs.splice(1, 0, 'Additional Fields');
			}
			return _react2.default.createElement(
				_Dialog2.default,
				{
					onClose: this.handleClose,
					disableEnforceFocus: true,
					open: this.state.isOpen,
					fullScreen: false,
					maxWidth: false
				},
				_react2.default.createElement(
					'div',
					{ className: (0, _classnames2.default)(classes.editFirmDialog, showMoreInformation ? classes.normal : classes.collapsed, 'vyaparFormToValidate') },
					_react2.default.createElement(
						'div',
						{ className: 'editFirmDialogHeader' },
						_react2.default.createElement(
							'h4',
							null,
							firmId ? 'Edit Firm' : 'Add Firm'
						),
						_react2.default.createElement('img', { id: 'close', src: '../inlineSVG/close_black.svg', alt: 'X', onClick: this.handleClose })
					),
					_react2.default.createElement(
						'div',
						{ className: 'editFirmDialogBody' },
						_react2.default.createElement(
							'div',
							{ className: 'basicDetails' },
							_react2.default.createElement(_CustomTextInput2.default, {
								label: 'Business Name',
								value: businessName,
								name: 'businessName',
								error: !!businessNameErrorText,
								helperText: businessNameErrorText,
								onChange: this.handleChange,
								autoFocus: true
							}),
							_react2.default.createElement(_CustomTextInput2.default, {
								label: 'Phone No.',
								value: phone,
								name: 'phone',
								onChange: this.handleChange,
								helperText: phoneErrorText,
								error: !!phoneErrorText || phoneEmailErrorText == 'Atleast one of phone or email are required'
							}),
							_react2.default.createElement(_CustomTextInput2.default, {
								label: 'Email ID',
								value: emailId,
								name: 'emailId',
								onChange: this.handleChange,
								error: !!phoneEmailErrorText,
								helperText: phoneEmailErrorText
							}),
							_react2.default.createElement(
								'div',
								{ className: 'firmLogoDiv' },
								_react2.default.createElement(
									'div',
									{
										name: 'firmLogo',
										className: (0, _classnames2.default)('firmLogo', firmLogo ? classes.noBorder : ''),
										onClick: this.updateLogo
									},
									firmLogo ? _react2.default.createElement('img', { src: firmLogo, onClick: this.zoomImage, alt: 'Firm Logo' }) : '',
									_react2.default.createElement(
										'span',
										{ className: 'updateLogo', onClick: this.updateLogo },
										firmLogo ? 'Change' : 'Upload'
									),
									firmLogo ? _react2.default.createElement('span', { onClick: this.deleteLogo, name: 'firmLogo', className: 'deleteLogo' }) : '',
									!firmLogo && _react2.default.createElement(
										'span',
										{ className: 'addLogoBg' },
										'Add Logo'
									)
								)
							)
						),
						showMoreInformation && _react2.default.createElement(
							'div',
							{ className: 'otherDetails' },
							_react2.default.createElement(
								'div',
								{ className: classes.tabContainer },
								tabs.map(function (tab, index) {
									return _react2.default.createElement(
										'button',
										{ key: index, 'data-index': index, className: (0, _classnames2.default)(classes.tab, activeTab == index ? classes.active : ''), onClick: _this3.handleTabClick },
										tab
									);
								})
							),
							activeTab == tabs.indexOf('Business Details') && this.getBusinessDetailsDiv(),
							activeTab == tabs.indexOf('Additional Fields') && this.getAdditionalFieldsDiv(),
							activeTab == tabs.indexOf('Bank Details') && this.getBankDetailsDiv()
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'editFirmDialogFooter' },
						!showMoreInformation && _react2.default.createElement(
							'a',
							{
								className: 'showMoreInfoDiv',
								onClick: this.handleMoreInformationClick,
								tabIndex: '0',
								onKeyUp: function onKeyUp(event) {
									if (event.keyCode == 13) {
										_this3.handleMoreInformationClick(event);
									}
								}
							},
							_react2.default.createElement('span', { className: 'showMoreInfoIcon' }),
							_react2.default.createElement(
								'span',
								{ className: 'showMoreInfoText' },
								'More Information'
							)
						),
						_react2.default.createElement(
							_Button2.default,
							{
								'data-shortcut': 'CTRL_S',
								disabled: saveInProgress,
								classes: {
									root: classes.saveButton
								},
								color: 'primary',
								variant: 'contained',
								onClick: this.onSave
							},
							'Save'
						)
					)
				)
			);
		}
	}]);
	return AddEditFirmDialog;
}(_react2.default.Component);

AddEditFirmDialog.propTypes = {
	firmId: _propTypes2.default.string,
	onClose: _propTypes2.default.func,
	onSave: _propTypes2.default.func,
	title: _propTypes2.default.string,
	classes: _propTypes2.default.object
};

exports.default = (0, _styles.withStyles)(styles)(AddEditFirmDialog);