Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _SelectUnitModal = require('./SelectUnitModal');

var _SelectUnitModal2 = _interopRequireDefault(_SelectUnitModal);

var _ItemUnitMappingCache = require('../../Cache/ItemUnitMappingCache');

var _ItemUnitMappingCache2 = _interopRequireDefault(_ItemUnitMappingCache);

var _ItemUnitCache = require('../../Cache/ItemUnitCache');

var _ItemUnitCache2 = _interopRequireDefault(_ItemUnitCache);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SelectUnitButton = function (_React$Component) {
	(0, _inherits3.default)(SelectUnitButton, _React$Component);

	function SelectUnitButton(props) {
		(0, _classCallCheck3.default)(this, SelectUnitButton);

		var _this = (0, _possibleConstructorReturn3.default)(this, (SelectUnitButton.__proto__ || (0, _getPrototypeOf2.default)(SelectUnitButton)).call(this, props));

		_this.state = {
			show: false,
			baseUnit: $('#baseUnitId').val(),
			secondaryUnit: $('#secondaryUnitId').val(),
			selectedUnit: $('#mappingId').val()
		};
		_this.onClick = _this.onClick.bind(_this);
		_this.onClose = _this.onClose.bind(_this);
		_this.onSave = _this.onSave.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(SelectUnitButton, [{
		key: 'onClick',
		value: function onClick() {
			this.setState({
				show: true,
				baseUnit: $('#baseUnitId').val(),
				secondaryUnit: $('#secondaryUnitId').val(),
				selectedUnit: $('#mappingId').val()
			});
		}
	}, {
		key: 'onClose',
		value: function onClose() {
			this.setState({ show: false });
		}
	}, {
		key: 'onSave',
		value: function onSave(_ref) {
			var baseUnit = _ref.baseUnit,
			    selectedMappingId = _ref.selectedMappingId;

			var itemUnitMappingCache = new _ItemUnitMappingCache2.default();
			var itemUnitCache = new _ItemUnitCache2.default();
			if (selectedMappingId) {
				var mapping = itemUnitMappingCache.getItemUnitMapping(selectedMappingId);
				var baseUnitObj = itemUnitCache.getItemUnitById(mapping.getBaseUnitId());
				var secondaryUnit = itemUnitCache.getItemUnitById(mapping.getSecondaryUnitId());
				$('#baseUnitId').val(mapping.getBaseUnitId());
				$('#secondaryUnitId').val(mapping.getSecondaryUnitId());
				$('#mappingId').val(selectedMappingId);
				$('#selectedUnit').html('1 ' + baseUnitObj.getUnitShortName() + ' = ' + mapping.getConversionRate() + ' ' + secondaryUnit.getUnitShortName());
			} else if (baseUnit) {
				var _baseUnitObj = itemUnitCache.getItemUnitById(baseUnit);
				$('#baseUnitId').val(baseUnit);
				$('#secondaryUnitId').val('');
				$('#mappingId').val('');
				$('#selectedUnit').html('' + _baseUnitObj.getUnitShortName());
			} else {
				$('#baseUnitId').val('');
				$('#secondaryUnitId').val('');
				$('#mappingId').val('');
				$('#selectedUnit').html('');
			}
			this.setState({ show: false });
		}
	}, {
		key: 'render',
		value: function render() {
			var Label = this.state.selectedUnit ? 'Edit Unit' : 'Select Unit';
			return _react2.default.createElement(
				'div',
				{ id: 'rootElement' },
				this.state.show && _react2.default.createElement(_SelectUnitModal2.default, {
					isOpen: this.state.show,
					baseUnit: this.state.baseUnit,
					secondaryUnit: this.state.secondaryUnit,
					selectedUnit: this.state.selectedUnit,
					onSave: this.onSave,
					allowBlank: true,
					onClose: this.onClose }),
				_react2.default.createElement(
					'div',
					{ id: 'selectUnitLabel', onClick: this.onClick },
					Label
				)
			);
		}
	}]);
	return SelectUnitButton;
}(_react2.default.Component);

exports.default = SelectUnitButton;