Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _AddEditCategoryModal = require('./AddEditCategoryModal');

var _AddEditCategoryModal2 = _interopRequireDefault(_AddEditCategoryModal);

var _ButtonDropdown = require('./ButtonDropdown');

var _ButtonDropdown2 = _interopRequireDefault(_ButtonDropdown);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AddExpenseCategoryButton = function (_React$Component) {
	(0, _inherits3.default)(AddExpenseCategoryButton, _React$Component);

	function AddExpenseCategoryButton(props) {
		(0, _classCallCheck3.default)(this, AddExpenseCategoryButton);

		var _this = (0, _possibleConstructorReturn3.default)(this, (AddExpenseCategoryButton.__proto__ || (0, _getPrototypeOf2.default)(AddExpenseCategoryButton)).call(this, props));

		_this.state = {
			isOpen: false
		};
		_this.addNewCategory = _this.addNewCategory.bind(_this);
		_this.onSave = _this.onSave.bind(_this);
		_this.onClose = _this.onClose.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(AddExpenseCategoryButton, [{
		key: 'addNewCategory',
		value: function addNewCategory() {
			this.setState({ isOpen: true });
			MyAnalytics.pushEvent('Add New Expense Category Open');
		}
	}, {
		key: 'getMenuOptions',
		value: function getMenuOptions() {
			var menuOptions = [];
			menuOptions.push({
				buttonText: 'Add Category',
				buttonIcon: _react2.default.createElement(
					'span',
					null,
					'+'
				),
				onClick: this.addNewCategory
			});
			return menuOptions;
		}
	}, {
		key: 'onSave',
		value: function onSave(name) {
			var ToastHelper = require('../../UIControllers/ToastHelper');
			name = name || '';
			name = name.trim();
			if (name) {
				var NameLogic = require('../../BizLogic/nameLogic.js');
				var NameType = require('../../Constants/NameType.js');
				var namelogic = new NameLogic();
				var obj = {
					name: name,
					phone_number: '',
					addressStr: '',
					email: '',
					groupId: '',
					tinNumber: '',
					opening_balance: '',
					opening_balance_date: '',
					isReceivable: true,
					nameType: NameType.NAME_TYPE_EXPENSE
				};
				var statusCode = namelogic.saveNewName(obj, true);
				if (statusCode == ErrorCode.ERROR_NAME_SAVE_SUCCESS) {
					statusCode = ErrorCode.ERROR_EXPENSE_SAVE_SUCCESS;
					ToastHelper.success(statusCode);
					MyAnalytics.pushEvent('Add Expense Category Save');
				} else if (statusCode == ErrorCode.ERROR_NAME_ALREADY_EXISTS) {
					statusCode = ErrorCode.ERROR_EXPENSE_NAME_EXISTS;
					ToastHelper.error(statusCode);
				} else {
					statusCode = ErrorCode.ERROR_EXPENSE_SAVE_FAILED;
					ToastHelper.error(statusCode);
				}
				this.setState({
					isOpen: false
				});
				window.onResume && window.onResume();
			} else {
				ToastHelper.error('Expense category name can not be empty');
			}
		}
	}, {
		key: 'onClose',
		value: function onClose() {
			this.setState({ isOpen: false });
		}
	}, {
		key: 'render',
		value: function render() {
			var isOpen = this.state.isOpen;

			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement(_AddEditCategoryModal2.default, {
					categoryName: '',
					onClose: this.onClose,
					onSave: this.onSave,
					isOpen: isOpen,
					title: 'Add Expense Category',
					label: 'Enter Expense Category Name'
				}),
				_react2.default.createElement(_ButtonDropdown2.default, {
					collapsed: this.props.collapsed,
					addPadding: true,
					menuOptions: this.getMenuOptions()
				})
			);
		}
	}]);
	return AddExpenseCategoryButton;
}(_react2.default.Component);

AddExpenseCategoryButton.propTypes = {
	classNames: _propTypes2.default.string,
	collapsed: _propTypes2.default.bool
};

exports.default = AddExpenseCategoryButton;