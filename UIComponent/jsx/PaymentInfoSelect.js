Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/core/styles');

var _DynamicList = require('./UIControls/DynamicList');

var _DynamicList2 = _interopRequireDefault(_DynamicList);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _AddEditBankAccountModal = require('./AddEditBankAccountModal');

var _AddEditBankAccountModal2 = _interopRequireDefault(_AddEditBankAccountModal);

var _MenuItem = require('@material-ui/core/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _PaymentInfoCache = require('../../Cache/PaymentInfoCache');

var _PaymentInfoCache2 = _interopRequireDefault(_PaymentInfoCache);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
	return {};
};

var PaymentInfoSelect = function (_Component) {
	(0, _inherits3.default)(PaymentInfoSelect, _Component);

	function PaymentInfoSelect(props) {
		(0, _classCallCheck3.default)(this, PaymentInfoSelect);

		var _this = (0, _possibleConstructorReturn3.default)(this, (PaymentInfoSelect.__proto__ || (0, _getPrototypeOf2.default)(PaymentInfoSelect)).call(this, props));

		_this.addNewBankOption = function () {
			var comp = _react2.default.createElement(
				_Button2.default,
				{
					onClick: function onClick() {
						return _this.setState({ showAddBankAccountModal: true });
					},
					variant: 'outlined',
					color: 'primary',
					size: 'small'
				},
				'Add new bank account?'
			);
			return comp;
		};

		_this.onSaveNewBankAccount = function (paymentType) {
			var paymentInfoCache = new _PaymentInfoCache2.default();
			var paymentTypeId = paymentInfoCache.getPaymentBankId(paymentType);
			paymentTypeId = paymentTypeId || '1';
			_this.setState({
				selectedPaymentType: paymentType,
				showAddBankAccountModal: false
			});
			_this.props.optionSelectCallback && _this.props.optionSelectCallback(paymentTypeId);
			_this.props.onPaymentOptionsUpdate && _this.props.onPaymentOptionsUpdate();
		};

		_this.onSelect = function (paymentType) {
			var paymentInfoCache = new _PaymentInfoCache2.default();
			var paymentTypeId = paymentInfoCache.getPaymentBankId(paymentType);
			paymentTypeId = paymentTypeId || '1';
			_this.setState({
				selectedPaymentType: paymentType
			});
			_this.props.optionSelectCallback(paymentTypeId);
		};

		_this.listRef = _react2.default.createRef();
		var selectedPaymentType = 'Cash';
		if (_this.props.defaultPaymentTypeId) {
			var paymentInfoCache = new _PaymentInfoCache2.default();
			selectedPaymentType = paymentInfoCache.getPaymentBankName(_this.props.defaultPaymentTypeId) || 'Cash';
		}
		_this.state = {
			showAddBankAccountModal: false,
			selectedPaymentType: selectedPaymentType
		};
		return _this;
	}

	(0, _createClass3.default)(PaymentInfoSelect, [{
		key: 'componentDidUpdate',
		value: function componentDidUpdate(prevProps) {
			if (prevProps.paymentOptions != this.props.paymentOptions) {
				this.listRef.current.updateComponent(this.props.paymentOptions, this.state.selectedPaymentType, this.state.selectedPaymentType);
			}
			if (prevProps.defaultPaymentTypeId != this.props.defaultPaymentTypeId) {
				var paymentInfoCache = new _PaymentInfoCache2.default();
				var selectedPaymentType = paymentInfoCache.getPaymentBankName(this.props.defaultPaymentTypeId) || 'Cash';
				this.setState({
					selectedPaymentType: selectedPaymentType
				});
				this.listRef.current.updateComponent(this.props.paymentOptions, selectedPaymentType, selectedPaymentType);
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var _this2 = this;

			var selectedPaymentType = this.state.selectedPaymentType;

			return _react2.default.createElement(
				_react.Fragment,
				null,
				_react2.default.createElement(
					_DynamicList2.default,
					{
						ref: this.listRef,
						required: true,
						label: this.props.label,
						value: selectedPaymentType,
						optionList: this.props.paymentOptions,
						selectedOption: selectedPaymentType,
						optionSelectCallback: this.onSelect,
						addComponent: this.addNewBankOption(),
						disabled: !!this.props.disabled,
						allowDelete: false
					},
					this.props.paymentOptions.map(function (paymentOption) {
						return _react2.default.createElement(
							_MenuItem2.default,
							{ key: paymentOption.key, value: paymentOption.value },
							paymentOption.value
						);
					})
				),
				_react2.default.createElement(_AddEditBankAccountModal2.default, {
					isOpen: this.state.showAddBankAccountModal,
					onClose: function onClose() {
						return _this2.setState({ showAddBankAccountModal: false });
					},
					onSave: this.onSaveNewBankAccount
				})
			);
		}
	}]);
	return PaymentInfoSelect;
}(_react.Component);

PaymentInfoSelect.propTypes = {
	classes: _propTypes2.default.object,
	label: _propTypes2.default.string,
	optionSelectCallback: _propTypes2.default.func,
	defaultPaymentTypeId: _propTypes2.default.string,
	paymentOptions: _propTypes2.default.arrayOf(_propTypes2.default.shape({
		key: _propTypes2.default.string,
		value: _propTypes2.default.string
	})),
	disabled: _propTypes2.default.bool,
	onPaymentOptionsUpdate: _propTypes2.default.func
};

exports.default = (0, _styles.withStyles)(styles)(PaymentInfoSelect);