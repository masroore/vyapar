Object.defineProperty(exports, "__esModule", {
  value: true
});

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Modal = require('./Modal');

var _Modal2 = _interopRequireDefault(_Modal);

var _PaymentTermList = require('./PaymentTermList');

var _PaymentTermList2 = _interopRequireDefault(_PaymentTermList);

var _ErrorCode = require('../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

var _PaymentTermCache = require('../../Cache/PaymentTermCache');

var _PaymentTermCache2 = _interopRequireDefault(_PaymentTermCache);

var _PaymentTermModel = require('../../Models/PaymentTermModel');

var _PaymentTermModel2 = _interopRequireDefault(_PaymentTermModel);

var _DataLoader = require('../../DBManager/DataLoader');

var _DataLoader2 = _interopRequireDefault(_DataLoader);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SetPaymentTermModal = function (_React$Component) {
  (0, _inherits3.default)(SetPaymentTermModal, _React$Component);

  function SetPaymentTermModal(props) {
    (0, _classCallCheck3.default)(this, SetPaymentTermModal);

    var _this = (0, _possibleConstructorReturn3.default)(this, (SetPaymentTermModal.__proto__ || (0, _getPrototypeOf2.default)(SetPaymentTermModal)).call(this, props));

    _this.modifyIsDefault = function (id, e) {
      var is_default = e.target.checked;
      if (!is_default) {
        ToastHelper.info('There should be atleast one default payment term.');
        return;
      }
      var tempObj = _this.state.record;
      var status = _ErrorCode2.default.ERROR_PAYMENT_TERM_UPDATE_SUCCESS;

      if (id > _this.TEMP_ID) {
        ToastHelper.info('Please create payment term first!');
        return;
      }

      for (i in tempObj) {
        var rowObj = tempObj[i];
        if (rowObj.is_default) {
          var paymentTermModel = new _PaymentTermModel2.default();
          paymentTermModel.setPaymentTermId(rowObj.id);
          paymentTermModel.setPaymentTermIsDefault(0);
          status = paymentTermModel.changeDefaultPaymentTerm();
          if (status == _ErrorCode2.default.ERROR_PAYMENT_TERM_UPDATE_SUCCESS) {
            tempObj[rowObj.id].is_default = false;
          }
        }
      }

      if (status == _ErrorCode2.default.ERROR_PAYMENT_TERM_UPDATE_SUCCESS) {
        tempObj[id].is_default = is_default;
        var paymentTermModel = new _PaymentTermModel2.default();
        paymentTermModel.setPaymentTermId(tempObj[id].id);
        paymentTermModel.setPaymentTermIsDefault(tempObj[id].is_default ? 1 : 0);
        status = paymentTermModel.changeDefaultPaymentTerm();
        if (status == _ErrorCode2.default.ERROR_PAYMENT_TERM_UPDATE_SUCCESS) {
          _this.setState({
            record: tempObj,
            defaultState: JSON.parse((0, _stringify2.default)(tempObj))
          });
          ToastHelper.success(status);
        } else {
          ToastHelper.error(status);
        }
      }
    };

    _this.onChange = function (field, id, e) {
      var value = e.target.value;
      var tempObj = _this.state.record;
      tempObj[id][field] = value;
      _this.setState({
        record: tempObj
      });
    };

    _this.isDuplicatePaymentTerm = function (id) {
      var defaultTempObj = _this.state.defaultState;
      var tempObj = _this.state.record;
      var days = tempObj[id].days;
      var term = tempObj[id].term;
      var isDuplicate = false;
      (0, _keys2.default)(defaultTempObj).map(function (key) {
        var rowObj = tempObj[key];
        if (key != id && (rowObj.term == term.trim() || rowObj.days == days)) {
          isDuplicate = true;
        }
      });
      return isDuplicate;
    };

    _this.updatePaymentTermObj = function (id) {
      var status = _ErrorCode2.default.ERROR_PAYMENT_TERM_UPDATE_FAILED;
      var tempObj = _this.state.record;
      var term = tempObj[id].term;
      var days = tempObj[id].days;
      if (!term || term.trim() == '') {
        ToastHelper.info(_ErrorCode2.default.ERROR_PAYMENT_TERM_NAME_EMPTY);
        return;
      }
      if (!days) {
        ToastHelper.info(_ErrorCode2.default.ERROR_PAYMENT_TERM_DAYS_EMPTY);
        return;
      }

      if (_this.isDuplicatePaymentTerm(id)) {
        ToastHelper.info(_ErrorCode2.default.ERROR_PAYMENT_TERM_DUPLICATE);
        return;
      }

      tempObj[id].show_edit = true;
      tempObj[id].show_update = false;
      tempObj[id].show_revert = false;
      tempObj[id].show_delete = !(id == _this.DEFAULT_PAYMENT_TERM_ID || !tempObj[id].is_editable);
      tempObj[id].disabled = true;
      var paymentTermModel = new _PaymentTermModel2.default();
      paymentTermModel.setPaymentTermId(id);
      paymentTermModel.setPaymentTermName(tempObj[id].term);
      paymentTermModel.setPaymentTermDays(tempObj[id].days);
      paymentTermModel.setPaymentTermIsDefault(tempObj[id].is_default ? 1 : 0);

      if (tempObj[id].is_new_payment_term) {
        var termId = paymentTermModel.createPaymentTerm();
        if (termId) {
          status = _ErrorCode2.default.ERROR_PAYMENT_TERM_UPDATE_SUCCESS;
          tempObj[id].is_new_payment_term = false;
          tempObj[termId] = tempObj[id];
          tempObj[termId].id = termId;
          delete tempObj[id];
        }
      } else {
        status = paymentTermModel.updatePaymentTerm();
      }

      if (status == _ErrorCode2.default.ERROR_PAYMENT_TERM_UPDATE_SUCCESS) {
        _this.setState({
          defaultState: JSON.parse((0, _stringify2.default)(tempObj)),
          record: tempObj,
          disable_edit: false
        });
        ToastHelper.success(status);
      } else {
        _this.setState({
          disable_edit: false
        });
        ToastHelper.error(status);
      }
    };

    _this.revertPaymentTermObj = function (id) {
      var tempObj = _this.state.defaultState;
      if (!tempObj[id]) {
        tempObj = _this.state.record;
        delete tempObj[id];
      } else {
        tempObj[id].show_edit = true;
        tempObj[id].show_update = false;
        tempObj[id].show_revert = false;
        tempObj[id].show_delete = !(id == _this.DEFAULT_PAYMENT_TERM_ID || !tempObj[id].is_editable);
        tempObj[id].disabled = true;
      }
      _this.setState({
        record: JSON.parse((0, _stringify2.default)(tempObj)),
        disable_edit: false
      });
    };

    _this.editPaymentTermObj = function (id) {
      var tempObj = _this.state.record;
      tempObj[id].show_edit = false;
      tempObj[id].show_update = true;
      tempObj[id].show_revert = true;
      tempObj[id].show_delete = false;
      tempObj[id].disabled = false;
      _this.setState({
        record: tempObj,
        disable_edit: true
      });
    };

    _this.deletePaymentTermObj = function (id) {
      var tempObj = _this.state.record;
      if (Number(id) > _this.TEMP_ID) {
        // for new row in UI having temporary id
        delete tempObj[id];
        _this.setState({
          record: tempObj,
          defaultState: JSON.parse((0, _stringify2.default)(tempObj))
        });
      } else {
        // rows from db
        var paymentTermModel = new _PaymentTermModel2.default();
        paymentTermModel.setPaymentTermId(id);
        var status = paymentTermModel.deletePaymentTerm();
        if (status == _ErrorCode2.default.ERROR_PAYMENT_TERM_UPDATE_SUCCESS) {
          if (tempObj[id].is_default) {
            var defaultId = 1;
            tempObj[defaultId].is_default = true;
            var _paymentTermModel = new _PaymentTermModel2.default();
            _paymentTermModel.setPaymentTermId(defaultId);
            _paymentTermModel.setPaymentTermIsDefault(tempObj[defaultId].is_default ? 1 : 0);
            status = _paymentTermModel.changeDefaultPaymentTerm();
          }
          if (status == _ErrorCode2.default.ERROR_PAYMENT_TERM_UPDATE_SUCCESS) {
            delete tempObj[id]; // delete obj from state
          }
          _this.setState({
            record: tempObj,
            defaultState: JSON.parse((0, _stringify2.default)(tempObj))
          });
          ToastHelper.success(status);
        } else {
          ToastHelper.error(status);
        }
      }
    };

    _this.options = {
      onChange: _this.onChange.bind(_this),
      modifyIsDefault: _this.modifyIsDefault.bind(_this),
      updatePaymentTermObj: _this.updatePaymentTermObj.bind(_this),
      revertPaymentTermObj: _this.revertPaymentTermObj.bind(_this),
      editPaymentTermObj: _this.editPaymentTermObj.bind(_this),
      deletePaymentTermObj: _this.deletePaymentTermObj.bind(_this),
      showEditToast: _this.showEditToast.bind(_this)
    };
    _this.tableHead = [{ 'headerName': 'Term', 'width': 40 }, { 'headerName': 'Days', 'width': 40 }, { 'headerName': 'Default', 'width': 15 }, { 'headerName': '', 'width': 15 }];
    _this.DEFAULT_PAYMENT_TERM_ID = 1;
    _this.TEMP_ID = 9999;

    _this.state = {
      disable_edit: false,
      record: {}
    };
    _this.onClose = _this.onClose.bind(_this);
    _this.onChange = _this.onChange.bind(_this);
    _this.showEditToast = _this.showEditToast.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(SetPaymentTermModal, [{
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps() {
      this.paymentTermCache = new _PaymentTermCache2.default();
      this.paymentTermCache.refreshPaymentTermCache();
      this.setPaymentTermsRecord();
    }
  }, {
    key: 'showEditToast',
    value: function showEditToast() {
      ToastHelper.info('You are already editing another payment term!');
    }
  }, {
    key: 'onClose',
    value: function onClose() {
      this.props.onClose && this.props.onClose();
      this.paymentTermCache.refreshPaymentTermCache();
    }
  }, {
    key: 'addNewRow',
    value: function addNewRow() {
      var tempRow = {
        id: 0,
        term: '',
        days: '',
        is_default: false,
        is_editable: true,
        show_edit: false,
        show_delete: false,
        show_revert: true,
        show_update: true,
        disabled: false,
        is_new_payment_term: true
      };
      var tempObj = this.state.record;
      var totalRows = (0, _keys2.default)(tempObj).length;
      tempObj[totalRows + this.TEMP_ID] = tempRow; // temporary id for new added row
      this.setState({
        record: tempObj,
        defaultState: JSON.parse((0, _stringify2.default)(tempObj))
      });
      setTimeout(function () {
        var objDiv = document.getElementById('paymentTermTableDiv');
        objDiv.scrollTop = objDiv.scrollHeight;
      }, 1);
    }
  }, {
    key: 'setPaymentTermsRecord',
    value: function setPaymentTermsRecord() {
      var paymentTermDefaultData = this.paymentTermCache.getAllPayamentTerms();
      this.dataLoader = new _DataLoader2.default();
      this.paymentIdList = this.dataLoader.getUneditablePaymentTermIds();
      var data = {};
      for (var j in paymentTermDefaultData) {
        var row = paymentTermDefaultData[j];
        var tempRow = {};
        tempRow.id = row.getPaymentTermId();
        tempRow.term = row.getPaymentTermName();
        tempRow.days = row.getPaymentTermDays();
        tempRow.is_default = !!Number(row.getPaymentTermIsDefault());
        tempRow.is_editable = !this.paymentIdList.includes(row.getPaymentTermId());
        tempRow.show_edit = true;
        tempRow.show_delete = !this.paymentIdList.includes(row.getPaymentTermId());
        tempRow.show_revert = false;
        tempRow.show_update = false;
        tempRow.disabled = true;
        tempRow.is_new_payment_term = false;
        data[row.getPaymentTermId()] = tempRow;
      }
      this.setState({
        record: data,
        defaultState: JSON.parse((0, _stringify2.default)(data))
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        _Modal2.default,
        { onClose: this.onClose, isOpen: this.props.isOpen, title: 'Set Payment Terms' },
        _react2.default.createElement(
          'div',
          { className: 'modalBody', style: { height: '400px', width: '500px' } },
          _react2.default.createElement(
            'div',
            { className: 'd-flex flex-column' },
            _react2.default.createElement(
              'div',
              { onClick: function onClick() {
                  return _this2.addNewRow();
                }, className: 'button pointer addRowButtonPaymentTerm' },
              _react2.default.createElement('img', { src: './../inlineSVG/Add_20px.svg' }),
              _react2.default.createElement(
                'span',
                { className: 'addPaymentTermText' },
                'Add'
              )
            ),
            _react2.default.createElement(_PaymentTermList2.default, { key: 1, data: this.state.record, options: this.options, columns: this.tableHead, disableEdit: this.state.disable_edit })
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'modalFooter align-items-center justify-content-end d-flex' },
          _react2.default.createElement(
            'button',
            { onClick: this.onClose, value: '', className: 'modal-footer-button terminalButton' },
            'Done'
          )
        )
      );
    }
  }]);
  return SetPaymentTermModal;
}(_react2.default.Component);

exports.default = SetPaymentTermModal;