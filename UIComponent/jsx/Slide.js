Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _assign = require('babel-runtime/core-js/object/assign');

var _assign2 = _interopRequireDefault(_assign);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactSwipeableViews = require('react-swipeable-views');

var _reactSwipeableViews2 = _interopRequireDefault(_reactSwipeableViews);

var _reactSwipeableViewsUtils = require('react-swipeable-views-utils');

var _reactSwipeableViewsCore = require('react-swipeable-views-core');

var _Pagination = require('./Pagination');

var _Pagination2 = _interopRequireDefault(_Pagination);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var VirtualizeSwipeableViews = (0, _reactSwipeableViewsUtils.virtualize)(_reactSwipeableViews2.default);
var AutoPlaySwipeableViews = (0, _reactSwipeableViewsUtils.autoPlay)(VirtualizeSwipeableViews);
function slideRenderer(params) {
  var index = params.index,
      key = params.key;


  switch ((0, _reactSwipeableViewsCore.mod)(index, 3)) {
    case 0:
      return _react2.default.createElement('div', { key: key, style: (0, _assign2.default)({}, styles.slide, styles.slide1) });

    case 1:
      return _react2.default.createElement('div', { key: key, style: (0, _assign2.default)({}, styles.slide, styles.slide2) });

    case 2:
      return _react2.default.createElement('div', { key: key, style: (0, _assign2.default)({}, styles.slide, styles.slide3) });

    default:
      return null;
  }
}
var styles = {
  root: {
    position: 'relative'
  },
  slide: {
    color: '#fff',
    height: '75vh',
    backgroundColor: '#EAF3FD',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat'
  },
  slide1: {
    backgroundImage: 'url(\'' + '../Images/gst_ready_invoices.svg' + '\')'
  },
  slide2: {
    backgroundImage: 'url(\'' + '../Images/payment_reminders.png' + '\')'
  },
  slide3: {
    backgroundImage: 'url(\'' + '../Images/manage_stocks.png' + '\')'
  }
};

var DemoAutoPlay = function (_React$Component) {
  (0, _inherits3.default)(DemoAutoPlay, _React$Component);

  function DemoAutoPlay() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, DemoAutoPlay);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = DemoAutoPlay.__proto__ || (0, _getPrototypeOf2.default)(DemoAutoPlay)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      index: 0
    }, _this.handleChangeIndex = function (index) {
      _this.setState({
        index: (0, _reactSwipeableViewsCore.mod)(index, 3)
      });
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(DemoAutoPlay, [{
    key: 'render',
    value: function render() {
      var index = this.state.index;


      return _react2.default.createElement(
        'div',
        { className: 'slideDiv', style: styles.root },
        _react2.default.createElement(AutoPlaySwipeableViews, { interval: 5000, index: index, onChangeIndex: this.handleChangeIndex, slideRenderer: slideRenderer }),
        _react2.default.createElement(_Pagination2.default, { dots: 3, index: index, onChangeIndex: this.handleChangeIndex })
      );
    }
  }]);
  return DemoAutoPlay;
}(_react2.default.Component);

exports.default = DemoAutoPlay;