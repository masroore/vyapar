Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _MyDouble = require('../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _ItemSharingOptions = require('./ItemSharingOptions');

var _ItemSharingOptions2 = _interopRequireDefault(_ItemSharingOptions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ServiceDetail = function (_React$Component) {
	(0, _inherits3.default)(ServiceDetail, _React$Component);

	function ServiceDetail() {
		var _ref;

		var _temp, _this, _ret;

		(0, _classCallCheck3.default)(this, ServiceDetail);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = ServiceDetail.__proto__ || (0, _getPrototypeOf2.default)(ServiceDetail)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
			showSharingOptions: false
		}, _this.shareItem = function () {
			_this.setState({
				showSharingOptions: !_this.state.showSharingOptions
			});
		}, _this.handleClickOutside = function (event) {
			if (_this.node && !_this.node.contains(event.target)) {
				_this.setState({
					showSharingOptions: false
				});
			}
		}, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
	}

	(0, _createClass3.default)(ServiceDetail, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			document.addEventListener('mousedown', this.handleClickOutside);
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			document.removeEventListener('mousedown', this.handleClickOutside);
		}
	}, {
		key: 'render',
		value: function render() {
			var _this2 = this;

			var item = this.props.currentItem;
			if (!item) {
				return false;
			}

			return _react2.default.createElement(
				'div',
				{ className: 'containerForServiceList' },
				_react2.default.createElement(
					'div',
					{ className: 'clearfix' },
					_react2.default.createElement(
						'div',
						{ id: 'currentItemName', className: 'titleColor halfdiv floatLeft d-flex align-items-center width50' },
						_react2.default.createElement(
							'span',
							{ className: 'ellipseText' },
							item.getItemName()
						),
						_react2.default.createElement(
							'div',
							{ className: 'position-relative item-sharing-container' },
							_react2.default.createElement(
								'div',
								{ ref: function ref(node) {
										_this2.node = node;
									} },
								_react2.default.createElement('img', { onClick: this.shareItem, className: 'ml-10', src: '../inlineSVG/share_icon.svg' }),
								_react2.default.createElement(_ItemSharingOptions2.default, {
									itemId: item.getItemId(),
									isVisible: this.state.showSharingOptions,
									customClasses: 'itemMenuSharing'
								})
							)
						)
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'row' },
					_react2.default.createElement(
						'div',
						{ className: 'col' },
						_react2.default.createElement(
							'span',
							null,
							'SALE PRICE: ',
							_react2.default.createElement('span', { dangerouslySetInnerHTML: _MyDouble2.default.getBalanceAmountWithDecimalAndCurrency(item['itemSaleUnitPrice'], undefined, true) })
						)
					)
				)
			);
		}
	}]);
	return ServiceDetail;
}(_react2.default.Component);

exports.default = ServiceDetail;