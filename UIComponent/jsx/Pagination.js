Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _PaginationDot = require('./PaginationDot');

var _PaginationDot2 = _interopRequireDefault(_PaginationDot);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {
    position: 'absolute',
    bottom: '5%',
    right: '50%',
    display: 'flex',
    flexDirection: 'row'
  }
};

var Pagination = function (_React$Component) {
  (0, _inherits3.default)(Pagination, _React$Component);

  function Pagination() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Pagination);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Pagination.__proto__ || (0, _getPrototypeOf2.default)(Pagination)).call.apply(_ref, [this].concat(args))), _this), _this.handleClick = function (event, index) {
      _this.props.onChangeIndex(index);
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Pagination, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          index = _props.index,
          dots = _props.dots;


      var children = [];

      for (var i = 0; i < dots; i += 1) {
        children.push(_react2.default.createElement(_PaginationDot2.default, { key: i, index: i, active: i === index, onClick: this.handleClick }));
      }

      return _react2.default.createElement(
        'div',
        { style: styles.root },
        children
      );
    }
  }]);
  return Pagination;
}(_react2.default.Component);

exports.default = Pagination;