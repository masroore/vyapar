Object.defineProperty(exports, "__esModule", {
  value: true
});

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = require('babel-runtime/helpers/objectWithoutProperties');

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _deburr = require('lodash/deburr');

var _deburr2 = _interopRequireDefault(_deburr);

var _reactAutosuggest = require('react-autosuggest');

var _reactAutosuggest2 = _interopRequireDefault(_reactAutosuggest);

var _match = require('autosuggest-highlight/match');

var _match2 = _interopRequireDefault(_match);

var _parse = require('autosuggest-highlight/parse');

var _parse2 = _interopRequireDefault(_parse);

var _TextField = require('@material-ui/core/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _Paper = require('@material-ui/core/Paper');

var _Paper2 = _interopRequireDefault(_Paper);

var _MenuItem = require('@material-ui/core/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _styles = require('@material-ui/core/styles');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function renderInputComponent(inputProps) {
  var classes = inputProps.classes,
      _inputProps$inputRef = inputProps.inputRef,
      _inputRef = _inputProps$inputRef === undefined ? function () {} : _inputProps$inputRef,
      ref = inputProps.ref,
      other = (0, _objectWithoutProperties3.default)(inputProps, ['classes', 'inputRef', 'ref']);

  return _react2.default.createElement(_TextField2.default, (0, _extends3.default)({
    fullWidth: true,
    InputProps: {
      inputRef: function inputRef(node) {
        ref(node);
        _inputRef(node);
      },
      classes: {
        input: classes.input
      }
    }
  }, other));
}

function renderSuggestion(suggestion, _ref) {
  var query = _ref.query,
      isHighlighted = _ref.isHighlighted;

  var matches = (0, _match2.default)(suggestion, query);
  var parts = (0, _parse2.default)(suggestion, matches);

  return _react2.default.createElement(
    _MenuItem2.default,
    { selected: isHighlighted, component: 'div' },
    _react2.default.createElement(
      'div',
      null,
      parts.map(function (part, index) {
        return part.highlight ? _react2.default.createElement(
          'span',
          { key: String(index), style: { fontWeight: 500 } },
          part.text
        ) : _react2.default.createElement(
          'strong',
          { key: String(index), style: { fontWeight: 300 } },
          part.text
        );
      })
    )
  );
}

function getSuggestions(value, suggestions) {
  var inputValue = (0, _deburr2.default)(value.trim()).toLowerCase();
  var inputLength = inputValue.length;
  var count = 0;

  return inputLength === 0 ? [] : suggestions.filter(function (suggestion) {
    var keep = count < 5 && suggestion.toLowerCase().indexOf(inputValue.toLowerCase()) >= 0;

    if (keep) {
      count += 1;
    }

    return keep;
  });
}

function getSuggestionValue(suggestion) {
  return suggestion;
}

var styles = function styles(theme) {
  return {
    root: {
      flexGrow: 1
    },
    container: {
      position: 'relative'
    },
    suggestionsContainerOpen: {
      position: 'absolute',
      zIndex: 1,
      marginTop: theme.spacing(1),
      left: 0,
      right: 0
    },
    suggestion: {
      display: 'block'
    },
    suggestionsList: {
      margin: 0,
      padding: 0,
      listStyleType: 'none'
    },
    divider: {
      height: theme.spacing(2)
    }
  };
};

var IntegrationAutosuggest = function (_React$Component) {
  (0, _inherits3.default)(IntegrationAutosuggest, _React$Component);

  function IntegrationAutosuggest(props) {
    (0, _classCallCheck3.default)(this, IntegrationAutosuggest);

    var _this = (0, _possibleConstructorReturn3.default)(this, (IntegrationAutosuggest.__proto__ || (0, _getPrototypeOf2.default)(IntegrationAutosuggest)).call(this, props));

    _this.handleSuggestionsFetchRequested = function (_ref2) {
      var value = _ref2.value;

      if (_this.isInvalidInput(value)) {
        _this.props.onValueChange();
        return;
      }
      _this.props.onValueChange(_this.props.name, value);
      _this.setState({
        suggestions: getSuggestions(value, _this.state.list)
      });
    };

    _this.handleSuggestionsClearRequested = function () {
      _this.setState({
        suggestions: []
      });
    };

    _this.handleChange = function (name) {
      return function (event, _ref3) {
        var newValue = _ref3.newValue;

        if (_this.isInvalidInput(newValue)) {
          event.preventDefault();
          return;
        }
        _this.props.onValueChange(_this.props.name, newValue);
        _this.setState((0, _defineProperty3.default)({}, name, newValue));
      };
    };

    _this.state = {
      single: _this.props.value,
      list: _this.props.list,
      suggestions: []
    };
    return _this;
  }

  (0, _createClass3.default)(IntegrationAutosuggest, [{
    key: 'isInvalidInput',
    value: function isInvalidInput(value) {
      if (value && (value.length > parseInt(this.props.maxLength) || this.props.pattern && !this.props.pattern.test(value))) {
        return true;
      }
      return false;
    }
  }, {
    key: 'render',
    value: function render() {
      var classes = this.props.classes;
      var suggestions = this.state.suggestions;


      var autosuggestProps = {
        renderInputComponent: renderInputComponent,
        suggestions: suggestions,
        onSuggestionsFetchRequested: this.handleSuggestionsFetchRequested,
        onSuggestionsClearRequested: this.handleSuggestionsClearRequested,
        getSuggestionValue: getSuggestionValue,
        renderSuggestion: renderSuggestion
      };
      var props = {
        classes: classes,
        placeholder: this.props.label,
        value: this.state.single,
        onChange: this.handleChange('single'),
        label: this.props.label,
        name: this.props.name,
        type: this.props.type ? this.props.type : 'text'
      };
      if (this.props.minLength) {
        props.minLength = this.props.minLength;
      }
      if (this.props.maxLength) {
        props.maxLength = this.props.maxLength;
      }
      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(_reactAutosuggest2.default, (0, _extends3.default)({}, autosuggestProps, {
          inputProps: props,
          theme: {
            container: classes.container,
            suggestionsContainerOpen: classes.suggestionsContainerOpen,
            suggestionsList: classes.suggestionsList,
            suggestion: classes.suggestion
          },
          renderSuggestionsContainer: function renderSuggestionsContainer(options) {
            return _react2.default.createElement(
              _Paper2.default,
              (0, _extends3.default)({}, options.containerProps, { square: true }),
              options.children
            );
          }
        })),
        _react2.default.createElement('div', { className: classes.divider })
      );
    }
  }]);
  return IntegrationAutosuggest;
}(_react2.default.Component);

IntegrationAutosuggest.propTypes = {
  classes: _propTypes2.default.object.isRequired
};

exports.default = (0, _styles.withStyles)(styles)(IntegrationAutosuggest);