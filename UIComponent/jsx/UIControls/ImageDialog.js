Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _Dialog = require('@material-ui/core/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _AppBar = require('@material-ui/core/AppBar');

var _AppBar2 = _interopRequireDefault(_AppBar);

var _Toolbar = require('@material-ui/core/Toolbar');

var _Toolbar2 = _interopRequireDefault(_Toolbar);

var _IconButton = require('@material-ui/core/IconButton');

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Typography = require('@material-ui/core/Typography');

var _Typography2 = _interopRequireDefault(_Typography);

var _Close = require('@material-ui/icons/Close');

var _Close2 = _interopRequireDefault(_Close);

var _Slide = require('@material-ui/core/Slide');

var _Slide2 = _interopRequireDefault(_Slide);

var _reactEasyCrop = require('react-easy-crop');

var _reactEasyCrop2 = _interopRequireDefault(_reactEasyCrop);

var _Slider = require('@material-ui/core/Slider');

var _Slider2 = _interopRequireDefault(_Slider);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _ItemImagesUtility = require('../../../Utilities/ItemImagesUtility');

var _ItemImagesUtility2 = _interopRequireDefault(_ItemImagesUtility);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
    return {
        appBar: {
            position: 'relative'
        },
        flex: {
            flex: 1
        },
        imgContainer: {
            position: 'relative',
            flex: 1
        },
        toolbar: {
            minHeight: 'initial'
        },
        img: {
            position: 'absolute',
            top: 20,
            bottom: 20,
            left: 20,
            right: 20,
            margin: 'auto',
            maxWidth: '100%',
            maxHeight: '100%'
        },
        cropContainer: {
            position: 'relative',
            width: '100%',
            background: '#FFFFFF',
            height: '60%'
        },
        imageControlBtns: {
            flexShrink: 0,
            marginLeft: 8,
            marginRight: 8
        },
        controls: {
            padding: 8,
            display: 'flex',
            flexDirection: 'column'
        },
        sliderContainer: {
            display: 'flex',
            flex: '1',
            alignItems: 'center'
        },
        sliderLabel: {
            minWidth: 65,
            marginRight: 8
        },
        slider: {
            padding: '22px 0px',
            marginLeft: 16,
            flexDirection: 'row',
            alignItems: 'center',
            margin: '0 16px'
        },
        imageDialog: {
            zIndex: '1302 !important',
            '& .MuiDialog-container > .MuiPaper-root': {
                width: '80%',
                height: '80%'
            }
        }
    };
};

function Transition(props) {
    return _react2.default.createElement(_Slide2.default, (0, _extends3.default)({ direction: 'up' }, props));
}

var ImgDialog = function (_React$Component) {
    (0, _inherits3.default)(ImgDialog, _React$Component);

    function ImgDialog() {
        var _this2 = this;

        (0, _classCallCheck3.default)(this, ImgDialog);

        var _this = (0, _possibleConstructorReturn3.default)(this, (ImgDialog.__proto__ || (0, _getPrototypeOf2.default)(ImgDialog)).call(this));

        _this.onCropComplete = function (croppedArea, croppedAreaPixels) {
            _this.setState({ croppedAreaPixels: croppedAreaPixels });
        };

        _this.openFileExplorer = function () {
            var callback = function callback(result) {
                _this.setState({ imgSrc: 'data:image/jpeg;base64,' + result, crop: { x: 0, y: 0 },
                    rotation: 0,
                    zoom: 1 });
            };
            _ItemImagesUtility2.default.selectImage(callback);
        };

        _this.showCroppedImage = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
            var croppedImage;
            return _regenerator2.default.wrap(function _callee$(_context) {
                while (1) {
                    switch (_context.prev = _context.next) {
                        case 0:
                            _context.prev = 0;
                            _context.next = 3;
                            return _ItemImagesUtility2.default.getCroppedImg(_this.state.imgSrc || _this.props.img, _this.state.croppedAreaPixels, _this.state.rotation);

                        case 3:
                            croppedImage = _context.sent;

                            _this.props.callback && _this.props.callback(croppedImage);
                            _this.handleClose();
                            _context.next = 11;
                            break;

                        case 8:
                            _context.prev = 8;
                            _context.t0 = _context['catch'](0);

                            console.error(_context.t0);

                        case 11:
                        case 'end':
                            return _context.stop();
                    }
                }
            }, _callee, _this2, [[0, 8]]);
        }));

        _this.handleClose = function () {
            _this.setState({ imgSrc: null, zoom: 1 });
            _this.props.close && _this.props.close();
        };

        _this.handleEscape = function (e) {
            e.preventDefault();
            e.nativeEvent.stopImmediatePropagation();
            _this.handleClose();
        };

        _this.state = {
            open: false,
            imgSrc: null,
            crop: { x: 0, y: 0 },
            rotation: 0,
            zoom: 1,
            croppedAreaPixels: null,
            croppedImage: null
        };
        return _this;
    }

    (0, _createClass3.default)(ImgDialog, [{
        key: 'render',
        value: function render() {
            var _this3 = this;

            var _state = this.state,
                crop = _state.crop,
                rotation = _state.rotation,
                zoom = _state.zoom;
            var classes = this.props.classes;

            var imageToShow = this.state.imgSrc || this.props.img;
            return _react2.default.createElement(
                _Dialog2.default,
                {
                    onEscapeKeyDown: this.handleEscape,
                    className: classes.imageDialog,
                    open: this.props.open,
                    onClose: this.handleClose,
                    TransitionComponent: Transition
                },
                _react2.default.createElement(
                    _AppBar2.default,
                    { className: classes.appBar },
                    _react2.default.createElement(
                        _Toolbar2.default,
                        { classes: { root: classes.toolbar } },
                        _react2.default.createElement(
                            _Typography2.default,
                            {
                                variant: 'title',
                                color: 'inherit',
                                className: classes.flex
                            },
                            'Crop Image'
                        ),
                        _react2.default.createElement(
                            _IconButton2.default,
                            {
                                color: 'inherit',
                                onClick: this.handleClose,
                                'aria-label': 'Close'
                            },
                            _react2.default.createElement(_Close2.default, null)
                        )
                    )
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'd-flex flex-column h-100-per' },
                    _react2.default.createElement(
                        'div',
                        { className: classes.cropContainer + ' form-wrapper' },
                        _react2.default.createElement(_reactEasyCrop2.default, {
                            minZoom: 0.6,
                            restrictPosition: false,
                            image: imageToShow,
                            crop: crop,
                            rotation: rotation,
                            zoom: zoom,
                            aspect: 1 / 1,
                            onCropChange: function onCropChange(crop) {
                                _this3.setState({ crop: crop });
                            },
                            onRotationChange: function onRotationChange(rotation) {
                                _this3.setState({ rotation: rotation });
                            },
                            onCropComplete: this.onCropComplete,
                            onZoomChange: function onZoomChange(zoom) {
                                _this3.setState({ zoom: zoom });
                            }
                        })
                    ),
                    _react2.default.createElement(
                        'div',
                        { className: classes.controls },
                        _react2.default.createElement(
                            'div',
                            { className: classes.sliderContainer },
                            _react2.default.createElement(
                                _Typography2.default,
                                {
                                    variant: 'overline',
                                    classes: { root: classes.sliderLabel }
                                },
                                'Zoom'
                            ),
                            _react2.default.createElement(_Slider2.default, {
                                value: zoom,
                                step: 0.1,
                                max: 3,
                                min: 0.6,
                                'aria-labelledby': 'Zoom',
                                onChange: function onChange(e, zoom) {
                                    return _this3.setState({ zoom: zoom });
                                }
                            })
                        ),
                        _react2.default.createElement(
                            'div',
                            { className: classes.sliderContainer },
                            _react2.default.createElement(
                                _Typography2.default,
                                {
                                    variant: 'overline',
                                    classes: { root: classes.sliderLabel }
                                },
                                'Rotation'
                            ),
                            _react2.default.createElement(_Slider2.default, {
                                value: rotation,
                                min: 0,
                                max: 360,
                                step: 1,
                                'aria-labelledby': 'rotation',
                                onChange: function onChange(e, rotation) {
                                    return _this3.setState({ rotation: rotation });
                                }
                            })
                        )
                    ),
                    _react2.default.createElement(
                        'div',
                        { className: 'd-flex justify-content-end', style: { paddingBottom: 16 } },
                        _react2.default.createElement(
                            _Button2.default,
                            {
                                variant: 'contained',
                                classes: { root: classes.imageControlBtns },
                                onClick: this.openFileExplorer
                            },
                            'Change Image'
                        ),
                        _react2.default.createElement(
                            _Button2.default,
                            {
                                autoFocus: true,
                                onClick: this.showCroppedImage,
                                variant: 'contained',
                                color: 'primary',
                                classes: { root: classes.imageControlBtns }
                            },
                            'Crop & Save'
                        )
                    )
                )
            );
        }
    }]);
    return ImgDialog;
}(_react2.default.Component);

exports.default = (0, _styles.withStyles)(styles)(ImgDialog);