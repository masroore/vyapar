Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Paper = require('@material-ui/core/Paper');

var _Paper2 = _interopRequireDefault(_Paper);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/core/styles');

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var useStyles = (0, _styles.makeStyles)({
	text: {
		marginTop: 20,
		marginBottom: 25,
		fontSize: 16,
		color: '#80808B',
		textAlign: 'center'
	},
	buttonRoot: {
		textAlign: 'center',
		backgroundColor: '#F3A33A',
		textTransform: 'capitalize',
		color: 'white',
		'&:hover': {
			backgroundColor: 'rgba(245, 179, 88, 1)'
		}
	}
});

function FirstScreen(_ref) {
	var image = _ref.image,
	    text = _ref.text,
	    text2 = _ref.text2,
	    buttonLabel = _ref.buttonLabel,
	    onClick = _ref.onClick;

	var classes = useStyles();
	return _react2.default.createElement(
		_Paper2.default,
		{
			className: 'd-flex flex-column flex-1 justify-content-center align-items-center'
		},
		_react2.default.createElement(
			'div',
			{ className: classes.image },
			_react2.default.createElement('img', { src: image, alt: text })
		),
		_react2.default.createElement(
			'div',
			{ className: classes.text },
			_react2.default.createElement(
				'div',
				null,
				text
			),
			_react2.default.createElement(
				'div',
				null,
				text2
			)
		),
		buttonLabel && _react2.default.createElement(
			'div',
			{ className: classes.button },
			_react2.default.createElement(
				_Button2.default,
				{
					classes: {
						root: classes.buttonRoot
					},
					color: 'primary',
					variant: 'contained',
					onClick: onClick
				},
				buttonLabel
			)
		)
	);
}

FirstScreen.propTypes = {
	image: _propTypes2.default.string,
	text: _propTypes2.default.string,
	text2: _propTypes2.default.string,
	buttonLabel: _propTypes2.default.string,
	onClick: _propTypes2.default.func
};
exports.default = FirstScreen;