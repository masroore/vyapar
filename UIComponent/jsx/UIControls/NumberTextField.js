Object.defineProperty(exports, "__esModule", {
	value: true
});

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _TextField = require('@material-ui/core/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
	return {
		container: {
			display: 'flex',
			flexWrap: 'wrap'
		},
		numberField: {
			position: 'relative'
		},
		textField: {
			marginLeft: theme.spacing(1),
			marginRight: theme.spacing(1),
			'& input': {
				textAlign: 'center'
			}
		},
		arrowBtn: {
			marginRight: theme.spacing(1),
			'& img': {
				backgroundColor: '#c6dff9',
				width: '16px',
				height: '12px',
				marginBottom: '1px'
			}
		}
	};
};

var NumberTextField = function (_React$Component) {
	(0, _inherits3.default)(NumberTextField, _React$Component);

	function NumberTextField(props) {
		(0, _classCallCheck3.default)(this, NumberTextField);

		var _this = (0, _possibleConstructorReturn3.default)(this, (NumberTextField.__proto__ || (0, _getPrototypeOf2.default)(NumberTextField)).call(this, props));

		_this.handleChange = function (event) {
			var value = event.target.value;
			if (!_this.handleMaxValue(value)) return;
			if (!_this.handleMinValue(value)) return;
			_this.setState({
				value: value
			}, function () {
				if (_this.props.handleChange && typeof _this.props.handleChange === 'function') {
					_this.props.handleChange(value);
				}
			});
		};

		_this.handleBlur = function (event) {
			var value = event.target.value;
			if (!value) return;
			_this.handleBlurEvent(value);
		};

		_this.increment = function () {
			var updatedValue = parseInt(_this.state.value) + 1;
			if (!_this.handleMaxValue(updatedValue)) return;
			_this.setState({
				value: updatedValue
			});
			_this.handleBlurEvent(updatedValue);
		};

		_this.decrement = function () {
			var updatedValue = parseInt(_this.state.value) - 1;
			if (!_this.handleMinValue(updatedValue)) return;
			if (updatedValue >= 0) {
				_this.setState({
					value: updatedValue
				});
				_this.handleBlurEvent(updatedValue);
			}
		};

		_this.state = {
			value: !isNaN(_this.props.value) ? _this.props.value : 0,
			showArrow: _this.props.showArrow !== undefined ? _this.props.showArrow : true,
			margin: _this.props.margin || 'none',
			variant: _this.props.variant || ''
		};
		return _this;
	}

	(0, _createClass3.default)(NumberTextField, [{
		key: 'componentWillUpdate',
		value: function componentWillUpdate(props, state) {
			if (window.isSyncSettingUpdate) {
				this.resetState(props.value);
			}
		}
	}, {
		key: 'resetState',
		value: function resetState(value) {
			this.setState({
				value: value
			});
		}
	}, {
		key: 'handleBlurEvent',
		value: function handleBlurEvent(value) {
			if (this.props.handleBlur && typeof this.props.handleBlur === 'function') {
				this.props.handleBlur(value);
			}
		}
	}, {
		key: 'handleMinValue',
		value: function handleMinValue(value) {
			if (this.props.minValue && parseInt(value) < parseInt(this.props.minValue)) {
				ToastHelper.error('Cannot enter value less than ' + this.props.minValue + '.');
				return false;
			}
			return true;
		}
	}, {
		key: 'handleMaxValue',
		value: function handleMaxValue(value) {
			if (this.props.maxValue && parseInt(value) > parseInt(this.props.maxValue)) {
				ToastHelper.error('Cannot enter value greater than ' + this.props.maxValue + '.');
				return false;
			}
			return true;
		}
	}, {
		key: 'render',
		value: function render() {
			var _classNames,
			    _this2 = this;

			var classes = this.props.classes;

			var rootClasses = (0, _classnames2.default)((_classNames = {}, (0, _defineProperty3.default)(_classNames, classes.container, true), (0, _defineProperty3.default)(_classNames, this.props.className, true), (0, _defineProperty3.default)(_classNames, classes.numberField, true), _classNames));
			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement(_TextField2.default, {
					label: this.props.label ? this.props.label : '',
					disabled: this.props.disabled ? this.props.disabled : false,
					id: this.props.id,
					value: this.state.value,
					onChange: this.handleChange,
					onBlur: this.handleBlur,
					type: 'number',
					className: rootClasses,
					InputLabelProps: {
						shrink: true
					},
					margin: this.state.margin,
					variant: this.props.variant,
					onInput: function onInput(e) {
						e.target.value = Math.max(0, parseInt(e.target.value)).toString().slice(0, _this2.props.maxLength ? _this2.props.maxLength : 1);
					}
				}),
				this.state.showArrow && _react2.default.createElement(
					'div',
					{ className: classes.arrowBtn + ' ml-10 d-inline-flex flex-column' },
					_react2.default.createElement('img', { onClick: this.increment, src: '../inlineSVG/outline-arrow-up.svg' }),
					_react2.default.createElement('img', { onClick: this.decrement, src: '../inlineSVG/outline-arrow-down.svg' })
				)
			);
		}
	}]);
	return NumberTextField;
}(_react2.default.Component);

exports.default = (0, _styles.withStyles)(styles)(NumberTextField);