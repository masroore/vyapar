Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = require('babel-runtime/helpers/objectWithoutProperties');

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _styles = require('@material-ui/core/styles');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles() {
	return {
		textAreaDiv: {
			position: 'relative',
			width: '100%',
			display: 'flex',
			padding: '8px 0'
		},
		textAreaInput: {
			textDecoration: 'none',
			border: '1px rgba(0, 0, 0, 0.23) solid',
			borderRadius: '5px',
			outline: 'none',
			display: 'block',
			padding: '8px',
			width: '100%',
			resize: 'none',
			fontSize: '14px',
			fontFamily: 'roboto',
			'&::placeholder': {
				color: '#A4AFB3'
			},
			'&:hover': {
				borderColor: 'rgba(0, 0, 0, 0.9)'
			},
			'&:focus': {
				border: '2px #1789FC solid',
				padding: '7px'
			},
			'&:focus+label': {
				color: '#1789FC',
				top: '8px',
				transform: 'translate(0, -50%) scale(.75)',
				opacity: 1
			}
		},
		textAreaLabel: {
			position: 'absolute',
			top: '12px',
			left: '4px',
			zIndex: 1,
			opacity: 0,
			transform: 'translate(0, -50%) scale(1)',
			transition: '.2s ease-in-out',
			pointerEvents: 'none',
			background: 'white',
			padding: '0 4px',
			color: '#A4AFB3'
		},
		labelShrink: {
			top: '8px',
			transform: 'translate(0, -50%) scale(.75)',
			opacity: 1
		}
	};
};

var TextInput = function TextInput(_ref) {
	var classes = _ref.classes,
	    label = _ref.label,
	    placeholder = _ref.placeholder,
	    rootClass = _ref.rootClass,
	    labelClass = _ref.labelClass,
	    inputClass = _ref.inputClass,
	    value = _ref.value,
	    onChange = _ref.onChange,
	    inputProps = (0, _objectWithoutProperties3.default)(_ref, ['classes', 'label', 'placeholder', 'rootClass', 'labelClass', 'inputClass', 'value', 'onChange']);

	return _react2.default.createElement(
		'div',
		{ className: (0, _classnames2.default)(classes.textAreaDiv, rootClass || '') },
		_react2.default.createElement('textarea', (0, _extends3.default)({
			type: 'text',
			className: (0, _classnames2.default)(classes.textAreaInput, inputClass || ''),
			value: value,
			onChange: onChange,
			placeholder: placeholder || label || ''
		}, inputProps)),
		!!label && _react2.default.createElement(
			'label',
			{ className: (0, _classnames2.default)(classes.textAreaLabel, value ? classes.labelShrink : '', labelClass || '') },
			label
		)
	);
};

TextInput.propTypes = {
	label: _propTypes2.default.string,
	placeholder: _propTypes2.default.string,
	rootClass: _propTypes2.default.string,
	labelClass: _propTypes2.default.string,
	inputClass: _propTypes2.default.string,
	onChange: _propTypes2.default.func,
	value: _propTypes2.default.string,
	classes: _propTypes2.default.object
};
exports.default = (0, _styles.withStyles)(styles)(TextInput);