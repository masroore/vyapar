Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = require('babel-runtime/helpers/objectWithoutProperties');

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/core/styles');

var _SalePurchaseHelpers = require('../SalePurchaseContainer/SalePurchaseHelpers');

var _MyDate = require('../../../Utilities/MyDate');

var _MyDate2 = _interopRequireDefault(_MyDate);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles() {
	return {
		error: {
			border: '1px solid red !important'
		},
		input: {
			maxWidth: '10vw'
		}
	};
};

var JqueryDatePicker = function (_PureComponent) {
	(0, _inherits3.default)(JqueryDatePicker, _PureComponent);

	function JqueryDatePicker(props) {
		(0, _classCallCheck3.default)(this, JqueryDatePicker);

		var _this = (0, _possibleConstructorReturn3.default)(this, (JqueryDatePicker.__proto__ || (0, _getPrototypeOf2.default)(JqueryDatePicker)).call(this, props));

		_this.onBlur = function (e) {
			var _this$props = _this.props,
			    validate = _this$props.validate,
			    onChange = _this$props.onChange,
			    _this$props$format = _this$props.format,
			    format = _this$props$format === undefined ? 'dd/mm/yy' : _this$props$format;
			var target = e.target;

			var dateString = target.value;
			if (validate && dateString) {
				var date = _MyDate2.default.getDateObj(dateString, format == 'dd/mm/yy' ? 'dd/mm/yyyy' : 'mm/yyyy', '/');
				if (date && date instanceof Date && date != 'Invalid Date') {
					_this.setState({ hasError: false });
					return true;
				} else {
					e.preventDefault();
					_this.setState({ hasError: true });
					onChange && onChange(e);
					ToastHelper.error('Please enter valid date');
				}
			} else {
				_this.setState({ hasError: false });
			}
		};

		_this.inputRef = props.id || 'id-' + (0, _SalePurchaseHelpers.getUniqueKey)();
		_this.state = {
			hasError: false
		};
		return _this;
	}

	(0, _createClass3.default)(JqueryDatePicker, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			var _this2 = this;

			var _props = this.props,
			    onChange = _props.onChange,
			    _props$format = _props.format,
			    format = _props$format === undefined ? 'dd/mm/yy' : _props$format,
			    monthPicker = _props.monthPicker;

			this.timeout = setTimeout(function () {
				if (monthPicker) {
					$('#' + _this2.inputRef).monthpicker({ dateFormat: format, onSelect: onChange });
				} else {
					$('#' + _this2.inputRef).datepicker({ dateFormat: format, onSelect: onChange });
				}
			}, 1000);
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			clearTimeout(this.timeout);
		}
	}, {
		key: 'render',
		value: function render() {
			var hasError = this.state.hasError;
			var _props2 = this.props,
			    classes = _props2.classes,
			    CustomInput = _props2.CustomInput,
			    id = _props2.id,
			    _props2$validate = _props2.validate,
			    validate = _props2$validate === undefined ? false : _props2$validate,
			    rest = (0, _objectWithoutProperties3.default)(_props2, ['classes', 'CustomInput', 'id', 'validate']);
			// const Input = (customInput || (props) => (return (<input type="text" {...props} />));

			var props = (0, _extends3.default)({
				id: this.inputRef,
				onBlur: validate ? this.onBlur : undefined
			}, rest);
			var inputClassName = props.className;
			if (hasError) {
				inputClassName += ' ' + classes.error;
			}
			return _react2.default.createElement(
				'div',
				null,
				CustomInput && _react2.default.createElement(CustomInput, (0, _extends3.default)({ error: hasError, inputProps: { id: id || this.inputRef, onBlur: props.onBlur } }, rest)),
				!CustomInput && _react2.default.createElement('input', (0, _extends3.default)({}, props, { className: (0, _classnames2.default)(classes.input, inputClassName) }))
			);
		}
	}]);
	return JqueryDatePicker;
}(_react.PureComponent);

JqueryDatePicker.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	className: _propTypes2.default.string,
	value: _propTypes2.default.string,
	type: _propTypes2.default.string,
	customClasses: _propTypes2.default.string
};

exports.default = (0, _styles.withStyles)(styles)(JqueryDatePicker);