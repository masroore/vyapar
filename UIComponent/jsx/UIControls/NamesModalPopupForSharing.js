Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _trashableReact = require('trashable-react');

var _trashableReact2 = _interopRequireDefault(_trashableReact);

var _Grid = require('../Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _StringConstants = require('../../../Constants/StringConstants');

var _StringConstants2 = _interopRequireDefault(_StringConstants);

var _ErrorCode = require('../../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

var _Modal = require('../Modal');

var _Modal2 = _interopRequireDefault(_Modal);

var _TextField = require('@material-ui/core/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _PrimaryButton = require('./PrimaryButton');

var _PrimaryButton2 = _interopRequireDefault(_PrimaryButton);

var _MountComponent = require('../MountComponent');

var _MountComponent2 = _interopRequireDefault(_MountComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
	return {
		'@global': {
			'.sharing-options-form-body .ag-root': {
				backgroundColor: '#FFFFFF'
			},
			'.ReactModal__Content .modalContainer': {
				height: '100%'
			},
			'.sharing-options-form-body .ag-header-select-all span, .sharing-options-form-body .ag-selection-checkbox span': {
				height: 16,
				width: 16,
				backgroundSize: 16
			},
			'.sharing-options-form-body .ag-cell-value': {},
			'.sharing-options-form-body .ag-row .ag-cell': {
				display: 'flex',
				alignItems: 'center',
				outline: 'none'
			},
			'.sharing-options-form-body .ag-cell-value span': {
				width: '100%'
			},
			'.sharing-options-form-body .ag-cell-value div': {
				whiteSpace: 'nowrap',
				overflow: 'hidden',
				textOverflow: 'ellipsis'
			},
			'.sharing-options-form-body .ag-cell-value .cell-error': {
				borderBottom: '1px solid red'
			},
			'.sharing-options-form-body .ag-header-cell.alignLeft': {
				display: 'flex',
				alignItems: 'center'
			},
			'.sharing-options-form-body .gridContainer  .quickFilter': {
				justifyContent: 'flex-start !important',
				paddingBottom: 10
			},
			'.sharing-options-form-body .gridContainer .quickFilter .gridTitle': {
				padding: 0
			},
			'.sharing-options-form-body .gridContainer .quickFilter .gridQuickFilter div': {
				marginLeft: 0
			},
			'.sharing-options-form-body  .ag-header-container .ag-header-cell': {
				fontWeight: 600
			},
			'.sharing-options-form-body .ag-icon-checkbox-unchecked:empty': {
				backgroundImage: 'url(../inlineSVG/checkbox_empty.svg) !important;',
				color: '#fff !important'
			},
			'.sharing-options-form-body .ag-icon-checkbox-checked:empty': {
				backgroundImage: 'url(../inlineSVG/checkbox-blue.svg) !important;',
				color: '#097AA8 !important'
			}
		}
	};
};

var InputText = function (_React$Component) {
	(0, _inherits3.default)(InputText, _React$Component);

	function InputText(props) {
		(0, _classCallCheck3.default)(this, InputText);

		var _this = (0, _possibleConstructorReturn3.default)(this, (InputText.__proto__ || (0, _getPrototypeOf2.default)(InputText)).call(this, props));

		_this.handleChange = function (event) {
			var newVal = event.currentTarget.value;
			_this.setState({
				value: newVal
			});
		};

		_this.handleBlur = function () {
			var data = _this.props.data;
			var value = _this.state.value;

			if (value && !_StringConstants2.default.emailRegex.test(value)) {
				_this.setState({
					showError: true
				});
			} else {
				_this.setState({
					showError: false
				}, function () {
					var nameObj = data;
					nameObj.setEmail(value);
					var statusCode = nameObj.updateNameToDBTransaction();
					if (statusCode !== _ErrorCode2.default.ERROR_NAME_SAVE_SUCCESS) {
						ToastHelper.error(_ErrorCode2.default.ERROR_NAME_UPDATE_FAILED);
					}
				});
			}
		};

		_this.state = {
			value: ''
		};
		return _this;
	}

	(0, _createClass3.default)(InputText, [{
		key: 'render',
		value: function render() {
			var propsValue = this.props.value;
			var _state = this.state,
			    value = _state.value,
			    showError = _state.showError;

			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				propsValue && _react2.default.createElement(
					'span',
					null,
					propsValue
				),
				!propsValue && _react2.default.createElement(
					_react2.default.Fragment,
					null,
					_react2.default.createElement(_TextField2.default, {
						className: 'itemGridCell ' + (showError ? 'cell-error' : ''),
						type: 'text',
						style: { width: '100%' },
						value: value || propsValue,
						onChange: this.handleChange,
						onBlur: this.handleBlur
					})
				)
			);
		}
	}]);
	return InputText;
}(_react2.default.Component);

;

var PhoneNumberInputText = function (_React$Component2) {
	(0, _inherits3.default)(PhoneNumberInputText, _React$Component2);

	function PhoneNumberInputText(props) {
		(0, _classCallCheck3.default)(this, PhoneNumberInputText);

		var _this2 = (0, _possibleConstructorReturn3.default)(this, (PhoneNumberInputText.__proto__ || (0, _getPrototypeOf2.default)(PhoneNumberInputText)).call(this, props));

		_this2.handleChange = function (event) {
			var newVal = event.currentTarget.value;
			_this2.setState({
				value: newVal
			});
		};

		_this2.handleBlur = function () {
			var data = _this2.props.data;
			var value = _this2.state.value;

			if (value && value.length < 10) {
				_this2.setState({
					showError: true
				});
			} else {
				_this2.setState({
					showError: false
				}, function () {
					var nameObj = data;
					nameObj.setPhoneNumber(value);
					var statusCode = nameObj.updateNameToDBTransaction();
					if (statusCode !== _ErrorCode2.default.ERROR_NAME_SAVE_SUCCESS) {
						ToastHelper.error(_ErrorCode2.default.ERROR_NAME_UPDATE_FAILED);
					}
				});
			}
		};

		_this2.state = {
			value: ''
		};
		return _this2;
	}

	(0, _createClass3.default)(PhoneNumberInputText, [{
		key: 'render',
		value: function render() {
			var propsValue = this.props.value;
			var _state2 = this.state,
			    value = _state2.value,
			    showError = _state2.showError;

			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				propsValue && _react2.default.createElement(
					'span',
					null,
					propsValue
				),
				!propsValue && _react2.default.createElement(
					_react2.default.Fragment,
					null,
					_react2.default.createElement(_TextField2.default, {
						className: 'itemGridCell ' + (showError ? 'cell-error' : ''),
						type: 'text',
						style: { width: '100%' },
						value: value || propsValue,
						onChange: this.handleChange,
						onBlur: this.handleBlur,
						onInput: function onInput(e) {
							return DecimalInputFilter.inputTextFilterForNumber(e.target, 10);
						}
					})
				)
			);
		}
	}]);
	return PhoneNumberInputText;
}(_react2.default.Component);

;

function TextBoxCellRenderer() {}

TextBoxCellRenderer.prototype.getGui = function () {
	return this.eGui;
};

var NamesModalPopupForSharing = function (_React$Component3) {
	(0, _inherits3.default)(NamesModalPopupForSharing, _React$Component3);

	function NamesModalPopupForSharing(props) {
		(0, _classCallCheck3.default)(this, NamesModalPopupForSharing);

		var _this3 = (0, _possibleConstructorReturn3.default)(this, (NamesModalPopupForSharing.__proto__ || (0, _getPrototypeOf2.default)(NamesModalPopupForSharing)).call(this, props));

		_this3.state = {
			allNames: []
		};

		_this3.getColumnDefs = function () {
			var columnsVisibility = _this3.props.columnsVisibility;

			var columnDefs = [{
				headerName: '',
				field: '',
				headerCheckboxSelection: true,
				headerCheckboxSelectionFilteredOnly: false,
				checkboxSelection: true,
				width: 40
			}, {
				field: 'fullName',
				headerName: 'PARTY',
				headerTooltip: 'PARTY',
				cellRenderer: function cellRenderer(params) {
					var value = params.value;
					return '<div className="itemGridCell" title="' + value + '">' + value + '</div>';
				}
			}];

			if (columnsVisibility.email) {
				TextBoxCellRenderer.prototype.init = function (params) {
					this.eGui = document.createElement('div');
					(0, _MountComponent2.default)(InputText, this.eGui, params);
				};

				columnDefs.push({
					field: 'email',
					headerName: 'EMAIL',
					headerTooltip: 'EMAIL',
					cellRenderer: TextBoxCellRenderer
				});
			}

			if (columnsVisibility.phone) {
				TextBoxCellRenderer.prototype.init = function (params) {
					this.eGui = document.createElement('div');
					(0, _MountComponent2.default)(PhoneNumberInputText, this.eGui, params);
				};
				columnDefs.push({
					field: 'phoneNumber',
					headerName: 'Phone',
					headerTooltip: 'Phone',
					cellRenderer: TextBoxCellRenderer
				});
			}

			return columnDefs;
		};

		_this3.getApi = function (params) {
			_this3.api = params.api;
			setTimeout(function () {
				_this3.api.setRowData(_this3.state.allNames);
			});
		};

		_this3.renderNameGrid = function () {
			var columnDefs = _this3.getColumnDefs();
			var gridOptions = {
				columnDefs: columnDefs,
				rowClass: '',
				cellClass: '',
				rowHeight: 36,
				headerHeight: 36,
				suppressAutoSize: true,
				suppressMovableColumns: true,
				suppressContextMenu: true,
				getRowNodeId: function getRowNodeId(data) {
					return data.nameId;
				},
				overlayNoRowsTemplate: 'No Parties to show',
				suppressRowClickSelection: true,
				rowSelection: _this3.props.rowSelection || 'multiple'
			};
			return _react2.default.createElement(_Grid2.default, {
				height: '100%',
				width: '100%',
				avoidDefaultTheme: true,
				gridOptions: gridOptions,
				getApi: _this3.getApi,
				quickFilter: true,
				notSelectFirstRow: true
			});
		};

		return _this3;
	}

	(0, _createClass3.default)(NamesModalPopupForSharing, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			var NameCache = require('../../../Cache/NameCache');
			var nameCache = new NameCache();
			this.setState({
				allNames: nameCache.getListOfNamesObject()
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _this4 = this;

			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				_react2.default.createElement(
					_Modal2.default,
					{
						width: '600px',
						height: '450px',
						isOpen: this.props.openNamesModal,
						canHideHeader: true,
						title: this.props.namesModalTitle || 'Share Item Details With Parties',
						onClose: this.props.closeNamesModal
					},
					_react2.default.createElement(
						'div',
						{ className: 'sharing-options-container form-container d-flex flex-column h-100-per' },
						_react2.default.createElement(
							'p',
							{ className: 'mb-10' },
							this.props.namesModalSubTitle || 'Select the parties and share item details with them by Email.'
						),
						_react2.default.createElement(
							'div',
							{ className: 'sharing-options-form-body gridContainer form-wrapper d-flex' },
							this.renderNameGrid()
						),
						_react2.default.createElement(
							'div',
							{ className: 'sharing-options-form-footer' },
							_react2.default.createElement(
								'p',
								{ className: 'mt-20 mb-20' },
								this.props.namesModalNote || 'Note: Item will be shared with only the parties whose email address is present.'
							),
							_react2.default.createElement(
								'div',
								{ className: 'd-flex ' },
								_react2.default.createElement(
									_PrimaryButton2.default,
									{ onClick: this.props.closeNamesModal, style: { backgroundColor: 'white', color: '#1789FC', marginLeft: 'auto' } },
									this.props.cancelBtnName || 'Cancel'
								),
								_react2.default.createElement(
									_PrimaryButton2.default,
									{ style: { color: 'white', backgroundColor: '#1789FC', marginLeft: '1rem' }, onClick: function onClick() {
											return _this4.props.handleShare(_this4.api);
										} },
									this.props.shareBtnName || 'Share Item Details'
								)
							)
						)
					)
				)
			);
		}
	}]);
	return NamesModalPopupForSharing;
}(_react2.default.Component);

exports.default = (0, _styles.withStyles)(styles)((0, _trashableReact2.default)(NamesModalPopupForSharing));