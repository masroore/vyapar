Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _makeStyles = require('@material-ui/core/styles/makeStyles');

var _makeStyles2 = _interopRequireDefault(_makeStyles);

var _Fab = require('@material-ui/core/Fab');

var _Fab2 = _interopRequireDefault(_Fab);

var _CircularProgress = require('@material-ui/core/CircularProgress');

var _CircularProgress2 = _interopRequireDefault(_CircularProgress);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var useStyles = (0, _makeStyles2.default)({
	printButtonContainer: {
		width: '100%',
		textAlign: 'center'
	},
	printButtonRoot: {
		background: 'transparent',
		color: '#284561',
		boxShadow: 'none',
		'&:hover': {
			background: 'transparent',
			color: '#284561',
			boxShadow: 'none'
		}
	},
	label: {
		marginTop: 2,
		fontSize: 10
	},
	spinner: {
		position: 'absolute',
		top: -4,
		left: -4,
		zIndex: 1
	},
	iconWrapper: {
		position: 'relative',
		width: 42,
		margin: '0 auto',
		textAlign: 'center'
	}
});

function LoaderButton(_ref) {
	var onClick = _ref.onClick,
	    Icon = _ref.Icon,
	    label = _ref.label,
	    _ref$loading = _ref.loading,
	    loading = _ref$loading === undefined ? false : _ref$loading;

	var classes = useStyles();
	return _react2.default.createElement(
		'div',
		{
			className: 'button-wrapper floatRight ' + classes.printButtonContainer
		},
		_react2.default.createElement(
			'div',
			{ className: classes.iconWrapper },
			_react2.default.createElement(
				_Fab2.default,
				{
					classes: { root: classes.printButtonRoot },
					color: 'primary',
					size: 'small',
					onClick: onClick
				},
				Icon
			),
			loading && _react2.default.createElement(_CircularProgress2.default, { className: classes.spinner, size: 48 })
		),
		label && _react2.default.createElement(
			'div',
			{ className: classes.label },
			label
		)
	);
}
LoaderButton.propTypes = {
	classes: _propTypes2.default.object,
	onClick: _propTypes2.default.func,
	Icon: _propTypes2.default.obj,
	label: _propTypes2.default.string,
	loading: _propTypes2.default.boolean
};

exports.default = LoaderButton;