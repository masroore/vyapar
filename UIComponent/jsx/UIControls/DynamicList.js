Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _clsx = require('clsx');

var _clsx2 = _interopRequireDefault(_clsx);

var _styles = require('@material-ui/core/styles');

var _TextField = require('@material-ui/core/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _Paper = require('@material-ui/core/Paper');

var _Paper2 = _interopRequireDefault(_Paper);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _ConfirmModal = require('./../ConfirmModal');

var _ConfirmModal2 = _interopRequireDefault(_ConfirmModal);

var _Delete = require('@material-ui/icons/Delete');

var _Delete2 = _interopRequireDefault(_Delete);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
	return {
		container: {
			flexGrow: 1,
			position: 'relative'
		},
		paper: {
			position: 'absolute',
			zIndex: 100,
			marginTop: theme.spacing(1),
			left: 0,
			right: 0
		},
		chip: {
			margin: theme.spacing(1) / 2 + 'px ' + theme.spacing(1) / 4 + 'px'
		},
		inputRoot: {
			flexWrap: 'wrap'
		},
		inputBox: {
			width: '100%',
			flexGrow: 1,
			caretColor: 'transparent'
		},
		divider: {
			height: theme.spacing(2)
		},
		textField: {
			backgroundColor: '#E9ECF0',
			height: '30px',
			width: '60%'
		},
		'@global': {
			'.option-list': {
				padding: '10px',
				minWidth: '200px',
				width: 'min-content',
				'& input': {
					borderRadius: '4px'
				},
				'& ul': {
					maxHeight: '144px',
					overflow: 'scroll'
				},
				'& ul > li': {
					lineHeight: '26px',
					borderBottom: '1px solid #e8e8e8',
					paddingLeft: '10px',
					outline: 'none',
					borderRadius: '4px'
				},
				'& ul > li:hover': {
					backgroundColor: '#E9ECF0'
				},
				'& ul > li:last-child': {
					borderBottom: 'none'
				},
				'& ul > li.selected': {
					backgroundColor: '#E9ECF0'
				},
				'& .add-btn': {
					padding: '2px'
				}
			}
		}

	};
};

var DynamicList = function (_React$Component) {
	(0, _inherits3.default)(DynamicList, _React$Component);

	function DynamicList(props) {
		(0, _classCallCheck3.default)(this, DynamicList);

		var _this = (0, _possibleConstructorReturn3.default)(this, (DynamicList.__proto__ || (0, _getPrototypeOf2.default)(DynamicList)).call(this, props));

		_this.handleKeyDown = function (event) {
			if (event.keyCode === 13) {
				_this.addOption();
			}
		};

		_this.handleAddBtnKeyDown = function (event) {
			if (event.keyCode === 9) {
				_this.setState({
					showOptions: false
				});
			}
		};

		_this.addOption = function (event) {
			var newValue = _this.state.addValue;
			var newOptionList = [];
			if (!newValue) {
				ToastHelper.error('Please enter some value to add.');
				if (_this.addNewValueRef) {
					_this.addNewValueRef.focus();
				}
			} else {
				newOptionList = _this.state.optionList || [];
				var isAlreadyExists = newOptionList.filter(function (option) {
					if (option.value === newValue) return option;
				});

				if (isAlreadyExists.length > 0) {
					ToastHelper.error('Value Already Exists in list. Try with different value again.');
					_this.addNewValueRef.focus();
				} else {
					var newValueKey = newOptionList.length + 1;
					var addNewValueObj = { key: newValueKey, value: newValue };
					newOptionList.push(addNewValueObj);
					_this.setState({
						optionList: newOptionList,
						addValue: '',
						value: newValue,
						selectedOption: newValueKey,
						showOptions: false
					}, function () {
						if (_this.props.addNewValueCallback && typeof _this.props.addNewValueCallback === 'function') {
							_this.props.addNewValueCallback(addNewValueObj);
						}
						if (_this.addNewValueRef) {
							_this.addNewValueRef.focus();
						}
					});
				}
			}
		};

		_this.removeOption = function (status) {
			if (status) {
				var option = _this.state.selectedDeleteOption;
				var updatedOptionList = _this.state.optionList.filter(function (obj) {
					return obj.key !== option.key;
				});
				var wasDefault = _this.state.value === option.value;
				_this.setState({
					optionList: updatedOptionList,
					value: wasDefault ? updatedOptionList ? updatedOptionList[0]['value'] : '' : _this.state.value,
					selectedOption: wasDefault ? updatedOptionList ? updatedOptionList[0]['key'] : '' : _this.state.selectedOption,
					canDelete: !_this.state.canDelete
				}, function () {
					if (_this.props.modalConfirmCallback && typeof _this.props.modalConfirmCallback === 'function') {
						_this.props.modalConfirmCallback(option);
					}
				});
			} else {
				_this.setState({ canDelete: !_this.state.canDelete
				});
			}
		};

		_this.toggleOptions = function (event) {
			event.preventDefault();
			event.stopPropagation();
			!_this.props.disabled && _this.setState({
				showOptions: true
			}, function () {
				if (_this.addNewValueRef) {
					_this.addNewValueRef.focus();
				}
			});
		};

		_this.handlePrefixTxtFocus = function (event) {
			event.preventDefault();
			event.stopPropagation();
		};

		_this.handleClickOutside = function (event) {
			if (!_this.node.contains(event.target)) {
				_this.setState({
					showOptions: false,
					addValue: ''
				});
			}
		};

		_this.state = {
			showOptions: false,
			optionList: _this.props.optionList,
			allowAdd: _this.props.allowAdd !== undefined ? _this.props.allowAdd : true,
			allowDelete: _this.props.allowDelete !== undefined ? _this.props.allowDelete : true,
			value: _this.props.value,
			selectedOption: _this.props.selectedOption,
			canDelete: false,
			selectedDeleteOption: _this.props.selectedOption
		};
		// this.addNewValueRef = React.createRef();
		return _this;
	}

	(0, _createClass3.default)(DynamicList, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			document.addEventListener('mousedown', this.handleClickOutside);
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			document.removeEventListener('mousedown', this.handleClickOutside);
		}
	}, {
		key: 'handleDelete',
		value: function handleDelete(option) {
			var canDelete = this.props.checkCanDelete(option);
			if (canDelete) {
				this.setState({ canDelete: true,
					selectedDeleteOption: option
				});
			} else {
				this.setState({
					canDelete: false
				});
				ToastHelper.error(this.props.errorCode);
			}
		}
	}, {
		key: 'updateComponent',
		value: function updateComponent(optionList, key, value) {
			var _this2 = this;

			if (optionList && typeof key === 'undefined') {
				var selectedKey = optionList.find(function (option) {
					return option.key == _this2.state.selectedOption;
				});
				selectedKey = selectedKey || optionList[0];
				if (selectedKey) {
					key = selectedKey.key;
					value = selectedKey.value;
				}
			}
			this.setState({
				optionList: optionList,
				value: value || '',
				selectedOption: key
			});
		}
	}, {
		key: 'changeOption',
		value: function changeOption(key, value) {
			var _this3 = this;

			this.setState({
				value: value,
				selectedOption: key,
				showOptions: false
			}, function () {
				if (_this3.props.optionSelectCallback && typeof _this3.props.optionSelectCallback === 'function') {
					_this3.props.optionSelectCallback(key);
				}
			});
		}
	}, {
		key: 'storeAddInput',
		value: function storeAddInput() {
			this.setState({
				addValue: event.target.value
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _this4 = this;

			var classes = this.props.classes;

			var showOptions = this.state.showOptions || this.props.optionOpen;
			var selectedDeleteOption = this.state.selectedDeleteOption;

			if (!this.props.addComponent && !this.props.allowAdd && this.state.optionList && this.state.optionList.length == 1) {
				showOptions = false;
			}
			return _react2.default.createElement(
				'div',
				{ ref: function ref(node) {
						_this4.node = node;
					}, style: { position: 'relative' }, className: 'option-list-box' },
				_react2.default.createElement(
					'div',
					{ style: { position: 'relative' } },
					_react2.default.createElement('img', { style: { position: 'absolute', top: '50%', right: 0, transform: 'translateY(-42%)' }, src: '../inlineSVG/outline-arrow-down-grey.svg' }),
					_react2.default.createElement(_TextField2.default, {
						id: 'outlined-dense',
						label: this.props.label,
						className: (0, _clsx2.default)(classes.inputBox, 'dynamic-list-input-box'),
						margin: 'dense',
						variant: 'outlined',
						onFocus: this.toggleOptions,
						value: this.state.value,
						disabled: !!this.props.disabled
					})
				),
				showOptions && _react2.default.createElement(
					_Paper2.default,
					{ className: classes.paper + ' option-list ' + (this.props.reverseMenu ? 'reverse' : ''), style: { minHeight: '30px' }, square: true },
					this.props.addComponent ? this.props.addComponent : this.state.allowAdd && _react2.default.createElement(
						'div',
						{ className: 'add-field d-flex align-items-center' },
						_react2.default.createElement(_TextField2.default, {
							onKeyDown: this.handleKeyDown,
							className: classes.textField,
							margin: 'dense',
							variant: 'outlined',
							value: this.state.addValue,
							onInput: this.storeAddInput.bind(this),
							inputRef: function inputRef(node) {
								_this4.addNewValueRef = node;
							},
							inputProps: {
								maxLength: this.props.maxLength
							},
							onFocus: this.handlePrefixTxtFocus
						}),
						_react2.default.createElement(
							_Button2.default,
							{ onKeyDown: this.handleAddBtnKeyDown, onClick: this.addOption, margin: 'dense', className: 'add-btn', variant: 'contained', color: 'primary' },
							'Add'
						)
					),
					_react2.default.createElement(
						'ul',
						null,
						this.state.optionList && this.state.optionList.map(function (option, index) {
							return _react2.default.createElement(
								'li',
								{ className: _this4.state.selectedOption === option.key || _this4.state.value === option.value ? 'selected' : '',
									key: option.key },
								_react2.default.createElement(
									'div',
									{ className: 'add-field d-flex align-items-center justify-content-between' },
									_react2.default.createElement(
										'div',
										{
											style: { cursor: 'pointer', width: '100%', whiteSpace: 'pre' },
											onClick: _this4.changeOption.bind(_this4, option.key, option.value)
										},
										option.value
									),
									_this4.state.allowDelete && option.key !== '0' && _react2.default.createElement(_Delete2.default, {
										style: { backgroundColor: 'unset', cursor: 'pointer', marginTop: '2px' },
										color: 'action',
										onClick: _this4.handleDelete.bind(_this4, option) })
								)
							);
						})
					)
				),
				this.state.allowDelete && this.state.canDelete && _react2.default.createElement(_ConfirmModal2.default, {
					title: 'Delete Prefix',
					message: 'Are you sure you want to delete the Prefix "' + selectedDeleteOption.value + '" ?',
					cancelText: 'Cancel',
					OKText: 'Delete',
					onClose: this.removeOption
				})
			);
		}
	}]);
	return DynamicList;
}(_react2.default.Component);

DynamicList.propTypes = {
	optionList: _propTypes2.default.array,
	allowAdd: _propTypes2.default.bool,
	maxLength: _propTypes2.default.oneOfType([_propTypes2.default.number, _propTypes2.default.string]),
	classes: _propTypes2.default.object,
	reverseMenu: _propTypes2.default.bool,
	label: _propTypes2.default.string,
	optionOpen: _propTypes2.default.bool,
	optionSelectCallback: _propTypes2.default.func,
	addNewValueCallback: _propTypes2.default.func,
	modalConfirmCallback: _propTypes2.default.func,
	value: _propTypes2.default.string,
	selectedOption: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
	addComponent: _propTypes2.default.node,
	disabled: _propTypes2.default.bool,
	allowDelete: _propTypes2.default.bool,
	checkCanDelete: _propTypes2.default.func,
	errorCode: _propTypes2.default.string
};

exports.default = (0, _styles.withStyles)(styles)(DynamicList);