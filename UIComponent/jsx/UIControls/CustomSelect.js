Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = require('babel-runtime/helpers/objectWithoutProperties');

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _styles = require('@material-ui/core/styles');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles() {
	return {
		selectDiv: {
			position: 'relative',
			width: '100%',
			display: 'flex',
			padding: '8px 0'
		},
		customSelect: {
			textDecoration: 'none',
			border: '1px rgba(0, 0, 0, 0.23) solid',
			borderRadius: '5px',
			outline: 'none',
			display: 'inline-block',
			padding: '4px',
			width: '100%',
			minHeight: '32px',
			backgroundSize: '20px',
			backgroundImage: 'url(../inlineSVG/baseline-expand_more-24px.svg)',
			backgroundRepeat: 'no-repeat',
			backgroundPosition: 'right center',
			backgroundOrigin: 'content-box',
			paddingRight: '8px',
			WebkitAppearance: 'none',
			textIndent: '6px',
			fontSize: '14px',
			fontFamily: 'roboto',
			'&:hover': {
				borderColor: 'rgba(0, 0, 0, 0.9)'
			},
			'&:focus': {
				border: '2px #1789FC solid',
				padding: '4px',
				paddingRight: '8px',
				backgroundImage: 'url(../inlineSVG/baseline-expand_less-24px.svg)'
			},
			'&:focus+label': {
				color: '#1789FC',
				top: '8px',
				transform: 'translate(-12.5%, -50%) scale(.75)'
			}
		},
		selectLabel: {
			position: 'absolute',
			top: '50%',
			left: '12px',
			zIndex: 1,
			opacity: 1,
			transform: 'translate(0, -50%) scale(1)',
			transition: '.2s ease-in-out',
			pointerEvents: 'none',
			background: 'white',
			padding: '0 4px',
			color: '#A4AFB3',
			fontSize: '16px',
			fontFamily: 'roboto'
		},
		labelShrink: {
			top: '8px',
			transform: 'translate(-12.5%, -50%) scale(.75)'
		}
	};
};

var TextInput = function TextInput(_ref) {
	var classes = _ref.classes,
	    label = _ref.label,
	    rootClass = _ref.rootClass,
	    labelClass = _ref.labelClass,
	    dropdownClass = _ref.dropdownClass,
	    value = _ref.value,
	    onChange = _ref.onChange,
	    children = _ref.children,
	    shrinkLabel = _ref.shrinkLabel,
	    inputProps = (0, _objectWithoutProperties3.default)(_ref, ['classes', 'label', 'rootClass', 'labelClass', 'dropdownClass', 'value', 'onChange', 'children', 'shrinkLabel']);

	return _react2.default.createElement(
		'div',
		{ className: (0, _classnames2.default)(classes.selectDiv, rootClass || '') },
		_react2.default.createElement(
			'select',
			(0, _extends3.default)({
				className: (0, _classnames2.default)(classes.customSelect, dropdownClass || ''),
				value: value,
				onChange: onChange
			}, inputProps),
			children
		),
		!!label && _react2.default.createElement(
			'label',
			{ className: (0, _classnames2.default)(classes.selectLabel, shrinkLabel || value ? classes.labelShrink : '', labelClass || '') },
			label
		)
	);
};

TextInput.propTypes = {
	label: _propTypes2.default.string,
	placeholder: _propTypes2.default.string,
	rootClass: _propTypes2.default.string,
	labelClass: _propTypes2.default.string,
	dropdownClass: _propTypes2.default.string,
	onChange: _propTypes2.default.func,
	value: _propTypes2.default.string,
	shrinkLabel: _propTypes2.default.bool,
	classes: _propTypes2.default.object,
	children: _propTypes2.default.oneOfType([_propTypes2.default.arrayOf(_propTypes2.default.node), _propTypes2.default.node])
};
exports.default = (0, _styles.withStyles)(styles)(TextInput);