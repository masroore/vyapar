Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles() {
	return {
		'modal': {
			position: 'fixed',
			left: 0,
			top: 0,
			width: '100%',
			height: '100%',
			zIndex: '990',
			'& .overlay': {
				position: 'absolute',
				left: 0,
				top: 0,
				width: '100%',
				height: '100%',
				zIndex: 995,
				background: 'rgba(0,0,0,0.85)'
			},
			'& .modal_content': {
				zIndex: 999,
				position: 'absolute',
				top: '50%',
				left: '50%',
				transform: 'translate(-50%, -50%)',
				maxHeight: '90%',
				overflow: 'auto',
				background: '#fff',
				padding: '20px 0 0 0',
				boxShadow: '0 1px 5px rgba(0,0,0,0.7)',
				textAlign: 'center',
				borderRadius: '4px',
				width: 'auto'
			},
			'& .modal_content > h2': {
				fontSize: '28px',
				fontWeight: 200,
				margin: '20px 0 40px',
				textAlign: 'center'
			},

			'& .close_modal': {
				position: 'absolute',
				right: '15px',
				top: '10px',
				cursor: 'pointer',
				fontSize: '18px',
				opacity: 0.5,
				background: 'none',
				border: 'none',
				transition: 'opacity 0.2s ease',
				'& :hover': {
					opacity: 0.9
				}
			}
		}
	};
};

var CustomModal = function CustomModal(_ref) {
	var children = _ref.children,
	    customClass = _ref.customClass,
	    show = _ref.show,
	    closeCallback = _ref.closeCallback,
	    classes = _ref.classes;
	return _react2.default.createElement(
		'div',
		{ className: (0, _classnames2.default)(classes.modal, customClass), style: { display: show ? 'block' : 'none' } },
		_react2.default.createElement('div', { className: 'overlay', onClick: closeCallback }),
		_react2.default.createElement(
			'div',
			{ className: 'modal_content' },
			children,
			_react2.default.createElement(
				'button',
				{ title: 'Close', className: 'close_modal', onClick: closeCallback },
				_react2.default.createElement(
					'span',
					null,
					'\u2716'
				)
			)
		)
	);
};

CustomModal.propTypes = {
	children: _propTypes2.default.element,
	customClass: _propTypes2.default.string,
	show: _propTypes2.default.bool,
	closeCallback: _propTypes2.default.func,
	classes: _propTypes2.default.object
};

CustomModal.defaultProps = {
	children: _react2.default.createElement(
		'div',
		null,
		'Empty Modal'
	),
	customClass: '',
	show: false,
	closeCallback: function closeCallback() {
		return false;
	}
};

exports.default = (0, _styles.withStyles)(styles)(CustomModal);