Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = require('babel-runtime/helpers/objectWithoutProperties');

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _styles = require('@material-ui/core/styles');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles() {
	return {
		textDiv: {
			position: 'relative',
			width: '100%',
			display: 'flex',
			padding: '8px 0'
		},
		textInput: {
			textDecoration: 'none',
			border: '1px rgba(0, 0, 0, 0.23) solid',
			borderRadius: '5px',
			outline: 'none',
			display: 'block',
			padding: '8px',
			width: '100%',
			textIndent: '4px',
			fontSize: '14px',
			fontFamily: 'roboto',
			'&::placeholder': {
				color: '#A4AFB3'
			},
			'&:disabled': {
				backgroundColor: 'rgb(235, 235, 228, 0.3)'
			},
			'&:hover': {
				borderColor: 'rgba(0, 0, 0, 0.9)'
			},
			'&:focus': {
				border: '2px #1789FC solid',
				padding: '7px'
			},
			'&:focus+label': {
				color: '#1789FC',
				top: '4px',
				left: '8px',
				transform: 'scale(.75) translate(-12.5%, -50%)',
				visibility: 'visible'
			}
		},
		textLabel: {
			position: 'absolute',
			top: '50%',
			left: '4px',
			zIndex: 1,
			opacity: 1,
			transform: 'translate(0, -50%) scale(1)',
			transition: '.2s ease-in-out',
			pointerEvents: 'none',
			background: 'white',
			padding: '0 4px',
			color: '#A4AFB3',
			visibility: 'hidden'
		},
		labelShrink: {
			top: '4px',
			left: '8px',
			transform: 'scale(.75) translate(-12.5%, -50%)',
			visibility: 'visible'
		},
		error: {
			borderColor: '#f44336',
			color: '#f44336'
		},
		helperText: {
			position: 'absolute',
			fontSize: '12px',
			bottom: '-6px',
			left: '8px'
		}
	};
};

var TextInput = function TextInput(_ref) {
	var classes = _ref.classes,
	    label = _ref.label,
	    placeholder = _ref.placeholder,
	    rootClass = _ref.rootClass,
	    labelClass = _ref.labelClass,
	    inputClass = _ref.inputClass,
	    helperText = _ref.helperText,
	    error = _ref.error,
	    value = _ref.value,
	    onChange = _ref.onChange,
	    inputRef = _ref.inputRef,
	    inputProps = (0, _objectWithoutProperties3.default)(_ref, ['classes', 'label', 'placeholder', 'rootClass', 'labelClass', 'inputClass', 'helperText', 'error', 'value', 'onChange', 'inputRef']);

	return _react2.default.createElement(
		'div',
		{ className: (0, _classnames2.default)(classes.textDiv, rootClass || '', error ? classes.error : '') },
		_react2.default.createElement('input', (0, _extends3.default)({
			type: 'text',
			className: (0, _classnames2.default)(classes.textInput, inputClass || '', error ? classes.error : ''),
			value: value,
			onChange: onChange,
			placeholder: placeholder || label || '',
			ref: inputRef
		}, inputProps)),
		!!label && _react2.default.createElement(
			'label',
			{ className: (0, _classnames2.default)(classes.textLabel, value ? classes.labelShrink : '', labelClass || '', error ? classes.error : '') },
			label
		),
		!!helperText && _react2.default.createElement(
			'span',
			{ className: (0, _classnames2.default)(classes.helperText, error ? classes.error : '') },
			helperText
		)
	);
};

TextInput.propTypes = {
	label: _propTypes2.default.string,
	placeholder: _propTypes2.default.string,
	rootClass: _propTypes2.default.string,
	labelClass: _propTypes2.default.string,
	inputClass: _propTypes2.default.string,
	onChange: _propTypes2.default.func,
	value: _propTypes2.default.string,
	classes: _propTypes2.default.object,
	helperText: _propTypes2.default.string,
	error: _propTypes2.default.bool,
	inputRef: _propTypes2.default.func
};
exports.default = (0, _styles.withStyles)(styles)(TextInput);