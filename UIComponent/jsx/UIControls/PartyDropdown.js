Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _objectWithoutProperties2 = require('babel-runtime/helpers/objectWithoutProperties');

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/core/styles');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _OutlineInput = require('./OutlineInput');

var _OutlineInput2 = _interopRequireDefault(_OutlineInput);

var _TxnTypeConstant = require('../../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _StringConstants = require('../../../Constants/StringConstants');

var StringConstant = _interopRequireWildcard(_StringConstants);

var _AddCircleOutline = require('@material-ui/icons/AddCircleOutline');

var _AddCircleOutline2 = _interopRequireDefault(_AddCircleOutline);

var _AutoSuggest = require('../AutoSuggest');

var _AutoSuggest2 = _interopRequireDefault(_AutoSuggest);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var menuStyles = {
	header: {
		fontSize: 12,
		fontFamily: 'RobotoMedium',
		height: 30,
		padding: 10,
		width: 125,
		whiteSpace: 'nowrap',
		marginRight: 5,
		overflow: 'hidden',
		textOverflow: 'ellipsis'
	},
	menuItems: {
		fontSize: 12,
		color: '#222A3F',
		textAlign: 'right'
	},
	headerCommon: {
		color: 'rgba(34, 42, 63, 0.5)',
		textAlign: 'right'
	},
	addItem: {
		color: '#1789FC',
		cursor: 'pointer',
		position: 'relative',
		top: -4,
		left: 4
	},
	addIcon: {
		color: '#1789FC',
		cursor: 'pointer',
		fontSize: 16
	},
	container: {
		position: 'absolute',
		maxHeight: 200,
		overflowY: 'auto',
		overflowX: 'hidden',
		background: '#fff',
		zIndex: 9999,
		border: '1px solid #ddd',
		borderTop: 0
	},
	sectionContainer: {
		overflow: 'auto',
		background: '#fff',
		zIndex: 99999,
		position: 'relative',
		border: '1px solid #ddd',
		borderRight: 0
	},
	selected: {
		background: '#EFF4FD',
		border: '1px solid #BBE3FF',
		cursor: 'pointer'
	},
	notSelected: {
		border: '1px solid transparent'
	},
	nameWidth: {
		width: 230,
		textAlign: 'left'
	},
	partyBalance: {
		marginRight: 5,
		position: 'relative',
		top: -3
	},
	dropdownContainer: {
		position: 'relative'
	},
	balance: {
		position: 'absolute',
		fontSize: 11,
		fontFamily: 'robotoMedium',
		left: 15
	},
	payable: {
		color: '#FE6B6B'
	},
	receivable: {
		color: '#21C2A7'
	},
	disabled: {
		background: 'transparent'
	}
};

var getSuggestionValue = function getSuggestionValue(suggestion) {
	return suggestion.label;
};

// Use your imagination to render suggestions.
var CustomSuggession = (0, _styles.withStyles)(menuStyles)(function (props) {
	var classes = props.classes,
	    label = props.label,
	    balance = props.balance,
	    visible = props.visible,
	    selectProps = props.selectProps;
	var txnType = selectProps.txnType;

	return _react2.default.createElement(
		'div',
		null,
		visible !== false && _react2.default.createElement(
			'div',
			{ className: (0, _classnames2.default)('d-flex justify-content-between') },
			_react2.default.createElement(
				'div',
				{
					title: label,
					className: (0, _classnames2.default)(classes.header, classes.menuItems, classes.nameWidth)
				},
				label
			),
			txnType !== _TxnTypeConstant2.default.TXN_TYPE_OTHER_INCOME && txnType !== _TxnTypeConstant2.default.TXN_TYPE_EXPENSE && _react2.default.createElement(
				'div',
				{ className: (0, _classnames2.default)(classes.header, classes.menuItems) },
				_react2.default.createElement(
					'span',
					{ className: classes.partyBalance },
					Math.abs(balance)
				),
				balance > 0 && _react2.default.createElement('img', { src: '../inlineSVG/receivable.svg' }),
				balance < 0 && _react2.default.createElement('img', { src: '../inlineSVG/payable.svg' })
			)
		)
	);
});
CustomSuggession.propTypes = {
	Component: _propTypes2.default.element
};

var renderSuggestion = function renderSuggestion(selectProps) {
	function RenderSuggession(props) {
		return _react2.default.createElement(CustomSuggession, (0, _extends3.default)({ selectProps: selectProps }, props));
	}
	return RenderSuggession;
};
renderSuggestion.propTypes = {
	Component: _propTypes2.default.element
};

function renderSectionTitle(props) {
	function RenderSectionTitle(section) {
		var classes = props.classes,
		    createNewItem = props.createNewItem,
		    addPartyLabel = props.addPartyLabel,
		    txnType = props.txnType,
		    value = props.value;

		return _react2.default.createElement(
			'div',
			{
				className: (0, _classnames2.default)(classes.sectionContainer, 'd-flex justify-content-between')
			},
			_react2.default.createElement(
				'div',
				{
					onClick: function onClick(e) {
						e.persist();
						createNewItem(value);
					},
					className: (0, _classnames2.default)(classes.header, classes.addItem, classes.nameWidth)
				},
				_react2.default.createElement(_AddCircleOutline2.default, { className: classes.addIcon, color: 'secondary' }),
				_react2.default.createElement(
					'span',
					{ className: classes.addItem },
					addPartyLabel
				)
			),
			txnType !== _TxnTypeConstant2.default.TXN_TYPE_OTHER_INCOME && txnType !== _TxnTypeConstant2.default.TXN_TYPE_EXPENSE && _react2.default.createElement(
				'div',
				{
					className: (0, _classnames2.default)(classes.header, classes.headerCommon, 'dropdownHeader')
				},
				'Party Balance'
			)
		);
	}
	RenderSectionTitle.propTypes = {
		classes: _propTypes2.default.object,
		createNewItem: _propTypes2.default.func,
		addPartyLabel: _propTypes2.default.string,
		txnType: _propTypes2.default.number,
		value: _propTypes2.default.string
	};
	return RenderSectionTitle;
}

renderSectionTitle.propTypes = {
	Component: _propTypes2.default.element
};

function getSectionSuggestions(section) {
	return section.names;
}

function InputControl(props) {
	var rest = (0, _objectWithoutProperties3.default)(props, []);

	return _react2.default.createElement(_OutlineInput2.default, (0, _extends3.default)({}, rest, { inputProps: { className: 'autoSuggestInput' } }));
}
InputControl.propTypes = {
	onBlur: _propTypes2.default.func,
	onNewBlur: _propTypes2.default.func
};

var AutoSuggest = function (_React$Component) {
	(0, _inherits3.default)(AutoSuggest, _React$Component);

	function AutoSuggest(props) {
		(0, _classCallCheck3.default)(this, AutoSuggest);

		var _this = (0, _possibleConstructorReturn3.default)(this, (AutoSuggest.__proto__ || (0, _getPrototypeOf2.default)(AutoSuggest)).call(this, props));

		_this.getSuggestions = function (value) {
			var items = _this.props.items;

			var inputValue = value.trim().toLowerCase();
			var inputLength = inputValue.length;
			var filtered = [];
			if (inputLength === 0) {
				filtered = items;
			} else {
				filtered = items.filter(function (party) {
					return party.label.toLowerCase().indexOf(inputValue) !== -1;
				});
			}
			if (_this.showFirstTimeAllOptions) {
				_this.showFirstTimeAllOptions = false;
				filtered = items;
			}
			return [{
				title: '',
				names: filtered.length ? filtered.slice(0, 100) : [{
					value: 0,
					label: '',
					balance: '',
					partyLogic: null,
					visible: false
				}]
			}];
		};

		_this.onChange = function (event, party) {
			var newValue = party.newValue;

			_this.setState({
				value: newValue
			});
		};

		_this.updateParty = function (name) {
			_this.setState({ value: name });
		};

		_this.onBlur = function (event, _ref) {
			var highlightedSuggestion = _ref.highlightedSuggestion;

			var value = '';
			if (highlightedSuggestion) {
				var onChange = _this.props.onChange;

				onChange && onChange(highlightedSuggestion);
				return false;
			} else if (event.target) {
				value = event.target.value.trim();
			}

			_this.changeValue(value);
		};

		_this.changeValue = function (value) {
			var _this$props = _this.props,
			    items = _this$props.items,
			    onChange = _this$props.onChange,
			    item = _this$props.value;

			var lowerCaseValue = value.toLowerCase();

			if ((!item || !item.label) && value == '' || (item && item.label.toLowerCase()) === lowerCaseValue) {
				return false;
			}
			var selected = items.find(function (party) {
				return party.label.toLowerCase() === lowerCaseValue;
			});
			if (!selected) {
				selected = {
					value: 0,
					label: value,
					balance: 0,
					partyLogic: null
				};
			}

			onChange && onChange(selected);
		};

		_this.onSuggestionsFetchRequested = function (_ref2) {
			var value = _ref2.value;

			_this.setState({
				suggestions: _this.getSuggestions(value)
			});
		};

		_this.onSuggestionsClearRequested = function () {
			_this.setState({
				suggestions: []
			});
		};

		_this.forceUpdate = function () {
			var partyTextBox = document.querySelector('#salePurchasePartyDropdown');
			if (partyTextBox && document.activeElement == partyTextBox) {
				partyTextBox.blur();
				setTimeout(function () {
					partyTextBox.focus();
				});
			}
		};

		_this.state = {
			value: _this.getValueFromProps(props.value),
			suggestions: []
			// changed: false
		};
		var classes = props.classes,
		    showFirstTimeAllOptions = props.showFirstTimeAllOptions;

		_this.showFirstTimeAllOptions = showFirstTimeAllOptions;
		_this.theme = {
			containerOpen: 'react-autosuggest__container--open',
			input: classes.nameWidth,
			inputOpen: 'react-autosuggest__input--open',
			inputFocused: 'react-autosuggest__input--focused',
			suggestionsContainer: 'react-autosuggest__suggestions-container',
			suggestionsContainerOpen: classes.container,
			suggestionsList: '',
			suggestion: classes.notSelected,
			suggestionFirst: 'react-autosuggest__suggestion--first',
			suggestionHighlighted: classes.selected,
			sectionContainer: 'react-autosuggest__section-container',
			sectionContainerFirst: 'react-autosuggest__section-container--first',
			sectionTitle: 'react-autosuggest__section-title'
		};
		_this.dropdownRef = _react2.default.createRef();
		_this._inputString = '';
		props.updateParty && props.updateParty(_this.updateParty);
		return _this;
	}

	(0, _createClass3.default)(AutoSuggest, [{
		key: 'getValueFromProps',
		value: function getValueFromProps(value) {
			if (value && value.label) {
				return value.label;
			}
			return '';
		}
		// onSuggestionSelected = (event, { suggestion }) => {
		// 	// const { onChange } = this.props;
		// 	// onChange && onChange(suggestion);
		// };

		// Autosuggest will call this function every time you need to update suggestions.
		// You already implemented this logic above, so just use it.


		// Autosuggest will call this function every time you need to clear suggestions.

	}, {
		key: 'componentDidUpdate',
		value: function componentDidUpdate(props, state) {
			var party = props.value;
			if (party && party.label !== state.value) {
				return {
					value: party.label
				};
			}
			return null;
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			var onLoad = this.props.onLoad;

			onLoad && onLoad();
		}
	}, {
		key: 'render',
		value: function render() {
			var _state = this.state,
			    value = _state.value,
			    suggestions = _state.suggestions;
			var _props = this.props,
			    className = _props.className,
			    label = _props.label,
			    txnType = _props.txnType,
			    classes = _props.classes,
			    showBalance = _props.showBalance,
			    selected = _props.value,
			    createNewItem = _props.createNewItem,
			    addPartyLabel = _props.addPartyLabel,
			    isDisabled = _props.isDisabled,
			    isEdit = _props.isEdit,
			    isConvert = _props.isConvert,
			    showPartiesClicked = _props.showPartiesClicked,
			    renderDropdown = _props.renderDropdown,
			    items = _props.items,
			    renderForFirstAutofocus = _props.renderForFirstAutofocus;


			var parties = items.filter(function (item) {
				return item.value != 0;
			});
			var shouldRenderDropdown = true;
			if (renderDropdown === false) {
				shouldRenderDropdown = parties.length > 0;
			}

			if (!renderForFirstAutofocus && shouldRenderDropdown) {
				shouldRenderDropdown = false;
			}

			var dropdownClassName = className;
			if (isDisabled) {
				dropdownClassName = (0, _classnames2.default)(className, classes.disabled);
			}

			// Autosuggest will pass through all these props to the input.
			var inputProps = {
				value: value || '',
				onChange: this.onChange,
				className: dropdownClassName,
				onBlur: this.onBlur,
				fullWidth: true,
				autoFocus: !isEdit && !isConvert || showPartiesClicked,
				onClick: this.forceUpdate,
				label: label,
				variant: 'outlined',
				disabled: isDisabled,
				id: 'salePurchasePartyDropdown',
				ref: this.dropdownRef
			};

			var dropdownProps = {
				suggestions: suggestions,
				onSuggestionsFetchRequested: this.onSuggestionsFetchRequested,
				onSuggestionsClearRequested: this.onSuggestionsClearRequested,
				getSuggestionValue: getSuggestionValue,
				renderSuggestion: renderSuggestion({ txnType: txnType }),
				renderSectionTitle: renderSectionTitle({
					classes: classes,
					createNewItem: createNewItem,
					addPartyLabel: addPartyLabel,
					txnType: txnType,
					value: value
				}),
				getSectionSuggestions: getSectionSuggestions,
				renderInputComponent: InputControl,
				inputProps: inputProps,
				multiSection: true,
				focusInputOnSuggestionClick: false,
				theme: this.theme,
				shouldRenderSuggestions: function shouldRenderSuggestions() {
					return shouldRenderDropdown;
				}
			};

			// if (
			// 	txnType != TxnTypeConstant.TXN_TYPE_CASHIN &&
			// txnType != TxnTypeConstant.TXN_TYPE_CASHOUT
			// ) {
			// 	dropdownProps.shouldRenderSuggestions = () => shouldRenderDropdown;
			// }

			// Finally, render it!
			return _react2.default.createElement(
				'div',
				{ className: classes.dropdownContainer },
				_react2.default.createElement(_AutoSuggest2.default, dropdownProps),
				showBalance == true && selected.value != 0 && selected.label !== StringConstant.cash && selected.label !== StringConstant.cashSale && _react2.default.createElement(
					'div',
					{ className: (0, _classnames2.default)(classes.balance, Number(selected.balance) > 0 ? classes.receivable : classes.payable)
					},
					'BAL: ',
					Math.abs(selected.balance)
				)
			);
		}
	}]);
	return AutoSuggest;
}(_react2.default.Component);

AutoSuggest.propTypes = {
	classes: _propTypes2.default.object,
	createNewItem: _propTypes2.default.func,
	addPartyLabel: _propTypes2.default.string,
	txnType: _propTypes2.default.number,
	value: _propTypes2.default.object,
	onBlur: _propTypes2.default.func,
	onNewBlur: _propTypes2.default.func,
	items: _propTypes2.default.array,
	onChange: _propTypes2.default.func,
	className: _propTypes2.default.string,
	label: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object]),
	showBalance: _propTypes2.default.bool,
	isDisabled: _propTypes2.default.bool,
	isEdit: _propTypes2.default.bool,
	isConvert: _propTypes2.default.bool,
	showFirstTimeAllOptions: _propTypes2.default.bool,
	showPartiesClicked: _propTypes2.default.bool,
	renderDropdown: _propTypes2.default.bool,
	updateParty: _propTypes2.default.func
};

exports.default = (0, _styles.withStyles)(menuStyles)(AutoSuggest);