Object.defineProperty(exports, "__esModule", {
  value: true
});

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _InputLabel = require('@material-ui/core/InputLabel');

var _InputLabel2 = _interopRequireDefault(_InputLabel);

var _FormControl = require('@material-ui/core/FormControl');

var _FormControl2 = _interopRequireDefault(_FormControl);

var _Select = require('@material-ui/core/Select');

var _Select2 = _interopRequireDefault(_Select);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _OutlinedInput = require('@material-ui/core/OutlinedInput');

var _OutlinedInput2 = _interopRequireDefault(_OutlinedInput);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
  return {
    root: {
      display: 'flex',
      flexWrap: 'wrap'
    },
    formControl: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      '& label + .MuiInput-formControl': {
        marginTop: 0,
        fontSize: 14
      }
    }
  };
};

var SelectDd = function (_React$Component) {
  (0, _inherits3.default)(SelectDd, _React$Component);

  function SelectDd(props) {
    (0, _classCallCheck3.default)(this, SelectDd);

    var _this = (0, _possibleConstructorReturn3.default)(this, (SelectDd.__proto__ || (0, _getPrototypeOf2.default)(SelectDd)).call(this, props));

    _this.handleChange = function (event) {
      _this.setState({
        selectDdValue: event.target.value
      });
      if (_this.props.handleChange && typeof _this.props.handleChange === 'function') _this.props.handleChange(event.target.value);
    };

    _this.state = {
      selectDdValue: _this.props.value
    };
    return _this;
  }

  (0, _createClass3.default)(SelectDd, [{
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(props) {
      this.setState({
        selectDdValue: props.value
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _classNames;

      var classes = this.props.classes;

      var rootClasses = (0, _classnames2.default)((_classNames = {}, (0, _defineProperty3.default)(_classNames, classes.root, true), (0, _defineProperty3.default)(_classNames, this.props.className, true), _classNames));
      var selectDd = void 0;
      if (this.props.outlined) {
        selectDd = _react2.default.createElement(
          _Select2.default,
          {
            disabled: this.props.isDisabled,
            value: this.state.selectDdValue,
            onChange: this.handleChange,
            inputProps: {
              name: this.props.name,
              id: this.props.name
            },
            input: _react2.default.createElement(_OutlinedInput2.default, { margin: 'dense' })
          },
          this.props.children
        );
      } else {
        selectDd = _react2.default.createElement(
          _Select2.default,
          {
            disabled: this.props.isDisabled,
            value: this.state.selectDdValue,
            onChange: this.handleChange,
            inputProps: {
              name: this.props.name,
              id: this.props.name
            }
          },
          this.props.children
        );
      }
      return _react2.default.createElement(
        'form',
        { className: rootClasses + ' selectDropDown-formControl', autoComplete: 'off' },
        _react2.default.createElement(
          _FormControl2.default,
          { className: classes.formControl },
          _react2.default.createElement(
            _InputLabel2.default,
            { htmlFor: this.props.id },
            this.props.title
          ),
          selectDd
        )
      );
    }
  }]);
  return SelectDd;
}(_react2.default.Component);

exports.default = (0, _styles.withStyles)(styles)(SelectDd);