Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = require('babel-runtime/helpers/objectWithoutProperties');

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _TextField = require('@material-ui/core/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CustomDatePicker = function (_React$Component) {
	(0, _inherits3.default)(CustomDatePicker, _React$Component);

	function CustomDatePicker(props) {
		(0, _classCallCheck3.default)(this, CustomDatePicker);

		var _this = (0, _possibleConstructorReturn3.default)(this, (CustomDatePicker.__proto__ || (0, _getPrototypeOf2.default)(CustomDatePicker)).call(this, props));

		_this.handleDateChange = function () {
			var _this$props;

			(_this$props = _this.props).onChange.apply(_this$props, arguments);
		};

		_this.state = {};
		return _this;
	}

	(0, _createClass3.default)(CustomDatePicker, [{
		key: 'render',
		value: function render() {
			var _props = this.props,
			    onChange = _props.onChange,
			    classes = _props.classes,
			    otherProps = (0, _objectWithoutProperties3.default)(_props, ['onChange', 'classes']);

			return _react2.default.createElement(_TextField2.default, (0, _extends3.default)({
				margin: 'dense',
				variant: 'outlined',
				fullWidth: true,
				color: 'primary',
				type: 'date'
			}, otherProps, {
				onChange: this.handleDateChange,
				InputLabelProps: {
					shrink: true
				}
			}));
		}
	}]);
	return CustomDatePicker;
}(_react2.default.Component);

CustomDatePicker.propTypes = {
	classes: _propTypes2.default.object,
	label: _propTypes2.default.string,
	name: _propTypes2.default.string,
	value: _propTypes2.default.string,
	onChange: _propTypes2.default.func
};

exports.default = CustomDatePicker;