Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = require('babel-runtime/helpers/objectWithoutProperties');

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/core/styles');

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _blue = require('@material-ui/core/colors/blue');

var _blue2 = _interopRequireDefault(_blue);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
	return {
		cssRoot: {
			color: theme.palette.getContrastText(_blue2.default[500]),
			backgroundColor: _blue2.default[900],
			letterSpacing: 0,
			'&:hover': {
				backgroundColor: _blue2.default[800]
			}
		}
	};
};

var PrimaryButton = function (_Component) {
	(0, _inherits3.default)(PrimaryButton, _Component);

	function PrimaryButton() {
		(0, _classCallCheck3.default)(this, PrimaryButton);
		return (0, _possibleConstructorReturn3.default)(this, (PrimaryButton.__proto__ || (0, _getPrototypeOf2.default)(PrimaryButton)).apply(this, arguments));
	}

	(0, _createClass3.default)(PrimaryButton, [{
		key: 'render',
		value: function render() {
			var _props = this.props,
			    classes = _props.classes,
			    rest = (0, _objectWithoutProperties3.default)(_props, ['classes']);

			return _react2.default.createElement(
				_Button2.default,
				(0, _extends3.default)({ variant: 'contained' }, rest, { className: classes.cssRoot }),
				this.props.children
			);
		}
	}]);
	return PrimaryButton;
}(_react.Component);

PrimaryButton.propTypes = {
	classes: _propTypes2.default.object.isRequired
};

exports.default = (0, _styles.withStyles)(styles)(PrimaryButton);