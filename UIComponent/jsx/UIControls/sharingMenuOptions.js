Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _trashableReact = require('trashable-react');

var _trashableReact2 = _interopRequireDefault(_trashableReact);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
	return {
		sharingMenu: {
			backgroundColor: '#F5F6F7',
			marginTop: 30,
			paddingBottom: 30
		},

		sharingMenuOptions: {
			display: 'flex',
			flexWrap: 'nowrap',
			'& .sharing-menu-item': {
				flex: 1,
				textAlign: 'center',
				cursor: 'pointer',
				'& img': {
					width: 32,
					height: 32
				},
				'& .item-label': {
					fontSize: 10,
					paddingTop: 5
				}
			}
		}
	};
};

var SharingMenuOptions = function (_React$Component) {
	(0, _inherits3.default)(SharingMenuOptions, _React$Component);

	function SharingMenuOptions(props) {
		(0, _classCallCheck3.default)(this, SharingMenuOptions);

		var _this = (0, _possibleConstructorReturn3.default)(this, (SharingMenuOptions.__proto__ || (0, _getPrototypeOf2.default)(SharingMenuOptions)).call(this, props));

		_this.handleEmail = function () {
			_this.props.handleEmail && _this.props.handleEmail();
		};

		_this.handleSms = function () {
			_this.props.handleSms && _this.props.handleSms();
		};

		_this.handleWhatsapp = function () {
			_this.props.handleWhatsapp && _this.props.handleWhatsapp();
		};

		return _this;
	}

	(0, _createClass3.default)(SharingMenuOptions, [{
		key: 'render',
		value: function render() {
			var classes = this.props.classes;

			return _react2.default.createElement(
				_react2.default.Fragment,
				null,
				this.props.isVisible && _react2.default.createElement(
					'div',
					{ className: classes.sharingMenu + ' ' + this.props.customClasses },
					this.props.headerTitle && _react2.default.createElement(
						'span',
						{ className: 'sub-heading' },
						this.props.headerTitle
					),
					_react2.default.createElement(
						'div',
						{ className: classes.sharingMenuOptions + ' font-family-regular' },
						_react2.default.createElement(
							'div',
							{ onClick: this.handleEmail,
								className: (0, _classnames2.default)({
									'sharing-menu-item': true,
									'disable-input': !this.props.handleEmail || typeof this.props.handleEmail !== 'function'
								})
							},
							_react2.default.createElement('img', { src: '../inlineSVG/gmail.svg' }),
							_react2.default.createElement(
								'div',
								{ className: 'item-label' },
								'EMAIL'
							)
						),
						_react2.default.createElement(
							'div',
							{ onClick: this.handleSms, className: (0, _classnames2.default)({
									'sharing-menu-item': true,
									'disable-input': !this.props.handleSms || typeof this.props.handleSms !== 'function'
								}) },
							_react2.default.createElement('img', { src: '../inlineSVG/baseline-sms-24px.svg' }),
							_react2.default.createElement(
								'div',
								{ className: 'item-label' },
								'SMS'
							)
						),
						_react2.default.createElement(
							'div',
							{ onClick: this.handleWhatsapp, className: (0, _classnames2.default)({
									'sharing-menu-item': true,
									'disable-input': !this.props.handleWhatsapp || typeof this.props.handleWhatsapp !== 'function'
								}) },
							_react2.default.createElement('img', { src: '../inlineSVG/whatsapp.svg' }),
							_react2.default.createElement(
								'div',
								{ className: 'item-label' },
								'WHATSAPP'
							)
						)
					)
				)
			);
		}
	}]);
	return SharingMenuOptions;
}(_react2.default.Component);

exports.default = (0, _styles.withStyles)(styles)((0, _trashableReact2.default)(SharingMenuOptions));