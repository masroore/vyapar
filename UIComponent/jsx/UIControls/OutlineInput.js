Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = require('babel-runtime/helpers/objectWithoutProperties');

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/core/styles');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _TextField = require('@material-ui/core/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles() {
	return {
		textFieldContainer: {
			height: 36,
			background: '#fff'
		},
		textField: {
			fontSize: 13,
			color: '#222A3F',
			zIndex: 0,
			height: 36,
			paddingTop: 22,
			'&:focus': {
				outline: '0 !important'
			}
		},
		textFieldLabel: {
			fontSize: 16,
			zIndex: 0,
			fontFamily: 'RobotoMedium',
			color: 'rgba(34, 42, 63, 0.5)',
			transform: 'translate(14px, 13px) scale(0.81)',
			whiteSpace: 'nowrap',
			overflow: 'hidden',
			maxWidth: 200,
			textOverflow: 'ellipsis'
		},
		ellipsis: {
			overflow: 'hidden',
			textOverflow: 'ellipsis'
		},
		notchedOutline: {}
	};
};

var OutlineInput = function (_React$PureComponent) {
	(0, _inherits3.default)(OutlineInput, _React$PureComponent);

	function OutlineInput() {
		(0, _classCallCheck3.default)(this, OutlineInput);
		return (0, _possibleConstructorReturn3.default)(this, (OutlineInput.__proto__ || (0, _getPrototypeOf2.default)(OutlineInput)).apply(this, arguments));
	}

	(0, _createClass3.default)(OutlineInput, [{
		key: 'render',
		value: function render() {
			var _props = this.props,
			    classes = _props.classes,
			    label = _props.label,
			    className = _props.className,
			    value = _props.value,
			    type = _props.type,
			    customClasses = _props.customClasses,
			    inputProps = _props.inputProps,
			    rest = (0, _objectWithoutProperties3.default)(_props, ['classes', 'label', 'className', 'value', 'type', 'customClasses', 'inputProps']);


			var props = {
				label: label || '',
				value: value,
				type: type || 'text',
				classes: customClasses ? (0, _extends3.default)({ root: classes.textFieldContainer }, customClasses) : { root: classes.textFieldContainer },
				className: className ? (0, _classnames2.default)(classes.textFieldContainer, className) : classes.textFieldContainer,
				InputLabelProps: {
					classes: {
						root: classes.textFieldLabel
					},
					shrink: !!value
				},
				InputProps: (0, _extends3.default)({
					classes: {
						input: (0, _classnames2.default)(classes.textField, classes.ellipsis),
						notchedOutline: classes.notchedOutline
					},
					notched: !!value
				}, inputProps),
				margin: 'none',
				variant: 'outlined'
			};
			return _react2.default.createElement(_TextField2.default, (0, _extends3.default)({ title: value }, props, rest));
		}
	}]);
	return OutlineInput;
}(_react2.default.PureComponent);

OutlineInput.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	options: _propTypes2.default.object,
	inputProps: _propTypes2.default.object,
	label: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object]),
	className: _propTypes2.default.string,
	value: _propTypes2.default.string,
	type: _propTypes2.default.string,
	customClasses: _propTypes2.default.string
};

exports.default = (0, _styles.withStyles)(styles)(OutlineInput);