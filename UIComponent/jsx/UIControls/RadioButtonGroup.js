Object.defineProperty(exports, "__esModule", {
    value: true
});

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _RadioGroup = require('@material-ui/core/RadioGroup');

var _RadioGroup2 = _interopRequireDefault(_RadioGroup);

var _FormControl = require('@material-ui/core/FormControl');

var _FormControl2 = _interopRequireDefault(_FormControl);

var _FormLabel = require('@material-ui/core/FormLabel');

var _FormLabel2 = _interopRequireDefault(_FormLabel);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
    return {
        root: {
            display: 'flex'
        },
        formControl: {
            margin: theme.spacing(3)
        },
        group: {
            margin: theme.spacing(1, 0)
        }
    };
};

var RadioButtonsGroup = function (_React$Component) {
    (0, _inherits3.default)(RadioButtonsGroup, _React$Component);

    function RadioButtonsGroup(props) {
        (0, _classCallCheck3.default)(this, RadioButtonsGroup);

        var _this = (0, _possibleConstructorReturn3.default)(this, (RadioButtonsGroup.__proto__ || (0, _getPrototypeOf2.default)(RadioButtonsGroup)).call(this, props));

        _this.handleChange = function (event) {
            _this.setState({
                value: event.target.value
            });
            if (_this.props.handleChange && typeof _this.props.handleChange === 'function') _this.props.handleChange(event);
        };

        _this.state = {
            value: _this.props.value
        };
        return _this;
    }

    (0, _createClass3.default)(RadioButtonsGroup, [{
        key: 'render',
        value: function render() {
            var _classNames;

            var classes = this.props.classes;

            var rootClasses = (0, _classnames2.default)((_classNames = {}, (0, _defineProperty3.default)(_classNames, classes.root, true), (0, _defineProperty3.default)(_classNames, this.props.className, true), _classNames));
            return _react2.default.createElement(
                'div',
                { className: rootClasses },
                _react2.default.createElement(
                    _FormControl2.default,
                    { component: 'fieldset', className: classes.formControl },
                    this.props.title && _react2.default.createElement(
                        _FormLabel2.default,
                        { component: 'legend' },
                        this.props.title
                    ),
                    _react2.default.createElement(
                        _RadioGroup2.default,
                        {
                            'aria-label': this.props.ariaLabel,
                            name: this.props.name,
                            className: classes.group,
                            value: this.state.value,
                            onChange: this.handleChange,
                            id: this.props.id
                        },
                        this.props.children
                    )
                )
            );
        }
    }]);
    return RadioButtonsGroup;
}(_react2.default.Component);

exports.default = (0, _styles.withStyles)(styles)(RadioButtonsGroup);