Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = require('babel-runtime/helpers/objectWithoutProperties');

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _deburr = require('lodash/deburr');

var _deburr2 = _interopRequireDefault(_deburr);

var _downshift = require('downshift');

var _downshift2 = _interopRequireDefault(_downshift);

var _styles = require('@material-ui/core/styles');

var _TextField = require('@material-ui/core/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _Paper = require('@material-ui/core/Paper');

var _Paper2 = _interopRequireDefault(_Paper);

var _MenuItem = require('@material-ui/core/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
	return {
		container: {
			flexGrow: 1,
			position: 'relative'
		},
		paper: {
			position: 'absolute',
			zIndex: 1,
			marginTop: theme.spacing(1),
			left: 0,
			right: 0
		},
		chip: {
			margin: theme.spacing(1) / 2 + 'px ' + theme.spacing(1) / 4 + 'px'
		},
		inputRoot: {
			flexWrap: 'wrap'
		},
		inputInput: {
			width: 'auto',
			flexGrow: 1
		},
		divider: {
			height: theme.spacing(2)
		}
	};
};

function renderInput(inputProps) {
	var InputProps = inputProps.InputProps,
	    classes = inputProps.classes,
	    ref = inputProps.ref,
	    other = (0, _objectWithoutProperties3.default)(inputProps, ['InputProps', 'classes', 'ref']);


	return _react2.default.createElement(_TextField2.default, (0, _extends3.default)({
		InputProps: (0, _extends3.default)({
			inputRef: ref,
			classes: {
				input: classes.inputInput
			}
		}, InputProps)
	}, other));
}

function renderSuggestion(_ref) {
	var suggestion = _ref.suggestion,
	    index = _ref.index,
	    itemProps = _ref.itemProps,
	    highlightedIndex = _ref.highlightedIndex,
	    selectedItem = _ref.selectedItem;

	var isHighlighted = highlightedIndex === index;
	var isSelected = (selectedItem || '').indexOf(suggestion.label) > -1;

	return _react2.default.createElement(
		_MenuItem2.default,
		(0, _extends3.default)({}, itemProps, {
			key: suggestion,
			selected: isHighlighted,
			component: 'div',
			style: {
				fontWeight: isSelected ? 500 : 400
			}
		}),
		suggestion
	);
}
renderSuggestion.propTypes = {
	highlightedIndex: _propTypes2.default.number,
	index: _propTypes2.default.number,
	itemProps: _propTypes2.default.object,
	selectedItem: _propTypes2.default.string,
	suggestion: _propTypes2.default.shape({ label: _propTypes2.default.string }).isRequired
};

function getSuggestions(value, list) {
	var inputValue = (0, _deburr2.default)(value.trim()).toLowerCase();
	var inputLength = inputValue.length;
	var count = 0;

	return inputLength === 0 ? [] : list.filter(function (suggestion) {
		var keep = count < 5 && suggestion.toLowerCase().indexOf(inputValue) >= 0;

		if (keep) {
			count += 1;
		}

		return keep;
	});
}

var Autocomplete = function (_Component) {
	(0, _inherits3.default)(Autocomplete, _Component);

	function Autocomplete() {
		(0, _classCallCheck3.default)(this, Autocomplete);
		return (0, _possibleConstructorReturn3.default)(this, (Autocomplete.__proto__ || (0, _getPrototypeOf2.default)(Autocomplete)).apply(this, arguments));
	}

	(0, _createClass3.default)(Autocomplete, [{
		key: 'render',
		value: function render(props) {
			var _this2 = this;

			var classes = this.props.classes;

			return _react2.default.createElement(
				'div',
				null,
				_react2.default.createElement(
					_downshift2.default,
					{ id: this.props.id },
					function (_ref2) {
						var getInputProps = _ref2.getInputProps,
						    getItemProps = _ref2.getItemProps,
						    getMenuProps = _ref2.getMenuProps,
						    highlightedIndex = _ref2.highlightedIndex,
						    inputValue = _ref2.inputValue,
						    isOpen = _ref2.isOpen,
						    selectedItem = _ref2.selectedItem;
						return _react2.default.createElement(
							'div',
							{ className: classes.container },
							renderInput({
								fullWidth: true,
								classes: classes,
								InputProps: getInputProps({
									placeholder: _this2.props.label
								}),
								label: _this2.props.label
							}),
							_react2.default.createElement(
								'div',
								getMenuProps(),
								isOpen ? _react2.default.createElement(
									_Paper2.default,
									{ className: classes.paper, square: true },
									getSuggestions(inputValue, _this2.props.list).map(function (suggestion, index) {
										return renderSuggestion({
											suggestion: suggestion,
											index: index,
											itemProps: getItemProps({ item: suggestion }),
											highlightedIndex: highlightedIndex,
											selectedItem: selectedItem
										});
									})
								) : null
							)
						);
					}
				),
				_react2.default.createElement('div', { className: classes.divider })
			);
		}
	}]);
	return Autocomplete;
}(Component);

Autocomplete.propTypes = {
	classes: _propTypes2.default.object.isRequired
};

exports.default = (0, _styles.withStyles)(styles)(Autocomplete);