Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _List = require('@material-ui/core/List');

var _List2 = _interopRequireDefault(_List);

var _ListItem = require('@material-ui/core/ListItem');

var _ListItem2 = _interopRequireDefault(_ListItem);

var _ListItemSecondaryAction = require('@material-ui/core/ListItemSecondaryAction');

var _ListItemSecondaryAction2 = _interopRequireDefault(_ListItemSecondaryAction);

var _ListItemText = require('@material-ui/core/ListItemText');

var _ListItemText2 = _interopRequireDefault(_ListItemText);

var _Checkbox = require('@material-ui/core/Checkbox');

var _Checkbox2 = _interopRequireDefault(_Checkbox);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _Dialog = require('@material-ui/core/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _DialogActions = require('@material-ui/core/DialogActions');

var _DialogActions2 = _interopRequireDefault(_DialogActions);

var _DialogContent = require('@material-ui/core/DialogContent');

var _DialogContent2 = _interopRequireDefault(_DialogContent);

var _DialogContentText = require('@material-ui/core/DialogContentText');

var _DialogContentText2 = _interopRequireDefault(_DialogContentText);

var _DialogTitle = require('@material-ui/core/DialogTitle');

var _DialogTitle2 = _interopRequireDefault(_DialogTitle);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _makeStyles = require('@material-ui/core/styles/makeStyles');

var _makeStyles2 = _interopRequireDefault(_makeStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var useStyles = (0, _makeStyles2.default)({});

function ListOptionModal(_ref) {
	var OKButton = _ref.OKButton,
	    CancelButton = _ref.CancelButton,
	    title = _ref.title,
	    _ref$options = _ref.options,
	    options = _ref$options === undefined ? [] : _ref$options,
	    _onChange = _ref.onChange,
	    _onClose = _ref.onClose,
	    _ref$dialogOptions = _ref.dialogOptions,
	    dialogOptions = _ref$dialogOptions === undefined ? {} : _ref$dialogOptions;

	var classes = useStyles();
	return _react2.default.createElement(
		_Dialog2.default,
		(0, _extends3.default)({ open: true, onClose: function onClose() {
				return _onClose();
			} }, dialogOptions),
		_react2.default.createElement(
			_DialogTitle2.default,
			null,
			title || 'Print Options'
		),
		_react2.default.createElement(
			_DialogContent2.default,
			null,
			_react2.default.createElement(
				_DialogContentText2.default,
				{ id: 'alert-dialog-description' },
				_react2.default.createElement(
					_List2.default,
					{ dense: true, className: classes.root },
					options.map(function (_ref2, index) {
						var key = _ref2.key,
						    label = _ref2.label,
						    value = _ref2.value;

						var labelId = 'checkbox-list-label-' + key;
						return _react2.default.createElement(
							_ListItem2.default,
							{ key: key, button: true },
							_react2.default.createElement(_ListItemText2.default, { id: labelId, primary: label }),
							_react2.default.createElement(
								_ListItemSecondaryAction2.default,
								null,
								_react2.default.createElement(_Checkbox2.default, {
									id: labelId,
									color: 'primary',
									edge: 'end',
									checked: value,
									onChange: function onChange(e) {
										return _onChange(e, index);
									},
									inputProps: { 'aria-labelledby': key }
								})
							)
						);
					})
				)
			)
		),
		_react2.default.createElement(
			_DialogActions2.default,
			null,
			_react2.default.createElement(
				_Button2.default,
				{ onClick: function onClick() {
						return _onClose();
					}, color: 'primary' },
				CancelButton || 'Cancel'
			),
			_react2.default.createElement(
				_Button2.default,
				{ onClick: function onClick() {
						return _onClose(true);
					}, color: 'primary', autoFocus: true },
				OKButton || 'OK'
			)
		)
	);
}

ListOptionModal.propTypes = {
	onChange: _propTypes2.default.func,
	onClose: _propTypes2.default.func,
	title: _propTypes2.default.string,
	OKButton: _propTypes2.default.string,
	CancelButton: _propTypes2.default.string,
	options: _propTypes2.default.array,
	dialogOptions: _propTypes2.default.object
};

exports.default = ListOptionModal;