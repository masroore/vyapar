Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactItemsCarousel = require('react-items-carousel');

var _reactItemsCarousel2 = _interopRequireDefault(_reactItemsCarousel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isStateUpdated = false;

var CustomItemsCarousel = function (_React$Component) {
  (0, _inherits3.default)(CustomItemsCarousel, _React$Component);

  function CustomItemsCarousel(props) {
    (0, _classCallCheck3.default)(this, CustomItemsCarousel);

    var _this = (0, _possibleConstructorReturn3.default)(this, (CustomItemsCarousel.__proto__ || (0, _getPrototypeOf2.default)(CustomItemsCarousel)).call(this, props));

    _this.changeActiveItem = function (activeItemIndex) {
      return _this.setState({
        activeItemIndex: activeItemIndex
      });
    };

    _this.state = {
      activeItemIndex: _this.props.activeItemIndex || 0
    };
    return _this;
  }

  (0, _createClass3.default)(CustomItemsCarousel, [{
    key: 'componentWillUpdate',
    value: function componentWillUpdate(props) {
      if (window.isSyncSettingUpdate) {
        this.setState({
          activeItemIndex: props.activeItemIndex
        });
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var activeItemIndex = this.state.activeItemIndex;


      return _react2.default.createElement(
        _reactItemsCarousel2.default
        // Placeholder configurations
        ,
        { enablePlaceholder: true,
          numberOfPlaceholderItems: 5,
          minimumPlaceholderTime: 1000,
          placeholderItem: _react2.default.createElement(
            'div',
            { style: { height: 200, background: '#900' } },
            'Placeholder'
          )

          // Carousel configurations
          , numberOfCards: 4,
          gutter: 12,
          showSlither: true,
          firstAndLastGutter: true,
          freeScrolling: false

          // Active item configurations
          , requestToChangeActive: this.changeActiveItem,
          activeItemIndex: activeItemIndex,
          activePosition: 'center',

          chevronWidth: 24,
          rightChevron: _react2.default.createElement('img', { src: '../inlineSVG/round-chevron_right-24px.svg' }),
          leftChevron: _react2.default.createElement('img', { src: '../inlineSVG/round-chevron_left-24px.svg' }),
          outsideChevron: false
        },
        this.props.children
      );
    }
  }]);
  return CustomItemsCarousel;
}(_react2.default.Component);

exports.default = CustomItemsCarousel;