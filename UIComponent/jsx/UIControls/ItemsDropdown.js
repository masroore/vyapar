Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/core/styles');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _AutoSuggest = require('../AutoSuggest');

var _AutoSuggest2 = _interopRequireDefault(_AutoSuggest);

var _AddCircleOutline = require('@material-ui/icons/AddCircleOutline');

var _AddCircleOutline2 = _interopRequireDefault(_AddCircleOutline);

var _TxnTypeConstant = require('../../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var menuStyles = {
  header: {
    fontSize: 12,
    fontFamily: 'RobotoMedium',
    height: 30,
    padding: 10,
    width: 125,
    whiteSpace: 'nowrap',
    marginRight: 5,
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },
  menuItems: {
    fontSize: 12,
    color: '#222A3F',
    textAlign: 'right'
  },
  headerCommon: {
    color: 'rgba(34, 42, 63, 0.5)',
    textAlign: 'right'
  },
  addItem: {
    color: '#1789FC',
    cursor: 'pointer',
    position: 'relative',
    left: 4
  },
  addIcon: {
    color: '#1789FC',
    cursor: 'pointer',
    fontSize: 16,
    position: 'relative',
    top: -2
  },
  container: {
    position: 'absolute',
    maxHeight: 200,
    overflowY: 'auto',
    overflowX: 'hidden',
    background: '#fff',
    zIndex: 9999,
    border: '1px solid #ddd',
    borderTop: 0
  },
  sectionContainer: {
    overflow: 'auto',
    background: '#fff',
    zIndex: 99999,
    position: 'relative',
    border: '1px solid #ddd',
    borderRight: 0
  },
  selected: {
    background: '#EFF4FD',
    border: '1px solid #BBE3FF',
    cursor: 'pointer',
    fontFamily: 'Roboto'
  },
  notSelected: {
    border: '1px solid transparent',
    fontFamily: 'Roboto'
  },
  nameWidth: {
    width: 400,
    textAlign: 'left',
    display: 'flex'
  },
  partyBalance: {
    marginRight: 5,
    position: 'relative',
    top: -3
  },
  dropdownContainer: {},
  balance: {
    position: 'absolute',
    fontSize: 11,
    fontFamily: 'robotoMedium',
    left: 15
  },
  payable: {
    color: '#FE6B6B'
  },
  receivable: {
    color: '#21C2A7'
  },
  hide: {
    display: 'none'
  },
  itemLabel: {
    textTransform: 'none'
  },
  itemCode: {
    fontSize: 9,
    color: '#80808B',
    marginLeft: 5
  }
};

var getSuggestionValue = function getSuggestionValue(suggestion) {
  return suggestion.label;
};

function getSectionSuggestions(section) {
  return section.items;
}

var AutoSuggest = function (_React$Component) {
  (0, _inherits3.default)(AutoSuggest, _React$Component);

  function AutoSuggest(props) {
    (0, _classCallCheck3.default)(this, AutoSuggest);

    var _this = (0, _possibleConstructorReturn3.default)(this, (AutoSuggest.__proto__ || (0, _getPrototypeOf2.default)(AutoSuggest)).call(this, props));

    _initialiseProps.call(_this);

    _this.state = {
      value: _this.getValueFromProps(props.value),
      suggestions: []
    };
    var classes = props.classes,
        showFirstTimeAllOptions = props.showFirstTimeAllOptions;

    _this.showFirstTimeAllOptions = showFirstTimeAllOptions;
    _this.theme = {
      containerOpen: 'react-autosuggest__container--open',
      input: classes.nameWidth,
      inputOpen: 'react-autosuggest__input--open',
      inputFocused: 'react-autosuggest__input--focused',
      suggestionsContainer: 'react-autosuggest__suggestions-container',
      suggestionsContainerOpen: classes.container,
      suggestionsList: '',
      suggestion: classes.notSelected,
      suggestionFirst: 'react-autosuggest__suggestion--first',
      suggestionHighlighted: classes.selected,
      sectionContainer: 'react-autosuggest__section-container',
      sectionContainerFirst: 'react-autosuggest__section-container--first',
      sectionTitle: 'react-autosuggest__section-title'
    };
    _this._inputString = '';
    if (props.getClearDropdownFunction) {
      props.getClearDropdownFunction(_this.clearDropdown);
    }
    if (props.updateItem) {
      props.updateItem(_this.updateItem);
    }
    return _this;
  }

  (0, _createClass3.default)(AutoSuggest, [{
    key: 'getValueFromProps',
    value: function getValueFromProps(value) {
      if (value && value.label) {
        return value.label;
      }
      return '';
    }

    // Autosuggest will call this function every time you need to update suggestions.
    // You already implemented this logic above, so just use it.


    // Autosuggest will call this function every time you need to clear suggestions.

  }, {
    key: 'render',
    value: function render() {
      var _state = this.state,
          value = _state.value,
          suggestions = _state.suggestions;
      var _props = this.props,
          className = _props.className,
          classes = _props.classes,
          isFTU = _props.isFTU,
          isBarcodeEnabled = _props.isBarcodeEnabled,
          index = _props.index,
          isQuickEntry = _props.isQuickEntry,
          items = _props.items;

      // Autosuggest will pass through all these props to the input.

      var inputProps = {
        value: value || '',
        onChange: this.onChange,
        className: (0, _classnames2.default)(className, 'autoSuggestInput'),
        autoFocus: isQuickEntry,
        onFocus: this.onFocus,
        onBlur: this.onBlur,
        id: 'salePurchaseItemDropdown' + index
      };
      if (isBarcodeEnabled) {
        inputProps.onKeyPress = this.onKeyPress;
      }
      var shouldRenderSuggession = false;
      if (isFTU) {
        if (items.length > 0) {
          shouldRenderSuggession = true;
        }
      } else {
        shouldRenderSuggession = true;
      }

      return _react2.default.createElement(
        'div',
        { className: classes.dropdownContainer + ' auto-suggest-container' },
        _react2.default.createElement(_AutoSuggest2.default, {
          suggestions: suggestions,
          onSuggestionsFetchRequested: this.onSuggestionsFetchRequested,
          onSuggestionsClearRequested: this.onSuggestionsClearRequested,
          renderSuggestionsContainer: this.renderSuggestionsContainer,
          onSuggestionSelected: this.onSuggestionSelected,
          getSuggestionValue: getSuggestionValue,
          renderSuggestion: this.renderSuggestion,
          renderSectionTitle: this.renderSectionTitle,
          getSectionSuggestions: getSectionSuggestions,
          inputProps: inputProps,
          multiSection: true,
          shouldRenderSuggestions: function shouldRenderSuggestions() {
            return shouldRenderSuggession;
          },
          focusInputOnSuggestionClick: true
          // alwaysRenderSuggestions={true}
          , theme: this.theme
        })
      );
    }
  }]);
  return AutoSuggest;
}(_react2.default.Component);

var _initialiseProps = function _initialiseProps() {
  var _this2 = this;

  this.updateItem = function (value) {
    _this2.setState({ value: value });
  };

  this.getSuggestions = function (value) {
    var _props2 = _this2.props,
        items = _props2.items,
        txnType = _props2.txnType;


    var inputValue = value.trim().toLowerCase();
    var inputLength = inputValue.length;
    var filtered = [];
    if (txnType !== _TxnTypeConstant2.default.TXN_TYPE_EXPENSE && txnType !== _TxnTypeConstant2.default.TXN_TYPE_OTHER_INCOME) {
      items = items.filter(function (item) {
        if (item.value == 0) {
          return true;
        }
        if (item.item && item.item.getIsItemActive() == 1) {
          return true;
        }
        return false;
      });
    }
    if (inputLength === 0) {
      filtered = items;
    } else {
      filtered = items.filter(function (party) {
        return party.searchText.toLowerCase().indexOf(inputValue) !== -1;
      });
    }
    if (_this2.showFirstTimeAllOptions) {
      _this2.showFirstTimeAllOptions = false;
      filtered = items;
    }

    return [{
      title: '',
      items: filtered.length ? filtered.slice(0, 100) : [{
        value: 0,
        label: '',
        searchText: '',
        purchasePrice: 0,
        salePrice: 0,
        stockQty: { __html: '<span>0</span>' },
        item: null,
        visible: false
      }]
    }];
  };

  this.onChange = function (event, party) {
    var newValue = party.newValue;

    _this2.setState({
      value: newValue
    });
  };

  this.clearDropdown = function () {
    _this2.setState({ value: '' });
  };

  this.onBlur = function (event) {
    event.persist();
    if (!event || !event.target) {
      return false;
    }

    var value = event.target.value.trim();
    var lowercaseValue = value.toLowerCase();
    var _props3 = _this2.props,
        items = _props3.items,
        onChange = _props3.onChange,
        item = _props3.value;


    if ((!item || !item.label) && value == '' || (item && item.label && item.label.toLowerCase()) === lowercaseValue) {
      return false;
    }

    var selected = void 0;
    if (value) {
      selected = items.find(function (party) {
        return party.label.toLowerCase() === lowercaseValue;
      });
    }
    if (!selected) {
      if (!value) {
        selected = {};
      } else {
        selected = {
          value: 0,
          label: value,
          purchasePrice: 0,
          salePrice: 0,
          stockQty: { __html: '<span>0</span>' },
          item: null
        };
      }
    }
    onChange && onChange(selected);
  };

  this.onKeyPress = function (e) {
    if (_this2._timeoutHandler) {
      clearTimeout(_this2._timeoutHandler);
    }
    _this2._inputString += String.fromCharCode(e.which);
    // CHECKS FOR VALID CHARACTERS WHILE SCANNING
    var keyCode = e.which;
    if (keyCode === 13) {
      e.stopPropagation();
      e.preventDefault();
    }
    var value = e.target.value.trim();
    _this2._timeoutHandler = setTimeout(function () {
      if (keyCode === 13) {
        var barcode = _this2._inputString.trim();
        var _props4 = _this2.props,
            isBarcodeEnabled = _props4.isBarcodeEnabled,
            items = _props4.items,
            onChange = _props4.onChange;

        if (barcode.length && barcode === value && isBarcodeEnabled) {
          var found = items.find(function (item) {
            if (item.item) {
              return item.item.getItemCode() === barcode;
            }
            return false;
          });
          if (found) {
            _this2.setState({ value: found.label });
            onChange && onChange(found, true);
          }
        }
      }
      _this2._inputString = '';
    }, 50);
  };

  this.onSuggestionSelected = function (event, _ref) {
    var suggestion = _ref.suggestion;

    event.preventDefault();
    var onChange = _this2.props.onChange;

    console.log(suggestion);
    onChange && onChange(suggestion);
  };

  this.onSuggestionsFetchRequested = function (_ref2) {
    var value = _ref2.value;

    _this2.setState({
      suggestions: _this2.getSuggestions(value)
    });
  };

  this.onSuggestionsClearRequested = function () {
    _this2.setState({
      suggestions: []
    });
  };

  this.renderSuggestion = function (props) {
    var stockQty = props.stockQty,
        label = props.label,
        itemCode = props.itemCode,
        purchasePrice = props.purchasePrice,
        salePrice = props.salePrice,
        visible = props.visible,
        item = props.item;
    var _props5 = _this2.props,
        txnType = _props5.txnType,
        classes = _props5.classes,
        showItemPurchasePrice = _props5.showItemPurchasePrice;

    var showAllColumns = true;
    if (txnType === _TxnTypeConstant2.default.TXN_TYPE_EXPENSE || txnType === _TxnTypeConstant2.default.TXN_TYPE_OTHER_INCOME) {
      showAllColumns = false;
    }
    return _react2.default.createElement(
      'div',
      { className: visible === false ? classes.hide : '' },
      _react2.default.createElement(
        'div',
        {
          className: (0, _classnames2.default)('render-suggestions d-flex justify-content-between')
        },
        _react2.default.createElement(
          'div',
          {
            title: label,
            className: (0, _classnames2.default)(classes.header, classes.menuItems, classes.nameWidth, classes.itemLabel)
          },
          label,
          ' ',
          itemCode && _react2.default.createElement(
            'span',
            { className: classes.itemCode },
            '(',
            itemCode,
            ')'
          )
        ),
        showAllColumns && _react2.default.createElement(
          _react2.default.Fragment,
          null,
          _react2.default.createElement(
            'div',
            { className: (0, _classnames2.default)(classes.header, classes.menuItems) },
            salePrice
          ),
          showItemPurchasePrice && _react2.default.createElement(
            'div',
            { className: (0, _classnames2.default)(classes.header, classes.menuItems) },
            item && item.isItemService() ? '' : purchasePrice
          ),
          _react2.default.createElement('div', {
            className: (0, _classnames2.default)(classes.header, classes.menuItems),
            dangerouslySetInnerHTML: item && item.isItemService() ? { __html: '' } : stockQty
          })
        )
      )
    );
  };

  this.createItem = function (e) {
    e.persist();
    var createNewItem = _this2.props.createNewItem;
    var value = _this2.state.value;

    createNewItem(value);
  };

  this.renderSectionTitle = function () {
    var _props6 = _this2.props,
        classes = _props6.classes,
        showItemPurchasePrice = _props6.showItemPurchasePrice,
        txnType = _props6.txnType;

    var showAllColumns = true;
    if (txnType === _TxnTypeConstant2.default.TXN_TYPE_EXPENSE || txnType === _TxnTypeConstant2.default.TXN_TYPE_OTHER_INCOME) {
      showAllColumns = false;
    }
    return _react2.default.createElement(
      'div',
      {
        className: (0, _classnames2.default)(classes.sectionContainer, 'render-section-title d-flex justify-content-between')
      },
      showAllColumns && _react2.default.createElement(
        _react2.default.Fragment,
        null,
        _react2.default.createElement(
          'div',
          {
            onClick: _this2.createItem,
            className: (0, _classnames2.default)(classes.header, classes.nameWidth)
          },
          _react2.default.createElement(_AddCircleOutline2.default, { className: classes.addIcon, color: 'secondary' }),
          _react2.default.createElement(
            'span',
            { className: classes.addItem },
            'Add Item'
          )
        ),
        _react2.default.createElement(
          'div',
          {
            className: (0, _classnames2.default)(classes.header, classes.headerCommon, 'dropdownHeader')
          },
          'Sale Price'
        ),
        showItemPurchasePrice && _react2.default.createElement(
          'div',
          {
            className: (0, _classnames2.default)(classes.header, classes.headerCommon, 'dropdownHeader')
          },
          'Purchase Price'
        ),
        _react2.default.createElement(
          'div',
          {
            className: (0, _classnames2.default)(classes.header, classes.headerCommon, 'dropdownHeader')
          },
          'Stock'
        )
      )
    );
  };

  this.renderSuggestionsContainer = function (_ref3) {
    var containerProps = _ref3.containerProps,
        children = _ref3.children,
        query = _ref3.query;

    return _react2.default.createElement(
      'div',
      containerProps,
      children
    );
  };

  this.onFocus = function (e) {
    var target = e.target;

    if (!target || !target.nextSibling) {
      return false;
    }
    var next = target.nextSibling;

    var _target$getBoundingCl = target.getBoundingClientRect(),
        height = _target$getBoundingCl.height,
        bottom = _target$getBoundingCl.bottom,
        left = _target$getBoundingCl.left;

    next.style.top = bottom - 20 + 'px';
    next.style.left = left + 'px';
  };
};

AutoSuggest.propTypes = {
  classes: _propTypes2.default.object,
  createNewItem: _propTypes2.default.func,
  addItemLabel: _propTypes2.default.string,
  label: _propTypes2.default.string,
  txnType: _propTypes2.default.number,
  index: _propTypes2.default.number,
  value: _propTypes2.default.object,
  onBlur: _propTypes2.default.func,
  updateItem: _propTypes2.default.func,
  onNewBlur: _propTypes2.default.func,
  items: _propTypes2.default.array,
  onChange: _propTypes2.default.func,
  className: _propTypes2.default.string,
  isDisabled: _propTypes2.default.bool,
  isEdit: _propTypes2.default.bool,
  showFirstTimeAllOptions: _propTypes2.default.bool,
  showPartiesClicked: _propTypes2.default.bool,
  isBarcodeEnabled: _propTypes2.default.bool,
  showItemPurchasePrice: _propTypes2.default.bool,
  isFTU: _propTypes2.default.bool,
  isQuickEntry: _propTypes2.default.bool,
  getClearDropdownFunction: _propTypes2.default.func
};

exports.default = (0, _styles.withStyles)(menuStyles)(AutoSuggest);