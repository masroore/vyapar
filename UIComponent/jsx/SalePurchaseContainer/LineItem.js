Object.defineProperty(exports, "__esModule", {
  value: true
});

var _assign = require('babel-runtime/core-js/object/assign');

var _assign2 = _interopRequireDefault(_assign);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _extends4 = require('babel-runtime/helpers/extends');

var _extends5 = _interopRequireDefault(_extends4);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/core/styles');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _Delete = require('@material-ui/icons/Delete');

var _Delete2 = _interopRequireDefault(_Delete);

var _JqueryDatePicker = require('../UIControls/JqueryDatePicker');

var _JqueryDatePicker2 = _interopRequireDefault(_JqueryDatePicker);

var _QuickEntryIcon = require('../icons/QuickEntryIcon');

var _QuickEntryIcon2 = _interopRequireDefault(_QuickEntryIcon);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _Check = require('@material-ui/icons/Check');

var _Check2 = _interopRequireDefault(_Check);

var _SelectBatchesModal = require('../SelectBatchesModal');

var _SelectBatchesModal2 = _interopRequireDefault(_SelectBatchesModal);

var _SettingCache = require('../../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

var _DataLoader = require('../../../DBManager/DataLoader');

var _DataLoader2 = _interopRequireDefault(_DataLoader);

var _ItemsDropdown = require('../UIControls/ItemsDropdown');

var _ItemsDropdown2 = _interopRequireDefault(_ItemsDropdown);

var _TxnTypeConstant = require('../../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _ToastHelper = require('../../../UIControllers/ToastHelper');

var ToastHelper = _interopRequireWildcard(_ToastHelper);

var _MyDouble = require('../../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _throttleDebounce = require('throttle-debounce');

var _SalePurchaseHelpers = require('./SalePurchaseHelpers');

var _DecimalInputFilter = require('../../../Utilities/DecimalInputFilter');

var DecimalInputFilter = _interopRequireWildcard(_DecimalInputFilter);

var _StringConstants = require('../../../Constants/StringConstants');

var _StringConstants2 = _interopRequireDefault(_StringConstants);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var settingCache = new _SettingCache2.default();
var dataLoader = new _DataLoader2.default();

var styles = function styles() {
  return {
    lineItemContainer: {
      borderBottom: '1px solid #DDDDDD',
      '&:nth-child(even)': {
        background: '#fff'
      },
      '&:last-child': {
        background: '#f8f8f9'
        // '& td': {
        // 	paddingLeft: 5,
        // 	paddingRight: 5,
        // }
      } },
    lineItem: {
      height: 40
    },
    lineItemColumn: {
      borderRight: '1px solid #DDDDDD !important',
      fontSize: 12,
      color: 'rgba(34, 42, 63, 0.5)',
      alignItems: 'center',
      fontFamily: 'RobotoMedium',
      textTransform: 'uppercase',
      paddingLeft: 5,
      paddingRight: 5,
      '&:last-child': {
        borderRight: 0
      },
      '& input': {
        padding: '0 4px',
        height: 36,
        overflow: 'hidden',
        textOverflow: 'ellipsis'
      }
    },
    deleteIcon: {
      position: 'absolute',
      top: 4,
      left: 4,
      cursor: 'pointer'
    },
    lineItemSrNo: {
      maxWidth: 49,
      minWidth: 18,
      textAlign: 'center',
      position: 'relative'
    },
    description: {
      '& input': {
        width: '100%'
      }
    },
    HSNCode: {
      textAlign: 'right',
      paddingRight: 8,
      whiteSpace: 'nowrap',
      maxWidth: 92,
      textOverflow: 'ellipsis',
      overflow: 'hidden'
    },
    code: {
      maxWidth: 135,
      textOverflow: 'ellipsis',
      overflow: 'hidden',
      paddingLeft: 8,
      whiteSpace: 'nowrap'
    },
    count: {
      '& input': {
        minWidth: 30,
        textAlign: 'right',
        overflow: 'hidden',
        textOverflow: 'ellipsis'
      }
    },
    slNo: {
      '& input': {
        minWidth: 40,
        width: '17ch',
        overflow: 'hidden',
        textOverflow: 'ellipsis'
      }
    },
    batchNo: {
      '& input': {
        minWidth: 40,
        width: '17ch',
        overflow: 'hidden',
        textOverflow: 'ellipsis'
      }
    },
    expDate: {
      '& input': {
        minWidth: 30
      }
    },
    mfgDate: {
      '& input': {
        minWidth: 30
      }
    },
    mrp: {
      '& input': {
        minWidth: 30,
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        textAlign: 'right'
      }
    },
    size: {
      '& input': {
        minWidth: 30,
        overflow: 'hidden',
        textOverflow: 'ellipsis'
      }
    },
    qty: {
      textAlign: 'right',
      '& input': {
        minWidth: 30,
        textAlign: 'right',
        overflow: 'hidden',
        textOverflow: 'ellipsis'
      }
    },
    freeQty: {
      textAlign: 'right',
      '& input': {
        minWidth: 30,
        textAlign: 'right',
        overflow: 'hidden',
        textOverflow: 'ellipsis'
      }
    },
    priceUnit: {
      textAlign: 'right',
      '& input': {
        minWidth: 60,
        textAlign: 'right',
        overflow: 'hidden',
        textOverflow: 'ellipsis'
      }
    },
    cess: {
      '& input': {
        minWidth: 30,
        textAlign: 'right'
      }
    },
    amount: {
      '& input': {
        textAlign: 'right',
        minWidth: 60
      }
    },
    columnHeader: {
      height: 18,
      paddingTop: 3,
      textAlign: 'center'
    },
    lineItemSubheader: {
      width: '100%',
      display: 'flex',
      '& input': {
        margin: 'auto 0',
        textAlign: 'right'
      }
    },
    left: {
      width: '55%',
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
      borderRight: '1px solid #ddd;',
      paddingRight: 5
    },
    right: {
      width: '45%',
      height: '100%',
      paddingLeft: 5,
      display: 'flex'
    },
    discountLeft: {
      width: '40%'
    },
    discountRight: {
      width: '60%'
    },
    discountAmount: {
      minWidth: 50,
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    },
    taxPercent: {
      minWidth: '50px !important'
    },
    ITCDropdownOptions: {
      fontSize: 10,
      height: '20px !important'
    },
    taxAmount: {
      minWidth: '50px !important',
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    },
    dropdownTextField: {
      paddingLeft: 10,
      width: '100%'
    },
    lineItemTextField: {
      background: 'transparent',
      outline: 0,
      border: 0,
      height: 36,
      width: '100%',
      '&:focus': {
        outline: '1px solid #1789FC !important'
      }
    },

    positionRelative: {
      position: 'relative'
    },
    lastSalePriceContainer: {
      position: 'fixed',
      background: '#F5F7FA',
      padding: '12px 20px',
      boxShadow: '0 6px 12px rgba(0, 0, 0, 0.16)',
      zIndex: 999,
      textAlign: 'left'
    },
    lastSalePriceHeader: {
      fontSize: 10,
      color: 'rgba(34, 42, 63, 0.5)',
      fontFamily: 'RobotoMedium',
      paddingTop: 7
    },
    lastSalePriceTitle: {
      fontSize: 12,
      color: '#1C2A3D'
    },
    dateColumn: {
      width: 100
    },
    qtyColumn: {
      width: 60,
      textAlign: 'right'
    },
    priceColumn: {
      width: 60,
      textAlign: 'right',
      marginLeft: 30
    },
    lastSalePriceRow: {
      fontSize: 12,
      color: '#222A3F',
      paddingTop: 14
    },
    quickEntryTextField: {
      color: '#fff',
      background: '#152F42 !important',
      outline: 0,
      border: 0,
      height: 36,
      '&:focus': {
        outline: '1px solid #1789FC !important'
      }
    },
    quickEntryContainer: {
      background: '#152F42 !important'
    },
    quickEntryCode: {
      color: '#fff'
    },
    quickEntryHSNCode: {
      color: '#fff'
    },
    quickEntryIconContainer: {
      margin: 'auto',
      width: 16,
      height: 16
    },
    quickEntryIcon: {
      color: '#4bb3fd'
    },
    quickEntrySubmitButton: {
      boxShadow: 'none',
      textTransform: 'none',
      fontSize: 16,
      minWidth: 32,
      padding: '6px 12px',
      border: '1px solid',
      lineHeight: 1.5,
      background: '#4bb3fd',
      height: 32,
      width: 32,
      borderRadius: 8,
      marginRight: 5,
      float: 'right',
      top: 2,
      '&:hover': {
        background: '#4bb3fd'
      }
    },
    addIconContainer: {
      width: '1%',
      maxWidth: 20
    },
    quickEntryAmountText: {
      float: 'left',
      width: '55% !important'
    },
    quickEntrySubmitButtonLabel: {
      position: 'relative',
      top: -3
    },
    colorWhite: {
      color: '#fff'
    },
    '@global': {
      '.focused-row': {
        boxShadow: '0px 4px 16px 0px rgba(0,0,0,0.3)',
        background: '#F5F6F9 0% 0% no-repeat padding-box',
        '& input, select': {
          background: '#FFFFFF 0% 0% no-repeat padding-box',
          border: '1px solid #C4C4C4',
          borderRadius: 4
        },
        '& td': {
          padding: 5
        }
      }
    },
    categoryDropdown: {
      width: '98%'
    }
  };
};

var LineItem = function (_Component) {
  (0, _inherits3.default)(LineItem, _Component);

  function LineItem(props) {
    (0, _classCallCheck3.default)(this, LineItem);

    var _this = (0, _possibleConstructorReturn3.default)(this, (LineItem.__proto__ || (0, _getPrototypeOf2.default)(LineItem)).call(this, props));

    _initialiseProps.call(_this);

    var unitDropdown = [];
    var taxList = [];
    if (props.lineItem && props.lineItem.item) {
      var item = props.lineItem.item;
      var itemLogic = item.item ? item.item : null;

      if (props.lineItem && props.lineItem.unitObj) {
        unitDropdown = props.lineItem.unitObj;
      } else {
        unitDropdown = (0, _SalePurchaseHelpers.getUnitDropdownOptions)(itemLogic, props.lineItem.unit);
      }

      var _this$props = _this.props,
          txnType = _this$props.txnType,
          party = _this$props.party,
          firmLogic = _this$props.firmLogic,
          placeOfSupply = _this$props.placeOfSupply,
          txnId = _this$props.txnId,
          lineItem = _this$props.lineItem;

      var partyLogic = party && party.partyLogic ? party.partyLogic : null;
      taxList = (0, _SalePurchaseHelpers.getLineItemTaxDropdownList)({
        txnType: txnType,
        txnId: txnId,
        firmLogic: firmLogic,
        placeOfSupply: placeOfSupply,
        partyLogic: partyLogic,
        itemId: item.value,
        currentTaxCodeId: [_MyDouble2.default.convertStringToDouble(lineItem.taxPercent || lineItem.oldTaxId) || 0]
      });
    }
    var quickEntryLineItem = (0, _extends5.default)({}, _this.getDefaultLineItem(), {
      unitObj: unitDropdown,
      showITCEligible: (0, _SalePurchaseHelpers.isITCEligible)(props.lineItem.taxPercent, props.txnType),
      ITCValue: props.lineItem.ITCValue
    });
    _this.state = {
      toggleDeleteButton: false,
      toggleLastPrice: false,
      anchorEl: null,
      quickEntryLineItem: quickEntryLineItem,
      openBatchesModal: false,
      unitDropdown: unitDropdown,
      taxList: taxList,
      itemCategoryId: 0,
      isFocused: false,
      filteredItems: props.items
    };
    _this.itemStockTrackingList = [];
    _this.lastFiveTransaction = [];
    _this.ITCDropdownOptions = (0, _SalePurchaseHelpers.getITCDropdownOptions)();
    _this.showDeleteButton = _this.showDeleteButton.bind(_this);
    _this.hideDeleteButton = _this.hideDeleteButton.bind(_this);
    _this.toggleLastPrice = _this.toggleLastPrice.bind(_this);
    _this.onSubmit = _this.onSubmit.bind(_this);
    _this.changeLineItem = (0, _throttleDebounce.debounce)(300, false, _this.changeLineItem.bind(_this));
    _this.onBatchesModalClose = _this.onBatchesModalClose.bind(_this);
    _this.onChange = _this.onChange.bind(_this);
    _this.createNewIncomeOrExpense = _this.createNewIncomeOrExpense.bind(_this);

    _this.changeItem = _this.changeItem.bind(_this);
    _this.changeExpDate = _this.changeField('expDate');
    _this.changeMfgDate = _this.changeField('mfgDate');
    _this.createNewItem = _this.createNewItem.bind(_this);
    _this.itemDropdownRef = _react2.default.createRef();
    // this.changeLineItem();
    return _this;
  }

  (0, _createClass3.default)(LineItem, [{
    key: 'filterCategories',
    value: function filterCategories(itemCategoryId, currentLineItem) {
      var _props = this.props,
          items = _props.items,
          showItemCategory = _props.showItemCategory;

      if (itemCategoryId == 0 || !showItemCategory) {
        return items;
      }
      var currentItemId = currentLineItem && currentLineItem.item && currentLineItem.item.value;

      var filteredItems = items.filter(function (item) {
        var id = item.item.getItemCategoryId();
        return id == itemCategoryId || currentItemId && currentItemId == item.item.getItemId();
      });
      return filteredItems;
    }
  }, {
    key: 'changeField',
    value: function changeField(key) {
      var _this2 = this;

      return function (event) {
        var quickEntryLineItem = _this2.state.quickEntryLineItem;
        var isQuickEntry = _this2.props.isQuickEntry;

        var value = event && event.target ? event.target.value : event;

        var lineItem = (0, _extends5.default)({}, quickEntryLineItem, (0, _defineProperty3.default)({}, key, value));
        _this2.setState({
          quickEntryLineItem: lineItem
        }, function () {
          if (!isQuickEntry) {
            _this2.changeLineItem();
          }
        });
      };
    }
  }, {
    key: 'changeLineItem',
    value: function changeLineItem() {
      var quickEntryLineItem = this.state.quickEntryLineItem;
      var _props2 = this.props,
          index = _props2.index,
          changeLineItem = _props2.changeLineItem;

      changeLineItem(index, quickEntryLineItem);
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      if (this.nextElementFocusTimeout) {
        clearTimeout(this.nextElementFocusTimeout);
      }
    }
  }, {
    key: 'onChange',
    value: function onChange(event) {
      var _this3 = this;

      var target = event.target;

      var key = target.getAttribute('data-key');
      var value = event && event.target ? event.target.value : event;
      var _state = this.state,
          quickEntryLineItem = _state.quickEntryLineItem,
          unitDropdown = _state.unitDropdown;
      var _props3 = this.props,
          taxInclusive = _props3.taxInclusive,
          isQuickEntry = _props3.isQuickEntry,
          txnType = _props3.txnType;

      if (!key) {
        return false;
      }
      if (key === 'category') {
        this.setState({
          filteredItems: this.filterCategories(value, quickEntryLineItem),
          itemCategoryId: value
        });
        return false;
      }
      var lineItem = (0, _assign2.default)((0, _extends5.default)({}, quickEntryLineItem, (0, _defineProperty3.default)({}, key, value)));
      var newLineItem = lineItem;
      var unitInfo = void 0;
      switch (key) {
        case 'qty':
          lineItem.qty = DecimalInputFilter.inputTextFilterForQuantity(target);
          newLineItem = (0, _SalePurchaseHelpers.updateLineItemFields)(lineItem, txnType, taxInclusive);
          break;
        case 'priceUnit':
          lineItem.priceUnit = DecimalInputFilter.inputTextFilterForAmount(target);
          newLineItem = (0, _SalePurchaseHelpers.updateLineItemFields)(lineItem, txnType, taxInclusive);
          break;
        case 'unit':
          unitInfo = unitDropdown.find(function (item) {
            return item.value == lineItem.unit;
          });
          var unitLineItemChanges = (0, _SalePurchaseHelpers.changeUnit)(lineItem, unitInfo);
          lineItem.priceUnit = unitLineItemChanges.itemPrice;
          lineItem.cess = unitLineItemChanges.adCess;
          lineItem = (0, _SalePurchaseHelpers.changeDiscountFields)(lineItem, false, txnType, taxInclusive);
          lineItem.amount = _MyDouble2.default.getBalanceAmountWithDecimal(lineItem.priceUnit * lineItem.qty);
          newLineItem = (0, _SalePurchaseHelpers.changeTaxFields)(lineItem, txnType, taxInclusive);
          break;
        case 'discountPercent':
          var discountPercent = DecimalInputFilter.inputTextFilterForPercentage(target);
          if (_MyDouble2.default.convertStringToDouble(discountPercent) > 100) {
            ToastHelper.error('Discount Percentage can not be more than 100 percent');
            return false;
          }
          lineItem.discountPercent = discountPercent;
          newLineItem = (0, _SalePurchaseHelpers.changeDiscountFields)(lineItem, false, txnType, taxInclusive);
          break;
        case 'discountAmount':
          lineItem.discountAmount = DecimalInputFilter.inputTextFilterForAmount(target);
          newLineItem = (0, _SalePurchaseHelpers.changeDiscountFields)(lineItem, true, txnType, taxInclusive);
          if (_MyDouble2.default.convertStringToDouble(newLineItem.discountPercent) > 100) {
            ToastHelper.error('Discount Percentage can not be more than 100');
            return false;
          }
          break;
        case 'taxPercent':
          newLineItem = (0, _SalePurchaseHelpers.changeTaxFields)(lineItem, txnType, taxInclusive);
          break;
        case 'cess':
          lineItem.cess = DecimalInputFilter.inputTextFilterForAmount(target);
          newLineItem = (0, _SalePurchaseHelpers.changeCESS)(lineItem, taxInclusive);
          break;
        case 'amount':
          var amount = DecimalInputFilter.inputTextFilterForAmount(target);
          lineItem.amount = amount;
          newLineItem = (0, _SalePurchaseHelpers.changeAmount)(lineItem, taxInclusive);
          newLineItem.amount = amount;
          break;
        case 'count':
          newLineItem.count = DecimalInputFilter.inputTextFilterForQuantity(target);
          break;
        case 'description':
          newLineItem.description = DecimalInputFilter.inputTextFilterForLength(target, 1024, true);
          break;
        case 'slNo':
          newLineItem.slNo = DecimalInputFilter.inputTextFilterForLength(target, 30, true);
          break;
        case 'batchNo':
          newLineItem.batchNo = DecimalInputFilter.inputTextFilterForLength(target, 30, true);
          break;
        case 'mrp':
          newLineItem.mrp = DecimalInputFilter.inputTextFilterForAmount(target);
          break;
        case 'size':
          newLineItem.size = DecimalInputFilter.inputTextFilterForLength(target, 100, true);
          break;
        case 'freeQty':
          newLineItem.freeQty = DecimalInputFilter.inputTextFilterForQuantity(target);
          break;
        default:
          newLineItem[key] = value;
          break;
      }
      this.setState({
        quickEntryLineItem: newLineItem
      }, function () {
        if (!isQuickEntry) {
          _this3.changeLineItem();
        }
      });
    }
  }, {
    key: 'changeItem',
    value: function changeItem(item) {
      var _this4 = this;

      var withBarcode = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      if (withBarcode) {
        this.withBarcode = true;
      }
      var quickEntryLineItem = this.state.quickEntryLineItem;
      var _props4 = this.props,
          txnType = _props4.txnType,
          party = _props4.party,
          firmLogic = _props4.firmLogic,
          placeOfSupply = _props4.placeOfSupply,
          txnId = _props4.txnId,
          isQuickEntry = _props4.isQuickEntry,
          showBatchDialog = _props4.showBatchDialog,
          taxInclusive = _props4.taxInclusive,
          lineItemColumnsVisibility = _props4.lineItemColumnsVisibility;

      var partyId = party && party.value ? party.value : '';
      var partyLogic = party && party.partyLogic ? party.partyLogic : null;
      var itemLogic = item.item;

      var lineItem = (0, _SalePurchaseHelpers.changeLineItem)({
        defaultLineItem: quickEntryLineItem,
        partyLogic: partyLogic,
        itemLogic: itemLogic,
        firmLogic: firmLogic,
        txnType: txnType,
        txnId: txnId,
        placeOfSupply: placeOfSupply,
        taxInclusive: taxInclusive
      });

      if (!lineItemColumnsVisibility.unit) {
        lineItem.unit = 0;
      }

      var oldTaxId = itemLogic && itemLogic.getItemTaxId && itemLogic.getItemTaxId();
      lineItem.oldTaxId = _MyDouble2.default.convertStringToDouble(oldTaxId || 0);

      var taxList = (0, _SalePurchaseHelpers.getLineItemTaxDropdownList)({
        txnType: txnType,
        txnId: txnId,
        firmLogic: firmLogic,
        placeOfSupply: placeOfSupply,
        partyLogic: partyLogic,
        itemId: item.value,
        currentTaxCodeId: [lineItem.taxPercent]
      });

      var openBatchesModal = false;
      if (showBatchDialog) {
        /* batch start */
        this.itemStockTrackingList = (0, _SalePurchaseHelpers.getItemStockTrackingList)({
          itemId: itemLogic && itemLogic.getItemId() || 0,
          itemCode: null,
          nameId: partyId,
          txnType: txnType
        });

        if (this.itemStockTrackingList.length > 1) {
          // show dialog
          openBatchesModal = true;
        } else if (this.itemStockTrackingList.length === 1) {
          var batchInfo = this.itemStockTrackingList[0];
          lineItem.istId = batchInfo.getIstId();
          lineItem.batchNo = batchInfo.getIstBatchNumber() ? String(batchInfo.getIstBatchNumber()) : '';
          var mfgDate = batchInfo.getIstManufacturingDate() ? batchInfo.getIstManufacturingDate() : '';
          if (mfgDate && mfgDate instanceof Date && mfgDate != 'Invalid Date') {
            var format = settingCache.getManufacturingDateFormat() == _StringConstants2.default.monthYear ? 'm/y' : 'd/m/y';
            mfgDate = MyDate.getDate(format, mfgDate);
          }
          lineItem.mfgDate = mfgDate;
          var expDate = batchInfo.getIstExpiryDate() ? batchInfo.getIstExpiryDate() : '';
          if (expDate && expDate instanceof Date && expDate != 'Invalid Date') {
            var _format = settingCache.getExpiryDateFormat() == _StringConstants2.default.monthYear ? 'm/y' : 'd/m/y';
            expDate = MyDate.getDate(_format, expDate);
          }
          lineItem.expDate = expDate;
          lineItem.slNo = batchInfo.getIstSerialNumber() ? String(batchInfo.getIstSerialNumber()) : '';
          lineItem.size = batchInfo.getIstSize() ? String(batchInfo.getIstSize()) : '';
          lineItem.mrp = batchInfo.getIstMrp() ? String(batchInfo.getIstMrp()) : '';
        }
        /* batch end */
      }
      this.setState({
        quickEntryLineItem: (0, _extends5.default)({}, lineItem, {
          item: item
        }),
        openBatchesModal: openBatchesModal,
        unitDropdown: lineItem.unitObj,
        taxList: taxList
      }, function () {
        if (!isQuickEntry) {
          _this4.changeLineItem();
        }
      });
    }
  }, {
    key: 'getDefaultLineItem',
    value: function getDefaultLineItem() {
      var _props5 = this.props,
          isQuickEntry = _props5.isQuickEntry,
          lineItem = _props5.lineItem,
          getDefaultLineItem = _props5.getDefaultLineItem;

      if (!isQuickEntry) {
        return lineItem;
      }
      return getDefaultLineItem();
    }
  }, {
    key: 'onSubmit',
    value: function onSubmit(e) {
      var _this5 = this;

      // e.preventDefault();
      if (e && e.keyCode === 13) {
        var _props6 = this.props,
            isQuickEntry = _props6.isQuickEntry,
            addLineItem = _props6.addLineItem,
            index = _props6.index;
        var quickEntryLineItem = this.state.quickEntryLineItem;

        if (isQuickEntry) {
          quickEntryLineItem.uniqueId = (0, _SalePurchaseHelpers.getUniqueKey)();
          addLineItem(quickEntryLineItem, true);
          this.setState({
            quickEntryLineItem: this.getDefaultLineItem()
          }, function () {
            _this5.clearDropdown && _this5.clearDropdown();
            var dropdown = document.querySelector('#salePurchaseItemDropdown-1');
            if (dropdown) {
              dropdown.focus();
            }
          });
        }
        // else {
        // 	// setNextLineItemFocus && setNextLineItemFocus(index);
        // }
      }
    }
  }, {
    key: 'showDeleteButton',
    value: function showDeleteButton(e) {
      this.setState({ toggleDeleteButton: true });
    }
  }, {
    key: 'hideDeleteButton',
    value: function hideDeleteButton(e) {
      this.setState({ toggleDeleteButton: false });
    }
  }, {
    key: 'toggleLastPrice',
    value: function toggleLastPrice(e) {
      var _state2 = this.state,
          anchorEl = _state2.anchorEl,
          quickEntryLineItem = _state2.quickEntryLineItem;
      var _props7 = this.props,
          txnType = _props7.txnType,
          party = _props7.party;

      var partyId = party && party.value ? party.value : '';
      var itemId = quickEntryLineItem.item && quickEntryLineItem.item.value ? quickEntryLineItem.item.value : null;
      if (partyId && itemId && !anchorEl && settingCache.ifShowLastFiveSalePrice() && txnType == _TxnTypeConstant2.default.TXN_TYPE_SALE) {
        this.lastFiveTransaction = dataLoader.loadLastFiveRecordsOfLineItemOfSaleTransactions(partyId, itemId, txnType);
        this.setState({
          anchorEl: e.currentTarget
        });
      } else {
        this.setState({
          anchorEl: null
        });
      }
    }
  }, {
    key: 'onBatchesModalClose',
    value: function onBatchesModalClose() {
      var _this6 = this;

      var selectedList = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

      if (selectedList.length) {
        var _state3 = this.state,
            quickEntryLineItem = _state3.quickEntryLineItem,
            unitDropdown = _state3.unitDropdown;
        var _props8 = this.props,
            txnType = _props8.txnType,
            party = _props8.party,
            firmLogic = _props8.firmLogic,
            placeOfSupply = _props8.placeOfSupply,
            txnId = _props8.txnId,
            isQuickEntry = _props8.isQuickEntry,
            getDefaultLineItem = _props8.getDefaultLineItem,
            taxInclusive = _props8.taxInclusive;

        var partyLogic = party && party.partyLogic ? party.partyLogic : null;
        var allLineItems = (0, _SalePurchaseHelpers.addItemStockTrackingInLineItem)(getDefaultLineItem(), selectedList, firmLogic, partyLogic, txnType, txnId, placeOfSupply);
        if (allLineItems.length) {
          var first = allLineItems.shift();
          first.unitObj = unitDropdown;
          var firstLineItem = (0, _SalePurchaseHelpers.changeLineItem)({
            defaultLineItem: first,
            partyLogic: partyLogic,
            itemLogic: first.item.item,
            firmLogic: firmLogic,
            txnType: txnType,
            txnId: txnId,
            placeOfSupply: placeOfSupply,
            taxInclusive: taxInclusive
          });
          firstLineItem.uniqueId = quickEntryLineItem.uniqueId;
          this.setState({
            quickEntryLineItem: firstLineItem,
            openBatchesModal: false
          }, function () {
            if (!isQuickEntry) {
              _this6.changeLineItem();
            }
            allLineItems = allLineItems.map(function (currentItem) {
              currentItem.unitObj = unitDropdown;
              var lineItem = (0, _SalePurchaseHelpers.changeLineItem)({
                defaultLineItem: currentItem,
                partyLogic: partyLogic,
                itemLogic: currentItem.item.item,
                firmLogic: firmLogic,
                txnType: txnType,
                txnId: txnId,
                placeOfSupply: placeOfSupply,
                taxInclusive: taxInclusive
              });
              lineItem.uniqueId = (0, _SalePurchaseHelpers.getUniqueKey)();
              return lineItem;
            });
            if (allLineItems.length) {
              if (isQuickEntry) {
                _this6.props.addLineItem(allLineItems, true);
              } else {
                _this6.props.addLineItem(allLineItems);
              }
            }
          });
        }
      } else {
        this.setState({
          openBatchesModal: false
        });
      }
      this.withBarcode = false;
      this.nextElementFocusTimeout = setTimeout(function () {
        var index = _this6.props.index;

        var id = '#salePurchaseItemDropdown' + index;
        var found = document.querySelector(id);
        if (found) {
          try {
            var closest = found.closest('tbody');
            if (closest) {
              var inputEl = closest.lastChild.querySelector('input');
              if (inputEl) {
                inputEl.focus();
              }
            }
          } catch (ex) {}
        }
      }, 100);
    }
  }, {
    key: 'createNewItem',
    value: function createNewItem(itemName) {
      var _this7 = this;

      var _props9 = this.props,
          txnType = _props9.txnType,
          createItem = _props9.createItem,
          index = _props9.index;

      var closeDialog = function closeDialog(evt) {
        if (evt.keyCode === 27) {
          try {
            $('#addNewItemDialog').dialog('close');
          } catch (err) {}
          evt.stopPropagation();
        }
      };
      window.updateItemSaveSuccess = function (newItemName) {
        var ItemCache = require('../../../Cache/ItemCache');
        var itemName = (newItemName || '').trim();
        var itemCache = new ItemCache();
        var logic = itemCache.findItemByName(itemName, txnType);
        var item = {
          value: _MyDouble2.default.convertStringToDouble(logic.getItemId()),
          label: logic.getItemName(),
          itemCode: '' + (logic.getItemCode() || ''),
          searchText: logic.getItemName() + ' ' + (logic.getItemCode() || ''),
          purchasePrice: _MyDouble2.default.getAmountWithDecimal(logic.getItemPurchaseUnitPrice()),
          salePrice: _MyDouble2.default.getAmountWithDecimal(logic.getItemSaleUnitPrice()),
          stockQty: _MyDouble2.default.getQuantityWithDecimal(logic.getAvailableQuantity(), logic.getItemMinStockQuantity(), true),
          item: logic
        };
        _this7.changeItem(item);
        createItem(item);
        _this7.updateDropdownItem && _this7.updateDropdownItem(itemName);
        document.querySelector('#salePurchaseItemDropdown' + index).focus();
        window.updateItemSaveSuccess = null;
      };
      $('#addNewItemDialog').dialog({
        title: 'Add New Item',
        width: '800px',
        height: 'auto',
        closeOnEscapte: false,
        appendTo: 'body',
        close: function close(event, ui) {
          setTimeout(function () {
            window.updateItemSaveSuccess = null;
          }, 3000);
          $('#addNewItemDialog').off('keydown', closeDialog);
          $('#addNewItemDialog').hide().dialog('destroy');
          $('#addNewItemDialog').html('');
          document.querySelector('#salePurchaseItemDropdown' + index).focus();
        }
      });
      window.itemNameGlobal = '';
      $('#addNewItemDialog').load('NewItems.html', function () {
        $('#discardChange').hide();
        $('#addNewItemDialog #newfm').css('top', 0);
        $('#addNewItemDialog .heading').hide();
        $('#addNewItemDialog').dialog('option', 'position', {
          my: 'center',
          at: 'center',
          of: window
        }).css({ overflowX: 'hidden' });
        $(this).find('.transactionStyle .form').css('width', '100%');
        $(this).find('#itemName').val(itemName || '');
        $('#addNewItemDialog #itemName').focus();
        $('#addNewItemDialog #submitNew').css('display', 'none');
        $('[aria-describedby="addNewItemDialog"]').css('z-index', 1301);
      });
    }
  }, {
    key: 'createNewIncomeOrExpense',
    value: function createNewIncomeOrExpense(name) {
      var createItem = this.props.createItem;

      var ItemLogic = require('../../../BizLogic/ItemLogic');
      var itemLogic = new ItemLogic();
      itemLogic.setItemName(name);
      itemLogic.setItemId(0);
      var item = {
        value: 0,
        label: name,
        item: itemLogic
      };
      this.changeItem(item);
      createItem(item);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this8 = this;

      var _state4 = this.state,
          toggleDeleteButton = _state4.toggleDeleteButton,
          anchorEl = _state4.anchorEl,
          quickEntryLineItem = _state4.quickEntryLineItem,
          openBatchesModal = _state4.openBatchesModal,
          unitDropdown = _state4.unitDropdown,
          taxList = _state4.taxList,
          filteredItems = _state4.filteredItems,
          itemCategoryId = _state4.itemCategoryId;
      var _props10 = this.props,
          classes = _props10.classes,
          items = _props10.items,
          deleteLineItem = _props10.deleteLineItem,
          index = _props10.index,
          txnType = _props10.txnType,
          isQuickEntry = _props10.isQuickEntry,
          lineItemColumnsVisibility = _props10.lineItemColumnsVisibility,
          showItemCode = _props10.showItemCode,
          showPurchasePriceInDropdown = _props10.showPurchasePriceInDropdown,
          showItemCategory = _props10.showItemCategory,
          showItemHSNCode = _props10.showItemHSNCode,
          itemCategories = _props10.itemCategories,
          isFTU = _props10.isFTU;
      var item = quickEntryLineItem.item,
          description = quickEntryLineItem.description,
          count = quickEntryLineItem.count,
          slNo = quickEntryLineItem.slNo,
          batchNo = quickEntryLineItem.batchNo,
          expDate = quickEntryLineItem.expDate,
          mfgDate = quickEntryLineItem.mfgDate,
          mrp = quickEntryLineItem.mrp,
          size = quickEntryLineItem.size,
          qty = quickEntryLineItem.qty,
          freeQty = quickEntryLineItem.freeQty,
          priceUnit = quickEntryLineItem.priceUnit,
          unit = quickEntryLineItem.unit,
          discountPercent = quickEntryLineItem.discountPercent,
          discountAmount = quickEntryLineItem.discountAmount,
          taxPercent = quickEntryLineItem.taxPercent,
          taxAmount = quickEntryLineItem.taxAmount,
          cess = quickEntryLineItem.cess,
          amount = quickEntryLineItem.amount,
          showITCEligible = quickEntryLineItem.showITCEligible,
          ITCValue = quickEntryLineItem.ITCValue;

      var open = Boolean(anchorEl);
      var left = 0;
      var top = 0;
      if (open) {
        var rect = anchorEl.getBoundingClientRect();
        left = rect.left;
        top = rect.top + 36;
      }
      var lineItemTextField = classes.lineItemTextField;
      if (isQuickEntry) {
        lineItemTextField = classes.quickEntryTextField;
      }
      return _react2.default.createElement(
        _react2.default.Fragment,
        null,
        openBatchesModal && _react2.default.createElement(_SelectBatchesModal2.default, {
          itemStockTrackingList: this.itemStockTrackingList,
          onClose: this.onBatchesModalClose
        }),
        _react2.default.createElement(
          'tr',
          {
            className: (0, _classnames2.default)(classes.lineItemContainer, isQuickEntry ? classes.quickEntryContainer : ''),
            onMouseOver: this.showDeleteButton,
            onMouseLeave: this.hideDeleteButton,
            onKeyUp: this.onSubmit,
            onFocus: this.focusRow(),
            onBlur: this.removeFocus(),
            onInput: this.onChange
          },
          _react2.default.createElement(
            'td',
            {
              className: (0, _classnames2.default)(classes.lineItemSrNo, classes.lineItemColumn)
            },
            _react2.default.createElement(
              'div',
              {
                className: isQuickEntry ? classes.quickEntryIconContainer : ''
              },
              isQuickEntry && _react2.default.createElement(_QuickEntryIcon2.default, { className: classes.quickEntryIcon }),
              !isQuickEntry && index + 1,
              (toggleDeleteButton || this.showDeleteIcon) && !isQuickEntry && _react2.default.createElement(
                'div',
                {
                  title: 'Click delete icon to delete row',
                  className: classes.deleteIcon
                },
                _react2.default.createElement(_Delete2.default, { onClick: function onClick() {
                    return deleteLineItem(index);
                  } })
              )
            )
          ),
          showItemCategory && _react2.default.createElement(
            'td',
            { className: classes.lineItemColumn },
            _react2.default.createElement(
              'select',
              {
                title: 'Select category to see items for selected category',
                value: itemCategoryId,
                'data-key': 'category',
                className: (0, _classnames2.default)(lineItemTextField, classes.categoryDropdown)
              },
              _react2.default.createElement(
                'option',
                { value: '0' },
                'ALL'
              ),
              itemCategories.map(function (category) {
                return _react2.default.createElement(
                  'option',
                  {
                    key: category.getCategoryId(),
                    value: category.getCategoryId()
                  },
                  category.getCategoryName()
                );
              })
            )
          ),
          _react2.default.createElement(
            'td',
            {
              className: (0, _classnames2.default)(classes.item, classes.lineItemColumn, isQuickEntry ? (0, _classnames2.default)(lineItemTextField, classes.quickEntryItem) : '')
            },
            txnType !== _TxnTypeConstant2.default.TXN_TYPE_EXPENSE && txnType !== _TxnTypeConstant2.default.TXN_TYPE_OTHER_INCOME && _react2.default.createElement(_ItemsDropdown2.default, {
              items: showItemCategory ? filteredItems : items,
              onChange: this.changeItem,
              className: (0, _classnames2.default)(lineItemTextField, classes.dropdownTextField),
              isBarcodeEnabled: true,
              value: item,
              index: index,
              isQuickEntry: isQuickEntry,
              isFTU: isFTU,
              updateItem: this.updateItemFunction,
              createNewItem: this.createNewItem,
              getClearDropdownFunction: this.getClearDropdownFunction,
              showItemPurchasePrice: showPurchasePriceInDropdown
            }),
            (txnType === _TxnTypeConstant2.default.TXN_TYPE_EXPENSE || txnType === _TxnTypeConstant2.default.TXN_TYPE_OTHER_INCOME) && _react2.default.createElement(_ItemsDropdown2.default, {
              txnType: txnType,
              items: showItemCategory ? filteredItems : items,
              onChange: this.changeItem,
              className: (0, _classnames2.default)(lineItemTextField, classes.dropdownTextField),
              isBarcodeEnabled: false,
              value: item,
              index: index,
              isQuickEntry: isQuickEntry,
              isFTU: isFTU,
              updateItem: this.updateItemFunction,
              createNewItem: this.createNewIncomeOrExpense,
              getClearDropdownFunction: this.getClearDropdownFunction,
              showItemPurchasePrice: false
            })
          ),
          showItemCode && _react2.default.createElement(
            'td',
            {
              title: item && item.item && item.item.getItemCode() || '',
              className: (0, _classnames2.default)(classes.lineItemColumn, classes.code, isQuickEntry ? classes.quickEntryCode : '')
            },
            item && item.item && item.item.getItemCode() || ''
          ),
          showItemHSNCode && _react2.default.createElement(
            'td',
            {
              title: item && item.item && item.item.getItemHSNCode() || '',
              className: (0, _classnames2.default)(classes.lineItemColumn, classes.HSNCode, isQuickEntry ? classes.quickEntryHSNCode : '')
            },
            item && item.item && item.item.getItemHSNCode() || ''
          ),
          lineItemColumnsVisibility.description && _react2.default.createElement(
            'td',
            {
              title: description,
              className: (0, _classnames2.default)(classes.description, classes.lineItemColumn)
            },
            _react2.default.createElement('input', {
              value: description,
              type: 'text',
              'data-key': 'description',
              className: lineItemTextField
            })
          ),
          lineItemColumnsVisibility.count && _react2.default.createElement(
            'td',
            {
              title: count,
              className: (0, _classnames2.default)(classes.count, classes.lineItemColumn)
            },
            _react2.default.createElement('input', {
              onInput: DecimalInputFilter.inputTextFilterForDate(this),
              value: count,
              type: 'text',
              'data-key': 'count',
              className: lineItemTextField
            })
          ),
          lineItemColumnsVisibility.slNo && _react2.default.createElement(
            'td',
            {
              title: slNo,
              className: (0, _classnames2.default)(classes.slNo, classes.lineItemColumn)
            },
            _react2.default.createElement('input', {
              value: slNo,
              type: 'text',
              'data-key': 'slNo',
              className: lineItemTextField
            })
          ),
          lineItemColumnsVisibility.batchNo && _react2.default.createElement(
            'td',
            {
              title: batchNo,
              className: (0, _classnames2.default)(classes.batchNo, classes.lineItemColumn)
            },
            _react2.default.createElement('input', {
              value: batchNo,
              type: 'text',
              'data-key': 'batchNo',
              className: lineItemTextField
            })
          ),
          lineItemColumnsVisibility.expDate && _react2.default.createElement(
            'td',
            { className: (0, _classnames2.default)(classes.expDate, classes.lineItemColumn) },
            _react2.default.createElement(_JqueryDatePicker2.default, {
              className: lineItemTextField,
              id: 'lineitem-expdate-' + index,
              format: settingCache.getExpiryDateFormat() == _StringConstants2.default.dateMonthYear ? 'dd/mm/yy' : 'mm/yy',
              validate: true,
              monthPicker: settingCache.getExpiryDateFormat() == _StringConstants2.default.monthYear,
              value: expDate,
              onChange: this.changeExpDate,
              onInput: function onInput(e) {
                return DecimalInputFilter.inputTextFilterForDate(e.target);
              }
            })
          ),
          lineItemColumnsVisibility.mfgDate && _react2.default.createElement(
            'td',
            { className: (0, _classnames2.default)(classes.mfgDate, classes.lineItemColumn) },
            _react2.default.createElement(_JqueryDatePicker2.default, {
              className: lineItemTextField,
              id: 'lineitem-mfgdate-' + index,
              format: settingCache.getManufacturingDateFormat() == _StringConstants2.default.dateMonthYear ? 'dd/mm/yy' : 'mm/yy',
              validate: true,
              monthPicker: settingCache.getManufacturingDateFormat() == _StringConstants2.default.monthYear,
              value: mfgDate,
              onChange: this.changeMfgDate,
              onInput: function onInput(e) {
                return DecimalInputFilter.inputTextFilterForDate(e.target);
              }
            })
          ),
          lineItemColumnsVisibility.mrp && _react2.default.createElement(
            'td',
            {
              title: mrp,
              className: (0, _classnames2.default)(classes.mrp, classes.lineItemColumn)
            },
            _react2.default.createElement('input', {
              value: mrp,
              type: 'text',
              'data-key': 'mrp',
              className: lineItemTextField
            })
          ),
          lineItemColumnsVisibility.size && _react2.default.createElement(
            'td',
            {
              title: size,
              className: (0, _classnames2.default)(classes.size, classes.lineItemColumn)
            },
            _react2.default.createElement('input', {
              value: size,
              type: 'text',
              'data-key': 'size',
              className: lineItemTextField
            })
          ),
          _react2.default.createElement(
            'td',
            {
              title: qty,
              className: (0, _classnames2.default)(classes.qty, classes.lineItemColumn)
            },
            _react2.default.createElement('input', {
              value: qty || '',
              type: 'text',
              onBlur: this.changeQuantity,
              'data-key': 'qty',
              className: lineItemTextField
            })
          ),
          lineItemColumnsVisibility.freeQty && _react2.default.createElement(
            'td',
            {
              title: freeQty,
              className: (0, _classnames2.default)(classes.freeQty, classes.lineItemColumn)
            },
            _react2.default.createElement('input', {
              value: freeQty,
              type: 'text',
              className: lineItemTextField,
              'data-key': 'freeQty'
            })
          ),
          lineItemColumnsVisibility.unit && _react2.default.createElement(
            'td',
            {
              className: (0, _classnames2.default)(classes.unit, classes.lineItemColumn, isQuickEntry ? classes.colorWhite : '')
            },
            _react2.default.createElement(
              'select',
              {
                value: unit,
                'data-key': 'unit',
                className: (0, _classnames2.default)(lineItemTextField, classes.unit)
              },
              unitDropdown.map(function (unit) {
                return _react2.default.createElement(
                  'option',
                  { key: unit.value, value: unit.value },
                  unit.label
                );
              })
            )
          ),
          _react2.default.createElement(
            'td',
            { className: (0, _classnames2.default)(classes.priceUnit, classes.lineItemColumn) },
            _react2.default.createElement(
              'div',
              { className: classes.positionRelative },
              _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement('input', {
                  title: priceUnit || '',
                  value: priceUnit,
                  onFocus: this.toggleLastPrice,
                  onBlur: this.toggleLastPrice,
                  type: 'text',
                  'data-key': 'priceUnit',
                  className: lineItemTextField
                }),
                open && settingCache.ifShowLastFiveSalePrice() && _react2.default.createElement(
                  'div',
                  { style: { left: left + 'px', top: top + 'px' }, className: classes.lastSalePriceContainer },
                  _react2.default.createElement(
                    'div',
                    { className: classes.lastSalePriceTitle },
                    'Last 5 Sale Prices'
                  ),
                  _react2.default.createElement(
                    'div',
                    {
                      className: (0, _classnames2.default)(classes.lastSalePriceHeader, 'd-flex')
                    },
                    _react2.default.createElement(
                      'div',
                      {
                        className: (0, _classnames2.default)(classes.dateColumn, classes.header)
                      },
                      'Date'
                    ),
                    _react2.default.createElement(
                      'div',
                      {
                        className: (0, _classnames2.default)(classes.qtyColumn, classes.header)
                      },
                      'Qty'
                    ),
                    _react2.default.createElement(
                      'div',
                      {
                        className: (0, _classnames2.default)(classes.priceColumn, classes.header)
                      },
                      'Price/Unit'
                    )
                  ),
                  this.lastFiveTransaction.map(function (txn, index) {
                    return _react2.default.createElement(
                      'div',
                      {
                        key: index,
                        className: (0, _classnames2.default)(classes.lastSalePriceRow, 'd-flex')
                      },
                      _react2.default.createElement(
                        'div',
                        { className: (0, _classnames2.default)(classes.dateColumn) },
                        txn.date
                      ),
                      _react2.default.createElement(
                        'div',
                        { className: (0, _classnames2.default)(classes.qtyColumn) },
                        txn.quantity
                      ),
                      _react2.default.createElement(
                        'div',
                        { className: (0, _classnames2.default)(classes.priceColumn) },
                        _MyDouble2.default.getAmountWithDecimal(txn.Price)
                      )
                    );
                  }),
                  !this.lastFiveTransaction.length && _react2.default.createElement(
                    'div',
                    { className: 'd-flex justify-content-center' },
                    'No Transaction Found.'
                  )
                )
              )
            )
          ),
          lineItemColumnsVisibility.discount && _react2.default.createElement(
            'td',
            {
              className: (0, _classnames2.default)(classes.disc, classes.positionRelative, classes.lineItemColumn, isQuickEntry ? classes.quickEntryContainer : '')
            },
            _react2.default.createElement(
              'div',
              { className: classes.discountWrapper },
              _react2.default.createElement(
                'div',
                {
                  className: (0, _classnames2.default)(classes.lineItemSubheader, 'd-flex')
                },
                _react2.default.createElement(
                  'div',
                  {
                    title: discountPercent,
                    className: (0, _classnames2.default)(classes.left, classes.discountLeft)
                  },
                  _react2.default.createElement('input', {
                    type: 'text',
                    value: discountPercent || '',
                    'data-key': 'discountPercent',
                    className: (0, _classnames2.default)(lineItemTextField, classes.discountPercent)
                  })
                ),
                _react2.default.createElement(
                  'div',
                  {
                    title: discountAmount,
                    className: (0, _classnames2.default)(classes.right, classes.discountRight)
                  },
                  _react2.default.createElement('input', {
                    value: discountAmount || '',
                    'data-key': 'discountAmount',
                    type: 'text',
                    className: (0, _classnames2.default)(lineItemTextField, classes.discountAmount)
                  })
                )
              )
            )
          ),
          lineItemColumnsVisibility.tax && _react2.default.createElement(
            'td',
            { className: (0, _classnames2.default)(classes.tax, classes.lineItemColumn) },
            _react2.default.createElement(
              'div',
              {
                className: (0, _classnames2.default)(classes.lineItemSubheader, 'd-flex align-items-center')
              },
              _react2.default.createElement(
                'div',
                { className: (0, _classnames2.default)(classes.left) },
                _react2.default.createElement(
                  'select',
                  {
                    value: taxPercent,
                    'data-key': 'taxPercent',
                    className: (0, _classnames2.default)(lineItemTextField, classes.taxPercent, showITCEligible ? classes.ITCDropdownOptions : '')
                  },
                  taxList.map(function (tax) {
                    return _react2.default.createElement(
                      'option',
                      { key: tax.value, value: tax.value },
                      tax.label
                    );
                  })
                ),
                _react2.default.createElement(
                  'div',
                  null,
                  showITCEligible && _react2.default.createElement(
                    'select',
                    {
                      'data-key': 'ITCValue',
                      value: ITCValue,
                      className: (0, _classnames2.default)(lineItemTextField, classes.taxPercent, classes.ITCDropdownOptions)
                    },
                    this.ITCDropdownOptions.map(function (_ref) {
                      var value = _ref.value,
                          label = _ref.label;
                      return _react2.default.createElement(
                        'option',
                        { key: value, value: value },
                        label
                      );
                    })
                  )
                )
              ),
              _react2.default.createElement(
                'div',
                {
                  className: (0, _classnames2.default)(classes.right, 'd-flex justify-content-center align-items-center')
                },
                _react2.default.createElement('input', {
                  title: taxAmount,
                  value: _MyDouble2.default.getAmountWithDecimal(taxAmount) || '',
                  disabled: true,
                  'data-key': 'taxAmount',
                  type: 'text',
                  className: (0, _classnames2.default)(lineItemTextField, classes.taxAmount)
                })
              )
            )
          ),
          lineItemColumnsVisibility.cess && _react2.default.createElement(
            'td',
            {
              title: cess,
              className: (0, _classnames2.default)(classes.cess, classes.lineItemColumn)
            },
            _react2.default.createElement('input', {
              value: cess || '',
              type: 'text',
              'data-key': 'cess',
              className: lineItemTextField
            })
          ),
          _react2.default.createElement(
            'td',
            {
              title: amount,
              className: (0, _classnames2.default)(classes.amount, classes.lineItemColumn)
            },
            _react2.default.createElement('input', {
              title: amount,
              value: amount || '',
              type: 'text',
              'data-key': 'amount',
              disabled: discountPercent == 100,
              className: (0, _classnames2.default)(lineItemTextField, classes.amount, isQuickEntry ? classes.quickEntryAmountText : '')
            }),
            isQuickEntry && _react2.default.createElement(
              _Button2.default,
              {
                onClick: function onClick() {
                  return _this8.onSubmit({ keyCode: 13 });
                },
                classes: { label: classes.quickEntrySubmitButtonLabel },
                type: 'submit',
                color: 'primary',
                className: classes.quickEntrySubmitButton
              },
              _react2.default.createElement(_Check2.default, { color: 'primary', className: classes.colorWhite })
            )
          )
        )
      );
    }
  }]);
  return LineItem;
}(_react.Component);

var _initialiseProps = function _initialiseProps() {
  var _this9 = this;

  this.changeQuantity = function (event) {
    var value = event.target.value;
    var quickEntryLineItem = _this9.state.quickEntryLineItem;
    var _props11 = _this9.props,
        taxInclusive = _props11.taxInclusive,
        lineItemColumnsVisibility = _props11.lineItemColumnsVisibility,
        isQuickEntry = _props11.isQuickEntry,
        txnType = _props11.txnType,
        index = _props11.index,
        changeLineItem = _props11.changeLineItem;

    var lineItem = (0, _assign2.default)((0, _extends5.default)({}, quickEntryLineItem, { qty: value }));
    if (quickEntryLineItem.qty == lineItem.qty) {
      return false;
    }
    var newLineItem = lineItem;
    lineItem.qty = DecimalInputFilter.inputTextFilterForQuantity(event.target);
    if (!_MyDouble2.default.convertStringToDouble(lineItem.qty)) {
      if (!lineItemColumnsVisibility.freeQty) {
        lineItem.qty = 1;
      }
    }
    newLineItem = (0, _SalePurchaseHelpers.updateLineItemFields)(lineItem, txnType, taxInclusive);

    if (!isQuickEntry) {
      changeLineItem(index, newLineItem);
    }
    _this9.setState({
      quickEntryLineItem: newLineItem
    });
  };

  this.onEnterKeyPress = function (e) {};

  this.updateItemFunction = function (fn) {
    _this9.updateDropdownItem = fn;
  };

  this.getClearDropdownFunction = function (fn) {
    _this9.clearDropdown = fn;
  };

  this.focusRow = function () {
    return function (event) {
      var isQuickEntry = _this9.props.isQuickEntry;

      if (!isQuickEntry) {
        _this9.showDeleteIcon = true;
        event.currentTarget.classList.add('focused-row');
        _this9.forceUpdate();
      }
    };
  };

  this.removeFocus = function () {
    return function (event) {
      var isQuickEntry = _this9.props.isQuickEntry;

      if (!isQuickEntry) {
        _this9.showDeleteIcon = false;
        event.currentTarget.classList.remove('focused-row');
        _this9.forceUpdate();
      }
    };
  };
};

LineItem.propTypes = {
  classes: _propTypes2.default.object,
  changeLineItem: _propTypes2.default.func,
  lineItem: _propTypes2.default.object,
  party: _propTypes2.default.object,
  firmLogic: _propTypes2.default.object,
  placeOfSupply: _propTypes2.default.string,
  txnId: _propTypes2.default.number,
  getDefaultLineItem: _propTypes2.default.func,
  addLineItem: _propTypes2.default.func,
  createItem: _propTypes2.default.func,
  // setNextLineItemFocus: PropTypes.func,
  items: _propTypes2.default.array,
  deleteLineItem: _propTypes2.default.func,
  index: _propTypes2.default.number,
  txnType: _propTypes2.default.number,
  isQuickEntry: _propTypes2.default.bool,
  lineItemColumnsVisibility: _propTypes2.default.object,
  showItemCode: _propTypes2.default.bool,
  showPurchasePriceInDropdown: _propTypes2.default.bool,
  showItemCategory: _propTypes2.default.bool,
  showItemHSNCode: _propTypes2.default.bool,
  isBarcodeEnabled: _propTypes2.default.bool,
  isFTU: _propTypes2.default.bool,
  itemCategories: _propTypes2.default.array,
  showBatchDialog: _propTypes2.default.bool,
  taxInclusive: _propTypes2.default.number
};
exports.default = (0, _styles.withStyles)(styles)(LineItem);