Object.defineProperty(exports, "__esModule", {
	value: true
});

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

var _extends4 = require('babel-runtime/helpers/extends');

var _extends5 = _interopRequireDefault(_extends4);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Dialog = require('@material-ui/core/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/core/styles');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _themes = require('../../../themes');

var _themes2 = _interopRequireDefault(_themes);

var _SettingCache = require('../../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

var _FirmCache = require('../../../Cache/FirmCache');

var _FirmCache2 = _interopRequireDefault(_FirmCache);

var _NameCache = require('../../../Cache/NameCache');

var _NameCache2 = _interopRequireDefault(_NameCache);

var _PaymentInfoCache = require('../../../Cache/PaymentInfoCache');

var _PaymentInfoCache2 = _interopRequireDefault(_PaymentInfoCache);

var _TxnTypeConstant = require('../../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _ErrorCode = require('../../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

var _StringConstants = require('../../../Constants/StringConstants');

var _StringConstants2 = _interopRequireDefault(_StringConstants);

var _MyDouble = require('../../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _analyticsHelper = require('../../../Utilities/analyticsHelper');

var _analyticsHelper2 = _interopRequireDefault(_analyticsHelper);

var _MyDate = require('../../../Utilities/MyDate');

var _MyDate2 = _interopRequireDefault(_MyDate);

var _JqueryDatePicker = require('../UIControls/JqueryDatePicker');

var _JqueryDatePicker2 = _interopRequireDefault(_JqueryDatePicker);

var _DataLoader = require('../../../DBManager/DataLoader');

var _DataLoader2 = _interopRequireDefault(_DataLoader);

var _TransactionFactory = require('../../../BizLogic/TransactionFactory');

var _TransactionFactory2 = _interopRequireDefault(_TransactionFactory);

var _udfValueModel = require('../../../Models/udfValueModel');

var _udfValueModel2 = _interopRequireDefault(_udfValueModel);

var _FTUHelper = require('../../../Utilities/FTUHelper');

var _FTUHelper2 = _interopRequireDefault(_FTUHelper);

var _LicenseUtility = require('../../../Utilities/LicenseUtility');

var _LicenseUtility2 = _interopRequireDefault(_LicenseUtility);

var _Select = require('@material-ui/core/Select');

var _Select2 = _interopRequireDefault(_Select);

var _FormControl = require('@material-ui/core/FormControl');

var _FormControl2 = _interopRequireDefault(_FormControl);

var _InputLabel = require('@material-ui/core/InputLabel');

var _InputLabel2 = _interopRequireDefault(_InputLabel);

var _OutlinedInput = require('@material-ui/core/OutlinedInput');

var _OutlinedInput2 = _interopRequireDefault(_OutlinedInput);

var _OutlineInput = require('../UIControls/OutlineInput');

var _OutlineInput2 = _interopRequireDefault(_OutlineInput);

var _DateRange = require('@material-ui/icons/DateRange');

var _DateRange2 = _interopRequireDefault(_DateRange);

var _Slide = require('@material-ui/core/Slide');

var _Slide2 = _interopRequireDefault(_Slide);

var _NoteAdd = require('@material-ui/icons/NoteAdd');

var _NoteAdd2 = _interopRequireDefault(_NoteAdd);

var _AddAPhoto = require('@material-ui/icons/AddAPhoto');

var _AddAPhoto2 = _interopRequireDefault(_AddAPhoto);

var _IconButton = require('@material-ui/core/IconButton');

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _ImagePreview = require('./ImagePreview');

var _ImagePreview2 = _interopRequireDefault(_ImagePreview);

var _SalePurchaseHelpers = require('./SalePurchaseHelpers');

var _nav = require('./nav');

var _nav2 = _interopRequireDefault(_nav);

var _footerActions = require('./footerActions');

var _footerActions2 = _interopRequireDefault(_footerActions);

var _ConfirmModal = require('../ConfirmModal');

var _ConfirmModal2 = _interopRequireDefault(_ConfirmModal);

var _LicenseExpiredModal = require('../LicenseExpiredModal');

var _LicenseExpiredModal2 = _interopRequireDefault(_LicenseExpiredModal);

var _ToastHelper = require('../../../UIControllers/ToastHelper');

var ToastHelper = _interopRequireWildcard(_ToastHelper);

var _UDFFieldsConstant = require('../../../Constants/UDFFieldsConstant');

var udfConstants = _interopRequireWildcard(_UDFFieldsConstant);

var _PartyDropdown = require('../UIControls/PartyDropdown');

var _PartyDropdown2 = _interopRequireDefault(_PartyDropdown);

var _TextField = require('@material-ui/core/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _DecimalInputFilter = require('../../../Utilities/DecimalInputFilter');

var DecimalInputFilter = _interopRequireWildcard(_DecimalInputFilter);

var _AddNewBankAccountModal = require('../AddNewBankAccountModal');

var _AddNewBankAccountModal2 = _interopRequireDefault(_AddNewBankAccountModal);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DATEFORMATE = 'd/m/y';
var DATE_FORMATE_FULL = 'dd/mm/yyyy';

var settingCache = new _SettingCache2.default();
var theme = (0, _themes2.default)();

var styles = function styles() {
	return {
		invoiceFieldsContainer: {
			paddingRight: 40
		},
		salePurchaseContainer: {
			border: '1px solid #ddd',
			background: '#F4F5F8',
			width: 885
		},
		cashInCashOutContainer: {
			background: '#fff'
		},
		content: {
			marginTop: 25,
			marginLeft: 25,
			minHeight: 400
		},
		leftContainer: {
			width: 475
		},
		flex1: {
			flex: 1
		},
		overflowAuto: {
			overflow: 'auto'
		},
		textFieldWidth230: {
			width: 230
		},
		mb15: {
			marginBottom: 15
		},
		mb20: {
			marginBottom: 20
		},
		mb30: {
			marginBottom: 30
		},
		positionRelative: {
			position: 'relative'
		},
		connector: {
			position: 'absolute',
			height: 65,
			border: '1px solid #ddd',
			borderLeft: 0,
			width: 7,
			left: 230,
			top: 17
		},

		formControl: {
			height: 36,
			'& label': {
				color: 'rgba(34, 42, 63, 0.5)',
				fontFamily: 'RobotoMedium',
				fontSize: 16,
				transform: 'translate(14px, 13px) scale(0.81)'
			},
			'& select': {
				padding: '10px 25px 10px 15px',
				width: 190,
				height: 18,
				fontSize: 13,
				'&:focus': {
					background: 'transparent'
				}
			}
		},
		inputRoot: {
			background: '#fff'
		},

		mb25: {
			marginBottom: 25
		},
		button: {
			border: '1px solid #ddd',
			color: 'darkgray'
		},
		buttonText: {
			fontSize: 12,
			position: 'relative',
			top: 2,
			margin: '5px 0'
		},
		leftIcon: {
			marginRight: theme.spacing.unit
		},
		iconSmall: {
			fontSize: 20
		},
		textArea: {
			height: 'auto',
			background: 'transparent',
			width: 230
		},
		imageContainer: {
			position: 'relative',
			overflow: 'hidden',
			width: 170,
			marginTop: 30
		},
		image: {
			width: 170,
			height: 128,
			backgroundSize: 'contain',
			cursor: 'pointer',
			backgroundRepeat: 'no-repeat'
		},
		iconButton: {
			color: 'darkgrey'
		},
		imageActions: {
			position: 'absolute',
			top: 90,
			display: 'flex',
			background: 'rgba(0,0,0,0.5)',
			height: 40,
			alignItems: 'center',
			width: '100%',
			justifyContent: 'space-between'
		},
		imageAction: {
			color: '#fff',
			padding: 10,
			textTransform: 'uppercase',
			fontSize: 13,
			cursor: 'pointer'
		},
		transparentDatepicker: {
			background: 'transparent',
			width: 170,
			border: 0
		},
		datePickerWrapper: {
			position: 'relative',
			zIndex: 100
		},
		dateIcon: {
			position: 'relative',
			top: -20,
			left: 75,
			fill: '#aaa',
			fontSize: 18
		},
		PODateIcon: {
			position: 'absolute',
			color: '#aaa',
			right: 5,
			top: 7
		},
		width100: {
			width: 100
		},
		width180: {
			width: 180
		},
		inlineLabel: {
			fontSize: 12,
			fontFamily: 'RobotoMedium',
			color: 'rgba(34, 42, 63, 0.5)'
		},
		height20: {
			height: 20
		},
		inlineTextField: {
			color: '#222A3F',
			fontSize: 13,
			background: 'transparent',
			border: '1px solid transparent',
			padding: 2,
			'&:focus': {
				border: '1px solid #1789fc !important'
			}
		},
		invoiceNumber: {
			borderBottom: '1px solid #ddd !important',
			overflow: 'hidden',
			textOverflow: 'ellipsis',
			whiteSpace: 'nowrap',
			width: 75
		},
		footerFields: {
			marginTop: 200,
			'& input': {
				height: 32,
				borderRadius: 4,
				border: '1px solid #C4C4C4',
				paddingLeft: 5,
				paddingRight: 8,
				textAlign: 'right'
			}
		},
		errorTextField: {
			border: '1px solid red !important'
		},
		footerFieldsContainer: {
			marginBottom: 25
		},
		balance: {
			fontWeight: 'bold',
			color: '#222A3F'
		},
		balanceColor: {
			color: '#1789FC',
			fontWeight: 'bold',
			fontSize: 14,
			fontFamily: 'RobotoMedium',
			textAlign: 'right'
		},
		mandatory: {
			color: '#e85454'
		},
		bgGray: {
			background: 'rgb(235, 235, 228)'
		},
		right: {
			width: 320
		}
	};
};

var SalePurchaseContainer = function (_Component) {
	(0, _inherits3.default)(SalePurchaseContainer, _Component);

	function SalePurchaseContainer(props) {
		(0, _classCallCheck3.default)(this, SalePurchaseContainer);

		var _this = (0, _possibleConstructorReturn3.default)(this, (SalePurchaseContainer.__proto__ || (0, _getPrototypeOf2.default)(SalePurchaseContainer)).call(this, props));

		_this.createNewParty = function (name, txnType) {
			var nameCache = new _NameCache2.default();
			var success = (0, _SalePurchaseHelpers.saveNewName)(name, txnType);
			if (success) {
				return nameCache.findNameModelByName(name);
			} else {
				return false;
			}
		};

		_this.changePaymentType = function (event) {
			var transaction = _this.state.transaction;

			var value = null;
			if (event && event.target) {
				value = event.target.value;
			}
			if (value) {
				_this.setState({
					transaction: (0, _extends5.default)({}, transaction, { paymentType: value })
				});
			} else {
				_this.setState({ showNewBankAccountDialog: true });
			}
		};

		_this.refreshBankAccounts = function (createdBankAccount) {
			var transaction = _this.state.transaction;

			if (createdBankAccount) {
				var value = transaction.paymentType;
				var paymentInfoCache = new _PaymentInfoCache2.default();
				_this.paymentTypeOptions = paymentInfoCache.getListOfPaymentAccountObjects();
				var found = _this.paymentTypeOptions.find(function (p) {
					return p.getName() === createdBankAccount;
				});
				if (found) {
					value = found.getId();
				}
				_this.setState({
					transaction: (0, _extends5.default)({}, transaction, { paymentType: value })
				});
			}
			_this.setState({ showNewBankAccountDialog: false });
		};

		_this.deleteImage = function () {
			_this.changeField('image')('');
		};

		_this.licenseExpired = function () {
			_this.setState({ isLicenseExpired: false }, function () {
				unmountReactComponent(document.querySelector('#salePurchaseContainer'));
			});
		};

		_this.changeInvoiceDate = function (invoiceDate) {
			_this.changeFields({
				invoiceDate: invoiceDate
			});
		};

		_this.updatePartyFunction = function (fn) {
			_this.updateParty = fn;
		};

		_this.saveNewParty = function (partyName) {
			var txnType = _this.props.txnType;

			var $addNewName = $('#addNewNameDialog');
			window.userNameGlobal = '';
			var closeDialog = function closeDialog(evt) {
				if (evt.keyCode === 27) {
					try {
						$addNewName.dialog('close');
						$addNewName.html('');
					} catch (err) {}
					evt.stopPropagation();
				}
			};
			var saveName = function saveName(e, ui) {
				var name = $('#contactName').val().trim();
				if (!name) {
					document.querySelector('#salePurchasePartyDropdown').focus();
					return false;
				}
				var found = _this.dropdownNames.find(function (item) {
					return item.label.trim() == name;
				});
				if (found) {
					document.querySelector('#salePurchasePartyDropdown').focus();
					return false;
				}
				var NameCache = require('../../../Cache/NameCache');
				var NameType = require('../../../Constants/NameType');
				var nameCache = new NameCache();
				var nameLogic = nameCache.getNameObjectByName(name, NameType.NAME_TYPE_PARTY, txnType);
				if (nameLogic) {
					var item = {
						value: _MyDouble2.default.convertStringToDouble(nameLogic.getNameId()),
						label: nameLogic.getFullName(),
						balance: _MyDouble2.default.getAmountWithDecimal(nameLogic.getOpeningBalance()),
						partyLogic: nameLogic
					};
					_this.createParty(item);
					_this.changeParty(item);
					_this.updateParty && _this.updateParty(nameLogic.getFullName());
					document.querySelector('#salePurchasePartyDropdown').focus();
				}
				$('#addNewNameDialog').hide().dialog('destroy').html('');
			};

			$addNewName.dialog({
				title: 'Add New Name',
				width: '800',
				height: '600',
				closeOnEscape: false,
				appendTo: 'body',
				close: saveName
			});
			$addNewName.on('keydown', closeDialog);

			$addNewName.load('NewContact.html', function () {
				$addNewName.dialog('option', 'position', {
					my: 'center',
					at: 'center',
					of: window
				});
				$('#contactName').val(partyName);
				$(this).find('.transactionStyle .form').css('width', '100%');
				$(this).find('.transactionStyle .form').css('height', '100%');
				$(this).find('.transactionStyle .heading').html('');
				$(this).css('max-height', '650px');
				$('#submitNew', $addNewName).css('display', 'none');
				$('[aria-describedby="addNewNameDialog"]').css('z-index', 9999);
			});
		};

		_this.showImageActions = function () {
			_this.setState({
				imageActionVisible: true
			});
		};

		_this.hideImageActions = function () {
			_this.setState({
				imageActionVisible: false
			});
		};

		_this.changeTotal = function (e) {
			var value = DecimalInputFilter.inputTextFilterForAmount(e.target);
			var _this$state$transacti = _this.state.transaction,
			    discountForCash = _this$state$transacti.discountForCash,
			    receivedLater = _this$state$transacti.receivedLater;

			var total = _MyDouble2.default.convertStringToDouble(value);
			var currentTxnBalance = _MyDouble2.default.convertStringToDouble(total) + _MyDouble2.default.convertStringToDouble(discountForCash) - _MyDouble2.default.convertStringToDouble(receivedLater || 0);
			if (_this.isConvert) {
				currentTxnBalance = total + _MyDouble2.default.convertStringToDouble(discountForCash) - _MyDouble2.default.convertStringToDouble(_this.currentTxnBalance || 0);
			}
			currentTxnBalance = _MyDouble2.default.getAmountWithDecimal(currentTxnBalance);
			var balance = total + _MyDouble2.default.convertStringToDouble(discountForCash);
			balance = _MyDouble2.default.getAmountWithDecimal(balance);
			_this.changeFields({
				balance: balance,
				total: value,
				currentTxnBalance: currentTxnBalance
			});
		};

		_this.changeDiscountForCash = function (e) {
			var value = DecimalInputFilter.inputTextFilterForAmount(e.target);
			var _this$state$transacti2 = _this.state.transaction,
			    total = _this$state$transacti2.total,
			    receivedLater = _this$state$transacti2.receivedLater;

			var discountForCash = _MyDouble2.default.convertStringToDouble(value);
			var currentTxnBalance = _MyDouble2.default.convertStringToDouble(total) + _MyDouble2.default.convertStringToDouble(discountForCash) - _MyDouble2.default.convertStringToDouble(receivedLater || 0);
			if (_this.isConvert) {
				currentTxnBalance = _MyDouble2.default.convertStringToDouble(total) + _MyDouble2.default.convertStringToDouble(discountForCash) - _MyDouble2.default.convertStringToDouble(_this.currentTxnBalance || 0);
			}
			currentTxnBalance = _MyDouble2.default.getAmountWithDecimal(currentTxnBalance);
			var balance = _MyDouble2.default.convertStringToDouble(total) + _MyDouble2.default.convertStringToDouble(discountForCash);
			balance = _MyDouble2.default.getAmountWithDecimal(balance);
			_this.changeFields({
				balance: _MyDouble2.default.convertStringToDouble(balance),
				discountForCash: value,
				currentTxnBalance: _MyDouble2.default.convertStringToDouble(currentTxnBalance)
			});
		};

		_this.changeInvoiceNo = function (e) {
			var txnType = _this.props.txnType;

			var invoiceNo = e.target.value;
			if (txnType != _TxnTypeConstant2.default.TXN_TYPE_CASHOUT) {
				invoiceNo = DecimalInputFilter.inputTextFilterForInvoice(e.target, txnType);
			}
			_this.changeFields({ invoiceNo: invoiceNo });
		};

		_this.setDatePickerFocus = function (e) {
			e.target.closest('div').querySelector('input').focus();
		};

		_this.changePartyFocus = function () {
			_this.setState({ renderForFirstAutofocus: true });
		};

		_this.showImagePreview = function () {
			_this.setState({ showImagePreview: true });
		};

		_this.closeImagePreview = function () {
			_this.setState({ showImagePreview: false });
		};

		_this.state = {
			renderForFirstAutofocus: false,
			open: true,
			firmList: [],
			confirm: false,
			showDescription: false,
			cashSaleOption: {},
			invoicePrefixOptions: [],
			showNewBankAccountDialog: false,
			paymentTermsOptions: [],
			partyAmount: 0,
			subTotal: 0,
			lineItemTotalTax: 0,
			totalCount: 0,
			totalQty: 0,
			txnListB2B: new _map2.default(),
			selectedTransactionMap: new _map2.default(),
			copyOfSelectedTransactionMap: new _map2.default(),
			closedOn: '',
			linkedTransactionInvoiceNumber: '',
			showErrorForTotal: false,
			saveInProgress: false,
			isLicenseExpired: !_LicenseUtility2.default.isLicenseValid(),
			imageActionVisible: false,
			showImagePreview: false,
			transaction: {
				cashSale: false,
				party: '',
				firm: '',
				displayName: '',
				billingAddress: '',
				shippingAddress: '',
				invoiceType: 1,
				invoiceNo: '',
				invoicePrefix: '',
				invoiceDate: props.invoiceDate ? _MyDate2.default.getDate(DATEFORMATE, props.invoiceDate) : _MyDate2.default.getDate(DATEFORMATE, new Date()),
				paymentTerms: '',
				dueDate: '',
				txnReturnRefNumber: '',
				txnReturnRefDate: '',
				placeOfSupply: '',
				paymentType: '',
				referenceNo: '',
				description: '',
				imageId: '',
				image: '',
				imageBlob: '',
				extraFields: [],
				discountPercent: 0,
				discountAmount: 0,
				taxPercent: 0,
				taxAmount: 0,
				ITCVisibility: false,
				ITC: 0,
				additionalCharges: [],
				roundOff: false,
				roundOffAmount: 0,
				total: 0,
				payable: 0,
				received: 0,
				totalReceivedAmount: 0,
				receivedLater: 0,
				balance: 0,
				currentTxnBalance: 0,
				discountForCash: 0
			}
		};

		_this.init = _this.init.bind(_this);
		_this.onClose = _this.onClose.bind(_this);
		_this.changeCashSale = _this.changeCashSale.bind(_this);
		_this.changeParty = _this.changeParty.bind(_this);
		_this.changeField = _this.changeField.bind(_this);
		_this.changeFields = _this.changeFields.bind(_this);
		_this.changeCheckbox = _this.changeCheckbox.bind(_this);
		_this.changeFirm = _this.changeFirm.bind(_this);
		_this.changePrefix = _this.changePrefix.bind(_this);
		_this.changeExtraField = _this.changeExtraField.bind(_this);
		_this.toggleDescription = _this.toggleDescription.bind(_this);
		_this.changeImage = _this.changeImage.bind(_this);
		_this.onSave = _this.onSave.bind(_this);
		_this.saveTransaction = _this.saveTransaction.bind(_this);
		_this.changeTransactionLinks = _this.changeTransactionLinks.bind(_this);
		_this.createParty = _this.createParty.bind(_this);

		_this.fieldsVisibility = {};
		_this.labels = {
			name: 'NAME',
			received: 'RECEIVED',
			total: 'TOTAL',
			invoiceNo: 'Invoice Number',
			invoiceDate: 'Invoice Date'
		};
		_this.currencySymbol = settingCache.getCurrencySymbol();
		_this.isFTU = false;
		_this.descriptionButtonClicked = false;
		_this.showDefaultPage = true;
		setTimeout(function () {
			$('#modelContainer #close:visible').trigger('click');
			unmountReactComponent(document.querySelector('#frameDiv'));
			$('#frameDiv').empty();
		});
		return _this;
	}

	(0, _createClass3.default)(SalePurchaseContainer, [{
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			if (this.showDefaultPage) {
				$('#defaultPage').show();
			}
			window.onmessage = null;
		}
	}, {
		key: 'componentDidCatch',
		value: function componentDidCatch(error, errorInfo) {
			var saveInProgress = this.state.saveInProgress;

			if (saveInProgress) {
				this.setState({
					saveInProgress: false
				});
			}
			window.logger && window.logger.error(error, errorInfo);
		}
	}, {
		key: 'init',
		value: function init() {
			var transaction = this.state.transaction;
			var _props = this.props,
			    txnType = _props.txnType,
			    txnId = _props.txnId,
			    txnObj = _props.txnObj;

			var firmCache = new _FirmCache2.default();
			var nameCache = new _NameCache2.default();
			var paymentInfoCache = new _PaymentInfoCache2.default();
			var dataLoader = new _DataLoader2.default();
			var state = {};

			var _getTransactionSettin = (0, _SalePurchaseHelpers.getTransactionSettings)(txnType, txnId, transaction),
			    labels = _getTransactionSettin.labels,
			    fieldsVisibility = _getTransactionSettin.fieldsVisibility;

			this.labels = labels;
			this.fieldsVisibility = fieldsVisibility;

			var selectedTransactionMap = new _map2.default();
			var copyOfSelectedTransactionMap = new _map2.default();
			var txnListB2B = new _map2.default();

			this.allNames = nameCache.getListOfNamesObject();

			var cashSaleOption = null;
			this.title = _TxnTypeConstant2.default.getTxnTypeForUI(txnType) || '';
			this.dropdownNames = this.allNames.map(function (logic) {
				var fullName = logic.getFullName();
				var option = {
					value: _MyDouble2.default.convertStringToDouble(logic.getNameId()),
					label: fullName,
					balance: _MyDouble2.default.getAmountWithDecimal(logic.getAmount()),
					partyLogic: logic
				};
				if (fullName === _StringConstants2.default.cash || fullName === _StringConstants2.default.cashSale) {
					cashSaleOption = option;
				}
				return option;
			});

			this.paymentTypeOptions = paymentInfoCache.getListOfPaymentAccountObjects();
			var invoicePrefixOptions = [];
			var transactionFactory = new _TransactionFactory2.default();

			if (txnId) {
				var txnObject = transactionFactory.getTransactionObject(txnType);
				var oldTxnObj = txnObj || txnObject.getTransactionFromId(txnId);
				this.oldTxnObj = oldTxnObj;
				if (oldTxnObj && oldTxnObj.getTxnType() == txnType) {
					_analyticsHelper2.default.pushEvent('Edit ' + _TxnTypeConstant2.default.getTxnTypeForUI(oldTxnObj.getTxnType()) + ' Open');
				}

				var nameLogic = oldTxnObj.getNameRef();
				var firmId = oldTxnObj.getFirmId();

				invoicePrefixOptions = (0, _SalePurchaseHelpers.getInvoicePrefixOptions)(firmId, txnType);
				var invoicePrefix = _MyDouble2.default.convertStringToDouble(oldTxnObj.getInvoicePrefixId());

				var receivedLaterTotalAmount = 0;

				if (settingCache.isBillToBillEnabled() && (txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHIN && oldTxnObj.getTxnType() != _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT && oldTxnObj.getTxnType() != _TxnTypeConstant2.default.TXN_TYPE_CASHOUT)) {
					var txnCurrentBalanceAmount = _MyDouble2.default.convertStringToDouble(oldTxnObj.getTxnCurrentBalanceAmount());

					selectedTransactionMap.set(_MyDouble2.default.convertStringToDouble(txnId), [oldTxnObj, txnCurrentBalanceAmount, null]);

					receivedLaterTotalAmount += txnCurrentBalanceAmount;
				} else {
					var txnLinksList = dataLoader.loadAllTransactionLinksByTxnId(txnId);

					if (txnLinksList) {
						for (var i = 0; i < txnLinksList.length; i++) {
							var linkTxn = txnLinksList[i];
							var txnLink1Id = _MyDouble2.default.convertStringToDouble(linkTxn.getTxnLinksTxn1Id()); // in case of null returns 0
							var txnLink2Id = _MyDouble2.default.convertStringToDouble(linkTxn.getTxnLinksTxn2Id()); // in case of null returns 0
							var txnLinkAmount = _MyDouble2.default.convertStringToDouble(linkTxn.getTxnLinksAmount());
							var closedTxnId = _MyDouble2.default.convertStringToDouble(linkTxn.getTxnLinkClosedTxnRefId());

							receivedLaterTotalAmount += txnLinkAmount;
							var selectedTxnId = void 0;
							if (_MyDouble2.default.convertStringToDouble(txnId) == txnLink1Id) {
								selectedTxnId = txnLink2Id;
							} else if (_MyDouble2.default.convertStringToDouble(txnId) == txnLink2Id) {
								selectedTxnId = txnLink1Id;
							}

							var linkTxnObject = void 0;
							if (selectedTxnId > 0) {
								linkTxnObject = dataLoader.LoadTransactionFromId(selectedTxnId);
							} else {
								// for unEditable closed linked transactions
								linkTxnObject = dataLoader.loadClosedTransactionDataById(closedTxnId);

								selectedTxnId = i + 'closed';
							}

							selectedTransactionMap.set(_MyDouble2.default.convertStringToDouble(selectedTxnId), [linkTxnObject, txnLinkAmount, linkTxn]);
						}
					}
				}

				txnListB2B = (0, _SalePurchaseHelpers.getTransactionsForB2BLinking)(nameLogic.getNameId(), firmId, txnType, selectedTransactionMap);

				if (selectedTransactionMap.size > 0) {
					copyOfSelectedTransactionMap = new _map2.default(selectedTransactionMap);
					this.changeTransactionLinks(txnListB2B, selectedTransactionMap, copyOfSelectedTransactionMap);
				} else {
					if (!this.fieldsVisibility.discountForCash) {
						this.fieldsVisibility.balance = false;
					} else {
						this.fieldsVisibility.balance = true;
					}
				}
				state.receivedLater = receivedLaterTotalAmount;

				var invoiceDate = oldTxnObj.getTxnDate();
				if (invoiceDate && invoiceDate instanceof Date && invoiceDate != 'Invalid Date') {
					invoiceDate = _MyDate2.default.getDate(DATEFORMATE, invoiceDate);
				} else {
					invoiceDate = '';
				}

				transaction = {
					cashSale: false,
					party: {
						value: _MyDouble2.default.convertStringToDouble(nameLogic.getNameId()),
						label: nameLogic.getFullName(),
						balance: _MyDouble2.default.getAmountWithDecimal(nameLogic.getAmount()),
						partyLogic: nameLogic
					},
					firm: firmCache.getFirmById(oldTxnObj.getFirmId()),
					invoiceType: oldTxnObj.getTxnSubType(),
					invoiceNo: oldTxnObj.getTxnRefNumber(),
					invoicePrefix: invoicePrefix,
					invoiceDate: invoiceDate,
					paymentType: oldTxnObj.getPaymentTypeId(),
					referenceNo: oldTxnObj.getPaymentTypeReference(),
					description: oldTxnObj.getDescription(),
					imageId: _MyDouble2.default.convertStringToDouble(oldTxnObj.getImageId()),
					imageBlob: oldTxnObj.getImageBlob(),
					image: '',
					extraFields: (0, _SalePurchaseHelpers.getExtraFields)(oldTxnObj.getUdfObjectArray(), txnType, firmId),
					discountPercent: 0,
					discountAmount: _MyDouble2.default.convertStringToDouble(oldTxnObj.getDiscountAmount()),
					taxPercent: _MyDouble2.default.convertStringToDouble(oldTxnObj.getTransactionTaxId()),
					taxAmount: _MyDouble2.default.convertStringToDouble(oldTxnObj.getTaxAmount()),
					ITC: oldTxnObj.getTxnITCApplicable(),
					additionalCharges: (0, _SalePurchaseHelpers.getAdditionalChargeFields)(oldTxnObj, txnType),
					roundOff: !!_MyDouble2.default.convertStringToDouble(oldTxnObj.getRoundOffValue()),
					roundOffAmount: _MyDouble2.default.convertStringToDouble(oldTxnObj.getRoundOffValue()),
					total: _MyDouble2.default.convertStringToDouble(oldTxnObj.getBalanceAmount()) ? _MyDouble2.default.convertStringToDouble(oldTxnObj.getBalanceAmount()) + _MyDouble2.default.convertStringToDouble(oldTxnObj.getCashAmount()) : 0 + _MyDouble2.default.convertStringToDouble(oldTxnObj.getCashAmount()),
					payable: 0,
					received: _MyDouble2.default.convertStringToDouble(oldTxnObj.getCashAmount()),
					balance: oldTxnObj.getBalanceAmount() ? _MyDouble2.default.convertStringToDouble(oldTxnObj.getBalanceAmount()) : 0,
					currentTxnBalance: _MyDouble2.default.convertStringToDouble(oldTxnObj.getTxnCurrentBalanceAmount()) || 0,
					txnSubType: oldTxnObj.getTxnSubType(),
					discountForCash: _MyDouble2.default.convertStringToDouble(oldTxnObj.getDiscountAmount()) || 0
				};

				if (selectedTransactionMap.size > 0 || !this.fieldsVisibility.discountForCash) {
					this.fieldsVisibility.balance = false;
				} else {
					this.fieldsVisibility.balance = true;
					transaction.balance = oldTxnObj.getTxnCurrentBalanceAmount() || 0;
				}

				this.fieldsVisibility.totalReceivedAmount = false;
				if (oldTxnObj.getTxnType() != txnType) {
					transaction.totalReceivedAmount = receivedLaterTotalAmount;
					transaction.received = 0;
					this.isConvert = true;
					this.fieldsVisibility.total = true;
					this.labels.balance = 'Balance';
					transaction.discountForCash = 0;
					transaction.total = transaction.currentTxnBalance;
					this.currentTxnBalance = transaction.currentTxnBalance;
					transaction.currentTxnBalance = 0;
					transaction.invoiceDate = _MyDate2.default.getDate(DATEFORMATE, new Date());
					transaction.image = '';
					transaction.imageBlob = '';
					transaction.imageId = '';
					transaction.taxAmount = 0;
					transaction.taxPercent = 0;
					transaction.roundOffAmount = 0;
					transaction.discountPercent = 0;
					transaction.discountAmount = 0;
					transaction.ITC = null;
					transaction.dueDate = null;

					var cashPaymentType = this.paymentTypeOptions.find(function (p) {
						return p.getName() === 'Cash';
					});
					transaction.referenceNo = '';
					if (cashPaymentType) {
						transaction.paymentType = cashPaymentType.getId();
					}

					/* invoice prefix */
					invoicePrefixOptions = (0, _SalePurchaseHelpers.getInvoicePrefixOptions)(firmId, txnType);
					transaction.invoicePrefix = '';
					invoicePrefixOptions.forEach(function (prefix) {
						if (_MyDouble2.default.convertStringToDouble(prefix.getIsDefault())) {
							transaction.invoicePrefix = prefix.getPrefixId();
						}
					});
					transaction.invoiceNo = (0, _SalePurchaseHelpers.getMaxInvoiceNumber)(transaction.firm.getFirmId(), txnType, transaction.invoicePrefix);
				}
			} else {
				// new transaction
				this.isFTU = !_FTUHelper2.default.isFirstSaleTxnCreated();
				if (txnObj) {
					this.oldTxnObj = txnObj;
				} else {
					this.oldTxnObj = transactionFactory.getTransactionObject(txnType);
				}
				var firm = firmCache.getDefaultFirm();
				var _firmId = firm.getFirmId();

				transaction.firm = firm;
				transaction.party = null;

				/* invoice prefix */
				invoicePrefixOptions = (0, _SalePurchaseHelpers.getInvoicePrefixOptions)(_firmId, txnType);
				invoicePrefixOptions.forEach(function (prefix) {
					if (_MyDouble2.default.convertStringToDouble(prefix.getIsDefault())) {
						transaction.invoicePrefix = prefix.getPrefixId();
					}
				});
				var invoiceNumber = (0, _SalePurchaseHelpers.getMaxInvoiceNumber)(firm.getFirmId(), txnType, transaction.invoicePrefix);
				transaction.invoiceNo = invoiceNumber;

				/* extra fields */
				transaction.extraFields = (0, _SalePurchaseHelpers.getExtraFields)(null, txnType, _firmId);

				/* custom fields */
				var firstPaymentType = this.paymentTypeOptions[0];
				transaction.paymentType = firstPaymentType && firstPaymentType.getId();

				/* additional charges */
				transaction.additionalCharges = (0, _SalePurchaseHelpers.getAdditionalChargeFields)(null, txnType);

				if (!this.fieldsVisibility.discountForCash && !transaction.discountForCash) {
					this.fieldsVisibility.balance = false;
				} else {
					this.fieldsVisibility.balance = true;
				}
				_analyticsHelper2.default.pushEvent('New ' + _TxnTypeConstant2.default.getTxnTypeForUI(txnType) + ' Open');
			}

			// fields visibility

			transaction = (0, _extends5.default)({}, transaction, state);

			this.fieldsVisibility.linkPayment = this.fieldsVisibility.linkPayment && transaction.party && transaction.party.value != 0 && (txnListB2B.size > 0 || selectedTransactionMap.size > 0);

			if (this.isConvert && (this.oldTxnObj.getTxnType() == _TxnTypeConstant2.default.TXN_TYPE_SALE || this.oldTxnObj.getTxnType() == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE)) {
				this.fieldsVisibility.linkPayment = false;
				this.fieldsVisibility.paymentHistory = false;
			}

			if (txnId && !this.isConvert) {
				this.isEdit = true;
			} else {
				this.isEdit = false;
			}

			this.setState({
				firmList: (0, _values2.default)(firmCache.getFirmList()),
				cashSaleOption: cashSaleOption,
				invoicePrefixOptions: invoicePrefixOptions,
				copyOfSelectedTransactionMap: copyOfSelectedTransactionMap,
				selectedTransactionMap: selectedTransactionMap,
				txnListB2B: txnListB2B,
				transaction: transaction
			});
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			var _this2 = this;

			$('#defaultPage').hide();
			window.onmessage = function () {
				var txnType = _this2.props.txnType;
				var transaction = _this2.state.transaction;

				if (event.data == 'increment invoice number') {
					if (!_this2.isEdit) {
						var firmCache = new _FirmCache2.default();
						firmCache.refreshFirmCache();
						var invoiceNo = (0, _SalePurchaseHelpers.getMaxInvoiceNumber)(transaction.firm.getFirmId(), txnType, transaction.invoicePrefix);
						_this2.setState({ transaction: (0, _extends5.default)({}, transaction, { invoiceNo: invoiceNo }) });
					}
				}
			};
			this.init();
		}
	}, {
		key: 'changeField',
		value: function changeField(name) {
			var _this3 = this;

			return function (event) {
				var transaction = _this3.state.transaction;

				var value = null;
				if (event.target) {
					value = event.target.value;
				} else {
					value = event;
				}
				if (name === 'image' && !value) {
					_this3.setState({
						transaction: (0, _extends5.default)({}, transaction, {
							image: '',
							imageBlob: ''
						})
					});
				} else {
					_this3.setState({
						transaction: (0, _extends5.default)({}, transaction, (0, _defineProperty3.default)({}, name, value))
					});
				}
			};
		}
	}, {
		key: 'changeFields',
		value: function changeFields(fields) {
			var transaction = this.state.transaction;

			this.setState({
				transaction: (0, _extends5.default)({}, transaction, fields)
			});
		}
	}, {
		key: 'changeParty',
		value: function changeParty(party) {
			var _state = this.state,
			    transaction = _state.transaction,
			    selectedTransactionMap = _state.selectedTransactionMap;
			var txnListB2B = this.state.txnListB2B;
			var txnType = this.props.txnType;

			var transactionState = {};
			if (selectedTransactionMap.size > 0 || settingCache.isBillToBillEnabled()) {
				txnListB2B = (0, _SalePurchaseHelpers.getTransactionsForB2BLinking)(party.value, transaction.firm.getFirmId(), txnType, selectedTransactionMap);
				this.fieldsVisibility.linkPayment = settingCache.isBillToBillEnabled() && !transaction.cashSale && party.value != 0 && txnListB2B.size > 0;
			}

			transactionState.party = party;
			var partyLogic = party.partyLogic || null;

			this.setState({
				transaction: (0, _extends5.default)({}, transaction, transactionState),
				txnListB2B: txnListB2B
			});
		}
	}, {
		key: 'changeFirm',
		value: function changeFirm(firm) {
			var _state2 = this.state,
			    transaction = _state2.transaction,
			    selectedTransactionMap = _state2.selectedTransactionMap;

			if (selectedTransactionMap.size > 0) {
				ToastHelper.error('You can not change firm when transaction is linked');
				return false;
			}
			var txnListB2B = this.state.txnListB2B;
			var txnType = this.props.txnType;

			var partyId = transaction.party && transaction.party.value ? transaction.party.value : 0;

			var firmId = firm.getFirmId();

			transaction.invoicePrefix = '';
			var invoicePrefixOptions = (0, _SalePurchaseHelpers.getInvoicePrefixOptions)(firmId, txnType);
			invoicePrefixOptions.forEach(function (prefix) {
				if (_MyDouble2.default.convertStringToDouble(prefix.getIsDefault())) {
					transaction.invoicePrefix = prefix.getPrefixId();
				}
			});
			transaction.invoiceNo = (0, _SalePurchaseHelpers.getMaxInvoiceNumber)(firmId, txnType, transaction.invoicePrefix);
			transaction.extraFields = (0, _SalePurchaseHelpers.getExtraFields)(this.oldTxnObj.getUdfObjectArray(), txnType, firm.getFirmId());
			transaction.firm = firm;

			if (selectedTransactionMap.size > 0 || settingCache.isBillToBillEnabled()) {
				txnListB2B = (0, _SalePurchaseHelpers.getTransactionsForB2BLinking)(partyId, firm.getFirmId(), txnType, selectedTransactionMap);

				this.fieldsVisibility.linkPayment = settingCache.isBillToBillEnabled() && !transaction.cashSale && partyId != 0 && txnListB2B.size > 0;
			}
			this.setState({ transaction: transaction, txnListB2B: txnListB2B, invoicePrefixOptions: invoicePrefixOptions });
		}
	}, {
		key: 'changePrefix',
		value: function changePrefix(e) {
			var transaction = this.state.transaction;
			var txnType = this.props.txnType;

			transaction.invoicePrefix = e.target.value;
			transaction.invoiceNo = (0, _SalePurchaseHelpers.getMaxInvoiceNumber)(transaction.firm.getFirmId(), txnType, transaction.invoicePrefix);
			this.setState({ transaction: transaction });
		}
	}, {
		key: 'changeExtraField',
		value: function changeExtraField(index, value) {
			var transaction = this.state.transaction;

			transaction.extraFields[index].value = value;
			this.setState({ transaction: transaction });
		}
	}, {
		key: 'changeCashSale',
		value: function changeCashSale(e) {
			var checked = e.target.checked;
			var _state3 = this.state,
			    transaction = _state3.transaction,
			    cashSaleOption = _state3.cashSaleOption;
			var txnType = this.props.txnType;

			if (!cashSaleOption) {
				var name = 'Cash Sale';
				var logic = this.createNewParty(name, txnType);
				cashSaleOption = {
					value: _MyDouble2.default.convertStringToDouble(logic.getNameId()),
					label: name,
					balance: _MyDouble2.default.getAmountWithDecimal(logic.getAmount()),
					partyLogic: logic
				};
				this.dropdownNames.push(cashSaleOption);
			}
			var state = {};
			if (checked) {
				state = (0, _extends5.default)({}, transaction, {
					cashSale: checked,
					received: transaction.total,
					balance: 0
				});
				if (!transaction.party) {
					state.party = cashSaleOption;
				}
			} else {
				state = (0, _extends5.default)({}, transaction, {
					cashSale: checked
				});
			}

			this.fieldsVisibility.linkPayment = settingCache.isBillToBillEnabled() && !state.cashSale;
			this.setState({ transaction: state, cashSaleOption: cashSaleOption });
		}
	}, {
		key: 'changeCheckbox',
		value: function changeCheckbox(name) {
			var _this4 = this;

			return function (event) {
				var transaction = _this4.state.transaction;

				var value = event.target.checked;
				_this4.setState({
					transaction: (0, _extends5.default)({}, transaction, (0, _defineProperty3.default)({}, name, value))
				});
			};
		}
	}, {
		key: 'toggleDescription',
		value: function toggleDescription() {
			var showDescription = this.state.showDescription;

			this.descriptionButtonClicked = true;
			this.setState({
				showDescription: !showDescription
			});
		}
	}, {
		key: 'changeImage',
		value: function changeImage() {
			var _this5 = this;

			var transaction = this.state.transaction;

			(0, _SalePurchaseHelpers.selectImage)(function (imageName, imagePath) {
				if (imagePath) {
					_this5.setState({
						transaction: (0, _extends5.default)({}, transaction, { image: imagePath, imageBlob: '' })
					});
				}
			});
		}
	}, {
		key: 'onClose',
		value: function onClose(event) {
			event.stopPropagation();
			if (canCloseDialogue()) {
				this.setState({ open: false });
				unmountReactComponent(document.querySelector('#salePurchaseContainer'));
			}
		}
	}, {
		key: 'onSave',
		value: function onSave(id) {
			var _this6 = this;

			if (!id) {
				return false;
			}
			this.saveAction = id;
			this.setState({
				saveInProgress: true,
				confirm: false
			}, function () {
				_this6.saveTransaction(true);
			});
		}
	}, {
		key: 'saveTransaction',
		value: function saveTransaction(allOK) {
			var _this7 = this;

			if (!allOK) {
				this.setState({
					confirm: false,
					saveInProgress: false
				});
				return false;
			}
			this.setState({
				confirm: false
			}, function () {
				var _props2 = _this7.props,
				    txnType = _props2.txnType,
				    txnId = _props2.txnId;

				var oldTxnObj = _this7.oldTxnObj;
				if (_this7.isEdit) {
					oldTxnObj = oldTxnObj.getTransactionFromId(oldTxnObj.getTxnId());
					var canEditTransaction = oldTxnObj.canEditTransaction();
					if (canEditTransaction != _ErrorCode2.default.SUCCESS) {
						ToastHelper.error(canEditTransaction);
						_this7.setState({
							saveInProgress: false
						});
						return;
					}
				}
				var _state4 = _this7.state,
				    _state4$transaction = _state4.transaction,
				    party = _state4$transaction.party,
				    firm = _state4$transaction.firm,
				    displayName = _state4$transaction.displayName,
				    billingAddress = _state4$transaction.billingAddress,
				    shippingAddress = _state4$transaction.shippingAddress,
				    invoiceType = _state4$transaction.invoiceType,
				    invoiceNo = _state4$transaction.invoiceNo,
				    invoicePrefix = _state4$transaction.invoicePrefix,
				    invoiceDate = _state4$transaction.invoiceDate,
				    paymentTerms = _state4$transaction.paymentTerms,
				    dueDate = _state4$transaction.dueDate,
				    txnReturnRefNumber = _state4$transaction.txnReturnRefNumber,
				    txnReturnRefDate = _state4$transaction.txnReturnRefDate,
				    placeOfSupply = _state4$transaction.placeOfSupply,
				    paymentType = _state4$transaction.paymentType,
				    referenceNo = _state4$transaction.referenceNo,
				    description = _state4$transaction.description,
				    imageId = _state4$transaction.imageId,
				    image = _state4$transaction.image,
				    imageBlob = _state4$transaction.imageBlob,
				    extraFields = _state4$transaction.extraFields,
				    taxPercent = _state4$transaction.taxPercent,
				    taxAmount = _state4$transaction.taxAmount,
				    ITC = _state4$transaction.ITC,
				    additionalCharges = _state4$transaction.additionalCharges,
				    roundOff = _state4$transaction.roundOff,
				    roundOffAmount = _state4$transaction.roundOffAmount,
				    total = _state4$transaction.total,
				    received = _state4$transaction.received,
				    balance = _state4$transaction.balance,
				    discountForCash = _state4$transaction.discountForCash,
				    cashSale = _state4$transaction.cashSale,
				    cashSaleOption = _state4.cashSaleOption,
				    transaction = _state4.transaction,
				    selectedTransactionMap = _state4.selectedTransactionMap,
				    invoicePrefixOptions = _state4.invoicePrefixOptions;


				try {
					if (cashSale) {
						if (!_MyDouble2.default.convertStringToDouble(party.value)) {
							if (!party.label) {
								if (!cashSaleOption) {
									var name = 'Cash Sale';
									var logic = _this7.createNewParty(name, txnType);
									_analyticsHelper2.default.pushEvent('Party save direct from sale - ' + txnType);
									cashSaleOption = {
										value: _MyDouble2.default.convertStringToDouble(logic.getNameId()),
										label: name,
										balance: _MyDouble2.default.getAmountWithDecimal(logic.getAmount()),
										partyLogic: logic
									};
									_this7.dropdownNames.push(cashSaleOption);
								}
								party = cashSaleOption;
							} else {
								var _name = party.label;
								var _logic = _this7.createNewParty(_name, txnType);
								_analyticsHelper2.default.pushEvent('Party save direct from sale - ' + txnType);
								party = {
									value: _MyDouble2.default.convertStringToDouble(_logic.getNameId()),
									label: _name,
									balance: _MyDouble2.default.getAmountWithDecimal(_logic.getAmount()),
									partyLogic: _logic
								};
								_this7.dropdownNames.push(party);
							}
						}
					} else {
						if (!_MyDouble2.default.convertStringToDouble(party.value)) {
							if (!party.label) {
								ToastHelper.error(_ErrorCode2.default.ERROR_NAME_EMPTY);
								var dropdown = document.querySelector('#salePurchasePartyDropdown');
								if (dropdown) {
									dropdown.focus();
								}
								_this7.setState({
									saveInProgress: false
								});
								return false;
							} else {
								var _name2 = party.label;
								var _logic2 = _this7.createNewParty(_name2, txnType);
								_analyticsHelper2.default.pushEvent('Party save direct from sale - ' + txnType);
								party = {
									value: _MyDouble2.default.convertStringToDouble(_logic2.getNameId()),
									label: _name2,
									balance: _MyDouble2.default.getAmountWithDecimal(_logic2.getAmount()),
									partyLogic: _logic2
								};
								_this7.dropdownNames.push(party);
							}
						}
					}
				} catch (e) {
					ToastHelper.error(_ErrorCode2.default.ERROR_NAME_DOESNT_EXIST);
					_this7.setState({
						saveInProgress: false
					});
					return false;
				}
				var additionalChargesMap = additionalCharges.reduce(function (all, charge) {
					all['ac' + charge.id + 'Amount'] = charge.value || 0;
					return all;
				}, {
					ac1Amount: 0,
					ac2Amount: 0,
					ac3Amount: 0
				});

				invoiceDate = invoiceDate ? _MyDate2.default.getDateObj(invoiceDate, DATE_FORMATE_FULL, '/') : null;
				if (invoiceDate instanceof Date && invoiceDate != 'Invalid Date') {
					invoiceDate.setHours(0, 0, 0, 0);
				} else {
					invoiceDate = '';
				}

				if (txnId) {
					(0, _SalePurchaseHelpers.additionalAdjustmentForReceivePayment)(txnType, transaction, selectedTransactionMap, _this7.oldTxnObj);
				}
				var txnCurrentBalance = transaction.txnCurrentBalance;
				if (selectedTransactionMap.size > 0) {
					txnCurrentBalance = _MyDouble2.default.convertStringToDouble(transaction.currentTxnBalance);
				} else {
					txnCurrentBalance = _MyDouble2.default.convertStringToDouble(total) + _MyDouble2.default.convertStringToDouble(discountForCash);
				}

				if (_MyDouble2.default.convertStringToDouble(txnCurrentBalance) < 0) {
					if (selectedTransactionMap.size > 0) {
						ToastHelper.error(_ErrorCode2.default.ERROR_TXN_LINKED_PAYMENTS_AMOUNT_NEGATIVE_CASHIN);
						_this7.setState({
							saveInProgress: false
						});
						return false;
					}
				}

				var today = new Date();
				today.setHours(0, 0, 0, 0);

				var obj = (0, _extends5.default)({
					txnType: txnType,
					name: party.label,
					imgFile: image || (imageBlob ? 'data:image/jpg;base64,' + imageBlob : ''),
					date: invoiceDate == '' ? today : invoiceDate,
					dueDate: dueDate,
					billNo: _this7.fieldsVisibility.invoiceNo ? invoiceNo : '',
					totalAmount: total,
					amount: received,
					balance: balance,
					paymentType: paymentType,
					discountAmount: discountForCash,
					taxAmount: taxAmount,
					transactionTaxId: taxPercent != 0 ? taxPercent : null,
					refNo: paymentType != 1 ? referenceNo : '',
					description: description,
					txnId: _this7.isEdit ? oldTxnObj.getTxnId() : ''
				}, additionalChargesMap, {
					prefix: _this7.fieldsVisibility.invoiceNo && _this7.fieldsVisibility.invoicePrefix ? invoicePrefix : null,
					invoicePrefixOptions: invoicePrefixOptions,
					firmId: firm.getFirmId(),
					invoiceType: invoiceType, // for updating firm table
					customFields: (0, _stringify2.default)({
						transportation_details: []
					}),
					displayName: settingCache.getDisplayNameEnabled() ? displayName : '',
					reverseCharge: 0,
					placeOfSupply: placeOfSupply,
					roundOffValue: roundOff ? roundOffAmount : 0,
					txnITCValue: ITC,
					txnReturnRefDate: txnReturnRefDate,
					txnReturnRefNumber: txnReturnRefNumber,
					txnCurrentBalance: txnCurrentBalance,
					selectedTxnMap: selectedTransactionMap && selectedTransactionMap.size > 0 ? selectedTransactionMap : new _map2.default(),
					paymentTermId: settingCache.isPaymentTermEnabled() && paymentTerms ? paymentTerms : 1,
					extraFieldValueArray: extraFields.map(function (field) {
						var udfField = new _udfValueModel2.default();
						var value = field.value;
						if (field.udfModel.getUdfFieldDataType() == udfConstants.DATA_TYPE_DATE) {
							if (value) {
								var format = field.udfModel.getUdfFieldDataFormat() == _StringConstants2.default.dateMonthYear ? 'dd/mm/yyyy' : 'mm/yyyy';
								var date = _MyDate2.default.getDateObj(value, format, '/');
								if (date && date instanceof Date && date != 'Invalid Date') {
									value = _MyDate2.default.getDate('y-m-d H:M:S', _MyDate2.default.getDateObj(value, format, '/'));
								} else {
									value = '';
								}
							}
						}
						udfField.setUdfFieldId(field.udfModel.getUdfFieldId());
						udfField.setUdfFieldValue(value);
						udfField.setUdfFieldType(udfConstants.FIELD_TYPE_TRANSACTION);
						return udfField;
					}),
					billingAddress: '',
					shippingAddress: ''
				});

				var TransactionLogic = require('../../../BizLogic/TransactionLogic.js');
				var transactionLogic = new TransactionLogic();

				var statusCode = void 0;
				if (_this7.isEdit) {
					obj.imageId = imageId || 0;
					statusCode = transactionLogic.UpdateTransactionIntr(obj, oldTxnObj, [], discountForCash);
				} else {
					statusCode = transactionLogic.SaveTransactionIntr(obj, _this7.oldTxnObj, [], discountForCash);
				}
				_this7.setState({
					saveInProgress: false
				});
				if (statusCode === _ErrorCode2.default.ERROR_TXN_SAVE_SUCCESS) {
					document.dispatchEvent(new CustomEvent('saveTxnEvent'));
					ToastHelper.success(statusCode);
					switch (_this7.saveAction) {
						case 'submit':
							if (oldTxnObj && oldTxnObj.getTxnType() == _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER && txnType == _TxnTypeConstant2.default.TXN_TYPE_SALE) {
								_analyticsHelper2.default.pushEvent('Sale Save - 1');
								_analyticsHelper2.default.pushEvent('Sale Order Convert to Sale Save - 1');
							} else {
								_analyticsHelper2.default.pushEvent('Add ' + _TxnTypeConstant2.default.getTxnTypeForUI(txnType) + ' Save - 1');
								_analyticsHelper2.default.pushEvent('Sale Order Convert to Sale Open - 1');
							}
							unmountReactComponent(document.querySelector('#salePurchaseContainer'));
							setTimeout(function () {
								if (!_this7.isEdit) {
									_this7.invoicePreviewOnSave(obj['txnId']);
								}
							});
							if (typeof onResume === 'function') {
								onResume(true, obj['name'].toLowerCase());
							}
							break;
						case 'submitNew':
							_analyticsHelper2.default.pushEvent('Add ' + _TxnTypeConstant2.default.getTxnTypeForUI(txnType) + ' Save - 1');
							unmountReactComponent(document.querySelector('#salePurchaseContainer'));
							setTimeout(function () {
								var MountComponent = require('../MountComponent').default;
								var Component = require('../SalePurchaseContainer/PaymentTransaction').default;
								MountComponent(Component, document.querySelector('#salePurchaseContainer'), {
									txnType: txnType,
									invoiceDate: obj.date
								});
							});
							if (typeof onResume === 'function') {
								onResume(true, obj['name'].toLowerCase());
							}
							// this.init();
							break;
						case 'submitPrint':
							var ThermalPrinterConstant = require('../../../Constants/ThermalPrinterConstant');
							if (settingCache.getDefaultPrinterType() == ThermalPrinterConstant.THERMAL_PRINTER) {
								var PrintUtil = require('../../../Utilities/PrintUtil');
								PrintUtil.printTransactionUsingThermalPrinter(obj['txnId']);
							} else {
								var TransactionHTMLGenerator = require('../../../ReportHTMLGenerator/TransactionHTMLGenerator');
								var transactionHTMLGenerator = new TransactionHTMLGenerator();
								var html = transactionHTMLGenerator.getTransactionHTML(obj['txnId']);
								if (html) {
									var PDFHandler = require('../../../Utilities/PDFHandler');
									var pdfHandler = new PDFHandler();
									pdfHandler.print(html);
								}
							}
							try {
								if (typeof onResume === 'function') {
									onResume(true, obj['name'].toLowerCase());
								}
							} catch (err) {
								logger.error(err);
							}

							_analyticsHelper2.default.pushEvent('Add ' + _TxnTypeConstant2.default.getTxnTypeForUI(txnType) + ' Save - 1');

							_analyticsHelper2.default.pushEvent(_TxnTypeConstant2.default.getTxnTypeForUI(txnType) + ' Print - 1');
							unmountReactComponent(document.querySelector('#salePurchaseContainer'));
							break;
						case 'submitSend':
							_this7.openPreview(obj['txnId']);
							_analyticsHelper2.default.pushEvent('Add ' + _TxnTypeConstant2.default.getTxnTypeForUI(txnType) + ' Save - 1');

							_analyticsHelper2.default.pushEvent(_TxnTypeConstant2.default.getTxnTypeForUI(txnType) + ' Preview - 1');
							unmountReactComponent(document.querySelector('#salePurchaseContainer'));
							if (typeof onResume === 'function') {
								onResume(true, obj['name'].toLowerCase());
							}
							break;
					}
				} else {
					ToastHelper.error(statusCode);
				}
			});
		}
	}, {
		key: 'openPreview',
		value: function openPreview(txnId) {
			var frameDiv = document.querySelector('#frameDiv');
			try {
				unmountReactComponent(frameDiv);
			} catch (ex) {}
			frameDiv.innerHTML = '\n\t\t\t<link rel="stylesheet" href="../styles/invoicePreview.css">\n\t\t\t<link rel="stylesheet" href="../styles/collectPayment.css">\n            <div id="invoicePreviewWrapper" style="width:100%;height: 100%;"></div>\n\t\t';

			var MountComponent = require('../MountComponent').default;
			var InvoicePreview = require('../InvoicePreview').default;
			this.showDefaultPage = false;
			setTimeout(function () {
				document.querySelector('#modelContainer').style.display = 'block';
				MountComponent(InvoicePreview, document.getElementById('invoicePreviewWrapper'), { transactionId: txnId });
			});
		}
	}, {
		key: 'invoicePreviewOnSave',
		value: function invoicePreviewOnSave(txnId) {
			var txnType = this.props.txnType;

			var oldTxnObj = this.oldTxnObj;
			try {
				var isPreviewDisabled = settingCache.getIsInvoicePreviewDisabled();
				if (!isPreviewDisabled && txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHIN && oldTxnObj && oldTxnObj.getTxnType() == txnType) {
					this.openPreview(txnId);
				}
			} catch (e) {}
		}
	}, {
		key: 'changeTransactionLinks',
		value: function changeTransactionLinks(txnListB2B, selectedTransactionMap, copyOfSelectedTransactionMap) {
			var transaction = this.state.transaction;

			var balance = transaction.balance;

			if (selectedTransactionMap.size > 0) {
				this.fieldsVisibility.received = false;
				this.fieldsVisibility.balance = false;

				this.fieldsVisibility.currentTxnBalance = true;
				this.fieldsVisibility.totalReceivedAmount = false;

				if (this.fieldsVisibility.linkPayment) {
					this.fieldsVisibility.paymentHistory = true;
				}
				this.fieldsVisibility.nameDisabled = false;

				this.labels.balance = 'Unused';
			} else {
				this.fieldsVisibility.received = true;
				this.fieldsVisibility.balance = true;

				this.fieldsVisibility.currentTxnBalance = false;

				this.fieldsVisibility.totalReceivedAmount = false;
				this.fieldsVisibility.paymentHistory = false;
				this.fieldsVisibility.nameDisabled = true;
				balance = _MyDouble2.default.convertStringToDouble(transaction.total) + _MyDouble2.default.convertStringToDouble(transaction.discountForCash);
				this.labels.balance = 'Total';
				this.fieldsVisibility.received = false;
				this.fieldsVisibility.totalDisabled = false;
				this.fieldsVisibility.total = true;
			}

			this.setState({
				transaction: (0, _extends5.default)({}, transaction, {
					balance: balance
				}),
				txnListB2B: txnListB2B,
				selectedTransactionMap: selectedTransactionMap,
				copyOfSelectedTransactionMap: copyOfSelectedTransactionMap
			});
		}
	}, {
		key: 'createParty',
		value: function createParty(item) {
			this.dropdownNames.push(item);
		}
	}, {
		key: 'render',
		value: function render() {
			var _this8 = this;

			var _props3 = this.props,
			    classes = _props3.classes,
			    txnType = _props3.txnType,
			    txnId = _props3.txnId;
			var _state5 = this.state,
			    firmList = _state5.firmList,
			    transaction = _state5.transaction,
			    invoicePrefixOptions = _state5.invoicePrefixOptions,
			    showDescription = _state5.showDescription,
			    selectedTransactionMap = _state5.selectedTransactionMap,
			    copyOfSelectedTransactionMap = _state5.copyOfSelectedTransactionMap,
			    txnListB2B = _state5.txnListB2B,
			    confirm = _state5.confirm,
			    saveInProgress = _state5.saveInProgress,
			    isLicenseExpired = _state5.isLicenseExpired,
			    showNewBankAccountDialog = _state5.showNewBankAccountDialog,
			    imageActionVisible = _state5.imageActionVisible,
			    renderForFirstAutofocus = _state5.renderForFirstAutofocus,
			    showImagePreview = _state5.showImagePreview;
			var cashSale = transaction.cashSale,
			    firm = transaction.firm,
			    party = transaction.party,
			    extraFields = transaction.extraFields,
			    invoiceType = transaction.invoiceType,
			    invoicePrefix = transaction.invoicePrefix,
			    invoiceNo = transaction.invoiceNo,
			    invoiceDate = transaction.invoiceDate,
			    description = transaction.description,
			    paymentType = transaction.paymentType,
			    referenceNo = transaction.referenceNo,
			    image = transaction.image,
			    imageBlob = transaction.imageBlob,
			    total = transaction.total,
			    totalReceivedAmount = transaction.totalReceivedAmount,
			    balance = transaction.balance,
			    currentTxnBalance = transaction.currentTxnBalance,
			    discountForCash = transaction.discountForCash;


			var isTotalError = !!(selectedTransactionMap.size > 0 && currentTxnBalance < 0);

			return _react2.default.createElement(
				_styles.MuiThemeProvider,
				{ theme: theme },
				confirm && _react2.default.createElement(_ConfirmModal2.default, { onClose: this.saveTransaction }),
				showNewBankAccountDialog && _react2.default.createElement(_AddNewBankAccountModal2.default, { onCreate: this.refreshBankAccounts }),
				showImagePreview && _react2.default.createElement(_ImagePreview2.default, {
					onClose: this.closeImagePreview,
					src: image || 'data:image/jpg;base64,' + imageBlob }),
				_react2.default.createElement(
					_Dialog2.default,
					{
						fullScreen: false,
						maxWidth: false,
						open: this.state.open,
						onClose: this.onClose,
						disableEnforceFocus: true,
						className: 'salePurchaseDialog vyaparFormToValidate'
					},
					_react2.default.createElement(
						'div',
						{
							id: 'SalePurchaseFormDialogRef',
							className: (0, _classnames2.default)(classes.salePurchaseContainer, classes.flex1, 'd-flex salePurchaseContainer flex-column', txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHOUT ? classes.cashInCashOutContainer : '')
						},
						isLicenseExpired && _react2.default.createElement(_LicenseExpiredModal2.default, { onClose: this.licenseExpired }),
						_react2.default.createElement(_nav2.default, {
							fieldsVisibility: this.fieldsVisibility,
							txnType: txnType,
							onClose: this.onClose,
							changeField: this.changeField,
							firmList: firmList,
							firm: firm,
							changeFirm: this.changeFirm,
							changeCashSale: this.changeCashSale,
							cashSale: cashSale,
							showCashSale: false,
							invoiceType: invoiceType,
							title: this.title
						}),
						_react2.default.createElement(
							'div',
							{
								className: (0, _classnames2.default)('d-flex justify-content-between', classes.content)
							},
							_react2.default.createElement(
								'div',
								{
									className: (0, _classnames2.default)(classes.leftContainer, 'd-flex justify-content-between')
								},
								_react2.default.createElement(
									'div',
									null,
									_react2.default.createElement(
										'div',
										{ className: classes.mb30 },
										_react2.default.createElement(_PartyDropdown2.default, {
											items: this.dropdownNames,
											label: _react2.default.createElement(
												'div',
												null,
												'Party ',
												_react2.default.createElement(
													'span',
													{ className: classes.mandatory },
													'*'
												)
											),
											addPartyLabel: this.labels.addPartyLabel,
											onChange: this.changeParty,
											className: (0, _classnames2.default)(classes.textFieldWidth230, this.fieldsVisibility.nameDisabled === false ? classes.bgGray : ''),
											onLoad: this.changePartyFocus,
											renderForFirstAutofocus: renderForFirstAutofocus,
											inputComponent: _TextField2.default,
											value: party,
											updateParty: this.updatePartyFunction,
											showFirstTimeAllOptions: true,
											showPartiesClicked: false,
											txnType: txnType,
											isEdit: this.isEdit,
											showBalance: Boolean(party && party.value && this.fieldsVisibility.partyBalance),
											createNewItem: this.saveNewParty,
											isDisabled: this.fieldsVisibility.nameDisabled === false
										})
									),
									_react2.default.createElement(
										'div',
										{ className: classes.mb15 },
										this.fieldsVisibility.paymentType && _react2.default.createElement(
											_react2.default.Fragment,
											null,
											_react2.default.createElement(
												'div',
												{
													className: (0, _classnames2.default)(classes.mb30, classes.positionRelative)
												},
												paymentType != 1 && _react2.default.createElement('div', { className: classes.connector }),
												_react2.default.createElement(
													_FormControl2.default,
													{
														variant: 'outlined',
														className: classes.formControl
													},
													_react2.default.createElement(
														_InputLabel2.default,
														{
															shrink: true,
															htmlFor: 'paymentTypeSelect'
														},
														'Payment Type'
													),
													_react2.default.createElement(
														_Select2.default,
														{
															native: true,
															value: paymentType,
															onChange: this.changePaymentType,
															input: _react2.default.createElement(_OutlinedInput2.default, {
																classes: { root: classes.inputRoot },
																className: classes.textFieldWidth230,
																name: 'paymentType',
																labelWidth: 100,
																id: 'paymentTypeSelect'
															})
														},
														this.paymentTypeOptions.map(function (type) {
															return _react2.default.createElement(
																'option',
																{ key: type.getId(), value: type.getId() },
																' ',
																type.getName()
															);
														}),
														_react2.default.createElement(
															'option',
															{ value: '' },
															'+ Add Bank A/C'
														)
													)
												)
											),
											paymentType != 1 && _react2.default.createElement(
												'div',
												{ className: classes.mb30 },
												_react2.default.createElement(_OutlineInput2.default, {
													className: classes.textFieldWidth230,
													label: 'Reference No.',
													value: referenceNo,
													onChange: this.changeField('referenceNo')
												})
											)
										),
										this.fieldsVisibility.description && _react2.default.createElement(
											_react2.default.Fragment,
											null,
											!showDescription && !description && _react2.default.createElement(
												'div',
												{ className: classes.mb30 },
												_react2.default.createElement(
													_Button2.default,
													{
														onFocus: this.toggleDescription,
														onClick: this.toggleDescription,
														variant: 'outlined',
														color: 'primary',
														className: classes.button
													},
													_react2.default.createElement(_NoteAdd2.default, {
														className: (0, _classnames2.default)(classes.leftIcon, classes.iconSmall)
													}),
													_react2.default.createElement(
														'span',
														{ className: classes.buttonText },
														'Add Description'
													)
												)
											),
											(showDescription || description) && _react2.default.createElement(
												'div',
												null,
												_react2.default.createElement(_OutlineInput2.default, {
													label: 'Description',
													value: description,
													multiline: true,
													autoFocus: this.descriptionButtonClicked,
													rows: 4,
													rowsMax: 4,
													className: classes.textArea,
													InputProps: {
														className: classes.textFieldWidth230,
														classes: {
															input: classes.textField,
															notchedOutline: classes.textArea
														}
													},
													onChange: this.changeField('description')
												})
											)
										),
										this.fieldsVisibility.image && _react2.default.createElement(
											_react2.default.Fragment,
											null,
											!image && !imageBlob && _react2.default.createElement(
												'label',
												{ htmlFor: 'icon-button-file' },
												_react2.default.createElement(
													_IconButton2.default,
													{
														onClick: this.changeImage,
														color: 'primary',
														className: classes.iconButton,
														component: 'span'
													},
													_react2.default.createElement(_AddAPhoto2.default, null)
												)
											),
											(image || imageBlob) && _react2.default.createElement(
												'div',
												{
													className: classes.imageContainer,
													onMouseOut: this.hideImageActions,
													onMouseOver: this.showImageActions
												},
												_react2.default.createElement('img', {
													className: classes.image,
													onClick: this.showImagePreview,
													src: image || 'data:image/jpg;base64,' + imageBlob
												}),
												_react2.default.createElement(
													_Slide2.default,
													{ direction: 'up', 'in': imageActionVisible },
													_react2.default.createElement(
														'div',
														{ onClick: function onClick(e) {
																return e.stopPropagation();
															}, className: classes.imageActions },
														_react2.default.createElement(
															'div',
															{
																className: classes.imageAction,
																onClick: this.changeImage
															},
															'change'
														),
														_react2.default.createElement(
															'div',
															{
																className: classes.imageAction,
																onClick: this.deleteImage
															},
															'delete'
														)
													)
												)
											)
										)
									)
								),
								_react2.default.createElement(
									'div',
									null,
									extraFields.map(function (field, index) {
										return _react2.default.createElement(
											_react.Fragment,
											{ key: field.udfModel.getUdfFieldId() },
											field.udfModel.getUdfFieldDataType() != udfConstants.DATA_TYPE_DATE && _react2.default.createElement(
												'div',
												{
													className: (0, _classnames2.default)(classes.mb30, classes.textFieldWidth230)
												},
												_react2.default.createElement(_OutlineInput2.default, {
													className: classes.textFieldWidth230,
													label: field.udfModel.getUdfFieldName(),
													value: field.value,
													onChange: function onChange(e) {
														return _this8.changeExtraField(index, e.target.value);
													}
												})
											),
											field.udfModel.getUdfFieldDataType() == udfConstants.DATA_TYPE_DATE && _react2.default.createElement(
												'div',
												{
													className: (0, _classnames2.default)(classes.mb30, classes.textFieldWidth230)
												},
												_react2.default.createElement(
													'div',
													{
														className: (0, _classnames2.default)(classes.datePickerWrapper)
													},
													_react2.default.createElement(_JqueryDatePicker2.default, {
														className: classes.textFieldWidth230,
														CustomInput: _OutlineInput2.default,
														label: field.udfModel.getUdfFieldName(),
														validate: true,
														onChange: function onChange(e) {
															return _this8.changeExtraField(index, typeof e === 'string' ? e : e.target.value);
														},
														format: field.udfModel.getUdfFieldDataFormat() == 1 ? 'dd/mm/yy' : 'mm/yy',
														value: field.value
													}),
													_react2.default.createElement(_DateRange2.default, {
														color: 'primary',
														onClick: _this8.setDatePickerFocus,
														fontSize: 'small',
														className: classes.PODateIcon
													})
												)
											)
										);
									})
								)
							),
							_react2.default.createElement(
								'div',
								{ className: classes.right },
								_react2.default.createElement(
									'div',
									{ className: classes.invoiceFieldsContainer },
									this.fieldsVisibility.invoiceNo && _react2.default.createElement(
										'div',
										{ className: (0, _classnames2.default)(classes.mb20, 'd-flex') },
										_react2.default.createElement(
											'div',
											{ className: classes.width100 },
											_react2.default.createElement(
												'label',
												{ className: classes.inlineLabel },
												this.labels.invoiceNo
											),
											' '
										),
										_react2.default.createElement(
											'div',
											{ className: (0, _classnames2.default)('d-flex', classes.width180) },
											this.fieldsVisibility.invoicePrefix && invoicePrefixOptions.length > 0 && _react2.default.createElement(
												'select',
												{
													className: classes.invoiceDropdown,
													value: invoicePrefix,
													onChange: this.changePrefix
												},
												_react2.default.createElement(
													'option',
													{ value: '' },
													'NONE'
												),
												invoicePrefixOptions.map(function (prefix) {
													return _react2.default.createElement(
														'option',
														{
															key: prefix.getPrefixId(),
															value: prefix.getPrefixId()
														},
														prefix.getPrefixValue()
													);
												})
											),
											_react2.default.createElement('input', {
												className: (0, _classnames2.default)(classes.inlineTextField, classes.invoiceNumber),
												value: invoiceNo,
												title: invoiceNo,
												onChange: this.changeInvoiceNo
											})
										)
									),
									this.fieldsVisibility.invoiceDate && _react2.default.createElement(
										'div',
										{ className: (0, _classnames2.default)(classes.mb20, 'd-flex') },
										_react2.default.createElement(
											'div',
											{ className: (0, _classnames2.default)(classes.width100) },
											_react2.default.createElement(
												'label',
												{ className: classes.inlineLabel },
												this.labels.invoiceDate
											)
										),
										_react2.default.createElement(
											'div',
											{ className: (0, _classnames2.default)(classes.height20) },
											_react2.default.createElement(_JqueryDatePicker2.default, {
												validate: true,
												className: (0, _classnames2.default)(classes.inlineTextField, classes.transparentDatepicker),
												value: invoiceDate,
												onChange: function onChange(e) {
													return _this8.changeInvoiceDate(typeof e === 'string' ? e : e.target.value);
												}
											}),
											_react2.default.createElement(_DateRange2.default, {
												onClick: this.setDatePickerFocus,
												className: classes.dateIcon
											})
										)
									),
									_react2.default.createElement(
										'div',
										{ className: classes.footerFields },
										this.fieldsVisibility.total && _react2.default.createElement(
											'div',
											{
												className: (0, _classnames2.default)(classes.footerFieldsContainer, 'align-items-center d-flex justify-content-between')
											},
											_react2.default.createElement(
												'div',
												{
													className: (0, _classnames2.default)(classes.inlineLabel, 'd-flex justify-content-between align-items-center')
												},
												_react2.default.createElement(
													'div',
													null,
													this.labels.total
												)
											),
											_react2.default.createElement(
												'div',
												{ className: classes.inlineTextFieldContainer },
												_react2.default.createElement('input', {
													onChange: this.changeTotal,
													value: total === 0 ? '' : total,
													className: (0, _classnames2.default)(classes.footerInlineTextField, isTotalError ? classes.errorTextField : ''),
													type: 'text'
												})
											)
										),
										this.fieldsVisibility.totalReceivedAmount && !this.isConvert && _react2.default.createElement(
											'div',
											{
												className: (0, _classnames2.default)(classes.footerFieldsContainer, 'align-items-center d-flex justify-content-between')
											},
											_react2.default.createElement(
												'div',
												{ className: classes.inlineLabel },
												this.labels.totalReceivedAmount
											),
											_react2.default.createElement(
												'div',
												{ className: classes.inlineTextFieldContainer },
												_react2.default.createElement('input', {
													disabled: true,
													value: totalReceivedAmount,
													className: classes.footerInlineTextField,
													type: 'text'
												})
											)
										),
										this.fieldsVisibility.discountForCash && _react2.default.createElement(
											'div',
											{
												className: (0, _classnames2.default)(classes.footerFieldsContainer, 'align-items-center d-flex justify-content-between')
											},
											_react2.default.createElement(
												'div',
												{ className: classes.inlineLabel },
												'Discount'
											),
											_react2.default.createElement(
												'div',
												{ className: classes.inlineTextFieldContainer },
												_react2.default.createElement('input', {
													onChange: this.changeDiscountForCash,
													value: discountForCash === 0 ? '' : discountForCash,
													className: classes.footerInlineTextField,
													type: 'text'
												})
											)
										),
										this.fieldsVisibility.balance && !isTotalError && !cashSale && _react2.default.createElement(
											'div',
											{
												className: (0, _classnames2.default)(classes.footerFieldsContainer, 'align-items-center d-flex justify-content-between')
											},
											_react2.default.createElement(
												'div',
												{
													className: (0, _classnames2.default)(classes.inlineLabel, classes.balance)
												},
												this.labels.balance
											),
											_react2.default.createElement(
												'div',
												{
													className: (0, _classnames2.default)(classes.inlineTextFieldContainer, classes.balanceColor)
												},
												balance
											)
										),
										this.fieldsVisibility.currentTxnBalance && !isTotalError && _react2.default.createElement(
											'div',
											{
												className: (0, _classnames2.default)(classes.footerFieldsContainer, 'align-items-center d-flex justify-content-between')
											},
											_react2.default.createElement(
												'div',
												{
													className: (0, _classnames2.default)(classes.footerInlineLabel, classes.balance)
												},
												this.labels.balance
											),
											_react2.default.createElement(
												'div',
												{
													className: (0, _classnames2.default)(classes.inlineTextFieldContainer, classes.balanceColor)
												},
												' ',
												Math.abs(currentTxnBalance)
											)
										)
									)
								)
							)
						),
						_react2.default.createElement(_footerActions2.default, {
							fieldsVisibility: this.fieldsVisibility,
							txnType: txnType,
							txnListB2B: txnListB2B,
							copyOfSelectedTransactionMap: copyOfSelectedTransactionMap,
							selectedTransactionMap: selectedTransactionMap,
							changeFields: this.changeFields,
							isConvert: this.isConvert,
							onSave: this.onSave,
							transaction: transaction,
							txnId: txnId,
							oldTxnObj: this.oldTxnObj,
							saveInProgress: saveInProgress,
							changeTransactionLinks: this.changeTransactionLinks
						})
					)
				)
			);
		}
	}]);
	return SalePurchaseContainer;
}(_react.Component);

SalePurchaseContainer.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	txnType: _propTypes2.default.number.isRequired,
	txnId: _propTypes2.default.number,
	txnObj: _propTypes2.default.object,
	invoiceDate: _propTypes2.default.instanceOf(Date)
};

exports.default = (0, _styles.withStyles)(styles)(SalePurchaseContainer);