Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/core/styles');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _ListItem = require('@material-ui/core/ListItem');

var _ListItem2 = _interopRequireDefault(_ListItem);

var _ListItemSecondaryAction = require('@material-ui/core/ListItemSecondaryAction');

var _ListItemSecondaryAction2 = _interopRequireDefault(_ListItemSecondaryAction);

var _ListItemText = require('@material-ui/core/ListItemText');

var _ListItemText2 = _interopRequireDefault(_ListItemText);

var _Menu = require('@material-ui/core/Menu');

var _Menu2 = _interopRequireDefault(_Menu);

var _IconButton = require('@material-ui/core/IconButton');

var _IconButton2 = _interopRequireDefault(_IconButton);

var _ExpandMore = require('@material-ui/icons/ExpandMore');

var _ExpandMore2 = _interopRequireDefault(_ExpandMore);

var _Print = require('@material-ui/icons/Print');

var _Print2 = _interopRequireDefault(_Print);

var _Reply = require('@material-ui/icons/Reply');

var _Reply2 = _interopRequireDefault(_Reply);

var _themes = require('../../../themes');

var _themes2 = _interopRequireDefault(_themes);

var _analyticsHelper = require('../../../Utilities/analyticsHelper');

var MyAnalytics = _interopRequireWildcard(_analyticsHelper);

var _LinkPaymentButton = require('../LinkPaymentButton');

var _LinkPaymentButton2 = _interopRequireDefault(_LinkPaymentButton);

var _PaymentHistoryButton = require('../PaymentHistoryButton');

var _PaymentHistoryButton2 = _interopRequireDefault(_PaymentHistoryButton);

var _TxnTypeConstant = require('../../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _TransactionHelper = require('../../../Utilities/TransactionHelper');

var TransactionHelper = _interopRequireWildcard(_TransactionHelper);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var theme = (0, _themes2.default)();

var styles = function styles() {
	return {
		footerActionContainer: {
			background: '#fff',
			height: 56,
			padding: 10,
			// position: 'fixed',
			minWidth: '100%',
			zIndex: 1,
			bottom: 0,
			borderTop: '1px solid #ddd'
		},
		mr18: {
			marginRight: 18
		},
		button: {
			whiteSpace: 'nowrap',
			padding: 0,
			paddingLeft: 15,
			height: 36
		},
		devider: {
			borderLeft: '1px solid rgba(23, 137, 252, 0.5)',
			height: 35,
			marginLeft: 10
		},
		dropdownIcon: {
			height: 34,
			width: 25,
			position: 'relative',
			left: 1,
			paddingRight: 1
		},
		left: {
			width: 400,
			marginLeft: 52
		},
		leftForPayment: {
			marginLeft: 15
		},
		right: {
			marginRight: 36
		},
		rightForPayment: {
			marginRight: 15
		},
		linkPaymentButton: {
			background: '#1BDA94',
			borderColor: '#1BDA94',
			color: '#fff',
			'&:hover': {
				background: '#1BDA94',
				borderColor: '#1BDA94'
			}
		},
		shareIcon: {
			transform: 'scaleX(-1)'
		},
		menuIcons: {
			right: 0
		},
		hiddenButtons: {
			visibility: 'hidden'
		},
		saveButton: {
			width: 120
		},
		colorRed: {
			color: 'red'
		},
		colorBlue: {
			color: 'blue'
		},
		underline: {
			textDecoration: 'underline'
		}
	};
};

var SalePurchaseFooterActions = function (_Component) {
	(0, _inherits3.default)(SalePurchaseFooterActions, _Component);

	function SalePurchaseFooterActions(props) {
		(0, _classCallCheck3.default)(this, SalePurchaseFooterActions);

		var _this = (0, _possibleConstructorReturn3.default)(this, (SalePurchaseFooterActions.__proto__ || (0, _getPrototypeOf2.default)(SalePurchaseFooterActions)).call(this, props));

		_this.onSave = function (e, id) {
			e.persist();
			e.stopPropagation();
			if (!id) {
				id = e.target.id || e.currentTarget.id;
			}
			var focusedElement = document.activeElement;
			if (focusedElement) {
				focusedElement.blur();
			}
			setTimeout(function () {
				_this.props.onSave(id);
			}, focusedElement ? 100 : 0);
		};

		var lastActionId = localStorage.getItem('SalePurchaseLastAction');
		var submitLabel = _react2.default.createElement('span', null);
		var dataShortcut = '';
		var classes = props.classes;
		if (props.fieldsVisibility.saveAndNewButton && lastActionId == 'submitNew') {
			submitLabel = _react2.default.createElement(
				'span',
				null,
				'Save & ',
				_react2.default.createElement(
					'span',
					{ className: classes.underline },
					'N'
				),
				'ew'
			);
			dataShortcut = 'CTRL_N';
		} else if (props.fieldsVisibility.saveAndPrintButton && lastActionId == 'submitPrint') {
			submitLabel = _react2.default.createElement(
				'span',
				null,
				_react2.default.createElement(
					'span',
					{ className: classes.underline },
					'P'
				),
				'rint'
			);
			dataShortcut = 'CTRL_P';
		} else {
			lastActionId = 'submitSend';
			submitLabel = _react2.default.createElement(
				'span',
				null,
				'Sha',
				_react2.default.createElement(
					'span',
					{ className: classes.underline },
					'r'
				),
				'e'
			);
			dataShortcut = 'CTRL_R';
		}
		_this.state = {
			anchorEl: null,
			lastActionId: lastActionId,
			submitLabel: submitLabel,
			dataShortcut: dataShortcut
		};
		_this.saveButtonRef = _react2.default.createRef();
		_this.handleClick = _this.handleClick.bind(_this);
		_this.handleClose = _this.handleClose.bind(_this);
		_this.openCalculator = _this.openCalculator.bind(_this);
		_this.openLinkedTransaction = _this.openLinkedTransaction.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(SalePurchaseFooterActions, [{
		key: 'openLinkedTransaction',
		value: function openLinkedTransaction() {
			var txnId = this.props.linkedTransactionId;
			var txnType = this.props.linkedtxnTransactionType;
			if (txnId && txnType) {
				TransactionHelper.viewTransaction(txnId, _TxnTypeConstant2.default.getTxnType(txnType));
			}
		}
	}, {
		key: 'handleClick',
		value: function handleClick(event) {
			event.stopPropagation();
			this.setState({ anchorEl: this.saveButtonRef.current });
		}
	}, {
		key: 'handleClose',
		value: function handleClose(e, reason) {
			e.persist();
			var classes = this.props.classes;

			var id = e.currentTarget.id;
			if (reason === 'escapeKeyDown' || e.currentTarget.classList.length === 0) {
				this.setState({
					anchorEl: null
				});
				return false;
			}
			if (!id) {
				return false;
			}
			var lastActionId = id;
			var submitLabel = '';
			var dataShortcut = '';
			if (lastActionId == 'submitNew') {
				submitLabel = _react2.default.createElement(
					'span',
					null,
					'Save & ',
					_react2.default.createElement(
						'span',
						{ className: classes.underline },
						'N'
					),
					'ew'
				);
				dataShortcut = 'CTRL_N';
			} else if (lastActionId == 'submitPrint') {
				submitLabel = _react2.default.createElement(
					'span',
					null,
					_react2.default.createElement(
						'span',
						{ className: classes.underline },
						'P'
					),
					'rint'
				);
				dataShortcut = 'CTRL_P';
			} else {
				lastActionId = 'submitSend';
				submitLabel = _react2.default.createElement(
					'span',
					null,
					'Sha',
					_react2.default.createElement(
						'span',
						{ className: classes.underline },
						'r'
					),
					'e'
				);
				dataShortcut = 'CTRL_R';
			}
			localStorage.setItem('SalePurchaseLastAction', id);
			this.setState({
				anchorEl: null,
				lastActionId: lastActionId,
				submitLabel: submitLabel,
				dataShortcut: dataShortcut
			});
			this.onSave(e, id);
		}
	}, {
		key: 'openCalculator',
		value: function openCalculator() {
			MyAnalytics.pushEvent('Open Calculator', {
				Action: 'Click'
			});

			var _require = require('electron'),
			    shell = _require.shell;

			shell.openItem('calc');
		}
	}, {
		key: 'render',
		value: function render() {
			var _props = this.props,
			    classes = _props.classes,
			    txnType = _props.txnType,
			    txnId = _props.txnId,
			    oldTxnObj = _props.oldTxnObj,
			    transaction = _props.transaction,
			    selectedTransactionMap = _props.selectedTransactionMap,
			    copyOfSelectedTransactionMap = _props.copyOfSelectedTransactionMap,
			    changeFields = _props.changeFields,
			    txnListB2B = _props.txnListB2B,
			    changeTransactionLinks = _props.changeTransactionLinks,
			    fieldsVisibility = _props.fieldsVisibility,
			    saveInProgress = _props.saveInProgress,
			    closedOn = _props.closedOn,
			    linkedTransactionInvoiceNumber = _props.linkedTransactionInvoiceNumber,
			    isConvert = _props.isConvert;
			var _state = this.state,
			    anchorEl = _state.anchorEl,
			    lastActionId = _state.lastActionId,
			    submitLabel = _state.submitLabel,
			    dataShortcut = _state.dataShortcut;

			var open = Boolean(anchorEl);

			return _react2.default.createElement(
				_styles.MuiThemeProvider,
				{ theme: theme },
				_react2.default.createElement(
					'div',
					{
						className: (0, _classnames2.default)(classes.footerActionContainer, 'd-flex', 'justify-content-between', 'align-items-center')
					},
					_react2.default.createElement(
						'div',
						{
							className: (0, _classnames2.default)(classes.left, txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHOUT ? classes.leftForPayment : '', 'd-flex align-items-center')
						},
						closedOn && _react2.default.createElement(
							'div',
							null,
							_react2.default.createElement(
								'span',
								{ className: classes.colorRed },
								closedOn
							),
							'\xA0',
							_react2.default.createElement(
								'a',
								{
									onClick: this.openLinkedTransaction,
									className: classes.colorBlue
								},
								linkedTransactionInvoiceNumber
							)
						),
						fieldsVisibility.linkPayment && _react2.default.createElement(_LinkPaymentButton2.default, {
							txnType: txnType,
							selectedTransactionMap: selectedTransactionMap,
							txnListB2B: txnListB2B,
							changeFields: changeFields,
							copyOfSelectedTransactionMap: copyOfSelectedTransactionMap,
							changeTransactionLinks: changeTransactionLinks,
							transaction: transaction,
							txnId: txnId,
							oldTxnObj: oldTxnObj
						}),
						selectedTransactionMap && selectedTransactionMap.size > 0 && !(isConvert && (txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHOUT)) && _react2.default.createElement(_PaymentHistoryButton2.default, {
							txnType: txnType,
							totalReceivedAmount: transaction.totalReceivedAmount,
							receivedLater: transaction.receivedLater,
							selectedTransactionMap: selectedTransactionMap
						})
					),
					_react2.default.createElement(
						'div',
						{
							className: (0, _classnames2.default)(classes.right, txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHOUT ? classes.rightForPayment : '', 'd-flex align-items-center justify-content-end')
						},
						_react2.default.createElement(
							'div',
							{ className: classes.hiddenButtons },
							_react2.default.createElement(
								_Button2.default,
								{
									id: 'submitSend',
									onClick: this.handleClose,
									'data-shortcut': 'CTRL_R'
								},
								''
							),
							fieldsVisibility.saveAndPrintButton && _react2.default.createElement(
								_Button2.default,
								{
									id: 'submitPrint',
									onClick: this.handleClose,
									'data-shortcut': 'CTRL_P'
								},
								''
							),
							fieldsVisibility.saveAndNewButton && _react2.default.createElement(
								_Button2.default,
								{
									id: 'submitNew',
									onClick: this.handleClose,
									'data-shortcut': 'CTRL_N'
								},
								''
							)
						),
						_react2.default.createElement(
							_Button2.default,
							{
								disabled: saveInProgress,
								'aria-owns': anchorEl ? 'action-menu' : undefined,
								id: lastActionId,
								'data-shortcut': dataShortcut,
								buttonRef: this.saveButtonRef,
								onClick: this.onSave,
								'aria-haspopup': 'true',
								variant: 'outlined',
								color: 'primary',
								className: (0, _classnames2.default)(classes.button, classes.mr18)
							},
							submitLabel,
							_react2.default.createElement('div', { className: classes.devider }),
							_react2.default.createElement(_ExpandMore2.default, {
								className: classes.dropdownIcon,
								onClick: this.handleClick
							})
						),
						!saveInProgress && _react2.default.createElement(
							_Menu2.default,
							{
								id: 'action-menu',
								keepMounted: true,
								anchorEl: anchorEl,
								open: open,
								onClose: this.handleClose
							},
							_react2.default.createElement(
								_ListItem2.default,
								{
									id: 'submitSend',
									'data-shortcut': 'CTRL_R',
									role: undefined,
									dense: true,
									button: true,
									onClick: this.handleClose
								},
								_react2.default.createElement(_ListItemText2.default, { primary: _react2.default.createElement(
										'span',
										null,
										'SHA',
										_react2.default.createElement(
											'span',
											{ className: classes.underline },
											'R'
										),
										'E'
									) }),
								_react2.default.createElement(
									_ListItemSecondaryAction2.default,
									{ className: classes.menuIcons },
									_react2.default.createElement(
										_IconButton2.default,
										{
											'aria-label': 'Share',
											onClick: this.handleClose,
											id: 'submitSend'
										},
										_react2.default.createElement(_Reply2.default, { className: classes.shareIcon })
									)
								)
							),
							fieldsVisibility.saveAndPrintButton && _react2.default.createElement(
								_ListItem2.default,
								{
									id: 'submitPrint',
									'data-shortcut': 'CTRL_P',
									role: undefined,
									dense: true,
									button: true,
									onClick: this.handleClose
								},
								_react2.default.createElement(_ListItemText2.default, { primary: _react2.default.createElement(
										'span',
										null,
										_react2.default.createElement(
											'span',
											{ className: classes.underline },
											'P'
										),
										'RINT'
									) }),
								_react2.default.createElement(
									_ListItemSecondaryAction2.default,
									{ className: classes.menuIcons },
									_react2.default.createElement(
										_IconButton2.default,
										{
											id: 'submitPrint',
											onClick: this.handleClose,
											'aria-label': 'PRINT'
										},
										_react2.default.createElement(_Print2.default, null)
									)
								)
							),
							fieldsVisibility.saveAndNewButton && _react2.default.createElement(
								_ListItem2.default,
								{
									id: 'submitNew',
									'data-shortcut': 'CTRL_N',
									role: undefined,
									dense: true,
									button: true,
									onClick: this.handleClose
								},
								_react2.default.createElement(_ListItemText2.default, { primary: _react2.default.createElement(
										'span',
										null,
										'SAVE & ',
										_react2.default.createElement(
											'span',
											{ className: classes.underline },
											'N'
										),
										'EW'
									) }),
								_react2.default.createElement(
									_ListItemSecondaryAction2.default,
									{ className: classes.menuIcons },
									_react2.default.createElement(
										_IconButton2.default,
										{
											id: 'submitNew',
											onClick: this.handleClose,
											centerRipple: 1,
											'aria-label': 'SAVE & NEW'
										},
										_react2.default.createElement(_ExpandMore2.default, null)
									)
								)
							)
						),
						_react2.default.createElement(
							_Button2.default,
							{
								disabled: saveInProgress,
								onClick: this.onSave,
								'data-shortcut': 'CTRL_S',
								id: 'submit',
								variant: 'contained',
								color: 'primary',
								className: classes.saveButton
							},
							_react2.default.createElement(
								'span',
								{ className: classes.underline },
								'S'
							),
							'ave'
						)
					)
				)
			);
		}
	}]);
	return SalePurchaseFooterActions;
}(_react.Component);

SalePurchaseFooterActions.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	txnType: _propTypes2.default.number.isRequired,
	fieldsVisibility: _propTypes2.default.object,
	onSave: _propTypes2.default.func,
	txnId: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
	oldTxnObj: _propTypes2.default.object,
	transaction: _propTypes2.default.object,
	selectedTransactionMap: _propTypes2.default.object,
	copyOfSelectedTransactionMap: _propTypes2.default.object,
	changeFields: _propTypes2.default.func,
	txnListB2B: _propTypes2.default.object,
	changeTransactionLinks: _propTypes2.default.func,
	saveInProgress: _propTypes2.default.bool,
	isConvert: _propTypes2.default.bool
};

exports.default = (0, _styles.withStyles)(styles)(SalePurchaseFooterActions);