Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Dialog = require('@material-ui/core/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _Slide = require('@material-ui/core/Slide');

var _Slide2 = _interopRequireDefault(_Slide);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/core/styles');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _SettingCache = require('../../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

var _themes = require('../../../themes');

var _themes2 = _interopRequireDefault(_themes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var settingCache = new _SettingCache2.default();
var theme = (0, _themes2.default)();

var styles = function styles() {
	return {
		salePurchaseContainer: {
			border: '1px solid #ddd',
			background: '#F2F4F8'
		},
		width1367: {
			width: 1367
		}
	};
};

function Transition(props) {
	return _react2.default.createElement(_Slide2.default, (0, _extends3.default)({ direction: 'up' }, props));
}

var SalePurchaseContainer = function (_Component) {
	(0, _inherits3.default)(SalePurchaseContainer, _Component);

	function SalePurchaseContainer(props) {
		(0, _classCallCheck3.default)(this, SalePurchaseContainer);

		var _this = (0, _possibleConstructorReturn3.default)(this, (SalePurchaseContainer.__proto__ || (0, _getPrototypeOf2.default)(SalePurchaseContainer)).call(this, props));

		_this.state = {};
		return _this;
	}

	(0, _createClass3.default)(SalePurchaseContainer, [{
		key: 'componentDidMount',
		value: function componentDidMount() {}
	}, {
		key: 'render',
		value: function render() {
			var _props = this.props,
			    classes = _props.classes,
			    open = _props.open,
			    onClose = _props.onClose,
			    txnType = _props.txnType;

			return _react2.default.createElement(
				_styles.MuiThemeProvider,
				{ theme: theme },
				_react2.default.createElement(
					_Dialog2.default,
					{
						open: open,
						onClose: onClose,
						TransitionComponent: Transition,
						className: 'itemStockTrackingDialog'
					},
					_react2.default.createElement(
						'div',
						{ className: (0, _classnames2.default)(classes.itemStockTrackingContainer) },
						_react2.default.createElement('div', { style: { overflow: 'auto' } })
					)
				)
			);
		}
	}]);
	return SalePurchaseContainer;
}(_react.Component);

SalePurchaseContainer.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	txnType: _propTypes2.default.number.isRequired,
	txnId: _propTypes2.default.number
};

exports.default = (0, _styles.withStyles)(styles)(SalePurchaseContainer);