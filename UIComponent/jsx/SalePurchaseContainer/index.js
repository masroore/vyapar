Object.defineProperty(exports, "__esModule", {
  value: true
});

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

var _extends4 = require('babel-runtime/helpers/extends');

var _extends5 = _interopRequireDefault(_extends4);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Dialog = require('@material-ui/core/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/core/styles');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _SettingCache = require('../../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

var _FirmCache = require('../../../Cache/FirmCache');

var _FirmCache2 = _interopRequireDefault(_FirmCache);

var _themes = require('../../../themes');

var _themes2 = _interopRequireDefault(_themes);

var _NameCache = require('../../../Cache/NameCache');

var _NameCache2 = _interopRequireDefault(_NameCache);

var _ItemCache = require('../../../Cache/ItemCache');

var _ItemCache2 = _interopRequireDefault(_ItemCache);

var _PaymentTermCache = require('../../../Cache/PaymentTermCache');

var _PaymentTermCache2 = _interopRequireDefault(_PaymentTermCache);

var _PaymentInfoCache = require('../../../Cache/PaymentInfoCache');

var _PaymentInfoCache2 = _interopRequireDefault(_PaymentInfoCache);

var _ItemUnitMappingCache2 = require('../../../Cache/ItemUnitMappingCache');

var _ItemUnitMappingCache3 = _interopRequireDefault(_ItemUnitMappingCache2);

var _TaxCodeCache = require('../../../Cache/TaxCodeCache');

var _TaxCodeCache2 = _interopRequireDefault(_TaxCodeCache);

var _StateCode = require('../../../Constants/StateCode');

var _StateCode2 = _interopRequireDefault(_StateCode);

var _TxnTypeConstant = require('../../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _TransactionStatus = require('../../../Constants/TransactionStatus');

var _TransactionStatus2 = _interopRequireDefault(_TransactionStatus);

var _ErrorCode = require('../../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

var _StringConstants = require('../../../Constants/StringConstants');

var _StringConstants2 = _interopRequireDefault(_StringConstants);

var _NameCustomerType = require('../../../Constants/NameCustomerType');

var NameCustomerType = _interopRequireWildcard(_NameCustomerType);

var _MyDouble = require('../../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _analyticsHelper = require('../../../Utilities/analyticsHelper');

var _analyticsHelper2 = _interopRequireDefault(_analyticsHelper);

var _MyDate = require('../../../Utilities/MyDate');

var _MyDate2 = _interopRequireDefault(_MyDate);

var _DataLoader = require('../../../DBManager/DataLoader');

var _DataLoader2 = _interopRequireDefault(_DataLoader);

var _TransactionFactory = require('../../../BizLogic/TransactionFactory');

var _TransactionFactory2 = _interopRequireDefault(_TransactionFactory);

var _udfValueModel = require('../../../Models/udfValueModel');

var _udfValueModel2 = _interopRequireDefault(_udfValueModel);

var _FTUHelper = require('../../../Utilities/FTUHelper');

var _FTUHelper2 = _interopRequireDefault(_FTUHelper);

var _LicenseUtility = require('../../../Utilities/LicenseUtility');

var _LicenseUtility2 = _interopRequireDefault(_LicenseUtility);

var _GSTRReportHelper = require('../../../UIControllers/GSTRReportHelper');

var _GSTRReportHelper2 = _interopRequireDefault(_GSTRReportHelper);

var _SalePurchaseHelpers = require('./SalePurchaseHelpers');

var _nav = require('./nav');

var _nav2 = _interopRequireDefault(_nav);

var _HeaderFields = require('./HeaderFields');

var _HeaderFields2 = _interopRequireDefault(_HeaderFields);

var _LineItemsContainer = require('./LineItemsContainer');

var _LineItemsContainer2 = _interopRequireDefault(_LineItemsContainer);

var _FooterFields = require('./FooterFields');

var _FooterFields2 = _interopRequireDefault(_FooterFields);

var _footerActions = require('./footerActions');

var _footerActions2 = _interopRequireDefault(_footerActions);

var _ConfirmModal = require('../ConfirmModal');

var _ConfirmModal2 = _interopRequireDefault(_ConfirmModal);

var _LowStockConfirmModal = require('../LowStockConfirmModal');

var _LowStockConfirmModal2 = _interopRequireDefault(_LowStockConfirmModal);

var _LicenseExpiredModal = require('../LicenseExpiredModal');

var _LicenseExpiredModal2 = _interopRequireDefault(_LicenseExpiredModal);

var _AddNewBankAccountModal = require('../AddNewBankAccountModal');

var _AddNewBankAccountModal2 = _interopRequireDefault(_AddNewBankAccountModal);

var _ToastHelper = require('../../../UIControllers/ToastHelper');

var ToastHelper = _interopRequireWildcard(_ToastHelper);

var _UDFFieldsConstant = require('../../../Constants/UDFFieldsConstant');

var udfConstants = _interopRequireWildcard(_UDFFieldsConstant);

var _ItemType = require('../../../Constants/ItemType');

var ItemType = _interopRequireWildcard(_ItemType);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import QuickEntry from './QuickEntry';
var DATEFORMATE = 'd/m/y';
var DATE_FORMATE_FULL = 'dd/mm/yyyy';

var settingCache = new _SettingCache2.default();
var theme = (0, _themes2.default)();

var listOfStates = _StateCode2.default.getStateList();

var styles = function styles() {
  return {
    salePurchaseContainer: {
      border: '1px solid #ddd',
      background: '#FCFCFC'
    },
    root: {
      top: '30px !important',
      overflow: 'hidden'
    },
    flex1: {
      flex: 1
    },
    width1367: {
      minWidth: 1024
    },
    overflowAuto: {
      overflow: 'auto'
    }
  };
};

var SalePurchaseContainer = function (_Component) {
  (0, _inherits3.default)(SalePurchaseContainer, _Component);

  function SalePurchaseContainer(props) {
    (0, _classCallCheck3.default)(this, SalePurchaseContainer);

    var _this = (0, _possibleConstructorReturn3.default)(this, (SalePurchaseContainer.__proto__ || (0, _getPrototypeOf2.default)(SalePurchaseContainer)).call(this, props));

    _this.changeLineItemVisibility = function (key, checked) {
      _this.lineItemColumnsVisibility[key] = checked;
      if (key == 'description' && !checked) {
        var lineItems = _this.state.lineItems;

        var modifiedLineItems = lineItems.map(function (lineItem) {
          lineItem.description = '';
          return lineItem;
        });
        _this.setState({ lineItems: modifiedLineItems });
      } else if (key == 'discount' && !checked) {
        var _this$state = _this.state,
            _lineItems = _this$state.lineItems,
            transaction = _this$state.transaction;
        var txnType = _this.props.txnType;

        var _modifiedLineItems = _lineItems.map(function (lineItem) {
          lineItem.discountPercent = 0;
          lineItem.discountAmount = 0;
          return (0, _SalePurchaseHelpers.changeDiscountFields)((0, _extends5.default)({}, lineItem, { uniqueId: (0, _SalePurchaseHelpers.getUniqueKey)() }), false, txnType, transaction.taxInclusive);
        });
        _this.renderLineItems(_modifiedLineItems);
      } else {
        _this.forceUpdate();
      }
    };

    _this.setTaxZero = function (partyLogic) {
      var txnType = _this.props.txnType;

      var isCompositeEnabled = settingCache.isCompositeSchemeEnabled();
      var shouldSetTaxZero = false;
      var isPartyComposite = (partyLogic && partyLogic.getCustomerType()) == NameCustomerType.REGISTERED_COMPOSITE;
      if (isCompositeEnabled && isPartyComposite) {
        shouldSetTaxZero = true;
      } else if (isCompositeEnabled) {
        switch (txnType) {
          case _TxnTypeConstant2.default.TXN_TYPE_SALE:
          case _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN:
          case _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER:
          case _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE:
          case _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN:
            shouldSetTaxZero = true;
            break;
        }
      } else if (isPartyComposite) {
        switch (txnType) {
          case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE:
          case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN:
          case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER:
            shouldSetTaxZero = true;
            break;
        }
      }
      return shouldSetTaxZero;
    };

    _this.changePlaceOfSupply = function (placeOfSupply) {
      var _this$state2 = _this.state,
          transaction = _this$state2.transaction,
          lineItems = _this$state2.lineItems;
      var _this$props = _this.props,
          txnId = _this$props.txnId,
          txnType = _this$props.txnType;

      if (!_this.fieldsVisibility.lineItems) {
        _this.setState({
          transaction: (0, _extends5.default)({}, transaction, {
            placeOfSupply: placeOfSupply
          })
        });
        return false;
      }
      var partyLogic = transaction.party && transaction.party.partyLogic ? transaction.party.partyLogic : null;
      _this.taxDropdownList = (0, _SalePurchaseHelpers.getLineItemTaxDropdownList)({
        txnType: txnType,
        txnId: txnId,
        firmLogic: transaction.firm,
        placeOfSupply: placeOfSupply,
        partyLogic: partyLogic
      });

      var taxPercent = (0, _SalePurchaseHelpers.getCorrespondingIdOfTaxRate)({
        taxId: transaction.taxPercent,
        txnType: txnType,
        partyLogic: partyLogic,
        firmLogic: transaction.firm,
        placeOfSupply: placeOfSupply
      });

      _this.transactionTaxDropdownList = (0, _SalePurchaseHelpers.getLineItemTaxDropdownList)({
        txnType: txnType,
        txnId: txnId,
        firmLogic: transaction.firm,
        placeOfSupply: placeOfSupply,
        partyLogic: partyLogic,
        currentTaxCodeId: [_MyDouble2.default.convertStringToDouble(taxPercent)]
      });
      var modifiedLineItems = lineItems.map(function (lineItem) {
        lineItem.uniqueId = (0, _SalePurchaseHelpers.getUniqueKey)();
        lineItem.taxPercent = (0, _SalePurchaseHelpers.getCorrespondingIdOfTaxRate)({
          taxId: lineItem.taxPercent,
          txnType: txnType,
          partyLogic: partyLogic,
          firmLogic: transaction.firm,
          placeOfSupply: placeOfSupply
        });
        return lineItem;
      });
      _this.setState({
        transaction: (0, _extends5.default)({}, transaction, {
          taxPercent: taxPercent,
          placeOfSupply: placeOfSupply
        })
      }, function () {
        return _this.renderLineItems(modifiedLineItems);
      });
    };

    _this.changePaymentType = function (event) {
      var transaction = _this.state.transaction;

      var value = null;
      if (event && event.target) {
        value = event.target.value;
      }
      if (value) {
        _this.setState({
          transaction: (0, _extends5.default)({}, transaction, { paymentType: value })
        });
      } else {
        _this.setState({ showNewBankAccountDialog: true });
      }
    };

    _this.refreshBankAccounts = function (createdBankAccount) {
      var transaction = _this.state.transaction;

      if (createdBankAccount) {
        var value = transaction.paymentType;
        var paymentInfoCache = new _PaymentInfoCache2.default();
        _this.paymentTypeOptions = paymentInfoCache.getListOfPaymentAccountObjects();
        var found = _this.paymentTypeOptions.find(function (p) {
          return p.getName() === createdBankAccount;
        });
        if (found) {
          value = found.getId();
        }
        _this.setState({
          transaction: (0, _extends5.default)({}, transaction, { paymentType: value })
        });
      }
      _this.setState({ showNewBankAccountDialog: false });
    };

    _this.renderLineItems = function (modifiedLineItems) {
      var shouldSetTaxZero = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      var found = modifiedLineItems.find(function (item) {
        return item.item && item.item.label && item.item.label.trim() !== '';
      });
      if (!found) {
        return false;
      }
      var _this$state3 = _this.state,
          transaction = _this$state3.transaction,
          selectedTransactionMap = _this$state3.selectedTransactionMap;
      var txnType = _this.props.txnType;


      var total = _this.calculateTotal(modifiedLineItems);

      var discountPercent = transaction.discountPercent,
          roundOff = transaction.roundOff,
          roundOffAmount = transaction.roundOffAmount,
          isReverseCharge = transaction.isReverseCharge,
          additionalCharges = transaction.additionalCharges,
          cashSale = transaction.cashSale,
          received = transaction.received,
          discountForCash = transaction.discountForCash,
          receivedLater = transaction.receivedLater,
          taxPercent = transaction.taxPercent;


      var discountAmount = (0, _SalePurchaseHelpers.changeTotalDiscountPercent)(total.subTotal, discountPercent);

      var tax = (0, _SalePurchaseHelpers.changeTotalTaxPercent)(txnType, total.subTotal, discountAmount, transaction.taxPercent);

      var newReverseChargeAmount = isReverseCharge ? _MyDouble2.default.convertStringToDouble(total.lineItemTotalTax + tax.taxAmount) : 0;

      var newTransaction = (0, _SalePurchaseHelpers.changeTotalAndBalWithDiscountView)({
        subTotal: total.subTotal,
        discountAmount: discountAmount,
        discountPercent: discountPercent,
        taxAmount: tax.taxAmount,
        roundOff: roundOff,
        roundOffAmount: roundOffAmount,
        isReverseCharge: isReverseCharge,
        reverseChargeAmount: newReverseChargeAmount,
        additionalCharges: additionalCharges,
        cashSale: cashSale,
        received: received,
        roundOffChanged: false,
        ITCVisibility: false
      });

      if (selectedTransactionMap.size > 0) {
        newTransaction = (0, _SalePurchaseHelpers.calculateCurrentTransactionAmount)(txnType, (0, _extends5.default)({}, newTransaction, {
          receivedLater: receivedLater,
          isReverseCharge: isReverseCharge,
          discountForCash: discountForCash
        }));
      }
      var state = (0, _extends5.default)({
        transaction: (0, _extends5.default)({}, transaction, newTransaction, {
          taxPercent: taxPercent,
          taxAmount: tax.taxAmount,
          discountAmount: discountAmount,
          reverseChargeAmount: newReverseChargeAmount
        })
      }, total, {
        lineItems: modifiedLineItems
      });
      _this.setState((0, _extends5.default)({}, state));
    };

    _this.createNewParty = function (name, txnType) {
      var nameCache = new _NameCache2.default();
      var success = (0, _SalePurchaseHelpers.saveNewName)(name, txnType);
      if (success) {
        if (txnType === _TxnTypeConstant2.default.TXN_TYPE_OTHER_INCOME) {
          return nameCache.findIncomeModelByName(name);
        } else if (txnType === _TxnTypeConstant2.default.TXN_TYPE_EXPENSE) {
          return nameCache.findExpenseModelByName(name);
        } else {
          return nameCache.findNameModelByName(name);
        }
      } else {
        return false;
      }
    };

    _this.calculateFooterFields = function () {
      var _this$state4 = _this.state,
          lineItems = _this$state4.lineItems,
          transaction = _this$state4.transaction,
          selectedTransactionMap = _this$state4.selectedTransactionMap;
      var txnType = _this.props.txnType;

      var total = _this.calculateTotal(lineItems);

      var discAmount = (0, _SalePurchaseHelpers.changeTotalDiscountPercent)(total.subTotal, transaction.discountPercent);
      var tax = (0, _SalePurchaseHelpers.changeTotalTaxPercent)(txnType, total.subTotal, discAmount, transaction.taxPercent);

      if (transaction.isReverseCharge) {
        transaction.reverseChargeAmount = _MyDouble2.default.getAmountWithDecimal(total.lineItemTotalTax + tax.taxAmount);
      } else {
        transaction.reverseChargeAmount = 0;
      }

      var newTransaction = (0, _SalePurchaseHelpers.changeTotalAndBalWithDiscountView)({
        subTotal: total.subTotal,
        discountAmount: discAmount,
        discountPercent: transaction.discountPercent,
        taxAmount: tax.taxAmount,
        roundOff: transaction.roundOff,
        roundOffAmount: transaction.roundOffAmount,
        isReverseCharge: transaction.isReverseCharge,
        reverseChargeAmount: transaction.reverseChargeAmount,
        additionalCharges: transaction.additionalCharges,
        cashSale: transaction.cashSale,
        received: transaction.received,
        roundOffChanged: false,
        ITCVisibility: tax.ITCVisibility
      });

      if (selectedTransactionMap.size > 0) {
        newTransaction = (0, _SalePurchaseHelpers.calculateCurrentTransactionAmount)(txnType, (0, _extends5.default)({}, newTransaction, {
          receivedLater: transaction.receivedLater,
          isReverseCharge: transaction.isReverseCharge,
          discountForCash: transaction.discountForCash
        }));
      }

      _this.setState((0, _extends5.default)({
        lineItems: lineItems
      }, total, {
        transaction: (0, _extends5.default)({}, transaction, {
          discountAmount: discAmount,
          taxAmount: tax.taxAmount
        }, newTransaction)
      }));
    };

    _this.licenseExpired = function () {
      _this.setState({ isLicenseExpired: false }, function () {
        unmountReactComponent(document.querySelector('#salePurchaseContainer'));
      });
    };

    _this.toggleTaxInclusive = function (flag) {
      var _this$state5 = _this.state,
          transaction = _this$state5.transaction,
          lineItems = _this$state5.lineItems;
      var txnType = _this.props.txnType;

      _this.setState({ transaction: (0, _extends5.default)({}, transaction, { taxInclusive: flag }) }, function () {
        var modifiedLineItems = lineItems.map(function (lineItem) {
          return (0, _SalePurchaseHelpers.changeDiscountFields)((0, _extends5.default)({}, lineItem, { uniqueId: (0, _SalePurchaseHelpers.getUniqueKey)() }), false, txnType, flag);
        });
        _this.renderLineItems(modifiedLineItems);
      });
    };

    _this.state = {
      open: true,
      showLowStockDialog: false,
      firmList: [],
      lineItems: [],
      confirm: false,
      quickEntryOpen: false,
      showDescription: false,
      showReverseCharge: false,
      showNewBankAccountDialog: false,
      showCashSale: false,
      cashSaleOption: {},
      invoicePrefixOptions: [],
      paymentTermsOptions: [],
      partyAmount: 0,
      subTotal: 0,
      lineItemTotalTax: 0,
      totalCount: 0,
      totalQty: 0,
      totalFreeQty: 0,
      txnListB2B: new _map2.default(),
      selectedTransactionMap: new _map2.default(),
      copyOfSelectedTransactionMap: new _map2.default(),
      closedOn: '',
      linkedTransactionInvoiceNumber: '',
      showErrorForTotal: false,
      saveInProgress: false,
      isLicenseExpired: !_LicenseUtility2.default.isLicenseValid(),
      transaction: {
        cashSale: false,
        party: '',
        firm: '',
        displayName: '',
        billingAddress: '',
        shippingAddress: '',
        PONo: '',
        PODate: '',
        eWayBillNo: '',
        invoiceType: 1,
        invoiceNo: '',
        invoicePrefix: '',
        invoiceDate: props.invoiceDate ? _MyDate2.default.getDate(DATEFORMATE, props.invoiceDate) : _MyDate2.default.getDate(DATEFORMATE, new Date()),
        paymentTerms: '',
        dueDate: '',
        dueDays: 0,
        txnReturnRefNumber: '',
        txnReturnRefDate: '',
        placeOfSupply: '',
        paymentType: '',
        referenceNo: '',
        description: '',
        imageId: '',
        image: '',
        imageBlob: '',
        extraFields: [],
        transportationDetails: [],
        discountPercent: 0,
        discountAmount: 0,
        taxPercent: 0,
        taxAmount: 0,
        ITCVisibility: false,
        ITC: 0,
        additionalCharges: [],
        roundOff: false,
        roundOffAmount: 0,
        isReverseCharge: false,
        reverseChargeAmount: 0,
        total: 0,
        payable: 0,
        received: 0,
        totalReceivedAmount: 0,
        receivedLater: 0,
        balance: 0,
        currentTxnBalance: 0,
        discountForCash: 0,
        taxInclusive: ItemType.ITEM_TXN_TAX_EXCLUSIVE
      }
    };

    _this.init = _this.init.bind(_this);
    _this.onClose = _this.onClose.bind(_this);
    _this.changeCashSale = _this.changeCashSale.bind(_this);
    _this.changeReverseCharge = _this.changeReverseCharge.bind(_this);
    _this.toggleQuickEntry = _this.toggleQuickEntry.bind(_this);
    _this.changeLineItem = _this.changeLineItem.bind(_this);
    _this.changeParty = _this.changeParty.bind(_this);
    _this.changeField = _this.changeField.bind(_this);
    _this.changeFields = _this.changeFields.bind(_this);
    _this.changeCheckbox = _this.changeCheckbox.bind(_this);
    _this.changeFirm = _this.changeFirm.bind(_this);
    _this.changePrefix = _this.changePrefix.bind(_this);
    _this.changeDueDate = _this.changeDueDate.bind(_this);
    _this.changePaymentTerms = _this.changePaymentTerms.bind(_this);
    _this.changeExtraField = _this.changeExtraField.bind(_this);
    _this.changeTransportationField = _this.changeTransportationField.bind(_this);
    _this.toggleDescription = _this.toggleDescription.bind(_this);
    _this.changeImage = _this.changeImage.bind(_this);
    _this.onSave = _this.onSave.bind(_this);
    _this.saveTransaction = _this.saveTransaction.bind(_this);
    _this.changeTransactionLinks = _this.changeTransactionLinks.bind(_this);
    _this.createItem = _this.createItem.bind(_this);
    _this.createParty = _this.createParty.bind(_this);
    _this.addLineItem = _this.addLineItem.bind(_this);
    _this.deleteLineItem = _this.deleteLineItem.bind(_this);

    _this.fieldsVisibility = {};
    _this.lineItemColumnsVisibility = {};
    _this.labels = {
      name: 'NAME',
      received: 'RECEIVED',
      total: 'TOTAL',
      invoiceNo: 'Invoice Number',
      invoiceDate: 'Invoice Date'
    };
    _this.currencySymbol = settingCache.getCurrencySymbol();
    _this.isFTU = !_FTUHelper2.default.isFirstSaleTxnCreated();
    _this.showDefaultPage = true;
    return _this;
  }

  (0, _createClass3.default)(SalePurchaseContainer, [{
    key: 'getDefaultLineItem',
    value: function getDefaultLineItem() {
      return {
        item: {},
        description: '',
        count: '',
        slNo: '',
        batchNo: '',
        expDate: '',
        mfgDate: '',
        mrp: '',
        size: '',
        qty: '',
        freeQty: '',
        priceUnit: '',
        unit: 0,
        unitObj: [],
        discountPercent: '',
        discountAmount: '',
        taxPercent: '',
        taxAmount: '',
        cess: '',
        amount: '',
        istId: 0,
        oldTaxId: '',
        uniqueId: (0, _SalePurchaseHelpers.getUniqueKey)()
      };
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      if (this.showDefaultPage) {
        $('#defaultPage').show();
        setTimeout(function () {
          document.dispatchEvent(new CustomEvent('resize-grid'));
        }, 100);
      }
      window.onmessage = null;
    }
  }, {
    key: 'componentDidCatch',
    value: function componentDidCatch(error, errorInfo) {
      var saveInProgress = this.state.saveInProgress;

      if (saveInProgress) {
        this.setState({
          saveInProgress: false
        });
      }
      window.logger && window.logger.error(error, errorInfo);
    }
  }, {
    key: 'init',
    value: function init() {
      var _this2 = this;

      var _state = this.state,
          transaction = _state.transaction,
          lineItems = _state.lineItems;
      var _props = this.props,
          txnType = _props.txnType,
          txnId = _props.txnId,
          txnObj = _props.txnObj;

      var firmCache = new _FirmCache2.default();
      var nameCache = new _NameCache2.default();
      var itemCache = new _ItemCache2.default();
      var paymentTermCache = new _PaymentTermCache2.default();
      var paymentInfoCache = new _PaymentInfoCache2.default();
      var dataLoader = new _DataLoader2.default();
      var state = {};
      var lineItemColumnsVisibility = (0, _SalePurchaseHelpers.getLineItemSettings)(txnType, this.isFTU);

      var _getTransactionSettin = (0, _SalePurchaseHelpers.getTransactionSettings)(txnType, txnId, transaction),
          labels = _getTransactionSettin.labels,
          fieldsVisibility = _getTransactionSettin.fieldsVisibility;

      this.labels = labels;
      this.fieldsVisibility = fieldsVisibility;
      this.lineItemColumnsVisibility = lineItemColumnsVisibility;

      var selectedTransactionMap = new _map2.default();
      var copyOfSelectedTransactionMap = new _map2.default();
      var txnListB2B = new _map2.default();

      var showCashSale = settingCache.getDefaultCashSaleEnabled() && txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE;
      var showReverseCharge = false;

      if (txnType === _TxnTypeConstant2.default.TXN_TYPE_EXPENSE) {
        this.allNames = nameCache.getListOfExpenseObject();
        this.allItems = itemCache.getListOfExpenseObject();
      } else if (txnType === _TxnTypeConstant2.default.TXN_TYPE_OTHER_INCOME) {
        this.allNames = nameCache.getListOfIncomeObject();
        this.allItems = itemCache.getListOfIncomeObject();
      } else {
        this.allNames = nameCache.getListOfNamesObject();
        this.allItems = itemCache.getListOfItemsAndServiceObject();
      }
      var cashSaleOption = null;
      if (txnType === _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE) {
        this.title = 'Estimate/Quotation';
      } else {
        this.title = _TxnTypeConstant2.default.getTxnTypeForUI(txnType) || '';
      }
      this.dropdownNames = this.allNames.map(function (logic) {
        var fullName = logic.getFullName();
        var option = {
          value: _MyDouble2.default.convertStringToDouble(logic.getNameId()),
          label: fullName,
          balance: _MyDouble2.default.getAmountWithDecimal(logic.getAmount()),
          partyLogic: logic
        };
        if (fullName === _StringConstants2.default.cash || fullName === _StringConstants2.default.cashSale) {
          cashSaleOption = option;
        }
        return option;
      });
      this.dropdownItems = this.allItems.map(function (logic) {
        return {
          value: _MyDouble2.default.convertStringToDouble(logic.getItemId()),
          label: logic.getItemName(),
          searchText: logic.getItemName() + ' ' + (logic.getItemCode() || ''),
          itemCode: '' + (logic.getItemCode() || ''),
          purchasePrice: _MyDouble2.default.getAmountWithDecimal(logic.getItemPurchaseUnitPrice()),
          salePrice: _MyDouble2.default.getAmountWithDecimal(logic.getItemSaleUnitPrice()),
          stockQty: _MyDouble2.default.getQuantityWithDecimal(logic.getAvailableQuantity(), logic.getItemMinStockQuantity(), true),
          item: logic
        };
      });
      this.paymentTypeOptions = paymentInfoCache.getListOfPaymentAccountObjects();
      var paymentTermsOptions = (0, _SalePurchaseHelpers.getPaymentTermsOptions)(txnType);
      var invoicePrefixOptions = [];
      var transactionFactory = new _TransactionFactory2.default();
      var isConvert = false;

      if (txnId) {
        var txnObject = transactionFactory.getTransactionObject(txnType);
        var oldTxnObj = txnObj || txnObject.getTransactionFromId(txnId);
        this.oldTxnObj = oldTxnObj;
        if (oldTxnObj && oldTxnObj.getTxnType() == txnType) {
          _analyticsHelper2.default.pushEvent('Edit ' + _TxnTypeConstant2.default.getTxnTypeForUI(oldTxnObj.getTxnType()) + ' Open');
        } else {
          isConvert = true;
          if (oldTxnObj.getTxnType() == _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER && txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE) {
            _analyticsHelper2.default.pushEvent('Sale Order Convert to Sale Open');
            _analyticsHelper2.default.pushEvent('Sale Open');
          }
        }

        var nameLogic = oldTxnObj.getNameRef();
        var firmId = oldTxnObj.getFirmId();

        var returnNo = '';

        invoicePrefixOptions = (0, _SalePurchaseHelpers.getInvoicePrefixOptions)(firmId, txnType);
        var invoicePrefix = _MyDouble2.default.convertStringToDouble(oldTxnObj.getInvoicePrefixId());
        if (isConvert) {
          invoicePrefix = null;
          invoicePrefixOptions.forEach(function (prefix) {
            if (_MyDouble2.default.convertStringToDouble(prefix.getIsDefault())) {
              invoicePrefix = prefix.getPrefixId();
            }
          });
        }

        if (oldTxnObj.getTxnType() == txnType && oldTxnObj.getTxnType() == _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN && oldTxnObj.getStatus && oldTxnObj.getStatus() == _TransactionStatus2.default.TXN_ORDER_COMPLETE) {
          var linkedTransactions = dataLoader.loadAllLinkedTransactionByTxnId(txnId);
          var linkedTransactionId = void 0;
          var linkedTransaction = void 0;
          if (linkedTransactions.length) {
            linkedTransaction = linkedTransactions[0];
            linkedTransactionId = linkedTransaction.getTxnLinksTxn2Id();
            linkedTransaction = dataLoader.LoadTransactionFromId(linkedTransactionId);
            state.closedOn = 'Closed On ' + _MyDate2.default.getDate('d/m/y', linkedTransaction.getTxnDate());
            if (linkedTransaction.getFullTxnRefNumber()) {
              state.linkedTransactionInvoiceNumber = 'Invoice No. ' + linkedTransaction.getFullTxnRefNumber();
              state.linkedTransactionId = linkedTransactionId;
              state.linkedtxnTransactionType = linkedTransaction.txnType;
            } else {
              state.linkedTransactionInvoiceNumber = 'Converted Invoice';
            }
          } else {
            state.closedOn = 'Closed';
          }
          state.submitButtonDisabled = true;
        }

        if (oldTxnObj && oldTxnObj.getTxnType() == _TxnTypeConstant2.default.TXN_TYPE_SALE && txnType == _TxnTypeConstant2.default.TXN_TYPE_SALE) {
          // disable invoice type in edit mode
          state.invoiceTypeDisabled = true;
        }

        var receivedLaterTotalAmount = 0;

        if (settingCache.isBillToBillEnabled() && (txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHIN && oldTxnObj.getTxnType() != _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT && oldTxnObj.getTxnType() != _TxnTypeConstant2.default.TXN_TYPE_CASHOUT || txnType == _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN && oldTxnObj.getTxnType() == _TxnTypeConstant2.default.TXN_TYPE_SALE || txnType == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN && oldTxnObj.getTxnType() == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE)) {
          var txnCurrentBalanceAmount = _MyDouble2.default.convertStringToDouble(oldTxnObj.getTxnCurrentBalanceAmount());

          selectedTransactionMap.set(_MyDouble2.default.convertStringToDouble(txnId), [oldTxnObj, txnCurrentBalanceAmount, null]);

          receivedLaterTotalAmount += txnCurrentBalanceAmount;
        } else {
          var txnLinksList = dataLoader.loadAllTransactionLinksByTxnId(txnId);

          if (txnLinksList) {
            for (var i = 0; i < txnLinksList.length; i++) {
              var linkTxn = txnLinksList[i];
              var txnLink1Id = _MyDouble2.default.convertStringToDouble(linkTxn.getTxnLinksTxn1Id()); // in case of null returns 0
              var txnLink2Id = _MyDouble2.default.convertStringToDouble(linkTxn.getTxnLinksTxn2Id()); // in case of null returns 0
              var txnLinkAmount = _MyDouble2.default.convertStringToDouble(linkTxn.getTxnLinksAmount());
              var closedTxnId = _MyDouble2.default.convertStringToDouble(linkTxn.getTxnLinkClosedTxnRefId());

              receivedLaterTotalAmount += txnLinkAmount;
              var selectedTxnId = void 0;
              if (_MyDouble2.default.convertStringToDouble(txnId) == txnLink1Id) {
                selectedTxnId = txnLink2Id;
              } else if (_MyDouble2.default.convertStringToDouble(txnId) == txnLink2Id) {
                selectedTxnId = txnLink1Id;
              }

              var linkTxnObject = void 0;
              if (selectedTxnId > 0) {
                linkTxnObject = dataLoader.LoadTransactionFromId(selectedTxnId);
              } else {
                // for uneditable closed linked transactions
                linkTxnObject = dataLoader.loadClosedTransactionDataById(closedTxnId);

                selectedTxnId = i + 'closed';
              }

              selectedTransactionMap.set(_MyDouble2.default.convertStringToDouble(selectedTxnId), [linkTxnObject, txnLinkAmount, linkTxn]);
            }
          }
        }

        txnListB2B = (0, _SalePurchaseHelpers.getTransactionsForB2BLinking)(nameLogic.getNameId(), firmId, txnType, selectedTransactionMap);

        if (selectedTransactionMap.size > 0) {
          copyOfSelectedTransactionMap = new _map2.default(selectedTransactionMap);
        }
        state.receivedLater = receivedLaterTotalAmount;

        showCashSale = false;

        if (txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN) {
          if (isConvert) {
            returnNo = (0, _SalePurchaseHelpers.getMaxInvoiceNumber)(firmId, txnType, invoicePrefix);
          } else {
            returnNo = oldTxnObj.getTxnRefNumber();
          }
        }

        var txnBillAddress = oldTxnObj.getBillingAddress();
        txnBillAddress = txnBillAddress || nameLogic.getAddress();
        var txnShipAddress = oldTxnObj.getShippingAddress();
        txnShipAddress = txnShipAddress || nameLogic.getShippingAddress();
        var PODate = '';
        var txnReturnRefDate = '';
        var invoiceDate = '';
        var dueDate = oldTxnObj.getTxnDueDate();
        if (isConvert) {
          PODate = oldTxnObj.getTxnDate();
          txnReturnRefDate = oldTxnObj.getTxnDate();
          invoiceDate = new Date();
        } else {
          PODate = oldTxnObj.getPODate();
          txnReturnRefDate = oldTxnObj.getTxnReturnDate();
          invoiceDate = oldTxnObj.getTxnDate();
        }

        if (PODate && PODate instanceof Date && PODate != 'Invalid Date') {
          PODate = _MyDate2.default.getDate(DATEFORMATE, PODate);
        }
        if (txnReturnRefDate && txnReturnRefDate instanceof Date && txnReturnRefDate != 'Invalid Date') {
          txnReturnRefDate = _MyDate2.default.getDate(DATEFORMATE, txnReturnRefDate);
        }
        if (invoiceDate && invoiceDate instanceof Date && invoiceDate != 'Invalid Date') {
          invoiceDate = _MyDate2.default.getDate(DATEFORMATE, invoiceDate);
        }
        if (dueDate && dueDate instanceof Date && dueDate != 'Invalid Date') {
          dueDate = _MyDate2.default.getDate(DATEFORMATE, dueDate);
        } else {
          dueDate = '';
        }

        transaction = {
          cashSale: false,
          party: {
            value: _MyDouble2.default.convertStringToDouble(nameLogic.getNameId()),
            label: nameLogic.getFullName(),
            balance: _MyDouble2.default.getAmountWithDecimal(nameLogic.getAmount()),
            partyLogic: nameLogic
          },
          firm: firmCache.getFirmById(oldTxnObj.getFirmId()),
          displayName: oldTxnObj.getDisplayName(),
          billingAddress: txnBillAddress,
          shippingAddress: txnShipAddress,
          PONo: isConvert ? oldTxnObj.getFullTxnRefNumber() : oldTxnObj.getPONumber(),
          PODate: PODate,
          eWayBillNo: oldTxnObj.getEwayBillNumber(),
          invoiceType: oldTxnObj.getTxnSubType(),
          returnNo: returnNo,
          txnReturnRefNumber: isConvert ? oldTxnObj.getFullTxnRefNumber() : oldTxnObj.getTxnReturnRefNumber(),
          txnReturnRefDate: txnReturnRefDate,
          invoicePrefix: invoicePrefix,
          invoiceDate: invoiceDate,
          paymentTerms: oldTxnObj.getTxnPaymentTermId(),
          dueDate: dueDate,
          placeOfSupply: oldTxnObj.getPlaceOfSupply(),
          paymentType: oldTxnObj.getPaymentTypeId(),
          referenceNo: oldTxnObj.getPaymentTypeReference(),
          description: oldTxnObj.getDescription(),
          imageId: _MyDouble2.default.convertStringToDouble(oldTxnObj.getImageId()),
          imageBlob: oldTxnObj.getImageBlob(),
          image: '',
          extraFields: (0, _SalePurchaseHelpers.getExtraFields)(oldTxnObj.getUdfObjectArray(), txnType, firmId, isConvert, _MyDouble2.default.convertStringToDouble(oldTxnObj.getTxnType())),
          transportationDetails: (0, _SalePurchaseHelpers.getCustomFields)(oldTxnObj.getCustomFields(), txnType),
          discountPercent: _MyDouble2.default.convertStringToDouble(oldTxnObj.getDiscountPercent()),
          discountAmount: _MyDouble2.default.getAmountWithDecimal(oldTxnObj.getDiscountAmount()),
          taxPercent: _MyDouble2.default.convertStringToDouble(oldTxnObj.getTransactionTaxId()),
          taxAmount: _MyDouble2.default.getAmountWithDecimal(oldTxnObj.getTaxAmount()),
          ITC: oldTxnObj.getTxnITCApplicable(),
          additionalCharges: (0, _SalePurchaseHelpers.getAdditionalChargeFields)(oldTxnObj, txnType),
          roundOff: !!_MyDouble2.default.convertStringToDouble(oldTxnObj.getRoundOffValue()) || this.fieldsVisibility.roundOff,
          roundOffAmount: _MyDouble2.default.convertStringToDouble(oldTxnObj.getRoundOffValue()),
          isReverseCharge: _MyDouble2.default.convertStringToDouble(oldTxnObj.getReverseCharge()) === 1 && (txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN),
          reverseChargeAmount: _MyDouble2.default.getAmountWithDecimal(oldTxnObj.getReverseChargeAmount()),
          total: _MyDouble2.default.getAmountWithDecimal(_MyDouble2.default.convertStringToDouble(oldTxnObj.getBalanceAmount()) ? _MyDouble2.default.convertStringToDouble(oldTxnObj.getBalanceAmount()) + _MyDouble2.default.convertStringToDouble(oldTxnObj.getCashAmount()) : 0 + _MyDouble2.default.convertStringToDouble(oldTxnObj.getCashAmount())),
          payable: 0,
          received: _MyDouble2.default.getAmountWithDecimal(oldTxnObj.getCashAmount()),
          balance: _MyDouble2.default.getAmountWithDecimal(oldTxnObj.getBalanceAmount() ? oldTxnObj.getBalanceAmount() : 0),
          currentTxnBalance: _MyDouble2.default.getAmountWithDecimal(oldTxnObj.getTxnCurrentBalanceAmount()) || 0,
          txnSubType: oldTxnObj.getTxnSubType(),
          discountForCash: txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHOUT ? _MyDouble2.default.getAmountWithDecimal(oldTxnObj.getDiscountAmount()) : 0,
          taxInclusive: _MyDouble2.default.convertStringToDouble(oldTxnObj.getTaxTypeInclusive())
        };

        if (isConvert && (txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN)) {
          transaction.PONo = '';
          transaction.PODate = '';
          if (settingCache.isBillToBillEnabled()) {
            transaction.balance = transaction.received;
            transaction.received = 0;
          }
          transaction.currentTxnBalance = _MyDouble2.default.getAmountWithDecimal(transaction.total - transaction.currentTxnBalance);
          if (txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN) {
            this.labels.totalReceivedAmount = 'TOTAL PAID';
          }
          var cashPaymentType = this.paymentTypeOptions.find(function (p) {
            return p.getName() === 'Cash';
          });
          transaction.referenceNo = '';
          if (cashPaymentType) {
            transaction.paymentType = cashPaymentType.getId();
          }
          if (txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN) {
            if (!transaction.billingAddress) {
              transaction.billingAddress = transaction.party && transaction.party.partyLogic && transaction.party.partyLogic.getAddress() || '';
            }
            if (!transaction.shippingAddress) {
              transaction.shippingAddress = this.fieldsVisibility.shippingAddress && transaction.party && transaction.party.partyLogic && transaction.party.partyLogic.getShippingAddress() || '';
            }
          }
        }

        if (isConvert && (txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE)) {
          if (!this.fieldsVisibility.PODetails) {
            transaction.PONo = '';
            transaction.PODate = '';
            transaction.description = '';
          }
        }

        if (transaction.PONo || transaction.PODate) {
          this.fieldsVisibility.PODetails = true;
        }

        if (isConvert) {
          transaction.image = '';
          transaction.imageBlob = '';
          transaction.imageId = '';
          transaction.taxInclusive = _MyDouble2.default.convertStringToDouble((0, _SalePurchaseHelpers.getTaxInclusiveValue)(txnType));
        }
        if (transaction.discountAmount) {
          this.fieldsVisibility.discount = true;
        }

        if (transaction.taxAmount) {
          this.fieldsVisibility.tax = true;
        }

        transaction.totalReceivedAmount = _MyDouble2.default.getAmountWithDecimal(transaction.received + receivedLaterTotalAmount);

        if (returnNo) {
          transaction.invoiceNo = '';
        } else if (isConvert) {
          transaction.invoiceNo = (0, _SalePurchaseHelpers.getMaxInvoiceNumber)(transaction.firm.getFirmId(), txnType, transaction.invoicePrefix);
        } else {
          transaction.invoiceNo = oldTxnObj.getTxnRefNumber();
        }

        if (isConvert) {
          var defaultPaymentTermObj = paymentTermCache.getDefaultPaymentTermObject();
          transaction.paymentTerms = defaultPaymentTermObj.getPaymentTermId();
          var _dueDate = (0, _SalePurchaseHelpers.getDueDate)(txnType, transaction.paymentTerms, transaction.invoiceDate);
          transaction.dueDate = _MyDate2.default.getDate(DATEFORMATE, _dueDate);
        }
        var existingLineItems = oldTxnObj.getLineItems();
        var itemUnitMappingCache = new _ItemUnitMappingCache3.default();
        var taxCodeCache = new _TaxCodeCache2.default();
        lineItems = existingLineItems.map(function (lineItem) {
          var itemLogic = itemCache.findItemByName(lineItem.getItemName(), txnType);
          var newLineItem = _this2.getDefaultLineItem();
          newLineItem.item = {
            value: _MyDouble2.default.convertStringToDouble(itemLogic.getItemId()),
            label: itemLogic.getItemName(),
            purchasePrice: _MyDouble2.default.getAmountWithDecimal(itemLogic.getItemPurchaseUnitPrice()),
            salePrice: _MyDouble2.default.getAmountWithDecimal(itemLogic.getItemSaleUnitPrice()),
            stockQty: _MyDouble2.default.getQuantityWithDecimal(itemLogic.getAvailableQuantity(), itemLogic.getItemMinStockQuantity(), true),
            item: itemLogic
          };
          newLineItem.description = lineItem.getLineItemDescription();
          if (newLineItem.description) {
            lineItemColumnsVisibility.description = true;
          }
          newLineItem.count = _MyDouble2.default.getQuantityWithDecimalWithoutColor(lineItem.getLineItemCount());
          if (newLineItem.count) {
            lineItemColumnsVisibility.count = true;
          }
          newLineItem.slNo = lineItem.getLineItemSerialNumber();
          if (newLineItem.slNo) {
            lineItemColumnsVisibility.slNo = true;
          }
          newLineItem.batchNo = lineItem.getLineItemBatchNumber();
          if (newLineItem.batchNo) {
            lineItemColumnsVisibility.batchNo = true;
          }
          var expDate = lineItem.getLineItemExpiryDate();
          if (expDate && expDate instanceof Date && expDate != 'Invalid Date') {
            newLineItem.expDate = _MyDate2.default.getDate(settingCache.getExpiryDateFormat() == _StringConstants2.default.dateMonthYear ? DATEFORMATE : 'm/y', expDate);
          } else {
            newLineItem.expDate = '';
          }
          if (expDate) {
            lineItemColumnsVisibility.expDate = true;
          }
          var mfgDate = lineItem.getLineItemManufacturingDate();
          if (mfgDate && mfgDate instanceof Date && mfgDate != 'Invalid Date') {
            newLineItem.mfgDate = _MyDate2.default.getDate(settingCache.getManufacturingDateFormat() == _StringConstants2.default.dateMonthYear ? DATEFORMATE : 'm/y', mfgDate);
          } else {
            newLineItem.mfgDate = '';
          }
          if (mfgDate) {
            lineItemColumnsVisibility.mfgDate = true;
          }
          newLineItem.mrp = lineItem.getLineItemMRP();
          if (newLineItem.mrp) {
            lineItemColumnsVisibility.mrp = true;
          }
          newLineItem.size = lineItem.getLineItemSize();
          if (newLineItem.size) {
            lineItemColumnsVisibility.size = true;
          }
          newLineItem.qty = _MyDouble2.default.getQuantityWithDecimalWithoutColor(lineItem.itemQuantity);
          newLineItem.freeQty = _MyDouble2.default.getQuantityWithDecimalWithoutColor(lineItem.itemFreeQuantity);
          if (newLineItem.freeQty) {
            lineItemColumnsVisibility.freeQty = true;
          }
          newLineItem.ITCValue = lineItem.getLineItemITCValue();
          newLineItem.priceUnit = transaction.taxInclusive == ItemType.ITEM_TXN_TAX_INCLUSIVE ? _MyDouble2.default.getAmountWithDecimal(_MyDouble2.default.convertStringToDouble(lineItem.itemUnitPrice) + _MyDouble2.default.convertStringToDouble(lineItem.itemUnitPrice) * _MyDouble2.default.getPercentageWithDecimal(taxCodeCache.getRateForTaxId(lineItem.getLineItemTaxId())) / 100) : _MyDouble2.default.getAmountWithDecimal(lineItem.itemUnitPrice);
          newLineItem.unit = lineItem.getLineItemUnitId() ? lineItem.getLineItemUnitId() : '';
          var existing = null;

          if (lineItem.getLineItemUnitId()) {
            existing = {
              selectedId: lineItem.getLineItemUnitId(),
              mappingId: lineItem.getLineItemUnitMappingId()
            };
          } else {
            if (lineItemColumnsVisibility.unit) {
              newLineItem.unit = itemLogic && itemLogic.getBaseUnitId() || '';
            }
          }

          newLineItem.unitObj = (0, _SalePurchaseHelpers.getUnitDropdownOptions)(itemLogic, existing);
          if (newLineItem.unit) {
            var found = newLineItem.unitObj.find(function (unit) {
              return unit.value == newLineItem.unit;
            });
            if (found) {
              var itemMappingId = found.mappingId || null;
              if (found.type === 'secondary') {
                var mappingObj = itemUnitMappingCache.getItemUnitMapping(itemMappingId);
                var rate = mappingObj['conversionRate'];
                newLineItem.priceUnit = _MyDouble2.default.getAmountWithDecimal(_MyDouble2.default.convertStringToDouble(newLineItem.priceUnit) / _MyDouble2.default.convertStringToDouble(rate));
                newLineItem.qty = _MyDouble2.default.getQuantityWithDecimalWithoutColor(_MyDouble2.default.convertStringToDouble(lineItem.itemQuantity) * _MyDouble2.default.convertStringToDouble(rate));
                newLineItem.freeQty = _MyDouble2.default.getQuantityWithDecimalWithoutColor(_MyDouble2.default.convertStringToDouble(lineItem.itemFreeQuantity) * _MyDouble2.default.convertStringToDouble(rate));
              }
            }
            lineItemColumnsVisibility.unit = true;
          }
          newLineItem.discountPercent = _MyDouble2.default.getPercentageWithDecimal(_MyDouble2.default.convertStringToDouble(lineItem.getLineItemDiscountPercent()));
          newLineItem.discountAmount = _MyDouble2.default.getBalanceAmountWithDecimal(lineItem.lineItemDiscount);
          if (newLineItem.discountPercent) {
            lineItemColumnsVisibility.discount = true;
          }
          newLineItem.oldTaxId = _MyDouble2.default.convertStringToDouble(lineItem.getLineItemTaxId());
          if (!newLineItem.oldTaxId) {
            newLineItem.oldTaxId = _MyDouble2.default.convertStringToDouble(itemLogic && itemLogic.getItemTaxId() || 0);
          }
          newLineItem.taxPercent = _MyDouble2.default.convertStringToDouble(lineItem.getLineItemTaxId());
          newLineItem.taxAmount = _MyDouble2.default.getAmountWithDecimal(lineItem.lineItemTax);
          if (_MyDouble2.default.convertStringToDouble(newLineItem.taxAmount) > 0) {
            lineItemColumnsVisibility.tax = true;
          }
          newLineItem.cess = _MyDouble2.default.getAmountWithDecimal(lineItem.getLineItemAdditionalCESS());
          if (_MyDouble2.default.convertStringToDouble(newLineItem.cess) > 0) {
            lineItemColumnsVisibility.cess = true;
          }
          newLineItem.amount = lineItem.lineItemTotal;
          newLineItem.istId = _MyDouble2.default.convertStringToDouble(lineItem.getLineItemIstId());
          return newLineItem;
        });

        if (lineItems.length) {
          this.fieldsVisibility.lineItems = true;
        }

        this.transactionTaxDropdownList = (0, _SalePurchaseHelpers.getLineItemTaxDropdownList)({
          txnType: txnType,
          txnId: txnId,
          firmLogic: transaction.firm,
          placeOfSupply: transaction.placeOfSupply,
          partyLogic: transaction.party && transaction.party.partyLogic,
          currentTaxCodeId: [_MyDouble2.default.convertStringToDouble(transaction.taxPercent)],
          oldTaxId: [_MyDouble2.default.convertStringToDouble(transaction.taxPercent)]
        });
        if (fieldsVisibility.dueDate && !this.isConvert && (txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE)) {
          transaction.dueDays = _MyDouble2.default.getDueDays(_MyDate2.default.getDateObj(transaction.dueDate, DATE_FORMATE_FULL, '/'));
        }
      } else {
        // new transaction
        if (txnObj) {
          this.oldTxnObj = txnObj;
        } else {
          this.oldTxnObj = transactionFactory.getTransactionObject(txnType);
        }
        var firm = firmCache.getDefaultFirm();
        var _firmId = firm.getFirmId();

        transaction.cashSale = showCashSale;

        transaction.party = null;

        transaction.firm = firm;

        switch (txnType) {
          case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN:
          case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE:
          case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER:
            transaction.placeOfSupply = firm.getFirmState();
            break;
        }

        this.taxDropdownList = (0, _SalePurchaseHelpers.getLineItemTaxDropdownList)({
          txnType: txnType,
          txnId: txnId,
          firmLogic: transaction.firm,
          placeOfSupply: transaction.placeOfSupply,
          partyLogic: transaction.party && transaction.partyLogic || null,
          currentTaxCodeId: []
        });
        this.transactionTaxDropdownList = (0, _SalePurchaseHelpers.getLineItemTaxDropdownList)({
          txnType: txnType,
          txnId: txnId,
          firmLogic: transaction.firm,
          placeOfSupply: transaction.placeOfSupply,
          partyLogic: transaction.party && transaction.partyLogic || null,
          currentTaxCodeId: [_MyDouble2.default.convertStringToDouble(transaction.taxPercent)]
        });
        /* payment terms */
        var _defaultPaymentTermObj = paymentTermCache.getDefaultPaymentTermObject();
        transaction.paymentTerms = _defaultPaymentTermObj.getPaymentTermId();

        /* due date */
        transaction.dueDate = (0, _SalePurchaseHelpers.getDueDate)(txnType, transaction.paymentTerms, transaction.invoiceDate);
        transaction.dueDate = _MyDate2.default.getDate(DATEFORMATE, transaction.dueDate);

        /* invoice prefix */
        invoicePrefixOptions = (0, _SalePurchaseHelpers.getInvoicePrefixOptions)(_firmId, txnType);
        invoicePrefixOptions.forEach(function (prefix) {
          if (_MyDouble2.default.convertStringToDouble(prefix.getIsDefault())) {
            transaction.invoicePrefix = prefix.getPrefixId();
          }
        });
        var invoiceNumber = (0, _SalePurchaseHelpers.getMaxInvoiceNumber)(firm.getFirmId(), txnType, transaction.invoicePrefix);
        if (txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN) {
          transaction.returnNo = invoiceNumber;
        } else {
          transaction.invoiceNo = invoiceNumber;
        }

        /* extra fields */
        transaction.extraFields = (0, _SalePurchaseHelpers.getExtraFields)(null, txnType, _firmId);

        /* custom fields */
        transaction.transportationDetails = (0, _SalePurchaseHelpers.getCustomFields)(null, txnType);
        var firstPaymentType = this.paymentTypeOptions[0];
        transaction.paymentType = firstPaymentType && firstPaymentType.getId();

        /* additional charges */
        transaction.additionalCharges = (0, _SalePurchaseHelpers.getAdditionalChargeFields)(null, txnType);

        /* reverse charge */
        if (settingCache.getReverseChargeEnabled()) {
          if (txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE) {
            showReverseCharge = true;
          }
        }
        transaction.taxInclusive = _MyDouble2.default.convertStringToDouble((0, _SalePurchaseHelpers.getTaxInclusiveValue)(txnType));
        _analyticsHelper2.default.pushEvent('New ' + _TxnTypeConstant2.default.getTxnTypeForUI(txnType) + ' Open');
      }

      if (!this.fieldsVisibility.lineItems) {
        this.fieldsVisibility.roundOff = false;
        this.fieldsVisibility.tax = false;
        this.fieldsVisibility.discount = false;
      }

      if (isConvert) {
        var oldTxnType = this.oldTxnObj.getTxnType();
        if (oldTxnType == _TxnTypeConstant2.default.TXN_TYPE_SALE && txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN || oldTxnType == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE && txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN) {
          transaction.description = '';
        }
      }

      // fields visibility
      lineItems.push(this.getDefaultLineItem());
      transaction = (0, _extends5.default)({}, transaction, state);
      this.isConvert = isConvert;
      this.isEdit = false;
      if (txnId) {
        if ((this.oldTxnObj.getTxnType() == _TxnTypeConstant2.default.TXN_TYPE_SALE || this.oldTxnObj.getTxnType() == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE) && (txnType == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN || txnType == _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN) || txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHIN && this.oldTxnObj.getTxnType() != _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT && this.oldTxnObj.getTxnType() != _TxnTypeConstant2.default.TXN_TYPE_CASHOUT) {
          this.isEdit = false;
        } else {
          this.isEdit = true;
        }
      }

      if (this.isEdit) {
        this.shouldSetTaxZero = this.setTaxZero(transaction.party.partyLogic);
      }

      // round off
      transaction.roundOff = this.fieldsVisibility.roundOff;
      if (this.isEdit || this.isConvert) {
        if (!transaction.roundOffAmount) {
          transaction.roundOff = false;
        }
      }

      this.lineItemColumnsVisibility.taxInclusiveDropdown = (0, _SalePurchaseHelpers.displayInclusiveOfTaxDropDown)(txnType, transaction.taxInclusive, transaction.party && transaction.party.partyLogic || null, txnId);

      this.fieldsVisibility.linkPayment = this.fieldsVisibility.linkPayment && transaction.party && transaction.party.value != 0 && (txnListB2B.size > 0 || selectedTransactionMap.size > 0);

      this.setState({
        firmList: (0, _values2.default)(firmCache.getFirmList()),
        cashSaleOption: cashSaleOption,
        showCashSale: showCashSale,
        showReverseCharge: showReverseCharge,
        invoicePrefixOptions: invoicePrefixOptions,
        paymentTermsOptions: paymentTermsOptions,
        copyOfSelectedTransactionMap: copyOfSelectedTransactionMap,
        selectedTransactionMap: selectedTransactionMap,
        txnListB2B: txnListB2B,
        transaction: transaction,
        lineItems: lineItems
      }, function () {
        if (selectedTransactionMap.size > 0) {
          _this2.changeTransactionLinks(txnListB2B, selectedTransactionMap, copyOfSelectedTransactionMap);
        }
        var total = _this2.calculateTotal(lineItems);
        _this2.setState((0, _extends5.default)({ transaction: (0, _extends5.default)({}, transaction) }, total), function () {
          if (transaction.isReverseCharge || isConvert && _this2.oldTxnObj.getTxnType() == _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN && txnType == _TxnTypeConstant2.default.TXN_TYPE_SALE) {
            _this2.renderLineItems(lineItems);
          }
        });
      });
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      var _this3 = this;

      $('#defaultPage').hide();
      window.onmessage = function () {
        var txnType = _this3.props.txnType;
        var transaction = _this3.state.transaction;

        if (event.data == 'increment invoice number') {
          if (!_this3.isEdit) {
            var firmCache = new _FirmCache2.default();
            firmCache.refreshFirmCache();
            var invoiceNo = (0, _SalePurchaseHelpers.getMaxInvoiceNumber)(transaction.firm.getFirmId(), txnType, transaction.invoicePrefix);
            if (txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN) {
              _this3.setState({ transaction: (0, _extends5.default)({}, transaction, { returnNo: invoiceNo }) });
            } else {
              _this3.setState({ transaction: (0, _extends5.default)({}, transaction, { invoiceNo: invoiceNo }) });
            }
          }
        }
      };
      this.init();
      setTimeout(function () {
        $('#modelContainer #close:visible').trigger('click');
        unmountReactComponent(document.querySelector('#frameDiv'));
        $('#frameDiv').empty();
      });
    }
  }, {
    key: 'changeField',
    value: function changeField(name) {
      var _this4 = this;

      return function (event) {
        var transaction = _this4.state.transaction;

        var value = null;
        if (event && event.target) {
          value = event.target.value;
        } else {
          value = event || '';
        }
        if (name === 'image' && !value) {
          _this4.setState({
            transaction: (0, _extends5.default)({}, transaction, {
              image: '',
              imageBlob: ''
            })
          });
        } else {
          _this4.setState({
            transaction: (0, _extends5.default)({}, transaction, (0, _defineProperty3.default)({}, name, value))
          });
        }
      };
    }
  }, {
    key: 'changeFields',
    value: function changeFields(fields) {
      var transaction = this.state.transaction;

      this.setState({
        transaction: (0, _extends5.default)({}, transaction, fields)
      });
    }
  }, {
    key: 'changeParty',
    value: function changeParty(party) {
      var _this5 = this;

      var _state2 = this.state,
          transaction = _state2.transaction,
          selectedTransactionMap = _state2.selectedTransactionMap;
      var txnListB2B = this.state.txnListB2B;
      var _props2 = this.props,
          txnType = _props2.txnType,
          txnId = _props2.txnId;

      var transactionState = {};

      if (transaction.cashSale) {
        transactionState.displayName = party.label;
      } else {
        if (this.fieldsVisibility.displayName || transaction.displayName != '') {
          transactionState.displayName = party.label;
        }
      }
      if (selectedTransactionMap.size > 0 || settingCache.isBillToBillEnabled()) {
        txnListB2B = (0, _SalePurchaseHelpers.getTransactionsForB2BLinking)(party.value, transaction.firm.getFirmId(), txnType, selectedTransactionMap);
      }

      this.fieldsVisibility.linkPayment = settingCache.isBillToBillEnabled() && !transaction.cashSale && party.value != 0 && txnListB2B.size > 0;

      transactionState.party = party;
      var partyLogic = party.partyLogic || null;
      if (txnType == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN || txnType == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE || txnType == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER) {
        transactionState.placeOfSupply = transaction.firm.getFirmState();
      } else if (this.fieldsVisibility.placeOfSupply) {
        transactionState.placeOfSupply = partyLogic && partyLogic.getContactState() || '';
      } else {
        transactionState.placeOfSupply = transaction.placeOfSupply;
      }

      transactionState.taxPercent = (0, _SalePurchaseHelpers.getCorrespondingIdOfTaxRate)({
        taxId: transaction.taxPercent,
        txnType: txnType,
        partyLogic: partyLogic,
        firmLogic: transaction.firm,
        placeOfSupply: transactionState.placeOfSupply
      });

      this.transactionTaxDropdownList = (0, _SalePurchaseHelpers.getLineItemTaxDropdownList)({
        txnType: txnType,
        txnId: txnId,
        firmLogic: transaction.firm,
        placeOfSupply: transactionState.placeOfSupply,
        partyLogic: partyLogic,
        currentTaxCodeId: [_MyDouble2.default.convertStringToDouble(transactionState.taxPercent)]
      });

      var shouldSetTaxZero = this.setTaxZero(partyLogic);

      this.lineItemColumnsVisibility.taxInclusiveDropdown = (0, _SalePurchaseHelpers.displayInclusiveOfTaxDropDown)(txnType, transaction.taxInclusive, partyLogic, txnId);

      this.setState({
        transaction: (0, _extends5.default)({}, transaction, transactionState, {
          billingAddress: this.fieldsVisibility.billingAddress && partyLogic && partyLogic.getAddress() || '',
          shippingAddress: this.fieldsVisibility.shippingAddress && partyLogic && partyLogic.getShippingAddress() || ''
        }),
        txnListB2B: txnListB2B
      }, function () {
        var _state3 = _this5.state,
            lineItems = _state3.lineItems,
            transaction = _state3.transaction;

        var modifiedLineItems = void 0;
        if (shouldSetTaxZero) {
          _this5.shouldSetTaxZero = true;
          modifiedLineItems = lineItems.map(function (lineItem) {
            var taxPercent = (0, _SalePurchaseHelpers.getCorrespondingIdOfTaxRate)({
              taxId: lineItem.taxPercent,
              txnType: txnType,
              partyLogic: partyLogic,
              firmLogic: transaction.firm,
              placeOfSupply: transactionState.placeOfSupply
            });
            return (0, _SalePurchaseHelpers.changeTaxFields)((0, _extends5.default)({}, lineItem, {
              taxPercent: lineItem.taxPercent == lineItem.oldTaxId && !_GSTRReportHelper2.default.isTaxOfTypeOthers(lineItem.taxPercent) ? 0 : taxPercent,
              uniqueId: (0, _SalePurchaseHelpers.getUniqueKey)()
            }), txnType, transaction.taxInclusive);
          });
        } else {
          modifiedLineItems = lineItems.map(function (lineItem) {
            lineItem.uniqueId = (0, _SalePurchaseHelpers.getUniqueKey)();
            var taxPercent = _MyDouble2.default.convertStringToDouble(lineItem.taxPercent);
            lineItem.taxPercent = (0, _SalePurchaseHelpers.getCorrespondingIdOfTaxRate)({
              taxId: _MyDouble2.default.convertStringToDouble(_this5.shouldSetTaxZero ? taxPercent ? lineItem.taxPercent : lineItem.oldTaxId || 0 : lineItem.taxPercent),
              txnType: txnType,
              partyLogic: transaction.party.partyLogic,
              firmLogic: transaction.firm,
              placeOfSupply: transaction.placeOfSupply
            });
            return (0, _SalePurchaseHelpers.changeTaxFields)((0, _extends5.default)({}, lineItem, { uniqueId: (0, _SalePurchaseHelpers.getUniqueKey)() }), txnType, transaction.taxInclusive);
          });
          _this5.shouldSetTaxZero = false;
        }
        _this5.renderLineItems(modifiedLineItems, shouldSetTaxZero);
      });
    }
  }, {
    key: 'changeFirm',
    value: function changeFirm(firm) {
      var _this6 = this;

      var _state4 = this.state,
          transaction = _state4.transaction,
          selectedTransactionMap = _state4.selectedTransactionMap;

      if (selectedTransactionMap.size > 0) {
        ToastHelper.error('Can not change firm when transaction is linked');
        return false;
      }
      var txnListB2B = this.state.txnListB2B;
      var txnType = this.props.txnType;

      var partyId = transaction.party && transaction.party.value ? transaction.party.value : 0;

      switch (txnType) {
        case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN:
        case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE:
        case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER:
          transaction.placeOfSupply = firm.getFirmState();
          break;
      }
      var firmId = firm.getFirmId();

      transaction.invoicePrefix = '';
      var invoicePrefixOptions = (0, _SalePurchaseHelpers.getInvoicePrefixOptions)(firmId, txnType);
      invoicePrefixOptions.forEach(function (prefix) {
        if (_MyDouble2.default.convertStringToDouble(prefix.getIsDefault())) {
          transaction.invoicePrefix = prefix.getPrefixId();
        }
      });
      var invoiceNumber = (0, _SalePurchaseHelpers.getMaxInvoiceNumber)(firmId, txnType, transaction.invoicePrefix);
      if (txnType == _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN) {
        transaction.returnNo = invoiceNumber;
      } else {
        transaction.invoiceNo = invoiceNumber;
      }

      transaction.extraFields = (0, _SalePurchaseHelpers.getExtraFields)(this.oldTxnObj.getUdfObjectArray(), txnType, firm.getFirmId(), this.isConvert, _MyDouble2.default.convertStringToDouble(this.oldTxnObj.getTxnType()));

      transaction.firm = firm;

      if (selectedTransactionMap.size > 0 || settingCache.isBillToBillEnabled()) {
        txnListB2B = (0, _SalePurchaseHelpers.getTransactionsForB2BLinking)(partyId, firm.getFirmId(), txnType, selectedTransactionMap);

        this.fieldsVisibility.linkPayment = settingCache.isBillToBillEnabled() && !transaction.cashSale && partyId != 0 && txnListB2B.size > 0;
      }

      this.setState({ transaction: transaction, txnListB2B: txnListB2B, invoicePrefixOptions: invoicePrefixOptions }, function () {
        return _this6.changePlaceOfSupply(transaction.placeOfSupply);
      });
    }
  }, {
    key: 'changePrefix',
    value: function changePrefix(e) {
      var transaction = this.state.transaction;
      var txnType = this.props.txnType;

      transaction.invoicePrefix = e.target.value;
      var number = (0, _SalePurchaseHelpers.getMaxInvoiceNumber)(transaction.firm.getFirmId(), txnType, transaction.invoicePrefix);
      if (txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN) {
        transaction.returnNo = number;
      } else {
        transaction.invoiceNo = number;
      }
      this.setState({ transaction: transaction });
    }
  }, {
    key: 'changeDueDate',
    value: function changeDueDate(date) {
      var transaction = this.state.transaction;
      var txnType = this.props.txnType;

      var dueDate = date || transaction.invoiceDate;

      if (dueDate instanceof Date) {
        dueDate = _MyDate2.default.getDate(DATEFORMATE, dueDate);
      }
      transaction.dueDate = dueDate;
      transaction.paymentTerms = '';

      if (txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE) {
        transaction.dueDays = _MyDouble2.default.getDueDays(_MyDate2.default.getDateObj(transaction.dueDate, DATE_FORMATE_FULL, '/'));
      }
      this.setState({ transaction: transaction });
    }
  }, {
    key: 'changePaymentTerms',
    value: function changePaymentTerms(e) {
      var transaction = this.state.transaction;
      var txnType = this.props.txnType;

      var value = e.target.value;
      var invoiceDate = _MyDate2.default.getDateObj(transaction.invoiceDate, DATE_FORMATE_FULL, '/');
      if (!(invoiceDate && invoiceDate instanceof Date && invoiceDate != 'Invalid Date')) {
        if (txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE) {
          ToastHelper.error('Please enter valid invoice date');
        } else {
          ToastHelper.error('Please enter valid bill date');
        }
        return false;
      }
      transaction.paymentTerms = value;
      var dueDate = (0, _SalePurchaseHelpers.getDueDate)(txnType, transaction.paymentTerms, transaction.invoiceDate);

      if (dueDate && dueDate instanceof Date && dueDate != 'Invalid Date') {
        transaction.dueDate = _MyDate2.default.getDate(DATEFORMATE, dueDate);
        if (txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE) {
          transaction.dueDays = _MyDouble2.default.getDueDays(dueDate);
        }
      }
      this.setState({ transaction: transaction });
    }
  }, {
    key: 'changeExtraField',
    value: function changeExtraField(index, value) {
      var transaction = this.state.transaction;

      transaction.extraFields[index].value = value;
      this.setState({ transaction: transaction });
    }
  }, {
    key: 'changeTransportationField',
    value: function changeTransportationField(index, value) {
      var transaction = this.state.transaction;

      transaction.transportationDetails[index].value = value;
      this.setState({ transaction: transaction });
    }
  }, {
    key: 'changeCashSale',
    value: function changeCashSale(e) {
      var checked = e.target.checked;
      var _state5 = this.state,
          transaction = _state5.transaction,
          txnListB2B = _state5.txnListB2B;

      var state = {};
      if (checked) {
        state = (0, _extends5.default)({}, transaction, {
          cashSale: checked,
          received: transaction.total,
          balance: 0
        });
      } else {
        state = (0, _extends5.default)({}, transaction, {
          cashSale: checked
        });
      }

      this.fieldsVisibility.linkPayment = settingCache.isBillToBillEnabled() && !state.cashSale && transaction.party && transaction.party.value != 0 && txnListB2B.size > 0;

      this.setState({ transaction: state });
    }
  }, {
    key: 'changeReverseCharge',
    value: function changeReverseCharge(e) {
      var _this7 = this;

      var checked = e.target.checked;
      var _state6 = this.state,
          transaction = _state6.transaction,
          selectedTransactionMap = _state6.selectedTransactionMap;

      this.setState({
        transaction: (0, _extends5.default)({}, transaction, {
          isReverseCharge: checked
        })
      }, function () {
        var lineItems = _this7.state.lineItems;

        _this7.renderLineItems(lineItems);
      });
    }
  }, {
    key: 'changeCheckbox',
    value: function changeCheckbox(name) {
      var _this8 = this;

      return function (event) {
        var transaction = _this8.state.transaction;

        var value = event.target.checked;
        _this8.setState({
          transaction: (0, _extends5.default)({}, transaction, (0, _defineProperty3.default)({}, name, value))
        });
      };
    }
  }, {
    key: 'toggleQuickEntry',
    value: function toggleQuickEntry() {
      var quickEntryOpen = this.state.quickEntryOpen;

      this.setState({
        quickEntryOpen: !quickEntryOpen
      });
    }
  }, {
    key: 'toggleDescription',
    value: function toggleDescription() {
      var showDescription = this.state.showDescription;

      this.setState({
        showDescription: !showDescription
      });
    }
  }, {
    key: 'changeImage',
    value: function changeImage() {
      var _this9 = this;

      var transaction = this.state.transaction;

      var callback = function callback(imageName, imagePath) {
        if (imagePath) {
          _this9.setState({
            transaction: (0, _extends5.default)({}, transaction, { image: imagePath, imageBlob: '' })
          });
        }
      };
      (0, _SalePurchaseHelpers.selectImage)(callback);
    }
  }, {
    key: 'onClose',
    value: function onClose(event) {
      event.stopPropagation();
      if (canCloseDialogue()) {
        this.setState({ open: false });
        if (typeof window.onResume === 'function') {
          window.onResume();
        } else {
          logger.error(new Error('onResume is not a function'));
        }
        window.onRes && window.onRes();
        unmountReactComponent(document.querySelector('#salePurchaseContainer'));
        $('#defaultPage').show();
        setTimeout(function () {
          document.dispatchEvent(new CustomEvent('resize-grid'));
        }, 100);
      }
    }
  }, {
    key: 'addLineItem',
    value: function addLineItem(lineItem) {
      var atBeginning = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      var lineItems = this.state.lineItems;

      var items = [];
      if (!lineItem) {
        if (atBeginning) {
          items = [this.getDefaultLineItem()].concat((0, _toConsumableArray3.default)(lineItems));
        } else {
          items = [].concat((0, _toConsumableArray3.default)(lineItems), [this.getDefaultLineItem()]);
        }
      } else if (Array.isArray(lineItem)) {
        if (atBeginning) {
          items = [].concat((0, _toConsumableArray3.default)(lineItem), (0, _toConsumableArray3.default)(lineItems));
        } else {
          var emptyRowIndexes = lineItems.reduce(function (all, row, index) {
            if (row.item && !row.item.label) {
              return [].concat((0, _toConsumableArray3.default)(all), [index]);
            }
            return all;
          }, []);
          var newItems = [].concat((0, _toConsumableArray3.default)(lineItem));
          emptyRowIndexes.forEach(function (index) {
            var item = newItems.shift();
            if (item) {
              lineItems[index] = item;
            }
          });
          items = [].concat((0, _toConsumableArray3.default)(lineItems), (0, _toConsumableArray3.default)(newItems));
        }
      } else {
        if (atBeginning) {
          items = [lineItem].concat((0, _toConsumableArray3.default)(lineItems));
        } else {
          items = [].concat((0, _toConsumableArray3.default)(lineItems), [lineItem]);
        }
      }

      var lastLineItem = items[items.length - 1];
      if (lastLineItem) {
        if (typeof lastLineItem.item.value !== 'undefined') {
          items = [].concat((0, _toConsumableArray3.default)(items), [this.getDefaultLineItem()]);
        }
      }
      this.setState({ lineItems: items }, this.calculateFooterFields);
    }
  }, {
    key: 'changeLineItem',
    value: function changeLineItem(index, item) {
      var lineItems = this.state.lineItems;

      if (lineItems.length === index + 1) {
        lineItems.push(this.getDefaultLineItem());
      }
      lineItems[index] = item;
      this.setState({ lineItems: lineItems }, this.calculateFooterFields);
    }
  }, {
    key: 'deleteLineItem',
    value: function deleteLineItem(index) {
      var _state7 = this.state,
          lineItems = _state7.lineItems,
          transaction = _state7.transaction;

      var items = lineItems.filter(function (item, idx) {
        return idx !== index;
      });
      var total = this.calculateTotal(items);
      transaction.reverseChargeAmount = total.reverseChargeAmount;
      this.setState({ lineItems: items }, this.calculateFooterFields);
    }
  }, {
    key: 'calculateTotal',
    value: function calculateTotal(lineItems) {
      var transaction = this.state.transaction;

      var total = lineItems.reduce(function (total, lineItem) {
        total.totalCount += _MyDouble2.default.convertStringToDouble(lineItem.count);
        total.totalQty += _MyDouble2.default.convertStringToDouble(lineItem.qty);
        total.totalFreeQty += _MyDouble2.default.convertStringToDouble(lineItem.freeQty);
        total.subTotal += _MyDouble2.default.convertStringToDouble(lineItem.amount);
        total.reverseChargeAmount += _MyDouble2.default.convertStringToDouble(lineItem.taxAmount) + _MyDouble2.default.convertStringToDouble(lineItem.cess);
        return total;
      }, {
        totalCount: 0,
        totalQty: 0,
        totalFreeQty: 0,
        subTotal: 0,
        reverseChargeAmount: 0
      });
      total.subTotal = _MyDouble2.default.getBalanceAmountWithDecimal(total.subTotal);
      total.lineItemTotalTax = total.reverseChargeAmount;
      if (transaction.isReverseCharge) {
        total.reverseChargeAmount = _MyDouble2.default.getBalanceAmountWithDecimal(total.lineItemTotalTax + _MyDouble2.default.convertStringToDouble(transaction.taxAmount));
      } else {
        total.reverseChargeAmount = 0;
      }
      return total;
    }
  }, {
    key: 'onSave',
    value: function onSave(id) {
      var _this10 = this;

      var txnType = this.props.txnType;
      var lineItems = this.state.lineItems;

      if (!id) {
        return false;
      }
      this.saveAction = id;

      var isFTU = !_FTUHelper2.default.isPurchaseTxnCreated();
      if (!isFTU && settingCache.showLowStockDialog()) {
        var isStockEnabled = settingCache.getStockEnabled();
        if (isStockEnabled && (txnType == _TxnTypeConstant2.default.TXN_TYPE_SALE || txnType == _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER || txnType == _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN)) {
          var _ItemUnitMappingCache = require('../../../Cache/ItemUnitMappingCache');
          var itemUnitMappingCache = new _ItemUnitMappingCache();
          var itemList = lineItems.reduce(function (all, lineItem) {
            var item = lineItem.item;
            if (!item || !item.item || item.value == 0 || item.item.getItemType() == ItemType.ITEM_TYPE_SERVICE) {
              return all;
            }
            var itemUnitId = lineItem.unit;
            var lineItemStock = Number(lineItem.qty) + Number(lineItem.freeQty);
            if (itemUnitId && itemUnitId != 0) {
              var found = lineItem.unitObj.find(function (unit) {
                return unit.value == itemUnitId;
              });
              if (found && found.type != 'base') {
                var mappingObj = itemUnitMappingCache.getItemUnitMapping(found.mappingId);
                var rate = mappingObj['conversionRate'];
                lineItemStock = Number(lineItem.qty) / Number(rate) + Number(lineItem.freeQty) / Number(rate);
              }
            }

            var currentStock = Number(item.item.getAvailableQuantity());
            if (currentStock < lineItemStock) {
              return [].concat((0, _toConsumableArray3.default)(all), [lineItem.item.label]);
            }
            return all;
          }, []);

          if (itemList.length > 0) {
            this.lowStockItemLists = itemList;
            this.setState({
              showLowStockDialog: true
            });
            return false;
          }
        }
      }

      this.setState({
        saveInProgress: true,
        confirm: false
      }, function () {
        _this10.saveTransaction(true);
      });
    }
  }, {
    key: 'saveTransaction',
    value: function saveTransaction(allOK) {
      var _this11 = this;

      if (!allOK) {
        this.setState({
          confirm: false,
          showLowStockDialog: false,
          saveInProgress: false
        });
        return false;
      }
      this.setState({
        confirm: false,
        showLowStockDialog: false
      }, function () {
        var _props3 = _this11.props,
            txnType = _props3.txnType,
            txnId = _props3.txnId;

        var oldTxnObj = _this11.oldTxnObj;
        if (_this11.isEdit) {
          oldTxnObj = oldTxnObj.getTransactionFromId(oldTxnObj.getTxnId());
          var canEditTransaction = oldTxnObj.canEditTransaction();
          if (canEditTransaction != _ErrorCode2.default.SUCCESS) {
            ToastHelper.error(canEditTransaction);
            _this11.setState({
              saveInProgress: false
            });
            return;
          }
        }
        var _state8 = _this11.state,
            _state8$transaction = _state8.transaction,
            party = _state8$transaction.party,
            firm = _state8$transaction.firm,
            displayName = _state8$transaction.displayName,
            billingAddress = _state8$transaction.billingAddress,
            shippingAddress = _state8$transaction.shippingAddress,
            PONo = _state8$transaction.PONo,
            PODate = _state8$transaction.PODate,
            eWayBillNo = _state8$transaction.eWayBillNo,
            invoiceType = _state8$transaction.invoiceType,
            invoiceNo = _state8$transaction.invoiceNo,
            returnNo = _state8$transaction.returnNo,
            invoicePrefix = _state8$transaction.invoicePrefix,
            invoiceDate = _state8$transaction.invoiceDate,
            paymentTerms = _state8$transaction.paymentTerms,
            dueDate = _state8$transaction.dueDate,
            txnReturnRefNumber = _state8$transaction.txnReturnRefNumber,
            txnReturnRefDate = _state8$transaction.txnReturnRefDate,
            placeOfSupply = _state8$transaction.placeOfSupply,
            paymentType = _state8$transaction.paymentType,
            referenceNo = _state8$transaction.referenceNo,
            description = _state8$transaction.description,
            imageId = _state8$transaction.imageId,
            image = _state8$transaction.image,
            imageBlob = _state8$transaction.imageBlob,
            extraFields = _state8$transaction.extraFields,
            transportationDetails = _state8$transaction.transportationDetails,
            discountAmount = _state8$transaction.discountAmount,
            discountPercent = _state8$transaction.discountPercent,
            taxPercent = _state8$transaction.taxPercent,
            taxAmount = _state8$transaction.taxAmount,
            ITC = _state8$transaction.ITC,
            additionalCharges = _state8$transaction.additionalCharges,
            roundOff = _state8$transaction.roundOff,
            roundOffAmount = _state8$transaction.roundOffAmount,
            isReverseCharge = _state8$transaction.isReverseCharge,
            payable = _state8$transaction.payable,
            total = _state8$transaction.total,
            received = _state8$transaction.received,
            balance = _state8$transaction.balance,
            currentTxnBalance = _state8$transaction.currentTxnBalance,
            discountForCash = _state8$transaction.discountForCash,
            cashSale = _state8$transaction.cashSale,
            taxInclusive = _state8$transaction.taxInclusive,
            cashSaleOption = _state8.cashSaleOption,
            transaction = _state8.transaction,
            lineItems = _state8.lineItems,
            selectedTransactionMap = _state8.selectedTransactionMap,
            invoicePrefixOptions = _state8.invoicePrefixOptions;


        try {
          if (cashSale) {
            if (!party || !_MyDouble2.default.convertStringToDouble(party.value)) {
              if (!party || !party.label) {
                if (!cashSaleOption || cashSaleOption && cashSaleOption.value == 0) {
                  var name = _StringConstants2.default.cashSale;
                  var nameCache = new _NameCache2.default();
                  var logic = nameCache.getNameObjectByName(name, txnType);
                  if (!logic) {
                    logic = _this11.createNewParty(name, txnType);
                    _analyticsHelper2.default.pushEvent('Party save direct from sale - ' + txnType);
                  }
                  cashSaleOption = {
                    value: _MyDouble2.default.convertStringToDouble(logic.getNameId()),
                    label: name,
                    balance: _MyDouble2.default.getAmountWithDecimal(logic.getAmount()),
                    partyLogic: logic
                  };
                  _this11.dropdownNames.push(cashSaleOption);
                }
                party = cashSaleOption;
              } else {
                var _name = party.label;
                var _nameCache = new _NameCache2.default();
                var _logic = _nameCache.getNameObjectByName(_name, txnType);
                if (!_logic) {
                  _logic = _this11.createNewParty(_name, txnType);
                  _analyticsHelper2.default.pushEvent('Party save direct from sale - ' + txnType);
                }
                party = {
                  value: _MyDouble2.default.convertStringToDouble(_logic.getNameId()),
                  label: _name,
                  balance: _MyDouble2.default.getAmountWithDecimal(_logic.getAmount()),
                  partyLogic: _logic
                };
                _this11.dropdownNames.push(party);
              }
            }
          } else {
            if (!_MyDouble2.default.convertStringToDouble(party.value)) {
              if (!party.label) {
                ToastHelper.error(_ErrorCode2.default.ERROR_NAME_EMPTY);
                var dropdown = document.querySelector('#salePurchasePartyDropdown');
                if (dropdown) {
                  dropdown.focus();
                }
                _this11.setState({
                  saveInProgress: false
                });
                return false;
              } else {
                var _name2 = party.label;
                var _nameCache2 = new _NameCache2.default();
                var _logic2 = _nameCache2.getNameObjectByName(_name2, null, txnType);
                if (!_logic2) {
                  _logic2 = _this11.createNewParty(_name2, txnType);
                  _analyticsHelper2.default.pushEvent('Party save direct from sale - ' + txnType);
                }
                party = {
                  value: _MyDouble2.default.convertStringToDouble(_logic2.getNameId()),
                  label: _name2,
                  balance: _MyDouble2.default.getAmountWithDecimal(_logic2.getAmount()),
                  partyLogic: _logic2
                };
                _this11.dropdownNames.push(party);
              }
            }
          }
        } catch (e) {
          if (txnType == _TxnTypeConstant2.default.TXN_TYPE_EXPENSE || txnType == _TxnTypeConstant2.default.TXN_TYPE_OTHER_INCOME) {
            ToastHelper.error(_ErrorCode2.default.ERROR_ITEMCATEGORY_NAME_EMPTY_FAILED);
          } else {
            ToastHelper.error(_ErrorCode2.default.ERROR_NAME_DOESNT_EXIST);
          }
          _this11.setState({
            saveInProgress: false
          });
          return false;
        }
        var additionalChargesMap = additionalCharges.reduce(function (all, charge) {
          all['ac' + charge.id + 'Amount'] = charge.value || 0;
          return all;
        }, {
          ac1Amount: 0,
          ac2Amount: 0,
          ac3Amount: 0
        });

        dueDate = dueDate ? _MyDate2.default.getDateObj(dueDate, DATE_FORMATE_FULL, '/') : null;
        invoiceDate = invoiceDate ? _MyDate2.default.getDateObj(invoiceDate, DATE_FORMATE_FULL, '/') : null;
        PODate = PODate ? _MyDate2.default.getDateObj(PODate, DATE_FORMATE_FULL, '/') : null;
        txnReturnRefDate = txnReturnRefDate ? _MyDate2.default.getDateObj(txnReturnRefDate, DATE_FORMATE_FULL, '/') : null;

        if (PODate && PODate instanceof Date && PODate != 'Invalid Date') {
          PODate.setHours(0, 0, 0, 0);
        } else {
          PODate = null;
        }

        if (txnReturnRefDate && txnReturnRefDate instanceof Date && txnReturnRefDate != 'Invalid Date') {
          txnReturnRefDate.setHours(0, 0, 0, 0);
        } else {
          txnReturnRefDate = null;
        }

        if (dueDate && dueDate instanceof Date && dueDate != 'Invalid Date') {
          dueDate.setHours(0, 0, 0, 0);
        } else {
          dueDate = null;
        }
        if (invoiceDate && invoiceDate instanceof Date && invoiceDate != 'Invalid Date') {
          invoiceDate.setHours(0, 0, 0, 0);
        } else {
          invoiceDate = null;
        }

        if (settingCache.isPaymentTermEnabled() && (txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE) && invoiceDate > dueDate) {
          ToastHelper.error('Due date cannot be older than transaction date');
          _this11.setState({
            saveInProgress: false
          });
          return false;
        }

        if (txnId) {
          (0, _SalePurchaseHelpers.additionalAdjustmentForReceivePayment)(txnType, transaction, selectedTransactionMap, _this11.oldTxnObj);
        }
        var txnCurrentBalance = 0;
        if (selectedTransactionMap.size > 0) {
          txnCurrentBalance = _MyDouble2.default.convertStringToDouble(currentTxnBalance);
        } else {
          if (txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT) {
            txnCurrentBalance = _MyDouble2.default.convertStringToDouble(total) + _MyDouble2.default.convertStringToDouble(discountForCash);
          } else {
            txnCurrentBalance = _MyDouble2.default.convertStringToDouble(balance);
          }
        }

        if (_MyDouble2.default.convertStringToDouble(txnCurrentBalance) < 0) {
          if (selectedTransactionMap.size > 0) {
            var errorMsg = '';

            switch (txnType) {
              case _TxnTypeConstant2.default.TXN_TYPE_CASHIN:
                errorMsg = _ErrorCode2.default.ERROR_TXN_LINKED_PAYMENTS_AMOUNT_NEGATIVE_CASHIN;
                break;
              case _TxnTypeConstant2.default.TXN_TYPE_CASHOUT:
                errorMsg = _ErrorCode2.default.ERROR_TXN_LINKED_PAYMENTS_AMOUNT_NEGATIVE_CASHIN;
                break;
              case _TxnTypeConstant2.default.TXN_TYPE_SALE:
                errorMsg = _ErrorCode2.default.ERROR_TXN_LINKED_PAYMENTS_AMOUNT_NEGATIVE_CASHIN;
                break;
              case _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN:
                errorMsg = _ErrorCode2.default.ERROR_TXN_LINKED_PAYMENTS_AMOUNT_NEGATIVE_CASHIN;
                break;
              case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE:
                errorMsg = _ErrorCode2.default.ERROR_TXN_LINKED_PAYMENTS_AMOUNT_NEGATIVE_CASHIN;
                break;
              case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN:
                errorMsg = _ErrorCode2.default.ERROR_TXN_LINKED_PAYMENTS_AMOUNT_NEGATIVE_CASHIN;
                break;
            }

            ToastHelper.error(errorMsg);
            _this11.setState({
              saveInProgress: false
            });
            return false;
          }
        }

        var today = new Date();
        today.setHours(0, 0, 0, 0);

        var billNo = void 0;
        if ((txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN || txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN) && _this11.fieldsVisibility.invoiceNo) {
          billNo = returnNo;
        } else if (_this11.fieldsVisibility.invoiceNo) {
          billNo = invoiceNo;
        } else {
          billNo = '';
        }
        var linksMap = selectedTransactionMap || new _map2.default();
        if (!_this11.isEdit) {
          linksMap = selectedTransactionMap && selectedTransactionMap.size > 0 ? selectedTransactionMap : new _map2.default();
        }

        if (txnType == _TxnTypeConstant2.default.TXN_TYPE_SALE || txnType == _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN || txnType == _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER || txnType == _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN) {
          if (!_this11.isEdit && !settingCache.getPartyShippingAddressEnabled()) {
            shippingAddress = '';
          }
        } else {
          billingAddress = '';
          shippingAddress = '';
        }

        var obj = (0, _extends5.default)({
          txnType: txnType,
          name: party.label,
          imgFile: image || (imageBlob ? 'data:image/jpg;base64,' + imageBlob : ''),
          date: invoiceDate == '' ? today : invoiceDate,
          dueDate: dueDate == '' ? today : dueDate,
          billNo: billNo,
          totalAmount: isReverseCharge ? payable : total,
          amount: txnType !== _TxnTypeConstant2.default.TXN_TYPE_EXPENSE && txnType !== _TxnTypeConstant2.default.TXN_TYPE_OTHER_INCOME ? received : total,
          balance: balance,
          paymentType: paymentType,
          discountAmount: txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHOUT || txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHIN ? discountForCash : discountAmount,
          discountPercent: discountPercent,
          taxAmount: taxAmount,
          transactionTaxId: taxPercent != 0 ? taxPercent : null,
          refNo: paymentType != 1 ? referenceNo : '',
          description: description,
          txnId: _this11.isEdit ? oldTxnObj.getTxnId() : ''
        }, additionalChargesMap, {
          prefix: _this11.fieldsVisibility.invoiceNo && _this11.fieldsVisibility.invoicePrefix ? invoicePrefix : null,
          invoicePrefixOptions: invoicePrefixOptions,
          firmId: firm.getFirmId(),
          invoiceType: invoiceType, // for updating firm table
          customFields: transportationDetails.length ? (0, _stringify2.default)({
            transportation_details: transportationDetails
          }) : '',
          displayName: settingCache.getDisplayNameEnabled() || displayName != '' ? displayName : '',
          reverseCharge: isReverseCharge ? 1 : 0,
          placeOfSupply: placeOfSupply,
          roundOffValue: roundOff ? roundOffAmount : 0,
          txnITCValue: ITC,
          PONumber: PONo,
          PODate: PODate,
          txnReturnRefDate: txnReturnRefDate,
          txnReturnRefNumber: txnReturnRefNumber,
          ewayBillNumber: eWayBillNo,
          txnCurrentBalance: txnCurrentBalance,
          selectedTxnMap: linksMap,
          paymentTermId: settingCache.isPaymentTermEnabled() && paymentTerms ? paymentTerms : 1,
          extraFieldValueArray: extraFields.map(function (field) {
            var udfField = new _udfValueModel2.default();
            var value = field.value;
            if (field.udfModel.getUdfFieldDataType() == udfConstants.DATA_TYPE_DATE) {
              if (value) {
                var format = field.udfModel.getUdfFieldDataFormat() == _StringConstants2.default.dateMonthYear ? 'dd/mm/yyyy' : 'mm/yyyy';
                var date = _MyDate2.default.getDateObj(value, format, '/');
                if (date && date instanceof Date && date != 'Invalid Date') {
                  value = _MyDate2.default.getDate('y-m-d H:M:S', date);
                } else {
                  value = '';
                }
              }
            }
            udfField.setUdfFieldId(field.udfModel.getUdfFieldId());
            udfField.setUdfFieldValue(value);
            udfField.setUdfFieldType(udfConstants.FIELD_TYPE_TRANSACTION);
            return udfField;
          }),
          billingAddress: billingAddress,
          shippingAddress: shippingAddress,
          taxInclusive: taxInclusive
        });

        var TransactionLogic = require('../../../BizLogic/TransactionLogic.js');
        var transactionLogic = new TransactionLogic();

        var statusCode = void 0;
        if (_this11.isEdit) {
          obj.imageId = imageId || 0;
          statusCode = transactionLogic.UpdateTransactionIntr(obj, oldTxnObj, lineItems, discountForCash);
        } else {
          statusCode = transactionLogic.SaveTransactionIntr(obj, _this11.oldTxnObj, lineItems, discountForCash);
        }
        _this11.setState({
          saveInProgress: false
        });
        if (statusCode === _ErrorCode2.default.ERROR_TXN_SAVE_SUCCESS) {
          document.dispatchEvent(new CustomEvent('saveTxnEvent'));
          ToastHelper.success(statusCode);
          switch (_this11.saveAction) {
            case 'submit':
              if (oldTxnObj && oldTxnObj.getTxnType() == _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER && txnType == _TxnTypeConstant2.default.TXN_TYPE_SALE) {
                _analyticsHelper2.default.pushEvent('Sale Save - 1');
                _analyticsHelper2.default.pushEvent('Sale Order Convert to Sale Save - 1');
              } else {
                _analyticsHelper2.default.pushEvent('Add ' + _TxnTypeConstant2.default.getTxnTypeForUI(txnType) + ' Save - 1');
                _analyticsHelper2.default.pushEvent('Sale Order Convert to Sale Open - 1');
              }
              unmountReactComponent(document.querySelector('#salePurchaseContainer'));
              setTimeout(function () {
                if (!_this11.isEdit || _this11.isConvert) {
                  _this11.invoicePreviewOnSave(obj['txnId']);
                } else if (_this11.isEdit && _MyDouble2.default.convertStringToDouble(oldTxnObj.getTxnType()) === _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER && txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE) {
                  if (!settingCache.getIsInvoicePreviewDisabled()) {
                    _this11.openPreview(obj['txnId']);
                  }
                } else if (_this11.isEdit && _MyDouble2.default.convertStringToDouble(oldTxnObj.getTxnType()) === _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN && txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE) {
                  if (!settingCache.getIsInvoicePreviewDisabled()) {
                    _this11.openPreview(obj['txnId']);
                  }
                }
              });
              if (typeof onResume === 'function') {
                onResume(true, obj['name'].toLowerCase());
              } else {
                logger.error(new Error('onResume is not a function'));
              }
              break;
            case 'submitNew':
              _analyticsHelper2.default.pushEvent('Add ' + _TxnTypeConstant2.default.getTxnTypeForUI(txnType) + ' Save - 1');
              unmountReactComponent(document.querySelector('#salePurchaseContainer'));
              setTimeout(function () {
                var MountComponent = require('../MountComponent').default;
                var Component = require('../SalePurchaseContainer').default;
                MountComponent(Component, document.querySelector('#salePurchaseContainer'), {
                  txnType: txnType,
                  invoiceDate: obj.date
                });
              });
              if (typeof onResume === 'function') {
                onResume(true, obj['name'].toLowerCase());
              } else {
                logger.error(new Error('onResume is not a function'));
              }
              // this.init();
              break;
            case 'submitPrint':
              var ThermalPrinterConstant = require('../../../Constants/ThermalPrinterConstant');
              if (settingCache.getDefaultPrinterType() == ThermalPrinterConstant.THERMAL_PRINTER) {
                var PrintUtil = require('../../../Utilities/PrintUtil');
                PrintUtil.printTransactionUsingThermalPrinter(obj['txnId']);
              } else {
                var TransactionHTMLGenerator = require('../../../ReportHTMLGenerator/TransactionHTMLGenerator');
                var transactionHTMLGenerator = new TransactionHTMLGenerator();
                var html = transactionHTMLGenerator.getTransactionHTML(obj['txnId']);
                if (html) {
                  var PDFHandler = require('../../../Utilities/PDFHandler');
                  var pdfHandler = new PDFHandler();
                  pdfHandler.print(html);
                }
              }
              try {
                if (typeof onResume === 'function') {
                  onResume(true, obj['name'].toLowerCase());
                } else {
                  logger.error(new Error('onResume is not a function'));
                }
              } catch (err) {
                logger.error(err);
              }

              _analyticsHelper2.default.pushEvent('Add ' + _TxnTypeConstant2.default.getTxnTypeForUI(txnType) + ' Save - 1');

              _analyticsHelper2.default.pushEvent(_TxnTypeConstant2.default.getTxnTypeForUI(txnType) + ' Print - 1');
              unmountReactComponent(document.querySelector('#salePurchaseContainer'));
              break;
            case 'submitSend':
              _this11.openPreview(obj['txnId']);
              _analyticsHelper2.default.pushEvent('Add ' + _TxnTypeConstant2.default.getTxnTypeForUI(txnType) + ' Save - 1');

              _analyticsHelper2.default.pushEvent(_TxnTypeConstant2.default.getTxnTypeForUI(txnType) + ' Preview - 1');
              unmountReactComponent(document.querySelector('#salePurchaseContainer'));
              // onResume(true, obj['name'].toLowerCase());
              break;
          }
        } else {
          if (statusCode) {
            ToastHelper.error(statusCode);
          }
        }
      });
    }
  }, {
    key: 'openPreview',
    value: function openPreview(txnId) {
      var frameDiv = document.querySelector('#frameDiv');
      try {
        unmountReactComponent(frameDiv);
      } catch (ex) {}
      frameDiv.innerHTML = '\n            <div id="invoicePreviewWrapper" style="width:100%;height: 100%;"></div>\n\t\t\t<link rel="stylesheet" href="../styles/invoicePreview.css">\n\t\t\t<link rel="stylesheet" href="../styles/collectPayment.css">\n\t\t';
      var MountComponent = require('../MountComponent').default;
      var InvoicePreview = require('../InvoicePreview').default;
      this.showDefaultPage = false;
      setTimeout(function () {
        MountComponent(InvoicePreview, document.getElementById('invoicePreviewWrapper'), { transactionId: txnId });
      }, 10);
      document.querySelector('#modelContainer').style.display = 'block';
    }
  }, {
    key: 'invoicePreviewOnSave',
    value: function invoicePreviewOnSave(txnId) {
      var txnType = this.props.txnType;

      try {
        var isPreviewDisabled = settingCache.getIsInvoicePreviewDisabled();
        if (!isPreviewDisabled && (txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE || txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN || txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER || txnType === _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE || txnType === _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN) && (!this.isEdit || this.isConvert)) {
          this.openPreview(txnId);
        }
      } catch (e) {}
    }
  }, {
    key: 'changeTransactionLinks',
    value: function changeTransactionLinks(txnListB2B, selectedTransactionMap, copyOfSelectedTransactionMap) {
      var txnType = this.props.txnType;
      var transaction = this.state.transaction;

      var balance = transaction.balance;

      if (selectedTransactionMap.size > 0) {
        if (txnType != _TxnTypeConstant2.default.TXN_TYPE_CASHIN && txnType != _TxnTypeConstant2.default.TXN_TYPE_CASHOUT && transaction.received == 0) {
          this.fieldsVisibility.paymentType = false;
        } else {
          this.fieldsVisibility.paymentType = true;
        }

        this.fieldsVisibility.received = false;
        this.fieldsVisibility.balance = false;

        this.fieldsVisibility.currentTxnBalance = true;

        this.fieldsVisibility.totalReceivedAmount = true;

        if (this.fieldsVisibility.linkPayment) {
          this.fieldsVisibility.paymentHistory = true;
        }
        this.fieldsVisibility.nameDisabled = false;

        if (txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT) {
          this.fieldsVisibility.totalReceivedAmount = false;
          if (_MyDouble2.default.convertStringToDouble(transaction.currentTxnBalance) != 0) {
            // this.fieldsVisibility.discountForCash = true;
            this.labels.balance = 'UNUSED AMOUNT';
          }
        }
      } else {
        if (txnType != _TxnTypeConstant2.default.TXN_TYPE_CASHIN && txnType != _TxnTypeConstant2.default.TXN_TYPE_CASHOUT) {
          this.fieldsVisibility.paymentType = true;
        }

        this.fieldsVisibility.received = true;
        this.fieldsVisibility.balance = true;

        this.fieldsVisibility.currentTxnBalance = false;

        this.fieldsVisibility.totalReceivedAmount = false;
        this.fieldsVisibility.paymentHistory = false;
        this.fieldsVisibility.nameDisabled = true;

        if (txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT) {
          balance = _MyDouble2.default.convertStringToDouble(transaction.total) + _MyDouble2.default.convertStringToDouble(transaction.discountForCash);
          this.labels.balance = 'BALANCE';
          this.fieldsVisibility.received = false;
          this.fieldsVisibility.totalDisabled = false;
          this.fieldsVisibility.total = true;
        }
      }

      this.setState({
        transaction: (0, _extends5.default)({}, transaction, {
          balance: balance
        }),
        txnListB2B: txnListB2B,
        selectedTransactionMap: selectedTransactionMap,
        copyOfSelectedTransactionMap: copyOfSelectedTransactionMap
      });
    }
  }, {
    key: 'createItem',
    value: function createItem(item) {
      this.dropdownItems.push(item);
      this.forceUpdate();
    }
  }, {
    key: 'createParty',
    value: function createParty(item) {
      this.dropdownNames.push(item);
    }
  }, {
    key: 'render',
    value: function render() {
      var _props4 = this.props,
          classes = _props4.classes,
          txnType = _props4.txnType,
          txnId = _props4.txnId;
      var _state9 = this.state,
          firmList = _state9.firmList,
          quickEntryOpen = _state9.quickEntryOpen,
          lineItems = _state9.lineItems,
          transaction = _state9.transaction,
          invoicePrefixOptions = _state9.invoicePrefixOptions,
          paymentTermsOptions = _state9.paymentTermsOptions,
          showDescription = _state9.showDescription,
          showReverseCharge = _state9.showReverseCharge,
          showCashSale = _state9.showCashSale,
          totalCount = _state9.totalCount,
          totalQty = _state9.totalQty,
          totalFreeQty = _state9.totalFreeQty,
          subTotal = _state9.subTotal,
          lineItemTotalTax = _state9.lineItemTotalTax,
          selectedTransactionMap = _state9.selectedTransactionMap,
          copyOfSelectedTransactionMap = _state9.copyOfSelectedTransactionMap,
          txnListB2B = _state9.txnListB2B,
          confirm = _state9.confirm,
          saveInProgress = _state9.saveInProgress,
          isLicenseExpired = _state9.isLicenseExpired,
          showNewBankAccountDialog = _state9.showNewBankAccountDialog,
          showLowStockDialog = _state9.showLowStockDialog;
      var cashSale = transaction.cashSale,
          firm = transaction.firm,
          party = transaction.party,
          displayName = transaction.displayName,
          billingAddress = transaction.billingAddress,
          shippingAddress = transaction.shippingAddress,
          PONo = transaction.PONo,
          PODate = transaction.PODate,
          eWayBillNo = transaction.eWayBillNo,
          extraFields = transaction.extraFields,
          invoiceType = transaction.invoiceType,
          invoicePrefix = transaction.invoicePrefix,
          returnNo = transaction.returnNo,
          invoiceNo = transaction.invoiceNo,
          invoiceDate = transaction.invoiceDate,
          paymentTerms = transaction.paymentTerms,
          dueDate = transaction.dueDate,
          dueDays = transaction.dueDays,
          txnReturnRefNumber = transaction.txnReturnRefNumber,
          txnReturnRefDate = transaction.txnReturnRefDate,
          placeOfSupply = transaction.placeOfSupply,
          description = transaction.description,
          transportationDetails = transaction.transportationDetails,
          paymentType = transaction.paymentType,
          referenceNo = transaction.referenceNo,
          image = transaction.image,
          imageBlob = transaction.imageBlob,
          additionalCharges = transaction.additionalCharges,
          discountPercent = transaction.discountPercent,
          discountAmount = transaction.discountAmount,
          taxPercent = transaction.taxPercent,
          taxAmount = transaction.taxAmount,
          ITC = transaction.ITC,
          ITCVisibility = transaction.ITCVisibility,
          roundOff = transaction.roundOff,
          roundOffAmount = transaction.roundOffAmount,
          isReverseCharge = transaction.isReverseCharge,
          reverseChargeAmount = transaction.reverseChargeAmount,
          total = transaction.total,
          payable = transaction.payable,
          received = transaction.received,
          totalReceivedAmount = transaction.totalReceivedAmount,
          receivedLater = transaction.receivedLater,
          balance = transaction.balance,
          currentTxnBalance = transaction.currentTxnBalance,
          discountForCash = transaction.discountForCash,
          taxInclusive = transaction.taxInclusive;


      if (selectedTransactionMap.size > 0) {
        txnCurrentBalance = _MyDouble2.default.convertStringToDouble(currentTxnBalance);
      } else {
        txnCurrentBalance = _MyDouble2.default.convertStringToDouble(balance);
      }

      return _react2.default.createElement(
        _styles.MuiThemeProvider,
        { theme: theme },
        confirm && _react2.default.createElement(_ConfirmModal2.default, {
          title: this.confirmModalTitle,
          message: this.confirmModalMessage,
          onClose: this.saveTransaction
        }),
        showLowStockDialog && _react2.default.createElement(_LowStockConfirmModal2.default, {
          title: 'Confirm',
          message: _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement(
              'div',
              null,
              'Your stock for following item is not sufficient to fulfill the sale.'
            ),
            this.lowStockItemLists.map(function (name) {
              return _react2.default.createElement(
                'div',
                { key: name },
                ' ',
                name,
                ' '
              );
            }),
            _react2.default.createElement(
              'div',
              null,
              'Do you want to continue ?'
            )
          ),
          cancelText: 'No',
          OKText: 'Yes',
          onClose: this.saveTransaction
        }),
        showNewBankAccountDialog && _react2.default.createElement(_AddNewBankAccountModal2.default, { onCreate: this.refreshBankAccounts }),
        _react2.default.createElement(
          _Dialog2.default,
          {
            fullScreen: true,
            open: this.state.open,
            onClose: this.onClose,
            disableEnforceFocus: true,
            classes: {
              root: classes.root
            }
            // TransitionComponent={Transition}
            , className: 'salePurchaseDialog vyaparFormToValidate'
          },
          _react2.default.createElement(
            'div',
            {
              className: (0, _classnames2.default)(classes.salePurchaseContainer, classes.flex1, classes.width1367, 'd-flex salePurchaseContainer flex-column'),
              id: 'SalePurchaseFormDialogRef'
            },
            isLicenseExpired && _react2.default.createElement(_LicenseExpiredModal2.default, { onClose: this.licenseExpired }),
            _react2.default.createElement(_nav2.default, {
              fieldsVisibility: this.fieldsVisibility,
              txnType: txnType,
              onClose: this.onClose,
              changeField: this.changeField,
              firmList: firmList,
              firm: firm,
              changeFirm: this.changeFirm,
              changeCashSale: this.changeCashSale,
              cashSale: cashSale,
              changeReverseCharge: this.changeReverseCharge,
              showCashSale: showCashSale,
              showReverseCharge: showReverseCharge,
              isReverseCharge: isReverseCharge,
              invoiceType: invoiceType,
              title: this.title,
              isEdit: this.isEdit
            }),
            _react2.default.createElement(
              'div',
              { className: (0, _classnames2.default)(classes.flex1, classes.overflowAuto) },
              _react2.default.createElement(_HeaderFields2.default, {
                fieldsVisibility: this.fieldsVisibility,
                labels: this.labels,
                items: this.dropdownNames,
                changeField: this.changeField,
                changeFields: this.changeFields,
                changeParty: this.changeParty,
                createParty: this.createParty,
                changePrefix: this.changePrefix,
                changeDueDate: this.changeDueDate,
                changePaymentTerms: this.changePaymentTerms,
                changeExtraField: this.changeExtraField,
                cashSale: cashSale,
                invoicePrefixOptions: invoicePrefixOptions,
                paymentTermsOptions: paymentTermsOptions,
                listOfStates: listOfStates,
                txnType: txnType,
                party: party,
                displayName: displayName,
                billingAddress: billingAddress,
                shippingAddress: shippingAddress,
                PONo: PONo,
                PODate: PODate,
                eWayBillNo: eWayBillNo,
                extraFields: extraFields,
                invoiceType: invoiceDate,
                returnNo: returnNo,
                invoicePrefix: invoicePrefix,
                invoiceNo: invoiceNo,
                invoiceDate: invoiceDate,
                paymentTerms: paymentTerms,
                dueDate: dueDate,
                dueDays: txnCurrentBalance > 0 ? dueDays : 0,
                txnReturnRefNumber: txnReturnRefNumber,
                txnReturnRefDate: txnReturnRefDate,
                placeOfSupply: placeOfSupply,
                changePlaceOfSupply: this.changePlaceOfSupply,
                isFTU: this.isFTU,
                isEdit: this.isEdit,
                isConvert: this.isConvert
              }),
              this.fieldsVisibility.lineItems && _react2.default.createElement(
                _react2.default.Fragment,
                null,
                _react2.default.createElement(_LineItemsContainer2.default, {
                  lineItemColumnsVisibility: this.lineItemColumnsVisibility,
                  quickEntryOpen: quickEntryOpen,
                  items: this.dropdownItems,
                  lineItems: lineItems,
                  txnType: txnType,
                  party: party,
                  cashSale: cashSale,
                  firmLogic: firm,
                  getDefaultLineItem: this.getDefaultLineItem,
                  addLineItem: this.addLineItem,
                  changeLineItem: this.changeLineItem,
                  deleteLineItem: this.deleteLineItem,
                  placeOfSupply: placeOfSupply,
                  totalCount: totalCount,
                  totalQty: totalQty,
                  totalFreeQty: totalFreeQty,
                  subTotal: subTotal,
                  createItem: this.createItem,
                  taxInclusive: taxInclusive,
                  toggleTaxInclusive: this.toggleTaxInclusive,
                  isFTU: this.isFTU,
                  isEdit: this.isEdit,
                  changeLineItemVisibility: this.changeLineItemVisibility
                })
              ),
              _react2.default.createElement(_FooterFields2.default, {
                fieldsVisibility: this.fieldsVisibility,
                labels: this.labels,
                items: this.dropdownNames,
                cashSale: cashSale,
                taxDropdownList: this.transactionTaxDropdownList,
                changeField: this.changeField,
                changeFields: this.changeFields,
                changeCheckbox: this.changeCheckbox,
                changeTransportationField: this.changeTransportationField,
                toggleDescription: this.toggleDescription,
                showDescription: showDescription,
                paymentTypeOptions: this.paymentTypeOptions,
                txnType: txnType,
                description: description,
                transportationDetails: transportationDetails,
                paymentType: paymentType,
                imageBlob: imageBlob,
                image: image,
                changeImage: this.changeImage,
                discountPercent: discountPercent,
                discountAmount: discountAmount,
                taxPercent: taxPercent,
                taxAmount: taxAmount,
                ITCVisibility: ITCVisibility,
                ITC: ITC,
                additionalCharges: additionalCharges,
                roundOff: roundOff,
                roundOffAmount: roundOffAmount,
                isReverseCharge: isReverseCharge,
                reverseChargeAmount: reverseChargeAmount,
                total: total,
                totalQty: totalQty + totalFreeQty,
                payable: payable,
                received: received,
                totalReceivedAmount: totalReceivedAmount,
                receivedLater: receivedLater,
                discountForCash: discountForCash,
                balance: balance,
                currentTxnBalance: currentTxnBalance,
                subTotal: subTotal,
                referenceNo: referenceNo,
                lineItemTotalTax: lineItemTotalTax,
                currencySymbol: this.currencySymbol,
                selectedTransactionMap: selectedTransactionMap,
                changePaymentType: this.changePaymentType,
                isFTU: this.isFTU
              })
            ),
            _react2.default.createElement(_footerActions2.default, {
              fieldsVisibility: this.fieldsVisibility,
              txnType: txnType,
              txnListB2B: txnListB2B,
              copyOfSelectedTransactionMap: copyOfSelectedTransactionMap,
              selectedTransactionMap: selectedTransactionMap,
              changeFields: this.changeFields,
              onSave: this.onSave,
              transaction: transaction,
              txnId: txnId,
              oldTxnObj: this.oldTxnObj,
              saveInProgress: saveInProgress || transaction.submitButtonDisabled,
              changeTransactionLinks: this.changeTransactionLinks,
              closedOn: transaction.closedOn,
              linkedTransactionInvoiceNumber: transaction.linkedTransactionInvoiceNumber,
              linkedTransactionId: transaction.linkedTransactionId,
              linkedtxnTransactionType: transaction.linkedtxnTransactionType
            })
          )
        )
      );
    }
  }]);
  return SalePurchaseContainer;
}(_react.Component);

SalePurchaseContainer.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  txnType: _propTypes2.default.number.isRequired,
  txnId: _propTypes2.default.number,
  txnObj: _propTypes2.default.object,
  invoiceDate: _propTypes2.default.instanceOf(Date)
};

exports.default = (0, _styles.withStyles)(styles)(SalePurchaseContainer);