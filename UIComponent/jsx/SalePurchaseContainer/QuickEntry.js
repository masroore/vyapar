Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/core/styles');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _themes = require('../../../themes');

var _themes2 = _interopRequireDefault(_themes);

var _QuickEntryIcon = require('../icons/QuickEntryIcon');

var _QuickEntryIcon2 = _interopRequireDefault(_QuickEntryIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import ItemsDropdown from '../UIControls/ItemsDropdown';

// import TextField from '@material-ui/core/TextField';
// import CheckIcon from '@material-ui/icons/Check';

var theme = (0, _themes2.default)();
// import DatePicker from 'react-datepicker';

// import Select, {components} from 'react-select';

var styles = function styles() {
	return {
		quickEntryContainer: {
			borderRadius: 4,
			margin: '0 35px'
		},
		quickEntryIcon: {
			color: '#4bb3fd',
			cursor: 'pointer'
		},
		quickEntryButton: {
			margin: '-2px auto auto -10px'
			// width20: {
			// 	width: 20
			// },
			// minWidth50: {
			// 	minWidth: 50
			// },
			// minWidth60: {
			// 	minWidth: 59
			// },
			// minWidth75: {
			// 	minWidth: 75
			// },
			// minWidth100: {
			// 	minWidth: 100
			// },
			// textField: {
			// 	fontSize: 13,
			// 	color: '#222A3F',
			// 	zIndex: 1,
			// 	height: 36
			// },
			// notchedOutlineAmt: {
			// 	borderTopRightRadius: 0,
			// 	borderBottomRightRadius: 0
			// },
			// quickEntryRowContainer: {
			// 	height: 36
			// },
			// quickEntryOpen: {
			// 	background: '#fff',
			// 	border: '1px solid #DDDDDD',
			// 	height: 110,
			// 	padding: '20px 5px 20px 5px'
			// },
			// quickEntrySubmitButton: {
			// 	boxShadow: 'none',
			// 	textTransform: 'none',
			// 	fontSize: 16,
			// 	minWidth: 40,
			// 	padding: '6px 12px',
			// 	border: '1px solid',
			// 	borderLeft: 0,
			// 	lineHeight: 1.5,
			// 	background: '#4bb3fd',
			// 	borderBottomLeftRadius: 0,
			// 	borderTopLeftRadius: 0,
			// 	'&:hover': {
			// 		background: '#4bb3fd'
			// 	}
			// },
			// quickEntryEnterItemDropdown: {
			// 	minWidth: 160,
			// 	width: 160,
			// 	borderRadius: 0,
			// 	fontSize: 13,
			// 	height: 36
			// },
			// colorWhite: {
			// 	color: '#fff'
			// },
			// quickEntryDateInput: {
			// 	padding: '0 10px'
			// },
			// quickMultilineInputLabel: {
			// 	marginTop: -5
			// },
			// quickEntryInputPadding: {
			// 	padding: '0 10px'
			// },
			// textFieldLabel: {
			// 	fontSize: 12,
			// 	fontFamily: 'RobotoMedium',
			// 	color: 'rgba(34, 42, 63, 0.5)',
			// 	transform: 'translate(14px, 13px) scale(1)'
			// }
		} };
};

// const TextBoxStyles = {
// 	textFieldContainer: {
// 		height: 36
// 	},
// 	textField: {
// 		fontSize: 13,
// 		color: '#222A3F',
// 		zIndex: 1,
// 		height: 36
// 	},
// 	textFieldLabel: {
// 		fontSize: 12,
// 		fontFamily: 'RobotoMedium',
// 		color: 'rgba(34, 42, 63, 0.5)',
// 		transform: 'translate(14px, 13px) scale(1)'
// 	},
// 	quickEntryInputPadding: {
// 		padding: '0 10px'
// 	},
// 	notchedOutline: {
// 		background: '#fff'
// 	}
// };
//
// const TextBox = withStyles(TextBoxStyles)((props) => {
// 	const {classes, label, value, onChange, width, input, outline, labelRoot} = props;
// 	return (<TextField
// 		label={label}
// 		value={value}
// 		onChange={onChange}
// 		type="text"
// 		className={classNames(classes.textFieldContainer, width || '')}
// 		InputLabelProps={{
// 			classes: {
// 				root: labelRoot || classes.textFieldLabel
// 			}
// 		}}
// 		InputProps={{
// 			classes: {
// 				input: input || classNames(classes.textField, classes.quickEntryInputPadding),
// 				notchedOutline: outline || classes.notchedOutline
// 			}
// 		}}
// 		margin="none"
// 		variant="outlined"
// 	/>);
// });
// TextBox.propTypes = {
// 	classes: PropTypes.object,
// 	label: PropTypes.string,
// 	value: PropTypes.any,
// 	width: PropTypes.string,
// 	input: PropTypes.string,
// 	onChange: PropTypes.func,
// 	outline: PropTypes.string
// };

var QuickEntry = function (_Component) {
	(0, _inherits3.default)(QuickEntry, _Component);

	function QuickEntry(props) {
		(0, _classCallCheck3.default)(this, QuickEntry);

		var _this = (0, _possibleConstructorReturn3.default)(this, (QuickEntry.__proto__ || (0, _getPrototypeOf2.default)(QuickEntry)).call(this, props));

		_this.state = {
			quickEntryOpen: false
		};
		_this.selectItem = _this.selectItem.bind(_this);
		_this.submitQuickEntry = _this.submitQuickEntry.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(QuickEntry, [{
		key: 'selectItem',
		value: function selectItem(item) {
			this.setState({ quickEntrySelectedItem: item.value });
		}
	}, {
		key: 'submitQuickEntry',
		value: function submitQuickEntry(e) {
			e.preventDefault();
		}
	}, {
		key: 'render',
		value: function render() {
			var _props = this.props,
			    classes = _props.classes,
			    toggleQuickEntry = _props.toggleQuickEntry,
			    quickEntryOpen = _props.quickEntryOpen;
			// const {quickEntrySelectedItem} = this.state;

			return _react2.default.createElement(
				_styles.MuiThemeProvider,
				{ theme: theme },
				_react2.default.createElement(
					'div',
					{ className: (0, _classnames2.default)(classes.quickEntryContainer) },
					_react2.default.createElement(
						'div',
						{ className: 'd-flex align-items-center' },
						_react2.default.createElement(_QuickEntryIcon2.default, { onClick: toggleQuickEntry, className: classes.quickEntryIcon }),
						_react2.default.createElement(
							_Button2.default,
							{ color: 'primary', variant: 'text', tabIndex: -1, className: classes.quickEntryButton, onClick: toggleQuickEntry },
							quickEntryOpen ? 'Close' : '',
							' Quick Entry'
						)
					)
				)
			);
		}
	}]);
	return QuickEntry;
}(_react.Component);

QuickEntry.propTypes = {
	classes: _propTypes2.default.object,
	items: _propTypes2.default.array,
	toggleQuickEntry: _propTypes2.default.func,
	quickEntryOpen: _propTypes2.default.bool
};

exports.default = (0, _styles.withStyles)(styles)(QuickEntry);