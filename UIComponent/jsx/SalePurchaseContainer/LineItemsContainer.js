Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/core/styles');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _themes = require('../../../themes');

var _themes2 = _interopRequireDefault(_themes);

var _BarcodeIcon = require('../icons/BarcodeIcon');

var _BarcodeIcon2 = _interopRequireDefault(_BarcodeIcon);

var _SettingsOutlined = require('@material-ui/icons/SettingsOutlined');

var _SettingsOutlined2 = _interopRequireDefault(_SettingsOutlined);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _ExpandMore = require('@material-ui/icons/ExpandMore');

var _ExpandMore2 = _interopRequireDefault(_ExpandMore);

var _SettingCache = require('../../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

var _ItemCategoryCache = require('../../../Cache/ItemCategoryCache');

var _ItemCategoryCache2 = _interopRequireDefault(_ItemCategoryCache);

var _TxnTypeConstant = require('../../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _ItemType = require('../../../Constants/ItemType');

var ItemType = _interopRequireWildcard(_ItemType);

var _Menu = require('@material-ui/core/Menu');

var _Menu2 = _interopRequireDefault(_Menu);

var _MenuItem = require('@material-ui/core/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _LineItem = require('./LineItem');

var _LineItem2 = _interopRequireDefault(_LineItem);

var _BarcodeModal = require('../BarcodeModal');

var _BarcodeModal2 = _interopRequireDefault(_BarcodeModal);

var _ClickAwayListener = require('@material-ui/core/ClickAwayListener');

var _ClickAwayListener2 = _interopRequireDefault(_ClickAwayListener);

var _Paper = require('@material-ui/core/Paper');

var _Paper2 = _interopRequireDefault(_Paper);

var _AddCircleOutline = require('@material-ui/icons/AddCircleOutline');

var _AddCircleOutline2 = _interopRequireDefault(_AddCircleOutline);

var _Checkbox = require('@material-ui/core/Checkbox');

var _Checkbox2 = _interopRequireDefault(_Checkbox);

var _FormControlLabel = require('@material-ui/core/FormControlLabel');

var _FormControlLabel2 = _interopRequireDefault(_FormControlLabel);

var _Queries = require('../../../Constants/Queries');

var _Queries2 = _interopRequireDefault(_Queries);

var _MyDouble = require('../../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _ConfirmModal = require('../ConfirmModal');

var _ConfirmModal2 = _interopRequireDefault(_ConfirmModal);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var theme = (0, _themes2.default)();
var settingCache = new _SettingCache2.default();

var styles = function styles() {
	return {
		lineItemsContainer: {
			marginTop: 2,
			marginLeft: 0,
			marginRight: 0,
			overflowX: 'auto'
		},
		additionalColumns: {
			width: '7%',
			minWidth: 50,
			paddingLeft: 5
		},
		lineItemColumn: {
			borderRight: '1px solid #DDDDDD',
			borderTop: '1px solid #DDDDDD',
			fontSize: 12,
			color: '#284561',
			fontFamily: 'RobotoMedium',
			textTransform: 'uppercase',
			'&:last-child': {
				borderRight: 0
			}
		},
		lineItemFooterColumn: {
			fontSize: 12,
			color: '#284561',
			fontFamily: 'RobotoMedium',
			textTransform: 'uppercase',
			'&:last-child': {
				borderRight: 0
			}
		},
		lineItemFooterColumnText: {
			textAlign: 'right',
			paddingRight: 15
		},
		lineItemWrapper: {
			width: '100%',
			marginTop: 20
			// maxHeight: 280,
			// overflowY: 'auto',
		},
		header: {
			height: 40,
			border: '1px solid #DDDDDD',
			background: '#fff',
			boxShadow: '0 2px 4px -4px #000',
			'&:hover .line-item-cross-icon-opacity': {
				opacity: 1
			}
		},
		footer: {
			height: 40,
			border: '1px solid #DDDDDD',
			background: '#fff',
			borderRight: 0,
			margin: 0,
			padding: '0 20px',
			paddingLeft: 0,
			marginLeft: -1,
			borderLeft: 0
		},
		footerText: {
			fontSize: 12,
			fontFamily: 'RobotoMedium'
		},
		SrNo: {
			textAlign: 'center',
			minWidth: 30,
			width: '3%',
			maxWidth: 49
		},
		barcodeContainer: {
			borderRadius: 5,
			height: 25,
			width: 32,
			padding: 0,
			margin: '0 auto',
			cursor: 'pointer',
			background: 'rgba(27, 218, 148, .15)'
		},
		barcodeIcon: {
			color: 'rgba(34,58,82,0.54)',
			marginTop: 4,
			paddingLeft: 3
		},
		item: {
			width: '32%',
			minWidth: '15%',
			'&.itemColumnHeader': {
				width: '40%'
			}
		},
		description: {
			width: '10%',
			maxWidth: 100,
			minWidth: 50
		},
		count: {
			width: '7%',
			minWidth: 30,
			maxWidth: 65,
			paddingLeft: '4px !important'
		},
		slNo: {
			width: '8%',
			maxWidth: 125,
			minWidth: 40
		},
		batchNo: {
			width: '7%',
			maxWidth: 90,
			minWidth: 40
		},
		expDate: {
			width: '7%',
			maxWidth: 90,
			minWidth: 30
		},
		mfgDate: {
			width: '7%',
			maxWidth: 90,
			minWidth: 30
		},
		mrp: {
			width: '7%',
			maxWidth: 90,
			minWidth: 30
		},
		size: {
			width: '7%',
			maxWidth: 90,
			minWidth: 30
		},
		qty: {
			width: '7%',
			minWidth: 50
		},
		freeQty: {
			width: '7%',
			minWidth: 50
		},
		priceUnit: {
			width: '10%',
			minWidth: 60
		},
		unit: {
			width: '6%',
			minWidth: 60
		},
		disc: {
			width: '12%',
			paddingLeft: 0,
			minWidth: 140
		},
		tax: {
			width: '15%',
			minWidth: 140,
			paddingLeft: 0
		},
		cess: {
			width: '8%',
			minWidth: 60
		},
		amount: {
			width: '10%',
			minWidth: 100
		},
		columnHeader: {
			height: 18,
			paddingTop: 3,
			textAlign: 'center'
		},
		subHeader: {
			color: '#284561',
			fontSize: 10,
			fontFamily: 'roboto',
			width: '100%',
			borderTop: '1px solid #DDDDDD'
		},
		left: {
			height: 20,
			borderRight: '1px solid #ddd;'
		},
		height36: {
			height: 36
		},
		discountPercent: {
			width: '40%'
		},
		discountAmount: {
			width: '60%'
		},
		taxAmount: {
			width: '45%'
		},
		taxPercent: {
			width: '55%'
		},
		pl8: {
			paddingLeft: 8
		},
		pr7: {
			paddingRight: 7
		},
		positionRelative: {
			position: 'relative'
		},
		taxInclusiveWrapper: {
			width: '100%',
			minWidth: 90,
			whiteSpace: 'nowrap',
			left: 0,
			cursor: 'pointer'
		},
		taxInclusive: {
			justifyContent: 'space-between',
			position: 'absolute',
			top: -20,
			background: '#fff',
			fontSize: 12,
			color: 'rgba(34, 58, 82, 0.54)',
			textTransform: 'none',
			border: '1px solid #DDDDDD',
			borderRadius: 2,
			height: 19,
			padding: '3px 5px 2px 5px',
			marginLeft: -8,
			width: '100%'
		},
		expandMoreIcon: {
			fontSize: 16,
			marginLeft: 5,
			position: 'relative',
			top: -2
		},
		columnsMenuContainer: {
			position: 'fixed',
			right: 0,
			padding: 10,
			textAlign: 'left',
			zIndex: 9
		},
		columnsMenuRoot: {
			position: 'relative'
		},
		columnsMenuLabel: {
			whiteSpace: 'nowrap',
			textTransform: 'none'
		},
		addIcon: {
			cursor: 'pointer',
			fontSize: 20,
			color: '#1789fc'
		},
		addIconContainer: {
			width: 20,
			textAlign: 'center'
		},
		moreSettings: {
			fontSize: '1rem',
			fontWeight: 'normal',
			fontFamily: 'roboto',
			marginTop: 6,
			display: 'flex',
			height: 25,
			alignItems: 'center',
			color: '#1789FC',
			cursor: 'pointer',
			textTransform: 'none'
		},
		moreSettingsLabel: {
			marginLeft: 8
		},
		addNewItemDialog: {
			'& .form-container': {
				maxHeight: '600px !important'
			}
		},
		ellipsis: {
			overflow: 'hidden',
			whiteSpace: 'nowrap',
			textOverflow: 'ellipsis'
		},
		removeColumnCrossIcon: {
			marginRight: 5,
			fontWeight: 'normal',
			fontFamily: 'Roboto',
			fontSize: 10,
			cursor: 'pointer',
			opacity: 0
		},
		discountCrossIcon: {
			float: 'right'
		}
	};
};

var LineItems = function (_Component) {
	(0, _inherits3.default)(LineItems, _Component);

	function LineItems(props) {
		(0, _classCallCheck3.default)(this, LineItems);

		var _this = (0, _possibleConstructorReturn3.default)(this, (LineItems.__proto__ || (0, _getPrototypeOf2.default)(LineItems)).call(this, props));

		_this.toggleDescriptionVisibility = function (e) {
			var changeLineItemVisibility = _this.props.changeLineItemVisibility;

			var SettingModel = require('../../../Models/SettingsModel');
			var Queries = require('../../../Constants/Queries');
			var ErrorCode = require('../../../Constants/ErrorCode');

			var settingModel = new SettingModel();
			settingModel.setSettingKey(Queries.SETTING_ENABLE_ITEM_DESCRIPTION);

			var checked = false;

			if (e && e.target) {
				checked = e.target.checked;
			}
			var statusCode = settingModel.UpdateSetting(_MyDouble2.default.convertStringToDouble(checked));
			if (statusCode === ErrorCode.ERROR_SETTING_SAVE_SUCCESS) {
				changeLineItemVisibility('description', checked);
				_this.setColumnsWidth();
			} else {
				e.preventDefault();
			}
		};

		_this.toggleDiscountColumnVisibility = function (e) {
			var changeLineItemVisibility = _this.props.changeLineItemVisibility;

			var SettingModel = require('../../../Models/SettingsModel');
			var Queries = require('../../../Constants/Queries');
			var ErrorCode = require('../../../Constants/ErrorCode');

			var settingModel = new SettingModel();
			settingModel.setSettingKey(Queries.SETTING_ITEMWISE_DISCOUNT_ENABLED);
			var checked = false;

			if (e && e.target) {
				checked = e.target.checked;
			}
			var statusCode = settingModel.UpdateSetting(_MyDouble2.default.convertStringToDouble(checked));
			if (statusCode === ErrorCode.ERROR_SETTING_SAVE_SUCCESS) {
				changeLineItemVisibility('discount', checked);
				_this.setColumnsWidth();
			} else {
				e.preventDefault();
			}
		};

		_this.openSettings = function () {
			if (canCloseDialogue()) {
				var Settings = require('../Settings/Settings').default;
				var mountComponent = require('../MountComponent').default;
				mountComponent(Settings, document.querySelector('#frameDiv'), {
					activeIndex: 5
				});
				MyAnalytics.pushEvent('Settings.html', {
					Action: 'Click'
				});
				unmountReactComponent(document.querySelector('#salePurchaseContainer'));
			}
		};

		_this.setColumnsWidth = function () {
			setTimeout(function () {
				var columns = document.querySelectorAll('.lineItemTableContainer thead td').length;
				if (columns <= 8) {
					_this.setState({ tableHeaderClass: 'itemColumnHeader' });
				} else {
					_this.setState({ tableHeaderClass: '' });
				}
			}, 300);
		};

		_this.removeDescriptionColumn = function (yes) {
			_this.setState({ showDescriptionRemoveDialog: false });
			if (yes) {
				_this.toggleDescriptionVisibility();
			}
		};

		_this.removeDiscountColumn = function (yes) {
			_this.setState({ showDiscountRemoveDialog: false });
			if (yes) {
				_this.toggleDiscountColumnVisibility();
			}
		};

		_this.onRemoveDescriptionColumn = function (e) {
			var lineItems = _this.props.lineItems;

			var found = lineItems.find(function (lineItem) {
				return lineItem.description;
			});
			if (found) {
				_this.setState({ showDescriptionRemoveDialog: true });
			} else {
				_this.toggleDescriptionVisibility(e);
			}
		};

		_this.onRemoveDiscountColumn = function (e) {
			var lineItems = _this.props.lineItems;

			var found = lineItems.find(function (lineItem) {
				return _MyDouble2.default.convertStringToDouble(lineItem.discountAmount) > 0;
			});
			if (found) {
				_this.setState({ showDiscountRemoveDialog: true });
			} else {
				_this.toggleDiscountColumnVisibility(e);
			}
		};

		var txnType = props.txnType,
		    cashSale = props.cashSale;

		var itemCategories = [];
		if (props.lineItemColumnsVisibility.additionalColumns && _MyDouble2.default.convertStringToDouble(settingCache.isItemCategoryEnabled())) {
			var itemCategoryCache = new _ItemCategoryCache2.default();
			itemCategories = itemCategoryCache.getItemCategoriesArray();
		}
		_this.state = {
			showDescriptionRemoveDialog: false,
			showDiscountRemoveDialog: false,
			invoiceDate: new Date(),
			anchorEl: null,
			openBarcodeDialog: cashSale && settingCache.getDirectBarcodeScanEnabled(),
			showColumnsMenu: false,
			tableHeaderClass: '',
			showItemCode: Boolean(_MyDouble2.default.convertStringToDouble(settingCache.showItemCodeInLineItem()) && props.lineItemColumnsVisibility.additionalColumns),
			showItemCategory: Boolean(_MyDouble2.default.convertStringToDouble(settingCache.showItemCategoryInLineItem()) && _MyDouble2.default.convertStringToDouble(settingCache.isItemCategoryEnabled()) && props.lineItemColumnsVisibility.additionalColumns),
			showPurchasePriceInDropdown: Boolean(_MyDouble2.default.convertStringToDouble(settingCache.getPurchasePriceDisplayEnabled())),
			showHSNCode: Boolean(_MyDouble2.default.convertStringToDouble(settingCache.showItemHSNSACCodeInLineItem()) && _MyDouble2.default.convertStringToDouble(settingCache.getHSNSACEnabled()) && props.lineItemColumnsVisibility.additionalColumns),
			itemCategories: itemCategories
		};

		_this.isBarcodeEnabled = settingCache.getBarcodeScanEnabled() && (txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN || txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE || txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER || txnType === _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE || txnType === _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER);

		_this.handleClick = _this.handleClick.bind(_this);
		_this.handleClose = _this.handleClose.bind(_this);
		_this.showBarcodeDialog = _this.showBarcodeDialog.bind(_this);
		_this.onBarcodeDialogClose = _this.onBarcodeDialogClose.bind(_this);
		_this.showColumnsMenu = _this.showColumnsMenu.bind(_this);
		_this.hideColumnsMenu = _this.hideColumnsMenu.bind(_this);
		_this.toggleItemCategoryColumnVisibility = _this.toggleItemCategoryColumnVisibility.bind(_this);
		_this.toggleItemCodeColumnVisibility = _this.toggleItemCodeColumnVisibility.bind(_this);
		_this.toggleItemHSNColumnVisibility = _this.toggleItemHSNColumnVisibility.bind(_this);

		_this.mrpHeader = settingCache.getAdditionalItemDetailsHeaderValue(_Queries2.default.SETTING_ITEM_MRP_VALUE);
		_this.batchNoHeader = settingCache.getAdditionalItemDetailsHeaderValue(_Queries2.default.SETTING_ITEM_BATCH_NUMBER_VALUE);
		_this.srNoHeader = settingCache.getAdditionalItemDetailsHeaderValue(_Queries2.default.SETTING_ITEM_SERIAL_NUMBER_VALUE);
		_this.mfgDateHeader = settingCache.getAdditionalItemDetailsHeaderValue(_Queries2.default.SETTING_ITEM_MANUFACTURING_DATE_VALUE);
		_this.expDateHeader = settingCache.getAdditionalItemDetailsHeaderValue(_Queries2.default.SETTING_ITEM_EXPIRY_DATE_VALUE);
		_this.sizeHeader = settingCache.getAdditionalItemDetailsHeaderValue(_Queries2.default.SETTING_ITEM_SIZE_VALUE);

		_this.countHeader = settingCache.getAdditionalItemDetailsHeaderValue(_Queries2.default.SETTING_ITEM_COUNT_VALUE);

		_this.descHeader = settingCache.getAdditionalItemDetailsHeaderValue(_Queries2.default.SETTING_ITEM_DESCRIPTION_VALUE);

		var mrpColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(_Queries2.default.SETTING_ENABLE_ITEM_MRP);
		var batchNumberColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(_Queries2.default.SETTING_ENABLE_ITEM_BATCH_NUMBER);
		var serialNumberColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(_Queries2.default.SETTING_ENABLE_SERIAL_ITEM_NUMBER);
		var mfgDateColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(_Queries2.default.SETTING_ENABLE_ITEM_MANUFACTURING_DATE);
		var expDateColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(_Queries2.default.SETTING_ENABLE_ITEM_EXPIRY_DATE);
		var sizeColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(_Queries2.default.SETTING_ENABLE_ITEM_SIZE);
		_this.showBatchDialog = false;
		if (mrpColumnVisibility || batchNumberColumnVisibility || serialNumberColumnVisibility || mfgDateColumnVisibility || expDateColumnVisibility || sizeColumnVisibility) {
			_this.showBatchDialog = true;
		}
		return _this;
	}

	(0, _createClass3.default)(LineItems, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.setColumnsWidth();
		}
	}, {
		key: 'toggleItemCodeColumnVisibility',
		value: function toggleItemCodeColumnVisibility(e) {
			var SettingModel = require('../../../Models/SettingsModel');
			var Queries = require('../../../Constants/Queries');
			var ErrorCode = require('../../../Constants/ErrorCode');

			var settingModel = new SettingModel();
			settingModel.setSettingKey(Queries.SETTING_SHOW_ITEM_CODE_IN_LINE_ITEM);

			var checked = e.target.checked;
			var statusCode = settingModel.UpdateSetting(_MyDouble2.default.convertStringToDouble(checked), {
				nonSyncableSetting: true
			});
			if (statusCode === ErrorCode.ERROR_SETTING_SAVE_SUCCESS) {
				this.setState({ showItemCode: checked });
				this.setColumnsWidth();
			} else {
				e.preventDefault();
			}
		}
	}, {
		key: 'toggleItemCategoryColumnVisibility',
		value: function toggleItemCategoryColumnVisibility(e) {
			var SettingModel = require('../../../Models/SettingsModel');
			var Queries = require('../../../Constants/Queries');
			var ErrorCode = require('../../../Constants/ErrorCode');

			var checked = e.target.checked;
			var statusCode = ErrorCode.ERROR_SETTING_SAVE_SUCCESS;
			if (checked && !_MyDouble2.default.convertStringToDouble(settingCache.isItemCategoryEnabled())) {
				var settingModel = new SettingModel();
				settingModel.setSettingKey(Queries.SETTING_ITEM_CATEGORY);
				statusCode = settingModel.UpdateSetting(_MyDouble2.default.convertStringToDouble(checked));
			}

			if (statusCode === ErrorCode.ERROR_SETTING_SAVE_SUCCESS) {
				var _settingModel = new SettingModel();
				_settingModel.setSettingKey(Queries.SETTING_SHOW_ITEM_CATEGORY_IN_LINE_ITEM);
				statusCode = _settingModel.UpdateSetting(_MyDouble2.default.convertStringToDouble(checked), {
					nonSyncableSetting: true
				});
			}

			if (statusCode === ErrorCode.ERROR_SETTING_SAVE_SUCCESS) {
				this.setState({ showItemCategory: checked });
				this.setColumnsWidth();
			} else {
				e.preventDefault();
			}
		}
	}, {
		key: 'toggleItemHSNColumnVisibility',
		value: function toggleItemHSNColumnVisibility(e) {
			var SettingModel = require('../../../Models/SettingsModel');
			var Queries = require('../../../Constants/Queries');
			var ErrorCode = require('../../../Constants/ErrorCode');

			var settingModel = new SettingModel();
			settingModel.setSettingKey(Queries.SETTING_SHOW_ITEM_HSN_SAC_CODE_IN_LINE_ITEM);

			var checked = e.target.checked;
			var statusCode = settingModel.UpdateSetting(_MyDouble2.default.convertStringToDouble(checked), {
				nonSyncableSetting: true
			});
			if (statusCode === ErrorCode.ERROR_SETTING_SAVE_SUCCESS) {
				this.setState({ showHSNCode: checked });
				this.setColumnsWidth();
			} else {
				e.preventDefault();
			}
		}
	}, {
		key: 'showColumnsMenu',
		value: function showColumnsMenu() {
			this.setState({
				showColumnsMenu: true
			});
		}
	}, {
		key: 'hideColumnsMenu',
		value: function hideColumnsMenu() {
			var showColumnsMenu = this.state.showColumnsMenu;

			if (showColumnsMenu) {
				this.setState({
					showColumnsMenu: false
				});
			}
		}
	}, {
		key: 'handleClick',
		value: function handleClick(event) {
			this.setState({ anchorEl: event.currentTarget });
		}
	}, {
		key: 'handleClose',
		value: function handleClose(inclusive) {
			var _props = this.props,
			    lineItems = _props.lineItems,
			    toggleTaxInclusive = _props.toggleTaxInclusive,
			    taxInclusive = _props.taxInclusive;

			var OK = true;
			if (inclusive == taxInclusive || inclusive === undefined) {
				this.setState({ anchorEl: null });
				return false;
			}
			if (lineItems.find(function (lineItem) {
				return lineItem.item && lineItem.item.label;
			})) {
				OK = confirm('Amount will be changed for already added items. Do you want to continue?');
			}
			if (OK) {
				inclusive !== undefined && toggleTaxInclusive && toggleTaxInclusive(inclusive);
			}
			this.setState({ anchorEl: null });
		}
	}, {
		key: 'showBarcodeDialog',
		value: function showBarcodeDialog() {
			this.setState({
				openBarcodeDialog: true
			});
		}
	}, {
		key: 'onBarcodeDialogClose',
		value: function onBarcodeDialogClose(lineItems) {
			var addLineItem = this.props.addLineItem;

			addLineItem(lineItems);
			this.setState({
				openBarcodeDialog: false
			});
		}

		// setNextLineItemFocus = (index) => {
		// 	const {addLineItem, lineItems} = this.props;
		// 	const id = `#salePurchaseItemDropdown${index + 1}`;
		// 	const currentItemId = `#salePurchaseItemDropdown${index}`;
		// 	const input = document.querySelector(id);
		// 	if (input) {
		// 		input.focus();
		// 	} else {
		// 		const lineItem = lineItems[index];
		// 		if (document.querySelector(currentItemId).value.trim()) {
		// 			addLineItem();
		// 			setTimeout(() => {
		// 				const input = document.querySelector(id);
		// 				if (input) {
		// 					input.focus();
		// 				}
		// 			});
		// 		} else {
		// 			document.querySelector('#sale-form-footer-fields-container input').focus();
		// 		}
		// 	}
		// }

	}, {
		key: 'render',
		value: function render() {
			var _this2 = this;

			var _props2 = this.props,
			    classes = _props2.classes,
			    items = _props2.items,
			    lineItems = _props2.lineItems,
			    addLineItem = _props2.addLineItem,
			    deleteLineItem = _props2.deleteLineItem,
			    quickEntryOpen = _props2.quickEntryOpen,
			    changeLineItem = _props2.changeLineItem,
			    txnType = _props2.txnType,
			    party = _props2.party,
			    placeOfSupply = _props2.placeOfSupply,
			    firmLogic = _props2.firmLogic,
			    getDefaultLineItem = _props2.getDefaultLineItem,
			    totalQty = _props2.totalQty,
			    totalFreeQty = _props2.totalFreeQty,
			    totalCount = _props2.totalCount,
			    subTotal = _props2.subTotal,
			    txnId = _props2.txnId,
			    lineItemColumnsVisibility = _props2.lineItemColumnsVisibility,
			    createItem = _props2.createItem,
			    isFTU = _props2.isFTU,
			    taxInclusive = _props2.taxInclusive;
			var _state = this.state,
			    anchorEl = _state.anchorEl,
			    openBarcodeDialog = _state.openBarcodeDialog,
			    showColumnsMenu = _state.showColumnsMenu,
			    showItemCode = _state.showItemCode,
			    showItemCategory = _state.showItemCategory,
			    showPurchasePriceInDropdown = _state.showPurchasePriceInDropdown,
			    showHSNCode = _state.showHSNCode,
			    itemCategories = _state.itemCategories,
			    tableHeaderClass = _state.tableHeaderClass,
			    showDescriptionRemoveDialog = _state.showDescriptionRemoveDialog,
			    showDiscountRemoveDialog = _state.showDiscountRemoveDialog;

			var open = Boolean(anchorEl);

			return _react2.default.createElement(
				_styles.MuiThemeProvider,
				{ theme: theme },
				_react2.default.createElement('div', { className: classes.addNewItemDialog, id: 'addNewItemDialog' }),
				openBarcodeDialog && _react2.default.createElement(_BarcodeModal2.default, {
					onClose: this.onBarcodeDialogClose,
					party: party,
					firmLogic: firmLogic,
					addLineItem: addLineItem,
					txnType: txnType,
					txnId: txnId,
					placeOfSupply: placeOfSupply,
					getDefaultLineItem: getDefaultLineItem,
					taxInclusive: taxInclusive
				}),
				showDescriptionRemoveDialog && _react2.default.createElement(_ConfirmModal2.default, {
					message: 'Are you sure you want to remove "' + this.descHeader + '" column?',
					cancelText: 'No',
					OKText: 'Yes',
					onClose: this.removeDescriptionColumn
				}),
				showDiscountRemoveDialog && _react2.default.createElement(_ConfirmModal2.default, {
					message: 'Are you sure you want to remove "Discount" column?',
					cancelText: 'No',
					OKText: 'Yes',
					onClose: this.removeDiscountColumn
				}),
				_react2.default.createElement(
					'div',
					{ className: (0, _classnames2.default)(classes.lineItemsContainer) },
					_react2.default.createElement(
						'table',
						{ className: (0, _classnames2.default)(classes.lineItemWrapper, 'lineItemTableContainer') },
						_react2.default.createElement(
							'thead',
							null,
							_react2.default.createElement(
								'tr',
								{
									className: (0, _classnames2.default)(classes.header)
								},
								_react2.default.createElement(
									'td',
									{
										className: (0, _classnames2.default)(classes.SrNo, classes.lineItemColumn)
									},
									this.isBarcodeEnabled && _react2.default.createElement(
										'div',
										{
											onClick: this.showBarcodeDialog,
											className: classes.barcodeContainer
										},
										_react2.default.createElement(_BarcodeIcon2.default, { className: classes.barcodeIcon })
									),
									!this.isBarcodeEnabled && _react2.default.createElement(
										'div',
										null,
										' # '
									)
								),
								showItemCategory && _react2.default.createElement(
									'td',
									{
										className: (0, _classnames2.default)(classes.additionalColumns, classes.lineItemColumn, classes.pl8)
									},
									_react2.default.createElement(
										'div',
										{ className: 'd-flex justify-content-between' },
										_react2.default.createElement(
											'span',
											null,
											'Category'
										),
										_react2.default.createElement(
											'span',
											{
												onClick: this.toggleItemCategoryColumnVisibility,
												className: (0, _classnames2.default)(classes.removeColumnCrossIcon, 'line-item-cross-icon-opacity') },
											'x'
										)
									)
								),
								_react2.default.createElement(
									'td',
									{
										className: (0, _classnames2.default)(classes.item, classes.lineItemColumn, classes.pl8, tableHeaderClass)
									},
									'ITEM'
								),
								showItemCode && _react2.default.createElement(
									'td',
									{
										className: (0, _classnames2.default)(classes.additionalColumns, classes.lineItemColumn, classes.pl8)
									},
									_react2.default.createElement(
										'div',
										{ className: 'd-flex justify-content-between' },
										_react2.default.createElement(
											'span',
											null,
											'ITEM CODE'
										),
										_react2.default.createElement(
											'span',
											{
												onClick: this.toggleItemCodeColumnVisibility,
												className: (0, _classnames2.default)(classes.removeColumnCrossIcon, 'line-item-cross-icon-opacity') },
											'x'
										)
									)
								),
								showHSNCode && _react2.default.createElement(
									'td',
									{
										className: (0, _classnames2.default)(classes.additionalColumns, classes.lineItemColumn)
									},
									_react2.default.createElement(
										'div',
										{ className: 'd-flex justify-content-between' },
										_react2.default.createElement(
											'span',
											null,
											'HSN Code'
										),
										_react2.default.createElement(
											'span',
											{
												onClick: this.toggleItemHSNColumnVisibility,
												className: (0, _classnames2.default)(classes.removeColumnCrossIcon, 'line-item-cross-icon-opacity') },
											'x'
										)
									)
								),
								lineItemColumnsVisibility.description && _react2.default.createElement(
									'td',
									{
										className: (0, _classnames2.default)(classes.description, classes.lineItemColumn, classes.pl8)
									},
									_react2.default.createElement(
										'div',
										{ className: 'd-flex justify-content-between' },
										_react2.default.createElement(
											'div',
											{ className: classes.ellipsis },
											this.descHeader
										),
										_react2.default.createElement(
											'span',
											{
												onClick: this.onRemoveDescriptionColumn,
												className: (0, _classnames2.default)(classes.removeColumnCrossIcon, 'line-item-cross-icon-opacity') },
											'x'
										)
									)
								),
								lineItemColumnsVisibility.count && _react2.default.createElement(
									'td',
									{
										className: (0, _classnames2.default)(classes.count, classes.lineItemColumn, classes.pl8)
									},
									_react2.default.createElement(
										'div',
										{ className: classes.ellipsis },
										this.countHeader
									)
								),
								lineItemColumnsVisibility.slNo && _react2.default.createElement(
									'td',
									{
										className: (0, _classnames2.default)(classes.slNo, classes.lineItemColumn, classes.pl8)
									},
									_react2.default.createElement(
										'div',
										{ className: classes.ellipsis },
										this.srNoHeader
									)
								),
								lineItemColumnsVisibility.batchNo && _react2.default.createElement(
									'td',
									{
										className: (0, _classnames2.default)(classes.batchNo, classes.lineItemColumn, classes.pl8)
									},
									_react2.default.createElement(
										'div',
										{ className: classes.ellipsis },
										this.batchNoHeader
									)
								),
								lineItemColumnsVisibility.expDate && _react2.default.createElement(
									'td',
									{
										className: (0, _classnames2.default)(classes.expDate, classes.lineItemColumn, classes.pl8)
									},
									_react2.default.createElement(
										'div',
										{ className: classes.ellipsis },
										this.expDateHeader
									)
								),
								lineItemColumnsVisibility.mfgDate && _react2.default.createElement(
									'td',
									{
										className: (0, _classnames2.default)(classes.mfgDate, classes.lineItemColumn, classes.pl8)
									},
									_react2.default.createElement(
										'div',
										{ className: classes.ellipsis },
										this.mfgDateHeader
									)
								),
								lineItemColumnsVisibility.mrp && _react2.default.createElement(
									'td',
									{
										className: (0, _classnames2.default)(classes.mrp, classes.lineItemColumn, classes.pl8)
									},
									_react2.default.createElement(
										'div',
										{ className: classes.ellipsis },
										this.mrpHeader
									)
								),
								lineItemColumnsVisibility.size && _react2.default.createElement(
									'td',
									{
										className: (0, _classnames2.default)(classes.size, classes.lineItemColumn, classes.pl8)
									},
									_react2.default.createElement(
										'div',
										{ className: classes.ellipsis },
										this.sizeHeader
									)
								),
								_react2.default.createElement(
									'td',
									{
										className: (0, _classnames2.default)(classes.qty, classes.lineItemColumn, classes.pl8)
									},
									'Qty'
								),
								lineItemColumnsVisibility.freeQty && _react2.default.createElement(
									'td',
									{
										className: (0, _classnames2.default)(classes.freeQty, classes.lineItemColumn, classes.pl8)
									},
									'Free',
									_react2.default.createElement('br', null),
									'Qty'
								),
								lineItemColumnsVisibility.unit && _react2.default.createElement(
									'td',
									{
										className: (0, _classnames2.default)(classes.unit, classes.lineItemColumn, classes.pl8)
									},
									'Unit'
								),
								_react2.default.createElement(
									'td',
									{
										className: (0, _classnames2.default)(classes.priceUnit, classes.lineItemColumn, classes.pl8, classes.positionRelative)
									},
									'Price/Unit',
									lineItemColumnsVisibility.taxInclusiveDropdown && _react2.default.createElement(
										'div',
										{ className: classes.taxInclusiveWrapper },
										_react2.default.createElement(
											'div',
											{
												className: (0, _classnames2.default)(classes.taxInclusive, 'd-flex'),
												onClick: this.handleClick
											},
											taxInclusive === ItemType.ITEM_TXN_TAX_INCLUSIVE && _react2.default.createElement(
												'span',
												null,
												'Tax Included'
											),
											taxInclusive === ItemType.ITEM_TXN_TAX_EXCLUSIVE && _react2.default.createElement(
												'span',
												null,
												'Tax Excluded'
											),
											_react2.default.createElement(_ExpandMore2.default, { className: classes.expandMoreIcon })
										),
										_react2.default.createElement(
											_Menu2.default,
											{
												id: 'tax-inclusive-dropdown',
												anchorEl: anchorEl,
												open: open,
												onClose: function onClose() {
													return _this2.handleClose();
												}
											},
											_react2.default.createElement(
												_MenuItem2.default,
												{
													onClick: function onClick() {
														return _this2.handleClose(ItemType.ITEM_TXN_TAX_INCLUSIVE);
													}
												},
												'Tax Included'
											),
											_react2.default.createElement(
												_MenuItem2.default,
												{
													onClick: function onClick() {
														return _this2.handleClose(ItemType.ITEM_TXN_TAX_EXCLUSIVE);
													}
												},
												'Tax Excluded'
											)
										)
									)
								),
								lineItemColumnsVisibility.discount && _react2.default.createElement(
									'td',
									{
										className: (0, _classnames2.default)(classes.disc, classes.lineItemColumn, 'flex-column')
									},
									_react2.default.createElement(
										'div',
										{ className: classes.columnHeader },
										'Discount',
										_react2.default.createElement(
											'span',
											{
												onClick: this.onRemoveDiscountColumn,
												className: (0, _classnames2.default)(classes.removeColumnCrossIcon, classes.discountCrossIcon, 'line-item-cross-icon-opacity') },
											'x'
										)
									),
									_react2.default.createElement(
										'div',
										{ className: (0, _classnames2.default)(classes.subHeader, 'd-flex') },
										_react2.default.createElement(
											'div',
											{
												className: (0, _classnames2.default)(classes.left, classes.discountPercent, 'd-flex justify-content-center align-items-center')
											},
											'%'
										),
										_react2.default.createElement(
											'div',
											{
												className: (0, _classnames2.default)(classes.right, classes.discountAmount, 'd-flex justify-content-center align-items-center')
											},
											'AMOUNT'
										)
									)
								),
								lineItemColumnsVisibility.tax && _react2.default.createElement(
									'td',
									{
										className: (0, _classnames2.default)(classes.tax, classes.lineItemColumn, 'flex-column')
									},
									_react2.default.createElement(
										'div',
										{ className: classes.columnHeader },
										'Tax'
									),
									_react2.default.createElement(
										'div',
										{ className: (0, _classnames2.default)(classes.subHeader, 'd-flex') },
										_react2.default.createElement(
											'div',
											{
												className: (0, _classnames2.default)(classes.left, classes.taxPercent, 'd-flex justify-content-center align-items-center')
											},
											'%'
										),
										_react2.default.createElement(
											'div',
											{
												className: (0, _classnames2.default)(classes.right, classes.taxAmount, 'd-flex justify-content-center align-items-center')
											},
											'AMOUNT'
										)
									)
								),
								lineItemColumnsVisibility.cess && _react2.default.createElement(
									'td',
									{
										className: (0, _classnames2.default)(classes.cess, classes.lineItemColumn, classes.pl8)
									},
									'Add',
									_react2.default.createElement('br', null),
									'Cess'
								),
								_react2.default.createElement(
									'td',
									{
										className: (0, _classnames2.default)(classes.amount, classes.lineItemColumn, classes.positionRelative, classes.pl8)
									},
									_react2.default.createElement(
										'div',
										{ className: 'd-flex align-items-center justify-content-between' },
										_react2.default.createElement(
											'div',
											null,
											'Amount'
										),
										_react2.default.createElement(
											'div',
											{ className: classes.addIconContainer },
											lineItemColumnsVisibility.additionalColumns && _react2.default.createElement(
												_ClickAwayListener2.default,
												{ onClickAway: this.hideColumnsMenu },
												_react2.default.createElement(
													'div',
													{ className: classes.columnsMenuRoot },
													_react2.default.createElement(
														_AddCircleOutline2.default,
														{
															onClick: this.showColumnsMenu,
															className: classes.addIcon,
															color: 'secondary'
														},
														'add'
													),
													showColumnsMenu ? _react2.default.createElement(
														_Paper2.default,
														{ className: classes.columnsMenuContainer },
														_react2.default.createElement(
															'div',
															null,
															_react2.default.createElement(_FormControlLabel2.default, {
																className: classes.columnsMenuLabel,
																control: _react2.default.createElement(_Checkbox2.default, {
																	checked: settingCache.showItemCategoryInLineItem() == '1' && showItemCategory,
																	onChange: this.toggleItemCategoryColumnVisibility,
																	color: 'primary'
																}),
																label: 'Item Category'
															})
														),
														_react2.default.createElement(
															'div',
															null,
															_react2.default.createElement(_FormControlLabel2.default, {
																className: classes.columnsMenuLabel,
																control: _react2.default.createElement(_Checkbox2.default, {
																	checked: settingCache.showItemCodeInLineItem() == '1',
																	onChange: this.toggleItemCodeColumnVisibility,
																	color: 'primary'
																}),
																label: 'Item Code'
															})
														),
														settingCache.getHSNSACEnabled() == 1 && _react2.default.createElement(
															'div',
															null,
															_react2.default.createElement(_FormControlLabel2.default, {
																className: classes.columnsMenuLabel,
																control: _react2.default.createElement(_Checkbox2.default, {
																	checked: settingCache.showItemHSNSACCodeInLineItem() == '1',
																	onChange: this.toggleItemHSNColumnVisibility,
																	color: 'primary'
																}),
																label: 'HSN/SAC Code'
															})
														),
														_react2.default.createElement(
															'div',
															null,
															_react2.default.createElement(_FormControlLabel2.default, {
																className: classes.columnsMenuLabel,
																control: _react2.default.createElement(_Checkbox2.default, {
																	checked: settingCache.isAdditionalItemDetailsEnabled(_Queries2.default.SETTING_ENABLE_ITEM_DESCRIPTION),
																	onChange: this.onRemoveDescriptionColumn,
																	color: 'primary'
																}),
																label: this.descHeader
															})
														),
														!this.props.isEdit && _react2.default.createElement(
															'div',
															null,
															_react2.default.createElement(_FormControlLabel2.default, {
																className: classes.columnsMenuLabel,
																control: _react2.default.createElement(_Checkbox2.default, {
																	checked: lineItemColumnsVisibility.discount,
																	onChange: this.onRemoveDiscountColumn,
																	color: 'primary'
																}),
																label: 'Discount'
															})
														),
														_react2.default.createElement(
															'div',
															{
																onClick: this.openSettings,
																className: classes.moreSettings
															},
															_react2.default.createElement(
																'div',
																null,
																_react2.default.createElement(_SettingsOutlined2.default, null)
															),
															_react2.default.createElement(
																'div',
																{ className: classes.moreSettingsLabel },
																'More Settings'
															)
														)
													) : null
												)
											)
										)
									)
								)
							)
						),
						_react2.default.createElement(
							'tbody',
							null,
							quickEntryOpen && _react2.default.createElement(_LineItem2.default, {
								lineItemColumnsVisibility: lineItemColumnsVisibility,
								index: -1,
								isQuickEntry: true,
								party: party,
								addLineItem: addLineItem,
								txnType: txnType,
								txnId: txnId,
								items: items,
								createItem: createItem,
								firmLogic: firmLogic,
								placeOfSupply: placeOfSupply,
								getDefaultLineItem: getDefaultLineItem,
								showItemCode: showItemCode,
								showPurchasePriceInDropdown: showPurchasePriceInDropdown,
								showItemCategory: showItemCategory,
								showItemHSNCode: showHSNCode,
								itemCategories: itemCategories,
								isFTU: isFTU,
								isBarcodeEnabled: this.isBarcodeEnabled,
								showBatchDialog: this.showBatchDialog,
								taxInclusive: taxInclusive
							}),
							lineItems.map(function (lineItem, index) {
								return _react2.default.createElement(_LineItem2.default, {
									key: lineItem.uniqueId,
									lineItemColumnsVisibility: lineItemColumnsVisibility,
									party: party,
									createItem: createItem,
									firmLogic: firmLogic,
									index: index,
									isQuickEntry: false,
									addLineItem: addLineItem,
									lineItem: lineItem,
									changeLineItem: changeLineItem,
									deleteLineItem: deleteLineItem,
									txnType: txnType,
									txnId: txnId,
									placeOfSupply: placeOfSupply,
									items: items,
									getDefaultLineItem: getDefaultLineItem,
									showItemCode: showItemCode,
									showPurchasePriceInDropdown: showPurchasePriceInDropdown,
									showItemCategory: showItemCategory,
									showItemHSNCode: showHSNCode,
									itemCategories: itemCategories,
									isFTU: isFTU,
									isBarcodeEnabled: _this2.isBarcodeEnabled,
									showBatchDialog: _this2.showBatchDialog,
									taxInclusive: taxInclusive
								});
							})
						),
						_react2.default.createElement(
							'tfoot',
							null,
							_react2.default.createElement(
								'tr',
								{ className: classes.footer },
								_react2.default.createElement('td', null),
								showItemCategory && _react2.default.createElement('td', null),
								_react2.default.createElement(
									'td',
									null,
									_react2.default.createElement(
										'div',
										{ className: 'd-flex justify-content-between align-items-center mr-10' },
										_react2.default.createElement(
											'div',
											null,
											_react2.default.createElement(
												_Button2.default,
												{
													onClick: function onClick() {
														return addLineItem();
													},
													variant: 'outlined',
													id: 'btn-sale-form-add-row',
													size: 'small',
													color: 'primary'
												},
												'Add Row'
											)
										),
										_react2.default.createElement(
											'div',
											{ className: classes.lineItemFooterColumn },
											'Total'
										)
									)
								),
								showItemCode && _react2.default.createElement('td', null),
								showHSNCode && _react2.default.createElement('td', null),
								lineItemColumnsVisibility.description && _react2.default.createElement('td', null),
								lineItemColumnsVisibility.count && _react2.default.createElement(
									'td',
									{
										className: (0, _classnames2.default)(classes.lineItemFooterColumn, classes.lineItemFooterColumnText)
									},
									_MyDouble2.default.getQuantityWithDecimalWithoutColor(totalCount)
								),
								lineItemColumnsVisibility.slNo && _react2.default.createElement('td', null),
								lineItemColumnsVisibility.batchNo && _react2.default.createElement('td', null),
								lineItemColumnsVisibility.expDate && _react2.default.createElement('td', null),
								lineItemColumnsVisibility.mfgDate && _react2.default.createElement('td', null),
								lineItemColumnsVisibility.mrp && _react2.default.createElement('td', null),
								lineItemColumnsVisibility.size && _react2.default.createElement('td', null),
								_react2.default.createElement(
									'td',
									{
										className: (0, _classnames2.default)(classes.lineItemFooterColumnText, classes.lineItemFooterColumn)
									},
									_MyDouble2.default.getQuantityWithDecimalWithoutColor(totalQty)
								),
								lineItemColumnsVisibility.freeQty && _react2.default.createElement(
									'td',
									{
										className: (0, _classnames2.default)(classes.lineItemFooterColumnText, classes.lineItemFooterColumn)
									},
									totalFreeQty > 0 ? _MyDouble2.default.getQuantityWithDecimalWithoutColor(totalFreeQty) : '0'
								),
								lineItemColumnsVisibility.unit && _react2.default.createElement('td', null),
								_react2.default.createElement('td', null),
								lineItemColumnsVisibility.discount && _react2.default.createElement('td', null),
								lineItemColumnsVisibility.tax && _react2.default.createElement('td', null),
								lineItemColumnsVisibility.cess && _react2.default.createElement('td', null),
								_react2.default.createElement(
									'td',
									{
										className: (0, _classnames2.default)(classes.lineItemFooterColumnText, classes.lineItemFooterColumn)
									},
									_MyDouble2.default.getAmountWithDecimal(subTotal)
								)
							)
						)
					)
				)
			);
		}
	}]);
	return LineItems;
}(_react.Component);

LineItems.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	lineItemColumnsVisibility: _propTypes2.default.object,
	addLineItem: _propTypes2.default.func,
	items: _propTypes2.default.array,
	lineItems: _propTypes2.default.array,
	deleteLineItem: _propTypes2.default.func,
	quickEntryOpen: _propTypes2.default.bool,
	cashSale: _propTypes2.default.bool,
	changeLineItem: _propTypes2.default.func,
	txnType: _propTypes2.default.number,
	party: _propTypes2.default.object,
	placeOfSupply: _propTypes2.default.string,
	firmLogic: _propTypes2.default.object,
	getDefaultLineItem: _propTypes2.default.func,
	totalQty: _propTypes2.default.number,
	totalFreeQty: _propTypes2.default.number,
	totalCount: _propTypes2.default.number,
	subTotal: _propTypes2.default.number,
	txnId: _propTypes2.default.number,
	isFTU: _propTypes2.default.bool,
	isEdit: _propTypes2.default.bool,
	taxInclusive: _propTypes2.default.number,
	createItem: _propTypes2.default.func,
	toggleTaxInclusive: _propTypes2.default.func,
	changeLineItemVisibility: _propTypes2.default.func
};

exports.default = (0, _styles.withStyles)(styles)(LineItems);