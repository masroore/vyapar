Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/core/styles');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _themes = require('../../../themes');

var _themes2 = _interopRequireDefault(_themes);

var _DateRange = require('@material-ui/icons/DateRange');

var _DateRange2 = _interopRequireDefault(_DateRange);

var _OutlineInput = require('../UIControls/OutlineInput');

var _OutlineInput2 = _interopRequireDefault(_OutlineInput);

var _PartyDropdown = require('../UIControls/PartyDropdown');

var _PartyDropdown2 = _interopRequireDefault(_PartyDropdown);

var _JqueryDatePicker = require('../UIControls/JqueryDatePicker');

var _JqueryDatePicker2 = _interopRequireDefault(_JqueryDatePicker);

var _TxnTypeConstant = require('../../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _UDFFieldsConstant = require('../../../Constants/UDFFieldsConstant');

var _UDFFieldsConstant2 = _interopRequireDefault(_UDFFieldsConstant);

var _MyDouble = require('../../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _MyDate = require('../../../Utilities/MyDate');

var _MyDate2 = _interopRequireDefault(_MyDate);

var _SalePurchaseHelpers = require('./SalePurchaseHelpers');

var _BillingName = require('./BillingName');

var _BillingName2 = _interopRequireDefault(_BillingName);

var _Label = require('./Label');

var _Label2 = _interopRequireDefault(_Label);

var _StringConstants = require('../../../Constants/StringConstants');

var _StringConstants2 = _interopRequireDefault(_StringConstants);

var _DecimalInputFilter = require('../../../Utilities/DecimalInputFilter');

var DecimalInputFilter = _interopRequireWildcard(_DecimalInputFilter);

var _throttleDebounce = require('throttle-debounce');

var _DateFormat = require('../../../Constants/DateFormat');

var _DateFormat2 = _interopRequireDefault(_DateFormat);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var theme = (0, _themes2.default)();

var styles = function styles() {
  return {
    transactionHeaderFields: {
      minHeight: 180,
      marginTop: 40,
      marginLeft: 35,
      marginRight: 35
    },
    invoiceDropdown: {
      background: 'rgba(23, 137, 252, 0.16)',
      border: 0,
      color: '#223A52',
      fontSize: 10,
      borderRadius: 2,
      width: 60
    },
    invoiceNumber: {
      borderBottom: '1px solid #ddd !important',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      width: 115
    },
    transparentDropdown: {
      background: 'transparent',
      border: 0,
      color: '#223A52',
      fontSize: 13
    },
    placeOfSupply: {
      width: 155
    },
    textField: {
      fontSize: 13
    },
    textArea: {
      height: 'auto',
      background: 'transparent',
      '& label': {
        height: 20
      }
    },
    height20: {
      height: 20
    },
    transparentDatepicker: {
      background: 'transparent',
      border: 0
    },
    dueDateContainer: {
      position: 'relative',
      maxHeight: 34
    },
    overdueContainer: {
      fontSize: 12,
      fontFamily: 'robotoMedium',
      letterSpacing: 0.5
    },
    overdueDays: {
      color: '#e85454'
    },
    mandatory: {
      color: '#e85454'
    },
    dateIcon: {
      position: 'absolute',
      cursor: 'pointer',
      top: 0,
      left: 75,
      fill: '#aaa',
      fontSize: 18
    },
    PODateIcon: {
      cursor: 'pointer',
      position: 'absolute',
      color: '#aaa',
      right: 5,
      top: 7
    },
    ml15: {
      marginLeft: 15
    },
    ml50: {
      marginLeft: 50
    },
    mr20: {
      marginRight: 20
    },
    mb15: {
      marginBottom: 15
    },
    mb18: {
      marginBottom: 15
    },
    mb30: {
      marginBottom: 30
    },
    mt30: {
      marginTop: 30
    },
    textFieldWidth150: {
      width: 150
    },
    width180: {
      width: 180
    },
    width150: {
      width: 150
    },
    textFieldWidth230: {
      width: 230
    },
    width75: {
      width: 75
    },
    width120: {
      width: 120
    },
    width300: {
      width: 300
    },
    width350: {
      width: 350
    },
    width480: {
      width: 480
    },
    maxHeight200: {
      maxHeight: 200
    },
    inlineLabel: {
      fontSize: 12,
      fontFamily: 'Roboto',
      color: '#222A3F8A'
    },
    expenseIncomeLabel: {
      textAlign: 'right',
      paddingRight: 10
    },
    inlineTextField: {
      color: '#222A3F',
      fontSize: 13,
      background: 'transparent',
      border: '1px solid transparent',
      padding: 2,
      '&:focus': {
        border: '1px solid #1789fc !important'
      }
    },
    dropdownInput: {
      padding: '0 5px',
      fontSize: 13,
      paddingTop: 2
    },
    datePickerWrapper: {
      position: 'relative'
    },
    maxWidth490: {
      maxWidth: 490
    },
    maxWidth320: {
      maxWidth: 320
    },
    maxWidth150: {
      maxWidth: 150
    }
  };
};

var SalePurchaseHeaderFields = function (_Component) {
  (0, _inherits3.default)(SalePurchaseHeaderFields, _Component);

  function SalePurchaseHeaderFields(props) {
    (0, _classCallCheck3.default)(this, SalePurchaseHeaderFields);

    var _this = (0, _possibleConstructorReturn3.default)(this, (SalePurchaseHeaderFields.__proto__ || (0, _getPrototypeOf2.default)(SalePurchaseHeaderFields)).call(this, props));

    _this.changeReturnNo = function (e) {
      var _this$props = _this.props,
          txnType = _this$props.txnType,
          changeFields = _this$props.changeFields;

      var returnNo = e.target.value;
      if (txnType == _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN) {
        returnNo = DecimalInputFilter.inputTextFilterForInvoice(e.target);
      }
      changeFields({ returnNo: returnNo });
    };

    _this.updatePartyFunction = function (fn) {
      _this.updateParty = fn;
    };

    _this.mountNewExpenseModel = function () {
      var _this$state = _this.state,
          partyName = _this$state.partyName,
          newExpenseModal = _this$state.newExpenseModal;

      var AddNewExpenseModal = require('../NewExpenseModal').default;
      return _react2.default.createElement(AddNewExpenseModal, {
        partyName: partyName,
        isOpen: newExpenseModal,
        onClose: _this.closeNewExpenseModal
      });
    };

    _this.mountNewIncomeModel = function () {
      var _this$state2 = _this.state,
          partyName = _this$state2.partyName,
          newIncomeModal = _this$state2.newIncomeModal;

      var AddNewIncomeModal = require('../NewIncomeModal').default;
      return _react2.default.createElement(AddNewIncomeModal, {
        partyName: partyName,
        isOpen: newIncomeModal,
        onClose: _this.closeNewIncomeModal
      });
    };

    _this.showPartiesDropdown = function () {
      var displayName = _this.props.displayName;

      _this.setState({ showPartiesDropdown: true }, function () {
        var partyTextBox = document.querySelector('#salePurchasePartyDropdown');
        if (displayName) {
          _this.updateParty(displayName);
        }
        if (partyTextBox) {
          partyTextBox.blur();
          setTimeout(function () {
            partyTextBox.focus();
          });
        }
      });
    };

    _this.setDatePickerFocus = function (e) {
      var target = e.target.closest('div').querySelector('input');
      target.click();
      target.focus();
    };

    _this.openInvoiceDate = function (e) {
      var target = e.target;
      if (e.keyCode === 13) {
        setTimeout(function () {
          target.click();
          target.focus();
        }, 100);
      }
    };

    _this.changeInvoiceNumber = function (key) {
      var _this$props2 = _this.props,
          txnType = _this$props2.txnType,
          changeFields = _this$props2.changeFields;

      return function (e) {
        var val = '';
        if (txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN || txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN) {
          val = e.target.value;
        } else {
          val = DecimalInputFilter.inputTextFilterForInvoice(e.target);
        }
        changeFields((0, _defineProperty3.default)({}, key, val));
      };
    };

    _this.state = {
      invoiceDate: new Date(),
      newExpenseModal: false,
      newIncomeModal: false,
      partyName: '',
      showPartiesDropdown: false
    };
    _this.renderForFirstAutofocus = false;
    _this.createNewParty = _this.createNewParty.bind(_this);
    _this.closeNewExpenseModal = _this.closeNewExpenseModal.bind(_this);
    _this.closeNewIncomeModal = _this.closeNewIncomeModal.bind(_this);
    _this.openNewExpenseModal = _this.openNewExpenseModal.bind(_this);
    _this.openNewIncomeModal = _this.openNewIncomeModal.bind(_this);
    _this.changeInvoiceDate = _this.changeInvoiceDate.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(SalePurchaseHeaderFields, [{
    key: 'changeInvoiceDate',
    value: function changeInvoiceDate(invoiceDate) {
      var _props = this.props,
          changeFields = _props.changeFields,
          txnType = _props.txnType,
          paymentTerms = _props.paymentTerms,
          fieldsVisibility = _props.fieldsVisibility;

      var dueDays = 0;
      var state = {
        invoiceDate: invoiceDate
      };
      if (txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE) {
        try {
          var dueDate = (0, _SalePurchaseHelpers.getDueDate)(txnType, paymentTerms, invoiceDate);
          if (dueDate && dueDate instanceof Date && dueDate != 'Invalid Date') {
            dueDays = _MyDouble2.default.getDueDays(dueDate);
            state.dueDate = _MyDate2.default.getDate('d/m/y', dueDate);
            state.dueDays = dueDays;
          }
        } catch (e) {}
      }
      changeFields(state);
    }
  }, {
    key: 'createNewParty',
    value: function createNewParty(partyName) {
      var _this2 = this;

      var _props2 = this.props,
          txnType = _props2.txnType,
          createParty = _props2.createParty,
          changeParty = _props2.changeParty,
          items = _props2.items;

      var $addNewName = $('#addNewNameDialog');
      var closeDialog = function closeDialog(evt) {
        if (evt.keyCode === 27) {
          try {
            $addNewName.dialog('close');
            $addNewName.html('');
          } catch (err) {}
          evt.stopPropagation();
        }
      };
      var saveName = function saveName(e, ui) {
        var name = $('#contactName').val().trim();
        if (!name) {
          document.querySelector('#salePurchasePartyDropdown').focus();
          return false;
        }
        var found = items.find(function (item) {
          return item.label.trim() == name;
        });
        if (found) {
          document.querySelector('#salePurchasePartyDropdown').focus();
          return false;
        }
        var NameCache = require('../../../Cache/NameCache');
        var NameType = require('../../../Constants/NameType');
        var nameCache = new NameCache();
        var nameLogic = nameCache.getNameObjectByName(name, NameType.NAME_TYPE_PARTY, txnType);
        if (nameLogic) {
          var item = {
            value: _MyDouble2.default.convertStringToDouble(nameLogic.getNameId()),
            label: nameLogic.getFullName(),
            balance: _MyDouble2.default.getAmountWithDecimal(nameLogic.getAmount()),
            partyLogic: nameLogic
          };
          createParty(item);
          changeParty(item);
          _this2.updateParty && _this2.updateParty(nameLogic.getFullName());
          document.querySelector('#salePurchasePartyDropdown').focus();
        }
      };

      $addNewName.dialog({
        title: 'Add New Name',
        width: '800',
        height: '600',
        closeOnEscape: false,
        close: saveName
      });
      $addNewName.on('keydown', closeDialog);

      window.userNameGlobal = '';

      $addNewName.load('NewContact.html', function () {
        $addNewName.dialog('option', 'position', {
          my: 'center',
          at: 'center',
          of: window
        });
        $('#contactName').val(partyName);
        $(this).find('.transactionStyle .form').css('width', '100%');
        $(this).find('.transactionStyle .form').css('height', '100%');
        $(this).find('.transactionStyle .heading').html('');
        $(this).css('max-height', '650px');
        $('#submitNew', $addNewName).css('display', 'none');
        $('[aria-describedby="addNewNameDialog"]').css('z-index', 9999);
      });
    }
  }, {
    key: 'closeNewExpenseModal',
    value: function closeNewExpenseModal(nameLogic) {
      var _props3 = this.props,
          createParty = _props3.createParty,
          changeParty = _props3.changeParty;

      if (nameLogic) {
        var item = {
          value: _MyDouble2.default.convertStringToDouble(nameLogic.getNameId()),
          label: nameLogic.getFullName(),
          balance: _MyDouble2.default.getAmountWithDecimal(nameLogic.getOpeningBalance()),
          partyLogic: nameLogic
        };
        createParty(item);
        changeParty(item);
        this.updateParty && this.updateParty(nameLogic.getFullName());
        setTimeout(function () {
          document.querySelector('#salePurchasePartyDropdown').focus();
        }, 100);
      }
      this.setState({
        newExpenseModal: false,
        partyName: ''
      });
    }
  }, {
    key: 'closeNewIncomeModal',
    value: function closeNewIncomeModal(nameLogic) {
      var _props4 = this.props,
          createParty = _props4.createParty,
          changeParty = _props4.changeParty;

      if (nameLogic) {
        var item = {
          value: _MyDouble2.default.convertStringToDouble(nameLogic.getNameId()),
          label: nameLogic.getFullName(),
          balance: _MyDouble2.default.getAmountWithDecimal(nameLogic.getOpeningBalance()),
          partyLogic: nameLogic
        };
        createParty(item);
        changeParty(item);
        this.updateParty && this.updateParty(nameLogic.getFullName());
        setTimeout(function () {
          document.querySelector('#salePurchasePartyDropdown').focus();
        }, 100);
      }
      this.setState({
        newIncomeModal: false,
        partyName: ''
      });
    }
  }, {
    key: 'openNewExpenseModal',
    value: function openNewExpenseModal(partyName) {
      this.setState({
        newExpenseModal: true,
        partyName: partyName
      });
    }
  }, {
    key: 'openNewIncomeModal',
    value: function openNewIncomeModal(partyName) {
      this.setState({
        newIncomeModal: true,
        partyName: partyName
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      var _state = this.state,
          partyName = _state.partyName,
          newIncomeModal = _state.newIncomeModal,
          newExpenseModal = _state.newExpenseModal,
          showPartiesDropdown = _state.showPartiesDropdown;
      var _props5 = this.props,
          classes = _props5.classes,
          items = _props5.items,
          changeParty = _props5.changeParty,
          changeField = _props5.changeField,
          changePrefix = _props5.changePrefix,
          changeDueDate = _props5.changeDueDate,
          changePaymentTerms = _props5.changePaymentTerms,
          changeExtraField = _props5.changeExtraField,
          cashSale = _props5.cashSale,
          listOfStates = _props5.listOfStates,
          invoicePrefixOptions = _props5.invoicePrefixOptions,
          paymentTermsOptions = _props5.paymentTermsOptions,
          txnType = _props5.txnType,
          party = _props5.party,
          displayName = _props5.displayName,
          billingAddress = _props5.billingAddress,
          shippingAddress = _props5.shippingAddress,
          PONo = _props5.PONo,
          PODate = _props5.PODate,
          eWayBillNo = _props5.eWayBillNo,
          extraFields = _props5.extraFields,
          returnNo = _props5.returnNo,
          txnReturnRefNumber = _props5.txnReturnRefNumber,
          txnReturnRefDate = _props5.txnReturnRefDate,
          invoicePrefix = _props5.invoicePrefix,
          invoiceNo = _props5.invoiceNo,
          invoiceDate = _props5.invoiceDate,
          paymentTerms = _props5.paymentTerms,
          dueDate = _props5.dueDate,
          dueDays = _props5.dueDays,
          placeOfSupply = _props5.placeOfSupply,
          changePlaceOfSupply = _props5.changePlaceOfSupply,
          fieldsVisibility = _props5.fieldsVisibility,
          labels = _props5.labels,
          isFTU = _props5.isFTU,
          isEdit = _props5.isEdit;


      var createNewItem = this.createNewParty;

      if (txnType === _TxnTypeConstant2.default.TXN_TYPE_EXPENSE) {
        createNewItem = this.openNewExpenseModal;
      } else if (txnType === _TxnTypeConstant2.default.TXN_TYPE_OTHER_INCOME) {
        createNewItem = this.openNewIncomeModal;
      }
      var partyLabel = '';
      if (txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE) {
        if (cashSale) {
          partyLabel = 'Customer(optional)';
        } else {
          partyLabel = 'Customer';
        }
      } else {
        partyLabel = labels.name;
      }

      var partyDropdown = _react2.default.createElement(_PartyDropdown2.default, {
        items: items,
        label: cashSale ? partyLabel : _react2.default.createElement(
          'div',
          null,
          partyLabel,
          ' ',
          _react2.default.createElement(
            'span',
            { className: classes.mandatory },
            '*'
          )
        ),
        addPartyLabel: labels.addPartyLabel,
        onChange: (0, _throttleDebounce.debounce)(100, changeParty),
        className: (0, _classnames2.default)(classes.textFieldWidth230, classes.mr20),
        inputComponent: _OutlineInput2.default,
        value: party,
        updateParty: this.updatePartyFunction,
        showFirstTimeAllOptions: true,
        renderDropdown: !isFTU,
        renderForFirstAutofocus: this.renderForFirstAutofocus,
        isConvert: this.props.isConvert,
        showPartiesClicked: showPartiesDropdown,
        txnType: txnType,
        isEdit: isEdit,
        showBalance: Boolean(party && party.value && fieldsVisibility.partyBalance),
        createNewItem: createNewItem,
        isDisabled: fieldsVisibility.nameDisabled === false
      });

      this.renderForFirstAutofocus = true;

      var extrafieldColumns = extraFields.length;

      if (fieldsVisibility.PODetails) {
        extrafieldColumns += 2;
      }
      if (fieldsVisibility.eWayBillNo) {
        extrafieldColumns += 1;
      }
      extrafieldColumns = Math.ceil(extrafieldColumns / 3);

      return _react2.default.createElement(
        _styles.MuiThemeProvider,
        { theme: theme },
        newExpenseModal && this.mountNewExpenseModel(),
        newIncomeModal && this.mountNewIncomeModel(),
        _react2.default.createElement(
          'div',
          {
            className: (0, _classnames2.default)(classes.transactionHeaderFields, 'd-flex', 'justify-content-between')
          },
          _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement(
              'div',
              { className: (0, _classnames2.default)('d-flex', classes.mb15) },
              cashSale && _react2.default.createElement(
                _react.Fragment,
                null,
                showPartiesDropdown && _react2.default.createElement(
                  'span',
                  null,
                  partyDropdown
                ),
                _react2.default.createElement(_BillingName2.default, (0, _extends3.default)({
                  label: 'Billing Name(Optional)',
                  onChange: changeField('displayName'),
                  className: (0, _classnames2.default)(classes.textFieldWidth230),
                  value: displayName,
                  autoFocus: !showPartiesDropdown
                }, !showPartiesDropdown ? { showPartiesDropdown: this.showPartiesDropdown } : {}, {
                  items: items
                }))
              ),
              !cashSale && _react2.default.createElement(
                _react.Fragment,
                null,
                partyDropdown,
                (fieldsVisibility.displayName || displayName != '') && _react2.default.createElement(_OutlineInput2.default, {
                  label: 'Billing Name(Optional)',
                  className: (0, _classnames2.default)(classes.textFieldWidth230),
                  onChange: changeField('displayName'),
                  value: displayName
                })
              )
            ),
            party && isFTU === false && party.label && party.label != _StringConstants2.default.cash && party.label != _StringConstants2.default.cashSale && _react2.default.createElement(
              'div',
              { className: classes.mt30 },
              fieldsVisibility.billingAddress && _react2.default.createElement(_OutlineInput2.default, {
                label: 'Billing Address',
                value: billingAddress,
                multiline: true,
                rows: 4,
                rowsMax: 4,
                tabIndex: -1,
                className: classes.textArea,
                InputProps: {
                  className: classes.textFieldWidth230,
                  classes: {
                    input: classes.textField,
                    notchedOutline: classes.textArea
                  }
                },
                onChange: changeField('billingAddress')
              }),
              fieldsVisibility.shippingAddress && _react2.default.createElement(_OutlineInput2.default, {
                label: 'Shipping Address',
                value: shippingAddress,
                multiline: true,
                rows: 4,
                rowsMax: 4,
                className: (0, _classnames2.default)(classes.ml15, classes.textArea),
                InputProps: {
                  className: classes.textFieldWidth230,
                  classes: {
                    input: classes.textField,
                    notchedOutline: classes.textArea
                  },
                  tabIndex: -1
                },
                onChange: changeField('shippingAddress')
              })
            )
          ),
          _react2.default.createElement(
            'div',
            {
              className: (0, _classnames2.default)('d-flex flex-column flex-wrap flex-grow-1', classes.maxHeight200, extrafieldColumns === 3 ? classes.maxWidth490 : '', extrafieldColumns === 2 ? classes.maxWidth320 : '', extrafieldColumns === 1 ? classes.maxWidth150 : '')
            },
            fieldsVisibility.PODetails && _react2.default.createElement(
              _react.Fragment,
              null,
              _react2.default.createElement(
                'div',
                {
                  className: (0, _classnames2.default)(classes.mb30, classes.width150, classes.mr20)
                },
                _react2.default.createElement(_OutlineInput2.default, {
                  label: 'PO No.',
                  value: PONo,
                  onChange: changeField('PONo'),
                  onInput: function onInput(e) {
                    return DecimalInputFilter.inputTextFilterForLength(e.target, 30);
                  }
                })
              ),
              _react2.default.createElement(
                'div',
                {
                  className: (0, _classnames2.default)(classes.mb30, classes.width150, classes.mr20)
                },
                _react2.default.createElement(
                  'div',
                  { className: (0, _classnames2.default)(classes.datePickerWrapper) },
                  _react2.default.createElement(_JqueryDatePicker2.default, {
                    CustomInput: _OutlineInput2.default,
                    format: 'dd/mm/yy',
                    label: 'PO Date.',
                    value: PODate,
                    validate: true,
                    onChange: changeField('PODate'),
                    onInput: function onInput(e) {
                      return DecimalInputFilter.inputTextFilterForDate(e.target);
                    }
                  }),
                  _react2.default.createElement(_DateRange2.default, {
                    onClick: this.setDatePickerFocus,
                    fontSize: 'small',
                    className: classes.PODateIcon
                  })
                )
              )
            ),
            fieldsVisibility.eWayBillNo && _react2.default.createElement(
              'div',
              {
                className: (0, _classnames2.default)(classes.mb30, classes.width150, classes.mr20)
              },
              _react2.default.createElement(_OutlineInput2.default, {
                label: 'E-Way Bill No.',
                value: eWayBillNo,
                onChange: changeField('eWayBillNo'),
                onInput: function onInput(e) {
                  return DecimalInputFilter.inputTextFilterForLength(e.target, 30);
                }
              })
            ),
            extraFields.map(function (field, index) {
              return _react2.default.createElement(
                _react.Fragment,
                { key: field.udfModel.getUdfFieldId() },
                field.udfModel.getUdfFieldDataType() != _UDFFieldsConstant2.default.DATA_TYPE_DATE && _react2.default.createElement(
                  'div',
                  {
                    className: (0, _classnames2.default)(classes.mb30, classes.width150, classes.mr20)
                  },
                  _react2.default.createElement(_OutlineInput2.default, {
                    label: _react2.default.createElement(_Label2.default, {
                      label: field.udfModel.getUdfFieldName(),
                      length: 15,
                      dots: '..'
                    }),
                    onInput: function onInput(e) {
                      return DecimalInputFilter.inputTextFilterForLength(e.target, 30);
                    },
                    value: field.value,
                    onChange: function onChange(e) {
                      return changeExtraField(index, e.target.value);
                    }
                  })
                ),
                field.udfModel.getUdfFieldDataType() == _UDFFieldsConstant2.default.DATA_TYPE_DATE && _react2.default.createElement(
                  'div',
                  {
                    className: (0, _classnames2.default)(classes.mb30, classes.width150, classes.mr20)
                  },
                  _react2.default.createElement(
                    'div',
                    { className: (0, _classnames2.default)(classes.datePickerWrapper) },
                    _react2.default.createElement(_JqueryDatePicker2.default, {
                      CustomInput: _OutlineInput2.default,
                      label: _react2.default.createElement(_Label2.default, {
                        label: field.udfModel.getUdfFieldName(),
                        length: 15,
                        dots: '..'
                      }),
                      format: field.udfModel.getUdfFieldDataFormat() == 1 ? 'dd/mm/yy' : 'mm/yy',
                      validate: true,
                      monthPicker: field.udfModel.getUdfFieldDataFormat() != _StringConstants2.default.dateMonthYear,
                      value: field.value,
                      onChange: function onChange(e) {
                        return changeExtraField(index, typeof e === 'string' ? e : e.target.value);
                      },
                      onInput: function onInput(e) {
                        return DecimalInputFilter.inputTextFilterForDate(e.target);
                      }
                    }),
                    _react2.default.createElement(_DateRange2.default, {
                      onClick: _this3.setDatePickerFocus,
                      color: 'primary',
                      fontSize: 'small',
                      className: classes.PODateIcon
                    })
                  )
                )
              );
            })
          ),
          _react2.default.createElement(
            'div',
            { className: classes.width300 },
            fieldsVisibility.returnNo && _react2.default.createElement(
              _react.Fragment,
              null,
              fieldsVisibility.invoiceDate && _react2.default.createElement(
                _react.Fragment,
                null,
                _react2.default.createElement(
                  'div',
                  { className: (0, _classnames2.default)(classes.mb18, 'd-flex') },
                  _react2.default.createElement(
                    'div',
                    { className: classes.width120 },
                    _react2.default.createElement(
                      'label',
                      { className: classes.inlineLabel },
                      'Return No.'
                    )
                  ),
                  _react2.default.createElement(
                    'div',
                    { className: classes.width180 },
                    txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN && fieldsVisibility.invoicePrefix && invoicePrefixOptions.length > 0 && _react2.default.createElement(
                      'select',
                      {
                        className: classes.invoiceDropdown,
                        value: invoicePrefix || '',
                        onChange: changePrefix
                      },
                      _react2.default.createElement(
                        'option',
                        { value: '' },
                        'NONE'
                      ),
                      invoicePrefixOptions.map(function (prefix) {
                        return _react2.default.createElement(
                          'option',
                          {
                            key: prefix.getPrefixId(),
                            value: prefix.getPrefixId()
                          },
                          prefix.getPrefixValue()
                        );
                      })
                    ),
                    _react2.default.createElement('input', {
                      className: (0, _classnames2.default)(classes.inlineTextField, classes.invoiceNumber),
                      value: returnNo,
                      title: returnNo,
                      onChange: this.changeReturnNo
                    })
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: (0, _classnames2.default)(classes.mb18, 'd-flex') },
                  _react2.default.createElement(
                    'div',
                    { className: classes.width120 },
                    _react2.default.createElement(
                      'label',
                      { className: classes.inlineLabel },
                      labels.invoiceNo
                    )
                  ),
                  _react2.default.createElement(
                    'div',
                    { className: classes.width180 },
                    _react2.default.createElement('input', {
                      className: (0, _classnames2.default)(classes.inlineTextField, classes.invoiceNumber),
                      value: txnReturnRefNumber,
                      title: txnReturnRefNumber,
                      onChange: this.changeInvoiceNumber('txnReturnRefNumber')
                    })
                  )
                )
              ),
              _react2.default.createElement(
                'div',
                { className: (0, _classnames2.default)(classes.mb18, 'd-flex') },
                _react2.default.createElement(
                  'div',
                  { className: classes.width120 },
                  _react2.default.createElement(
                    'label',
                    { className: classes.inlineLabel },
                    labels.invoiceDate
                  )
                ),
                _react2.default.createElement(
                  'div',
                  {
                    className: (0, _classnames2.default)(classes.width180, classes.height20, classes.datePickerWrapper)
                  },
                  _react2.default.createElement(_JqueryDatePicker2.default, {
                    className: (0, _classnames2.default)(classes.inlineTextField, classes.transparentDatepicker),
                    validate: true,
                    value: txnReturnRefDate,
                    onChange: changeField('txnReturnRefDate'),
                    onInput: function onInput(e) {
                      return DecimalInputFilter.inputTextFilterForDate(e.target);
                    }
                  }),
                  _react2.default.createElement(_DateRange2.default, {
                    onClick: this.setDatePickerFocus,
                    className: classes.dateIcon
                  })
                )
              ),
              _react2.default.createElement(
                'div',
                { className: (0, _classnames2.default)(classes.mb18, 'd-flex') },
                _react2.default.createElement(
                  'div',
                  { className: classes.width120 },
                  _react2.default.createElement(
                    'label',
                    { className: classes.inlineLabel },
                    'Date'
                  ),
                  ' '
                ),
                _react2.default.createElement(
                  'div',
                  {
                    className: (0, _classnames2.default)(classes.width180, classes.height20, classes.datePickerWrapper)
                  },
                  _react2.default.createElement(_JqueryDatePicker2.default, {
                    className: (0, _classnames2.default)(classes.inlineTextField, classes.transparentDatepicker),
                    value: invoiceDate,
                    validate: true,
                    onChange: function onChange(e) {
                      return _this3.changeInvoiceDate(typeof e === 'string' ? e : e.target.value);
                    },
                    onInput: function onInput(e) {
                      return DecimalInputFilter.inputTextFilterForDate(e.target);
                    }
                  }),
                  _react2.default.createElement(_DateRange2.default, {
                    onClick: this.setDatePickerFocus,
                    className: classes.dateIcon
                  })
                )
              )
            ),
            !fieldsVisibility.returnNo && _react2.default.createElement(
              _react.Fragment,
              null,
              fieldsVisibility.invoiceNo && _react2.default.createElement(
                'div',
                { className: (0, _classnames2.default)(classes.mb18, 'd-flex') },
                _react2.default.createElement(
                  'div',
                  { className: classes.width120 },
                  _react2.default.createElement(
                    'label',
                    { className: classes.inlineLabel },
                    labels.invoiceNo
                  ),
                  ' '
                ),
                _react2.default.createElement(
                  'div',
                  { className: (0, _classnames2.default)(classes.width180, 'd-flex') },
                  fieldsVisibility.invoicePrefix && invoicePrefixOptions.length > 0 && _react2.default.createElement(
                    'select',
                    {
                      className: classes.invoiceDropdown,
                      value: invoicePrefix || '',
                      onChange: changePrefix
                    },
                    _react2.default.createElement(
                      'option',
                      { value: '' },
                      'NONE'
                    ),
                    invoicePrefixOptions.map(function (prefix) {
                      return _react2.default.createElement(
                        'option',
                        {
                          key: prefix.getPrefixId(),
                          value: prefix.getPrefixId()
                        },
                        prefix.getPrefixValue()
                      );
                    })
                  ),
                  _react2.default.createElement('input', {
                    className: (0, _classnames2.default)(classes.inlineTextField, classes.invoiceNumber),
                    value: invoiceNo,
                    title: invoiceNo,
                    onChange: this.changeInvoiceNumber('invoiceNo')
                  })
                )
              ),
              !fieldsVisibility.invoiceNo && (txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN) && _react2.default.createElement(
                'div',
                { className: (0, _classnames2.default)(classes.mb18, 'd-flex') },
                _react2.default.createElement(
                  'div',
                  { className: classes.width120 },
                  _react2.default.createElement(
                    'label',
                    { className: classes.inlineLabel },
                    labels.invoiceNo
                  ),
                  ' '
                ),
                _react2.default.createElement(
                  'div',
                  { className: (0, _classnames2.default)(classes.width180, 'd-flex') },
                  _react2.default.createElement('input', {
                    className: (0, _classnames2.default)(classes.inlineTextField, classes.invoiceNumber),
                    value: txnReturnRefNumber,
                    title: txnReturnRefNumber,
                    onChange: this.changeInvoiceNumber('txnReturnRefNumber')
                  })
                )
              ),
              fieldsVisibility.invoiceDate && _react2.default.createElement(
                'div',
                { className: (0, _classnames2.default)(classes.mb18, 'd-flex') },
                _react2.default.createElement(
                  'div',
                  {
                    className: (0, _classnames2.default)(classes.width120, txnType === _TxnTypeConstant2.default.TXN_TYPE_EXPENSE || txnType === _TxnTypeConstant2.default.TXN_TYPE_OTHER_INCOME ? classes.expenseIncomeLabel : '')
                  },
                  _react2.default.createElement(
                    'label',
                    { className: classes.inlineLabel },
                    labels.invoiceDate
                  )
                ),
                _react2.default.createElement(
                  'div',
                  {
                    className: (0, _classnames2.default)(classes.width180, classes.height20, classes.datePickerWrapper)
                  },
                  _react2.default.createElement(_JqueryDatePicker2.default, {
                    className: (0, _classnames2.default)(classes.inlineTextField, classes.transparentDatepicker),
                    value: invoiceDate,
                    validate: true,
                    onChange: function onChange(e) {
                      return _this3.changeInvoiceDate(typeof e === 'string' ? e : e.target.value);
                    },
                    onInput: function onInput(e) {
                      return DecimalInputFilter.inputTextFilterForDate(e.target);
                    }
                  }),
                  _react2.default.createElement(_DateRange2.default, {
                    onClick: this.setDatePickerFocus,
                    className: classes.dateIcon
                  })
                )
              )
            ),
            !cashSale && fieldsVisibility.paymentTerms && paymentTermsOptions.length > 0 && _react2.default.createElement(
              'div',
              { className: (0, _classnames2.default)(classes.mb18, 'd-flex') },
              _react2.default.createElement(
                'div',
                { className: classes.width120 },
                _react2.default.createElement(
                  'label',
                  { className: classes.inlineLabel },
                  'Payment Terms'
                ),
                ' '
              ),
              _react2.default.createElement(
                'div',
                { className: classes.width180 },
                _react2.default.createElement(
                  'select',
                  {
                    className: (0, _classnames2.default)(classes.inlineTextField, classes.transparentDropdown),
                    value: paymentTerms,
                    onChange: changePaymentTerms
                  },
                  _react2.default.createElement(
                    'option',
                    { value: '' },
                    'Custom'
                  ),
                  paymentTermsOptions.map(function (option) {
                    return _react2.default.createElement(
                      'option',
                      {
                        key: option.getPaymentTermId(),
                        value: option.getPaymentTermId()
                      },
                      option.getPaymentTermName()
                    );
                  })
                )
              )
            ),
            fieldsVisibility.dueDate && !cashSale && _react2.default.createElement(
              'div',
              { className: (0, _classnames2.default)(classes.mb18, 'd-flex') },
              _react2.default.createElement(
                'div',
                { className: classes.width120 },
                _react2.default.createElement(
                  'label',
                  { className: classes.inlineLabel },
                  'Due Date'
                ),
                ' '
              ),
              _react2.default.createElement(
                'div',
                {
                  className: (0, _classnames2.default)(classes.width180, classes.dueDateContainer)
                },
                _react2.default.createElement(_JqueryDatePicker2.default, {
                  className: (0, _classnames2.default)(classes.inlineTextField, classes.transparentDatepicker),
                  value: dueDate,
                  validate: true,
                  onChange: function onChange(e) {
                    return changeDueDate(typeof e === 'string' ? e : e.target.value);
                  },
                  onInput: function onInput(e) {
                    return DecimalInputFilter.inputTextFilterForDate(e.target);
                  }
                }),
                _react2.default.createElement(_DateRange2.default, {
                  onClick: this.setDatePickerFocus,
                  className: classes.dateIcon
                }),
                dueDays > 0 && _react2.default.createElement(
                  'div',
                  { className: classes.overdueContainer },
                  'OVERDUE',
                  ' ',
                  _react2.default.createElement(
                    'span',
                    { className: classes.overdueDays },
                    dueDays,
                    ' DAYS'
                  )
                )
              )
            ),
            fieldsVisibility.placeOfSupply && !isFTU && _react2.default.createElement(
              'div',
              { className: (0, _classnames2.default)(classes.mb18, 'd-flex') },
              _react2.default.createElement(
                'div',
                { className: classes.width120 },
                _react2.default.createElement(
                  'label',
                  { className: classes.inlineLabel },
                  'State of supply'
                ),
                ' '
              ),
              _react2.default.createElement(
                'div',
                { className: classes.width180 },
                _react2.default.createElement(
                  'select',
                  {
                    disabled: !fieldsVisibility.placeOfSupplyEnabled,
                    className: (0, _classnames2.default)(classes.inlineTextField, classes.placeOfSupply, classes.transparentDropdown),
                    value: placeOfSupply,
                    onChange: function onChange(e) {
                      return changePlaceOfSupply(e.target.value);
                    }
                  },
                  _react2.default.createElement(
                    'option',
                    { value: '' },
                    'NONE'
                  ),
                  listOfStates.map(function (state) {
                    return _react2.default.createElement(
                      'option',
                      { key: state, value: state },
                      state
                    );
                  })
                )
              )
            )
          )
        )
      );
    }
  }]);
  return SalePurchaseHeaderFields;
}(_react.Component);

SalePurchaseHeaderFields.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  txnType: _propTypes2.default.number,
  createParty: _propTypes2.default.func,
  changeParty: _propTypes2.default.func,
  items: _propTypes2.default.array,
  changeField: _propTypes2.default.func,
  changeFields: _propTypes2.default.func,
  changePrefix: _propTypes2.default.func,
  changeDueDate: _propTypes2.default.func,
  changePaymentTerms: _propTypes2.default.func,
  changeExtraField: _propTypes2.default.func,
  cashSale: _propTypes2.default.bool,
  listOfStates: _propTypes2.default.array,
  invoicePrefixOptions: _propTypes2.default.array,
  paymentTermsOptions: _propTypes2.default.array,
  party: _propTypes2.default.object,
  displayName: _propTypes2.default.string,
  billingAddress: _propTypes2.default.string,
  shippingAddress: _propTypes2.default.string,
  PONo: _propTypes2.default.string,
  PODate: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.instanceOf(Date)]),
  eWayBillNo: _propTypes2.default.string,
  extraFields: _propTypes2.default.array,
  returnNo: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
  txnReturnRefNumber: _propTypes2.default.string,
  txnReturnRefDate: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.instanceOf(Date)]),
  invoicePrefix: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
  invoiceNo: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
  invoiceDate: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.instanceOf(Date)]),
  paymentTerms: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
  dueDate: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.instanceOf(Date)]),
  dueDays: _propTypes2.default.number,
  placeOfSupply: _propTypes2.default.string,
  changePlaceOfSupply: _propTypes2.default.func,
  fieldsVisibility: _propTypes2.default.object,
  labels: _propTypes2.default.object,
  isFTU: _propTypes2.default.bool,
  isConvert: _propTypes2.default.bool,
  isEdit: _propTypes2.default.bool
};

exports.default = (0, _styles.withStyles)(styles)(SalePurchaseHeaderFields);