Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/core/styles');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _themes = require('../../../themes');

var _themes2 = _interopRequireDefault(_themes);

var _Slide = require('@material-ui/core/Slide');

var _Slide2 = _interopRequireDefault(_Slide);

var _NoteAdd = require('@material-ui/icons/NoteAdd');

var _NoteAdd2 = _interopRequireDefault(_NoteAdd);

var _AddAPhoto = require('@material-ui/icons/AddAPhoto');

var _AddAPhoto2 = _interopRequireDefault(_AddAPhoto);

var _IconButton = require('@material-ui/core/IconButton');

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _OutlineInput = require('../UIControls/OutlineInput');

var _OutlineInput2 = _interopRequireDefault(_OutlineInput);

var _FormControlLabel = require('@material-ui/core/FormControlLabel');

var _FormControlLabel2 = _interopRequireDefault(_FormControlLabel);

var _Checkbox = require('@material-ui/core/Checkbox');

var _Checkbox2 = _interopRequireDefault(_Checkbox);

var _Select = require('@material-ui/core/Select');

var _Select2 = _interopRequireDefault(_Select);

var _FormControl = require('@material-ui/core/FormControl');

var _FormControl2 = _interopRequireDefault(_FormControl);

var _InputLabel = require('@material-ui/core/InputLabel');

var _InputLabel2 = _interopRequireDefault(_InputLabel);

var _OutlinedInput = require('@material-ui/core/OutlinedInput');

var _OutlinedInput2 = _interopRequireDefault(_OutlinedInput);

var _MyDouble = require('../../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _TxnTypeConstant = require('../../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _ImagePreview = require('./ImagePreview');

var _ImagePreview2 = _interopRequireDefault(_ImagePreview);

var _SalePurchaseHelpers = require('./SalePurchaseHelpers');

var _DecimalInputFilter = require('../../../Utilities/DecimalInputFilter');

var DecimalInputFilter = _interopRequireWildcard(_DecimalInputFilter);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var theme = (0, _themes2.default)();

var styles = function styles(theme) {
	return {
		transactionFooterFields: {
			margin: '28px 35px 56px 35px'
		},
		textFieldContainer: {
			height: 36
		},
		textField: {
			fontSize: 13,
			color: '#222A3F',
			zIndex: 1,
			height: 36
		},
		textFieldLabel: {
			fontSize: 12,
			fontFamily: 'RobotoMedium',
			color: 'rgba(34, 42, 63, 0.5)',
			transform: 'translate(14px, 13px) scale(1)'
		},
		mt20: {
			marginTop: 20
		},
		mt60: {
			marginTop: 60
		},
		mb15: {
			marginBottom: 15
		},
		mb25: {
			marginBottom: 25
		},
		ml80: {
			marginLeft: 80
		},
		ml15: {
			marginLeft: 15
		},
		mr20: {
			marginRight: 20
		},
		mlAuto: {
			marginLeft: 'auto'
		},
		notchedOutline: {
			background: '#fff'
		},
		button: {
			border: '1px solid #ddd',
			color: 'darkgray'
		},
		buttonText: {
			fontSize: 12,
			position: 'relative',
			top: 2,
			margin: '5px 0'
		},
		leftIcon: {
			marginRight: theme.spacing.unit
		},
		iconSmall: {
			fontSize: 20
		},
		positionRelative: {
			position: 'relative'
		},
		connector: {
			position: 'absolute',
			height: 55,
			border: '1px solid #ddd',
			borderLeft: 0,
			width: 7,
			left: 170,
			top: 17
		},
		iconButton: {
			borderRadius: 4,
			color: 'darkgray'
		},
		dateIcon: {
			position: 'absolute',
			right: 5,
			top: 7
		},
		datePickerWrapper: {
			position: 'relative'
		},
		inlineLabelContainer: {},
		footerInlineLabel: {
			fontSize: 12,
			color: '#222A3FB3',
			fontFamily: 'RobotoMedium'
		},
		inlineTextFieldContainer: {
			width: 190,
			marginLeft: 10
		},
		footerInlineTextField: {
			height: 32,
			border: '1px solid #ddd',
			borderRadius: 4,
			padding: 5,
			width: 190,
			textAlign: 'right'
		},
		footerFieldsContainer: {
			height: 50
		},
		discountPercent: {
			width: 90,
			paddingRight: 25
		},
		discountAmount: {
			width: 90,
			paddingRight: 30
		},
		taxPercent: {
			height: 32,
			color: 'rgba(34, 58, 82, 0.54)',
			background: 'transparent',
			borderRadius: 4,
			fontSize: 10
		},
		ITCDropdown: {
			height: 32,
			color: 'rgba(34, 58, 82, 0.54)',
			background: 'transparent',
			borderRadius: 4,
			fontSize: 10,
			width: '100%'
		},
		taxAmount: {
			fontSize: 13,
			color: '#222A3F',
			fontFamily: 'RobotoMedium'
		},
		addon: {
			position: 'absolute',
			right: 5,
			top: 9,
			fontSize: 12,
			color: 'rgba(34, 42, 63, 0.5)',
			fontFamily: 'RobotoMedium'
		},
		balance: {
			fontWeight: 'bold',
			color: '#222A3F'
		},
		balanceColor: {
			color: '#1789FC',
			fontWeight: 'bold',
			fontSize: 14,
			fontFamily: 'RobotoMedium',
			textAlign: 'right'
		},
		roundOff: {
			// background: '#fff',
			borderRadius: 4,
			height: 32,
			padding: '2px 4px',
			marginRight: 24,
			fontFamily: 'RobotoMedium'
		},
		roundOffAmount: {
			// background: '#E2E7F1',
			height: 24,
			width: 50,
			textAlign: 'right',
			borderRadius: 4,
			paddingRight: 5,
			paddingTop: 5,
			fontSize: 12,
			color: '#222A3F'
		},
		roundOffLabel: {
			fontSize: 10,
			color: 'rgba(34, 58, 82, 0.54)'
		},
		transportFieldsContainer: {
			maxWidth: 400,
			width: '100%',
			height: 200,
			marginRight: 60
		},
		formControl: {
			height: 36,
			'& label': {
				color: 'rgba(34, 42, 63, 0.5)',
				fontFamily: 'RobotoMedium',
				fontSize: 16,
				transform: 'translate(14px, 13px) scale(0.81)'
			},
			'& select': {
				padding: '10px 25px 10px 15px',
				width: 128,
				height: 18,
				fontSize: 13,
				'&:focus': {
					background: 'transparent'
				}
			}
		},
		inputRoot: {
			background: '#fff'
		},
		textArea: {
			height: 'auto',
			width: 300
		},
		imageContainer: {
			position: 'relative',
			overflow: 'hidden',
			width: 170,
			marginTop: 20
		},
		image: {
			width: 170,
			height: 128,
			cursor: 'pointer',
			backgroundSize: 'contain',
			backgroundRepeat: 'no-repeat'
		},
		imageActions: {
			position: 'absolute',
			top: 90,
			display: 'flex',
			background: 'rgba(0,0,0,0.5)',
			height: 40,
			alignItems: 'center',
			width: '100%',
			justifyContent: 'space-between'
		},
		imageAction: {
			color: '#fff',
			padding: 10,
			textTransform: 'uppercase',
			fontSize: 13,
			cursor: 'pointer'
		}
	};
};

var SalePurchaseFooterFields = function (_Component) {
	(0, _inherits3.default)(SalePurchaseFooterFields, _Component);

	function SalePurchaseFooterFields(props) {
		(0, _classCallCheck3.default)(this, SalePurchaseFooterFields);

		var _this = (0, _possibleConstructorReturn3.default)(this, (SalePurchaseFooterFields.__proto__ || (0, _getPrototypeOf2.default)(SalePurchaseFooterFields)).call(this, props));

		_this.toggleDescription = function (e) {
			var toggleDescription = _this.props.toggleDescription;

			_this.descriptionButtonClicked = true;
			toggleDescription && toggleDescription(e);
		};

		_this.showImagePreview = function () {
			_this.setState({ showImagePreview: true });
		};

		_this.closeImagePreview = function () {
			_this.setState({ showImagePreview: false });
		};

		_this.state = {
			imageActionVisible: false,
			showImagePreview: false
		};
		_this.showImageActions = _this.showImageActions.bind(_this);
		_this.hideImageActions = _this.hideImageActions.bind(_this);
		_this.deleteImage = _this.deleteImage.bind(_this);
		_this.changeTotalDiscountPercent = _this.changeTotalDiscountPercent.bind(_this);
		_this.changeTotalDiscountAmount = _this.changeTotalDiscountAmount.bind(_this);
		_this.changeTotalTaxPercent = _this.changeTotalTaxPercent.bind(_this);
		_this.changeAdditionalCharge = _this.changeAdditionalCharge.bind(_this);
		_this.changeRoundOffAmount = _this.changeRoundOffAmount.bind(_this);
		_this.toggleRoundOff = _this.toggleRoundOff.bind(_this);
		_this.changeTotal = _this.changeTotal.bind(_this);
		_this.changeReceivedAmount = _this.changeReceivedAmount.bind(_this);
		_this.changeITC = props.changeField('ITC');

		_this.roundOffChanged = false;
		_this.ITCDropdownOptions = (0, _SalePurchaseHelpers.getITCDropdownOptions)();
		_this.descriptionButtonClicked = false;
		return _this;
	}

	(0, _createClass3.default)(SalePurchaseFooterFields, [{
		key: 'showImageActions',
		value: function showImageActions() {
			this.setState({
				imageActionVisible: true
			});
		}
	}, {
		key: 'hideImageActions',
		value: function hideImageActions() {
			this.setState({
				imageActionVisible: false
			});
		}
	}, {
		key: 'deleteImage',
		value: function deleteImage(e) {
			e.stopPropagation();
			this.props.changeField('image')('');
		}
	}, {
		key: 'changeTotalDiscountAmount',
		value: function changeTotalDiscountAmount(e) {
			var value = DecimalInputFilter.inputTextFilterForAmount(e.target);
			var _props = this.props,
			    txnType = _props.txnType,
			    subTotal = _props.subTotal,
			    taxPercent = _props.taxPercent,
			    roundOff = _props.roundOff,
			    roundOffAmount = _props.roundOffAmount,
			    isReverseCharge = _props.isReverseCharge,
			    reverseChargeAmount = _props.reverseChargeAmount,
			    additionalCharges = _props.additionalCharges,
			    cashSale = _props.cashSale,
			    received = _props.received,
			    receivedLater = _props.receivedLater,
			    discountForCash = _props.discountForCash,
			    selectedTransactionMap = _props.selectedTransactionMap,
			    changeFields = _props.changeFields;


			var discountPercent = (0, _SalePurchaseHelpers.changeTotalDiscountAmount)(subTotal, value);

			var tax = (0, _SalePurchaseHelpers.changeTotalTaxPercent)(txnType, subTotal, value, taxPercent);

			var newTransaction = (0, _SalePurchaseHelpers.changeTotalAndBalWithDiscountView)({
				subTotal: subTotal,
				discountAmount: value,
				discountPercent: discountPercent,
				taxAmount: tax.taxAmount,
				roundOff: roundOff,
				roundOffAmount: roundOffAmount,
				isReverseCharge: isReverseCharge,
				reverseChargeAmount: reverseChargeAmount,
				additionalCharges: additionalCharges,
				cashSale: cashSale,
				received: received,
				roundOffChanged: this.roundOffChanged,
				ITCVisibility: tax.ITCVisibility
			});

			if (selectedTransactionMap.size > 0) {
				newTransaction = (0, _SalePurchaseHelpers.calculateCurrentTransactionAmount)(txnType, (0, _extends3.default)({}, newTransaction, {
					receivedLater: receivedLater,
					isReverseCharge: isReverseCharge,
					discountForCash: discountForCash
				}));
			}

			changeFields((0, _extends3.default)({}, newTransaction, {
				taxAmount: tax.taxAmount,
				discountAmount: value,
				discountPercent: discountPercent
			}));
		}
	}, {
		key: 'changeTotalDiscountPercent',
		value: function changeTotalDiscountPercent(e) {
			var value = DecimalInputFilter.inputTextFilterForPercentage(e.target);

			if (Number(value) > 100) {
				ToastHelper.error('Discount Percentage can not be more than 100 percent');
				return false;
			}

			var _props2 = this.props,
			    txnType = _props2.txnType,
			    subTotal = _props2.subTotal,
			    taxPercent = _props2.taxPercent,
			    roundOff = _props2.roundOff,
			    roundOffAmount = _props2.roundOffAmount,
			    isReverseCharge = _props2.isReverseCharge,
			    reverseChargeAmount = _props2.reverseChargeAmount,
			    additionalCharges = _props2.additionalCharges,
			    cashSale = _props2.cashSale,
			    received = _props2.received,
			    changeFields = _props2.changeFields,
			    selectedTransactionMap = _props2.selectedTransactionMap,
			    receivedLater = _props2.receivedLater,
			    discountForCash = _props2.discountForCash;


			var discountAmount = (0, _SalePurchaseHelpers.changeTotalDiscountPercent)(subTotal, value);

			var tax = (0, _SalePurchaseHelpers.changeTotalTaxPercent)(txnType, subTotal, discountAmount, taxPercent);

			var newTransaction = (0, _SalePurchaseHelpers.changeTotalAndBalWithDiscountView)({
				subTotal: subTotal,
				discountAmount: discountAmount,
				discountPercent: value,
				taxAmount: tax.taxAmount,
				roundOff: roundOff,
				roundOffAmount: roundOffAmount,
				isReverseCharge: isReverseCharge,
				reverseChargeAmount: reverseChargeAmount,
				additionalCharges: additionalCharges,
				cashSale: cashSale,
				received: received,
				roundOffChanged: this.roundOffChanged,
				ITCVisibility: tax.ITCVisibility
			});

			if (selectedTransactionMap.size > 0) {
				newTransaction = (0, _SalePurchaseHelpers.calculateCurrentTransactionAmount)(txnType, (0, _extends3.default)({}, newTransaction, {
					receivedLater: receivedLater,
					isReverseCharge: isReverseCharge,
					discountForCash: discountForCash
				}));
			}

			changeFields((0, _extends3.default)({}, newTransaction, {
				taxAmount: tax.taxAmount,
				discountAmount: discountAmount,
				discountPercent: value
			}));
		}
	}, {
		key: 'changeTotalTaxPercent',
		value: function changeTotalTaxPercent(e) {
			var value = e.target.value;
			var _props3 = this.props,
			    txnType = _props3.txnType,
			    subTotal = _props3.subTotal,
			    discountAmount = _props3.discountAmount,
			    discountPercent = _props3.discountPercent,
			    roundOff = _props3.roundOff,
			    roundOffAmount = _props3.roundOffAmount,
			    isReverseCharge = _props3.isReverseCharge,
			    additionalCharges = _props3.additionalCharges,
			    cashSale = _props3.cashSale,
			    received = _props3.received,
			    changeFields = _props3.changeFields,
			    lineItemTotalTax = _props3.lineItemTotalTax,
			    discountForCash = _props3.discountForCash,
			    receivedLater = _props3.receivedLater,
			    selectedTransactionMap = _props3.selectedTransactionMap;


			var tax = (0, _SalePurchaseHelpers.changeTotalTaxPercent)(txnType, subTotal, discountAmount, value);

			var reverseChargeAmount = isReverseCharge ? _MyDouble2.default.convertStringToDouble(lineItemTotalTax) + _MyDouble2.default.convertStringToDouble(tax.taxAmount) : 0;

			var newTransaction = (0, _SalePurchaseHelpers.changeTotalAndBalWithDiscountView)({
				subTotal: subTotal,
				discountAmount: discountAmount,
				discountPercent: discountPercent,
				taxAmount: tax.taxAmount,
				roundOff: roundOff,
				roundOffAmount: roundOffAmount,
				isReverseCharge: isReverseCharge,
				reverseChargeAmount: reverseChargeAmount,
				additionalCharges: additionalCharges,
				cashSale: cashSale,
				received: received,
				roundOffChanged: this.roundOffChanged,
				ITCVisibility: tax.ITCVisibility
			});

			if (selectedTransactionMap.size > 0) {
				newTransaction = (0, _SalePurchaseHelpers.calculateCurrentTransactionAmount)(txnType, (0, _extends3.default)({}, newTransaction, {
					receivedLater: receivedLater,
					isReverseCharge: isReverseCharge,
					discountForCash: discountForCash
				}));
			}
			changeFields((0, _extends3.default)({}, newTransaction, tax, {
				reverseChargeAmount: reverseChargeAmount,
				taxPercent: value
			}));
		}
	}, {
		key: 'changeAdditionalCharge',
		value: function changeAdditionalCharge(e, index) {
			var value = DecimalInputFilter.inputTextFilterForAmount(e.target, true);
			if (isNaN(_MyDouble2.default.convertStringToDouble(value))) {
				return false;
			}
			var _props4 = this.props,
			    additionalCharges = _props4.additionalCharges,
			    subTotal = _props4.subTotal,
			    discountAmount = _props4.discountAmount,
			    discountPercent = _props4.discountPercent,
			    taxAmount = _props4.taxAmount,
			    roundOff = _props4.roundOff,
			    roundOffAmount = _props4.roundOffAmount,
			    isReverseCharge = _props4.isReverseCharge,
			    reverseChargeAmount = _props4.reverseChargeAmount,
			    cashSale = _props4.cashSale,
			    received = _props4.received,
			    ITCVisibility = _props4.ITCVisibility,
			    changeFields = _props4.changeFields,
			    receivedLater = _props4.receivedLater,
			    discountForCash = _props4.discountForCash,
			    selectedTransactionMap = _props4.selectedTransactionMap,
			    txnType = _props4.txnType;

			additionalCharges[index].value = value;

			var newTransaction = (0, _SalePurchaseHelpers.changeTotalAndBalWithDiscountView)({
				subTotal: subTotal,
				discountAmount: discountAmount,
				discountPercent: discountPercent,
				taxAmount: taxAmount,
				roundOff: roundOff,
				roundOffAmount: roundOffAmount,
				isReverseCharge: isReverseCharge,
				reverseChargeAmount: reverseChargeAmount,
				additionalCharges: additionalCharges,
				cashSale: cashSale,
				received: received,
				roundOffChanged: this.roundOffChanged,
				ITCVisibility: ITCVisibility
			});

			if (selectedTransactionMap.size > 0) {
				newTransaction = (0, _SalePurchaseHelpers.calculateCurrentTransactionAmount)(txnType, (0, _extends3.default)({}, newTransaction, {
					receivedLater: receivedLater,
					isReverseCharge: isReverseCharge,
					discountForCash: discountForCash
				}));
			}

			changeFields((0, _extends3.default)({}, newTransaction, { additionalCharges: additionalCharges }));
		}
	}, {
		key: 'changeReceivedAmount',
		value: function changeReceivedAmount(e) {
			var value = DecimalInputFilter.inputTextFilterForAmount(e.target);
			var _props5 = this.props,
			    changeFields = _props5.changeFields,
			    subTotal = _props5.subTotal,
			    discountAmount = _props5.discountAmount,
			    discountPercent = _props5.discountPercent,
			    taxAmount = _props5.taxAmount,
			    roundOff = _props5.roundOff,
			    roundOffAmount = _props5.roundOffAmount,
			    isReverseCharge = _props5.isReverseCharge,
			    reverseChargeAmount = _props5.reverseChargeAmount,
			    additionalCharges = _props5.additionalCharges,
			    cashSale = _props5.cashSale,
			    ITCVisibility = _props5.ITCVisibility,
			    total = _props5.total;


			var newTransaction = (0, _SalePurchaseHelpers.changeTotalAndBalWithDiscountView)({
				subTotal: subTotal,
				discountAmount: discountAmount,
				discountPercent: discountPercent,
				taxAmount: taxAmount,
				roundOff: roundOff,
				roundOffAmount: _MyDouble2.default.convertStringToDouble(roundOffAmount),
				isReverseCharge: isReverseCharge,
				reverseChargeAmount: reverseChargeAmount,
				additionalCharges: additionalCharges,
				cashSale: cashSale,
				received: _MyDouble2.default.convertStringToDouble(value),
				roundOffChanged: this.roundOffChanged,
				ITCVisibility: ITCVisibility,
				totalAmount: total
			});
			changeFields((0, _extends3.default)({}, newTransaction, {
				received: value
			}));
		}
	}, {
		key: 'changeTotal',
		value: function changeTotal(e) {
			var _props6 = this.props,
			    changeFields = _props6.changeFields,
			    payable = _props6.payable,
			    isReverseCharge = _props6.isReverseCharge,
			    cashSale = _props6.cashSale,
			    received = _props6.received,
			    txnType = _props6.txnType,
			    discountForCash = _props6.discountForCash,
			    receivedLater = _props6.receivedLater,
			    selectedTransactionMap = _props6.selectedTransactionMap,
			    roundOff = _props6.roundOff,
			    roundOffAmount = _props6.roundOffAmount,
			    reverseChargeAmount = _props6.reverseChargeAmount;

			var value = DecimalInputFilter.inputTextFilterForAmount(e.target);

			if (txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHOUT) {
				var total = _MyDouble2.default.convertStringToDouble(value);
				var currentTxnBalance = total + _MyDouble2.default.convertStringToDouble(discountForCash) - _MyDouble2.default.convertStringToDouble(receivedLater || 0);
				var balance = _MyDouble2.default.convertStringToDouble(total + _MyDouble2.default.convertStringToDouble(discountForCash));
				changeFields({
					balance: balance,
					total: total,
					currentTxnBalance: _MyDouble2.default.getAmountWithDecimal(currentTxnBalance)
				});
			} else {
				var _balance = 0;
				var receivedAmount = received;
				if (cashSale) {
					receivedAmount = value;
				}
				if (isReverseCharge) {
					receivedAmount = payable;
				}
				_balance = _MyDouble2.default.getAmountWithDecimal(_MyDouble2.default.convertStringToDouble(value) - _MyDouble2.default.convertStringToDouble(receivedAmount));

				var transaction = {
					received: receivedAmount,
					total: value,
					balance: _balance,
					isReverseCharge: isReverseCharge,
					receivedLater: receivedLater,
					payable: payable,
					discountForCash: discountForCash
				};

				if (selectedTransactionMap.size > 0) {
					transaction = (0, _SalePurchaseHelpers.calculateCurrentTransactionAmount)(txnType, transaction);
				}
				changeFields((0, _extends3.default)({}, transaction, {
					roundOffAmount: roundOffAmount
				}));
			}
		}
	}, {
		key: 'changeRoundOffAmount',
		value: function changeRoundOffAmount(e) {
			var value = DecimalInputFilter.inputTextFilterForAmount(e.target, true);
			this.roundOffChanged = true;
			var _props7 = this.props,
			    changeFields = _props7.changeFields,
			    subTotal = _props7.subTotal,
			    discountAmount = _props7.discountAmount,
			    discountPercent = _props7.discountPercent,
			    taxAmount = _props7.taxAmount,
			    roundOff = _props7.roundOff,
			    isReverseCharge = _props7.isReverseCharge,
			    reverseChargeAmount = _props7.reverseChargeAmount,
			    additionalCharges = _props7.additionalCharges,
			    cashSale = _props7.cashSale,
			    received = _props7.received,
			    ITCVisibility = _props7.ITCVisibility,
			    txnType = _props7.txnType,
			    receivedLater = _props7.receivedLater,
			    discountForCash = _props7.discountForCash,
			    selectedTransactionMap = _props7.selectedTransactionMap;


			var newTransaction = (0, _SalePurchaseHelpers.changeTotalAndBalWithDiscountView)({
				subTotal: subTotal,
				discountAmount: discountAmount,
				discountPercent: discountPercent,
				taxAmount: taxAmount,
				roundOff: roundOff,
				roundOffAmount: _MyDouble2.default.convertStringToDouble(value),
				isReverseCharge: isReverseCharge,
				reverseChargeAmount: reverseChargeAmount,
				additionalCharges: additionalCharges,
				cashSale: cashSale,
				received: received,
				roundOffChanged: this.roundOffChanged,
				ITCVisibility: ITCVisibility
			});
			if (selectedTransactionMap.size > 0) {
				newTransaction = (0, _SalePurchaseHelpers.calculateCurrentTransactionAmount)(txnType, (0, _extends3.default)({}, newTransaction, {
					receivedLater: receivedLater,
					isReverseCharge: isReverseCharge,
					discountForCash: discountForCash
				}));
			}
			changeFields((0, _extends3.default)({}, newTransaction, { roundOffAmount: value }));
		}
	}, {
		key: 'toggleRoundOff',
		value: function toggleRoundOff(e) {
			this.roundOffChanged = false;
			var value = e.target.checked;
			var _props8 = this.props,
			    changeFields = _props8.changeFields,
			    subTotal = _props8.subTotal,
			    discountAmount = _props8.discountAmount,
			    discountPercent = _props8.discountPercent,
			    taxAmount = _props8.taxAmount,
			    roundOffAmount = _props8.roundOffAmount,
			    isReverseCharge = _props8.isReverseCharge,
			    reverseChargeAmount = _props8.reverseChargeAmount,
			    additionalCharges = _props8.additionalCharges,
			    cashSale = _props8.cashSale,
			    received = _props8.received,
			    ITCVisibility = _props8.ITCVisibility,
			    txnType = _props8.txnType,
			    receivedLater = _props8.receivedLater,
			    discountForCash = _props8.discountForCash,
			    selectedTransactionMap = _props8.selectedTransactionMap;


			var newTransaction = (0, _SalePurchaseHelpers.changeTotalAndBalWithDiscountView)({
				subTotal: subTotal,
				discountAmount: discountAmount,
				discountPercent: discountPercent,
				taxAmount: taxAmount,
				roundOff: value,
				roundOffAmount: roundOffAmount,
				isReverseCharge: isReverseCharge,
				reverseChargeAmount: reverseChargeAmount,
				additionalCharges: additionalCharges,
				cashSale: cashSale,
				received: received,
				roundOffChanged: this.roundOffChanged,
				ITCVisibility: ITCVisibility
			});
			if (selectedTransactionMap.size > 0) {
				newTransaction = (0, _SalePurchaseHelpers.calculateCurrentTransactionAmount)(txnType, (0, _extends3.default)({}, newTransaction, {
					receivedLater: receivedLater,
					isReverseCharge: isReverseCharge,
					discountForCash: discountForCash
				}));
			}
			changeFields((0, _extends3.default)({}, newTransaction, { roundOff: value }));
		}
	}, {
		key: 'render',
		value: function render() {
			var _this2 = this;

			var _state = this.state,
			    imageActionVisible = _state.imageActionVisible,
			    showImagePreview = _state.showImagePreview;
			var _props9 = this.props,
			    classes = _props9.classes,
			    cashSale = _props9.cashSale,
			    taxDropdownList = _props9.taxDropdownList,
			    changeField = _props9.changeField,
			    changeTransportationField = _props9.changeTransportationField,
			    paymentTypeOptions = _props9.paymentTypeOptions,
			    showDescription = _props9.showDescription,
			    description = _props9.description,
			    paymentType = _props9.paymentType,
			    transportationDetails = _props9.transportationDetails,
			    changeImage = _props9.changeImage,
			    imageBlob = _props9.imageBlob,
			    image = _props9.image,
			    additionalCharges = _props9.additionalCharges,
			    discountPercent = _props9.discountPercent,
			    discountAmount = _props9.discountAmount,
			    discountForCash = _props9.discountForCash,
			    taxPercent = _props9.taxPercent,
			    taxAmount = _props9.taxAmount,
			    ITC = _props9.ITC,
			    roundOff = _props9.roundOff,
			    roundOffAmount = _props9.roundOffAmount,
			    total = _props9.total,
			    totalQty = _props9.totalQty,
			    received = _props9.received,
			    balance = _props9.balance,
			    currentTxnBalance = _props9.currentTxnBalance,
			    isReverseCharge = _props9.isReverseCharge,
			    reverseChargeAmount = _props9.reverseChargeAmount,
			    fieldsVisibility = _props9.fieldsVisibility,
			    labels = _props9.labels,
			    totalReceivedAmount = _props9.totalReceivedAmount,
			    payable = _props9.payable,
			    currencySymbol = _props9.currencySymbol,
			    referenceNo = _props9.referenceNo,
			    changePaymentType = _props9.changePaymentType,
			    isFTU = _props9.isFTU,
			    txnType = _props9.txnType;

			var ITCVisibility = (0, _SalePurchaseHelpers.isITCEligible)(taxPercent, txnType);
			var roundOffHTML = void 0;
			if (fieldsVisibility.roundOff || roundOffAmount) {
				roundOffHTML = _react2.default.createElement(
					'div',
					{
						className: (0, _classnames2.default)(classes.roundOff, 'd-flex align-items-center')
					},
					_react2.default.createElement(_FormControlLabel2.default, {
						classes: {
							label: classes.roundOffLabel
						},
						control: _react2.default.createElement(_Checkbox2.default, {
							checked: roundOff,
							onChange: this.toggleRoundOff,
							value: 'checkedB',
							color: 'primary'
						}),
						label: 'ROUND OFF'
					}),
					_react2.default.createElement('input', {
						onChange: this.changeRoundOffAmount,
						disabled: !roundOff,
						className: (0, _classnames2.default)(classes.footerInlineTextField, classes.roundOffAmount),
						value: roundOffAmount
					})
				);
			}

			return _react2.default.createElement(
				_styles.MuiThemeProvider,
				{ theme: theme },
				showImagePreview && _react2.default.createElement(_ImagePreview2.default, {
					onClose: this.closeImagePreview,
					src: image || 'data:image/jpg;base64,' + imageBlob }),
				_react2.default.createElement(
					'div',
					{
						id: 'sale-form-footer-fields-container',
						className: (0, _classnames2.default)(classes.transactionFooterFields) },
					_react2.default.createElement(
						'div',
						{ className: 'd-flex' },
						fieldsVisibility.transportationDetails && transportationDetails.length > 0 && _react2.default.createElement(
							'div',
							{
								className: (0, _classnames2.default)(classes.transportFieldsContainer, 'd-flex flex-column flex-wrap')
							},
							transportationDetails.map(function (field, index) {
								return _react2.default.createElement(
									'div',
									{
										key: field.id,
										className: (0, _classnames2.default)(classes.mb25, classes.mr20)
									},
									_react2.default.createElement(_OutlineInput2.default, {
										label: field.label,
										value: field.value,
										onChange: function onChange(e) {
											return changeTransportationField(index, e.target.value);
										},
										onInput: function onInput(e) {
											return DecimalInputFilter.inputTextFilterForLength(e.target, 200);
										}
									})
								);
							})
						),
						(txnType != _TxnTypeConstant2.default.TXN_TYPE_SALE || !isFTU && total > 0) && _react2.default.createElement(
							'div',
							{ className: classes.mb15 },
							fieldsVisibility.paymentType && _react2.default.createElement(
								_react2.default.Fragment,
								null,
								_react2.default.createElement(
									'div',
									{
										className: (0, _classnames2.default)(classes.mb15, classes.positionRelative)
									},
									paymentType != 1 && _react2.default.createElement('div', { className: classes.connector }),
									_react2.default.createElement(
										_FormControl2.default,
										{
											variant: 'outlined',
											className: classes.formControl
										},
										_react2.default.createElement(
											_InputLabel2.default,
											{ shrink: true, htmlFor: 'paymentTypeSelect' },
											'Payment Type'
										),
										_react2.default.createElement(
											_Select2.default,
											{
												native: true,
												value: paymentType,
												onChange: changePaymentType,
												input: _react2.default.createElement(_OutlinedInput2.default, {
													classes: { root: classes.inputRoot },
													name: 'paymentType',
													labelWidth: 100,
													id: 'paymentTypeSelect'
												})
											},
											paymentTypeOptions.map(function (type) {
												return _react2.default.createElement(
													'option',
													{ key: type.getId(), value: type.getId() },
													' ',
													type.getName()
												);
											}),
											_react2.default.createElement(
												'option',
												{ value: '' },
												'+ Add Bank A/C'
											)
										)
									)
								),
								paymentType != 1 && _react2.default.createElement(
									'div',
									{ className: classes.mb25 },
									_react2.default.createElement(_OutlineInput2.default, {
										label: 'Reference No.',
										value: referenceNo,
										onChange: changeField('referenceNo'),
										onInput: function onInput(e) {
											return DecimalInputFilter.inputTextFilterForLength(e.target, 40);
										}
									})
								)
							),
							fieldsVisibility.description && _react2.default.createElement(
								_react2.default.Fragment,
								null,
								!showDescription && !description && _react2.default.createElement(
									'div',
									{ className: classes.mb15 },
									_react2.default.createElement(
										_Button2.default,
										{
											onFocus: this.toggleDescription,
											onClick: this.toggleDescription,
											variant: 'outlined',
											color: 'primary',
											className: classes.button
										},
										_react2.default.createElement(_NoteAdd2.default, {
											className: (0, _classnames2.default)(classes.leftIcon, classes.iconSmall)
										}),
										_react2.default.createElement(
											'span',
											{ className: classes.buttonText },
											'Add Description'
										)
									)
								),
								(showDescription || description) && _react2.default.createElement(
									'div',
									null,
									_react2.default.createElement(_OutlineInput2.default, {
										label: 'Description',
										value: description,
										multiline: true,
										autoFocus: this.descriptionButtonClicked,
										rows: 4,
										rowsMax: 4,
										className: classes.textArea,
										InputProps: {
											className: classes.textFieldWidth230,
											classes: {
												input: classes.textField,
												notchedOutline: classes.textArea
											}
										},
										onChange: changeField('description'),
										onInput: function onInput(e) {
											return DecimalInputFilter.inputTextFilterForLength(e.target, 1024, true);
										}
									})
								)
							),
							fieldsVisibility.image && _react2.default.createElement(
								_react2.default.Fragment,
								null,
								!image && !imageBlob && _react2.default.createElement(
									'label',
									{ htmlFor: 'icon-button-file' },
									_react2.default.createElement(
										_IconButton2.default,
										{
											onClick: changeImage,
											color: 'primary',
											className: classes.iconButton,
											component: 'span'
										},
										_react2.default.createElement(_AddAPhoto2.default, null)
									)
								),
								(image || imageBlob) && _react2.default.createElement(
									'div',
									{
										className: classes.imageContainer,
										onClick: this.showImagePreview,
										onMouseOut: this.hideImageActions,
										onMouseOver: this.showImageActions
									},
									_react2.default.createElement('img', {
										className: classes.image,
										src: image || 'data:image/jpg;base64,' + imageBlob
									}),
									_react2.default.createElement(
										_Slide2.default,
										{ direction: 'up', 'in': imageActionVisible },
										_react2.default.createElement(
											'div',
											{ onClick: function onClick(e) {
													return e.stopPropagation();
												}, className: classes.imageActions },
											_react2.default.createElement(
												'div',
												{
													className: classes.imageAction,
													onClick: changeImage
												},
												'change'
											),
											_react2.default.createElement(
												'div',
												{
													className: classes.imageAction,
													onClick: this.deleteImage
												},
												'delete'
											)
										)
									)
								)
							)
						),
						_react2.default.createElement(
							'div',
							{ className: (0, _classnames2.default)('d-flex flex-fill justify-content-end') },
							_react2.default.createElement(
								'div',
								{ className: classes.inlineLabelContainer },
								fieldsVisibility.discount && _react2.default.createElement(
									'div',
									{
										className: (0, _classnames2.default)(classes.footerFieldsContainer, 'align-items-center d-flex justify-content-end')
									},
									_react2.default.createElement(
										'div',
										{ className: classes.footerInlineLabel },
										'DISCOUNT'
									),
									_react2.default.createElement(
										'div',
										{
											className: (0, _classnames2.default)(classes.inlineTextFieldContainer, 'd-flex justify-content-between align-items-center')
										},
										_react2.default.createElement(
											'div',
											{ className: classes.positionRelative },
											_react2.default.createElement('input', {
												value: discountPercent === 0 ? '' : discountPercent,
												onChange: this.changeTotalDiscountPercent,
												className: (0, _classnames2.default)(classes.footerInlineTextField, classes.discountPercent),
												type: 'text'
											}),
											_react2.default.createElement(
												'div',
												{ className: classes.addon },
												'(%)'
											)
										),
										_react2.default.createElement(
											'div',
											null,
											'-'
										),
										_react2.default.createElement(
											'div',
											{ className: classes.positionRelative },
											_react2.default.createElement('input', {
												onChange: this.changeTotalDiscountAmount,
												value: discountAmount === 0 ? '' : discountAmount,
												className: (0, _classnames2.default)(classes.footerInlineTextField, classes.discountAmount),
												type: 'text'
											}),
											_react2.default.createElement(
												'div',
												{ className: classes.addon },
												'(',
												currencySymbol,
												')'
											)
										)
									)
								),
								fieldsVisibility.tax && _react2.default.createElement(
									_react2.default.Fragment,
									null,
									_react2.default.createElement(
										'div',
										{
											className: (0, _classnames2.default)(classes.footerFieldsContainer, 'align-items-center d-flex justify-content-end')
										},
										_react2.default.createElement(
											'div',
											{ className: classes.footerInlineLabel },
											'TAX'
										),
										_react2.default.createElement(
											'div',
											{ className: classes.inlineTextFieldContainer },
											_react2.default.createElement(
												'div',
												{
													className: 'd-flex justify-content-between align-items-center'
												},
												_react2.default.createElement(
													'select',
													{
														onChange: this.changeTotalTaxPercent,
														value: taxPercent,
														className: classes.taxPercent
													},
													taxDropdownList.map(function (tax) {
														return _react2.default.createElement(
															'option',
															{ key: tax.value, value: tax.value },
															tax.label
														);
													})
												),
												_react2.default.createElement(
													'div',
													{ className: classes.taxAmount },
													_MyDouble2.default.getAmountWithDecimal(taxAmount)
												)
											)
										)
									),
									ITCVisibility && _react2.default.createElement(
										'div',
										{
											className: (0, _classnames2.default)(classes.footerFieldsContainer, 'align-items-center d-flex justify-content-end')
										},
										_react2.default.createElement(
											'div',
											{ className: classes.footerInlineLabel },
											'ELIGIBLE FOR ITC'
										),
										_react2.default.createElement(
											'div',
											{ className: classes.inlineTextFieldContainer },
											_react2.default.createElement(
												'select',
												{
													onChange: this.changeITC,
													value: ITC,
													className: classes.ITCDropdown
												},
												this.ITCDropdownOptions.map(function (_ref) {
													var value = _ref.value,
													    label = _ref.label;
													return _react2.default.createElement(
														'option',
														{ key: value, value: value },
														label
													);
												})
											)
										)
									)
								),
								fieldsVisibility.additionalCharges && additionalCharges.map(function (_ref2, index) {
									var id = _ref2.id,
									    label = _ref2.label,
									    value = _ref2.value;
									return _react2.default.createElement(
										'div',
										{
											key: id,
											className: (0, _classnames2.default)(classes.footerFieldsContainer, 'align-items-center d-flex justify-content-end')
										},
										_react2.default.createElement(
											'div',
											{ className: classes.footerInlineLabel },
											label
										),
										_react2.default.createElement(
											'div',
											{ className: classes.inlineTextFieldContainer },
											_react2.default.createElement('input', {
												onChange: function onChange(e) {
													return _this2.changeAdditionalCharge(e, index);
												},
												value: value === 0 ? '' : value,
												className: classes.footerInlineTextField,
												type: 'text'
											})
										)
									);
								}),
								fieldsVisibility.total && _react2.default.createElement(
									'div',
									{
										className: (0, _classnames2.default)(classes.footerFieldsContainer, 'align-items-center d-flex justify-content-end')
									},
									_react2.default.createElement(
										'div',
										{
											className: (0, _classnames2.default)(classes.footerInlineLabel, 'd-flex justify-content-between align-items-center')
										},
										!isReverseCharge && roundOffHTML,
										_react2.default.createElement(
											'div',
											null,
											labels.total
										)
									),
									_react2.default.createElement(
										'div',
										{ className: classes.inlineTextFieldContainer },
										_react2.default.createElement('input', {
											onChange: this.changeTotal,
											disabled: totalQty > 0,
											value: total === 0 ? '' : total,
											className: classes.footerInlineTextField,
											type: 'text'
										})
									)
								),
								isReverseCharge && _react2.default.createElement(
									_react2.default.Fragment,
									null,
									_react2.default.createElement(
										'div',
										{
											className: (0, _classnames2.default)(classes.footerFieldsContainer, 'align-items-center d-flex justify-content-end')
										},
										_react2.default.createElement(
											'div',
											{ className: classes.footerInlineLabel },
											'TAX UNDER REVERSE CHARGE'
										),
										_react2.default.createElement(
											'div',
											{ className: classes.inlineTextFieldContainer },
											_react2.default.createElement('input', {
												disabled: true,
												value: _MyDouble2.default.getAmountWithDecimal(reverseChargeAmount),
												className: classes.footerInlineTextField,
												type: 'text'
											})
										)
									),
									_react2.default.createElement(
										'div',
										{
											className: (0, _classnames2.default)(classes.footerFieldsContainer, 'align-items-center d-flex justify-content-end')
										},
										_react2.default.createElement(
											'div',
											{
												className: (0, _classnames2.default)(classes.footerInlineLabel, 'd-flex justify-content-between align-items-center')
											},
											roundOffHTML,
											_react2.default.createElement(
												'div',
												null,
												labels.payable
											)
										),
										_react2.default.createElement(
											'div',
											{ className: classes.inlineTextFieldContainer },
											_react2.default.createElement('input', {
												disabled: true,
												value: _MyDouble2.default.getAmountWithDecimal(payable),
												className: classes.footerInlineTextField,
												type: 'text'
											})
										)
									)
								),
								fieldsVisibility.totalReceivedAmount && _react2.default.createElement(
									'div',
									{
										className: (0, _classnames2.default)(classes.footerFieldsContainer, 'align-items-center d-flex justify-content-end')
									},
									_react2.default.createElement(
										'div',
										{ className: classes.footerInlineLabel },
										labels.totalReceivedAmount
									),
									_react2.default.createElement(
										'div',
										{ className: classes.inlineTextFieldContainer },
										_react2.default.createElement('input', {
											disabled: true,
											value: _MyDouble2.default.getAmountWithDecimal(totalReceivedAmount),
											className: classes.footerInlineTextField,
											type: 'text'
										})
									)
								),
								fieldsVisibility.received && !cashSale && (payable > 0 || total > 0) && _react2.default.createElement(
									'div',
									{
										className: (0, _classnames2.default)(classes.footerFieldsContainer, 'align-items-center d-flex justify-content-end')
									},
									_react2.default.createElement(
										'div',
										{ className: classes.footerInlineLabel },
										labels.received
									),
									_react2.default.createElement(
										'div',
										{ className: classes.inlineTextFieldContainer },
										_react2.default.createElement('input', {
											disabled: cashSale,
											onChange: this.changeReceivedAmount,
											value: received === 0 ? '' : received,
											className: classes.footerInlineTextField,
											type: 'text'
										})
									)
								),
								fieldsVisibility.discountForCash && _react2.default.createElement(
									'div',
									{
										className: (0, _classnames2.default)(classes.footerFieldsContainer, 'align-items-center d-flex justify-content-end')
									},
									_react2.default.createElement(
										'div',
										{ className: classes.footerInlineLabel },
										'DISCOUNT AMOUNT'
									),
									_react2.default.createElement(
										'div',
										{ className: classes.inlineTextFieldContainer },
										_react2.default.createElement('input', {
											onChange: changeField('discountForCash'),
											value: discountForCash === 0 ? '' : _MyDouble2.default.getAmountWithDecimal(discountForCash),
											className: classes.footerInlineTextField,
											type: 'text'
										})
									)
								),
								fieldsVisibility.balance && !cashSale && (payable > 0 || total > 0) && _react2.default.createElement(
									'div',
									{
										className: (0, _classnames2.default)(classes.footerFieldsContainer, 'align-items-center d-flex justify-content-end')
									},
									_react2.default.createElement(
										'div',
										{
											className: (0, _classnames2.default)(classes.footerInlineLabel, classes.balance)
										},
										labels.balance
									),
									_react2.default.createElement(
										'div',
										{
											className: (0, _classnames2.default)(classes.inlineTextFieldContainer, classes.balanceColor)
										},
										' ',
										_MyDouble2.default.getAmountWithDecimal(balance),
										' '
									)
								),
								fieldsVisibility.currentTxnBalance && _react2.default.createElement(
									'div',
									{
										className: (0, _classnames2.default)(classes.footerFieldsContainer, 'align-items-center d-flex justify-content-end')
									},
									_react2.default.createElement(
										'div',
										{
											className: (0, _classnames2.default)(classes.footerInlineLabel, classes.balance)
										},
										labels.balance
									),
									_react2.default.createElement(
										'div',
										{
											className: (0, _classnames2.default)(classes.inlineTextFieldContainer, classes.balanceColor)
										},
										' ',
										_MyDouble2.default.getAmountWithDecimal(currentTxnBalance),
										' '
									)
								)
							)
						)
					)
				)
			);
		}
	}]);
	return SalePurchaseFooterFields;
}(_react.Component);

SalePurchaseFooterFields.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	changeField: _propTypes2.default.func,
	changeFields: _propTypes2.default.func,
	txnType: _propTypes2.default.number,
	subTotal: _propTypes2.default.number,
	taxPercent: _propTypes2.default.number,
	roundOff: _propTypes2.default.bool,
	isFTU: _propTypes2.default.bool,
	roundOffAmount: _propTypes2.default.number,
	isReverseCharge: _propTypes2.default.bool,
	reverseChargeAmount: _propTypes2.default.number,
	additionalCharges: _propTypes2.default.array,
	cashSale: _propTypes2.default.bool,
	received: _propTypes2.default.number,
	discountAmount: _propTypes2.default.number,
	discountPercent: _propTypes2.default.number,
	taxAmount: _propTypes2.default.number,
	ITCVisibility: _propTypes2.default.bool,
	payable: _propTypes2.default.number,
	taxDropdownList: _propTypes2.default.array,
	changeTransportationField: _propTypes2.default.func,
	paymentTypeOptions: _propTypes2.default.array,
	toggleDescription: _propTypes2.default.func,
	showDescription: _propTypes2.default.bool,
	description: _propTypes2.default.string,
	paymentType: _propTypes2.default.string,
	transportationDetails: _propTypes2.default.array,
	changeImage: _propTypes2.default.func,
	imageBlob: _propTypes2.default.string,
	image: _propTypes2.default.string,
	discountForCash: _propTypes2.default.number,
	ITC: _propTypes2.default.number,
	total: _propTypes2.default.number,
	totalQty: _propTypes2.default.number,
	balance: _propTypes2.default.number,
	currentTxnBalance: _propTypes2.default.number,
	fieldsVisibility: _propTypes2.default.object,
	labels: _propTypes2.default.object,
	totalReceivedAmount: _propTypes2.default.number,
	receivedLater: _propTypes2.default.number,
	changeCheckbox: _propTypes2.default.func,
	lineItemTotalTax: _propTypes2.default.number,
	currencySymbol: _propTypes2.default.string,
	referenceNo: _propTypes2.default.string,
	hangePaymentType: _propTypes2.default.func,
	selectedTransactionMap: _propTypes2.default.object
};

exports.default = (0, _styles.withStyles)(styles)(SalePurchaseFooterFields);