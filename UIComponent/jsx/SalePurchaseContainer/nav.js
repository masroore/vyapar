Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/core/styles');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _Menu = require('@material-ui/core/Menu');

var _Menu2 = _interopRequireDefault(_Menu);

var _MenuItem = require('@material-ui/core/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _Avatar = require('@material-ui/core/Avatar');

var _Avatar2 = _interopRequireDefault(_Avatar);

var _IconButton = require('@material-ui/core/IconButton');

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Switch = require('@material-ui/core/Switch');

var _Switch2 = _interopRequireDefault(_Switch);

var _ExpandMore = require('@material-ui/icons/ExpandMore');

var _ExpandMore2 = _interopRequireDefault(_ExpandMore);

var _SettingsOutlined = require('@material-ui/icons/SettingsOutlined');

var _SettingsOutlined2 = _interopRequireDefault(_SettingsOutlined);

var _Cancel = require('@material-ui/icons/Cancel');

var _Cancel2 = _interopRequireDefault(_Cancel);

var _calculatorIcon = require('../icons/calculatorIcon');

var _calculatorIcon2 = _interopRequireDefault(_calculatorIcon);

var _themes = require('../../../themes');

var _themes2 = _interopRequireDefault(_themes);

var _analyticsHelper = require('../../../Utilities/analyticsHelper');

var MyAnalytics = _interopRequireWildcard(_analyticsHelper);

var _Radio = require('@material-ui/core/Radio');

var _Radio2 = _interopRequireDefault(_Radio);

var _TxnSubtypeConstant = require('../../../Constants/TxnSubtypeConstant');

var _TxnSubtypeConstant2 = _interopRequireDefault(_TxnSubtypeConstant);

var _TxnTypeConstant = require('../../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _FTUHelper = require('../../../Utilities/FTUHelper');

var _FTUHelper2 = _interopRequireDefault(_FTUHelper);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var theme = (0, _themes2.default)();

var styles = function styles() {
	return {
		nav: {
			background: '#fff',
			height: 50,
			padding: 10,
			minWidth: '100%',
			zIndex: 1,
			borderBottom: '1px solid #ddd'
		},
		cashInCashOutContainer: {
			borderBottom: 0
		},
		cashInCashOUtTitle: {
			paddingLeft: '15px !important'
		},
		transactionTitle: {
			color: '#1C2A3D',
			paddingLeft: 25,
			fontWeight: 'bold',
			fontSize: 18
		},
		cashSale: {
			fontSize: 12,
			color: '#1C2A3D',
			marginLeft: 25,
			fontFamily: 'RobotoMedium'
		},
		firmContainer: {},
		firmImage: {
			backgroundColor: '#C0DFFE',
			width: 30,
			height: 30
		},
		firmName: {
			color: '#222A3F',
			fontFamily: 'robotomedium',
			fontSize: 12,
			marginLeft: 8,
			cursor: 'pointer'
		},
		firmDropdownPaper: {
			maxHeight: 215,
			position: 'absolute'
		},
		actionIcons: {
			color: 'rgba(34,58,82,0.54)',
			marginLeft: 40,
			cursor: 'pointer'
		},
		blue: {
			color: '#1789FC'
		},
		cashSaleBar: {
			backgroundColor: '#1789FC'
		},
		positionRelative: {
			position: 'relative'
		},
		redDot: {
			color: 'red',
			borderRadius: 20,
			position: 'absolute',
			background: 'red',
			top: -4,
			height: 6,
			width: 6,
			right: 0
		}
	};
};

var SalePurchaseNav = function (_PureComponent) {
	(0, _inherits3.default)(SalePurchaseNav, _PureComponent);

	function SalePurchaseNav(props) {
		(0, _classCallCheck3.default)(this, SalePurchaseNav);

		var _this = (0, _possibleConstructorReturn3.default)(this, (SalePurchaseNav.__proto__ || (0, _getPrototypeOf2.default)(SalePurchaseNav)).call(this, props));

		_this.state = {
			anchorEl: null,
			settings: null
		};
		_this.isFirstSaleTxnCreated = _FTUHelper2.default.isFirstSaleTxnCreated();
		_this.handleClick = _this.handleClick.bind(_this);
		_this.handleClose = _this.handleClose.bind(_this);
		_this.changeFirm = _this.changeFirm.bind(_this);
		_this.openCalculator = _this.openCalculator.bind(_this);
		_this.openSettings = _this.openSettings.bind(_this);
		_this.showRedDot = localStorage.getItem('SalePurchaseSettingIconClicked') != '1';
		return _this;
	}

	(0, _createClass3.default)(SalePurchaseNav, [{
		key: 'handleClick',
		value: function handleClick(event) {
			this.setState({ anchorEl: event.currentTarget });
		}
	}, {
		key: 'handleClose',
		value: function handleClose(e, reason) {
			this.setState({ anchorEl: null });
		}
	}, {
		key: 'changeFirm',
		value: function changeFirm(e, reason) {
			this.setState({ anchorEl: null });
			this.props.changeFirm(reason);
		}
	}, {
		key: 'openCalculator',
		value: function openCalculator() {
			MyAnalytics.pushEvent('Open Calculator', {
				Action: 'Click'
			});

			var _require = require('electron'),
			    shell = _require.shell;

			shell.openItem('calc');
		}
	}, {
		key: 'openSettings',
		value: function openSettings() {
			localStorage.setItem('SalePurchaseSettingIconClicked', 1);
			if (canCloseDialogue()) {
				var Settings = require('../Settings/Settings').default;
				var mountComponent = require('../MountComponent').default;
				mountComponent(Settings, document.querySelector('#frameDiv'), {
					activeIndex: 2
				});
				MyAnalytics.pushEvent('Settings.html', {
					Action: 'Click'
				});
				unmountReactComponent(document.querySelector('#salePurchaseContainer'));
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var _this2 = this;

			var _props = this.props,
			    classes = _props.classes,
			    txnType = _props.txnType,
			    firmList = _props.firmList,
			    firm = _props.firm,
			    cashSale = _props.cashSale,
			    changeCashSale = _props.changeCashSale,
			    isReverseCharge = _props.isReverseCharge,
			    changeReverseCharge = _props.changeReverseCharge,
			    title = _props.title,
			    invoiceType = _props.invoiceType,
			    fieldsVisibility = _props.fieldsVisibility,
			    changeField = _props.changeField,
			    onClose = _props.onClose,
			    isEdit = _props.isEdit;
			var anchorEl = this.state.anchorEl;

			var open = Boolean(anchorEl);
			var changeInvoiceType = changeField('invoiceType');
			return _react2.default.createElement(
				_styles.MuiThemeProvider,
				{ theme: theme },
				_react2.default.createElement(
					'div',
					{
						className: (0, _classnames2.default)(classes.nav, txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHOUT ? classes.cashInCashOutContainer : '', 'd-flex', 'justify-content-between', 'align-items-center')
					},
					_react2.default.createElement(
						'div',
						{ className: 'd-flex align-items-center' },
						_react2.default.createElement(
							'div',
							{
								className: (0, _classnames2.default)(classes.transactionTitle, txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHOUT ? classes.cashInCashOUtTitle : '')
							},
							title
						),
						txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE && !isEdit && _react2.default.createElement(
							'div',
							{
								className: (0, _classnames2.default)(classes.cashSale, 'd-flex', 'align-items-center')
							},
							_react2.default.createElement(
								'span',
								{ className: !cashSale ? classes.blue : '' },
								'Credit'
							),
							_react2.default.createElement(_Switch2.default, {
								classes: {
									colorPrimary: classes.blue,
									track: classes.cashSaleBar
								},
								checked: cashSale,
								value: 'Credit',
								onChange: changeCashSale,
								color: 'primary'
							}),
							_react2.default.createElement(
								'span',
								{ className: cashSale ? classes.blue : '' },
								'Cash'
							)
						),
						fieldsVisibility.reverseCharge && _react2.default.createElement(
							'div',
							{
								className: (0, _classnames2.default)(classes.cashSale, 'd-flex', 'align-items-center')
							},
							_react2.default.createElement(
								'span',
								null,
								'Reverse Charge'
							),
							_react2.default.createElement(_Switch2.default, {
								checked: isReverseCharge,
								value: 'Reverse Charge',
								onChange: changeReverseCharge,
								color: 'primary'
							})
						)
					),
					_react2.default.createElement(
						'div',
						{ className: (0, _classnames2.default)(classes.firmContainer, 'd-flex') },
						_react2.default.createElement(
							'div',
							{
								className: (0, _classnames2.default)(classes.firmImageContainer, 'd-flex', 'align-items-center')
							},
							firmList.length > 1 && firm && firm.getFirmImagePath() && _react2.default.createElement(_Avatar2.default, {
								className: classes.firmImage,
								src: 'data:image/gif;base64,' + firm.getFirmImagePath()
							}),
							firmList.length > 1 && firm && !firm.getFirmImagePath() && _react2.default.createElement(
								_Avatar2.default,
								{ className: classes.firmImage },
								firm.getFirmName()[0].toUpperCase()
							)
						),
						_react2.default.createElement(
							'div',
							{ className: 'd-flex align-items-center' },
							_react2.default.createElement(
								'div',
								{ className: 'd-flex align-items-center', onClick: firmList.length > 1 ? this.handleClick : undefined },
								firmList.length > 1 && _react2.default.createElement(
									_react.Fragment,
									null,
									_react2.default.createElement(
										'span',
										{ className: classes.firmName },
										firm && firm.getFirmName()
									),
									_react2.default.createElement(
										_IconButton2.default,
										{
											'aria-label': 'More',
											className: classes.firmDropdownIcon,
											'aria-owns': open ? 'firm-dropdown' : undefined,
											'aria-haspopup': 'true'
										},
										_react2.default.createElement(_ExpandMore2.default, null)
									)
								)
							),
							firmList.length > 1 && _react2.default.createElement(
								_Menu2.default,
								{
									id: 'firm-dropdown',
									anchorEl: anchorEl,
									open: open,
									onClose: this.handleClose,
									PaperProps: {
										className: classes.firmDropdownPaper
									}
								},
								firmList.map(function (firmObj) {
									return _react2.default.createElement(
										_MenuItem2.default,
										{
											key: firmObj.getFirmId(),
											onClick: function onClick(e) {
												return _this2.changeFirm(e, firmObj);
											}
										},
										firmObj.getFirmName()
									);
								})
							)
						)
					),
					fieldsVisibility.invoiceType && _react2.default.createElement(
						'div',
						null,
						_react2.default.createElement(_Radio2.default, {
							checked: _TxnSubtypeConstant2.default.invoice == invoiceType,
							value: _TxnSubtypeConstant2.default.invoice,
							onChange: changeInvoiceType,
							name: 'invoiceType'
						}),
						' ',
						_TxnSubtypeConstant2.default.getTypeName(_TxnSubtypeConstant2.default.invoice),
						_react2.default.createElement(_Radio2.default, {
							checked: _TxnSubtypeConstant2.default['tax invoice'] == invoiceType,
							value: _TxnSubtypeConstant2.default['tax invoice'],
							onChange: changeInvoiceType,
							name: 'invoiceType'
						}),
						' ',
						_TxnSubtypeConstant2.default.getTypeName(_TxnSubtypeConstant2.default['tax invoice'])
					),
					_react2.default.createElement(
						'div',
						{ className: 'd-flex' },
						_react2.default.createElement(
							'div',
							null,
							_react2.default.createElement(_calculatorIcon2.default, {
								onClick: this.openCalculator,
								className: classes.actionIcons
							})
						),
						this.isFirstSaleTxnCreated && _react2.default.createElement(
							'div',
							{ className: classes.positionRelative },
							_react2.default.createElement(_SettingsOutlined2.default, {
								onClick: this.openSettings,
								className: classes.actionIcons
							}),
							this.showRedDot && _react2.default.createElement('div', { className: classes.redDot })
						),
						_react2.default.createElement(
							'div',
							null,
							_react2.default.createElement(_Cancel2.default, { onClick: onClose, className: classes.actionIcons })
						)
					)
				)
			);
		}
	}]);
	return SalePurchaseNav;
}(_react.PureComponent);

SalePurchaseNav.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	txnType: _propTypes2.default.number.isRequired,
	firmList: _propTypes2.default.array.isRequired,
	changeFirm: _propTypes2.default.func.isRequired,
	changeCashSale: _propTypes2.default.func.isRequired,
	firm: _propTypes2.default.object,
	cashSale: _propTypes2.default.bool,
	isReverseCharge: _propTypes2.default.bool,
	isEdit: _propTypes2.default.bool,
	changeReverseCharge: _propTypes2.default.func,
	title: _propTypes2.default.string,
	invoiceType: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
	fieldsVisibility: _propTypes2.default.object,
	changeField: _propTypes2.default.func,
	onClose: _propTypes2.default.func
};

exports.default = (0, _styles.withStyles)(styles)(SalePurchaseNav);