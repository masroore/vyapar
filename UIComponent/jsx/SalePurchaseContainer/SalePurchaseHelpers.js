Object.defineProperty(exports, "__esModule", {
	value: true
});

var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

var _assign = require('babel-runtime/core-js/object/assign');

var _assign2 = _interopRequireDefault(_assign);

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

exports.changeUnit = changeUnit;
exports.isITCEligible = isITCEligible;
exports.changeTaxFields = changeTaxFields;
exports.changeDiscountFields = changeDiscountFields;
exports.getTaxInclusivePrice = getTaxInclusivePrice;
exports.displayInclusiveOfTaxDropDown = displayInclusiveOfTaxDropDown;
exports.getTaxInclusiveValue = getTaxInclusiveValue;
exports.changeTotalDiscountPercent = changeTotalDiscountPercent;
exports.changeTotalDiscountAmount = changeTotalDiscountAmount;
exports.changeTotalTaxPercent = changeTotalTaxPercent;
exports.changeTotalAndBalWithDiscountView = changeTotalAndBalWithDiscountView;
exports.calculateCurrentTransactionAmount = calculateCurrentTransactionAmount;
exports.calculateRoundOffAmount = calculateRoundOffAmount;
exports.calculateAmount = calculateAmount;
exports.changeAmount = changeAmount;
exports.updateLineItemFields = updateLineItemFields;
exports.changeCESS = changeCESS;
exports.getCorrespondingIdOfTaxRate = getCorrespondingIdOfTaxRate;
exports.getLineItemTaxDropdownList = getLineItemTaxDropdownList;
exports.getUnitDropdownOptions = getUnitDropdownOptions;
exports.getMaxInvoiceNumber = getMaxInvoiceNumber;
exports.getInvoicePrefixOptions = getInvoicePrefixOptions;
exports.getPaymentTermsOptions = getPaymentTermsOptions;
exports.getDueDate = getDueDate;
exports.getExtraFields = getExtraFields;
exports.getCustomFields = getCustomFields;
exports.getAdditionalChargeFields = getAdditionalChargeFields;
exports.getITCDropdownOptions = getITCDropdownOptions;
exports.getItemStockTrackingList = getItemStockTrackingList;
exports.addItemStockTrackingInLineItem = addItemStockTrackingInLineItem;
exports.changeLineItem = changeLineItem;
exports.selectImage = selectImage;
exports.getUniqueKey = getUniqueKey;
exports.getTransactionsForB2BLinking = getTransactionsForB2BLinking;
exports.getTransactionSettings = getTransactionSettings;
exports.getLineItemSettings = getLineItemSettings;
exports.additionalAdjustmentForReceivePayment = additionalAdjustmentForReceivePayment;
exports.saveNewName = saveNewName;

var _SettingCache = require('../../../Cache/SettingCache');

var _SettingCache2 = _interopRequireDefault(_SettingCache);

var _TaxCodeCache = require('../../../Cache/TaxCodeCache');

var _TaxCodeCache2 = _interopRequireDefault(_TaxCodeCache);

var _ItemUnitMappingCache = require('../../../Cache/ItemUnitMappingCache');

var _ItemUnitMappingCache2 = _interopRequireDefault(_ItemUnitMappingCache);

var _PaymentTermCache = require('../../../Cache/PaymentTermCache');

var _PaymentTermCache2 = _interopRequireDefault(_PaymentTermCache);

var _UDFCache = require('../../../Cache/UDFCache');

var _UDFCache2 = _interopRequireDefault(_UDFCache);

var _CustomFieldsCache = require('../../../Cache/CustomFieldsCache');

var _CustomFieldsCache2 = _interopRequireDefault(_CustomFieldsCache);

var _ItemUnitCache = require('../../../Cache/ItemUnitCache');

var _ItemUnitCache2 = _interopRequireDefault(_ItemUnitCache);

var _ItemCache = require('../../../Cache/ItemCache');

var _ItemCache2 = _interopRequireDefault(_ItemCache);

var _ItemStockTracking = require('../../../BizLogic/ItemStockTracking');

var _ItemStockTracking2 = _interopRequireDefault(_ItemStockTracking);

var _DataLoader = require('../../../DBManager/DataLoader');

var _DataLoader2 = _interopRequireDefault(_DataLoader);

var _TaxCodeConstants = require('../../../Constants/TaxCodeConstants');

var _TaxCodeConstants2 = _interopRequireDefault(_TaxCodeConstants);

var _TxnITCConstants = require('../../../Constants/TxnITCConstants');

var _TxnITCConstants2 = _interopRequireDefault(_TxnITCConstants);

var _TxnTypeConstant = require('../../../Constants/TxnTypeConstant');

var _TxnTypeConstant2 = _interopRequireDefault(_TxnTypeConstant);

var _ItemType = require('../../../Constants/ItemType');

var _ItemType2 = _interopRequireDefault(_ItemType);

var _ErrorCode = require('../../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

var _MyDouble = require('../../../Utilities/MyDouble');

var _MyDouble2 = _interopRequireDefault(_MyDouble);

var _Queries = require('../../../Constants/Queries');

var Queries = _interopRequireWildcard(_Queries);

var _UDFFieldsConstant = require('../../../Constants/UDFFieldsConstant');

var _UDFFieldsConstant2 = _interopRequireDefault(_UDFFieldsConstant);

var _MyDate = require('../../../Utilities/MyDate');

var MyDate = _interopRequireWildcard(_MyDate);

var _NameType = require('../../../Constants/NameType');

var NameType = _interopRequireWildcard(_NameType);

var _NameCustomerType = require('../../../Constants/NameCustomerType');

var NameCustomerType = _interopRequireWildcard(_NameCustomerType);

var _GSTHelper = require('../../../Utilities/GSTHelper');

var _GSTHelper2 = _interopRequireDefault(_GSTHelper);

var _GSTRReportHelper = require('../../../UIControllers/GSTRReportHelper');

var _GSTRReportHelper2 = _interopRequireDefault(_GSTRReportHelper);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var settingCache = new _SettingCache2.default();

function changeUnit(lineItem, unitInfo) {
	var _convertLineItemField = convertLineItemFieldsToNumber(lineItem),
	    priceUnit = _convertLineItemField.priceUnit,
	    cess = _convertLineItemField.cess;

	var itemUnitMappingCache = new _ItemUnitMappingCache2.default();
	var mappingObj = itemUnitMappingCache.getItemUnitMapping(unitInfo.mappingId);
	var rate = _MyDouble2.default.convertStringToDouble(mappingObj.conversionRate);
	var itemPrice = 0;
	var adCess = 0;

	if (unitInfo.type === 'base') {
		itemPrice = priceUnit * rate;
		adCess = cess * rate;
	} else {
		itemPrice = priceUnit / rate;
		adCess = cess / rate;
	}
	return {
		itemPrice: _MyDouble2.default.getBalanceAmountWithDecimal(itemPrice),
		adCess: _MyDouble2.default.getBalanceAmountWithDecimal(adCess)
	};
}

function isITCEligible(taxPercent, txnType) {
	if (!_MyDouble2.default.convertStringToDouble(taxPercent)) return false;
	var taxCodeCache = new _TaxCodeCache2.default();
	var taxObject = taxCodeCache.getTaxCodeObjectById(taxPercent);
	var taxRateType = taxObject.getTaxRateType();
	if (taxRateType != _TaxCodeConstants2.default.OTHER && taxRateType != _TaxCodeConstants2.default.Exempted && (txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN) && settingCache.getGSTEnabled()) {
		return true;
	} else {
		return false;
	}
}

function changeTaxFields(lineItem, txnType, taxInclusive) {
	var shouldCalculateDiscount = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;

	var _convertLineItemField2 = convertLineItemFieldsToNumber(lineItem),
	    priceUnit = _convertLineItemField2.priceUnit,
	    qty = _convertLineItemField2.qty,
	    taxPercent = _convertLineItemField2.taxPercent;

	if (taxInclusive && _ItemType2.default.ITEM_TXN_TAX_INCLUSIVE == taxInclusive && shouldCalculateDiscount) {
		changeDiscountFields(lineItem, false, txnType, taxInclusive, false);
	}

	var discountAmount = _MyDouble2.default.convertStringToDouble(lineItem.discountAmount);

	var exclusivePriceUnit = getTaxInclusivePrice(priceUnit, lineItem.taxPercent, taxInclusive);
	var taxCodeCache = new _TaxCodeCache2.default();
	var taxObject = taxCodeCache.getTaxCodeObjectById(taxPercent);
	var taxPercentRate = taxObject && taxObject.getTaxRate() || 0;
	if (qty && priceUnit) {
		var tAmount = _MyDouble2.default.convertStringToDouble(taxPercentRate) / 100 * (exclusivePriceUnit * qty - discountAmount);
		lineItem.taxAmount = _MyDouble2.default.getBalanceAmountWithDecimal(tAmount);
	} else {
		// lineItem.taxPercent = 0;
		lineItem.taxAmount = 0;
	}

	if (taxPercent) {
		var taxRateType = taxObject.getTaxRateType();
		if (taxRateType != _TaxCodeConstants2.default.OTHER && taxRateType != _TaxCodeConstants2.default.Exempted && (txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN) && settingCache.getGSTEnabled()) {
			lineItem.ITCValue = _TxnITCConstants2.default.ITC_ELIGIBLE;
			lineItem.showITCEligible = true;
		} else {
			lineItem.ITCValue = _TxnITCConstants2.default.ITC_ELIGIBLE;
			lineItem.showITCEligible = false;
		}
	} else {
		lineItem.showITCEligible = false;
	}

	return calculateAmount(lineItem, taxInclusive);
}

function changeDiscountFields(lineItem) {
	var discountAmountChanged = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
	var txnType = arguments[2];
	var taxInclusive = arguments[3];
	var calculateTax = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : true;

	var _convertLineItemField3 = convertLineItemFieldsToNumber(lineItem),
	    priceUnit = _convertLineItemField3.priceUnit,
	    qty = _convertLineItemField3.qty,
	    discountAmount = _convertLineItemField3.discountAmount,
	    discountPercent = _convertLineItemField3.discountPercent;

	if (priceUnit && qty) {
		var exclusivePriceUnit = getTaxInclusivePrice(priceUnit, lineItem.taxPercent, taxInclusive);
		if (discountAmountChanged) {
			var newDiscountPercent = discountAmount * 100 / (exclusivePriceUnit * qty);
			lineItem.discountPercent = _MyDouble2.default.getPercentageWithDecimal(newDiscountPercent);
		} else {
			var newDiscountAmount = _MyDouble2.default.convertStringToDouble(discountPercent) / 100 * (exclusivePriceUnit * qty);
			lineItem.discountAmount = _MyDouble2.default.getBalanceAmountWithDecimal(newDiscountAmount);
		}
	} else {
		lineItem.discountPercent = 0;
		lineItem.discountAmount = 0;
	}
	if (!calculateTax) {
		return lineItem;
	}
	return changeTaxFields(lineItem, txnType, taxInclusive, false);
}

function getTaxInclusivePrice(price, taxPercentId, inclusive) {
	var inclusivePrice = price;
	if (inclusivePrice && _ItemType2.default.ITEM_TXN_TAX_INCLUSIVE == inclusive) {
		var taxCodeCache = new _TaxCodeCache2.default();
		var taxPercentRate = _MyDouble2.default.getPercentageWithDecimal(taxCodeCache.getRateForTaxId(taxPercentId));
		inclusivePrice = _MyDouble2.default.convertStringToDouble(inclusivePrice) * 100 / (_MyDouble2.default.convertStringToDouble(taxPercentRate) + 100);
	}
	return inclusivePrice;
}

function displayInclusiveOfTaxDropDown(txnType, taxInclusive) {
	var partyLogic = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
	var txnId = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : '';

	var visible = true;
	var taxTypeTxnArray = [_TxnTypeConstant2.default.TXN_TYPE_SALE, _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER, _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN, _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE, _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN, _TxnTypeConstant2.default.TXN_TYPE_PURCHASE, _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER, _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN];

	if (taxTypeTxnArray.includes(txnType) && !_GSTHelper2.default.isCompositeTxn(txnType, partyLogic)) {
		if (settingCache.isItemwiseTaxEnabled() && settingCache.isInclusiveExclusiveTaxEnabledForTransactions() || settingCache.isItemwiseTaxEnabled() && !settingCache.isInclusiveExclusiveTaxEnabledForTransactions() && txnId && taxInclusive === _ItemType2.default.ITEM_TXN_TAX_INCLUSIVE || !settingCache.isItemwiseTaxEnabled() && txnId && taxInclusive == _ItemType2.default.ITEM_TXN_TAX_INCLUSIVE) {
			visible = true;
		} else {
			visible = false;
		}
	} else {
		visible = false;
	}
	return visible;
}

function getTaxInclusiveValue(txnType) {
	switch (txnType) {
		case _TxnTypeConstant2.default.TXN_TYPE_SALE:
		case _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN:
		case _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER:
		case _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE:
			return settingCache.getTaxTypeOnOutWardTxn();
		case _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN:
		case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE:
		case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER:
		case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN:
			return settingCache.getTaxTypeOnInwardTxn();
	}
	return _ItemType2.default.ITEM_TXN_TAX_EXCLUSIVE;
}

function changeTotalDiscountPercent(subTotal, discountPercent) {
	var discountAmt = subTotal * _MyDouble2.default.convertStringToDouble(discountPercent) / 100;
	return _MyDouble2.default.getBalanceAmountWithDecimal(discountAmt);
	// changeTotalTaxPercent();
}

function changeTotalDiscountAmount(subtotal, discountAmount) {
	if (subtotal > 0) {
		var discountPercent = discountAmount / subtotal * 100;
		if (isNaN(discountPercent)) {
			discountPercent = 0;
		}
		return _MyDouble2.default.getPercentageWithDecimal(discountPercent);
	} else {
		return 0;
	}
	// changeTotalTaxPercent();
}

function changeTotalTaxPercent(txnType, subTotal, discountAmt, taxPercent) {
	var taxCodeCache = new _TaxCodeCache2.default();
	var transaction = {
		ITCVisibility: false,
		taxAmount: 0
	};

	if (_MyDouble2.default.convertStringToDouble(taxPercent)) {
		var taxCodeObj = taxCodeCache.getTaxCodeObjectById(taxPercent);
		var taxRateType = taxCodeObj.getTaxRateType();
		if (taxRateType != _TaxCodeConstants2.default.OTHER && taxRateType != _TaxCodeConstants2.default.Exempted && (txnType == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE || txnType == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN) && settingCache.getGSTEnabled()) {
			transaction.ITCVisibility = true;
		} else {
			transaction.ITCVisibility = false;
		}
	}

	var taxPercentRate = taxCodeCache.getRateForTaxId(taxPercent);
	var taxAmt = 0;

	if (_MyDouble2.default.convertStringToDouble(discountAmt) && _MyDouble2.default.convertStringToDouble(taxPercentRate)) {
		taxAmt = (subTotal - _MyDouble2.default.convertStringToDouble(discountAmt)) * _MyDouble2.default.convertStringToDouble(taxPercentRate) / 100;
	} else {
		taxAmt = subTotal * _MyDouble2.default.convertStringToDouble(taxPercentRate) / 100;
	}
	transaction.taxAmount = _MyDouble2.default.getBalanceAmountWithDecimal(taxAmt);
	return transaction;

	// changeTotalAndBalWithDiscountView();
}

function changeTotalAndBalWithDiscountView(_ref) {
	var subTotal = _ref.subTotal,
	    discountAmount = _ref.discountAmount,
	    discountPercent = _ref.discountPercent,
	    taxAmount = _ref.taxAmount,
	    roundOff = _ref.roundOff,
	    roundOffAmount = _ref.roundOffAmount,
	    isReverseCharge = _ref.isReverseCharge,
	    reverseChargeAmount = _ref.reverseChargeAmount,
	    _ref$additionalCharge = _ref.additionalCharges,
	    additionalCharges = _ref$additionalCharge === undefined ? [] : _ref$additionalCharge,
	    cashSale = _ref.cashSale,
	    received = _ref.received,
	    _ref$roundOffChanged = _ref.roundOffChanged,
	    roundOffChanged = _ref$roundOffChanged === undefined ? null : _ref$roundOffChanged,
	    _ref$totalAmount = _ref.totalAmount,
	    totalAmount = _ref$totalAmount === undefined ? 0 : _ref$totalAmount;

	var total = subTotal === 0 ? totalAmount : subTotal;
	var payable = 0;
	var balance = 0;

	discountAmount = discountAmount > 0 ? discountAmount : 0.0;
	taxAmount = taxAmount > 0 ? taxAmount : 0.0;

	if (total) {
		total = _MyDouble2.default.convertStringToDouble(total) - _MyDouble2.default.convertStringToDouble(discountAmount) + _MyDouble2.default.convertStringToDouble(taxAmount);
	}
	total = additionalCharges.reduce(function (acc, charge) {
		return acc + _MyDouble2.default.convertStringToDouble(charge.value);
	}, total);

	if (roundOff) {
		roundOffAmount = roundOffChanged ? roundOffAmount : calculateRoundOffAmount(total, isReverseCharge, reverseChargeAmount);
	} else {
		roundOffAmount = 0;
	}

	if (isReverseCharge) {
		payable = total - reverseChargeAmount + roundOffAmount;
		balance = payable - received;
	} else {
		total += roundOffAmount;
		if (cashSale) {
			received = _MyDouble2.default.getBalanceAmountWithDecimal(total);
			balance = 0;
		} else {
			balance = total - received;
		}
	}
	return {
		roundOffAmount: roundOffAmount,
		total: _MyDouble2.default.getBalanceAmountWithDecimal(total),
		payable: _MyDouble2.default.getBalanceAmountWithDecimal(payable),
		received: received,
		balance: _MyDouble2.default.getBalanceAmountWithDecimal(balance)
	};
}
function calculateCurrentTransactionAmount(txnType, transaction) {
	var updateForCashIn = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
	var total = transaction.total,
	    received = transaction.received,
	    receivedLater = transaction.receivedLater,
	    isReverseCharge = transaction.isReverseCharge,
	    payable = transaction.payable,
	    discountForCash = transaction.discountForCash;
	// transaction current balance calculation

	var totalAmount = isReverseCharge ? _MyDouble2.default.convertStringToDouble(payable) : _MyDouble2.default.convertStringToDouble(total);
	var cashAmount = _MyDouble2.default.convertStringToDouble(received);
	var receivedLaterAmount = _MyDouble2.default.convertStringToDouble(receivedLater);
	var discountTotalAmount = _MyDouble2.default.convertStringToDouble(discountForCash);

	if (txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT) {
		cashAmount = 0;
	}
	var txnCurrentBalance = _MyDouble2.default.convertStringToDouble(totalAmount) - _MyDouble2.default.convertStringToDouble(cashAmount) - _MyDouble2.default.convertStringToDouble(receivedLaterAmount);

	if (txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHIN || txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT) {
		txnCurrentBalance = txnCurrentBalance + discountTotalAmount;

		if (_MyDouble2.default.convertStringToDouble(totalAmount) == 0) {
			//                        txnCurrentBalance = 0;
			if (!updateForCashIn) {
				transaction.total = _MyDouble2.default.getAmountWithDecimal(receivedLaterAmount - discountTotalAmount);
			}
		}
	}

	transaction.currentTxnBalance = _MyDouble2.default.getBalanceAmountWithDecimal(txnCurrentBalance);

	transaction.totalReceivedAmount = _MyDouble2.default.convertStringToDouble(received) + _MyDouble2.default.convertStringToDouble(receivedLater);

	return transaction;
}

function calculateRoundOffAmount(total, isReverseCharge) {
	var reverseChargeAmount = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
	var receivedAmount = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;

	var newTotal = 0;
	var roundOffValue = 0;
	if (isReverseCharge) {
		newTotal = _MyDouble2.default.getRoundedOffTransactionAmount(total - reverseChargeAmount);
		roundOffValue = newTotal - (total - reverseChargeAmount);
	} else {
		newTotal = _MyDouble2.default.getRoundedOffTransactionAmount(total);
		roundOffValue = newTotal - total;
	}
	return _MyDouble2.default.getAmountWithDecimal(roundOffValue);
}

function calculateAmount(lineItem, taxInclusive) {
	var _convertLineItemField4 = convertLineItemFieldsToNumber(lineItem),
	    priceUnit = _convertLineItemField4.priceUnit,
	    qty = _convertLineItemField4.qty,
	    discountAmount = _convertLineItemField4.discountAmount,
	    taxAmount = _convertLineItemField4.taxAmount,
	    cess = _convertLineItemField4.cess;

	var exclusivePriceUnit = getTaxInclusivePrice(priceUnit, lineItem.taxPercent, taxInclusive);
	lineItem.amount = _MyDouble2.default.getBalanceAmountWithDecimal(exclusivePriceUnit * qty - discountAmount + taxAmount + cess);
	return lineItem;
}

function changeAmount(lineItem, taxInclusive) {
	var _convertLineItemField5 = convertLineItemFieldsToNumber(lineItem),
	    qty = _convertLineItemField5.qty,
	    cess = _convertLineItemField5.cess,
	    taxPercent = _convertLineItemField5.taxPercent,
	    discountAmount = _convertLineItemField5.discountAmount,
	    discountPercent = _convertLineItemField5.discountPercent,
	    taxAmount = _convertLineItemField5.taxAmount,
	    priceUnit = _convertLineItemField5.priceUnit,
	    amount = _convertLineItemField5.amount;

	if (!qty) {
		return (0, _extends3.default)({}, lineItem, { amount: 0 });
	}
	var taxCodeCache = new _TaxCodeCache2.default();
	taxPercent = _MyDouble2.default.convertStringToDouble(taxCodeCache.getRateForTaxId(taxPercent));
	discountPercent = discountPercent / 100;
	taxPercent = taxPercent / 100;

	var newItemPrice = 0;
	if (taxInclusive === _ItemType2.default.ITEM_TXN_TAX_INCLUSIVE) {
		newItemPrice = (amount - cess) / (qty * (1 - discountPercent));
	} else {
		newItemPrice = (amount - cess) / (qty * (1 - discountPercent) * (1 + taxPercent));
	}

	if (_MyDouble2.default.getAmountWithDecimal(newItemPrice) > -1) {
		priceUnit = _MyDouble2.default.getAmountWithDecimal(newItemPrice);
		if (taxInclusive === _ItemType2.default.ITEM_TXN_TAX_INCLUSIVE) {
			newItemPrice = _MyDouble2.default.getAmountWithDecimal(getTaxInclusivePrice(newItemPrice, lineItem.taxPercent, taxInclusive));
		}
		var newDiscountAmount = newItemPrice * qty * discountPercent;
		var newTaxAmount = (newItemPrice * qty - newDiscountAmount) * taxPercent;
		discountAmount = _MyDouble2.default.getAmountWithDecimal(newDiscountAmount);
		taxAmount = _MyDouble2.default.getAmountWithDecimal(newTaxAmount);
	}
	return (0, _extends3.default)({}, lineItem, {
		amount: amount,
		discountAmount: discountAmount,
		taxAmount: taxAmount,
		priceUnit: priceUnit
	});
}

function updateLineItemFields(lineItem, txnType, taxInclusive) {
	var _convertLineItemField6 = convertLineItemFieldsToNumber(lineItem),
	    priceUnit = _convertLineItemField6.priceUnit,
	    qty = _convertLineItemField6.qty,
	    item = _convertLineItemField6.item;

	if (priceUnit === 0) {
		lineItem.discountPercent = 0;
	}
	lineItem.amount = priceUnit * qty;

	if (settingCache.getAdditionalCessEnabled()) {
		var itemLogic = item && item.item ? item.item : null;
		var additionalCESSAmount = itemLogic && itemLogic.getItemAdditionalCESS() || 0;
		var cess = _MyDouble2.default.convertStringToDouble(qty) * _MyDouble2.default.convertStringToDouble(additionalCESSAmount);
		lineItem.cess = _MyDouble2.default.getBalanceAmountWithDecimal(cess);
	}
	return changeDiscountFields(lineItem, false, txnType, taxInclusive);
}

function changeCESS(lineItem, taxInclusive) {
	var _convertLineItemField7 = convertLineItemFieldsToNumber(lineItem),
	    priceUnit = _convertLineItemField7.priceUnit,
	    qty = _convertLineItemField7.qty,
	    discountAmount = _convertLineItemField7.discountAmount,
	    taxAmount = _convertLineItemField7.taxAmount,
	    cess = _convertLineItemField7.cess;

	var exclusivePriceUnit = getTaxInclusivePrice(priceUnit, lineItem.taxPercent, taxInclusive);
	var totalAmount = exclusivePriceUnit * qty - discountAmount + taxAmount + cess;
	lineItem.amount = _MyDouble2.default.getBalanceAmountWithDecimal(totalAmount);
	return lineItem;
}

function getCorrespondingIdOfTaxRate(_ref2) {
	var taxId = _ref2.taxId,
	    txnType = _ref2.txnType,
	    partyLogic = _ref2.partyLogic,
	    firmLogic = _ref2.firmLogic,
	    placeOfSupply = _ref2.placeOfSupply;

	var taxCodeCache = new _TaxCodeCache2.default();
	return taxCodeCache.getTaxCodeBasedOnPlaceOfSupply(taxId, txnType, partyLogic, firmLogic, placeOfSupply);
}

function getLineItemTaxDropdownList(_ref3) {
	var txnType = _ref3.txnType,
	    txnId = _ref3.txnId,
	    firmLogic = _ref3.firmLogic,
	    placeOfSupply = _ref3.placeOfSupply,
	    itemId = _ref3.itemId,
	    partyLogic = _ref3.partyLogic,
	    _ref3$currentTaxCodeI = _ref3.currentTaxCodeId,
	    currentTaxCodeId = _ref3$currentTaxCodeI === undefined ? [] : _ref3$currentTaxCodeI,
	    oldTaxId = _ref3.oldTaxId;

	if (_GSTHelper2.default.isCompositeTxn(txnType, partyLogic)) {
		currentTaxCodeId.push(0);
	}

	var taxCodeIdsToBeIncluded = currentTaxCodeId;
	var taxCodeCache = new _TaxCodeCache2.default();
	var listOfTaxCodes = taxCodeCache.getTaxListForTransaction(txnType, partyLogic, firmLogic, itemId, placeOfSupply, taxCodeIdsToBeIncluded);

	var list = listOfTaxCodes.reduce(function (list, taxCode) {
		var value = taxCode.getTaxCodeId();
		var label = taxCode.getTaxCodeName();
		var rateType = taxCode.getTaxRateType();

		if (txnId) {
			list.push({ value: value, label: label });
		} else if (rateType == _TaxCodeConstants2.default.GSTFromDB || rateType == _TaxCodeConstants2.default.GST || rateType == _TaxCodeConstants2.default.OTHER || rateType == _TaxCodeConstants2.default.IGST || rateType == _TaxCodeConstants2.default.Exempted) {
			list.push({ value: value, label: label });
		}
		return list;
	}, []);return [{ value: 0, label: 'NONE' }].concat((0, _toConsumableArray3.default)(list));
}

function getUnitDropdownOptions(itemLogic) {
	var existing = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

	if (!itemLogic) {
		return [{
			value: 0,
			label: 'NONE'
		}];
	}
	var baseUnitId = '';
	var secondaryUnitId = '';
	var mappingId = 0;

	if (existing) {
		mappingId = existing.mappingId;
		if (!mappingId) {
			baseUnitId = existing.selectedId;
			mappingId = itemLogic && itemLogic.getUnitMappingId() || '';
			secondaryUnitId = itemLogic && itemLogic.getSecondaryUnitId() || '';
		} else {
			var itemUnitMappingCache = new _ItemUnitMappingCache2.default();
			var mappingObj = itemUnitMappingCache.getItemUnitMapping(mappingId);
			baseUnitId = mappingObj && mappingObj.getBaseUnitId() ? mappingObj.getBaseUnitId() : '';
			secondaryUnitId = mappingObj && mappingObj.getSecondaryUnitId() ? mappingObj.getSecondaryUnitId() : '';
		}
	} else {
		baseUnitId = itemLogic && itemLogic.getBaseUnitId() || '';
		mappingId = itemLogic && itemLogic.getUnitMappingId() || '';
		secondaryUnitId = itemLogic && itemLogic.getSecondaryUnitId() || '';
	}
	var unitDropdown = [];
	var itemUnitCache = new _ItemUnitCache2.default();
	var baseUnitLabel = itemUnitCache.getItemUnitShortNameById(baseUnitId);
	var secUnitLabel = itemUnitCache.getItemUnitShortNameById(secondaryUnitId);

	/* generate unit dropdown  start */
	if (baseUnitId) {
		unitDropdown.push({
			label: baseUnitLabel,
			value: baseUnitId,
			type: 'base',
			mappingId: mappingId
		});
		if (secondaryUnitId) {
			unitDropdown.push({
				label: secUnitLabel,
				value: secondaryUnitId,
				type: 'secondary',
				mappingId: mappingId
			});
		}
	} else {
		unitDropdown.push({
			value: 0,
			label: 'NONE'
		});
	}
	return unitDropdown;
}

function getMaxInvoiceNumber(firmId, txnType) {
	var prefix = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

	switch (txnType) {
		case _TxnTypeConstant2.default.TXN_TYPE_SALE:
		case _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE:
		case _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN:
		case _TxnTypeConstant2.default.TXN_TYPE_CASHIN:
		case _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER:
		case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER:
		case _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN:
			var dataLoader = new _DataLoader2.default();
			var idToAppend = dataLoader.getMaxRefNumber(firmId, txnType, prefix);
			return idToAppend ? _MyDouble2.default.convertStringToDouble(idToAppend) + 1 : 1;
	}
	return '';
}

function getInvoicePrefixOptions(firmId, txnType) {
	var prefix = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

	switch (txnType) {
		case _TxnTypeConstant2.default.TXN_TYPE_SALE:
		case _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE:
		case _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN:
		case _TxnTypeConstant2.default.TXN_TYPE_CASHIN:
		case _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER:
		case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER:
		case _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN:
			var dataLoader = new _DataLoader2.default();
			return dataLoader.getPrefixDetailsOnFirmId(firmId, txnType);
	}
	return [];
}

function getPaymentTermsOptions(txnType) {
	if (settingCache.isPaymentTermEnabled()) {
		if (txnType == _TxnTypeConstant2.default.TXN_TYPE_SALE || txnType == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE) {
			var paymentTermCache = new _PaymentTermCache2.default();
			return paymentTermCache.getAllPayamentTerms();
		}
	}
	return [];
}

function getDueDate(txnType) {
	var paymentTermId = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
	var invoiceDate = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : MyDate.getDate('d/m/y', new Date());

	if (!paymentTermId || !txnType) {
		return invoiceDate;
	}
	if (settingCache.isPaymentTermEnabled()) {
		if (txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE) {
			var paymentTermCache = new _PaymentTermCache2.default();
			var paymentTermObj = paymentTermCache.getPaymentTermObjectById(paymentTermId);
			var paymentTermDays = _MyDouble2.default.convertStringToDouble(paymentTermObj.getPaymentTermDays());
			var dueDate = MyDate.getDateObj(invoiceDate, 'dd/mm/yyyy', '/');
			if (dueDate && dueDate instanceof Date && dueDate != 'Invalid Date') {
				dueDate.setDate(dueDate.getDate() + paymentTermDays);
				return dueDate;
			} else {
				if (typeof invoiceDate === 'string') {
					return MyDate.getDateObj(invoiceDate, 'dd/mm/yyyy', '/');
				} else {
					return invoiceDate;
				}
			}
		}
	}
	if (typeof invoiceDate === 'string') {
		return MyDate.getDateObj(invoiceDate, 'dd/mm/yyyy', '/');
	}
	return invoiceDate;
}

function getExtraFields(fields, txnType, firmId, isConvert, oldTxnType) {
	var extraFields = [];
	var udfCache = new _UDFCache2.default();
	var txnExtraFields = udfCache.getTransactionFieldsMap();
	var firmExtraFields = txnExtraFields.get(_MyDouble2.default.convertStringToDouble(firmId)) || [];
	var allFields = [];
	firmExtraFields.forEach(function (field) {
		allFields.push({
			value: field.getUdfFieldValue() || '',
			udfModel: field
		});
		if (field.getUdfFieldStatus() == 1 && field.getUdfTxnType() == txnType) {
			extraFields.push({
				value: field.getUdfFieldValue() || '',
				udfModel: field
			});
		}
	});
	var newFields = [];
	if (fields && fields.length) {
		fields.forEach(function (fieldValueModel) {
			var udfModel = udfCache.getUdfModelByFirmAndFieldId(firmId, fieldValueModel.getUdfFieldId());
			if (!udfModel) {
				return false;
			}
			var displayValue = fieldValueModel.getDisplayValue(udfModel);
			// if (udfModel.getUdfFieldDataType() == udfConstants.DATA_TYPE_DATE) {
			// 	displayValue = MyDate.getDateObj(
			// 		fieldValueModel.getUdfFieldValue(),
			// 		'yyyy-mm-dd',
			// 		'-'
			// 	);
			// }

			if (oldTxnType === _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER && txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE || oldTxnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER && txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE || oldTxnType === _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN && txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE || oldTxnType === _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE && txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE) {
				displayValue = '';
			}
			var found = extraFields.find(function (field) {
				return udfModel.getUdfFieldNumber() == field.udfModel.getUdfFieldNumber();
			});
			if (found) {
				found.value = displayValue || '';
			} else {
				if (!isConvert) {
					var _found = allFields.find(function (field) {
						return udfModel.getUdfFieldNumber() == field.udfModel.getUdfFieldNumber();
					});
					newFields.push({
						value: displayValue || '',
						udfModel: _found.udfModel
					});
				}
			}
		});
	}
	return [].concat(extraFields, newFields);
}

function getCustomFields(customFields, txnType) {
	var fields = [];
	switch (txnType) {
		case _TxnTypeConstant2.default.TXN_TYPE_SALE:
		case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE:
		case _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN:
		case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN:
		case _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER:
		case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER:
		case _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN:
			try {
				var customFieldsCache = new _CustomFieldsCache2.default();
				if (customFields) {
					customFields = JSON.parse(customFields).transportation_details;
					customFields.forEach(function (entry) {
						var field = customFieldsCache.getCustomFieldById(entry.id);
						if (field.getCustomFieldVisibility() == 1 || entry.value) {
							fields.push({
								value: entry.value,
								id: field.getCustomFieldId(),
								label: field.getCustomFieldName()
							});
						}
					});
				}
				var getListOfDeliveryCustomFields = customFieldsCache.getListOfDeliveryCustomFields();
				return (0, _values2.default)(getListOfDeliveryCustomFields).reduce(function (all, field) {
					if (field.getCustomFieldVisibility() == 1) {
						var found = fields.find(function (f) {
							return f.id == field.getCustomFieldId();
						});
						if (found) {
							all.push(found);
						} else {
							all.push({
								value: '',
								id: field.getCustomFieldId(),
								label: field.getCustomFieldName()
							});
						}
					}
					return all;
				}, []);
			} catch (err) {
				logger.error(err);
			}
	}
	return fields;
}

function getAdditionalChargeFields(oldTxnObj, txnType) {
	var additionalCharges = [];
	var ac1Amount = void 0,
	    ac2Amount = void 0,
	    ac3Amount = void 0;
	if (oldTxnObj) {
		ac1Amount = _MyDouble2.default.getBalanceAmountWithDecimal(oldTxnObj.getAc1Amount());
		ac2Amount = _MyDouble2.default.getBalanceAmountWithDecimal(oldTxnObj.getAc2Amount());
		ac3Amount = _MyDouble2.default.getBalanceAmountWithDecimal(oldTxnObj.getAc3Amount());
	}
	if (settingCache.getItemEnabled() && (settingCache.isACEnabled() || ac1Amount || ac2Amount || ac3Amount) && txnType !== _TxnTypeConstant2.default.TXN_TYPE_OTHER_INCOME && txnType !== _TxnTypeConstant2.default.TXN_TYPE_EXPENSE && txnType !== _TxnTypeConstant2.default.TXN_TYPE_CASHOUT && txnType !== _TxnTypeConstant2.default.TXN_TYPE_CASHIN) {
		var dataLoader = new _DataLoader2.default();
		if (settingCache.isAC1Enabled() || ac1Amount) {
			additionalCharges.push({
				id: 1,
				label: dataLoader.getCurrentExtraChargesValue('1').toUpperCase(),
				value: ac1Amount || 0
			});
		}
		if (settingCache.isAC2Enabled() || ac2Amount) {
			additionalCharges.push({
				id: 2,
				label: dataLoader.getCurrentExtraChargesValue('2').toUpperCase(),
				value: ac2Amount || 0
			});
		}
		if (settingCache.isAC3Enabled() || ac3Amount) {
			additionalCharges.push({
				id: 3,
				label: dataLoader.getCurrentExtraChargesValue('3').toUpperCase(),
				value: ac3Amount || 0
			});
		}
	}
	return additionalCharges;
}

function getITCDropdownOptions() {
	return _TxnITCConstants2.default.getDropDownOptions();
}

function getItemStockTrackingList(_ref4) {
	var itemId = _ref4.itemId,
	    itemCode = _ref4.itemCode,
	    _ref4$checkSerialNumb = _ref4.checkSerialNumber,
	    checkSerialNumber = _ref4$checkSerialNumb === undefined ? true : _ref4$checkSerialNumb,
	    _ref4$checkType = _ref4.checkType,
	    checkType = _ref4$checkType === undefined ? false : _ref4$checkType,
	    _ref4$istType = _ref4.istType,
	    istType = _ref4$istType === undefined ? null : _ref4$istType,
	    nameId = _ref4.nameId,
	    txnType = _ref4.txnType;

	var newTxnType = 0;
	var newNameId = 0;
	var filterZeroAndNegativeQty = true;
	switch (txnType) {
		case _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN:
			newTxnType = _TxnTypeConstant2.default.TXN_TYPE_SALE;
			newNameId = nameId;
			filterZeroAndNegativeQty = false;
			break;
		case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN:
			newTxnType = _TxnTypeConstant2.default.TXN_TYPE_PURCHASE;
			newNameId = nameId;
			break;
		case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE:
		case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER:
			filterZeroAndNegativeQty = false;
			break;
	}
	var itemStockTracking = new _ItemStockTracking2.default();
	return itemStockTracking.getItemStockTrackingData(itemId, filterZeroAndNegativeQty, itemCode, checkSerialNumber, checkType, istType, newNameId, newTxnType);
}

function addItemStockTrackingInLineItem(defaultLineItem, selectedList) {
	var firmLogic = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
	var partyLogic = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
	var txnType = arguments[4];
	var txnId = arguments[5];
	var placeOfSupply = arguments[6];

	if (selectedList.length) {
		var mrpColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_MRP);
		var batchNumberColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_BATCH_NUMBER);
		var serialNumberColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_SERIAL_ITEM_NUMBER);
		var mfgDateColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_MANUFACTURING_DATE);
		var expDateColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_EXPIRY_DATE);
		var sizeColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_SIZE);
		var itemCache = new _ItemCache2.default();
		var StringConstant = require('../../../Constants/StringConstants');
		return selectedList.map(function (batchInfo) {
			var lineItem = (0, _assign2.default)({}, defaultLineItem);
			var istItemId = batchInfo.logic.getIstItemId();
			var itemLogic = itemCache.getItemById(istItemId);
			lineItem.item = {
				value: _MyDouble2.default.convertStringToDouble(itemLogic.getItemId()),
				label: itemLogic.getItemName(),
				purchasePrice: _MyDouble2.default.getAmountWithDecimal(itemLogic.getItemPurchaseUnitPrice()),
				salePrice: _MyDouble2.default.getAmountWithDecimal(itemLogic.getItemSaleUnitPrice()),
				stockQty: _MyDouble2.default.getQuantityWithDecimal(itemLogic.getAvailableQuantity(), itemLogic.getItemMinStockQuantity(), true),
				item: itemLogic
			};
			lineItem.istId = batchInfo.logic.getIstId();
			if (mrpColumnVisibility) {
				lineItem.mrp = batchInfo.istMrp;
			}
			if (batchNumberColumnVisibility) {
				lineItem.batchNo = batchInfo.istBatchNumber;
			}
			if (serialNumberColumnVisibility) {
				lineItem.slNo = batchInfo.istSerialNumber;
			}
			if (mfgDateColumnVisibility) {
				var mfgDate = '';
				var mfgDateObj = batchInfo.istManufacturingDateObj;
				if (mfgDateObj && mfgDateObj != 'Invalid Date') {
					var format = settingCache.getManufacturingDateFormat() == StringConstant.monthYear ? 'm/y' : 'd/m/y';
					mfgDate = MyDate.getDate(format, mfgDateObj);
				}
				lineItem.mfgDate = mfgDate;
			}
			if (expDateColumnVisibility) {
				var expDate = '';
				var expDateObj = batchInfo.istExpiryDateObj;
				if (expDateObj && expDateObj != 'Invalid Date') {
					var _format = settingCache.getExpiryDateFormat() == StringConstant.monthYear ? 'm/y' : 'd/m/y';
					expDate = MyDate.getDate(_format, expDateObj);
				}
				lineItem.expDate = expDate;
			}
			if (sizeColumnVisibility) {
				lineItem.size = batchInfo.istSize;
			}
			lineItem.qty = batchInfo.qty;
			return lineItem;
		});
	}
	return [];
}

function changeLineItem(_ref5) {
	var defaultLineItem = _ref5.defaultLineItem,
	    partyLogic = _ref5.partyLogic,
	    itemLogic = _ref5.itemLogic,
	    firmLogic = _ref5.firmLogic,
	    txnType = _ref5.txnType,
	    txnId = _ref5.txnId,
	    placeOfSupply = _ref5.placeOfSupply,
	    taxInclusive = _ref5.taxInclusive;

	if (txnType == _TxnTypeConstant2.default.TXN_TYPE_EXPENSE || txnType == _TxnTypeConstant2.default.TXN_TYPE_OTHER_INCOME) {
		if (defaultLineItem.qty < 1) {
			defaultLineItem.qty = 1;
		}
		defaultLineItem.amount = _MyDouble2.default.convertStringToDouble(defaultLineItem.qty * (defaultLineItem.priceUnit || 0));
		return defaultLineItem;
	}

	var partyId = partyLogic && partyLogic.getNameId() ? partyLogic.getNameId() : 0;
	var itemId = itemLogic && itemLogic.getItemId() || 0;
	var taxCodeCache = new _TaxCodeCache2.default();
	var isCompositeEnabled = settingCache.isCompositeSchemeEnabled();
	var isPartyComposite = (partyLogic && partyLogic.getCustomerType()) == NameCustomerType.REGISTERED_COMPOSITE;
	var exclusivePrice = 0;

	var taxId = _MyDouble2.default.convertStringToDouble(itemLogic && itemLogic.getItemTaxId() || 0);

	if (_GSTHelper2.default.isCompositeTxn(txnType, partyLogic) && !_GSTRReportHelper2.default.isTaxOfTypeOthers(taxId)) {
		taxId = 0;
	}

	var additionalCESSAmount = _MyDouble2.default.getBalanceAmountWithDecimal(itemLogic && itemLogic.getItemAdditionalCESS() || 0);

	// this flag is to check if party wise item rate exists, but the transaction is being done for the first time
	// in that case, even if party wise item rate is enabled, we should treat it as inclusive.
	var partyRatePriceExists = false;
	var priceUnit = 0;
	var dataLoader = new _DataLoader2.default();
	switch (txnType) {
		case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE:
		case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN:
		case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER:
			if (!settingCache.getPartyWiseItemRateEnabled()) {
				priceUnit = itemLogic && itemLogic.getItemPurchaseUnitPrice() || 0;
			} else {
				var partyRate = dataLoader.LoadPartyWiseItemRate(partyId, itemId);

				if (partyRate.getPurchasePrice() > 0) {
					partyRatePriceExists = true;
					priceUnit = partyRate.getPurchasePrice();
				} else {
					priceUnit = itemLogic && itemLogic.getItemPurchaseUnitPrice() || 0;
				}
			}
			break;
		case _TxnTypeConstant2.default.TXN_TYPE_SALE:
		case _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN:
		case _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER:
		case _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE:
		case _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN:
			if (!settingCache.getPartyWiseItemRateEnabled()) {
				priceUnit = itemLogic && itemLogic.getItemSaleUnitPrice() || 0;
			} else {
				var _partyRate = dataLoader.LoadPartyWiseItemRate(partyId, itemId);

				if (_partyRate.getSalePrice() > 0) {
					partyRatePriceExists = true;
					priceUnit = _partyRate.getSalePrice();
				} else {
					priceUnit = itemLogic && itemLogic.getItemSaleUnitPrice() || 0;
				}
			}
			break;
	}

	if (!defaultLineItem.qty) defaultLineItem.qty = 1;

	if (settingCache.getAdditionalCessEnabled()) {
		defaultLineItem.cess = additionalCESSAmount ? _MyDouble2.default.getBalanceAmountWithDecimal(additionalCESSAmount) : 0;
	}
	defaultLineItem.priceUnit = priceUnit;

	var itemTaxCodeIdNew = getCorrespondingIdOfTaxRate({
		taxId: taxId,
		txnType: txnType,
		partyLogic: partyLogic,
		firmLogic: firmLogic,
		placeOfSupply: placeOfSupply
	});
	var discountAmount = defaultLineItem.discountAmount ? _MyDouble2.default.getBalanceAmountWithDecimal(defaultLineItem.discountAmount) : 0;
	var taxType = 0;
	if (settingCache.isItemwiseTaxEnabled()) {
		defaultLineItem.taxPercent = itemTaxCodeIdNew;
		defaultLineItem.showITCEligible = false;
		if (taxId > 0) {
			var taxCodeObj = taxCodeCache.getTaxCodeObjectById(itemTaxCodeIdNew);
			var taxRateType = taxCodeObj.getTaxRateType();

			if (taxRateType !== _TaxCodeConstants2.default.OTHER && taxRateType !== _TaxCodeConstants2.default.Exempted && (txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN) && settingCache.getGSTEnabled()) {
				defaultLineItem.ITCValue = _TxnITCConstants2.default.ITC_ELIGIBLE;
				defaultLineItem.showITCEligible = true;
			}
		}

		if (txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE || txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN || txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER || txnType === _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE || txnType === _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN) {
			taxType = itemLogic && itemLogic.getItemTaxTypeSale() || 0;
		} else if (txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER) {
			taxType = itemLogic && itemLogic.getItemTaxTypePurchase() || 0;
		}
		var taxPercent = _MyDouble2.default.getPercentageWithDecimal(taxCodeCache.getRateForTaxId(itemTaxCodeIdNew));
		var quantity = _MyDouble2.default.getQuantityWithDecimalWithoutColor(defaultLineItem.qty);

		exclusivePrice = _MyDouble2.default.convertStringToDouble(priceUnit);
		if (partyRatePriceExists) {
			if (taxInclusive == _ItemType2.default.ITEM_TXN_TAX_INCLUSIVE) {
				exclusivePrice = exclusivePrice + exclusivePrice * _MyDouble2.default.convertStringToDouble(taxPercent) / 100;
				defaultLineItem.taxAmount = _MyDouble2.default.convertStringToDouble(taxPercent) / 100 * (priceUnit * quantity - _MyDouble2.default.convertStringToDouble(discountAmount));
			} else {
				defaultLineItem.taxAmount = _MyDouble2.default.convertStringToDouble(taxPercent) / 100 * (exclusivePrice * quantity - _MyDouble2.default.convertStringToDouble(discountAmount));
			}
		} else {
			switch (_MyDouble2.default.convertStringToDouble(taxType)) {
				case _ItemType2.default.ITEM_TXN_TAX_INCLUSIVE:
					exclusivePrice = taxInclusive == _ItemType2.default.ITEM_TXN_TAX_EXCLUSIVE ? _MyDouble2.default.convertStringToDouble(exclusivePrice) * 100 / (_MyDouble2.default.convertStringToDouble(taxPercent) + 100) : exclusivePrice;
					var priceForTax = exclusivePrice;
					if (taxInclusive == _ItemType2.default.ITEM_TXN_TAX_INCLUSIVE) {
						priceForTax = _MyDouble2.default.convertStringToDouble(exclusivePrice) * 100 / (_MyDouble2.default.convertStringToDouble(taxPercent) + 100);
					}
					defaultLineItem.taxAmount = _MyDouble2.default.convertStringToDouble(taxPercent) / 100 * (priceForTax * quantity - _MyDouble2.default.convertStringToDouble(discountAmount));
					break;
				case _ItemType2.default.ITEM_TXN_TAX_EXCLUSIVE:
					defaultLineItem.taxAmount = _MyDouble2.default.convertStringToDouble(taxPercent) / 100 * (exclusivePrice * quantity - _MyDouble2.default.convertStringToDouble(discountAmount));
					exclusivePrice = taxInclusive == _ItemType2.default.ITEM_TXN_TAX_INCLUSIVE ? _MyDouble2.default.convertStringToDouble(exclusivePrice) + _MyDouble2.default.convertStringToDouble(exclusivePrice) * _MyDouble2.default.convertStringToDouble(taxPercent) / 100 : exclusivePrice;
					break;
				default:
					defaultLineItem.taxAmount = _MyDouble2.default.convertStringToDouble(taxPercent) / 100 * (exclusivePrice * quantity - _MyDouble2.default.convertStringToDouble(discountAmount));
					break;
			}
		}

		defaultLineItem.priceUnit = _MyDouble2.default.getBalanceAmountWithDecimal(exclusivePrice);
		defaultLineItem.taxAmount = _MyDouble2.default.getBalanceAmountWithDecimal(defaultLineItem.taxAmount);
	}

	defaultLineItem.description = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_DESCRIPTION) && itemLogic && itemLogic.getItemDescription() || '';

	if (defaultLineItem.priceUnit === 0) {
		defaultLineItem.discountPercent = '';
	}
	var qty = _MyDouble2.default.convertStringToDouble(defaultLineItem.qty);

	if (settingCache.getAdditionalCessEnabled()) {
		var additionalCessAmount = _MyDouble2.default.convertStringToDouble(itemLogic && itemLogic.getItemAdditionalCESS() || 0);
		defaultLineItem.cess = _MyDouble2.default.getAmountWithDecimal(qty * additionalCessAmount);
	}

	var lineItemTaxAmount = defaultLineItem.taxAmount;

	if (!partyRatePriceExists) {
		if (_MyDouble2.default.convertStringToDouble(taxType) === _ItemType2.default.ITEM_TXN_TAX_INCLUSIVE && taxInclusive == _ItemType2.default.ITEM_TXN_TAX_INCLUSIVE) {
			lineItemTaxAmount = 0;
		} else if (_MyDouble2.default.convertStringToDouble(taxType) === _ItemType2.default.ITEM_TXN_TAX_INCLUSIVE && taxInclusive == _ItemType2.default.ITEM_TXN_TAX_EXCLUSIVE) {
			lineItemTaxAmount = 0;
		}
	}

	var unitDropdown = getUnitDropdownOptions(itemLogic);
	defaultLineItem.unitObj = unitDropdown;

	defaultLineItem.amount = _MyDouble2.default.getBalanceAmountWithDecimal(_MyDouble2.default.convertStringToDouble(priceUnit) * _MyDouble2.default.convertStringToDouble(qty) + (_MyDouble2.default.convertStringToDouble(lineItemTaxAmount) + _MyDouble2.default.convertStringToDouble(defaultLineItem.cess) - _MyDouble2.default.convertStringToDouble(discountAmount)));
	var baseUnitId = itemLogic && itemLogic.getBaseUnitId() || '';
	if (baseUnitId) {
		defaultLineItem.unit = baseUnitId;
	} else {
		defaultLineItem.unit = 0;
	}

	return defaultLineItem;
}

function selectImage(callback) {
	var ImageHelper = require('../../../Utilities/ImageHelper');
	ImageHelper.openSaveImageDialog({}, callback);
}

function getUniqueKey() {
	var length = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1000000000;

	return Math.round(Math.random() * length * length);
}

function getTransactionsForB2BLinking(nameId, firmId, txnType) {
	var selectedTransactionMap = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : new _map2.default();
	var loadDataForBillToBill = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : true;

	var dataLoader = new _DataLoader2.default();
	var transactionListB2B = dataLoader.loadTransactionsForBillToBill(firmId, nameId, txnType, loadDataForBillToBill);

	var txnMap = new _map2.default();
	transactionListB2B.forEach(function (txnObj) {
		var txnId = _MyDouble2.default.convertStringToDouble(txnObj.getTxnId());
		var mapVal = selectedTransactionMap.get(txnId) ? selectedTransactionMap.get(txnId) : false;
		var selectedAmount = mapVal ? _MyDouble2.default.convertStringToDouble(mapVal[0].getTxnCurrentBalanceAmount()) : _MyDouble2.default.convertStringToDouble(txnObj.getTxnCurrentBalanceAmount());
		txnMap.set(txnId, [txnObj, selectedAmount]);
	});

	return txnMap;
}

function getTransactionSettings(txnType, txnId, transaction, cashSale) {
	var fieldsVisibility = {};
	var labels = {
		name: 'Party',
		addPartyLabel: 'Add Party',
		received: 'RECEIVED',
		total: 'TOTAL',
		invoiceNo: 'Invoice Number',
		invoiceDate: 'Invoice Date',
		balance: 'BALANCE',
		totalReceivedAmount: 'TOTAL RECEIVED',
		payable: 'PAYABLE'
	};

	switch (txnType) {
		case _TxnTypeConstant2.default.TXN_TYPE_SALE:
			fieldsVisibility = {
				invoiceType: settingCache.getTaxInvoiceEnabled() && settingCache.getTxnRefNoEnabled(),
				cashSale: settingCache.getDefaultCashSaleEnabled(),
				reverseCharge: false,
				partyBalance: true,
				invoiceDate: true,
				paymentTerms: transaction.paymentTerms || settingCache.isPaymentTermEnabled(),
				dueDate: transaction.dueDate || settingCache.isPaymentTermEnabled(),
				placeOfSupply: transaction.placeOfSupply || settingCache.isPlaceOfSupplyEnabled(),
				placeOfSupplyEnabled: true,
				displayName: transaction.displayName || settingCache.getDisplayNameEnabled(),
				returnNo: false,
				invoicePrefix: true,
				invoiceNo: settingCache.getTxnRefNoEnabled(),
				PODetails: transaction.PONo || transaction.PODate || settingCache.isPODetailsEnabled(),
				eWayBillNo: transaction.eWayBillNo || settingCache.isEWayBillNumberEnabled(),
				lineItems: settingCache.getItemEnabled(),
				transportationDetails: true,
				paymentType: true,
				image: true,
				description: true,
				discount: transaction.discountAmount || transaction.discountPercent || settingCache.getDiscountEnabled(),
				tax: transaction.taxAmount || transaction.taxPercent || settingCache.getTaxEnabled(),
				roundOff: _MyDouble2.default.convertStringToDouble(transaction.roundOffAmount) || settingCache.isRoundOffEnabled(),
				additionalCharges: true,
				total: true,
				received: true,
				balance: true,
				discountForCash: false,
				linkPayment: settingCache.isBillToBillEnabled() && !transaction.cashSale,
				saveAndPrintButton: true,
				billingAddress: true,
				shippingAddress: settingCache.getPartyShippingAddressEnabled(),
				saveAndNewButton: !txnId
			};
			break;
		case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE:
			fieldsVisibility = {
				invoiceType: false,
				cashSale: false,
				reverseCharge: settingCache.getReverseChargeEnabled(),
				partyBalance: true,
				invoiceDate: true,
				paymentTerms: transaction.paymentTerms || settingCache.isPaymentTermEnabled(),
				dueDate: transaction.dueDate || settingCache.isPaymentTermEnabled(),
				placeOfSupply: transaction.placeOfSupply || settingCache.isPlaceOfSupplyEnabled(),
				placeOfSupplyEnabled: true,
				displayName: false,
				returnNo: false,
				invoicePrefix: false,
				invoiceNo: settingCache.getTxnRefNoEnabled(),
				PODetails: transaction.PONo || transaction.PODate || settingCache.isPODetailsEnabled(),
				eWayBillNo: transaction.eWayBillNo || settingCache.isEWayBillNumberEnabled(),
				lineItems: settingCache.getItemEnabled(),
				transportationDetails: true,
				paymentType: true,
				image: true,
				description: true,
				discount: transaction.discountAmount || transaction.discountPercent || settingCache.getDiscountEnabled(),
				tax: transaction.taxAmount || transaction.taxPercent || settingCache.getTaxEnabled(),
				roundOff: _MyDouble2.default.convertStringToDouble(transaction.roundOffAmount) || settingCache.isRoundOffEnabled(),
				additionalCharges: true,
				total: true,
				received: true,
				balance: true,
				discountForCash: false,
				linkPayment: settingCache.isBillToBillEnabled() && !transaction.cashSale,
				saveAndPrintButton: true,
				billingAddress: false,
				shippingAddress: false,
				saveAndNewButton: !txnId
			};
			if (settingCache.isPaymentTermEnabled()) {
				fieldsVisibility.dueDate = true;
				fieldsVisibility.paymentTerms = true;
			}
			labels.received = 'PAID';
			labels.invoiceNo = 'Bill Number';
			labels.invoiceDate = 'Bill Date';
			labels.totalReceivedAmount = 'TOTAL PAID';
			break;
		case _TxnTypeConstant2.default.TXN_TYPE_CASHIN:
		case _TxnTypeConstant2.default.TXN_TYPE_CASHOUT:
			fieldsVisibility = {
				invoiceType: false,
				cashSale: false,
				reverseCharge: false,
				partyBalance: true,
				invoiceDate: true,
				paymentTerms: false,
				dueDate: false,
				placeOfSupply: false,
				placeOfSupplyEnabled: false,
				displayName: false,
				returnNo: false,
				invoicePrefix: true,
				invoiceNo: settingCache.getTxnRefNoEnabled(),
				PODetails: false,
				eWayBillNo: false,
				lineItems: false,
				transportationDetails: false,
				paymentType: true,
				image: true,
				description: true,
				discount: false,
				tax: false,
				roundOff: false,
				additionalCharges: false,
				total: true,
				received: false,
				balance: false,
				discountForCash: settingCache.getDiscountInMoneyTxnEnabled(),
				linkPayment: settingCache.isBillToBillEnabled() && !transaction.cashSale,
				saveAndPrintButton: true,
				billingAddress: false,
				shippingAddress: false,
				saveAndNewButton: !txnId
			};

			labels.invoiceNo = 'Receipt No';
			labels.invoiceDate = 'Date';
			labels.balance = 'Total';
			if (txnType === _TxnTypeConstant2.default.TXN_TYPE_CASHIN) {
				labels.total = 'Received';
				labels.totalReceivedAmount = 'Total Received';
			} else {
				labels.total = 'Paid';
				labels.totalReceivedAmount = 'Total Paid';
			}
			break;
		case _TxnTypeConstant2.default.TXN_TYPE_EXPENSE:
		case _TxnTypeConstant2.default.TXN_TYPE_OTHER_INCOME:
			fieldsVisibility = {
				invoiceType: false,
				cashSale: false,
				reverseCharge: false,
				partyBalance: false,
				invoiceDate: true,
				paymentTerms: false,
				dueDate: false,
				placeOfSupply: false,
				placeOfSupplyEnabled: false,
				displayName: false,
				returnNo: false,
				invoicePrefix: false,
				invoiceNo: false,
				PODetails: false,
				eWayBillNo: false,
				lineItems: settingCache.getItemEnabled(),
				transportationDetails: false,
				paymentType: true,
				image: true,
				description: true,
				discount: false,
				tax: false,
				roundOff: _MyDouble2.default.convertStringToDouble(transaction.roundOffAmount) || settingCache.isRoundOffEnabled(),
				additionalCharges: false,
				total: true,
				received: false,
				balance: false,
				discountForCash: false,
				linkPayment: false,
				saveAndPrintButton: true,
				billingAddress: false,
				shippingAddress: false,
				saveAndNewButton: !txnId
			};

			labels.invoiceDate = 'Date';
			if (txnType === _TxnTypeConstant2.default.TXN_TYPE_EXPENSE) {
				labels.name = 'Expense Category';
				labels.addPartyLabel = 'Add Expense Category';
			} else {
				labels.name = 'Income Category';
				labels.addPartyLabel = 'Add Income Category';
			}
			break;
		case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN:
			fieldsVisibility = {
				invoiceType: false,
				cashSale: false,
				reverseCharge: settingCache.getReverseChargeEnabled(),
				partyBalance: true,
				invoiceDate: true,
				paymentTerms: false,
				dueDate: false,
				placeOfSupply: transaction.placeOfSupply || settingCache.isPlaceOfSupplyEnabled(),
				placeOfSupplyEnabled: false,
				displayName: false,
				returnNo: settingCache.getTxnRefNoEnabled(),
				invoicePrefix: false,
				invoiceNo: settingCache.getTxnRefNoEnabled(),
				PODetails: false,
				eWayBillNo: false,
				lineItems: settingCache.getItemEnabled(),
				transportationDetails: true,
				paymentType: true,
				image: true,
				description: true,
				discount: transaction.discountAmount || transaction.discountPercent || settingCache.getDiscountEnabled(),
				tax: transaction.taxAmount || transaction.taxPercent || settingCache.getTaxEnabled(),
				roundOff: _MyDouble2.default.convertStringToDouble(transaction.roundOffAmount) || settingCache.isRoundOffEnabled(),
				additionalCharges: true,
				total: true,
				received: true,
				balance: true,
				discountForCash: false,
				linkPayment: settingCache.isBillToBillEnabled() && !transaction.cashSale,
				saveAndPrintButton: true,
				billingAddress: false,
				shippingAddress: false,
				saveAndNewButton: !txnId
			};
			labels.invoiceNo = 'Bill Number';
			labels.invoiceDate = 'Bill Date';
			labels.payable = 'RECEIVABLE';
			break;
		case _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN:
			fieldsVisibility = {
				invoiceType: false,
				cashSale: false,
				reverseCharge: false,
				partyBalance: true,
				invoiceDate: true,
				paymentTerms: false,
				dueDate: false,
				placeOfSupply: transaction.placeOfSupply || settingCache.isPlaceOfSupplyEnabled(),
				placeOfSupplyEnabled: true,
				displayName: false,
				returnNo: settingCache.getTxnRefNoEnabled(),
				invoicePrefix: true,
				invoiceNo: settingCache.getTxnRefNoEnabled(),
				PODetails: false,
				eWayBillNo: false,
				lineItems: settingCache.getItemEnabled(),
				transportationDetails: true,
				paymentType: true,
				image: true,
				description: true,
				discount: transaction.discountAmount || transaction.discountPercent || settingCache.getDiscountEnabled(),
				tax: transaction.taxAmount || transaction.taxPercent || settingCache.getTaxEnabled(),
				roundOff: _MyDouble2.default.convertStringToDouble(transaction.roundOffAmount) || settingCache.isRoundOffEnabled(),
				additionalCharges: true,
				total: true,
				received: true,
				balance: true,
				discountForCash: false,
				linkPayment: settingCache.isBillToBillEnabled(),
				saveAndPrintButton: true,
				billingAddress: true,
				shippingAddress: settingCache.getPartyShippingAddressEnabled(),
				saveAndNewButton: !txnId
			};
			labels.received = 'PAID AMOUNT';
			labels.invoiceNo = 'Invoice Number';
			labels.invoiceDate = 'Invoice Date';
			labels.totalReceivedAmount = 'TOTAL PAID';
			break;
		case _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER:
		case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER:
			fieldsVisibility = {
				invoiceType: false,
				cashSale: false,
				reverseCharge: false,
				partyBalance: true,
				invoiceDate: true,
				paymentTerms: false,
				dueDate: transaction.dueDate || settingCache.isPaymentTermEnabled() || txnType === _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER || txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER,
				placeOfSupply: transaction.placeOfSupply || settingCache.isPlaceOfSupplyEnabled(),
				placeOfSupplyEnabled: true,
				displayName: transaction.displayName || settingCache.getDisplayNameEnabled(),
				returnNo: false,
				invoicePrefix: true,
				invoiceNo: settingCache.getTxnRefNoEnabled(),
				PODetails: false,
				eWayBillNo: false,
				lineItems: settingCache.getItemEnabled(),
				transportationDetails: true,
				paymentType: true,
				image: true,
				description: true,
				discount: transaction.discountAmount || transaction.discountPercent || settingCache.getDiscountEnabled(),
				tax: transaction.taxAmount || transaction.taxPercent || settingCache.getTaxEnabled(),
				roundOff: _MyDouble2.default.convertStringToDouble(transaction.roundOffAmount) || settingCache.isRoundOffEnabled(),
				additionalCharges: true,
				total: true,
				received: true,
				balance: true,
				discountForCash: false,
				linkPayment: false,
				saveAndPrintButton: true,
				billingAddress: true,
				shippingAddress: settingCache.getPartyShippingAddressEnabled(),
				saveAndNewButton: !txnId
			};

			labels.invoiceNo = 'Order No.';
			labels.invoiceDate = 'Order Date';
			labels.received = 'ADVANCE AMOUNT';
			if (txnType === _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER) {
				fieldsVisibility.displayName = false;
				fieldsVisibility.billingAddress = false;
				fieldsVisibility.shippingAddress = false;
				fieldsVisibility.placeOfSupplyEnabled = false;
			}
			break;
		case _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE:
		case _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN:
			fieldsVisibility = {
				invoiceType: false,
				cashSale: false,
				reverseCharge: false,
				partyBalance: true,
				invoiceDate: true,
				paymentTerms: false,
				dueDate: false,
				placeOfSupply: transaction.placeOfSupply || settingCache.isPlaceOfSupplyEnabled(),
				placeOfSupplyEnabled: true,
				displayName: transaction.displayName || settingCache.getDisplayNameEnabled(),
				returnNo: false,
				invoicePrefix: true,
				invoiceNo: settingCache.getTxnRefNoEnabled(),
				PODetails: false,
				eWayBillNo: false,
				lineItems: settingCache.getItemEnabled(),
				transportationDetails: false,
				paymentType: false,
				image: true,
				description: true,
				discount: transaction.discountAmount || transaction.discountPercent || settingCache.getDiscountEnabled(),
				tax: transaction.taxAmount || transaction.taxPercent || settingCache.getTaxEnabled(),
				roundOff: _MyDouble2.default.convertStringToDouble(transaction.roundOffAmount) || settingCache.isRoundOffEnabled(),
				additionalCharges: true,
				total: true,
				received: false,
				balance: false,
				discountForCash: false,
				linkPayment: false,
				saveAndPrintButton: true,
				billingAddress: true,
				shippingAddress: settingCache.getPartyShippingAddressEnabled(),
				saveAndNewButton: !txnId
			};
			if (txnType === _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN) {
				fieldsVisibility.dueDate = true;
				fieldsVisibility.transportationDetails = true;
				labels.invoiceNo = 'Challan No.';
			} else {
				fieldsVisibility.billingAddress = false;
				fieldsVisibility.shippingAddress = false;
				labels.invoiceNo = 'Ref No.';
			}
			break;
	}

	return { labels: labels, fieldsVisibility: fieldsVisibility };
}

function getLineItemSettings(txnType, isFTU) {
	var fieldsVisibility = {};

	switch (txnType) {
		case _TxnTypeConstant2.default.TXN_TYPE_SALE:
		case _TxnTypeConstant2.default.TXN_TYPE_SALE_ORDER:
		case _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN:
		case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE:
		case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_ORDER:
		case _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN:
		case _TxnTypeConstant2.default.TXN_TYPE_ESTIMATE:
		case _TxnTypeConstant2.default.TXN_TYPE_DELIVERY_CHALLAN:
			fieldsVisibility = {
				description: settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_DESCRIPTION),
				count: settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_COUNT),
				slNo: settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_SERIAL_ITEM_NUMBER),
				batchNo: settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_BATCH_NUMBER),
				expDate: settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_EXPIRY_DATE),
				mfgDate: settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_MANUFACTURING_DATE),
				mrp: settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_MRP),
				size: settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_SIZE),
				freeQty: settingCache.isFreeQuantityEnabled(),
				unit: settingCache.getItemUnitEnabled(),
				tax: settingCache.isItemwiseTaxEnabled(),
				discount: !isFTU && settingCache.isItemwiseDiscountEnabled(),
				cess: settingCache.getAdditionalCessEnabled(),
				additionalColumns: true
			};
			break;
		case _TxnTypeConstant2.default.TXN_TYPE_EXPENSE:
		case _TxnTypeConstant2.default.TXN_TYPE_OTHER_INCOME:
			fieldsVisibility = {
				description: false,
				count: false,
				slNo: false,
				batchNo: false,
				expDate: false,
				mfgDate: false,
				mrp: false,
				size: false,
				freeQty: false,
				unit: false,
				tax: false,
				discount: false,
				cess: false,
				additionalColumns: false,
				taxInclusiveDropdown: false
			};
			break;
	}

	return fieldsVisibility;
}

function additionalAdjustmentForReceivePayment(txnType, transaction, selectedTransactionMap) {
	var oldTxnObj = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

	var totalAmount = _MyDouble2.default.convertStringToDouble(transaction.total);
	var discountAmount = _MyDouble2.default.convertStringToDouble(transaction.discountAmount);
	var currentTxnBalance = _MyDouble2.default.convertStringToDouble(transaction.currentTxnBalance);

	if (oldTxnObj.getTxnType() == _TxnTypeConstant2.default.TXN_TYPE_SALE && txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHIN || oldTxnObj.getTxnType() == _TxnTypeConstant2.default.TXN_TYPE_SALE_RETURN && txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT || oldTxnObj.getTxnType() == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE && txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHOUT || oldTxnObj.getTxnType() == _TxnTypeConstant2.default.TXN_TYPE_PURCHASE_RETURN && txnType == _TxnTypeConstant2.default.TXN_TYPE_CASHIN) {
		var mapVal = selectedTransactionMap.get(_MyDouble2.default.convertStringToDouble(oldTxnObj.getTxnId()));
		var txnObj = mapVal[0];
		var linkTxn = mapVal[2];

		if (currentTxnBalance < 0) {
			selectedTransactionMap.set(_MyDouble2.default.convertStringToDouble(oldTxnObj.getTxnId()), [txnObj, _MyDouble2.default.convertStringToDouble(totalAmount + discountAmount), linkTxn]);
			transaction.currentTxnBalance = _MyDouble2.default.getBalanceAmountWithDecimal(0);
		}
	}
	return transaction;
}

// export function checkInvoiceReturnFieldForPresnt (
// 	txnType,
// 	invoiceReturnRefNumPresent,
// 	invoiceReturnDatePresent
// ) {
// 	let msgDateNotPresent =
// 		'Invoice Date is mandatory for GST filing. Do you wish to proceed anyway?';
// 	let msgRefNotPresent =
// 		txnType == TxnTypeConstants.TXN_TYPE_SALE_RETURN
// 			? 'Invoice number is mandatory for GST filing. Do you wish to proceed anyway?'
// 			: 'Bill number is mandatory for GST filing. Do you wish to proceed anyway?';
// 	let msgDateAndRefNotPresent =
// 		txnType == TxnTypeConstants.TXN_TYPE_SALE_RETURN
// 			? 'Invoice number and Invoice Date is mandatory for GST filing. Do you wish to proceed anyway?'
// 			: 'Bill number and Bill Date is mandatory for GST filing. Do you wish to proceed anyway?';
// 	if (settingCache.getGSTEnabled()) {
// 		if (!invoiceReturnDatePresent && !invoiceReturnRefNumPresent) {
// 			return msgDateAndRefNotPresent;
// 		} else if (!invoiceReturnDatePresent) {
// 			return msgDateNotPresent;
// 		} else if (!invoiceReturnRefNumPresent) {
// 			return msgRefNotPresent;
// 		} else {
// 			return false;
// 		}
// 	}
// }
function saveNewName(name, txnType) {
	var NameLogic = require('../../../BizLogic/nameLogic.js');
	var nameLogic = new NameLogic();
	var obj = void 0,
	    statusCode = void 0;
	switch (txnType) {
		case _TxnTypeConstant2.default.TXN_TYPE_EXPENSE:
			obj = {
				name: name,
				phone_number: '',
				addressStr: '',
				email: '',
				groupId: null,
				tinNumber: '',
				opening_balance: '0.0',
				opening_balance_date: '',
				isReceivable: true,
				nameType: NameType.NAME_TYPE_EXPENSE,
				contactState: '',
				gstinNumber: '',
				shippingAddress: ''
			};
			statusCode = nameLogic.saveNewName(obj, true, false);

			break;
		case _TxnTypeConstant2.default.TXN_TYPE_OTHER_INCOME:
			obj = {
				name: name,
				phone_number: '',
				addressStr: '',
				email: '',
				groupId: null,
				tinNumber: '',
				opening_balance: '0.0',
				opening_balance_date: '',
				isReceivable: true,
				nameType: NameType.NAME_TYPE_INCOME,
				contactState: '',
				gstinNumber: '',
				shippingAddress: ''
			};
			statusCode = nameLogic.saveNewName(obj, false, true);
			break;
		default:
			obj = {
				name: name,
				phone_number: '',
				addressStr: '',
				email: '',
				opening_balance: '0.0',
				opening_balance_date: new Date(),
				groupId: 1,
				customerType: '0',
				tinNumber: '',
				gstinNumber: '',
				isReceivable: 1,
				contactState: '',
				nameType: NameType.NAME_TYPE_PARTY
			};
			statusCode = nameLogic.saveNewName(obj, false, false);
			break;
	}
	return statusCode == _ErrorCode2.default.ERROR_NAME_SAVE_SUCCESS;
}

function convertLineItemFieldsToNumber(lineItem) {
	return (0, _extends3.default)({}, lineItem, {
		qty: _MyDouble2.default.convertStringToDouble(lineItem.qty),
		priceUnit: _MyDouble2.default.convertStringToDouble(lineItem.priceUnit),
		unit: _MyDouble2.default.convertStringToDouble(lineItem.unit),
		discountPercent: _MyDouble2.default.convertStringToDouble(lineItem.discountPercent),
		discountAmount: _MyDouble2.default.convertStringToDouble(lineItem.discountAmount),
		taxPercent: _MyDouble2.default.convertStringToDouble(lineItem.taxPercent),
		taxAmount: _MyDouble2.default.convertStringToDouble(lineItem.taxAmount),
		cess: _MyDouble2.default.convertStringToDouble(lineItem.cess),
		amount: _MyDouble2.default.convertStringToDouble(lineItem.amount)
	});
}