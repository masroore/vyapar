Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styles = require('@material-ui/core/styles');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _Dialog = require('@material-ui/core/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _DialogTitle = require('@material-ui/core/DialogTitle');

var _DialogTitle2 = _interopRequireDefault(_DialogTitle);

var _DialogContent = require('@material-ui/core/DialogContent');

var _DialogContent2 = _interopRequireDefault(_DialogContent);

var _DialogActions = require('@material-ui/core/DialogActions');

var _DialogActions2 = _interopRequireDefault(_DialogActions);

var _DialogContentText = require('@material-ui/core/DialogContentText');

var _DialogContentText2 = _interopRequireDefault(_DialogContentText);

var _Close = require('@material-ui/icons/Close');

var _Close2 = _interopRequireDefault(_Close);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

var _themes = require('../../../themes');

var _themes2 = _interopRequireDefault(_themes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var theme = (0, _themes2.default)();

var ImagePreview = function (_React$Component) {
	(0, _inherits3.default)(ImagePreview, _React$Component);

	function ImagePreview(props) {
		(0, _classCallCheck3.default)(this, ImagePreview);

		var _this = (0, _possibleConstructorReturn3.default)(this, (ImagePreview.__proto__ || (0, _getPrototypeOf2.default)(ImagePreview)).call(this, props));

		_this.onClose = _this.onClose.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(ImagePreview, [{
		key: 'onClose',
		value: function onClose(e) {
			e.stopPropagation();
			this.props.onClose && this.props.onClose(false);
		}
	}, {
		key: 'render',
		value: function render() {
			var _props = this.props,
			    classes = _props.classes,
			    _props$title = _props.title,
			    title = _props$title === undefined ? 'Image Preview' : _props$title,
			    _props$message = _props.message,
			    message = _props$message === undefined ? 'Are you sure you want to continue?' : _props$message,
			    _props$cancelText = _props.cancelText,
			    cancelText = _props$cancelText === undefined ? 'Close' : _props$cancelText,
			    _props$open = _props.open,
			    open = _props$open === undefined ? true : _props$open,
			    _props$src = _props.src,
			    src = _props$src === undefined ? '' : _props$src;

			return _react2.default.createElement(
				_styles.MuiThemeProvider,
				{ theme: theme },
				_react2.default.createElement(
					_Dialog2.default,
					{
						maxWidth: false,
						open: open,
						onClose: this.onClose,
						onEnter: this.save
					},
					_react2.default.createElement(
						_DialogTitle2.default,
						{ id: 'alert-dialog-title' },
						title
					),
					_react2.default.createElement(
						_DialogContent2.default,
						null,
						_react2.default.createElement(
							_DialogContentText2.default,
							{ id: 'alert-dialog-description' },
							_react2.default.createElement('img', { src: src })
						)
					),
					_react2.default.createElement(
						_DialogActions2.default,
						null,
						_react2.default.createElement(
							_Button2.default,
							{ tabindex: 0, onClick: this.onClose, color: 'primary' },
							cancelText
						)
					)
				)
			);
		}
	}]);
	return ImagePreview;
}(_react2.default.Component);

exports.default = ImagePreview;