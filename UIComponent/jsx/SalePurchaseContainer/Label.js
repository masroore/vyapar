Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Label(_ref) {
	var label = _ref.label,
	    _ref$length = _ref.length,
	    length = _ref$length === undefined ? 0 : _ref$length,
	    _ref$dots = _ref.dots,
	    dots = _ref$dots === undefined ? '...' : _ref$dots;

	var newLabel = label;
	if (length && label.length > length) {
		newLabel = label.substr(0, length) + dots;
	}
	return _react2.default.createElement(
		'span',
		null,
		newLabel
	);
}

Label.propTypes = {
	label: _propTypes2.default.string,
	length: _propTypes2.default.number,
	dots: _propTypes2.default.string
};

exports.default = Label;