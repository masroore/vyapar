Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = require('babel-runtime/helpers/objectWithoutProperties');

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('@material-ui/core/styles');

var _OutlineInput = require('../UIControls/OutlineInput');

var _OutlineInput2 = _interopRequireDefault(_OutlineInput);

var _Button = require('@material-ui/core/Button');

var _Button2 = _interopRequireDefault(_Button);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles() {
	return {
		textFieldContainer: {
			height: 36,
			background: '#fff'
		},
		textField: {
			fontSize: 13,
			color: '#222A3F',
			zIndex: 0,
			height: 36,
			paddingTop: 22
		},
		textFieldLabel: {
			fontSize: 16,
			zIndex: 0,
			fontFamily: 'RobotoMedium',
			color: 'rgba(34, 42, 63, 0.5)',
			transform: 'translate(14px, 13px) scale(0.81)',
			whiteSpace: 'nowrap',
			overflow: 'hidden',
			maxWidth: 200,
			textOverflow: 'ellipsis'
		},
		notchedOutline: {},
		billingNameContainer: {
			position: 'relative'
		},
		billingNameDropdown: {
			position: 'absolute',
			width: 230,
			background: '#fff',
			border: '1px solid #ddd',
			cursor: 'pointer',
			zIndex: 999999
		}
	};
};

var BillingNameInput = function (_React$PureComponent) {
	(0, _inherits3.default)(BillingNameInput, _React$PureComponent);

	function BillingNameInput(props) {
		(0, _classCallCheck3.default)(this, BillingNameInput);

		var _this = (0, _possibleConstructorReturn3.default)(this, (BillingNameInput.__proto__ || (0, _getPrototypeOf2.default)(BillingNameInput)).call(this, props));

		_this.onFocus = function () {
			var isPartyDropdownVisible = _this.state.isPartyDropdownVisible;
			var items = _this.props.items;

			var party = items.find(function (item) {
				return item.value != 0;
			});
			if (!isPartyDropdownVisible && party) {
				_this.setState({ showDropdown: true });
			}
		};

		_this.onFocusOut = function () {
			_this.timeout = setTimeout(function () {
				_this.setState({ showDropdown: false });
			}, 500);
		};

		_this.showParties = function () {
			var showPartiesDropdown = _this.props.showPartiesDropdown;

			_this.setState({
				showDropdown: false,
				isPartyDropdownVisible: true
			});
			showPartiesDropdown && showPartiesDropdown();
		};

		_this.state = {
			showDropdown: false,
			isPartyDropdownVisible: false
		};
		_this.timeout = null;
		return _this;
	}

	(0, _createClass3.default)(BillingNameInput, [{
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			clearTimeout(this.timeout);
		}
	}, {
		key: 'render',
		value: function render() {
			var _props = this.props,
			    classes = _props.classes,
			    showPartiesDropdown = _props.showPartiesDropdown,
			    rest = (0, _objectWithoutProperties3.default)(_props, ['classes', 'showPartiesDropdown']);
			var showDropdown = this.state.showDropdown;


			return _react2.default.createElement(
				'div',
				{ className: classes.billingNameContainer },
				_react2.default.createElement(_OutlineInput2.default, (0, _extends3.default)({
					onFocus: this.onFocus,
					onBlur: this.onFocusOut
				}, rest)),
				showDropdown && showPartiesDropdown && _react2.default.createElement(
					'div',
					{
						onClick: this.showParties,
						className: classes.billingNameDropdown
					},
					_react2.default.createElement(
						_Button2.default,
						{ variant: 'text', color: 'primary' },
						'Show Parties'
					)
				)
			);
		}
	}]);
	return BillingNameInput;
}(_react2.default.PureComponent);

BillingNameInput.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	options: _propTypes2.default.object,
	label: _propTypes2.default.string,
	className: _propTypes2.default.string,
	value: _propTypes2.default.string,
	type: _propTypes2.default.string,
	customClasses: _propTypes2.default.string,
	items: _propTypes2.default.array,
	showPartiesDropdown: _propTypes2.default.func
};

exports.default = (0, _styles.withStyles)(styles)(BillingNameInput);