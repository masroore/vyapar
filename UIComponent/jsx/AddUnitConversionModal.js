Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Modal = require('./Modal');

var _Modal2 = _interopRequireDefault(_Modal);

var _ItemUnitCache = require('../../Cache/ItemUnitCache');

var _ItemUnitCache2 = _interopRequireDefault(_ItemUnitCache);

var _ItemUnitMappingCache = require('../../Cache/ItemUnitMappingCache.js');

var _ItemUnitMappingCache2 = _interopRequireDefault(_ItemUnitMappingCache);

var _ItemunitMapping = require('../../BizLogic/ItemunitMapping');

var _ItemunitMapping2 = _interopRequireDefault(_ItemunitMapping);

var _ErrorCode = require('../../Constants/ErrorCode');

var _ErrorCode2 = _interopRequireDefault(_ErrorCode);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SelectUnitModal = function (_React$Component) {
	(0, _inherits3.default)(SelectUnitModal, _React$Component);

	function SelectUnitModal(props) {
		(0, _classCallCheck3.default)(this, SelectUnitModal);

		var _this = (0, _possibleConstructorReturn3.default)(this, (SelectUnitModal.__proto__ || (0, _getPrototypeOf2.default)(SelectUnitModal)).call(this, props));

		_this.getMappings = function (baseUnit, secondaryUnit) {
			var itemUnitMappingCache = new _ItemUnitMappingCache2.default();
			return itemUnitMappingCache.getMappingFromBaseAndSecondaryUnitIds(baseUnit, secondaryUnit);
		};

		_this.onSave = function (saveAndNew) {
			// check if both None
			if (_this.state.baseUnit == 0 || _this.state.secondaryUnit == 0) {
				ToastHelper.info('Please select base and secondary Unit.');
				return false;
			}
			// check if both same
			if (_this.state.baseUnit == _this.state.secondaryUnit) {
				ToastHelper.info('Please base and secondary Unit can not be same.');
				return false;
			}
			// check if first selected
			// validate conversion rate. must be greater than zero if new
			if (_this.state.conversionRate <= 0) {
				ToastHelper.info('Please enter conversion rate.');
				return false;
			}
			// check if mapping already exists
			var available = _this.state.mappings.filter(function (mapping) {
				return _this.state.conversionRate == Number(mapping.getConversionRate());
			});
			if (available.length) {
				if (saveAndNew) {
					ToastHelper.info('Mapping already exists.');
					_this.clear();
				} else {
					_this.props.onSave();
				}
				return false;
			}
			// if not exists, create new
			return _this.addNewUnitMapping(saveAndNew);
		};

		_this.clear = function () {
			_this.setState({
				baseUnit: _this.props.selected || 0,
				secondaryUnit: 0,
				mappings: [],
				conversionRate: 0
			});
		};

		_this.addNewUnitMapping = function (saveAndNew) {
			var itemUnitMapping = new _ItemunitMapping2.default();
			var statusCode = itemUnitMapping.addNewUnitMapping(_this.state.baseUnit, _this.state.secondaryUnit, _this.state.conversionRate);

			if (statusCode == _ErrorCode2.default.ERROR_UNIT_MAPPING_SAVE_SUCCESS) {
				ToastHelper.success(statusCode);
				if (saveAndNew) {
					_this.clear();
				} else {
					_this.props.onSave();
				}
				return true;
			} else {
				ToastHelper.error(statusCode);
				return false;
			}
		};

		_this.setConversionRate = function (e) {
			return _this.setState({ conversionRate: e.target.value });
		};

		_this.state = {
			baseUnit: _this.props.selected || 0,
			secondaryUnit: 0,
			mappings: [],
			conversionRate: 0
		};
		_this.onClose = _this.onClose.bind(_this);
		_this.changeBaseUnit = _this.changeBaseUnit.bind(_this);
		_this.changeSecondaryUnit = _this.changeSecondaryUnit.bind(_this);
		_this.itemUnitCache = new _ItemUnitCache2.default();
		_this.baseUnits = _this.itemUnitCache.getAllItemUnits();
		_this.unitMappings = [];
		return _this;
	}

	(0, _createClass3.default)(SelectUnitModal, [{
		key: 'onClose',
		value: function onClose() {
			this.props.onClose && this.props.onClose();
		}
	}, {
		key: 'changeBaseUnit',
		value: function changeBaseUnit(e) {
			var baseUnit = e.target.value;
			var mappings = [];
			if (this.state.secondaryUnit !== baseUnit && baseUnit != 0 && this.state.secondaryUnit != 0) {
				mappings = this.getMappings(baseUnit, this.state.secondaryUnit);
			}
			this.setState({
				baseUnit: baseUnit,
				mappings: mappings
			});
		}
	}, {
		key: 'changeSecondaryUnit',
		value: function changeSecondaryUnit(e) {
			var secondaryUnit = e.target.value;
			var mappings = [];
			if (this.state.baseUnit == secondaryUnit && secondaryUnit != 0) {
				ToastHelper.error('Base Unit and Secondary Unit can not be same.');
			}

			if (this.state.baseUnit !== secondaryUnit && secondaryUnit != 0 && this.state.baseUnit != 0) {
				mappings = this.getMappings(this.state.baseUnit, secondaryUnit);
			}
			this.setState({
				secondaryUnit: secondaryUnit,
				mappings: mappings
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _this2 = this;

			var baseUnits = this.baseUnits.map(function (unit) {
				return _react2.default.createElement(
					'option',
					{ key: unit.getUnitId(), selected: unit.getUnitId() == _this2.state.baseUnit, value: unit.getUnitId() },
					unit.getUnitName() + ' (' + unit.getUnitShortName() + ')'
				);
			});
			var secondaryUnits = this.baseUnits.map(function (unit) {
				return _react2.default.createElement(
					'option',
					{ key: unit.getUnitId(), selected: unit.getUnitId() == _this2.state.secondaryUnit, value: unit.getUnitId() },
					unit.getUnitName() + ' (' + unit.getUnitShortName() + ')'
				);
			});
			return _react2.default.createElement(
				_Modal2.default,
				{ onClose: this.onClose, isOpen: this.props.isOpen, title: 'Add Conversion' },
				_react2.default.createElement(
					'div',
					{ className: 'modalBody' },
					_react2.default.createElement(
						'div',
						{ className: 'modal-content-wrapper select-unit' },
						_react2.default.createElement(
							'div',
							{ className: 'form-control inline' },
							_react2.default.createElement(
								'span',
								{ className: 'input-addon' },
								'1'
							),
							_react2.default.createElement(
								'label',
								{ className: 'block-label', htmlFor: '' },
								'base unit'
							),
							_react2.default.createElement(
								'select',
								{ style: { width: '150px' }, onChange: this.changeBaseUnit, className: 'input-control with-addon' },
								_react2.default.createElement(
									'option',
									{ value: '0' },
									'None'
								),
								baseUnits
							)
						),
						_react2.default.createElement(
							'div',
							{ className: 'form-control inline ml-15' },
							' = '
						),
						_react2.default.createElement(
							'div',
							{ className: 'form-control inline ml-15' },
							_react2.default.createElement(
								'label',
								{ className: 'block-label', htmlFor: '' },
								'Rate'
							),
							_react2.default.createElement('input', { className: 'input-control small-input', value: this.state.conversionRate, onChange: this.setConversionRate, type: 'number' })
						),
						_react2.default.createElement(
							'div',
							{ className: 'form-control inline ml-15' },
							_react2.default.createElement(
								'label',
								{ className: 'block-label', htmlFor: '' },
								'secondary unit'
							),
							_react2.default.createElement(
								'select',
								{ style: { width: '200px' }, onChange: this.changeSecondaryUnit, className: 'input-control' },
								_react2.default.createElement(
									'option',
									{ value: '0' },
									'None'
								),
								secondaryUnits
							)
						)
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'modalFooter align-items-center justify-content-end d-flex' },
					_react2.default.createElement(
						'button',
						{ onClick: function onClick() {
								return _this2.onSave(false);
							}, className: 'modal-footer-button terminalButton' },
						'Save'
					),
					_react2.default.createElement(
						'button',
						{ onClick: function onClick() {
								return _this2.onSave(true);
							}, className: 'modal-footer-button terminalButton' },
						'Save & New'
					)
				)
			);
		}
	}]);
	return SelectUnitModal;
}(_react2.default.Component);

exports.default = SelectUnitModal;