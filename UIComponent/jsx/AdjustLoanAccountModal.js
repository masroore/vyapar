Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Modal = require('./Modal');

var _Modal2 = _interopRequireDefault(_Modal);

var _TextField = require('@material-ui/core/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _LoanAccountCache = require('../../Cache/LoanAccountCache');

var _LoanAccountCache2 = _interopRequireDefault(_LoanAccountCache);

var _DecimalInputFilter = require('../../Utilities/DecimalInputFilter');

var _DecimalInputFilter2 = _interopRequireDefault(_DecimalInputFilter);

var _PrimaryButton = require('./UIControls/PrimaryButton');

var _PrimaryButton2 = _interopRequireDefault(_PrimaryButton);

var _styles = require('@material-ui/core/styles');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
	return {
		'@global': {
			'.adjustLoanAccountDiv': {
				marginTop: '1rem'
			},
			'.adjustLoanAccountFooter': {
				display: 'flex',
				justifyContent: 'flex-end'
			}
		}
	};
};
var commonTextFieldProps = {
	margin: 'dense',
	variant: 'outlined',
	fullWidth: true,
	color: 'primary'
};

var AdjustLoanAccountModal = function (_React$Component) {
	(0, _inherits3.default)(AdjustLoanAccountModal, _React$Component);

	function AdjustLoanAccountModal(props) {
		(0, _classCallCheck3.default)(this, AdjustLoanAccountModal);

		var _this = (0, _possibleConstructorReturn3.default)(this, (AdjustLoanAccountModal.__proto__ || (0, _getPrototypeOf2.default)(AdjustLoanAccountModal)).call(this, props));

		_this.handleChange = function (event) {
			var _event$target = event.target,
			    name = _event$target.name,
			    value = _event$target.value;

			if (name == 'adjustment') {
				value = _DecimalInputFilter2.default.inputTextFilterForAmount(event.target);
			}
			_this.setState((0, _defineProperty3.default)({}, name, value));
		};

		_this.onOpen = function () {
			var transaction = _this.props.transaction || {};
			var _transaction$principa = transaction.principalAmount,
			    principalAmount = _transaction$principa === undefined ? '' : _transaction$principa,
			    _transaction$date = transaction.date,
			    date = _transaction$date === undefined ? new Date() : _transaction$date;


			var accountId = _this.props.accountId;
			var loanAccountCache = new _LoanAccountCache2.default();
			var loanAccount = loanAccountCache.getLoanAccountById(accountId);

			var adjustment = Number(principalAmount || '');
			var currentBalance = Number(loanAccount.getCurrentBalance() || '') - adjustment;
			_this.currentBalance = currentBalance;
			_this.setState({
				adjustment: adjustment,
				transactionDate: MyDate.getDate('d/m/y', date)
			});
		};

		_this.onSubmit = function (e) {
			e.preventDefault();
			var LoanTransactionModel = require('../../Models/LoanTransactionModel');
			var TxnTypeConstant = require('../../Constants/TxnTypeConstant');
			var _this$state = _this.state,
			    adjustment = _this$state.adjustment,
			    transactionDate = _this$state.transactionDate;

			var transaction = _this.props.transaction || {};

			var loanAccountId = _this.props.accountId;
			var loanAccountCache = new _LoanAccountCache2.default();
			var loanAccount = loanAccountCache.getLoanAccountById(loanAccountId);
			// Validate transaction date
			var startingDate = loanAccount.getOpeningDate();
			transactionDate = MyDate.getDateObj(transactionDate, 'dd/mm/yyyy', '/');
			transactionDate.setHours(0, 0, 0, 0);
			if (startingDate.getTime() > transactionDate.getTime()) {
				ToastHelper.error('Loan adjustment date cannot be before loan starting date.');
				return;
			}
			// Validate adjustment
			if (!Number(adjustment)) {
				ToastHelper.error('Amount cannot be left zero or empty');
				return;
			}
			var currentBalance = Number(loanAccount.getCurrentBalance()) - Number(transaction.principalAmount || 0);
			var currentBalanceAfterAdjustment = currentBalance + Number(adjustment);
			if (currentBalanceAfterAdjustment < 0) {
				ToastHelper.error('Current Balance cannot be less than zero.');
				return;
			}
			var principalAmount = Number(adjustment);
			var paymentTypeId = loanAccount.getLoanReceivedIn();
			var loanTxnId = transaction.loanTxnId || '';
			var loanType = TxnTypeConstant.TXN_TYPE_LOAN_ADJUSTMENT;
			var loanTransactionModel = new LoanTransactionModel({
				loanTxnId: loanTxnId,
				principalAmount: principalAmount,
				paymentTypeId: paymentTypeId,
				loanAccountId: loanAccountId,
				loanType: loanType,
				transactionDate: transactionDate
			});
			var success = loanTransactionModel.saveLoanTxn();
			if (success) {
				_this.props.onSave && _this.props.onSave(loanTransactionModel.getLoanTxnId());
			} else {
				ToastHelper.error('Failed to save loan transaction.');
			}
		};

		_this.handleClose = function () {
			_this.props.onClose && _this.props.onClose();
		};

		_this.inputRef = _react2.default.createRef();
		_this.state = {
			adjustment: '',
			transactionDate: MyDate.getDate('d/m/y', new Date())
		};
		return _this;
	}

	(0, _createClass3.default)(AdjustLoanAccountModal, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			var _this2 = this;

			setTimeout(function () {
				$('#transactionDate').datepicker({
					dateFormat: 'dd/mm/yy',
					onSelect: function onSelect(value) {
						_this2.setState({
							transactionDate: value
						});
					},
					onClose: function onClose(value) {
						try {
							var dt = MyDate.getDateObj(value, 'dd/mm/yyyy', '/');
							if (dt && dt.toString() === 'Invalid Date' || !value || value.length < 10) {
								_this2.setState({
									transactionDate: MyDate.getDate('d/m/y', new Date())
								});
							}
						} catch (ex) {
							_this2.setState({
								transactionDate: MyDate.getDate('d/m/y', new Date())
							});
						}
					}
				});
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _state = this.state,
			    adjustment = _state.adjustment,
			    transactionDate = _state.transactionDate;

			return _react2.default.createElement(
				_Modal2.default,
				{
					height: '240px',
					width: '430px',
					onClose: this.handleClose,
					onAfterOpen: this.onOpen,
					isOpen: this.props.isOpen,
					title: 'Take more loan',
					canHideHeader: true
				},
				_react2.default.createElement(
					'div',
					{ className: 'adjustLoanAccountDiv' },
					_react2.default.createElement(
						'div',
						{ className: 'adjustLoanAccountBody' },
						_react2.default.createElement(_TextField2.default, (0, _extends3.default)({
							required: true
						}, commonTextFieldProps, {
							label: 'Increase Loan By',
							placeholder: 'Loan Increment',
							name: 'adjustment',
							value: adjustment,
							onChange: this.handleChange
						})),
						_react2.default.createElement(_TextField2.default, (0, _extends3.default)({}, commonTextFieldProps, {
							label: 'Date',
							className: 'transactionDate',
							value: transactionDate,
							name: 'transactionDate',
							id: 'transactionDate',
							onChange: this.handleChange
						}))
					),
					_react2.default.createElement(
						'div',
						{ className: 'adjustLoanAccountFooter' },
						_react2.default.createElement(
							_PrimaryButton2.default,
							{ 'data-shortcut': 'CTRL_S', style: { color: 'white', backgroundColor: '#1789FC', marginTop: '.5rem', float: 'right' }, onClick: this.onSubmit },
							'Save'
						)
					)
				)
			);
		}
	}]);
	return AdjustLoanAccountModal;
}(_react2.default.Component);

AdjustLoanAccountModal.propTypes = {
	accountId: _propTypes2.default.string,
	txnId: _propTypes2.default.string,
	onClose: _propTypes2.default.func,
	onSave: _propTypes2.default.func,
	isOpen: _propTypes2.default.bool
};

exports.default = (0, _styles.withStyles)(styles)(AdjustLoanAccountModal);