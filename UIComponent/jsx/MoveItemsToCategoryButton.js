Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _ItemCache = require('../../Cache/ItemCache');

var _ItemCache2 = _interopRequireDefault(_ItemCache);

var _DataInserter = require('../../DBManager/DataInserter');

var _DataInserter2 = _interopRequireDefault(_DataInserter);

var _SelectItemsModal = require('./SelectItemsModal');

var _SelectItemsModal2 = _interopRequireDefault(_SelectItemsModal);

var _ButtonDropdown = require('./ButtonDropdown');

var _ButtonDropdown2 = _interopRequireDefault(_ButtonDropdown);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MoveItemsToCategoryButton = function (_React$Component) {
	(0, _inherits3.default)(MoveItemsToCategoryButton, _React$Component);

	function MoveItemsToCategoryButton(props) {
		(0, _classCallCheck3.default)(this, MoveItemsToCategoryButton);

		var _this = (0, _possibleConstructorReturn3.default)(this, (MoveItemsToCategoryButton.__proto__ || (0, _getPrototypeOf2.default)(MoveItemsToCategoryButton)).call(this, props));

		_this.state = {
			open: false,
			records: []
		};
		_this.openDialog = _this.openDialog.bind(_this);
		_this.add = _this.add.bind(_this);
		_this.onClose = _this.onClose.bind(_this);
		return _this;
	}

	(0, _createClass3.default)(MoveItemsToCategoryButton, [{
		key: 'onClose',
		value: function onClose() {
			this.setState({
				open: false
			});
		}
	}, {
		key: 'add',
		value: function add(selected) {
			if (!selected.length) {
				ToastHelper.error('select at least one item.');
				return;
			}
			var dataInserter = new _DataInserter2.default();
			var statusCode = dataInserter.updateItemCategoryId(this.props.category.getCategoryId(), selected);
			if (statusCode) {
				ToastHelper.success('Items moved successfully');
				window.onResume && window.onResume();
				this.setState({
					open: false
				});
			} else {
				ToastHelper.error('Unable to move items');
			}
		}
	}, {
		key: 'openDialog',
		value: function openDialog() {
			var itemCache = new _ItemCache2.default();
			var records = itemCache.getItemListNotInCategoryId(this.props.category.getCategoryId());

			if (records.length < 1) {
				ToastHelper.info('This category contains all items');
				return;
			}

			this.setState({
				open: true,
				records: records
			});
		}
	}, {
		key: 'getMenuOptions',
		value: function getMenuOptions() {
			var menuOptions = [];
			menuOptions.push({
				buttonText: 'Move to this category',
				onClick: this.openDialog
			});
			return menuOptions;
		}
	}, {
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				'div',
				null,
				_react2.default.createElement(
					_SelectItemsModal2.default,
					{
						isOpen: this.state.open,
						onSave: this.add,
						onClose: this.onClose,
						items: this.state.records,
						notSelectFirstRow: true },
					' Move to this category '
				),
				_react2.default.createElement(_ButtonDropdown2.default, {
					menuOptions: this.getMenuOptions(),
					color: 'secondary',
					classNames: 'floatRight'
				})
			);
		}
	}]);
	return MoveItemsToCategoryButton;
}(_react2.default.Component);

exports.default = MoveItemsToCategoryButton;