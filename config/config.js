var fs = require('fs');

var app = require('electron').remote.app;

var appPath = app.getPath('userData');
var newDB = fs.readFileSync(appPath + '/BusinessNames/currentDB.txt').toString();

module.exports = {
	'development': {
		'dialect': 'sqlite',
		'storage': newDB,
		'define': {
			timestamps: false
		}
	},
	'test': {
		'dialect': 'sqlite',
		'storage': newDB
	},
	'production': {
		'dialect': 'sqlite',
		'storage': newDB,
		'define': {
			timestamps: false
		}
	},
	'define': {
		timestamps: false
	}
};