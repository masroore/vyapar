var PaymentReminderModel = function PaymentReminderModel() {
	var nameId = void 0;
	var name = void 0;
	var balanceAmount = void 0;
	var remindOnDate = null;
	var sendSMSOnDate = null;

	this.getNameId = function () {
		return nameId;
	};

	this.setNameId = function (NameId) {
		nameId = NameId;
	};

	this.getName = function () {
		return name;
	};

	this.setName = function (Name) {
		name = Name;
	};

	this.getBalanceAmount = function () {
		return balanceAmount;
	};

	this.setBalanceAmount = function (BalanceAmount) {
		balanceAmount = BalanceAmount;
	};

	this.getRemindOnDate = function () {
		return remindOnDate;
	};

	this.setRemindOnDate = function (RemindOnDate) {
		remindOnDate = RemindOnDate;
	};

	this.getSendSMSOnDate = function () {
		return sendSMSOnDate;
	};

	this.setSendSMSOnDate = function (SendSMSOnDate) {
		sendSMSOnDate = SendSMSOnDate;
	};
};

module.exports = PaymentReminderModel;