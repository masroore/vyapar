var TaxCodeMappingModel = function TaxCodeMappingModel() {
	this.taxMappingId;
	this.taxMappingGroupId;
	this.taxMappingCodeId;
	this.taxMappingCreationDate;
	this.taxMappingModificationDate;

	this.getTaxMappingId = function () {
		return this.taxMappingId;
	};
	this.setTaxMappingId = function (taxMappingId) {
		this.taxMappingId = taxMappingId;
	};

	this.getTaxMappingGroupId = function () {
		return this.taxMappingGroupId;
	};
	this.setTaxMappingGroupId = function (taxMappingGroupId) {
		this.taxMappingGroupId = taxMappingGroupId;
	};

	this.getTaxMappingCodeId = function () {
		return this.taxMappingCodeId;
	};
	this.setTaxMappingCodeId = function (taxMappingCodeId) {
		this.taxMappingCodeId = taxMappingCodeId;
	};

	this.getTaxMappingCreationDate = function () {
		return this.taxMappingCreationDate;
	};
	this.setTaxMappingCreationDate = function (taxMappingCreationDate) {
		this.taxMappingCreationDate = taxMappingCreationDate;
	};

	this.getTaxMappingModificationDate = function () {
		return this.taxMappingModificationDate;
	};
	this.setTaxMappingModificationDate = function (taxMappingModificationDate) {
		this.taxMappingModificationDate = taxMappingModificationDate;
	};
};
module.exports = TaxCodeMappingModel;