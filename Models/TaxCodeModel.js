var TaxCodeModel = function TaxCodeModel() {
	this.taxCodeId;
	this.taxCodeName;
	this.taxRate;
	this.taxType;
	this.taxDateCreated;
	this.taxDateModified;

	var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
	var sqliteDBHelper = new SqliteDBHelper();

	this.getTaxCodeId = function () {
		return this.taxCodeId;
	};
	this.setTaxCodeId = function (taxCodeId) {
		this.taxCodeId = taxCodeId;
	};

	this.getTaxCodeName = function () {
		return this.taxCodeName;
	};
	this.setTaxCodeName = function (taxCodeName) {
		this.taxCodeName = taxCodeName;
	};

	this.getTaxRate = function () {
		return this.taxRate;
	};
	this.setTaxRate = function (taxRate) {
		this.taxRate = taxRate;
	};

	this.getTaxType = function () {
		return this.taxType;
	};
	this.setTaxType = function (taxType) {
		this.taxType = taxType;
	};

	this.getTaxDateCreated = function () {
		return this.taxDateCreated;
	};
	this.setTaxDateCreated = function (taxDateCreated) {
		this.taxDateCreated = taxDateCreated;
	};

	this.getTaxDateModified = function () {
		return this.taxDateModified;
	};
	this.setTaxDateModified = function (taxDateModified) {
		this.taxDateModified = taxDateModified;
	};

	this.getTaxCodeMap = function () {
		return this.taxCodeMap;
	};
	this.setTaxCodeMap = function (taxCodeMap) {
		this.taxCodeMap = taxCodeMap;
	};

	this.getTaxRateType = function () {
		return this.taxRateType;
	};
	this.setTaxRateType = function (taxRateType) {
		this.taxRateType = taxRateType;
	};

	this.addNewTaxRate = function () {
		return sqliteDBHelper.addNewTaxRate(this);
	};

	this.updatesTaxRate = function () {
		return sqliteDBHelper.updateTaxRate(this);
	};

	this.canUpdateTaxRate = function () {
		return sqliteDBHelper.canUpdateTaxRate(this);
	};

	this.deleteTaxRate = function () {
		return sqliteDBHelper.deleteTaxRate(this);
	};

	this.canDeleteTaxRate = function () {
		return sqliteDBHelper.canDeleteTaxRate(this);
	};
};

module.exports = TaxCodeModel;