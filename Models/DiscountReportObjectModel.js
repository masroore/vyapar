var DiscountReportObjectModel = function DiscountReportObjectModel() {
	this.nameid = 0;
	this.saleDiscount = 0;
	this.purchaseDiscount = 0;

	this.getNameId = function () {
		return this.nameid;
	};

	this.setNameId = function (nameid) {
		this.nameid = nameid;
	};

	this.getSaleDiscount = function () {
		return this.saleDiscount;
	};

	this.setSaleDiscount = function (saleDiscount) {
		this.saleDiscount = saleDiscount;
	};

	this.getPurchaseDiscount = function () {
		return this.purchaseDiscount;
	};

	this.setPurchaseDiscount = function (purchaseDiscount) {
		this.purchaseDiscount = purchaseDiscount;
	};
};

module.exports = DiscountReportObjectModel;