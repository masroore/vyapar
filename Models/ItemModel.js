var constants;
var ItemModel = function ItemModel() {
	if (!constants) {
		ErrorCode = require('./../Constants/ErrorCode.js');
		ItemType = require('./../Constants/ItemType.js');
	}
	var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
	var sqlitedbhelper = new SqliteDBHelper();
	this.itemId;
	this.itemName;
	this.itemSaleUnitPrice;
	this.itemPurchaseUnitPrice;
	this.itemStockQuantity;
	this.itemMinStockQuantity;
	this.itemLocation;
	this.itemStockValue;
	this.itemOpeningStock;
	this.itemCategoryId;
	this.itemType;
	this.itemCode;
	this.baseUnitId = null;
	this.secondaryUnitId = null;
	this.unitMappingId = null;

	this.itemOpeningStock = 0.0;
	this.itemStockValue = 0.0;
	this.itemType = 1;
	this.itemDescription = '';
	this.itemAtPrice;
	var isItemActive = 1;
	this.itemTaxTypeSale = ItemType.ITEM_TXN_TAX_EXCLUSIVE;
	this.itemTaxTypePurchase = ItemType.ITEM_TXN_TAX_EXCLUSIVE;

	this.getItemAtPrice = function () {
		return this.itemAtPrice;
	};
	this.setItemAtPrice = function (itemAtPrice) {
		this.itemAtPrice = itemAtPrice;
	};

	this.getItemDescription = function () {
		return this.itemDescription;
	};
	this.setItemDescription = function (itemDescription) {
		this.itemDescription = itemDescription;
	};

	this.getItemAdditionalCESS = function () {
		return this.itemAdditionalCess;
	};
	this.setItemAdditionalCESS = function (cess) {
		this.itemAdditionalCess = cess;
	};

	this.getItemHSNCode = function () {
		return this.itemHSNCode;
	};
	this.setItemHSNCode = function (itemHSNCode) {
		this.itemHSNCode = itemHSNCode;
	};

	this.getItemTaxId = function () {
		return this.itemTaxId;
	};
	this.setItemTaxId = function (itemTaxId) {
		this.itemTaxId = itemTaxId;
	};

	this.getItemTaxTypeSale = function () {
		return this.itemTaxTypeSale;
	};
	this.setItemTaxTypeSale = function (itemTaxType) {
		this.itemTaxTypeSale = itemTaxType;
	};

	this.getItemTaxTypePurchase = function () {
		return this.itemTaxTypePurchase;
	};
	this.setItemTaxTypePurchase = function (itemTaxType) {
		this.itemTaxTypePurchase = itemTaxType;
	};

	this.getItemOpeningStock = function () {
		return this.itemOpeningStock;
	};
	this.setItemOpeningStock = function (itemOpeningStock) {
		this.itemOpeningStock = itemOpeningStock;
	};

	this.getItemOpeningDate = function () {
		return this.itemOpeningDate;
	};
	this.setItemOpeningDate = function (itemOpeningDate) {
		this.itemOpeningDate = itemOpeningDate;
	};

	this.getUnitMappingId = function () {
		return this.unitMappingId;
	};
	this.setUnitMappingId = function (unitMappingId) {
		this.unitMappingId = unitMappingId;
	};

	this.getSecondaryUnitId = function () {
		return this.secondaryUnitId;
	};
	this.setSecondaryUnitId = function (secondaryUnitId) {
		this.secondaryUnitId = secondaryUnitId;
	};

	this.getBaseUnitId = function () {
		return this.baseUnitId;
	};
	this.setBaseUnitId = function (baseUnitId) {
		this.baseUnitId = baseUnitId;
	};

	this.getItemCode = function () {
		return this.itemCode;
	};
	this.setItemCode = function (itemCode) {
		this.itemCode = itemCode;
	};

	this.getItemType = function () {
		return this.itemType;
	};
	this.setItemType = function (itemType) {
		this.itemType = itemType;
	};

	this.getItemStockValue = function () {
		return this.itemStockValue;
	};
	this.setItemStockValue = function (itemStockValue) {
		this.itemStockValue = itemStockValue;
	};

	this.getItemLocation = function () {
		return this.itemLocation;
	};
	this.setItemLocation = function (itemLocation) {
		this.itemLocation = itemLocation;
	};

	this.getItemId = function () {
		return this.itemId;
	};
	this.setItemId = function (itemId) {
		this.itemId = itemId;
	};

	this.getIsItemActive = function () {
		return isItemActive;
	};
	this.setIsItemActive = function (IsItemActive) {
		isItemActive = IsItemActive;
	};

	this.getItemName = function () {
		return this.itemName;
	};
	this.setItemName = function (itemName) {
		this.itemName = itemName;
	};

	this.getItemSaleUnitPrice = function () {
		return this.itemSaleUnitPrice;
	};
	this.setItemSaleUnitPrice = function (itemSaleUnitPrice) {
		this.itemSaleUnitPrice = itemSaleUnitPrice;
	};

	this.getItemPurchaseUnitPrice = function () {
		return this.itemPurchaseUnitPrice;
	};
	this.setItemPurchaseUnitPrice = function (itemPurchaseUnitPrice) {
		this.itemPurchaseUnitPrice = itemPurchaseUnitPrice;
	};

	this.getItemStockQuantity = function () {
		return this.itemStockQuantity;
	};
	this.setItemStockQuantity = function (itemStockQuantity) {
		this.itemStockQuantity = itemStockQuantity;
	};

	this.getItemMinStockQuantity = function () {
		return this.itemMinStockQuantity;
	};
	this.setItemMinStockQuantity = function (itemMinStockQuantity) {
		this.itemMinStockQuantity = itemMinStockQuantity;
	};

	this.getITemCategoryId = function () {
		return this.itemCategoryId;
	};
	this.setItemCategoryId = function (categoryId) {
		this.itemCategoryId = categoryId;
	};

	this.addItem = function () {
		var statusCode = ErrorCode['ERROR_ITEM_SAVE_FAILED'];
		if (this.unitMappingId) {
			var ItemUnitMappingCache = require('./../Cache/ItemUnitMappingCache.js');
			var itemUnitMappingCache = new ItemUnitMappingCache();
			var mappingObj = itemUnitMappingCache.getItemUnitMapping(this.unitMappingId);
			var baseUnitId = mappingObj.getBaseUnitId();
			var secondaryUnitId = mappingObj.getSecondaryUnitId();
			this.setBaseUnitId(baseUnitId);
			this.setSecondaryUnitId(secondaryUnitId);
		}
		this.unitMappingId = this.unitMappingId ? this.unitMappingId : null;
		this.baseUnitId = this.baseUnitId ? this.baseUnitId : null;
		this.secondaryUnitId = this.secondaryUnitId ? this.secondaryUnitId : null;
		var retVal = sqlitedbhelper.createItemRecord(this);
		if (retVal > 0) {
			this.setItemId(retVal);
			statusCode = ErrorCode['ERROR_ITEM_SAVE_SUCCESS'];
		}
		return statusCode;
	};

	this.updateItem = function () {
		// console.log(this);
		if (this.unitMappingId) {
			var ItemUnitMappingCache = require('./../Cache/ItemUnitMappingCache.js');
			var itemUnitMappingCache = new ItemUnitMappingCache();
			var mappingObj = itemUnitMappingCache.getItemUnitMapping(this.unitMappingId);
			var baseUnitId = mappingObj.getBaseUnitId();
			var secondaryUnitId = mappingObj.getSecondaryUnitId();
			this.setBaseUnitId(baseUnitId);
			this.setSecondaryUnitId(secondaryUnitId);
		}
		this.unitMappingId = this.unitMappingId ? this.unitMappingId : null;
		this.baseUnitId = this.baseUnitId ? this.baseUnitId : null;
		this.secondaryUnitId = this.secondaryUnitId ? this.secondaryUnitId : null;
		var statusCode = sqlitedbhelper.updateItemRecord(this);
		return statusCode;
	};

	this.deleteItem = function () {
		$('#loading').show();
		var sqliteDBHelper = new SqliteDBHelper();
		var isUsed = sqliteDBHelper.isItemUsed(this.itemId);
		var statusCode = ErrorCode.ERROR_ITEM_USED;

		if (!isUsed) {
			var ItemStockTrackingModel = require('./../Models/ItemStockTrackingModel.js');
			var itemStockTrackingModel = new ItemStockTrackingModel();
			statusCode = itemStockTrackingModel.deleteAllItemStockTrackingForItem(this.itemId);

			if (statusCode == ErrorCode.ERROR_IST_SUCCESS) {
				var ItemImageModel = require('./ItemImagesModel');
				var itemImageModel = new ItemImageModel();
				itemImageModel.deleteAllItemImages(this.itemId);
				statusCode = sqliteDBHelper.deleteItemRecord(this.itemId);
				$('#loading').hide();
			} else {
				statusCode = ErrorCode.ERROR_ITEM_DELETE_FAILED;
				$('#loading').hide();
			}
		}

		return statusCode;
	};

	this.canDeleteItem = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.canDeleteItem(this.getItemId());
	};
};

module.exports = ItemModel;