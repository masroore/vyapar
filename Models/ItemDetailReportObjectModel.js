/**
 * Created by Ashish on 9/21/2017.
 */
var ItemDetailReportObjectModel = function ItemDetailReportObjectModel() {
	this.date;
	this.saleQuantity;
	this.purchaseQuantity;
	this.saleFreeQuantity;
	this.purchaseFreeQuantity;
	this.adjustmentQuantity;
	this.closingQuantity;

	this.getSaleFreeQuantity = function () {
		return this.saleFreeQuantity;
	};
	this.setSaleFreeQuantity = function (saleFreeQuantity) {
		this.saleFreeQuantity = saleFreeQuantity;
	};
	this.getPurchaseFreeQuantity = function () {
		return this.purchaseFreeQuantity;
	};
	this.setPurchaseFreeQuantity = function (purchaseFreeQuantity) {
		this.purchaseFreeQuantity = purchaseFreeQuantity;
	};
	this.getDate = function () {
		return this.date;
	};
	this.setDate = function (date) {
		this.date = date;
	};
	this.getSaleQuantity = function () {
		return this.saleQuantity;
	};
	this.setSaleQuantity = function (saleQuantity) {
		this.saleQuantity = saleQuantity;
	};
	this.getPurchaseQuantity = function () {
		return this.purchaseQuantity;
	};
	this.setPurchaseQuantity = function (purchaseQuantity) {
		this.purchaseQuantity = purchaseQuantity;
	};
	this.getAdjustmentQuantity = function () {
		return this.adjustmentQuantity;
	};
	this.setAdjustmentQuantity = function (adjustmentQuantity) {
		this.adjustmentQuantity = adjustmentQuantity;
	};
	this.getClosingQuantity = function () {
		return this.closingQuantity;
	};
	this.setClosingQuantity = function (closingQuantity) {
		this.closingQuantity = closingQuantity;
	};
};

module.exports = ItemDetailReportObjectModel;