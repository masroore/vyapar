/**
 * Created by Ishwar Rimal on 17-04-2017.
 */
var FirmObject = function FirmObject() {
	var ErrorCode = require('./../Constants/ErrorCode.js');
	var FirmCache = require('./../Cache/FirmCache.js');
	var TransactionManager = require('./../DBManager/TransactionManager.js');
	var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
	this.firmId;
	this.firmName;
	this.firmInvoicePrefix = '';
	this.firmTaxInvoicePrefix = '';
	this.firmInvoiceNumber = 0;
	this.firmTaxInvoiceNumber = 0;
	this.firmDeliveryChallanPrefix = '';
	this.firmEstimateNumber = 0;
	this.firmEstimatePrefix = '';
	this.firmCashInPrefix = '';
	this.firmEmail;
	this.firmPhone;
	this.firmAddress;
	this.firmTin;
	this.firmLogoId;
	this.firmSignId;
	this.firmHSNSAC;
	this.firmState;
	this.bankName;
	this.bankIFSC;
	this.accountNumber;
	this.upiAccountNumber;
	this.upiIFSC;
	this.businessType;
	this.businessCategory;
	var extraFieldObjectArray = [];

	this.getFirmCashInPrefix = function () {
		return this.firmCashInPrefix;
	};

	this.setFirmCashInPrefix = function (firmCashInPrefix) {
		this.firmCashInPrefix = firmCashInPrefix;
	};

	this.getBusinessType = function () {
		return this.businessType;
	};
	this.setBusinessType = function (businessType) {
		this.businessType = businessType;
	};

	this.getBusinessCategory = function () {
		return this.businessCategory;
	};
	this.setBusinessCategory = function (businessCategory) {
		this.businessCategory = businessCategory;
	};

	this.getBankName = function () {
		return this.bankName;
	};
	this.setBankName = function (bankName) {
		this.bankName = bankName;
	};

	this.getBankIFSC = function () {
		return this.bankIFSC;
	};
	this.setBankIFSC = function (bankIFSC) {
		this.bankIFSC = bankIFSC;
	};

	this.getAccountNumber = function () {
		return this.accountNumber;
	};
	this.setAccountNumber = function (accountNumber) {
		this.accountNumber = accountNumber;
	};

	this.getUpiAccountNumber = function () {
		return this.upiAccountNumber;
	};
	this.setUpiAccountNumber = function (upiAccountNumber) {
		this.upiAccountNumber = upiAccountNumber;
	};

	this.getUpiIFSC = function () {
		return this.upiIFSC;
	};
	this.setUpiIFSC = function (upiIFSC) {
		this.upiIFSC = upiIFSC;
	};

	this.getFirmHSNSAC = function () {
		return this.firmHSNSAC;
	};
	this.setFirmHSNSAC = function (HSNSAC) {
		this.firmHSNSAC = HSNSAC;
	};

	this.getFirmState = function () {
		return this.firmState;
	};
	this.setFirmState = function (firmState) {
		this.firmState = firmState;
	};

	this.getFirmImagePath = function () {
		return this.firmImagePath;
	};
	this.setFirmImagePath = function (firmImagePath) {
		this.firmImagePath = firmImagePath;
	};

	this.getFirmSignImagePath = function () {
		return this.firmSignImagePath;
	};
	this.setFirmSignImagePath = function (firmSignImagePath) {
		this.firmSignImagePath = firmSignImagePath;
	};

	this.getFirmSignId = function () {
		return this.firmSignId;
	};

	this.setFirmSignId = function (firmSignId) {
		this.firmSignId = firmSignId;
	};

	this.getFirmEmail = function () {
		return this.firmEmail;
	};

	this.setFirmEmail = function (firmEmail) {
		this.firmEmail = firmEmail;
	};

	this.getFirmPhone = function () {
		return this.firmPhone;
	};

	this.setFirmPhone = function (firmPhone) {
		this.firmPhone = firmPhone;
	};

	this.getFirmAddress = function () {
		return this.firmAddress;
	};

	this.setFirmAddress = function (firmAddress) {
		this.firmAddress = firmAddress;
	};

	this.getFirmTin = function () {
		return this.firmTin;
	};

	this.setFirmTin = function (firmTin) {
		this.firmTin = firmTin;
	};

	this.getFirmLogoId = function () {
		return this.firmLogoId;
	};

	this.setFirmLogoId = function (firmLogoId) {
		this.firmLogoId = firmLogoId;
	};

	this.getFirmId = function () {
		return this.firmId;
	};

	this.setFirmId = function (firmId) {
		this.firmId = firmId;
	};

	this.getFirmName = function () {
		return this.firmName;
	};

	this.setFirmName = function (firmName) {
		this.firmName = firmName;
	};

	this.getFirmInvoicePrefix = function () {
		return this.firmInvoicePrefix;
	};

	this.setFirmInvoicePrefix = function (firmInvoicePrefix) {
		this.firmInvoicePrefix = firmInvoicePrefix;
	};

	this.getFirmTaxInvoicePrefix = function () {
		return this.firmTaxInvoicePrefix;
	};

	this.setFirmTaxInvoicePrefix = function (firmTaxInvoicePrefix) {
		this.firmTaxInvoicePrefix = firmTaxInvoicePrefix;
	};

	this.getFirmInvoiceNumber = function () {
		return this.firmInvoiceNumber;
	};

	this.setFirmInvoiceNumber = function (firmInvoiceNumber) {
		this.firmInvoiceNumber = firmInvoiceNumber;
	};

	this.getFirmTaxInvoiceNumber = function () {
		return this.firmTaxInvoiceNumber;
	};

	this.setFirmTaxInvoiceNumber = function (firmTaxInvoiceNumber) {
		this.firmTaxInvoiceNumber = firmTaxInvoiceNumber;
	};

	this.setFirmDeliveryChallanPrefix = function (prefix) {
		this.firmDeliveryChallanPrefix = prefix;
	};

	this.getFirmDeliveryChallanPrefix = function () {
		return this.firmDeliveryChallanPrefix;
	};

	this.setFirmEstimatePrefix = function (firmEstimatePrefix) {
		this.firmEstimatePrefix = firmEstimatePrefix;
	};

	this.setFirmEstimateNumber = function (firmEstimateNumber) {
		this.firmEstimateNumber = firmEstimateNumber;
	};

	this.getFirmEstimatePrefix = function () {
		return this.firmEstimatePrefix;
	};

	this.getFirmEstimateNumber = function () {
		return this.firmEstimateNumber;
	};

	this.setUdfObjectArray = function (ExtraFieldObjectArray) {
		if (!Array.isArray(ExtraFieldObjectArray)) {
			throw new Error('udf fields are not in an array');
		}
		extraFieldObjectArray = ExtraFieldObjectArray;
	};

	this.getUdfObjectArray = function () {
		return extraFieldObjectArray;
	};

	this.addFirm = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		var statusCode = ErrorCode.ERROR_FIRM_SAVE_FAILED;
		var firmImagePath = this.getFirmImagePath();
		var firmSignImagePath = this.getFirmSignImagePath();
		var sqlitedbhelper = new SqliteDBHelper();
		if (firmImagePath) {
			var txnImgId = sqlitedbhelper.createTransactionImage(firmImagePath);
			if (txnImgId > 0) {
				this.setFirmLogoId(txnImgId);
			} else {
				this.setFirmLogoId(null);
			}
		} else {
			this.setFirmLogoId(null);
		}

		if (firmSignImagePath) {
			var txnImgId = sqlitedbhelper.createTransactionImage(firmSignImagePath);
			if (txnImgId > 0) {
				this.setFirmSignId(txnImgId);
			} else {
				this.setFirmSignId(null);
			}
		} else {
			this.setFirmSignId(null);
		}

		var retVal = sqliteDBHelper.createFirmRecord(this);
		if (retVal > 0) {
			this.setFirmId(retVal);
			statusCode = ErrorCode.ERROR_FIRM_SAVE_SUCCESS;
		} else {
			statusCode = ErrorCode.ERROR_FIRM_SAVE_FAILED;
		}
		return statusCode;
	};

	this.updateFirmWithoutObject = function () {
		var statusCode = ErrorCode.ERROR_FIRM_UPDATE_FAILED;
		var transactionManager = new TransactionManager();
		var sqliteDBHelper = new SqliteDBHelper();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_FIRM_UPDATE_FAILED;
			return statusCode;
		}

		statusCode = sqliteDBHelper.updateFirm(this);
		if (statusCode == ErrorCode.ERROR_FIRM_UPDATE_SUCCESS) {
			if (!transactionManager.CommitTransaction()) {
				statusCode = ErrorCode.ERROR_FIRM_UPDATE_FAILED;
			} else {
				var firmCache = new FirmCache();
				firmCache.refreshFirmCache();
			}
		}
		transactionManager.EndTransaction();
		return statusCode;
	};

	this.updateFirm = function () {
		var withoutImageChanges = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

		var statusCode = ErrorCode.ERROR_FIRM_UPDATE_FAILED;
		var transactionManager = new TransactionManager();
		var sqliteDBHelper = new SqliteDBHelper();

		var isTransactionBeginSuccess = transactionManager.BeginTransaction();

		if (!isTransactionBeginSuccess) {
			statusCode = ErrorCode.ERROR_FIRM_UPDATE_FAILED;
			return statusCode;
		}
		var logoImageToBeRemoved = 0;
		var signImageToBeRemoved = 0;
		if (!withoutImageChanges) {
			var firmImagePath = this.getFirmImagePath();
			var firmSignImagePath = this.getFirmSignImagePath();
			var typeImg = [];
			var txnImgId = 0;
			typeImg = firmImagePath.match('^data');
			// image contains ^data means data has not been modified.
			if (!typeImg) {
				if (Number(this.getFirmLogoId())) {
					if (!firmImagePath) {
						logoImageToBeRemoved = Number(this.getFirmLogoId());
						txnImgId = 0;
						this.setFirmLogoId(txnImgId);
					} else {
						txnImgId = sqliteDBHelper.updateTransactionImage(firmImagePath, this.getFirmLogoId());
					}
				} else if (firmImagePath && firmImagePath.indexOf('.svg') == -1) {
					txnImgId = sqliteDBHelper.createTransactionImage(firmImagePath);
					if (txnImgId > 0) {
						this.setFirmLogoId(txnImgId);
					} else {
						this.setFirmLogoId(null);
					}
				}
			}

			var typeImg = [];
			var txnImgId = 0;
			typeImg = firmSignImagePath.match('^data');
			// image contains ^data means data has not been modified.
			if (!typeImg) {
				if (Number(this.getFirmSignId())) {
					if (!firmSignImagePath) {
						signImageToBeRemoved = Number(this.getFirmSignId());
						txnImgId = 0;
						this.setFirmSignId(txnImgId);
					} else {
						txnImgId = sqliteDBHelper.updateTransactionImage(firmSignImagePath, this.getFirmSignId());
					}
				} else if (firmSignImagePath && firmSignImagePath.indexOf('.svg') == -1) {
					txnImgId = sqliteDBHelper.createTransactionImage(firmSignImagePath);
					if (txnImgId > 0) {
						this.setFirmSignId(txnImgId);
					} else {
						this.setFirmSignId(null);
					}
				}
			}
		}
		statusCode = sqliteDBHelper.updateFirm(this);
		if (statusCode == ErrorCode.ERROR_FIRM_UPDATE_SUCCESS) {
			if (logoImageToBeRemoved) {
				sqliteDBHelper.removeTransactionImage(logoImageToBeRemoved);
			}
			if (signImageToBeRemoved) {
				sqliteDBHelper.removeTransactionImage(signImageToBeRemoved);
			}
			if (!transactionManager.CommitTransaction()) {
				statusCode = ErrorCode.ERROR_FIRM_UPDATE_FAILED;
			} else {
				var firmCache = new FirmCache();
				firmCache.refreshFirmCache();
			}
		}
		transactionManager.EndTransaction();
		return statusCode;
	};
};
module.exports = FirmObject;