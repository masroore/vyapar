var sqliteDBHelperForUDf = null;
var UDFModel = function UDFModel() {
	var args = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	var _args$udfFieldId = args.udfFieldId,
	    udfFieldId = _args$udfFieldId === undefined ? 0 : _args$udfFieldId,
	    _args$udfFieldName = args.udfFieldName,
	    udfFieldName = _args$udfFieldName === undefined ? '' : _args$udfFieldName,
	    _args$udfFieldType = args.udfFieldType,
	    udfFieldType = _args$udfFieldType === undefined ? 0 : _args$udfFieldType,
	    _args$udfFieldDataTyp = args.udfFieldDataType,
	    udfFieldDataType = _args$udfFieldDataTyp === undefined ? 0 : _args$udfFieldDataTyp,
	    _args$udfFieldDataFor = args.udfFieldDataFormat,
	    udfFieldDataFormat = _args$udfFieldDataFor === undefined ? 0 : _args$udfFieldDataFor,
	    _args$udfPrintOnInvoi = args.udfPrintOnInvoice,
	    udfPrintOnInvoice = _args$udfPrintOnInvoi === undefined ? 1 : _args$udfPrintOnInvoi,
	    _args$udfTxnType = args.udfTxnType,
	    udfTxnType = _args$udfTxnType === undefined ? 0 : _args$udfTxnType,
	    _args$udfFirmId = args.udfFirmId,
	    udfFirmId = _args$udfFirmId === undefined ? 1 : _args$udfFirmId,
	    _args$udfFieldNumber = args.udfFieldNumber,
	    udfFieldNumber = _args$udfFieldNumber === undefined ? 0 : _args$udfFieldNumber,
	    _args$udfFieldStatus = args.udfFieldStatus,
	    udfFieldStatus = _args$udfFieldStatus === undefined ? 0 : _args$udfFieldStatus,
	    _args$udfFieldValue = args.udfFieldValue,
	    udfFieldValue = _args$udfFieldValue === undefined ? '' : _args$udfFieldValue,
	    _args$udfRefId = args.udfRefId,
	    udfRefId = _args$udfRefId === undefined ? 0 : _args$udfRefId;


	this.getUdfFieldId = function () {
		return udfFieldId;
	};
	this.setUdfFieldId = function (UdfFieldId) {
		udfFieldId = UdfFieldId;
	};

	this.getUdfFieldName = function () {
		return udfFieldName;
	};
	this.setUdfFieldName = function (UdfFieldName) {
		udfFieldName = UdfFieldName;
	};

	this.getUdfFieldType = function () {
		return udfFieldType;
	};
	this.setUdfFieldType = function (UdfFieldType) {
		udfFieldType = UdfFieldType;
	};

	this.getUdfFieldDataType = function () {
		return udfFieldDataType;
	};
	this.setUdfFieldDataType = function (UdfFieldDataType) {
		udfFieldDataType = UdfFieldDataType;
	};

	this.getUdfFieldDataFormat = function () {
		return udfFieldDataFormat;
	};
	this.setUdfFieldDataFormat = function (UdfFieldDataFormat) {
		udfFieldDataFormat = UdfFieldDataFormat;
	};

	this.getUdfFieldPrintOnInvoice = function () {
		return udfPrintOnInvoice;
	};
	this.setUdfFieldPrintOnInvoice = function (UdfPrintOnInvoice) {
		udfPrintOnInvoice = UdfPrintOnInvoice;
	};

	this.getUdfTxnType = function () {
		return udfTxnType;
	};
	this.setUdfTxnType = function (UdfTxnType) {
		udfTxnType = UdfTxnType;
	};

	this.getUdfFirmId = function () {
		return udfFirmId;
	};
	this.setUdfFirmId = function (UdfFirmId) {
		udfFirmId = UdfFirmId;
	};

	this.getUdfFieldNumber = function () {
		return udfFieldNumber;
	};
	this.setUdfFieldNuber = function (UdfFieldNumber) {
		udfFieldNumber = UdfFieldNumber;
	};

	this.getUdfFieldStatus = function () {
		return udfFieldStatus;
	};
	this.setUdfFieldStatus = function (UdfFieldStatus) {
		udfFieldStatus = UdfFieldStatus;
	};

	this.getUdfFieldValue = function () {
		return udfFieldValue;
	};
	this.setUdfFieldValue = function (UdfFieldValue) {
		udfFieldValue = UdfFieldValue;
	};

	this.getUdfRefId = function () {
		return udfRefId;
	};
	this.setUdfRefId = function (UdfRefId) {
		udfRefId = UdfRefId;
	};
};
module.exports = UDFModel;