/**
 * Created by Ashish on 9/21/2017.
 */
var StockDetailReportObjectModel = function StockDetailReportObjectModel() {
	this.date;
	this.itemId;
	this.quantityIn;
	this.qunatityOut;
	this.openingQuantity;

	this.getDate = function () {
		return this.date;
	};
	this.setDate = function (date) {
		this.date = date;
	};
	this.getItemId = function () {
		return this.itemId;
	};
	this.setItemId = function (itemId) {
		this.itemId = itemId;
	};
	this.getQuantityIn = function () {
		return this.quantityIn;
	};
	this.setQuantityIn = function (quantityIn) {
		this.quantityIn = quantityIn;
	};
	this.getQuantityOut = function () {
		return this.qunatityOut;
	};
	this.setQuantityOut = function (qunatityOut) {
		this.qunatityOut = qunatityOut;
	};

	this.getOpeningQuantity = function () {
		return this.openingQuantity;
	};
	this.setOpeningQuantity = function (openingQuantity) {
		this.openingQuantity = openingQuantity;
	};
};

module.exports = StockDetailReportObjectModel;