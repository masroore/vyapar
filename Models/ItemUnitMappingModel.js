var ItemUnitMappingModel = function ItemUnitMappingModel() {
	var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
	var Queries = require('./../Constants/Queries.js');

	this.mappingId;
	this.baseUnitId;
	this.secondaryUnitId;
	this.conversionRate;

	this.getMappingId = function () {
		return this.mappingId;
	};

	this.setMappingId = function (mappingId) {
		this.mappingId = mappingId;
	};

	this.getBaseUnitId = function () {
		return this.baseUnitId;
	};

	this.setBaseUnitId = function (baseUnitId) {
		this.baseUnitId = baseUnitId;
	};

	this.getSecondaryUnitId = function () {
		return this.secondaryUnitId;
	};

	this.setSecondaryUnitId = function (secondaryUnitId) {
		this.secondaryUnitId = secondaryUnitId;
	};

	this.getConversionRate = function () {
		return this.conversionRate;
	};

	this.setConversionRate = function (conversionRate) {
		this.conversionRate = conversionRate;
	};

	this.addItemUnitMapping = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		var errorCode = ErrorCode.SUCCESS;
		var retVal = sqliteDBHelper.createItemUnitMapping(this);
		if (retVal > 0) {
			this.setMappingId(retVal);
			errorCode = ErrorCode.ERROR_UNIT_MAPPING_SAVE_SUCCESS;
		} else {
			errorCode = ErrorCode.ERROR_UNIT_MAPPING_SAVE_FAILED;
		}
		return errorCode;
	};

	this.updateItemUnitMapping = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.updateItemUnitMapping(this);
	};

	this.isItemUnitMappingUsed = function (mappingId) {
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.isItemUnitMappingUsed(mappingId);
	};

	this.deleteUnitMapping = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		var isUsed = sqliteDBHelper.isItemUnitMappingUsed(mappingId);
		var statusCode = ErrorCode.ERROR_UNIT_MAPPING_IS_USED;

		if (!isUsed) {
			statusCode = sqliteDBHelper.deleteItemUnitMapping(this.mappingId);
		}
		return statusCode;
	};

	this.getDefaultMappingValue = function () {
		var queryStrings = [];
		queryStrings.push('insert into ' + Queries.DB_TABLE_ITEM_UNITS_MAPPING + ' values(1, 1, 2, 1000)');
		queryStrings.push('insert into ' + Queries.DB_TABLE_ITEM_UNITS_MAPPING + ' values(2, 3, 4, 1000)');
		queryStrings.push('insert into ' + Queries.DB_TABLE_ITEM_UNITS_MAPPING + ' values(3, 5, 6, 100)');
		return queryStrings;
	};
};

module.exports = ItemUnitMappingModel;