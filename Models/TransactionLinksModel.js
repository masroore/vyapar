/**
 * Created by Ashish on 2/21/2018.
 */
var TransactionLinksModel = function TransactionLinksModel() {
	this.txnLinksId;
	this.txnLinksTxn1Id;
	this.txnLinksTxn2Id;
	this.txnLinksAmount;
	this.txnLinksTxn1Type;
	this.txnLinksTxn2Type;
	this.txnLinkClosedTxnRefId;

	var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
	var sqlitedbhelper = new SqliteDBHelper();

	this.getTxnLinksId = function () {
		return this.txnLinksId;
	};
	this.setTxnLinksId = function (txnLinksId) {
		this.txnLinksId = txnLinksId;
	};

	this.getTxnLinksTxn1Id = function () {
		return this.txnLinksTxn1Id;
	};
	this.setTxnLinksTxn1Id = function (txnLinksTxn1Id) {
		this.txnLinksTxn1Id = txnLinksTxn1Id;
	};

	this.getTxnLinksTxn2Id = function () {
		return this.txnLinksTxn2Id;
	};
	this.setTxnLinksTxn2Id = function (txnLinksTxn2Id) {
		this.txnLinksTxn2Id = txnLinksTxn2Id;
	};

	this.getTxnLinksAmount = function () {
		return this.txnLinksAmount;
	};
	this.setTxnLinksAmount = function (txnLinksAmount) {
		this.txnLinksAmount = txnLinksAmount;
	};

	this.getTxnLinksTxn1Type = function () {
		return this.txnLinksTxn1Type;
	};
	this.setTxnLinksTxn1Type = function (txnLinksTxn1Type) {
		this.txnLinksTxn1Type = txnLinksTxn1Type;
	};

	this.getTxnLinksTxn2Type = function () {
		return this.txnLinksTxn2Type;
	};
	this.setTxnLinksTxn2Type = function (txnLinksTxn2Type) {
		this.txnLinksTxn2Type = txnLinksTxn2Type;
	};

	this.getTxnLinkClosedTxnRefId = function () {
		return this.txnLinkClosedTxnRefId;
	};

	this.setTxnLinkClosedTxnRefId = function (txnLinkClosedTxnRefId) {
		this.txnLinkClosedTxnRefId = txnLinkClosedTxnRefId;
	};

	this.createTransactionLinks = function () {
		var txnLinkId = sqlitedbhelper.createTransactionLinksRecord(this);

		if (txnLinkId) {
			return ErrorCode.ERROR_TXN_LINK_CREATE_SUCCESS;
		} else {
			return ErrorCode.ERROR_TXN_LINK_CREATE_FAILED;
		}
	};

	this.deleteTransactionLinkByLinkId = function (linkId) {
		var statusCode = sqlitedbhelper.deleteTransactionLinkByLinkId(linkId);

		return statusCode;
	};

	this.isTransactionLinked = function (txnId) {
		var statusCode = sqlitedbhelper.isTransactionLinkedByTxnId(txnId);

		return statusCode;
	};
};

module.exports = TransactionLinksModel;