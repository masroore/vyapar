var instance;
var BankAdjustmentTxnModel = function BankAdjustmentTxnModel() {
	this.adjId;
	this.adjBankId;
	this.adjType;
	this.adjAmount;
	this.adjDescription;
	this.adjDate;

	var ErrorCode = require('./../Constants/ErrorCode.js');
	if (!instance) {
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqlitedbhelper = new SqliteDBHelper();
	}

	this.getAdjId = function () {
		return this.adjId;
	};
	this.setAdjId = function (adjId) {
		this.adjId = adjId;
	};

	this.getAdjBankId = function () {
		return this.adjBankId;
	};
	this.setAdjBankId = function (adjBankId) {
		this.adjBankId = adjBankId;
	};

	this.getAdjToBankId = function () {
		return this.adjToBankId;
	};
	this.setAdjToBankId = function (adjToBankId) {
		this.adjToBankId = adjToBankId;
	};

	this.getAdjType = function () {
		return this.adjType;
	};
	this.setAdjType = function (adjType) {
		this.adjType = adjType;
	};

	this.getAdjAmount = function () {
		return this.adjAmount;
	};
	this.setAdjAmount = function (adjAmount) {
		this.adjAmount = adjAmount;
	};

	this.getAdjDescription = function () {
		return this.adjDescription;
	};
	this.setAdjDescription = function (adjDescription) {
		this.adjDescription = adjDescription;
	};

	this.getAdjDate = function () {
		return this.adjDate;
	};
	this.setAdjDate = function (adjDate) {
		this.adjDate = adjDate;
	};

	this.createAdjustment = function () {
		var statusCode = ErrorCode.ERROR_NEW_BANK_ADJUSTMENT_FAILED;
		var retVal = sqlitedbhelper.createBankAdjTxn(this);
		console.log(retVal);
		if (retVal > 0) {
			this.setAdjId(retVal);
			statusCode = ErrorCode.ERROR_NEW_BANK_ADJUSTMENT_SUCCESS;
		}
		return statusCode;
	};

	this.updateAdjustment = function () {
		return sqlitedbhelper.updateBankAdjTxn(this);
	};

	this.deleteAdjTxn = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.deleteBankAdjTxn(this.getAdjId());
	};
};
module.exports = BankAdjustmentTxnModel;