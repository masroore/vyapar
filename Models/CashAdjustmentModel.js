var CashAdjustmentModel = function CashAdjustmentModel() {
	var adjType, adjAmount, adjDate, adjDescription;
	var ErrorCode = require('./../Constants/ErrorCode.js');
	this.adjAmount = 0;

	this.getAdjId = function () {
		return this.adjId;
	};
	this.setAdjId = function (adjId) {
		this.adjId = adjId;
	};

	this.getAdjType = function () {
		return this.adjType;
	};
	this.setAdjType = function (adjType) {
		this.adjType = adjType;
	};

	this.getAdjAmount = function () {
		return this.adjAmount;
	};
	this.setAdjAmount = function (adjAmount) {
		this.adjAmount = adjAmount;
	};

	this.getAdjDate = function () {
		return this.adjDate;
	};
	this.setAdjDate = function (adjDate) {
		this.adjDate = adjDate;
	};

	this.getAdjDescription = function () {
		return this.adjDescription;
	};
	this.setAdjDescription = function (adjDescription) {
		this.adjDescription = adjDescription;
	};

	this.saveNewAdjustment = function () {
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqliteDBHelper = new SqliteDBHelper();

		var statusCode = sqliteDBHelper.saveNewCashAdjustment(this);
		return statusCode;
	};

	this.updateCashAdjustment = function () {
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqliteDBHelper = new SqliteDBHelper();
		var statusCode = ErrorCode.ERROR_UPDATE_CASH_ADJUSTMENT_FAILED;
		var statusCode = sqliteDBHelper.updateCashAdjustment(this);
		if (statusCode) {
			var statusCode = ErrorCode.ERROR_UPDATE_CASH_ADJUSTMENT_SUCCESS;
		}
		return statusCode;
	};

	this.deleteAdjTxn = function () {
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.deleteCashAdjTxn(this.getAdjId());
	};
};

module.exports = CashAdjustmentModel;