var PrefixModel = function PrefixModel() {
	var prefixId = void 0;
	var firmId = void 0;
	var txnType = void 0;
	var prefixValue = '';
	var isDefault = 0;

	this.getPrefixId = function () {
		return prefixId;
	};
	this.setPrefixId = function (prefix_id) {
		prefixId = prefix_id;
	};
	this.getFirmId = function () {
		return firmId;
	};
	this.setFirmId = function (firm_id) {
		firmId = firm_id;
	};
	this.getTxnType = function () {
		return txnType;
	};
	this.setTxnType = function (txn_type) {
		txnType = txn_type;
	};
	this.getPrefixValue = function () {
		return prefixValue;
	};
	this.setPrefixValue = function (prefix_value) {
		prefixValue = prefix_value;
	};
	this.getIsDefault = function () {
		return isDefault;
	};
	this.setIsDefault = function (is_default) {
		isDefault = is_default;
	};
	this.addNewPrefix = function () {
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.addNewPrefix(this);
	};
	this.updatePrefixValue = function () {
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.updatePrefixValue(this);
	};

	this.removeSelectedPrefix = function () {
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.removeSelectedPrefix(this);
	};
};
module.exports = PrefixModel;