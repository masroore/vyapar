var ClosedLinkTxnModel = function ClosedLinkTxnModel() {
	this.closedLinkTxnId;
	this.closedLinkTxnDate;
	this.closedLinkTxnAmount;
	this.closedLinkTxnType;
	this.closedLinkTxnRefNumber;

	var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
	var sqlitedbhelper = new SqliteDBHelper();

	this.getClosedLinkTxnId = function () {
		return this.closedLinkTxnId;
	};

	this.setClosedLinkTxnId = function (closedLinkTxnId) {
		this.closedLinkTxnId = closedLinkTxnId;
	};

	this.getClosedLinkTxnDate = function () {
		return this.closedLinkTxnDate;
	};

	this.setClosedLinkTxnDate = function (closedLinkTxnDate) {
		this.closedLinkTxnDate = closedLinkTxnDate;
	};

	this.getClosedLinkTxnAmount = function () {
		return this.closedLinkTxnAmount;
	};

	this.setClosedLinkTxnAmount = function (closedLinkTxnAmount) {
		this.closedLinkTxnAmount = closedLinkTxnAmount;
	};

	this.getClosedLinkTxnType = function () {
		return this.closedLinkTxnType;
	};

	this.setClosedLinkTxnType = function (closedLinkTxnType) {
		this.closedLinkTxnType = closedLinkTxnType;
	};

	this.getClosedLinkTxnRefNumber = function () {
		return this.closedLinkTxnRefNumber;
	};

	this.setClosedLinkTxnRefNumber = function (closedLinkTxnRefNumber) {
		this.closedLinkTxnRefNumber = closedLinkTxnRefNumber;
	};

	this.createClosedLinkTxnDetail = function () {
		var statusCode = sqlitedbhelper.createClosedTxnLinkDetail(this);

		return statusCode;
	};

	this.deleteClosedLinkTxnDetail = function (closedTxnId) {
		var statusCode = sqlitedbhelper.deleteClosedLinkTxnDetail(closedTxnId);

		return statusCode;
	};
};

module.exports = ClosedLinkTxnModel;