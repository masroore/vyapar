var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var instance = 0;
var macAd = null;
var SettingsModel = function SettingsModel() {
	var ErrorCode = require('./../Constants/ErrorCode.js');
	var Defaults = require('./../Constants/Defaults.js');
	var InvoiceTheme = require('./../Constants/InvoiceTheme.js');

	var TransactionPDFPaperSize = require('./../Constants/TransactionPDFPaperSize.js');
	var TransactionManager = require('./../DBManager/TransactionManager.js');
	var trasnactionManager = new TransactionManager();
	var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
	var StringConstants = require('./../Constants/StringConstants.js');
	var sqlitedbhelper = new SqliteDBHelper();
	this.settingId;
	this.settingValue;
	this.settingKey;

	this.getSettingId = function () {
		return this.settingId;
	};
	this.setSettingId = function (settingId) {
		this.settingId = settingId;
	};

	this.getSettingValue = function () {
		return this.settingValue;
	};
	this.setSettingValue = function (settingValue) {
		this.settingValue = settingValue;
	};

	this.getSettingKey = function () {
		return this.settingKey;
	};
	this.setSettingKey = function (settingKey) {
		this.settingKey = settingKey;
	};

	this.UpdateSetting = function (value) {
		var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

		var i = 0;
		var statusCode = void 0;
		if (!trasnactionManager.BeginTransaction()) {
			statusCode = ErrorCode.ERROR_SETTING_SAVE_FAILED;
			return statusCode;
		}

		var settingToBeSynced = !(options && options.nonSyncableSetting);
		var settingCacheToBeRefreshed = !(options && options.refreshOnSuccess == false);

		this.setSettingValue(value);
		statusCode = sqlitedbhelper.updateSetting(this, settingToBeSynced);

		if (statusCode == ErrorCode.ERROR_SETTING_SAVE_SUCCESS) {
			if (!trasnactionManager.CommitTransaction()) {
				statusCode = ErrorCode.ERROR_SETTING_SAVE_FAILED;
			} else {
				if (settingCacheToBeRefreshed) {
					var _SettingCache = require('./../Cache/SettingCache.js');
					var settingCache = new _SettingCache();
					settingCache.refreshSettingsCache(this);
				}
			}
		}
		trasnactionManager.EndTransaction();

		return statusCode;
	};

	this.updateMultipleSettings = function (values) {
		var statusCode = void 0;
		var allSettingsSavedSuccessFully = true;
		if (values && (0, _keys2.default)(values).length && (0, _keys2.default)(values).length > 0) {
			if (!trasnactionManager.BeginTransaction()) {
				statusCode = ErrorCode.ERROR_SETTING_SAVE_FAILED;
				return statusCode;
			}
			for (var key in values) {
				this.setSettingKey(key);
				this.setSettingValue(values[key]);
				statusCode = sqlitedbhelper.updateSetting(this);
				if (statusCode != ErrorCode.ERROR_SETTING_SAVE_SUCCESS) {
					allSettingsSavedSuccessFully = false;
					break;
				}
			}
			if (allSettingsSavedSuccessFully) {
				if (!trasnactionManager.CommitTransaction()) {
					allSettingsSavedSuccessFully = false;
				}
			}
			trasnactionManager.EndTransaction();
		}
		if (allSettingsSavedSuccessFully) {
			var settingCache = new SettingCache();
			settingCache.reloadSettingCache();
			return ErrorCode.ERROR_SETTING_SAVE_SUCCESS;
		} else {
			return ErrorCode.ERROR_SETTING_SAVE_FAILED;
		}
	};

	this.getSettingsKey = function () {
		var settingsKeys = [];
		var Queries = require('./../Constants/Queries.js');
		settingsKeys.push(Queries.SETTING_ITEM_ENABLED);
		settingsKeys.push(Queries.SETTING_TAX_ENABLED);
		settingsKeys.push(Queries.SETTING_DISCOUNT_ENABLED);
		settingsKeys.push(Queries.SETTING_PASSCODE_ENABLED);
		settingsKeys.push(Queries.SETTING_DELETE_PASSCODE_ENABLED);
		settingsKeys.push(Queries.SETTING_DELETE_PASSCODE);
		settingsKeys.push(Queries.SETTING_LAST_BACKUP_TIME);
		settingsKeys.push(Queries.SETTING_BACKUP_REMINDER_DAYS);
		settingsKeys.push(Queries.SETTING_LAST_BACKUP_REMINDER_TIME);
		settingsKeys.push(Queries.SETTING_BACKUP_REMINDER_SNOOZE);
		settingsKeys.push(Queries.SETTING_CURRENCY_SYMBOL);
		settingsKeys.push(Queries.SETTING_STOCK_ENABLED);
		settingsKeys.push(Queries.SETTING_TXNREFNO_ENABLED);
		settingsKeys.push(Queries.SETTING_TXNREFNO_MAXVALUE);
		settingsKeys.push(Queries.SETTING_PARTY_ITEM_RATE);
		settingsKeys.push(Queries.SETTING_PAYMENTREMIDNER_ENABLED);
		settingsKeys.push(Queries.SETTING_PAYMENT_REMINDER_DAYS);
		settingsKeys.push(Queries.SETTING_CUSTOM_PAYMENT_REMINDER_MESSAGE);
		settingsKeys.push(Queries.SETTING_TRANSACTION_MESSAGE_ENABLED);
		settingsKeys.push(Queries.SETTING_ADDRESS);
		settingsKeys.push(Queries.SETTING_PRINT_TINNUMBER);
		settingsKeys.push(Queries.SETTING_QUANTITY_DECIMAL);
		settingsKeys.push(Queries.SETTING_ITEMWISE_TAX_ENABLED);
		settingsKeys.push(Queries.SETTING_ITEMWISE_DISCOUNT_ENABLED);
		settingsKeys.push(Queries.SETTING_ORDER_FORM_ENABLED);
		settingsKeys.push(Queries.SETTING_ESTIMATE_ENABLED);
		settingsKeys.push(Queries.SETTING_DELIVERY_CHALLAN_ENABLED);
		settingsKeys.push(Queries.SETTING_DELIVERY_CHALLAN_RETURN_ENABLED);
		settingsKeys.push(Queries.SETTING_AMOUNT_DECIMAL);
		settingsKeys.push(Queries.SETTING_DISCOUNT_IN_MONEY_TXN);
		settingsKeys.push(Queries.SETTING_AUTO_BACKUP_ENABLED);
		settingsKeys.push(Queries.SETTING_AUTO_BACKUP_LAST_BACKUP);
		settingsKeys.push(Queries.SETTING_AUTO_BACKUP_DURATION_DAYS);
		settingsKeys.push(Queries.SETTING_REMIND_RATING);
		settingsKeys.push(Queries.SETTING_PARTY_GROUP);
		settingsKeys.push(Queries.SETTING_COMPANY_LOGO_ID);
		settingsKeys.push(Queries.SETTING_ITEM_CATEGORY);
		settingsKeys.push(Queries.SETTING_AC_ENABLED);
		settingsKeys.push(Queries.SETTING_AC1_ENABLED);
		settingsKeys.push(Queries.SETTING_AC2_ENABLED);
		settingsKeys.push(Queries.SETTING_AC3_ENABLED);
		settingsKeys.push(Queries.SETTING_EXTRA_SPACE_ON_TXN_PDF);
		settingsKeys.push(Queries.SETTING_PRINT_LOGO_ON_TXN_PDF);
		settingsKeys.push(Queries.SETTING_PRINT_COMPANY_NAME_ON_TXN_PDF);
		settingsKeys.push(Queries.SETTING_PRINT_COMPANY_ADDRESS_ON_TXN_PDF);
		settingsKeys.push(Queries.SETTING_PRINT_COMPANY_NUMBER_ON_TXN_PDF);
		settingsKeys.push(Queries.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF);
		settingsKeys.push(Queries.SETTING_TERMS_AND_CONDITIONS);
		settingsKeys.push(Queries.SETTING_PRINT_DESCRIPTION_ON_TXN_PDF);
		settingsKeys.push(Queries.SETTING_TXN_PDF_THEME);
		settingsKeys.push(Queries.SETTING_TXN_PDF_THEME_COLOR);
		settingsKeys.push(Queries.SETTING_TXN_MSG_TO_OWNER);
		settingsKeys.push(Queries.SETTING_TXN_MSG_OWNER_NUMBER);
		settingsKeys.push(Queries.SETTING_PUSH_SETTING_TO_CLEVERTAP_DESKTOP);
		settingsKeys.push(Queries.SETTING_TIN_NUMBER_ENABLED);
		settingsKeys.push(Queries.SETTING_SIGNATURE_ENABLED);
		settingsKeys.push(Queries.SETTING_SIGNATURE_ID);
		settingsKeys.push(Queries.SETTING_IS_ZERO_BAL_PARTY_ALLOWED_IN_RECEIVABLE);
		settingsKeys.push(Queries.SETTING_MULTI_LANGUAGE_ENABLED);
		settingsKeys.push(Queries.SETTING_IS_ITEM_UNIT_ENABLED);
		settingsKeys.push(Queries.SETTING_TAXINVOICE_ENABLED);
		settingsKeys.push(Queries.SETTING_MULTIFIRM_ENABLED);
		settingsKeys.push(Queries.SETTING_DEFAULT_FIRM_ID);
		settingsKeys.push(Queries.SETTING_FIRST_TIME_LOGIN_DATE);
		settingsKeys.push(Queries.SETTING_FREE_TRIAL_START_DATE);
		settingsKeys.push(Queries.SETTING_FREE_TRIAL_START_DATE_DESKTOP);
		settingsKeys.push(Queries.SETTING_BARCODE_SCANNING_ENABLED);
		settingsKeys.push(Queries.SETTING_CUSTOM_NAME_FOR_PURCHASE);
		settingsKeys.push(Queries.SETTING_CUSTOM_NAME_FOR_DELIVERY_CHALLAN);
		settingsKeys.push(Queries.SETTING_CUSTOM_NAME_FOR_SALE);
		settingsKeys.push(Queries.SETTING_CUSTOM_NAME_FOR_CASH_IN);
		settingsKeys.push(Queries.SETTING_CUSTOM_NAME_FOR_CASH_OUT);
		settingsKeys.push(Queries.SETTING_CUSTOM_NAME_FOR_PURCHASE_RETURN);
		settingsKeys.push(Queries.SETTING_CUSTOM_NAME_FOR_SALE_RETURN);
		settingsKeys.push(Queries.SETTING_CUSTOM_NAME_FOR_EXPENSE);
		settingsKeys.push(Queries.SETTING_CUSTOM_NAME_FOR_INCOME);
		settingsKeys.push(Queries.SETTING_CUSTOM_NAME_FOR_SALE_ORDER);
		settingsKeys.push(Queries.SETTING_CUSTOM_NAME_FOR_PURCHASE_ORDER);
		settingsKeys.push(Queries.SETTING_CUSTOM_NAME_FOR_TAX_INVOICE);
		settingsKeys.push(Queries.SETTING_CUSTOM_NAME_FOR_ESTIMATE);
		settingsKeys.push(Queries.SETTING_GST_ENABLED);
		settingsKeys.push(Queries.SETTING_HSN_SAC_ENABLED);
		settingsKeys.push(Queries.SETTING_LEADS_INFO_SENT);
		settingsKeys.push(Queries.SETTING_USER_COUNTRY);
		settingsKeys.push(Queries.SETTING_BARCODE_SCANNER_TYPE);
		settingsKeys.push(Queries.SETTING_PRINT_TAX_DETAILS);
		settingsKeys.push(Queries.SETTING_MIN_ITEM_ROWS_ON_TXN_PDF);
		settingsKeys.push(Queries.SETTING_AMOUNT_IN_WORD_FORMAT);
		settingsKeys.push(Queries.SETTING_PRINT_TEXT_SIZE);
		settingsKeys.push(Queries.SETTING_PRINT_COMPANY_NAME_TEXT_SIZE);
		settingsKeys.push(Queries.SETTING_PRINT_PAGE_SIZE);
		settingsKeys.push(Queries.SETTING_PARTY_SHIPPING_ADDRESS_ENABLED);
		settingsKeys.push(Queries.SETTING_PRINT_PARTY_SHIPPING_ADDRESS);
		settingsKeys.push(Queries.SETTING_SHOW_LAST_FIVE_SALE_PRICE);
		// settingsKeys.push(Queries.SETTING_USER_BANK_IFSC);
		// settingsKeys.push(Queries.SETTING_USER_BANK_NAME);
		// settingsKeys.push(Queries.SETTING_USER_BANK_ACCOUNT_NUMBER);
		settingsKeys.push(Queries.SETTING_PRINT_BANK_DETAIL);
		settingsKeys.push(Queries.SETTING_SHOW_PURCHASE_PRICE_IN_ITEM_DROP_DOWN);
		settingsKeys.push(Queries.SETTING_SIGNATURE_TEXT);
		settingsKeys.push(Queries.SETTING_PRINT_COPY_NUMBER);
		settingsKeys.push(Queries.SETTING_ENABLE_DISPLAY_NAME);
		settingsKeys.push(Queries.SETTING_ENABLE_ITEM_MRP);
		settingsKeys.push(Queries.SETTING_ENABLE_ITEM_BATCH_NUMBER);
		settingsKeys.push(Queries.SETTING_ENABLE_ITEM_EXPIRY_DATE);
		settingsKeys.push(Queries.SETTING_ENABLE_ITEM_MANUFACTURING_DATE);
		settingsKeys.push(Queries.SETTING_ENABLE_SERIAL_ITEM_NUMBER);
		settingsKeys.push(Queries.SETTING_ENABLE_ITEM_COUNT);
		settingsKeys.push(Queries.SETTING_ENABLE_ITEM_DESCRIPTION);
		settingsKeys.push(Queries.SETTING_ITEM_MRP_VALUE);
		settingsKeys.push(Queries.SETTING_ITEM_BATCH_NUMBER_VALUE);
		settingsKeys.push(Queries.SETTING_ITEM_EXPIRY_DATE_VALUE);
		settingsKeys.push(Queries.SETTING_ITEM_MANUFACTURING_DATE_VALUE);
		settingsKeys.push(Queries.SETTING_ITEM_SERIAL_NUMBER_VALUE);
		settingsKeys.push(Queries.SETTING_ITEM_COUNT_VALUE);
		settingsKeys.push(Queries.SETTING_ITEM_DESCRIPTION_VALUE);
		settingsKeys.push(Queries.SETTING_EXPIRY_DATE_TYPE);
		settingsKeys.push(Queries.SETTING_MANUFACTURING_DATE_TYPE);
		settingsKeys.push(Queries.SETTING_ENABLE_ADDITIONAL_CESS_ON_ITEM);
		settingsKeys.push(Queries.SETTING_ENABLE_REVERSE_CHARGE);
		settingsKeys.push(Queries.SETTING_SHOW_CURRENT_BALANCE_OF_PARTY);
		settingsKeys.push(Queries.SETTING_SHOW_RECEIVED_AMOUNT_OF_TRANSACTION);
		settingsKeys.push(Queries.SETTING_SHOW_BALANCE_AMOUNT_OF_TRANSACTION);
		settingsKeys.push(Queries.SETTING_PRINT_COMPANY_EMAIL_ON_PDF);
		settingsKeys.push(Queries.SETTING_ENABLE_DEFAULT_CASH_SALE);
		settingsKeys.push(Queries.SETTING_ITEM_TYPE);
		settingsKeys.push(Queries.SETTING_ENABLE_EDITING_TOTAL_AMOUNT);
		settingsKeys.push(Queries.SETTING_ENABLE_PLACE_OF_SUPPLY);
		settingsKeys.push(Queries.SETTING_ENABLE_EWAY_BILL_NUMBER);
		settingsKeys.push(Queries.SETTING_PRINT_ITEM_QUANTITY_TOTAL_ON_TXN_PDF);
		settingsKeys.push(Queries.SETTING_IS_ROUND_OFF_ENABLED);
		settingsKeys.push(Queries.SETTING_ROUND_OFF_TYPE);
		settingsKeys.push(Queries.SETTING_ROUND_OFF_UPTO);
		settingsKeys.push(Queries.SETTING_PO_DETAILS_ENABLED);
		settingsKeys.push(Queries.SETTING_IS_COMPOSITE_SCHEME_ENABLED);
		settingsKeys.push(Queries.SETTING_COMPOSITE_USER_TYPE);
		settingsKeys.push(Queries.SETTING_CURRENT_DATE_FORMAT);
		settingsKeys.push(Queries.SETTING_ENABLE_ITEM_SIZE);
		settingsKeys.push(Queries.SETTING_ITEM_SIZE_VALUE);
		settingsKeys.push(Queries.SETTING_COMPANY_GLOBAL_ID);
		settingsKeys.push(Queries.SETTING_CHANGE_LOG_NUMBER);
		settingsKeys.push(Queries.SETTING_SYNC_ENABLED);
		settingsKeys.push(Queries.SETTING_FREE_QTY_ENABLED);
		settingsKeys.push(Queries.SETTING_PRINT_SINNUMBER_ENABLED);
		settingsKeys.push(Queries.SETTING_PRINT_HSNCODE_ENABLED);
		settingsKeys.push(Queries.SETTING_PRINT_ITEMCODE_ENABLED);
		settingsKeys.push(Queries.SETTING_PRINT_ITEMCOUNT_ENABLED);
		settingsKeys.push(Queries.SETTING_PRINT_BATCHNO_ENABLED);
		settingsKeys.push(Queries.SETTING_PRINT_ITEMEXPIRYDATE_ENABLED);
		settingsKeys.push(Queries.SETTING_PRINT_ITEMMANUFACTURINGDATE_ENABLED);
		settingsKeys.push(Queries.SETTING_PRINT_ITEMMRP_ENABLED);
		settingsKeys.push(Queries.SETTING_PRINT_ITEMSIZE_ENABLED);
		settingsKeys.push(Queries.SETTING_PRINT_ITEMSERIALNUMBER_ENABLED);
		settingsKeys.push(Queries.SETTING_PRINT_ITEMDESCRIPTION_ENABLED);
		settingsKeys.push(Queries.SETTING_PRINT_ITEMQUANTITY_ENABLED);
		settingsKeys.push(Queries.SETTING_PRINT_ITEMUNIT_ENABLED);
		settingsKeys.push(Queries.SETTING_PRINT_ITEMPRICE_ENABLED);
		settingsKeys.push(Queries.SETTING_PRINT_ITEMDISCOUNTAMOUNT_ENABLED);
		settingsKeys.push(Queries.SETTING_PRINT_ITEMDISCOUNTPERCENTAGE_ENABLED);
		settingsKeys.push(Queries.SETTING_PRINT_ITEMTAXAMOUNT_ENABLED);
		settingsKeys.push(Queries.SETTING_PRINT_ITEMTAXPERCENT_ENABLED);
		settingsKeys.push(Queries.SETTING_PRINT_ITEMTOTALAMOUNT_ENABLED);
		settingsKeys.push(Queries.SETTING_PRINT_FINALITEMPRICE_ENABLED);

		settingsKeys.push(Queries.SETTING_SI_COLUMNRATIO_VALUE);
		settingsKeys.push(Queries.SETTING_ITEMNAME_COLUMNRATIO_VALUE);
		settingsKeys.push(Queries.SETTING_HSNCODE_COLUMNRATIO_VALUE);
		settingsKeys.push(Queries.SETTING_ITEM_CODE_COLUMNRATIO_VALUE);
		settingsKeys.push(Queries.SETTING_ITEMCOUNT_COLUMNRATIO_VALUE);
		settingsKeys.push(Queries.SETTING_BATCHNO_COLUMNRATIO_VALUE);
		settingsKeys.push(Queries.SETTING_ITEMEXPIRYDATE_COLUMNRATIO_VALUE);
		settingsKeys.push(Queries.SETTING_ITEMMANUFACTURINGDATE_COLUMNRATIO_VALUE);
		settingsKeys.push(Queries.SETTING_ITEMMRP_COLUMNRATIO_VALUE);
		settingsKeys.push(Queries.SETTING_ITEMSIZE_COLUMNRATIO_VALUE);
		settingsKeys.push(Queries.SETTING_QUANTITY_COLUMNRATIO_VALUE);
		settingsKeys.push(Queries.SETTING_ITEMUNIT_COLUMNRATIO_VALUE);
		settingsKeys.push(Queries.SETTING_PRICEPERUNIT_COLUMNRATIO_VALUE);
		settingsKeys.push(Queries.SETTING_DISCOUNT_COLUMNRATIO_VALUE);
		settingsKeys.push(Queries.SETTING_TAXTOTALAMOUNT_COLUMNRATIO_VALUE);
		settingsKeys.push(Queries.SETTING_TAXABLEAMOUNT_COLUMNRATIO_VALUE);
		settingsKeys.push(Queries.SETTING_ADDITIONALCESS_COLUMNRATIO_VALUE);
		settingsKeys.push(Queries.SETTING_FINALITEMPRICE_COLUMNRATIO_VALUE);
		settingsKeys.push(Queries.SETTING_TOTALAMOUNT_COLUMNRATIO_VALUE);

		settingsKeys.push(Queries.SETTING_SI_COLUMNHEADER_VALUE);
		settingsKeys.push(Queries.SETTING_ITEMNAME_COLUMNHEADER_VALUE);
		settingsKeys.push(Queries.SETTING_HSNCODE_COLUMNHEADER_VALUE);
		settingsKeys.push(Queries.SETTING_ITEMCODE_COLUMNHEADER_VALUE);
		settingsKeys.push(Queries.SETTING_QUNATITY_COLUMNHEADER_VALUE);
		settingsKeys.push(Queries.SETTING_ITEMUNIT_COLUMNHEADER_VALUE);
		settingsKeys.push(Queries.SETTING_PRICEPERUNIT_COLUMNHEADER_VALUE);
		settingsKeys.push(Queries.SETTING_DISCOUNT_COLUMNHEADER_VALUE);
		settingsKeys.push(Queries.SETTING_TAXABLEAMOUNT_COLUMNHEADER_VALUE);
		settingsKeys.push(Queries.SETTING_ADDITIONALCESS_COLUMNHEADER_VALUE);
		settingsKeys.push(Queries.SETTING_FINALITEMPRICE_COLUMNHEADER_VALUE);
		settingsKeys.push(Queries.SETTING_TOTALAMOUNT_COLUMNHEADER_VALUE);
		settingsKeys.push(Queries.SETTING_BILL_TO_BILL_ENABLED);
		settingsKeys.push(Queries.SETTING_TAX_SETUP_COMPLETED);
		settingsKeys.push(Queries.SETTING_THERMAL_PRINTER_PAGE_SIZE);
		settingsKeys.push(Queries.SETTING_THERMAL_PRINTER_CUSTOMIZE_CHARACTER_COUNT);
		settingsKeys.push(Queries.SETTING_DEFAULT_PRINTER);
		settingsKeys.push(Queries.SETTING_DEFAULT_THERMAL_PRINTER_ADDRESS);
		settingsKeys.push(Queries.SETTING_THERMAL_PRINTER_TEXT_SIZE);
		settingsKeys.push(Queries.SETTING_USE_ESC_POS_CODES_IN_THERMAL_PRINTER);
		settingsKeys.push(Queries.SETTING_ENABLE_AUTOCUT_PAPER);
		settingsKeys.push(Queries.SETTING_PRINT_AMOUNTS_IN_DELIVERY_CHALLAN);
		settingsKeys.push(Queries.SETTING_ENABLE_OPEN_DRAWER_COMMAND);

		settingsKeys.push(Queries.SETTING_IS_NEW_UI_ENABLED);
		settingsKeys.push(Queries.SETTING_PRINT_BILL_OF_SUPPLY_FOR_NON_TAX_TXN);
		settingsKeys.push(Queries.SETTING_THERMAL_PRINTER_EXTRA_FOOTER_LINES);
		settingsKeys.push(Queries.SETTING_PRINT_AMOUNT_TILL_SPECIFIED_PLACES);
		settingsKeys.push(Queries.SETTING_PRINT_PAYMENT_MODE);
		settingsKeys.push(Queries.SETTING_PRINT_ACKNOWLEDGMENT);
		settingsKeys.push(Queries.SETTING_NO_OF_DECIMAL_IN_PERCENTAGE);
		settingsKeys.push(Queries.SETTING_CUSTOM_EXPENSE_TXN_MESSAGE);
		settingsKeys.push(Queries.SETTING_SHOW_EXIT_DIALOG);
		settingsKeys.push(Queries.SETTING_PARTY_STATEMENT_VIEW_MODE);
		settingsKeys.push(Queries.SETTING_ALL_SETTINGS_PUSHED_TO_CLEVERTAP);
		settingsKeys.push(Queries.SETTING_THERMAL_PRINTER_NATIVE_LANG);
		settingsKeys.push(Queries.SETTING_OTHER_INCOME_ENABLED);

		settingsKeys.push(Queries.SETTING_TXN_MSG_SENDER_ID);
		settingsKeys.push(Queries.SETTING_TXN_MESSAGE_ENABLED_SALE);
		settingsKeys.push(Queries.SETTING_TXN_MESSAGE_ENABLED_PURCHASE);
		settingsKeys.push(Queries.SETTING_TXN_MESSAGE_ENABLED_SALERETURN);
		settingsKeys.push(Queries.SETTING_TXN_MESSAGE_ENABLED_PURCHASERETURN);
		settingsKeys.push(Queries.SETTING_TXN_MESSAGE_ENABLED_CASHIN);
		settingsKeys.push(Queries.SETTING_TXN_MESSAGE_ENABLED_CASHOUT);
		settingsKeys.push(Queries.SETTING_TXN_MESSAGE_ENABLED_SALE_ORDER);
		settingsKeys.push(Queries.SETTING_TXN_MESSAGE_ENABLED_PURCHASE_ORDER);
		settingsKeys.push(Queries.SETTING_TXN_MESSAGE_ENABLED_ESTIMATE_FORM);
		settingsKeys.push(Queries.SETTING_TXN_MESSAGE_ENABLED_DELIVERY_CHALLAN);

		settingsKeys.push(Queries.SETTING_FTU_EMAIL_LOGIN);
		settingsKeys.push(Queries.SETTING_FTU_PHONE_NO_LOGIN);
		settingsKeys.push(Queries.SETTING_PRINT_AMOUNT_GROUPING);
		settingsKeys.push(Queries.SETTING_PRINT_REPEAT_HEADER_IN_ALL_PAGES);
		settingsKeys.push(Queries.SETTING_PAYMENT_TERM_ENABLED);
		settingsKeys.push(Queries.SETTING_THERMAL_PRINTER_COPY_COUNT);
		settingsKeys.push(Queries.SETTING_DISABLE_INVOICE_PREVIEW);
		settingsKeys.push(Queries.SETTING_PRINT_ENABLE_QR_CODE);
		settingsKeys.push(Queries.SETTING_PRIVACY_MODE);

		settingsKeys.push(Queries.SETTING_SHOW_ITEM_CODE_IN_LINE_ITEM);
		settingsKeys.push(Queries.SETTING_SHOW_ITEM_CATEGORY_IN_LINE_ITEM);
		settingsKeys.push(Queries.SETTING_SHOW_ITEM_HSN_SAC_CODE_IN_LINE_ITEM);
		settingsKeys.push(Queries.SETTING_SHOW_LOW_STOCK_DIALOG);

		settingsKeys.push(Queries.SETTING_THERMAL_PRINTER_THEME);
		settingsKeys.push(Queries.SETTING_INCLUSIVE_TAX_ON_INWARD_TXN);
		settingsKeys.push(Queries.SETTING_INCLUSIVE_TAX_ON_OUTWARD_TXN);
		settingsKeys.push(Queries.SETTING_YOUTUBE_SUBSCRIPTION_TRIAL_START_DATE);
		settingsKeys.push(Queries.SETTING_FACEBOOK_SUBSCRIPTION_TRIAL_START_DATE);
		settingsKeys.push(Queries.SETTING_ENABLE_INCLUSIVE_EXCLUSIVE_TAX_ON_TRANSACTION);
		return settingsKeys;
	};

	this.getSettingDefaultValue = function (settingKey) {
		var Queries = require('./../Constants/Queries.js');
		var ItemType = require('./../Constants/ItemType.js');
		// var MyDate = require('./../Utilities/MyDate.js');
		// var GetDate = require('./../BizLogic/GetDate.js');
		var settingValue = '';
		if (!settingKey) {
			return settingValue;
		}
		switch (settingKey) {
			case Queries.SETTING_SHOW_LOW_STOCK_DIALOG:
				settingValue = '1';
				break;
			case Queries.SETTING_PUSH_SETTING_TO_CLEVERTAP_DESKTOP:
				settingValue = '0';
				break;
			case Queries.SETTING_ITEM_ENABLED:
				settingValue = '1';
				break;
			case Queries.SETTING_SHOW_LAST_FIVE_SALE_PRICE:
				settingValue = '0';
				break;
			case Queries.SETTING_TAX_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_DISCOUNT_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_PASSCODE_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_DELETE_PASSCODE_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_DELETE_PASSCODE:
				settingValue = null;
				break;
			case Queries.SETTING_LAST_BACKUP_TIME:
				var MyDate = require('./../Utilities/MyDate.js');
				settingValue = MyDate.getDate('y-m-d H:M:S');
				break;
			case Queries.SETTING_CUSTOM_PAYMENT_REMINDER_MESSAGE:
				settingValue = StringConstants.DEFAULT_REMINDER_MESSAGE;
				break;
			case Queries.SETTING_BACKUP_REMINDER_DAYS:
				settingValue = '7';
				break;
			case Queries.SETTING_LAST_BACKUP_REMINDER_TIME:
				// getDate = new GetDate();
				var MyDate = require('./../Utilities/MyDate.js');
				settingValue = MyDate.getDate('y-m-d H:M:S');
				break;
			case Queries.SETTING_BACKUP_REMINDER_SNOOZE:
				settingValue = '1';
				break;
			case Queries.SETTING_FIRST_TIME_LOGIN_DATE:
				var MyDate = require('./../Utilities/MyDate.js');
				settingValue = MyDate.getDate('y-m-d H:M:S');
				break;
			case Queries.SETTING_FREE_TRIAL_START_DATE:
				settingValue = null;
				break;
			case Queries.SETTING_FREE_TRIAL_START_DATE_DESKTOP:
				var MyDate = require('./../Utilities/MyDate.js');
				settingValue = MyDate.getDate('y-m-d H:M:S');
				break;
			case Queries.SETTING_CURRENCY_SYMBOL:
				settingValue = '\u20B9';
				break;
			case Queries.SETTING_STOCK_ENABLED:
				settingValue = '1';
				break;
			case Queries.SETTING_TXNREFNO_ENABLED:
				settingValue = '1';
				break;
			case Queries.SETTING_TXNREFNO_MAXVALUE:
				settingValue = '0';
				break;
			case Queries.SETTING_PARTY_ITEM_RATE:
				settingValue = '0';
				break;
			case Queries.SETTING_PAYMENTREMIDNER_ENABLED:
				settingValue = '1';
				break;
			case Queries.SETTING_PAYMENT_REMINDER_DAYS:
				settingValue = '1';
				break;
			case Queries.SETTING_TRANSACTION_MESSAGE_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_ADDRESS:
				settingValue = '';
				break;
			case Queries.SETTING_PRINT_TINNUMBER:
				settingValue = '0';
				break;
			case Queries.SETTING_QUANTITY_DECIMAL:
				settingValue = '2';
				break;
			case Queries.SETTING_ITEMWISE_TAX_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_ITEMWISE_DISCOUNT_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_ORDER_FORM_ENABLED:
				settingValue = '1';
				break;
			case Queries.SETTING_AMOUNT_DECIMAL:
				settingValue = '2';
				break;
			case Queries.SETTING_TXN_MESSAGE_ENABLED_SALE:
				settingValue = '1';
				break;
			case Queries.SETTING_TXN_MESSAGE_ENABLED_PURCHASE:
				settingValue = '1';
				break;
			case Queries.SETTING_TXN_MESSAGE_ENABLED_SALERETURN:
				settingValue = '1';
				break;
			case Queries.SETTING_TXN_MESSAGE_ENABLED_PURCHASERETURN:
				settingValue = '1';
				break;
			case Queries.SETTING_TXN_MESSAGE_ENABLED_CASHIN:
				settingValue = '1';
				break;
			case Queries.SETTING_TXN_MESSAGE_ENABLED_CASHOUT:
				settingValue = '1';
				break;
			case Queries.SETTING_TXN_MESSAGE_ENABLED_SALE_ORDER:
				settingValue = '1';
				break;
			case Queries.SETTING_TXN_MESSAGE_ENABLED_PURCHASE_ORDER:
				settingValue = '0';
				break;
			case Queries.SETTING_TXN_MESSAGE_ENABLED_DELIVERY_CHALLAN:
				settingValue = '0';
				break;
			case Queries.SETTING_TXN_MESSAGE_ENABLED_ESTIMATE_FORM:
				settingValue = '0';
				break;
			case Queries.SETTING_DISCOUNT_IN_MONEY_TXN:
				settingValue = '0';
				break;
			case Queries.SETTING_AUTO_BACKUP_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_AUTO_BACKUP_LAST_BACKUP:
				settingValue = '';
				break;
			case Queries.SETTING_AUTO_BACKUP_DURATION_DAYS:
				settingValue = '2';
				break;
			case Queries.SETTING_REMIND_RATING:
				settingValue = '0';
				break;
			case Queries.SETTING_PARTY_GROUP:
				settingValue = '0';
				break;
			case Queries.SETTING_COMPANY_LOGO_ID:
				settingValue = '';
				break;
			case Queries.SETTING_ITEM_CATEGORY:
				settingValue = '0';
				break;
			case Queries.SETTING_AC_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_AC1_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_AC2_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_AC3_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_EXTRA_SPACE_ON_TXN_PDF:
				settingValue = '0';
				break;
			case Queries.SETTING_PRINT_LOGO_ON_TXN_PDF:
				settingValue = '1';
				break;
			case Queries.SETTING_PRINT_COMPANY_NAME_ON_TXN_PDF:
				settingValue = '1';
				break;
			case Queries.SETTING_PRINT_COMPANY_ADDRESS_ON_TXN_PDF:
				settingValue = '1';
				break;
			case Queries.SETTING_PRINT_COMPANY_NUMBER_ON_TXN_PDF:
				settingValue = '1';
				break;
			case Queries.SETTING_PRINT_TERM_AND_CONDITION_ON_TXN_PDF:
				settingValue = '1';
				break;
			case Queries.SETTING_TERMS_AND_CONDITIONS:
				settingValue = 'Thanks for doing business with us!';
				break;
			case Queries.SETTING_PRINT_DESCRIPTION_ON_TXN_PDF:
				settingValue = '1';
				break;
			case Queries.SETTING_TXN_PDF_THEME:
				settingValue = InvoiceTheme.THEME_10 + '';
				break;
			case Queries.SETTING_TXN_PDF_THEME_COLOR:
				settingValue = InvoiceTheme.THEME_COLOR_24;
				break;
			case Queries.SETTING_TXN_MSG_TO_OWNER:
				settingValue = '0';
				break;
			case Queries.SETTING_TXN_MSG_OWNER_NUMBER:
				settingValue = '';
				break;
			case Queries.SETTING_TIN_NUMBER_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_SIGNATURE_ENABLED:
				settingValue = '1';
				break;
			case Queries.SETTING_SIGNATURE_ID:
				settingValue = '';
				break;
			case Queries.SETTING_IS_ZERO_BAL_PARTY_ALLOWED_IN_RECEIVABLE:
				settingValue = '1';
				break;
			case Queries.SETTING_MULTI_LANGUAGE_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_IS_ITEM_UNIT_ENABLED:
				settingValue = '1';
				break;
			case Queries.SETTING_TAXINVOICE_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_MULTIFIRM_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_DEFAULT_FIRM_ID:
				settingValue = 1;
				break;
			case Queries.SETTING_CUSTOM_NAME_FOR_PURCHASE:
				settingValue = 'Bill';
				break;
			case Queries.SETTING_CUSTOM_NAME_FOR_DELIVERY_CHALLAN:
				settingValue = 'Delivery Challan';
				break;
			case Queries.SETTING_CUSTOM_NAME_FOR_SALE:
				settingValue = 'Invoice';
				break;
			case Queries.SETTING_CUSTOM_NAME_FOR_CASH_IN:
				settingValue = 'Payment Receipt';
				break;
			case Queries.SETTING_CUSTOM_NAME_FOR_CASH_OUT:
				settingValue = 'Payment Out';
				break;
			case Queries.SETTING_CUSTOM_NAME_FOR_PURCHASE_RETURN:
				settingValue = 'Debit Note';
				break;
			case Queries.SETTING_CUSTOM_NAME_FOR_SALE_RETURN:
				settingValue = 'Credit Note';
				break;
			case Queries.SETTING_CUSTOM_NAME_FOR_EXPENSE:
				settingValue = 'Expense';
				break;
			case Queries.SETTING_CUSTOM_NAME_FOR_INCOME:
				settingValue = 'Other Income';
				break;
			case Queries.SETTING_TXN_MSG_SENDER_ID:
				settingValue = '';
				break;
			case Queries.SETTING_CUSTOM_NAME_FOR_SALE_ORDER:
				settingValue = 'Sale Order';
				break;
			case Queries.SETTING_CUSTOM_NAME_FOR_PURCHASE_ORDER:
				settingValue = 'Purchase Order';
				break;
			case Queries.SETTING_CUSTOM_NAME_FOR_TAX_INVOICE:
				settingValue = 'Tax Invoice';
				break;
			case Queries.SETTING_CUSTOM_NAME_FOR_DELIVERY_CHALLAN:
				settingValue = 'Delivery Challan';
				break;
			case Queries.SETTING_CUSTOM_NAME_FOR_ESTIMATE:
				settingValue = 'Estimate';
				break;
			case Queries.SETTING_HSN_SAC_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_GST_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_CURRENT_MAC_ADDRESS:
				settingValue = macAd;
				break;
			case Queries.SETTING_LEADS_INFO_SENT:
				settingValue = '0';
				break;
			case Queries.SETTING_BARCODE_SCANNING_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_USER_COUNTRY:
				settingValue = '';
				break;
			case Queries.SETTING_BARCODE_SCANNER_TYPE:
				settingValue = '0';
				break;
			case Queries.SETTING_AMOUNT_IN_WORD_FORMAT:
				var CurrencyModeInWords = require('./../Constants/CurrencyModeInWords.js');
				settingValue = CurrencyModeInWords.INDIAN_MODE + '';
				break;
			case Queries.SETTING_PRINT_TAX_DETAILS:
				settingValue = '1';
				break;
			case Queries.SETTING_MIN_ITEM_ROWS_ON_TXN_PDF:
				settingValue = '0';
				break;
			case Queries.SETTING_PRINT_TEXT_SIZE:
				settingValue = '4';
				break;
			case Queries.SETTING_PRINT_COMPANY_NAME_TEXT_SIZE:
				settingValue = '4';
				break;
			case Queries.SETTING_PRINT_PAGE_SIZE:
				settingValue = '' + TransactionPDFPaperSize.A4;
				break;
			case Queries.SETTING_PARTY_SHIPPING_ADDRESS_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_PRINT_PARTY_SHIPPING_ADDRESS:
				settingValue = '1';
				break;
			case Queries.SETTING_PRINT_BANK_DETAIL:
				settingValue = '0';
				break;
			case Queries.SETTING_SHOW_PURCHASE_PRICE_IN_ITEM_DROP_DOWN:
				settingValue = '1';
				break;
			case Queries.SETTING_SIGNATURE_TEXT:
				settingValue = StringConstants.SIGNATURE_TEXT;
				break;
			case Queries.SETTING_PRINT_COPY_NUMBER:
				settingValue = '0';
				break;
			case Queries.SETTING_ENABLE_DISPLAY_NAME:
				settingValue = '0';
				break;
			case Queries.SETTING_ENABLE_ITEM_MRP:
				settingValue = '0';
				break;
			case Queries.SETTING_ENABLE_ITEM_BATCH_NUMBER:
				settingValue = '0';
				break;
			case Queries.SETTING_ENABLE_ITEM_EXPIRY_DATE:
				settingValue = '0';
				break;
			case Queries.SETTING_ENABLE_ITEM_MANUFACTURING_DATE:
				settingValue = '0';
				break;
			case Queries.SETTING_ENABLE_SERIAL_ITEM_NUMBER:
				settingValue = '0';
				break;
			case Queries.SETTING_ENABLE_ITEM_COUNT:
				settingValue = '0';
				break;
			case Queries.SETTING_ENABLE_ITEM_DESCRIPTION:
				settingValue = '0';
				break;
			case Queries.SETTING_ITEM_MRP_VALUE:
				settingValue = 'MRP';
				break;
			case Queries.SETTING_ITEM_BATCH_NUMBER_VALUE:
				settingValue = 'Batch No.';
				break;
			case Queries.SETTING_ITEM_EXPIRY_DATE_VALUE:
				settingValue = 'Exp. Date';
				break;
			case Queries.SETTING_ITEM_MANUFACTURING_DATE_VALUE:
				settingValue = 'Mfg. Date';
				break;
			case Queries.SETTING_ITEM_SERIAL_NUMBER_VALUE:
				settingValue = 'Serial No.';
				break;
			case Queries.SETTING_ITEM_COUNT_VALUE:
				settingValue = 'Count';
				break;
			case Queries.SETTING_ITEM_DESCRIPTION_VALUE:
				settingValue = 'Description';
				break;
			case Queries.SETTING_EXPIRY_DATE_TYPE:
				settingValue = StringConstants.monthYear;
				break;
			case Queries.SETTING_MANUFACTURING_DATE_TYPE:
				settingValue = StringConstants.dateMonthYear;
				break;
			case Queries.SETTING_ENABLE_ADDITIONAL_CESS_ON_ITEM:
				settingValue = '0';
				break;
			case Queries.SETTING_ENABLE_REVERSE_CHARGE:
				settingValue = '0';
				break;
			case Queries.SETTING_SHOW_CURRENT_BALANCE_OF_PARTY:
				settingValue = '0';
				break;
			case Queries.SETTING_SHOW_RECEIVED_AMOUNT_OF_TRANSACTION:
				settingValue = '1';
				break;
			case Queries.SETTING_SHOW_BALANCE_AMOUNT_OF_TRANSACTION:
				settingValue = '1';
				break;
			case Queries.SETTING_PRINT_COMPANY_EMAIL_ON_PDF:
				settingValue = '1';
				break;
			case Queries.SETTING_ENABLE_DEFAULT_CASH_SALE:
				settingValue = '0';
				break;
			case Queries.SETTING_ITEM_TYPE:
				settingValue = StringConstants.settingProductAndService;
				break;
			case Queries.SETTING_ENABLE_EDITING_TOTAL_AMOUNT:
				settingValue = '0';
				break;
			case Queries.SETTING_ENABLE_PLACE_OF_SUPPLY:
				settingValue = '0';
				break;
			case Queries.SETTING_ENABLE_EWAY_BILL_NUMBER:
				settingValue = '0';
				break;
			case Queries.SETTING_PRINT_ITEM_QUANTITY_TOTAL_ON_TXN_PDF:
				settingValue = '1';
				break;
			case Queries.SETTING_IS_ROUND_OFF_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_ROUND_OFF_TYPE:
				var RoundOffConstant = require('./../Constants/RoundOffConstant.js');
				settingValue = RoundOffConstant.ROUND_NEAR;
				break;
			case Queries.SETTING_ROUND_OFF_UPTO:
				var RoundOffConstant = require('./../Constants/RoundOffConstant.js');
				settingValue = RoundOffConstant.ONES;
				break;
			case Queries.SETTING_DELIVERY_CHALLAN_ENABLED:
				settingValue = '1';
				break;
			case Queries.SETTING_DELIVERY_CHALLAN_RETURN_ENABLED:
				settingValue = '1';
				break;
			case Queries.SETTING_ESTIMATE_ENABLED:
				settingValue = '1';
				break;
			case Queries.SETTING_PO_DETAILS_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_IS_COMPOSITE_SCHEME_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_COMPOSITE_USER_TYPE:
				settingValue = '0';
				break;
			case Queries.SETTING_CURRENT_DATE_FORMAT:
				settingValue = '0';
				break;
			case Queries.SETTING_ENABLE_ITEM_SIZE:
				settingValue = '0';
				break;
			case Queries.SETTING_ITEM_SIZE_VALUE:
				settingValue = 'Size';
				break;
			case Queries.SETTING_CHANGE_LOG_NUMBER:
				settingValue = '0';
				break;
			case Queries.SETTING_COMPANY_GLOBAL_ID:
				settingValue = null;
				break;
			case Queries.SETTING_SYNC_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_FREE_QTY_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_ENABLE_INCLUSIVE_EXCLUSIVE_TAX_ON_TRANSACTION:
				settingValue = '1';
				break;
			case Queries.SETTING_PRINT_SINNUMBER_ENABLED:
				settingValue = Defaults.DEFAULT_PRINT_SINNUMBER_ENABLED;
				break;
			case Queries.SETTING_PRINT_HSNCODE_ENABLED:
				settingValue = Defaults.DEFAULT_PRINT_HSNCODE_ENABLED;
				break;
			case Queries.SETTING_PRINT_ITEMCODE_ENABLED:
				settingValue = Defaults.DEFAULT_PRINT_ITEMCODE_ENABLED;
				break;
			case Queries.SETTING_PRINT_ITEMCOUNT_ENABLED:
				settingValue = Defaults.DEFAULT_PRINT_ITEMCOUNT_ENABLED;
				break;
			case Queries.SETTING_PRINT_BATCHNO_ENABLED:
				settingValue = Defaults.DEFAULT_PRINT_BATCHNO_ENABLED;
				break;
			case Queries.SETTING_PRINT_ITEMEXPIRYDATE_ENABLED:
				settingValue = Defaults.DEFAULT_PRINT_ITEMEXPIRYDATE_ENABLED;
				break;
			case Queries.SETTING_PRINT_ITEMMANUFACTURINGDATE_ENABLED:
				settingValue = Defaults.DEFAULT_PRINT_ITEMMANUFACTURINGDATE_ENABLED;
				break;
			case Queries.SETTING_PRINT_ITEMMRP_ENABLED:
				settingValue = Defaults.DEFAULT_PRINT_ITEMMRP_ENABLED;
				break;
			case Queries.SETTING_PRINT_ITEMSIZE_ENABLED:
				settingValue = Defaults.DEFAULT_PRINT_ITEMSIZE_ENABLED;
				break;
			case Queries.SETTING_PRINT_ITEMSERIALNUMBER_ENABLED:
				settingValue = Defaults.DEFAULT_PRINT_ITEMSERIALNUMBER_ENABLED;
				break;
			case Queries.SETTING_PRINT_ITEMDESCRIPTION_ENABLED:
				settingValue = Defaults.DEFAULT_PRINT_ITEMDESCRIPTION_ENABLED;
				break;
			case Queries.SETTING_PRINT_ITEMQUANTITY_ENABLED:
				settingValue = Defaults.DEFAULT_PRINT_ITEMQUANTITY_ENABLED;
				break;
			case Queries.SETTING_PRINT_ITEMUNIT_ENABLED:
				settingValue = Defaults.DEFAULT_PRINT_ITEMUNIT_ENABLED;
				break;
			case Queries.SETTING_PRINT_ITEMPRICE_ENABLED:
				settingValue = Defaults.DEFAULT_PRINT_ITEMPRICE_ENABLED;
				break;
			case Queries.SETTING_PRINT_ITEMDISCOUNTAMOUNT_ENABLED:
				settingValue = Defaults.DEFAULT_PRINT_ITEMDISCOUNTAMOUNT_ENABLED;
				break;
			case Queries.SETTING_PRINT_ITEMDISCOUNTPERCENTAGE_ENABLED:
				settingValue = Defaults.DEFAULT_PRINT_ITEMDISCOUNTPERCENTAGE_ENABLED;
				break;
			case Queries.SETTING_PRINT_ITEMTAXAMOUNT_ENABLED:
				settingValue = Defaults.DEFAULT_PRINT_ITEMTAXAMOUNT_ENABLED;
				break;
			case Queries.SETTING_PRINT_ITEMTAXPERCENT_ENABLED:
				settingValue = Defaults.DEFAULT_PRINT_ITEMTAXPERCENT_ENABLED;
				break;
			case Queries.SETTING_PRINT_ITEMTOTALAMOUNT_ENABLED:
				settingValue = Defaults.DEFAULT_PRINT_ITEMTOTALAMOUNT_ENABLED;
				break;
			case Queries.SETTING_PRINT_FINALITEMPRICE_ENABLED:
				settingValue = Defaults.DEFAULT_PRINT_FINALITEMPRICE_ENABLED;
				break;

			case Queries.SETTING_SI_COLUMNRATIO_VALUE:
				settingValue = Defaults.DEFAULT_SI_COLUMNRATIO_VALUE;
				break;
			case Queries.SETTING_ITEMNAME_COLUMNRATIO_VALUE:
				settingValue = Defaults.DEFAULT_ITEMNAME_COLUMNRATIO_VALUE;
				break;
			case Queries.SETTING_HSNCODE_COLUMNRATIO_VALUE:
				settingValue = Defaults.DEFAULT_HSNCODE_COLUMNRATIO_VALUE;
				break;
			case Queries.SETTING_ITEMCOUNT_COLUMNRATIO_VALUE:
				settingValue = Defaults.DEFAULT_ITEMCOUNT_COLUMNRATIO_VALUE;
				break;
			case Queries.SETTING_BATCHNO_COLUMNRATIO_VALUE:
				settingValue = Defaults.DEFAULT_BATCHNO_COLUMNRATIO_VALUE;
				break;
			case Queries.SETTING_ITEMEXPIRYDATE_COLUMNRATIO_VALUE:
				settingValue = Defaults.DEFAULT_ITEMEXPIRYDATE_COLUMNRATIO_VALUE;
				break;
			case Queries.SETTING_ITEMMANUFACTURINGDATE_COLUMNRATIO_VALUE:
				settingValue = Defaults.DEFAULT_ITEMMANUFACTURINGDATE_COLUMNRATIO_VALUE;
				break;
			case Queries.SETTING_ITEMMRP_COLUMNRATIO_VALUE:
				settingValue = Defaults.DEFAULT_ITEMMRP_COLUMNRATIO_VALUE;
				break;
			case Queries.SETTING_ITEMSIZE_COLUMNRATIO_VALUE:
				settingValue = Defaults.DEFAULT_ITEMSIZE_COLUMNRATIO_VALUE;
				break;
			case Queries.SETTING_QUANTITY_COLUMNRATIO_VALUE:
				settingValue = Defaults.DEFAULT_QUANTITY_COLUMNRATIO_VALUE;
				break;
			case Queries.SETTING_ITEMUNIT_COLUMNRATIO_VALUE:
				settingValue = Defaults.DEFAULT_ITEMUNIT_COLUMNRATIO_VALUE;
				break;
			case Queries.SETTING_PRICEPERUNIT_COLUMNRATIO_VALUE:
				settingValue = Defaults.DEFAULT_PRICEPERUNIT_COLUMNRATIO_VALUE;
				break;
			case Queries.SETTING_DISCOUNT_COLUMNRATIO_VALUE:
				settingValue = Defaults.DEFAULT_DISCOUNT_COLUMNRATIO_VALUE;
				break;
			case Queries.SETTING_TAXTOTALAMOUNT_COLUMNRATIO_VALUE:
				settingValue = Defaults.DEFAULT_TAXTOTALAMOUNT_COLUMNRATIO_VALUE;
				break;
			case Queries.SETTING_TAXABLEAMOUNT_COLUMNRATIO_VALUE:
				settingValue = Defaults.DEFAULT_TAXABLEAMOUNT_COLUMNRATIO_VALUE;
				break;
			case Queries.SETTING_ADDITIONALCESS_COLUMNRATIO_VALUE:
				settingValue = Defaults.DEFAULT_ADDITIONALCESS_COLUMNRATIO_VALUE;
				break;
			case Queries.SETTING_FINALITEMPRICE_COLUMNRATIO_VALUE:
				settingValue = Defaults.DEFAULT_FINALITEMPRICE_COLUMNRATIO_VALUE;
				break;
			case Queries.SETTING_TOTALAMOUNT_COLUMNRATIO_VALUE:
				settingValue = Defaults.DEFAULT_TOTALAMOUNT_COLUMNRATIO_VALUE;
				break;
			case Queries.SETTING_SI_COLUMNHEADER_VALUE:
				settingValue = Defaults.DEFAULT_SI_COLUMNHEADER_VALUE;
				break;
			case Queries.SETTING_ITEMNAME_COLUMNHEADER_VALUE:
				settingValue = Defaults.DEFAULT_ITEMNAME_COLUMNHEADER_VALUE;
				break;
			case Queries.SETTING_HSNCODE_COLUMNHEADER_VALUE:
				settingValue = Defaults.DEFAULT_HSNCODE_COLUMNHEADER_VALUE;
				break;
			case Queries.SETTING_ITEMCODE_COLUMNHEADER_VALUE:
				settingValue = Defaults.DEFAULT_ITEMCODE_COLUMNHEADER_VALUE;
				break;
			case Queries.SETTING_QUNATITY_COLUMNHEADER_VALUE:
				settingValue = Defaults.DEFAULT_QUNATITY_COLUMNHEADER_VALUE;
				break;
			case Queries.SETTING_ITEMUNIT_COLUMNHEADER_VALUE:
				settingValue = Defaults.DEFAULT_ITEMUNIT_COLUMNHEADER_VALUE;
				break;
			case Queries.SETTING_PRICEPERUNIT_COLUMNHEADER_VALUE:
				settingValue = Defaults.DEFAULT_PRICEPERUNIT_COLUMNHEADER_VALUE;
				break;
			case Queries.SETTING_DISCOUNT_COLUMNHEADER_VALUE:
				settingValue = Defaults.DEFAULT_DISCOUNT_COLUMNHEADER_VALUE;
				break;
			case Queries.SETTING_TAXABLEAMOUNT_COLUMNHEADER_VALUE:
				settingValue = Defaults.DEFAULT_TAXABLEAMOUNT_COLUMNHEADER_VALUE;
				break;
			case Queries.SETTING_FINALITEMPRICE_COLUMNHEADER_VALUE:
				settingValue = Defaults.DEFAULT_FINAL_ITEM_PRICE_COLUMNHEADER_VALUE;
				break;
			case Queries.SETTING_TOTALAMOUNT_COLUMNHEADER_VALUE:
				settingValue = Defaults.DEFAULT_TOTAL_AMOUNT_COLUMNHEADER_VALUE;
				break;
			case Queries.SETTING_ADDITIONALCESS_COLUMNHEADER_VALUE:
				settingValue = Defaults.DEFAULT_ADDITIONALCESS_COLUMNHEADER_VALUE;
				break;
			case Queries.SETTING_BILL_TO_BILL_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_TAX_SETUP_COMPLETED:
				settingValue = '0';
				break;
			case Queries.SETTING_THERMAL_PRINTER_PAGE_SIZE:
				settingValue = '2';
				break;
			case Queries.SETTING_THERMAL_PRINTER_CUSTOMIZE_CHARACTER_COUNT:
				settingValue = '48';
				break;
			case Queries.SETTING_DEFAULT_PRINTER:
				settingValue = '1';
				break;
			case Queries.SETTING_DEFAULT_THERMAL_PRINTER_ADDRESS:
				settingValue = '';
				break;
			case Queries.SETTING_THERMAL_PRINTER_TEXT_SIZE:
				settingValue = '0';
				break;
			case Queries.SETTING_USE_ESC_POS_CODES_IN_THERMAL_PRINTER:
				settingValue = '1';
				break;
			case Queries.SETTING_ENABLE_AUTOCUT_PAPER:
				settingValue = '0';
				break;
			case Queries.SETTING_ENABLE_OPEN_DRAWER_COMMAND:
				settingValue = '0';
				break;
			case Queries.SETTING_PRINT_AMOUNTS_IN_DELIVERY_CHALLAN:
				settingValue = '0';
				break;
			case Queries.SETTING_IS_NEW_UI_ENABLED:
				settingValue = '2';
				break;
			case Queries.SETTING_PRINT_BILL_OF_SUPPLY_FOR_NON_TAX_TXN:
				settingValue = '0';
				break;
			case Queries.SETTING_THERMAL_PRINTER_EXTRA_FOOTER_LINES:
				settingValue = '0';
				break;
			case Queries.SETTING_PRINT_AMOUNT_TILL_SPECIFIED_PLACES:
				settingValue = '1';
				break;
			case Queries.SETTING_PRINT_PAYMENT_MODE:
				settingValue = '0';
				break;
			case Queries.SETTING_NO_OF_DECIMAL_IN_PERCENTAGE:
				settingValue = '3';
				break;
			case Queries.SETTING_PRINT_ACKNOWLEDGMENT:
				settingValue = '0';
				break;
			case Queries.SETTING_CUSTOM_EXPENSE_TXN_MESSAGE:
				settingValue = 'Expense Message' + 'Expense Details:\nCategory:<category>\nTotal amount:<total amount>\n\n';
				break;
			case Queries.SETTING_SHOW_EXIT_DIALOG:
				settingValue = '1';
				break;
			case Queries.SETTING_PARTY_STATEMENT_VIEW_MODE:
				settingValue = '1';
				break;
			case Queries.SETTING_ALL_SETTINGS_PUSHED_TO_CLEVERTAP:
				settingValue = '0';
				break;
			case Queries.SETTING_THERMAL_PRINTER_NATIVE_LANG:
				settingValue = '0';
				break;
			case Queries.SETTING_OTHER_INCOME_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_FTU_EMAIL_LOGIN:
				settingValue = '1';
				break;
			case Queries.SETTING_FTU_PHONE_NO_LOGIN:
				settingValue = '1';
				break;
			case Queries.SETTING_PRINT_AMOUNT_GROUPING:
				settingValue = '1';
				break;
			case Queries.SETTING_PRINT_REPEAT_HEADER_IN_ALL_PAGES:
				settingValue = '1';
				break;
			case Queries.SETTING_PAYMENT_TERM_ENABLED:
				settingValue = '0';
				break;
			case Queries.SETTING_THERMAL_PRINTER_COPY_COUNT:
				settingValue = '1';
				break;
			case Queries.SETTING_PRIVACY_MODE:
				settingValue = '0';
				break;
			case Queries.SETTING_INCLUSIVE_TAX_ON_INWARD_TXN:
				settingValue = ItemType.ITEM_TXN_TAX_EXCLUSIVE;
				break;
			case Queries.SETTING_INCLUSIVE_TAX_ON_OUTWARD_TXN:
				settingValue = ItemType.ITEM_TXN_TAX_EXCLUSIVE;
				break;
			case Queries.SETTING_DISABLE_INVOICE_PREVIEW:
				settingValue = '0';
				break;
			case Queries.SETTING_PRINT_ENABLE_QR_CODE:
				settingValue = '1';
				break;
			case Queries.SETTING_SHOW_ITEM_CODE_IN_LINE_ITEM:
			case Queries.SETTING_SHOW_ITEM_CATEGORY_IN_LINE_ITEM:
			case Queries.SETTING_SHOW_ITEM_HSN_SAC_CODE_IN_LINE_ITEM:
				settingValue = '0';
				break;
			case Queries.SETTING_THERMAL_PRINTER_THEME:
				settingValue = InvoiceTheme.THERMAL_THEME_1 + '';
				break;
		}
		return settingValue;
	};

	this.setBackupDate = function () {
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqliteDBHelper = new SqliteDBHelper();
		sqliteDBHelper.setBackupDate();
	};
};

module.exports = SettingsModel;