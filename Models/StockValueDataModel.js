var StockValueDataModel = function StockValueDataModel() {
	var itemPurchaseDate, itemQuantity, itemFreeQunatity, itemPurchaseAmount, itemTxnType;

	this.setItemPurchaseDate = function (itemPurchaseDate) {
		this.itemPurchaseDate = itemPurchaseDate;
	};
	this.getItemPurchaseDate = function () {
		return this.itemPurchaseDate;
	};

	this.setItemQuantity = function (itemQuantity) {
		this.itemQuantity = itemQuantity;
	};
	this.getItemQuantity = function () {
		return this.itemQuantity;
	};

	this.setItemFreeQuantity = function (itemFreeQunatity) {
		this.itemFreeQunatity = itemFreeQunatity;
	};
	this.getItemFreeQuantity = function () {
		return this.itemFreeQunatity;
	};

	this.setItemPurchaseAmount = function (itemPurchaseAmount) {
		this.itemPurchaseAmount = itemPurchaseAmount;
	};
	this.getItemPurchaseAmount = function () {
		return this.itemPurchaseAmount;
	};

	this.setItemTxnType = function (itemTxnType) {
		this.itemTxnType = itemTxnType;
	};
	this.getItemTxnType = function () {
		return this.itemTxnType;
	};

	this.getTotalQuantityIncludingFreeQuantity = function () {
		return this.itemQuantity + this.itemFreeQunatity;
	};
};

module.exports = StockValueDataModel;