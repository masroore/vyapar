var ExpenseCategoryModel = function ExpenseCategoryModel() {
	this.nameId = 0;
	this.amount = 0.0;

	this.getAmount = function () {
		return this.amount;
	};
	this.setAmount = function (amount) {
		this.amount = amount;
	};

	this.getNameId = function () {
		return this.nameId;
	};
	this.setNameId = function (nameId) {
		this.nameId = nameId;
	};
};

module.exports = ExpenseCategoryModel;