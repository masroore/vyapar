var ItemCategoryModel = function ItemCategoryModel() {
	this.categoryId = 0;
	this.categoryName = '';
	this.memberCount = 0;

	var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
	var sqlitedbhelper = new SqliteDBHelper();

	var ErrorCode = require('./../Constants/ErrorCode.js');
	var ItemType = require('./../Constants/ItemType.js');

	this.getCategoryId = function () {
		return this.categoryId;
	};
	this.setCategoryId = function (categoryId) {
		this.categoryId = categoryId;
	};

	this.getCategoryName = function () {
		return this.categoryName;
	};
	this.setCategoryName = function (categoryName) {
		this.categoryName = categoryName;
	};

	this.getMemberCount = function () {
		return this.memberCount;
	};
	this.setMemberCount = function (memberCount) {
		this.memberCount = memberCount;
	};

	this.loadCategoryForItem = function () {
		this.setCategoryId(sqlitedbhelper.getCategoryId(this.categoryName));
	};

	this.loadCategoryNameForId = function () {
		this.setCategoryName(sqlitedbhelper.getCategoryName(this.categoryId));
	};

	this.addNewCategory = function () {
		var retVal = sqlitedbhelper.createItemCategoryRecord(this, null);
		var statusCode = ErrorCode.ERROR_ITEMCATEGORY_SAVE_FAILED;
		if (retVal > 0) {
			this.setCategoryId(retVal);
			statusCode = ErrorCode.ERROR_ITEMCATEGORY_SAVE_SUCCESS;
		} else {
			statusCode = ErrorCode.ERROR_ITEMCATEGORY_SAVE_FAILED;
		}
		console.log(statusCode);
		return statusCode;
	};

	this.editItemCategory = function (oldCategoryId) {
		var retVal = sqlitedbhelper.editItemCategoryRecord(oldCategoryId, this);
		if (retVal) {
			statusCode = ErrorCode.ERROR_ITEMCATEGORY_UPDATE_SUCCESS;
		} else {
			statusCode = ErrorCode.ERROR_ITEMCATEGORY_UPDATE_FAILED;
		}
		return statusCode;
	};

	this.canDeleteItemCategory = function (itemCategoryId) {
		return sqlitedbhelper.canDeleteItemCategory(itemCategoryId);
	};

	this.deleteItemCategory = function (itemCategoryId) {
		return sqlitedbhelper.deleteItemCategory(itemCategoryId);
	};
};

module.exports = ItemCategoryModel;