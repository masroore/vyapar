var OrderItemReportObjectModel = function OrderItemReportObjectModel() {
	this.itemId = 0;
	this.itemName = '';
	this.qty = 0;
	this.freeQty = 0;
	this.amount = 0;

	this.getItemId = function () {
		return this.itemId;
	};

	this.setItemId = function (itemId) {
		this.itemId = itemId;
	};

	this.getItemName = function () {
		return this.itemName;
	};

	this.setItemName = function (itemName) {
		this.itemName = itemName;
	};

	this.getQty = function () {
		return this.qty;
	};

	this.setQty = function (qty) {
		this.qty = qty;
	};

	this.getFreeQty = function () {
		return this.freeQty;
	};

	this.setFreeQty = function (freeQty) {
		this.freeQty = freeQty;
	};

	this.getAmount = function () {
		return this.amount;
	};

	this.setAmount = function (amount) {
		this.amount = amount;
	};
};

module.exports = OrderItemReportObjectModel;