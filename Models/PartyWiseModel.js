var PartyWiseModel = function PartyWiseModel() {
	var modelId, itemId, nameId, salePrice, purchasePrice;
	var ErrorCode = require('./../Constants/ErrorCode.js');
	var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');

	this.getModelId = function () {
		return this.modelId;
	};
	this.setModelId = function (modelId) {
		this.modelId = modelId;
	};

	this.getItemId = function () {
		return this.itemId;
	};
	this.setItemId = function (itemId) {
		this.itemId = itemId;
	};

	this.getNameId = function () {
		return this.nameId;
	};
	this.setNameId = function (nameId) {
		this.nameId = nameId;
	};

	this.getSalePrice = function () {
		return this.salePrice;
	};
	this.setSalePrice = function (salePrice) {
		this.salePrice = salePrice;
	};

	this.getPurchasePrice = function () {
		return this.purchasePrice;
	};
	this.setPurchasePrice = function (purchasePrice) {
		this.purchasePrice = purchasePrice;
	};

	this.addPartyWiseRate = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		var statusCode = sqliteDBHelper.createPartyWiseItemRateRecord(this);
		return statusCode;
	};

	this.updatePartyWiseRate = function (txnType) {
		var sqliteDBHelper = new SqliteDBHelper();
		var statusCode = sqliteDBHelper.updatePartyWiseItemRateRecord(this, txnType);
		return statusCode;
	};
};

module.exports = PartyWiseModel;