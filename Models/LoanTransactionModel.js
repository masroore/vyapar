var _assign = require('babel-runtime/core-js/object/assign');

var _assign2 = _interopRequireDefault(_assign);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
	var LoanTransaction = function LoanTransaction(obj) {
		this.loanTxnId = '';
		this.loanAccountId = '';
		this.loanType = '';
		this.principalAmount = '';
		this.interestAmount = '';
		this.transactionDate = '';
		this.createdDate = '';
		this.modifiedDate = '';
		this.paymentTypeId = '';
		this.description = '';
		this.txnImageId = '';
		(0, _assign2.default)(this, obj);
	};
	LoanTransaction.prototype.getLoanTxnId = function getLoanTxnId() {
		return this.loanTxnId;
	};
	LoanTransaction.prototype.setLoanTxnId = function setLoanTxnId(loanTxnId) {
		this.loanTxnId = loanTxnId;
	};
	LoanTransaction.prototype.getLoanAccountId = function getLoanAccountId() {
		return this.loanAccountId;
	};
	LoanTransaction.prototype.setLoanAccountId = function setLoanAccountId(loanAccountId) {
		this.loanAccountId = loanAccountId;
	};
	LoanTransaction.prototype.getPaymentTypeId = function getPaymentTypeId() {
		return this.paymentTypeId;
	};
	LoanTransaction.prototype.setPaymentTypeId = function setPaymentTypeId(paymentTypeId) {
		this.paymentTypeId = paymentTypeId;
	};
	LoanTransaction.prototype.getLoanType = function getLoanType() {
		return this.loanType;
	};
	LoanTransaction.prototype.setLoanType = function setLoanType(loanType) {
		this.loanType = loanType;
	};
	LoanTransaction.prototype.getPrincipalAmount = function getPrincipalAmount() {
		return this.principalAmount;
	};
	LoanTransaction.prototype.setPrincipalAmount = function setPrincipalAmount(principalAmount) {
		this.principalAmount = principalAmount;
	};
	LoanTransaction.prototype.getInterestAmount = function getInterestAmount() {
		return this.interestAmount;
	};
	LoanTransaction.prototype.setInterestAmount = function setInterestAmount(interestAmount) {
		this.interestAmount = interestAmount;
	};
	LoanTransaction.prototype.getTotalAmount = function getTotalAmount() {
		return Number(this.principalAmount || '') + Number(this.interestAmount || '');
	};
	LoanTransaction.prototype.getTransactionDate = function getTransactionDate() {
		return this.transactionDate;
	};
	LoanTransaction.prototype.setTransactionDate = function setTransactionDate(transactionDate) {
		this.transactionDate = transactionDate;
	};
	LoanTransaction.prototype.getCreatedDate = function getCreatedDate() {
		return this.createdDate;
	};
	LoanTransaction.prototype.setCreatedDate = function setCreatedDate(createdDate) {
		this.createdDate = createdDate;
	};
	LoanTransaction.prototype.getModifiedDate = function getModifiedDate() {
		return this.modifiedDate;
	};
	LoanTransaction.prototype.setModifiedDate = function setModifiedDate(modifiedDate) {
		this.modifiedDate = modifiedDate;
	};
	LoanTransaction.prototype.getDescription = function getDescription() {
		return this.description;
	};
	LoanTransaction.prototype.setDescription = function setDescription(description) {
		this.description = description;
	};
	LoanTransaction.prototype.getTxnImageId = function getTxnImageId() {
		return this.txnImageId;
	};
	LoanTransaction.prototype.setTxnImageId = function setTxnImageId(txnImageId) {
		this.txnImageId = txnImageId;
	};
	LoanTransaction.prototype.saveLoanTxn = function saveLoanTxn() {
		var showToast = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
		var logResult = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

		if (!(this instanceof LoanTransaction)) {
			return;
		}
		var retVal = 0;
		var MyAnalytics = require('../Utilities/analyticsHelper');
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqliteDBHelper = new SqliteDBHelper();
		if (!this.loanTxnId) {
			var loanTxnId = sqliteDBHelper.createLoanTxn(this);
			if (loanTxnId) {
				this.loanTxnId = loanTxnId;
				showToast && ToastHelper.success(ErrorCode.ERROR_LOAN_TRANSACTION_SAVE_SUCCESS);
				logResult && MyAnalytics.pushEvent('Add Loan Transaction');
			} else {
				showToast && ToastHelper.error(ErrorCode.ERROR_LOAN_TRANSACTION_SAVE_FAILED);
			}
			retVal = loanTxnId;
		} else {
			retVal = sqliteDBHelper.updateLoanTxn(this);
			if (retVal) {
				showToast && ToastHelper.success(ErrorCode.ERROR_LOAN_TRANSACTION_UPDATE_SUCCESS);
				logResult && MyAnalytics.pushEvent('Update Loan Transaction');
			} else {
				showToast && ToastHelper.error(ErrorCode.ERROR_LOAN_TRANSACTION_UPDATE_FAILED);
			}
		}
		return retVal;
	};
	LoanTransaction.prototype.deleteLoanTxn = function deleteLoanTxn() {
		if (!(this instanceof LoanTransaction)) {
			return;
		}
		var retVal = 0;
		var MyAnalytics = require('../Utilities/analyticsHelper');
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqliteDBHelper = new SqliteDBHelper();
		if (this.loanTxnId) {
			MyAnalytics.pushEvent('Delete Loan Transaction');
			retVal = sqliteDBHelper.deleteLoanTxn(this.loanTxnId);
		}
		return retVal;
	};
	module.exports = LoanTransaction;
})();