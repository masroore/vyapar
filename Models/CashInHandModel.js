var CashInHandModel = function CashInHandModel() {
	var txnDate, txnId, userId, bankId, txnType, subTxnType, amount, description;

	this.getTxnDate = function () {
		return this.txnDate;
	};
	this.setTxnDate = function (txnDate) {
		this.txnDate = txnDate;
	};

	this.getTxnId = function () {
		return this.txnId;
	};
	this.setTxnId = function (txnId) {
		this.txnId = txnId;
	};

	this.getUserId = function () {
		return this.userId;
	};
	this.setUserId = function (userId) {
		this.userId = userId;
	};

	this.getBankId = function () {
		return this.bankId;
	};
	this.setBankId = function (bankId) {
		this.bankId = bankId;
	};

	this.getTxnType = function () {
		return this.txnType;
	};
	this.setTxnType = function (txnType) {
		this.txnType = txnType;
	};

	this.getSubTxnType = function () {
		return this.subTxnType;
	};
	this.setSubTxnType = function (subTxnType) {
		this.subTxnType = subTxnType;
	};

	this.getAmount = function () {
		return this.amount;
	};
	this.setAmount = function (amount) {
		this.amount = amount;
	};

	this.getDescription = function () {
		return this.description;
	};
	this.setDescription = function (description) {
		this.description = description;
	};
};

module.exports = CashInHandModel;