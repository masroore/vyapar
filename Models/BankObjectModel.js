var BankObjectModel = function BankObjectModel() {
	this.txnDate;
	this.txnModifiedDate;
	this.txnId;
	this.userId;
	this.txnType; // For cheque transfer, txnType will be TXN_TYPE_CHEQUE_TRANSFER; real txn type of transaction why cheque was received will be stored in subTxntype
	this.subTxnType; // Used only for cheque transfer;
	this.amount;
	this.description;
	this.toBankId;
	this.fromBankId;
	this.txnDescription;

	this.getUserId = function () {
		return this.userId;
	};

	this.setUserId = function (userId) {
		this.userId = userId;
	};

	this.getTxnId = function () {
		return this.txnId;
	};

	this.setTxnId = function (txnId) {
		this.txnId = txnId;
	};

	this.getTxnDate = function () {
		return this.txnDate;
	};

	this.setTxnDate = function (txnDate) {
		this.txnDate = txnDate;
	};
	this.getTxnDate = function () {
		return this.txnDate;
	};

	this.setTxnDate = function (txnDate) {
		this.txnDate = txnDate;
	};

	this.getTxnModifiedDate = function () {
		return this.txnModifiedDate;
	};

	this.setTxnModifiedDate = function (txnDate) {
		this.txnModifiedDate = txnDate;
	};

	this.getTxnType = function () {
		return this.txnType;
	};

	this.setTxnType = function (txnType) {
		this.txnType = txnType;
	};

	this.getAmount = function () {
		return this.amount;
	};

	this.setAmount = function (amount) {
		this.amount = amount;
	};

	this.getDescription = function () {
		return this.description;
	};

	this.setDescription = function (description) {
		this.description = description;
	};

	this.getTxnDescription = function () {
		return this.txnDescription;
	};

	this.setTxnDescription = function (txnDescription) {
		this.txnDescription = txnDescription;
	};

	this.getSubTxnType = function () {
		return this.subTxnType;
	};

	this.setSubTxnType = function (subTxnType) {
		this.subTxnType = subTxnType;
	};

	this.getToBankId = function () {
		return this.toBankId;
	};

	this.setToBankId = function (toBankId) {
		this.toBankId = toBankId;
	};

	this.getFromBankId = function () {
		return this.fromBankId;
	};

	this.setFromBankId = function (fromBankId) {
		this.fromBankId = fromBankId;
	};
};

module.exports = BankObjectModel;