var ReverseChargeObject = function ReverseChargeObject() {
	this.txnId;
	this.nameId;
	this.cashAmount;
	this.balanceAmount;
	this.invoicePrefix;
	this.invoiceNumber;
	this.txnDate;
	this.totalTaxAmount;
	this.txnType;

	this.getTxnId = function () {
		return this.txnId;
	};
	this.setTxnId = function (txnId) {
		this.txnId = txnId;
	};

	this.getNameId = function () {
		return this.nameId;
	};
	this.setNameId = function (nameId) {
		this.nameId = nameId;
	};

	this.getCashAmount = function () {
		return this.cashAmount;
	};
	this.setCashAmount = function (cashAmount) {
		this.cashAmount = cashAmount;
	};

	this.getBalanceAmount = function () {
		return this.balanceAmount;
	};
	this.setBalanceAmount = function (balanceAmount) {
		this.balanceAmount = balanceAmount;
	};

	this.getInvoicePrefix = function () {
		return this.invoicePrefix;
	};
	this.setInvoicePrefix = function (invoicePrefix) {
		this.invoicePrefix = invoicePrefix;
	};

	this.getInvoiceNumber = function () {
		return this.invoiceNumber;
	};
	this.setInvoiceNumber = function (invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	};

	this.getTxnDate = function () {
		return this.txnDate;
	};
	this.setTxnDate = function (txnDate) {
		this.txnDate = txnDate;
	};

	this.getTxnType = function () {
		return this.txnType;
	};
	this.setTxnType = function (txnType) {
		this.txnType = txnType;
	};

	this.getTotalTaxAmount = function () {
		return this.totalTaxAmount;
	};
	this.setTotalTaxAmount = function (totalTaxAmount) {
		this.totalTaxAmount = totalTaxAmount;
	};
};

module.exports = ReverseChargeObject;