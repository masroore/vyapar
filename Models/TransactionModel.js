var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var TransactionStatus = require('./../Constants/TransactionStatus.js');
var TxnTaxType = require('./../Constants/ItemType.js');
var ErrorCode = require('./../Constants/ErrorCode.js');
var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
var TransactionModel = function TransactionModel() {
	sqlitedbhelper = new SqliteDBHelper();
	var txn_id = void 0;
	var txn_name_id = void 0;
	var txn_date = void 0;
	var cash_amount = 0;
	var balance_amount = 0;
	var txn_type = void 0;
	var image_path = null;
	var txn_description = '';
	var nameModel = void 0;
	var discount_percent = 0.0;
	var tax_percent = 0.0;
	var discount_amount = 0.0;
	var tax_amount = 0.0;
	var txn_ref_no = 0;
	var paymentTypeId = 1;
	var bankId = void 0;
	var toBankId = void 0;
	var txn_status = TransactionStatus.TXN_OPEN;
	var custom_fields = '';
	var display_name = '';
	var reverse_charge = 0;
	var txn_due_date = new Date();
	var txn_sub_type = 0;
	var firm_id = null;
	var paymentTypeReference = '';
	var txnImageId = void 0;
	var imagePath = '';
	var ac1_amount = 0;
	var ac2_amount = 0;
	var ac3_amount = 0;
	var invoice_prefix = '';
	var transaction_tax_id = void 0;
	var placeOfSupply = '';
	var roundOffValue = 0;
	var txnITCApplicable = void 0;
	var PONumber = '';
	var PODate = null;
	var txnReturnDate = null;
	var txnReturnRefNumber = '';
	var createdAt = null;
	var ewayBillNumber = '';
	var txnCurrentBalanceAmount = 0;
	var txnPaymentStatus = void 0;
	var invoicePrefixId = null;
	var txnPaymentTermId = null;
	var extraFieldObjectArray = [];
	var billingAddress = '';
	var shippingAddress = '';

	this.setBillingAddress = function (value) {
		billingAddress = value;
	};

	this.getBillingAddress = function () {
		return billingAddress;
	};

	this.setShippingAddress = function (value) {
		shippingAddress = value;
	};

	this.getShippingAddress = function () {
		return shippingAddress;
	};
	var taxInclusive = TxnTaxType.ITEM_TXN_TAX_EXCLUSIVE;

	this.getTaxTypeInclusive = function () {
		return taxInclusive;
	};

	this.setTaxTypeInclusive = function (tax_inclusive) {
		taxInclusive = tax_inclusive;
	};

	this.setInvoicePrefixId = function (invoice_prefix_id) {
		invoicePrefixId = invoice_prefix_id;
	};

	this.getInvoicePrefixId = function () {
		return invoicePrefixId;
	};

	this.getUdfObjectArray = function () {
		return extraFieldObjectArray;
	};

	this.setUdfObjectArray = function (ExtraFieldObjectArray) {
		if (ExtraFieldObjectArray && ExtraFieldObjectArray.length) {
			if (!Array.isArray(ExtraFieldObjectArray)) {
				throw 'udf fields are noan array';
			}
		}
		extraFieldObjectArray = ExtraFieldObjectArray;
	};

	this.getTxnPaymentTermId = function () {
		return txnPaymentTermId;
	};
	this.setTxnPaymentTermId = function (txnPaymenttermId) {
		txnPaymentTermId = txnPaymenttermId;
	};
	this.getTxnCurrentBalanceAmount = function () {
		return txnCurrentBalanceAmount;
	};
	this.setTxnCurrentBalanceAmount = function (txnCurrentbalanceAmount) {
		txnCurrentBalanceAmount = txnCurrentbalanceAmount;
	};

	this.getTxnPaymentStatus = function () {
		return txnPaymentStatus;
	};
	this.setTxnPaymentStatus = function (txnPaymentstatus) {
		txnPaymentStatus = txnPaymentstatus;
	};

	this.getCreatedAt = function () {
		return createdAt;
	};
	this.setCreatedAt = function (createdat) {
		createdAt = createdat;
	};

	this.getEwayBillNumber = function () {
		return ewayBillNumber;
	};

	this.setEwayBillNumber = function (ewayBillnumber) {
		ewayBillNumber = ewayBillnumber;
	};

	this.getTxnReturnDate = function () {
		return txnReturnDate;
	};
	this.setTxnReturnDate = function (txnReturndate) {
		txnReturnDate = txnReturndate;
	};

	this.getTxnReturnRefNumber = function () {
		return txnReturnRefNumber;
	};
	this.setTxnReturnRefNumber = function (txnReturnrefNumber) {
		txnReturnRefNumber = txnReturnrefNumber;
	};

	this.getPONumber = function () {
		return PONumber;
	};
	this.setPONumber = function (PoNumber) {
		PONumber = PoNumber;
	};

	this.getPODate = function () {
		return PODate;
	};
	this.setPODate = function (PoDate) {
		PODate = PoDate;
	};

	this.getTxnITCApplicable = function () {
		return txnITCApplicable;
	};
	this.setTxnITCApplicable = function (txnITCapplicable) {
		txnITCApplicable = txnITCapplicable;
	};

	this.getRoundOffValue = function () {
		return roundOffValue;
	};
	this.setRoundOffValue = function (roundOffvalue) {
		roundOffValue = roundOffvalue;
	};

	this.getPlaceOfSupply = function () {
		return placeOfSupply;
	};
	this.setPlaceOfSupply = function (placeOfsupply) {
		placeOfSupply = placeOfsupply;
	};

	this.set_transaction_tax_id = function (transaction_tax_Id) {
		transaction_tax_id = transaction_tax_Id;
	};
	this.get_transaction_tax_id = function () {
		return transaction_tax_id;
	};

	this.get_txn_sub_type = function () {
		return txn_sub_type;
	};
	this.set_txn_sub_type = function (txn_sub_Type) {
		txn_sub_type = txn_sub_Type;
	};

	this.get_firm_id = function () {
		return firm_id;
	};
	this.set_firm_id = function (firm_Id) {
		firm_id = firm_Id;
	};

	this.get_invoice_prefix = function () {
		return invoice_prefix;
	};
	this.set_invoice_prefix = function (invoice_Prefix) {
		invoice_prefix = invoice_Prefix;
	};

	this.get_ac1_amount = function () {
		return ac1_amount;
	};
	this.set_ac1_amount = function (ac1_Amount) {
		ac1_amount = ac1_Amount;
	};

	this.get_ac2_amount = function () {
		return ac2_amount;
	};
	this.set_ac2_amount = function (ac2_Amount) {
		ac2_amount = ac2_Amount;
	};

	this.get_ac3_amount = function () {
		return ac3_amount;
	};
	this.set_ac3_amount = function (ac3_Amount) {
		ac3_amount = ac3_Amount;
	};

	this.get_image_id = function () {
		return txnImageId;
	};
	this.set_image_id = function (txnImageid) {
		txnImageId = txnImageid;
	};

	this.set_txn_description = function (txn_Description) {
		txn_description = txn_Description;
	};
	this.get_txn_description = function () {
		return txn_description;
	};

	this.get_txn_name_id = function () {
		return txn_name_id;
	};
	this.set_txn_name_id = function (txn_name_Id) {
		txn_name_id = txn_name_Id;
	};

	this.set_cash_amount = function (cashAmount) {
		cash_amount = cashAmount;
	};
	this.get_cash_amount = function () {
		return cash_amount;
	};

	this.set_balance_amount = function (balance_Amount) {
		balance_amount = balance_Amount;
	};
	this.get_balance_amount = function () {
		return balance_amount;
	};

	this.set_txn_type = function (txn_Type) {
		txn_type = txn_Type;
	};
	this.get_txn_type = function () {
		return txn_type;
	};

	this.set_txn_date = function (txn_Date) {
		txn_date = txn_Date;
	};
	this.get_txn_date = function () {
		return txn_date;
	};

	this.set_txn_due_date = function (txn_due_Date) {
		txn_due_date = txn_due_Date;
	};
	this.get_txn_due_date = function () {
		return txn_due_date;
	};

	this.set_discount_percent = function (discount_Percent) {
		discount_percent = discount_Percent;
	};
	this.get_discount_percent = function () {
		return discount_percent;
	};

	this.set_discount_amount = function (discount_Amount) {
		discount_amount = discount_Amount;
	};
	this.get_discount_amount = function () {
		return discount_amount;
	};

	this.set_tax_percent = function (tax_Percent) {
		tax_percent = tax_Percent;
	};
	this.get_tax_percent = function () {
		return tax_percent;
	};

	this.set_tax_amount = function (tax_Amount) {
		tax_amount = tax_Amount;
	};
	this.get_tax_amount = function () {
		return tax_amount;
	};

	this.set_txn_ref_no = function (txn_ref_Number) {
		txn_ref_no = txn_ref_Number;
	};
	this.get_txn_ref_no = function () {
		return txn_ref_no;
	};

	this.get_image_path = function () {
		return image_path;
	};
	this.set_image_path = function (image_Path) {
		image_path = image_Path;
	};

	this.setPaymentTypeId = function (paymentTypeid) {
		paymentTypeId = paymentTypeid;
	};
	this.getPaymentTypeId = function () {
		return paymentTypeId;
	};

	this.set_txn_status = function (txn_Status) {
		txn_status = txn_Status;
	};
	this.get_txn_status = function () {
		return txn_status;
	};

	this.setPaymentTypeReference = function (paymentTypereference) {
		paymentTypeReference = paymentTypereference;
	};
	this.getPaymentTypeReference = function () {
		return paymentTypeReference;
	};
	this.get_txn_id = function () {
		return txn_id;
	};
	this.set_txn_id = function (txn_Id) {
		txn_id = txn_Id;
	};

	this.get_custom_fields = function () {
		return custom_fields;
	};
	this.set_custom_fields = function (custom_Fields) {
		custom_fields = custom_Fields;
	};

	this.get_display_name = function () {
		return display_name;
	};
	this.set_display_name = function (display_Name) {
		display_name = display_Name;
	};

	this.get_reverse_charge = function () {
		return reverse_charge;
	};
	this.set_reverse_charge = function (reverse_Charge) {
		reverse_charge = reverse_Charge;
	};

	this.addTransaction = function () {
		var statusCode = TxnTypeConstant['SUCCESS'];
		if (statusCode != ErrorCode['ERROR_TXN_REFNO_ALREADY_USED'] && statusCode != ErrorCode['FAILED']) {
			var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
			sqlitedbhelper = new SqliteDBHelper();
			var txnImgId = 0;
			if (image_path && image_path.indexOf('.svg') == -1) {
				txnImgId = sqlitedbhelper.createTransactionImage(image_path);
				if (txnImgId > 0) {
					this.set_image_id(txnImgId);
				}
			}
			var txn_id = sqlitedbhelper.createTransactionRecord(this);
			if (!txn_id || txn_id <= 0) {
				statusCode = ErrorCode['ERROR_TXN_SAVE_FAILED'];
			} else {
				this.set_txn_id(txn_id);

				statusCode = ErrorCode['ERROR_TXN_SAVE_SUCCESS'];
			}
			return statusCode;
		}
	};

	this.updateTransaction = function () {
		var sqlitedbhelper = new SqliteDBHelper();
		var statusCode = ErrorCode.SUCCESS;
		var imageToBeRemoved = 0;

		// if imageid is set it means its edit scenario and image was attached in old transaction
		if (txnImageId) {
			// if image path is set it means new transaction contains image
			if (image_path) {
				// if image is blob type it means image has not been modified.
				if (!image_path.match('^data')) {
					sqlitedbhelper.updateTransactionImage(image_path, txnImageId);
				}
			} else {
				// old image has been removed
				imageToBeRemoved = txnImageId;
				this.set_image_id(0);
			}
		} else if (image_path) {
			// image was not attached earlier and now it is attached
			var txnImgId = sqlitedbhelper.createTransactionImage(image_path);
			if (txnImgId > 0) {
				this.set_image_id(txnImgId);
			}
		}

		statusCode = sqlitedbhelper.UpdateTransactionRecord(this);
		if (statusCode == ErrorCode.ERROR_TXN_SAVE_SUCCESS && imageToBeRemoved) {
			sqlitedbhelper.removeTransactionImage(imageToBeRemoved);
		}
		return statusCode;
	};

	this.updateFTSTxnRecord = function (lineItems) {
		var that = this;
		var text = '';
		var ItemCache = require('./../Cache/ItemCache.js');
		var itemCache = new ItemCache();
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		if (lineItems.length > 0) {
			$.each(lineItems, function (key, value) {
				var itemCode = ' ';
				var itemHSNCode = ' ';
				var itemLogic = itemCache.findItemByName(value['itemName'], that.get_txn_type());
				if (itemLogic) {
					itemCode = itemLogic.getItemCode();
					itemHSNCode = itemLogic.getItemHSNCode();
				}
				text = text + ' ' + value['itemName'];
				text = text + ' ' + (value.getLineItemDescription() ? value.getLineItemDescription() : '');
				text = text + ' ' + (value.getLineItemSerialNumber() ? value.getLineItemSerialNumber() : '');
				text = text + ' ' + (value.getLineItemBatchNumber() ? value.getLineItemBatchNumber() : '');
				text = text + ' ' + itemCode;
				text = text + ' ' + itemHSNCode;
			});
		}
		if (ewayBillNumber) {
			text += ' ' + ewayBillNumber;
		}
		var sqlitedbhelper = new SqliteDBHelper();
		sqlitedbhelper.updateFTSTxnRecord(this, text);
	};

	this.deleteTransactionForNameId = function (name_id) {
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqliteDBHelper = new SqliteDBHelper();
		if (sqliteDBHelper.deleteTransactionsForName(name_id) == true) {
			sqliteDBHelper.deleteFTSTxnForNameRecord(name_id);

			return true;
		} else {
			return false;
		}
	};

	this.deleteLinkedTransactions = function () {
		var statusCode = ErrorCode.ERROR_TXN_LINK_DELETE_SUCCESS;
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.deleteLinkedTransactions(txn_id);
	};

	this.deleteTransaction = function () {
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqliteDBHelper = new SqliteDBHelper();
		debugger;
		if (sqliteDBHelper.deleteTransactionRecord(txn_id) == true) {
			if (txnImageId) {
				sqliteDBHelper.removeTransactionImage(txnImageId);
			}
			sqliteDBHelper.deleteFTSTxnRecord(txn_id);

			return true;
		} else {
			return false;
		}
	};

	this.deleteUnUsedIST = function (oldLineItemItemIds) {
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.deleteUnusedIst(oldLineItemItemIds);
	};

	this.canEditOrDeleteTransaction = function () {
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.canEditOrDeleteTransaction(txn_id);
	};

	this.convertToSale = function (destinationId) {
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqliteDBHelper = new SqliteDBHelper();
		var id = txn_id;
		return sqliteDBHelper.convertToSale(id, destinationId);
	};
	this.closeOrderForm = function () {
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqliteDBHelper = new SqliteDBHelper();
		var id = txn_id;
		return sqliteDBHelper.closeOrderForm(id);
	};

	this.getLatestTxnRefNumber = function () {
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.getLatestTxnRefNumber(txn_type, txn_sub_type, firm_id);
	};

	this.getBankId = function () {
		return bankId;
	};

	this.setBankId = function (bankid) {
		bankId = bankid;
	};

	this.getToBankId = function () {
		return toBankId;
	};
	this.setToBankId = function (tobankId) {
		toBankId = tobankId;
	};
};
module.exports = TransactionModel;