var ItemAdjModel = function ItemAdjModel() {
	var ErrorCode = require('./../Constants/ErrorCode.js');
	var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
	var itemAdjId, itemAdjItemId, itemAdjType, itemAdjQuantity, itemAdjDescription, itemAdjDate, itemAdjAtPrice;

	this.itemAdjIstId = null;
	this.itemAdjId = 0;

	this.getItemAdjIstId = function () {
		return this.itemAdjIstId;
	};
	this.setItemAdjIstId = function (itemAdjIstId) {
		this.itemAdjIstId = itemAdjIstId;
	};

	this.getItemAdjAtPrice = function () {
		return this.itemAdjAtPrice;
	};
	this.setItemAdjAtPrice = function (itemAdjAtPrice) {
		this.itemAdjAtPrice = itemAdjAtPrice;
	};

	this.getItemAdjDateCreated = function () {
		return this.itemAdjDateCreated;
	};
	this.setItemAdjDateCreated = function (itemAdjDateCreated) {
		this.itemAdjDateCreated = itemAdjDateCreated;
	};

	this.getItemAdjDateModified = function () {
		return this.itemAdjDateModified;
	};
	this.setItemAdjDateModified = function (itemAdjDateModified) {
		this.itemAdjDateModified = itemAdjDateModified;
	};

	this.getItemAdjId = function () {
		return this.itemAdjId;
	};
	this.setItemAdjId = function (itemAdjId) {
		this.itemAdjId = itemAdjId;
	};

	this.getItemAdjItemId = function () {
		return this.itemAdjItemId;
	};
	this.setItemAdjItemId = function (itemAdjItemId) {
		this.itemAdjItemId = itemAdjItemId;
	};

	this.getItemAdjType = function () {
		return this.itemAdjType;
	};
	this.setItemAdjType = function (itemAdjType) {
		this.itemAdjType = itemAdjType;
	};

	this.getItemAdjQuantity = function () {
		return this.itemAdjQuantity;
	};
	this.setItemAdjQuantity = function (itemAdjQuantity) {
		this.itemAdjQuantity = itemAdjQuantity;
	};

	this.getItemAdjDescription = function () {
		return this.itemAdjDescription;
	};
	this.setItemAdjDescription = function (itemAdjDescription) {
		this.itemAdjDescription = itemAdjDescription;
	};

	this.getItemAdjDate = function () {
		return this.itemAdjDate;
	};
	this.setItemAdjDate = function (itemAdjDate) {
		this.itemAdjDate = itemAdjDate;
	};

	this.addItemAdjustment = function () {
		var statusCode = ErrorCode.ERROR_ITEM_ADJ_SAVE_FAILED;
		sqlitedbhelper = new SqliteDBHelper();
		var retVal = sqlitedbhelper.createItemAdjRecord(this);
		if (retVal > 0) {
			this.setItemAdjId(retVal);
			statusCode = ErrorCode.ERROR_ITEM_ADJ_SAVE_SUCCESS;
		} else {
			statusCode = ErrorCode.ERROR_ITEM_ADJ_SAVE_FAILED;
		}
		return statusCode;
	};

	this.deleteItemAdjustment = function () {
		sqlitedbhelper = new SqliteDBHelper();
		var errorCode = ErrorCode.ERROR_ITEM_ADJ_DELETE_FAILED;
		var retVal = sqlitedbhelper.deleteItemAdjRecord(this.itemAdjId);
		if (retVal == true) {
			statusCode = ErrorCode.ERROR_ITEM_ADJ_DELETE_SUCCESS;
		}
		return statusCode;
	};

	this.updateItemAdjustment = function () {
		sqlitedbhelper = new SqliteDBHelper();
		return sqlitedbhelper.updateItemAdjustment(this);
	};
};

module.exports = ItemAdjModel;