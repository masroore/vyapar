var sqliteDBHelperForUDf = null;
var uDFConstants = require('./../Constants/UDFFieldsConstant.js');
var StringConstant = require('./../Constants/StringConstants.js');
var MyDate = require('./../Utilities/MyDate.js');
var UDFValueModel = function UDFValueModel() {
	var udfFieldId = 0;
	var udfFieldType = 0;
	var udfFieldValue = '';
	var udfRefId = 0;

	this.getUdfFieldId = function () {
		return udfFieldId;
	};
	this.setUdfFieldId = function (UdfFieldId) {
		udfFieldId = UdfFieldId;
	};

	this.getUdfFieldType = function () {
		return udfFieldType;
	};
	this.setUdfFieldType = function (UdfFieldType) {
		udfFieldType = UdfFieldType;
	};

	this.getUdfFieldValue = function () {
		return udfFieldValue;
	};
	this.setUdfFieldValue = function (UdfFieldValue) {
		udfFieldValue = UdfFieldValue;
	};

	this.getUdfRefId = function () {
		return udfRefId;
	};
	this.setUdfRefId = function (UdfRefId) {
		udfRefId = UdfRefId;
	};

	this.getDisplayValue = function (fieldModel) {
		if (udfFieldValue) {
			if (fieldModel.getUdfFieldDataType() == uDFConstants.DATA_TYPE_DATE) {
				if (fieldModel.getUdfFieldDataFormat() == StringConstant.monthYear) {
					var dateVal = MyDate.getDateObj(udfFieldValue, 'yyyy-mm-dd', '-');
					dateVal = MyDate.getDate('m/y', dateVal);
					return dateVal;
				} else {
					var _dateVal = MyDate.getDateObj(udfFieldValue, 'yyyy-mm-dd', '-');
					_dateVal = MyDate.getDate('d/m/y', _dateVal);
					return _dateVal;
				}
			}
		}
		return udfFieldValue;
	};

	this.updateUdfValues = function () {
		if (!sqliteDBHelperForUDf) {
			var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
			sqliteDBHelperForUDf = new SqliteDBHelper();
		}
		return sqliteDBHelperForUDf.updateUDFValues(this);
	};
	this.deleteExtraFieldsValue = function (txnId, fieldType) {
		if (!sqliteDBHelperForUDf) {
			var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
			sqliteDBHelperForUDf = new SqliteDBHelper();
		}
		return sqliteDBHelperForUDf.deleteExtraFieldsValue(txnId, fieldType);
	};
};
module.exports = UDFValueModel;