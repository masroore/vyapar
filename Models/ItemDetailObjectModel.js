var ItemDetailObjectModel = function ItemDetailObjectModel() {
	this.txnId = 0;
	this.txnType = 0;
	this.txnDate = '';
	this.txnQuantity = 0.0;
	this.itemFreeQuantity;
	this.userId = 0;
	this.description = '';
	this.lineItemunitId = '';
	this.mappingId = '';
	this.paymentStatus = '';
	this.itemTaxId;

	this.getItemTaxId = function () {
		return this.itemTaxId;
	};
	this.setItemTaxId = function (itemTaxId) {
		this.itemTaxId = itemTaxId;
	};

	this.getDescription = function () {
		return this.description;
	};
	this.setDescription = function (description) {
		this.description = description;
	};

	this.getPaymentStatus = function () {
		return this.paymentStatus;
	};
	this.setPaymentStatus = function (status) {
		this.paymentStatus = status;
	};

	this.getTxnId = function () {
		return this.txnId;
	};
	this.setTxnId = function (txnId) {
		this.txnId = txnId;
	};

	this.getTxnType = function () {
		return this.txnType;
	};
	this.setTxnType = function (txnType) {
		this.txnType = txnType;
	};

	this.getTxnDate = function () {
		return this.txnDate;
	};
	this.setTxnDate = function (txnDate) {
		this.txnDate = txnDate;
	};

	this.getTxnQuantity = function () {
		return this.txnQuantity;
	};
	this.setTxnQuantity = function (txnQuantity) {
		this.txnQuantity = txnQuantity;
	};

	this.getUserId = function () {
		return this.userId;
	};
	this.setUserId = function (userId) {
		this.userId = userId;
	};

	this.getMappingId = function () {
		return this.mappingId;
	};
	this.setMappingId = function (mappingId) {
		this.mappingId = mappingId;
	};

	this.getLineItemUnitId = function () {
		return this.lineItemUnitId;
	};
	this.setLineItemUnitId = function (lineItemUnitId) {
		this.lineItemUnitId = lineItemUnitId;
	};

	this.getPricePerUnit = function () {
		return this.pricePerUnit;
	};
	this.setPricePerUnit = function (pricePerUnit) {
		this.pricePerUnit = pricePerUnit;
	};

	this.getItemFreeQuantity = function () {
		return this.itemFreeQuantity;
	};
	this.setItemFreeQuantity = function (itemFreeQuantity) {
		this.itemFreeQuantity = itemFreeQuantity;
	};
};

module.exports = ItemDetailObjectModel;