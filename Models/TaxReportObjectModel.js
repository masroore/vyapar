var TaxReportObjectModel = function TaxReportObjectModel() {
	this.nameid = 0;
	this.taxIn = 0;
	this.taxOut = 0;

	this.getNameId = function () {
		return this.nameid;
	};

	this.setNameId = function (nameid) {
		this.nameid = nameid;
	};

	this.getTaxIn = function () {
		return this.taxIn;
	};

	this.setTaxIn = function (taxIn) {
		this.taxIn = taxIn;
	};

	this.getTaxOut = function () {
		return this.taxOut;
	};

	this.setTaxOut = function (taxOut) {
		this.taxOut = taxOut;
	};
};

module.exports = TaxReportObjectModel;