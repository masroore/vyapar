var ItemStockTrackingModel = function ItemStockTrackingModel() {
	var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
	var sqlitedbhelper = new SqliteDBHelper();

	this.istId;
	this.istBatchNumber;
	this.istSerialNumber;
	this.istMrp;
	this.istExpiryDate;
	this.istManufacturingDate;
	this.istSize;
	this.istSize;
	this.istItemId;
	this.istCurrentQuantity;
	this.istOpeningQuantity;
	this.istType;
	this.dataEditable = true;

	this.isDataEditable = function () {
		return this.dataEditable;
	};
	this.setDataEditable = function (dataEditable) {
		this.dataEditable = dataEditable;
	};

	this.getIstOpeningQuantity = function () {
		return this.istOpeningQuantity;
	};
	this.setIstOpeningQuantity = function (istOpeningQuantity) {
		this.istOpeningQuantity = istOpeningQuantity;
	};

	this.getIstType = function () {
		return this.istType;
	};
	this.setIstType = function (istType) {
		this.istType = istType;
	};

	this.getIstId = function () {
		return this.istId;
	};
	this.setIstId = function (istId) {
		this.istId = istId;
	};

	this.getIstBatchNumber = function () {
		return this.istBatchNumber;
	};
	this.setIstBatchNumber = function (istBatchNuber) {
		this.istBatchNumber = istBatchNuber;
	};

	this.getIstSerialNumber = function () {
		return this.istSerialNumber;
	};
	this.setIstSerialNumber = function (istSerialNumber) {
		this.istSerialNumber = istSerialNumber;
	};

	this.getIstMrp = function () {
		return this.istMrp;
	};
	this.setIstMrp = function (istMrp) {
		this.istMrp = istMrp;
	};

	this.getIstExpiryDate = function () {
		return this.istExpiryDate;
	};
	this.setIstExpiryDate = function (istExpiryDate) {
		this.istExpiryDate = istExpiryDate;
	};

	this.getIstManufacturingDate = function () {
		return this.istManufacturingDate;
	};
	this.setIstManufacturingDate = function (istManufacturingDate) {
		this.istManufacturingDate = istManufacturingDate;
	};

	this.getIstSize = function () {
		return this.istSize;
	};
	this.setIstSize = function (istSize) {
		this.istSize = istSize;
	};

	this.getIstItemId = function () {
		return this.istItemId;
	};
	this.setIstItemId = function (istItemId) {
		this.istItemId = istItemId;
	};

	this.getIstCurrentQuantity = function () {
		return this.istCurrentQuantity;
	};
	this.setIstCurrentQuantity = function (istCurrentQuantity) {
		this.istCurrentQuantity = istCurrentQuantity;
	};

	this.getItemStockTrackingRecordsByIstId = function (istId) {
		var itemStockTrackingModel = sqlitedbhelper.getItemStockTrackingRecordsByIstId(istId);
		var ItemStockTracking = require('./../BizLogic/ItemStockTracking.js');
		var itemStockTracking;
		if (itemStockTrackingModel) {
			var txn = itemStockTrackingModel;
			itemStockTracking = new ItemStockTracking();
			itemStockTracking.setIstId(txn.getIstId());
			itemStockTracking.setIstItemId(txn.getIstItemId());
			itemStockTracking.setIstBatchNumber(txn.getIstBatchNumber());
			itemStockTracking.setIstCurrentQuantity(txn.getIstCurrentQuantity());
			itemStockTracking.setIstExpiryDate(txn.getIstExpiryDate() ? MyDate.getDateObj(txn.getIstExpiryDate(), 'yyyy-mm-dd', '-') : null);
			itemStockTracking.setIstManufacturingDate(txn.getIstManufacturingDate() ? MyDate.getDateObj(txn.getIstManufacturingDate(), 'yyyy-mm-dd', '-') : null);
			itemStockTracking.setIstMrp(txn.getIstMrp());
			itemStockTracking.setIstSerialNumber(txn.getIstSerialNumber());
			itemStockTracking.setIstSize(txn.getIstSize());
		}
		return itemStockTracking;
	};

	this.getISTWithParameters = function (itemBatchNo, itemSerialNumber, itemMRP, itemExpiryDate, itemManufacturingDate, itemSize, itemId) {
		var itemStockTrackingModel = sqlitedbhelper.loadISTWithParameters(itemBatchNo, itemSerialNumber, itemMRP, itemExpiryDate, itemManufacturingDate, itemSize, itemId);
		return itemStockTrackingModel;
	};

	this.changeISTQuantity = function (istId, currentQty, OpeningQty, istType) {
		var statusCode = sqlitedbhelper.changeISTQuantity(istId, currentQty, OpeningQty, istType);
		return statusCode;
	};

	this.addItemStockTracking = function (itemBatchNo, itemSerialNumber, itemMRP, itemExpiryDate, itemManufacturingDate, itemSize, itemId, itemQuantityStr, istType) {
		var txn_id = sqlitedbhelper.createItemStockTrackingRecord(itemBatchNo, itemSerialNumber, itemMRP, itemExpiryDate, itemManufacturingDate, itemSize, itemId, itemQuantityStr, istType);
		return txn_id;
	};

	this.getItemStockTrackingList = function (itemId, filterZeroAndNegativeQty, serialNumber, checkSerialNumber, checkType, istType, nameId, txnType) {
		var itemStockTrackingModel = sqlitedbhelper.getItemStockTrackingModelList(itemId, filterZeroAndNegativeQty, serialNumber, checkSerialNumber, checkType, istType, nameId, txnType);
		var ItemStockTracking = require('./../BizLogic/ItemStockTracking.js');
		var istList = [];
		if (itemStockTrackingModel.length > 0) {
			for (var i = 0; i < itemStockTrackingModel.length; i++) {
				var txn = itemStockTrackingModel[i];
				var itemStockTracking = new ItemStockTracking();
				itemStockTracking.setIstId(txn.getIstId());
				itemStockTracking.setIstItemId(txn.getIstItemId());
				itemStockTracking.setIstBatchNumber(txn.getIstBatchNumber());
				itemStockTracking.setIstCurrentQuantity(txn.getIstCurrentQuantity());
				itemStockTracking.setIstExpiryDate(txn.getIstExpiryDate() ? MyDate.getDateObj(txn.getIstExpiryDate(), 'yyyy-mm-dd', '-') : null);
				itemStockTracking.setIstManufacturingDate(txn.getIstManufacturingDate() ? MyDate.getDateObj(txn.getIstManufacturingDate(), 'yyyy-mm-dd', '-') : null);
				itemStockTracking.setIstMrp(txn.getIstMrp());
				itemStockTracking.setIstSerialNumber(txn.getIstSerialNumber());
				itemStockTracking.setIstSize(txn.getIstSize());
				itemStockTracking.setIstOpeningQuantity(txn.getIstOpeningQuantity());
				itemStockTracking.setDataEditable(txn.isDataEditable());
				itemStockTracking.setIstType(txn.getIstType());
				istList.push(itemStockTracking);
			}
		}

		return istList;
	};

	this.deleteItemStockTracking = function (istId) {
		var statusCode = sqlitedbhelper.deleteItemStockTracking(istId);
		return statusCode;
	};

	this.deleteAllItemStockTrackingForItem = function (itemId) {
		var statusCode = sqlitedbhelper.deleteAllItemStockTrackingForItem(itemId);
		return statusCode;
	};
};

module.exports = ItemStockTrackingModel;