var instance, SqliteDBHelper;
var ChequeModel = function ChequeModel() {
	var ErrorCode = require('./../Constants/ErrorCode.js');
	if (!instance) {
		SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
	}
	this.chequeId = 0;
	this.chequeTxnId = 0;
	this.transferDate;
	this.chequeCurrentStatus;
	this.transferredToAccount = 0;
	this.chequeCloseDescription;
	this.chequeCreationDate;
	this.chequeModificationDate;
	this.txnDate;
	this.chequeAmount;
	this.chequeTxnType;
	this.chequeTxnRefNo;
	var chequeNameId = null;
	var chequeClosedTxnRefId = null;

	this.getChequeClosedTxnRefId = function () {
		return chequeClosedTxnRefId;
	};

	this.setChequeClosedTxnRefId = function (id) {
		chequeClosedTxnRefId = id;
	};

	this.setChequeNameId = function (nameId) {
		chequeNameId = nameId;
	};
	this.getChequeNameId = function () {
		return chequeNameId;
	};

	this.getChequeTxnRefNo = function () {
		return this.chequeTxnRefNo;
	};
	this.setChequeTxnRefNo = function (chequeTxnRefNo) {
		this.chequeTxnRefNo = chequeTxnRefNo;
	};

	this.getChequeId = function () {
		return this.chequeId;
	};
	this.setChequeId = function (chequeId) {
		this.chequeId = chequeId;
	};

	this.getChequeTxnId = function () {
		return this.chequeTxnId;
	};
	this.setChequeTxnId = function (chequeTxnId) {
		this.chequeTxnId = chequeTxnId;
	};

	this.getTransferDate = function () {
		return this.transferDate;
	};
	this.setTransferDate = function (transferDate) {
		this.transferDate = transferDate;
	};

	this.getChequeCurrentStatus = function () {
		return this.chequeCurrentStatus;
	};
	this.setChequeCurrentStatus = function (chequeCurrentStatus) {
		this.chequeCurrentStatus = chequeCurrentStatus;
	};

	this.getTransferredToAccount = function () {
		return this.transferredToAccount;
	};
	this.setTransferredToAccount = function (transferredToAccount) {
		this.transferredToAccount = transferredToAccount;
	};

	this.getChequeCloseDescription = function () {
		return this.chequeCloseDescription;
	};
	this.setChequeCloseDescription = function (chequeCloseDescription) {
		this.chequeCloseDescription = chequeCloseDescription;
	};

	this.getChequeCreationDate = function () {
		return this.chequeCreationDate;
	};
	this.setChequeCreationDate = function (chequeCurrentDate) {
		this.chequeCreationDate = chequeCurrentDate;
	};

	this.getChequeModificationDate = function () {
		return this.chequeModificationDate;
	};
	this.setChequeModificationDate = function (chequeModificationDate) {
		this.chequeModificationDate = chequeModificationDate;
	};

	this.getTxnDate = function () {
		return this.txnDate;
	};
	this.setTxnDate = function (txnDate) {
		this.txnDate = txnDate;
	};

	this.getChequeAmount = function () {
		return this.chequeAmount;
	};
	this.setChequeAmount = function (chequeAmount) {
		this.chequeAmount = chequeAmount;
	};

	this.getChequeTxnType = function () {
		return this.chequeTxnType;
	};
	this.setChequeTxnType = function (chequeTxnType) {
		this.chequeTxnType = chequeTxnType;
	};

	this.addCheque = function () {
		var errorCode = ErrorCode.ERROR_CHEQUE_SAVE_FAILED;
		var sqlitedbhelper = new SqliteDBHelper();
		var retVal = sqlitedbhelper.createChequeRecord(this);
		if (retVal > 0) {
			errorCode = ErrorCode.ERROR_CHEQUE_SAVE_SUCCESS;
		}
		return errorCode;
	};

	this.updateChequeStatus = function () {
		var sqlitedbhelper = new SqliteDBHelper();
		return sqlitedbhelper.UpdateChequeRecord(this);
	};

	this.deleteChequeForTxn = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.deleteChequeForTxn(this.getChequeTxnId());
	};
};

module.exports = ChequeModel;