var LineItemModel = function LineItemModel() {
	var ErrorCode = require('./../Constants/ErrorCode.js');
	var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
	var sqlitedbhelper = new SqliteDBHelper();
	var lineItemId, itemId, transactionId, itemQunatity, itemFreeQuantity, itemUniPrice, lineItemTotal, lineItemTaxAmount, lineItemDiscountAmount;
	this.lineItemUnitId = null;
	this.lineItemUnitMappingId = null;
	this.lineItemTaxId;
	this.lineItemITCValue = 0;
	this.lineItemIstId;
	this.lineItemDiscountPercent = 0;

	this.getLineItemDiscountPercent = function () {
		return this.lineItemDiscountPercent;
	};

	this.setLineItemDiscountPercent = function (lineItemDiscountPercent) {
		this.lineItemDiscountPercent = lineItemDiscountPercent;
	};

	this.getLineItemIstId = function () {
		return this.lineItemIstId;
	};
	this.setLineItemIstId = function (lineItemIstId) {
		this.lineItemIstId = lineItemIstId;
	};

	this.getLineItemITCValue = function () {
		return this.lineItemITCValue;
	};
	this.setLineItemITCValue = function (lineItemITCValue) {
		this.lineItemITCValue = lineItemITCValue;
	};

	this.getLineItemAdditionalCESS = function () {
		return this.itemAdditionalCess;
	};
	this.setLineItemAdditionalCESS = function (cess) {
		this.itemAdditionalCess = cess;
	};

	this.getLineItemMRP = function () {
		return this.itemMRP;
	};
	this.setLineItemMRP = function (itemMRP) {
		this.itemMRP = itemMRP;
	};

	this.getLineItemSize = function () {
		return this.itemSize;
	};
	this.setLineItemSize = function (itemSize) {
		this.itemSize = itemSize;
	};

	this.getLineItemBatchNumber = function () {
		return this.itemBatchNumber;
	};
	this.setLineItemBatchNumber = function (itemBatchNumber) {
		this.itemBatchNumber = itemBatchNumber;
	};

	this.getLineItemExpiryDate = function () {
		return this.itemExpiryDate;
	};
	this.setLineItemExpiryDate = function (itemExpiryDate) {
		this.itemExpiryDate = itemExpiryDate;
	};

	this.getLineItemManufacturingDate = function () {
		return this.itemManufacturingDate;
	};
	this.setLineItemManufacturingDate = function (itemMfDate) {
		this.itemManufacturingDate = itemMfDate;
	};

	this.getLineItemSerialNumber = function () {
		return this.itemSerialNumber;
	};
	this.setLineItemSerialNumber = function (itemSerialNumber) {
		this.itemSerialNumber = itemSerialNumber;
	};

	this.getLineItemCount = function () {
		return this.itemCount;
	};
	this.setLineItemCount = function (itemCount) {
		this.itemCount = itemCount;
	};

	this.getLineItemDescription = function () {
		return this.itemDescription;
	};
	this.setLineItemDescription = function (itemDescription) {
		this.itemDescription = itemDescription;
	};

	this.getLineItemTaxId = function () {
		return this.lineItemTaxId;
	};
	this.setLineItemTaxId = function (lineItemTaxId) {
		this.lineItemTaxId = lineItemTaxId;
	};

	this.setLineItemUnitId = function (lineItemUnitId) {
		this.lineItemUnitId = lineItemUnitId;
	};
	this.getLineItemUnitId = function () {
		return this.lineItemUnitId;
	};

	this.setLineItemUnitMappingId = function (lineItemUnitMappingId) {
		this.lineItemUnitMappingId = lineItemUnitMappingId;
	};
	this.getLineItemUnitMappingId = function () {
		return this.lineItemUnitMappingId;
	};

	this.setLineItemId = function (lineItemId) {
		this.lineItemId = lineItemId;
	};
	this.getLineItemId = function () {
		return this.lineItemId;
	};

	this.setItemId = function (itemId) {
		this.itemId = itemId;
	};
	this.getItemId = function () {
		return this.itemId;
	};

	this.setTransactionId = function (transactionId) {
		this.transactionId = transactionId;
	};
	this.getTransactionId = function () {
		return this.transactionId;
	};

	this.setItemQuantity = function (itemQunatity) {
		this.itemQunatity = itemQunatity;
	};
	this.getItemQuantity = function () {
		return this.itemQunatity;
	};

	this.setItemFreeQuantity = function (itemFreeQuantity) {
		this.itemFreeQuantity = itemFreeQuantity;
	};
	this.getItemFreeQuantity = function () {
		return this.itemFreeQuantity;
	};

	this.setItemUnitPrice = function (itemUniPrice) {
		this.itemUniPrice = itemUniPrice;
	};
	this.getItemUnitPrice = function () {
		return this.itemUniPrice;
	};

	this.setLineItemTotal = function (lineItemTotal) {
		this.lineItemTotal = lineItemTotal;
	};
	this.getLineItemTotal = function () {
		return this.lineItemTotal;
	};

	this.setLineItemTaxAmount = function (lineItemTaxAmount) {
		this.lineItemTaxAmount = lineItemTaxAmount;
	};
	this.getLineItemTaxAmount = function () {
		return this.lineItemTaxAmount;
	};

	this.setLineItemDiscountAmount = function (lineItemDiscountAmount) {
		this.lineItemDiscountAmount = lineItemDiscountAmount;
	};
	this.getLineItemDiscountAmount = function () {
		return this.lineItemDiscountAmount;
	};

	this.addLineItem = function () {
		var statusCode = ErrorCode.ERROR_TXN_SAVE_FAILED;
		// console.log(this);
		var retVal = sqlitedbhelper.createLineItemRecord(this);
		// console.log(retVal);
		if (retVal > 0) {
			this.setItemId(retVal);

			statusCode = ErrorCode.ERROR_TXN_SAVE_SUCCESS;
		} else {
			statusCode = ErrorCode.ERROR_TXN_SAVE_FAILED;
		}
		return statusCode;
	};
};

module.exports = LineItemModel;