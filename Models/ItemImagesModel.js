var ErrorCode = require('../Constants/ErrorCode');
var SqliteDBHelper = require('../DBManager/SqliteDBHelper.js');
var sqlitedbhelper = new SqliteDBHelper();
var ItemImagesModel = function ItemImagesModel() {
	this._itemId;
	this._itemImageId;
	this._base64Image;
	this._catalogueItemId;
	this._hexImage;
};

ItemImagesModel.prototype.getItemId = function () {
	return this._itemId;
};
ItemImagesModel.prototype.setItemId = function (itemId) {
	this._itemId = itemId;
};

ItemImagesModel.prototype.getCatalogueItemId = function () {
	return this._catalogueItemId;
};
ItemImagesModel.prototype.setCatalogueItemId = function (catalogueItemId) {
	this._catalogueItemId = catalogueItemId;
};

ItemImagesModel.prototype.getItemImageId = function () {
	return this._itemImageId;
};
ItemImagesModel.prototype.setItemImageId = function (itemImageId) {
	this._itemImageId = itemImageId;
};

ItemImagesModel.prototype.getBase64Image = function () {
	return this._base64Image;
};

ItemImagesModel.prototype.setBase64Image = function (base64Img) {
	this._base64Image = base64Img;
};

ItemImagesModel.prototype.getHexImage = function () {
	return this._hexImage;
};

ItemImagesModel.prototype.setHexImage = function (hexImg) {
	this._hexImage = hexImg;
};

ItemImagesModel.prototype.addItemImage = function (itemId) {
	try {
		var statusCode = ErrorCode.ERROR_ITEM_IMAGE_SAVE_FAILED;
		var itemImageId = sqlitedbhelper.addItemImage(itemId, this.getBase64Image());
		if (itemImageId > 0) {
			this.setItemId(itemId);
			this.setItemImageId(itemImageId);
			statusCode = ErrorCode.ERROR_ITEM_IMAGE_SAVE_SUCCESS;
		}
		return statusCode;
	} catch (ex) {
		if (logger) {
			logger.error('Error adding item images', ex);
		}
		return 0;
	}
};

ItemImagesModel.prototype.deleteItemImage = function () {
	try {
		var result = sqlitedbhelper.deleteItemImageByImageId(this.getItemImageId());
		return result === ErrorCode.ERROR_ITEM_IMAGE_DELETE_SUCCESS;
	} catch (ex) {
		if (logger) {
			logger.error('Error occurred in deleting item image', ex);
		}
		return false;
	}
};

ItemImagesModel.prototype.deleteAllItemImages = function (itemId) {
	try {
		var result = sqlitedbhelper.deleteAllItemImagesByItemId(itemId);
		return result === ErrorCode.ERROR_ALL_ITEM_IMAGE_DELETE_SUCCESS;
	} catch (ex) {
		if (logger) {
			logger.error('Error occurred in deleting item image', ex);
		}
		return false;
	}
};

ItemImagesModel.prototype.updateCatalogueItemIdOfItemImages = function () {
	try {
		return sqlitedbhelper.updateCatalogueItemIdOfItemImages(this.getCatalogueItemId(), this.getItemId());
	} catch (ex) {
		if (logger) {
			logger.error('Error occurred in updating item image', ex);
		}
		return false;
	}
};

ItemImagesModel.prototype.deleteAllCatalogueItemImagesByCatalogueId = function () {
	try {
		return sqlitedbhelper.deleteAllCatalogueItemImagesByCatalogueId(this.getItemId());
	} catch (ex) {
		if (logger) {
			logger.error('Error occurred in updating item image', ex);
		}
		return false;
	}
};

module.exports = ItemImagesModel;