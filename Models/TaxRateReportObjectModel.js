var TaxRateReportObjectModel = function TaxRateReportObjectModel() {
	this.taxid = 0;
	this.taxIn = 0;
	this.taxOut = 0;
	this.taxableSaleValue = 0;
	this.taxablePurchaseValue = 0;

	this.getTaxid = function () {
		return this.taxid;
	};

	this.setTaxid = function (taxid) {
		this.taxid = taxid;
	};

	this.getTaxIn = function () {
		return this.taxIn;
	};

	this.setTaxIn = function (taxIn) {
		this.taxIn = taxIn;
	};

	this.getTaxOut = function () {
		return this.taxOut;
	};

	this.setTaxOut = function (taxOut) {
		this.taxOut = taxOut;
	};

	this.getSaleTaxableValue = function () {
		return this.taxableSaleValue;
	};

	this.setSaleTaxableValue = function (taxableSaleValue) {
		this.taxableSaleValue = taxableSaleValue;
	};
	this.getPurchaseTaxableValue = function () {
		return this.taxablePurchaseValue;
	};

	this.setPurchaseTaxableValue = function (taxablePurchaseValue) {
		this.taxablePurchaseValue = taxablePurchaseValue;
	};
};

module.exports = TaxRateReportObjectModel;