var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
	var SqliteDBHelper = require('./../DBManager/SqliteDBHelper');
	var sqlitedbhelper = new SqliteDBHelper();

	var RefreshCacheTracker = require('./../BizLogic/RefreshCacheTracker.js');
	var ErrorCode = require('./../Constants/ErrorCode');

	var refreshCacheTracker = new RefreshCacheTracker();

	var TransactionMessageConfigModel = function () {
		function TransactionMessageConfigModel(txnType, txnFieldId, txnFieldName, txnFieldValue) {
			(0, _classCallCheck3.default)(this, TransactionMessageConfigModel);

			this.txnType = txnType;
			this.txnFieldId = txnFieldId;
			this.txnFieldName = txnFieldName;
			this.txnFieldValue = txnFieldValue;
		}

		(0, _createClass3.default)(TransactionMessageConfigModel, [{
			key: 'save',
			value: function save() {
				var _this = this;

				var refreshOnSuccess = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

				var MessageCache = require('./../Cache/MessageCache.js');
				var messageCache = new MessageCache();
				return new _promise2.default(function (resolve, reject) {
					var status = sqlitedbhelper.saveMessageField(_this);
					if (status == ErrorCode.ERROR_MESSAGE_CONFIG_SAVE_SUCCESS) {
						if (refreshOnSuccess) {
							refreshCacheTracker.messageCacheRefreshNeeded = true;
							messageCache.reload();
						}
						resolve(status);
					} else {
						reject(status);
					}
				});
				// ERROR_MESSAGE_CONFIG_SAVE_SUCCESS
				// ERROR_MESSAGE_CONFIG_SAVE_FAILED
			}
		}, {
			key: 'txnType',
			get: function get() {
				return this._txnType;
			},
			set: function set(txnType) {
				this._txnType = txnType;
			}
		}, {
			key: 'txnFieldId',
			get: function get() {
				return this._txnFieldId;
			},
			set: function set(txnFieldId) {
				this._txnFieldId = txnFieldId;
			}
		}, {
			key: 'txnFieldName',
			get: function get() {
				return this._txnFieldName;
			},
			set: function set(txnFieldName) {
				this._txnFieldName = txnFieldName;
			}
		}, {
			key: 'txnFieldValue',
			get: function get() {
				return this._txnFieldValue;
			},
			set: function set(txnFieldValue) {
				this._txnFieldValue = txnFieldValue;
			}
		}], [{
			key: 'defaultConfigFromDB',
			value: function defaultConfigFromDB() {
				return sqlitedbhelper.fetchMessageConfig();
			}
		}]);
		return TransactionMessageConfigModel;
	}();

	module.exports = TransactionMessageConfigModel;
})();