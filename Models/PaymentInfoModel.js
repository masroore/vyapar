var PaymentInfoModel = function PaymentInfoModel() {
	var ErrorCode = require('./../Constants/ErrorCode.js');
	var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
	var sqlitedbhelper = new SqliteDBHelper();
	this.accountName;
	this.accountNumber;
	this.bankName;
	this.id;
	this.type;
	this.name;
	this.currentBalance;
	this.openingBalance = 0.0;
	this.openingDate;

	this.setId = function (id) {
		this.id = id;
	};
	this.getId = function () {
		return this.id;
	};

	this.setName = function (name) {
		this.name = name;
	};
	this.getName = function () {
		return this.name;
	};

	this.setAccountName = function (accountName) {
		this.accountName = accountName;
	};
	this.getAccountName = function () {
		return this.accountName;
	};

	this.setAccountNumber = function (accountNumber) {
		this.accountNumber = accountNumber;
	};
	this.getAccountNumber = function () {
		return this.accountNumber;
	};

	this.setBankName = function (bankName) {
		this.bankName = bankName;
	};
	this.getBankName = function () {
		return this.bankName;
	};

	this.setType = function (type) {
		this.type = type;
	};
	this.getType = function () {
		return this.type;
	};

	this.setCurrentBalance = function (currentBalance) {
		this.currentBalance = currentBalance;
	};
	this.getCurrentBalance = function () {
		return this.currentBalance;
	};

	this.setOpeningBalance = function (openingBalance) {
		this.openingBalance = openingBalance;
	};
	this.getOpeningBalance = function () {
		return this.openingBalance;
	};

	this.setOpeningDate = function (openingDate) {
		this.openingDate = openingDate;
	};
	this.getOpeningDate = function () {
		return this.openingDate;
	};

	this.saveNewInfo = function () {
		var statusCode = ErrorCode.ERROR_NEW_BANK_INFO_FAILED;
		var retVal = sqlitedbhelper.createPaymentInfo(this);
		console.log(retVal);
		if (retVal > 0) {
			this.setId(retVal);
			statusCode = ErrorCode.ERROR_NEW_BANK_INFO_SUCCESS;
		}
		return statusCode;
	};

	this.updateInfo = function () {
		var sqlitedbhelper = new SqliteDBHelper();
		return sqlitedbhelper.updatePaymentInfo(this);
	};
	this.canDeletePaymentInfo = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.canDeletePaymentInfo(this.getId());
	};

	this.deletePaymentInfo = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.deletePaymentInfo(this.getId());
	};
};

module.exports = PaymentInfoModel;