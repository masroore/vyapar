var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
var sqlitedbhelper = new SqliteDBHelper();

var PartyGroupModel = function PartyGroupModel() {
	this.groupId = 0;
	this.groupName = '';
	this.memberCount = 0;

	var ErrorCode = require('./../Constants/ErrorCode.js');
	var NameType = require('./../Constants/NameType.js');

	this.getGroupId = function () {
		return this.groupId;
	};
	this.setGroupId = function (groupId) {
		this.groupId = groupId;
	};

	this.getGroupName = function () {
		return this.groupName;
	};
	this.setGroupName = function (groupName) {
		this.groupName = groupName;
	};

	this.getMemberCount = function () {
		return this.memberCount;
	};
	this.setMemberCount = function (memberCount) {
		this.memberCount = memberCount;
	};

	this.addNewGroup = function () {
		var retVal = sqlitedbhelper.createPartyGroupRecord(this, null);
		console.log(retVal);
		var statusCode = ErrorCode.ERROR_PARTYGROUP_SAVE_FAILED;
		if (retVal > 0) {
			this.setGroupId(retVal);
			statusCode = ErrorCode.ERROR_PARTYGROUP_SAVE_SUCCESS;
		} else {
			statusCode = ErrorCode.ERROR_PARTYGROUP_SAVE_FAILED;
		}
		// console.log(statusCode);
		return statusCode;
	};

	this.editPartyGroup = function (oldGroupId) {
		var retVal = sqlitedbhelper.editPartyGroupRecord(oldGroupId, this);
		if (retVal) {
			statusCode = ErrorCode.ERROR_PARTYGROUP_UPDATE_SUCCESS;
		} else {
			statusCode = ErrorCode.ERROR_PARTYGROUP_UPDATE_FAILED;
		}
		return statusCode;
	};

	this.canDeletePartyGroup = function (partyGroupId) {
		return sqlitedbhelper.canDeletePartyGroup(partyGroupId);
	};

	this.deletePartyGroup = function (partyGroupId) {
		return sqlitedbhelper.deletePartyGroup(partyGroupId);
	};
};

module.exports = PartyGroupModel;