var _assign = require('babel-runtime/core-js/object/assign');

var _assign2 = _interopRequireDefault(_assign);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
	var LoanAccount = function LoanAccount(obj) {
		this.accountId = '';
		this.firmId = '';
		this.accountName = '';
		this.lenderBank = '';
		this.accountNumber = '';
		this.description = '';
		this.currentBalance = '';
		this.openingBalance = '';
		this.openingDate = '';
		this.createdDate = '';
		this.modifiedDate = '';
		this.loanReceivedIn = '';
		this.interestRate = '';
		this.termDuration = '';
		this.processingFee = '';
		this.processingFeePaidFrom = '';
		this.isClosebookLoanAccount = false;
		(0, _assign2.default)(this, obj);
	};

	LoanAccount.prototype.getLoanAccountId = function getLoanAccountId() {
		return this.accountId;
	};
	LoanAccount.prototype.setLoanAccountId = function setLoanAccountId(accountId) {
		this.accountId = accountId;
	};

	LoanAccount.prototype.getFirmId = function getFirmId() {
		return this.firmId;
	};
	LoanAccount.prototype.setFirmId = function setFirmId(firmId) {
		this.firmId = firmId;
	};

	LoanAccount.prototype.getLoanAccName = function getLoanAccName() {
		return this.accountName;
	};
	LoanAccount.prototype.setLoanAccName = function setLoanAccName(accountName) {
		this.accountName = accountName;
	};

	LoanAccount.prototype.getLenderBank = function getLenderBank() {
		return this.lenderBank;
	};
	LoanAccount.prototype.setLenderBank = function setLenderBank(lenderBank) {
		this.lenderBank = lenderBank;
	};

	LoanAccount.prototype.getLoanAccountNumber = function getLoanAccountNumber() {
		return this.accountNumber;
	};
	LoanAccount.prototype.setLoanAccountNumber = function setLoanAccountNumber(accountNumber) {
		this.accountNumber = accountNumber;
	};

	LoanAccount.prototype.getDescription = function getDescription() {
		return this.description;
	};
	LoanAccount.prototype.setDescription = function setDescription(description) {
		this.description = description;
	};

	LoanAccount.prototype.getCurrentBalance = function getCurrentBalance() {
		return this.currentBalance;
	};
	LoanAccount.prototype.setCurrentBalance = function setCurrentBalance(currentBalance) {
		this.currentBalance = currentBalance;
	};

	LoanAccount.prototype.getIsClosebookLoanAccount = function getIsClosebookLoanAccount() {
		return this.isClosebookLoanAccount;
	};
	LoanAccount.prototype.setIsClosebookLoanAccount = function setIsClosebookLoanAccount(isClosebookLoanAccount) {
		this.isClosebookLoanAccount = isClosebookLoanAccount;
	};

	// This will be the latest closebook balance or loan opening balance (if closebook never happened on this account)
	LoanAccount.prototype.getOpeningBalance = function getOpeningBalance() {
		return this.openingBalance;
	};
	LoanAccount.prototype.setOpeningBalance = function setOpeningBalance(openingBalance) {
		this.openingBalance = openingBalance;
	};

	LoanAccount.prototype.getInterestRate = function getInterestRate() {
		return this.interestRate;
	};
	LoanAccount.prototype.setInterestRate = function setInterestRate(interestRate) {
		this.interestRate = interestRate;
	};

	LoanAccount.prototype.getTermDuration = function getTermDuration() {
		return this.termDuration;
	};
	LoanAccount.prototype.setTermDuration = function setTermDuration(termDuration) {
		this.termDuration = termDuration;
	};

	LoanAccount.prototype.getProcessingFee = function getProcessingFee() {
		return this.processingFee;
	};
	LoanAccount.prototype.setProcessingFee = function setProcessingFee(processingFee) {
		this.processingFee = processingFee;
	};

	LoanAccount.prototype.getProcessingFeePaidFrom = function getProcessingFeePaidFrom() {
		return this.processingFeePaidFrom;
	};
	LoanAccount.prototype.setProcessingFeePaidFrom = function setProcessingFeePaidFrom(processingFeePaidFrom) {
		this.processingFeePaidFrom = processingFeePaidFrom;
	};

	LoanAccount.prototype.getLoanReceivedIn = function getLoanReceivedIn() {
		return this.loanReceivedIn;
	};
	LoanAccount.prototype.setLoanReceivedIn = function setLoanReceivedIn(loanReceivedIn) {
		this.loanReceivedIn = loanReceivedIn;
	};

	// This will be the latest closebook date or loan opening date (if closebook never happened on this account)
	LoanAccount.prototype.getOpeningDate = function getOpeningDate() {
		return this.openingDate;
	};
	LoanAccount.prototype.setOpeningDate = function setOpeningDate(openingDate) {
		this.openingDate = openingDate;
	};

	LoanAccount.prototype.getCreatedDate = function getCreatedDate() {
		return this.createdDate;
	};
	LoanAccount.prototype.setCreatedDate = function setCreatedDate(createdDate) {
		this.createdDate = createdDate;
	};

	LoanAccount.prototype.getModifiedDate = function getModifiedDate() {
		return this.modifiedDate;
	};
	LoanAccount.prototype.setModifiedDate = function setModifiedDate(modifiedDate) {
		this.modifiedDate = modifiedDate;
	};
	LoanAccount.prototype.saveLoanAccount = function saveLoanAccount() {
		var showToast = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
		var logResult = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

		if (!(this instanceof LoanAccount)) {
			return;
		}
		var statusCode = validateLoanAccount(this);
		if (statusCode == ErrorCode.ERROR_LOAN_ACCOUNT_UPDATE_SUCCESS || statusCode == ErrorCode.ERROR_LOAN_ACCOUNT_SAVE_SUCCESS) {
			var MyAnalytics = require('../Utilities/analyticsHelper');
			var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
			var sqliteDBHelper = new SqliteDBHelper();
			if (!this.accountId) {
				var accountId = sqliteDBHelper.createLoanAccount(this);
				if (accountId) {
					this.accountId = accountId;
					MyAnalytics.pushEvent('Add Loan Account');
				} else {
					statusCode = ErrorCode.ERROR_LOAN_ACCOUNT_SAVE_FAILED;
				}
			} else {
				var _accountId = sqliteDBHelper.updateLoanAccount(this);
				if (_accountId) {
					MyAnalytics.pushEvent('Update Loan Account');
				} else {
					statusCode = ErrorCode.ERROR_LOAN_ACCOUNT_UPDATE_FAILED;
				}
			}
		}
		return statusCode;
	};
	LoanAccount.prototype.deleteLoanAccount = function deleteLoanAccount() {
		if (!(this instanceof LoanAccount)) {
			return;
		}
		var retVal = 0;
		var MyAnalytics = require('../Utilities/analyticsHelper');
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var sqliteDBHelper = new SqliteDBHelper();
		if (this.accountId) {
			MyAnalytics.pushEvent('Delete Loan Account');
			retVal = sqliteDBHelper.deleteLoanAccount(this);
		}
		return retVal;
	};

	function validateLoanAccount(loanAccount) {
		var TxnTypeConstant = require('../Constants/TxnTypeConstant');
		var LoanAccountCache = require('../Cache/LoanAccountCache');
		var loanAccountCache = new LoanAccountCache();
		var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
		var statusCode = loanAccount.accountId ? ErrorCode.ERROR_LOAN_ACCOUNT_UPDATE_SUCCESS : ErrorCode.ERROR_LOAN_ACCOUNT_SAVE_SUCCESS;
		if (loanAccount.accountId) {
			var existingAcc = loanAccountCache.getLoanAccountById(loanAccount.accountId);
			// Validate openingBalance
			if (existingAcc.getOpeningBalance() != loanAccount.getOpeningBalance()) {
				var currentExistingBal = existingAcc.getCurrentBalance();
				var currentOpeningBal = existingAcc.getOpeningBalance();
				var minimumValidOpeningBal = currentOpeningBal - currentExistingBal; // Opening bal cannot go -ve, so min opening bal. should equal the principal paid till date.
				if (loanAccount.getOpeningBalance() < minimumValidOpeningBal) {
					statusCode = 'Opening Balance cannot be less than the principal amount paid till date';
					return statusCode;
				}
			}
			// Validate opening date
			if (existingAcc.getOpeningDate() < loanAccount.getOpeningDate()) {
				var sqliteDBHelper = new SqliteDBHelper();
				var txns = sqliteDBHelper.getLoanTransactions({
					loanAccountId: loanAccount.getLoanAccountId(),
					toDate: loanAccount.getOpeningDate(),
					loanTxnTypes: [TxnTypeConstant.TXN_TYPE_LOAN_ADJUSTMENT, TxnTypeConstant.TXN_TYPE_LOAN_EMI_PAYMENT]
				});
				if (txns && txns.length) {
					statusCode = 'Opening Date cannot come after existing transactions';
					return statusCode;
				}
			}
		}
		return statusCode;
	}
	module.exports = LoanAccount;
})();