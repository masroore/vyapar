var PaymentTermModel = function PaymentTermModel() {
	var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
	var sqlitedbhelper = new SqliteDBHelper();
	var paymentTermId = void 0;
	var paymentTermName = void 0;
	var paymentTermDays = void 0;
	var paymentTermIsDefault = void 0;
	var paymentTermIsEditable = void 0;

	this.getPaymentTermId = function () {
		return paymentTermId;
	};
	this.setPaymentTermId = function (id) {
		paymentTermId = id;
	};
	this.getPaymentTermName = function () {
		return paymentTermName;
	};
	this.setPaymentTermName = function (payment_term_name) {
		paymentTermName = payment_term_name;
	};
	this.getPaymentTermDays = function () {
		return paymentTermDays;
	};
	this.setPaymentTermDays = function (payment_term_days) {
		paymentTermDays = payment_term_days;
	};
	this.getPaymentTermIsDefault = function () {
		return paymentTermIsDefault;
	};
	this.setPaymentTermIsDefault = function (payment_term_is_default) {
		paymentTermIsDefault = payment_term_is_default;
	};
	this.getPaymentTermIsEditable = function () {
		return paymentTermIsEditable;
	};
	this.setPaymentTermIsEditable = function (payment_term_is_editable) {
		paymentTermIsEditable = payment_term_is_editable;
	};
	this.updatePaymentTerm = function () {
		var status = sqlitedbhelper.updatePaymentTermRecord(this);
		return status;
	};
	this.changeDefaultPaymentTerm = function () {
		var status = sqlitedbhelper.changeDefaultPaymentTerm(this);
		return status;
	};
	this.createPaymentTerm = function () {
		var termId = sqlitedbhelper.createPaymentTermRecord(this);
		return termId;
	};
	this.deletePaymentTerm = function () {
		var status = sqlitedbhelper.deletePaymentTermRecord(this);
		return status;
	};
};

module.exports = PaymentTermModel;