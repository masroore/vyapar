var ItemUnitModel = function ItemUnitModel() {
	var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
	var Defaults = require('./../Constants/Defaults.js');
	this.unitId;
	this.unitName;
	this.unitShortName;
	this.fullNameEditable = Defaults.DEFAULT_FULL_NAME_EDITABLE;
	this.unitDeletable = Defaults.DEFAULT_UNIT_DELETABLE;

	this.getUnitId = function () {
		return this.unitId;
	};

	this.setUnitId = function (unitId) {
		this.unitId = unitId;
	};

	this.getUnitName = function () {
		return this.unitName;
	};

	this.setUnitName = function (unitName) {
		this.unitName = unitName;
	};

	this.getUnitShortName = function () {
		return this.unitShortName;
	};

	this.setUnitShortName = function (unitShortName) {
		this.unitShortName = unitShortName;
	};

	this.isFullNameEditable = function () {
		return this.fullNameEditable;
	};

	this.setFullNameEditable = function (fullNameEditable) {
		this.fullNameEditable = fullNameEditable;
	};

	this.isUnitDeletable = function () {
		return this.unitDeletable;
	};

	this.setUnitDeletable = function (unitDeletable) {
		this.unitDeletable = unitDeletable;
	};

	this.addItemUnit = function () {
		var errorCode = ErrorCode.SUCCESS;
		var sqliteDBHelper = new SqliteDBHelper();
		var retVal = sqliteDBHelper.createItemUnit(this);
		if (retVal > 0) {
			this.setUnitId(retVal);
			errorCode = ErrorCode.ERROR_UNIT_SAVE_SUCCESS;
		} else {
			errorCode = ErrorCode.ERROR_UNIT_SAVE_FAILED;
		}
		return errorCode;
	};

	this.updateUnit = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		return sqliteDBHelper.updateItemUnit(this);
	};

	this.deleteUnit = function () {
		var sqliteDBHelper = new SqliteDBHelper();
		var unitId = this.getUnitId();
		var isUsed = sqliteDBHelper.isItemUnitUsed(unitId);
		var statusCode = ErrorCode.ERROR_UNIT_IS_USED;
		if (!isUsed) {
			var conf = confirm('Do you want to delete this unit?');
			if (conf) {
				statusCode = sqliteDBHelper.deleteItemUnit(unitId);
			} else {
				return false;
			}
		}
		return statusCode;
	};
};
module.exports = ItemUnitModel;