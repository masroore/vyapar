var ErrorCode = require('../Constants/ErrorCode');
var SqliteDBHelper = require('../DBManager/SqliteDBHelper.js');
var sqlitedbhelper = new SqliteDBHelper();
var CatalogueModel = function CatalogueModel() {
	this._catalogueItemId;
	this._itemId;
	this._catalogueItemName;
	this._catalogueItemCode;
	this._catalogueItemDescription;
	this._catalogueItemSaleUnitPrice;
	this._catalogueItemCategoryName;
};

CatalogueModel.prototype.getCatalogueItemId = function () {
	return this._catalogueItemId;
};
CatalogueModel.prototype.setCatalogueItemId = function (catalogueItemId) {
	this._catalogueItemId = catalogueItemId;
};

CatalogueModel.prototype.getItemId = function () {
	return this._itemId;
};
CatalogueModel.prototype.setItemId = function (itemId) {
	this._itemId = itemId;
};

CatalogueModel.prototype.getCatalogueItemName = function () {
	return this._catalogueItemName;
};
CatalogueModel.prototype.setCatalogueItemName = function (catalogueItemName) {
	this._catalogueItemName = catalogueItemName;
};

CatalogueModel.prototype.getCatalogueItemDescription = function () {
	return this._catalogueItemDescription;
};

CatalogueModel.prototype.setCatalogueItemDescription = function (catalogueItemDescription) {
	this._catalogueItemDescription = catalogueItemDescription;
};

CatalogueModel.prototype.getCatalogueItemCode = function () {
	return this._catalogueItemCode;
};

CatalogueModel.prototype.setCatalogueItemCode = function (catalogueItemCode) {
	this._catalogueItemCode = catalogueItemCode;
};

CatalogueModel.prototype.getCatalogueItemSaleUnitPrice = function () {
	return this._catalogueItemSaleUnitPrice;
};

CatalogueModel.prototype.setCatalogueItemSaleUnitPrice = function (catalogueItemSaleUnitPrice) {
	this._catalogueItemSaleUnitPrice = catalogueItemSaleUnitPrice;
};

CatalogueModel.prototype.getCatalogueItemCategoryName = function () {
	return this._catalogueItemCategoryName;
};

CatalogueModel.prototype.setCatalogueItemCategoryName = function (catalogueItemCategoryName) {
	this._catalogueItemCategoryName = catalogueItemCategoryName;
};

CatalogueModel.prototype.addCatalogueItem = function () {
	try {
		var catalogueItemId = sqlitedbhelper.addCatalogueItemRecord(this);
		if (catalogueItemId > 0) {
			this.setCatalogueItemId(catalogueItemId);
			return catalogueItemId;
		}
		return 0;
	} catch (ex) {
		if (logger) {
			logger.error('Error adding catalogue item.', ex);
		}
		return 0;
	}
};

CatalogueModel.prototype.deleteCatalogueItem = function () {
	try {
		return sqlitedbhelper.deleteCatalogueItem(this.getItemId());
	} catch (ex) {
		if (logger) {
			logger.error('Error adding catalogue item.', ex);
		}
		return ErrorCode.FAILED;
	}
};

CatalogueModel.prototype.updateCatalogueItem = function () {
	try {
		return sqlitedbhelper.updateCatalogueItemRecord(this);
	} catch (ex) {
		if (logger) {
			logger.error('Error adding catalogue item.', ex);
		}
		return ErrorCode.FAILED;
	}
};

module.exports = CatalogueModel;