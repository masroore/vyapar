var CustomFieldsModel = function CustomFieldsModel() {
	var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
	var sqliteDBHelper = new SqliteDBHelper();
	var ErrorCode = require('./../Constants/ErrorCode.js');
	this.customFieldId;
	this.customFieldName;
	this.customFieldType;
	this.customFieldVisibility;

	this.getCustomFieldId = function () {
		return this.customFieldId;
	};
	this.setCustomFieldId = function (customFieldId) {
		this.customFieldId = customFieldId;
	};

	this.getCustomFieldName = function () {
		return this.customFieldName;
	};
	this.setCustomFieldName = function (customFieldName) {
		this.customFieldName = customFieldName;
	};

	this.getCustomFieldType = function () {
		return this.customFieldType;
	};
	this.setCustomFieldType = function (customFieldType) {
		this.customFieldType = customFieldType;
	};

	this.getCustomFieldVisibility = function () {
		this.customFieldVisibility = this.customFieldVisibility == '1';
		return this.customFieldVisibility;
	};
	this.setCustomFieldVisibility = function (customFieldVisibility) {
		this.customFieldVisibility = customFieldVisibility;
	};

	this.updateCustomField = function () {
		var refreshCache = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

		var statusCode = sqliteDBHelper.updateCustomField(this);
		if (statusCode == ErrorCode.ERROR_SETTING_SAVE_SUCCESS && refreshCache) {
			var CustomFieldsCache = require('./../Cache/CustomFieldsCache.js');
			var customFieldsCache = new CustomFieldsCache();
			customFieldsCache.refreshCustomFields();
		}
		return statusCode;
	};
};
module.exports = CustomFieldsModel;