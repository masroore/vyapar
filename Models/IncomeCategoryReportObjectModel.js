var IncomeCategoryReportObjectModel = function IncomeCategoryReportObjectModel() {
	var categoryId;
	var amount = 0;

	this.getCategoryId = function () {
		return this.categoryId;
	};

	this.setCategoryId = function (categoryId) {
		this.categoryId = categoryId;
	};

	this.getAmount = function () {
		return this.amount;
	};

	this.setAmount = function (amount) {
		this.amount = amount;
	};
};

module.exports = IncomeCategoryReportObjectModel;