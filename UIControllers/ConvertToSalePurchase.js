var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var convertToSalePurchase = function convertToSalePurchase(that) {
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant');
	var SettingCache = require('./../Cache/SettingCache');

	var _that$split = that.split(':'),
	    _that$split2 = (0, _slicedToArray3.default)(_that$split, 2),
	    txnIdReq = _that$split2[0],
	    txnType = _that$split2[1];

	if (txnType == 'TXN_TYPE_SALE_ORDER' || txnType == 'TXN_TYPE_ESTIMATE' || txnType == 'TXN_TYPE_DELIVERY_CHALLAN') {
		var value = 'TXN_TYPE_SALE';
	} else if (txnType == 'TXN_TYPE_PURCHASE_ORDER') {
		var value = 'TXN_TYPE_PURCHASE';
	}

	var getReturnGoodsHTML = function getReturnGoodsHTML(id, txnType) {
		var BaseTransaction = require('./../BizLogic/BaseTransaction.js');
		var baseTransaction = new BaseTransaction();
		var ItemUnitCache = require('../Cache/ItemUnitCache');
		var itemUnitCache = new ItemUnitCache();
		var oldTxnObj = baseTransaction.getTransactionFromId(id);
		var settingCache = new SettingCache();

		var lineItems = oldTxnObj.getLineItems();
		var html = '';
		var i = 0;
		var ItemUnitMappingCache = require('./../Cache/ItemUnitMappingCache.js');
		var itemUnitMappingCache = new ItemUnitMappingCache();
		var anyFreeQuantityPresent = false;
		var _iteratorNormalCompletion = true;
		var _didIteratorError = false;
		var _iteratorError = undefined;

		try {
			for (var _iterator = (0, _getIterator3.default)(lineItems), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
				var lineItem = _step.value;

				var freeQuantityToBeShown = Number(lineItem.getItemFreeQuantity());
				if (freeQuantityToBeShown > 0) {
					anyFreeQuantityPresent = true;
				}
			}
		} catch (err) {
			_didIteratorError = true;
			_iteratorError = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion && _iterator.return) {
					_iterator.return();
				}
			} finally {
				if (_didIteratorError) {
					throw _iteratorError;
				}
			}
		}

		var _iteratorNormalCompletion2 = true;
		var _didIteratorError2 = false;
		var _iteratorError2 = undefined;

		try {
			for (var _iterator2 = (0, _getIterator3.default)(lineItems), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
				var _lineItem = _step2.value;

				var unitToBeShown = '';
				if (_lineItem.getLineItemUnitId()) {
					unitToBeShown = itemUnitCache.getItemUnitShortNameById(_lineItem.getLineItemUnitId());
				}
				var quantityToBeShown = Number(_lineItem.getItemQuantity());
				var freeQuantityToBeShown = Number(_lineItem.getItemFreeQuantity());
				if (_lineItem.getLineItemUnitMappingId() > 0) {
					var mappingObj = itemUnitMappingCache.getItemUnitMapping(_lineItem.getLineItemUnitMappingId());
					quantityToBeShown = Number(_lineItem.getItemQuantity()) * mappingObj.getConversionRate();
					freeQuantityToBeShown = Number(_lineItem.getItemFreeQuantity()) * mappingObj.getConversionRate();
				}

				html += '<div class="row" data-item-id="' + _lineItem.getLineItemId() + '">\n                  <div class="col">' + ++i + '. ' + _lineItem.getItemName() + '</div>\n                  <div class="col">' + (MyDouble.getQuantityWithDecimalWithoutColor(quantityToBeShown) + MyDouble.quantityDoubleToStringWithSignExplicitly(freeQuantityToBeShown, true)) + ' ' + unitToBeShown + '</div>\n                  <div class="col"><input type="text" class="returnedQty" placeholder="0"  oninput="DecimalInputFilter.inputTextFilterForQuantity(this)">  ' + unitToBeShown + '</div>\n                  ' + (freeQuantityToBeShown > 0 ? '<div class="col"><input type="text" class="freeQty"  placeholder="0"  oninput="DecimalInputFilter.inputTextFilterForQuantity(this)">  ' + unitToBeShown + '</div>' : anyFreeQuantityPresent ? '<div class="col"></div>' : '') + '\n                 </div>';

				var additionalDetails = [];

				if (_lineItem.getLineItemSerialNumber()) {
					additionalDetails.push(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_SERIAL_NUMBER_VALUE) + ': ' + _lineItem.getLineItemSerialNumber());
				}

				if (_lineItem.getLineItemCount() && Number(_lineItem.getLineItemCount()) > 0) {
					additionalDetails.push(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_COUNT_VALUE) + ': ' + MyDouble.trimQuantityToString(Number(_lineItem.getLineItemCount())));
				}

				if (_lineItem.getLineItemBatchNumber()) {
					additionalDetails.push(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_BATCH_NUMBER_VALUE) + ': ' + _lineItem.getLineItemBatchNumber());
				}

				if (_lineItem.getLineItemExpiryDate()) {
					var format = settingCache.getExpiryDateFormat();
					additionalDetails.push(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_EXPIRY_DATE_VALUE) + ': ' + MyDate.getDate(format == 1 ? 'd/m/y' : 'm/y', _lineItem.getLineItemExpiryDate()));
				}

				if (_lineItem.getLineItemManufacturingDate()) {
					var format = settingCache.getManufacturingDateFormat();
					additionalDetails.push(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_MANUFACTURING_DATE_VALUE) + ': ' + MyDate.getDate(format == 1 ? 'd/m/y' : 'm/y', _lineItem.getLineItemManufacturingDate()));
				}

				if (_lineItem.getLineItemMRP() && Number(_lineItem.getLineItemMRP()) > 0) {
					additionalDetails.push(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_MRP_VALUE) + ': ' + MyDouble.getAmountWithDecimal(_lineItem.getLineItemMRP()));
				}

				if (_lineItem.getLineItemSize()) {
					additionalDetails.push(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_SIZE_VALUE) + ': ' + _lineItem.getLineItemSize());
				}

				if (additionalDetails.length > 0) {
					var detailsString = additionalDetails.join(', ');
					html += '<div class="row additionalDetails">' + detailsString + '</div>';
				}
			}
		} catch (err) {
			_didIteratorError2 = true;
			_iteratorError2 = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion2 && _iterator2.return) {
					_iterator2.return();
				}
			} finally {
				if (_didIteratorError2) {
					throw _iteratorError2;
				}
			}
		}

		html += '<div class="row actionsContainer">\n                  <div class="col">\n                  <div class="md-checkbox">\n                    <input\n                        id="chkBoxReturnGoods"\n                        ' + (settingCache.isDeliveryChallanReturnEnabled() ? '' : 'checked=checked') + '\n                        type="checkbox">\n                        <label for="chkBoxReturnGoods">Do not show this dialog in future.</label>\n                  </div>\n                  </div>\n                  <div class="col">\n                    <button class="terminalButton" id="continueReturnGoods">Done</button>\n                  </div>\n                  </div>';
		html += '</div>';
		html = '\n        <div class="container-fluid">\n          <h2>List of Items on Challan</h2>\n          <div class="row headerRow">\n            <div class="col">Items</div>\n            <div class="col">Shipped</div>\n            <div class="col">Returned Quantity</div>\n            ' + (anyFreeQuantityPresent ? '<div class="col">Returned Free Quantity</div>' : '') + '\n          </div>' + html;
		return html;
	};

	var continueReturnGoodsToSale = function continueReturnGoodsToSale() {
		var BaseTransaction = require('./../BizLogic/BaseTransaction.js');
		var baseTransaction = new BaseTransaction();
		var oldTxnObj = baseTransaction.getTransactionFromId(txnIdReq);
		var $el = $('#returnGoodsDialog');
		var settingCache = new SettingCache();
		var ItemUnitMappingCache = require('./../Cache/ItemUnitMappingCache.js');
		var itemUnitMappingCache = new ItemUnitMappingCache();
		var isQtyReturned = false;
		var newSubtotal = 0;
		var oldTxnDiscountPercentage = MyDouble.convertStringToDouble(oldTxnObj.getDiscountPercent());
		var oldTxnTaxPercentage = MyDouble.convertStringToDouble(oldTxnObj.getTaxPercent());
		var _iteratorNormalCompletion3 = true;
		var _didIteratorError3 = false;
		var _iteratorError3 = undefined;

		try {
			for (var _iterator3 = (0, _getIterator3.default)(oldTxnObj.getLineItems()), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
				var lineItem = _step3.value;

				var id = lineItem.getLineItemId();
				var $row = $('[data-item-id=' + id + ']', $el);
				var $returnedQty = $('.returnedQty', $row);
				var $freeQty = $('.freeQty', $row);
				var returnedQty = Number($returnedQty.val());
				var returnedFreeQty = 0;
				var oldQty = Number(lineItem.getItemQuantity());
				var oldFreeQuantity = Number(lineItem.getItemFreeQuantity());

				if (lineItem.getLineItemUnitMappingId() > 0) {
					var mappingObj = itemUnitMappingCache.getItemUnitMapping(lineItem.getLineItemUnitMappingId());
					oldQty = Number(lineItem.getItemQuantity()) * mappingObj.getConversionRate();
					oldFreeQuantity = Number(lineItem.getItemFreeQuantity()) * mappingObj.getConversionRate();
				}

				if (oldFreeQuantity > 0) {
					returnedFreeQty = Number($freeQty.val());
				}

				if (isNaN(returnedQty)) {
					ToastHelper.error('Please enter valid returned quantity. return quantity should be 0 or more than 0');
					$returnedQty.focus();
					return false;
				}
				if (returnedQty > oldQty) {
					ToastHelper.error('Please enter valid returned quantity. return quantity should be less than sold quantity');
					$returnedQty.focus();
					return false;
				}

				if (isNaN(returnedFreeQty)) {
					ToastHelper.error('Please enter valid free quantity. free quantity should be 0 or more than 0');
					$freeQty.focus();
					return false;
				}
				if (returnedFreeQty > oldFreeQuantity) {
					ToastHelper.error('Please enter valid free quantity. free quantity should be less than sold quantity');
					$freeQty.focus();
					return false;
				}
				var newQty = oldQty - returnedQty;
				var newFreeQty = oldFreeQuantity - returnedFreeQty;
				if (lineItem.getLineItemUnitMappingId() > 0) {
					var mappingObj = itemUnitMappingCache.getItemUnitMapping(lineItem.getLineItemUnitMappingId());
					newQty = newQty / mappingObj.getConversionRate();
					newFreeQty = newFreeQty / mappingObj.getConversionRate();
				}
				if (returnedQty > 0) {
					var oldDiscountPercent = lineItem.getLineItemDiscountPercent();
					var oldTaxPercent = lineItem.getLineItemTaxPercent();
					var totalPrice = newQty * Number(lineItem.getItemUnitPrice());
					var oldAdditionalCESSAmount = MyDouble.convertStringToDouble(lineItem.getLineItemAdditionalCESS());
					if (oldDiscountPercent > 0) {
						var _newDiscountAmount = totalPrice * oldDiscountPercent / 100;
						lineItem.setLineItemDiscountAmount(MyDouble.getAmountWithDecimal(_newDiscountAmount));
						totalPrice -= _newDiscountAmount;
					}
					if (oldTaxPercent > 0) {
						var _newTaxAmount = totalPrice * oldTaxPercent / 100;
						lineItem.setLineItemTaxAmount(MyDouble.getAmountWithDecimal(_newTaxAmount));
						totalPrice += _newTaxAmount;
					}
					if (oldAdditionalCESSAmount > 0) {
						var oldAdditionalCESSPrice = oldAdditionalCESSAmount / oldQty;
						var newAdditionalCESSAmount = oldAdditionalCESSPrice * newQty;
						lineItem.setLineItemAdditionalCESS(MyDouble.getAmountWithDecimal(newAdditionalCESSAmount));
						totalPrice += newAdditionalCESSAmount;
					}
					lineItem.setLineItemTotal(MyDouble.getAmountWithDecimal(totalPrice));
					lineItem.setItemQuantity(newQty);

					isQtyReturned = true;
				}
				if (returnedFreeQty > 0) {
					lineItem.setItemFreeQuantity(newFreeQty);
				}

				newSubtotal += lineItem.getLineItemTotal();
			}
			// Calculate new invoice value if qty was returned
		} catch (err) {
			_didIteratorError3 = true;
			_iteratorError3 = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion3 && _iterator3.return) {
					_iterator3.return();
				}
			} finally {
				if (_didIteratorError3) {
					throw _iteratorError3;
				}
			}
		}

		if (isQtyReturned) {
			var newBillAmount = newSubtotal;

			if (oldTxnDiscountPercentage > 0) {
				var newDiscountAmount = newBillAmount * oldTxnDiscountPercentage / 100;
				oldTxnObj.setDiscountAmount(MyDouble.getAmountWithDecimal(newDiscountAmount));
				newBillAmount -= newDiscountAmount;
			}
			if (oldTxnTaxPercentage > 0) {
				var newTaxAmount = newBillAmount * oldTxnTaxPercentage / 100;
				oldTxnObj.setTaxAmount(MyDouble.getAmountWithDecimal(newTaxAmount));
				newBillAmount += newTaxAmount;
			}
			var ac1 = MyDouble.convertStringToDouble(oldTxnObj.getAc1Amount());
			var ac2 = MyDouble.convertStringToDouble(oldTxnObj.getAc2Amount());
			var ac3 = MyDouble.convertStringToDouble(oldTxnObj.getAc3Amount());
			var roundOff = MyDouble.convertStringToDouble(oldTxnObj.getRoundOffValue());

			newBillAmount += ac1 + ac2 + ac3 + roundOff;
			oldTxnObj.setBalanceAmount(MyDouble.getAmountWithDecimal(newBillAmount));
		}
		window.CONVERT_DELIVERY_CHALLAN_TO_SALE_OBJECT = oldTxnObj;
		$('#returnGoodsDialog').dialog('close');
		openTransaction(oldTxnObj);
	};

	var openTransaction = function openTransaction(oldTxnObj) {
		var MountComponent = require('../UIComponent/jsx/MountComponent').default;
		var Component = require('../UIComponent/jsx/SalePurchaseContainer').default;
		var props = {
			txnType: TxnTypeConstant[value],
			txnId: txnIdReq
		};
		if (oldTxnObj) {
			props.txnObj = oldTxnObj;
		}
		MountComponent(Component, document.querySelector('#salePurchaseContainer'), props);
	};

	if (txnType == 'TXN_TYPE_DELIVERY_CHALLAN') {
		var settingCache = new SettingCache();
		if (settingCache.isDeliveryChallanReturnEnabled()) {
			var $el = $('#returnGoodsDialog');
			$el.html(getReturnGoodsHTML(txnIdReq, txnType));
			$el.dialog({
				appendTo: '#dynamicDiv',
				modal: true,
				width: 750,
				height: 600,
				close: function close(e) {
					e.stopPropagation();
					$el.empty();
				}
			});
		} else {
			openTransaction();
		}
	} else {
		openTransaction();
	}

	var saveReturnGoodSettings = function saveReturnGoodSettings(e) {
		var SettingsModel = require('../Models/SettingsModel');
		var Queries = require('../Constants/Queries');
		var ErrorCode = require('../Constants/ErrorCode');
		var settingsModel = new SettingsModel();
		var $target = $(e.target);
		settingsModel.setSettingKey(Queries.SETTING_DELIVERY_CHALLAN_RETURN_ENABLED);
		var statusCode = settingsModel.UpdateSetting(!$target.is(':checked'));
		if (statusCode == ErrorCode.ERROR_SETTING_SAVE_FAILED) {
			ToastHelper.error(ErrorCode.ERROR_SETTING_SAVE_FAILED);
			$target.prop('checked', !$target.is(':checked'));
			return false;
		}
	};

	$('#chkBoxReturnGoods').off('change').on('change', saveReturnGoodSettings);
	$('#continueReturnGoods').off('click').on('click', continueReturnGoodsToSale);
};

if (module && module.exports) {
	module.exports = convertToSalePurchase;
}