module.exports = function toasterHelper() {
	var toastr = require('toastr');
	var SyncHandler = require('./../Utilities/SyncHandler');
	var defaultOptions = {
		'closeButton': true,
		'debug': false,
		'newestOnTop': false,
		'progressBar': true,
		'positionClass': 'toast-top-right',
		'preventDuplicates': true,
		'onclick': null,
		'showDuration': '300',
		'hideDuration': '1000',
		'timeOut': '5000',
		'extendedTimeOut': '1000',
		'showEasing': 'swing',
		'hideEasing': 'linear',
		'showMethod': 'fadeIn',
		'hideMethod': 'fadeOut'
	};
	toastr.options = defaultOptions;
	return {
		success: function success(message, title) {
			var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

			toastr.success(this.getMessage(message), title, this.getOptions(options));
		},
		warning: function warning(message, title) {
			var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

			toastr.warning(this.getMessage(message), title, this.getOptions(options));
		},
		error: function error(message, title) {
			var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

			toastr.error(this.getMessage(message), title, this.getOptions(options));
		},
		info: function info(message, title) {
			var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

			toastr.info(this.getMessage(message), title, this.getOptions(options));
		},
		successWithoutCompanyAccess: function successWithoutCompanyAccess(message, title) {
			var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

			toastr.success(message, title, this.getOptions(options));
		},
		warningWithoutCompanyAccess: function warningWithoutCompanyAccess(message, title) {
			var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

			toastr.warning(message, title, this.getOptions(options));
		},
		errorWithoutCompanyAccess: function errorWithoutCompanyAccess(message, title) {
			var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

			toastr.error(message, title, this.getOptions(options));
		},
		infoWithoutCompanyAccess: function infoWithoutCompanyAccess(message, title) {
			var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

			toastr.info(message, title, this.getOptions(options));
		},
		getOptions: function getOptions() {
			var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

			return $.extend({}, defaultOptions, options);
		},
		getMessage: function getMessage(msg) {
			var message = '';
			if (typeof globalErrorMessage !== 'undefined' && globalErrorMessage && SyncHandler.isSyncEnabled()) {
				message = globalErrorMessage;
			} else {
				message = msg;
			}

			if (!message) {
				var ErrorCode = require('./../Constants/ErrorCode.js');
				message = ErrorCode.ERROR_CONTACT_VYAPAR_SUPPORT;
			}
			globalErrorMessage = '';
			return message;
		}
	};
}();