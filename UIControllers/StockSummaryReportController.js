var ItemCache = require('./../Cache/ItemCache.js');
var itemCache = new ItemCache();
var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();
var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var ItemSummaryReportObject = require('./../BizLogic/ItemSummaryReportObject.js');
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');
var DateFormat = require('./../Constants/DateFormat.js');
$('#itemSummaryFilterDate').datepicker({ dateFormat: DateFormat.format });

var date = MyDate.getDate('d/m/y');
$('#itemSummaryFilterDate').val(date);

var printForSharePDF = false;

var itemListToBeShown = [];

var populateTable = function populateTable() {
	itemCache = new ItemCache();
	settingCache = new SettingCache();

	CommonUtility.showLoader(function () {
		itemListToBeShown = [];
		var groupFilter = Number($('#groupFilterOptions').val());
		var isDateFilterOn = $('#dateFilter').prop('checked');
		var showItemsOnlyInStock = $('#zeroStockFilter').prop('checked');
		var showOnlyActiveItems = true;
		if ($('#showInactiveItems').is(':checked')) {
			showOnlyActiveItems = false;
		}
		if (isDateFilterOn) {
			var DataLoader = require('./../DBManager/DataLoader.js');
			var dataLoader = new DataLoader();
			var endDateString = $('#itemSummaryFilterDate').val();
			var toDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');
			var _itemSummaryReportData = dataLoader.getItemStockSummaryReportData(toDate, groupFilter, showOnlyActiveItems);
			_itemSummaryReportData.forEach(function (value, key) {
				if (!showItemsOnlyInStock || Number(value[0]) > 0.000001) {
					var itemSummaryReportObject = new ItemSummaryReportObject();
					itemSummaryReportObject.setItemId(key);
					itemSummaryReportObject.setItemStockQuantity(value[0]);
					itemSummaryReportObject.setItemStockValue(value[1]);
					var item = itemCache.getItemById(key);
					itemSummaryReportObject.setItemName(item.getItemName());
					itemSummaryReportObject.setItemSaleUnitPrice(item.getItemSaleUnitPrice());
					itemSummaryReportObject.setItemPurchaseUnitPrice(item.getItemPurchaseUnitPrice());
					itemListToBeShown.push(itemSummaryReportObject);
				}
			});
		} else {
			var itemList = itemCache.getListOfItemsObject($('#showInactiveItems').is(':checked'));
			var itemSummaryReportData = [];
			for (i in itemList) {
				var itemObject = itemList[i];
				if (groupFilter > 0 && itemObject.getItemCategoryId() != groupFilter) {
					continue;
				}
				itemSummaryReportData.push(itemObject);
			}

			for (i = 0; i < itemSummaryReportData.length; i++) {
				var item = itemSummaryReportData[i];
				if (!showItemsOnlyInStock || Number(item.getItemStockQuantity()) > 0.000001) {
					var itemSummaryReportObject = new ItemSummaryReportObject();
					itemSummaryReportObject.setItemId(item.getItemId());
					itemSummaryReportObject.setItemStockQuantity(item.getItemStockQuantity());
					itemSummaryReportObject.setItemStockValue(item.getItemStockValue());
					itemSummaryReportObject.setItemName(item.getItemName());
					itemSummaryReportObject.setItemSaleUnitPrice(item.getItemSaleUnitPrice());
					itemSummaryReportObject.setItemPurchaseUnitPrice(item.getItemPurchaseUnitPrice());
					itemListToBeShown.push(itemSummaryReportObject);
				}
			}
		}
		sortItemListForSummaryReport(itemListToBeShown);
		displayItemList(itemListToBeShown);
	});
};

if (itemCache.IsAnyItemInactive()) {
	$('#activeInactiveItems').removeClass('hide');
} else {
	$('#activeInactiveItems').addClass('hide');
}

$('#showInactiveItems').on('change', function () {
	populateTable();
});

function sortItemListForSummaryReport(itemList) {
	itemList.sort(function (a, b) {
		a = a.getItemName();
		b = b.getItemName();
		if (a.toLowerCase() > b.toLowerCase()) return 1;
		if (a.toLowerCase() < b.toLowerCase()) return -1;
		return 0;
	});
}

if (settingCache.isItemCategoryEnabled()) {
	var ItemCategoryCache = require('./../Cache/ItemCategoryCache.js');
	var itemCategoryCache = new ItemCategoryCache();
	var itemCategoryList = itemCategoryCache.getItemCategoriesList();

	var optionTemp = document.createElement('option');
	for (i in itemCategoryList) {
		var option = optionTemp.cloneNode();
		option.text = itemCategoryList[i].getCategoryName();
		option.value = i;
		$('#groupFilterOptions').append(option);
	}

	$('#stockSummaryCategoryFilter').show();
} else {
	$('#stockSummaryCategoryFilter').hide();
}

$('#groupFilterOptions').on('selectmenuchange', function (event) {
	populateTable();
});

$('#itemSummaryFilterDate').change(function () {
	populateTable();
});

$('#dateFilter').change(function () {
	populateTable();
	var isDateFilterEnabled = $('#dateFilter').prop('checked');
	if (isDateFilterEnabled) {
		$('#itemSummaryFilterDate').prop('disabled', false);
	} else {
		$('#itemSummaryFilterDate').prop('disabled', true);
	}
});

$('#zeroStockFilter').change(function () {
	populateTable();
});

var stockSummaryReportClusterize;

var displayItemList = function displayItemList(itemList) {
	var len = itemList.length;

	var isStockEnabled = settingCache.getStockEnabled();
	var extraColumnWidth = isStockEnabled ? 0 : 12;

	var dynamicRow = '';
	var dynamicFooter = '';
	var totalStockQty = 0;
	var totalStockValue = 0;
	var rowData = [];

	dynamicRow += "<thead><tr><th width='7%'></th><th width='" + (21 + extraColumnWidth) + "%'>Item Name</th><th width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>Sale Price</th><th width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>Purchase Price</th>";
	if (isStockEnabled) {
		dynamicRow += "<th width='18%' class='tableCellTextAlignRight'>Stock qty</th><th width='18%' class='tableCellTextAlignRight'>Stock value</th>";
		dynamicFooter += "<thead><tr><th width='7%'></th><th width='" + (21 + extraColumnWidth) + "%'>Total</th><th width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'></th><th width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'></th>";
	}
	dynamicRow += '</tr></thead>';
	for (var _i = 0; _i < len; _i++) {
		var itemObject = itemList[_i];
		var row = '';

		row = "<tr ondblclick='openItemDetails(" + itemObject.getItemId() + ")' class='currentRow'><td>" + (_i + 1) + "</td><td width='" + (21 + extraColumnWidth) + "%'>" + itemObject.getItemName() + "</td><td class='tableCellTextAlignRight' width='" + (18 + extraColumnWidth) + "%'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(itemObject.getItemSaleUnitPrice()) + "</td><td class='tableCellTextAlignRight' width='" + (18 + extraColumnWidth) + "%'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(itemObject.getItemPurchaseUnitPrice()) + '</td>';
		if (isStockEnabled) {
			row += "<td class='tableCellTextAlignRight' width='18%'>" + MyDouble.getQuantityWithDecimal(itemObject.getItemStockQuantity()) + "</td><td class='tableCellTextAlignRight' width='18%'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(itemObject.getItemStockValue()) + '</td>';
			totalStockQty += Number(itemObject.getItemStockQuantity());
			totalStockValue += Number(itemObject.getItemStockValue());
		}
		row += '</tr>';
		rowData.push(row);
	}
	var data = rowData;

	if ($('#stockSummaryReportContainer').length === 0) {
		return;
	}
	if (stockSummaryReportClusterize && stockSummaryReportClusterize.destroy) {
		stockSummaryReportClusterize.clear();
		stockSummaryReportClusterize.destroy();
	}

	stockSummaryReportClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollArea',
		contentId: 'contentArea',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});
	if (isStockEnabled) {
		dynamicFooter += "<th width='18%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimal(totalStockQty) + "</th><th width='18%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalStockValue) + '</th>';
	}
	$('#tableHead').html(dynamicRow);
	$('#total').html(dynamicFooter);
};

var openItemDetails = function openItemDetails(itemId) {
	var selectedItemName = itemCache.findItemNameById(itemId);
	selectedItemNameFromReportsGlobal = selectedItemName;;
	// angular.element(document.getElementById('body')).scope().selectedItemNameFromReports = selectedItemName;
	$('#viewItems').trigger('click');
};

if (settingCache.getStockEnabled()) {
	$('#stockQuantityPrintOption').show();
	$('#stockValuePrintOption').show();
} else {
	$('#stockQuantityPrintOption').hide();
	$('#stockValuePrintOption').hide();
}

var getHTMLTextForReport = function getHTMLTextForReport() {
	var StockSummaryReportHTMLGenerator = require('./../ReportHTMLGenerator/StockSummaryReportHTMLGenerator.js');
	var stockSummaryReportHTMLGenerator = new StockSummaryReportHTMLGenerator();

	var printSalePrice = $('#printSalePrice').is(':checked');
	var printPurchasePrice = $('#printPurchasePrice').is(':checked');
	var printStockQuantity = $('#printStockQuantity').is(':checked');
	var printStockValue = $('#printStockValue').is(':checked');
	var htmlText = stockSummaryReportHTMLGenerator.getHTMLText(itemListToBeShown, printSalePrice, printPurchasePrice, printStockQuantity, printStockValue);
	return htmlText;
};

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	// $('#frameDiv').load('');
	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable();
};

/// ///////////////////////////PDF/////////////////////////////////////

function openPrintOptions() {
	$('#stockSummaryPrintDialog').removeClass('hide');
	$('#stockSummaryPrintDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closePrintOptions() {
	$('#stockSummaryPrintDialog').addClass('hide');
	$('#stockSummaryPrintDialog').dialog('close').dialog('destroy');
}

var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.STOCK_SUMMARY_REPORT, fromDate: $('#itemSummaryFilterDate').val() });
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#stockSummaryPrintDialog').addClass('hide');
	$('#stockSummaryPrintDialog').dialog('close').dialog('destroy');
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

function openExcelOptions() {
	$('#ExcelDialog').removeClass('hide');
	$('#ExcelDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closeExcelOptions() {
	$('#ExcelDialog').addClass('hide');
	$('#ExcelDialog').dialog('close').dialog('destroy');
}

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.STOCK_SUMMARY_REPORT, fromDate: $('#itemSummaryFilterDate').val() });
	excelHelper.saveExcel(fileName);
};

var prepareObjectForExcel = function prepareObjectForExcel(itemList) {
	var isStockEnabled = settingCache.getStockEnabled();
	var len = itemList.length;
	var rowObj = [];
	var tableHeadArray = [];
	var totalArray = [];
	var ExcelSalePrice = $('#excelSalePrice').is(':checked');
	var ExcelPurchasePrice = $('#excelPurchasePrice').is(':checked');
	var ExcelStockQuantity = $('#excelStockQuantity').is(':checked');
	var ExcelStockValue = $('#excelStockValue').is(':checked');
	tableHeadArray.push('Item Name');
	if (ExcelSalePrice) {
		tableHeadArray.push('Sale Price');
		totalArray.push('');
	}
	if (ExcelPurchasePrice) {
		tableHeadArray.push('Purchase Price');
		totalArray.push('');
	}
	if (isStockEnabled) {
		totalArray.push('Total');
		if (ExcelStockQuantity) {
			tableHeadArray.push('Stock Quantity');
		}
		if (ExcelStockValue) {
			tableHeadArray.push('Stock Value');
		}
	}
	rowObj.push(tableHeadArray);
	rowObj.push([]);

	var stockQuantityTotal = 0;
	var stockValueTotal = 0;
	for (var j = 0; j < len; j++) {
		var itemObject = itemList[j];
		var tempArray = [];
		tempArray.push(itemObject.getItemName());
		if (ExcelSalePrice) {
			tempArray.push(MyDouble.getAmountWithDecimal(itemObject.getItemSaleUnitPrice()));
		}
		if (ExcelPurchasePrice) {
			tempArray.push(MyDouble.getAmountWithDecimal(itemObject.getItemPurchaseUnitPrice()));
		}
		if (isStockEnabled) {
			if (ExcelStockQuantity) {
				tempArray.push(MyDouble.getQuantityWithDecimalWithoutColor(itemObject.getItemStockQuantity()));
				stockQuantityTotal += Number(itemObject.getItemStockQuantity());
			}
			if (ExcelStockValue) {
				tempArray.push(MyDouble.getAmountWithDecimal(itemObject.getItemStockValue()));
				stockValueTotal += Number(itemObject.getItemStockValue());
			}
		}
		rowObj.push(tempArray);
	}
	rowObj.push([]);
	if (isStockEnabled) {
		if (ExcelStockQuantity) {
			totalArray.push(MyDouble.getQuantityWithDecimalWithoutColor(stockQuantityTotal));
		}
		if (ExcelStockValue) {
			totalArray.push(MyDouble.getAmountWithDecimal(stockValueTotal));
		}
	}
	rowObj.push(totalArray);
	return rowObj;
};

function printExcel() {
	closeExcelOptions();
	var XLSX = require('xlsx');
	var dataArray = prepareObjectForExcel(itemListToBeShown);
	var worksheet = XLSX.utils.aoa_to_sheet(dataArray);

	var workbook = {
		'SheetNames': [],
		'Sheets': {}
	};

	var wsName = 'Stock Summary Report';
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet;
	var wscolWidth = [{ wch: 15 }, { wch: 30 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 30 }];
	worksheet['!cols'] = wscolWidth;
	console.log(workbook);
	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

populateTable();