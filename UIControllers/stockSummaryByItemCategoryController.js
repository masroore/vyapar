var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var startDate;
var endDate;
var DateFormat = require('./../Constants/DateFormat.js');
var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');
var printForSharePDF = false;

var listOfTransactions = [];

var populateTable = function populateTable() {
	var DataLoader = require('./../DBManager/DataLoader.js');
	var dataLoader = new DataLoader();
	CommonUtility.showLoader(function () {
		listOfTransactions = dataLoader.getItemCategoryStockList();
		displayTransactionList(listOfTransactions);
	});
};

var stockSummaryByItemCategoryClusterize;

var displayTransactionList = function displayTransactionList(listOfTransactions) {
	var len = listOfTransactions.length;
	var rowData = [];
	var dynamicRow = '';
	dynamicRow += "<thead><tr><th width='33%'>Item Category</th><th width='33%' class='tableCellTextAlignRight'>Stock quantity</th><th width='33%' class='tableCellTextAlignRight'>Stock Value</th></tr></thead>";
	var totalStockQuantity = 0;
	var totalStockValue = 0;
	var _iteratorNormalCompletion = true;
	var _didIteratorError = false;
	var _iteratorError = undefined;

	try {
		for (var _iterator = (0, _getIterator3.default)(listOfTransactions), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
			var _ref = _step.value;

			var _ref2 = (0, _slicedToArray3.default)(_ref, 2);

			var key = _ref2[0];
			var value = _ref2[1];

			var row = '';
			var itemLine = listOfTransactions.get(key);
			var itemName = itemLine.get('name');
			var stockQty = itemLine.get('qty');
			var stockValue = itemLine.get('amount');
			row += "<tr><td width='33%'>" + itemName + '</td>';

			row += "<td width='33%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(stockQty) + '</td>';
			row += "<td width='33%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(stockValue) + '</td>';
			row += '</tr>';
			totalStockQuantity += stockQty;
			totalStockValue += stockValue;
			rowData.push(row);
		}
	} catch (err) {
		_didIteratorError = true;
		_iteratorError = err;
	} finally {
		try {
			if (!_iteratorNormalCompletion && _iterator.return) {
				_iterator.return();
			}
		} finally {
			if (_didIteratorError) {
				throw _iteratorError;
			}
		}
	}

	var data = rowData;

	if ($('#stockSummaryByItemCategoryDetails').length === 0) {
		return;
	}
	if (stockSummaryByItemCategoryClusterize && stockSummaryByItemCategoryClusterize.destroy) {
		stockSummaryByItemCategoryClusterize.clear();
		stockSummaryByItemCategoryClusterize.destroy();
	}

	stockSummaryByItemCategoryClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollArea',
		contentId: 'contentArea',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});

	$('#tableHead').html(dynamicRow);
	$('#totalStockQuantity').html(MyDouble.getQuantityWithDecimalWithoutColor(totalStockQuantity));
	$('#totalStockValue').html(MyDouble.getQuantityWithDecimalWithoutColor(totalStockValue));
};

var getHTMLTextForReport = function getHTMLTextForReport() {
	var stockSummaryByItemCategoryHTMLGenerator = require('./../ReportHTMLGenerator/stockSummaryByItemCategoryHTMLGenerator.js');
	var stockSummaryByItemCategoryHTMLGenerator = new stockSummaryByItemCategoryHTMLGenerator();
	var htmlText = stockSummaryByItemCategoryHTMLGenerator.getHTMLText(listOfTransactions);
	return htmlText;
};

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable();
};

/// ///////////////////////////PDF/////////////////////////////////////
var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.STOCK_SUMMARY_BY_ITEM_CATEGORY });
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.STOCK_SUMMARY_BY_ITEM_CATEGORY });
	excelHelper.saveExcel(fileName);
};

var prepareObjectForExcel = function prepareObjectForExcel(listOfTransactions) {
	//
	var len = listOfTransactions.length;
	var rowObj = [];
	var tableHeadArray = [];
	tableHeadArray.push('Item Category');
	tableHeadArray.push('Stock Quantity');
	tableHeadArray.push('Stock Value');
	rowObj.push(tableHeadArray);
	rowObj.push([]);

	var totalStockQuantity = 0;
	var totalStockValue = 0;
	var _iteratorNormalCompletion2 = true;
	var _didIteratorError2 = false;
	var _iteratorError2 = undefined;

	try {
		for (var _iterator2 = (0, _getIterator3.default)(listOfTransactions), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
			var _ref3 = _step2.value;

			var _ref4 = (0, _slicedToArray3.default)(_ref3, 2);

			var key = _ref4[0];
			var value = _ref4[1];

			var tempArray = [];
			var itemLine = listOfTransactions.get(key);
			var itemName = itemLine.get('name');
			var stockQty = itemLine.get('qty');
			var stockValue = itemLine.get('amount');
			tempArray.push(itemName);
			tempArray.push(MyDouble.getQuantityWithDecimalWithoutColor(stockQty));
			tempArray.push(MyDouble.getQuantityWithDecimalWithoutColor(stockValue));
			totalStockQuantity += stockQty;
			totalStockValue += stockValue;
			rowObj.push(tempArray);
		}
	} catch (err) {
		_didIteratorError2 = true;
		_iteratorError2 = err;
	} finally {
		try {
			if (!_iteratorNormalCompletion2 && _iterator2.return) {
				_iterator2.return();
			}
		} finally {
			if (_didIteratorError2) {
				throw _iteratorError2;
			}
		}
	}

	rowObj.push([]);
	var totalArray = ['Total', MyDouble.getQuantityWithDecimalWithoutColor(totalStockQuantity), MyDouble.getQuantityWithDecimalWithoutColor(totalStockValue)];
	rowObj.push(totalArray);
	return rowObj;
};

function printExcel() {
	var XLSX = require('xlsx');
	var dataArray = prepareObjectForExcel(listOfTransactions);
	var worksheet = XLSX.utils.aoa_to_sheet(dataArray);

	var workbook = {
		'SheetNames': [],
		'Sheets': {}
	};

	var wsName = 'StockSummaryByItemCategory';
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet;
	var wscolWidth = [{ wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }];
	worksheet['!cols'] = wscolWidth;
	console.log(workbook);
	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

populateTable();