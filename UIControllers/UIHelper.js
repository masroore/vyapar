var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var UIHelper = {
	showDeleteTxnPasscodeDialog: function showDeleteTxnPasscodeDialog() {
		var callBackArguments = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
		var successCallBack = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
		var cancelCallBack = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

		$('#passCodeDeleteDialog').dialog({

			closeOnEscape: false,
			resizable: false,
			height: 'auto',
			minHeight: 170,
			width: 350,
			appendTo: '#dynamicDiv',
			modal: true,
			maxWidth: 500,
			close: function close() {
				$(this).dialog('close');
				$(this).dialog('destroy');
			}

		});
		$('#confirmPasscode input').on('keyup', function () {
			if (event.keyCode == 8 && this.id != 'c1') {
				$(this).prev().focus();
			} else if (this.value.length == 1 && this.id != 'c4') {
				$(this).next().focus();
			}
			var e1 = $('#e1').val();
			var e2 = $('#e2').val();
			var e3 = $('#e3').val();
			var e4 = $('#e4').val();
			var confirmPasscode = e1 + '' + e2 + '' + e3 + '' + e4;
			var statusCode = '';

			if (confirmPasscode && confirmPasscode.length == 4) {
				$('#terminalButtonDeletePassCode').show();
				$('#passcodeDeleteError').text('');
			} else {
				$('#terminalButtonDeletePassCode').hide();
			}
		});

		$('#terminalButtonDeletePassCode').off('click').on('click', function () {
			var e1 = $('#e1').val();
			var e2 = $('#e2').val();
			var e3 = $('#e3').val();
			var e4 = $('#e4').val();
			var confirmPasscode = e1 + '' + e2 + '' + e3 + '' + e4;
			var SettingCache = require('./../Cache/SettingCache.js');
			var settingCache = new SettingCache();
			var passcode = settingCache.getDeletePasscode();
			if (passcode == confirmPasscode) {
				if (successCallBack) {
					successCallBack(callBackArguments);

					$('#passCodeDeleteDialog').dialog('close');
				}
			} else {
				$('#passcodeDeleteError').text('Passcode mismatch. Please try again.');
				$('#confirmPasscode').find('input').val('');
				$('#e1').focus();
			}
		});

		$('#forgotDeletePasscode').off('click').on('click', function () {
			$('#loading').show(function () {
				var PassCodeHelperUtility = require('../Utilities/passCodeHelperUtitlity');
				var errorCallBack = function errorCallBack(statusMsg) {
					$('#loading').hide();
					statusMsg && alert(statusMsg);
				};
				var successCallBack = function successCallBack(statusMsg) {
					$('#loading').hide();
					statusMsg && alert(statusMsg);
				};
				var passcodeType = 'deleteTxn';
				PassCodeHelperUtility.resetPassCode({
					passcodeType: passcodeType,
					errorCallBack: errorCallBack,
					successCallBack: successCallBack
				});
			});
		});
	},
	getAutocompleteDefaultOptions: function getAutocompleteDefaultOptions() {
		var _source = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

		var key = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
		var numberOfResults = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 50;

		return {
			minLength: 0,
			delay: 500,
			search: function search() {
				$(this).data('ui-autocomplete').menu.bindings = $();
			},
			select: function select(event, ui) {
				$(this).val(ui.item.value);
				$(this).blur();
			},
			source: function source(request, response) {
				var textToBeSearched = request.term ? request.term.toUpperCase() : '';
				var results = [];
				var _iteratorNormalCompletion = true;
				var _didIteratorError = false;
				var _iteratorError = undefined;

				try {
					for (var _iterator = (0, _getIterator3.default)(_source), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
						var item = _step.value;

						var currentItem = key ? item[key] : item;
						if (currentItem.toUpperCase().indexOf(textToBeSearched) != -1) {
							results.push(item);
							if (results.length == numberOfResults) {
								break;
							}
						}
					}
				} catch (err) {
					_didIteratorError = true;
					_iteratorError = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion && _iterator.return) {
							_iterator.return();
						}
					} finally {
						if (_didIteratorError) {
							throw _iteratorError;
						}
					}
				}

				response(results);
			}
		};
	}
};

module.exports = UIHelper;