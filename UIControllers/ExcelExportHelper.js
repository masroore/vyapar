var ExcelExportHelper = function ExcelExportHelper() {
	this.prepareItemObjectForExcel = function (listOfTransactions) {
		var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
		var ItemUnitCache = require('./../Cache/ItemUnitCache.js');
		var SettingCache = require('./../Cache/SettingCache.js');
		var ItemUnitMappingCache = require('./../Cache/ItemUnitMappingCache.js');
		var settingCache = new SettingCache();
		var itemUnitCache = new ItemUnitCache();
		var isTransactionRefNumberEnabled = settingCache.getTxnRefNoEnabled();
		var itemWiseDiscount = settingCache.isItemwiseDiscountEnabled();
		var itemWiseTax = settingCache.isItemwiseTaxEnabled();
		var itemUnit = settingCache.getItemUnitEnabled();
		var isHSNCodeEnabled = settingCache.getHSNSACEnabled();
		itemUnitMappingCache = new ItemUnitMappingCache();
		var totalAmount = 0;
		var quantityTotal = 0;
		var taxTotal = 0;
		var discountTotal = 0;
		var subTotal = 0;
		var tableHeadArray = [];
		var rowObj = [];
		var totalArray = [];
		var len = listOfTransactions.length;
		tableHeadArray.push('Date');
		totalArray.push('');
		if (isTransactionRefNumberEnabled) {
			tableHeadArray.push('Invoice');
			totalArray.push('');
		}
		tableHeadArray.push('Party Name');
		tableHeadArray.push('Item Name');
		totalArray.push('');
		var mrpColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_MRP);
		var batchNumberColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_BATCH_NUMBER);
		var serialNumberColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_SERIAL_ITEM_NUMBER);
		var mfgDateColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_MANUFACTURING_DATE);
		var expDateColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_EXPIRY_DATE);
		var sizeColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_SIZE);

		if (mrpColumnVisibility) {
			tableHeadArray.push(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_MRP_VALUE));
			totalArray.push('');
		}
		if (batchNumberColumnVisibility) {
			tableHeadArray.push(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_BATCH_NUMBER_VALUE));
			totalArray.push('');
		}
		if (expDateColumnVisibility) {
			tableHeadArray.push(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_EXPIRY_DATE_VALUE));
			totalArray.push('');
		}
		if (mfgDateColumnVisibility) {
			tableHeadArray.push(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_MANUFACTURING_DATE_VALUE));
			totalArray.push('');
		}
		if (serialNumberColumnVisibility) {
			tableHeadArray.push(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_SERIAL_NUMBER_VALUE));
			totalArray.push('');
		}
		if (settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_COUNT)) {
			tableHeadArray.push(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_COUNT_VALUE));
			totalArray.push('');
		}
		if (settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_DESCRIPTION)) {
			tableHeadArray.push(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_DESCRIPTION_VALUE));
			totalArray.push('');
		}
		if (sizeColumnVisibility) {
			tableHeadArray.push(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_SIZE_VALUE));
			totalArray.push('');
		}
		tableHeadArray.push('Quantity');
		totalArray.push('');
		if (itemUnit) {
			tableHeadArray.push('Unit');
			totalArray.push('');
		}
		tableHeadArray.push('UnitPrice');
		totalArray.push('');
		if (itemWiseDiscount) {
			tableHeadArray.push('Discount Percent');
			tableHeadArray.push('Discount');
		}
		if (itemWiseTax) {
			tableHeadArray.push('Tax Percent');
			tableHeadArray.push('Tax');
		}
		if (settingCache.getAdditionalCessEnabled()) {
			tableHeadArray.push('Cess');
		}
		tableHeadArray.push('Transaction Type');
		tableHeadArray.push('Amount');
		totalArray.push('');
		rowObj.push(tableHeadArray);
		rowObj.push([]);
		console.log(listOfTransactions);
		for (var j = 0; j < len; j++) {
			var txn = listOfTransactions[j];

			var itemLen = 0;
			var txnItemLines = [];

			if (txn.getLineItems) {
				txnItemLines = txn.getLineItems();
				if (txnItemLines && txnItemLines.length) {
					itemLen = txnItemLines.length;
				}
			}

			if (itemLen > 0) {
				var txnType = txn.getTxnType();
				var typeString = TxnTypeConstant.getTxnTypeForUI(txn.getTxnType());
				var date = MyDate.getDate('d/m/y', txn.getTxnDate());
				var refNo = txn.getFullTxnRefNumber();

				for (var i = 0; i < itemLen; i++) {
					var currentLineItem = txnItemLines[i];
					var tempArray = [];
					tempArray.push(date);
					if (isTransactionRefNumberEnabled) {
						tempArray.push(refNo);
					}
					var partyName = txn.getNameRef().getFullName();
					if (txn.getDisplayName() && txn.getDisplayName().trim() && partyName !== txn.getDisplayName().trim()) {
						partyName = partyName + ' (' + txn.getDisplayName().trim() + ')';
					}
					tempArray.push(partyName);
					tempArray.push(currentLineItem.getItemName());

					if (mrpColumnVisibility) {
						tempArray.push(MyDouble.getAmountWithDecimal(currentLineItem.getLineItemMRP()));
					}
					if (batchNumberColumnVisibility) {
						tempArray.push(currentLineItem.getLineItemBatchNumber());
					}
					if (expDateColumnVisibility) {
						tempArray.push(currentLineItem.getLineItemExpiryDate() ? MyDate.getDate('d/m/y', currentLineItem.getLineItemExpiryDate()) : '');
					}
					if (mfgDateColumnVisibility) {
						tempArray.push(currentLineItem.getLineItemManufacturingDate() ? MyDate.getDate('d/m/y', currentLineItem.getLineItemManufacturingDate()) : '');
					}
					if (serialNumberColumnVisibility) {
						tempArray.push(currentLineItem.getLineItemSerialNumber());
					}
					if (settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_COUNT)) {
						tempArray.push(MyDouble.getQuantityWithDecimalWithoutColor(currentLineItem.getLineItemCount()));
					}
					if (settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_DESCRIPTION)) {
						tempArray.push(currentLineItem.getLineItemDescription());
					}
					if (sizeColumnVisibility) {
						tempArray.push(currentLineItem.getLineItemSize());
					}

					var unitPrice = currentLineItem.getItemUnitPrice();
					var quantity = currentLineItem.getItemQuantity();
					var txnTypeString = TxnTypeConstant.getTxnTypeForUI(txn.getTxnType());

					if (currentLineItem.getLineItemUnitMappingId() > 0) {
						var mappingObj = itemUnitMappingCache.getItemUnitMapping(currentLineItem.getLineItemUnitMappingId());
						quantity = Number(currentLineItem.getItemQuantity()) * mappingObj.getConversionRate();
						unitPrice = currentLineItem.getItemUnitPrice() / mappingObj.getConversionRate();
					}
					quantityTotal += quantity;
					tempArray.push(MyDouble.getQuantityWithDecimalWithoutColor(quantity) + MyDouble.quantityDoubleToStringWithSignExplicitly(currentLineItem.getItemFreeQuantity(), true));

					taxTotal += currentLineItem.getLineItemTaxAmount() ? Number(currentLineItem.getLineItemTaxAmount()) : 0;
					discountTotal += currentLineItem.getLineItemDiscountAmount() ? Number(currentLineItem.getLineItemDiscountAmount()) : 0;

					if (itemUnit) {
						if (txn.getTxnType() != TxnTypeConstant.TXN_TYPE_EXPENSE && txn.getTxnType() != TxnTypeConstant.TXN_TYPE_OTHER_INCOME) {
							var baseUnit = itemUnitCache.getItemUnitShortNameById(currentLineItem.getLineItemUnitId());
						} else {
							var baseUnit = '';
						}
						tempArray.push(baseUnit);
					}
					tempArray.push(MyDouble.getAmountWithDecimal(unitPrice));

					if (itemWiseDiscount) {
						if (txn.getTxnType() != TxnTypeConstant.TXN_TYPE_EXPENSE && txn.getTxnType() != TxnTypeConstant.TXN_TYPE_OTHER_INCOME) {
							var discPercent = currentLineItem.getLineItemDiscountPercent();
							var discAmount = currentLineItem.getLineItemDiscountAmount();
							tempArray.push(MyDouble.getPercentageWithDecimal(discPercent));
							tempArray.push(MyDouble.getAmountWithDecimal(discAmount));
						} else {
							tempArray.push('');
							tempArray.push('');
						}
					}
					if (itemWiseTax) {
						if (txn.getTxnType() != TxnTypeConstant.TXN_TYPE_EXPENSE && txn.getTxnType() != TxnTypeConstant.TXN_TYPE_OTHER_INCOME) {
							var taxPercent = currentLineItem.getLineItemTaxPercent();
							var taxAmount = currentLineItem.getLineItemTaxAmount();
							tempArray.push(MyDouble.getPercentageWithDecimal(taxPercent));
							tempArray.push(MyDouble.getAmountWithDecimal(taxAmount));
						} else {
							tempArray.push('');
							tempArray.push('');
						}
					}
					if (settingCache.getAdditionalCessEnabled()) {
						tempArray.push(MyDouble.getAmountWithDecimal(currentLineItem.getLineItemAdditionalCESS()));
					}
					tempArray.push(txnTypeString);
					tempArray.push(MyDouble.getAmountWithDecimal(currentLineItem.getLineItemTotal()));
					totalAmount += Number(currentLineItem.getLineItemTotal());
					rowObj.push(tempArray);
				}
			}
		}
		rowObj.push([]);
		if (itemWiseDiscount) {
			//  totalArray.push(MyDouble.getAmountWithDecimal(discountTotal));
			totalArray.push('');
		}
		if (itemWiseTax) {
			//   totalArray.push(MyDouble.getAmountWithDecimal(taxTotal));
		}
		if (settingCache.getAdditionalCessEnabled()) {
			totalArray.push('');
		}
		//  totalArray.push(MyDouble.getAmountWithDecimal(totalAmount));
		rowObj.push(totalArray);
		return rowObj;
	};
};

module.exports = ExcelExportHelper;