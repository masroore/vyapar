var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var startDate;
var endDate;
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');
var DateFormat = require('./../Constants/DateFormat.js');
$('#startDate, #endDate').datepicker({ dateFormat: DateFormat.format });
var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();

var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var NameCache = require('./../Cache/NameCache.js');
var nameCache = new NameCache();

var printForSharePDF = false;

var listOfTransactions = [];

var populateTable = function populateTable() {
	var DataLoader = require('./../DBManager/DataLoader.js');
	var dataLoader = new DataLoader();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var nameString = $('#nameInPrintCenter').val();
	var selectedTransaction = Number($('#transactionFilterOptions').val());
	if (isNaN(selectedTransaction)) {
		selectedTransaction = 0;
	}
	var nameId = -1;
	if (nameString) {
		var nameModel = nameCache.findNameModelByName(nameString);
		if (nameModel) {
			nameId = nameModel.getNameId();
		} else {
			nameId = 0;
		}
	}

	startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
	endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');
	var firmId = Number($('#firmFilterOptions').val());
	CommonUtility.showLoader(function () {
		listOfTransactions = dataLoader.loadTransactionsForPrintCenter(startDate, endDate, nameId, selectedTransaction, firmId);
		displayTransactionList(listOfTransactions, selectedTransaction);
	});
};

var date = MyDate.getDate('d/m/y');
$('#startDate').val(date);
$('#endDate').val(date);

var date = MyDate.getDate('d/m/y');
$('#startDate').val(date);

if (settingCache.getMultipleFirmEnabled()) {
	var optionTemp = document.createElement('option');
	var FirmCache = require('./../Cache/FirmCache.js');
	var firmCache = new FirmCache();
	var firmList = firmCache.getFirmList();
	for (i in firmList) {
		var option = optionTemp.cloneNode();
		option.text = firmList[i].getFirmName();
		option.value = firmList[i].getFirmId();
		$('#firmFilterOptions').append(option);
	}
	$('#firmChooser').show();
} else {
	$('#firmChooser').hide();
}

$('#firmFilterOptions').on('selectmenuchange', function (event) {
	populateTable();
});

$('#startDate').change(function () {
	populateTable();
});

$('#endDate').change(function () {
	populateTable();
});

$(function () {
	$('#nameInPrintCenter').autocomplete($.extend({}, UIHelper.getAutocompleteDefaultOptions(nameCache.getListOfNames()), {
		select: function select(event, ui) {
			$('#nameInPrintCenter').val(ui.item.value);
			populateTable();
		}
	}));
});

var optionTemp = document.createElement('option');
var transactionListArray = ['Sale', 'Purchase', 'Payment-In', 'Payment-Out', 'Credit Note', 'Debit Note', 'Sale Order', 'Purchase Order', 'Estimate', 'Delivery Challan'];
var transactionListId = [TxnTypeConstant.TXN_TYPE_SALE, TxnTypeConstant.TXN_TYPE_PURCHASE, TxnTypeConstant.TXN_TYPE_CASHIN, TxnTypeConstant.TXN_TYPE_CASHOUT, TxnTypeConstant.TXN_TYPE_SALE_RETURN, TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN, TxnTypeConstant.TXN_TYPE_SALE_ORDER, TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER, TxnTypeConstant.TXN_TYPE_ESTIMATE, TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN];
// var len = transactionsList.length;
for (i in transactionListArray) {
	var option = optionTemp.cloneNode();
	option.text = transactionListArray[i];
	option.value = transactionListId[i];
	$('#transactionFilterOptions').append(option);
}

$('#nameInPrintCenter').on('change', function (event) {
	populateTable();
});

$('#transactionFilterOptions').on('selectmenuchange', function () {
	populateTable();
});

var printCenterClusterize;

var displayTransactionList = function displayTransactionList(listOfTransactions, selectedTransaction) {
	var len = listOfTransactions.length;
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var dynamicRow = '';
	var rowData = [];
	var isTransactionRefNumberEnabled = settingCache.getTxnRefNoEnabled();
	var extraColumnWidth = isTransactionRefNumberEnabled ? 0 : 2;

	var className = '';
	if (!$('#sortIcon').attr('class')) {
		className = 'ui-selectmenu-icon ui-icon ui-icon-triangle-1-s';
	} else {
		className = $('#sortIcon').attr('class');
	}

	dynamicRow += "<thead><tr><th width='4%'><input type='checkbox' id='checkAll' class=''></th><th width='" + (9 + extraColumnWidth) + "%'>DATE</th>";
	if (isTransactionRefNumberEnabled) {
		dynamicRow += '<th width=\'12%\' id=\'invoiceColId\' style=\'cursor:pointer;\'>Ref No.<span id=\'sortIcon\' class=\'' + className + '\'></span></th>';
	}
	dynamicRow += "<th width='" + (16 + extraColumnWidth) + "%'>NAME</th><th width='" + (13 + extraColumnWidth) + "%'>TYPE</th><th width='" + (14 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>TOTAL</th><th width='" + (16 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>RECEIVED / PAID</th><th width='" + (16 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>BALANCE</th></tr></thead>";
	var finalTotalAmount = 0;
	for (var _i = 0; _i < len; _i++) {
		var txn = listOfTransactions[_i];
		var typeString = TxnTypeConstant.getTxnTypeForUI(txn.getTxnType());
		var name = txn.getNameRef().getFullName();
		var date = MyDate.getDate('d/m/y', txn.getTxnDate());
		var receivedAmt = txn.getCashAmount() ? txn.getCashAmount() : 0;
		var balanceAmt = txn.getBalanceAmount() ? txn.getBalanceAmount() : 0;
		var totalAmt = 0;
		var discountAmt = txn.getDiscountAmount() ? txn.getDiscountAmount() : 0;
		var typeTxn = TxnTypeConstant.getTxnType(txn.getTxnType());
		var refNo = txn.getTxnRefNumber() === '' ? '' : txn.getFullTxnRefNumber();
		if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHIN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHOUT) {
			totalAmt = Number(receivedAmt) + Number(discountAmt);
		} else {
			totalAmt = Number(receivedAmt) + Number(balanceAmt);
		}
		finalTotalAmount += totalAmt;
		var row = '<tr id=' + txn.getTxnId() + ':' + typeTxn + " ondblclick='openTransaction(" + txn.getTxnId() + ',' + txn.getTxnType() + ")' class='currentRow'><td width='4%'><input type='checkbox'  class='printCheck' value='" + txn.getTxnId() + "'></td><td width='" + (9 + extraColumnWidth) + "%'>" + date + '</td>';
		if (isTransactionRefNumberEnabled) {
			row += "<td width='12%'>" + refNo + '</td>';
		}
		row += "<td width='" + (16 + extraColumnWidth) + "%'>" + name + "</td><td width='" + (13 + extraColumnWidth) + "%'>" + typeString + "</td><td width='" + (14 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalAmt) + "</td><td width='" + (16 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(receivedAmt) + '</td>';
		if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHIN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHOUT) {
			row += "<td width='" + (16 + extraColumnWidth) + "%' class='tableCellTextAlignRight'></td></tr>";
		} else {
			row += "<td width='" + (16 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(balanceAmt) + '</td></tr>';
		}
		rowData.push(row);
	}
	var data = rowData;
	if ($('#printerCenterContainer').length === 0) {
		return;
	}
	if (printCenterClusterize && printCenterClusterize.destroy) {
		printCenterClusterize.clear();
		printCenterClusterize.destroy();
	}

	printCenterClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollArea',
		contentId: 'contentArea',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});
	$('#tableHead').html(dynamicRow);
	if (selectedTransaction > 0) {
		$('#totalAmount').html(MyDouble.getAmountWithDecimalAndCurrencyWithSign(finalTotalAmount));
		$('#total').css('display', 'block');
		if (totalAmount < 0) {
			$('#totalAmount').css('color', '#e35959');
		} else {
			$('#totalAmount').css('color', '#1faf9d');
		}
	} else {
		$('#total').css('display', 'none');
	}
};

$('#tableHead').on('click', '#checkAll', function () {
	$('.printCheck:checkbox').not(this).prop('checked', this.checked);
});

$('#tableHead').on('click', '#invoiceColId', function (e) {
	var SortHelper = require('./../Utilities/SortHelper.js');
	var sortHelper = new SortHelper();
	listOfTransactions = sortHelper.getSortedListByInvoice(listOfTransactions, 'txnRefNumber');
	displayTransactionList(listOfTransactions);
});

var openTransaction = function openTransaction(txnId, txnType) {
	var ViewTransaction = require('./../BizLogic/ViewTransaction.js');
	var viewTransaction = new ViewTransaction();
	viewTransaction.viewTransactionFile(txnId + ':' + TxnTypeConstant.getTxnType(txnType));
};

if (settingCache.getItemEnabled()) {
	$('#itemDetailPrintDiv').show();
} else {
	$('#itemDetailPrintDiv').hide();
}

var getHTMLTextForReport = function getHTMLTextForReport() {
	var checkedInputList = $('.printCheck:checkbox:checked');
	if (checkedInputList.length) {
		var txnIdlist = [];
		checkedInputList.each(function () {
			txnIdlist.push(this.value);
		});
		var TransactionHTMLGenerator = require('./../ReportHTMLGenerator/TransactionHTMLGenerator.js');
		var transactionHTMLGenerator = new TransactionHTMLGenerator();
		var html = transactionHTMLGenerator.getTransactionListHTML(txnIdlist);
	} else {
		$('#loading').hide();
		ToastHelper.error('Please select transaction.');
	}
	return html;
};

var printTransaction = function printTransaction(that) {
	var settingCache = new SettingCache();
	var ThermalPrinterConstant = require('./../Constants/ThermalPrinterConstant.js');
	if (settingCache.getDefaultPrinterType() == ThermalPrinterConstant.THERMAL_PRINTER) {
		var txnId = that.id.split(':')[0];
		var PrintUtil = require('./../Utilities/PrintUtil.js');
		PrintUtil.printTransactionUsingThermalPrinter(txnId);
	} else {
		var html = transactionPDFHandler.transactionToHTML(that.id);
		if (html) {
			pdfHandler.print(html);
		}
	}
};

$('#currentAccountTransactionDetails').contextmenu({
	delegate: '.currentRow',
	addClass: 'width10',
	menu: [{ title: 'Open PDF', cmd: 'openpdf' }, { title: '----' }, { title: 'Print', cmd: 'print' }, { title: '----' }, { title: 'Delete', cmd: 'delete' }],
	select: function select(event, ui) {
		var id = ui.target[0].parentNode.id;
		if (ui.cmd == 'print') {
			printTransaction(ui.target[0].parentNode);
		} else if (ui.cmd == 'openpdf') {
			openPdfFromRightClickMenu(ui.target[0].parentNode);
		} else if (ui.cmd == 'delete') {
			var DeleteTransaction = require('./../BizLogic/DeleteTransaction.js');
			var deleteTransaction = new DeleteTransaction();
			deleteTransaction.RemoveTransaction(id);
		}
	}
});

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	if (notFromAutoSyncFlow) {
		// $('#frameDiv').load('');
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable();
};

/// ///////////////////////////PDF/////////////////////////////////////

function openPrintOptions() {
	$('#customPrintDialog').removeClass('hide');
	$('#customPrintDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closePrintOptions() {
	$('#customPrintDialog').addClass('hide');
	$('#customPrintDialog').dialog('close').dialog('destroy');
}

var openPdfFromRightClickMenu = function openPdfFromRightClickMenu(that) {
	$('#loading').show(function () {
		var html = transactionPDFHandler.transactionToHTML(that.id);
		if (html) {
			pdfHandler.openPDF(html, false);
		}
	});
};

var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.PRINT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		} else {}
	});
};

populateTable();