var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
	var isMigrationDoneKey = 'isMigrationDone';
	var islastValidTimeMigrationKey = 'isLastValidTimeMigrationDone';
	function migrateWinTdToLocalStorage() {
		var fs = require('fs');
		var CommonUtility = require('../Utilities/CommonUtility');

		var app = require('electron').remote.app;

		var EncryptDecrypt = require('../Utilities/EncryptDecrypt.js');
		var LicenseInfoConstant = require('../Constants/LicenseInfoConstant');
		var logger = require('../Utilities/logger');
		var appPath = app.getPath('userData');
		var winTdPath = appPath + '/wintd.json';

		function getDefaultLicenseValues() {
			var _ref;

			var curDate = new Date();
			var existingDeviceId = localStorage.getItem(LicenseInfoConstant.DEVICE_ID);
			return _ref = {}, (0, _defineProperty3.default)(_ref, LicenseInfoConstant.trialStartDate, EncryptDecrypt.encrypt(curDate.toString())), (0, _defineProperty3.default)(_ref, LicenseInfoConstant.DEVICE_ID, existingDeviceId || EncryptDecrypt.encrypt(CommonUtility.generateNewGUID(12))), (0, _defineProperty3.default)(_ref, LicenseInfoConstant.lastValidTime, curDate.getTime()), (0, _defineProperty3.default)(_ref, LicenseInfoConstant.device_id_validation_done, true), _ref;
		}

		try {
			var isFileExists = fs.existsSync(winTdPath);
			var dataToBeMigrated = {};
			if (!isFileExists) {
				dataToBeMigrated = getDefaultLicenseValues();
			} else {
				var _encryptedLicenseInfo = void 0;var encryptedLicenseInfoJSON = {};
				try {
					_encryptedLicenseInfo = fs.readFileSync(winTdPath, 'utf8');
					encryptedLicenseInfoJSON = JSON.parse(_encryptedLicenseInfo);
					var listOfLicenseKeysEncrytionRequired = LicenseInfoConstant.getLicenseKeyEncrytionRequired();
					if (encryptedLicenseInfoJSON) {
						$.each(encryptedLicenseInfoJSON, function (k, v) {
							try {
								var licenseKey = EncryptDecrypt.decrypt(k);
								if (licenseKey === LicenseInfoConstant.DEVICE_ID) {
									return;
								}
								var licenseValue = listOfLicenseKeysEncrytionRequired.indexOf(licenseKey) >= 0 ? v : EncryptDecrypt.decrypt(v);
								dataToBeMigrated[licenseKey] = licenseValue;
							} catch (err) {
								if (logger !== 'undefined' && logger && logger.error) {
									logger.error('Error occurred in data migration ' + err);
								}
							}
						});
					}
				} catch (err) {
					dataToBeMigrated = getDefaultLicenseValues();
					if (logger !== 'undefined' && logger && logger.error) {
						logger.error('Error occurred in data parsing json(json) ' + _encryptedLicenseInfo);
						logger.error('Error occurred in data parsing json(err) ' + err);
					}
				}
			}
			$.each(dataToBeMigrated, function (k, v) {
				try {
					if (!localStorage.getItem(k)) {
						localStorage.setItem(k, v);
					}
				} catch (err) {}
			});
		} catch (err) {
			if (logger !== 'undefined' && logger && logger.error) {
				logger.error('Error occurred in migrating winTd file (json)-> ' + encryptedLicenseInfo);
				logger.error('Error occurred in migrating winTd file (error)-> ' + err);
			}
		}
		try {
			if (fs.existsSync(winTdPath)) {
				fs.unlink(winTdPath, function (err) {
					if (err) {
						logger.error(err);
					}
				});
			}
		} catch (err) {
			if (logger !== 'undefined' && logger && logger.error) {
				logger.error('Error occurred while deleting wintd file ' + err);
			}
		}
		localStorage.setItem(isMigrationDoneKey, true);
		localStorage.setItem(islastValidTimeMigrationKey, true);
	}

	var isMigrationDone = localStorage.getItem(isMigrationDoneKey);
	var islastValidTimeMigrationDone = localStorage.getItem(islastValidTimeMigrationKey);
	if (!isMigrationDone) {
		migrateWinTdToLocalStorage();
	} else if (isMigrationDone && !islastValidTimeMigrationDone) {
		var LicenseInfoConstant = require('../Constants/LicenseInfoConstant');
		var existingLastValidTime = localStorage.getItem(LicenseInfoConstant.lastValidTime);
		try {
			existingLastValidTime = new Date(existingLastValidTime).getTime();
			if (existingLastValidTime && !isNaN(existingLastValidTime)) {
				localStorage.setItem(LicenseInfoConstant.lastValidTime, existingLastValidTime);
			} else {
				localStorage.setItem(LicenseInfoConstant.lastValidTime, new Date().getTime());
			}
		} catch (ex) {
			localStorage.setItem(LicenseInfoConstant.lastValidTime, new Date().getTime());
		}
		localStorage.setItem(islastValidTimeMigrationKey, true);
	}
})();