var TxnTypeConstant = require('./../Constants/TxnTypeConstant');
var convertToReturn = function convertToReturn(that) {
	var id = that;
	id = id.split(':');
	clickedId = $('#listOfAccounts .itemsWhenClicked').attr('id');
	var txnIdReq = id[0];
	var value = id[1];
	if (TxnTypeConstant[value] == TxnTypeConstant.TXN_TYPE_SALE) {
		value = 'TXN_TYPE_SALE_RETURN';
	} else if (TxnTypeConstant[value] == TxnTypeConstant.TXN_TYPE_PURCHASE) {
		value = 'TXN_TYPE_PURCHASE_RETURN';
	} else {
		ToastHelper.error('not able to convert to return');
		return false;
	}
	var MountComponent = require('../UIComponent/jsx/MountComponent').default;
	var Component = require('../UIComponent/jsx/SalePurchaseContainer').default;
	MountComponent(Component, document.querySelector('#salePurchaseContainer'), {
		txnType: TxnTypeConstant[value],
		txnId: txnIdReq
	});
};

if (module && module.exports) {
	module.exports = convertToReturn;
}