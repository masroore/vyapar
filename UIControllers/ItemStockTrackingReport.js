var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();

var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var ItemCache = require('./../Cache/ItemCache.js');
var itemCache = new ItemCache();
var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');
var printForSharePDF = false;

var listOfTransactions = [];

var listOfTransactionsToBeShown = [];

var populateTable = function populateTable() {
	settingCache = new SettingCache();
	itemCache = new ItemCache();
	var DataLoader = require('./../DBManager/DataLoader.js');
	var dataLoader = new DataLoader();
	var itemNameString = $('#name_ist').val();

	CommonUtility.showLoader(function () {
		listOfTransactions = dataLoader.getItemStockTrackingReportData();
		filterReport();
	}, false);
};

var filterReport = function filterReport() {
	var ItemCache = require('./../Cache/ItemCache.js');
	var itemCache = new ItemCache();
	listOfTransactionsToBeShown = [];

	var itemNameToCompare = $('#name_ist').val().trim().toLowerCase();
	var batchNoToCompare = $('#batchNo_ist').val().trim().toLowerCase();
	var serialNoToCompare = $('#serialNo_ist').val().trim().toLowerCase();
	var sizeToCompare = $('#size_ist').val().trim().toLowerCase();
	var fromMfgDateToCompare = $('#startDateMfg').val();
	var toMfgDateToCompare = $('#endDateMfg').val();
	var fromExpDateToCompare = $('#startDateExp').val();
	var toExpDateToCompare = $('#endDateExp').val();

	if (settingCache.getManufacturingDateFormat() == StringConstant.monthYear) {
		fromMfgDateToCompare = fromMfgDateToCompare ? MyDate.getDateObj('01/' + fromMfgDateToCompare, 'dd/mm/yyyy', '/') : fromMfgDateToCompare;
		if (toMfgDateToCompare) {
			var month = toMfgDateToCompare.split('/')[0];
			var year = toMfgDateToCompare.split('/')[1];
			toMfgDateToCompare = new Date(year, month, 0);
		}
	} else {
		if (fromMfgDateToCompare) {
			fromMfgDateToCompare = MyDate.getDateObj(fromMfgDateToCompare, 'dd/mm/yyyy', '/');
		}
		if (toMfgDateToCompare) {
			toMfgDateToCompare = MyDate.getDateObj(toMfgDateToCompare, 'dd/mm/yyyy', '/');
		}
	}

	if (settingCache.getExpiryDateFormat() == StringConstant.monthYear) {
		fromExpDateToCompare = fromExpDateToCompare ? MyDate.getDateObj('01/' + fromExpDateToCompare, 'dd/mm/yyyy', '/') : fromExpDateToCompare;
		if (toExpDateToCompare) {
			var _month = toExpDateToCompare.split('/')[0];
			var _year = toExpDateToCompare.split('/')[1];
			toExpDateToCompare = new Date(_year, _month, 0);
		}
	} else {
		if (fromExpDateToCompare) {
			fromExpDateToCompare = MyDate.getDateObj(fromExpDateToCompare, 'dd/mm/yyyy', '/');
		}
		if (toExpDateToCompare) {
			toExpDateToCompare = MyDate.getDateObj(toExpDateToCompare, 'dd/mm/yyyy', '/');
		}
	}

	var showOnlyStockItem = $('#showOnlyStockItem').prop('checked');
	var showInactiveItems = $('#showInactiveItems').is(':checked');
	for (var i = 0; i < listOfTransactions.length; i++) {
		var txn = listOfTransactions[i];
		var itemId = txn.getIstItemId();
		var itemobj = itemCache.getItemById(itemId);
		if (!showInactiveItems && itemobj.getIsItemActive() != 1) continue;
		var itemName = itemobj.getItemName();
		var batchNo = txn.getIstBatchNumber();
		var serialNo = txn.getIstSerialNumber();
		var size = txn.getIstSize();
		var mfgDate = txn.getIstManufacturingDate();
		var expDate = txn.getIstExpiryDate();

		var nameFilter = itemNameToCompare ? itemName && itemName.toLowerCase().indexOf(itemNameToCompare) >= 0 : true;
		var batchNoFilter = batchNoToCompare ? batchNo && batchNo.toLowerCase().indexOf(batchNoToCompare) >= 0 : true;
		var serialNoFilter = serialNoToCompare ? serialNo.toLowerCase().indexOf(serialNoToCompare) >= 0 : true;
		var sizeFilter = sizeToCompare ? size && size.toLowerCase().indexOf(sizeToCompare) >= 0 : true;
		var fromMfgDateFilter = fromMfgDateToCompare ? mfgDate && mfgDate >= fromMfgDateToCompare : true;
		var toMfgDateFilter = toMfgDateToCompare ? mfgDate && mfgDate <= toMfgDateToCompare : true;
		var fromExpDateFilter = fromExpDateToCompare ? expDate && expDate >= fromExpDateToCompare : true;
		var toExpDateFilter = toExpDateToCompare ? expDate && expDate <= toExpDateToCompare : true;
		var stockQuantityFilter = showOnlyStockItem ? MyDouble.convertStringToDouble(txn.getIstCurrentQuantity(), true) > 0 : true;

		if (nameFilter && batchNoFilter && serialNoFilter && sizeFilter && fromMfgDateFilter && toMfgDateFilter && fromExpDateFilter && toExpDateFilter && stockQuantityFilter) {
			listOfTransactionsToBeShown.push(txn);
		}
	}

	CommonUtility.showLoader(function () {
		displayTransactionList(listOfTransactionsToBeShown);
	});
};

if (settingCache.getManufacturingDateFormat() == StringConstant.monthYear) {
	$('#startDateMfg, #endDateMfg ').monthpicker({ dateFormat: 'mm/yy' });
} else {
	$('#startDateMfg, #endDateMfg ').datepicker({ dateFormat: 'dd/mm/yy' });
}

if (settingCache.getExpiryDateFormat() == StringConstant.monthYear) {
	$('#startDateExp, #endDateExp ').monthpicker({ dateFormat: 'mm/yy' });
} else {
	$('#startDateExp, #endDateExp ').datepicker({ dateFormat: 'dd/mm/yy' });
}
$('#showInactiveItems').on('change', function () {
	populateTable();
	$('#name_ist').autocomplete(UIHelper.getAutocompleteDefaultOptions(itemCache.getListOfItems($('#showInactiveItems').is(':checked')), '', 100));
});

$(function () {
	$('#name_ist').autocomplete(UIHelper.getAutocompleteDefaultOptions(itemCache.getListOfItems($('#showInactiveItems').is(':checked')), '', 100));
});

if (itemCache.IsAnyItemInactive()) {
	$('#activeInactiveItems').removeClass('hide');
} else {
	$('#activeInactiveItems').addClass('hide');
}

$('#batchNo_ist').on('change', function (event) {
	filterReport();
});

$('#name_ist').on('change', function (event) {
	filterReport();
});

$('#serialNo_ist').on('change', function (event) {
	filterReport();
});

$('#size_ist').on('change', function (event) {
	filterReport();
});

$('#startDateMfg').on('change', function (event) {
	filterReport();
});

$('#endDateMfg').on('change', function (event) {
	filterReport();
});

$('#startDateExp').on('change', function (event) {
	filterReport();
});

$('#endDateExp').on('change', function (event) {
	filterReport();
});

$('#showOnlyStockItem').change(function () {
	filterReport();
});

if (settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_BATCH_NUMBER)) {
	$('#batchNoFilter').removeClass('hide');
	$('#batchNoLabel').html(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_BATCH_NUMBER_VALUE));
}

if (settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_SERIAL_ITEM_NUMBER)) {
	$('#serialNoFilter').removeClass('hide');
	$('#serialNoLabel').html(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_SERIAL_NUMBER_VALUE));
}

if (settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_SIZE)) {
	$('#sizeFilter').removeClass('hide');
	$('#sizeLabel').html(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_SIZE_VALUE));
}

if (settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_MANUFACTURING_DATE)) {
	$('#mfgDateLabel').removeClass('hide');
	$('#mfgDateLabel').html(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_MANUFACTURING_DATE_VALUE));
	$('#mfgDateField').removeClass('hide');
}

if (settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_EXPIRY_DATE)) {
	$('#expDateLabel').removeClass('hide');
	$('#expDateLabel').html(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_EXPIRY_DATE_VALUE));
	$('#expDateField').removeClass('hide');
}

var itemStockTrackingClusterize;

var sortISTData = function sortISTData(listOfTransactions) {
	if (listOfTransactions) {
		listOfTransactions.sort(function (lhs, rhs) {
			var item1 = itemCache.findItemNameById(lhs.getIstItemId()).toLowerCase();
			var item2 = itemCache.findItemNameById(rhs.getIstItemId()).toLowerCase();
			if (item2 < item1) {
				return 1;
			} else if (item2 > item1) {
				return -1;
			} else {
				return MyDouble.convertStringToDouble(rhs.getIstCurrentQuantity(), true) - MyDouble.convertStringToDouble(lhs.getIstCurrentQuantity(), true);
			}
		});
	}
};

var displayTransactionList = function displayTransactionList(listOfTransactions) {
	var len = listOfTransactions.length;
	sortISTData(listOfTransactions);
	var rowData = [];
	var dynamicRow = '';
	var mrpColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_MRP);
	var batchNumberColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_BATCH_NUMBER);
	var serialNumberColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_SERIAL_ITEM_NUMBER);
	var mfgDateColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_MANUFACTURING_DATE);
	var expDateColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_EXPIRY_DATE);
	var sizeColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_SIZE);

	dynamicRow += "<thead><tr><th width='12%' class='tableCellTextAlignLeft'>Item Name</th>";
	if (mrpColumnVisibility) {
		dynamicRow += "<th width='11%' class='tableCellTextAlignRight'> " + settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_MRP_VALUE) + ' </th>';
	}
	if (batchNumberColumnVisibility) {
		dynamicRow += "<th width='11%' class='tableCellTextAlignRight'> " + settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_BATCH_NUMBER_VALUE) + ' </th>';
	}
	if (serialNumberColumnVisibility) {
		dynamicRow += "<th width='11%' class='tableCellTextAlignRight'> " + settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_SERIAL_NUMBER_VALUE) + ' </th>';
	}
	if (mfgDateColumnVisibility) {
		dynamicRow += "<th width='11%' class='tableCellTextAlignRight'> " + settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_MANUFACTURING_DATE_VALUE) + ' </th>';
	}
	if (expDateColumnVisibility) {
		dynamicRow += "<th width='11%' class='tableCellTextAlignRight'> " + settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_EXPIRY_DATE_VALUE) + ' </th>';
	}
	if (sizeColumnVisibility) {
		dynamicRow += "<th width='11%' class='tableCellTextAlignRight'> " + settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_SIZE_VALUE) + ' </th>';
	}

	dynamicRow += "<th width='11%' class='tableCellTextAlignRight'>Current Quantity</th></tr></thead>";
	//
	var totalSaleQuantity = 0;
	var totalPurchaseQuantity = 0;
	for (var i = 0; i < len; i++) {
		var row = '';
		var txn = listOfTransactions[i];
		var batchNo = txn.getIstBatchNumber();
		var serialNo = txn.getIstSerialNumber();
		var mrp = txn.getIstMrp();
		var expDate = '';
		var mfgDate = '';
		var size = txn.getIstSize();
		var currentQuantity = txn.getIstCurrentQuantity();
		var itemId = txn.getIstItemId();
		var itemName = itemCache.findItemNameById(itemId);

		if (txn.getIstExpiryDate() && settingCache.getExpiryDateFormat() == StringConstant.monthYear) {
			expDate = MyDate.getDate('m/y', txn.getIstExpiryDate());
		} else if (txn.getIstExpiryDate() && settingCache.getExpiryDateFormat() == StringConstant.dateMonthYear) {
			expDate = MyDate.getDate('d/m/y', txn.getIstExpiryDate());
		}

		if (txn.getIstManufacturingDate() && settingCache.getManufacturingDateFormat() == StringConstant.monthYear) {
			mfgDate = MyDate.getDate('m/y', txn.getIstManufacturingDate());
		} else if (txn.getIstManufacturingDate() && settingCache.getManufacturingDateFormat() == StringConstant.dateMonthYear) {
			mfgDate = MyDate.getDate('d/m/y', txn.getIstManufacturingDate());
		}

		row += "<tr class='currentRow'><td class='tableCellTextAlignLeft' width='12%'> " + itemName + ' </td>';
		if (mrpColumnVisibility) {
			row += "<td class='tableCellTextAlignRight' width='11%'> " + MyDouble.getAmountWithDecimal(mrp) + ' </td>';
		}
		if (batchNumberColumnVisibility) {
			row += "<td class='tableCellTextAlignRight' width='11%'> " + batchNo + ' </td>';
		}
		if (serialNumberColumnVisibility) {
			row += "<td class='tableCellTextAlignRight' width='11%'> " + serialNo + ' </td>';
		}
		if (mfgDateColumnVisibility) {
			row += "<td class='tableCellTextAlignRight' width='11%'> " + mfgDate + ' </td>';
		}
		if (expDateColumnVisibility) {
			row += "<td class='tableCellTextAlignRight' width='11%'> " + expDate + ' </td>';
		}
		if (sizeColumnVisibility) {
			row += "<td class='tableCellTextAlignRight' width='11%'> " + size + ' </td>';
		}
		row += "<td class='tableCellTextAlignRight' width='11%'>" + MyDouble.getQuantityWithDecimalWithoutColor(currentQuantity) + '</td></tr>';

		rowData.push(row);
	}
	var data = rowData;

	if ($('#itemStockTrackingReportContainer').length === 0) {
		return;
	}
	if (itemStockTrackingClusterize && itemStockTrackingClusterize.destroy) {
		itemStockTrackingClusterize.clear();
		itemStockTrackingClusterize.destroy();
	}

	itemStockTrackingClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollArea',
		contentId: 'contentArea',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});

	$('#tableHead').html(dynamicRow);
};

var openTransaction = function openTransaction(txnId, txnType) {
	var ViewTransaction = require('./../BizLogic/ViewTransaction.js');
	var viewTransaction = new ViewTransaction();
	viewTransaction.viewTransactionFile(txnId + ':' + TxnTypeConstant.getTxnType(txnType));
};

if (settingCache.getItemEnabled()) {
	$('#itemDetailPrintDiv').show();
} else {
	$('#itemDetailPrintDiv').hide();
}

function openPrintOptions(sharePDF) {
	printForSharePDF = sharePDF;
	printReport();
}

function closePrintOptions() {
	$('#customPrintDialog').addClass('hide');
	$('#customPrintDialog').dialog('close').dialog('destroy');
}

function printReport() {
	if (printForSharePDF) {
		pdfHandler.sharePdf(getHTMLTextForReport(), 'Report', false);
	} else {
		pdfHandler.printPdf(getHTMLTextForReport(), 'Report', false);
	}
	// closePrintOptions();
}

var getHTMLTextForReport = function getHTMLTextForReport() {
	var ItemStockTrackingReportHTMLGenerator = require('./../ReportHTMLGenerator/ItemStockTrackingReportHTMLGenerator.js');
	var itemStockTrackingReportHTMLGenerator = new ItemStockTrackingReportHTMLGenerator();
	var htmlText = itemStockTrackingReportHTMLGenerator.getHTMLText(listOfTransactionsToBeShown.length ? listOfTransactionsToBeShown : listOfTransactions);
	return htmlText;
};

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	// $('#frameDiv').load('');
	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable();
};

/// ///////////////////////////PDF/////////////////////////////////////
var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.ITEM_STOCK_TRACKING_REPORT, fromDate: $('#startDateMfg').val(), toDate: $('#endDateMfg').val() });
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

if (settingCache.getItemEnabled()) {
	$('#itemDetailsExcel').show();
} else {
	$('#itemDetailsExcel').hide();
}

function openExcelOptions() {
	$('#ExcelDialog').removeClass('hide');
	$('#ExcelDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closeExcelOptions() {
	$('#ExcelDialog').addClass('hide');
	$('#ExcelDialog').dialog('close').dialog('destroy');
}

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.ITEM_STOCK_TRACKING_REPORT, fromDate: $('#startDateMfg').val(), toDate: $('#endDateMfg').val() });
	excelHelper.saveExcel(fileName);
};

var prepareObjectForExcel = function prepareObjectForExcel(list) {
	var len = list.length;
	var rowObj = [];
	var tableHeadArray = [];
	var totalArray = [];
	tableHeadArray.push('Item Name');

	var mrpColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_MRP);
	var batchNumberColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_BATCH_NUMBER);
	var serialNumberColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_SERIAL_ITEM_NUMBER);
	var mfgDateColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_MANUFACTURING_DATE);
	var expDateColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_EXPIRY_DATE);
	var sizeColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_SIZE);

	if (mrpColumnVisibility) {
		tableHeadArray.push(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_MRP_VALUE));
	}
	if (batchNumberColumnVisibility) {
		tableHeadArray.push(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_BATCH_NUMBER_VALUE));
	}
	if (serialNumberColumnVisibility) {
		tableHeadArray.push(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_SERIAL_NUMBER_VALUE));
	}
	if (mfgDateColumnVisibility) {
		tableHeadArray.push(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_MANUFACTURING_DATE_VALUE));
	}
	if (expDateColumnVisibility) {
		tableHeadArray.push(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_EXPIRY_DATE_VALUE));
	}
	if (sizeColumnVisibility) {
		tableHeadArray.push(settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_SIZE_VALUE));
	}

	tableHeadArray.push('Current Quantity');

	rowObj.push(tableHeadArray);
	rowObj.push([]);

	for (var i = 0; i < len; i++) {
		var tempArray = [];
		var txn = list[i];
		var batchNo = txn.getIstBatchNumber();
		var serialNo = txn.getIstSerialNumber();
		var mrp = txn.getIstMrp();
		var expDate = '';
		var mfgDate = '';
		var size = txn.getIstSize();
		var currentQuantity = txn.getIstCurrentQuantity();
		var itemId = txn.getIstItemId();
		var itemName = itemCache.findItemNameById(itemId);

		if (txn.getIstExpiryDate() && settingCache.getExpiryDateFormat() == StringConstant.monthYear) {
			expDate = MyDate.getDate('m/y', txn.getIstExpiryDate());
		} else if (txn.getIstExpiryDate() && settingCache.getExpiryDateFormat() == StringConstant.dateMonthYear) {
			expDate = MyDate.getDate('d/m/y', txn.getIstExpiryDate());
		}

		if (txn.getIstManufacturingDate() && settingCache.getManufacturingDateFormat() == StringConstant.monthYear) {
			mfgDate = MyDate.getDate('m/y', txn.getIstManufacturingDate());
		} else if (txn.getIstManufacturingDate() && settingCache.getManufacturingDateFormat() == StringConstant.dateMonthYear) {
			mfgDate = MyDate.getDate('d/m/y', txn.getIstManufacturingDate());
		}

		tempArray.push(itemName);

		if (mrpColumnVisibility) {
			tempArray.push(MyDouble.getAmountWithDecimal(mrp));
		}
		if (batchNumberColumnVisibility) {
			tempArray.push(batchNo);
		}
		if (serialNumberColumnVisibility) {
			tempArray.push(serialNo);
		}
		if (mfgDateColumnVisibility) {
			tempArray.push(mfgDate);
		}
		if (expDateColumnVisibility) {
			tempArray.push(expDate);
		}
		if (sizeColumnVisibility) {
			tempArray.push(size);
		}

		tempArray.push(MyDouble.getQuantityWithDecimalWithoutColor(currentQuantity));
		rowObj.push(tempArray);
	}
	return rowObj;
};

function printExcel() {
	var XLSX = require('xlsx');
	var dataArray = prepareObjectForExcel(listOfTransactionsToBeShown.length ? listOfTransactionsToBeShown : listOfTransactions);
	var worksheet = XLSX.utils.aoa_to_sheet(dataArray);

	var workbook = {
		'SheetNames': [],
		'Sheets': {}
	};

	var wsName = 'Item Stock Tracking Report';
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet;
	var wscolWidth = [{ wch: 40 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	worksheet['!cols'] = wscolWidth;
	console.log(workbook);
	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

setTimeout(populateTable, 200);