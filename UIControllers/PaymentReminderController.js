var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _throttleDebounce = require('throttle-debounce');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function ($) {
	var DataLoader = require('./../DBManager/DataLoader.js');
	var logger = require('../Utilities/logger');
	var SettingCache = require('../Cache/SettingCache');
	var settingCache = new SettingCache();
	var isCountryIndia = settingCache.isCurrentCountryIndia();
	var dataLoader = new DataLoader();
	var MyDouble = require('./../Utilities/MyDouble');
	var paymentReminderClusterize = void 0;
	var search = void 0;
	var partyReminderController = {
		defaults: {
			$mountPoint: '#frameDiv',
			$el: '',
			partyList: [],
			currentList: []
			// fired : false
		},
		init: function init(options) {
			this.defaults = $.extend({}, this.defaults, options || {});
			this.defaults.$mountPoint = $(this.defaults.$mountPoint);
			this.defaults.partyList = dataLoader.getPaymentReminderList();
			this.defaults.currentList = this.defaults.partyList;
			this.render();
			this.defaults.$el = this.defaults.$mountPoint.find('#paymentReminderMainDiv');
			this.searchPaymentReminderText = this.searchPaymentReminderText.bind(this);
			this.attachEvents();
			search = (0, _throttleDebounce.debounce)(100, this.searchPaymentReminderText);
		},
		sendSMSToMultipleParty: function sendSMSToMultipleParty() {
			var nameIdList = [];
			nameIdList = this.defaults.partyList.filter(function (party) {
				return party.checked;
			}).map(function (party) {
				return party.getNameId();
			});
			if (!nameIdList.length) {
				ToastHelper.error('Please select parties to send bulk sms reminder');
			}
			this.sendPaymentReminderSms(nameIdList);
		},
		searchPaymentReminderText: function searchPaymentReminderText() {
			var input = document.getElementById('searchPaymentReminderText');
			var filter = input.value.toUpperCase();
			this.defaults.currentList = this.defaults.partyList.filter(function (partyReminderObject) {
				return partyReminderObject.getName().toUpperCase().includes(filter);
			});
			var contentArray = this.getPaymentReminderHTMLContent(this.defaults.currentList);
			paymentReminderClusterize && paymentReminderClusterize.update(contentArray);
			$('#loading').hide();
		},
		selectAll: function selectAll(event) {
			var _this = this;

			$('#loading').show(function () {
				var isChecked = event.currentTarget.checked;
				_this.defaults.currentList.forEach(function (partyReminderObject) {
					partyReminderObject.checked = isChecked;
				});
				var contentArray = _this.getPaymentReminderHTMLContent(_this.defaults.currentList);
				paymentReminderClusterize && paymentReminderClusterize.update(contentArray);
				$('#loading').hide();
			});
		},
		initialiseSearch: function initialiseSearch() {
			$('#loading').show(function () {
				search();
				$('#selectAll').prop('checked', false);
			});
		},
		sortRows: function sortRows(event) {
			var _this2 = this;

			$('#loading').show(function () {
				try {
					var nextSortOrder = {
						unsorted: 'asc',
						asc: 'desc',
						desc: 'unsorted'
					};
					var sortBy = event.currentTarget.dataset.sortby;
					var sortOrder = event.currentTarget.dataset.sortorder;
					sortOrder = nextSortOrder[sortOrder] || 'unsorted';
					var sortedArray = [].concat((0, _toConsumableArray3.default)(_this2.defaults.currentList));
					if (sortOrder !== 'unsorted') {
						sortedArray.sort(function (paymentReminderObj1, paymentReminderObj2) {
							var comp1 = sortBy == 'name' ? paymentReminderObj1.getName().toUpperCase() : Number(paymentReminderObj1.getBalanceAmount());
							var comp2 = sortBy == 'name' ? paymentReminderObj2.getName().toUpperCase() : Number(paymentReminderObj2.getBalanceAmount());
							if (comp1 == comp2) {
								return 0;
							} else if (comp1 < comp2) {
								return sortOrder == 'asc' ? -1 : 1;
							} else {
								return sortOrder == 'desc' ? -1 : 1;
							}
						});
					}
					var contentArray = _this2.getPaymentReminderHTMLContent(sortedArray);
					paymentReminderClusterize && paymentReminderClusterize.update(contentArray);
					// Reset and apply latest sortOrder styles
					var rowHeaders = document.querySelectorAll('.paymentReminderPartyNamesHeading sortRows');
					var _iteratorNormalCompletion = true;
					var _didIteratorError = false;
					var _iteratorError = undefined;

					try {
						for (var _iterator = (0, _getIterator3.default)(rowHeaders), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
							var row = _step.value;

							row.dataset.sortorder = 'unsorted';
						}
					} catch (err) {
						_didIteratorError = true;
						_iteratorError = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion && _iterator.return) {
								_iterator.return();
							}
						} finally {
							if (_didIteratorError) {
								throw _iteratorError;
							}
						}
					}

					event.currentTarget.dataset.sortorder = sortOrder;
				} catch (err) {
					logger.error(err);
					$('#loading').hide();
				}
				$('#loading').hide();
			});
		},
		attachEvents: function attachEvents() {
			this.defaults.$el.on('click', '#sendSMSToMultipleParty', this.sendSMSToMultipleParty.bind(this));
			this.defaults.$el.on('keyup', '#searchPaymentReminderText', this.initialiseSearch.bind(this));
			this.defaults.$el.on('click', '#selectAll', this.selectAll.bind(this));
			this.defaults.$el.on('click', '.sortRows', this.sortRows.bind(this));
			this.defaults.$el.on('click', '#contentAreaPaymentReminder', this.handleClickEvents.bind(this));
			$('#close').on('click', function removeListeners() {
				if (paymentReminderClusterize && paymentReminderClusterize.destroy) {
					paymentReminderClusterize.clear();
					paymentReminderClusterize.destroy();
				}
				$('#close').off('click', '', removeListeners);
			});
		},
		handleClickEvents: function handleClickEvents(event) {
			var target = event.target;
			var targetParent = target.parentNode;
			var targetClassList = target.classList;
			var parentClassList = targetParent.classList;
			if (parentClassList && parentClassList.contains('paymentReminderSendSMS')) {
				var nameId = $(targetParent).data('value');
				this.sendPaymentReminderSms([nameId]);
			} else if (parentClassList && parentClassList.contains('paymentReminderSendWhatsApp')) {
				var _nameId = $(targetParent).data('value');
				this.sendWhatsAppMessage(_nameId);
			} else if (targetClassList.contains('paymentReminderCheckbox')) {
				var _nameId2 = $(target).data('value');
				var partyObj = this.defaults.partyList.find(function (party) {
					return party.getNameId() == _nameId2;
				});
				var isChecked = $(target).prop('checked');
				partyObj && (partyObj.checked = isChecked);
				if (!isChecked) {
					$('#selectAll').prop('checked', false);
				}
			}
		},
		sendWhatsAppMessage: function sendWhatsAppMessage(nameId) {
			var MessageDraftLogic = require('../BizLogic/MessageDraftLogic');
			var messageDraftLogic = new MessageDraftLogic();
			var NameCache = require('./../Cache/NameCache.js');
			var nameCache = new NameCache();
			var partyObject = nameCache.findNameObjectByNameId(nameId);
			messageDraftLogic.sendPaymentReminderMessage(partyObject, 'whatsapp', true, true);
		},
		sendPaymentReminderSms: function sendPaymentReminderSms(nameIdList) {
			var MessageDraftLogic = require('../BizLogic/MessageDraftLogic');
			var messageDraftLogic = new MessageDraftLogic();
			var NameCache = require('./../Cache/NameCache.js');
			var nameCache = new NameCache();
			var partyObjects = nameIdList.map(function (nameId) {
				return nameCache.findNameObjectByNameId(nameId);
			});
			if (partyObjects.length == 1) {
				messageDraftLogic.sendPaymentReminderMessage(partyObjects[0], 'sms', true, true);
			} else {
				var isOnline = require('is-online');
				isOnline({ timeout: 2000 }).then(function (online) {
					if (online) {
						messageDraftLogic.sendBulkSMS(partyObjects, { bulkMessageType: 'paymentReminder' });
					} else {
						ToastHelper.error('Please check internet connection.');
					}
				});
			}
		},
		render: function render() {
			this.defaults.$mountPoint.html(this.getMainTemplate());
			if (paymentReminderClusterize && paymentReminderClusterize.destroy) {
				paymentReminderClusterize.clear();
				paymentReminderClusterize.destroy();
			}
			var data = this.getPaymentReminderHTMLContent(this.defaults.partyList);
			paymentReminderClusterize = new Clusterize({
				rows: data,
				scrollId: 'scrollAreaPaymentReminder',
				contentId: 'contentAreaPaymentReminder'
			});
		},
		getMainTemplate: function getMainTemplate() {
			return '\n\t\t\t<div id="paymentReminderMainDiv">\n\t\t\t\t<div class="paymentReminderHeading">\n\t\t\t\t\t<span>Payment Reminder</span>\n\t\t\t\t</div>\n\t\t\t\t<div class="paymentReminderBody">\t\t\n\t\t\t\t\t<div class="paymentReminderSearchBox">\n\t\t\t\t\t\t<div class="paymentReminderSearchBoxInput">\n\t\t\t\t\t\t\t<input type="text" id="searchPaymentReminderText" placeholder="Search party">\n\t\t\t\t\t\t\t<div class="svgIconLeft">\n\t\t\t\t\t\t\t\t<img src="./../inlineSVG/searchIcon.svg">\n\t\t\t\t\t\t\t</div>\n                    \t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<table class="paymentReminderTable paymentReminderContent">\n\t\t\t\t\t\t<thead>\n\t\t\t\t\t\t\t<tr class="paymentReminderPartyNamesHeading">\n\t\t\t\t\t\t\t\t<th style="width:3rem">\n\t\t\t\t\t\t\t\t\t' + (isCountryIndia ? '\n\t\t\t\t\t\t\t\t\t<label for=\'selectAll\' class=\'input-checkbox floatRight\'>\n\t\t\t\t\t\t\t\t\t\t<input type=\'checkbox\' id=\'selectAll\'>\n\t\t\t\t\t\t\t\t\t\t<svg width=\'18\' height=\'18\'><path class=\'checked\'  d=\'M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M7,14L2,9l1.4-1.4L7,11.2l7.6-7.6L16,5L7,14z\'/><path class=\'indeterminate\' d=\'M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M14,10H4V8h10V10z\'/><path class=\'unchecked\' fill=\'#097AA8\' d=\'M16,2v14H2V2H16 M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z\'/></svg>\n\t\t\t\t\t\t\t\t\t</label>' : '') + '\n\t\t\t\t\t\t\t\t</th>\n\t\t\t\t\t\t\t\t<th class="width30 partyNameCol">\n\t\t\t\t\t\t\t\t\t<div class="sortRows" data-sortBy="name" data-sortOrder="unsorted">\n\t\t\t\t\t\t\t\t\t\t<span>Party</span>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</th>\n\t\t\t\t\t\t\t\t<th class="width30 partyAmountCol">\n\t\t\t\t\t\t\t\t\t<div class = "sortRows" data-sortBy="amount" data-sortOrder="unsorted">\n\t\t\t\t\t\t\t\t\t\t<span>Amount</span>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</th>\n\t\t\t\t\t\t\t\t<th class="width15">SMS Reminder</th>\n\t\t\t\t\t\t\t\t<th class="width15">WhatsApp Reminder</th>\n\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t</thead>\n\t\t\t\t\t\t</table>\n\t\t\t\t\t\t<div class="clusterize-scroll" id="scrollAreaPaymentReminder">\n\t\t\t\t\t\t\t<table class="paymentReminderTable">\n\t\t\t\t\t\t\t\t<thead>\n\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t<th style="width:3rem"/>\n\t\t\t\t\t\t\t\t\t\t<th class="width30"/>\n\t\t\t\t\t\t\t\t\t\t<th class="width30"/>\n\t\t\t\t\t\t\t\t\t\t<th class="width15"/>\n\t\t\t\t\t\t\t\t\t\t<th class="width15"/>\n\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t</thead>\n\t\t\t\t\t\t\t\t<tbody class="paymentReminderPartyNamesBody clusterize-content" id="contentAreaPaymentReminder">\n\t\t\t\t\t\t\t\t</tbody>\n\t\t\t\t\t\t\t</table>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t<div class="paymentReminderPartyNamesFooter">\n\t\t\t\t\t\t' + (isCountryIndia ? '\n\t\t\t\t\t\t<button id="sendSMSToMultipleParty" class="sendSMSToMultipleParty paymentReminderButton floatLeft">\n\t\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\t\t<img src="./../inlineSVG/sendReminderSMS.svg">\n\t\t\t\t\t\t\t\t<span>Send Bulk SMS</span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</button>' : '') + '\n\t\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t\t  ';
		},
		getPaymentReminderHTMLContent: function getPaymentReminderHTMLContent(paymentReminderList) {
			var contentArray = [];
			for (var x = 0; x < paymentReminderList.length; x++) {
				var paymentReminderParty = paymentReminderList[x];
				var thisString = '\n\t\t\t\t<tr class="paymentReminderRow" data-name="' + paymentReminderParty.getName().toUpperCase() + '">\n\t\t\t\t\t' + (isCountryIndia ? '\n\t\t\t\t\t<td>\t\t\t\t\t\t\n\t\t\t\t\t\t<label class=\'input-checkbox floatRight\'>\n\t\t\t\t\t\t\t<input class=\'paymentReminderCheckbox\' type=\'checkbox\' ' + (paymentReminderParty.checked ? 'checked' : '') + ' data-value=' + paymentReminderParty.getNameId() + '>\n\t\t\t\t\t\t\t<svg width=\'18\' height=\'18\'><path class=\'checked\'  d=\'M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M7,14L2,9l1.4-1.4L7,11.2l7.6-7.6L16,5L7,14z\'/><path class=\'indeterminate\' d=\'M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M14,10H4V8h10V10z\'/><path class=\'unchecked\' fill=\'#097AA8\' d=\'M16,2v14H2V2H16 M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z\'/></svg>\n\t\t\t\t\t\t</label>\n\t\t\t\t\t</td>' : '<td></td>') + '\n\t\t\t\t\t<td class="partyNameCol">' + paymentReminderParty.getName() + '\n\t\t\t\t\t</td>\n\t\t\t\t\t<td class="partyAmountCol">\n\t\t\t\t\t' + MyDouble.getAmountWithDecimalAndCurrencyWithSign(paymentReminderParty.getBalanceAmount()) + ('\n\t\t\t\t\t</td>\n\t\t\t\t\t' + (isCountryIndia ? '\n\t\t\t\t\t<td class = "center paymentReminderSendSMS" data-value = ' + paymentReminderParty.getNameId() + '>\n\t\t\t\t\t\t<img src="./../inlineSVG/sendSMS.svg">\n\t\t\t\t\t</td>' : '<td></td>') + '\n\t\t\t\t\t<td class = "center paymentReminderSendWhatsApp" data-value = ') + paymentReminderParty.getNameId() + '>\n\t\t\t\t\t\t<img src="./../inlineSVG/sendReminderWhatsApp.svg">\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t\t';
				contentArray.push(thisString);
			}
			return contentArray;
		}
	};
	return partyReminderController.init.bind(partyReminderController);
}(jQuery); /* global jQuery */