var DateFormat = require('./../Constants/DateFormat.js');
var ErrorCode = require('./../Constants/ErrorCode.js');
var SettingCache = require('./../Cache/SettingCache.js');
var DataLoader = require('./../DBManager/DataLoader.js');
$('#closeDate').datepicker({ dateFormat: DateFormat.format });
var date = '31/03/2020';
$('#closeDate').val(date);

var CloseBookBiz = require('./../BizLogic/CloseBookBiz.js');
var closeBookBiz;

$('.closeBookStepStatusImage').hide();
$('.closeBookloading').hide();

$('#submitButtonCloseBook').off('click').on('click', closeBook);

function closeBook() {
	$('#submitButtonCloseBook').attr('disabled', true);
	var status = false;
	var closeBookDateString = $('#closeDate').val();
	var closeBookDate = MyDate.getDateObj(closeBookDateString, 'dd/MM/yyyy', '/');
	closeBookBiz = new CloseBookBiz(closeBookDate);
	if (closeBookDate) {
		status = closeBookBiz.canCloseBook(closeBookDate);
		if (status) {
			var VerifyMyData = require('./../Utilities/VerifyMyData.js');
			var verifyData = new VerifyMyData();
			verifyData.verifyData();
			status = verifyData.fixDataForCloseBook();
			if (status) {
				MyAnalytics.pushEvent('Close Financial Year Start Closing');
				takeBackupBeforeBookClose();
			} else {
				ToastHelper.error('Some error occurred, please contact vyapar customer support');
				$('#submitButtonCloseBook').attr('disabled', false);
			}
		} else {
			ToastHelper.error('Cannot close book as there are some opening cashin and opening cashout transactions after closing date');
			$('#submitButtonCloseBook').attr('disabled', false);
		}
	} else {
		showCloseBookErrorMessage('Select a valid date.');
		$('#submitButtonCloseBook').attr('disabled', false);
	}
}

function closeBookMigrateOnlyPrefix() {
	var settingCache = new SettingCache();
	if (settingCache.isEstimateEnabled()) {
		$('#EstimateRow').removeClass('hide');
	}
	if (settingCache.isDeliveryChallanEnabled()) {
		$('#DeliveryChallanRow').removeClass('hide');
	}
	if (settingCache.isOrderFormEnabled()) {
		$('#SaleOrderRow').removeClass('hide');
		$('#PurchaseOrderRow').removeClass('hide');
	}
	MyAnalytics.pushEvent('Close Financial Year Start Closing');
	var closeBookDateString = $('#closeDate').val();
	var closeBookDate = MyDate.getDateObj(closeBookDateString, 'dd/MM/yyyy', '/');
	if (closeBookDate) {
		closeBookBiz = new CloseBookBiz(closeBookDate);
		$('#closeBookPrefixDiv').addClass('classForPrefixCloseBook');
		$('#closeBookDefaultDiv').addClass('disableClassCloseBooks');
		$('#submitButtonCloseBook').attr('disabled', true);
		$('#submitButtonCloseBookPrefix').css('display', 'none');
		$('#prefixUpdateView').css('display', 'block');
	} else {
		showCloseBookErrorMessage('Select a valid date.');
	}
}

function showCloseBookErrorMessage(message) {
	var enableCloseBookButton = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

	$('#customAlertDialog #customAlertDialogBody').text(message);
	$('#customAlertDialog').show().dialog({
		'title': 'Close Book Error',
		'closeX': true,
		'closeOnEscape': true,
		closeText: 'hide',
		modal: true,
		width: 500,
		open: function open(event, ui) {
			$('.ui-dialog-titlebar-close').hide();
		}
	});
	$('#customAlertDialog #confirm').text('Ok').show();
	$('#customAlertDialog #cancel').hide();
	$('#customAlertDialog button').off('click').on('click', function () {
		$('#customAlertDialog').dialog('close');
		$('#customAlertDialog').dialog('destroy').hide();
	});
	if (enableCloseBookButton) {
		$('#closeBookButton').prop('disabled', false);
	}
}

function condensePartyProcess() {
	setTimeout(function () {
		var success = false;
		try {
			success = closeBookBiz.condenseParties();
		} catch (err) {
			success = false;
		}
		if (success) {
			$('#partyLoader').addClass('hide');
			$('#partyCheck').removeClass('hide');
			condenseItemProcess();
		} else {
			stopCondensingProcess(ErrorCode.ERROR_CLOSE_BOOK_PARTY_ERROR);
		}
	}, 0);
}

function condensePrefixFlow(prefixObj) {
	return closeBookBiz.condensePrefixFlow(prefixObj);
}

function condenseItemProcess() {
	setTimeout(function () {
		var success = false;
		try {
			success = closeBookBiz.condenseItems();
		} catch (err) {
			success = false;
		}
		if (success) {
			$('#itemLoader').addClass('hide');
			$('#itemCheck').removeClass('hide');
			condenseBankProcess();
		} else {
			stopCondensingProcess(ErrorCode.ERROR_CLOSE_BOOK_ITEM_ERROR);
		}
	}, 0);
}

function condenseBankProcess() {
	setTimeout(function () {
		var success = false;
		try {
			success = closeBookBiz.condenseBanks();
		} catch (err) {
			success = false;
		}
		if (success) {
			$('#bankLoader').addClass('hide');
			$('#bankCheck').removeClass('hide');
			condenseCashInHandProcess();
		} else {
			stopCondensingProcess(ErrorCode.ERROR_CLOSE_BOOK_BANK_ERROR);
		}
	}, 0);
}

function condenseCashInHandProcess() {
	setTimeout(function () {
		var success = false;
		try {
			success = closeBookBiz.condenseCashInHand();
		} catch (err) {
			success = false;
		}
		if (success) {
			$('#cashInHandLoader').addClass('hide');
			$('#cashInHandCheck').removeClass('hide');
			condenseLoanProcess();
		} else {
			stopCondensingProcess(ErrorCode.ERROR_CLOSE_BOOK_CASH_IN_HAND_ERROR);
		}
	}, 0);
}

function condenseLoanProcess() {
	setTimeout(function () {
		var success = false;
		try {
			success = closeBookBiz.condenseLoans();
		} catch (err) {
			success = false;
		}
		if (success) {
			$('#loanLoader').addClass('hide');
			$('#loanCheck').removeClass('hide');
			condenseChequeProcess();
		} else {
			stopCondensingProcess(ErrorCode.ERROR_CLOSE_BOOK_LOAN_ERROR);
		}
	}, 0);
}

function condenseChequeProcess() {
	setTimeout(function () {
		var success = false;
		try {
			success = closeBookBiz.condenseCheques();
		} catch (err) {
			success = false;
		}
		if (success) {
			$('#chequeLoader').addClass('hide');
			$('#chequeCheck').removeClass('hide');
			condenseTransactionProcess();
		} else {
			stopCondensingProcess(ErrorCode.ERROR_CLOSE_BOOK_CHEQUE_ERROR);
		}
	}, 0);
}

function condenseTransactionProcess() {
	setTimeout(function () {
		var success = false;
		try {
			success = closeBookBiz.condenseTransactions();
		} catch (err) {
			success = false;
		}
		if (success) {
			$('#transactionLoader').addClass('hide');
			$('#transactionCheck').removeClass('hide');
			stopCondensingProcess(ErrorCode.ERROR_CONDENSE_PROCESS_SUCCESS);
		} else {
			stopCondensingProcess(ErrorCode.ERROR_CLOSE_BOOK_TRANSACTION_ERROR);
		}
	}, 0);
}

function stopCondensingProcess(success_code) {
	var beginWasSuccessFull = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
	var isCloseBook = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

	if (success_code == ErrorCode.ERROR_CONDENSE_PROCESS_SUCCESS) {
		if (closeBookBiz.stopCondensing(true, beginWasSuccessFull, isCloseBook)) {
			MyAnalytics.pushEvent('Close Financial Year Complete');
			$('#customAlertDialog #customAlertDialogBody').text('Previous year Financial Year Books have been closed. Please restart your app.');
			$('#customAlertDialog').show().dialog({
				'title': 'Books closed successfully',
				'closeX': false,
				'closeOnEscape': false,
				closeText: 'hide',
				modal: true,
				open: function open(event, ui) {
					$('.ui-dialog-titlebar-close').hide();
				}
			});
			$('#customAlertDialog #confirm').text('Ok').show();
			$('#customAlertDialog #cancel').hide();
			$('#customAlertDialog button').off('click').on('click', function () {
				// $('#customAlertDialog').dialog('close').dialog('destroy');
				if (this.id == 'confirm') {
					var _require = require('electron'),
					    ipcRenderer = _require.ipcRenderer;

					ipcRenderer.send('changeURL', 'Index1.html');
				} else {}
			});
		} else {
			$('#closeBookPrefixDiv').removeClass('disableClassCloseBooks');
			$('#closeBookDefaultDiv').removeClass('classForDefaultCloseBook');
			$('#submitButtonCloseBookPrefix').attr('disabled', false);
			if (isCloseBook) {
				showCloseBookErrorMessage(globalErrorMessage);
			} else {
				showCloseBookErrorMessage(ErrorCode.ERROR_CLOSE_BOOK_ERROR);
			}
		}
	} else {
		$('#closeBookPrefixDiv').removeClass('disableClassCloseBooks');
		$('#closeBookDefaultDiv').removeClass('classForDefaultCloseBook');
		$('#submitButtonCloseBookPrefix').attr('disabled', false);
		closeBookBiz.stopCondensing(false, beginWasSuccessFull);
		success_code = isCloseBook ? globalErrorMessage : success_code;
		showCloseBookErrorMessage(success_code || 'Some Error Occured in closebook, please contact vyapar customer support.');
	}
	cleanCachesForCloseBook();
}

function startCondensingProcess() {
	if (closeBookBiz.startCondensing()) {
		condensePartyProcess();
	} else {
		stopCondensingProcess(ErrorCode.ERROR_CLOSE_BOOK_ERROR, false);
	}
}

function startCondensingProcessPrefixFlow(prefixObj) {
	if (closeBookBiz.startCondensing()) {
		return condensePrefixFlow(prefixObj);
	} else {
		return false;
	}
}

function closeBookBackupFailed(err) {
	if (err) {
		showCloseBookErrorMessage(ErrorCode.ERROR_CLOSE_BOOK_BACKUP_FAILED);
	} else {
		showCloseBookErrorMessage(ErrorCode.ERROR_CLOSE_BOOK_BACKUP_NOT_SELECTED);
	}
	$('#closeBookPrefixDiv').removeClass('disableClassCloseBooks');
	$('#closeBookDefaultDiv').removeClass('classForDefaultCloseBook');
	$('#submitButtonCloseBookPrefix').attr('disabled', false);
	$('#submitButtonCloseBook').attr('disabled', false);
}

function cancelPrefixCloseBook() {
	$('#closeBookPrefixDiv').removeClass('classForPrefixCloseBook');
	$('#closeBookDefaultDiv').removeClass('disableClassCloseBooks');
	$('#submitButtonCloseBook').attr('disabled', false);
	$('#submitButtonCloseBookPrefix').css('display', 'block');
	$('#prefixUpdateView').css('display', 'none');
}

function updatePrefixCloseBook() {
	MyAnalytics.pushEvent('Close Financial Year Start Closing Prefix Flow');
	var settingCache = new SettingCache();
	var dataLoader = new DataLoader();
	var invoicePrefixVal = $('#invoicePrefixUpdate').val();
	var estimatePrefixVal = $('#estimatePrefixUpdate').val();
	var cashInPrefixVal = $('#cashInPrefixUpdate').val();
	var deliveryChallanPrefixVal = $('#deliveryChallanPrefixUpdate').val();
	var saleOrderPrefixVal = $('#saleOrderPrefixUpdate').val();
	var purchaseOrderPrefixVal = $('#purchaseOrderPrefixUpdate').val();
	var saleReturnPrefixVal = $('#saleReturnPrefixUpdate').val();

	var duplicatePrefixMessageString = '';

	if (invoicePrefixVal.trim() == '') {
		ToastHelper.error('Please fill Invoice prefix');
		return;
	} else {
		var isInvoicePrefixExist = dataLoader.isPrefixExist(invoicePrefixVal, TxnTypeConstant.TXN_TYPE_SALE);
		if (isInvoicePrefixExist) {
			duplicatePrefixMessageString += 'Invoice ';
		}
	}

	if (settingCache.isEstimateEnabled() && estimatePrefixVal.trim() != '') {
		var isEstimatePrefixExist = dataLoader.isPrefixExist(estimatePrefixVal, TxnTypeConstant.TXN_TYPE_ESTIMATE);
		if (isEstimatePrefixExist) {
			duplicatePrefixMessageString += duplicatePrefixMessageString.length > 0 ? ',Estimate ' : 'Estimate';
		}
	}
	if (cashInPrefixVal.trim() != '') {
		var isCashInPrefixExist = dataLoader.isPrefixExist(cashInPrefixVal, TxnTypeConstant.TXN_TYPE_CASHIN);
		if (isCashInPrefixExist) {
			duplicatePrefixMessageString += duplicatePrefixMessageString.length > 0 ? ',Payment-In ' : 'Payment-In';
		}
	}
	if (saleReturnPrefixVal.trim() != '') {
		var isSaleReturnPrefixExist = dataLoader.isPrefixExist(saleReturnPrefixVal, TxnTypeConstant.TXN_TYPE_SALE_RETURN);
		if (isSaleReturnPrefixExist) {
			duplicatePrefixMessageString += duplicatePrefixMessageString.length > 0 ? ',Sale Return ' : 'Sale Return';
		}
	}

	if (settingCache.isDeliveryChallanEnabled() && deliveryChallanPrefixVal.trim() != '') {
		var isDeliveryChallanPrefixExist = dataLoader.isPrefixExist(deliveryChallanPrefixVal, TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN);
		if (isDeliveryChallanPrefixExist) {
			duplicatePrefixMessageString += duplicatePrefixMessageString.length > 0 ? ',Delivery Challan ' : 'Delivery Challan';
		}
	}

	if (settingCache.isOrderFormEnabled()) {
		if (saleOrderPrefixVal.trim() != '') {
			var isSaleOrderPrefixExist = dataLoader.isPrefixExist(saleOrderPrefixVal, TxnTypeConstant.TXN_TYPE_SALE_ORDER);
			if (isSaleOrderPrefixExist) {
				duplicatePrefixMessageString += duplicatePrefixMessageString.length > 0 ? ',Sale Order ' : 'Sale Order';
			}
		}
		if (purchaseOrderPrefixVal.trim() != '') {
			var isPurchaseOrderPrefixExist = dataLoader.isPrefixExist(purchaseOrderPrefixVal, TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER);
			if (isPurchaseOrderPrefixExist) {
				duplicatePrefixMessageString += duplicatePrefixMessageString.length > 0 ? ',Purchase Order ' : 'Purchase Order';
			}
		}
	}

	if (duplicatePrefixMessageString.length > 0) {
		ToastHelper.error(duplicatePrefixMessageString + 'prefix already exist');
		return;
	}

	var prefixObj = {
		'invoicePrefix': invoicePrefixVal,
		'estimatePrefix': estimatePrefixVal,
		'cashInPrefix': cashInPrefixVal,
		'deliveryChallanPrefix': deliveryChallanPrefixVal,
		'saleOrderPrefix': saleOrderPrefixVal,
		'purchaseOrderPrefix': purchaseOrderPrefixVal,
		'saleReturnPrefix': saleReturnPrefixVal
	};
	var isSuccess = startCondensingProcessPrefixFlow(prefixObj);
	if (isSuccess) {
		closeBookBiz.stopCondensing(true, true, false);
		cancelPrefixCloseBook();
		ToastHelper.success(ErrorCode.ERROR_CLOSEBOOK_PROCESS_PREFIX_SUCCESS);
	} else {
		stopCondensingProcess(ErrorCode.ERROR_CLOSEBOOK_PROCESS_PREFIX_FAILED, true, false);
	}
}

function closeBookBackupSuccess(filePath) {
	$('#closeBookPrefixDiv').addClass('disableClassCloseBooks');
	$('#closeBookDefaultDiv').addClass('classForDefaultCloseBook');
	$('#submitButtonCloseBookPrefix').attr('disabled', true);
	$('#backupLocationText').html(filePath);
	startCondensingProcess();
	$('#submitButtonCloseBook').attr('disabled', false);
}

function cleanCachesForCloseBook() {
	try {
		var NameCache = require('./../Cache/NameCache.js');
		var nameCache = new NameCache();
		nameCache.reloadNameCache();
		var FirmCache = require('./../Cache/FirmCache.js');
		var firmCache = new FirmCache();
		firmCache.refreshFirmCache();
		var ItemCache = require('./../Cache/ItemCache.js');
		var itemCache = new ItemCache();
		itemCache.reloadItemCache();
	} catch (err) {
		logger.error('Error cleaning caches during close book', err);
	}
}

function takeBackupBeforeBookClose() {
	var BackupDB = require('./../BizLogic/BackupDB.js');
	var backUp = new BackupDB();
	setTimeout(function () {
		backUp.closeBookBackup(closeBookBackupSuccess, closeBookBackupFailed);
	}, 5);
}