var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var url = document.URL;
var ErrorCode = require('./../Constants/ErrorCode.js');
var CompanyListUtility = require('./../Utilities/CompanyListUtility.js');
var MyAnalytics = require('../Utilities/analyticsHelper.js');
var LicenseInfoConstant = require('./../Constants/LicenseInfoConstant.js');
var LocalStorageHelper = require('../Utilities/LocalStorageHelper');

if (url.split('=')[1] == 'true') {
	var SettingCache = require('./../Cache/SettingCache.js');
	var settingCache = new SettingCache();
	$('#businessName').val(settingCache.getLoginUserName());
	$('#phone').val(settingCache.getLoginUserNumber());
	$('#email').val(settingCache.getLoginUserEmailId());
	$('#login').val('Edit');
}

var referralCodeCheck = function referralCodeCheck() {
	var existingReferralCode = LocalStorageHelper.getValue(LicenseInfoConstant.referralCode);
	if (existingReferralCode) {
		$('#referralCode').val(existingReferralCode);
		$('.referralCodeParent').addClass('hide');
	}
};
referralCodeCheck();

var inputTextFilterForLength = function inputTextFilterForLength(object, length) {
	var re = '^.{0,' + (length || '15') + '}$';
	var objectStringValue = '' + object.value; // convert to string
	if (objectStringValue.search(re) < 0) {
		object.value = object.oldValue ? object.oldValue + '' : '';
	}
	object.oldValue = '' + object.value;
};

var inputTextFilterForPhone = function inputTextFilterForPhone(object) {
	var re = '^[-() 0-9]{0,15}$';
	var objectStringValue = '' + object.value; // convert to string
	if (objectStringValue.search(re) < 0) {
		object.value = object.oldValue ? object.oldValue + '' : '';
	}
	object.oldValue = '' + object.value;
};

$('#businessName').focus();

var ValidateEmail = function ValidateEmail(mail) {
	var mailValidator = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	if (mailValidator.test(mail)) {
		return ErrorCode['SUCCESS'];
	}
	errorCode = ErrorCode['ERROR_INVALID_EMAILID'];
	$('#email').focus();
	return errorCode;
};

var ValidatePhoneNumber = function ValidatePhoneNumber(phone) {
	// console.log(phone);
	if (/^\+?[0-9. ()-]{5,25}$/.test(phone)) {
		return ErrorCode['SUCCESS'];
	}
	errorCode = ErrorCode['ERROR_INVALID_PHONENUMBER'];
	$('#phone').focus();
	return errorCode;
};

var editUserDetailsToDb = function editUserDetailsToDb() {
	var obj = {};
	var ErrorCode = require('./../Constants/ErrorCode.js');
	var businessName = $('#businessName').val().trim();
	// businessName = businessName.replace("^[.\\\\/:*?\"<>|]?[\\\\/:*?\"<>|]*", "");
	obj['businessName'] = businessName;
	obj['phone'] = $('#phone').val();
	obj['email'] = $('#email').val();
	var statusCode = ValidateEmail(obj['email']);
	if (statusCode == ErrorCode['SUCCESS']) {
		statusCode = ValidatePhoneNumber(obj['phone']);
	}
	obj['firstTime'] = 1;
	if (statusCode == ErrorCode['SUCCESS']) {
		if (obj['businessName'] && obj['phone'] && obj['email']) {
			var newDB = obj['businessName'] + '.vyp';
			var oldCompany = settingCache.getLoginUserName();
			console.log(oldCompany);
			try {
				var Queries = require('./../Constants/Queries.js');
				var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
				var TransactionManager = require('./../DBManager/TransactionManager.js');
				var sqlitedbhelper = new SqliteDBHelper();
				var transactionManager = new TransactionManager();

				var isTransactionBeginSuccess = transactionManager.BeginTransaction();

				if (isTransactionBeginSuccess) {
					var statusCode = sqlitedbhelper.editCompanyDetails(obj);
					if (statusCode == ErrorCode.ERROR_COMPANY_EDIT_SUCCESS) {
						if (!transactionManager.CommitTransaction()) {
							statusCode = ErrorCode.ERROR_COMPANY_EDIT_FAILURE;
						}
					}
					transactionManager.EndTransaction();
				} else {
					statusCode = ErrorCode.ERROR_COMPANY_EDIT_FAILURE;
				}
				if (statusCode == ErrorCode.ERROR_COMPANY_EDIT_SUCCESS) {
					MyAnalytics.pushEvent('Create Company Save');

					var _require = require('electron'),
					    ipcRenderer = _require.ipcRenderer;

					ipcRenderer.send('changeURL', 'Index1.html');
				} else {
					alert(statusCode);
				}
			} catch (err) {
				console.log(err);
				alert('There seems to be some problem editing your company. Please restart your app and try agian. If the problem persists, please contact vyapar technical support');
			}
		} else {
			alert('Enter All the fields');
		}
	} else {
		alert(statusCode);
	}
};

var updateUserDetailsToDb = function updateUserDetailsToDb() {
	var sanitize = require('sanitize-filename');
	var obj = {};
	var ErrorCode = require('./../Constants/ErrorCode.js');
	var businessName = $('#businessName').val().trim();

	if (businessName == '') {
		$('.loaderWrapper').hide(function () {
			alert('Please fill business name.');
		});
		return;
	}
	var businessFileName = sanitize(businessName);
	if (businessFileName == '') {
		$('.loaderWrapper').hide(function () {
			alert('Please remove invalid characters from your file name.');
		});

		return;
	}
	obj['businessName'] = businessName;
	obj['phone'] = $('#phone').val();
	obj['email'] = $('#email').val();
	var userEnteredReferrerCode = $('#referralCode').val();
	var statusCode = '';
	if (!obj['phone'] && !obj['email']) {
		$('.loaderWrapper').hide(function () {
			alert('Please enter phone or email address.');
		});
		return;
	}
	if (obj['phone']) {
		statusCode = ValidatePhoneNumber(obj['phone']);
	}
	if (obj['email'] && (statusCode == '' || statusCode == ErrorCode['SUCCESS'])) {
		statusCode = ValidateEmail(obj['email']);
	}

	obj['firstTime'] = 1;
	if (statusCode == ErrorCode['SUCCESS']) {
		var newDB = businessFileName + '.vyp';
		var fs = require('fs');
		var path = require('path');

		var app = require('electron').remote.app;

		var appPath = app.getPath('userData');
		var businessNamesPath = path.join(appPath, '/BusinessNames');
		try {
			fs.mkdir(businessNamesPath, function (err, data, stderr) {
				if (fs.existsSync(appPath + '/BusinessNames/' + newDB)) {
					var statusCode = ErrorCode.ERROR_DB_ALREADY_EXISTS;
					$('.loaderWrapper').hide(function () {
						alert(statusCode);
					});
				} else {
					var fileNameObj = CompanyListUtility.getCompaniesFileNameObject();
					var op = fs.writeFileSync(appPath + '/BusinessNames/currentDB.txt', appPath + '/BusinessNames/' + newDB);
					var Queries = require('./../Constants/Queries.js');
					var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
					var TransactionManager = require('./../DBManager/TransactionManager.js');
					var transactionManager = new TransactionManager();
					var sqlitedbhelper = new SqliteDBHelper();
					var isTransactionBeginSuccess = transactionManager.BeginTransaction();
					if (isTransactionBeginSuccess) {
						MyAnalytics.pushProfile(obj);
						var statusCode = sqlitedbhelper.setFirstTimeLogin(obj);
						try {
							LocalStorageHelper.setValue(LicenseInfoConstant.referralCode, userEnteredReferrerCode);
						} catch (err1) {}
						if (statusCode == true) {
							if (statusCode = transactionManager.CommitTransaction()) {
								if ((0, _keys2.default)(fileNameObj).includes(businessName)) {
									businessName = businessName + '_' + new Date().getTime();
								}
								var lastNotificationSentAt = new Date().getTime();
								fileNameObj[businessName] = {
									'path': appPath + '/BusinessNames/' + newDB,
									'company_creation_type': 0,
									'lastNotificationSentAt': lastNotificationSentAt
								};
								CompanyListUtility.updateCompanyListFile(fileNameObj);
								statusCode = ErrorCode.ERROR_DB_CREATION_SUCCESS;
								$('.loaderWrapper').hide();
							} else {
								statusCode = ErrorCode.ERROR_DB_CREATION_FAILED;
							}
						}
						transactionManager.EndTransaction();
					} else {
						statusCode = ErrorCode.ERROR_DB_CREATION_FAILED;
					}
					if (statusCode == ErrorCode.ERROR_DB_CREATION_SUCCESS) {
						MyAnalytics.pushEvent('Create Company Save');

						var _require2 = require('electron'),
						    ipcRenderer = _require2.ipcRenderer;

						ipcRenderer.send('changeURL', 'Index1.html');
					} else {
						$('.loaderWrapper').hide(function () {
							alert(statusCode);
						});
					}
				}
			});
		} catch (err) {}
	} else {
		$('.loaderWrapper').hide(function () {
			alert(statusCode);
		});
	}
};

$(document).ready(function () {
	$('body').keydown(function (e) {
		if (e.keyCode == 13) {
			$('.loaderWrapper').show(function () {
				if ($('#login').val() == 'Edit') {
					editUserDetailsToDb();
				} else {
					updateUserDetailsToDb();
				}
			});
		}
	});
});

$('#login').on('click', function () {
	$('.loaderWrapper').show(function () {
		if ($('#login').val() == 'Edit') {
			editUserDetailsToDb();
		} else {
			updateUserDetailsToDb();
		}
	});
});

$('#cancel').on('click', function () {
	var _require3 = require('electron'),
	    ipcRenderer = _require3.ipcRenderer;

	ipcRenderer.send('changeURL', 'SwitchCompany.html');
});