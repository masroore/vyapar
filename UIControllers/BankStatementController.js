var startDate;
var endDate;
var DateFormat = require('./../Constants/DateFormat.js');
$('#startDate, #endDate').datepicker({ dateFormat: DateFormat.format });
var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
var NameCache = require('./../Cache/NameCache.js');
var paymentInfoCache = new PaymentInfoCache();
var nameCache = new NameCache();
var PaymentInfo = require('./../BizLogic/PaymentInfo.js');
var paymentInfo = new PaymentInfo();
var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var TxnTypeConstant = require('./../Constants/TxnTypeConstant');
var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var PDFReportConstant = require('./../Constants/PDFReportConstant');
var printForSharePDF = false;
var listOfTransactions = [];

var populateTable = function populateTable() {
	paymentInfoCache = new PaymentInfoCache();
	nameCache = new NameCache();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
	endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');

	bankId = $('#bankFilterOptions').val();
	CommonUtility.showLoader(function () {
		listOfTransactions = paymentInfo.getBankStatementDetailObjectList(bankId, startDate, endDate);
		displayTransactionList(listOfTransactions);
	});
};

var date = MyDate.getDate('d/m/y');
$('#startDate').val(MyDate.getFirstDateOfCurrentMonthString());
$('#endDate').val(date);

$('#startDate').change(function () {
	populateTable();
});

$('#endDate').change(function () {
	populateTable();
});

var bankList = paymentInfoCache.getBankListObj();

var optionTemp = document.createElement('option');
for (i in bankList) {
	var option = optionTemp.cloneNode();
	option.text = bankList[i].getName();
	option.value = bankList[i].getId();
	$('#bankFilterOptions').append(option);
}

try {
	$('#bankFilterOptions').selectmenu('refresh', true);
} catch (err) {}

$('#bankFilterOptions').on('selectmenuchange', function (event) {
	populateTable();
});

var bankStatementClusterize;

var displayTransactionList = function displayTransactionList(bankDetailObjectList) {
	var len = bankDetailObjectList.length;

	var dynamicRow = '';
	var dynamicFooter = '';
	var runningBalance = 0;
	var rowData = [];

	dynamicRow += "<thead><tr><th width='14%'>Date</th><th width='26%'>Description</th>";
	dynamicRow += "<th width='20%' class='tableCellTextAlignRight'>Withdrawal Amount</th><th width='20%' class='tableCellTextAlignRight'>Deposit Amount</th><th width='20%' class='tableCellTextAlignRight'>Balance Amount</th></tr></thead>";

	for (var _i = 0; _i < len; _i++) {
		var bankDetailObject = bankDetailObjectList[_i];
		var date = MyDate.getDate('d/m/y', bankDetailObject.getTxnDate());

		var bankDetailObjectAmount = bankDetailObject.getAmount() ? Number(bankDetailObject.getAmount()) : 0;

		var row = '';
		row = "<td width='14%'>" + date + '</td>';

		var txnType = Number(bankDetailObject.getTxnType());
		var description = '';

		if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT || txnType == TxnTypeConstant.TXN_TYPE_EXPENSE || txnType == TxnTypeConstant.TXN_TYPE_OTHER_INCOME || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_ESTIMATE || txnType == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
			var userId = bankDetailObject.getUserId();
			var name = nameCache.findNameObjectByNameId(userId);
			description = '[' + TxnTypeConstant.getTxnTypeForUI(txnType) + '] ' + (name ? name.getFullName() : '');
		} else if (txnType == TxnTypeConstant.TXN_TYPE_BANK_OPENING || txnType == TxnTypeConstant.TXN_TYPE_BANK_BALANCE_FORWARD) {
			description = TxnTypeConstant.getTxnTypeForUI(txnType);
		} else if (txnType == TxnTypeConstant.TXN_TYPE_CHEQUE_TRANSFER) {
			var userId = bankDetailObject.getUserId();
			var name = nameCache.findNameObjectByNameId(userId);
			description = '[' + TxnTypeConstant.getTxnTypeForUI(txnType) + '] ' + (name ? name.getFullName() : '');
		} else if (txnType == TxnTypeConstant.TXN_TYPE_BANK_TO_BANK) {
			if (bankDetailObject.getToBankId() == bankId) {
				var bankName = paymentInfoCache.getPaymentBankName(bankDetailObject.getFromBankId());
				description = 'From ' + bankName;
			} else {
				var bankName = paymentInfoCache.getPaymentBankName(bankDetailObject.getToBankId());
				description = 'To ' + bankName;
			}
		} else {
			description = '[' + TxnTypeConstant.getTxnTypeForUI(txnType) + '] ' + (bankDetailObject.getDescription() ? bankDetailObject.getDescription() : '');
		}

		row += "<td width='26%'>" + description + '</td>';

		var appliedTxnType = txnType == TxnTypeConstant.TXN_TYPE_CHEQUE_TRANSFER ? Number(bankDetailObject.getSubTxnType()) : txnType;

		switch (appliedTxnType) {
			case TxnTypeConstant.TXN_TYPE_PURCHASE:
			case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
			case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
			case TxnTypeConstant.TXN_TYPE_CASHOUT:
			case TxnTypeConstant.TXN_TYPE_EXPENSE:
			case TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE:
			case TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE_WITHOUT_CASH:
				runningBalance -= bankDetailObjectAmount;
				row += "<td width='20%' align=\"right\">" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(bankDetailObjectAmount) + '</td>';
				row += '<td></td>';
				row += "<td width='20%' align=\"right\">" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(runningBalance) + '</td>';
				break;
			case TxnTypeConstant.TXN_TYPE_OTHER_INCOME:
			case TxnTypeConstant.TXN_TYPE_SALE:
			case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
			case TxnTypeConstant.TXN_TYPE_ESTIMATE:
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
			case TxnTypeConstant.TXN_TYPE_CASHIN:
			case TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD:
			case TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD_WITHOUT_CASH:
				runningBalance += bankDetailObjectAmount;
				row += '<td></td>';
				row += "<td width='20%' align=\"right\">" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(bankDetailObjectAmount) + '</td>';
				row += "<td width='20%' align=\"right\">" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(runningBalance) + '</td>';
				break;
			case TxnTypeConstant.TXN_TYPE_BANK_BALANCE_FORWARD:
				runningBalance += bankDetailObjectAmount;
				row += '<td></td>';
				row += '<td></td>';
				row += "<td width='20%' align=\"right\">" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(runningBalance) + '</td>';
				break;
			case TxnTypeConstant.TXN_TYPE_BANK_OPENING:
				runningBalance += bankDetailObjectAmount;
				if (bankDetailObjectAmount > 0) {
					row += '<td></td>';
					row += "<td width='20%' align=\"right\">" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(Math.abs(bankDetailObjectAmount)) + '</td>';
				} else {
					row += "<td width='20%' align=\"right\">" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(Math.abs(bankDetailObjectAmount)) + '</td>';
					row += '<td></td>';
				}
				row += "<td width='20%' align=\"right\">" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(runningBalance) + '</td>';
				break;
			case TxnTypeConstant.TXN_TYPE_BANK_TO_BANK:
				if (bankDetailObject.getFromBankId() == bankId) {
					runningBalance -= bankDetailObjectAmount;
					row += "<td width='20%' align=\"right\">" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(bankDetailObjectAmount) + '</td>';
					row += '<td></td>';
				} else {
					runningBalance += bankDetailObjectAmount;
					row += '<td></td>';
					row += "<td width='20%' align=\"right\">" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(bankDetailObjectAmount) + '</td>';
				}
				row += "<td width='20%' align=\"right\">" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(runningBalance) + '</td>';
				break;
			case TxnTypeConstant.TXN_TYPE_LOAN_STARTING_BALANCE:
			case TxnTypeConstant.TXN_TYPE_LOAN_PROCESSING_FEE:
			case TxnTypeConstant.TXN_TYPE_LOAN_ADJUSTMENT:
			case TxnTypeConstant.TXN_TYPE_LOAN_EMI_PAYMENT:
			case TxnTypeConstant.TXN_TYPE_LOAN_CLOSE_BOOK_ADJ:
				if (txnType == TxnTypeConstant.TXN_TYPE_LOAN_EMI_PAYMENT || txnType == TxnTypeConstant.TXN_TYPE_LOAN_PROCESSING_FEE) {
					runningBalance -= bankDetailObjectAmount;
					row += "<td width='20%' align=\"right\">" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(bankDetailObjectAmount) + '</td>';
					row += '<td></td>';
				} else {
					runningBalance += bankDetailObjectAmount;
					row += '<td></td>';
					row += "<td width='20%' align=\"right\">" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(bankDetailObjectAmount) + '</td>';
				}
				row += "<td width='20%' align=\"right\">" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(runningBalance) + '</td>';
				break;
		}

		row = "<tr ondblclick='openTransaction(" + bankDetailObject.getTxnId() + ',' + bankDetailObject.getTxnType() + ")' class='currentRow'>" + row + '</tr>';
		rowData.push(row);
	}
	var data = rowData;

	if ($('#bankStatementContainer').length === 0) {
		return;
	}

	if (bankStatementClusterize && bankStatementClusterize.destroy) {
		bankStatementClusterize.clear();
		bankStatementClusterize.destroy();
	}

	bankStatementClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollArea',
		contentId: 'contentArea',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});
	$('#tableHead').html(dynamicRow);
	dynamicFooter += "<thead><tr><th width='14%'></th><th width='26%'></th><th width='20%' class='tableCellTextAlignRight'></th><th width='20%' class='tableCellTextAlignRight'>Balance</th><th width='20%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(runningBalance) + '</th></tr></thead>';

	$('#total').html(dynamicFooter);
};

var openTransaction = function openTransaction(txnId, txnType) {
	var ViewTransaction = require('./../BizLogic/ViewTransaction.js');
	var viewTransaction = new ViewTransaction();
	viewTransaction.viewTransactionFile(txnId + ':' + TxnTypeConstant.getTxnType(txnType));
};

var getHTMLTextForReport = function getHTMLTextForReport() {
	var BankStatementHTMLGenerator = require('./../ReportHTMLGenerator/BankStatementHTMLGenerator.js');
	var bankStatementHTMLGenerator = new BankStatementHTMLGenerator();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var bankId = $('#bankFilterOptions').val();
	var bankName = '';
	if (bankId) {
		var bankName = paymentInfoCache.getPaymentBankName(bankId);
	}
	var printDescription = $('#printBankDescription').is(':checked');
	var htmlText = bankStatementHTMLGenerator.getHTMLText(listOfTransactions, startDateString, endDateString, bankName, printDescription);
	return htmlText;
};

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	// $('#frameDiv').load('');
	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable();
};

/// ///////////////////////////PDF/////////////////////////////////////

function openPrintOptions() {
	$('#PrintDialog').removeClass('hide');
	$('#PrintDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closePrintOptions() {
	$('#PrintDialog').addClass('hide');
	$('#PrintDialog').dialog('close').dialog('destroy');
}

var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.BANK_STATEMENT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#PrintDialog').addClass('hide');
	$('#PrintDialog').dialog('close').dialog('destroy');
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

function openExcelOptions() {
	$('#ExcelDialog').removeClass('hide');
	$('#ExcelDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closeExcelOptions() {
	$('#ExcelDialog').addClass('hide');
	$('#ExcelDialog').dialog('close').dialog('destroy');
}

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.BANK_STATEMENT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
	excelHelper.saveExcel(fileName);
};

var prepareObjectForExcel = function prepareObjectForExcel(bankDetailObjectList) {
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var isTransactionRefNumberEnabled = settingCache.getTxnRefNoEnabled();
	var len = bankDetailObjectList.length;
	var ExcelDescription = $('#ExcelBankTxnDescription').is(':checked');
	var rowObj = [];
	var tableHeadArray = [];
	var totalArray = [];
	tableHeadArray.push('Date');
	tableHeadArray.push('Transaction Details');
	tableHeadArray.push('Withdrawl Amount');
	tableHeadArray.push('Deposit Amount');
	tableHeadArray.push('Balance Amount');
	if (ExcelDescription) {
		tableHeadArray.push('Description');
	}
	rowObj.push(tableHeadArray);
	rowObj.push([]);

	var runningBalance = 0;
	var totalWithdrawlAmount = 0;
	var totaldepositAmount = 0;
	for (var j = 0; j < len; j++) {
		var tempArray = [];
		var bankDetailObject = bankDetailObjectList[j];
		var date = MyDate.getDate('d/m/y', bankDetailObject.getTxnDate());
		var bankDetailObjectAmount = bankDetailObject.getAmount() ? Number(bankDetailObject.getAmount()) : 0;
		tempArray.push(date);
		var txnType = Number(bankDetailObject.getTxnType());
		var description = '';
		var withdrawlAmount = '';
		var depositAmount = '';
		var balanceAmount = '';
		if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT || txnType == TxnTypeConstant.TXN_TYPE_EXPENSE || txnType == TxnTypeConstant.TXN_TYPE_OTHER_INCOME || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_ESTIMATE || txnType == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
			var userId = bankDetailObject.getUserId();
			var name = nameCache.findNameObjectByNameId(userId);
			description = '[' + TxnTypeConstant.getTxnTypeForUI(txnType) + '] ' + (name ? name.getFullName() : '');
		} else if (txnType == TxnTypeConstant.TXN_TYPE_BANK_OPENING || txnType == TxnTypeConstant.TXN_TYPE_BANK_BALANCE_FORWARD) {
			description = TxnTypeConstant.getTxnTypeForUI(txnType);
		} else if (txnType == TxnTypeConstant.TXN_TYPE_CHEQUE_TRANSFER) {
			var userId = bankDetailObject.getUserId();
			var name = nameCache.findNameObjectByNameId(userId);
			description = '[' + TxnTypeConstant.getTxnTypeForUI(txnType) + '] ' + (name ? name.getFullName() : '');
		} else if (txnType == TxnTypeConstant.TXN_TYPE_BANK_TO_BANK) {
			if (bankDetailObject.getToBankId() == bankId) {
				var bankName = paymentInfoCache.getPaymentBankName(bankDetailObject.getFromBankId());
				description = 'From ' + bankName;
			} else {
				var bankName = paymentInfoCache.getPaymentBankName(bankDetailObject.getToBankId());
				description = 'To ' + bankName;
			}
		} else {
			description = '[' + TxnTypeConstant.getTxnTypeForUI(txnType) + '] ' + (bankDetailObject.getDescription() ? bankDetailObject.getDescription() : '');
		}

		tempArray.push(description);

		var appliedTxnType = txnType == TxnTypeConstant.TXN_TYPE_CHEQUE_TRANSFER ? Number(bankDetailObject.getSubTxnType()) : txnType;

		switch (appliedTxnType) {
			case TxnTypeConstant.TXN_TYPE_PURCHASE:
			case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
			case TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER:
			case TxnTypeConstant.TXN_TYPE_CASHOUT:
			case TxnTypeConstant.TXN_TYPE_EXPENSE:
			case TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE:
			case TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE_WITHOUT_CASH:
				runningBalance -= bankDetailObjectAmount;
				withdrawlAmount = bankDetailObjectAmount;
				balanceAmount = runningBalance;
				totalWithdrawlAmount += withdrawlAmount;
				break;
			case TxnTypeConstant.TXN_TYPE_OTHER_INCOME:
			case TxnTypeConstant.TXN_TYPE_SALE:
			case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
			case TxnTypeConstant.TXN_TYPE_SALE_ORDER:
			case TxnTypeConstant.TXN_TYPE_ESTIMATE:
			case TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN:
			case TxnTypeConstant.TXN_TYPE_CASHIN:
			case TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD:
			case TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD_WITHOUT_CASH:
				runningBalance += bankDetailObjectAmount;
				depositAmount = bankDetailObjectAmount;
				balanceAmount = runningBalance;
				totaldepositAmount += depositAmount;
				break;
			case TxnTypeConstant.TXN_TYPE_BANK_BALANCE_FORWARD:
				runningBalance += bankDetailObjectAmount;
				balanceAmount = runningBalance;
				break;
			case TxnTypeConstant.TXN_TYPE_BANK_OPENING:
				runningBalance += bankDetailObjectAmount;
				if (bankDetailObjectAmount > 0) {
					depositAmount = bankDetailObjectAmount;
					totaldepositAmount += depositAmount;
				} else {
					withdrawlAmount = bankDetailObjectAmount;
					totalWithdrawlAmount += withdrawlAmount;
				}
				balanceAmount = runningBalance;
				break;
			case TxnTypeConstant.TXN_TYPE_BANK_TO_BANK:
				if (bankDetailObject.getFromBankId() == bankId) {
					runningBalance -= bankDetailObjectAmount;
					withdrawlAmount = bankDetailObjectAmount;
					totalWithdrawlAmount += withdrawlAmount;
				} else {
					runningBalance += bankDetailObjectAmount;
					depositAmount = bankDetailObjectAmount;
					totaldepositAmount += depositAmount;
				}
				balanceAmount = runningBalance;
				break;
			case TxnTypeConstant.TXN_TYPE_LOAN_STARTING_BALANCE:
			case TxnTypeConstant.TXN_TYPE_LOAN_PROCESSING_FEE:
			case TxnTypeConstant.TXN_TYPE_LOAN_ADJUSTMENT:
			case TxnTypeConstant.TXN_TYPE_LOAN_EMI_PAYMENT:
			case TxnTypeConstant.TXN_TYPE_LOAN_CLOSE_BOOK_ADJ:
				if (txnType == TxnTypeConstant.TXN_TYPE_LOAN_EMI_PAYMENT || txnType == TxnTypeConstant.TXN_TYPE_LOAN_PROCESSING_FEE) {
					runningBalance -= bankDetailObjectAmount;
					withdrawlAmount = bankDetailObjectAmount;
					totalWithdrawlAmount += withdrawlAmount;
				} else {
					runningBalance += bankDetailObjectAmount;
					depositAmount = bankDetailObjectAmount;
					totaldepositAmount += depositAmount;
				}
				balanceAmount = runningBalance;
				break;
		}
		tempArray.push(MyDouble.getAmountWithDecimal(withdrawlAmount));
		tempArray.push(MyDouble.getAmountWithDecimal(depositAmount));
		tempArray.push(MyDouble.getAmountWithDecimal(balanceAmount));
		if (ExcelDescription) {
			tempArray.push(bankDetailObject.getTxnDescription());
		}
		rowObj.push(tempArray);
	}
	var totalArray = [];
	rowObj.push([]);
	totalArray.push('');
	totalArray.push('Total');
	totalArray.push(MyDouble.getAmountWithDecimal(totalWithdrawlAmount));
	totalArray.push(MyDouble.getAmountWithDecimal(totaldepositAmount));
	totalArray.push(MyDouble.getAmountWithDecimal(runningBalance));
	rowObj.push(totalArray);
	return rowObj;
};

function printExcel() {
	closeExcelOptions();
	var XLSX = require('xlsx');
	var dataArray = prepareObjectForExcel(listOfTransactions);
	var worksheet = XLSX.utils.aoa_to_sheet(dataArray);

	var workbook = {
		'SheetNames': [],
		'Sheets': {}
	};

	var wsName = 'Bank Statement';
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet;
	var wscolWidth = [{ wch: 15 }, { wch: 30 }, { wch: 20 }, { wch: 20 }, { wch: 20 }];
	worksheet['!cols'] = wscolWidth;
	console.log(workbook);
	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

populateTable();