var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var TransactionStatus = require('./../Constants/TransactionStatus.js');
var startDate;
var endDate;
var DateFormat = require('./../Constants/DateFormat.js');
$('#startDate, #endDate').datepicker({ dateFormat: DateFormat.format });
var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();

var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var NameCache = require('./../Cache/NameCache.js');
var nameCache = new NameCache();
var printForSharePDF = false;
var listOfTransactions = [];
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');

var populateTable = function populateTable() {
	settingCache = new SettingCache();
	var DataLoader = require('./../DBManager/DataLoader.js');
	var dataLoader = new DataLoader();

	startDateString = $('#startDate').val();
	endDateString = $('#endDate').val();
	nameString = $('#nameForReport').val();
	var selectedTransactionStatus = Number($('#transactionFilterOptions').val());
	var orderType = Number($('#transactionTypeFilterOptions').val());
	var nameId = -1;

	if (nameString) {
		var nameModel = nameCache.findNameModelByName(nameString);
		if (nameModel) {
			nameId = nameModel.getNameId();
		} else {
			nameId = 0;
		}
	}

	startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
	endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');
	var firmId = Number($('#firmFilterOptions').val());
	CommonUtility.showLoader(function () {
		listOfTransactions = dataLoader.getOrderItemReportList(startDate, endDate, nameId, firmId, selectedTransactionStatus, orderType);
		displayTransactionList(listOfTransactions);
	});
};

var date = MyDate.getDate('d/m/y');
$('#startDate').val(MyDate.getFirstDateOfCurrentMonthString());
$('#endDate').val(date);

if (settingCache.getMultipleFirmEnabled()) {
	var optionTemp = document.createElement('option');
	var FirmCache = require('./../Cache/FirmCache.js');
	var firmCache = new FirmCache();
	var firmList = firmCache.getFirmList();
	for (i in firmList) {
		var option = optionTemp.cloneNode();
		option.text = firmList[i].getFirmName();
		option.value = firmList[i].getFirmId();
		$('#firmFilterOptions').append(option);
	}
	$('#firmChooser').show();
} else {
	$('#firmChooser').hide();
}

var optionTemp = document.createElement('option');
var transactionListArray = ['Open Orders', 'Close Orders'];
var transactionListId = [TransactionStatus.TXN_ORDER_OPEN, TransactionStatus.TXN_ORDER_COMPLETE];
for (i in transactionListArray) {
	//        console.log(i);
	var option = optionTemp.cloneNode();
	option.text = transactionListArray[i];
	option.value = transactionListId[i];
	$('#transactionFilterOptions').append(option);
}

var optionTemp = document.createElement('option');
var transactionTypeListArray = ['Sale Order', 'Purchase Order'];
var transactionTypeListId = [TxnTypeConstant.TXN_TYPE_SALE_ORDER, TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER];
for (i in transactionTypeListArray) {
	var option = optionTemp.cloneNode();
	option.text = transactionTypeListArray[i];
	option.value = transactionTypeListId[i];
	$('#transactionTypeFilterOptions').append(option);
}

try {
	$('#transactionTypeFilterOptions').selectmenu('refresh', true);
} catch (err) {}

$('#firmFilterOptions').on('selectmenuchange', function (event) {
	populateTable();
});

$('#startDate').change(function () {
	populateTable();
});

$('#endDate').change(function () {
	populateTable();
});

$(function () {
	$('#nameForReport').autocomplete(UIHelper.getAutocompleteDefaultOptions(nameCache.getListOfNames()));
});

$('#nameForReport').on('change', function (event) {
	populateTable();
});

$('#transactionFilterOptions').on('selectmenuchange', function () {
	populateTable();
});

$('#transactionTypeFilterOptions').on('selectmenuchange', function () {
	populateTable();
});

var orderItemReportClusterize;

var displayTransactionList = function displayTransactionList(listOfTransactions) {
	var len = listOfTransactions.length;
	var rowData = [];
	var dynamicRow = '';
	dynamicRow += "<thead><tr><th width='40%'>Item Name</th><th width='30%' class='tableCellTextAlignRight'>Quantity</th><th width='30%' class='tableCellTextAlignRight'>Amount</th></tr></thead>";
	//
	var totalQuantity = 0;
	var totalFreeQuantity = 0;
	var totalAmount = 0;
	var row = '';
	for (var _i = 0; _i < len; _i++) {
		var txn = listOfTransactions[_i];
		var itemName = txn.getItemName();
		var qty = txn.getQty();
		var freeQty = txn.getFreeQty();
		var amount = txn.getAmount();
		row = '<tr class=\'currentRow\'><td width=\'40%\'>' + itemName + '</td><td width=\'30%\' class=\'tableCellTextAlignRight\'>' + (MyDouble.getQuantityWithDecimalWithoutColor(qty) + MyDouble.quantityDoubleToStringWithSignExplicitly(freeQty, true)) + '</td><td width=\'30%\' class=\'tableCellTextAlignRight\'>' + MyDouble.getAmountWithDecimalAndCurrency(amount) + '</td></tr>';
		rowData.push(row);
		totalQuantity += qty;
		totalFreeQuantity += freeQty;
		totalAmount += amount;
	}
	row = '<tr ><td width=\'40%\'><b>Total</b></td><td width=\'30%\' class=\'tableCellTextAlignRight\'><b>' + (MyDouble.getQuantityWithDecimalWithoutColor(totalQuantity) + MyDouble.quantityDoubleToStringWithSignExplicitly(totalFreeQuantity, true)) + '</b></td><td width=\'30%\' class=\'tableCellTextAlignRight\'><b>' + MyDouble.getAmountWithDecimalAndCurrency(totalAmount) + '</b></td></tr>';
	rowData.push(row);
	var data = rowData;

	if ($('#orderItemReportContainer').length === 0) {
		return;
	}
	if (orderItemReportClusterize && orderItemReportClusterize.destroy) {
		orderItemReportClusterize.clear();
		orderItemReportClusterize.destroy();
	}
	orderItemReportClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollArea',
		contentId: 'contentArea',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});
	$('#tableHead').html(dynamicRow);
};

var getHTMLTextForReport = function getHTMLTextForReport() {
	var OrderItemReportHTMLGenerator = require('./../ReportHTMLGenerator/OrderItemReportHTMLGenerator');
	var orderItemReportHTMLGenerator = new OrderItemReportHTMLGenerator();

	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var firmId = Number($('#firmFilterOptions').val());
	var nameString = $('#nameForReport').val();
	if (!nameString) {
		nameString = 'All parties';
	}
	var printItemQuantity = $('#printItemQuantity').is(':checked');
	var printItemAmount = $('#printItemAmount').is(':checked');
	var htmlText = orderItemReportHTMLGenerator.getHTMLText(listOfTransactions, startDateString, endDateString, firmId, nameString, printItemQuantity, printItemAmount);
	return htmlText;
};

var printTransaction = function printTransaction(that) {
	var settingCache = new SettingCache();
	var ThermalPrinterConstant = require('./../Constants/ThermalPrinterConstant.js');
	if (settingCache.getDefaultPrinterType() == ThermalPrinterConstant.THERMAL_PRINTER) {
		var txnId = that.id.split(':')[0];
		var PrintUtil = require('./../Utilities/PrintUtil.js');
		PrintUtil.printTransactionUsingThermalPrinter(txnId);
	} else {
		var html = transactionPDFHandler.transactionToHTML(that.id);
		if (html) {
			pdfHandler.print(html);
		}
	}
};

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	// $('#frameDiv').load('');
	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ display: 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable();
};

/// ///////////////////////////PDF/////////////////////////////////////

function openPrintOptions() {
	$('#orderItemPrintDialog').removeClass('hide');
	$('#orderItemPrintDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closePrintOptions() {
	$('#orderItemPrintDialog').addClass('hide');
	$('#orderItemPrintDialog').dialog('close').dialog('destroy');
}

var openPdfFromRightClickMenu = function openPdfFromRightClickMenu(that) {
	$('#loading').show(function () {
		var html = transactionPDFHandler.transactionToHTML(that.id);
		if (html) {
			pdfHandler.openPDF(html, false);
		}
	});
};

var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.ORDER_ITEM_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#orderItemPrintDialog').addClass('hide');
	$('#orderItemPrintDialog').dialog('close').dialog('destroy');
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

function openExcelOptions() {
	$('#ExcelDialog').removeClass('hide');
	$('#ExcelDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

if (settingCache.getItemEnabled()) {
	$('#itemDetailsExcel').show();
} else {
	$('#itemDetailsExcel').hide();
}

function closeExcelOptions() {
	$('#ExcelDialog').addClass('hide');
	$('#ExcelDialog').dialog('close').dialog('destroy');
}

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.ORDER_ITEM_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
	excelHelper.saveExcel(fileName);
};

var prepareObjectForExcel = function prepareObjectForExcel(listOfTransactions) {
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var shouldPrintItemQuantity = $('#ExcelCheck #printItemQuantity').is(':checked');
	var shouldPrintItemAmount = $('#ExcelCheck #printItemAmount').is(':checked');
	var rowObj = [];
	var len = listOfTransactions.length;

	startDateString = $('#startDate').val();
	endDateString = $('#endDate').val();
	rowObj.push(['', 'Order Item Report', '']);
	rowObj.push(['', startDateString + ' to ' + endDateString, '']);
	rowObj.push([]);

	var headers = [];
	headers.push('Item Name');
	if (shouldPrintItemQuantity) {
		headers.push('Quantity');
	}
	if (shouldPrintItemAmount) {
		headers.push('Amount');
	}
	rowObj.push(headers);

	var totalQuantity = 0;
	var totalFreeQuantity = 0;
	var totalAmount = 0;
	var tempArray = [];
	for (var _i2 = 0; _i2 < len; _i2++) {
		var txn = listOfTransactions[_i2];
		var itemName = txn.getItemName();
		var qty = txn.getQty();
		var freeQty = txn.getFreeQty();
		var amount = txn.getAmount();

		totalQuantity += qty;
		totalFreeQuantity += freeQty;
		totalAmount += amount;

		tempArray.push(itemName);
		if (shouldPrintItemQuantity) {
			tempArray.push(qty);
		}
		if (shouldPrintItemAmount) {
			tempArray.push(amount);
		}
		rowObj.push(tempArray);
		tempArray = [];
	}

	rowObj.push([]);
	if (shouldPrintItemQuantity) {
		rowObj.push(['', 'Total Quantity', totalQuantity]);
	}
	if (shouldPrintItemAmount) {
		rowObj.push(['', 'Total Amount', totalAmount]);
	}

	rowObj.push([]);
	return rowObj;
};

function printExcel() {
	closeExcelOptions();
	var XLSX = require('xlsx');
	var dataArray = prepareObjectForExcel(listOfTransactions);
	var worksheet = XLSX.utils.aoa_to_sheet(dataArray);
	var ExcelExportHelper = require('./../UIControllers/ExcelExportHelper.js');
	var excelExportHelper = new ExcelExportHelper();

	var workbook = {
		SheetNames: [],
		Sheets: {}
	};

	var wsName = 'Order Item Transaction Report';
	var lenCols = 4;
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet;
	var wscolWidth = new Array(lenCols).fill({ wch: 30 });
	worksheet['!cols'] = wscolWidth;

	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

/// //////////////////////////////////////////////////////////////

populateTable();