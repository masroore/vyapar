var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FirmSettingUtility = function () {
	var FirmCache = require('./../Cache/FirmCache.js');
	var firmCache = new FirmCache();
	var firmCount = 0;
	var maxFirmCount = 5;
	var UDFFieldConstant = require('./../Constants/UDFFieldsConstant.js');
	var addNewFirm = function addNewFirm(callback) {
		MyAnalytics.pushEvent('Add Firm Open');
		firmCount = firmCache ? (0, _keys2.default)(firmCache.getFirmList()).length : 0;
		if (firmCount < maxFirmCount) {
			var component = require('../UIComponent/jsx/AddEditFirmDialog').default;
			var MountComponent = require('../UIComponent/jsx/MountComponent').default;
			MountComponent(component, document.getElementById('addEditFirmDialog'), {
				onClose: callback
			});
		} else {
			ToastHelper.error('Cannot create more than 5 firm');
		}
	};

	var removeUDFFields = function removeUDFFields() {
		for (var i = 1; i <= UDFFieldConstant.TOTAL_FIRM_EXTRA_FIELDS; i++) {
			if ($('#extraHeaderField' + i + 'Div').length > 0) {
				$('#extraHeaderField' + i + 'Div').remove();
			}
		}
	};

	var updateFirm = function updateFirm(firmId, callback) {
		var component = require('../UIComponent/jsx/AddEditFirmDialog').default;
		var MountComponent = require('../UIComponent/jsx/MountComponent').default;
		MountComponent(component, document.getElementById('addEditFirmDialog'), {
			firmId: firmId,
			onClose: callback
		});
	};

	function updateLeadsInfoToFalse() {
		var SettingsModel = require('./../Models/SettingsModel.js');
		var settingsModel = new SettingsModel();
		settingsModel.setSettingKey(Queries.SETTING_LEADS_INFO_SENT);
		settingsModel.UpdateSetting('0', { nonSyncableSetting: true });
	}

	function setDefaultFirm(firmId) {
		var SettingsModel = require('./../Models/SettingsModel.js');
		var settingsModel = new SettingsModel();
		settingsModel.setSettingKey(Queries.SETTING_DEFAULT_FIRM_ID);
		settingsModel.setSettingValue(firmId);
		var statusCode = settingsModel.UpdateSetting(firmId);
		updateCompanyInfo();
		updateLeadsInfoToFalse();
		return statusCode;
	}

	return {
		updateFirm: updateFirm,
		setDefaultFirm: setDefaultFirm,
		removeUDFFields: removeUDFFields,
		addNewFirm: addNewFirm
	};
}();

module.exports = FirmSettingUtility;