var DateFormat = require('./../Constants/DateFormat.js');
var ColorConstants = require('./../Constants/ColorConstants.js');
var MyGraph = require('../Utilities/GraphHelper.js');

var Chart = require('chart.js');
$('#startDate, #endDate ').datepicker({ dateFormat: DateFormat.format });
var date = MyDate.getDate('d/m/y');

$('#startDate').val(MyDate.getFirstDateOfCurrentYearString());
$('#endDate').val(date);

var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();

var expenseGraph = document.getElementById('expenseGraph');

var expenseChartInReport = null;

var listOfExpense;

var showGraph = function showGraph() {
	settingCache = new SettingCache();
	var DataLoader = require('./../DBManager/DataLoader.js');
	var dataLoader = new DataLoader();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var firmId = Number($('#firmFilterOptions').val());
	var startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
	var endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');
	CommonUtility.showLoader(function () {
		listOfExpense = dataLoader.loadExpenseForGraph(startDate, endDate, firmId);

		var expense_date = [];
		var expense = [];

		var data = MyGraph.getData(listOfExpense.expense, 'daily');

		expense_date = data.x_axis;
		expense = data.y_axis;

		displayGraph(expense_date, expense);
	});
};

var showDailyExpenseGraph = function showDailyExpenseGraph() {
	var data = MyGraph.getData(listOfExpense.expense, 'daily');
	expenseChartInReport.data.labels = data.x_axis;
	expenseChartInReport.data.datasets[0].data = data.y_axis;
	expenseChartInReport.update();
};

var showWeeklyExpenseGraph = function showWeeklyExpenseGraph() {
	var data = MyGraph.getData(listOfExpense.expense, 'weekly');
	expenseChartInReport.data.labels = data.x_axis;
	expenseChartInReport.data.datasets[0].data = data.y_axis;
	expenseChartInReport.update();
};

var showMonthlyExpenseGraph = function showMonthlyExpenseGraph() {
	var data = MyGraph.getData(listOfExpense.expense, 'monthly');
	expenseChartInReport.data.labels = data.x_axis;
	expenseChartInReport.data.datasets[0].data = data.y_axis;
	expenseChartInReport.update();
};

var showYearlyExpenseGraph = function showYearlyExpenseGraph() {
	var data = MyGraph.getData(listOfExpense.expense, 'yearly');
	expenseChartInReport.data.labels = data.x_axis;
	expenseChartInReport.data.datasets[0].data = data.y_axis;
	expenseChartInReport.update();
};

if (settingCache.getMultipleFirmEnabled()) {
	var FirmCache = require('./../Cache/FirmCache.js');
	var firmCache = new FirmCache();
	var firmList = firmCache.getFirmList();
	var optionTemp = document.createElement('option');
	var option_list = [];
	for (i in firmList) {
		var option = optionTemp.cloneNode();
		option.text = firmList[i].getFirmName();
		option.value = firmList[i].getFirmId();
		option_list.push(option);
	}
	$('#firmFilterOptions').append(option_list);
	$('#firmChooser').show();
} else {
	$('#firmChooser').hide();
}

$('#firmFilterOptions').on('selectmenuchange', function (event) {
	showGraph();
});

$('#startDate').change(function () {
	showGraph();
});

$('#endDate').change(function () {
	showGraph();
});

$('.timespan').click(function () {
	$('.timespan').removeClass('timespanactive');
	$(this).addClass('timespanactive');

	var type = $(this).attr('value'); // document.getElementById("displaytype");
	// type = type.options[type.selectedIndex].value;
	if (type == 'daily') {
		showDailyExpenseGraph();
	} else if (type == 'weekly') {
		showWeeklyExpenseGraph();
	} else if (type == 'monthly') {
		showMonthlyExpenseGraph();
	} else if (type == 'yearly') {
		showYearlyExpenseGraph();
	}
});

var displayGraph = function displayGraph(expense_date, expense) {
	var options = MyGraph.getGraphFormatOption();

	$('.timespan').removeClass('timespanactive');
	$('.daily-span').addClass('timespanactive');

	var ctx = document.getElementById('expenseGraph').getContext('2d');
	var gradientStroke = ctx.createLinearGradient(0, 5000, 0, 0);
	gradientStroke.addColorStop(0, ColorConstants.graphbg);
	gradientStroke.addColorStop(1, ColorConstants.white);

	var expenseData = {
		labels: expense_date,
		datasets: [{
			label: '',
			fillOpacity: 0,
			strokeColor: ColorConstants.graphStrokeColor,
			pointColor: ColorConstants.graphPointColor,
			backgroundColor: gradientStroke,
			borderColor: ColorConstants.graphStrokeColor,
			borderWidth: 3,
			pointBorderColor: ColorConstants.graphPointColor,
			pointBackgroundColor: ColorConstants.white,
			pointHoverRadius: 5,
			pointHoverBackgroundColor: ColorConstants.graphPointColor,
			pointHoverBorderColor: ColorConstants.graphPointColor,
			pointHoverBorderWidth: 2,
			pointRadius: 1,
			pointHitRadius: 10,
			data: expense,
			spanGaps: false
		}]
	};

	if (expenseChartInReport) {
		expenseChartInReport.data = expenseData;
		expenseChartInReport.update();
	} else {
		expenseChartInReport = new Chart(expenseGraph, {
			type: 'line',
			data: expenseData,
			options: options

		});
	}
};

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}
	showGraph();
};

showGraph();

function loadReportPage(reportFileName) {
	if (document.getElementById('reportPage')) {
		changeReportPage(reportFileName);
	} else {
		$('#defaultPage').load(reportFileName);
	}
}