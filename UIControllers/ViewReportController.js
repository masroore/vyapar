module.exports = function () {
	var SettingCache = require('./../Cache/SettingCache.js');
	var settingCache = new SettingCache();

	function onResume() {
		settingCache.reloadSettingCache();
		if (settingCache.getItemEnabled() && settingCache.getStockEnabled()) {
			$('#lowStockSummaryReportOption').show();
			$('#stockSummaryReportOption').show();
			$('#itemDetailReportOption').show();
			$('#stockDetailReportOption').show();
			if (settingCache.isAnyAdditionalItemDetailsEnabled()) {
				$('#itemStockTrackingReport').show();
			}
		} else {
			$('#lowStockSummaryReportOption').hide();
			$('#stockSummaryReportOption').hide();
			$('#itemDetailReportOption').hide();
			$('#stockDetailReportOption').hide();
			$('#itemStockTrackingReport').hide();
		}

		if (settingCache.isPaymentTermEnabled()) {
			$('.saleAgingReport').show();
		} else {
			$('.saleAgingReport').hide();
		}

		if (settingCache.getTaxEnabled() || settingCache.isItemwiseTaxEnabled()) {
			$('#taxReport').show();
		} else {
			$('#taxReport').hide();
		}

		if (!settingCache.getDiscountEnabled() && !settingCache.isItemwiseDiscountEnabled()) {
			$('#discountReport').hide();
		} else {
			$('#discountReport').show();
		}

		if (settingCache.isOrderFormEnabled()) {
			$('.orderRelatedReport').show();
		} else {
			$('.orderRelatedReport').hide();
		}

		if (settingCache.isExtraIncomeEnabled()) {
			$('.extraIncomeReportLinks').show();
		} else {
			$('.extraIncomeReportLinks').hide();
		}

		if (settingCache.isItemCategoryEnabled()) {
			$('#itemCategorySalePurchase').show();
		} else {
			$('#itemCategorySalePurchase').hide();
		}

		if (settingCache.getStockEnabled() && settingCache.isItemCategoryEnabled()) {
			$('#itemCategoryStockSummary').show();
		} else {
			$('#itemCategoryStockSummary').hide();
		}

		if (settingCache.getGSTEnabled()) {
			$('#gstReportSection').show();
			if (!settingCache.isCompositeSchemeEnabled()) {
				$('#GSTR1Report').show();
				$('#GSTR2Report').show();
				$('#GSTR4Report').hide();
				$('#GSTR9AReport').hide();
				$('#GSTR9Report').show();
			} else {
				$('#GSTR1Report').hide();
				$('#GSTR2Report').hide();
				$('#GSTR4Report').show();
				$('#GSTR9AReport').show();
				$('#GSTR9Report').hide();
			}
		} else {
			$('#gstReportSection').hide();
			$('#GSTR1Report').hide();
			$('#GSTR2Report').hide();
			$('#GSTR3BReport').hide();
			$('#GSTR4Report').hide();
			$('#GSTR9AReport').hide();
			$('#GSTR9Report').hide();
		}

		if (settingCache.isPartyGroupEnabled()) {
			$('.groupEnabled').show();
		} else {
			$('.groupEnabled').hide();
		}

		if (settingCache.getTaxEnabled() || settingCache.isItemwiseTaxEnabled()) {
			$('#taxRateReport').show();
		} else {
			$('#taxRateReport').hide();
		}

		$('#bankStatementOption').show();

		// keyboard events for tax rate list
		$('#listOfReportsName').on('focus', 'li', function () {
			$this = $(this);
			$this.addClass('itemsWhenClicked').siblings().removeClass('itemsWhenClicked');
		}).on('keydown', '.currentItem', function (e) {
			$this = $(this);
			var id = this.id;
			if (e.keyCode == 40) {
				$this.next().focus();
				return false;
			} else if (e.keyCode == 38) {
				$this.prev().focus();
				return false;
			}
		});
	}
	window.onResume = onResume;
	onResume();

	$('#listOfReportsName').on('click', '[data-url]', function () {
		changeReportPage($(this).data('url'));
	});

	window.changeReportPage = function (page, onLoadCallback) {
		var reportName = page.split('.')[0];

		var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
		var mountComponent = require('../UIComponent/jsx/MountComponent').default;
		MyAnalytics.pushEvent(reportName, { Action: 'Click' });
		if (page === 'SaleAgeingReport') {
			var SaleAgingReportComponent = require('../UIComponent/jsx/SaleAgeingReportContainer').default;
			mountComponent(SaleAgingReportComponent, document.getElementById('reportPage'));
		} else if (page === 'SaleReport') {
			var Component = require('../UIComponent/jsx/SalePurchaseReportContainer').default;
			mountComponent(Component, document.getElementById('reportPage'), {
				txnType: TxnTypeConstant.TXN_TYPE_SALE,
				includeReturnTransaction: true
			});
		} else if (page === 'PurchaseReport') {
			var Component = require('../UIComponent/jsx/SalePurchaseReportContainer').default;
			mountComponent(Component, document.getElementById('reportPage'), {
				txnType: TxnTypeConstant.TXN_TYPE_PURCHASE,
				includeReturnTransaction: true
			});
		} else if (page === 'LoanStatement.html') {
			var _Component = require('../UIComponent/jsx/Reports/LoanAccountReportContainer').default;
			mountComponent(_Component, document.getElementById('reportPage'));
		} else {
			$('#reportPage').load(page, onLoadCallback);
		}
	};
};