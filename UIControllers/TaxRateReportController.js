var startDate;
var endDate;
var DateFormat = require('./../Constants/DateFormat.js');
$('#startDate, #endDate').datepicker({ dateFormat: DateFormat.format });
var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');

var printForSharePDF = false;

var listOfTransactions = [];

var populateTable = function populateTable() {
	//
	var DataLoader = require('./../DBManager/DataLoader.js');
	var dataLoader = new DataLoader();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
	endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');
	var firmId = Number($('#firmFilterOptions').val());
	CommonUtility.showLoader(function () {
		listOfTransactions = dataLoader.getTaxListForTaxRateReport(startDate, endDate, firmId);
		displayTransactionList(listOfTransactions);
	});
};

var date = MyDate.getDate('d/m/y');
$('#startDate').val(MyDate.getFirstDateOfCurrentMonthString());
$('#endDate').val(date);

if (settingCache.getMultipleFirmEnabled()) {
	var optionTemp = document.createElement('option');
	var FirmCache = require('./../Cache/FirmCache.js');
	var firmCache = new FirmCache();
	var firmList = firmCache.getFirmList();
	for (i in firmList) {
		var option = optionTemp.cloneNode();
		option.text = firmList[i].getFirmName();
		option.value = firmList[i].getFirmId();
		$('#firmFilterOptions').append(option);
	}
	$('#firmChooser').show();
} else {
	$('#firmChooser').hide();
}

$('#firmFilterOptions').on('selectmenuchange', function (event) {
	populateTable();
});

$('#startDate').change(function () {
	populateTable();
});

$('#endDate').change(function () {
	populateTable();
});

var taxRateReportClusterize;

var displayTransactionList = function displayTransactionList(listOfTransactions) {
	var len = listOfTransactions.length;
	var rowData = [];
	var dynamicRow = '';
	dynamicRow += "<thead><tr><th width='20%'>Tax Name</th><th width='12%' class='tableCellTextAlignRight'>Tax Percent</th><th width='17%' class='tableCellTextAlignRight'>Taxable Sale Amount </th><th width='17%' class='tableCellTextAlignRight'>Tax In</th><th width='17%' class='tableCellTextAlignRight'>Taxable Purchase Amount</th><th width='17%' class='tableCellTextAlignRight'>Tax Out</th></tr></thead>";
	//
	var totalTaxIn = 0;
	var totalTaxOut = 0;
	for (var _i = 0; _i < len; _i++) {
		var row = '';
		var txn = listOfTransactions[_i];
		var taxName = txn.getTaxName();
		var taxIn = txn.getTaxIn();
		var taxOut = txn.getTaxOut();
		var taxPercent = txn.getTaxPercent();
		var sale_taxable = txn.getSaleTaxableValue();
		var purchase_taxable = txn.getPurchaseTaxableValue();
		row += "<tr><td width='20%'>" + taxName + '</td>';

		row += "<td width='12%' class='tableCellTextAlignRight'>";
		if (taxPercent == -1) {
			row += ' - ';
		} else {
			row += MyDouble.getPercentageWithDecimal(taxPercent) + '%';
		}
		if (taxName == 'Additional CESS') {
			row += "</td><td width='17%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(sale_taxable) + ' (Quantity)</td>';
		} else {
			row += "</td><td width='17%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(sale_taxable) + '</td>';
		}
		row += "<td width='17%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(taxIn) + '</td>';
		if (taxName == 'Additional CESS') {
			row += "<td width='17%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(purchase_taxable) + ' (Quantity)</td>';
		} else {
			row += "<td width='17%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(purchase_taxable) + '</td>';
		}
		row += "<td width='17%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(taxOut) + '</td>';
		row += '</tr>';
		totalTaxIn += taxIn;
		totalTaxOut += taxOut;
		rowData.push(row);
	}
	var data = rowData;
	if ($('#taxRateReportContainer').length === 0) {
		return;
	}
	if (taxRateReportClusterize && taxRateReportClusterize.destroy) {
		taxRateReportClusterize.clear();
		taxRateReportClusterize.destroy();
	}

	taxRateReportClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollArea',
		contentId: 'contentArea',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});

	$('#tableHead').html(dynamicRow);
	$('#totalTaxInAmount').html(MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalTaxIn));
	$('#totalTaxOutAmount').html(MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalTaxOut));
};

var openTransaction = function openTransaction(txnId, txnType) {
	var ViewTransaction = require('./../BizLogic/ViewTransaction.js');
	var viewTransaction = new ViewTransaction();
	viewTransaction.viewTransactionFile(txnId + ':' + TxnTypeConstant.getTxnType(txnType));
};

var getHTMLTextForReport = function getHTMLTextForReport() {
	var TaxRateReportHTMLGenerator = require('./../ReportHTMLGenerator/TaxRateReportHTMLGenerator.js');
	var taxRateReportHTMLGenerator = new TaxRateReportHTMLGenerator();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var firmId = Number($('#firmFilterOptions').val());
	var htmlText = taxRateReportHTMLGenerator.getHTMLText(listOfTransactions, startDateString, endDateString, firmId);
	return htmlText;
};

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable();
};

/// ///////////////////////////PDF/////////////////////////////////////
var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.TAX_RATE_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.TAX_RATE_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
	excelHelper.saveExcel(fileName);
};

var prepareObjectForExcel = function prepareObjectForExcel(listOfTransactions) {
	//
	var len = listOfTransactions.length;
	var rowObj = [];
	var tableHeadArray = [];
	tableHeadArray.push('Tax Name');
	tableHeadArray.push('Tax Percent');
	tableHeadArray.push('Taxable Sale Amount');
	tableHeadArray.push('Tax In');
	tableHeadArray.push('Taxable Purchase Amount');
	tableHeadArray.push('Tax Out');
	rowObj.push(tableHeadArray);
	rowObj.push([]);

	var totalTaxIn = 0;
	var totalTaxOut = 0;
	for (var _i2 = 0; _i2 < len; _i2++) {
		var tempArray = [];
		var txn = listOfTransactions[_i2];
		var taxName = txn.getTaxName();
		var taxIn = txn.getTaxIn();
		var taxOut = txn.getTaxOut();
		var taxPercent = txn.getTaxPercent();
		var sale_taxable = txn.getSaleTaxableValue();
		var purchase_taxable = txn.getPurchaseTaxableValue();
		tempArray.push(taxName);
		tempArray.push(MyDouble.getPercentageWithDecimal(taxPercent));
		if (taxName == 'Additional CESS') {
			tempArray.push(MyDouble.getQuantityWithDecimalWithoutColor(sale_taxable) + ' (Quantity)');
		} else {
			tempArray.push(MyDouble.getAmountWithDecimal(sale_taxable));
		}
		tempArray.push(MyDouble.getAmountWithDecimal(taxIn));
		if (taxName == 'Additional CESS') {
			tempArray.push(MyDouble.getQuantityWithDecimalWithoutColor(purchase_taxable) + ' (Quantity)');
		} else {
			tempArray.push(MyDouble.getAmountWithDecimal(purchase_taxable));
		}
		tempArray.push(MyDouble.getAmountWithDecimal(taxOut));

		totalTaxIn += taxIn;
		totalTaxOut += taxOut;
		rowObj.push(tempArray);
	}
	rowObj.push([]);
	var totalArray = ['', 'Total', '', MyDouble.getAmountWithDecimal(totalTaxIn), '', MyDouble.getAmountWithDecimal(totalTaxOut)];
	rowObj.push(totalArray);
	return rowObj;
};

function printExcel() {
	var XLSX = require('xlsx');
	var dataArray = prepareObjectForExcel(listOfTransactions);
	var worksheet = XLSX.utils.aoa_to_sheet(dataArray);

	var workbook = {
		'SheetNames': [],
		'Sheets': {}
	};

	var wsName = 'Tax Rate Report';
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet;
	var wscolWidth = [{ wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }];
	worksheet['!cols'] = wscolWidth;
	console.log(workbook);
	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

populateTable();