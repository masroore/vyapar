var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var startDate;
var endDate;
var DateFormat = require('./../Constants/DateFormat.js');
$('#startDate, #endDate').datepicker({ dateFormat: DateFormat.format });

var TxnPaymentStatusConstants = require('./../Constants/TxnPaymentStatusConstants.js');

var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var NameCache = require('./../Cache/NameCache.js');
var nameCache = new NameCache();

var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();

var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');
var PaymentCache = require('./../Cache/PaymentInfoCache.js');
var paymentCache = new PaymentCache();

var printForSharePDF = false;

var listOfTransactions = [];

var totalDataObj = {};

var populateTable = function populateTable() {
	var DataLoader = require('./../DBManager/DataLoader.js');
	var dataLoader = new DataLoader();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var nameString = $('#nameForReport').val();
	var firmId = Number($('#firmFilterOptions').val());
	nameCache = new NameCache();
	settingCache = new SettingCache();

	var nameId = 0;

	if (nameString) {
		var nameModel = nameCache.findNameModelByName(nameString);
		if (nameModel) {
			nameId = nameModel.getNameId();
		}
	}

	startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
	endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');
	CommonUtility.showLoader(function () {
		listOfTransactions = dataLoader.loadListForItemCategorySalePurchaseReport(nameId, startDate, endDate, firmId);
		displayTransactionList(listOfTransactions);
	});
};

var date = MyDate.getDate('d/m/y');
$('#startDate').val(MyDate.getFirstDateOfCurrentMonthString());
$('#endDate').val(date);

$('#startDate').change(function () {
	populateTable();
});

$('#endDate').change(function () {
	populateTable();
});

if (settingCache.getMultipleFirmEnabled()) {
	var optionTemp = document.createElement('option');
	var FirmCache = require('./../Cache/FirmCache.js');
	var firmCache = new FirmCache();
	var firmList = firmCache.getFirmList();
	for (i in firmList) {
		var option = optionTemp.cloneNode();
		option.text = firmList[i].getFirmName();
		option.value = firmList[i].getFirmId();
		$('#firmFilterOptions').append(option);
	}
	$('#firmChooser').show();
} else {
	$('#firmChooser').hide();
}

$('#firmFilterOptions').on('selectmenuchange', function (event) {
	populateTable();
});

$(function () {
	$('#nameForReport').autocomplete(UIHelper.getAutocompleteDefaultOptions(nameCache.getListOfNames()));
});

$('#nameForReport').on('change', function (event) {
	populateTable();
});

var salePurchaseReportClusterize;

var displayTransactionList = function displayTransactionList(listOfTransactions) {
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var dynamicRow = '';
	var rowData = [];

	dynamicRow += "<thead><tr><th width='28%'>Item Category</th><th width='18%' class='tableCellTextAlignRight'>Sale Quantity</th><th width='18%' class='tableCellTextAlignRight'>Total Sale Amount</th><th width='18%' class='tableCellTextAlignRight'>Purchase Quantity</th><th width='18%' class='tableCellTextAlignRight'>Total Purchase Amount</th></tr></thead>";

	var totalSale = 0;
	var totalSaleFree = 0;
	var totalPurchase = 0;
	var totalPurchaseFree = 0;
	var totalSaleAmt = 0;
	var totalPurchaseAmt = 0;
	var _iteratorNormalCompletion = true;
	var _didIteratorError = false;
	var _iteratorError = undefined;

	try {
		for (var _iterator = (0, _getIterator3.default)(listOfTransactions), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
			var _ref = _step.value;

			var _ref2 = (0, _slicedToArray3.default)(_ref, 2);

			var key = _ref2[0];
			var value = _ref2[1];

			var itemLine = listOfTransactions.get(key);
			var itemName = itemLine.get('name');
			var saleQty = itemLine.get(TxnTypeConstant.TXN_TYPE_SALE);
			var purchaseQty = itemLine.get(TxnTypeConstant.TXN_TYPE_PURCHASE);
			var purchaseReturnQty = itemLine.get(TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN);
			var saleReturnQty = itemLine.get(TxnTypeConstant.TXN_TYPE_SALE_RETURN);

			var netSaleQty = (saleQty == null ? 0 : saleQty[0]) - (saleReturnQty == null ? 0 : saleReturnQty[0]);
			var netSaleFreeQty = (saleQty == null ? 0 : saleQty[1]) - (saleReturnQty == null ? 0 : saleReturnQty[1]);
			var netPurchaseQty = (purchaseQty == null ? 0 : purchaseQty[0]) - (purchaseReturnQty == null ? 0 : purchaseReturnQty[0]);
			var netPurchaseFreeQty = (purchaseQty == null ? 0 : purchaseQty[1]) - (purchaseReturnQty == null ? 0 : purchaseReturnQty[1]);

			var netSaleAmt = (saleQty == null ? 0 : saleQty[2]) - (saleReturnQty == null ? 0 : saleReturnQty[2]);
			var netPurchaseAmt = (purchaseQty == null ? 0 : purchaseQty[2]) - (purchaseReturnQty == null ? 0 : purchaseReturnQty[2]);
			var row = '';
			row += "<tr><td width='28%'>" + itemName + '</td>';

			row += "<td width='18%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(netSaleQty) + MyDouble.quantityDoubleToStringWithSignExplicitly(netSaleFreeQty, true) + '</td>';
			row += "<td width='18%' class='tableCellTextAlignRight'>" + MyDouble.getAmountForInvoicePrint(netSaleAmt) + '</td>';
			row += "<td width='18%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(netPurchaseQty) + MyDouble.quantityDoubleToStringWithSignExplicitly(netPurchaseFreeQty, true) + '</td>';
			row += "<td width='18%' class='tableCellTextAlignRight'>" + MyDouble.getAmountForInvoicePrint(netPurchaseAmt) + '</td>';

			row += '</tr>';
			rowData.push(row);
			totalSale += netSaleQty;
			totalSaleFree += netSaleFreeQty;
			totalPurchase += netPurchaseQty;
			totalPurchaseFree += netPurchaseFreeQty;
			totalSaleAmt += netSaleAmt;
			totalPurchaseAmt += netPurchaseAmt;
		}
	} catch (err) {
		_didIteratorError = true;
		_iteratorError = err;
	} finally {
		try {
			if (!_iteratorNormalCompletion && _iterator.return) {
				_iterator.return();
			}
		} finally {
			if (_didIteratorError) {
				throw _iteratorError;
			}
		}
	}

	var data = rowData;

	if ($('#itemCategorySalePurchaseReportContainer').length === 0) {
		return;
	}
	if (salePurchaseReportClusterize && salePurchaseReportClusterize.destroy) {
		salePurchaseReportClusterize.clear();
		salePurchaseReportClusterize.destroy();
	}

	salePurchaseReportClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollArea',
		contentId: 'contentArea',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});
	$('#tableHead').html(dynamicRow);

	$('#totalSaleQuantity').html(MyDouble.getQuantityWithDecimalWithoutColor(totalSale) + MyDouble.quantityDoubleToStringWithSignExplicitly(totalSaleFree, true));
	$('#totalSaleAmt').html(MyDouble.getAmountForInvoicePrint(totalSaleAmt));
	$('#totalPurchaseQuantity').html(MyDouble.getQuantityWithDecimalWithoutColor(totalPurchase) + MyDouble.quantityDoubleToStringWithSignExplicitly(totalPurchaseFree, true));
	$('#totalPurchaseAmt').html(MyDouble.getAmountForInvoicePrint(totalPurchaseAmt));
};

var getHTMLTextForReport = function getHTMLTextForReport() {
	var salePurchaseReportByItemCategoryHTMLGenerator = require('./../ReportHTMLGenerator/salePurchaseReportByItemCategoryHTMLGenerator.js');
	var salePurchaseReportByItemCategoryHTMLGenerator = new salePurchaseReportByItemCategoryHTMLGenerator();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var nameString = $('#nameForReport').val();
	var firmId = Number($('#firmFilterOptions').val());

	var htmlText = salePurchaseReportByItemCategoryHTMLGenerator.getHTMLText(listOfTransactions, startDateString, endDateString, nameString, firmId);
	return htmlText;
};

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	// $('#frameDiv').load('');
	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable();
};

/// ///////////////////////////PDF/////////////////////////////////////

var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.ITEM_CATEGORY_SALE_PURCHASE_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
}

function sharePDF() {
	$('#loading').show(function () {
		var receiverEmail = '';
		var nameId = 0;
		var nameString = $('#nameForReport').val();
		if (nameString) {
			var nameModel = nameCache.findNameModelByName(nameString);
			if (nameModel) {
				receiverEmail = nameModel.getEmail();
			}
		}
		if (receiverEmail) {
			pdfHandler.sharePDF(receiverEmail);
		} else {
			pdfHandler.sharePDF();
		}
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#salePurchaseReportByItemCategoryPrintDialog').addClass('hide');
	$('#salePurchaseReportByItemCategoryPrintDialog').dialog('close').dialog('destroy');
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

if (settingCache.getItemEnabled()) {
	$('#itemDetailsExcel').show();
} else {
	$('#itemDetailsExcel').hide();
}

function openExcelOptions() {
	$('#ExcelDialog').removeClass('hide');
	$('#ExcelDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closeExcelOptions() {
	$('#ExcelDialog').addClass('hide');
	$('#ExcelDialog').dialog('close').dialog('destroy');
}

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.ITEM_CATEGORY_SALE_PURCHASE_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
	excelHelper.saveExcel(fileName);
};

var prepareObjectForExcel = function prepareObjectForExcel(listOfTransactions) {
	console.log(listOfTransactions);
	var selectedMode = Number($('input[name=viewMode]:checked').val());
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');

	var rowObj = [];
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var nameString = $('#nameForReport').val();

	var nameId = 0;

	if (nameString) {
		var nameModel = nameCache.findNameModelByName(nameString);
		if (nameModel) {
			rowObj.push(['Party Name', nameModel.getFullName()]);
			if (nameModel.getPhoneNumber()) {
				rowObj.push(['Party Contact', nameModel.getPhoneNumber()]);
			}
			if (nameModel.getEmail()) {
				rowObj.push(['Party Email', nameModel.getEmail()]);
			}
			if (nameModel.getAddress()) {
				rowObj.push(['Party Address', nameModel.getAddress()]);
			}
			if (settingCache.getGSTEnabled()) {
				if (nameModel.getGstinNumber()) {
					rowObj.push(['Party GSTIN', nameModel.getGstinNumber()]);
				}
			} else {
				if (nameModel.getTinNumber()) {
					rowObj.push(['Party ' + settingCache.getTINText(), nameModel.getTinNumber()]);
				}
			}
			rowObj.push([]);
			rowObj.push(['Duration', startDateString + ' to ' + endDateString]);
			rowObj.push([]);
			rowObj.push([]);
		}
	}

	var tableHeadArray = [];
	var totalArray = [];

	tableHeadArray.push('Item Category');
	tableHeadArray.push('Sale Quantity');
	tableHeadArray.push('Total Sale Amount');
	tableHeadArray.push('Purchase Quantity');
	tableHeadArray.push('Total Purchase Amount');

	rowObj.push(tableHeadArray);
	rowObj.push([]);

	var totalSale = 0;
	var totalSaleFree = 0;
	var totalPurchase = 0;
	var totalPurchaseFree = 0;
	var totalSaleAmt = 0;
	var totalPurchaseAmt = 0;

	var _iteratorNormalCompletion2 = true;
	var _didIteratorError2 = false;
	var _iteratorError2 = undefined;

	try {
		for (var _iterator2 = (0, _getIterator3.default)(listOfTransactions), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
			var _ref3 = _step2.value;

			var _ref4 = (0, _slicedToArray3.default)(_ref3, 2);

			var key = _ref4[0];
			var value = _ref4[1];

			var tempArray = [];
			var itemLine = listOfTransactions.get(key);
			var itemName = itemLine.get('name');
			var saleQty = itemLine.get(TxnTypeConstant.TXN_TYPE_SALE);
			var purchaseQty = itemLine.get(TxnTypeConstant.TXN_TYPE_PURCHASE);
			var purchaseReturnQty = itemLine.get(TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN);
			var saleReturnQty = itemLine.get(TxnTypeConstant.TXN_TYPE_SALE_RETURN);

			var netSaleQty = (saleQty == null ? 0 : saleQty[0]) - (saleReturnQty == null ? 0 : saleReturnQty[0]);
			var netSaleFreeQty = (saleQty == null ? 0 : saleQty[1]) - (saleReturnQty == null ? 0 : saleReturnQty[1]);
			var netPurchaseQty = (purchaseQty == null ? 0 : purchaseQty[0]) - (purchaseReturnQty == null ? 0 : purchaseReturnQty[0]);
			var netPurchaseFreeQty = (purchaseQty == null ? 0 : purchaseQty[1]) - (purchaseReturnQty == null ? 0 : purchaseReturnQty[1]);

			var netSaleAmt = (saleQty == null ? 0 : saleQty[2]) - (saleReturnQty == null ? 0 : saleReturnQty[2]);
			var netPurchaseAmt = (purchaseQty == null ? 0 : purchaseQty[2]) - (purchaseReturnQty == null ? 0 : purchaseReturnQty[2]);

			tempArray.push(itemName);

			tempArray.push(MyDouble.getQuantityWithDecimalWithoutColor(netSaleQty) + MyDouble.quantityDoubleToStringWithSignExplicitly(netSaleFreeQty, true));
			tempArray.push(MyDouble.getAmountForInvoicePrint(netSaleAmt));

			tempArray.push(MyDouble.getQuantityWithDecimalWithoutColor(netPurchaseQty) + MyDouble.quantityDoubleToStringWithSignExplicitly(netPurchaseFreeQty, true));
			tempArray.push(MyDouble.getAmountForInvoicePrint(netPurchaseAmt));

			totalSale += netSaleQty;
			totalSaleFree += netSaleFreeQty;
			totalPurchase += netPurchaseQty;
			totalPurchaseFree += netPurchaseFreeQty;
			totalSaleAmt += netSaleAmt;
			totalPurchaseAmt += netPurchaseAmt;

			rowObj.push(tempArray);
		}
	} catch (err) {
		_didIteratorError2 = true;
		_iteratorError2 = err;
	} finally {
		try {
			if (!_iteratorNormalCompletion2 && _iterator2.return) {
				_iterator2.return();
			}
		} finally {
			if (_didIteratorError2) {
				throw _iteratorError2;
			}
		}
	}

	rowObj.push([]);
	totalArray.push('Total');
	totalArray.push(MyDouble.getQuantityWithDecimalWithoutColor(totalSale) + MyDouble.quantityDoubleToStringWithSignExplicitly(totalSaleFree, true));
	totalArray.push(MyDouble.getAmountForInvoicePrint(totalSaleAmt));
	totalArray.push(MyDouble.getQuantityWithDecimalWithoutColor(totalPurchase) + MyDouble.quantityDoubleToStringWithSignExplicitly(totalPurchaseFree, true));
	totalArray.push(MyDouble.getAmountForInvoicePrint(totalPurchaseAmt));
	rowObj.push(totalArray);
	return rowObj;
};

function printExcel() {
	closeExcelOptions();
	var XLSX = require('xlsx');
	var dataArray = prepareObjectForExcel(listOfTransactions);
	var worksheet = XLSX.utils.aoa_to_sheet(dataArray);
	var ExcelItemDetails = $('#ExcelPartyItemDetails').is(':checked');
	var ExcelExportHelper = require('./../UIControllers/ExcelExportHelper.js');
	var excelExportHelper = new ExcelExportHelper();

	var workbook = {
		'SheetNames': [],
		'Sheets': {}
	};

	var wsName = 'ItemCategorySalePurchase Report';
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet;
	var wscolWidth = [{ wch: 15 }, { wch: 30 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	worksheet['!cols'] = wscolWidth;

	if (ExcelItemDetails) {
		var wsNameItem = 'Item Details';
		workbook.SheetNames[1] = wsNameItem;
		var itemArray = excelExportHelper.prepareItemObjectForExcel(listOfTransactions);
		var itemWorksheet = XLSX.utils.aoa_to_sheet(itemArray);
		workbook.Sheets[wsNameItem] = itemWorksheet;
		var wsItemcolWidth = [{ wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }];
		itemWorksheet['!cols'] = wsItemcolWidth;
	}
	console.log(workbook);
	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

populateTable();