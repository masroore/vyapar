var istListForButton;

$('.terminalButtonItemAdj').on('click', function () {
	var ErrorCode = require('./../Constants/ErrorCode.js');
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var ItemCache = require('./../Cache/ItemCache.js');
	var itemcache = new ItemCache();
	var itemName = document.getElementById('itemName').value;
	itemName = itemName.trim();
	var adjustmentType = document.getElementById('adjustmentType').value;
	var quantity = document.getElementById('quantity').value;
	var date = document.getElementById('datepicker').value;
	// var getDate = new GetDate();
	date = MyDate.getDateObj(date, 'dd/mm/yyyy', '/');
	// console.log(date);
	var details = document.getElementById('details').value;
	var statusCode = ErrorCode.SUCCESS;
	var itemId = 0;
	var itemAdjTypeValue = 0;
	var atPrice = $('#atPrice').val();

	var istData = {
		'istMRP': $('#mrp').val().trim(),
		'istBatchNo': $('#batchNo').val().trim(),
		'istExpDate': MyDate.getDateByMonthYearString($('#expDate').val()),
		'istMfgDate': MyDate.getDateByMonthYearString($('#mfgDate').val()),
		'istSerialNo': $('#serialNo').val().trim(),
		'istSize': $('#size').val().trim()
	};

	if (!itemName) {
		statusCode = ErrorCode.ERROR_ITEM_NAME_EMPTY;
	} else if (!itemcache.itemExists(itemName)) {
		statusCode = ErrorCode.ERROR_ITEM_ADJ_NEW_ITEM;
	} else {
		itemId = itemcache.findItemByName(itemName, itemAdjTypeValue).getItemId();
	}

	if (!quantity) {
		statusCode = ErrorCode.ERROR_ITEM_ADJ_QUANTITY_EMPTY;
		ToastHelper.error(statusCode);
	}
	if (statusCode == ErrorCode.SUCCESS) {
		if (adjustmentType == StringConstant.itemAddAdjustmentString) {
			itemAdjTypeValue = TxnTypeConstant.TXN_TYPE_ADJ_ADD;
		} else {
			itemAdjTypeValue = TxnTypeConstant.TXN_TYPE_ADJ_REDUCE;
		}
		MyAnalytics.pushEvent('Item Adjustment Save');
		var ItemAdjustmentTxn = require('./../BizLogic/ItemAdjustmentTxn.js');
		var itemAdjustmentTxn = new ItemAdjustmentTxn();

		if (itemAdjId) {
			itemAdjustmentTxn.loadItemAdjustmentTxn(itemAdjId);
			statusCode = itemAdjustmentTxn.updateItemAdjustment(itemId, itemAdjTypeValue, quantity, date, details, atPrice, istData);
		} else {
			statusCode = itemAdjustmentTxn.addItemAdjustment(itemId, itemAdjTypeValue, quantity, date, details, atPrice, istData);
		}
		if (statusCode == ErrorCode.ERROR_ITEM_ADJ_SAVE_FAILED) {
			ToastHelper.error(statusCode);
		} else {
			ToastHelper.success(statusCode);
		}
		$('#modelContainer').css('display', 'none');
		$('.viewItems').css('display', 'block');
		itemAdjustmentIdGlobal = '';
		$('#frameDiv *').unbind().html('');
		$('#defaultPage').show();
		onResume();
	}
});

$('#itemtrackingDialogShow').on('click', function () {
	var curItem = $('#currentItemName')[0].textContent;
	var ItemCache = require('./../Cache/ItemCache.js');
	var itemCache = new ItemCache();
	var itemObj = itemCache.findItemByName(curItem);
	if (itemObj) {
		var itemId = itemObj.getItemId();
		var ItemStockTracking = require('./../BizLogic/ItemStockTracking.js');
		var itemStockTracking = new ItemStockTracking();
		var itemStockTrackingList = itemStockTracking.getItemStockTrackingData(itemId, false, null, false, false, null, 0, 0);

		showTrackingListDialog(itemStockTrackingList);
	}
});

var showTrackingListDialog = function showTrackingListDialog(itemStockTrackingList) {
	if (settingCache.isAnyAdditionalItemDetailsEnabled()) {
		var mrpColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_MRP);
		var batchNumberColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_BATCH_NUMBER);
		var serialNumberColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_SERIAL_ITEM_NUMBER);
		var mfgDateColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_MANUFACTURING_DATE);
		var expDateColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_EXPIRY_DATE);
		var sizeColumnVisibility = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_SIZE);

		var html = "<thead><tr class='listHeaderlist textLeft' style='border-collapse:collapse'><th width='1%' class='textAlignCentre'></th>" + "<th width='15%'" + (mrpColumnVisibility ? '' : 'hidden') + '> ' + settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_MRP_VALUE) + ' </th>' + "<th width='15%'" + (batchNumberColumnVisibility ? '' : 'hidden') + '> ' + settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_BATCH_NUMBER_VALUE) + ' </th>' + "<th width='15%' " + (serialNumberColumnVisibility ? '' : 'hidden') + '> ' + settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_SERIAL_NUMBER_VALUE) + ' </th>' + "<th width='15%' " + (mfgDateColumnVisibility ? '' : 'hidden') + '> ' + settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_MANUFACTURING_DATE_VALUE) + ' </th>' + "<th width='10%' " + (expDateColumnVisibility ? '' : 'hidden') + '> ' + settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_EXPIRY_DATE_VALUE) + ' </th>' + "<th width='12%' " + (sizeColumnVisibility ? '' : 'hidden') + '> ' + settingCache.getAdditionalItemDetailsHeaderValue(Queries.SETTING_ITEM_SIZE_VALUE) + ' </th>';
		html += "<th width='12%'>Current Quantity</th></tr></thead><tbody>";

		for (var i = 0; i < itemStockTrackingList.length; i++) {
			var txn = itemStockTrackingList[i];
			var istId = txn.getIstId();
			var istBatchNumber = txn.getIstBatchNumber() ? txn.getIstBatchNumber() : '';
			var istManufacturingDate = txn.getIstManufacturingDate() ? txn.getIstManufacturingDate() : '';
			var istExpiryDate = txn.getIstExpiryDate() ? txn.getIstExpiryDate() : '';
			var istSerialNumber = txn.getIstSerialNumber() ? txn.getIstSerialNumber() : '';
			var istSize = txn.getIstSize() ? txn.getIstSize() : '';
			var istMrp = txn.getIstMrp() ? txn.getIstMrp() : '';
			var currentQuantity = txn.getIstCurrentQuantity();

			if (istExpiryDate && settingCache.getExpiryDateFormat() == StringConstant.monthYear) {
				istExpiryDate = MyDate.getDate('m/y', istExpiryDate);
			} else if (istExpiryDate && settingCache.getExpiryDateFormat() == StringConstant.dateMonthYear) {
				istExpiryDate = MyDate.getDate('d/m/y', istExpiryDate);
			}

			if (istManufacturingDate && settingCache.getManufacturingDateFormat() == StringConstant.monthYear) {
				istManufacturingDate = MyDate.getDate('m/y', istManufacturingDate);
			} else if (istManufacturingDate && settingCache.getManufacturingDateFormat() == StringConstant.dateMonthYear) {
				istManufacturingDate = MyDate.getDate('d/m/y', istManufacturingDate);
			}

			if (i == 0) {
				html += "<tr class='currentRow itemsWhenClicked' style='height: 35px;'><td class='tdSpacing'><input  type='radio'  class='istSelect' name='itemSelectForAdjustment' value='" + istId + "'checked></td>" + "<td class='tdSpacing' " + (mrpColumnVisibility ? '' : 'hidden') + '>' + istMrp + '</td>' + "<td class='tdSpacing' " + (batchNumberColumnVisibility ? '' : 'hidden') + '>' + istBatchNumber + '</td>' + "<td class='tdSpacing' " + (serialNumberColumnVisibility ? '' : 'hidden') + '>' + istSerialNumber + '</td>' + "<td class='tdSpacing' " + (mfgDateColumnVisibility ? '' : 'hidden') + '>' + istManufacturingDate + '</td>' + "<td class='tdSpacing' " + (expDateColumnVisibility ? '' : 'hidden') + '>' + istExpiryDate + '</td>' + "<td class='tdSpacing' " + (sizeColumnVisibility ? '' : 'hidden') + '>' + istSize + '</td>';
			} else {
				html += "<tr class='currentRow' style='height: 35px;'><td class='tdSpacing'><input type='radio' class='istSelect' name='itemSelectForAdjustment' value='" + istId + "'></td>" + "<td class='tdSpacing' " + (mrpColumnVisibility ? '' : 'hidden') + '>' + istMrp + '</td>' + "<td class='tdSpacing' " + (batchNumberColumnVisibility ? '' : 'hidden') + '>' + istBatchNumber + '</td>' + "<td class='tdSpacing' " + (serialNumberColumnVisibility ? '' : 'hidden') + '>' + istSerialNumber + '</td>' + "<td class='tdSpacing' " + (mfgDateColumnVisibility ? '' : 'hidden') + '>' + istManufacturingDate + '</td>' + "<td class='tdSpacing' " + (expDateColumnVisibility ? '' : 'hidden') + '>' + istExpiryDate + '</td>' + "<td class='tdSpacing' " + (sizeColumnVisibility ? '' : 'hidden') + '>' + istSize + '</td>';
			}

			html += "<td class='tdSpacing'> " + MyDouble.getQuantityWithDecimalWithoutColor(currentQuantity) + ' </td></tr>';
		}
		html += '</tbody>';

		$('#stockTrackingListForAdjustment').html(html);

		$('#itemtrackingListDialogForAdjustment').dialog({
			'title': 'Items',
			'width': '1000',
			'closeOnEscape': false,
			appendTo: '#dynamicDiv',
			modal: true,
			close: function close(event, ui) {
				$(this).dialog('destroy');
			}
		});
	}
};

var fillISTDataInItemAdjustment = function fillISTDataInItemAdjustment(itemStockTrackingData) {
	var mrp = itemStockTrackingData.getIstMrp();
	var batchNo = itemStockTrackingData.getIstBatchNumber();
	var expDate = '';
	var mfgDate = '';
	var serialNo = itemStockTrackingData.getIstSerialNumber();
	var size = itemStockTrackingData.getIstSize();

	if (itemStockTrackingData.getIstExpiryDate() && settingCache.getExpiryDateFormat() == StringConstant.monthYear) {
		expDate = MyDate.getDate('m/y', itemStockTrackingData.getIstExpiryDate());
	} else if (itemStockTrackingData.getIstExpiryDate() && settingCache.getExpiryDateFormat() == StringConstant.dateMonthYear) {
		expDate = MyDate.getDate('d/m/y', itemStockTrackingData.getIstExpiryDate());
	}

	if (itemStockTrackingData.getIstManufacturingDate() && settingCache.getManufacturingDateFormat() == StringConstant.monthYear) {
		mfgDate = MyDate.getDate('m/y', itemStockTrackingData.getIstManufacturingDate());
	} else if (itemStockTrackingData.getIstManufacturingDate() && settingCache.getManufacturingDateFormat() == StringConstant.dateMonthYear) {
		mfgDate = MyDate.getDate('d/m/y', itemStockTrackingData.getIstManufacturingDate());
	}

	$('#mrp').val(mrp);
	$('#batchNo').val(batchNo);
	$('#expDate').val(expDate);
	$('#mfgDate').val(mfgDate);
	$('#serialNo').val(serialNo);
	$('#size').val(size);
};

var ItemCache = require('./../Cache/ItemCache.js');
var itemCache = new ItemCache();
var ItemStockTracking = require('./../BizLogic/ItemStockTracking.js');
var itemStockTracking = new ItemStockTracking();
if (itemAdjId) {
	document.getElementById('title').innerText = 'Edit Item Adjustment';
	var DataLoader = require('./../DBManager/DataLoader.js');
	var dataloader = new DataLoader();

	var itemAdjRow = dataloader.getItemAdjRowForItemAdjId(itemAdjId);
	itemAdjRow = itemAdjRow[0];

	var itemStockTrackingData = itemStockTracking.getItemStockTrackingDataByIstId(itemAdjRow.getItemAdjIstId());

	istListForButton = [];
	istListForButton = itemStockTrackingData;

	fillISTDataInItemAdjustment(itemStockTrackingData);

	var oldItemAdjAtPrice = MyDouble.convertStringToDouble(itemAdjRow.getItemAdjAtPrice(), true);

	if (oldItemAdjAtPrice >= 0) {
		$('#atPrice').val(oldItemAdjAtPrice + '');
	}

	console.log(itemAdjRow);
	var itemName = itemAdjRow['itemAdjItemId'];
	itemName = itemCache.findItemNameById(itemName);
	$('#itemName').val(itemName);
	document.getElementById('adjustmentType').value = Number(itemAdjRow['itemAdjType']) == 12 ? 'Reduce Stock' : 'Add Stock';
	document.getElementById('quantity').value = itemAdjRow['itemAdjQuantity'];
	var date = MyDate.getDate('d/m/y', itemAdjRow['itemAdjDate']);
	document.getElementById('datepicker').value = date;
	document.getElementById('details').value = itemAdjRow['itemAdjDescription'];
} else {
	var curSelectedItem = $('#itemName').val();
	var itemObj = itemCache.findItemByName(curSelectedItem);
	if (itemObj) {
		var itemId = itemObj.getItemId();
		var itemStockTrackingList = itemStockTracking.getItemStockTrackingData(itemId, false, null, false, false, null, 0, 0);

		istListForButton = [];
		istListForButton = itemStockTrackingList;

		if (itemStockTrackingList.length == 1) {
			var itemStockTrackingData = itemStockTrackingList[0];
			fillISTDataInItemAdjustment(itemStockTrackingData);
		} else if (itemStockTrackingList.length > 1) {
			showTrackingListDialog(itemStockTrackingList);
		}
	}
}

$('#stockTrackingListForAdjustment').on('click focus', '.currentRow', function () {
	$this = $(this);
	$this.find('input[name=itemSelectForAdjustment]').prop('checked', true);
	$this.siblings().find('input[class=quantity]').val('');
	$this.addClass('itemsWhenClicked').siblings().removeClass('itemsWhenClicked');
});

$('#stockTrackingListForAdjustment').on('keydown', '.currentRow', function (e) {
	$this = $(this);
	var id = this.id;
	if (e.keyCode == 40) {
		$this.next().focus();
		return false;
	} else if (e.keyCode == 38) {
		$this.prev().focus();
		return false;
	}
});

$('#itemtrackingListDialogForAdjustment').on('click', '#fillSelectedItemToAdjustment', function () {
	if ($('input[name=itemSelectForAdjustment]:checked').length < 1) {
		ToastHelper.error('Please select item.');
		return;
	}
	var istId = $('input[name=itemSelectForAdjustment]:checked')[0].value;
	var itemStockTracking = new ItemStockTracking();
	var itemStockTrackingData = itemStockTracking.getItemStockTrackingDataByIstId(istId);
	fillISTDataInItemAdjustment(itemStockTrackingData);

	$('#itemtrackingListDialogForAdjustment').dialog('close');
	$('#itemtrackingListDialogForAdjustment').css('display', 'none');
});
if (istListForButton.length == 0 || !settingCache.isAnyAdditionalItemDetailsEnabled()) {
	$('#itemtrackingDialogShow').addClass('hide');
}