var DateFormat = require('./../Constants/DateFormat.js');
var ColorConstants = require('./../Constants/ColorConstants.js');
var Chart = require('chart.js');
var MyGraph = require('../Utilities/GraphHelper.js');

$('#startDate, #endDate ').datepicker({ dateFormat: DateFormat.format });
var date = MyDate.getDate('d/m/y');

$('#startDate').val(MyDate.getFirstDateOfCurrentYearString());
$('#endDate').val(date);

var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();

var salesGraph = document.getElementById('salesGraph');
var salesChartInReport = null;

var listOfSales;

var showGraph = function showGraph() {
	settingCache = new SettingCache();
	var DataLoader = require('./../DBManager/DataLoader.js');
	var dataLoader = new DataLoader();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var firmId = Number($('#firmFilterOptions').val());
	var startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
	var endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');
	CommonUtility.showLoader(function () {
		listOfSales = dataLoader.loadSalesForGraph(startDate, endDate, firmId);

		var sales_date = [];
		var sales = [];

		var data = MyGraph.getData(listOfSales.sales, 'daily');
		sales_date = data.x_axis;
		sales = data.y_axis;
		displayGraph(sales_date, sales);
	});
};

var showDailySalesGraph = function showDailySalesGraph() {
	var data = MyGraph.getData(listOfSales.sales, 'daily');
	salesChartInReport.data.labels = data.x_axis;
	salesChartInReport.data.datasets[0].data = data.y_axis;
	salesChartInReport.update();
};

var showWeeklySalesGraph = function showWeeklySalesGraph() {
	var data = MyGraph.getData(listOfSales.sales, 'weekly');
	salesChartInReport.data.labels = data.x_axis;
	salesChartInReport.data.datasets[0].data = data.y_axis;
	salesChartInReport.update();
};

var showMonthlySalesGraph = function showMonthlySalesGraph() {
	var data = MyGraph.getData(listOfSales.sales, 'monthly');
	salesChartInReport.data.labels = data.x_axis;
	salesChartInReport.data.datasets[0].data = data.y_axis;
	salesChartInReport.update();
};

var showYearlySalesGraph = function showYearlySalesGraph() {
	var data = MyGraph.getData(listOfSales.sales, 'yearly');
	salesChartInReport.data.labels = data.x_axis;
	salesChartInReport.data.datasets[0].data = data.y_axis;
	salesChartInReport.update();
};

if (settingCache.getMultipleFirmEnabled()) {
	var FirmCache = require('./../Cache/FirmCache.js');
	var firmCache = new FirmCache();
	var firmList = firmCache.getFirmList();
	var optionTemp = document.createElement('option');
	var option_list = [];
	for (i in firmList) {
		var option = optionTemp.cloneNode();
		option.text = firmList[i].getFirmName();
		option.value = firmList[i].getFirmId();
		option_list.push(option);
	}
	$('#firmFilterOptions').append(option_list);
	$('#firmChooser').show();
} else {
	$('#firmChooser').hide();
}

$('#firmFilterOptions').on('selectmenuchange', function (event) {
	showGraph();
});

$('#startDate').change(function () {
	showGraph();
});

$('#endDate').change(function () {
	showGraph();
});

$('.timespan').click(function () {
	$('.timespan').removeClass('timespanactive');
	$(this).addClass('timespanactive');

	var type = $(this).attr('value');
	if (type == 'daily') {
		showDailySalesGraph();
	} else if (type == 'weekly') {
		showWeeklySalesGraph();
	} else if (type == 'monthly') {
		showMonthlySalesGraph();
	} else if (type == 'yearly') {
		showYearlySalesGraph();
	}
});

var displayGraph = function displayGraph(sales_date, sales) {
	var options = MyGraph.getGraphFormatOption();
	$('.timespan').removeClass('timespanactive');
	$('.daily-span').addClass('timespanactive');

	var ctx = document.getElementById('salesGraph').getContext('2d');
	var gradientStroke = ctx.createLinearGradient(0, 5000, 0, 0);
	gradientStroke.addColorStop(0, ColorConstants.graphbg);
	gradientStroke.addColorStop(1, ColorConstants.white);

	var salesData = {
		labels: sales_date,
		datasets: [{
			label: '',
			fillOpacity: 0,
			strokeColor: ColorConstants.graphStrokeColor,
			pointColor: ColorConstants.graphPointColor,
			backgroundColor: gradientStroke,
			borderColor: ColorConstants.graphStrokeColor,
			borderWidth: 3,
			pointBorderColor: ColorConstants.graphPointColor,
			pointBackgroundColor: ColorConstants.white,
			pointHoverRadius: 5,
			pointHoverBackgroundColor: ColorConstants.graphPointColor,
			pointHoverBorderColor: ColorConstants.graphPointColor,
			pointHoverBorderWidth: 2,
			pointRadius: 1,
			pointHitRadius: 10,
			data: sales,
			spanGaps: false
		}]
	};

	if (salesChartInReport) {
		salesChartInReport.data = salesData;
		salesChartInReport.update();
	} else {
		salesChartInReport = new Chart(salesGraph, {
			type: 'line',
			data: salesData,
			options: options
		});
	}
};

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ display: 'none' });
		$('.viewItems').css('display', 'block');
	}
	showGraph();
};

showGraph();

function loadReportPage(reportFileName) {
	if (window.HIDE_GRAPH_PAGE) {
		window.HIDE_GRAPH_PAGE();
	} else {
		var MountComponent = require('../UIComponent/jsx/MountComponent').default;
		var Component = require('../UIComponent/jsx/SalePurchaseReportContainer').default;
		var TxnTypeConstant = require('../Constants/TxnTypeConstant');
		MountComponent(Component, document.querySelector('#defaultPage'), {
			txnType: TxnTypeConstant.TXN_TYPE_SALE,
			includeReturnTransaction: true
		});
	}
}