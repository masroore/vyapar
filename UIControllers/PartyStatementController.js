var startDate;
var endDate;
var DateFormat = require('./../Constants/DateFormat.js');
$('#startDate, #endDate').datepicker({ dateFormat: DateFormat.format });

var TxnPaymentStatusConstants = require('./../Constants/TxnPaymentStatusConstants.js');

var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var NameCache = require('./../Cache/NameCache.js');
var nameCache = new NameCache();

var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();

var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();

var PaymentCache = require('./../Cache/PaymentInfoCache.js');
var paymentCache = new PaymentCache();

var printForSharePDF = false;

var listOfTransactions = [];

var totalDataObj = {};

var populateTable = function populateTable() {
	var DataLoader = require('./../DBManager/DataLoader.js');
	var dataLoader = new DataLoader();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var nameString = $('#nameForReport').val();
	nameCache = new NameCache();
	settingCache = new SettingCache();
	paymentCache = new PaymentCache();

	var nameId = 0;
	var selectedMode = Number($('input[name=viewMode]:checked').val());
	if (selectedMode == 1) {
		$('#summaryInTable').show();
	} else {
		$('#summaryInTable').hide();
	}

	if (nameString) {
		var nameModel = nameCache.findNameModelByName(nameString);
		if (nameModel) {
			nameId = nameModel.getNameId();
		}
	}

	startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
	endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');
	CommonUtility.showLoader(function () {
		listOfTransactions = dataLoader.loadTransactionsForPartyStatement(startDate, endDate, nameId, selectedMode);
		displayTransactionList(listOfTransactions);
	});
};

var date = MyDate.getDate('d/m/y');
$('#startDate').val(MyDate.getFirstDateOfCurrentMonthString());
$('#endDate').val(date);

$('#startDate').change(function () {
	populateTable();
});

$('#endDate').change(function () {
	populateTable();
});

$('.expand-icon-statement').off('click').on('click', function () {
	$(this).parent().find('.expand-icon-statement svg').toggleClass('rotate180');
	$('#partyStatementSummaryDetails').slideToggle();
});

if (settingCache.isBillToBillEnabled()) {
	$('#paymentStatusPrintDiv').removeClass('hide');

	$('#paymentStatusExcel').removeClass('hide');
} else {
	$('#paymentStatusPrintDiv').addClass('hide');

	$('#paymentStatusExcel').addClass('hide');
}

$(function () {
	$('#nameForReport').autocomplete(UIHelper.getAutocompleteDefaultOptions(nameCache.getListOfNames()));
});

$('input[name=viewMode]').on('change', function (event) {
	populateTable();
});

$('#nameForReport').on('change', function (event) {
	populateTable();
});

var ShowPaymentStatusForTheseTransactions = [TxnTypeConstant.TXN_TYPE_SALE, TxnTypeConstant.TXN_TYPE_PURCHASE, TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN, TxnTypeConstant.TXN_TYPE_SALE_RETURN, TxnTypeConstant.TXN_TYPE_CASHIN, TxnTypeConstant.TXN_TYPE_CASHOUT, TxnTypeConstant.TXN_TYPE_ROPENBALANCE, TxnTypeConstant.TXN_TYPE_POPENBALANCE];

var partyStatementClusterize;

var displayTransactionList = function displayTransactionList(listOfTransactions) {
	var selectedMode = Number($('input[name=viewMode]:checked').val());
	var len = listOfTransactions.length;
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var dynamicRow = '';
	var rowData = [];
	var dynamicFooter = '';
	var isTransactionRefNumberEnabled = settingCache.getTxnRefNoEnabled();
	var isBillToBillEnabled = settingCache.isBillToBillEnabled();
	var isVyaparMode = selectedMode != 1;

	var dateColumnWidthRatio = 14;
	var txnTypeColumnWidthRatio = 13;
	var txnRefColumnWidthRatio = isTransactionRefNumberEnabled ? 8 : 0;
	var txnPaymentTypeColumnWidthRatio = 8;
	var txnPaymentStatusColumnWidthRatio = isBillToBillEnabled ? 14 : 0;
	var totalAmountColumnWidthRatio = isVyaparMode ? 14 : 0;
	var receivedPaidAmountColumnWidthRatio = isVyaparMode ? 13 : 0;
	var balanceAmountColumnWidthRatio = isVyaparMode ? 13 : 0;
	var receivableBalanceAmountColumnWidthRatio = isVyaparMode ? 13 : 0;
	var payableBalanceAmountColumnWidthRatio = isVyaparMode ? 13 : 0;
	var debitAmountColumnWidthRatio = !isVyaparMode ? 15 : 0;
	var creditAmountColumnWidthRatio = !isVyaparMode ? 15 : 0;
	var runningBalanceAmountColumnWidthRatio = !isVyaparMode ? 15 : 0;

	var totalColumnWidth = dateColumnWidthRatio + txnTypeColumnWidthRatio + txnRefColumnWidthRatio + txnPaymentTypeColumnWidthRatio + txnPaymentStatusColumnWidthRatio + totalAmountColumnWidthRatio + receivedPaidAmountColumnWidthRatio + balanceAmountColumnWidthRatio + receivableBalanceAmountColumnWidthRatio + payableBalanceAmountColumnWidthRatio + debitAmountColumnWidthRatio + creditAmountColumnWidthRatio + runningBalanceAmountColumnWidthRatio;

	if (selectedMode == 1) {
		dynamicRow += "<thead><tr><th width='" + dateColumnWidthRatio * 100 / totalColumnWidth + "%'>Date</th><th width='" + txnTypeColumnWidthRatio * 100 / totalColumnWidth + "%'>Txn Type</th>";
		dynamicFooter += "<thead><tr><th width='" + dateColumnWidthRatio * 100 / totalColumnWidth + "%'></th><th width='" + txnTypeColumnWidthRatio * 100 / totalColumnWidth + "%'>Total</th>";
		if (isTransactionRefNumberEnabled) {
			dynamicRow += "<th width='" + txnRefColumnWidthRatio * 100 / totalColumnWidth + "%'>Ref No.</th>";
			dynamicFooter += "<th width='" + txnRefColumnWidthRatio * 100 / totalColumnWidth + "%'></th>";
		}
		dynamicRow += '<th width=\'' + txnPaymentTypeColumnWidthRatio * 100 / totalColumnWidth + '%\'>Payment Type</th>';
		dynamicFooter += "<th width='" + txnPaymentTypeColumnWidthRatio * 100 / totalColumnWidth + "%'></th>";
		if (isBillToBillEnabled) {
			dynamicRow += '<th width=\'' + txnPaymentStatusColumnWidthRatio * 100 / totalColumnWidth + '%\'>Payment Status</th>';
			dynamicFooter += "<th width='" + txnPaymentStatusColumnWidthRatio * 100 / totalColumnWidth + "%'></th>";
		}
		dynamicRow += "<th width='" + debitAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>Debit</th><th width='" + creditAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>Credit</th><th width='" + runningBalanceAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>Running Balance</th></tr></thead>";
	} else {
		dynamicRow += "<thead><tr><th width='" + dateColumnWidthRatio * 100 / totalColumnWidth + "%'>Date</th><th width='" + txnTypeColumnWidthRatio * 100 / totalColumnWidth + "%'>Txn Type</th>";
		dynamicFooter += "<thead><tr><th width='" + dateColumnWidthRatio * 100 / totalColumnWidth + "%'></th><th width='" + txnTypeColumnWidthRatio * 100 / totalColumnWidth + "%'>Total</th>";
		if (isTransactionRefNumberEnabled) {
			dynamicRow += "<th width='" + txnRefColumnWidthRatio * 100 / totalColumnWidth + "%'>Ref No.</th>";
			dynamicFooter += "<th width='" + txnRefColumnWidthRatio * 100 / totalColumnWidth + "%'></th>";
		}
		dynamicRow += '<th width=\'' + txnPaymentTypeColumnWidthRatio * 100 / totalColumnWidth + '%\'>Payment Type</th>';
		dynamicFooter += "<th width='" + txnPaymentTypeColumnWidthRatio * 100 / totalColumnWidth + "%'></th>";
		if (isBillToBillEnabled) {
			dynamicRow += '<th width=\'' + txnPaymentStatusColumnWidthRatio * 100 / totalColumnWidth + '%\'>Payment Status</th>';
			dynamicFooter += "<th width='" + txnPaymentStatusColumnWidthRatio * 100 / totalColumnWidth + "%'></th>";
		}
		dynamicRow += "<th width='" + totalAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>Total</th><th width='" + receivedPaidAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>Received / Paid</th><th width='" + balanceAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>Txn Balance</th><th width='" + receivableBalanceAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>Receivable Balance</th><th width='" + payableBalanceAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>Payable Balance</th></tr></thead><tbody>";
		dynamicFooter += "<th width='" + totalAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'></th><th width='" + receivedPaidAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'></th><th width='" + balanceAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'></th>";
	}
	var totalDr = 0;
	var totalCr = 0;
	var totalRunningBalance = 0;
	var totalSale = 0;
	var totalPurchase = 0;
	var totalMoneyIn = 0;
	var totalMoneyOut = 0;
	for (var i = 0; i < len; i++) {
		var txn = listOfTransactions[i];
		var txnType = txn.getTxnType();
		var date = MyDate.getDate('d/m/y', txn.getTxnDate());
		var typeString = TxnTypeConstant.getTxnTypeForUI(txnType);
		var cashAmt = txn.getCashAmount() ? Number(txn.getCashAmount()) : 0;
		var balanceAmt = txn.getBalanceAmount() ? Number(txn.getBalanceAmount()) : 0;
		var discountAmt = txn.getDiscountAmount() ? Number(txn.getDiscountAmount()) : 0;
		var totalAmt = 0;
		var paymentTypeId = txn.getPaymentTypeId() + '';
		var paymentInfoObj = paymentCache.getPaymentInfoObjById(paymentTypeId);
		var refNo = txn.getTxnRefNumber() === '' ? '' : txn.getFullTxnRefNumber();

		if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
			totalAmt = Number(cashAmt) + Number(discountAmt);
		} else {
			totalAmt = Number(cashAmt) + Number(balanceAmt);
		}
		if (cashAmt > 0) {
			var paymentName = paymentInfoObj.getName();
			var paymentRef = txn.getPaymentTypeReference();
		} else {
			var paymentName = '';
			var paymentRef = '';
		}

		var paymentStatus = txn.getTxnPaymentStatus();
		var paymentStatusString = TxnPaymentStatusConstants.getPaymentStatusForUI(txn.getTxnType(), paymentStatus);

		if (!ShowPaymentStatusForTheseTransactions.includes(txn.getTxnType())) {
			paymentStatusString = '';
		}

		var row = "<tr ondblclick='openTransaction(" + txn.getTxnId() + ',' + txn.getTxnType() + ")' class='currentRow'>";

		if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
			if (txnType == TxnTypeConstant.TXN_TYPE_SALE) {
				totalSale += totalAmt;
				totalMoneyIn += cashAmt;
			} else if (txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
				totalSale -= totalAmt;
				totalMoneyOut += cashAmt;
			} else if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE) {
				totalPurchase += totalAmt;
				totalMoneyOut += cashAmt;
			} else if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
				totalPurchase -= totalAmt;
				totalMoneyIn += cashAmt;
			} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN) {
				totalMoneyIn += cashAmt;
			} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
				totalMoneyOut += cashAmt;
			}
		}

		if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
			totalDr += balanceAmt;
			totalRunningBalance += balanceAmt;
		} else if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
			totalCr += balanceAmt;
			totalRunningBalance -= balanceAmt;
		} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_BEGINNING_BALANCE_PAYABLE || txnType == TxnTypeConstant.TXN_TYPE_POPENBALANCE) {
			totalCr += totalAmt;
			totalRunningBalance -= totalAmt;
		} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHOUT || txnType == TxnTypeConstant.TXN_TYPE_BEGINNING_BALANCE_RECEIVABLE || txnType == TxnTypeConstant.TXN_TYPE_ROPENBALANCE) {
			totalDr += totalAmt;
			totalRunningBalance += totalAmt;
		}

		row += "<td width='" + dateColumnWidthRatio * 100 / totalColumnWidth + "%'>" + date + "</td><td width='" + txnTypeColumnWidthRatio * 100 / totalColumnWidth + "%'>" + typeString + '</td>';
		if (isTransactionRefNumberEnabled) {
			row += "<td width='" + txnRefColumnWidthRatio * 100 / totalColumnWidth + "%'>" + refNo + '</td>';
		}
		row += "<td width='" + txnPaymentTypeColumnWidthRatio * 100 / totalColumnWidth + "%'>" + paymentName + (paymentRef ? ' (' + paymentRef + ')' : '') + '</td>';
		if (isBillToBillEnabled) {
			row += "<td width='" + txnPaymentStatusColumnWidthRatio * 100 / totalColumnWidth + "%'>" + paymentStatusString + '</td>';
		}
		if (selectedMode == 1) {
			if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
				row += "<td width='" + debitAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(balanceAmt) + "</td><td width='" + creditAmountColumnWidthRatio * 100 / totalColumnWidth + "'></td>";
			} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHOUT || txnType == TxnTypeConstant.TXN_TYPE_BEGINNING_BALANCE_RECEIVABLE || txnType == TxnTypeConstant.TXN_TYPE_ROPENBALANCE) {
				row += "<td  width='" + debitAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalAmt) + "</td><td width='" + creditAmountColumnWidthRatio * 100 / totalColumnWidth + "'></td>";
			} else if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
				row += "<td width='" + debitAmountColumnWidthRatio * 100 / totalColumnWidth + "'></td><td width='" + creditAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(balanceAmt) + '</td>';
			} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_BEGINNING_BALANCE_PAYABLE || txnType == TxnTypeConstant.TXN_TYPE_POPENBALANCE) {
				row += "<td width='" + debitAmountColumnWidthRatio * 100 / totalColumnWidth + "'></td><td width='" + creditAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalAmt) + '</td>';
			}
			if (totalRunningBalance < 0) {
				row += "<td width='" + runningBalanceAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalRunningBalance) + ' (Cr)</td>';
			} else {
				row += "<td width='" + runningBalanceAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalRunningBalance) + ' (Dr)</td>';
			}
		} else {
			if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
				row += "<td width='" + totalAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalAmt) + '</td>';
				row += "<td width='" + receivedPaidAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(cashAmt) + '</td>';
			} else {
				row += "<td width='" + totalAmountColumnWidthRatio * 100 / totalColumnWidth + "%'></td><td width='" + receivedPaidAmountColumnWidthRatio * 100 / totalColumnWidth + "%'></td>";
			}
			if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
				row += "<td width='" + balanceAmountColumnWidthRatio * 100 / totalColumnWidth + "%'></td>";
			} else {
				row += "<td width='" + balanceAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(balanceAmt) + '</td>';
			}
			if (totalRunningBalance < 0) {
				row += "<td width='" + receivableBalanceAmountColumnWidthRatio * 100 / totalColumnWidth + "'></td><td width='" + payableBalanceAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalRunningBalance) + '</td>';
			} else {
				row += "<td width='" + receivableBalanceAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalRunningBalance) + "</td><td width='" + payableBalanceAmountColumnWidthRatio * 100 / totalColumnWidth + "'></td>";
			}
		}
		row += '</tr>';

		rowData.push(row);
	}
	var data = rowData;

	$('#totalSale').text(MyDouble.getAmountForInvoicePrint(totalSale));
	$('#totalPurchase').text(MyDouble.getAmountForInvoicePrint(totalPurchase));
	$('#totalMoneyIn').text(MyDouble.getAmountForInvoicePrint(totalMoneyIn));
	$('#totalMoneyOut').text(MyDouble.getAmountForInvoicePrint(totalMoneyOut));

	if ($('#partyStatementContainer').length === 0) {
		return;
	}
	if (partyStatementClusterize && partyStatementClusterize.destroy) {
		partyStatementClusterize.clear();
		partyStatementClusterize.destroy();
	}

	partyStatementClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollArea',
		contentId: 'contentArea',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});
	$('#tableHead').html(dynamicRow);

	if (selectedMode == 1) {
		dynamicFooter += "<th width='" + debitAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalDr) + "</th><th width='" + creditAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalCr) + "</th><th width='" + runningBalanceAmountColumnWidthRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalRunningBalance) + (totalRunningBalance < 0 ? ' (Cr)' : ' (Dr)') + '</th></tr></thead>';
	} else {
		if (totalRunningBalance >= 0) {
			$('#totalReceivableAmount').text(MyDouble.getAmountForInvoicePrint(totalRunningBalance));
			$('#totalReceivableText').text('Total Receivable');
			$('#totalReceivableAmount').css('color', 'green');
		} else {
			$('#totalReceivableAmount').text(MyDouble.getAmountForInvoicePrint(Math.abs(totalRunningBalance)));
			$('#totalReceivableText').text('Total Payable');
			$('#totalReceivableAmount').css('color', 'red');
		}
	}
	$('#total').html(dynamicFooter);
};

var openTransaction = function openTransaction(txnId, txnType) {
	var ViewTransaction = require('./../BizLogic/ViewTransaction.js');
	var viewTransaction = new ViewTransaction();
	viewTransaction.viewTransactionFile(txnId + ':' + TxnTypeConstant.getTxnType(txnType));
};

if (settingCache.getItemEnabled()) {
	$('#itemDetailPrintDiv').show();
} else {
	$('#itemDetailPrintDiv').hide();
}

var getHTMLTextForReport = function getHTMLTextForReport() {
	var PartyStatementHTMLGenerator = require('./../ReportHTMLGenerator/PartyStatementHTMLGenerator.js');
	var partyStatementHTMLGenerator = new PartyStatementHTMLGenerator();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var nameString = $('#nameForReport').val();
	var selectedMode = Number($('input[name=viewMode]:checked').val());
	var printItemDetails = $('#printItems').is(':checked');
	var printDescription = $('#printDescription').is(':checked');
	var printPaymentInfo = $('#printPaymentInfo').is(':checked');
	var printPaymentStatus = $('#printPaymentStatus').is(':checked');
	var htmlText = partyStatementHTMLGenerator.getHTMLText(listOfTransactions, startDateString, endDateString, nameString, selectedMode, printItemDetails, printDescription, printPaymentInfo, printPaymentStatus);
	return htmlText;
};

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	// $('#frameDiv').load('');
	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable();
};

/// ///////////////////////////PDF/////////////////////////////////////

function openPrintOptions() {
	$('#partyStatementPrintDialog').removeClass('hide');
	$('#partyStatementPrintDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closePrintOptions() {
	$('#partyStatementPrintDialog').addClass('hide');
	$('#partyStatementPrintDialog').dialog('close').dialog('destroy');
}

var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: $('#nameForReport').val(), fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
}

function sharePDF() {
	$('#loading').show(function () {
		var receiverEmail = '';
		var nameId = 0;
		var nameString = $('#nameForReport').val();
		if (nameString) {
			var nameModel = nameCache.findNameModelByName(nameString);
			if (nameModel) {
				receiverEmail = nameModel.getEmail();
			}
		}
		if (receiverEmail) {
			pdfHandler.sharePDF(receiverEmail);
		} else {
			pdfHandler.sharePDF();
		}
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#partyStatementPrintDialog').addClass('hide');
	$('#partyStatementPrintDialog').dialog('close').dialog('destroy');
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

if (settingCache.getItemEnabled()) {
	$('#itemDetailsExcel').show();
} else {
	$('#itemDetailsExcel').hide();
}

function openExcelOptions() {
	$('#ExcelDialog').removeClass('hide');
	$('#ExcelDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closeExcelOptions() {
	$('#ExcelDialog').addClass('hide');
	$('#ExcelDialog').dialog('close').dialog('destroy');
}

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: $('#nameForReport').val(), fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
	excelHelper.saveExcel(fileName);
};

var prepareObjectForExcel = function prepareObjectForExcel(listOfTransactions) {
	console.log(listOfTransactions);
	var selectedMode = Number($('input[name=viewMode]:checked').val());
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var isTransactionRefNumberEnabled = settingCache.getTxnRefNoEnabled();
	var ExcelDescription = $('#ExcelPartyDescription').is(':checked');
	var ExcelPaymentInfo = $('#ExcelPaymentInfo').is(':checked');
	var ExcelPaymentStatus = $('#ExcelPaymentStatus').is(':checked');
	var len = listOfTransactions.length;
	var rowObj = [];
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var nameString = $('#nameForReport').val();

	var nameId = 0;

	if (nameString) {
		var nameModel = nameCache.findNameModelByName(nameString);
		if (nameModel) {
			rowObj.push(['Party Name', nameModel.getFullName()]);
			if (nameModel.getPhoneNumber()) {
				rowObj.push(['Party Contact', nameModel.getPhoneNumber()]);
			}
			if (nameModel.getEmail()) {
				rowObj.push(['Party Email', nameModel.getEmail()]);
			}
			if (nameModel.getAddress()) {
				rowObj.push(['Party Address', nameModel.getAddress()]);
			}
			if (settingCache.getGSTEnabled()) {
				if (nameModel.getGstinNumber()) {
					rowObj.push(['Party GSTIN', nameModel.getGstinNumber()]);
				}
			} else {
				if (nameModel.getTinNumber()) {
					rowObj.push(['Party ' + settingCache.getTINText(), nameModel.getTinNumber()]);
				}
			}
			rowObj.push([]);
			rowObj.push(['Duration', startDateString + ' to ' + endDateString]);
			rowObj.push([]);
			rowObj.push([]);
		}
	}

	var tableHeadArray = [];
	var totalArray = [];
	tableHeadArray.push('Date');
	tableHeadArray.push('Txn Type');
	if (isTransactionRefNumberEnabled) {
		tableHeadArray.push('Invoice/Bill No.');
		totalArray.push('');
	}
	if (selectedMode == 1) {
		tableHeadArray.push('Debit');
		tableHeadArray.push('Credit');
		tableHeadArray.push('Running Balance');
		totalArray.push('');
	} else {
		tableHeadArray.push('Total Amount');
		tableHeadArray.push('Received');
		tableHeadArray.push('Paid');
		tableHeadArray.push('Txn Balance');
		tableHeadArray.push('Receivable Balance');
		tableHeadArray.push('Payable Balance');
		totalArray.push('');
	}
	if (ExcelPaymentInfo) {
		tableHeadArray.push('Payment Type');
		tableHeadArray.push('Payment Ref');
	}
	if (ExcelPaymentStatus) {
		tableHeadArray.push('Payment Status');
	}
	if (ExcelDescription) {
		tableHeadArray.push('Description');
	}
	rowObj.push(tableHeadArray);
	rowObj.push([]);

	var totalDr = 0;
	var totalCr = 0;
	var totalRunningBalance = 0;

	var totalSale = 0;
	var totalPurchase = 0;
	var totalMoneyIn = 0;
	var totalMoneyOut = 0;
	for (var j = 0; j < len; j++) {
		var tempArray = [];
		var txn = listOfTransactions[j];
		var txnType = txn.getTxnType();
		var date = MyDate.getDate('d/m/y', txn.getTxnDate());
		var typeString = TxnTypeConstant.getTxnTypeForUI(txnType);
		var cashAmt = txn.getCashAmount() ? Number(txn.getCashAmount()) : 0;
		var balanceAmt = txn.getBalanceAmount() ? Number(txn.getBalanceAmount()) : 0;
		var discountAmt = txn.getDiscountAmount() ? Number(txn.getDiscountAmount()) : 0;
		var totalAmt = 0;
		var debitAmount = '';
		var creditAmount = '';
		var totalAmount = '';
		var paidAmount = '';
		var receivedAmount = '';
		var txnBalanceAmount = '';

		var paymentTypeId = txn.getPaymentTypeId() + '';
		var paymentInfoObj = paymentCache.getPaymentInfoObjById(paymentTypeId);

		if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
			totalAmt = Number(cashAmt) + Number(discountAmt);
		} else {
			totalAmt = Number(cashAmt) + Number(balanceAmt);
		}

		if (cashAmt > 0) {
			var paymentName = paymentInfoObj.getName();
			var paymentRef = txn.getPaymentTypeReference();
		} else {
			var paymentName = '';
			var paymentRef = '';
		}

		var paymentStatus = txn.getTxnPaymentStatus();
		var paymentStatusString = TxnPaymentStatusConstants.getPaymentStatusForUI(txn.getTxnType(), paymentStatus);

		if (!ShowPaymentStatusForTheseTransactions.includes(txn.getTxnType())) {
			paymentStatusString = '';
		}

		if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
			if (txnType == TxnTypeConstant.TXN_TYPE_SALE) {
				totalSale += totalAmt;
				totalMoneyIn += cashAmt;
			} else if (txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
				totalSale -= totalAmt;
				totalMoneyOut += cashAmt;
			} else if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE) {
				totalPurchase += totalAmt;
				totalMoneyOut += cashAmt;
			} else if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
				totalPurchase -= totalAmt;
				totalMoneyIn += cashAmt;
			} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN) {
				totalMoneyIn += cashAmt;
			} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
				totalMoneyOut += cashAmt;
			}
		}

		if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
			totalDr += balanceAmt;
			totalRunningBalance += balanceAmt;
		} else if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
			totalCr += balanceAmt;
			totalRunningBalance -= balanceAmt;
		} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_BEGINNING_BALANCE_PAYABLE || txnType == TxnTypeConstant.TXN_TYPE_POPENBALANCE) {
			totalCr += totalAmt;
			totalRunningBalance -= totalAmt;
		} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHOUT || txnType == TxnTypeConstant.TXN_TYPE_BEGINNING_BALANCE_RECEIVABLE || txnType == TxnTypeConstant.TXN_TYPE_ROPENBALANCE) {
			totalDr += totalAmt;
			totalRunningBalance += totalAmt;
		}

		tempArray.push(date);
		tempArray.push(typeString);
		if (isTransactionRefNumberEnabled) {
			var refNo = txn.getTxnRefNumber() === '' ? '' : txn.getFullTxnRefNumber();
			tempArray.push(refNo);
		}
		if (selectedMode == 1) {
			if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
				debitAmount = balanceAmt;
				tempArray.push(MyDouble.getAmountWithDecimal(debitAmount));
				tempArray.push('');
			} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHOUT || txnType == TxnTypeConstant.TXN_TYPE_BEGINNING_BALANCE_RECEIVABLE || txnType == TxnTypeConstant.TXN_TYPE_ROPENBALANCE) {
				debitAmount = totalAmt;
				tempArray.push(MyDouble.getAmountWithDecimal(debitAmount));
				tempArray.push('');
			} else if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
				creditAmount = balanceAmt;
				tempArray.push('');
				tempArray.push(MyDouble.getAmountWithDecimal(creditAmount));
			} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_BEGINNING_BALANCE_PAYABLE || txnType == TxnTypeConstant.TXN_TYPE_POPENBALANCE) {
				creditAmount = totalAmt;
				tempArray.push('');
				tempArray.push(MyDouble.getAmountWithDecimal(creditAmount));
			}
			tempArray.push(MyDouble.getAmountWithDecimal(totalRunningBalance) + (totalRunningBalance < 0 ? ' (Cr)' : ' (Dr)'));
		} else {
			if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
				totalAmount = totalAmt;

				if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_OTHER_INCOME) {
					receivedAmount = Number(cashAmt);
				} else if (txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT || txnType == TxnTypeConstant.TXN_TYPE_EXPENSE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
					paidAmount = Number(cashAmt);
				}
			} else {
				totalAmount = '';
				receivedAmount = '';
			}
			if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
				txnBalanceAmount = '';
			} else {
				txnBalanceAmount = balanceAmt;
			}

			if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
				tempArray.push(MyDouble.getAmountWithDecimal(totalAmount));
			} else if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
				tempArray.push(MyDouble.getAmountWithDecimal(totalAmount));
			} else {
				tempArray.push('');
			}

			if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_ESTIMATE || txnType == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txnType == TxnTypeConstant.TXN_TYPE_OTHER_INCOME) {
				tempArray.push(MyDouble.getAmountWithDecimal(receivedAmount));
			} else {
				tempArray.push('');
			}

			if (txnType == TxnTypeConstant.TXN_TYPE_CASHOUT || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_EXPENSE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
				tempArray.push(MyDouble.getAmountWithDecimal(paidAmount));
			} else {
				tempArray.push('');
			}

			if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_POPENBALANCE || txnType == TxnTypeConstant.TXN_TYPE_ROPENBALANCE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_ESTIMATE || txnType == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
				tempArray.push(MyDouble.getAmountWithDecimal(txnBalanceAmount));
			} else {
				tempArray.push('');
			}

			if (totalRunningBalance >= 0) {
				tempArray.push(MyDouble.getAmountWithDecimal(totalRunningBalance));
				tempArray.push('');
			} else {
				tempArray.push('');
				tempArray.push(MyDouble.getAmountWithDecimal(Math.abs(totalRunningBalance)));
			}
		}
		if (ExcelPaymentInfo) {
			tempArray.push(paymentName);
			tempArray.push(paymentRef);
		}
		if (ExcelPaymentStatus) {
			tempArray.push(paymentStatusString);
		}
		if (ExcelDescription) {
			tempArray.push(txn.getDescription());
		}

		rowObj.push(tempArray);
	}
	rowObj.push([]);
	totalArray.push('Total');
	if (selectedMode == 1) {
		totalArray.push(MyDouble.getAmountWithDecimal(totalDr));
		totalArray.push(MyDouble.getAmountWithDecimal(totalCr));
		totalArray.push(MyDouble.getAmountWithDecimal(totalRunningBalance) + (totalRunningBalance < 0 ? ' (Cr)' : ' (Dr)'));
	} else {
		totalArray.push('');
		totalArray.push('');
		totalArray.push('');
		totalArray.push('');
		if (totalRunningBalance >= 0) {
			totalArray.push(MyDouble.getAmountWithDecimal(totalRunningBalance));
		} else {
			totalArray.push('');
			totalArray.push(MyDouble.getAmountWithDecimal(Math.abs(totalRunningBalance)));
		}
	}
	rowObj.push(totalArray);
	return rowObj;
};

function printExcel() {
	closeExcelOptions();
	var XLSX = require('xlsx');
	var dataArray = prepareObjectForExcel(listOfTransactions);
	var worksheet = XLSX.utils.aoa_to_sheet(dataArray);
	var ExcelItemDetails = $('#ExcelPartyItemDetails').is(':checked');
	var ExcelExportHelper = require('./../UIControllers/ExcelExportHelper.js');
	var excelExportHelper = new ExcelExportHelper();

	var workbook = {
		'SheetNames': [],
		'Sheets': {}
	};

	var wsName = 'Party Statement';
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet;
	var wscolWidth = [{ wch: 15 }, { wch: 30 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	worksheet['!cols'] = wscolWidth;

	if (ExcelItemDetails) {
		var wsNameItem = 'Item Details';
		workbook.SheetNames[1] = wsNameItem;
		var itemArray = excelExportHelper.prepareItemObjectForExcel(listOfTransactions);
		var itemWorksheet = XLSX.utils.aoa_to_sheet(itemArray);
		workbook.Sheets[wsNameItem] = itemWorksheet;
		var wsItemcolWidth = [{ wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }];
		itemWorksheet['!cols'] = wsItemcolWidth;
	}
	console.log(workbook);
	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

populateTable();