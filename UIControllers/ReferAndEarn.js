(function () {
	var CommonUtility = require('../Utilities/CommonUtility');
	var LicenseInfoConstant = require('./../Constants/LicenseInfoConstant.js');
	var LocalStorageHelper = require('../Utilities/LocalStorageHelper');
	var referral_code = LocalStorageHelper.getValue(LicenseInfoConstant.referralCodeSelf);
	var Domain = require('./../Constants/Domain');
	var domain = Domain.thisDomain;
	var referralAPI = domain + '/api/desktop/referral';
	var rewardsAPI = domain + '/rewards';
	var desktopLoginAPI = domain + '/home';
	function displaySelfReferralCode() {
		var referralCodeSelf = LocalStorageHelper.getValue(LicenseInfoConstant.referralCodeSelf);
		if (referralCodeSelf) {
			$('#referralDisplayCode').text(referralCodeSelf);
			$('#referralCodeShare').show();
			$('#copyReferralTextToClipboard').show();
			$('#referralCodeLogin').hide();
		} else {
			$('#referralCodeShare').hide();
			$('#copyReferralTextToClipboard').hide();
			$('#referralCodeLogin').show();
		}
	}

	window.onResume = function () {
		// Do nothing
	};

	function loggedInSuccessFully(loginData, loginWindow) {
		loginWindow.close();
		var userLoginEmail = loginData['email'];
		if (userLoginEmail) {
			getReferralCode(userLoginEmail);
		}
	}

	function submitSelectedEmailIds() {
		$('#userSelectDialog').hide().dialog('close');
		var listOfSelectedEmailIds = $('#userEmailList').val() ? $('#userEmailList').val().split(',') : [];
		listOfSelectedEmailIds = listOfSelectedEmailIds.map(function (i) {
			return i.trim();
		});
		if (listOfSelectedEmailIds.length > 0) {
			sendMail(listOfSelectedEmailIds.join(','));
		}
	}

	function displayPointDetails(pointsEarned, text, condition) {
		$('#pointsEarned').text(pointsEarned);
		$('#pointsText').text(text);
		$('#pointsCondition').text(condition);
		$('#referralCodeSelfCodePoints').text(pointsEarned);
		$('#referralCodeShare').show();
		$('#noInternet').hide();
		$('#redeemPoints').show();
	}

	function gmailShare() {
		var NameCache = require('./../Cache/NameCache.js');
		var nameCache = new NameCache();
		var listOfEmails = nameCache.getEmailList();
		var listToAppend = '<ul>';
		$.each(listOfEmails, function (k, v) {
			listToAppend += "<li style='margin-top:10px'><input type='checkbox' class='email' /><span>" + v + '</span></li>';
		});
		listToAppend += '</ul>';
		$('.listOfAvailableEmails').html(listToAppend);
		$('#userSelectDialog').show().dialog();
	}

	function displayReferralDetails(title, message, msgTitle, msg1, msg2, msg3, referralText, referral_code) {
		$('#referralDisplayCode').text(referral_code);
		$('#referralDisplayTitle').text(title);
		$('#msgTitle').html(msgTitle);
		$('#msg1').text(msg1);
		$('#msg2').html(msg2 + '<br />');
		$('#msg3').text(msg3);
		// $('#referralDisplayMessage').text(message);
		$('#copyReferralTextToClipboard').show();
		$('#referralDisplayText').text(referralText);
	}

	// API call for referral code
	function getReferralCode(userLoginEmail) {
		MyAnalytics.pushEvent('Get Referral Code');
		var licenseCode = LocalStorageHelper.getValue(LicenseInfoConstant.licenseCode);
		var params = '';
		if (licenseCode) {
			params = 'license_code=' + licenseCode;
		} else {
			params = 'email=' + userLoginEmail;
			//     params = 'email=rajdeep.vaghela610@gmail.com';
		}
		$.ajax({
			async: true,
			type: 'GET',
			url: referralAPI + '?' + params,
			success: function success(data) {
				// var res = JSON.parse(data);
				var res = data;
				referral_code = res['referral_code'] || referral_code;
				var msgTitle = res['msgTitle'];
				var msg1 = res['msg1'];
				var msg2 = res['msg2'];
				var msg3 = res['msg3'];
				var message = res['message'];
				var pointsEarned = res['points_earned'];
				var referralText = res['referral_text'];
				var text = res['text'];
				var title = res['title'];
				var condition = res['condition'];
				displayReferralDetails(title, message, msgTitle, msg1, msg2, msg3, referralText, referral_code);
				displayPointDetails(pointsEarned, text, condition);
				LocalStorageHelper.setValue(LicenseInfoConstant.referralCodeSelf, referral_code);
				$('#referralCodeLogin').hide();
			},
			error: function error(xhr, ajaxOptions, thrownError) {}

		});
	}

	function getMessageLink() {
		return $('#referralDisplayText').text().split('Download here')[0] + 'Download Vyapar Desktop App from http://vyaparapp.in/desktop?' + (referral_code ? 'referrer_code=' + referral_code : '');
	}

	function sendMail(listOfSelectedEmailIds) {
		var message = getMessageLink();
		var userEmail = listOfSelectedEmailIds;
		var sub = 'Vyapr Refer and Earn Programme';
		$('#loading').show(function () {
			var isOnline = require('is-online');
			isOnline().then(function (online) {
				if (online) {
					var GoogleMail = require('./../BizLogic/GoogleMail.js');
					var googleMail = new GoogleMail();
					var msg = message;

					googleMail.sendMail({
						fileNames: null,
						receiverId: userEmail,
						alertUser: false,
						subject: sub,
						message: msg,
						attachFileNames: '',
						eventLog: 'referAndEarn',
						successCallBack: function successCallBack() {
							$('#loading').hide();
							alert('Referral code have been sent to selected email ids');
						},
						errorCallBack: function errorCallBack() {
							$('#loading').hide();
							alert('Something went wrong. Please try again later. Contact Vyapar team if problem persists.');
						}
					});
				} else {
					alert('No internet connection available. Please check your connection and try again.');
				}
			});
		});
	}

	function showLoginPage(url) {
		$('#loading').hide();
		var rem = require('electron');
		var _rem$remote = rem.remote,
		    BrowserWindow = _rem$remote.BrowserWindow,
		    dialog = _rem$remote.dialog,
		    shell = _rem$remote.shell;

		var loginWindow = new BrowserWindow({
			show: false,
			minimizable: false,
			alwaysOnTop: true,
			skipTaskbar: true,
			autoHideMenuBar: true,
			webPreferences: { nodeIntegration: false }
		});
		loginWindow.on('closed', function () {
			loginWindow = null;
		});
		loginWindow.webContents.session.clearStorageData({ origin: domain, storages: ['cookies'] });
		loginWindow.loadURL(url);
		loginWindow.show();
		// payment_window.openDevTools();
		loginWindow.on('page-title-updated', function () {
			//
			try {
				var title = loginWindow.getTitle();
				var jsonData = JSON.parse(title);
				loggedInSuccessFully(jsonData, loginWindow);
				setTimeout(function () {
					if (loginWindow) {
						loginWindow.close();
					}
				}, 15000);
			} catch (e) {
				//
			}
		});
	}

	$(function () {
		displaySelfReferralCode();
		referral_code = LocalStorageHelper.getValue(LicenseInfoConstant.referralCodeSelf);
		var userLoginEmail = LocalStorageHelper.getValue(LicenseInfoConstant.emailAddress);
		if (userLoginEmail) {
			getReferralCode(userLoginEmail);
		}

		$('#submitSelectedEmailIdButton').click(submitSelectedEmailIds);

		$('#referAndEarnTitleTab').on('click', function (event) {
			$('#pointsDiv').css('display', 'none');
			$('#referAndEarnDiv').show();
			CommonUtility.highlightAndShowCurrentTab(event, 'referAndEarnDiv');
		});

		$('#pointsTitleTab').on('click', function (event) {
			$('#pointsDiv').css('display', 'block');
			$('#referAndEarnDiv').hide();
			CommonUtility.highlightAndShowCurrentTab(event, 'pointsDiv');
		});

		$('#loginReferralCodeButton').on('click', function () {
			var url = desktopLoginAPI + '?desktop_login=1';
			$('#loading').show(function () {
				var isOnline = require('is-online');
				isOnline().then(function (online) {
					if (online) {
						showLoginPage(url);
					} else {
						$('#loading').hide();
						ToastHelper.error('No internet connection. Connect to your internet and try again.');
					}
				});
			});
		});

		$('.shareReferralMessage').on('click', '.sharePlatform', function () {
			var _this = this;

			$('#loading').show(function () {
				var isOnline = require('is-online');
				isOnline().then(function (online) {
					if (online) {
						$('#loading').hide();
						var id = _this.id;

						var message = getMessageLink();
						var twitterShare = 'https://twitter.com/';
						var googlePlus = 'https://plus.google.com/people';
						var facebookShare = 'https://facebook.com/';
						switch (id) {
							case 'gmailShare':
								MyAnalytics.pushEvent('Referral Code Share Gmail');
								gmailShare();
								break;
							case 'whatsappShare':
								MyAnalytics.pushEvent('Referral Code Share WhatsApp');
								var send = require('../Utilities/whatsapp').default;
								send(message);
								break;
							case 'twitterShare':
								MyAnalytics.pushEvent('Referral Code Share Twitter');
								require('electron').shell.openExternal(twitterShare);
								break;
							case 'googlePlusShare':
								MyAnalytics.pushEvent('Referral Code Share Google+');
								require('electron').shell.openExternal(googlePlus);
								break;
							case 'facebookShare':
								MyAnalytics.pushEvent('Referral Code Share Facebook');
								require('electron').shell.openExternal(facebookShare);
								break;
						}
					} else {
						$('#loading').hide();
						ToastHelper.error('No internet connection. Connect to your internet and try again.');
					}
				});
			});
		});

		$('.listOfAvailableEmails').on('click', '.email', function () {
			var curMail = $(this).next().text();
			var listOfSelectedMail = $('#userEmailList').val() ? $('#userEmailList').val().split(',') : [];
			if ($(this).prop('checked')) {
				listOfSelectedMail.push(curMail);
			} else {
				var index = listOfSelectedMail.indexOf(curMail);
				if (index > -1) {
					listOfSelectedMail.splice(index, 1);
				}
			}
			$('#userEmailList').val(listOfSelectedMail.join(','));
		});

		// Redem points
		$('#redeemPointsButton').on('click', function () {
			var rem = require('electron');
			var _rem$remote2 = rem.remote,
			    BrowserWindow = _rem$remote2.BrowserWindow,
			    dialog = _rem$remote2.dialog,
			    shell = _rem$remote2.shell;

			var loginWindow = new BrowserWindow({
				show: false,
				minimizable: false,
				alwaysOnTop: true,
				skipTaskbar: true,
				autoHideMenuBar: true,
				webPreferences: { nodeIntegration: false }
			});
			loginWindow.on('closed', function () {
				loginWindow = null;
			});
			loginWindow.webContents.session.clearStorageData({ origin: domain, storages: ['cookies'] });
			loginWindow.loadURL(rewardsAPI);
			loginWindow.show();
		});

		// copy referral code text to clipboard
		$('#copyReferralTextToClipboard').on('click', function (event) {
			var message = getMessageLink();
			var inp = document.createElement('input');
			document.body.appendChild(inp);
			inp.value = message;
			inp.select();
			document.execCommand('copy', false);
			inp.remove();
			ToastHelper.success('Referral text have been copied to clipboard. Please paste it in your messages');
		});
	});
})();