
$('.terminalButtonB2B').on('click', function () {
	var ErrorCode = require('../Constants/ErrorCode');
	var fromBank = $('#fromBank').val().trim();
	var toBank = $('#toBank').val().trim();
	var amount = $('#b2bAmount').val();
	var date = $('#b2bDate').val();
	var description = $('#b2bDescription').val().trim();
	if (fromBank && fromBank != null && toBank && toBank != null && amount > 0 && fromBank != toBank) {
		var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
		var BankAdjustmentTxn = require('./../BizLogic/BankAdjustmentTxn.js');
		var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
		var paymentInfoCache = new PaymentInfoCache();
		var bankAdjustmentTxn = new BankAdjustmentTxn();
		bankAdjustmentTxn.setAdjAmount(amount);
		bankAdjustmentTxn.setAdjType(TxnTypeConstant.TXN_TYPE_BANK_TO_BANK);
		var paymentInfo = paymentInfoCache.getPaymentInfoObjOnName(fromBank);
		bankAdjustmentTxn.setAdjBankId(paymentInfo.getId());
		bankAdjustmentTxn.setAdjDescription(description.trim());
		bankAdjustmentTxn.setAdjDate(date ? MyDate.getDateObj(date, 'dd/mm/yyyy', '/') : new Date());
		var paymentInfo = paymentInfoCache.getPaymentInfoObjOnName(toBank);
		bankAdjustmentTxn.setAdjToBankId(paymentInfo.getId());
		if ($(this).text().toLowerCase() == 'done') {
			var adjId = $('#bankAdjId').val();
			bankAdjustmentTxn.setAdjId(adjId);
			var statusCode = bankAdjustmentTxn.updateAdjustment();
		} else {
			var statusCode = bankAdjustmentTxn.createAdjustment();
		}

		if (statusCode == ErrorCode.ERROR_NEW_BANK_ADJUSTMENT_SUCCESS || ErrorCode.ERROR_UPDATE_BANK_INFO_SUCCESS) {
			if (statusCode == ErrorCode.ERROR_NEW_BANK_ADJUSTMENT_SUCCESS) {
				MyAnalytics.pushEvent('Bank to Bank Transfer Save');
			} else {
				MyAnalytics.pushEvent('Edit Bank to Bank Transfer Save');
			}
			ToastHelper.success(statusCode);
			emptyB2BForm();
			$('#bankToBankDialog').dialog('destroy');
			onResume();
		} else {
			ToastHelper.error(statusCode);
		}
	} else {
		ToastHelper.error('Bank name and amount cannot be left empty');
	}
});
function emptyB2BForm() {
	var inputs = $('#bankToBankDialog input');
	for (var i = 0; i < inputs.length; i++) {
		inputs[i].value = '';
	}
	var inputs = $('#bankToBankDialog textarea');
	for (var i = 0; i < inputs.length; i++) {
		inputs[i].value = '';
	}
}