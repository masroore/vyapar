var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function SaleReportController() {
	var PDFHandler = require('./../Utilities/PDFHandler');
	var pdfHandler = new PDFHandler();
	var ExcelHelper = require('./../Utilities/ExcelHelper.js');
	var excelHelper = new ExcelHelper();
	var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler');
	var transactionPDFHandler = new TransactionPDFHandler();
	var DateFormat = require('./../Constants/DateFormat.js');
	var StringConstant = require('./../Constants/StringConstants.js');
	var MyDate = require('./../Utilities/MyDate.js');
	var TxnPaymentStatusConstants = require('./../Constants/TxnPaymentStatusConstants');
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var MyDouble = require('./../Utilities/MyDouble.js');
	var DataLoader = require('./../DBManager/DataLoader.js');
	var dataLoader = new DataLoader();
	var SettingCache = require('./../Cache/SettingCache.js');
	var UDFCache = require('./../Cache/UDFCache.js');
	var FirmCache = require('./../Cache/FirmCache.js');
	var PDFReportConstant = require('./../Constants/PDFReportConstant.js');
	var firmCache = new FirmCache();
	var settingCache = new SettingCache();
	var uDFCache = new UDFCache();
	var startDateElement = $('#startDate');
	startDateElement.datepicker({ dateFormat: DateFormat.format });
	var endDateElement = $('#endDate');
	endDateElement.datepicker({ dateFormat: DateFormat.format });
	startDateElement.val(MyDate.getFirstDateOfCurrentMonthString());
	endDateElement.val(MyDate.getCurrentDateStringForUI());

	var listOfTransactions = [];
	var transactionCount = 0;

	$('#openGraphReport').off('click').on('click', function (e) {
		loadReportPage('saleGraphReport.html');
	});

	$('#openReportExcel').off('click').on('click', function (e) {
		openExcelOptions();
	});

	$('#openReportPDF').off('click').on('click', function (e) {
		openPrintOptions();
	});

	$('#reportPDFCloseOption').off('click').on('click', function (e) {
		closePrintOptions();
	});

	$('#reportPDFOpenPreviewOption').off('click').on('click', function (e) {
		openPreview();
	});

	$('#reportExcelCloseOption').off('click').on('click', function (e) {
		closeExcelOptions();
	});

	$('#reportExcelOpenPreviewOption').off('click').on('click', function (e) {
		printExcel();
	});

	function loadTransactions(filteredTransactions) {
		// let selectedTransaction = Number($('#transactionFilterOptions').val());
		if (transactionCount === 0) {
			$('#loading').hide();
			$('#firstTransactionContainer').show();
			$('#saleReportContainer').hide();
			var FirstScreen = require('../UIComponent/jsx/UIControls/FirstScreen').default;
			var MountComponent = require('../UIComponent/jsx/MountComponent').default;
			MountComponent(FirstScreen, document.querySelector('#firstTransaction'), {
				image: '../inlineSVG/first-screens/sale.svg',
				text: 'Make Sale invoices & Print or share with your customers directly via WhatsApp or Email.',
				buttonLabel: 'Add Your First Sale Invoice',
				onClick: function onClick() {
					var _require = require('../Utilities/TransactionHelper'),
					    viewTransaction = _require.viewTransaction;

					viewTransaction('', TxnTypeConstant.getTxnType(TxnTypeConstant.TXN_TYPE_SALE));
				}
			});
		} else {
			$('#firstTransactionContainer').hide();
			$('#saleReportContainer').show();
			displayTransactionList(filteredTransactions || listOfTransactions);
		}
	}

	var populateTable = function populateTable() {
		settingCache = new SettingCache();

		var startDateString = startDateElement.val();
		var endDateString = endDateElement.val();
		var firmId = Number($('#firmFilterOptions').val());
		var startDate = MyDate.convertStringToDateObject(startDateString);
		var endDate = MyDate.convertStringToDateObject(endDateString);
		$('#loading').show(function () {
			transactionCount = dataLoader.getTransactionCountByTxnType([TxnTypeConstant.TXN_TYPE_SALE, TxnTypeConstant.TXN_TYPE_SALE_RETURN]);
			// transactionCount = 0;
			listOfTransactions = dataLoader.loadTransactionsForSaleReport(startDate, endDate, firmId);
			loadTransactions(listOfTransactions);
		});
	};

	if (settingCache.getMultipleFirmEnabled()) {
		var firmList = firmCache.getFirmList();
		var optionTemp = document.createElement('option');
		for (var i in firmList) {
			if (firmList.hasOwnProperty(i)) {
				var option = optionTemp.cloneNode();
				option.text = firmList[i].getFirmName();
				option.value = firmList[i].getFirmId();
				$('#firmFilterOptions').append(option);
			}
		}
		$('#firmChooser').show();
	} else {
		$('#firmChooser').hide();
	}

	if (settingCache.isBillToBillEnabled()) {
		var paymentStatusValues = [{
			value: TxnPaymentStatusConstants.UNPAID,
			displayValue: StringConstant.paymentStatusUnPaid
		}, {
			value: TxnPaymentStatusConstants.PARTIAL,
			displayValue: StringConstant.paymentStatusPartial
		}, {
			value: TxnPaymentStatusConstants.PAID,
			displayValue: StringConstant.paymentStatusPaid
		}];
		var _optionTemp = document.createElement('option');
		for (var _i in paymentStatusValues) {
			if (paymentStatusValues.hasOwnProperty(_i)) {
				var _option = _optionTemp.cloneNode();
				_option.clssName = 'optionClass';
				_option.text = paymentStatusValues[_i].displayValue;
				_option.value = paymentStatusValues[_i].value;
				$('#paymentStatusFilterOptions').append(_option);
			}
		}

		if (settingCache.isPaymentTermEnabled()) {
			var _option2 = _optionTemp.cloneNode();
			_option2.clssName = 'optionClass';
			_option2.text = StringConstant.paymentStatusOverDue;
			_option2.value = TxnPaymentStatusConstants.OVERDUE;
			$('#paymentStatusFilterOptions').append(_option2);
		}

		$('#paymentStatusFilterDiv').removeClass('hide');

		$('#paymentStatusPrintDiv').removeClass('hide');

		$('#paymentStatusExcel').removeClass('hide');
	} else {
		$('#paymentStatusFilterDiv').addClass('hide');

		$('#paymentStatusPrintDiv').addClass('hide');

		$('#paymentStatusExcel').addClass('hide');
	}

	$('#paymentStatusFilterOptions').on('selectmenuchange', function (event) {
		loadTransactions(listOfTransactions);
	});

	$('#firmFilterOptions').on('selectmenuchange', function (event) {
		populateTable();
	});

	startDateElement.change(function () {
		populateTable();
	});

	endDateElement.change(function () {
		populateTable();
	});

	var saleReportClusterize = void 0;

	var displayTransactionList = function displayTransactionList(listOfTransactions) {
		var len = listOfTransactions.length;
		var dynamicRow = '';
		var rowData = [];
		var isTransactionRefNumberEnabled = settingCache.getTxnRefNoEnabled();
		var isBillToBillEnabled = settingCache.isBillToBillEnabled();
		var isPaymentTermEnabled = settingCache.isPaymentTermEnabled();

		var dateColumnWidthRatio = 9;
		var refNumberColumnRatio = isTransactionRefNumberEnabled ? 11 : 0;
		var nameColumnRatio = 19;
		var txnTypeColumnRatio = 10;
		var totalAmountColumnRatio = 14;
		var cashAmountColumnRatio = 14;
		var balanceAmountColumnRatio = 14;
		var paymentStatusColumnRatio = isBillToBillEnabled && !isPaymentTermEnabled ? 18 : 0;
		var dueDateColumnRatio = isPaymentTermEnabled ? 10 : 0;
		var statusColumnRatio = isPaymentTermEnabled ? 8 : 0;

		var totalColumnWidth = dateColumnWidthRatio + refNumberColumnRatio + nameColumnRatio + txnTypeColumnRatio + totalAmountColumnRatio + cashAmountColumnRatio + balanceAmountColumnRatio + paymentStatusColumnRatio + dueDateColumnRatio + statusColumnRatio;

		var totalAmount = 0;

		var className = '';
		if (!$('#sortIcon').attr('class')) {
			className = 'ui-selectmenu-icon ui-icon ui-icon-triangle-1-s';
		} else {
			className = $('#sortIcon').attr('class');
		}

		dynamicRow += "<thead><tr><th width='" + dateColumnWidthRatio * 100 / totalColumnWidth + "%'>DATE</th>";
		if (isTransactionRefNumberEnabled) {
			dynamicRow += '<th width=\'' + refNumberColumnRatio * 100 / totalColumnWidth + '%\' id=\'invoiceColId\' style=\'cursor:pointer;\'>REF NO.<span id=\'sortIcon\' class=\'' + className + '\'></span></th>';
		}
		dynamicRow += "<th width='" + nameColumnRatio * 100 / totalColumnWidth + "%'>NAME</th><th width='" + txnTypeColumnRatio * 100 / totalColumnWidth + "%'>TYPE</th><th width='" + totalAmountColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>TOTAL</th><th width='" + cashAmountColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>RECEIVED / PAID</th>";

		dynamicRow += "<th width='" + balanceAmountColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>BALANCE</th>";

		if (isBillToBillEnabled) {
			if (isPaymentTermEnabled) {
				dynamicRow += "<th width='" + dueDateColumnRatio * 100 / totalColumnWidth + "%'>DUE DATE</th>";
				dynamicRow += "<th width='" + statusColumnRatio * 100 / totalColumnWidth + "%'>STATUS</th>";
			} else {
				dynamicRow += "<th width='" + paymentStatusColumnRatio * 100 / totalColumnWidth + "%'>PAYMENT STATUS</th>";
			}
		}

		dynamicRow += '</tr></thead>';

		var paymentStatVal = Number($('#paymentStatusFilterOptions').val());

		var displayTransactionPromise = new _promise2.default(function (resolve, reject) {
			try {
				for (var _i2 = 0; _i2 < len; _i2++) {
					var txn = listOfTransactions[_i2];
					var typeString = TxnTypeConstant.getTxnTypeForUI(txn.getTxnType());
					var name = txn.getNameRef().getFullName();
					if (txn.getDisplayName() && txn.getDisplayName().trim() && name !== txn.getDisplayName().trim()) {
						name = name + ' (' + txn.getDisplayName().trim() + ')';
					}
					var date = MyDate.getDateStringForUI(txn.getTxnDate());
					var receivedAmt = txn.getCashAmount() ? txn.getCashAmount() : 0;
					var balanceAmt = txn.getBalanceAmount() ? txn.getBalanceAmount() : 0;
					var txnCurrentBalanceAmt = txn.getTxnCurrentBalanceAmount() ? txn.getTxnCurrentBalanceAmount() : 0;
					var totalAmt = Number(receivedAmt) + Number(balanceAmt);
					totalAmt = totalAmt || 0;
					var typeTxn = TxnTypeConstant.getTxnType(txn.getTxnType());
					var refNo = txn.getTxnRefNumber() === '' ? '' : txn.getFullTxnRefNumber();
					var paymentStatus = txn.getTxnPaymentStatus();
					var paymentStatusString = TxnPaymentStatusConstants.getPaymentStatusForUI(txn.getTxnType(), paymentStatus);
					var txnTotalReceivedAmount = totalAmt - txnCurrentBalanceAmt;
					var dueDate = MyDate.convertDateToStringForUI(txn.getTxnDueDate());
					var dueByDays = MyDouble.getDueDays(txn.getTxnDueDate());

					var row = '<tr id=' + txn.getTxnId() + ':' + typeTxn + " ondblclick='openTransaction(" + txn.getTxnId() + ',' + txn.getTxnType() + ")' class='currentRow'><td width='" + dateColumnWidthRatio * 100 / totalColumnWidth + "%'>" + date + '</td>';
					if (isTransactionRefNumberEnabled) {
						row += "<td width='" + refNumberColumnRatio * 100 / totalColumnWidth + "%'>" + refNo + '</td>';
					}
					row += "<td width='" + nameColumnRatio * 100 / totalColumnWidth + "%'>" + name + "</td><td width='" + txnTypeColumnRatio * 100 / totalColumnWidth + "%'>" + typeString + "</td><td width='" + totalAmountColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalAmt) + "</td><td width='" + cashAmountColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(txnTotalReceivedAmount) + '</td>';

					row += "<td width='" + balanceAmountColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(txnCurrentBalanceAmt) + '</td>';

					if (isBillToBillEnabled) {
						if (isPaymentTermEnabled) {
							if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE) {
								row += "<td width='" + dueDateColumnRatio * 100 / totalColumnWidth + "%'>" + dueDate + '</td>';
								if (dueByDays > 0 && paymentStatus != TxnPaymentStatusConstants.PAID) {
									paymentStatusString = 'Overdue';
									row += "<td width='" + statusColumnRatio * 100 / totalColumnWidth + "%'>";
									row += '<div>' + paymentStatusString + '</div>';
									row += '<div>' + dueByDays + ' Days</div>';
								} else {
									row += "<td width='" + statusColumnRatio * 100 / totalColumnWidth + "%'>";
									row += '<div>' + paymentStatusString + '</div>';
								}
								row += '</td>';
							} else {
								row += "<td width='" + dueDateColumnRatio * 100 / totalColumnWidth + "%'></td>";
								row += "<td width='" + statusColumnRatio * 100 / totalColumnWidth + "%'>" + paymentStatusString + '</td>';
							}
						} else {
							row += "<td width='" + paymentStatusColumnRatio * 100 / totalColumnWidth + "%'>" + paymentStatusString + '</td>';
						}
					}

					row += '</tr>';

					// filters

					var paymentStatFilter = void 0;

					if (settingCache.isPaymentTermEnabled() && paymentStatVal == TxnPaymentStatusConstants.OVERDUE) {
						paymentStatFilter = dueByDays > 0 && paymentStatus != TxnPaymentStatusConstants.PAID;
					} else {
						paymentStatFilter = paymentStatVal ? paymentStatVal == paymentStatus : true;
					}

					paymentStatFilter = settingCache.isBillToBillEnabled() ? paymentStatFilter : true;

					if (paymentStatFilter) {
						if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE) {
							totalAmount += totalAmt;
						} else if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
							totalAmount -= totalAmt;
						}
						rowData.push(row);
					}
				}
				$('#tableHead').html(dynamicRow);
				resolve(rowData);
			} catch (err) {
				reject(err);
			}
		});
		displayTransactionPromise.then(function (rowData) {
			$('#loading').hide();

			if ($('#saleReportContainer').length === 0) {
				return;
			}
			if (saleReportClusterize && saleReportClusterize.destroy) {
				saleReportClusterize.clear();
				saleReportClusterize.destroy();
			}

			saleReportClusterize = new Clusterize({
				rows: rowData,
				scrollId: 'scrollArea',
				contentId: 'contentArea',
				rows_in_block: StringConstant.clusterizeRow,
				blocks_in_cluster: StringConstant.clusterizeBlock
			});
			$('#totalAmountSaleReport').html(MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalAmount));
			if (totalAmount < 0) {
				$('#totalAmountSaleReport').css('color', '#e35959');
			} else {
				$('#totalAmountSaleReport').css('color', '#1faf9d');
			}
		}).catch(function (err) {
			$('#loading').hide();
			logger.error('Error in Sale report loading' + err);
		});
	};

	$('#tableHead').off('click').on('click', '#invoiceColId', function (e) {
		var SortHelper = require('./../Utilities/SortHelper.js');
		var sortHelper = new SortHelper();
		listOfTransactions = sortHelper.getSortedListByInvoice(listOfTransactions, 'txnRefNumber');
		loadTransactions(listOfTransactions);
	});

	// TODO: don't put it in global; instead attach this event at run time using Jquery $('.parentId').on('click', '.className', fn() {})
	window.openTransaction = function (txnId, txnType) {
		var ViewTransaction = require('./../BizLogic/ViewTransaction.js');
		var viewTransaction = new ViewTransaction();
		viewTransaction.viewTransactionFile(txnId + ':' + TxnTypeConstant.getTxnType(txnType));
	};

	if (settingCache.getItemEnabled()) {
		$('#itemDetailPrintDiv').show();
	} else {
		$('#itemDetailPrintDiv').hide();
	}

	var getHTMLTextForReport = function getHTMLTextForReport() {
		var SaleReportHTMLGenerator = require('./../ReportHTMLGenerator/SaleReportHTMLGenerator.js');
		var saleReportHTMLGenerator = new SaleReportHTMLGenerator();
		var startDateString = startDateElement.val();
		var endDateString = endDateElement.val();
		var firmId = Number($('#firmFilterOptions').val());
		var printItemDetails = $('#printItems').is(':checked');
		var printDescription = $('#printDescription').is(':checked');
		var printPaymentStatus = $('#printPaymentStatus').is(':checked');
		var paymentStatVal = Number($('#paymentStatusFilterOptions').val());
		return saleReportHTMLGenerator.getHTMLText(listOfTransactions, startDateString, endDateString, firmId, printItemDetails, printDescription, printPaymentStatus, paymentStatVal);
	};

	var printTransaction = function printTransaction(that) {
		var ThermalPrinterConstant = require('./../Constants/ThermalPrinterConstant.js');
		if (settingCache.getDefaultPrinterType() === ThermalPrinterConstant.THERMAL_PRINTER) {
			var txnId = that.id.split(':')[0];
			var PrintUtil = require('./../Utilities/PrintUtil.js');
			PrintUtil.printTransactionUsingThermalPrinter(txnId);
		} else {
			var html = transactionPDFHandler.transactionToHTML(that.id);
			if (html) {
				pdfHandler.print(html);
			}
		}
	};

	$('#currentAccountTransactionDetails').contextmenu({
		delegate: '.currentRow',
		addClass: 'width10',
		menu: [{ title: 'Open PDF', cmd: 'openpdf' }, { title: '----' }, { title: 'Print', cmd: 'print' }, { title: '----' }, { title: 'Delete', cmd: 'delete' }],
		select: function select(event, ui) {
			var selectedTxnElement = ui.target[0].parentNode;
			if (ui.cmd === 'print') {
				printTransaction(selectedTxnElement);
			} else if (ui.cmd === 'openpdf') {
				openPdfFromRightClickMenu(selectedTxnElement);
			} else if (ui.cmd === 'delete') {
				var DeleteTransaction = require('./../BizLogic/DeleteTransaction.js');
				var deleteTransaction = new DeleteTransaction();
				deleteTransaction.RemoveTransaction(selectedTxnElement.id);
			}
		}
	});

	if (settingCache.getItemEnabled()) {
		$('#itemDetailsExcel').show();
	} else {
		$('#itemDetailsExcel').hide();
	}

	var openExcelOptions = function openExcelOptions() {
		$('#additionalFieldsExcel').addClass('hide');
		var firmId = Number(settingCache.getDefaultFirmId());
		var hasAdditionalTxnFieldsEnabled = uDFCache.hasAdditionalFieldsEnabledForTxnType(firmId, TxnTypeConstant.TXN_TYPE_SALE);
		if (hasAdditionalTxnFieldsEnabled) {
			$('#additionalFieldsExcel').removeClass('hide');
		}
		$('#ExcelDialog').dialog({
			closeOnEscape: true,
			draggable: true,
			modal: true
		});
		$('#ExcelDialog').removeClass('hide');
	};

	var closeExcelOptions = function closeExcelOptions() {
		$('#ExcelDialog').addClass('hide');
		$('#ExcelDialog').dialog('close').dialog('destroy');
	};

	var downloadExcelFile = function downloadExcelFile() {
		var fileUtil = require('./../Utilities/FileUtil.js');
		var fileName = fileUtil.getFileName({
			type: PDFReportConstant.SALE_REPORT,
			fromDate: $('#startDate').val(),
			toDate: $('#endDate').val()
		});
		excelHelper.saveExcel(fileName);
	};

	window.onResume = function () {
		var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

		if (notFromAutoSyncFlow) {
			$('#modelContainer').css({ display: 'none' });
			$('.viewItems').css('display', 'block');
		}
		populateTable();
	};

	/// ///////////////////////////PDF/////////////////////////////////////

	var openPrintOptions = function openPrintOptions() {
		$('#salePrintDialog').removeClass('hide');
		$('#salePrintDialog').dialog({
			closeOnEscape: true,
			draggable: true,
			modal: true
		});
	};

	var closePrintOptions = function closePrintOptions() {
		$('#salePrintDialog').addClass('hide');
		$('#salePrintDialog').dialog('close').dialog('destroy');
	};

	var openPdfFromRightClickMenu = function openPdfFromRightClickMenu(that) {
		$('#loading').show(function () {
			var html = transactionPDFHandler.transactionToHTML(that.id);
			if (html) {
				pdfHandler.openPDF(html, false);
			}
		});
	};

	window.openPDF = function () {
		var html = $('#htmlView').html();
		$('#loading').show(function () {
			pdfHandler.openPDF(html, true);
		});
	};

	window.printPDF = function () {
		var html = $('#htmlView').html();
		pdfHandler.print(html);
	};

	window.savePDF = function () {
		pdfHandler.savePDF({
			type: PDFReportConstant.SALE_REPORT,
			fromDate: startDateElement.val(),
			toDate: endDateElement.val()
		});
	};

	window.sharePDF = function () {
		$('#loading').show(function () {
			pdfHandler.sharePDF();
		});
	};

	window.closePreview = function () {
		try {
			$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
		} catch (err) {}
	};

	var openPreview = function openPreview() {
		$('#salePrintDialog').addClass('hide');
		$('#salePrintDialog').dialog('close').dialog('destroy');
		$('#loading').show(function () {
			var html = getHTMLTextForReport();
			if (html) {
				pdfHandler.generateHiddenBrowserWindowForPDF(html);
				transactionPDFHandler.openPreviewDialog(html);
			}
		});
	};

	/// ////////////////////////////////////////////////////////////////////

	var prepareObjectForExcel = function prepareObjectForExcel(listOfTransactions) {
		var isTransactionRefNumberEnabled = settingCache.getTxnRefNoEnabled();
		var len = listOfTransactions.length;
		var totalAmount = 0;
		var rowObj = [];
		var tableHeadArray = [];
		var totalArray = [];
		var ExcelDescription = $('#ExcelSaleDescription').is(':checked');
		var ExcelPaymentStatus = $('#ExcelPaymentStatus').is(':checked');
		var ExcelAdditionalFields = $('#ExcelAdditionalFields').is(':checked');

		tableHeadArray.push('Date');
		if (isTransactionRefNumberEnabled) {
			tableHeadArray.push('Reference No');
			totalArray.push('');
		}
		tableHeadArray.push('Name');
		tableHeadArray.push('Type');
		tableHeadArray.push('Total Amount');
		tableHeadArray.push('Received/Paid Amount');

		tableHeadArray.push('Balance Amount');

		if (settingCache.isBillToBillEnabled() && ExcelPaymentStatus) {
			if (settingCache.isPaymentTermEnabled()) {
				tableHeadArray.push('Due Date');
				tableHeadArray.push('Status');
			} else {
				tableHeadArray.push('Payment Status');
			}
		}

		if (ExcelDescription) {
			tableHeadArray.push('Description');
		}

		var firmId = Number(settingCache.getDefaultFirmId());
		var txnMap = uDFCache.getTransactionFieldsMap();
		var firmTxnMap = txnMap.get(Number(firmId));
		if (ExcelAdditionalFields) {
			if (firmTxnMap) {
				firmTxnMap.forEach(function (udfModel) {
					if (udfModel.getUdfFieldStatus() == 1 && udfModel.getUdfTxnType() == TxnTypeConstant.TXN_TYPE_SALE) {
						tableHeadArray.push(udfModel.getUdfFieldName());
					}
				});
			}
		}
		totalArray.push('');
		totalArray.push('');
		rowObj.push(tableHeadArray);
		rowObj.push([]);

		var paymentStatVal = Number($('#paymentStatusFilterOptions').val());

		var _loop = function _loop(j) {
			var txn = listOfTransactions[j];
			var typeString = TxnTypeConstant.getTxnTypeForUI(txn.getTxnType());
			var name = txn.getNameRef().getFullName();
			if (txn.getDisplayName() && txn.getDisplayName().trim() && name !== txn.getDisplayName().trim()) {
				name = name + ' (' + txn.getDisplayName().trim() + ')';
			}
			var date = MyDate.getDateStringForUI(txn.getTxnDate());
			var receivedAmt = txn.getCashAmount() ? txn.getCashAmount() : 0;
			var balanceAmt = txn.getBalanceAmount() ? txn.getBalanceAmount() : 0;
			var txnCurrentBalanceAmount = txn.getTxnCurrentBalanceAmount() ? txn.getTxnCurrentBalanceAmount() : 0;
			var totalAmt = Number(receivedAmt) + Number(balanceAmt);
			var paymentStatus = txn.getTxnPaymentStatus();
			var paymentStatusString = TxnPaymentStatusConstants.getPaymentStatusForUI(txn.getTxnType(), paymentStatus);

			var txnTotalReceivedAmount = totalAmt - txnCurrentBalanceAmount;
			var dueDate = MyDate.convertDateToStringForUI(txn.getTxnDueDate());
			var dueByDays = MyDouble.getDueDays(txn.getTxnDueDate());

			var paymentStatFilter = void 0;

			if (settingCache.isPaymentTermEnabled() && paymentStatVal == TxnPaymentStatusConstants.OVERDUE) {
				paymentStatFilter = dueByDays > 0 && paymentStatus != TxnPaymentStatusConstants.PAID;
			} else {
				paymentStatFilter = paymentStatVal ? paymentStatVal == paymentStatus : true;
			}

			paymentStatFilter = settingCache.isBillToBillEnabled() ? paymentStatFilter : true;

			totalAmt = totalAmt || 0;

			var refNo = txn.getTxnRefNumber() === '' ? '' : txn.getFullTxnRefNumber();
			var tempArray = [];
			tempArray.push(date);
			if (isTransactionRefNumberEnabled) {
				tempArray.push(refNo);
			}
			tempArray.push(name);
			tempArray.push(typeString);
			tempArray.push(MyDouble.getAmountWithDecimal(totalAmt));
			tempArray.push(MyDouble.getAmountWithDecimal(txnTotalReceivedAmount));

			tempArray.push(MyDouble.getAmountWithDecimal(txnCurrentBalanceAmount));

			if (settingCache.isBillToBillEnabled() && ExcelPaymentStatus) {
				if (settingCache.isPaymentTermEnabled()) {
					if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE) {
						tempArray.push(dueDate);
						if (dueByDays > 0) {
							tempArray.push('Overdue ' + dueByDays + ' Days');
						} else {
							tempArray.push(paymentStatusString);
						}
					} else {
						tempArray.push('');
						tempArray.push(paymentStatusString);
					}
				} else {
					tempArray.push(paymentStatusString);
				}
			}
			if (ExcelDescription) {
				tempArray.push(txn.getDescription());
			}
			if (ExcelAdditionalFields) {
				if (firmTxnMap) {
					firmTxnMap.forEach(function (udfModel) {
						if (udfModel.getUdfFieldStatus() == 1 && udfModel.getUdfTxnType() == TxnTypeConstant.TXN_TYPE_SALE) {
							var extraFieldObjectArray = txn.getUdfObjectArray();
							var udfValueModel = extraFieldObjectArray.find(function (udfValueModel) {
								return udfValueModel.getUdfFieldId() == udfModel.getUdfFieldId();
							});
							var displayValue = udfValueModel ? udfValueModel.getDisplayValue(udfModel) : '';
							tempArray.push(displayValue);
						}
					});
				}
			}

			if (paymentStatFilter) {
				if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE) {
					totalAmount += totalAmt;
				} else if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
					totalAmount -= totalAmt;
				}
				rowObj.push(tempArray);
			}
		};

		for (var j = 0; j < len; j++) {
			_loop(j);
		}
		rowObj.push([]);
		totalArray.push('Total');
		totalArray.push(MyDouble.getAmountWithDecimal(totalAmount));
		totalArray.push('');
		totalArray.push('');
		rowObj.push(totalArray);
		return rowObj;
	};

	var printExcel = function printExcel() {
		closeExcelOptions();
		var XLSX = require('xlsx');
		var dataArray = prepareObjectForExcel(listOfTransactions);
		var worksheet = XLSX.utils.aoa_to_sheet(dataArray);
		var ExcelItemDetails = $('#ExcelSaleItemDetails').is(':checked');
		var ExcelExportHelper = require('./../UIControllers/ExcelExportHelper.js');
		var excelExportHelper = new ExcelExportHelper();

		var workbook = {
			SheetNames: [],
			Sheets: {}
		};

		var wsName = 'Sale Report';
		workbook.SheetNames[0] = wsName;
		workbook.Sheets[wsName] = worksheet;
		var wscolWidth = [{ wch: 15 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
		worksheet['!cols'] = wscolWidth;

		if (ExcelItemDetails) {
			var wsNameItem = 'Item Details';
			workbook.SheetNames[1] = wsNameItem;
			var itemArray = excelExportHelper.prepareItemObjectForExcel(listOfTransactions);
			var itemWorksheet = XLSX.utils.aoa_to_sheet(itemArray);
			workbook.Sheets[wsNameItem] = itemWorksheet;
			var wsItemcolWidth = [{ wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }];
			itemWorksheet['!cols'] = wsItemcolWidth;
		}
		XLSX.writeFile(workbook, appPath + '/report.xlsx');
		downloadExcelFile();
	};

	populateTable();

	(function () {
		var TxnTypeConstant = require('../Constants/TxnTypeConstant.js');
		var MountComponent = require('../UIComponent/jsx/MountComponent').default;
		var Component = require('../UIComponent/jsx/NewTransactionButton').default;
		var target = document.querySelector('#NEW-TRANSACTION-BUTTON');
		var txnType = TxnTypeConstant.TXN_TYPE_SALE;
		MountComponent(Component, target, {
			label: '+ Add ' + TxnTypeConstant.getTxnTypeForUI(txnType),
			txnType: TxnTypeConstant.getTxnType(txnType)
		});
	})();

	var loadReportPage = function loadReportPage(reportFileName) {
		if (document.getElementById('reportPage')) {
			window.changeReportPage(reportFileName);
		} else {
			$('#defaultPage').load(reportFileName);
		}
	};
};