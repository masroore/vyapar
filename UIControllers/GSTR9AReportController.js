var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var startDate;
var endDate;
var DateFormat = require('./../Constants/DateFormat.js');
var MyString = require('./../Utilities/MyString.js');
var NameCache = require('./../Cache/NameCache.js');
var FirmCache = require('./../Cache/FirmCache.js');
var firmCache = new FirmCache();
var nameCache = new NameCache();
var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();
var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var TaxCodeConstants = require('./../Constants/TaxCodeConstants.js');
var GSTRReportHelper = require('./../UIControllers/GSTRReportHelper.js');
var CompositeSchemeTaxRate = require('./../Constants/CompositeSchemeTaxRate.js');
var CompositeUserType = require('./../Constants/CompositeUserType.js');
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');
var NameCustomerType = require('./../Constants/NameCustomerType.js');
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();

var workbook;
var turnOverValue = 0;
var HSNwiseDataForGoodsAttractingReverseCharge = [];
var HSNwiseDataForGoodsWithoutReverseCharge = [];
var HSNwiseDataForServicesAttractingReverseCharge = [];
var HSNwiseDataForServicesWithoutReverseCharge = [];
var expenseData = [];
var OutwardSuppliesData = [];
var otherIncomeData = [];
var reconciliationData = {};
var taxDataBasedOnReverseCharge = {};
var firmId;
var gstin;
var fyVal;
var firmName;
var startDate;
var startDateString;
var endDateString;
var turnOverObject = {};
var listOfTransactions = [];
var firm;

var fyDateMapUI = new _map2.default();
fyDateMapUI.set(1, ['2017', '2018']);
fyDateMapUI.set(2, ['2018', '2019']);
fyDateMapUI.set(3, ['2019', '2020']);
fyDateMapUI.set(4, ['2020', '2021']);

var GSTR_9A_totals = {
  IGSTtotal7A: 0,
  CGSTtotal7A: 0,
  SGSTtotal7A: 0,
  CESStotal7A: 0,
  TaxableValuetotal7A: 0,
  IGSTtotal7B: 0,
  CGSTtotal7B: 0,
  SGSTtotal7B: 0,
  CESStotal7B: 0,
  TaxableValuetotal7B: 0,
  IGSTtotal8A: 0,
  CGSTtotal8A: 0,
  SGSTtotal8A: 0,
  CESStotal8A: 0,
  TaxableValuetotal8A: 0
};

$('.monthYearPicker').datepicker({
  changeMonth: true,
  changeYear: true,
  showButtonPanel: true,
  dateFormat: 'mm/yy'
}).focus(function () {
  var thisCalendar = $(this);
  $('.ui-datepicker-calendar').detach();
  $('.ui-datepicker-close').click(function () {
    var month = $('#ui-datepicker-div .ui-datepicker-month :selected').val();
    var year = $('#ui-datepicker-div .ui-datepicker-year :selected').val();
    thisCalendar.datepicker('setDate', new Date(year, month));
    populateTable();
  });
});

$('#fyPicker').val(3);

var fyDateMap = new _map2.default();
fyDateMap.set(1, ['01/04/2017', '31/03/2018']);
fyDateMap.set(2, ['01/04/2018', '31/03/2019']);
fyDateMap.set(3, ['01/04/2019', '31/03/2020']);
fyDateMap.set(4, ['01/04/2020', '31/03/2021']);

var populateTable = function populateTable() {
  nameCache = new NameCache();
  settingCache = new SettingCache();
  fyVal = Number($('#fyPicker').val());
  debugger;
  startDateString = fyDateMap.get(fyVal)[0];
  endDateString = fyDateMap.get(fyVal)[1];
  startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
  var endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');
  var considerNonTaxAsExemptedCheck = $('#considerNonTaxAsExempted').prop('checked');
  firmId = Number($('#firmFilterOptions').val());
  if (firmId == 0) {
    firm = firmCache.getFirmById(settingCache.getDefaultFirmId());
  } else {
    firm = firmCache.getFirmById(firmId);
  }
  firmName = firm.getFirmName();
  gstin = firmCache.getFirmGSTINByFirmName(firmName);
  var GSTR9AReportHelper = require('./../UIControllers/GSTR9AReportHelper.js');
  var gSTR9AReportHelper = new GSTR9AReportHelper();

  CommonUtility.showLoader(function () {
    GSTR_9A_totals.IGSTtotal7A = 0;
    GSTR_9A_totals.CGSTtotal7A = 0;
    GSTR_9A_totals.SGSTtotal7A = 0;
    GSTR_9A_totals.CESStotal7A = 0;
    GSTR_9A_totals.TaxableValuetotal7A = 0;
    GSTR_9A_totals.IGSTtotal7B = 0;
    GSTR_9A_totals.CGSTtotal7B = 0;
    GSTR_9A_totals.SGSTtotal7B = 0;
    GSTR_9A_totals.CESStotal7B = 0;
    GSTR_9A_totals.TaxableValuetotal7B = 0;
    GSTR_9A_totals.IGSTtotal8A = 0;
    GSTR_9A_totals.CGSTtotal8A = 0;
    GSTR_9A_totals.SGSTtotal8A = 0;
    GSTR_9A_totals.CESStotal8A = 0;
    GSTR_9A_totals.TaxableValuetotal8A = 0;
    GSTR_9A_totals.financialYear = fyVal;

    listOfTransactions = gSTR9AReportHelper.getTxnListBasedOnDate(startDate, endDate, firmId, considerNonTaxAsExemptedCheck);
    turnOverObject = gSTR9AReportHelper.getTurnOverForGSTR9A();
    calculateTotals();
    displayTransactionList();
  });
};

var calculateTotals = function calculateTotals() {
  var len = listOfTransactions.length;
  for (var _i = 0; _i < len; _i++) {
    var txn = listOfTransactions[_i];
    var igstAmt = txn.getIGSTAmt();
    var cgstAmt = txn.getCGSTAmt();
    var sgstAmt = txn.getSGSTAmt();
    var cessAmt = txn.getCESSAmt() + txn.getAdditionalCessAmt();
    var taxableValue = txn.getInvoiceTaxableValue();
    var nameId = txn.getNameId();
    var nameObj = nameCache.findNameObjectByNameId(nameId);
    var partyType = nameObj.getCustomerType();
    if (txn.getReverseCharge() == 1) {
      if (partyType == NameCustomerType.REGISTERED_NORMAL || partyType == NameCustomerType.REGISTERED_COMPOSITE) {
        GSTR_9A_totals.IGSTtotal7A += igstAmt;
        GSTR_9A_totals.CGSTtotal7A += cgstAmt;
        GSTR_9A_totals.SGSTtotal7A += sgstAmt;
        GSTR_9A_totals.CESStotal7A += cessAmt;
        GSTR_9A_totals.TaxableValuetotal7A += taxableValue;
      } else if (partyType == NameCustomerType.UNREGISTERED) {
        GSTR_9A_totals.IGSTtotal7B += igstAmt;
        GSTR_9A_totals.CGSTtotal7B += cgstAmt;
        GSTR_9A_totals.SGSTtotal7B += sgstAmt;
        GSTR_9A_totals.CESStotal7B += cessAmt;
        GSTR_9A_totals.TaxableValuetotal7B += taxableValue;
      }
    } else {
      if (partyType == NameCustomerType.REGISTERED_NORMAL || partyType == NameCustomerType.REGISTERED_COMPOSITE) {
        GSTR_9A_totals.IGSTtotal8A += igstAmt;
        GSTR_9A_totals.CGSTtotal8A += cgstAmt;
        GSTR_9A_totals.SGSTtotal8A += sgstAmt;
        GSTR_9A_totals.CESStotal8A += cessAmt;
        GSTR_9A_totals.TaxableValuetotal8A += taxableValue;
      }
    }
  }
};

var date = MyDate.getDate('m/y');
$('#startDate').val(date);
$('#endDate').val(date);

if (settingCache.getMultipleFirmEnabled()) {
  var optionTemp = document.createElement('option');
  var firmList = firmCache.getFirmList();
  for (i in firmList) {
    var option = optionTemp.cloneNode();
    option.text = firmList[i].getFirmName();
    option.value = firmList[i].getFirmId();
    $('#firmFilterOptions').append(option);
  }
  $('#firmChooser').show();
} else {
  $('#firmChooser').hide();
}

$('#firmFilterOptions').on('selectmenuchange', function (event) {
  populateTable();
});

$('#fyPicker').change(function () {
  populateTable();
});

$('#considerNonTaxAsExempted').change(function () {
  populateTable();
});

var onResume = function onResume() {
  var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

  // $('#frameDiv').load('');
  if (notFromAutoSyncFlow) {
    $('#modelContainer').css({ 'display': 'none' });
    $('.viewItems').css('display', 'block');
  }

  populateTable();
};

var turnoverOne;
var turnoverTwo;
var turnoverFive;
var turnoverOneTax;
var turnoverTwoTax;
var turnoverFiveTax;

var displayTransactionList = function displayTransactionList() {
  turnoverOne = settingCache.getCompositeUserType() == CompositeUserType.TRADER || settingCache.getCompositeUserType() == CompositeUserType.MANUFACTURER ? turnOverObject.taxable_turn_over : 0;
  turnoverTwo = settingCache.getCompositeUserType() == CompositeUserType.MANUFACTURER && fyVal == 1 ? turnOverObject.taxable_turn_over_before : 0;
  turnoverFive = settingCache.getCompositeUserType() == CompositeUserType.RESTAURANT ? turnOverObject.taxable_turn_over : 0;
  turnoverOneTax = turnoverOne / 200;
  turnoverTwoTax = turnoverTwo / 100;
  turnoverFiveTax = turnoverFive / 40;
  var tableHTML = '<html>\n   <head>\n      <style> table { border-collapse: collapse; table-layout: fixed; text-overflow: ellipsis;} .brandingFooter {position: fixed; bottom: 0;} .brandingDummyFooterForSpacing {height:50px} .brandingLeft {vertical-align: bottom; font-size: 14px; font-weight: 500; color: #097AA8}.brandingRight {vertical-align: bottom;}tfoot { display:table-footer-group;} td,th { padding: 5px; font-size: 12px; word-wrap: break-word;} td.noBorder { border-bottom: 0px; border-color: white; padding: 5px; font-size: 12px;} p { margin: 0; padding: 2px;}td.extraTopPadding { padding-top: 12px;} td.extraTextSize { font-size: .50em;} td.thickBorder { border-bottom: 2px solid ;} .borderBottomForTxn { border-bottom: 1px solid; border-color: gray;}.borderTopForTxn { border-top: 1px solid; border-color: gray;}.borderLeftForTxn { border-left: 1px solid; border-color: gray;}.borderRightForTxn { border-right: 1px solid; border-color: gray;}.profitLossNetRow {font-weight: bold; padding-top: 12px; padding-bottom: 12px; padding-left: 10px; padding-left: 10px; font-size: 16px;} .paddingLeft { padding-left: 5px;} .paddingRight { padding-right: 5px;} .extraPaddingLeft {padding-left: 20px}.discountTaxTable{ border-bottom: 0px; border-color: white;} .boldText {font-weight: bold;} .normalTextSize {font-size: 13px;}.bigTextSize {font-size: 15px;}.largerTextSize {font-size: 12px;}.extraLargeTextSize {font-size: 24px;}.noTopPadding { padding-top: 0px}.lessMargin { margin: 0; padding-top: 3px;padding-bottom:3px;}.tableFooter td {border-bottom: 0px; font-weight: bold;}body { font-family: arial unicode ms, sans-serif;  }</style>\n   </head>\n   <body>\n      <p class = \'largerTextSize boldText\' style="background-color: lightgrey" align="center">SIMPLIFIED ANNUAL RETURN FOR COMPOUNDING TAXABLE PERSONS</p>\n      <p class = \'largerTextSize boldText\' >1.GSTIN : ' + gstin + '</p>\n      <p class = \'largerTextSize boldText\' >2.Name of the Taxable Person : ' + firmName + '</p>\n      <p class = \'largerTextSize boldText\' >3. Financial Year : ' + fyDateMapUI.get(fyVal)[0] + '-' + fyDateMapUI.get(fyVal)[1] + '</p>\n      <p class = \'largerTextSize boldText\' style="background-color: pink" align="center">Pt. II. Details of Outward supplies made during the financial year</p>\n      <p class = \'largerTextSize boldText\' style="background-color: lightgrey">Details of outward supplies made during financial year:</p>\n      <table width="100%">\n         <tr style="background-color: lightgrey">\n            <th class=\'borderBottomForTxn tableCellTextAlignLeft\' width="4%">6</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignLeft\' width="40%">Description</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >Turnover</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >Rate of Tax(%)</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >Central Tax</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >State/UT Tax</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >Sheet Validation Error(s)</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >GST portal validation error(s)</th>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%" rowspan="3"> A</td>\n            <td class=\'borderBottomForTxn\' width="40%" rowspan="3"> Taxable</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >' + MyDouble.getAmountDecimalForGSTRReport(turnoverOne) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >1</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >' + MyDouble.getAmountDecimalForGSTRReport(turnoverOneTax) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >' + MyDouble.getAmountDecimalForGSTRReport(turnoverOneTax) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" ></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >' + MyDouble.getAmountDecimalForGSTRReport(turnoverTwo) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >2</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >' + MyDouble.getAmountDecimalForGSTRReport(turnoverTwoTax) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >' + MyDouble.getAmountDecimalForGSTRReport(turnoverTwoTax) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" ></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >' + MyDouble.getAmountDecimalForGSTRReport(turnoverFive) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >5</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >' + MyDouble.getAmountDecimalForGSTRReport(turnoverFiveTax) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >' + MyDouble.getAmountDecimalForGSTRReport(turnoverFiveTax) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" ></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"> B</td>\n            <td class=\'borderBottomForTxn\' width="40%"> Exempted, Nil Rated</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >' + MyDouble.getAmountDecimalForGSTRReport(turnOverObject.non_taxable_turn_over) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >0</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >0</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >0</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" ></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"> C</td>\n            <td class=\'borderBottomForTxn\' width="40%">Total</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >' + MyDouble.getAmountDecimalForGSTRReport(turnoverOne + turnoverTwo + turnoverFive + turnOverObject.non_taxable_turn_over) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >' + MyDouble.getAmountDecimalForGSTRReport(turnoverOneTax + turnoverTwoTax + turnoverFiveTax) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >' + MyDouble.getAmountDecimalForGSTRReport(turnoverOneTax + turnoverTwoTax + turnoverFiveTax) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" ></td>\n         </tr>\n      </table>\n      <p class = \'largerTextSize boldText\' style="background-color: lightgrey">Details of inward supplies on which tax is payable on reverse charge basis (net of debit/credit notes) for the financial year</p>\n      <table width="100%">\n         <tr style="background-color: lightgrey">\n            <th class=\'borderBottomForTxn tableCellTextAlignLeft\' width="4%">7</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignLeft\' width="40%">Description</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >Taxable value</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >Central Tax</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >State/UT Tax</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >Integrated Tax</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >Cess</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >Sheet Validation Error(s)</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >GST portal validation error(s)</th>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"> A</td>\n            <td class=\'borderBottomForTxn\' width="40%"> Inward supplies liable to reverse charge received from registered persons</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >' + MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.TaxableValuetotal7A) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >' + MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.CGSTtotal7A) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >' + MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.SGSTtotal7A) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >' + MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.IGSTtotal7A) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >' + MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.CESStotal7A) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"> B</td>\n            <td class=\'borderBottomForTxn\' width="40%">Inward supplies liable to reverse charge received from unregistered persons</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >' + MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.TaxableValuetotal7B) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >' + MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.CGSTtotal7B) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >' + MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.SGSTtotal7B) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >' + MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.IGSTtotal7B) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >' + MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.CESStotal7B) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"> C</td>\n            <td class=\'borderBottomForTxn\' width="40%"> Import of services</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"> D</td>\n            <td class=\'borderBottomForTxn\' width="40%"> Net Tax payable on (A), (B) and ( C) above</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >' + MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.TaxableValuetotal7A + GSTR_9A_totals.TaxableValuetotal7B) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >' + MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.CGSTtotal7A + GSTR_9A_totals.CGSTtotal7B) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >' + MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.SGSTtotal7A + GSTR_9A_totals.SGSTtotal7B) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >' + MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.IGSTtotal7A + GSTR_9A_totals.IGSTtotal7B) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >' + MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.CESStotal7A + GSTR_9A_totals.CESStotal7B) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n         </tr>\n      </table>\n      <p class = \'largerTextSize boldText\' style="background-color: lightgrey">8. Details of other inward supplies for the financial year</p>\n      <table width="100%">\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"> A</td>\n            <td class=\'borderBottomForTxn\' width="40%"> Inward supplies from registered persons (other than 7A above)</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >' + MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.TaxableValuetotal8A) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >' + MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.CGSTtotal8A) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >' + MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.SGSTtotal8A) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >' + MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.IGSTtotal8A) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >' + MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.CESStotal8A) + '</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"> B</td>\n            <td class=\'borderBottomForTxn\' width="40%">Import of Goods</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n         </tr>\n      </table>\n      <p class = \'largerTextSize boldText\' style="background-color: pink " align="center">Pt. III. Details of tax paid as declared in returns filed during the financial year</p>\n      <table width="100%">\n         <tr style="background-color: lightgrey">\n            <th class=\'borderBottomForTxn\' align="left" width="4%">9</th>\n            <th class=\'borderBottomForTxn\' align="left" width="40%">Description</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" >Total Tax Payable</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" >Paid</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" >Sheet Validation Error(s)</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" >GST portal validation error(s)</th>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"></td>\n            <td class=\'borderBottomForTxn\' width="40%"> Integrated Tax</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"> </td>\n            <td class=\'borderBottomForTxn\' width="40%">Central Tax</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"></td>\n            <td class=\'borderBottomForTxn\' width="40%">State/UT Tax</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"> </td>\n            <td class=\'borderBottomForTxn\' width="40%">Cess</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n           \t<td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"> </td>\n            <td class=\'borderBottomForTxn\' width="40%">Interest</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"></td>\n            <td class=\'borderBottomForTxn\' width="40%">Late fee</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"> </td>\n            <td class=\'borderBottomForTxn\' width="40%">Penalty</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" ></td>\n         </tr>\n      </table>\n      <p class = \'largerTextSize boldText\' style="background-color: pink " align="center">Pt. IV. Particulars of the transactions for the previous FY declared in returns of April to September\n         of current FY or upto date of filing of annual return of previous FY whichever is earlier\n      </p>\n      <table width="100%">\n         <tr style="background-color: lightgrey">\n            <th class=\'borderBottomForTxn\' align="left" width="4%"></th>\n            <th class=\'borderBottomForTxn\' align="left" width="40%">Description</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >Turnover</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >Central Tax</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >State/UT Tax</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >Integrated Tax</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >Cess</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >Sheet Validation Error(s)</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" >GST portal validation error(s)</th>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"><b> 10 </b></td>\n            <td class=\'borderBottomForTxn\' width="40%"> Supplies / tax (outward) declared through amendments (+) (net of debit notes)</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"><b> 11 </b></td>\n            <td class=\'borderBottomForTxn\' width="40%">Inward supplies liable to reverse charge declared through amendments (+) (net of debit notes)</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n         \t<td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"><b> 12 </b></td>\n            <td class=\'borderBottomForTxn\' width="40%">Supplies / tax (outward) reduced through amendments (-) (net of credit notes)</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n         \t<td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"><b> 13 </b></td>\n            <td class=\'borderBottomForTxn\' width="40%">Inward supplies liable to reverse charge reduced through amendments (-) (net of credit notes)</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n         \t<td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"> </td>\n            <td class=\'borderBottomForTxn\' width="40%">Turnover (6C+10-12)</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n         \t<td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="8%" ></td>\n         </tr>\n      </table>\n      <p class = \'largerTextSize boldText\' style="background-color: lightgrey " align="left">Differential tax paid on account of declaration made in 10,11,12 & 13 above</p>\n      <table width="100%">\n         <tr style="background-color: lightgrey">\n            <th class=\'borderBottomForTxn tableCellTextAlignLeft\' align="left" width="4%">14</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignLeft\' align="left" width="40%">Description</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" align="right">Payable</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" align="right">Paid</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" >Sheet Validation Error(s)</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" >GST portal validation error(s)</th>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"></td>\n            <td class=\'borderBottomForTxn\' width="40%"> Integrated Tax</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" align="right"></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"> </td>\n            <td class=\'borderBottomForTxn\' width="40%">Central Tax</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" align="right"></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"></td>\n            <td class=\'borderBottomForTxn\' width="40%">State/UT Tax</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" align="right"></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"> </td>\n            <td class=\'borderBottomForTxn\' width="40%">Cess</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" align="right"></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"> </td>\n            <td class=\'borderBottomForTxn\' width="40%">Interest</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="14%" align="right"></td>\n         </tr>\n      </table>\n      <p class = \'largerTextSize boldText\' style="background-color: pink " align="center">Pt. V. Other information</p>\n      <p class = \'largerTextSize boldText\' style="background-color: lightgrey " align="left">Particulars of Demands and Refunds</p>\n      <table width="100%">\n         <tr style="background-color: lightgrey">\n            <th class=\'borderBottomForTxn tableCellTextAlignLeft\' align="left" width="5%">15</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignLeft\' align="left" width="39%">Description</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right">Central Tax</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right">State/UT Tax</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right">Integrated Tax</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right">Cess</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right">Interest</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right">Penalty</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right">Late/fee others</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" >Sheet Validation Error(s)</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" >GST portal validation error(s)</th>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"> A</td>\n            <td class=\'borderBottomForTxn\' width="40%"> Total Refund claimed</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%">B </td>\n            <td class=\'borderBottomForTxn\' width="40%">Total Refund sanctioned</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"> C</td>\n            <td class=\'borderBottomForTxn\' width="40%">Total Refund rejected</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"> D</td>\n            <td class=\'borderBottomForTxn\' width="40%">Total Refund pending</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"> E</td>\n            <td class=\'borderBottomForTxn\' width="40%">Total demand of taxes</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"> F</td>\n            <td class=\'borderBottomForTxn\' width="40%">Total taxes paid in respect of E above</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"> G</td>\n            <td class=\'borderBottomForTxn\' width="40%">Total demands pending out of E above</td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n            <td class=\'borderBottomForTxn tableCellTextAlignRight\' width="6.2%" align="right"></td>\n         </tr>\n      </table>\n      <p class = \'largerTextSize boldText\' style="background-color: lightgrey " align="left">Details of credit reversed or availed</p>\n      <table width="100%">\n         <tr style="background-color: lightgrey">\n            <th class=\'borderBottomForTxn\' align="left" width="5%">16</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignLeft\' width="39%">Description</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" align="right">Central Tax</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" align="right">State/UT Tax</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" align="right">Integrated Tax</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" align="right">Cess</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >Sheet Validation Error(s)</th>\n            <th class=\'borderBottomForTxn tableCellTextAlignRight\' width="9.3%" >GST portal validation error(s)</th>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%"> A</td>\n            <td class=\'borderBottomForTxn\' width="40%"> Credit reversed on opting in the composition scheme (-)</td>\n            <td class=\'borderBottomForTxn\' width="9.3%" align="right"></td>\n            <td class=\'borderBottomForTxn\' width="9.3%" align="right"></td>\n            <td class=\'borderBottomForTxn\' width="9.3%" align="right"></td>\n            <td class=\'borderBottomForTxn\' width="9.3%" align="right"></td>\n            <td class=\'borderBottomForTxn\' width="9.3%" align="right"></td>\n            <td class=\'borderBottomForTxn\' width="9.3%" align="right"></td>\n         </tr>\n         <tr>\n            <td class=\'borderBottomForTxn\' width="4%">B </td>\n            <td class=\'borderBottomForTxn\' width="40%">Credit availed on opting out of the composition scheme (+)</td>\n            <td class=\'borderBottomForTxn\' width="9.3%" align="right"></td>\n            <td class=\'borderBottomForTxn\' width="9.3%" align="right"></td>\n            <td class=\'borderBottomForTxn\' width="9.3%" align="right"></td>\n            <td class=\'borderBottomForTxn\' width="9.3%" align="right"></td>\n            <td class=\'borderBottomForTxn\' width="9.3%" align="right"></td>\n            <td class=\'borderBottomForTxn\' width="9.3%" align="right"></td>\n         </tr>\n      </table>\n   </body>\n</html>';
  $('#gstr9ADiv').html(tableHTML);
};

var getHTMLTextForReport = function getHTMLTextForReport() {
  var GSTR9AReportHTMLGenerator = require('./../ReportHTMLGenerator/GSTR9AReportHTMLGenerator.js');
  var gSTR9AReportHTMLGenerator = new GSTR9AReportHTMLGenerator();
  firmId = Number($('#firmFilterOptions').val());
  var htmlText = gSTR9AReportHTMLGenerator.getHTMLText(GSTR_9A_totals, turnOverObject, firmName, gstin);
  return htmlText;
};

/// ///////////////////////////PDF/////////////////////////////////////
var openPDF = function openPDF() {
  var html = $('#htmlView').html();
  $('#loading').show(function () {
    pdfHandler.openPDF(html, true);
  });
};

function printPDF() {
  var html = $('#htmlView').html();
  pdfHandler.print(html);
}

function savePDF() {
  pdfHandler.savePDF({ type: PDFReportConstant.GSTR9A_REPORT, fromDate: startDateString, toDate: endDateString });
}

function sharePDF() {
  $('#loading').show(function () {
    pdfHandler.sharePDF();
  });
}

var closePreview = function closePreview() {
  try {
    $('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
  } catch (err) {}
};

var openPreview = function openPreview() {
  $('#loading').show(function () {
    var html = getHTMLTextForReport();
    if (html) {
      pdfHandler.generateHiddenBrowserWindowForPDF(html);
      transactionPDFHandler.openPreviewDialog(html);
    }
  });
};

/// ////////////////////////////////////////////////////////////////////
var downloadExcelFile = function downloadExcelFile() {
  var fileUtil = require('./../Utilities/FileUtil.js');
  var fileName = fileUtil.getFileName({ type: PDFReportConstant.GSTR9A_REPORT, fromDate: startDateString, toDate: endDateString });
  excelHelper.saveExcel(fileName);
};

var getObjectFor_9 = function getObjectFor_9() {
  var rowObj = [];
  rowObj.push(['Pt. III. Details of tax paid as declared in returns filed during the financial year']);
  rowObj.push([]);
  rowObj.push(['9', 'Description', 'Total Tax Payable', 'paid', 'Sheet Validation Error(s)', 'GST portal validation error(s)']);
  rowObj.push(['', 'Integrated Tax', '']);
  rowObj.push(['', 'Central Tax', '']);
  rowObj.push(['', 'State/UT Tax', '']);
  rowObj.push(['', 'Cess', '']);
  rowObj.push(['', 'Interest', '']);
  rowObj.push(['', 'Late fee', '']);
  rowObj.push(['', 'Penalty', '']);
  return rowObj;
};

var getObjectFor_7_8 = function getObjectFor_7_8() {
  var rowObj = [];
  rowObj.push(['Details of inward supplies on which tax is payable on reverse charge basis(net of debit/credit notes) for the financial year']);
  rowObj.push([]);
  rowObj.push(['7', 'Description', 'Taxable value', 'Central Tax', 'State/UT Tax', 'Integrated Tax', 'Cess', 'Sheet Validation Error(s)', 'GST portal validation error(s)']);
  rowObj.push(['A', 'Inward supplies liable to reverse charge received from registered persons', MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.TaxableValuetotal7A), MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.CGSTtotal7A), MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.SGSTtotal7A), MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.IGSTtotal7A), MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.CESStotal7A)]);
  rowObj.push(['B', 'Inward supplies  liable to reverse charge received from unregistered persons', MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.TaxableValuetotal7B), MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.CGSTtotal7B), MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.SGSTtotal7B), MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.IGSTtotal7B), MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.CESStotal7B)]);
  rowObj.push(['C', 'Import of services', '', '', '', '', '']);
  rowObj.push(['D', 'Net Tax payable on (A), (B) and (C) above', MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.TaxableValuetotal7A + GSTR_9A_totals.TaxableValuetotal7B), MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.CGSTtotal7A + GSTR_9A_totals.CGSTtotal7B), MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.SGSTtotal7A + GSTR_9A_totals.SGSTtotal7B), MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.IGSTtotal7A + GSTR_9A_totals.IGSTtotal7B), MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.CESStotal7A + GSTR_9A_totals.CESStotal7B)]);
  rowObj.push([]);
  rowObj.push(['8', 'Details of other inward supplies for the financial year']);
  rowObj.push(['A', 'Inward supplies from registered persons (other than 7A above)', MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.TaxableValuetotal8A), MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.CGSTtotal8A), MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.SGSTtotal8A), MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.IGSTtotal8A), MyDouble.getAmountDecimalForGSTRReport(GSTR_9A_totals.CESStotal8A)]);
  rowObj.push(['B', 'Import of Goods']);
  return rowObj;
};

var getObjectFor1_2_3 = function getObjectFor1_2_3() {
  var rowObj = [];
  rowObj.push(['SIMPLIFIED ANNUAL RETURN FOR COMPOUNDING TAXABLE PERSONS']);
  rowObj.push([]);
  rowObj.push(['1', 'GSTIN', gstin]);
  rowObj.push(['2', 'Name of the taxable person', firmName]);
  rowObj.push(['3', 'Financial Year', fyDateMapUI.get(fyVal)[0] + '-' + fyDateMapUI.get(fyVal)[1]]);
  return rowObj;
};

var getObjectFor_6 = function getObjectFor_6() {
  var rowObj = [];
  rowObj.push(['', '', 'Pt. II Details of outward supplies made during the financial year']);
  rowObj.push([]);
  rowObj.push(['6', 'Description', 'TurnOver', 'Rate of tax(%)', 'Central Tax', 'State/UT Tax', 'Sheet Validation Error(s)', 'GST portal validation error(s)']);
  rowObj.push(['', '', MyDouble.getAmountDecimalForGSTRReport(turnoverOne), 1, MyDouble.getAmountDecimalForGSTRReport(turnoverOneTax), MyDouble.getAmountDecimalForGSTRReport(turnoverOneTax)]);
  rowObj.push(['A', 'Taxable', MyDouble.getAmountDecimalForGSTRReport(turnoverTwo), 2, MyDouble.getAmountDecimalForGSTRReport(turnoverTwoTax), MyDouble.getAmountDecimalForGSTRReport(turnoverTwoTax)]);
  rowObj.push(['', '', MyDouble.getAmountDecimalForGSTRReport(turnoverFive), 5, MyDouble.getAmountDecimalForGSTRReport(turnoverFiveTax), MyDouble.getAmountDecimalForGSTRReport(turnoverFiveTax)]);
  rowObj.push(['B', 'Exempted, Nil Rated', MyDouble.getAmountDecimalForGSTRReport(turnOverObject.non_taxable_turn_over), 0, 0, 0]);
  rowObj.push(['C', 'Total', MyDouble.getAmountDecimalForGSTRReport(turnoverOne + turnoverTwo + turnoverFive + turnOverObject.non_taxable_turn_over), '', MyDouble.getAmountDecimalForGSTRReport(turnoverOneTax + turnoverTwoTax + turnoverFiveTax), MyDouble.getAmountDecimalForGSTRReport(turnoverOneTax + turnoverTwoTax + turnoverFiveTax)]);
  return rowObj;
};

var getObjectFor_10_11_12_13_14 = function getObjectFor_10_11_12_13_14() {
  var rowObj = [];
  rowObj.push(['Pt. IV. Particulars of the transactions for the previous FY declared in returns of April to September of current FY or upto date of filing of annual return of previous FY whichever is earlier']);
  rowObj.push([]);
  rowObj.push(['', 'Description', 'Turnover', 'Central Tax', 'State/UT Tax', 'Integrated Tax', 'Cess', 'Sheet Validation Error(s)', 'GST portal validation error(s)']);
  rowObj.push(['10', 'Supplies / tax (outward) declared through amendments (+) (net of debit notes)']);
  rowObj.push(['11', 'Inward supplies liable to reverse charge declared through amendments (+) (net of debit notes)']);
  rowObj.push(['12', 'Supplies / tax (outward) reduced through amendments (-) (net of credit notes)']);
  rowObj.push(['13', 'Inward supplies liable to reverse charge reduced through amendments (-) (net of credit notes)']);
  rowObj.push(['', 'Turnover (6C+10-12)']);
  rowObj.push([]);
  rowObj.push(['Differential tax paid on account of declaration made in 10,11,12 & 13 above']);
  rowObj.push(['14', 'Description', 'Payable', 'Paid', 'Sheet Validation Error(s)', 'GST portal validation error(s)']);
  rowObj.push(['', 'Integrated Tax']);
  rowObj.push(['', 'Central Tax']);
  rowObj.push(['', 'State/UT Tax']);
  rowObj.push(['', 'Cess']);
  return rowObj;
};

var getObjectFor_15_16 = function getObjectFor_15_16() {
  var rowObj = [];
  rowObj.push(['Pt. V. Other information']);
  rowObj.push([]);
  rowObj.push(['Particulars of Demands and Refunds']);
  rowObj.push([]);
  rowObj.push(['15', 'Description', 'Central Tax', 'State/UT Tax', 'Integrated Tax', 'Cess', 'Interest', 'Penalty', 'Late/fee others', 'Sheet Validation Error(s)', 'GST portal validation error(s)']);
  rowObj.push(['A', 'Total Refund claimed']);
  rowObj.push(['B', 'Total Refund sanctioned']);
  rowObj.push(['C', 'Total Refund rejected']);
  rowObj.push(['D', 'Total Refund pending']);
  rowObj.push(['E', 'Total demand of taxes']);
  rowObj.push(['F', 'Total taxes paid in respect of E above']);
  rowObj.push(['G', 'Total demands pending out of E above']);
  rowObj.push([]);
  rowObj.push(['Details of credit reversed or availed']);
  rowObj.push(['16', 'Description', 'Central Tax', 'State/UT Tax', 'Integrated Tax', 'Cess', 'Sheet Validation Error(s)', 'GST portal validation error(s)']);
  rowObj.push(['A', 'Credit reversed on opting in the composition scheme (-)']);
  rowObj.push(['B', 'Credit availed on opting out of the composition scheme (+)']);
  return rowObj;
};

function printExcel() {
  var XLSX = require('xlsx');

  workbook = {
    'SheetNames': [],
    'Sheets': {}
  };

  /// /////////////////////////////////5A1///////////////////////////////////////

  var wsNameItem = '1_2_3';
  workbook.SheetNames[0] = wsNameItem;
  var data5A1bArray = getObjectFor1_2_3();
  var data5A1Worksheet = XLSX.utils.aoa_to_sheet(data5A1bArray);
  workbook.Sheets[wsNameItem] = data5A1Worksheet;
  var wsItemcolWidth = [{ wch: 10 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
  data5A1Worksheet['!cols'] = wsItemcolWidth;
  data5A1Worksheet['!merges'] = [{ s: { r: 0, c: 0 }, e: { r: 0, c: 2 } }, { s: { r: 5, c: 1 }, e: { r: 5, c: 2 } }];

  /// /////////////////////////////////5A2///////////////////////////////////////

  var wsNameItem = '6';
  workbook.SheetNames[1] = wsNameItem;
  var data5A2bArray = getObjectFor_6();
  var data5A2Worksheet = XLSX.utils.aoa_to_sheet(data5A2bArray);
  workbook.Sheets[wsNameItem] = data5A2Worksheet;
  var wsItemcolWidth = [{ wch: 10 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
  data5A2Worksheet['!cols'] = wsItemcolWidth;
  data5A2Worksheet['!merges'] = [{ s: { r: 0, c: 0 }, e: { r: 0, c: 6 } }, { s: { r: 3, c: 0 }, e: { r: 5, c: 0 } }, { s: { r: 3, c: 1 }, e: { r: 5, c: 1 } }];
  data5A2Worksheet.B4 = { t: 's', v: 'Taxable' };
  data5A2Worksheet.A4 = { t: 's', v: 'A' };
  data5A2Worksheet.A1 = { t: 's',
    v: 'Pt. II Details of outward supplies made during the financial year'
  };

  /// /////////////////////////////////5A3///////////////////////////////////////

  var wsNameItem = '7_8';
  workbook.SheetNames[2] = wsNameItem;
  var data5A3bArray = getObjectFor_7_8();
  var data5A3Worksheet = XLSX.utils.aoa_to_sheet(data5A3bArray);
  workbook.Sheets[wsNameItem] = data5A3Worksheet;
  var wsItemcolWidth = [{ wch: 10 }, { wch: 70 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
  data5A3Worksheet['!cols'] = wsItemcolWidth;
  data5A3Worksheet['!merges'] = [{ s: { r: 0, c: 0 }, e: { r: 0, c: 6 } }];

  /// /////////////////////////////////5A4///////////////////////////////////////

  var wsNameItem = '9';
  workbook.SheetNames[3] = wsNameItem;
  var data5A4bArray = getObjectFor_9();
  var data5A4Worksheet = XLSX.utils.aoa_to_sheet(data5A4bArray);
  workbook.Sheets[wsNameItem] = data5A4Worksheet;
  var wsItemcolWidth = [{ wch: 10 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
  data5A4Worksheet['!cols'] = wsItemcolWidth;
  data5A4Worksheet['!merges'] = [{ s: { r: 0, c: 0 }, e: { r: 0, c: 3 } }];

  var wsNameItem = '10_11_12_13_14';
  workbook.SheetNames[4] = wsNameItem;
  var data5A5bArray = getObjectFor_10_11_12_13_14();
  var data5A5Worksheet = XLSX.utils.aoa_to_sheet(data5A5bArray);
  workbook.Sheets[wsNameItem] = data5A5Worksheet;
  var wsItemcolWidth = [{ wch: 10 }, { wch: 85 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
  data5A5Worksheet['!cols'] = wsItemcolWidth;
  data5A5Worksheet['!merges'] = [{ s: { r: 0, c: 0 }, e: { r: 0, c: 6 } }, { s: { r: 9, c: 0 }, e: { r: 9, c: 6 } }];

  var wsNameItem = '15_16';
  workbook.SheetNames[5] = wsNameItem;
  var data5A6bArray = getObjectFor_15_16();
  var data5A6Worksheet = XLSX.utils.aoa_to_sheet(data5A6bArray);
  workbook.Sheets[wsNameItem] = data5A6Worksheet;
  var wsItemcolWidth = [{ wch: 10 }, { wch: 70 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
  data5A6Worksheet['!cols'] = wsItemcolWidth;
  data5A6Worksheet['!merges'] = [{ s: { r: 0, c: 0 }, e: { r: 0, c: 9 } }, { s: { r: 2, c: 0 }, e: { r: 2, c: 9 } }, { s: { r: 13, c: 0 }, e: { r: 13, c: 9 } }];

  var XLSX = require('xlsx');
  XLSX.writeFile(workbook, appPath + '/report.xlsx');
  downloadExcelFile();
}

populateTable();