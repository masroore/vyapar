var ItemUnitLogic = require('./../BizLogic/ItemUnitLogic.js');
var ItemUnitCache = require('./../Cache/ItemUnitCache.js');
var newItemUnitLogic = new ItemUnitLogic();
var unitName = unitNameGlobal; // angular.element(document.getElementById('body')).scope().unitName;
var ErrorCode = require('../Constants/ErrorCode');
$('#fullName').focus();
if (unitName) {
	$('#submitNew').hide();
	var itemUnitCache = new ItemUnitCache();
	MyAnalytics.pushEvent('Edit Item Unit Open');
	var oldUnitObject = itemUnitCache.getItemUnitObjectByUnitName(unitName);
	var fullName = oldUnitObject.getUnitName();
	var shortName = oldUnitObject.getUnitShortName();
	$('.heading').text('View Unit');
	$('#fullName').val(fullName);
	$('#shortName').val(shortName);
	$('.submitNewItem').on('click', function () {
		var fullName = $('#fullName').val();
		var shortName = $('#shortName').val();
		var unitId = oldUnitObject.getUnitId();

		if (oldUnitObject.isFullNameEditable() || fullName.trim() == oldUnitObject.getUnitName()) {
			var statusCode = oldUnitObject.updateUnit(unitId, fullName, shortName);
			if (statusCode) {
				if (statusCode == ErrorCode.ERROR_UNIT_UPDATE_SUCCESS) {
					ToastHelper.success(statusCode);
					emptyForm();
					$('#defaultPage').show();
					if (!($('#modelContainer').css('display') == 'none')) {
						$('#modelContainer').css('display', 'none');
					}
					$('.viewItems').css('display', 'block');
					// $('.hideDefaultPage').css('display','block');
					try {
						$('#addUnits').dialog('close');
					} catch (err) {}
					window.onResume && window.onResume();
					//            viewCategoriesPage();
					MyAnalytics.pushEvent('Edit Item Unit Save');
				} else {
					ToastHelper.error(statusCode);
				}
			}
		} else {
			ToastHelper.error('Full name cannot be edited for this unit.');
		}
	});
} else {
	MyAnalytics.pushEvent('Add Item Unit Open');
	var ItemUnitCache = require('./../Cache/ItemUnitCache.js');
	var newItemUnitLogic = new ItemUnitLogic();
	$('.submitNewItem').off('click').on('click', function () {
		var fullName = $('#fullName').val();
		var shortName = $('#shortName').val();
		var statusCode = newItemUnitLogic.addNewUnit(fullName, shortName);

		if (statusCode == ErrorCode.ERROR_UNIT_SAVE_SUCCESS) {
			ToastHelper.success(statusCode);
		} else {
			ToastHelper.error(statusCode);
		}

		if (statusCode == ErrorCode.ERROR_UNIT_SAVE_SUCCESS && this.id == 'submit') {
			emptyForm();
			$('#defaultPage').show();
			if (!($('#modelContainer').css('display') == 'none')) {
				$('#modelContainer').css('display', 'none');
			}
			$('.hideDefaultPage').css('display', 'block');
			$('.viewItems').css('display', 'block');
			try {
				$('#addUnits').dialog('close');
			} catch (err) {}
			MyAnalytics.pushEvent('Add Item Unit Save');
			window.onResume && window.onResume();

			//            viewCategoriesPage();
		} else if (statusCode == ErrorCode.ERROR_UNIT_SAVE_SUCCESS && this.id == 'submitNew') {
			emptyForm();
		} else {
			window.onResume && window.onResume();
			try {
				addUnitsFunc();
			} catch (err) {}
			// console.log(err);


			// $('#frameDiv').load('NewItemUnit.html');
			MyAnalytics.pushEvent('Add Item Unit Save');
		}
	});
}

var emptyForm = function emptyForm() {
	$('#fullName').val('');
	$('#shortName').val('');
};