var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var startDate;
var endDate;
var DateFormat = require('./../Constants/DateFormat.js');
$('#startDate, #endDate').datepicker({ dateFormat: DateFormat.format });
var SettingCache = require('./../Cache/SettingCache.js');
var LoanAccountCache = require('./../Cache/LoanAccountCache.js');
var settingCache = new SettingCache();
var NameCache = require('./../Cache/NameCache.js');
var nameCache = new NameCache();
var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');
var printForSharePDF = false;

var listOfTransactions = [];

var populateTable = function populateTable() {
	settingCache = new SettingCache();
	var DataLoader = require('./../DBManager/DataLoader.js');
	var dataLoader = new DataLoader();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var firmId = Number($('#firmFilterOptions').val());
	var nameString = $('#reportPartyName').val() || '';
	var nameId = -1;
	var loanAccountName = '';
	if (nameString.startsWith('[Loan Acc]')) {
		loanAccountName = nameString.substring('[Loan Acc] '.length) || '';
	} else if (nameString) {
		var nameCache = new NameCache();
		var nameModel = nameCache.findExpenseModelByName(nameString);
		if (nameModel) {
			nameId = nameModel.getNameId();
		} else {
			nameId = 0;
		}
	}
	startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
	endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');
	CommonUtility.showLoader(function () {
		listOfTransactions = dataLoader.loadTransactionsForExpenseReport(startDate, endDate, nameId, firmId, loanAccountName);
		displayTransactionList(listOfTransactions);
	});
};

var date = MyDate.getDate('d/m/y');
$('#startDate').val(MyDate.getFirstDateOfCurrentMonthString());
$('#endDate').val(date);

if (settingCache.getMultipleFirmEnabled()) {
	var optionTemp = document.createElement('option');
	var FirmCache = require('./../Cache/FirmCache.js');
	var firmCache = new FirmCache();
	var firmList = firmCache.getFirmList();
	for (i in firmList) {
		var option = optionTemp.cloneNode();
		option.text = firmList[i].getFirmName();
		option.value = firmList[i].getFirmId();
		$('#firmFilterOptions').append(option);
	}
	$('#firmChooser').show();
} else {
	$('#firmChooser').hide();
}

$(function () {
	var loanAccountCache = new LoanAccountCache();
	var loanAccountList = loanAccountCache.getAllLoanAccounts();
	var list = loanAccountList.map(function (loanAccount) {
		return '[Loan Acc] ' + loanAccount.getLoanAccName();
	});
	list = [].concat((0, _toConsumableArray3.default)(nameCache.getListOfExpense()), (0, _toConsumableArray3.default)(list));
	$('#reportPartyName').autocomplete(UIHelper.getAutocompleteDefaultOptions(list));
});

$('#reportPartyName').on('change', function (event) {
	populateTable();
});

$('#firmFilterOptions').on('selectmenuchange', function (event) {
	var firmId = Number($('#firmFilterOptions').val());
	$(function () {
		var loanAccountCache = new LoanAccountCache();
		var loanAccountList = loanAccountCache.getAllLoanAccounts(firmId);
		var list = loanAccountList.map(function (loanAccount) {
			return '[Loan Acc] ' + loanAccount.getLoanAccName();
		});
		list = [].concat((0, _toConsumableArray3.default)(nameCache.getListOfExpense()), (0, _toConsumableArray3.default)(list));
		$('#reportPartyName').autocomplete(UIHelper.getAutocompleteDefaultOptions(list));
	});
	populateTable();
});

$('#startDate').change(function () {
	populateTable();
});

$('#endDate').change(function () {
	populateTable();
});

var expenseReportClusterize;

var displayTransactionList = function displayTransactionList(listOfTransactions) {
	var len = listOfTransactions.length;
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var dynamicRow = '';
	var rowData = [];
	var totalAmount = 0;
	dynamicRow += "<thead><tr><th width='18%'>Date</th><th width='50%'>Expense Category</th><th width='32%' class='tableCellTextAlignRight'>Paid Amount</th></tr></thead>";
	for (var _i = 0; _i < len; _i++) {
		var txn = listOfTransactions[_i];
		var typeString = TxnTypeConstant.getTxnTypeForUI(txn.getTxnType());
		var name = TxnTypeConstant.isLoanTxnType(txn.getTxnType()) ? txn.getDescription() : txn.getNameRef().getFullName();
		var date = MyDate.getDate('d/m/y', txn.getTxnDate());
		var paid = txn.getCashAmount() ? txn.getCashAmount() : 0;
		var row = "<tr ondblclick='openTransaction(" + txn.getTxnId() + ',' + txn.getTxnType() + ")' class='currentRow'><td width='18%'>" + date + '</td>';
		row += "<td width='50%'>" + name + "</td><td width='32%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(paid) + '</td></tr>';
		totalAmount += Number(paid);
		rowData.push(row);
	}
	var data = rowData;

	if ($('#expenseReportContainer').length === 0) {
		return;
	}
	if (expenseReportClusterize && expenseReportClusterize.destroy) {
		expenseReportClusterize.clear();
		expenseReportClusterize.destroy();
	}

	expenseReportClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollArea',
		contentId: 'contentArea',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});
	$('#tableHead').html(dynamicRow);
	$('#totalAmount').html(MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalAmount));
};

var openTransaction = function openTransaction(txnId, txnType) {
	var ViewTransaction = require('./../BizLogic/ViewTransaction.js');
	var viewTransaction = new ViewTransaction();
	viewTransaction.viewTransactionFile(txnId + ':' + TxnTypeConstant.getTxnType(txnType));
};

if (settingCache.getItemEnabled()) {
	$('#itemDetailPrintDiv').show();
} else {
	$('#itemDetailPrintDiv').hide();
}

var getHTMLTextForReport = function getHTMLTextForReport() {
	var ExpenseReportHTMLGenerator = require('./../ReportHTMLGenerator/ExpenseReportHTMLGenerator.js');
	var expenseReportHTMLGenerator = new ExpenseReportHTMLGenerator();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var firmId = Number($('#firmFilterOptions').val());
	var nameString = $('#reportPartyName').val();
	if (!nameString) {
		nameString = 'All Expense categories';
	}
	var printItemDetails = $('#printItems').is(':checked');
	var printDescription = $('#printDescription').is(':checked');
	var htmlText = expenseReportHTMLGenerator.getHTMLText(listOfTransactions, startDateString, endDateString, firmId, nameString, printItemDetails, printDescription);
	return htmlText;
};

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	// $('#frameDiv').load('');
	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable();
};

/// ///////////////////////////PDF/////////////////////////////////////

function openPrintOptions() {
	$('#expensePrintDialog').removeClass('hide');
	$('#expensePrintDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closePrintOptions() {
	$('#expensePrintDialog').addClass('hide');
	$('#expensePrintDialog').dialog('close').dialog('destroy');
}

var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.EXPENSE_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#expensePrintDialog').addClass('hide');
	$('#expensePrintDialog').dialog('close').dialog('destroy');
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

function openExcelOptions() {
	$('#ExcelDialog').removeClass('hide');
	$('#ExcelDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closeExcelOptions() {
	$('#ExcelDialog').addClass('hide');
	$('#ExcelDialog').dialog('close').dialog('destroy');
}

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.EXPENSE_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
	excelHelper.saveExcel(fileName);
};

var prepareObjectForExcel = function prepareObjectForExcel(listOfTransactions) {
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var len = listOfTransactions.length;
	var ExcelDescription = $('#ExcelExpenseDescription').is(':checked');
	var rowObj = [];
	var tableHeadArray = [];
	var totalArray = [];
	tableHeadArray.push('Date');
	tableHeadArray.push('Expense Category');
	tableHeadArray.push('Paid Amount');
	if (ExcelDescription) {
		tableHeadArray.push('Description');
	}
	totalArray.push('');
	rowObj.push(tableHeadArray);
	rowObj.push([]);

	var totalAmount = 0;
	for (var j = 0; j < len; j++) {
		var txn = listOfTransactions[j];
		var typeString = TxnTypeConstant.getTxnTypeForUI(txn.getTxnType());
		var name = TxnTypeConstant.isLoanTxnType(txn.getTxnType()) ? txn.getDescription() : txn.getNameRef().getFullName();
		var date = MyDate.getDate('d/m/y', txn.getTxnDate());
		var paid = txn.getCashAmount() ? txn.getCashAmount() : 0;
		var tempArray = [];
		tempArray.push(date);
		tempArray.push(name);
		tempArray.push(MyDouble.getAmountWithDecimal(paid));
		totalAmount += paid;
		if (ExcelDescription) {
			tempArray.push(txn.getDescription());
		}
		rowObj.push(tempArray);
	}
	rowObj.push([]);
	totalArray.push('Total');
	totalArray.push(MyDouble.getAmountWithDecimal(totalAmount));
	rowObj.push(totalArray);
	return rowObj;
};

function printExcel() {
	closeExcelOptions();
	var XLSX = require('xlsx');
	var dataArray = prepareObjectForExcel(listOfTransactions);
	var worksheet = XLSX.utils.aoa_to_sheet(dataArray);
	var ExcelItemDetails = $('#ExcelExpenseItemDetails').is(':checked');
	var ExcelExportHelper = require('./../UIControllers/ExcelExportHelper.js');
	var excelExportHelper = new ExcelExportHelper();

	var workbook = {
		'SheetNames': [],
		'Sheets': {}
	};

	var wsName = 'Expense Report';
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet;
	var wscolWidth = [{ wch: 15 }, { wch: 30 }, { wch: 15 }];
	worksheet['!cols'] = wscolWidth;

	if (ExcelItemDetails) {
		var wsNameItem = 'Item Details';
		workbook.SheetNames[1] = wsNameItem;
		var itemArray = excelExportHelper.prepareItemObjectForExcel(listOfTransactions);
		var itemWorksheet = XLSX.utils.aoa_to_sheet(itemArray);
		workbook.Sheets[wsNameItem] = itemWorksheet;
		var wsItemcolWidth = [{ wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }];
		itemWorksheet['!cols'] = wsItemcolWidth;
	}
	console.log(workbook);
	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

populateTable();

function loadReportPage(reportFileName) {
	if (document.getElementById('reportPage')) {
		changeReportPage(reportFileName);
	} else {
		$('#defaultPage').load(reportFileName);
	}
}