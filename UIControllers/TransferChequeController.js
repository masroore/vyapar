var GetDate = require('./../BizLogic/GetDate.js');
var getDate = new GetDate();
var date = getDate.getDate('d/m/y');

var DataLoader = require('./../DBManager/DataLoader.js');
var dataloader = new DataLoader();

var chequeId = TransferChequeIdGlobal; // angular.element(document.getElementById('body')).scope().TransferChequeId;
var chequeDetail = dataloader.loadChequeById(chequeId);
var txnId = chequeDetail.getChequeTxnId() || chequeDetail.getChequeClosedTxnRefId();
var cashAmount = chequeDetail['chequeAmount'];
var chequeRefNo = chequeDetail.getChequeTxnRefNo();
var txnDetails = chequeDetail.getChequeClosedTxnRefId() ? dataloader.LoadTransactionFromIdForClosedTxn(txnId) : dataloader.LoadTransactionFromId(txnId);
var userName = txnDetails.getNameRef().getFullName();
var txnType = txnDetails.getTxnType();

$(function () {
	var DateFormat = require('./../Constants/DateFormat.js');
	$('#datepicker').datepicker({ dateFormat: DateFormat.format,
		onSelect: function onSelect() {
			this.focus();
		},
		onClose: function onClose() {
			this.focus();
		}
	});
});

//        console.log(date);
$('#datepicker').val(date);
MyAnalytics.pushScreen('Transfer Cheque');

var chequeType;
document.getElementById('userName').innerText = ': ' + userName.toUpperCase();
document.getElementById('chequeRefNo').innerText = chequeRefNo ? ': ' + chequeRefNo : ':';
document.getElementById('chequeAmount').innerText = ': ' + cashAmount;
// document.getElementById('txnName').innerText = (Number(txnType) == 1) ? "Deposit to" : "Withdraw from";
if (txnType == TxnTypeConstant.TXN_TYPE_CASHOUT || txnType == TxnTypeConstant.TXN_TYPE_EXPENSE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
	chequeType = 'Withdraw';
	$('#userNameHeader').html('Paid To');
	$('#txnName').html('Withdraw From: ');
} else {
	chequeType = 'Deposit';
	$('#userNameHeader').html('Received From');
	$('#txnName').html('Deposit To:');
}
MyAnalytics.pushEvent(chequeType + ' Cheque Open');
// to get list of payment type
var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
var paymentinfocache = new PaymentInfoCache();
var listOfAccountName = paymentinfocache.getListOfPaymentAccountName();
console.log(listOfAccountName);
var optionTemp = document.createElement('option');
var accountName = document.getElementById('accountName');
var len = listOfAccountName.length;
for (var i = 0; i < len; i++) {
	var option = optionTemp.cloneNode();
	option.text = listOfAccountName[i];
	option.value = listOfAccountName[i];
	if (option.text !== 'Cheque') {
		accountName.add(option);
	}
}

function saveChequeStatus() {
	debugger;
	var ErrorCode = require('./../Constants/ErrorCode.js');
	var statusCode = ErrorCode.SUCCESS;
	var ChequeLogic = require('./../BizLogic/ChequeLogic.js');
	var chequelogic = new ChequeLogic();
	var ChequeStatus = require('./../Constants/ChequeStatus.js');
	chequelogic.setChequeId(chequeId);
	chequelogic.setChequeCurrentStatus(ChequeStatus.CLOSE);
	var selectedName = document.getElementById('accountName').value;
	MyAnalytics.pushEvent(chequeType + ' Cheque Save');
	var selectedAccountId = 1;
	selectedAccountId = paymentinfocache.getPaymentBankId(selectedName);
	// console.log(selectedAccountId);
	chequelogic.setTransferredToAccount(selectedAccountId);
	var description = document.getElementById('description').value;
	chequelogic.setChequeCloseDescription(description);
	// var MyDate = require('./../Utilities/MyDate.js');
	var date = document.getElementById('datepicker').value;
	date = getDate.getDateObj(date, 'dd/mm/yyyy', '/');
	chequelogic.setTransferDate(date);
	statusCode = chequelogic.updateChequeStatus();

	if (statusCode == ErrorCode.ERROR_CHEQUE_STATUS_UPDATE_SUCCESS) {
		ToastHelper.success(statusCode);
	} else {
		ToastHelper.error(statusCode);
	}
	$('#frameDiv').empty();
	$('#modelContainer').hide();
	$('#defaultPage').show();
	onResume();
}

document.getElementById('submitTransferCheque').addEventListener('click', saveChequeStatus);