/**
 * Created by Ashish on 9/28/2017.
 */
var NameCache = require('./../Cache/NameCache.js');
var nameCache = new NameCache();
var TransactionFactory = require('./../BizLogic/TransactionFactory.js');
var transactionfactory = new TransactionFactory();

var TransactionPDFHandler = function TransactionPDFHandler() {
	this.transactionToHTML = function (param, perviewAsDeliveryChallan) {
		var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
		if (!param) {
			var loadingElement = $('#loading');
			if (loadingElement) {
				loadingElement.hide();
			}
			return;
		}
		var txnId = param.split(':')[0];
		var txnType = param.split(':')[1];
		txnType = TxnTypeConstant[txnType];
		var salePurchaseFile = [TxnTypeConstant.TXN_TYPE_SALE, TxnTypeConstant.TXN_TYPE_PURCHASE, TxnTypeConstant.TXN_TYPE_CASHIN, TxnTypeConstant.TXN_TYPE_CASHOUT, TxnTypeConstant.TXN_TYPE_EXPENSE, TxnTypeConstant.TXN_TYPE_SALE_RETURN, TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN, TxnTypeConstant.TXN_TYPE_SALE_ORDER, TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN, TxnTypeConstant.TXN_TYPE_ESTIMATE, TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER, TxnTypeConstant.TXN_TYPE_OTHER_INCOME];
		if (salePurchaseFile.indexOf(Number(txnType)) != -1) {
			var TransactionHTMLGenerator = require('./../ReportHTMLGenerator/TransactionHTMLGenerator.js');
			var transactionHTMLGenerator = new TransactionHTMLGenerator();
			var html = transactionHTMLGenerator.getTransactionHTML(txnId, perviewAsDeliveryChallan);
			return html;
		} else {
			ToastHelper.error('this transaction cant be printed');
			$('#loading').hide();
		}
	};

	this.openPreviewDialog = function (html, txnId, pdfHandlerArg, closePreviewArg) {
		$('#pdfViewDialog').removeClass('hide').dialog({
			'width': '75%',
			'modal': true,
			closeOnEscape: true,
			close: function close() {
				try {
					if (typeof pdfHandlerArg !== 'undefined') {
						pdfHandlerArg.closeWindow();
					} else {
						pdfHandler.closeWindow();
					}

					closePreviewArg ? closePreviewArg() : closePreview();
				} catch (err) {
					console.log(err);
				}
			}
		});
		$('#htmlView').html(html);
		if (txnId) {
			var DataLoader = require('./../DBManager/DataLoader.js');
			var dataLoader = new DataLoader();
			var transactionModel = dataLoader.getTxnModelForTxnId(txnId);

			var txnType = transactionModel.get_txn_type();
			var txnSubType = transactionModel.get_txn_sub_type();
			var customHeader = transactionfactory.getTransTypeStringForTransactionPDF(Number(txnType), Number(txnSubType));

			var txnNumber = transactionModel.get_txn_ref_no();
			if (txnNumber) {
				var invoicePrefix = transactionModel.get_invoice_prefix();
				if (invoicePrefix) {
					customHeader += '_' + invoicePrefix + txnNumber;
				} else {
					customHeader += '_' + txnNumber;
				}
			}
			$('#savePDF').attr('data-customheader', customHeader);
			var txnDate = transactionModel.get_txn_date();
			txnDate = MyDate.getDate('d/m/y', txnDate);
			$('#savePDF').attr('data-filedate', txnDate);
			$('#sharePDF').attr('data-receiverid', nameCache.findNameObjectByNameId(transactionModel.get_txn_name_id()).getEmail());
		}

		try {
			$('#loading').hide();
		} catch (err) {}
		$('#pdfViewDialog').dialog('option', 'position', { my: 'center', at: 'center', of: window });
	};
};

module.exports = TransactionPDFHandler;