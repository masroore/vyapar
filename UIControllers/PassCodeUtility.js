var PassCodeUtitlity = function () {
	var SettingCache = require('./../Cache/SettingCache');
	var settingCache = new SettingCache();
	var Queries = require('./../Constants/Queries.js');
	var SettingsModel = require('./../Models/SettingsModel.js');
	var settingsModel = new SettingsModel();

	var _require = require('electron'),
	    ipcRenderer = _require.ipcRenderer,
	    remote = _require.remote;

	var ErrorCode = require('./../Constants/ErrorCode.js');
	var passcodeForAppOrDelete;
	var prevValue = '0';

	$(function () {
		$('#oldPasscode input').on('keyup', function () {
			$('#terminalButtonPassCode').hide();
			if (event.keyCode == 8 && this.id != 'o1') {
				$(this).prev().focus();
			} else if (this.value.length == 1 && this.id != 'o4') {
				$(this).next().focus();
			}
			var o1 = $('#o1').val();
			var o2 = $('#o2').val();
			var o3 = $('#o3').val();
			var o4 = $('#o4').val();
			var oldPasscode = o1 + '' + o2 + '' + o3 + '' + o4;
			var statusCode = '';
			if (oldPasscode && oldPasscode.length == 4) {
				var PassCodeHelperUtility = require('../Utilities/passCodeHelperUtitlity');
				statusCode = PassCodeHelperUtility.validateOldPasscode(oldPasscode);
				if (statusCode == ErrorCode.ERROR_OLD_PASSCODE_CORRECT) {
					$('#newPasscode').find('input').val('').prop('disabled', false);
					$('#oldPasscodeError').text('');
					$('#p1').focus();
				} else {
					$('#oldPasscodeError').text(statusCode);
					$('#oldPasscode input').val('');
					$('#o1').focus();
				}
			}
		});

		$('#newPasscode input').on('keyup', function () {
			$('#terminalButtonPassCode').hide();
			if (event.keyCode == 8 && this.id != 'p1') {
				$(this).prev().focus();
			} else if (this.value.length == 1 && this.id != 'p4') {
				$(this).next().focus();
			}
			var p1 = $('#p1').val();
			var p2 = $('#p2').val();
			var p3 = $('#p3').val();
			var p4 = $('#p4').val();
			var passcode = p1 + '' + p2 + '' + p3 + '' + p4;
			if (passcode && passcode.length == 4) {
				$('#passCodeDialog').find('#confirmPasscode').find('input').val('').prop('disabled', false);
				$('#oldPasscodeError').text('');
				$('#passcodeError').text('');
				$('#c1').focus();
			}
		});

		$('#confirmPasscode input').on('keyup', function () {
			if (event.keyCode == 8 && this.id != 'c1') {
				$(this).prev().focus();
			} else if (this.value.length == 1 && this.id != 'c4') {
				$(this).next().focus();
			}
			var c1 = $('#c1').val();
			var c2 = $('#c2').val();
			var c3 = $('#c3').val();
			var c4 = $('#c4').val();
			var confirmPasscode = c1 + '' + c2 + '' + c3 + '' + c4;
			if (confirmPasscode && confirmPasscode.length == 4) {
				var p1 = $('#p1').val();
				var p2 = $('#p2').val();
				var p3 = $('#p3').val();
				var p4 = $('#p4').val();
				var passcode = p1 + '' + p2 + '' + p3 + '' + p4;
				if (confirmPasscode == passcode) {
					$('#terminalButtonPassCode').show();
					$('#confirmPasscodeError').text('');
				} else {
					$('#confirmPasscodeError').text('Passcode mismatch. Please try again.');
					$('#confirmPasscode').find('input').val('');
					$('#c1').focus();
				}
			} else {
				$('#terminalButtonPassCode').hide();
			}
		});

		$('#deletePasscode input').on('keyup', function () {
			if (event.keyCode == 8 && this.id != 'd1') {
				$(this).prev().focus();
			} else if (this.value.length == 1 && this.id != 'd4') {
				$(this).next().focus();
			}
			var d1 = $('#d1').val();
			var d2 = $('#d2').val();
			var d3 = $('#d3').val();
			var d4 = $('#d4').val();
			var deletePasscode = d1 + '' + d2 + '' + d3 + '' + d4;
			if (deletePasscode && deletePasscode.length == 4) {
				$('#terminalButtonDelPassCode').show();
				$('#confirmPasscodeError').text('');
			} else {
				$('#terminalButtonPassCode').hide();
			}
		});

		$('#forgotDeletePasscode').on('click', function () {
			var PassCodeHelperUtility = require('../Utilities/passCodeHelperUtitlity');
			PassCodeHelperUtility.resetPassCode({
				passcodeType: 'deleteTxn'
			});
		});
	});

	return {
		enablePasscode: function enablePasscode() {
			passcodeForAppOrDelete = 0;
			$('#oldPasscode').closest('td').hide();
			$('#newPasscode input').prop('disabled', false);
			$('#confirmPasscode input').prop('disabled', false);
			$('#confirmPasscode').closest('td').show();
			$('#newPasscode').closest('td').show();
			$('#passCodeDialog input').val('');
			$('#passCodeDialog').removeClass('hide').dialog({
				'width': '400px',
				modal: true,
				close: function close() {
					$('#passCodeDialog p').text('');
					$(this).dialog('destroy');
					$('#terminalButtonPassCode').hide();
				}
			});
		},

		deletePasscode: function deletePasscode(isChecked, deletePasscodeCallback) {
			if (isChecked) {
				passcodeForAppOrDelete = 1;
				$('#oldPasscode').closest('td').hide();
				$('#confirmPasscode').closest('td').show();
				$('#newPasscode').closest('td').show();
				$('#newPasscode input').prop('disabled', false);
				$('#confirmPasscode input').prop('disabled', false);
				$(this).prop('checked', false);
				$('#passCodeDialog input').val('');
				$('#passCodeDialog').removeClass('hide').dialog({
					'width': '400px',
					modal: true,
					close: function close() {
						$('#passCodeDialog p').text('');
						$(this).dialog('close');
						$(this).dialog('destroy');
						$('#terminalButtonPassCode').hide();
						if (deletePasscodeCallback && typeof deletePasscodeCallback === 'function') {
							deletePasscodeCallback();
						}
					}
				});
			} else {
				$('#deletePasscode').closest('td').show();
				$(this).prop('checked', true);
				$('#passDeleteDialog input').prop('disabled', false);
				$('#passDeleteDialog input').val('');
				$('#passDeleteDialog').removeClass('hide').dialog({
					'width': '400px',
					modal: true,
					close: function close() {
						$('#deletePasscodeDiv').find('.editField').prop('checked', true);
						$('#passDeleteDialog p').text('');
						$(this).dialog('close');
						$(this).dialog('destroy');
						$('#terminalButtonPassCode').hide();
						if (deletePasscodeCallback && typeof deletePasscodeCallback === 'function') {
							deletePasscodeCallback();
						}
					}
				});
			}
		},

		changePasscode: function changePasscode() {
			passcodeForAppOrDelete = 0;
			$('#oldPasscode').closest('td').show();
			$('#passCodeDialog input').val('');
			$('#newPasscode, #confirmPasscode').find('input').val('').prop('disabled', true);
			$('#passCodeDialog').removeClass('hide').dialog({
				'width': '500px',
				modal: true,
				close: function close() {
					$('#passCodeDialog p').text('');
					$(this).dialog('destroy');
					$('#terminalButtonPassCode').hide();
				}
			});
		},

		deletePasscodeFunction: function deletePasscodeFunction() {
			var d1 = $('#d1').val();
			var d2 = $('#d2').val();
			var d3 = $('#d3').val();
			var d4 = $('#d4').val();
			var deletePasscode = d1 + '' + d2 + '' + d3 + '' + d4;
			if (deletePasscode == settingCache.getDeletePasscode()) {
				var conf = confirm('Are you sure you want to remove your passcode?');
				if (conf) {
					settingsModel.setSettingKey(Queries.SETTING_DELETE_PASSCODE_ENABLED);
					var statusCode = settingsModel.UpdateSetting('0', { nonSyncableSetting: true });
					settingsModel.setSettingKey(Queries.SETTING_DELETE_PASSCODE);
					statusCode = settingsModel.UpdateSetting(null, { nonSyncableSetting: true });
					if (statusCode == ErrorCode.ERROR_SETTING_SAVE_SUCCESS) {
						ToastHelper.success(statusCode);
						$('#deletePasscodeDiv').find('.editField').prop('checked', false);
						$('#updatePasscode').hide();
					} else {
						ToastHelper.error(statusCode);
					}
				} else {
					$('#updatePasscode').show();
					$(this).prop('checked', prevValue);
					return false;
				}
				$('#passDeleteDialog').dialog('close');
			} else {
				$('#deletePasscodeError').text('Passcode mismatch. Please try again.');
				$('#deletePasscode').find('input').val('');
				$('#d1').focus();
			}
		},

		submitPassCodeFunction: function submitPassCodeFunction() {
			var c1 = $('#c1').val();
			var c2 = $('#c2').val();
			var c3 = $('#c3').val();
			var c4 = $('#c4').val();
			var confirmPasscode = c1 + '' + c2 + '' + c3 + '' + c4;
			var statusCode = '';
			if (passcodeForAppOrDelete == 0) {
				var PassCodeHelperUtility = require('../Utilities/passCodeHelperUtitlity');
				statusCode = PassCodeHelperUtility.updatePassCode(confirmPasscode);
				if (statusCode == ErrorCode.ERROR_PASSCODE_UPDATE_SUCCESS) {
					var app = remote.app;

					$('#passcodeDiv').find('.editField').prop('checked', true);
					$('#passCodeDialog').dialog('close');
					$('#updatePasscode').show();
					alert('Passcode updated successfully. Application will restart now.');
					app.hasUserPassCodeEntered = false;
					ipcRenderer.send('changeURL', 'Index1.html');
					$('#passCodeDialog').dialog('close');
				}
			} else {
				settingsModel.setSettingKey(Queries.SETTING_DELETE_PASSCODE);
				statusCode = settingsModel.UpdateSetting(confirmPasscode, { nonSyncableSetting: true });
				settingsModel.setSettingKey(Queries.SETTING_DELETE_PASSCODE_ENABLED);
				statusCode = settingsModel.UpdateSetting('1', { nonSyncableSetting: true });
				if (statusCode == ErrorCode.ERROR_SETTING_SAVE_SUCCESS) {
					$('#deletePasscodeDiv').find('.editField').prop('checked', true);

					$('#passCodeDialog').dialog('close');
				}
			}

			if (statusCode == ErrorCode.ERROR_PASSCODE_UPDATE_SUCCESS || statusCode == ErrorCode.ERROR_SETTING_SAVE_SUCCESS) {
				ToastHelper.success(statusCode);
			} else {
				ToastHelper.error(statusCode);
			}
		}
	};
}();

module.exports = PassCodeUtitlity;