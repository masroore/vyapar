/* global jQuery */

var logger = require('../Utilities/logger');

module.exports = function ($) {
	var SupportApp = {
		defaults: {
			mountPoint: '',
			$el: ''
		},
		mounted: false,
		init: function init() {
			var defaults = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

			if (this.mounted) {
				return false;
			}
			this.mounted = true;
			this.defaults = $.extend(this.defaults, defaults);
			this.defaults.mountPoint = $(this.defaults.mountPoint);
			this.render();
			this.defaults.$el = this.defaults.mountPoint.find('#supportAppContainer');
			this.attachEvents();
		},
		render: function render() {
			var html = this.getMainTemplate();
			var $mountPoint = this.defaults.mountPoint;
			$mountPoint.html(html);
			$mountPoint.dialog({
				width: 590,
				modal: true,
				close: function (e) {
					e.stopPropagation();
					this.mounted = false;
					this.defaults.$el.off('click', '**');
					$mountPoint.html('');
					$mountPoint.dialog('destroy');
				}.bind(this)
			});
		},
		attachEvents: function attachEvents() {
			this.defaults.$el.on('click', '#btnLaunch', this.launch.bind(this)).on('click', '#btnCancel', this.cancel.bind(this));
		},
		launch: function launch() {
			var MyAnalytics = require('../Utilities/analyticsHelper');
			var path = require('path');

			var app = require('electron').remote.app;

			MyAnalytics.pushEvent('Open Support', {
				Action: 'Click'
			});
			var exeName = 'AnyDesk.exe';
			var appUserDataPath = app.getPath('userData');
			var downloadPath = path.join(appUserDataPath, '/../../../Downloads/' + exeName);

			var _require = require('electron'),
			    shell = _require.shell;
			/* Launch App
   * Order of execution
   * 1. Execute from Program Files (x86) - (64 bit install)
   * 2. Execute from Program Files - (32 bit install)
   * 3. Try to find in download folder and execute it
   * 4. Open their download link in browser; It will directly start the downloading
    */


			shell.openExternal('C:\\Program Files (x86)/AnyDesk/AnyDesk.exe') || shell.openExternal('C:\\Program Files/AnyDesk/AnyDesk.exe') || shell.openExternal(downloadPath) || shell.openExternal('https://download.anydesk.com/AnyDesk.exe');
			this.cancel();
		},
		cancel: function cancel() {
			try {
				var $mountPoint = this.defaults.mountPoint;
				$mountPoint.dialog('close');
			} catch (err) {
				logger.error(err);
			}
		},
		getMainTemplate: function getMainTemplate() {
			return '\n                <div id="supportAppContainer">\n                    <div class="d-flex flex-row align-items-center">\n                        <div class="p-20"><img src="../inlineSVG/screenshare.svg"/></div>\n                        <div>Connect remotely to Vyapar Customer Care team and share your screen with our support executives. Do you want to continue?</div>\n                    </div>\n                    <div class="actionsContainer">\n                        <button class="terminalButton" id="btnCancel">Cancel</button>\n                        <button class="terminalButton" autofocus id="btnLaunch">Connect</button>\n                    </div>\n                </div>\n            ';
		}
	};

	return SupportApp.init.bind(SupportApp);
}(jQuery);