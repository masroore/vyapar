;(function ($) {
	var ItemCache = require('./../Cache/ItemCache.js');
	var itemCache = new ItemCache();
	var SettingCache = require('./../Cache/SettingCache.js');
	var settingCache = new SettingCache();
	var PDFHandler = require('./../Utilities/PDFHandler.js');
	var pdfHandler = new PDFHandler();
	var ExcelHelper = require('./../Utilities/ExcelHelper.js');
	var excelHelper = new ExcelHelper();
	var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
	var transactionPDFHandler = new TransactionPDFHandler();
	var ItemSummaryReportObject = require('./../BizLogic/ItemSummaryReportObject.js');
	var ItemTypes = require('./../Constants/ItemType.js');
	var PDFReportConstant = require('./../Constants/PDFReportConstant.js');
	var itemListToBeShown = [];

	var populateTable = function populateTable() {
		itemCache = new ItemCache();
		settingCache = new SettingCache();

		CommonUtility.showLoader(function () {
			var groupFilter = Number($('#groupFilterOptions').val());
			var showItemsOnlyInStock = $('#zeroStockFilter').prop('checked');
			var itemList = itemCache.getListOfItemsObject($('#showInactiveItems').is(':checked'));
			itemListToBeShown = [];
			for (var _i in itemList) {
				var itemObject = itemList[_i];
				if (groupFilter > 0 && itemObject.getItemCategoryId() != groupFilter) {
					continue;
				}
				if (Number(itemObject.getItemStockQuantity()) > MyDouble.convertStringToDouble(itemObject.getItemMinStockQuantity())) {
					continue;
				}

				if (Number(itemObject.getItemType()) !== ItemTypes.ITEM_TYPE_INVENTORY) {
					continue;
				}

				if (showItemsOnlyInStock && Number(itemObject.getItemStockQuantity()) <= 0) {
					continue;
				}
				itemListToBeShown.push(itemObject);
			}

			sortItemListForSummaryReport(itemListToBeShown);
			displayItemList(itemListToBeShown);
		});
	};

	$('#showInactiveItems').on('change', function () {
		populateTable();
	});

	if (itemCache.IsAnyItemInactive()) {
		$('#activeInactiveItems').removeClass('hide');
	} else {
		$('#activeInactiveItems').addClass('hide');
	}

	function sortItemListForSummaryReport(itemListToBeShown) {
		itemListToBeShown.sort(function (a, b) {
			a = a.getItemName();
			b = b.getItemName();
			if (a.toLowerCase() > b.toLowerCase()) return 1;
			if (a.toLowerCase() < b.toLowerCase()) return -1;
			return 0;
		});
	}

	if (settingCache.isItemCategoryEnabled()) {
		var ItemCategoryCache = require('./../Cache/ItemCategoryCache.js');
		var itemCategoryCache = new ItemCategoryCache();
		var itemCategoryList = itemCategoryCache.getItemCategoriesList();

		for (i in itemCategoryList) {
			var option = $('<option />').val(itemCategoryList[i].getCategoryId()).text(itemCategoryList[i].getCategoryName());

			$('#groupFilterOptions').append(option);
		}

		$('#stockSummaryCategoryFilter').show();
	} else {
		$('#stockSummaryCategoryFilter').hide();
	}

	$('#groupFilterOptions').on('selectmenuchange', function (event) {
		populateTable();
	});

	$('#zeroStockFilter').change(function () {
		populateTable();
	});

	var stockSummaryReportClusterize;

	var displayItemList = function displayItemList(itemListToBeShown) {
		var len = itemListToBeShown.length;

		var dynamicRow = '';
		var rowData = [];

		dynamicRow += "<thead><tr><th width='7%'></th><th width='21%'>Item Name</th>";
		dynamicRow += "<th width='18%' class='tableCellTextAlignRight'>Minimum stock qty</th><th width='18%' class='tableCellTextAlignRight'>Stock qty</th><th width='18%' class='tableCellTextAlignRight'>Stock value</th>";
		dynamicRow += '</tr></thead>';
		for (var _i2 = 0; _i2 < len; _i2++) {
			var itemObject = itemListToBeShown[_i2];
			var row = '';

			row = "<tr class='item-wraper-row' data-item-id='" + itemObject.getItemId() + "'  class='currentRow'><td width='7%'>" + (_i2 + 1) + "</td><td width='21%'>" + itemObject.getItemName() + '</td>';

			row += "<td class='tableCellTextAlignRight' width='18%'>" + MyDouble.getQuantityWithDecimalWithoutColor(itemObject.getItemMinStockQuantity()) + "</td><td class='tableCellTextAlignRight' width='18%'>" + MyDouble.getQuantityWithDecimalWithoutColor(itemObject.getItemStockQuantity()) + "</td><td class='tableCellTextAlignRight' width='18%'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(itemObject.getItemStockValue()) + '</td>';

			row += '</tr>';
			rowData.push(row);
		}
		var data = rowData;

		if ($('#lowStockSummaryReportContainer').length === 0) {
			return;
		}
		if (stockSummaryReportClusterize && stockSummaryReportClusterize.destroy) {
			stockSummaryReportClusterize.clear();
			stockSummaryReportClusterize.destroy();
		}
		stockSummaryReportClusterize = new Clusterize({
			rows: data,
			scrollId: 'scrollArea',
			contentId: 'contentArea',
			rows_in_block: StringConstant.clusterizeRow,
			blocks_in_cluster: StringConstant.clusterizeBlock
		});
		$('#tableHead').html(dynamicRow);
	};

	var getHTMLTextForReport = function getHTMLTextForReport() {
		var LowStockSummaryReportHTMLGenerator = require('./../ReportHTMLGenerator/LowStockSummaryReportHTMLGenerator.js');
		var lowStockSummaryReportHTMLGenerator = new LowStockSummaryReportHTMLGenerator();

		var printStockQuantity = $('#printStockQuantity').is(':checked');
		var printMinStockQuantity = $('#printMinStockQuantity').is(':checked');
		var printStockValue = $('#printStockValue').is(':checked');

		var htmlText = lowStockSummaryReportHTMLGenerator.getHTMLText(itemListToBeShown, printMinStockQuantity, printStockQuantity, printStockValue);
		return htmlText;
	};

	window.onResume = function () {
		var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

		// $('#frameDiv').load('');
		if (notFromAutoSyncFlow) {
			$('#modelContainer').css({ 'display': 'none' });
			$('.viewItems').css('display', 'block');
		}
		populateTable();
	};

	$('#openPreview').off('click').on('click', function (e) {
		openPreview();
	});

	$('#openPrintOptions').off('click').on('click', function (e) {
		openPrintOptions();
	});

	$('#closePrintOptions').off('click').on('click', function (e) {
		closePrintOptions();
	});

	$('#closePreview').off('click').on('click', function (e) {
		closePreview();
	});

	$('#openExcelOptions').off('click').on('click', function (e) {
		openExcelOptions();
	});

	$('#closeExcelOptions').off('click').on('click', function (e) {
		closeExcelOptions();
	});

	$('#printExcel').off('click').on('click', function (e) {
		printExcel();
	});

	$('#doneForHtml').off('click').on('click', function (e) {
		closePreview();
	});

	/// ///////////////////////////PDF/////////////////////////////////////

	function openPrintOptions() {
		$('#stockSummaryPrintDialog').removeClass('hide');
		$('#stockSummaryPrintDialog').dialog({
			closeOnEscape: true,
			draggable: true,
			modal: true
		});
	}

	function closePrintOptions() {
		closeDialog($('#stockSummaryPrintDialog'));
	}

	window.openPDF = function () {
		var html = $('#htmlView').html();
		$('#loading').show(function () {
			pdfHandler.openPDF(html, true);
		});
	};

	window.printPDF = function () {
		var html = $('#htmlView').html();
		pdfHandler.print(html);
	};

	window.savePDF = function () {
		pdfHandler.savePDF({ type: PDFReportConstant.LOW_STOCK_SUMMARY_REPORT });
	};

	window.sharePDF = function () {
		$('#loading').show(function () {
			pdfHandler.sharePDF();
		});
	};

	function closeDialog($el) {
		if ($el.is(':visible') && $el.dialog('isOpen')) {
			$el.addClass('hide').dialog('close').dialog('destroy');
		}
	}

	function closePreview() {
		try {
			closeDialog($('#pdfViewDialog'));
		} catch (error) {}
	}

	function openPreview() {
		closeDialog($('#stockSummaryPrintDialog'));
		$('#loading').show(function () {
			var html = getHTMLTextForReport();
			if (html) {
				pdfHandler.generateHiddenBrowserWindowForPDF(html);
				transactionPDFHandler.openPreviewDialog(html, null, pdfHandler, closePreview);
			}
		});
	}

	/// ////////////////////////////////////////////////////////////////////

	function openExcelOptions() {
		$('#ExcelDialog').removeClass('hide');
		$('#ExcelDialog').dialog({
			closeOnEscape: true,
			draggable: true,
			modal: true
		});
	}

	function closeExcelOptions() {
		closeDialog($('#ExcelDialog'));
	}

	var downloadExcelFile = function downloadExcelFile() {
		var fileUtil = require('./../Utilities/FileUtil.js');
		var fileName = fileUtil.getFileName({ type: PDFReportConstant.LOW_STOCK_SUMMARY_REPORT });
		excelHelper.saveExcel(fileName);
	};

	var prepareObjectForExcel = function prepareObjectForExcel(itemListToBeShown) {
		var len = itemListToBeShown.length;
		var rowObj = [];
		var tableHeadArray = [];
		var totalArray = [];
		var ExcelMinStockQuantity = $('#excelMinStockQuantity').is(':checked');
		var ExcelStockQuantity = $('#excelStockQuantity').is(':checked');
		var ExcelStockValue = $('#excelStockValue').is(':checked');
		tableHeadArray.push('Item Name');

		if (ExcelMinStockQuantity) {
			tableHeadArray.push('Minimum Stock Quantity');
		}
		if (ExcelStockQuantity) {
			tableHeadArray.push('Stock Quantity');
		}

		if (ExcelStockValue) {
			tableHeadArray.push('Stock Value');
		}
		rowObj.push(tableHeadArray);
		rowObj.push([]);

		for (var j = 0; j < len; j++) {
			var itemObject = itemListToBeShown[j];
			var tempArray = [];
			tempArray.push(itemObject.getItemName());

			if (ExcelMinStockQuantity) {
				tempArray.push(MyDouble.getQuantityWithDecimalWithoutColor(itemObject.getItemMinStockQuantity()));
			}
			if (ExcelStockQuantity) {
				tempArray.push(MyDouble.getQuantityWithDecimalWithoutColor(itemObject.getItemStockQuantity()));
			}
			if (ExcelStockValue) {
				tempArray.push(MyDouble.getAmountWithDecimal(itemObject.getItemStockValue()));
			}

			rowObj.push(tempArray);
		}
		return rowObj;
	};

	function printExcel() {
		closeExcelOptions();
		var XLSX = require('xlsx');
		var dataArray = prepareObjectForExcel(itemListToBeShown);
		var worksheet = XLSX.utils.aoa_to_sheet(dataArray);

		var workbook = {
			'SheetNames': [],
			'Sheets': {}
		};

		var wsName = 'Low Stock Summary Report';
		workbook.SheetNames[0] = wsName;
		workbook.Sheets[wsName] = worksheet;
		var wscolWidth = [{ wch: 15 }, { wch: 30 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 30 }];
		worksheet['!cols'] = wscolWidth;
		console.log(workbook);
		XLSX.writeFile(workbook, appPath + '/report.xlsx');
		downloadExcelFile();
	}

	populateTable();
})(jQuery);