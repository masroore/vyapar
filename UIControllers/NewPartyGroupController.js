$('#contactName').focus();
var ErrorCode = require('../Constants/ErrorCode');
var NewPartyGroupLogic = require('./../BizLogic/PartyGroupLogic.js');
var newPartyGroupLogic = new NewPartyGroupLogic();
$('.terminalButtonGroup').off('click').on('click', function () {
	if ($(this).text() == 'Edit') {
		MyAnalytics.pushEvent('Edit Party Group Open');
		var oldGroupName = $('#oldGroupName').val();
		var newGroupName = $('#groupName').val();
		if (newGroupName) {
			if (oldGroupName.toLowerCase() == newGroupName.toLowerCase()) {
				ToastHelper.error('no changes made');
			} else {
				var statusCode = newPartyGroupLogic.editPartyGroup(oldGroupName, newGroupName);
				if (statusCode == ErrorCode.ERROR_PARTYGROUP_UPDATE_SUCCESS) {
					MyAnalytics.pushEvent('Edit Party Group Save');
					ToastHelper.success(statusCode);
				} else {
					ToastHelper.error(statusCode);
				}
				try {
					viewGroupPage();
					$('#newGroupDialog').dialog('destroy');
				} catch (err) {
					console.log(err);
				}
			}
		} else {
			ToastHelper.error('group name cannot be empty');
		}
	} else {
		MyAnalytics.pushEvent('Add Party Group Open');
		var name = $('#groupName').val();
		statusCode = newPartyGroupLogic.saveNewGroup(name);

		if (statusCode == ErrorCode.ERROR_PARTYGROUP_SAVE_SUCCESS && this.id == 'submit') {
			MyAnalytics.pushEvent('Add Party Group Save');
			$('#defaultPage').show();
			// if (!$('#modelContainer').css('display')=='none') {

			$('#modelContainer').css('display', 'none');
			// }
			$('.hideDefaultPage').css('display', 'block');
			try {
				viewGroupPage();
				$('#newGroupDialog').dialog('destroy');
			} catch (err) {
				console.log(err);
			}

			MyAnalytics.pushEvent('Party Group Save');
		}
		if (statusCode == ErrorCode.ERROR_PARTYGROUP_SAVE_SUCCESS) {
			ToastHelper.success(statusCode);
		} else {
			ToastHelper.error(statusCode);
		}
	}
});
MyAnalytics.pushScreen('New Party Group');