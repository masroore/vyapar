var startDate;
var endDate;
var itemCategoryId;
var DateFormat = require('./../Constants/DateFormat.js');
$('#startDate, #endDate').datepicker({ dateFormat: DateFormat.format });
var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();

var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var ItemCache = require('./../Cache/ItemCache.js');
var itemCache = new ItemCache();
var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');
var DataLoader = require('./../DBManager/DataLoader.js');
var dataLoader = new DataLoader();

var printForSharePDF = false;

var listOfTransactions = [];

var populateTable = function populateTable() {
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	itemCategoryId = Number($('#groupFilterOptions').val());
	startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
	endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');
	var showOnlyActiveItems = true;
	if ($('#showInactiveItems').is(':checked')) {
		showOnlyActiveItems = false;
	}
	CommonUtility.showLoader(function () {
		listOfTransactions = dataLoader.getStockDetailReportData(itemCategoryId, startDate, endDate, showOnlyActiveItems);
		if (listOfTransactions) {
			displayTransactionList(listOfTransactions);
		}
	});
};

if (itemCache.IsAnyItemInactive()) {
	$('#activeInactiveItems').removeClass('hide');
} else {
	$('#activeInactiveItems').addClass('hide');
}

var date = MyDate.getDate('d/m/y');
$('#startDate').val(MyDate.getFirstDateOfCurrentMonthString());
$('#endDate').val(date);

$('#startDate').change(function () {
	populateTable();
});

$('#endDate').change(function () {
	populateTable();
});

$('#hideInactiveDates').change(function () {
	populateTable();
});

$('#groupFilterOptions').on('selectmenuchange', function (event) {
	populateTable();
});

if (settingCache.isItemCategoryEnabled()) {
	var ItemCategoryCache = require('./../Cache/ItemCategoryCache.js');
	var itemCategoryCache = new ItemCategoryCache();
	var itemCategoryList = itemCategoryCache.getItemCategoriesList();

	var optionTemp = document.createElement('option');
	for (i in itemCategoryList) {
		var option = optionTemp.cloneNode();
		option.text = itemCategoryList[i].getCategoryName();
		option.value = i;
		$('#groupFilterOptions').append(option);
	}

	$('#itemCategoryFilterForStockDetail').show();
} else {
	$('#itemCategoryFilterForStockDetail').hide();
}

var stockDetailReportClusterize;

$('#showInactiveItems').on('change', function () {
	populateTable();
});

var displayTransactionList = function displayTransactionList(listOfTransactions) {
	var len = listOfTransactions.length;
	var rowData = [];
	var dynamicRow = '';
	dynamicRow += "<thead><tr><th width='20%'>Item Name</th><th width='20%' class='tableCellTextAlignRight'>Begining Quantity</th><th width='20%' class='tableCellTextAlignRight'>Quantity In</th><th width='20%' class='tableCellTextAlignRight'>Quantity Out</th><th width='20%' class='tableCellTextAlignRight'>Closing Quantity</th></tr></thead>";
	//
	var totalSaleQuantity = 0;
	var totalPurchaseQuantity = 0;

	var ItemCache = require('./../Cache/ItemCache.js');
	var itemCache = new ItemCache();

	for (var _i = 0; _i < len; _i++) {
		var row = '';
		var txn = listOfTransactions[_i];
		var itemName = txn.getItemName();
		var itemObj = itemCache.findItemByName(itemName);
		var isItemService = itemObj.isItemService();
		var openingQuantity = MyDouble.getQuantityWithDecimalWithoutColor(txn.getOpeningQuantity());
		var quantityIn = txn.getQuantityIn();
		var quantityOut = txn.getQuantityOut();
		var closingQuantity = MyDouble.getQuantityWithDecimalWithoutColor(openingQuantity + quantityIn - quantityOut);

		if (isItemService) {
			openingQuantity = '';
			closingQuantity = '';
		}

		row += "<tr class='currentRow'><td width='20%'>" + itemName + "</td><td width='20%' class='tableCellTextAlignRight'>" + openingQuantity + "</td><td width='20%' class='tableCellTextAlignRight' >" + MyDouble.getQuantityWithDecimalWithoutColor(quantityIn) + "</td><td width='20%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(quantityOut) + "</td><td width='20%' class='tableCellTextAlignRight'>" + closingQuantity + '</td></tr>';
		rowData.push(row);
	}
	var data = rowData;

	if ($('#stockDetailReportContainer').length === 0) {
		return;
	}
	if (stockDetailReportClusterize && stockDetailReportClusterize.destroy) {
		stockDetailReportClusterize.clear();
		stockDetailReportClusterize.destroy();
	}

	stockDetailReportClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollArea',
		contentId: 'contentArea',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});

	$('#tableHead').html(dynamicRow);
};

var openTransaction = function openTransaction(txnId, txnType) {
	var ViewTransaction = require('./../BizLogic/ViewTransaction.js');
	var viewTransaction = new ViewTransaction();
	viewTransaction.viewTransactionFile(txnId + ':' + TxnTypeConstant.getTxnType(txnType));
};

if (settingCache.getItemEnabled()) {
	$('#stockDetailPrintDiv').show();
} else {
	$('#stockDetailPrintDiv').hide();
}

function openPrintOptions(sharePDF) {
	printForSharePDF = sharePDF;
	printReport();
}

function closePrintOptions() {
	$('#customPrintDialog').addClass('hide');
	$('#customPrintDialog').dialog('close').dialog('destroy');
}

function printReport() {
	if (printForSharePDF) {
		pdfHandler.sharePdf(getHTMLTextForReport(), 'Report', false);
	} else {
		pdfHandler.printPdf(getHTMLTextForReport(), 'Report', false);
	}
	// closePrintOptions();
}

var getHTMLTextForReport = function getHTMLTextForReport() {
	var StockDetailReportHTMLGenerator = require('./../ReportHTMLGenerator/StockDetailReportHTMLGenerator .js');
	var stockDetailReportHTMLGenerator = new StockDetailReportHTMLGenerator();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var htmlText = stockDetailReportHTMLGenerator.getHTMLText(listOfTransactions, startDateString, endDateString, itemCategoryId);
	return htmlText;
};

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	// $('#frameDiv').load('');
	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable();
};

/// ///////////////////////////PDF/////////////////////////////////////
var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.STOCK_DETAIL_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

if (settingCache.getItemEnabled()) {
	$('#stockDetailsExcel').show();
} else {
	$('#stockDetailsExcel').hide();
}

function openExcelOptions() {
	$('#ExcelDialog').removeClass('hide');
	$('#ExcelDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closeExcelOptions() {
	$('#ExcelDialog').addClass('hide');
	$('#ExcelDialog').dialog('close').dialog('destroy');
}

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.STOCK_DETAIL_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
	excelHelper.saveExcel(fileName);
};

var prepareObjectForExcel = function prepareObjectForExcel(listOfTransactions) {
	var len = listOfTransactions.length;
	var rowObj = [];
	var tableHeadArray = [];
	var totalArray = [];
	tableHeadArray.push('Item Name');
	tableHeadArray.push('Begining Quantity');
	tableHeadArray.push('Quantity In');
	tableHeadArray.push('Quantity Out');
	tableHeadArray.push('Closing Quantity');
	rowObj.push(tableHeadArray);
	rowObj.push([]);

	var ItemCache = require('./../Cache/ItemCache.js');
	var itemCache = new ItemCache();

	for (var i = 0; i < len; i++) {
		var tempArray = [];
		var txn = listOfTransactions[i];
		var itemName = txn.getItemName();
		var itemObj = itemCache.findItemByName(itemName);
		var isItemService = itemObj.isItemService();
		var openingQuantity = MyDouble.getQuantityWithDecimalWithoutColor(txn.getOpeningQuantity());
		var quantityIn = txn.getQuantityIn();
		var quantityOut = txn.getQuantityOut();
		var closingQuantity = MyDouble.getQuantityWithDecimalWithoutColor(openingQuantity + quantityIn - quantityOut);

		if (isItemService) {
			openingQuantity = '';
			closingQuantity = '';
		}
		tempArray.push(itemName);
		tempArray.push(openingQuantity);
		tempArray.push(MyDouble.getQuantityWithDecimalWithoutColor(quantityIn));
		tempArray.push(MyDouble.getQuantityWithDecimalWithoutColor(quantityOut));
		tempArray.push(closingQuantity);

		rowObj.push(tempArray);
	}
	return rowObj;
};

function printExcel() {
	var XLSX = require('xlsx');
	var dataArray = prepareObjectForExcel(listOfTransactions);
	var worksheet = XLSX.utils.aoa_to_sheet(dataArray);

	var workbook = {
		'SheetNames': [],
		'Sheets': {}
	};

	var wsName = 'Stock Detail Report';
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet;
	var wscolWidth = [{ wch: 40 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	worksheet['!cols'] = wscolWidth;
	console.log(workbook);
	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

populateTable();