var SettingUtility = function () {
	function setCustomFields() {
		var CustomFieldsCache = require('./../Cache/CustomFieldsCache.js');
		var customFieldsCache = new CustomFieldsCache();
		var listOfCustomFields = customFieldsCache.getListOfDeliveryCustomFields();
		var i = 1;
		$.each(listOfCustomFields, function (key, value) {
			$('#transportCustomField' + i).val(value.getCustomFieldName());
			$('#checkTransportCustomField' + i).prop('checked', true);
			if (!value.getCustomFieldVisibility()) {
				$('#transportCustomField' + i).prop('disabled', true);
				$('#checkTransportCustomField' + i).prop('checked', false);
			}
			i++;
		});
		$('#customFieldDialog').removeClass('hide').dialog();
		bindTransportationDialogEvents();
	}

	function editTransportCustomFields(event) {
		var targetElement = event.target;
		var transportationFieldId = $(targetElement).data('fieldId');
		if ($(targetElement).prop('checked')) {
			$('#' + transportationFieldId).prop('disabled', false);
		} else {
			$('#' + transportationFieldId).prop('disabled', true);
		}
	}

	function submitCustomFields() {
		var CustomFieldsModel = require('./../Models/CustomFieldsModel.js');
		var TransactionManager = require('./../DBManager/TransactionManager.js');
		var transactionManager = new TransactionManager();

		var statusCode = ErrorCode.ERROR_SETTING_SAVE_FAILED;
		var isTransactionBeginSuccess = transactionManager.BeginTransaction();
		var errorInTransportationField = false;
		if (isTransactionBeginSuccess) {
			var customInputs = $('#customFieldDialog input:checked');
			for (i = 1; i <= 6; i++) {
				if ($('#checkTransportCustomField' + i).prop('checked')) {
					var visibility = 1;
				} else {
					var visibility = 0;
				}
				var id = $('#transportCustomField' + i).data('fieldid');
				var fieldVal = $('#transportCustomField' + i).val();
				fieldVal = fieldVal || '';
				if (!fieldVal) {
					errorInTransportationField = visibility ? true : false;
					break;
				}
				var customFieldsModel = new CustomFieldsModel();
				customFieldsModel.setCustomFieldId(id);
				customFieldsModel.setCustomFieldVisibility(visibility);
				customFieldsModel.setCustomFieldName(fieldVal);
				statusCode = customFieldsModel.updateCustomField();
				if (statusCode != ErrorCode.ERROR_SETTING_SAVE_SUCCESS) {
					break;
				}
			}

			if (statusCode == ErrorCode.ERROR_SETTING_SAVE_SUCCESS) {
				MyAnalytics.pushEvent('Settings Transportation Details Save');
				if (!transactionManager.CommitTransaction()) {
					statusCode = ErrorCode.ERROR_SETTING_SAVE_FAILED;
				}
			}
			transactionManager.EndTransaction();
		}

		if (!errorInTransportationField && statusCode == ErrorCode.ERROR_SETTING_SAVE_SUCCESS) {
			var CustomFieldsCache = require('./../Cache/CustomFieldsCache.js');
			var customFieldsCache = new CustomFieldsCache();
			customFieldsCache.refreshCustomFields();
			ToastHelper.success(statusCode);
		} else if (errorInTransportationField) {
			ToastHelper.error('Transportation fields cannot be saved empty. Please enter some value.');
		} else {
			ToastHelper.error(statusCode);
		}

		if (!errorInTransportationField) {
			$('#customFieldDialog').addClass('hide').dialog('destroy');
		}
	}

	function bindTransportationDialogEvents() {
		$('#customHeaderDialog').off('keyup').on('keyup', function (e) {
			if (e.keyCode == '13') {
				submitCustomHeaders();
			}
		});

		$('#doneForCustomFields').off('click').on('click', submitCustomFields);
		$('#customFieldDialog input[type=checkbox]').off('change').on('change', editTransportCustomFields);
	}

	function setDefaultThermalPrinter() {
		var printer = require('printer');
		var printerList = printer.getPrinters();
		$('<div id="thermalPrinterChooserForPrinting"></div>').dialog({
			modal: true,
			closeOnEscape: true,
			title: 'Choose Default Thermal Printer',
			width: 400,
			maxHeight: 600,
			open: function open() {
				var currentDialog = $(this);
				var setPrinterAsDefault = function setPrinterAsDefault(evt) {
					var printerName = this.textContent;
					if (printerName) {
						var settingsModel = new SettingsModel();
						settingsModel.setSettingKey(Queries.SETTING_DEFAULT_THERMAL_PRINTER_ADDRESS);
						var statusCode = settingsModel.UpdateSetting(printerName, { nonSyncableSetting: true });
						if (statusCode == ErrorCode.ERROR_SETTING_SAVE_SUCCESS) {
							$('#currentDefaultPrinter').html(printerName);
							$('#defaultThermalPrinterNameDiv').show();
							ToastHelper.success(statusCode);
						} else {
							ToastHelper.error(statusCode);
						}
						currentDialog.dialog('close');
					}
				};
				var printerListElement = document.createElement('ul');
				printerListElement.classList.add('printerList');
				var optionTemp = document.createElement('li');
				optionTemp.classList.add('printerDetail');
				for (var i in printerList) {
					var printerObject = printerList[i];
					var option = optionTemp.cloneNode();
					option.onclick = setPrinterAsDefault;
					option.innerHTML = "<span><img src='./../inlineSVG/print_center.svg' style='width: 30px;' class='printerListName'><span class='printerListName'>" + printerObject.name + '</span></span>';
					printerListElement.append(option);
				}
				$(this).append(printerListElement);
			},
			close: function close() {
				$('#thermalPrinterChooserForPrinting').remove();
			},
			buttons: {
				Cancel: function Cancel() {
					$(this).dialog('close');
				}
			}
		});
	}

	function toggleAutoLaunch(turnOnAutoLaunch) {
		var _require = require('electron'),
		    ipcRenderer = _require.ipcRenderer;

		if (turnOnAutoLaunch) {
			localStorage.setItem('autoLaunch', 1);
			ipcRenderer.send('enableDisableAutoLaunch', 1);
		} else {
			localStorage.setItem('autoLaunch', 0);
			ipcRenderer.send('enableDisableAutoLaunch', 0);
		}
	}

	return {
		setCustomFields: setCustomFields,
		editTransportCustomFields: editTransportCustomFields,
		submitCustomFields: submitCustomFields,
		bindTransportationDialogEvents: bindTransportationDialogEvents,
		setDefaultThermalPrinter: setDefaultThermalPrinter,
		toggleAutoLaunch: toggleAutoLaunch
	};
}();

module.exports = SettingUtility;