var PaymentCache = require('./../Cache/PaymentInfoCache.js');
var paymentcache = new PaymentCache();
var listOfAccountName = paymentcache.getBankListObj();
var optionTemp = document.createElement('option');
var accountName = document.getElementById('accountName');
var len = listOfAccountName.length;

//
$.each(listOfAccountName, function (key, value) {
	var bank = value.getName();
	var id = value.getId();
	var option = optionTemp.cloneNode();
	option.text = bank;
	option.id = id;
	option.value = bank;
	accountName.add(option);
});

// $('#accountName').html(option);
//
$('#accountName').val(accName);
// $('#accountName').html('<option>'+accName + '</option>');
var ErrorCode = require('./../Constants/ErrorCode.js');

$('.terminalButtonAccountAdjustment').on('click', createAdjustment);

function createAdjustment() {
	var statusCode = ErrorCode.SUCCESS;
	// var accountName = document.getElementById('accountName').value;
	var accountName = $('#accountName').find(':selected').text();
	var entryType = document.getElementById('entryType').value.toUpperCase();
	// debugger
	var description = document.getElementById('description').value.trim();
	var date = document.getElementById('datepicker').value;
	date = date + MyDate.getDate(' H:M:S');
	date = MyDate.getDateObj(date, 'dd/mm/yyyy', '/', true);
	var amount = document.getElementById('amount').value.trim();
	if (!amount) {
		statusCode = ErrorCode.ERROR_BANK_ADJ_AMOUNT_EMPTY;
		ToastHelper.error(ErrorCode.ERROR_BANK_ADJ_AMOUNT_EMPTY);
		return;
	}
	var BankAdjustmentTxn = require('./../BizLogic/BankAdjustmentTxn.js');
	var bankadjustmenttxn = new BankAdjustmentTxn();
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	console.log(entryType);
	console.log(StringConstant.bankAddAdjustmentString.toUpperCase());
	switch (entryType) {
		case StringConstant.bankAddAdjustmentString.toUpperCase():
			bankadjustmenttxn.setAdjType(TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD);
			break;
		case StringConstant.bankReduceAdjustmentString.toUpperCase():
			bankadjustmenttxn.setAdjType(TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE);
			break;
		case StringConstant.bankAddAdjustmentWithoutCashString.toUpperCase():
			bankadjustmenttxn.setAdjType(TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD_WITHOUT_CASH);
			break;
		case StringConstant.bankReduceAdjustmentWithoutCashString.toUpperCase():
			bankadjustmenttxn.setAdjType(TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE_WITHOUT_CASH);
			break;
	}
	var id = paymentcache.getPaymentBankId(accountName);
	//
	// var id = $('#accountName').find(':selected').attr('id');
	bankadjustmenttxn.setAdjBankId(id);
	bankadjustmenttxn.setAdjDescription(description);
	bankadjustmenttxn.setAdjDate(date);
	bankadjustmenttxn.setAdjAmount(amount);
	if (accId) {
		bankadjustmenttxn.setAdjId(accId);
		var statusCode = bankadjustmenttxn.updateAdjustment();
	} else {
		var statusCode = bankadjustmenttxn.createAdjustment();
	}

	if (statusCode != ErrorCode.ERROR_NEW_BANK_ADJUSTMENT_FAILED && statusCode != ErrorCode.ERROR_UPDATE_BANK_ADJUSTMENT_FAILED) {
		// if (!$('#modelContainer').css('display')=='none') {
		ToastHelper.success(statusCode);
		$('#defaultPage').show();
		$('#modelContainer').css('display', 'none');
		// }
		$('.viewItems').css('display', 'block');
		onResume();
	} else {
		ToastHelper.error(statusCode);
		MyAnalytics.pushEvent('Bank Adjustment Save');
	}
}