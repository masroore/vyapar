/**
 * Created by Ashish on 3/2/2018.
 */
var receivePayment = function receivePayment(that) {
	var id = that;
	id = id.split(':');
	var txnIdReq = id[0];
	var value = 'TXN_TYPE_CASHIN';

	var MountComponent = require('../UIComponent/jsx/MountComponent').default;
	var Component = require('../UIComponent/jsx/SalePurchaseContainer/PaymentTransaction').default;
	var TxnTypeConstant = require('../Constants/TxnTypeConstant');
	MountComponent(Component, document.querySelector('#salePurchaseContainer'), {
		txnType: TxnTypeConstant[value],
		txnId: txnIdReq
	});
};

if (module && module.exports) {
	module.exports = receivePayment;
}