var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var startDate;
var endDate;
var DateFormat = require('./../Constants/DateFormat.js');
var NameCache = require('./../Cache/NameCache.js');
var nameCache = new NameCache();
var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();
var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var GSTR2ReportHelper = require('./../UIControllers/GSTR2ReportHelper.js');
var gSTR2ReportHelper = new GSTR2ReportHelper();
var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var ITCTypeConstantsGSTR2 = require('./../Constants/ITCTypeConstantsGSTR2.js');
var TaxCodeCache = require('./../Cache/TaxCodeCache.js');
var taxCodeCache = new TaxCodeCache();
var TaxCodeConstants = require('./../Constants/TaxCodeConstants.js');
var MyString = require('./../Utilities/MyString.js');
var NameCustomerType = require('./../Constants/NameCustomerType.js');
var GSTRReportHelper = require('./../UIControllers/GSTRReportHelper.js');
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');
var FirmCache = require('./../Cache/FirmCache.js');
var firmCache = new FirmCache();

var printForSharePDF = false;

var listOfTransactions = [];

var GSTR_2_data = {
	showOTHER: false,
	showAdditionalCess: false,
	invoiceNoMap: new _map2.default(),
	totalTaxableValue: 0,
	totalInvoiceValue: 0,
	totalIGSTAmount: 0,
	totalCGSTAmount: 0,
	totalSGSTAmount: 0,
	totalCESSAmount: 0,
	totalOTHERAmount: 0,
	totalAdditionalCessAmount: 0
};

$('.monthYearPicker').datepicker({
	changeMonth: true,
	changeYear: true,
	showButtonPanel: true,
	dateFormat: 'mm/yy'
}).focus(function () {
	var thisCalendar = $(this);
	$('.ui-datepicker-calendar').detach();
	$('.ui-datepicker-close').click(function () {
		var month = $('#ui-datepicker-div .ui-datepicker-month :selected').val();
		var year = $('#ui-datepicker-div .ui-datepicker-year :selected').val();
		thisCalendar.datepicker('setDate', new Date(year, month));
		populateTable();
	});
});

var populateTable = function populateTable() {
	nameCache = new NameCache();
	settingCache = new SettingCache();
	taxCodeCache = new TaxCodeCache();
	GSTR_2_data.showOTHER = false;
	GSTR_2_data.showAdditionalCess = false;
	GSTR_2_data.invoiceNoMap = new _map2.default();
	GSTR_2_data.totalTaxableValue = 0;
	GSTR_2_data.totalInvoiceValue = 0;
	GSTR_2_data.totalIGSTAmount = 0;
	GSTR_2_data.totalCGSTAmount = 0;
	GSTR_2_data.totalSGSTAmount = 0;
	GSTR_2_data.totalCESSAmount = 0;
	GSTR_2_data.totalOTHERAmount = 0;
	GSTR_2_data.totalAdditionalCessAmount = 0;

	var startDateString = $('#startDate').val();
	startDateString = '01/' + startDateString;
	var endDateString = $('#endDate').val();
	endDateString = '01/' + endDateString;
	startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
	endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');
	endDate = new Date(endDate.getFullYear(), endDate.getMonth() + 1, 0);
	var firmId = Number($('#firmFilterOptions').val());
	var partyFilterId = Number($('#transactionFilterOptions').val());
	var considerNonTaxAsExemptedCheck = $('#considerNonTaxAsExempted').prop('checked');
	CommonUtility.showLoader(function () {
		listOfTransactions = gSTR2ReportHelper.getTxnListBasedOnDate(startDate, endDate, firmId, considerNonTaxAsExemptedCheck);
		GSTR_2_data.showOTHER = gSTR2ReportHelper.showOTHER;
		GSTR_2_data.showAdditionalCess = gSTR2ReportHelper.showAdditionalCess;
		displayTransactionList(listOfTransactions);
	});
};

var date = MyDate.getDate('m/y');
$('#startDate').val(date);
$('#endDate').val(date);

if (settingCache.getMultipleFirmEnabled()) {
	var optionTemp = document.createElement('option');
	var firmList = firmCache.getFirmList();
	for (i in firmList) {
		var option = optionTemp.cloneNode();
		option.text = firmList[i].getFirmName();
		option.value = firmList[i].getFirmId();
		$('#firmFilterOptions').append(option);
	}
	$('#firmChooser').show();
} else {
	$('#firmChooser').hide();
}

$('#firmFilterOptions').on('selectmenuchange', function (event) {
	populateTable();
});

$('#startDate').change(function () {
	populateTable();
});

$('#endDate').change(function () {
	populateTable();
});

$('#considerNonTaxAsExempted').change(function () {
	populateTable();
});

var gstr2ReportClusterize;

var displayTransactionList = function displayTransactionList(listOfTransactions) {
	var len = listOfTransactions.length;
	var rowData = [];
	var dynamicRow = '';
	var amtcol = 6;
	if (!GSTR_2_data.showOTHER) {
		amtcol--;
	}
	if (!settingCache.getAdditionalCessEnabled()) {
		amtcol--;
	}

	var className = '';
	if (!$('#sortIcon').attr('class')) {
		className = 'ui-selectmenu-icon ui-icon ui-icon-triangle-1-s';
	} else {
		className = $('#sortIcon').attr('class');
	}
	//
	dynamicRow += "<thead><tr style='background: #F7F7F7;'><th style='width: 150px;'></th><th colspan='4' style='width:600px; text-align: center;'>Bill details</th><th class='tableCellTextAlignRight' style='width: 150px;'>Rate</th><th class='tableCellTextAlignRight' style='width: 150px;'>Cess Rate</th><th class='tableCellTextAlignRight' style='width: 150px;'>Taxable value</th><th align='center' style='width: 150px;' class='tableCellTextAlignRight'>Reverse Charge</th><th align='center' style='width:" + amtcol * 150 + "px; text-align: center;' colspan='" + amtcol + "'>Amount</th><th class='tableCellTextAlignRight' style='width: 150px;'>Place of Supply(Name of State)</th>";

	var invisibleRow = "<td style='width: 150px;'></td><td colspan='4' style='width:600px; text-align: center;'></td><td class='tableCellTextAlignRight' style='width: 150px;'></td><td class='tableCellTextAlignRight' style='width: 150px;'></td><td class='tableCellTextAlignRight' style='width: 150px;'></td><td class='tableCellTextAlignRight' style='width:150px;'></td><td align='center' style='width:" + amtcol * 150 + "px; text-align: center;' colspan='" + amtcol + "'></td><td class='tableCellTextAlignRight' style='width: 150px;'></td>";

	dynamicRow += '</tr><tr width=\'\'><th align=\'left\' style=\'width:150px;\'>GSTIN/UIN</th><th align=\'left\' style=\'width:150px;\'>Party Name</th><th style=\'width:150px;\' class=\'tableCellTextAlignRight\' id=\'invoiceColId\' style=\'cursor:pointer;\'>No.<span id=\'sortIcon\' class=\'' + className + '\'></span></th><th class=\'tableCellTextAlignRight\' style=\'width:150px;\'>Date</th><th class=\'tableCellTextAlignRight\' style=\'width:150px;\'>Value</th><th class=\'tableCellTextAlignRight\' style=\'width:150px;\'></th><th class=\'tableCellTextAlignRight\' style=\'width:150px;\'></th><th class=\'tableCellTextAlignRight\' style=\'width:150px;\'></th><th class=\'tableCellTextAlignRight\' style=\'width:150px;\'></th><th class=\'tableCellTextAlignRight\' style=\'width:150px;\'>Integrated Tax</th><th class=\'tableCellTextAlignRight\' style=\'width:150px;\'>Central Tax</th><th class=\'tableCellTextAlignRight\' style=\'width:150px;\'>State/UT Tax</th><th class=\'tableCellTextAlignRight\' style=\'width:150px;\'>Cess</th>';

	if (GSTR_2_data.showOTHER) {
		dynamicRow += "<th class='tableCellTextAlignRight' style='width:150px;'>Other</th>";
	}
	if (settingCache.getAdditionalCessEnabled()) {
		dynamicRow += "<th class='tableCellTextAlignRight' style='width:150px;'>Additional Cess</th>";
	}
	dynamicRow += "<th class='tableCellTextAlignRight' style='width:150px;'></th></tr></thead>";
	//

	for (var _i = 0; _i < len; _i++) {
		var row = '';
		var txn = listOfTransactions[_i];
		var GstinNo = txn.getGstinNo();
		var invoiceDate = MyDate.getDate('d/m/y', txn.getInvoiceDate());
		var invoiceValue = txn.getInvoiceValue();
		var cessRate = txn.getCessRate();
		var invoiceRate = txn.getRate() - cessRate;
		var invoceTaxableValue = txn.getInvoiceTaxableValue();
		var igstAmt = txn.getIGSTAmt();
		var sgstAmt = txn.getSGSTAmt();
		var cgstAmt = txn.getCGSTAmt();
		var cessAmt = txn.getCESSAmt();
		var otherAmt = txn.getOTHERAmt();
		var POS = txn.getPlaceOfSupply();
		var transactionId = txn.getTransactionId();
		var transactionType = txn.getTransactionType();
		var incorrectEntry = txn.getEntryIncorrect();
		var additionalCess = txn.getAdditionalCessAmt();
		var partyName = txn.getPartyName();
		var refNo = txn.getTxnRefNumber() === '' ? '' : txn.getInvoiceNo();
		var reverseCharge = txn.getReverseCharge();
		var reverseChargeString = reverseCharge ? 'Y' : 'N';

		if (transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE) {
			if (!GSTR_2_data.invoiceNoMap.has(transactionId)) {
				GSTR_2_data.invoiceNoMap.set(transactionId, invoiceValue);
				GSTR_2_data.totalInvoiceValue += invoiceValue;
			}
			GSTR_2_data.totalTaxableValue += invoceTaxableValue;
			GSTR_2_data.totalIGSTAmount += igstAmt;
			GSTR_2_data.totalCGSTAmount += cgstAmt;
			GSTR_2_data.totalSGSTAmount += sgstAmt;
			GSTR_2_data.totalCESSAmount += cessAmt;
			GSTR_2_data.totalOTHERAmount += otherAmt;
			GSTR_2_data.totalAdditionalCessAmount += additionalCess;
		} else if (transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
			if (!GSTR_2_data.invoiceNoMap.has(transactionId)) {
				GSTR_2_data.invoiceNoMap.set(transactionId, invoiceValue);
				GSTR_2_data.totalInvoiceValue -= invoiceValue;
			}
			GSTR_2_data.totalTaxableValue -= invoceTaxableValue;
			GSTR_2_data.totalIGSTAmount -= igstAmt;
			GSTR_2_data.totalCGSTAmount -= cgstAmt;
			GSTR_2_data.totalSGSTAmount -= sgstAmt;
			GSTR_2_data.totalCESSAmount -= cessAmt;
			GSTR_2_data.totalOTHERAmount -= otherAmt;
			GSTR_2_data.totalAdditionalCessAmount -= additionalCess;
		}

		row += "<tr ondblclick='openTransaction(" + transactionId + ',' + transactionType + ")' class='currentRow'";
		if (incorrectEntry) {
			row += "style='background-color: #e87475;' title='Incorrect Transaction'";
		}
		row += "><td align='left' style='width: 150px;";
		var regex = new RegExp(StringConstant.GSTINregex);
		if (!(regex.test(GstinNo) || GstinNo.trim() == '')) {
			row += 'background-color: #FAF197;';
		}
		row += "'";
		if (!(regex.test(GstinNo) || GstinNo.trim() == '')) {
			row += "title='Invalid GSTIN No.'";
		}
		row += '>' + GstinNo + "</td><td style='width: 150px;'>" + partyName + "</td><td class='tableCellTextAlignRight' style='width: 150px;'>" + refNo + "</td><td class='tableCellTextAlignRight' style='width: 150px;'>" + invoiceDate + "</td><td class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getAmountWithDecimal(invoiceValue) + "</td><td class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getPercentageWithDecimal(invoiceRate) + "</td><td class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getPercentageWithDecimal(cessRate) + "</td><td class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getAmountWithDecimal(invoceTaxableValue) + "</td><td class='tableCellTextAlignRight' style='width: 150px;'>" + reverseChargeString + "</td><td class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getAmountWithDecimal(igstAmt) + "</td><td class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getAmountWithDecimal(cgstAmt) + "</td><td class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getAmountWithDecimal(sgstAmt) + '</td>';

		row += "<td class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getAmountWithDecimal(cessAmt) + '</td>';
		if (GSTR_2_data.showOTHER) {
			row += "<td class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getAmountWithDecimal(otherAmt) + '</td>';
		}
		if (settingCache.getAdditionalCessEnabled()) {
			row += "<td class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getAmountWithDecimal(additionalCess) + '</td>';
		}
		row += "<td class='tableCellTextAlignRight' style='width: 150px;'>" + POS + '</td>';

		row += '</tr>';
		rowData.push(row);
	}
	if (len > 0) {
		rowData.push('<tr></tr>');
		var totalRow = "<thead><tr width=''><th align='left' style='width:150px;'>Total</th><th style='width:150px;'></th><th style='width:150px;'></th><th style='width:150px;'></th><th class='tableCellTextAlignRight' style='width:150px;'>" + MyDouble.getAmountWithDecimal(GSTR_2_data.totalInvoiceValue) + "</th><th style='width:150px;'></th><th class='tableCellTextAlignRight' style='width:150px;'></th><th class='tableCellTextAlignRight' style='width:150px;'>" + MyDouble.getAmountWithDecimal(GSTR_2_data.totalTaxableValue) + "</th><th class='tableCellTextAlignRight' style='width: 150px;'></th><th class='tableCellTextAlignRight' style='width:150px;'>" + MyDouble.getAmountWithDecimal(GSTR_2_data.totalIGSTAmount) + "</th><th class='tableCellTextAlignRight' style='width:150px;'>" + MyDouble.getAmountWithDecimal(GSTR_2_data.totalCGSTAmount) + "</th><th class='tableCellTextAlignRight' style='width:150px;'>" + MyDouble.getAmountWithDecimal(GSTR_2_data.totalSGSTAmount) + "</th><th class='tableCellTextAlignRight' style='width:150px;'>" + MyDouble.getAmountWithDecimal(GSTR_2_data.totalCESSAmount) + '</th>';

		if (GSTR_2_data.showOTHER) {
			totalRow += "<th class='tableCellTextAlignRight' style='width:150px;'>" + MyDouble.getAmountWithDecimal(GSTR_2_data.totalOTHERAmount) + '</th>';
		}
		if (settingCache.getAdditionalCessEnabled()) {
			totalRow += "<th class='tableCellTextAlignRight' style='width:150px;'>" + MyDouble.getAmountWithDecimal(GSTR_2_data.totalAdditionalCessAmount) + '</th>';
		}
		totalRow += "<th class='tableCellTextAlignRight' style='width:150px;'></th></tr></thead>";
		rowData.push(totalRow);
	}

	var data = rowData;
	if ($('#gstr2ReportContainer').length === 0) {
		return;
	}
	if (gstr2ReportClusterize && gstr2ReportClusterize.destroy) {
		gstr2ReportClusterize.clear();
		gstr2ReportClusterize.destroy();
	}

	gstr2ReportClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollArea',
		contentId: 'contentArea',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});

	$('#invisibleRow').html(invisibleRow);

	$('#tableHead').html(dynamicRow);
};

$('#scrollArea').on('scroll', function () {
	$('#tableHeader').scrollLeft($(this).scrollLeft());
});

$('#tableHead').on('click', '#invoiceColId', function (e) {
	var SortHelper = require('./../Utilities/SortHelper.js');
	var sortHelper = new SortHelper();
	listOfTransactions = sortHelper.getSortedListByInvoice(listOfTransactions, 'txnRefNumber');
	displayTransactionList(listOfTransactions);
});

var openTransaction = function openTransaction(txnId, txnType) {
	var ViewTransaction = require('./../BizLogic/ViewTransaction.js');
	var viewTransaction = new ViewTransaction();
	viewTransaction.viewTransactionFile(txnId + ':' + TxnTypeConstant.getTxnType(txnType));
};

var getHTMLTextForReport = function getHTMLTextForReport() {
	var GSTR2ReportHTMLGenerator = require('./../ReportHTMLGenerator/GSTR2ReportHTMLGenerator.js');
	var gSTR2ReportHTMLGenerator = new GSTR2ReportHTMLGenerator();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var firmId = Number($('#firmFilterOptions').val());
	var htmlText = gSTR2ReportHTMLGenerator.getHTMLText(listOfTransactions, startDateString, endDateString, firmId, GSTR_2_data.showOTHER, GSTR_2_data.showAdditionalCess);
	return htmlText;
};

$('#transactionFilterOptions').on('selectmenuchange', function () {
	populateTable();
});

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	if (notFromAutoSyncFlow) {
		// $('#frameDiv').load('');
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable();
};

/// ///////////////////////////PDF/////////////////////////////////////
var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.GSTR2_REPORT, fromDate: '01/' + $('#startDate').val(), toDate: '01/' + $('#endDate').val(), format: 'MM_YY' });
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.GSTR2_REPORT, fromDate: '01/' + $('#startDate').val(), toDate: '01/' + $('#endDate').val(), format: 'MM_YY' });
	excelHelper.saveExcel(fileName);
};

var prepareObjectForExcel = function prepareObjectForExcel(listOfTransactions) {
	var monthArray = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	var firmId = Number($('#firmFilterOptions').val());
	var fromDate = $('#startDate').val();
	var toDate = $('#endDate').val();
	fromDate = '01/' + fromDate;
	toDate = '01/' + toDate;
	var startDate = MyDate.getDateObj(fromDate, 'dd/MM/yyyy', '/');
	var startmonthIndex = startDate.getMonth();
	var startMonthName = monthArray[startmonthIndex];
	var startmonthYear = startDate.getFullYear();

	var endDate = MyDate.getDateObj(toDate, 'dd/MM/yyyy', '/');
	var endmonthIndex = endDate.getMonth();
	var endMonthName = monthArray[endmonthIndex];
	var endmonthYear = endDate.getFullYear();

	if (firmId == 0) {
		firmId = settingCache.getDefaultFirmId();
	}
	var firm = firmCache.getFirmById(firmId);
	var firmName = firm.getFirmName();
	var gstin = firmCache.getFirmGSTINByFirmName(firmName);

	var len = listOfTransactions.length;
	var rowObj = [];
	rowObj.push(['Period', startMonthName + ' ' + startmonthYear + ' - ' + endMonthName + ' ' + endmonthYear]);
	rowObj.push([]);
	rowObj.push(['1. GSTIN', gstin]);
	rowObj.push(['2.a Legal name of the registered person.', firmName]);
	rowObj.push(['2.b Trade name, if any']);
	rowObj.push(['3.a Aggregate turnover of the preceeding Financial Year']);
	rowObj.push(['3.b Aggregate turnover, April to June 2017']);
	rowObj.push([]);
	var totalArray = [];
	var tableHeadArray = [];
	tableHeadArray.push('GSTIN/UIN');
	tableHeadArray.push('Party Name');
	tableHeadArray.push('Transaction Type');
	tableHeadArray.push('Invoice No.');
	tableHeadArray.push('Invoice Date');
	tableHeadArray.push('Invoice Value');
	tableHeadArray.push('Rate');
	tableHeadArray.push('Cess Rate');
	tableHeadArray.push('Taxable value');
	tableHeadArray.push('Integrated Tax Amount');
	tableHeadArray.push('Central Tax Amount');
	tableHeadArray.push('State/UT Tax Amount');
	tableHeadArray.push('Cess Amount');
	if (GSTR_2_data.showOTHER) {
		tableHeadArray.push('Other Tax Amount');
	}
	if (settingCache.getAdditionalCessEnabled()) {
		tableHeadArray.push('Additional Cess Amount');
	}
	tableHeadArray.push('Place of Supply(Name of state)');
	rowObj.push(tableHeadArray);
	rowObj.push([]);

	for (var _i2 = 0; _i2 < len; _i2++) {
		var tempArray = [];
		var txn = listOfTransactions[_i2];
		var GstinNo = txn.getGstinNo();
		var invoiceDate = MyDate.getDate('d/m/y', txn.getInvoiceDate());
		var invoiceValue = txn.getInvoiceValue();
		var refNo = txn.getTxnRefNumber() === '' ? '' : txn.getInvoiceNo();
		var cessRate = txn.getCessRate();
		var invoiceRate = txn.getRate() - cessRate;
		var invoceTaxableValue = txn.getInvoiceTaxableValue();
		var igstAmt = txn.getIGSTAmt();
		var sgstAmt = txn.getSGSTAmt();
		var cgstAmt = txn.getCGSTAmt();
		var cessAmt = txn.getCESSAmt();
		var otherAmt = txn.getOTHERAmt();
		var POS = txn.getPlaceOfSupply();
		var additionalCess = txn.getAdditionalCessAmt();
		var partyName = txn.getPartyName();
		var transactionType = txn.getTransactionType();
		tempArray.push(GstinNo);
		tempArray.push(partyName);
		tempArray.push(TxnTypeConstant.getTxnTypeForUI(transactionType));
		tempArray.push(refNo);
		tempArray.push(invoiceDate);
		tempArray.push(invoiceValue);
		tempArray.push(MyDouble.getPercentageWithDecimal(invoiceRate));
		tempArray.push(MyDouble.getPercentageWithDecimal(cessRate));
		tempArray.push(invoceTaxableValue);
		tempArray.push(MyDouble.getAmountWithDecimal(igstAmt));
		tempArray.push(MyDouble.getAmountWithDecimal(cgstAmt));
		tempArray.push(MyDouble.getAmountWithDecimal(sgstAmt));
		tempArray.push(MyDouble.getAmountWithDecimal(cessAmt));
		if (GSTR_2_data.showOTHER) {
			tempArray.push(MyDouble.getAmountWithDecimal(otherAmt));
		}
		if (settingCache.getAdditionalCessEnabled()) {
			tempArray.push(MyDouble.getAmountWithDecimal(additionalCess));
		}
		tempArray.push(POS);
		rowObj.push(tempArray);
	}
	rowObj.push([]);
	totalArray.push('Total');
	totalArray.push('');
	totalArray.push('');
	totalArray.push('');
	totalArray.push('');
	totalArray.push(GSTR_2_data.totalInvoiceValue);
	totalArray.push('');
	totalArray.push('');
	totalArray.push(GSTR_2_data.totalTaxableValue);
	totalArray.push(GSTR_2_data.totalIGSTAmount);
	totalArray.push(GSTR_2_data.totalCGSTAmount);
	totalArray.push(GSTR_2_data.totalSGSTAmount);
	totalArray.push(GSTR_2_data.totalCESSAmount);
	if (GSTR_2_data.showOTHER) {
		totalArray.push(GSTR_2_data.totalOTHERAmount);
	}
	if (settingCache.getAdditionalCessEnabled()) {
		totalArray.push(GSTR_2_data.totalAdditionalCessAmount);
	}
	rowObj.push(totalArray);
	return rowObj;
};

var getObjectForB2B = function getObjectForB2B(listOfTransactions) {
	var tableHeadArray = [];
	var rowObj = [];
	rowObj.push(['Summary Of Supplies From Registered Suppliers B2B(3)', '', '', '', '', '', '', '', '', '', '']);
	rowObj.push(['No. of Suppliers', 'No. of Invoices', '', 'Total Invoice Value', '', '', '', '', 'Total Taxable Value', 'Total Integrated Tax Paid', 'Total Central Tax Paid', 'Total TState/UT Tax Paid', 'Total Cess', '', 'Total Availed ITC Integrated Tax', 'Total Availed ITC Central Tax', 'Total Availed ITC State/UT Tax', 'Total Availed ITC Cess']);
	rowObj.push(['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']);
	rowObj.push(['GSTIN of Supplier', 'Invoice Number', 'Invoice date', 'Invoice Value', 'Place Of Supply', 'Reverse Charge', 'Invoice Type', 'Rate', 'Taxable Value', 'Integrated Tax Paid', 'Central Tax Paid', 'State/UT Tax Paid', 'Cess Paid', 'Eligibility For ITC', 'Availed ITC Integrated Tax', 'Availed ITC Central Tax', 'Availed ITC State/UT Tax', 'Availed ITC Cess']);

	var firmId = Number($('#firmFilterOptions').val());

	var invoiceNoMap = new _map2.default();
	var gstinNoMap = new _map2.default();
	var totalTaxableValue = 0;
	var totalInvoiceValue = 0;
	var totalIGSTPaid = 0;
	var totalCGSTPaid = 0;
	var totalSGSTPaid = 0;
	var totalCessPaid = 0;
	var totalIGSTAvailedITC = 0;
	var totalCGSTAvailedITC = 0;
	var totalSGSTAvailedITC = 0;
	var totalCessAvailedITC = 0;

	for (var i = 0; i < listOfTransactions.length; i++) {
		var txn = listOfTransactions[i];
		var transactionId = txn.getTransactionId();
		var transactionType = txn.getTransactionType();
		var gstinNo = txn.getGstinNo();
		var firmId = txn.getFirmId();
		var firm = firmCache.getFirmById(firmId);
		var firmName = firm.getFirmName();
		var reverseCharge = txn.getReverseCharge();
		var firmId = txn.getFirmId();
		var firm = firmCache.getFirmById(firmId);
		var firmName = firm.getFirmName();

		if (transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE && gstinNo != '' && gstinNo != null && MyDouble.convertStringToDouble(txn.getTaxRateId(), true) && taxCodeCache.getTaxCodeObjectById(txn.getTaxRateId()).getTaxRateType() != TaxCodeConstants.Exempted && taxCodeCache.getTaxCodeObjectById(txn.getTaxRateId()).getTaxRate() != 0) {
			var taxableValue = txn.getInvoiceTaxableValue();
			var invoiceValue = txn.getInvoiceValue();
			if (!invoiceNoMap.has(transactionId)) {
				invoiceNoMap.set(transactionId, invoiceValue);
				totalInvoiceValue += invoiceValue;
			}
			totalTaxableValue += taxableValue;

			var tempArray = [];
			var invoiceNo = MyString.invoiceStringForGSTR1Report(txn.getInvoiceNo());
			var invoiceDate = GSTRReportHelper.getDateFormatForGSTRReport(txn.getInvoiceDate());
			var placeOfSupply = txn.getPlaceOfSupply() ? StateCode.getStateCode(txn.getPlaceOfSupply()) + '-' + txn.getPlaceOfSupply() : '';
			var reverseChargeString = reverseCharge ? 'Y' : 'N';
			var invoiceType = 'Regular';
			var ecommerceGSTIN = '';
			var cessRate = txn.getCessRate();
			var rate = txn.getRate() - cessRate;
			var cessAmount = txn.getCESSAmt() + txn.getAdditionalCessAmt();
			totalCessPaid += cessAmount;
			cessAmount = cessAmount || '';

			if (!gstinNoMap.has(gstinNo) && gstinNo != null && gstinNo.trim() != '') {
				gstinNoMap.set(gstinNo, 1);
			}

			var itcType = txn.getItcTypeForReport();
			if (itcType == ITCTypeConstantsGSTR2.INPUTS) {
				var eligibilityForITC = 'Inputs';
			} else if (itcType == ITCTypeConstantsGSTR2.INPUT_SERVICES) {
				var eligibilityForITC = 'Input services';
			} else if (itcType == ITCTypeConstantsGSTR2.CAPITAL_GOODS) {
				var eligibilityForITC = 'Capital goods';
			} else if (itcType == ITCTypeConstantsGSTR2.INELIGIBLE) {
				var eligibilityForITC = 'Ineligible';
			}

			totalIGSTPaid += txn.getIGSTAmt();
			totalCGSTPaid += txn.getCGSTAmt();
			totalSGSTPaid += txn.getSGSTAmt();

			tempArray.push(gstinNo);
			tempArray.push(invoiceNo);
			tempArray.push(invoiceDate);
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(invoiceValue));
			tempArray.push(placeOfSupply);
			tempArray.push(reverseChargeString);
			tempArray.push(invoiceType);
			tempArray.push(MyDouble.getPercentageWithDecimal(rate));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(taxableValue));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(txn.getIGSTAmt()));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(txn.getCGSTAmt()));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(txn.getSGSTAmt()));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(cessAmount));
			tempArray.push(eligibilityForITC);
			if (itcType == ITCTypeConstantsGSTR2.INELIGIBLE) {
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(0));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(0));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(0));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(0));
			} else {
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(txn.getIGSTAmt()));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(txn.getCGSTAmt()));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(txn.getSGSTAmt()));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(cessAmount));
				totalIGSTAvailedITC += txn.getIGSTAmt();
				totalCGSTAvailedITC += txn.getCGSTAmt();
				totalSGSTAvailedITC += txn.getSGSTAmt();
				totalCessAvailedITC += cessAmount || 0;
			}
			rowObj.push(tempArray);
		}
	}
	rowObj[2][0] = gstinNoMap.size;
	rowObj[2][1] = invoiceNoMap.size;
	rowObj[2][3] = MyDouble.getAmountDecimalForGSTRReport(totalInvoiceValue);
	rowObj[2][8] = MyDouble.getAmountDecimalForGSTRReport(totalTaxableValue);
	rowObj[2][9] = MyDouble.getAmountDecimalForGSTRReport(totalIGSTPaid);
	rowObj[2][10] = MyDouble.getAmountDecimalForGSTRReport(totalCGSTPaid);
	rowObj[2][11] = MyDouble.getAmountDecimalForGSTRReport(totalSGSTPaid);
	rowObj[2][12] = MyDouble.getAmountDecimalForGSTRReport(totalCessPaid);
	rowObj[2][14] = MyDouble.getAmountDecimalForGSTRReport(totalIGSTAvailedITC);
	rowObj[2][15] = MyDouble.getAmountDecimalForGSTRReport(totalCGSTAvailedITC);
	rowObj[2][16] = MyDouble.getAmountDecimalForGSTRReport(totalSGSTAvailedITC);
	rowObj[2][17] = MyDouble.getAmountDecimalForGSTRReport(totalCessAvailedITC);
	return rowObj;
};

var getObjectForB2BUR = function getObjectForB2BUR(listOfTransactions) {
	var tableHeadArray = [];
	var rowObj = [];
	rowObj.push(['Summary Of Supplies From Unregistered Suppliers B2BUR(4B)', '', '', '', '', '', '', '', '', '', '']);
	rowObj.push(['', 'No. of Invoices (Of Reg Recipient)', '', 'Total Invoice Value', '', '', '', 'Total Taxable Value', 'Total Integrated Tax Paid', 'Total Central Tax Paid', 'Total TState/UT Tax Paid', 'Total Cess Paid', '', 'Total Availed ITC Integrated Tax', 'Total Availed ITC Central Tax', 'Total Availed ITC State/UT Tax', 'Total Availed ITC Cess']);
	rowObj.push(['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']);
	rowObj.push(['Supplier Name', 'Invoice Number', 'Invoice date', 'Invoice Value', 'Place Of Supply', 'Supply Type', 'Rate', 'Taxable Value', 'Integrated Tax Paid', 'Central Tax Paid', 'State/UT Tax Paid', 'Cess Paid', 'Eligibility For ITC', 'Availed ITC Integrated Tax', 'Availed ITC Central Tax', 'Availed ITC State/UT Tax', 'Availed ITC Cess']);

	var firmId = Number($('#firmFilterOptions').val());

	var invoiceNoMap = new _map2.default();
	var totalTaxableValue = 0;
	var totalInvoiceValue = 0;
	var totalIGSTPaid = 0;
	var totalCGSTPaid = 0;
	var totalSGSTPaid = 0;
	var totalCessPaid = 0;
	var totalIGSTAvailedITC = 0;
	var totalCGSTAvailedITC = 0;
	var totalSGSTAvailedITC = 0;
	var totalCessAvailedITC = 0;

	for (var i = 0; i < listOfTransactions.length; i++) {
		var txn = listOfTransactions[i];
		var transactionId = txn.getTransactionId();
		var transactionType = txn.getTransactionType();
		var gstinNo = txn.getGstinNo();
		var firmId = txn.getFirmId();
		var firm = firmCache.getFirmById(firmId);
		var firmName = firm.getFirmName();
		var reverseCharge = txn.getReverseCharge();
		var supplierName = nameCache.findNameByNameId(txn.getNameId());

		if (transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE && (gstinNo == '' || gstinNo == null) && MyDouble.convertStringToDouble(txn.getTaxRateId(), true) && taxCodeCache.getTaxCodeObjectById(txn.getTaxRateId()).getTaxRateType() != TaxCodeConstants.Exempted && taxCodeCache.getTaxCodeObjectById(txn.getTaxRateId()).getTaxRate() != 0) {
			var taxableValue = txn.getInvoiceTaxableValue();
			var invoiceValue = txn.getInvoiceValue();
			if (!invoiceNoMap.has(transactionId)) {
				invoiceNoMap.set(transactionId, invoiceValue);
				totalInvoiceValue += invoiceValue;
			}
			totalTaxableValue += taxableValue;

			var tempArray = [];
			var invoiceNo = MyString.invoiceStringForGSTR1Report(txn.getInvoiceNo());
			var invoiceDate = GSTRReportHelper.getDateFormatForGSTRReport(txn.getInvoiceDate());
			var placeOfSupply = txn.getPlaceOfSupply() ? StateCode.getStateCode(txn.getPlaceOfSupply()) + '-' + txn.getPlaceOfSupply() : '';
			var reverseChargeString = reverseCharge ? 'Y' : 'N';
			var invoiceType = 'Regular';
			var ecommerceGSTIN = '';
			var cessRate = txn.getCessRate();
			var rate = txn.getRate() - cessRate;
			var cessAmount = txn.getCESSAmt() + txn.getAdditionalCessAmt();
			totalCessPaid += cessAmount;
			cessAmount = cessAmount || '';

			var partyState = nameCache.findNameObjectByNameId(txn.getNameId()).getContactState();

			if (partyState != '' && partyState != txn.getPlaceOfSupply()) {
				var supplyType = 'Inter State';
			} else {
				var supplyType = 'Intra State';
			}

			var itcType = txn.getItcTypeForReport();
			if (itcType == ITCTypeConstantsGSTR2.INPUTS) {
				var eligibilityForITC = 'Inputs';
			} else if (itcType == ITCTypeConstantsGSTR2.INPUT_SERVICES) {
				var eligibilityForITC = 'Input services';
			} else if (itcType == ITCTypeConstantsGSTR2.CAPITAL_GOODS) {
				var eligibilityForITC = 'Capital goods';
			} else if (itcType == ITCTypeConstantsGSTR2.INELIGIBLE) {
				var eligibilityForITC = 'Ineligible';
			}

			totalIGSTPaid += txn.getIGSTAmt();
			totalCGSTPaid += txn.getCGSTAmt();
			totalSGSTPaid += txn.getSGSTAmt();

			tempArray.push(supplierName);
			tempArray.push(invoiceNo);
			tempArray.push(invoiceDate);
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(invoiceValue));
			tempArray.push(placeOfSupply);
			tempArray.push(supplyType);
			tempArray.push(MyDouble.getPercentageWithDecimal(rate));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(taxableValue));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(txn.getIGSTAmt()));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(txn.getCGSTAmt()));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(txn.getSGSTAmt()));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(cessAmount));
			tempArray.push(eligibilityForITC);
			if (itcType == ITCTypeConstantsGSTR2.INELIGIBLE) {
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(0));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(0));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(0));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(0));
			} else {
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(txn.getIGSTAmt()));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(txn.getCGSTAmt()));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(txn.getSGSTAmt()));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(cessAmount));
				totalIGSTAvailedITC += txn.getIGSTAmt();
				totalCGSTAvailedITC += txn.getCGSTAmt();
				totalSGSTAvailedITC += txn.getSGSTAmt();
				totalCessAvailedITC += cessAmount || 0;
			}
			rowObj.push(tempArray);
		}
	}

	rowObj[2][1] = invoiceNoMap.size;
	rowObj[2][3] = MyDouble.getAmountDecimalForGSTRReport(totalInvoiceValue);
	rowObj[2][7] = MyDouble.getAmountDecimalForGSTRReport(totalTaxableValue);
	rowObj[2][8] = MyDouble.getAmountDecimalForGSTRReport(totalIGSTPaid);
	rowObj[2][9] = MyDouble.getAmountDecimalForGSTRReport(totalCGSTPaid);
	rowObj[2][10] = MyDouble.getAmountDecimalForGSTRReport(totalSGSTPaid);
	rowObj[2][11] = MyDouble.getAmountDecimalForGSTRReport(totalCessPaid);
	rowObj[2][13] = MyDouble.getAmountDecimalForGSTRReport(totalIGSTAvailedITC);
	rowObj[2][14] = MyDouble.getAmountDecimalForGSTRReport(totalCGSTAvailedITC);
	rowObj[2][15] = MyDouble.getAmountDecimalForGSTRReport(totalSGSTAvailedITC);
	rowObj[2][16] = MyDouble.getAmountDecimalForGSTRReport(totalCessAvailedITC);
	return rowObj;
};

var getObjectForCDNR = function getObjectForCDNR(listOfTransactions) {
	var tableHeadArray = [];
	var rowObj = [];
	rowObj.push(['Summary For CDNR(6C)', '', '', '', '', '']);
	rowObj.push(['No. of Supplier', 'No. of Notes/Vouchers', '', 'No. of Invoices', '', '', '', '', '', 'Total Note/Voucher Value', '', 'Total Taxable Value', 'Total Integrated Tax Paid', 'Total Central Tax Paid', 'Total TState/UT Tax Paid', 'Total Cess', '', 'Total Availed ITC Integrated Tax', 'Total Availed ITC Central Tax', 'Total Availed ITC State/UT Tax', 'Total Availed ITC Cess']);
	rowObj.push(['', '', '', '', '', '', '', '', '', '', '', '', '']);
	rowObj.push(['GSTIN of Supplier', 'Note/Refund Voucher Number', 'Note/Refund Voucher date', 'Invoice/Advance Payment Voucher Number', 'Invoice/Advance Payment Voucher date', 'Pre GST', 'Document Type', 'Reason For Issuing document', 'Supply Type', 'Note/Refund Voucher Value', 'Rate', 'Taxable Value', 'Integrated Tax Paid', 'Central Tax Paid', 'State/UT Tax Paid', 'Cess Paid', 'Eligibility For ITC', 'Availed ITC Integrated Tax', 'Availed ITC Central Tax', 'Availed ITC State/UT Tax', 'Availed ITC Cess']);
	var invoiceNoMap = new _map2.default();
	var gstinNoMap = new _map2.default();
	var totalTaxableValue = 0;
	var totalInvoiceValue = 0;
	var totalIGSTPaid = 0;
	var totalCGSTPaid = 0;
	var totalSGSTPaid = 0;
	var totalCessPaid = 0;
	var totalIGSTAvailedITC = 0;
	var totalCGSTAvailedITC = 0;
	var totalSGSTAvailedITC = 0;
	var totalCessAvailedITC = 0;

	for (var i = 0; i < listOfTransactions.length; i++) {
		var txn = listOfTransactions[i];
		var gstinNo = txn.getGstinNo();
		var firmId = txn.getFirmId();
		var firm = firmCache.getFirmById(firmId);
		var firmState = firm.getFirmState();
		var transactionId = txn.getTransactionId();
		var transactionType = txn.getTransactionType();

		if (transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN && gstinNo != '' && gstinNo != null) {
			var noteInvoiceValue = txn.getInvoiceValue();
			if (!invoiceNoMap.has(transactionId)) {
				invoiceNoMap.set(transactionId, noteInvoiceValue);
				totalInvoiceValue += noteInvoiceValue;
			}
			var noteTaxableValue = txn.getInvoiceTaxableValue();
			totalTaxableValue += noteTaxableValue;

			var tempArray = [];
			var mainInvoiceNo = MyString.invoiceStringForGSTR1Report(txn.getTxnReturnRefNumber());
			var mainInvoiceDate = txn.getTxnReturnDate() ? GSTRReportHelper.getDateFormatForGSTRReport(txn.getTxnReturnDate()) : '';
			var noteNumber = MyString.invoiceStringForGSTR1Report(txn.getInvoiceNo());
			var noteDate = GSTRReportHelper.getDateFormatForGSTRReport(txn.getInvoiceDate());
			if (txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
				var documentType = 'D';
			}
			var reasonForIssuingDocument = '01-Sales Return';
			var placeOfSupply = txn.getPlaceOfSupply() ? StateCode.getStateCode(txn.getPlaceOfSupply()) + '-' + txn.getPlaceOfSupply() : '';
			var cessRate = txn.getCessRate();
			var rate = txn.getRate() - cessRate;
			var cessAmount = txn.getCESSAmt() + txn.getAdditionalCessAmt();
			totalCessPaid += cessAmount;
			cessAmount = cessAmount || '';
			var preGST = 'N';

			if (!gstinNoMap.has(gstinNo) && gstinNo != null && gstinNo.trim() != '') {
				gstinNoMap.set(gstinNo, 1);
			}

			var partyState = nameCache.findNameObjectByNameId(txn.getNameId()).getContactState();

			if (partyState != '' && partyState != txn.getPlaceOfSupply()) {
				var supplyType = 'Inter State';
			} else {
				var supplyType = 'Intra State';
			}

			var itcType = txn.getItcTypeForReport();
			if (itcType == ITCTypeConstantsGSTR2.INPUTS) {
				var eligibilityForITC = 'Inputs';
			} else if (itcType == ITCTypeConstantsGSTR2.INPUT_SERVICES) {
				var eligibilityForITC = 'Input services';
			} else if (itcType == ITCTypeConstantsGSTR2.CAPITAL_GOODS) {
				var eligibilityForITC = 'Capital goods';
			} else if (itcType == ITCTypeConstantsGSTR2.INELIGIBLE) {
				var eligibilityForITC = 'Ineligible';
			}

			totalIGSTPaid += txn.getIGSTAmt();
			totalCGSTPaid += txn.getCGSTAmt();
			totalSGSTPaid += txn.getSGSTAmt();

			tempArray.push(gstinNo);
			tempArray.push(noteNumber);
			tempArray.push(noteDate);
			tempArray.push(mainInvoiceNo);
			tempArray.push(mainInvoiceDate);
			tempArray.push(preGST);
			tempArray.push(documentType);
			tempArray.push(reasonForIssuingDocument);
			tempArray.push(supplyType);
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(noteInvoiceValue));
			tempArray.push(MyDouble.getPercentageWithDecimal(rate));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(noteTaxableValue));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(txn.getIGSTAmt()));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(txn.getCGSTAmt()));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(txn.getSGSTAmt()));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(cessAmount));
			tempArray.push(eligibilityForITC);
			if (itcType == ITCTypeConstantsGSTR2.INELIGIBLE) {
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(0));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(0));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(0));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(0));
			} else {
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(txn.getIGSTAmt()));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(txn.getCGSTAmt()));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(txn.getSGSTAmt()));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(cessAmount));
				totalIGSTAvailedITC += txn.getIGSTAmt();
				totalCGSTAvailedITC += txn.getCGSTAmt();
				totalSGSTAvailedITC += txn.getSGSTAmt();
				totalCessAvailedITC += cessAmount || 0;
			}
			rowObj.push(tempArray);
		}
	}
	rowObj[2][1] = invoiceNoMap.size;
	rowObj[2][9] = MyDouble.getAmountDecimalForGSTRReport(totalInvoiceValue);
	rowObj[2][11] = MyDouble.getAmountDecimalForGSTRReport(totalTaxableValue);
	rowObj[2][12] = MyDouble.getAmountDecimalForGSTRReport(totalIGSTPaid);
	rowObj[2][13] = MyDouble.getAmountDecimalForGSTRReport(totalCGSTPaid);
	rowObj[2][14] = MyDouble.getAmountDecimalForGSTRReport(totalSGSTPaid);
	rowObj[2][15] = MyDouble.getAmountDecimalForGSTRReport(totalCessPaid);
	rowObj[2][17] = MyDouble.getAmountDecimalForGSTRReport(totalIGSTAvailedITC);
	rowObj[2][18] = MyDouble.getAmountDecimalForGSTRReport(totalCGSTAvailedITC);
	rowObj[2][19] = MyDouble.getAmountDecimalForGSTRReport(totalSGSTAvailedITC);
	rowObj[2][20] = MyDouble.getAmountDecimalForGSTRReport(totalCessAvailedITC);
	return rowObj;
};

var getObjectForCDNUR = function getObjectForCDNUR(listOfTransactions) {
	var tableHeadArray = [];
	var rowObj = [];
	rowObj.push(['Summary For CDNUR(6C)', '', '', '', '', '']);
	rowObj.push(['No. of Notes/Vouchers', '', 'No. of Invoices', '', '', '', '', '', 'Total Note/Voucher Value', '', 'Total Taxable Value', 'Total Integrated Tax Paid', 'Total Central Tax Paid', 'Total TState/UT Tax Paid', 'Total Cess Paid', '', 'Total ITC Integrated Tax Amount', 'Total Central Tax Amount', 'Total ITC State/UT Tax Amount', 'Total ITC Cess Amount']);
	rowObj.push(['', '', '', '', '', '', '', '', '', '', '', '', '']);
	rowObj.push(['Note/Voucher Number', 'Note/Voucher date', 'Invoice/Advance Payment Voucher Number', 'Invoice/Advance Payment Voucher date', 'Pre GST', 'Document Type', 'Reason For Issuing document', 'Supply Type', 'Note/Voucher Value', 'Rate', 'Taxable Value', 'Integrated Tax Paid', 'Central Tax Paid', 'State/UT Tax Paid', 'Cess Paid', 'Eligibility For ITC', 'Availed ITC Integrated Tax', 'Availed ITC Central Tax', 'Availed ITC State/UT Tax', 'Availed ITC Cess']);
	var invoiceNoMap = new _map2.default();
	var totalTaxableValue = 0;
	var totalInvoiceValue = 0;
	var totalIGSTPaid = 0;
	var totalCGSTPaid = 0;
	var totalSGSTPaid = 0;
	var totalCessPaid = 0;
	var totalIGSTAvailedITC = 0;
	var totalCGSTAvailedITC = 0;
	var totalSGSTAvailedITC = 0;
	var totalCessAvailedITC = 0;

	for (var i = 0; i < listOfTransactions.length; i++) {
		var txn = listOfTransactions[i];
		var gstinNo = txn.getGstinNo();
		var firmId = txn.getFirmId();
		var firm = firmCache.getFirmById(firmId);
		var firmState = firm.getFirmState();
		var transactionId = txn.getTransactionId();
		var transactionType = txn.getTransactionType();

		if (transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN && (!gstinNo || !gstinNo.trim())) {
			var noteInvoiceValue = txn.getInvoiceValue();
			if (!invoiceNoMap.has(transactionId)) {
				invoiceNoMap.set(transactionId, noteInvoiceValue);
				totalInvoiceValue += noteInvoiceValue;
			}
			var noteTaxableValue = txn.getInvoiceTaxableValue();
			totalTaxableValue += noteTaxableValue;

			var tempArray = [];
			var mainInvoiceNo = MyString.invoiceStringForGSTR1Report(txn.getTxnReturnRefNumber());
			var mainInvoiceDate = txn.getTxnReturnDate() ? GSTRReportHelper.getDateFormatForGSTRReport(txn.getTxnReturnDate()) : '';
			var noteNumber = MyString.invoiceStringForGSTR1Report(txn.getInvoiceNo());
			var noteDate = GSTRReportHelper.getDateFormatForGSTRReport(txn.getInvoiceDate());
			if (txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
				var documentType = 'D';
			}
			var reasonForIssuingDocument = '01-Sales Return';
			var placeOfSupply = txn.getPlaceOfSupply() ? StateCode.getStateCode(txn.getPlaceOfSupply()) + '-' + txn.getPlaceOfSupply() : '';
			var cessRate = txn.getCessRate();
			var rate = txn.getRate() - cessRate;
			var cessAmount = txn.getCESSAmt() + txn.getAdditionalCessAmt();
			totalCessPaid += cessAmount;
			cessAmount = cessAmount || '';
			var preGST = 'N';

			var partyState = nameCache.findNameObjectByNameId(txn.getNameId()).getContactState();

			if (partyState != '' && partyState != txn.getPlaceOfSupply()) {
				var supplyType = 'Inter State';
			} else {
				var supplyType = 'Intra State';
			}
			var itcType = txn.getItcTypeForReport();
			if (itcType == ITCTypeConstantsGSTR2.INPUTS) {
				var eligibilityForITC = 'Inputs';
			} else if (itcType == ITCTypeConstantsGSTR2.INPUT_SERVICES) {
				var eligibilityForITC = 'Input services';
			} else if (itcType == ITCTypeConstantsGSTR2.CAPITAL_GOODS) {
				var eligibilityForITC = 'Capital goods';
			} else if (itcType == ITCTypeConstantsGSTR2.INELIGIBLE) {
				var eligibilityForITC = 'Ineligible';
			}

			totalIGSTPaid += txn.getIGSTAmt();
			totalCGSTPaid += txn.getCGSTAmt();
			totalSGSTPaid += txn.getSGSTAmt();

			tempArray.push(noteNumber);
			tempArray.push(noteDate);
			tempArray.push(mainInvoiceNo);
			tempArray.push(mainInvoiceDate);
			tempArray.push(preGST);
			tempArray.push(documentType);
			tempArray.push(reasonForIssuingDocument);
			tempArray.push(supplyType);
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(noteInvoiceValue));
			tempArray.push(MyDouble.getPercentageWithDecimal(rate));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(noteTaxableValue));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(txn.getIGSTAmt()));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(txn.getCGSTAmt()));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(txn.getSGSTAmt()));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(cessAmount));
			tempArray.push(eligibilityForITC);
			if (itcType == ITCTypeConstantsGSTR2.INELIGIBLE) {
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(0));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(0));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(0));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(0));
			} else {
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(txn.getIGSTAmt()));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(txn.getCGSTAmt()));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(txn.getSGSTAmt()));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(cessAmount));
				totalIGSTAvailedITC += txn.getIGSTAmt();
				totalCGSTAvailedITC += txn.getCGSTAmt();
				totalSGSTAvailedITC += txn.getSGSTAmt();
				totalCessAvailedITC += cessAmount || 0;
			}
			rowObj.push(tempArray);
		}
	}
	rowObj[2][0] = invoiceNoMap.size;
	rowObj[2][8] = MyDouble.getAmountDecimalForGSTRReport(totalInvoiceValue);
	rowObj[2][10] = MyDouble.getAmountDecimalForGSTRReport(totalTaxableValue);
	rowObj[2][11] = MyDouble.getAmountDecimalForGSTRReport(totalIGSTPaid);
	rowObj[2][12] = MyDouble.getAmountDecimalForGSTRReport(totalCGSTPaid);
	rowObj[2][13] = MyDouble.getAmountDecimalForGSTRReport(totalSGSTPaid);
	rowObj[2][14] = MyDouble.getAmountDecimalForGSTRReport(totalCessPaid);
	rowObj[2][16] = MyDouble.getAmountDecimalForGSTRReport(totalIGSTAvailedITC);
	rowObj[2][17] = MyDouble.getAmountDecimalForGSTRReport(totalCGSTAvailedITC);
	rowObj[2][18] = MyDouble.getAmountDecimalForGSTRReport(totalSGSTAvailedITC);
	rowObj[2][19] = MyDouble.getAmountDecimalForGSTRReport(totalCessAvailedITC);
	return rowObj;
};

var getObjectForExempReport = function getObjectForExempReport(listOfTransactions) {
	var TaxCodeConstants = require('./../Constants/TaxCodeConstants.js');
	var rowObj = [];

	var interStateNilRatedSupplies = 0;
	var intraStateNilRatedSupplies = 0;
	var interStateExemptedSupplies = 0;
	var intraStateExemptedSupplies = 0;
	var interStateComposition = 0;
	var intraStateComposition = 0;

	for (var i = 0; i < listOfTransactions.length; i++) {
		var txn = listOfTransactions[i];
		var taxCode = taxCodeCache.getTaxCodeObjectById(txn.getTaxRateId());
		var partyState = nameCache.findNameObjectByNameId(txn.getNameId()).getContactState();
		var customerType = nameCache.findNameObjectByNameId(txn.getNameId()).getCustomerType();
		if (txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_PURCHASE) {
			if (partyState && txn.getPlaceOfSupply() && partyState != txn.getPlaceOfSupply()) {
				// inter state
				if (customerType == NameCustomerType.REGISTERED_COMPOSITE) {
					interStateComposition += txn.getTotalAmount();
				} else if (taxCode == null || taxCode.getTaxRateType() == TaxCodeConstants.Exempted) {
					interStateExemptedSupplies += txn.getInvoiceTaxableValue();
				} else if (taxCode != null && taxCode.getTaxRate() == 0) {
					interStateNilRatedSupplies += txn.getInvoiceTaxableValue();
				}
			} else {
				if (customerType == NameCustomerType.REGISTERED_COMPOSITE) {
					/// /intra state
					intraStateComposition += txn.getTotalAmount();
				} else if (taxCode == null || taxCode.getTaxRateType() == TaxCodeConstants.Exempted) {
					// exempted
					intraStateExemptedSupplies += txn.getInvoiceTaxableValue();
				} else if (taxCode != null && taxCode.getTaxRate() == 0) {
					// nil rated
					intraStateNilRatedSupplies += txn.getInvoiceTaxableValue();
				}
			}
		} else if (txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
			if (partyState && txn.getPlaceOfSupply() && partyState != txn.getPlaceOfSupply()) {
				if (customerType == NameCustomerType.REGISTERED_COMPOSITE) {
					/// /inter
					interStateComposition -= txn.getTotalAmount();
				} else if (taxCode == null || taxCode.getTaxRateType() == TaxCodeConstants.Exempted) {
					interStateExemptedSupplies -= txn.getInvoiceTaxableValue();
				} else if (taxCode != null && taxCode.getTaxRate() == 0) {
					interStateNilRatedSupplies -= txn.getInvoiceTaxableValue();
				}
			} else {
				if (customerType == NameCustomerType.REGISTERED_COMPOSITE) {
					/// /intra state
					intraStateComposition -= txn.getTotalAmount();
				} else if (taxCode == null || taxCode.getTaxRateType() == TaxCodeConstants.Exempted) {
					// exempted
					intraStateExemptedSupplies -= txn.getInvoiceTaxableValue();
				} else if (taxCode != null && taxCode.getTaxRate() == 0) {
					// nil rated
					intraStateNilRatedSupplies -= txn.getInvoiceTaxableValue();
				}
			}
		}
	}

	var totalComposition = intraStateComposition + interStateComposition;
	var totalNilRated = interStateNilRatedSupplies + intraStateNilRatedSupplies;
	var totalExempted = interStateExemptedSupplies + intraStateExemptedSupplies;

	rowObj.push(['Summary For Composition, Nil rated, exempted and non GST inward supplies (7))', '', '', '']);
	rowObj.push(['', 'Total Composition taxable person', 'Total Nil Rated Supplies', 'Total Exempted Supplies', 'Total Non-GST Supplies']);
	rowObj.push(['', MyDouble.getAmountDecimalForGSTRReport(totalComposition), MyDouble.getAmountDecimalForGSTRReport(totalNilRated), MyDouble.getAmountDecimalForGSTRReport(totalExempted), 0.0]);
	rowObj.push(['Description', 'Composition taxable person', 'Nil Rated Supplies', 'Exempted (other than nil rated/non GST supply )', 'Non-GST Supplies']);
	rowObj.push(['Inter-State supplies', MyDouble.getAmountDecimalForGSTRReport(interStateComposition), MyDouble.getAmountDecimalForGSTRReport(interStateNilRatedSupplies), MyDouble.getAmountDecimalForGSTRReport(interStateExemptedSupplies), 0.0]);
	rowObj.push(['Intra-State supplies', MyDouble.getAmountDecimalForGSTRReport(intraStateComposition), MyDouble.getAmountDecimalForGSTRReport(intraStateNilRatedSupplies), MyDouble.getAmountDecimalForGSTRReport(intraStateExemptedSupplies), 0.0]);

	return rowObj;
};

var getObjectForItemHSNReport = function getObjectForItemHSNReport(listOfItemData) {
	var showItemName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

	var tableHeadArray = [];
	var rowObj = [];
	rowObj.push(['Summary For HSN(13)', '', '', '', '', '', '', '', '', '', '', '', '']);
	rowObj.push(['No. of HSN', '', '', '', 'Total Value', 'Total Taxable Value', 'Total Integrated Tax', 'Total Central Tax', 'Total State/UT Tax', 'Total Cess']);
	rowObj.push(['', '', '', '', '', '', '', '', '', '', '', '', '']);
	rowObj.push(['HSN', 'Description', 'UQC', 'Total Quantity', 'Total Value', 'Taxable Value', 'Integrated Tax Amount', 'Central Tax Amount', 'State/UT Tax Amount', 'Cess Amount']);

	var hsnNoMap = new _map2.default();
	var totalTaxableValue = 0;
	var totalValue = 0;
	var totalCESSAmount = 0;
	var totalIGSTAmount = 0;
	var totalCGSTAmount = 0;
	var totalSGSTAmount = 0;

	for (var i = 0; i < listOfItemData.length; i++) {
		var txn = listOfItemData[i];
		var itemHsn = txn.getItemHSN();
		var itemDescription = showItemName && txn.getItemName() ? txn.getItemName() : '';
		var unit = 'OTH-OTHERS';
		var itemQuantity = MyDouble.convertStringToDouble(txn.getItemQuantity());
		var itemFreeQuantity = MyDouble.convertStringToDouble(txn.getItemFreeQuantity());
		var itemTotalValue = txn.getItemTotalValue();
		var itemTaxableValue = txn.getItemTaxableValue();
		var igstAmount = txn.getIGSTAmt();
		var cgstAmount = txn.getCGSTAmt();
		var sgstAmount = txn.getSGSTAmt();
		var cessAmount = txn.getCESSAmt() + txn.getAdditionalCessAmt();

		if (!hsnNoMap.has(itemHsn) && itemHsn != null && itemHsn.trim() != '') {
			hsnNoMap.set(itemHsn, 1);
		}
		totalTaxableValue += Number(itemTaxableValue);
		totalValue += Number(itemTotalValue);
		totalCESSAmount += Number(cessAmount);
		totalIGSTAmount += Number(igstAmount);
		totalCGSTAmount += Number(cgstAmount);
		totalSGSTAmount += Number(sgstAmount);

		var tempArray = [];
		tempArray.push(itemHsn);
		tempArray.push(itemDescription);
		tempArray.push(unit);
		tempArray.push(MyDouble.getQuantityDecimalForGSTRReport(itemQuantity + itemFreeQuantity));
		tempArray.push(MyDouble.getAmountDecimalForGSTRReport(itemTotalValue));
		tempArray.push(MyDouble.getAmountDecimalForGSTRReport(itemTaxableValue));
		tempArray.push(MyDouble.getAmountDecimalForGSTRReport(igstAmount));
		tempArray.push(MyDouble.getAmountDecimalForGSTRReport(cgstAmount));
		tempArray.push(MyDouble.getAmountDecimalForGSTRReport(sgstAmount));
		tempArray.push(MyDouble.getAmountDecimalForGSTRReport(cessAmount));
		rowObj.push(tempArray);
	}
	rowObj[2][0] = hsnNoMap.size;
	rowObj[2][4] = MyDouble.getAmountDecimalForGSTRReport(totalValue);
	rowObj[2][5] = MyDouble.getAmountDecimalForGSTRReport(totalTaxableValue);
	rowObj[2][6] = MyDouble.getAmountDecimalForGSTRReport(totalIGSTAmount);
	rowObj[2][7] = MyDouble.getAmountDecimalForGSTRReport(totalCGSTAmount);
	rowObj[2][8] = MyDouble.getAmountDecimalForGSTRReport(totalSGSTAmount);
	rowObj[2][9] = MyDouble.getAmountDecimalForGSTRReport(totalCESSAmount);
	return rowObj;
};

var getObjectForItcrReport = function getObjectForItcrReport(listOfTransactions) {
	var tableHeadArray = [];
	var rowObj = [];
	rowObj.push(['Summary Input Tax credit Reversal/Reclaim (11)', '', '', '', '', '']);
	rowObj.push(['', '', 'Total ITC Integrated Tax Amount', 'Total Central Tax Amount', 'Total ITC State/UT Tax Amount', 'Total ITC Cess Amount']);
	rowObj.push(['', '', '', '', '', '']);
	rowObj.push(['Description for reversal of ITC', 'To be added or reduced from output liability', 'ITC Integrated Tax Amount', 'ITC Central Tax Amount', 'ITC State/UT Tax Amount', 'ITC Cess Amount']);
	rowObj.push(['(a) Amount in terms of rule 37 (2)', '', '', '', '', '']);
	rowObj.push(['(b) Amount in terms of rule 42 (1) (m)', '', '', '', '', '']);
	rowObj.push(['(c) Amount in terms of rule 43(1) (h)', '', '', '', '', '']);
	rowObj.push(['(d) Amount in terms of rule 42 (2)(a)', '', '', '', '', '']);
	rowObj.push(['(e) Amount in terms of rule 42(2)(b)', '', '', '', '', '']);
	rowObj.push(['(f) On account of amount paid subsequent to reversal of ITC', '', '', '', '', '']);
	rowObj.push(['(g) Any other liability (Specify)', '', '', '', '', '']);
	return rowObj;
};

var getObjectForIMPS = function getObjectForIMPS(listOfTransactions) {
	var rowObj = [];
	rowObj.push(['Summary For IMPS(4C)', '', '', '', '', '', '', '', '', '', '']);
	rowObj.push(['No. of Invoices (Of Reg Recipient)', '', 'Total Invoice value', '', '', 'Total Taxable value', 'Total Integrated Tax Paid', 'Total Cess Paid', '', 'Total Availed ITC Integrated Tax', 'Total Availed ITC Cess']);
	rowObj.push(['', '', '', '', '', '', '', '', '', '', '']);
	rowObj.push(['Invoice Number of Reg Recipient', 'Invoice Date', 'Invoice Value', 'Place Of Supply', 'Rate', 'Taxable Value', 'Integrated Tax Paid', 'Cess Paid', 'Eligibility For ITC', 'Availed ITC Integrated Tax', 'Availed ITC Cess']);
	return rowObj;
};

var getObjectForIMPG = function getObjectForIMPG() {
	var rowObj = [];
	rowObj.push(['Summary For IMPG(5)', '', '', '', '', '', '', '', '', '', '']);
	rowObj.push(['', 'No. of Bill of Entry', '', 'Total Bill of Entry Value', '', '', '', 'Total Taxable Value', 'Total Integrated tax Paid', 'Total Cess Paid', '', 'Total Availed ITC Integrated Tax', 'Total Availed ITC Cess']);
	rowObj.push(['', '', '', '', '', '', '', '', '', '', '']);
	rowObj.push(['Port Code', 'Bill Of Entry Number', 'Bill Of Entry Date', 'Bill Of Entry Value', 'Document Type', 'GSTIN Of SEZ Supplier', 'Rate', 'Taxable Value', 'Integrated Tax Paid', 'Cess Paid', 'Eligibility For ITC', 'Availed ITC Integrated Tax', 'Availed ITC Cess']);
	return rowObj;
};

var getObjectForAT = function getObjectForAT() {
	var rowObj = [];
	rowObj.push(['Summary For Advance Paid under reverse charge (10 A)', '', '', '']);
	rowObj.push(['', '', 'Total Advance Paid', 'Total Cess Amount']);
	rowObj.push(['', '', '', '']);
	rowObj.push(['Place Of Supply', 'Supply Type', 'Gross Advance Paid', 'Cess Amount']);
	return rowObj;
};

var getObjectForATADJ = function getObjectForATADJ() {
	var rowObj = [];
	rowObj.push(['Summary For Adjustment of advance tax paid earlier for reverse charge supplies (10 B)', '', '', '']);
	rowObj.push(['', '', 'Total Advance Adjusted', 'Total Cess']);
	rowObj.push(['', '', '', '']);
	rowObj.push(['Place Of Supply', 'Supply Type', 'Gross Advance Paid to be Adjusted', 'Cess Adjusted']);
	return rowObj;
};

function printExcel() {
	var XLSX = require('xlsx');
	var dataArray = prepareObjectForExcel(listOfTransactions);
	var worksheet = XLSX.utils.aoa_to_sheet(dataArray);

	var workbook = {
		'SheetNames': [],
		'Sheets': {}
	};

	var wsName = 'GSTR2 Report';
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet;
	var wscolWidth = [{ wch: 20 }, { wch: 30 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 30 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }];
	worksheet['!cols'] = wscolWidth;

	/// /////////////////////////////////B2B///////////////////////////////////////

	var wsNameItem = 'b2b';
	workbook.SheetNames[1] = wsNameItem;
	var b2bArray = getObjectForB2B(listOfTransactions);
	var b2bWorksheet = XLSX.utils.aoa_to_sheet(b2bArray);
	workbook.Sheets[wsNameItem] = b2bWorksheet;
	var wsItemcolWidth = [{ wch: 30 }, { wch: 20 }, { wch: 20 }, { wch: 30 }, { wch: 40 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	b2bWorksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////B2BUR///////////////////////////////////////

	var wsNameItem = 'b2bur';
	workbook.SheetNames[2] = wsNameItem;
	var b2burArray = getObjectForB2BUR(listOfTransactions);
	var b2burWorksheet = XLSX.utils.aoa_to_sheet(b2burArray);
	workbook.Sheets[wsNameItem] = b2burWorksheet;
	var wsItemcolWidth = [{ wch: 40 }, { wch: 20 }, { wch: 20 }, { wch: 30 }, { wch: 40 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	b2burWorksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////IMPS///////////////////////////////////////

	var wsNameItem = 'imps';
	workbook.SheetNames[3] = wsNameItem;
	var impsArray = getObjectForIMPS(listOfTransactions);
	var impsWorksheet = XLSX.utils.aoa_to_sheet(impsArray);
	workbook.Sheets[wsNameItem] = impsWorksheet;
	var wsItemcolWidth = [{ wch: 40 }, { wch: 20 }, { wch: 20 }, { wch: 30 }, { wch: 40 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	impsWorksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////IMPG///////////////////////////////////////

	var wsNameItem = 'impg';
	workbook.SheetNames[4] = wsNameItem;
	var impgArray = getObjectForIMPG(listOfTransactions);
	var impgWorksheet = XLSX.utils.aoa_to_sheet(impgArray);
	workbook.Sheets[wsNameItem] = impgWorksheet;
	var wsItemcolWidth = [{ wch: 40 }, { wch: 20 }, { wch: 20 }, { wch: 30 }, { wch: 40 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	impgWorksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////CDNR///////////////////////////////////////

	var wsNameItem = 'cdnr';
	workbook.SheetNames[5] = wsNameItem;
	var cdnrArray = getObjectForCDNR(listOfTransactions);
	var cdnrWorksheet = XLSX.utils.aoa_to_sheet(cdnrArray);
	workbook.Sheets[wsNameItem] = cdnrWorksheet;
	var wsItemcolWidth = [{ wch: 22 }, { wch: 30 }, { wch: 25 }, { wch: 35 }, { wch: 35 }, { wch: 20 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	cdnrWorksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////CDNUR///////////////////////////////////////

	var wsNameItem = 'cdnur';
	workbook.SheetNames[6] = wsNameItem;
	var cdnurArray = getObjectForCDNUR(listOfTransactions);
	var cdnurWorksheet = XLSX.utils.aoa_to_sheet(cdnurArray);
	workbook.Sheets[wsNameItem] = cdnurWorksheet;
	var wsItemcolWidth = [{ wch: 22 }, { wch: 30 }, { wch: 25 }, { wch: 35 }, { wch: 35 }, { wch: 20 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	cdnurWorksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////AT///////////////////////////////////////

	var wsNameItem = 'at';
	workbook.SheetNames[7] = wsNameItem;
	var atArray = getObjectForAT(listOfTransactions);
	var atWorksheet = XLSX.utils.aoa_to_sheet(atArray);
	workbook.Sheets[wsNameItem] = atWorksheet;
	var wsItemcolWidth = [{ wch: 35 }, { wch: 35 }, { wch: 35 }, { wch: 35 }];
	atWorksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////ATADJ///////////////////////////////////////

	var wsNameItem = 'atadj';
	workbook.SheetNames[8] = wsNameItem;
	var atadjArray = getObjectForATADJ(listOfTransactions);
	var atadjWorksheet = XLSX.utils.aoa_to_sheet(atadjArray);
	workbook.Sheets[wsNameItem] = atadjWorksheet;
	var wsItemcolWidth = [{ wch: 35 }, { wch: 35 }, { wch: 35 }, { wch: 35 }];
	atadjWorksheet['!cols'] = wsItemcolWidth;

	/// ///////////////////////EXEMP /////////////////////////////////////

	var wsNameItem = 'exemp';
	workbook.SheetNames[9] = wsNameItem;
	var exempArray = getObjectForExempReport(listOfTransactions);
	var exempSheet = XLSX.utils.aoa_to_sheet(exempArray);
	workbook.Sheets[wsNameItem] = exempSheet;
	var wsItemcolWidth = [{ wch: 35 }, { wch: 25 }, { wch: 25 }, { wch: 25 }, { wch: 25 }, { wch: 25 }];
	exempSheet['!cols'] = wsItemcolWidth;

	/// ///////////////////////ITCR /////////////////////////////////////

	var wsNameItem = 'itcr';
	workbook.SheetNames[10] = wsNameItem;
	var itcrArray = getObjectForItcrReport(listOfTransactions);
	var itcrSheet = XLSX.utils.aoa_to_sheet(itcrArray);
	workbook.Sheets[wsNameItem] = itcrSheet;
	var wsItemcolWidth = [{ wch: 35 }, { wch: 35 }, { wch: 35 }, { wch: 35 }, { wch: 35 }, { wch: 35 }];
	itcrSheet['!cols'] = wsItemcolWidth;

	var considerNonTaxAsExemptedCheck = $('#considerNonTaxAsExempted').prop('checked');

	/// /////////////////////////////////Item Wise Purchase///////////////////////////////////////
	var firmId = Number($('#firmFilterOptions').val());
	var GSTR1ReportHelper = require('./../UIControllers/GSTR1ReportHelper.js');
	var gSTR1ReportHelper = new GSTR1ReportHelper();
	var listOfItemDataByHSNForPurchase = gSTR1ReportHelper.getItemWiseDataListBasedOnDate(startDate, endDate, firmId, TxnTypeConstant.TXN_TYPE_PURCHASE, considerNonTaxAsExemptedCheck);

	/// ///////////////////////////////Item Wise Purchase Return////////////////////////////////
	var GSTR1ReportHelper = require('./../UIControllers/GSTR1ReportHelper.js');
	var gSTR1ReportHelper = new GSTR1ReportHelper();
	var listOfItemDataByHSNForPurchaseReturn = gSTR1ReportHelper.getItemWiseDataListBasedOnDate(startDate, endDate, firmId, TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN, considerNonTaxAsExemptedCheck);

	/// //////////////////////////////////////// Item Summary ///////////////////////////////////////////////
	var itemSummaryList = [];
	var itemSummaryMap = new _map2.default();
	for (var i = 0; i < listOfItemDataByHSNForPurchase.length; i++) {
		var txn = listOfItemDataByHSNForPurchase[i];
		var itemId = txn.getItemId();
		itemSummaryMap.set(itemId, txn);
	}
	for (var i = 0; i < listOfItemDataByHSNForPurchaseReturn.length; i++) {
		var txn = listOfItemDataByHSNForPurchaseReturn[i];
		var itemId = txn.getItemId();
		if (!itemSummaryMap.has(itemId)) {
			var itemSummaryObject = txn;
			itemSummaryObject.setItemQuantity(0 - txn.getItemQuantity());
			itemSummaryObject.setItemFreeQuantity(0 - txn.getItemFreeQuantity());
			itemSummaryObject.setItemTotalValue(0 - txn.getItemTotalValue());
			itemSummaryObject.setItemTaxableValue(0 - txn.getItemTaxableValue());
			itemSummaryObject.setIGSTAmt(0 - txn.getIGSTAmt());
			itemSummaryObject.setCGSTAmt(0 - txn.getCGSTAmt());
			itemSummaryObject.setSGSTAmt(0 - txn.getSGSTAmt());
			itemSummaryObject.setCESSAmt(0 - txn.getCESSAmt());
			itemSummaryObject.setAdditionalCessAmt(0 - txn.getAdditionalCessAmt());
		} else {
			var itemSummaryObject = itemSummaryMap.get(itemId);
			itemSummaryObject.setItemQuantity(itemSummaryObject.getItemQuantity() - txn.getItemQuantity());
			itemSummaryObject.setItemFreeQuantity(itemSummaryObject.getItemFreeQuantity() - txn.getItemFreeQuantity());
			itemSummaryObject.setItemTotalValue(itemSummaryObject.getItemTotalValue() - txn.getItemTotalValue());
			itemSummaryObject.setItemTaxableValue(itemSummaryObject.getItemTaxableValue() - txn.getItemTaxableValue());
			itemSummaryObject.setIGSTAmt(itemSummaryObject.getIGSTAmt() - txn.getIGSTAmt());
			itemSummaryObject.setCGSTAmt(itemSummaryObject.getCGSTAmt() - txn.getCGSTAmt());
			itemSummaryObject.setSGSTAmt(itemSummaryObject.getSGSTAmt() - txn.getSGSTAmt());
			itemSummaryObject.setCESSAmt(itemSummaryObject.getCESSAmt() - txn.getCESSAmt());
			itemSummaryObject.setAdditionalCessAmt(itemSummaryObject.getAdditionalCessAmt() - txn.getAdditionalCessAmt());
		}
		itemSummaryMap.set(itemId, itemSummaryObject);
	}

	var _iteratorNormalCompletion = true;
	var _didIteratorError = false;
	var _iteratorError = undefined;

	try {
		for (var _iterator = (0, _getIterator3.default)(itemSummaryMap.values()), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
			var i = _step.value;

			itemSummaryList.push(i);
		}
	} catch (err) {
		_didIteratorError = true;
		_iteratorError = err;
	} finally {
		try {
			if (!_iteratorNormalCompletion && _iterator.return) {
				_iterator.return();
			}
		} finally {
			if (_didIteratorError) {
				throw _iteratorError;
			}
		}
	}

	var wsNameItem = 'itemSummary';
	workbook.SheetNames[12] = wsNameItem;
	var itemSummaryArray = getObjectForItemHSNReport(itemSummaryList, true);
	var itemSummaryWorksheet = XLSX.utils.aoa_to_sheet(itemSummaryArray);
	workbook.Sheets[wsNameItem] = itemSummaryWorksheet;
	var wsItemcolWidth = [{ wch: 22 }, { wch: 30 }, { wch: 25 }, { wch: 35 }, { wch: 35 }, { wch: 20 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	itemSummaryWorksheet['!cols'] = wsItemcolWidth;

	/// ///////////////////////// HSN Summary /////////////////////////////

	var hsnSummaryList = [];
	var hsnSummaryMap = new _map2.default();

	for (var i = 0; i < itemSummaryList.length; i++) {
		var txn = itemSummaryList[i];
		var hsnCode = txn.getItemHSN();
		hsnCode = hsnCode ? hsnCode.trim() : '';

		if (!hsnSummaryMap.has(hsnCode)) {
			var hsnSummaryObject = txn;
			hsnSummaryObject.setItemName('');
		} else {
			var hsnSummaryObject = hsnSummaryMap.get(hsnCode);
			hsnSummaryObject.setItemQuantity(hsnSummaryObject.getItemQuantity() + txn.getItemQuantity());
			hsnSummaryObject.setItemFreeQuantity(hsnSummaryObject.getItemFreeQuantity() + txn.getItemFreeQuantity());
			hsnSummaryObject.setItemTotalValue(hsnSummaryObject.getItemTotalValue() + txn.getItemTotalValue());
			hsnSummaryObject.setItemTaxableValue(hsnSummaryObject.getItemTaxableValue() + txn.getItemTaxableValue());
			hsnSummaryObject.setIGSTAmt(hsnSummaryObject.getIGSTAmt() + txn.getIGSTAmt());
			hsnSummaryObject.setCGSTAmt(hsnSummaryObject.getCGSTAmt() + txn.getCGSTAmt());
			hsnSummaryObject.setSGSTAmt(hsnSummaryObject.getSGSTAmt() + txn.getSGSTAmt());
			hsnSummaryObject.setCESSAmt(hsnSummaryObject.getCESSAmt() + txn.getCESSAmt());
			hsnSummaryObject.setAdditionalCessAmt(hsnSummaryObject.getAdditionalCessAmt() + txn.getAdditionalCessAmt());
		}
		hsnSummaryMap.set(hsnCode, hsnSummaryObject);
	}

	var _iteratorNormalCompletion2 = true;
	var _didIteratorError2 = false;
	var _iteratorError2 = undefined;

	try {
		for (var _iterator2 = (0, _getIterator3.default)(hsnSummaryMap.values()), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
			var i = _step2.value;

			hsnSummaryList.push(i);
		}
	} catch (err) {
		_didIteratorError2 = true;
		_iteratorError2 = err;
	} finally {
		try {
			if (!_iteratorNormalCompletion2 && _iterator2.return) {
				_iterator2.return();
			}
		} finally {
			if (_didIteratorError2) {
				throw _iteratorError2;
			}
		}
	}

	var wsNameItem = 'hsnsum';
	workbook.SheetNames[11] = wsNameItem;
	var hsnSummaryArray = getObjectForItemHSNReport(hsnSummaryList);
	var hsnSummaryWorksheet = XLSX.utils.aoa_to_sheet(hsnSummaryArray);
	workbook.Sheets[wsNameItem] = hsnSummaryWorksheet;
	var wsItemcolWidth = [{ wch: 22 }, { wch: 30 }, { wch: 25 }, { wch: 35 }, { wch: 35 }, { wch: 20 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	hsnSummaryWorksheet['!cols'] = wsItemcolWidth;

	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

populateTable();