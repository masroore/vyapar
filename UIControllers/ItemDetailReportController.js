var startDate;
var endDate;
var DateFormat = require('./../Constants/DateFormat.js');
$('#startDate, #endDate').datepicker({ dateFormat: DateFormat.format });
var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();

var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var ItemCache = require('./../Cache/ItemCache.js');
var itemCache = new ItemCache();
var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');
var ItemDetailReportHelper = require('./../UIControllers/ItemDetailReportHelper.js');
var itemDetailReportHelper = new ItemDetailReportHelper();

var printForSharePDF = false;

var listOfTransactions = [];

var populateTable = function populateTable() {
	startDateString = $('#startDate').val();
	endDateString = $('#endDate').val();
	var itemNameString = $('#itemNameForDetail').val();
	startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
	endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');
	var showOnlyActiveRows = $('#hideInactiveDates').prop('checked');
	CommonUtility.showLoader(function () {
		listOfTransactions = itemDetailReportHelper.getItemDetailReportObjectList(itemNameString, startDate, endDate, showOnlyActiveRows);
		if (listOfTransactions) {
			displayTransactionList(listOfTransactions);
		}
	});
};

var date = MyDate.getDate('d/m/y');
$('#startDate').val(MyDate.getFirstDateOfCurrentMonthString());
$('#endDate').val(date);

$('#startDate').change(function () {
	populateTable();
});

$('#endDate').change(function () {
	populateTable();
});

$('#hideInactiveDates').change(function () {
	populateTable();
});

$(function () {
	$('#itemNameForDetail').autocomplete($.extend({}, UIHelper.getAutocompleteDefaultOptions(itemCache.getListOfItems()), {
		select: function select(event, ui) {
			$('#itemNameForDetail').val(ui.item.value);
			$('#itemNameForDetail').blur();
			populateTable();
		}
	}));
});

$('#itemNameForDetail').on('change', function (event) {
	populateTable();
});

var itemDetailReportClusterize;

var displayTransactionList = function displayTransactionList(listOfTransactions) {
	var len = listOfTransactions.length;
	var rowData = [];
	var dynamicRow = '';
	dynamicRow += "<thead><tr><th width='20%'>Date</th><th width='20%' class='tableCellTextAlignRight'>Sale Quantity</th><th width='20%' class='tableCellTextAlignRight'>Purchase Quantity</th><th width='20%' class='tableCellTextAlignRight'>Adjustment Quantity</th><th width='20%' class='tableCellTextAlignRight'>Closing Quantity</th></tr></thead>";
	//
	var totalSaleQuantity = 0;
	var totalPurchaseQuantity = 0;
	for (var i = 0; i < len; i++) {
		var row = '';
		var txn = listOfTransactions[i];
		var date = MyDate.convertDateToStringForUI(txn.getDate());
		var saleQuantity = txn.getSaleQuantity();
		var purchaseQuantity = txn.getPurchaseQuantity();
		var adjustmentQuantity = txn.getAdjustmentQuantity();
		var closingQuantity = txn.getClosingQuantity();
		var saleFreeQuantity = txn.getSaleFreeQuantity();
		var purchaseFreeQuantity = txn.getPurchaseFreeQuantity();
		if (txn.isForwardedStock()) {
			row += "<tr class='currentRow'><td width='20%'>" + date + "</td><td width='20%' class='' style='border: none;'></td><td width='20%' class='tableCellTextAlignRight' style='border: none;'>Beginning stock</td><td width='20%' class='tableCellTextAlignRight'></td><td width='20%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(closingQuantity) + '</td></tr>';
		} else {
			row += "<tr class='currentRow'><td width='20%'>" + date + "</td><td width='20%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(saleQuantity) + MyDouble.quantityDoubleToStringWithSignExplicitly(saleFreeQuantity, true) + "</td><td width='20%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(purchaseQuantity) + MyDouble.quantityDoubleToStringWithSignExplicitly(purchaseFreeQuantity, true) + "</td><td width='20%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(adjustmentQuantity) + "</td><td width='20%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(closingQuantity) + '</td></tr>';
		}
		rowData.push(row);
	}
	var data = rowData;

	if ($('#itemDetailReportContainer').length === 0) {
		return;
	}
	if (itemDetailReportClusterize && itemDetailReportClusterize.destroy) {
		itemDetailReportClusterize.clear();
		itemDetailReportClusterize.destroy();
	}

	itemDetailReportClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollArea',
		contentId: 'contentArea',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});

	$('#tableHead').html(dynamicRow);
};

if (settingCache.getItemEnabled()) {
	$('#itemDetailPrintDiv').show();
} else {
	$('#itemDetailPrintDiv').hide();
}

function openPrintOptions(sharePDF) {
	printForSharePDF = sharePDF;
	printReport();
}

function closePrintOptions() {
	$('#customPrintDialog').addClass('hide');
	$('#customPrintDialog').dialog('close').dialog('destroy');
}

function printReport() {
	if (printForSharePDF) {
		pdfHandler.sharePdf(getHTMLTextForReport(), 'Report', false);
	} else {
		pdfHandler.printPdf(getHTMLTextForReport(), 'Report', false);
	}
	// closePrintOptions();
}

var getHTMLTextForReport = function getHTMLTextForReport() {
	var ItemDetailReportHTMLGenerator = require('./../ReportHTMLGenerator/ItemDetailReportHTMLGenerator.js');
	var itemDetailReportHTMLGenerator = new ItemDetailReportHTMLGenerator();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var nameString = $('#itemNameForDetail').val();

	var htmlText = itemDetailReportHTMLGenerator.getHTMLText(listOfTransactions, startDateString, endDateString, nameString);
	return htmlText;
};

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	// $('#frameDiv').load('');
	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable();
};

/// ///////////////////////////PDF/////////////////////////////////////
var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.ITEM_DETAIL_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

if (settingCache.getItemEnabled()) {
	$('#itemDetailsExcel').show();
} else {
	$('#itemDetailsExcel').hide();
}

function openExcelOptions() {
	$('#ExcelDialog').removeClass('hide');
	$('#ExcelDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closeExcelOptions() {
	$('#ExcelDialog').addClass('hide');
	$('#ExcelDialog').dialog('close').dialog('destroy');
}

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.ITEM_DETAIL_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
	excelHelper.saveExcel(fileName);
};

var prepareObjectForExcel = function prepareObjectForExcel(listOfTransactions) {
	var len = listOfTransactions.length;
	var rowObj = [];
	var tableHeadArray = [];
	var totalArray = [];
	tableHeadArray.push('Date');
	tableHeadArray.push('Sale Quantity');
	tableHeadArray.push('Purchase Quantity');
	tableHeadArray.push('Adjustment Quantity');
	tableHeadArray.push('Closing Quantity');
	rowObj.push(tableHeadArray);
	rowObj.push([]);

	for (var i = 0; i < len; i++) {
		var tempArray = [];
		var txn = listOfTransactions[i];
		var date = MyDate.convertDateToStringForUI(txn.getDate());
		var saleQuantity = txn.getSaleQuantity();
		var purchaseQuantity = txn.getPurchaseQuantity();
		var adjustmentQuantity = txn.getAdjustmentQuantity();
		var closingQuantity = txn.getClosingQuantity();
		var saleFreeQuantity = txn.getSaleFreeQuantity();
		var purchaseFreeQuantity = txn.getPurchaseFreeQuantity();

		tempArray.push(date);
		if (txn.isForwardedStock()) {
			tempArray.push('');
			tempArray.push('Beginning Stock');
			tempArray.push('');
		} else {
			tempArray.push(MyDouble.getQuantityWithDecimalWithoutColor(saleQuantity) + MyDouble.quantityDoubleToStringWithSignExplicitly(saleFreeQuantity, true));
			tempArray.push(MyDouble.getQuantityWithDecimalWithoutColor(purchaseQuantity) + MyDouble.quantityDoubleToStringWithSignExplicitly(purchaseFreeQuantity, true));
			tempArray.push(MyDouble.getQuantityWithDecimalWithoutColor(adjustmentQuantity));
		}
		tempArray.push(MyDouble.getQuantityWithDecimalWithoutColor(closingQuantity));
		rowObj.push(tempArray);
	}
	return rowObj;
};

function printExcel() {
	var XLSX = require('xlsx');
	var dataArray = prepareObjectForExcel(listOfTransactions);
	var worksheet = XLSX.utils.aoa_to_sheet(dataArray);

	var workbook = {
		'SheetNames': [],
		'Sheets': {}
	};

	var wsName = 'Item Detail Report';
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet;
	var wscolWidth = [{ wch: 40 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	worksheet['!cols'] = wscolWidth;
	console.log(workbook);
	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

populateTable();