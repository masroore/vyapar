var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* global jQuery */
module.exports = function shortcutControllerWrapper($) {
	var SettingCache = require('./../Cache/SettingCache.js');
	var TxnTypeConstant = require('../Constants/TxnTypeConstant');
	var CommonUtility = require('./../Utilities/CommonUtility.js');
	var settingCache = new SettingCache();
	var that = void 0;
	var shortcutController = {
		defaults: {
			$mountPoint: '#frameDiv',
			shortcuts: require('../Constants/Shortcuts')
			// fired : false
		},
		eventAlreadyAttached: false,
		init: function init(options) {
			if (!that) {
				that = this;
			}
			this.defaults = $.extend({}, this.defaults, options || {});
			this.defaults.$mountPoint = $(this.defaults.$mountPoint);
			this.render();
			this.attachEvents();
			if (settingCache.isDeliveryChallanEnabled()) {
				$('#action_deliveryChallan', this.defaults.$mountPoint).css('display', 'flex');
			} else {
				$('#action_deliveryChallan', this.defaults.$mountPoint).css('display', 'none');
			}
			if (settingCache.isEstimateEnabled()) {
				$('#estimate', this.defaults.$mountPoint).css('display', 'flex');
				$('#action_estimate', this.defaults.$mountPoint).css('display', 'flex');
			} else {
				$('#estimate', this.defaults.$mountPoint).css('display', 'none');
				$('#action_estimate', this.defaults.$mountPoint).css('display', 'none');
			}

			if (settingCache.isOrderFormEnabled()) {
				$('#order', this.defaults.$mountPoint).css('display', 'flex');
				$('#saleOrder', this.defaults.$mountPoint).css('display', 'flex');
				$('#purchaseOrder', this.defaults.$mountPoint).css('display', 'flex');
			} else {
				$('#order', this.defaults.$mountPoint).css('display', 'none');
				$('#saleOrder', this.defaults.$mountPoint).css('display', 'none');
				$('#purchaseOrder', this.defaults.$mountPoint).css('display', 'none');
			}

			if (settingCache.isExtraIncomeEnabled()) {
				$('#otherIncome', this.defaults.$mountPoint).css('display', 'flex');
				$('#otherIncomes', this.defaults.$mountPoint).css('display', 'flex');
			} else {
				$('#otherIncome', this.defaults.$mountPoint).css('display', 'none');
				$('#otherIncomes', this.defaults.$mountPoint).css('display', 'none');
			}
			if (settingCache.isCurrentCountryIndia()) {
				$('#referAndEarn', this.defaults.$mountPoint).css('display', 'flex');
			} else {
				$('#referAndEarn', this.defaults.$mountPoint).css('display', 'none');
			}

			if (settingCache.getItemEnabled()) {
				$('#actionShortcuts #addItemButton').show();
				$('#navigationShortcuts #items').show();
			} else {
				$('#actionShortcuts #addItemButton').hide();
				$('#navigationShortcuts #items').hide();
			}
		},
		attachEvents: function attachEvents() {
			var $mainShorcutDiv = $('#shortcutMainDiv', this.$mountPoint);
			$('#closeShortcut', $mainShorcutDiv).on('click', onCloseClick);

			$(document).off('keydown', '[role="dialog"], [role="document"], [role="presentation"], #modelContainer')
			// It should have the below code (commented code) not the above one.
			// But I am making the changes at the last moment of release so keeping it for safety
			/* .off(
   	'keydown',
   	'[role="dialog"], [role="document"], [role="presentation"], #modelContainer',
   	this.enableShortcut
   ) */
			.off('keydown', this.enableShortcut).off('keydown', this.enableOtherShortcut).off('click', '.shortcutKeys').on('keydown', '[role="dialog"], [role="document"], [role="presentation"], #modelContainer', this.enableShortcut).on('click', '.shortcutKeys', function () {
				var shortcutLabel = $(this).attr('data-pressKey');
				var shortcutKey = $(this).attr('data-shortcutKey');
				var keyCode = shortcutKey.charCodeAt(0);

				var eventObj = document.createEventObject ? document.createEventObject() : document.createEvent('Events');

				if (eventObj.initEvent) {
					eventObj.initEvent('keydown', true, true);
				}
				eventObj.key = shortcutKey;
				eventObj.which = keyCode;
				eventObj.altKey = shortcutLabel == 'ALT';
				eventObj.shiftKey = shortcutLabel == 'SHIFT';
				eventObj.ctrlKey = shortcutLabel == 'CTRL';

				document.body.dispatchEvent ? document.body.dispatchEvent(eventObj) : document.body.fireEvent('onkeydown', eventObj);
			});
			if (document && document.addEventListener) {
				document.addEventListener('keydown', this.enableOtherShortcut, true);
			}
		},
		render: function render() {
			this.defaults.$mountPoint.html(this.getMainTemplate());
		},
		getMainTemplate: function getMainTemplate() {
			return '\n      <div class="shortcutMainDiv">\n    <div class="shortcutHeading"><div style="flex: 40">SHORTCUTS</div>\n\n    </div>\n\n    <div class="shortcutBody">\n\n            <div class="shortcutDivAction">\n                <span style="float: left">Actions</span>\n                <span style="float: right"></span>\n            </div>\n            <div class="shortcutDivNavigation">\n                <span style="float: left">Navigation</span>\n                <span style="float: right"></span>\n            </div>\n            <div class="shortcutDivActivity">\n                <span style="float: left">Activities</span>\n                <span style="float: right"></span>\n            </div>\n\n    </div>\n\n    <div class="shortcutContent">\n        ' + this.getShortcutsHTML() + '\n    </div>\n</div>\n\n      ';
		},
		getShortcutsHTML: function getShortcutsHTML() {
			var $actionShortcuts = $('<div />');
			var $navigationShortcuts = $('<div />');
			var $activityShortcuts = $('<div />');
			var shortcuts = this.defaults.shortcuts;
			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;

			try {
				for (var _iterator = (0, _getIterator3.default)(shortcuts), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					var shortcut = _step.value;

					var labelName = shortcut['label'];
					var labelShortcutKey = shortcut['keyValue'];
					var labelShortcut = '';
					if (shortcut['altKey']) {
						labelShortcut = 'ALT';
						var html = " <div id='" + shortcut['id'] + "' data-pressKey='" + labelShortcut + "' data-shortcutKey='" + labelShortcutKey + "' class='shortcutKeys' style='display: flex; flex-flow: row'> <div class='shortcutName' >" + labelName + "</div> <div style='float: right'> <button class='shortcutButton actionShortcutButton shortcutButtonMasterKey'>" + labelShortcut + "</button><button class='shortcutButton actionShortcutButton shortcutButtonKey'>" + labelShortcutKey + '</button></div> </div>';
						$actionShortcuts.append(html);
					}
					if (shortcut['shiftKey']) {
						labelShortcut = 'SHIFT';
						html = " <div id='" + shortcut['id'] + "' data-pressKey='" + labelShortcut + "' data-shortcutKey='" + labelShortcutKey + "' class='shortcutKeys' style='display: flex; flex-flow: row'> <div class='shortcutName'>" + labelName + "</div> <div style='float: right'> <button class='shortcutButton navigationShortcutButton shortcutButtonMasterKey'>" + labelShortcut + "</button><button class='shortcutButton navigationShortcutButton shortcutButtonKey'>" + labelShortcutKey + '</button></div> </div>';
						$navigationShortcuts.append(html);
					}
					if (shortcut['ctrlKey']) {
						labelShortcut = 'CTRL';
						html = " <div data-pressKey='" + labelShortcut + "' data-shortcutKey='" + labelShortcutKey + "' class='shortcutKeys activity' style='display: flex; flex-flow: row'> <div class='shortcutName'>" + labelName + "</div> <div style='float: right'> <button class='shortcutButton activityShortcutButton shortcutButtonMasterKey'>" + labelShortcut + "</button><button class='shortcutButton activityShortcutButton shortcutButtonKey'>" + labelShortcutKey + '</button></div> </div>';
						$activityShortcuts.append(html);
					}
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}

			return '\n            <div class="shortcutContentAction" id="actionShortcuts">' + $actionShortcuts.html() + ' <br></div>\n            <div class="shortcutContentNavigation"  id="navigationShortcuts">' + $navigationShortcuts.html() + ' <br></div>\n            <div class="shortcutContentActivity" id="activityShortcuts">' + $activityShortcuts.html() + ' <br></div>\n          ';
		},
		actionNavigate: function actionNavigate(fileName) {
			if (!canCloseDialogue || canCloseDialogue()) {
				$('#defaultPage').hide();
				txnIdReqGlobal = '';
				valueGlobal = fileName;
				try {
					unmountReactComponent(document.querySelector('#frameDiv'));
					unmountReactComponent(document.querySelector('#salePurchaseContainer'));
				} catch (ex) {}
				$('#frameDiv').empty();

				if (fileName == 'NewBankAccount.html') {
					document.dispatchEvent(new CustomEvent('defaultpage-shortcut-triggered'));
					accountNameGlobal = '';
					$('#frameDiv').load('NewBankAccount.html');
				} else if (fileName == 'NewContact.html') {
					document.dispatchEvent(new CustomEvent('defaultpage-shortcut-triggered'));
					window.userNameGlobal = '';
					$('#frameDiv').load('NewContact.html');
				} else if (fileName == 'NewItems.html') {
					document.dispatchEvent(new CustomEvent('defaultpage-shortcut-triggered'));
					window.itemNameGlobal = '';
					$('#frameDiv').load('NewItems.html');
				} else {
					document.dispatchEvent(new CustomEvent('defaultpageother-shortcut-triggered'));
					setTimeout(function () {
						var Component = require('../UIComponent/jsx/SalePurchaseContainer').default;
						var mountComponent = require('../UIComponent/jsx/MountComponent').default;
						mountComponent(Component, document.getElementById('salePurchaseContainer'), {
							txnType: TxnTypeConstant.TXN_TYPE_SALE
						});
					});
				}
				$('#modelContainer').css('display', 'block');
			}
		},
		enableOtherShortcut: function enableOtherShortcut(zEvent) {
			if (zEvent.shiftKey && !zEvent.ctrlKey && (zEvent.which > 47 && zEvent.which < 58 || zEvent.which > 64 && zEvent.which < 91)) {
				var codeOfKey = zEvent.which;
				zEvent.which = '';
				var activeElement = document.activeElement;
				var inputs = ['input', 'textarea'];

				if (activeElement && inputs.indexOf(activeElement.tagName.toLowerCase()) == -1) {
					zEvent.stopPropagation();
					zEvent.preventDefault();
					$('.quickNavButton').tooltipster('close');
					var _iteratorNormalCompletion2 = true;
					var _didIteratorError2 = false;
					var _iteratorError2 = undefined;

					try {
						for (var _iterator2 = (0, _getIterator3.default)(that.defaults.shortcuts), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
							var shortcutItem = _step2.value;

							if (shortcutItem['keyValue'].charCodeAt(0) == codeOfKey && shortcutItem['shiftKey']) {
								if (shortcutItem['keyValue'] == '3') {
									var _require = require('electron'),
									    shell = _require.shell;

									shell.openItem('calc');
								} else {
									if (shortcutItem['keyValue'] == 'H') {
										document.dispatchEvent(new CustomEvent('defaultpage-shortcut-triggered'));
										var FTUHelper = require('../Utilities/FTUHelper');
										if (!FTUHelper.isFirstSaleTxnCreated()) {
											var component = require('../UIComponent/jsx/ftu').default;
											CommonUtility.MountDefaultPage(component);
										} else {
											var _component = require('../UIComponent/jsx/Dashboard/Dashboard').default;
											CommonUtility.MountDefaultPage(_component);
										}
									} else if (shortcutItem['keyValue'] == 'S') {
										if (settingCache.isEstimateEnabled()) {
											if (!canCloseDialogue || canCloseDialogue()) {
												document.dispatchEvent(new CustomEvent('defaultpage-shortcut-triggered'));
												unmountReactComponent(document.querySelector('#salePurchaseContainer'));
												CommonUtility.hideFramediv();
												$('#defaultPage').load(shortcutItem['fileName']);
											}
										}
									} else if (shortcutItem['keyValue'] == 'O') {
										if (settingCache.isOrderFormEnabled()) {
											if (!canCloseDialogue || canCloseDialogue()) {
												document.dispatchEvent(new CustomEvent('defaultpage-shortcut-triggered'));
												$('#defaultPage').show();
												unmountReactComponent(document.querySelector('#salePurchaseContainer'));
												var OrdersContainer = require('../UIComponent/jsx/OrdersContainer').default;
												var mountComponent = require('../UIComponent/jsx/MountComponent').default;
												mountComponent(OrdersContainer, document.getElementById('defaultPage'));
												$('#modelContainer').css({ display: 'none' });
											}
										}
									} else if (shortcutItem['keyValue'] === 'I') {
										// load items
										if (settingCache.getItemEnabled()) {
											if (!canCloseDialogue || canCloseDialogue()) {
												$('#defaultPage').show();
												document.dispatchEvent(new CustomEvent('defaultpage-shortcut-triggered'));
												unmountReactComponent(document.querySelector('#salePurchaseContainer'));
												var ItemsContainer = require('../UIComponent/jsx/ItemsContainer').default;
												var _mountComponent = require('../UIComponent/jsx/MountComponent').default;
												_mountComponent(ItemsContainer, document.getElementById('defaultPage'));
												$('#modelContainer').css({ display: 'none' });
											}
										}
									} else if (shortcutItem['keyValue'] === 'P') {
										// load items
										if (!canCloseDialogue || canCloseDialogue()) {
											$('#defaultPage').show();
											document.dispatchEvent(new CustomEvent('defaultpage-shortcut-triggered'));
											unmountReactComponent(document.querySelector('#salePurchaseContainer'));
											var PartiesContainer = require('../UIComponent/jsx/PartiesContainer').default;
											var _mountComponent2 = require('../UIComponent/jsx/MountComponent').default;
											_mountComponent2(PartiesContainer, document.getElementById('defaultPage'));
											$('#modelContainer').css({ display: 'none' });
										}
									} else if (shortcutItem['keyValue'] == 'E') {
										if (!canCloseDialogue || canCloseDialogue()) {
											$('#defaultPage').show();
											document.dispatchEvent(new CustomEvent('defaultpage-shortcut-triggered'));
											var ExpenseContainer = require('../UIComponent/jsx/ExpenseContainer').default;
											var mount = require('../UIComponent/jsx/MountComponent').default;
											mount(ExpenseContainer, document.querySelector('#defaultPage'));
											$('#modelContainer').css({ display: 'none' });
										}
									} else if (shortcutItem['keyValue'] == 'B') {
										if (!canCloseDialogue || canCloseDialogue()) {
											$('#defaultPage').show();
											document.dispatchEvent(new CustomEvent('defaultpage-shortcut-triggered'));
											var BankAccountsContainer = require('../UIComponent/jsx/BankAccountsContainer').default;
											var _mount = require('../UIComponent/jsx/MountComponent').default;
											_mount(BankAccountsContainer, document.querySelector('#defaultPage'));
											$('#modelContainer').css({ display: 'none' });
										}
									} else if (shortcutItem['keyValue'] == 'N') {
										if (!canCloseDialogue || canCloseDialogue()) {
											$('#defaultPage').show();
											document.dispatchEvent(new CustomEvent('defaultpage-shortcut-triggered'));
											var OtherIncomeContainer = require('../UIComponent/jsx/OtherIncomeContainer').default;
											var _mount2 = require('../UIComponent/jsx/MountComponent').default;
											_mount2(OtherIncomeContainer, document.querySelector('#defaultPage'));
											$('#modelContainer').css({ display: 'none' });
										}
									} else if (shortcutItem['keyValue'] == 'N') {
										if (settingCache.isExtraIncomeEnabled()) {
											if (!canCloseDialogue || canCloseDialogue()) {
												document.dispatchEvent(new CustomEvent('defaultpage-shortcut-triggered'));
												unmountReactComponent(document.querySelector('#salePurchaseContainer'));
												CommonUtility.hideFramediv();
												$('#defaultPage').load(shortcutItem['fileName']);
											}
										}
									} else if (shortcutItem['keyValue'] == 'C') {
										if (!canCloseDialogue || canCloseDialogue()) {
											$('#defaultPage').show();
											document.dispatchEvent(new CustomEvent('defaultpage-shortcut-triggered'));
											unmountReactComponent(document.querySelector('#salePurchaseContainer'));
											var CashInHandContainer = require('../UIComponent/jsx/CashInHandContainer').default;
											var _mount3 = require('../UIComponent/jsx/MountComponent').default;
											_mount3(CashInHandContainer, document.querySelector('#defaultPage'));
											$('#modelContainer').css({ display: 'none' });
										}
									} else if (shortcutItem['keyValue'] == 'U') {
										if (!canCloseDialogue || canCloseDialogue()) {
											$('#defaultPage').show();
											document.dispatchEvent(new CustomEvent('defaultpage-shortcut-triggered'));
											unmountReactComponent(document.querySelector('#salePurchaseContainer'));
											var ChequesContainer = require('../UIComponent/jsx/ChequesContainer').default;
											var _mount4 = require('../UIComponent/jsx/MountComponent').default;
											_mount4(ChequesContainer, document.querySelector('#defaultPage'));
											$('#modelContainer').css({ display: 'none' });
										}
									} else if (shortcutItem['keyValue'] == '4') {
										if (settingCache.isCurrentCountryIndia()) {
											if (!canCloseDialogue || canCloseDialogue()) {
												document.dispatchEvent(new CustomEvent('defaultpage-shortcut-triggered'));
												unmountReactComponent(document.querySelector('#salePurchaseContainer'));
												CommonUtility.hideFramediv();
												$('#defaultPage').load(shortcutItem['fileName']);
											}
										}
									} else if (shortcutItem['keyValue'] == '1') {
										if (!canCloseDialogue || canCloseDialogue()) {
											document.dispatchEvent(new CustomEvent('defaultpage-shortcut-triggered'));
											var Settings = require('../UIComponent/jsx/Settings/Settings').default;
											var _mountComponent3 = require('../UIComponent/jsx/MountComponent').default;
											unmountReactComponent(document.querySelector('#salePurchaseContainer'));
											_mountComponent3(Settings, document.querySelector('#frameDiv'));
										}
									} else {
										if (!canCloseDialogue || canCloseDialogue()) {
											document.dispatchEvent(new CustomEvent('defaultpage-shortcut-triggered'));
											CommonUtility.loadDefaultPage(shortcutItem['fileName']);
										}
									}
								}
								break;
							}
						}
					} catch (err) {
						_didIteratorError2 = true;
						_iteratorError2 = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion2 && _iterator2.return) {
								_iterator2.return();
							}
						} finally {
							if (_didIteratorError2) {
								throw _iteratorError2;
							}
						}
					}
				}
			} else if (zEvent.altKey && (zEvent.which > 47 && zEvent.which < 58 || zEvent.which > 64 && zEvent.which < 91)) {
				(function () {
					codeOfKey = zEvent.which;
					zEvent.which = '';
					$('.quickNavButton').tooltipster('close');
					var mountComponent = require('../UIComponent/jsx/MountComponent').default;
					var mountPoint = document.getElementById('salePurchaseContainer');
					var _iteratorNormalCompletion3 = true;
					var _didIteratorError3 = false;
					var _iteratorError3 = undefined;

					try {
						for (var _iterator3 = (0, _getIterator3.default)(that.defaults.shortcuts), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
							var _shortcutItem = _step3.value;

							if (_shortcutItem['keyValue'].charCodeAt(0) == codeOfKey && _shortcutItem['altKey']) {
								if (_shortcutItem['keyValue'] == 'S') {
									if (!canCloseDialogue || canCloseDialogue()) {
										document.dispatchEvent(new CustomEvent('defaultpageother-shortcut-triggered'));
										setTimeout(function () {
											var Component = require('../UIComponent/jsx/SalePurchaseContainer').default;
											mountComponent(Component, mountPoint, {
												txnType: TxnTypeConstant.TXN_TYPE_SALE
											});
										});
									}
								} else if (_shortcutItem['keyValue'] == 'P') {
									if (!canCloseDialogue || canCloseDialogue()) {
										document.dispatchEvent(new CustomEvent('defaultpageother-shortcut-triggered'));
										setTimeout(function () {
											var Component = require('../UIComponent/jsx/SalePurchaseContainer').default;
											mountComponent(Component, mountPoint, {
												txnType: TxnTypeConstant.TXN_TYPE_PURCHASE
											});
										});
									}
								} else if (_shortcutItem['keyValue'] == 'I') {
									if (!canCloseDialogue || canCloseDialogue()) {
										document.dispatchEvent(new CustomEvent('defaultpageother-shortcut-triggered'));
										setTimeout(function () {
											var Component = require('../UIComponent/jsx/SalePurchaseContainer/PaymentTransaction').default;
											mountComponent(Component, mountPoint, {
												txnType: TxnTypeConstant.TXN_TYPE_CASHIN
											});
										});
									}
								} else if (_shortcutItem['keyValue'] == 'O') {
									if (!canCloseDialogue || canCloseDialogue()) {
										document.dispatchEvent(new CustomEvent('defaultpageother-shortcut-triggered'));
										setTimeout(function () {
											var Component = require('../UIComponent/jsx/SalePurchaseContainer/PaymentTransaction').default;
											mountComponent(Component, mountPoint, {
												txnType: TxnTypeConstant.TXN_TYPE_CASHOUT
											});
										});
									}
								} else if (_shortcutItem['keyValue'] == 'E') {
									if (!canCloseDialogue || canCloseDialogue()) {
										document.dispatchEvent(new CustomEvent('defaultpageother-shortcut-triggered'));
										setTimeout(function () {
											var Component = require('../UIComponent/jsx/SalePurchaseContainer').default;
											mountComponent(Component, mountPoint, {
												txnType: TxnTypeConstant.TXN_TYPE_EXPENSE
											});
										});
									}
								} else if (_shortcutItem['keyValue'] == 'M') {
									if (settingCache.isEstimateEnabled()) {
										if (!canCloseDialogue || canCloseDialogue()) {
											document.dispatchEvent(new CustomEvent('defaultpageother-shortcut-triggered'));
											setTimeout(function () {
												var Component = require('../UIComponent/jsx/SalePurchaseContainer').default;
												mountComponent(Component, mountPoint, {
													txnType: TxnTypeConstant.TXN_TYPE_ESTIMATE
												});
											});
										}
									}
								} else if (_shortcutItem['keyValue'] == 'F') {
									if (settingCache.isOrderFormEnabled()) {
										if (!canCloseDialogue || canCloseDialogue()) {
											document.dispatchEvent(new CustomEvent('defaultpageother-shortcut-triggered'));
											setTimeout(function () {
												var Component = require('../UIComponent/jsx/SalePurchaseContainer').default;
												mountComponent(Component, mountPoint, {
													txnType: TxnTypeConstant.TXN_TYPE_SALE_ORDER
												});
											});
										}
									}
								} else if (_shortcutItem['keyValue'] == 'G') {
									if (settingCache.isOrderFormEnabled()) {
										if (!canCloseDialogue || canCloseDialogue()) {
											document.dispatchEvent(new CustomEvent('defaultpageother-shortcut-triggered'));
											setTimeout(function () {
												var Component = require('../UIComponent/jsx/SalePurchaseContainer').default;
												mountComponent(Component, mountPoint, {
													txnType: TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER
												});
											});
										}
									}
								} else if (_shortcutItem['keyValue'] == 'Q') {
									if (settingCache.isExtraIncomeEnabled()) {
										if (!canCloseDialogue || canCloseDialogue()) {
											document.dispatchEvent(new CustomEvent('defaultpageother-shortcut-triggered'));
											setTimeout(function () {
												var Component = require('../UIComponent/jsx/SalePurchaseContainer').default;
												mountComponent(Component, mountPoint, {
													txnType: TxnTypeConstant.TXN_TYPE_OTHER_INCOME
												});
											});
										}
									}
								} else if (_shortcutItem['keyValue'] == 'D') {
									if (settingCache.isDeliveryChallanEnabled()) {
										if (!canCloseDialogue || canCloseDialogue()) {
											document.dispatchEvent(new CustomEvent('defaultpageother-shortcut-triggered'));
											setTimeout(function () {
												var Component = require('../UIComponent/jsx/SalePurchaseContainer').default;
												mountComponent(Component, mountPoint, {
													txnType: TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN
												});
											});
										}
									}
								} else if (_shortcutItem['keyValue'] == 'R') {
									if (!canCloseDialogue || canCloseDialogue()) {
										document.dispatchEvent(new CustomEvent('defaultpageother-shortcut-triggered'));
										setTimeout(function () {
											var Component = require('../UIComponent/jsx/SalePurchaseContainer').default;
											mountComponent(Component, mountPoint, {
												txnType: TxnTypeConstant.TXN_TYPE_SALE_RETURN
											});
										});
									}
								} else if (_shortcutItem['keyValue'] == 'L') {
									if (!canCloseDialogue || canCloseDialogue()) {
										document.dispatchEvent(new CustomEvent('defaultpageother-shortcut-triggered'));
										setTimeout(function () {
											var Component = require('../UIComponent/jsx/SalePurchaseContainer').default;
											mountComponent(Component, mountPoint, {
												txnType: TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN
											});
										});
									}
								} else if (_shortcutItem['keyValue'] == 'A') {
									if (settingCache.getItemEnabled()) {
										that.actionNavigate(_shortcutItem['fileName']);
									}
								} else {
									that.actionNavigate(_shortcutItem['fileName']);
								}
								break;
							}
						}
						// }
					} catch (err) {
						_didIteratorError3 = true;
						_iteratorError3 = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion3 && _iterator3.return) {
								_iterator3.return();
							}
						} finally {
							if (_didIteratorError3) {
								throw _iteratorError3;
							}
						}
					}
				})();
			} else if (zEvent.ctrlKey && zEvent.which == 13) {
				var btn = $('[data-shortcut="CTRL_ENTER"]:visible')[0];
				if (btn) {
					// Checking if it is really present on UI. Visible just check if element is present in dom and visible.
					// It does not handle the part where some element is covering it.
					// I will move this logic to some utility class later so that other people will be able to use this logic
					// In most of the true scenario, currentElementOnPosition.contains should give the result output
					var positionForButton = btn.getBoundingClientRect && btn.getBoundingClientRect();
					if (positionForButton) {
						var currentElementOnPosition = document.elementFromPoint(positionForButton.left, positionForButton.top);
						if (currentElementOnPosition && (btn.isSameNode && btn.isSameNode(currentElementOnPosition) || currentElementOnPosition.contains && currentElementOnPosition.contains(btn))) {
							zEvent.stopPropagation();
							zEvent.preventDefault();
							btn.focus();
							btn.click();
						}
					}
				}
			}
		},
		enableShortcut: function enableShortcut(zEvent) {
			if (zEvent.ctrlKey && zEvent.keyCode === 83) {
				zEvent.stopPropagation();
				zEvent.preventDefault();

				zEvent.keyCode = '';
				$(zEvent.currentTarget).find('[data-shortcut="CTRL_S"]:visible:enabled').trigger('click');
			} else if (zEvent.ctrlKey && zEvent.keyCode === 78) {
				zEvent.stopPropagation();
				zEvent.preventDefault();
				zEvent.keyCode = '';
				$(zEvent.currentTarget).find('[data-shortcut="CTRL_N"]:visible:enabled').trigger('click');
			} else if (zEvent.ctrlKey && zEvent.keyCode === 80) {
				zEvent.stopPropagation();
				zEvent.preventDefault();
				zEvent.keyCode = '';
				$(zEvent.currentTarget).find('[data-shortcut="CTRL_P"]:visible:enabled').trigger('click');
			} else if (zEvent.ctrlKey && zEvent.keyCode === 82) {
				zEvent.stopPropagation();
				zEvent.preventDefault();
				zEvent.keyCode = '';
				$(zEvent.currentTarget).find('[data-shortcut="CTRL_R"]:visible:enabled').trigger('click');
			}

			//  }
		}
	};

	return shortcutController.init.bind(shortcutController);
}(jQuery);