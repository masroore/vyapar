var DataLoader = require('./../DBManager/DataLoader.js');
var dataloader = new DataLoader();
var listOfAccountFromDB = dataloader.LoadListOfAccountAdjustment();
var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var curObj;
$.each(listOfAccountFromDB, function (key, value) {
	if (value['adjId'] == accId) {
		curObj = value;
		return false;
	}
});
// var curObj = listOfAccountFromDB[accId - 1];
var entryType = curObj.adjType;
switch (Number(entryType)) {
	case TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD:
		document.getElementById('entryType').value = StringConstant.bankAddAdjustmentString;
		break;
	case TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE:
		document.getElementById('entryType').value = StringConstant.bankReduceAdjustmentString;
		break;
	case TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD_WITHOUT_CASH:
		document.getElementById('entryType').value = StringConstant.bankAddAdjustmentWithoutCashString;
		break;
	case TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE_WITHOUT_CASH:
		document.getElementById('entryType').value = StringConstant.bankReduceAdjustmentWithoutCashString;
		break;
}
document.getElementById('amount').value = curObj.adjAmount;
// var MyDate = reqire('./../Utilities/MyDate.js');
var date = curObj.adjDate;
// date = date.split(" ")[0];
date = MyDate.getDate('d/m/y', date);
console.log(date);
document.getElementById('datepicker').value = date;
document.getElementById('description').value = curObj.adjDescription;