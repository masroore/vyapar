var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var startDate;
var endDate;
var DateFormat = require('./../Constants/DateFormat.js');
// $( "#startDate, #endDate" ).datepicker({ dateFormat: DateFormat.format });
var MyString = require('./../Utilities/MyString.js');
var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();
var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var TaxCodeCache = require('./../Cache/TaxCodeCache.js');
var taxCodeCache = new TaxCodeCache();
var TaxCodeConstants = require('./../Constants/TaxCodeConstants.js');
var GSTRReportHelper = require('./../UIControllers/GSTRReportHelper.js');
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');
var FirmCache = require('./../Cache/FirmCache.js');
var firmCache = new FirmCache();

var printForSharePDF = false;
var listOfTransactions = [];
var showOTHER = false;
var showAdditionalCess = false;
var showStateSpecificCESS = false;

function showGSTRSection(evt, divId) {
	// Declare all variables
	var i, tabcontent, tablinks;
	// Get all elements with class="tabcontent" and hide them
	$('.gstrTabContent').hide();
	// Get all elements with class="tablinks" and remove the class "bottomLine"
	$('.bottomLine').removeClass('bottomLine');
	// Show the current tab, and add an "active" class to the link that opened the tab
	$('#' + divId).show();
	evt.currentTarget.className += ' bottomLine';
}

$('.gstrTabContent').hide();
$('#gstrSaleSection').show();

$('.monthYearPicker').datepicker({
	changeMonth: true,
	changeYear: true,
	showButtonPanel: true,
	dateFormat: 'mm/yy'
}).focus(function () {
	var thisCalendar = $(this);
	$('.ui-datepicker-calendar').detach();
	$('.ui-datepicker-close').click(function () {
		var month = $('#ui-datepicker-div .ui-datepicker-month :selected').val();
		var year = $('#ui-datepicker-div .ui-datepicker-year :selected').val();
		thisCalendar.datepicker('setDate', new Date(year, month));
		populateTable();
	});
});

var populateTable = function populateTable() {
	settingCache = new SettingCache();
	taxCodeCache = new TaxCodeCache();
	var startDateString = $('#startDate').val();
	startDateString = '01/' + startDateString;
	var endDateString = $('#endDate').val();
	endDateString = '01/' + endDateString;
	startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
	endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');
	endDate = new Date(endDate.getFullYear(), endDate.getMonth() + 1, 0);
	var considerNonTaxAsExemptedCheck = $('#considerNonTaxAsExempted').prop('checked');
	var firmId = Number($('#firmFilterOptions').val());
	var GSTR1ReportHelper = require('./../UIControllers/GSTR1ReportHelper.js');
	var gSTR1ReportHelper = new GSTR1ReportHelper();

	CommonUtility.showLoader(function () {
		listOfTransactions = gSTR1ReportHelper.getTxnListBasedOnDate(startDate, endDate, firmId, considerNonTaxAsExemptedCheck);
		showOTHER = gSTR1ReportHelper.showOTHER;
		showStateSpecificCESS = gSTR1ReportHelper.showStateSpecificCESS;
		showAdditionalCess = gSTR1ReportHelper.showAdditionalCess;
		displayTransactionList(listOfTransactions);
	});
};

var date = MyDate.getDate('m/y');
$('#startDate').val(date);
$('#endDate').val(date);

if (settingCache.getMultipleFirmEnabled()) {
	var optionTemp = document.createElement('option');
	var firmList = firmCache.getFirmList();
	for (i in firmList) {
		var option = optionTemp.cloneNode();
		option.text = firmList[i].getFirmName();
		option.value = firmList[i].getFirmId();
		$('#firmFilterOptions').append(option);
	}
	$('#firmChooser').show();
} else {
	$('#firmChooser').hide();
}

$('#firmFilterOptions').on('selectmenuchange', function (event) {
	populateTable();
});

$('#startDate').change(function () {
	populateTable();
});

$('#considerNonTaxAsExempted').change(function () {
	populateTable();
});

$('#endDate').change(function () {
	populateTable();
});

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	// $('#frameDiv').load('');
	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable();
};

var gstr1ReportSaleClusterize;
var gstr1ReportSaleReturnClusterize;

var displayTransactionList = function displayTransactionList(listOfTransactions) {
	var len = listOfTransactions.length;
	var rowData1 = [];
	var rowData2 = [];
	var dynamicRow1 = '';
	var dynamicRow2 = '';
	var amtcol = 7;
	if (!showOTHER) {
		amtcol--;
	}
	if (!showAdditionalCess) {
		amtcol--;
	}

	if (!showStateSpecificCESS) {
		amtcol--;
	}

	var className = '';
	if (!$('#sortIcon').attr('class')) {
		className = 'ui-selectmenu-icon ui-icon ui-icon-triangle-1-s';
	} else {
		className = $('#sortIcon').attr('class');
	}

	dynamicRow1 += "<thead><tr style='background: #F7F7F7;'><th style='width: 150px;'></th><th colspan='4' style='width:600px; text-align: center;'>Invoice details</th><th align='center' style='width: 150px;' class='tableCellTextAlignRight'>Tax Rate</th><th align='center' style='width: 150px;' class='tableCellTextAlignRight'>Cess Rate</th><th align='center' style='width: 150px;' class='tableCellTextAlignRight'>Taxable value</th><th align='center' style='width:" + amtcol * 150 + "px; text-align: center;' colspan='" + amtcol + "'>Amount</th><th align='center' style='width: 150px;' class='tableCellTextAlignRight'>Place of Supply(Name of State)</th>";
	dynamicRow1 += '</tr><tr width=\'\'><th align=\'left\' style=\'width:150px;\'>GSTIN/UIN</th><th align=\'left\' style=\'width:150px;\'>Party Name</th><th style=\'width:150px;\' class=\'tableCellTextAlignRight\' id=\'invoiceColId\' style=\'cursor:pointer;\'>Invoice No.<span id=\'sortIcon\' class=\'' + className + '\'></span></th><th  style=\'width:150px;\' class=\'tableCellTextAlignRight\'>Date</th><th style=\'width:150px;\' class=\'tableCellTextAlignRight\'>Value</th><th align=\'left\' style=\'width:150px;\'></th><th align=\'right\' style=\'width:150px;\'></th><th align=\'right\' style=\'width:150px;\'></th><th style=\'width:150px;\' class=\'tableCellTextAlignRight\'>Integrated Tax</th><th style=\'width:150px;\' class=\'tableCellTextAlignRight\'>Central Tax</th><th style=\'width:150px;\' class=\'tableCellTextAlignRight\'>State/UT Tax</th><th style=\'width:150px;\' class=\'tableCellTextAlignRight\'>Cess</th>';

	dynamicRow2 += "<thead><tr style='background: #F7F7F7;'><th style='width: 150px;'></th><th colspan='6' style='width:900px; text-align: center;'>Cr. Note details</th><th align='center' style='width: 150px;' class='tableCellTextAlignRight'>Tax Rate</th><th align='center' style='width: 150px;' class='tableCellTextAlignRight'>Cess Rate</th><th align='center' style='width: 150px;' class='tableCellTextAlignRight'>Taxable value</th><th align='center' style='width:" + amtcol * 150 + "px; text-align: center;' colspan='" + amtcol + "'>Amount</th><th align='center' style='width: 150px;' class='tableCellTextAlignRight'>Place of Supply(Name of State)</th>";
	dynamicRow2 += '</tr><tr width=\'\'><th align=\'left\' style=\'width:150px;\'>GSTIN/UIN</th><th align=\'left\' style=\'width:150px;\'>Party Name</th><th  style=\'width:150px;\' class=\'tableCellTextAlignRight\'>Invoice No.</th><th  style=\'width:150px;\' class=\'tableCellTextAlignRight\'>Invoice Date</th><th style=\'width:150px;\' class=\'tableCellTextAlignRight\' id=\'invoiceColId\' style=\'cursor:pointer;\'>Note No.<span id=\'sortIcon\' class=\'' + className + '\'></span></th><th  style=\'width:150px;\' class=\'tableCellTextAlignRight\'>Note Date</th><th style=\'width:150px;\' class=\'tableCellTextAlignRight\'>Value</th><th align=\'left\' style=\'width:150px;\'></th><th align=\'right\' style=\'width:150px;\'></th><th align=\'right\' style=\'width:150px;\'></th><th style=\'width:150px;\' class=\'tableCellTextAlignRight\'>Integrated Tax</th><th style=\'width:150px;\' class=\'tableCellTextAlignRight\'>Central Tax</th><th style=\'width:150px;\' class=\'tableCellTextAlignRight\'>State/UT Tax</th><th style=\'width:150px;\' class=\'tableCellTextAlignRight\'>Cess</th>';

	var invisibleRow1 = "<th style='width: 150px; height:1px;'></th><th colspan='4' style='width:600px; text-align: center;height:1px;'></th><th align='center' style='width: 150px;height:1px;' class='tableCellTextAlignRight'></th><th align='center' style='width: 150px;height:1px;' class='tableCellTextAlignRight'></th><th align='center' style='width: 150px;height:1px;' class='tableCellTextAlignRight'></th><th align='center' style='width:" + amtcol * 150 + "px; text-align: center;height:1px;' colspan='" + amtcol + "'></th><th align='center' style='width: 150px;height:1px;' class='tableCellTextAlignRight'></th>";
	var invisibleRow2 = "<th style='width: 150px; height:1px;'></th><th colspan='6' style='width:900px; text-align: center;height:1px;'></th><th align='center' style='width: 150px;height:1px;' class='tableCellTextAlignRight'></th><th align='center' style='width: 150px;height:1px;' class='tableCellTextAlignRight'></th><th align='center' style='width: 150px;height:1px;' class='tableCellTextAlignRight'></th><th align='center' style='width:" + amtcol * 150 + "px; text-align: center;height:1px;' colspan='" + amtcol + "'></th><th align='center' style='width: 150px;height:1px;' class='tableCellTextAlignRight'></th>";

	if (showOTHER) {
		dynamicRow1 += "<th width='150px' class='tableCellTextAlignRight'>Other</th>";
		dynamicRow2 += "<th width='150px' class='tableCellTextAlignRight'>Other</th>";
	}

	if (showStateSpecificCESS) {
		dynamicRow1 += "<th width='150px' class='tableCellTextAlignRight'>Flood CESS</th>";
		dynamicRow2 += "<th width='150px' class='tableCellTextAlignRight'>Flood CESS</th>";
	}
	if (showAdditionalCess) {
		dynamicRow1 += "<th width='150px' class='tableCellTextAlignRight'>Additional Cess</th>";
		dynamicRow2 += "<th width='150px' class='tableCellTextAlignRight'>Additional Cess</th>";
	}
	dynamicRow1 += "<th width='150px' class='tableCellTextAlignRight'></th></tr></thead>";
	dynamicRow2 += "<th width='150px' class='tableCellTextAlignRight'></th></tr></thead>";

	var invoiceNoMapSale = new _map2.default();
	var totalTaxableValueSale = 0;
	var totalInvoiceValueSale = 0;
	var totalIGSTAmountSale = 0;
	var totalCGSTAmountSale = 0;
	var totalSGSTAmountSale = 0;
	var totalCESSAmountSale = 0;
	var totalOTHERAmountSale = 0;
	var totalStateSpecificCESSAmountSale = 0;
	var totalAdditionalCessAmountSale = 0;

	var invoiceNoMapSaleReturn = new _map2.default();
	var totalTaxableValueSaleReturn = 0;
	var totalInvoiceValueSaleReturn = 0;
	var totalIGSTAmountSaleReturn = 0;
	var totalCGSTAmountSaleReturn = 0;
	var totalSGSTAmountSaleReturn = 0;
	var totalCESSAmountSaleReturn = 0;
	var totalOTHERAmountSaleReturn = 0;
	var totalStateSpecificCESSAmountSaleReturn = 0;
	var totalAdditionalCessAmountSaleReturn = 0;

	var regex = new RegExp(StringConstant.GSTINregex);

	for (var _i = 0; _i < len; _i++) {
		var row = '';
		var txn = listOfTransactions[_i];
		var GstinNo = txn.getGstinNo();
		var invoiceDate = MyDate.getDate('d/m/y', txn.getInvoiceDate());
		var invoiceValue = txn.getInvoiceValue();
		var cessRate = txn.getCessRate();
		var invoiceRate = txn.getRate() - cessRate - txn.getStateSpecificCESSRate();
		var invoceTaxableValue = txn.getInvoiceTaxableValue();
		var igstAmt = txn.getIGSTAmt();
		var sgstAmt = txn.getSGSTAmt();
		var cgstAmt = txn.getCGSTAmt();
		var cessAmt = txn.getCESSAmt();
		var additionalCess = txn.getAdditionalCessAmt();
		var stateSpecificCESS = txn.getStateSpecificCESSAmt();
		var otherAmt = txn.getOTHERAmt();
		var POS = txn.getPlaceOfSupply();
		var transactionId = txn.getTransactionId();
		var transactionType = txn.getTransactionType();
		var incorrectEntry = txn.getEntryIncorrect();

		var partyName = txn.getPartyName();
		var refNo = txn.getTxnRefNumber() === '' ? '' : txn.getInvoiceNo();
		var returnRefNo = txn.getTxnReturnRefNumber();
		var returnDate = txn.getTxnReturnDate() ? MyDate.getDate('d/m/y', txn.getTxnReturnDate()) : '';

		if (transactionType == TxnTypeConstant.TXN_TYPE_SALE) {
			if (!invoiceNoMapSale.has(transactionId)) {
				invoiceNoMapSale.set(transactionId, invoiceValue);
				totalInvoiceValueSale += invoiceValue;
			}
			totalTaxableValueSale += invoceTaxableValue;
			totalCESSAmountSale += cessAmt;
			totalIGSTAmountSale += igstAmt;
			totalCGSTAmountSale += cgstAmt;
			totalSGSTAmountSale += sgstAmt;
			totalOTHERAmountSale += otherAmt;
			totalAdditionalCessAmountSale += additionalCess;
			totalStateSpecificCESSAmountSale += stateSpecificCESS;
		} else if (transactionType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
			if (!invoiceNoMapSaleReturn.has(transactionId)) {
				invoiceNoMapSaleReturn.set(transactionId, invoiceValue);
				totalInvoiceValueSaleReturn += invoiceValue;
			}
			totalTaxableValueSaleReturn += invoceTaxableValue;
			totalCESSAmountSaleReturn += cessAmt;
			totalIGSTAmountSaleReturn += igstAmt;
			totalCGSTAmountSaleReturn += cgstAmt;
			totalSGSTAmountSaleReturn += sgstAmt;
			totalOTHERAmountSaleReturn += otherAmt;
			totalAdditionalCessAmountSaleReturn += additionalCess;
			totalStateSpecificCESSAmountSaleReturn += stateSpecificCESS;
		}

		row += "<tr ondblclick='openTransaction(" + transactionId + ',' + transactionType + ")' class='currentRow'";

		if (incorrectEntry) {
			row += "style='background-color: #e87475;' title='Incorrect Transaction'";
		}
		row += "><td align='left' style='width: 150px;";

		if (!(regex.test(GstinNo) || GstinNo.trim() == '')) {
			row += 'background-color: #FAF197;';
		}
		row += "'";
		if (!(regex.test(GstinNo) || GstinNo.trim() == '')) {
			row += "title='Invalid GSTIN No.'";
		}

		row += '>' + GstinNo + "</td><td align='left' style='width: 150px;'>" + partyName + '</td>';
		if (transactionType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
			row += "<td style='width: 150px;' class='tableCellTextAlignRight'>" + returnRefNo + "</td><td style='width: 150px;' class='tableCellTextAlignRight'>" + returnDate + '</td>';
		}
		row += "<td style='width: 150px;' class='tableCellTextAlignRight'>" + refNo + "</td><td style='width: 150px;' class='tableCellTextAlignRight'>" + invoiceDate + "</td><td class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getAmountWithDecimal(invoiceValue) + "</td><td class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getPercentageWithDecimal(invoiceRate) + "</td><td class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getPercentageWithDecimal(cessRate) + "</td><td class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getAmountWithDecimal(invoceTaxableValue) + "</td><td class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getAmountWithDecimal(igstAmt) + "</td><td class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getAmountWithDecimal(cgstAmt) + "</td><td class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getAmountWithDecimal(sgstAmt) + '</td>';
		row += "<td class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getAmountWithDecimal(cessAmt) + '</td>';
		if (showOTHER) {
			row += "<td class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getAmountWithDecimal(otherAmt) + '</td>';
		}
		if (showStateSpecificCESS) {
			row += "<td class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getAmountWithDecimal(stateSpecificCESS) + '</td>';
		}
		if (showAdditionalCess) {
			row += "<td class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getAmountWithDecimal(additionalCess) + '</td>';
		}
		row += "<td class='tableCellTextAlignRight' style='width: 150px;'>" + POS + '</td>';

		row += '</tr>';

		if (transactionType == TxnTypeConstant.TXN_TYPE_SALE) {
			rowData1.push(row);
		} else if (transactionType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
			rowData2.push(row);
		}
	}

	if (len > 0) {
		if (rowData1.length > 0) {
			rowData1.push('<tr></tr>');
			var totalRow1 = "<tr><th align='left' style='width:150px;'>Total</th><th colspan='3' align='left' style='width:450px;'><th class='tableCellTextAlignRight' style='width:150px;'>" + MyDouble.getAmountWithDecimal(totalInvoiceValueSale) + "</th><th colspan='2' align='left' style='width:300px;'></th><th class='tableCellTextAlignRight' style='width:150px;'>" + MyDouble.getAmountWithDecimal(totalTaxableValueSale) + "</th><th class='tableCellTextAlignRight' style='width:150px;'>" + MyDouble.getAmountWithDecimal(totalIGSTAmountSale) + "</th><th class='tableCellTextAlignRight' style='width:150px;'>" + MyDouble.getAmountWithDecimal(totalCGSTAmountSale) + "</th><th class='tableCellTextAlignRight' style='width:150px;'>" + MyDouble.getAmountWithDecimal(totalSGSTAmountSale) + "</th><th class='tableCellTextAlignRight' style='width:150px;'>" + MyDouble.getAmountWithDecimal(totalCESSAmountSale) + '</th>';
			if (showOTHER) {
				totalRow1 += "<th class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getAmountWithDecimal(totalOTHERAmountSale) + '</th>';
			}
			if (showStateSpecificCESS) {
				totalRow1 += "<th class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getAmountWithDecimal(totalStateSpecificCESSAmountSale) + '</th>';
			}
			if (showAdditionalCess) {
				totalRow1 += "<th class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getAmountWithDecimal(totalAdditionalCessAmountSale) + '</th>';
			}
			totalRow1 += "<th class='tableCellTextAlignRight' style='width: 150px;'></th></tr>";
			rowData1.push(totalRow1);
		}

		if (rowData2.length > 0) {
			rowData2.push('<tr></tr>');
			var totalRow2 = "<tr><th align='left' style='width:150px;'>Total</th><th colspan='5' align='left' style='width:750px;'><th class='tableCellTextAlignRight' style='width:150px;'>" + MyDouble.getAmountWithDecimal(totalInvoiceValueSaleReturn) + "</th><th colspan='2' align='left' style='width:300px;'></th><th class='tableCellTextAlignRight' style='width:150px;'>" + MyDouble.getAmountWithDecimal(totalTaxableValueSaleReturn) + "</th><th class='tableCellTextAlignRight' style='width:150px;'>" + MyDouble.getAmountWithDecimal(totalIGSTAmountSaleReturn) + "</th><th class='tableCellTextAlignRight' style='width:150px;'>" + MyDouble.getAmountWithDecimal(totalCGSTAmountSaleReturn) + "</th><th class='tableCellTextAlignRight' style='width:150px;'>" + MyDouble.getAmountWithDecimal(totalSGSTAmountSaleReturn) + "</th><th class='tableCellTextAlignRight' style='width:150px;'>" + MyDouble.getAmountWithDecimal(totalCESSAmountSaleReturn) + '</th>';
			if (showOTHER) {
				totalRow2 += "<th class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getAmountWithDecimal(totalOTHERAmountSaleReturn) + '</th>';
			}
			if (showStateSpecificCESS) {
				totalRow2 += "<th class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getAmountWithDecimal(totalStateSpecificCESSAmountSaleReturn) + '</th>';
			}
			if (showAdditionalCess) {
				totalRow2 += "<th class='tableCellTextAlignRight' style='width: 150px;'>" + MyDouble.getAmountWithDecimal(totalAdditionalCessAmountSaleReturn) + '</th>';
			}
			totalRow2 += "<th class='tableCellTextAlignRight' style='width: 150px;'></th></tr>";
			rowData2.push(totalRow2);
		}
	}

	$('#invisibleRow1').html(invisibleRow1);
	$('#invisibleRow2').html(invisibleRow2);

	var data = rowData1;

	if ($('#gstr1ReportContainer').length === 0) {
		return;
	}
	if (gstr1ReportSaleClusterize && gstr1ReportSaleClusterize.destroy) {
		gstr1ReportSaleClusterize.clear();
		gstr1ReportSaleClusterize.destroy();
	}

	gstr1ReportSaleClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollArea1',
		contentId: 'contentArea1',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});

	if (gstr1ReportSaleReturnClusterize && gstr1ReportSaleReturnClusterize.destroy) {
		gstr1ReportSaleReturnClusterize.clear();
		gstr1ReportSaleReturnClusterize.destroy();
	}

	var data = rowData2;
	gstr1ReportSaleReturnClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollArea2',
		contentId: 'contentArea2',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});

	$('#tableHead1').html(dynamicRow1);

	$('#tableHead2').html(dynamicRow2);
	$('#title1').html('Sale');
	$('#title2').html('Sale Return');
};

$('#scrollArea1').on('scroll', function () {
	$('#tableHeader1').scrollLeft($(this).scrollLeft());
});

$('#scrollArea2').on('scroll', function () {
	$('#tableHeader2').scrollLeft($(this).scrollLeft());
});

$('#tableHead').on('click', '#invoiceColId', function (e) {
	var SortHelper = require('./../Utilities/SortHelper.js');
	var sortHelper = new SortHelper();
	listOfTransactions = sortHelper.getSortedListByInvoice(listOfTransactions, 'txnRefNumber');
	displayTransactionList(listOfTransactions);
});

var openTransaction = function openTransaction(txnId, txnType) {
	var ViewTransaction = require('./../BizLogic/ViewTransaction.js');
	var viewTransaction = new ViewTransaction();
	viewTransaction.viewTransactionFile(txnId + ':' + TxnTypeConstant.getTxnType(txnType));
};

var getHTMLTextForReport = function getHTMLTextForReport() {
	var GSTR1ReportHTMLGenerator = require('./../ReportHTMLGenerator/GSTR1ReportHTMLGenerator.js');
	var gSTR1ReportHTMLGenerator = new GSTR1ReportHTMLGenerator();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var firmId = Number($('#firmFilterOptions').val());
	var htmlText = gSTR1ReportHTMLGenerator.getHTMLText(listOfTransactions, startDateString, endDateString, firmId, showOTHER, showAdditionalCess, showStateSpecificCESS);
	return htmlText;
};

/// ///////////////////////////PDF/////////////////////////////////////
var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.GSTR1_REPORT, fromDate: '01/' + $('#startDate').val(), toDate: '01/' + $('#endDate').val(), format: 'MM_YY' });
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

$('#partyFilterOptions').on('selectmenuchange', function () {
	displayTransactionList(listOfTransactions);
});

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.GSTR1_REPORT, fromDate: '01/' + $('#startDate').val(), toDate: '01/' + $('#endDate').val(), format: 'MM_YY' });
	excelHelper.saveExcel(fileName);
};

var prepareObjectForExcel = function prepareObjectForExcel(listOfTransactions) {
	var monthArray = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	var firmId = Number($('#firmFilterOptions').val());
	var fromDate = $('#startDate').val();
	var toDate = $('#endDate').val();
	fromDate = '01/' + fromDate;
	toDate = '01/' + toDate;
	var startDate = MyDate.getDateObj(fromDate, 'dd/MM/yyyy', '/');
	var startmonthIndex = startDate.getMonth();
	var startMonthName = monthArray[startmonthIndex];
	var startmonthYear = startDate.getFullYear();

	var endDate = MyDate.getDateObj(toDate, 'dd/MM/yyyy', '/');
	var endmonthIndex = endDate.getMonth();
	var endMonthName = monthArray[endmonthIndex];
	var endmonthYear = endDate.getFullYear();

	if (firmId == 0) {
		firmId = settingCache.getDefaultFirmId();
	}
	var firm = firmCache.getFirmById(firmId);
	var firmName = firm.getFirmName();
	var gstin = firmCache.getFirmGSTINByFirmName(firmName);

	var len = listOfTransactions.length;
	var rowObj = [];
	rowObj.push(['Period', startMonthName + ' ' + startmonthYear + ' - ' + endMonthName + ' ' + endmonthYear]);
	rowObj.push([]);
	rowObj.push(['1. GSTIN', gstin]);
	rowObj.push(['2.a Legal name of the registered person.', firmName]);
	rowObj.push(['2.b Trade name, if any']);
	rowObj.push(['3.a Aggregate turnover of the preceeding Financial Year']);
	rowObj.push(['3.b Aggregate turnover, April to June 2017']);
	rowObj.push([]);
	var totalArray = [];
	var tableHeadArray = [];
	tableHeadArray.push('GSTIN/UIN');
	tableHeadArray.push('Party Name');
	tableHeadArray.push('Transaction Type');
	tableHeadArray.push('Invoice No.');
	tableHeadArray.push('Invoice Date');
	tableHeadArray.push('Invoice Value');
	tableHeadArray.push('Rate');
	tableHeadArray.push('Cess Rate');
	tableHeadArray.push('Taxable value');
	tableHeadArray.push('Reverse Charge');
	tableHeadArray.push('Integrated Tax Amount');
	tableHeadArray.push('Central Tax Amount');
	tableHeadArray.push('State/UT Tax Amount');
	tableHeadArray.push('Cess Amount');
	if (showOTHER) {
		tableHeadArray.push('Other Tax Amount');
	}
	if (showStateSpecificCESS) {
		tableHeadArray.push('Flood CESS');
	}
	if (showAdditionalCess) {
		tableHeadArray.push('Additional Cess Amount');
	}
	tableHeadArray.push('Place of Supply(Name of state)');
	rowObj.push(tableHeadArray);
	rowObj.push([]);

	var invoiceNoMap = new _map2.default();
	var totalTaxableValue = 0;
	var totalInvoiceValue = 0;
	var totalIGSTAmount = 0;
	var totalCGSTAmount = 0;
	var totalSGSTAmount = 0;
	var totalCESSAmount = 0;
	var totalOTHERAmount = 0;
	var totalStateSpecificCESSAmount = 0;
	var totalAdditionalCessAmount = 0;

	for (var _i2 = 0; _i2 < len; _i2++) {
		var tempArray = [];
		var txn = listOfTransactions[_i2];
		var GstinNo = txn.getGstinNo();
		var invoiceDate = MyDate.getDate('d/m/y', txn.getInvoiceDate());
		var invoiceValue = txn.getInvoiceValue();
		var refNo = txn.getTxnRefNumber() === '' ? '' : txn.getInvoiceNo();
		var cessRate = txn.getCessRate();
		var invoiceRate = txn.getRate() - cessRate - txn.getStateSpecificCESSRate();
		var invoceTaxableValue = txn.getInvoiceTaxableValue();
		var igstAmt = txn.getIGSTAmt();
		var sgstAmt = txn.getSGSTAmt();
		var cgstAmt = txn.getCGSTAmt();
		var cessAmt = txn.getCESSAmt();
		var stateSpecificCESSAmt = txn.getStateSpecificCESSAmt();
		var otherAmt = txn.getOTHERAmt();
		var POS = txn.getPlaceOfSupply();
		var additionalCess = txn.getAdditionalCessAmt();
		var reverseChargeString = 'N';
		var transactionId = txn.getTransactionId();
		var transactionType = txn.getTransactionType();
		var partyName = txn.getPartyName();
		var firmId = txn.getFirmId();
		var firm = firmCache.getFirmById(firmId);

		if (!invoiceNoMap.has(transactionId)) {
			invoiceNoMap.set(transactionId, invoiceValue);
			if (transactionType == TxnTypeConstant.TXN_TYPE_SALE) {
				totalInvoiceValue += invoiceValue;
			} else {
				totalInvoiceValue -= invoiceValue;
			}
		}
		if (transactionType == TxnTypeConstant.TXN_TYPE_SALE) {
			totalTaxableValue += invoceTaxableValue;
			totalIGSTAmount += igstAmt;
			totalCGSTAmount += cgstAmt;
			totalSGSTAmount += sgstAmt;
			totalCESSAmount += cessAmt;
			totalOTHERAmount += otherAmt;
			totalAdditionalCessAmount += additionalCess;
			totalStateSpecificCESSAmount += stateSpecificCESSAmt;
		} else {
			totalTaxableValue -= invoceTaxableValue;
			totalIGSTAmount -= igstAmt;
			totalCGSTAmount -= cgstAmt;
			totalSGSTAmount -= sgstAmt;
			totalCESSAmount -= cessAmt;
			totalOTHERAmount -= otherAmt;
			totalAdditionalCessAmount -= additionalCess;
			totalStateSpecificCESSAmount -= stateSpecificCESSAmt;
		}

		tempArray.push(GstinNo);
		tempArray.push(partyName);
		tempArray.push(TxnTypeConstant.getTxnTypeForUI(transactionType));
		tempArray.push(refNo);
		tempArray.push(invoiceDate);
		tempArray.push(MyDouble.getAmountWithDecimal(invoiceValue));
		tempArray.push(MyDouble.getPercentageWithDecimal(invoiceRate));
		tempArray.push(MyDouble.getPercentageWithDecimal(cessRate));
		tempArray.push(MyDouble.getAmountWithDecimal(invoceTaxableValue));
		tempArray.push(reverseChargeString);
		tempArray.push(MyDouble.getAmountWithDecimal(igstAmt));
		tempArray.push(MyDouble.getAmountWithDecimal(cgstAmt));
		tempArray.push(MyDouble.getAmountWithDecimal(sgstAmt));
		tempArray.push(MyDouble.getAmountWithDecimal(cessAmt));
		if (showOTHER) {
			tempArray.push(MyDouble.getAmountWithDecimal(otherAmt));
		}
		if (showStateSpecificCESS) {
			tempArray.push(MyDouble.getAmountWithDecimal(stateSpecificCESSAmt));
		}
		if (showAdditionalCess) {
			tempArray.push(MyDouble.getAmountWithDecimal(additionalCess));
		}
		tempArray.push(POS);
		rowObj.push(tempArray);
	}
	rowObj.push([]);

	totalArray.push('Total');
	totalArray.push('');
	totalArray.push('');
	totalArray.push('');
	totalArray.push('');
	totalArray.push(MyDouble.getAmountWithDecimal(totalInvoiceValue));
	totalArray.push('');
	totalArray.push('');
	totalArray.push(MyDouble.getAmountWithDecimal(totalTaxableValue));
	totalArray.push('');
	totalArray.push(MyDouble.getAmountWithDecimal(totalIGSTAmount));
	totalArray.push(MyDouble.getAmountWithDecimal(totalCGSTAmount));
	totalArray.push(MyDouble.getAmountWithDecimal(totalSGSTAmount));
	totalArray.push(MyDouble.getAmountWithDecimal(totalCESSAmount));
	if (showOTHER) {
		totalArray.push(MyDouble.getAmountWithDecimal(totalOTHERAmount));
	}
	if (showStateSpecificCESS) {
		totalArray.push(MyDouble.getAmountWithDecimal(totalStateSpecificCESSAmount));
	}
	if (showAdditionalCess) {
		totalArray.push(MyDouble.getAmountWithDecimal(totalAdditionalCessAmount));
	}
	rowObj.push(totalArray);
	return rowObj;
};

var getObjectForB2B = function getObjectForB2B(listOfTransactions) {
	var rowObj = [];
	rowObj.push(['Summary For B2B(4)', '', '', '', '', '', '', '', '', '', '']);
	rowObj.push(['No. of Recipients', 'No. of Invoices', '', 'Total Invoice Value', '', '', '', '', '', 'Total Taxable Value', 'Total Cess']);
	rowObj.push(['', '', '', '', '', '', '', '', '', '', '']);
	rowObj.push(['GSTIN/UIN of Recipient', 'Invoice Number', 'Invoice date', 'Invoice Value', 'Place Of Supply', 'Reverse Charge', 'Invoice Type', 'E-Commerce GSTIN', 'Rate', 'Taxable Value', 'Cess Amount']);

	var firmId = Number($('#firmFilterOptions').val());

	var invoiceNoMap = new _map2.default();
	var gstinNoMap = new _map2.default();
	var totalTaxableValue = 0;
	var totalInvoiceValue = 0;
	var totalCESSAmount = 0;

	for (var i = 0; i < listOfTransactions.length; i++) {
		var txn = listOfTransactions[i];
		var transactionId = txn.getTransactionId();
		var transactionType = txn.getTransactionType();
		var gstinNo = txn.getGstinNo();
		var firmId = txn.getFirmId();
		var firm = firmCache.getFirmById(firmId);
		var reverseCharge = txn.getReverseCharge();
		var taxCode = taxCodeCache.getTaxCodeObjectById(txn.getTaxRateId());

		if (transactionType == TxnTypeConstant.TXN_TYPE_SALE && gstinNo != '' && gstinNo != null && MyDouble.convertStringToDouble(txn.getTaxRateId(), true) && taxCode.getTaxRateType() != TaxCodeConstants.Exempted && taxCode.getTaxRate() != 0) {
			var taxableValue = txn.getInvoiceTaxableValue();
			var invoiceValue = txn.getInvoiceValue();
			if (!invoiceNoMap.has(transactionId)) {
				invoiceNoMap.set(transactionId, invoiceValue);
				totalInvoiceValue += invoiceValue;
			}
			totalTaxableValue += taxableValue;

			var tempArray = [];
			var invoiceNo = MyString.invoiceStringForGSTR1Report(txn.getInvoiceNo());
			var invoiceDate = GSTRReportHelper.getDateFormatForGSTRReport(txn.getInvoiceDate());
			var placeOfSupply = txn.getPlaceOfSupply() ? StateCode.getStateCode(txn.getPlaceOfSupply()) + '-' + txn.getPlaceOfSupply() : '';
			var reverseChargeString = reverseCharge ? 'Y' : 'N';
			var invoiceType = 'Regular';
			var ecommerceGSTIN = '';
			var cessRate = txn.getCessRate();
			var rate = txn.getRate() - cessRate - txn.getStateSpecificCESSRate();
			var cessAmount = txn.getCESSAmt() + txn.getAdditionalCessAmt();
			totalCESSAmount += cessAmount;
			cessAmount = cessAmount || '';

			if (!gstinNoMap.has(gstinNo) && gstinNo != null && gstinNo.trim() != '') {
				gstinNoMap.set(gstinNo, 1);
			}

			tempArray.push(gstinNo);
			tempArray.push(invoiceNo);
			tempArray.push(invoiceDate);
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(invoiceValue));
			tempArray.push(placeOfSupply);
			tempArray.push(reverseChargeString);
			tempArray.push(invoiceType);
			tempArray.push(ecommerceGSTIN);
			tempArray.push(MyDouble.getPercentageWithDecimal(rate));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(taxableValue));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(cessAmount));
			rowObj.push(tempArray);
		}
	}
	rowObj[2][0] = gstinNoMap.size;
	rowObj[2][1] = invoiceNoMap.size;
	rowObj[2][3] = MyDouble.getAmountDecimalForGSTRReport(totalInvoiceValue);
	rowObj[2][9] = MyDouble.getAmountDecimalForGSTRReport(totalTaxableValue);
	rowObj[2][10] = MyDouble.getAmountDecimalForGSTRReport(totalCESSAmount);
	return rowObj;
};

var getObjectForB2CL = function getObjectForB2CL(listOfTransactions) {
	var rowObj = [];
	rowObj.push(['Summary For B2CL(5)', '', '', '', '', '', '', '']);
	rowObj.push(['No. of Invoices', '', 'Total Invoice Value', '', '', 'Total Taxable Value', 'Total Cess', '']);
	rowObj.push(['', '', '', '', '', '', '', '']);
	rowObj.push(['Invoice Number', 'Invoice date', 'Invoice Value', 'Place Of Supply', 'Rate', 'Taxable Value', 'Cess Amount', 'E-Commerce GSTIN']);

	var invoiceNoMap = new _map2.default();
	var totalTaxableValue = 0;
	var totalInvoiceValue = 0;
	var totalCESSAmount = 0;

	for (var i = 0; i < listOfTransactions.length; i++) {
		var txn = listOfTransactions[i];
		var transactionId = txn.getTransactionId();
		var transactionType = txn.getTransactionType();
		var gstinNo = txn.getGstinNo();
		var firmId = txn.getFirmId();
		var firm = firmCache.getFirmById(firmId);
		var firmState = firm.getFirmState();
		var taxCode = taxCodeCache.getTaxCodeObjectById(txn.getTaxRateId());

		if (transactionType == TxnTypeConstant.TXN_TYPE_SALE && (gstinNo == '' || gstinNo == null) && MyDouble.convertStringToDouble(txn.getTaxRateId(), true) && taxCode.getTaxRateType() != TaxCodeConstants.Exempted && taxCode.getTaxRate() != 0) {
			var invoiceValue = txn.getInvoiceValue();
			var taxableValue = txn.getInvoiceTaxableValue();

			if (invoiceValue > 250000 && firmState && firmState.trim() && txn.getPlaceOfSupply() && txn.getPlaceOfSupply().trim() && firmState.trim() != txn.getPlaceOfSupply().trim()) {
				if (!invoiceNoMap.has(transactionId)) {
					invoiceNoMap.set(transactionId, invoiceValue);
					totalInvoiceValue += invoiceValue;
				}
				totalTaxableValue += taxableValue;

				var tempArray = [];
				var invoiceNo = MyString.invoiceStringForGSTR1Report(txn.getInvoiceNo());
				var invoiceDate = GSTRReportHelper.getDateFormatForGSTRReport(txn.getInvoiceDate());
				var cessRate = txn.getCessRate();
				var rate = txn.getRate() - cessRate - txn.getStateSpecificCESSRate();
				var cessAmount = txn.getCESSAmt() + txn.getAdditionalCessAmt();
				totalCESSAmount += cessAmount;
				cessAmount = cessAmount || '';
				var ecommerceGSTIN = '';
				var placeOfSupply = txn.getPlaceOfSupply() ? StateCode.getStateCode(txn.getPlaceOfSupply()) + '-' + txn.getPlaceOfSupply() : '';

				tempArray.push(invoiceNo);
				tempArray.push(invoiceDate);
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(invoiceValue));
				tempArray.push(placeOfSupply);
				tempArray.push(MyDouble.getPercentageWithDecimal(rate));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(taxableValue));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(cessAmount));
				tempArray.push(ecommerceGSTIN);
				rowObj.push(tempArray);
			}
		}
	}
	var noOfInvoices = invoiceNoMap.size;
	rowObj[2][0] = noOfInvoices;
	rowObj[2][2] = MyDouble.getAmountDecimalForGSTRReport(totalInvoiceValue);
	rowObj[2][5] = MyDouble.getAmountDecimalForGSTRReport(totalTaxableValue);
	rowObj[2][6] = MyDouble.getAmountDecimalForGSTRReport(totalCESSAmount);
	return rowObj;
};

var getObjectForB2CS = function getObjectForB2CS(listOfTransactions) {
	var listoftransactionsB2CS = [];
	var b2csMap = new _map2.default();
	var rowObj = [];
	rowObj.push(['Summary For B2CS(7)', '', '', '', '', '']);
	var totalHeader = ['', '', '', 'Total Taxable Value', 'Total Cess'];
	var header = ['Type', 'Place Of Supply', 'Rate', 'Taxable Value', 'Cess Amount'];
	if (showStateSpecificCESS) {
		totalHeader.push('Total Flood CESS');
		header.push('Flood CESS');
	}
	header.push('E-Commerce GSTIN');
	rowObj.push(totalHeader);
	rowObj.push(['', '', '', '', '', '', '', '']);

	rowObj.push(header);

	var totalTaxableValue = 0;
	var totalCESSAmount = 0;
	var totalStateSpecificCESSAmount = 0;

	for (var i = 0; i < listOfTransactions.length; i++) {
		var txn = listOfTransactions[i];
		var transactionType = txn.getTransactionType();
		var gstinNo = txn.getGstinNo();
		var firmId = txn.getFirmId();
		var firm = firmCache.getFirmById(firmId);
		var firmState = firm.getFirmState();
		var taxCode = taxCodeCache.getTaxCodeObjectById(txn.getTaxRateId());

		if ((transactionType == TxnTypeConstant.TXN_TYPE_SALE || transactionType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) && (gstinNo == '' || gstinNo == null) && MyDouble.convertStringToDouble(txn.getTaxRateId(), true) && taxCode.getTaxRateType() != TaxCodeConstants.Exempted && taxCode.getTaxRate() != 0) {
			var invoiceValue = txn.getInvoiceValue();
			if (!firmState || !firmState.trim() || !txn.getPlaceOfSupply() || !txn.getPlaceOfSupply().trim() || firmState.trim() == txn.getPlaceOfSupply().trim() || invoiceValue <= 250000) {
				var cessRate = txn.getCessRate();
				var rate = txn.getRate() - cessRate - txn.getStateSpecificCESSRate();
				var b2csMapKey = txn.getPlaceOfSupply() ? StateCode.getStateCode(txn.getPlaceOfSupply()) + '-' + rate : 'nps' + '-' + rate;
				var newTxn = b2csMap.get(b2csMapKey);
				if (newTxn == null) {
					newTxn = {
						'type': 'OE',
						'pos': txn.getPlaceOfSupply(),
						'rate': txn.getRate(),
						'cessRate': txn.getCessRate(),
						'stateSpecificCESSRate': txn.getStateSpecificCESSRate(),
						'taxableValue': 0,
						'cessAmt': 0,
						'additionalCessAmt': 0,
						'stateSpecificCESSAmt': 0
					};
				}
				if (transactionType == TxnTypeConstant.TXN_TYPE_SALE) {
					newTxn['taxableValue'] += txn.getInvoiceTaxableValue();
					newTxn['cessAmt'] += txn.getCESSAmt();
					newTxn['additionalCessAmt'] += txn.getAdditionalCessAmt();
					newTxn['stateSpecificCESSAmt'] += txn.getStateSpecificCESSAmt();
				} else {
					newTxn['taxableValue'] -= txn.getInvoiceTaxableValue();
					newTxn['cessAmt'] -= txn.getCESSAmt();
					newTxn['additionalCessAmt'] -= txn.getAdditionalCessAmt();
					newTxn['stateSpecificCESSAmt'] -= txn.getStateSpecificCESSAmt();
				}

				b2csMap.set(b2csMapKey, newTxn);
			}
		}
	}

	var _iteratorNormalCompletion = true;
	var _didIteratorError = false;
	var _iteratorError = undefined;

	try {
		for (var _iterator = (0, _getIterator3.default)(b2csMap.values()), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
			var i = _step.value;

			listoftransactionsB2CS.push(i);
		}
	} catch (err) {
		_didIteratorError = true;
		_iteratorError = err;
	} finally {
		try {
			if (!_iteratorNormalCompletion && _iterator.return) {
				_iterator.return();
			}
		} finally {
			if (_didIteratorError) {
				throw _iteratorError;
			}
		}
	}

	for (var i = 0; i < listoftransactionsB2CS.length; i++) {
		var txn = listoftransactionsB2CS[i];
		var tempArray = [];
		var type = 'OE';
		var placeOfSupply = txn['pos'] ? StateCode.getStateCode(txn['pos']) + '-' + txn['pos'] : '';
		var cessRate = txn['cessRate'];
		var rate = txn['rate'] - cessRate - txn['stateSpecificCESSRate'];
		var cessAmount = txn['cessAmt'] + txn['additionalCessAmt'];
		var stateSpecificCESSAmount = txn['stateSpecificCESSAmt'];
		totalCESSAmount += cessAmount;
		totalStateSpecificCESSAmount += stateSpecificCESSAmount;
		cessAmount = cessAmount || '';
		var ecommerceGSTIN = '';
		var taxableValue = txn['taxableValue'];
		totalTaxableValue += taxableValue;

		tempArray.push(type);
		tempArray.push(placeOfSupply);
		tempArray.push(MyDouble.getPercentageWithDecimal(rate));
		tempArray.push(MyDouble.getAmountDecimalForGSTRReport(taxableValue));
		tempArray.push(MyDouble.getAmountDecimalForGSTRReport(cessAmount));
		if (showStateSpecificCESS) {
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(stateSpecificCESSAmount));
		}
		tempArray.push(ecommerceGSTIN);
		rowObj.push(tempArray);
	}
	rowObj[2][3] = MyDouble.getAmountDecimalForGSTRReport(totalTaxableValue);
	rowObj[2][4] = MyDouble.getAmountDecimalForGSTRReport(totalCESSAmount);
	if (showStateSpecificCESS) {
		rowObj[2][5] = MyDouble.getAmountDecimalForGSTRReport(totalStateSpecificCESSAmount);
	}
	return rowObj;
};

var getObjectForCDNR = function getObjectForCDNR(listOfTransactions) {
	var rowObj = [];
	rowObj.push(['Summary For CDNR(9B)', '', '', '', '', '']);
	rowObj.push(['No. of Recipients', 'No. of Invoices', '', 'No. of Notes/Vouchers', '', '', '', '', 'Total Note/Refund Voucher Value', '', 'Total Taxable Value', 'Total Cess', '']);
	rowObj.push(['', '', '', '', '', '', '', '', '', '', '', '', '']);
	rowObj.push(['GSTIN/UIN of Recipient', 'Invoice/Advance Receipt Number', 'Invoice/Advance Receipt date', 'Note/Refund Voucher Number', 'Note/Refund Voucher date', 'Document Type', 'Reason For Issuing document', 'Place Of Supply', 'Note/Refund Voucher Value', 'Rate', 'Taxable Value', 'Cess Amount', 'Pre GST']);
	var invoiceNoMap = new _map2.default();
	var gstinNoMap = new _map2.default();
	var totalTaxableValue = 0;
	var totalInvoiceValue = 0;
	var totalCESSAmount = 0;

	for (var i = 0; i < listOfTransactions.length; i++) {
		var txn = listOfTransactions[i];
		var gstinNo = txn.getGstinNo();
		var firmId = txn.getFirmId();
		var firm = firmCache.getFirmById(firmId);
		var firmState = firm.getFirmState();
		var transactionId = txn.getTransactionId();
		var transactionType = txn.getTransactionType();

		if (transactionType == TxnTypeConstant.TXN_TYPE_SALE_RETURN && gstinNo != '' && gstinNo != null) {
			var noteInvoiceValue = txn.getInvoiceValue();
			if (!invoiceNoMap.has(transactionId)) {
				invoiceNoMap.set(transactionId, noteInvoiceValue);
				totalInvoiceValue += noteInvoiceValue;
			}
			var noteTaxableValue = txn.getInvoiceTaxableValue();
			totalTaxableValue += noteTaxableValue;

			var tempArray = [];
			var mainInvoiceNo = txn.getTxnReturnRefNumber();
			var mainInvoiceDate = txn.getTxnReturnDate() ? GSTRReportHelper.getDateFormatForGSTRReport(txn.getTxnReturnDate()) : '';
			var noteNumber = MyString.invoiceStringForGSTR1Report(txn.getInvoiceNo());
			var noteDate = GSTRReportHelper.getDateFormatForGSTRReport(txn.getInvoiceDate());
			if (txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
				var documentType = 'C';
			}
			var reasonForIssuingDocument = '01-Sales Return';
			var placeOfSupply = txn.getPlaceOfSupply() ? StateCode.getStateCode(txn.getPlaceOfSupply()) + '-' + txn.getPlaceOfSupply() : '';
			var cessRate = txn.getCessRate();
			var rate = txn.getRate() - cessRate - txn.getStateSpecificCESSRate();
			var cessAmount = txn.getCESSAmt() + txn.getAdditionalCessAmt();
			totalCESSAmount += cessAmount;
			cessAmount = cessAmount || '';
			var preGST = 'N';

			if (!gstinNoMap.has(gstinNo) && gstinNo != null && gstinNo.trim() != '') {
				gstinNoMap.set(gstinNo, 1);
			}

			tempArray.push(gstinNo);
			tempArray.push(mainInvoiceNo);
			tempArray.push(mainInvoiceDate);
			tempArray.push(noteNumber);
			tempArray.push(noteDate);
			tempArray.push(documentType);
			tempArray.push(reasonForIssuingDocument);
			tempArray.push(placeOfSupply);
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(noteInvoiceValue));
			tempArray.push(MyDouble.getPercentageWithDecimal(rate));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(noteTaxableValue));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(cessAmount));
			tempArray.push(preGST);

			rowObj.push(tempArray);
		}
	}
	rowObj[2][0] = gstinNoMap.size;
	rowObj[2][3] = invoiceNoMap.size;
	rowObj[2][8] = MyDouble.getAmountDecimalForGSTRReport(totalInvoiceValue);
	rowObj[2][10] = MyDouble.getAmountDecimalForGSTRReport(totalTaxableValue);
	rowObj[2][11] = MyDouble.getAmountDecimalForGSTRReport(totalCESSAmount);
	return rowObj;
};

var getObjectForCDNUR = function getObjectForCDNUR(listOfTransactions) {
	var rowObj = [];
	rowObj.push(['Summary For CDNUR(9B)', '', '', '', '', '', '', '', '', '', '', '', '']);
	rowObj.push(['', 'No. of Notes/Vouchers', '', '', 'No. of Invoices', '', '', '', 'Total Note Value', '', 'Total Taxable Value', 'Total Cess', '']);
	rowObj.push(['', '', '', '', '', '', '', '', '', '', '', '', '']);
	rowObj.push(['UR Type', 'Note/Refund Voucher Number', 'Note/Refund Voucher date', 'Document Type', 'Invoice/Advance Receipt Number', 'Invoice/Advance Receipt date', 'Reason For Issuing document', 'Place Of Supply', 'Note/Refund Voucher Value', 'Rate', 'Taxable Value', 'Cess Amount', 'Pre GST']);

	var invoiceNoMap = new _map2.default();
	var totalTaxableValue = 0;
	var totalInvoiceValue = 0;
	var totalCESSAmount = 0;

	for (var i = 0; i < listOfTransactions.length; i++) {
		var txn = listOfTransactions[i];
		var gstinNo = txn.getGstinNo();
		var firmId = txn.getFirmId();
		var firm = firmCache.getFirmById(firmId);
		var firmState = firm.getFirmState();
		var transactionId = txn.getTransactionId();
		var transactionType = txn.getTransactionType();
		var firmId = txn.getFirmId();
		var firm = firmCache.getFirmById(firmId);
		var firmState = firm.getFirmState();

		if (transactionType == TxnTypeConstant.TXN_TYPE_SALE_RETURN && (!gstinNo || !gstinNo.trim())) {
			var invoiceValue = txn.getInvoiceValue();
			var taxableValue = txn.getInvoiceTaxableValue();

			if (invoiceValue > 250000 && firmState && txn.getPlaceOfSupply() && firmState.trim() && txn.getPlaceOfSupply().trim() && firmState != txn.getPlaceOfSupply()) {
				if (!invoiceNoMap.has(transactionId)) {
					invoiceNoMap.set(transactionId, invoiceValue);
					totalInvoiceValue += invoiceValue;
				}
				totalTaxableValue += taxableValue;

				var tempArray = [];
				var urType = '';
				var noteNumber = MyString.invoiceStringForGSTR1Report(txn.getInvoiceNo());
				var noteDate = GSTRReportHelper.getDateFormatForGSTRReport(txn.getInvoiceDate());
				if (txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
					var documentType = 'C';
				}
				var mainInvoiceNo = txn.getTxnReturnRefNumber();
				var mainInvoiceDate = txn.getTxnReturnDate() ? GSTRReportHelper.getDateFormatForGSTRReport(txn.getTxnReturnDate()) : '';
				var reasonForIssuingDocument = '01-Sales Return';
				var placeOfSupply = txn.getPlaceOfSupply() ? StateCode.getStateCode(txn.getPlaceOfSupply()) + '-' + txn.getPlaceOfSupply() : '';
				var noteInvoiceValue = txn.getInvoiceValue();
				var cessRate = txn.getCessRate();
				var rate = txn.getRate() - cessRate - txn.getStateSpecificCESSRate();
				var noteTaxableValue = txn.getInvoiceTaxableValue();
				var cessAmount = txn.getCESSAmt() + txn.getAdditionalCessAmt();
				totalCESSAmount += cessAmount;
				cessAmount = cessAmount || '';
				var preGST = 'N';

				tempArray.push(urType);
				tempArray.push(noteNumber);
				tempArray.push(noteDate);
				tempArray.push(documentType);
				tempArray.push(mainInvoiceNo);
				tempArray.push(mainInvoiceDate);
				tempArray.push(reasonForIssuingDocument);
				tempArray.push(placeOfSupply);
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(noteInvoiceValue));
				tempArray.push(MyDouble.getPercentageWithDecimal(rate));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(noteTaxableValue));
				tempArray.push(MyDouble.getAmountDecimalForGSTRReport(cessAmount));
				tempArray.push(preGST);
				rowObj.push(tempArray);
			}
		}
	}
	rowObj[2][1] = invoiceNoMap.size;
	rowObj[2][8] = MyDouble.getAmountDecimalForGSTRReport(totalInvoiceValue);
	rowObj[2][10] = MyDouble.getAmountDecimalForGSTRReport(totalTaxableValue);
	rowObj[2][11] = MyDouble.getAmountDecimalForGSTRReport(totalCESSAmount);
	return rowObj;
};

var getObjectForItemHSNReport = function getObjectForItemHSNReport(listOfItemData) {
	var showItemName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

	var rowObj = [];
	rowObj.push(['Summary For HSN(12)', '', '', '', '', '', '', '', '', '', '', '', '']);
	var totatHeader = ['No. of HSN', '', '', '', 'Total Value', 'Total Taxable Value', 'Total Integrated Tax', 'Total Central Tax', 'Total State/UT Tax', 'Total Cess'];
	var header = ['HSN', 'Description', 'UQC', 'Total Quantity', 'Total Value', 'Taxable Value', 'Integrated Tax Amount', 'Central Tax Amount', 'State/UT Tax Amount', 'Cess Amount'];
	if (showStateSpecificCESS) {
		totatHeader.push('Total Flood CESS');
		header.push('Flood CESS');
	}
	rowObj.push(totatHeader);
	rowObj.push(['', '', '', '', '', '', '', '', '', '', '', '', '', '']);
	rowObj.push(header);

	var hsnNoMap = new _map2.default();
	var totalTaxableValue = 0;
	var totalValue = 0;
	var totalCESSAmount = 0;
	var totalStateSpecificCESSAmount = 0;
	var totalIGSTAmount = 0;
	var totalCGSTAmount = 0;
	var totalSGSTAmount = 0;

	for (var i = 0; i < listOfItemData.length; i++) {
		var txn = listOfItemData[i];
		var itemHsn = txn.getItemHSN();
		var itemDescription = showItemName && txn.getItemName() ? txn.getItemName() : '';
		var unit = 'OTH-OTHERS';
		var itemQuantity = MyDouble.convertStringToDouble(txn.getItemQuantity());
		var itemFreeQuantity = MyDouble.convertStringToDouble(txn.getItemFreeQuantity());
		var itemTotalValue = txn.getItemTotalValue();
		var itemTaxableValue = txn.getItemTaxableValue();
		var igstAmount = txn.getIGSTAmt();
		var cgstAmount = txn.getCGSTAmt();
		var sgstAmount = txn.getSGSTAmt();
		var cessAmount = txn.getCESSAmt() + txn.getAdditionalCessAmt();
		var stateSpecificCessAmount = txn.getStateSpecificCESSAmt();

		if (!hsnNoMap.has(itemHsn) && itemHsn != null && itemHsn.trim() != '') {
			hsnNoMap.set(itemHsn, 1);
		}
		totalTaxableValue += Number(itemTaxableValue);
		totalValue += Number(itemTotalValue);
		totalCESSAmount += Number(cessAmount);
		totalStateSpecificCESSAmount += Number(stateSpecificCessAmount);
		totalIGSTAmount += Number(igstAmount);
		totalCGSTAmount += Number(cgstAmount);
		totalSGSTAmount += Number(sgstAmount);

		var tempArray = [];
		tempArray.push(itemHsn);
		tempArray.push(itemDescription);
		tempArray.push(unit);
		tempArray.push(MyDouble.getQuantityDecimalForGSTRReport(itemQuantity + itemFreeQuantity));
		tempArray.push(MyDouble.getAmountDecimalForGSTRReport(itemTotalValue));
		tempArray.push(MyDouble.getAmountDecimalForGSTRReport(itemTaxableValue));
		tempArray.push(MyDouble.getAmountDecimalForGSTRReport(igstAmount));
		tempArray.push(MyDouble.getAmountDecimalForGSTRReport(cgstAmount));
		tempArray.push(MyDouble.getAmountDecimalForGSTRReport(sgstAmount));
		tempArray.push(MyDouble.getAmountDecimalForGSTRReport(cessAmount));
		if (showStateSpecificCESS) {
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(stateSpecificCessAmount));
		}
		rowObj.push(tempArray);
	}
	rowObj[2][0] = hsnNoMap.size;
	rowObj[2][4] = MyDouble.getAmountDecimalForGSTRReport(totalValue);
	rowObj[2][5] = MyDouble.getAmountDecimalForGSTRReport(totalTaxableValue);
	rowObj[2][6] = MyDouble.getAmountDecimalForGSTRReport(totalIGSTAmount);
	rowObj[2][7] = MyDouble.getAmountDecimalForGSTRReport(totalCGSTAmount);
	rowObj[2][8] = MyDouble.getAmountDecimalForGSTRReport(totalSGSTAmount);
	rowObj[2][9] = MyDouble.getAmountDecimalForGSTRReport(totalCESSAmount);
	if (showStateSpecificCESS) {
		rowObj[2][10] = MyDouble.getAmountDecimalForGSTRReport(totalStateSpecificCESSAmount);
	}
	return rowObj;
};

var getObjectForExpReport = function getObjectForExpReport() {
	var rowObj = [];
	rowObj.push(['Summary For EXP(6)', '', '', '', '', '', '', '', '']);
	rowObj.push(['', 'No. of Invoices', '', 'Total Invoice Value', '', 'No. of Shipping Bill', '', '', 'Total Taxable Value']);
	rowObj.push(['', '', '', '', '', '', '', '', '']);
	rowObj.push(['Export Type', 'Invoice Number', 'Invoice date', 'Invoice Value', 'Port Code', 'Shipping Bill Number', 'Shipping Bill Date', 'Rate', 'Taxable Value']);
	return rowObj;
};

var getObjectForAtReport = function getObjectForAtReport() {
	var rowObj = [];
	rowObj.push(['Summary For Advance Received(11B)', '', '', '']);
	rowObj.push(['', '', 'Total Advance Received', 'Total Cess']);
	rowObj.push(['', '', '', '']);
	rowObj.push(['Place Of Supply', 'Rate', 'Gross Advance Received', 'Cess Amount']);
	return rowObj;
};

var getObjectForAtAdjReport = function getObjectForAtAdjReport() {
	var rowObj = [];
	rowObj.push(['Summary For Advance Adjusted(11B)', '', '', '']);
	rowObj.push(['', '', 'Total Advance Adjusted', 'Total Cess']);
	rowObj.push(['', '', '', '']);
	rowObj.push(['Place Of Supply', 'Rate', 'Gross Advance Adjusted', 'Cess Amount']);
	return rowObj;
};

var getObjectForExempReport = function getObjectForExempReport(listOfTransactions) {
	var TaxCodeConstants = require('./../Constants/TaxCodeConstants.js');
	var rowObj = [];

	var interStateNilRatedSuppliesForRegistered = 0;
	var intraStateNilRatedSuppliesForRegistered = 0;
	var interStateNilRatedSuppliesForUnRegistered = 0;
	var intraStateNilRatedSuppliesForUnRegistered = 0;
	var interStateExemptedSuppliesForRegistered = 0;
	var intraStateExemptedSuppliesForRegistered = 0;
	var interStateExemptedSuppliesForUnRegistered = 0;
	var intraStateExemptedSuppliesForUnRegistered = 0;

	for (var i = 0; i < listOfTransactions.length; i++) {
		var txn = listOfTransactions[i];
		var firm = firmCache.getFirmById(txn.getFirmId());
		var firmState = firm.getFirmState();
		var taxCode = taxCodeCache.getTaxCodeObjectById(txn.getTaxRateId());

		if (txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_SALE) {
			if (txn.getGstinNo() == null || txn.getGstinNo() == '') {
				// unregistered
				if (firmState && firmState.trim() && txn.getPlaceOfSupply() && txn.getPlaceOfSupply().trim() && firmState.trim() != txn.getPlaceOfSupply().trim()) {
					if (taxCode == null || taxCode.getTaxRateType() == TaxCodeConstants.Exempted) {
						interStateExemptedSuppliesForUnRegistered += txn.getInvoiceTaxableValue();
					} else if (taxCode != null && taxCode.getTaxRate() == 0) {
						interStateNilRatedSuppliesForUnRegistered += txn.getInvoiceTaxableValue();
					}
				} else {
					// intra state
					if (taxCode == null || taxCode.getTaxRateType() == TaxCodeConstants.Exempted) {
						// exempted
						intraStateExemptedSuppliesForUnRegistered += txn.getInvoiceTaxableValue();
					} else if (taxCode != null && taxCode.getTaxRate() == 0) {
						// nil rated
						intraStateNilRatedSuppliesForUnRegistered += txn.getInvoiceTaxableValue();
					}
				}
			} else {
				// registered
				if (firmState && firmState.trim() && txn.getPlaceOfSupply() && txn.getPlaceOfSupply().trim() && firmState.trim() != txn.getPlaceOfSupply().trim()) {
					// inter state
					if (taxCode == null || taxCode.getTaxRateType() == TaxCodeConstants.Exempted) {
						interStateExemptedSuppliesForRegistered += txn.getInvoiceTaxableValue();
					} else if (taxCode != null && taxCode.getTaxRate() == 0) {
						interStateNilRatedSuppliesForRegistered += txn.getInvoiceTaxableValue();
					}
				} else {
					//  intra state
					if (taxCode == null || taxCode.getTaxRateType() == TaxCodeConstants.Exempted) {
						// exempted
						intraStateExemptedSuppliesForRegistered += txn.getInvoiceTaxableValue();
					} else if (taxCode != null && taxCode.getTaxRate() == 0) {
						// nil rated
						intraStateNilRatedSuppliesForRegistered += txn.getInvoiceTaxableValue();
					}
				}
			}
		} else if (txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
			if (txn.getGstinNo() == null || txn.getGstinNo() == '') {
				// unregistered
				if (firmState && firmState.trim() && txn.getPlaceOfSupply() && txn.getPlaceOfSupply().trim() && firmState.trim() != txn.getPlaceOfSupply().trim()) {
					if (taxCode == null || taxCode.getTaxRateType() == TaxCodeConstants.Exempted) {
						interStateExemptedSuppliesForUnRegistered -= txn.getInvoiceTaxableValue();
					} else if (taxCode != null && taxCode.getTaxRate() == 0) {
						interStateNilRatedSuppliesForUnRegistered -= txn.getInvoiceTaxableValue();
					}
				} else {
					// intra state
					if (taxCode == null || taxCode.getTaxRateType() == TaxCodeConstants.Exempted) {
						// exempted
						intraStateExemptedSuppliesForUnRegistered -= txn.getInvoiceTaxableValue();
					} else if (taxCode != null && taxCode.getTaxRate() == 0) {
						// nil rated
						intraStateNilRatedSuppliesForUnRegistered -= txn.getInvoiceTaxableValue();
					}
				}
			} else {
				// registered
				if (firmState && firmState.trim() && txn.getPlaceOfSupply() && txn.getPlaceOfSupply().trim() && firmState.trim() != txn.getPlaceOfSupply().trim()) {
					// inter state
					if (taxCode == null || taxCode.getTaxRateType() == TaxCodeConstants.Exempted) {
						interStateExemptedSuppliesForRegistered -= txn.getInvoiceTaxableValue();
					} else if (taxCode != null && taxCode.getTaxRate() == 0) {
						interStateNilRatedSuppliesForRegistered -= txn.getInvoiceTaxableValue();
					}
				} else {
					//  intra state
					if (taxCode == null || taxCode.getTaxRateType() == TaxCodeConstants.Exempted) {
						// exempted
						intraStateExemptedSuppliesForRegistered -= txn.getInvoiceTaxableValue();
					} else if (taxCode != null && taxCode.getTaxRate() == 0) {
						// nil rated
						intraStateNilRatedSuppliesForRegistered -= txn.getInvoiceTaxableValue();
					}
				}
			}
		}
	}
	var totalNilRated = intraStateNilRatedSuppliesForUnRegistered + interStateNilRatedSuppliesForUnRegistered + intraStateNilRatedSuppliesForRegistered + interStateNilRatedSuppliesForRegistered;
	var totalExempted = intraStateExemptedSuppliesForUnRegistered + interStateExemptedSuppliesForUnRegistered + intraStateExemptedSuppliesForRegistered + interStateExemptedSuppliesForRegistered;

	rowObj.push(['Summary For Nil rated, exempted and non GST outward supplies (8)', '', '', '']);
	rowObj.push(['', 'Total Nil Rated Supplies', 'Total Exempted Supplies', 'Total Non-GST Supplies']);
	rowObj.push(['', MyDouble.getAmountDecimalForGSTRReport(totalNilRated), MyDouble.getAmountDecimalForGSTRReport(totalExempted), 0.0]);
	rowObj.push(['Description', 'Nil Rated Supplies', 'Exempted(other than nil rated/non GST supply)', 'Non-GST Supplies']);
	rowObj.push(['Inter-State supplies to registered persons', MyDouble.getAmountDecimalForGSTRReport(interStateNilRatedSuppliesForRegistered), MyDouble.getAmountDecimalForGSTRReport(interStateExemptedSuppliesForRegistered), 0.0]);
	rowObj.push(['Intra-State supplies to registered persons', MyDouble.getAmountDecimalForGSTRReport(intraStateNilRatedSuppliesForRegistered), MyDouble.getAmountDecimalForGSTRReport(intraStateExemptedSuppliesForRegistered), 0.0]);
	rowObj.push(['Inter-State supplies to unregistered persons', MyDouble.getAmountDecimalForGSTRReport(interStateNilRatedSuppliesForUnRegistered), MyDouble.getAmountDecimalForGSTRReport(interStateExemptedSuppliesForUnRegistered), 0.0]);
	rowObj.push(['Intra-State supplies to unregistered persons', MyDouble.getAmountDecimalForGSTRReport(intraStateNilRatedSuppliesForUnRegistered), MyDouble.getAmountDecimalForGSTRReport(intraStateExemptedSuppliesForUnRegistered), 0.0]);

	return rowObj;
};

var getObjectForDocsReport = function getObjectForDocsReport() {
	var rowObj = [];
	rowObj.push(['Summary of documents issued during the tax period (13)', '', '', '', '']);
	rowObj.push(['', '', '', 'Total Number', 'Total Cancelled']);
	rowObj.push(['', '', '', '']);
	rowObj.push(['Nature of Document', 'Sr. No. From', 'Sr. No. To', 'Total Number', 'Cancelled']);
	rowObj.push(['Invoice for outward supply', '', '', '', '']);
	rowObj.push(['Debit Note', '', '', '', '']);
	rowObj.push(['Delivery Challan for job work', '', '', '', '']);
	rowObj.push(['Invoice for inward supply from unregistered person', '', '', '', '']);
	rowObj.push(['Refund Voucher', '', '', '', '']);
	rowObj.push(['Invoice for outward supply', '', '', '', '']);
	return rowObj;
};

function printExcel() {
	var XLSX = require('xlsx');
	var dataArray = prepareObjectForExcel(listOfTransactions);
	var worksheet = XLSX.utils.aoa_to_sheet(dataArray);

	var workbook = {
		'SheetNames': [],
		'Sheets': {}
	};

	var wsName = 'GSTR1 Report';
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet;
	var wscolWidth = [{ wch: 20 }, { wch: 30 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 30 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }];
	worksheet['!cols'] = wscolWidth;

	/// /////////////////////////////////B2B///////////////////////////////////////

	var wsNameItem = 'b2b';
	workbook.SheetNames[1] = wsNameItem;
	var b2bArray = getObjectForB2B(listOfTransactions);
	var b2bWorksheet = XLSX.utils.aoa_to_sheet(b2bArray);
	workbook.Sheets[wsNameItem] = b2bWorksheet;
	var wsItemcolWidth = [{ wch: 20 }, { wch: 15 }, { wch: 15 }, { wch: 20 }, { wch: 40 }, { wch: 20 }, { wch: 20 }, { wch: 30 }, { wch: 15 }, { wch: 20 }, { wch: 20 }];
	b2bWorksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////B2CL///////////////////////////////////////

	var wsNameItem = 'b2cl';
	workbook.SheetNames[2] = wsNameItem;
	var b2clArray = getObjectForB2CL(listOfTransactions);
	var b2clWorksheet = XLSX.utils.aoa_to_sheet(b2clArray);
	workbook.Sheets[wsNameItem] = b2clWorksheet;
	var wsItemcolWidth = [{ wch: 20 }, { wch: 15 }, { wch: 20 }, { wch: 40 }, { wch: 20 }, { wch: 20 }, { wch: 30 }, { wch: 40 }];
	b2clWorksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////B2CS///////////////////////////////////////

	var wsNameItem = 'b2cs';
	workbook.SheetNames[3] = wsNameItem;
	var b2csArray = getObjectForB2CS(listOfTransactions);
	var b2csWorksheet = XLSX.utils.aoa_to_sheet(b2csArray);
	workbook.Sheets[wsNameItem] = b2csWorksheet;
	var wsItemcolWidth = [{ wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }];
	b2csWorksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////CDNR///////////////////////////////////////

	var wsNameItem = 'cdnr';
	workbook.SheetNames[4] = wsNameItem;
	var cdnrArray = getObjectForCDNR(listOfTransactions);
	var cdnrWorksheet = XLSX.utils.aoa_to_sheet(cdnrArray);
	workbook.Sheets[wsNameItem] = cdnrWorksheet;
	var wsItemcolWidth = [{ wch: 22 }, { wch: 30 }, { wch: 25 }, { wch: 35 }, { wch: 35 }, { wch: 20 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	cdnrWorksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////CDNUR///////////////////////////////////////

	var wsNameItem = 'cdnur';
	workbook.SheetNames[5] = wsNameItem;
	var cdnurArray = getObjectForCDNUR(listOfTransactions);
	var cdnurWorksheet = XLSX.utils.aoa_to_sheet(cdnurArray);
	workbook.Sheets[wsNameItem] = cdnurWorksheet;
	var wsItemcolWidth = [{ wch: 22 }, { wch: 30 }, { wch: 25 }, { wch: 35 }, { wch: 35 }, { wch: 20 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	cdnurWorksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////Item Wise Sale///////////////////////////////////////

	var considerNonTaxAsExemptedCheck = $('#considerNonTaxAsExempted').prop('checked');

	var firmId = Number($('#firmFilterOptions').val());
	var GSTR1ReportHelper = require('./../UIControllers/GSTR1ReportHelper.js');
	var gSTR1ReportHelper = new GSTR1ReportHelper();
	var listOfItemDataByHSNForSale = gSTR1ReportHelper.getItemWiseDataListBasedOnDate(startDate, endDate, firmId, TxnTypeConstant.TXN_TYPE_SALE, considerNonTaxAsExemptedCheck);
	var wsNameItem = 'itemWiseSale';
	//  workbook.SheetNames[7] = wsNameItem;
	var hsnSaleArray = getObjectForItemHSNReport(listOfItemDataByHSNForSale);
	var hsnSaleWorksheet = XLSX.utils.aoa_to_sheet(hsnSaleArray);
	workbook.Sheets[wsNameItem] = hsnSaleWorksheet;
	var wsItemcolWidth = [{ wch: 22 }, { wch: 30 }, { wch: 25 }, { wch: 35 }, { wch: 35 }, { wch: 20 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	hsnSaleWorksheet['!cols'] = wsItemcolWidth;

	/// ////////////////////////////////Item Wise Sale Return///////////////////////////////////////
	var firmId = Number($('#firmFilterOptions').val());
	var GSTR1ReportHelper = require('./../UIControllers/GSTR1ReportHelper.js');
	var gSTR1ReportHelper = new GSTR1ReportHelper();
	var listOfItemDataByHSNForSaleReturn = gSTR1ReportHelper.getItemWiseDataListBasedOnDate(startDate, endDate, firmId, TxnTypeConstant.TXN_TYPE_SALE_RETURN, considerNonTaxAsExemptedCheck);
	var wsNameItem = 'itemWiseSaleReturn';
	//  workbook.SheetNames[11] = wsNameItem;
	var hsnSaleReturnArray = getObjectForItemHSNReport(listOfItemDataByHSNForSaleReturn);
	var hsnSaleReturnWorksheet = XLSX.utils.aoa_to_sheet(hsnSaleReturnArray);
	workbook.Sheets[wsNameItem] = hsnSaleReturnWorksheet;
	var wsItemcolWidth = [{ wch: 22 }, { wch: 30 }, { wch: 25 }, { wch: 35 }, { wch: 35 }, { wch: 20 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	hsnSaleReturnWorksheet['!cols'] = wsItemcolWidth;

	/// //////////////////////////////////////// Item Summary ///////////////////////////////////////////////
	var itemSummaryList = [];
	var itemSummaryMap = new _map2.default();
	for (var i = 0; i < listOfItemDataByHSNForSale.length; i++) {
		var txn = listOfItemDataByHSNForSale[i];
		var itemId = txn.getItemId();
		itemSummaryMap.set(itemId, txn);
	}
	for (var i = 0; i < listOfItemDataByHSNForSaleReturn.length; i++) {
		var txn = listOfItemDataByHSNForSaleReturn[i];
		var itemId = txn.getItemId();
		if (!itemSummaryMap.has(itemId)) {
			var itemSummaryObject = txn;
			itemSummaryObject.setItemQuantity(0 - txn.getItemQuantity());
			itemSummaryObject.setItemFreeQuantity(0 - txn.getItemFreeQuantity());
			itemSummaryObject.setItemTotalValue(0 - txn.getItemTotalValue());
			itemSummaryObject.setItemTaxableValue(0 - txn.getItemTaxableValue());
			itemSummaryObject.setIGSTAmt(0 - txn.getIGSTAmt());
			itemSummaryObject.setCGSTAmt(0 - txn.getCGSTAmt());
			itemSummaryObject.setSGSTAmt(0 - txn.getSGSTAmt());
			itemSummaryObject.setCESSAmt(0 - txn.getCESSAmt());
			itemSummaryObject.setAdditionalCessAmt(0 - txn.getAdditionalCessAmt());
			itemSummaryObject.setStateSpecificCESSAmt(0 - txn.getStateSpecificCESSAmt());
		} else {
			var itemSummaryObject = itemSummaryMap.get(itemId);
			itemSummaryObject.setItemQuantity(itemSummaryObject.getItemQuantity() - txn.getItemQuantity());
			itemSummaryObject.setItemFreeQuantity(itemSummaryObject.getItemFreeQuantity() - txn.getItemFreeQuantity());
			itemSummaryObject.setItemTotalValue(itemSummaryObject.getItemTotalValue() - txn.getItemTotalValue());
			itemSummaryObject.setItemTaxableValue(itemSummaryObject.getItemTaxableValue() - txn.getItemTaxableValue());
			itemSummaryObject.setIGSTAmt(itemSummaryObject.getIGSTAmt() - txn.getIGSTAmt());
			itemSummaryObject.setCGSTAmt(itemSummaryObject.getCGSTAmt() - txn.getCGSTAmt());
			itemSummaryObject.setSGSTAmt(itemSummaryObject.getSGSTAmt() - txn.getSGSTAmt());
			itemSummaryObject.setCESSAmt(itemSummaryObject.getCESSAmt() - txn.getCESSAmt());
			itemSummaryObject.setAdditionalCessAmt(itemSummaryObject.getAdditionalCessAmt() - txn.getAdditionalCessAmt());
			itemSummaryObject.setStateSpecificCESSAmt(itemSummaryObject.getStateSpecificCESSAmt() - txn.getStateSpecificCESSAmt());
		}
		itemSummaryMap.set(itemId, itemSummaryObject);
	}

	var _iteratorNormalCompletion2 = true;
	var _didIteratorError2 = false;
	var _iteratorError2 = undefined;

	try {
		for (var _iterator2 = (0, _getIterator3.default)(itemSummaryMap.values()), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
			var i = _step2.value;

			itemSummaryList.push(i);
		}
	} catch (err) {
		_didIteratorError2 = true;
		_iteratorError2 = err;
	} finally {
		try {
			if (!_iteratorNormalCompletion2 && _iterator2.return) {
				_iterator2.return();
			}
		} finally {
			if (_didIteratorError2) {
				throw _iteratorError2;
			}
		}
	}

	var wsNameItem = 'itemSummary';
	workbook.SheetNames[11] = wsNameItem;
	var itemSummaryArray = getObjectForItemHSNReport(itemSummaryList, true);
	var itemSummaryWorksheet = XLSX.utils.aoa_to_sheet(itemSummaryArray);
	workbook.Sheets[wsNameItem] = itemSummaryWorksheet;
	var wsItemcolWidth = [{ wch: 22 }, { wch: 30 }, { wch: 25 }, { wch: 35 }, { wch: 35 }, { wch: 20 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	itemSummaryWorksheet['!cols'] = wsItemcolWidth;

	/// ///////////////////////// HSN Summary /////////////////////////////

	var hsnSummaryList = [];
	var hsnSummaryMap = new _map2.default();

	for (var i = 0; i < itemSummaryList.length; i++) {
		var txn = itemSummaryList[i];
		var hsnCode = txn.getItemHSN();
		hsnCode = hsnCode ? hsnCode.trim() : '';

		if (!hsnSummaryMap.has(hsnCode)) {
			var hsnSummaryObject = txn;
			hsnSummaryObject.setItemName('');
		} else {
			var hsnSummaryObject = hsnSummaryMap.get(hsnCode);
			hsnSummaryObject.setItemQuantity(hsnSummaryObject.getItemQuantity() + txn.getItemQuantity());
			hsnSummaryObject.setItemFreeQuantity(hsnSummaryObject.getItemFreeQuantity() + txn.getItemFreeQuantity());
			hsnSummaryObject.setItemTotalValue(hsnSummaryObject.getItemTotalValue() + txn.getItemTotalValue());
			hsnSummaryObject.setItemTaxableValue(hsnSummaryObject.getItemTaxableValue() + txn.getItemTaxableValue());
			hsnSummaryObject.setIGSTAmt(hsnSummaryObject.getIGSTAmt() + txn.getIGSTAmt());
			hsnSummaryObject.setCGSTAmt(hsnSummaryObject.getCGSTAmt() + txn.getCGSTAmt());
			hsnSummaryObject.setSGSTAmt(hsnSummaryObject.getSGSTAmt() + txn.getSGSTAmt());
			hsnSummaryObject.setCESSAmt(hsnSummaryObject.getCESSAmt() + txn.getCESSAmt());
			hsnSummaryObject.setAdditionalCessAmt(hsnSummaryObject.getAdditionalCessAmt() + txn.getAdditionalCessAmt());
			hsnSummaryObject.setStateSpecificCESSAmt(hsnSummaryObject.getStateSpecificCESSAmt() + txn.getStateSpecificCESSAmt());
		}
		hsnSummaryMap.set(hsnCode, hsnSummaryObject);
	}

	var _iteratorNormalCompletion3 = true;
	var _didIteratorError3 = false;
	var _iteratorError3 = undefined;

	try {
		for (var _iterator3 = (0, _getIterator3.default)(hsnSummaryMap.values()), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
			var i = _step3.value;

			hsnSummaryList.push(i);
		}
	} catch (err) {
		_didIteratorError3 = true;
		_iteratorError3 = err;
	} finally {
		try {
			if (!_iteratorNormalCompletion3 && _iterator3.return) {
				_iterator3.return();
			}
		} finally {
			if (_didIteratorError3) {
				throw _iteratorError3;
			}
		}
	}

	var wsNameItem = 'hsn';
	workbook.SheetNames[10] = wsNameItem;
	var hsnSummaryArray = getObjectForItemHSNReport(hsnSummaryList);
	var hsnSummaryWorksheet = XLSX.utils.aoa_to_sheet(hsnSummaryArray);
	workbook.Sheets[wsNameItem] = hsnSummaryWorksheet;
	var wsItemcolWidth = [{ wch: 22 }, { wch: 30 }, { wch: 25 }, { wch: 35 }, { wch: 35 }, { wch: 20 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	hsnSummaryWorksheet['!cols'] = wsItemcolWidth;

	var wsNameItem = 'exp';
	workbook.SheetNames[6] = wsNameItem;
	var expArray = getObjectForExpReport();
	var expSheet = XLSX.utils.aoa_to_sheet(expArray);
	workbook.Sheets[wsNameItem] = expSheet;
	var wsItemcolWidth = [{ wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }];
	expSheet['!cols'] = wsItemcolWidth;

	var wsNameItem = 'at';
	workbook.SheetNames[7] = wsNameItem;
	var atArray = getObjectForAtReport();
	var atSheet = XLSX.utils.aoa_to_sheet(atArray);
	workbook.Sheets[wsNameItem] = atSheet;
	var wsItemcolWidth = [{ wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }];
	atSheet['!cols'] = wsItemcolWidth;

	var wsNameItem = 'atadj';
	workbook.SheetNames[8] = wsNameItem;
	var atAdjArray = getObjectForAtAdjReport();
	var atAdjSheet = XLSX.utils.aoa_to_sheet(atAdjArray);
	workbook.Sheets[wsNameItem] = atAdjSheet;
	var wsItemcolWidth = [{ wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }];
	atAdjSheet['!cols'] = wsItemcolWidth;

	var wsNameItem = 'exemp';
	workbook.SheetNames[9] = wsNameItem;
	var exempArray = getObjectForExempReport(listOfTransactions);
	var exempSheet = XLSX.utils.aoa_to_sheet(exempArray);
	workbook.Sheets[wsNameItem] = exempSheet;
	var wsItemcolWidth = [{ wch: 35 }, { wch: 25 }, { wch: 25 }, { wch: 25 }];
	exempSheet['!cols'] = wsItemcolWidth;

	// var wsNameItem = "docs";
	// workbook.SheetNames[11] = wsNameItem;
	// var docsArray = getObjectForDocsReport();
	// var docsSheet = XLSX.utils.aoa_to_sheet(docsArray);
	// workbook.Sheets[wsNameItem] = docsSheet;
	// var wsItemcolWidth = [
	//     {wch: 35},
	//     {wch: 20},
	//     {wch: 20},
	//     {wch: 20},
	//     {wch: 20}
	// ];
	// docsSheet['!cols'] = wsItemcolWidth;

	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

populateTable();