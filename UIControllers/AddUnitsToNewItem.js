var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ItemUnitCache = require('./../Cache/ItemUnitCache.js');
var itemUnitCache = new ItemUnitCache();
var ItemUnitMappingCache = require('./../Cache/ItemUnitMappingCache.js');
var ItemUnitMapping = require('./../BizLogic/ItemunitMapping.js');
var listOfBaseUnits = itemUnitCache.getAllItemUnitsNameWithShortName();

function appendUnitListOnDropDown() {
	var baseUnit = document.getElementById('baseUnit');
	var secondaryUnit = document.getElementById('secondaryUnit');
	var optionTemp = document.createElement('option');
	var len = (0, _keys2.default)(listOfBaseUnits).length;
	var optionNoneForBaseUnit = optionTemp.cloneNode();
	var optionNoneForSecondaryUnit = optionTemp.cloneNode();
	optionNoneForBaseUnit.text = 'None';
	optionNoneForBaseUnit.value = '0';
	optionNoneForSecondaryUnit.text = 'None';
	optionNoneForSecondaryUnit.value = '0';

	var primaryVal = null;
	var secondaryVal = null;

	$(secondaryUnit).html('');
	$(baseUnit).html('');

	baseUnit.add(optionNoneForBaseUnit);
	secondaryUnit.add(optionNoneForSecondaryUnit);
	var baseUnitSelected = 0;
	$.each(listOfBaseUnits, function (key, value) {
		var optionNoneForBaseUnit = optionTemp.cloneNode();
		var optionNoneForSecondaryUnit = optionTemp.cloneNode();
		optionNoneForBaseUnit.text = key;
		optionNoneForSecondaryUnit.text = key;
		baseUnit.add(optionNoneForBaseUnit);
		secondaryUnit.add(optionNoneForSecondaryUnit);
	});

	if (!primaryVal) {
		if ($('#selectUnit :first-child').html() == 'Edit Unit') {
			$.each(listOfBaseUnits, function (key, value) {
				if (itemObj && itemObj.getBaseUnitId() == value.getUnitId()) {
					primaryVal = key;
				}
			});
		}
	}

	if (!secondaryVal) {
		if ($('#selectUnit :first-child').html() == 'Edit Unit') {
			$.each(listOfBaseUnits, function (key, value) {
				if (itemObj && itemObj.getSecondaryUnitId() == value.getUnitId()) {
					secondaryVal = key;
				}
			});
		}
	}

	if (!primaryVal) {
		primaryVal = 0;
	}
	if (!secondaryVal) {
		secondaryVal = 0;
	}

	if (itemObj) {
		$(baseUnit).val(primaryVal);
		$(secondaryUnit).val(secondaryVal);
	}
}

var displayAvailableMappings = function displayAvailableMappings() {
	var mappingId = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

	debugger;
	try {
		if (!$('#secondaryUnit :selected').text() || $('#secondaryUnit :selected').text().toLowerCase() == 'none') {
			$('#conversion').parent().addClass('hide');
			$('#listOfMappings').html('');
			return false;
		}
		$('#conversion').parent().removeClass('hide');
		var itemUnitMappingCache = new ItemUnitMappingCache();
		var itemUnitCache = new ItemUnitCache();
		var baseUnit = $('#baseUnit :selected').text();
		var secondaryUnit = $('#secondaryUnit :selected').text();
		var listToAppend = [];
		if (secondaryUnit && secondaryUnit.toLowerCase() != 'none' && baseUnit && baseUnit.toLowerCase() != 'none') {
			var baseUnitId = listOfBaseUnits[baseUnit].getUnitId();
			var secondaryUnitId = listOfBaseUnits[secondaryUnit].getUnitId();
			var listOfMapping = itemUnitMappingCache.getMappingFromBaseAndSecondaryUnitIds(baseUnitId, secondaryUnitId);
			$.each(listOfMapping, function (key, value) {
				var rate = value['conversionRate'];
				var editField = "<input type='radio' name='selectMapping' class='check' id='" + value['mappingId'] + "' " + (mappingId == value['mappingId'] ? 'checked >' : '>');
				listToAppend += "<div class='floatLeft clearfix' style='width:100%' ><div class='floatLeft'>" + editField + '</div>' + "<div class='floatLeft'>" + baseUnit + ' = ' + rate + ' ' + secondaryUnit + '</div></div>';
				// dynamicRow += "<tr class='currentRow' tabindex='100' id='"+key+"'><td>"+i+". 1 "+baseUnit+" = "+rate+" "+secondaryUnit+"</td></tr>";
			});
		}
		$('#listOfMappings').html('');
		$('#listOfMappings').html(listToAppend);
	} catch (err) {
		console.log(err);
	}
};

$('#baseUnit').on('change', displayAvailableMappings);

$('#secondaryUnit').on('change', displayAvailableMappings);

// when adding new unit while seleccting
function addNewUnit() {
	var baseUnit = $('#baseUnit :selected').text();
	var secondaryUnit = $('#secondaryUnit :selected').text();
	var baseUnitId;
	var secondaryUnitId;
	var conversion;
	if (baseUnit && baseUnit.toLowerCase() != 'none' && secondaryUnit && secondaryUnit.toLowerCase() != 'none') {
		baseUnitId = listOfBaseUnits[baseUnit].getUnitId();
		secondaryUnitId = listOfBaseUnits[secondaryUnit].getUnitId();
		conversion = MyDouble.convertStringToDouble($('#conversion').val());
	} else {
		ToastHelper.error('Unable to add conversion rate. Please select valid secondary unit.');
		return false;
	}
	if (!conversion) {
		ToastHelper.error('Please enter valid conversion rate.');
		return false;
	}

	if (baseUnitId == secondaryUnitId) {
		ToastHelper.error('Base and secondary unit cannot be same');
		return false;
	} else {
		var newItemUnitMapping = new ItemUnitMapping();
		var statusCode = newItemUnitMapping.addNewUnitMapping(baseUnitId, secondaryUnitId, conversion);

		if (statusCode == ErrorCode.ERROR_UNIT_MAPPING_SAVE_SUCCESS) {
			ToastHelper.success(statusCode);
			displayAvailableMappings();
		} else {
			ToastHelper.error(statusCode);
		}
	}
}

$('.terminalButtonUnit').off('click').on('click', addNewUnit);

var addUnitsToItemForDone = function addUnitsToItemForDone() {
	GlobalMappingId = $("input[type='radio'][name='selectMapping']:checked").attr('id');
	if ($('#baseUnit :selected').text() == $('#secondaryUnit :selected').text()) {
		ToastHelper.error('Primary and secondary unit cannot be same. Please retry.');
		return false;
	}
	if ($('#baseUnit :selected').text() && $('#secondaryUnit :selected').text() && !GlobalMappingId && $('#secondaryUnit :selected').text() != 'None') {
		ToastHelper.error('Please select conversion rate from the available list or add new conversion rate.');
		return false;
	}
	$('#addUnitToItemDialog').find('input:text').val('');
	//            $('#addUnitToItemDialog').dialog('close');
	//            $('#addUnitToItemDialog').dialog('destroy');
	var ItemUnitMappingCache = require('./../Cache/ItemUnitMappingCache.js');
	var itemUnitMappingCache = new ItemUnitMappingCache();
	var ItemUnitCache = require('./../Cache/ItemUnitCache.js');
	var itemUnitCache = new ItemUnitCache();
	if (GlobalMappingId) {
		var mappingObj = itemUnitMappingCache.getItemUnitMapping(GlobalMappingId);
		var toInsert = '1 ' + itemUnitCache.getItemUnitShortNameById(mappingObj.getBaseUnitId()) + ' = ' + mappingObj.getConversionRate() + ' ' + itemUnitCache.getItemUnitShortNameById(mappingObj.getSecondaryUnitId());
		$('#mappingId').val(GlobalMappingId);
		$('#baseUnitId').val(mappingObj.getBaseUnitId());
		$('#secondaryUnitId').val(mappingObj.getSecondaryUnitId());
		$('#selectedUnit').html(toInsert);
		$('#selectUnit :first-child').html('Edit Unit');
	} else {
		var listOfBaseUnits = itemUnitCache.getAllItemUnitsNameWithShortName();
		var baseUnit = $('#baseUnit :selected').text();
		if (baseUnit && listOfBaseUnits[baseUnit]) {
			var baseUnitId = listOfBaseUnits[baseUnit].getUnitId();
			$('#baseUnitId').val(baseUnitId);
			$('#mappingId').val(null);
			$('#secondaryUnitId').val(null);
			var toInsert = itemUnitCache.getItemUnitShortNameById(baseUnitId);
			$('#selectedUnit').html(toInsert);
			$('#selectUnit :first-child').html('Edit Unit');
		} else {
			$('#baseUnitId').val(null);
			$('#mappingId').val(null);
			$('#secondaryUnitId').val(null);
			$('#selectedUnit').html('');
			$('#selectUnit :first-child').html('Select Unit');
		}
	}
	$('#addUnitToItemDialog').dialog('close');
};

// $('#secondaryUnit').selectmenu();
// $('#baseUnit').selectmenu();

var preselectUnits = $('#mappingId').val();

if (preselectUnits) {
	$('#baseUnit').val(itemUnitCache.getItemUnitNameWithShortNameForUnitId(mappingObj.getBaseUnitId()).trim()).selectmenu('refresh');
	$('#secondaryUnit').val(itemUnitCache.getItemUnitNameWithShortNameForUnitId(mappingObj.getSecondaryUnitId()).trim()).selectmenu('refresh');
	$('#conversion').val(mappingObj.conversionRate);
	displayAvailableMappings(mappingObj.mappingId);
	$('#addUnitToItemDialog').dialog('option', 'position', { my: 'center', at: 'center', of: window });
} else {
	var baseUnitPresent = $('#baseUnitId').val();
	if (baseUnitPresent) {
		$('#baseUnit').val(itemUnitCache.getItemUnitNameWithShortNameForUnitId(baseUnitPresent)).selectmenu('refresh');
	}
}

var selectItemFunc = function selectItemFunc() {
	debugger;
	itemObj = null;
	appendUnitListOnDropDown();
	GlobalListOfIds = [];
	$('#listOfItemsToAddUnits input:checkbox').each(function () {
		var $this = $(this);
		if ($this.is(':checked')) {
			GlobalListOfIds.push($this.attr('id'));
		}
	});
	if (GlobalListOfIds.length > 0) {
		$('#addUnitToItemDialog').dialog({
			width: '800',
			title: 'Select Conversion Rate',
			modal: true,
			appendTo: '#defaultPage',
			close: function close(event, ui) {
				$(this).find('input:text').val('');
				$('.listOfItemsToCheck').css('display', 'block');
				$('.addUnitsToSelectedItem').css('display', 'none');
				$('.submitAddUnitsToItem > button').text('Next');
				$(this).dialog('destroy');
				$('#addUnitToItemDialog').hide();
			}

		});
		$('.listOfItemsToCheck').css('display', 'none');
		$('.addUnitsToSelectedItem').css('display', 'block');
		$('.submitAddUnitsToItem > button').text('Save');
	} else {
		ToastHelper.error('Select at least one item.');
	}
};

var addUnitsToItem = function addUnitsToItem() {
	// console.log(GlobalListOfIds);
	if ($('#baseUnit :selected').text() == $('#secondaryUnit :selected').text()) {
		ToastHelper.error('Primary and secondary unit cannot be same. Please retry.');
		return false;
	}
	if ($('#baseUnit :selected').text() && $('#secondaryUnit :selected').text() && $("input[type='radio'][name='selectMapping']:checked").length < 1) {
		ToastHelper.error('Please select conversion rate from the available list or add new converstion rate.');
		return false;
	}
	var idsOfItems = GlobalListOfIds;
	var mappingId = $("input[type='radio'][name='selectMapping']:checked").attr('id');
	var ItemUnitCache = require('./../Cache/ItemUnitCache.js');
	var itemUnitCache = new ItemUnitCache();
	var listOfBaseUnits = itemUnitCache.getAllItemUnitsNameWithShortName();
	var baseUnit = $('#baseUnit :selected').text();
	var baseUnitId = null;
	if (baseUnit && listOfBaseUnits[baseUnit]) {
		baseUnitId = listOfBaseUnits[baseUnit].getUnitId();
	}
	var DataInserter = require('./../DBManager/DataInserter.js');
	var dataInserter = new DataInserter();
	var statusCode = dataInserter.addMappingToItems(idsOfItems, mappingId, baseUnitId);

	if (statusCode == ErrorCode.ERROR_UNIT_MAPPING_UPDATE_SUCCESS) {
		ToastHelper.success(statusCode);
	} else {
		ToastHelper.error(statusCode);
	}
	MyAnalytics.pushEvent('Set Units to Multiple Items Save');
	$('#addUnitToItemDialog').find('input:text').val('');
	$('.listOfItemsToCheck').css('display', 'block');
	$('.addUnitsToSelectedItem').css('display', 'none');
	$('.submitAddUnitsToItem > button').text('Next');
	$('#addUnitToItemDialog').dialog('destroy');
	$('#addUnitToItemDialog').hide();
};

$('.submitAddUnitsToItem > button').off('click').on('click', function () {
	if ($(this).text() == 'Next') {
		selectItemFunc();
	} else if ($(this).text() == 'Save') {
		addUnitsToItem();
	} else if ($(this).text() == 'Done') {
		addUnitsToItemForDone();
	}
});