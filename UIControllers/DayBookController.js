var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var startDate;
var endDate;
var DateFormat = require('./../Constants/DateFormat.js');
$('#startDate, #endDate').datepicker({ dateFormat: DateFormat.format });
var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');
var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
var NameCache = require('./../Cache/NameCache.js');
var paymentInfoCache = new PaymentInfoCache();
var PaymentInfo = require('./../BizLogic/PaymentInfo.js');
var paymentInfo = new PaymentInfo();
var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var printForSharePDF = false;
var listOfTransactions = [];

var populateTable = function populateTable() {
	settingCache = new SettingCache();
	var DataLoader = require('./../DBManager/DataLoader.js');
	var dataLoader = new DataLoader();
	startDateString = $('#startDate').val();
	startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');

	var firmId = Number($('#firmFilterOptions').val());

	CommonUtility.showLoader(function () {
		listOfTransactions = dataLoader.loadTransactionsForDayBook(startDate, firmId);
		displayTransactionList(listOfTransactions);
	});
};

var date = MyDate.getDate('d/m/y');
$('#startDate').val(date);

if (settingCache.getMultipleFirmEnabled()) {
	var optionTemp = document.createElement('option');
	var FirmCache = require('./../Cache/FirmCache.js');
	var firmCache = new FirmCache();
	var firmList = firmCache.getFirmList();
	for (i in firmList) {
		var option = optionTemp.cloneNode();
		option.text = firmList[i].getFirmName();
		option.value = firmList[i].getFirmId();
		$('#firmFilterOptions').append(option);
	}
	$('#firmChooser').show();
} else {
	$('#firmChooser').hide();
}

$('#firmFilterOptions').on('selectmenuchange', function (event) {
	populateTable();
});

$('#startDate').change(function () {
	populateTable();
});

var dayBookClusterize;

var displayTransactionList = function displayTransactionList(listOfTransactions) {
	var len = listOfTransactions.length;
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var PaymentCache = require('./../Cache/PaymentInfoCache.js');
	var paymentCache = new PaymentCache();

	var dynamicRow = '';
	var rowData = [];
	var isTransactionRefNumberEnabled = settingCache.getTxnRefNoEnabled();
	var extraColumnWidth = isTransactionRefNumberEnabled ? 0 : 2;

	var className = '';
	if (!$('#sortIcon').attr('class')) {
		className = 'ui-selectmenu-icon ui-icon ui-icon-triangle-1-s';
	} else {
		className = $('#sortIcon').attr('class');
	}

	dynamicRow += "<thead><tr><th width='" + (19 + extraColumnWidth) + "%'>NAME</th>";
	if (isTransactionRefNumberEnabled) {
		dynamicRow += '<th width=\'12%\' id=\'invoiceColId\' style=\'cursor:pointer;\'>Ref No.<span id=\'sortIcon\' class=\'' + className + '\'></span></th>';
	}
	dynamicRow += "<th width='" + (15 + extraColumnWidth) + "%'>TYPE</th><th width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>Total</th><th width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>Money in</th><th width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>Money out</th></tr></thead>";
	var totalCashIn = 0;
	var totalCashOut = 0;
	for (var _i = 0; _i < len; _i++) {
		var txn = listOfTransactions[_i];
		var txnType = txn.getTxnType();
		var typeString = TxnTypeConstant.getTxnTypeForUI(txnType);
		var cashAmt = txn.getCashAmount() ? txn.getCashAmount() : 0;
		var balanceAmt = txn.getBalanceAmount() ? txn.getBalanceAmount() : 0;
		var totalAmt = 0;
		var discountAmt = 0;
		var refNo = '';
		var typeTxn = TxnTypeConstant.getTxnType(txn.getTxnType());
		if (!TxnTypeConstant.isLoanTxnType(txnType)) {
			discountAmt = txn.getDiscountAmount() ? txn.getDiscountAmount() : 0;
			refNo = txn.getTxnRefNumber() === '' ? '' : txn.getFullTxnRefNumber();
		} else {
			var subTxnType = txn.getTxnSubType();
			typeString = TxnTypeConstant.getTxnTypeForUI(txnType, subTxnType);
		}

		if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
			totalAmt = Number(cashAmt) + Number(discountAmt);
		} else {
			totalAmt = Number(cashAmt) + Number(balanceAmt);
		}
		var row = '<tr id=' + txn.getTxnId() + ':' + typeTxn + " ondblclick='openTransaction(" + txn.getTxnId() + ',' + txn.getTxnType() + ")' class='currentRow'>";

		// row for name
		if (txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD || txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE || txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD_WITHOUT_CASH || txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE_WITHOUT_CASH || txnType == TxnTypeConstant.TXN_TYPE_BANK_TO_BANK) {
			if (txnType == TxnTypeConstant.TXN_TYPE_BANK_TO_BANK) {
				row += "<td width='" + (19 + extraColumnWidth) + "%'>" + paymentCache.getPaymentBankName(txn.getBankId()) + ' TO ' + paymentCache.getPaymentBankName(txn.getToBankId()) + '</td>';
			} else {
				row += "<td width='" + (19 + extraColumnWidth) + "%'>" + paymentCache.getPaymentBankName(txn.getBankId()) + '</td>';
			}
		} else if (txnType == TxnTypeConstant.TXN_TYPE_CASH_ADJ_ADD || txnType == TxnTypeConstant.TXN_TYPE_CASH_ADJ_REDUCE) {
			row += "<td width='" + (19 + extraColumnWidth) + "%'> Cash Adjustment </td>";
		} else if (txnType == TxnTypeConstant.TXN_TYPE_CHEQUE_TRANSFER) {
			var name = txn.getNameRef().getFullName();
			if (txn.getTransferedTobankId()) {
				row += "<td width='" + (19 + extraColumnWidth) + "%'> Cheque to " + paymentCache.getPaymentBankName(txn.getTransferedTobankId()) + '</td>';
			} else {
				row += "<td width='" + (19 + extraColumnWidth) + "%'>" + name + '</td>';
			}
		} else if (TxnTypeConstant.isLoanTxnType(txnType)) {
			var _name = txn.getDescription();
			row += "<td width='" + (19 + extraColumnWidth) + "%'>" + _name + '</td>';
		} else {
			var _name2 = txn.getNameRef().getFullName();
			row += "<td width='" + (19 + extraColumnWidth) + "%'>" + _name2 + '</td>';
		}

		// row for reference number
		if (isTransactionRefNumberEnabled) {
			row += "<td width='12%'>" + refNo + '</td>';
		}

		// row for total

		row += "<td width='" + (15 + extraColumnWidth) + "%'>" + typeString + "</td><td width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalAmt) + '</td>';

		// rows for money in and money out
		if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_OTHER_INCOME || txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD_WITHOUT_CASH || txnType == TxnTypeConstant.TXN_TYPE_CASH_ADJ_ADD) {
			totalCashIn += cashAmt;
			row += "<td width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(cashAmt) + "</td><td width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'></td>";
		} else if (txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT || txnType == TxnTypeConstant.TXN_TYPE_EXPENSE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE_WITHOUT_CASH || txnType == TxnTypeConstant.TXN_TYPE_CASH_ADJ_REDUCE) {
			totalCashOut += cashAmt;
			row += "<td width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'></td><td width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(cashAmt) + '</td>';
		} else if (TxnTypeConstant.isLoanTxnType(txnType)) {
			if (txnType == TxnTypeConstant.TXN_TYPE_LOAN_STARTING_BALANCE || txnType == TxnTypeConstant.TXN_TYPE_LOAN_ADJUSTMENT) {
				totalCashIn += cashAmt;
				row += "<td width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(cashAmt) + "</td><td width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'></td>";
			} else {
				totalCashOut += cashAmt;
				row += "<td width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'></td><td width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(cashAmt) + '</td>';
			}
		} else {
			row += "<td width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'></td><td width='" + (18 + extraColumnWidth) + "%' class='tableCellTextAlignRight'></td>";
		}

		rowData.push(row);
	}
	var data = rowData;
	if ($('#dayBookContainer').length === 0) {
		return;
	}
	if (dayBookClusterize && dayBookClusterize.destroy) {
		dayBookClusterize.clear();
		dayBookClusterize.destroy();
	}

	dayBookClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollArea',
		contentId: 'contentArea',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});
	$('#tableHead').html(dynamicRow);
	$('#totalCashInAmount').html(MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalCashIn));
	$('#totalCashOutAmount').html(MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalCashOut));
	$('#diffCashInCashOut').html(MyDouble.getBalanceAmountWithDecimalAndCurrency(totalCashIn - totalCashOut, true));
};

$('#tableHead').on('click', '#invoiceColId', function (e) {
	var SortHelper = require('./../Utilities/SortHelper.js');
	var sortHelper = new SortHelper();
	listOfTransactions = sortHelper.getSortedListByInvoice(listOfTransactions, 'txnRefNumber');
	displayTransactionList(listOfTransactions);
});

var openTransaction = function openTransaction(txnId, txnType) {
	var ViewTransaction = require('./../BizLogic/ViewTransaction.js');
	var viewTransaction = new ViewTransaction();
	viewTransaction.viewTransactionFile(txnId + ':' + TxnTypeConstant.getTxnType(txnType));
};

if (settingCache.getItemEnabled()) {
	$('#itemDetailPrintDiv').show();
} else {
	$('#itemDetailPrintDiv').hide();
}

var getHTMLTextForReport = function getHTMLTextForReport() {
	var DayBookReportHTMLGenerator = require('./../ReportHTMLGenerator/DayBookReportHTMLGenerator.js');
	var dayBookReportHTMLGenerator = new DayBookReportHTMLGenerator();
	var startDateString = $('#startDate').val();
	var firmId = Number($('#firmFilterOptions').val());
	var printItemDetails = $('#printItems').is(':checked');
	var printDescription = $('#printDescription').is(':checked');
	var htmlText = dayBookReportHTMLGenerator.getHTMLText(listOfTransactions, startDateString, firmId, printItemDetails, printDescription);
	return htmlText;
};

var printTransaction = function printTransaction(that) {
	var settingCache = new SettingCache();
	var ThermalPrinterConstant = require('./../Constants/ThermalPrinterConstant.js');
	if (settingCache.getDefaultPrinterType() == ThermalPrinterConstant.THERMAL_PRINTER) {
		var txnId = that.id.split(':')[0];
		var PrintUtil = require('./../Utilities/PrintUtil.js');
		PrintUtil.printTransactionUsingThermalPrinter(txnId);
	} else {
		var html = transactionPDFHandler.transactionToHTML(that.id);
		if (html) {
			pdfHandler.print(html);
		}
	}
};

$('#currentAccountTransactionDetails').contextmenu({
	delegate: '.currentRow',
	addClass: 'width10',
	menu: [{ title: 'Open PDF', cmd: 'openpdf' }, { title: '----' }, { title: 'Print', cmd: 'print' }, { title: '----' }, { title: 'Delete', cmd: 'delete' }],
	select: function select(event, ui) {
		var id = ui.target[0].parentNode.id;
		if (ui.cmd == 'print') {
			printTransaction(ui.target[0].parentNode);
		} else if (ui.cmd == 'openpdf') {
			openPdfFromRightClickMenu(ui.target[0].parentNode);
		} else if (ui.cmd == 'delete') {
			var DeleteTransaction = require('./../BizLogic/DeleteTransaction.js');
			var deleteTransaction = new DeleteTransaction();
			deleteTransaction.RemoveTransaction(id);
		}
	}
});

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	// $('#frameDiv').load('');
	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable();
};

/// ///////////////////////////PDF/////////////////////////////////////

function openPrintOptions() {
	$('#dayBookPrintDialog').removeClass('hide');
	$('#dayBookPrintDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closePrintOptions() {
	$('#dayBookPrintDialog').addClass('hide');
	$('#dayBookPrintDialog').dialog('close').dialog('destroy');
}

var openPdfFromRightClickMenu = function openPdfFromRightClickMenu(that) {
	$('#loading').show(function () {
		var html = transactionPDFHandler.transactionToHTML(that.id);
		if (html) {
			pdfHandler.openPDF(html, false);
		}
	});
};

var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.DAYBOOK, fromDate: $('#startDate').val() });
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#dayBookPrintDialog').addClass('hide');
	$('#dayBookPrintDialog').dialog('close').dialog('destroy');
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

if (settingCache.getItemEnabled()) {
	$('#itemDetailsExcel').show();
} else {
	$('#itemDetailsExcel').hide();
}

function openExcelOptions() {
	$('#ExcelDialog').removeClass('hide');
	$('#ExcelDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closeExcelOptions() {
	$('#ExcelDialog').addClass('hide');
	$('#ExcelDialog').dialog('close').dialog('destroy');
}

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.DAYBOOK, fromDate: $('#startDate').val() });
	excelHelper.saveExcel(fileName);
};

var prepareObjectForExcel = function prepareObjectForExcel(listOfTransactions) {
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var PaymentCache = require('./../Cache/PaymentInfoCache.js');
	var paymentCache = new PaymentCache();
	var isTransactionRefNumberEnabled = settingCache.getTxnRefNoEnabled();
	var ExcelDescription = $('#ExcelDaybookDescription').is(':checked');
	var len = listOfTransactions.length;
	var rowObj = [];
	var tableHeadArray = [];
	var totalArray = [];
	var diffArray = [];
	tableHeadArray.push('Name');
	if (isTransactionRefNumberEnabled) {
		tableHeadArray.push('Ref No');
		totalArray.push('');
		diffArray.push('');
	}
	tableHeadArray.push('Type');
	tableHeadArray.push('Total');
	tableHeadArray.push('Money In');
	tableHeadArray.push('Money Out');
	if (ExcelDescription) {
		tableHeadArray.push('Description');
	}
	totalArray.push('');
	diffArray.push('');
	rowObj.push(tableHeadArray);
	rowObj.push([]);

	var totalAmount = 0;
	var totalmoneyInAmount = 0;
	var totalmoneyOutAmount = 0;
	for (var j = 0; j < len; j++) {
		var tempArray = [];
		var txn = listOfTransactions[j];
		var txnSubType = txn.getTxnSubType();
		var txnType = txn.getTxnType();
		var typeString = TxnTypeConstant.getTxnTypeForUI(txnType, txnSubType);
		var cashAmt = txn.getCashAmount() ? txn.getCashAmount() : 0;
		var balanceAmt = txn.getBalanceAmount() ? txn.getBalanceAmount() : 0;
		var totalAmt = 0;
		var moneyIn = '';
		var moneyOut = '';
		var discountAmt = txn.getDiscountAmount() ? txn.getDiscountAmount() : 0;
		if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
			totalAmt = Number(cashAmt) + Number(discountAmt);
		} else {
			totalAmt = Number(cashAmt) + Number(balanceAmt);
		}

		if (txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD || txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE || txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD_WITHOUT_CASH || txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE_WITHOUT_CASH || txnType == TxnTypeConstant.TXN_TYPE_BANK_TO_BANK) {
			if (txnType == TxnTypeConstant.TXN_TYPE_BANK_TO_BANK) {
				tempArray.push(paymentCache.getPaymentBankName(txn.getBankId()) + ' TO ' + paymentCache.getPaymentBankName(txn.getToBankId()));
			} else {
				tempArray.push(paymentCache.getPaymentBankName(txn.getBankId()));
			}
		} else if (txnType == TxnTypeConstant.TXN_TYPE_CASH_ADJ_ADD || txnType == TxnTypeConstant.TXN_TYPE_CASH_ADJ_REDUCE) {
			tempArray.push('Cash Adjustment');
		} else if (txnType == TxnTypeConstant.TXN_TYPE_CHEQUE_TRANSFER) {
			var name = txn.getNameRef().getFullName();
			if (txn.getTransferedTobankId()) {
				tempArray.push('Cheque to ' + paymentCache.getPaymentBankName(txn.getTransferedTobankId()));
			} else {
				tempArray.push(name);
			}
		} else if (TxnTypeConstant.isLoanTxnType(txnType)) {
			tempArray.push(txn.getDescription());
		} else {
			var name = txn.getNameRef().getFullName();
			tempArray.push(name);
		}

		if (isTransactionRefNumberEnabled) {
			var refNo = txn.getTxnRefNumber() === '' ? '' : txn.getFullTxnRefNumber();
			tempArray.push(refNo);
		}

		tempArray.push(typeString);

		tempArray.push(MyDouble.getAmountWithDecimal(totalAmt));

		if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_OTHER_INCOME || txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD_WITHOUT_CASH || txnType == TxnTypeConstant.TXN_TYPE_CASH_ADJ_ADD) {
			moneyIn = cashAmt;
			totalmoneyInAmount += moneyIn;
		} else if (txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT || txnType == TxnTypeConstant.TXN_TYPE_EXPENSE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER || txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE_WITHOUT_CASH || txnType == TxnTypeConstant.TXN_TYPE_CASH_ADJ_REDUCE) {
			moneyOut = cashAmt;
			totalmoneyOutAmount += moneyOut;
		} else if (TxnTypeConstant.isLoanTxnType(txnType)) {
			if (txnType == TxnTypeConstant.TXN_TYPE_LOAN_STARTING_BALANCE || txnType == TxnTypeConstant.TXN_TYPE_LOAN_ADJUSTMENT) {
				totalmoneyInAmount += cashAmt;
				moneyIn = cashAmt;
			} else {
				totalmoneyOutAmount += cashAmt;
				moneyOut = cashAmt;
			}
		}

		tempArray.push(MyDouble.getAmountWithDecimal(moneyIn));
		tempArray.push(MyDouble.getAmountWithDecimal(moneyOut));
		if (ExcelDescription) {
			!TxnTypeConstant.isLoanTxnType(txnType) && tempArray.push(txn.getDescription());
		}
		rowObj.push(tempArray);
		totalAmount += totalAmt;
	}
	rowObj.push([]);
	totalArray.push('Total');
	totalArray.push('');
	totalArray.push(MyDouble.getAmountWithDecimal(totalmoneyInAmount));
	totalArray.push(MyDouble.getAmountWithDecimal(totalmoneyOutAmount));
	rowObj.push(totalArray);
	rowObj.push([]);
	diffArray.push('');
	diffArray.push('');
	diffArray.push('Total Money In - Total Money Out');
	diffArray.push(MyDouble.getAmountWithDecimal(totalmoneyInAmount - totalmoneyOutAmount));
	rowObj.push(diffArray);
	return rowObj;
};

function printExcel() {
	closeExcelOptions();
	var XLSX = require('xlsx');
	var dataArray = prepareObjectForExcel(listOfTransactions);
	var worksheet = XLSX.utils.aoa_to_sheet(dataArray);
	var ExcelItemDetails = $('#ExcelDaybookItemDetails').is(':checked');
	var ExcelExportHelper = require('./../UIControllers/ExcelExportHelper.js');
	var excelExportHelper = new ExcelExportHelper();

	var workbook = {
		'SheetNames': [],
		'Sheets': {}
	};

	var wsName = 'Day Book';
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet;
	var wscolWidth = [{ wch: 30 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	worksheet['!cols'] = wscolWidth;

	if (ExcelItemDetails) {
		var wsNameItem = 'Item Details';
		workbook.SheetNames[1] = wsNameItem;
		var itemArray = excelExportHelper.prepareItemObjectForExcel(listOfTransactions);
		var itemWorksheet = XLSX.utils.aoa_to_sheet(itemArray);
		workbook.Sheets[wsNameItem] = itemWorksheet;
		var wsItemcolWidth = [{ wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }];
		itemWorksheet['!cols'] = wsItemcolWidth;
	}
	console.log(workbook);
	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

populateTable();