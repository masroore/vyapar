
function saveNewInfo() {
	console.log(id);
	console.log(bank);
	var accountName = document.getElementById('accountName').value.trim();
	var bankName = document.getElementById('bankName').value.trim();
	bankName = bankName ? bankName.trim() : '';
	var accountNumber = document.getElementById('accountNumber').value.trim().toString();;
	accountNumber = accountNumber || '';
	var curBal = MyDouble.convertStringToDouble(document.getElementById('curBal').value.trim());
	var date = document.getElementById('date').value;

	if (!accountName) {
		ToastHelper.error('Display Name Cannot Be Empty');
		return;
	}

	var PaymentInfo = require('./../BizLogic/PaymentInfo.js');
	var paymentinfo = new PaymentInfo();

	var ErrorCode = require('./../Constants/ErrorCode.js');

	var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
	var paymentinfocache = new PaymentInfoCache();

	var statusCode = ErrorCode.SUCCESS;

	paymentinfo.setName(accountName);
	paymentinfo.setAccountNumber(accountNumber);
	if (!id || bank != accountName) {
		if (paymentinfocache.paymentInfoExists(accountName) > -1) {
			ToastHelper.error(ErrorCode.ERROR_BANK_DISPLAY_NAME_EXIST);
			return;
		}
	}
	paymentinfo.setBankName(bankName);
	paymentinfo.setType('BANK');
	if (curBal) {
		paymentinfo.setOpeningBalance(curBal);
	}

	var GetDate = require('./../BizLogic/GetDate.js');
	var getDate = new GetDate();
	date = getDate.getDateObj(date);
	paymentinfo.setOpeningDate(date);
	if (id > 0) {
		paymentinfo.setId(id);
		statusCode = paymentinfo.updateInfo();
	} else {
		statusCode = paymentinfo.saveNewInfo();
	}

	if (statusCode != ErrorCode.ERROR_NEW_BANK_INFO_SUCCESS && statusCode != ErrorCode.ERROR_UPDATE_BANK_INFO_SUCCESS) {
		ToastHelper.error(statusCode);
	} else {
		ToastHelper.success(statusCode);
		console.log(statusCode);
	}
}