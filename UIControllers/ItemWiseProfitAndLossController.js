var startDate;
var endDate;
var DateFormat = require('./../Constants/DateFormat.js');
$('#startDate, #endDate').datepicker({ dateFormat: DateFormat.format });
var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();
var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var ItemCache = require('./../Cache/ItemCache.js');
var itemCache = new ItemCache();
var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');

var printForSharePDF = false;

var listOfTransactions = [];

var populateTable = function populateTable() {
	var DataLoader = require('./../DBManager/DataLoader.js');
	var dataLoader = new DataLoader();
	var showOnlySale = false;
	startDateString = $('#startDate').val();
	endDateString = $('#endDate').val();
	startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
	endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');
	showOnlySale = $('#showOnlySale').prop('checked');
	var showOnlyActiveItems = true;
	if ($('#showInactiveItems').is(':checked')) {
		showOnlyActiveItems = false;
	}
	CommonUtility.showLoader(function () {
		listOfTransactions = dataLoader.getItemWiseProfitAndLossReportObjectList(startDate, endDate, showOnlySale, showOnlyActiveItems);
		displayTransactionList(listOfTransactions);
	});
};

var date = MyDate.getDate('d/m/y');
$('#startDate').val(MyDate.getFirstDateOfCurrentMonthString());
$('#endDate').val(date);

if (settingCache.getMultipleFirmEnabled()) {
	var optionTemp = document.createElement('option');
	var FirmCache = require('./../Cache/FirmCache.js');
	var firmCache = new FirmCache();
	var firmList = firmCache.getFirmList();
	for (var _i in firmList) {
		var option = optionTemp.cloneNode();
		option.text = firmList[_i].getFirmName();
		option.value = firmList[_i].getFirmId();
		$('#firmFilterOptions').append(option);
	}
	$('#firmChooser').show();
} else {
	$('#firmChooser').hide();
}

if (itemCache.IsAnyItemInactive()) {
	$('#activeInactiveItems').removeClass('hide');
} else {
	$('#activeInactiveItems').addClass('hide');
}

$('#showOnlySale').on('change', function (event) {
	populateTable();
});

$('#startDate').change(function () {
	populateTable();
});

$('#endDate').change(function () {
	populateTable();
});

$(function () {
	$('#name').autocomplete($.extend({}, UIHelper.getAutocompleteDefaultOptions(itemCache.getListOfItems()), {
		select: function select(event, ui) {
			$('#name').val(ui.item.value);
			populateTable();
		}
	}));
});

var optionTemp = document.createElement('option');
var transactionListArray = ['Sale', 'Purchase', 'Payment-In', 'Payment-Out', 'Credit Note', 'Debit Note', 'Sale Order', 'Purchase Order', 'Estimate', 'Delivery Challan'];
var transactionListId = [TxnTypeConstant.TXN_TYPE_SALE, TxnTypeConstant.TXN_TYPE_PURCHASE, TxnTypeConstant.TXN_TYPE_CASHIN, TxnTypeConstant.TXN_TYPE_CASHOUT, TxnTypeConstant.TXN_TYPE_SALE_RETURN, TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN, TxnTypeConstant.TXN_TYPE_SALE_ORDER, TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER, TxnTypeConstant.TXN_TYPE_ESTIMATE, TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN];
// var len = transactionsList.length;
for (i in transactionListArray) {
	var option = optionTemp.cloneNode();
	option.text = transactionListArray[i];
	option.value = transactionListId[i];
	$('#transactionFilterOptions').append(option);
}

$('#name').on('change', function (event) {
	populateTable();
});

$('#transactionFilterOptions').on('selectmenuchange', function () {
	populateTable();
});

var itemwiseProfitAndLossClusterize;

var displayTransactionList = function displayTransactionList(listOfTransactions) {
	var len = listOfTransactions.length;
	var rowData = [];
	var dynamicRow = '';
	dynamicRow += "<thead><tr><th width='12%'>Item Name</th>" + "<th width='10%' class='tableCellTextAlignRight'>Sale</th>" + "<th width='10%' class='tableCellTextAlignRight'>Cr. Note / Sale return</th>" + "<th width='10%' class='tableCellTextAlignRight'>Purchase</th>" + "<th width='10%' class='tableCellTextAlignRight'>Dr. Note / Purchase return</th>" + "<th width='10%' class='tableCellTextAlignRight'>Opening Stock</th>" + "<th width='10%' class='tableCellTextAlignRight'>Closing Stock</th>" + "<th width='9%' class='tableCellTextAlignRight'>Tax Receivable</th>" + "<th width='9%' class='tableCellTextAlignRight'>Tax Payable</th>" + "<th width='10%' class='tableCellTextAlignRight'>Net Profit/Loss</th>" + '</tr></thead>';
	//
	var totalNetAmount = 0;
	for (var _i2 = 0; _i2 < len; _i2++) {
		var row = '';
		var txn = listOfTransactions[_i2];
		var item = txn.getItemName();
		var saleVal = txn.getSaleValue();
		var saleRetVal = txn.getSaleReturnValue();
		var purchaseVal = txn.getPurchaseValue();
		var purchaseReturnValue = txn.getPurchaseReturnValue();
		var openingStock = txn.getOpeningStockValue();
		var closingStock = txn.getClosingStockValue();
		txn.calculateProfitAndLossAmount();
		var taxReceivable = txn.getReceivableTax();
		var taxPayable = txn.getPayableTax();
		var netAmount = txn.getNetProfitAndLossAmount();
		row += "<tr class='currentRow'><td width='12%'>" + item + '</td>' + "<td width='10%' class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimal(saleVal) + '</td>' + "<td width='10%' class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimal(saleRetVal) + '</td>' + "<td width='10%' class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimal(purchaseVal) + '</td>' + "<td width='10%' class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimal(purchaseReturnValue) + '</td>' + "<td width='10%' class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimal(openingStock) + '</td>' + "<td width='10%' class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimal(closingStock) + '</td>' + "<td width='9%' class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimal(taxReceivable) + '</td>' + "<td width='9%' class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimal(taxPayable) + '</td>' + "<td width='10%' class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimalAndCurrency(netAmount, true) + '</td></tr>';
		rowData.push(row);
		totalNetAmount += MyDouble.convertStringToDouble(netAmount);
	}
	var data = rowData;

	if ($('#itemWiseProfitLossContainer').length === 0) {
		return;
	}
	if (itemwiseProfitAndLossClusterize && itemwiseProfitAndLossClusterize.destroy) {
		itemwiseProfitAndLossClusterize.clear();
		itemwiseProfitAndLossClusterize.destroy();
	}

	itemwiseProfitAndLossClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollArea',
		contentId: 'contentArea',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});

	$('#tableHead').html(dynamicRow);
	$('#totalPAndLAmount').html(MyDouble.getBalanceAmountWithDecimalAndCurrency(totalNetAmount, true));
};

var openTransaction = function openTransaction(txnId, txnType) {
	var ViewTransaction = require('./../BizLogic/ViewTransaction.js');
	var viewTransaction = new ViewTransaction();
	viewTransaction.viewTransactionFile(txnId + ':' + TxnTypeConstant.getTxnType(txnType));
};

if (settingCache.getItemEnabled()) {
	$('#itemDetailPrintDiv').show();
} else {
	$('#itemDetailPrintDiv').hide();
}

function openPrintOptions(sharePDF) {
	printForSharePDF = sharePDF;
	printReport();
}

function closePrintOptions() {
	$('#customPrintDialog').addClass('hide');
	$('#customPrintDialog').dialog('close').dialog('destroy');
}

function printReport() {
	if (printForSharePDF) {
		pdfHandler.sharePdf(getHTMLTextForReport(), 'Report', false);
	} else {
		pdfHandler.printPdf(getHTMLTextForReport(), 'Report', false);
	}
	// closePrintOptions();
}

var getHTMLTextForReport = function getHTMLTextForReport() {
	var ItemWiseProfitAndLossGenerator = require('./../ReportHTMLGenerator/ItemWiseProfitAndLossReportGenerator.js');
	var itemWiseProfitAndLossGenerator = new ItemWiseProfitAndLossGenerator();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var showOnlySale = $('#showOnlySale').prop('checked');
	var htmlText = itemWiseProfitAndLossGenerator.getHTMLText(listOfTransactions, startDateString, endDateString, showOnlySale);
	return htmlText;
};

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	// $('#frameDiv').load('');
	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable();
};

/// ///////////////////////////PDF/////////////////////////////////////
var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.ITEM_WISE_PROFIT_AND_LOSS_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

if (settingCache.getItemEnabled()) {
	$('#itemDetailsExcel').show();
} else {
	$('#itemDetailsExcel').hide();
}

function openExcelOptions() {
	$('#ExcelDialog').removeClass('hide');
	$('#ExcelDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closeExcelOptions() {
	$('#ExcelDialog').addClass('hide');
	$('#ExcelDialog').dialog('close').dialog('destroy');
}

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.ITEM_WISE_PROFIT_AND_LOSS_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
	excelHelper.saveExcel(fileName);
};

var prepareObjectForExcel = function prepareObjectForExcel(listOfTransactions) {
	var len = listOfTransactions.length;
	var rowObj = [];
	var tableHeadArray = [];
	var totalArray = [];
	tableHeadArray.push('Item Name');
	tableHeadArray.push('Sale Amount');
	tableHeadArray.push('Cr. Note / Sale Return Amount');
	tableHeadArray.push('Purchase Amount');
	tableHeadArray.push('Dr. Note / Purchase Return Amount');
	tableHeadArray.push('Opening Stock');
	tableHeadArray.push('Closing Stock');
	tableHeadArray.push('Tax Receivable');
	tableHeadArray.push('Tax Payable');
	tableHeadArray.push('Net Profit/Loss');
	rowObj.push(tableHeadArray);
	rowObj.push([]);

	var totalNetAmount = 0;
	for (var j = 0; j < len; j++) {
		var tempArray = [];
		var txn = listOfTransactions[j];
		var item = txn.getItemName();
		var saleVal = txn.getSaleValue();
		var saleRetVal = txn.getSaleReturnValue();
		var purchaseVal = txn.getPurchaseValue();
		var purchaseReturnValue = txn.getPurchaseReturnValue();
		var openingStock = txn.getOpeningStockValue();
		var closingStock = txn.getClosingStockValue();
		var taxReceivable = txn.getReceivableTax();
		var taxPayable = txn.getPayableTax();

		txn.calculateProfitAndLossAmount();
		var netAmount = txn.getNetProfitAndLossAmount();
		tempArray.push(item);
		tempArray.push(MyDouble.getBalanceAmountWithDecimal(saleVal));
		tempArray.push(MyDouble.getBalanceAmountWithDecimal(saleRetVal));
		tempArray.push(MyDouble.getBalanceAmountWithDecimal(purchaseVal));
		tempArray.push(MyDouble.getBalanceAmountWithDecimal(purchaseReturnValue));
		tempArray.push(MyDouble.getBalanceAmountWithDecimal(openingStock));
		tempArray.push(MyDouble.getBalanceAmountWithDecimal(closingStock));
		tempArray.push(MyDouble.getBalanceAmountWithDecimal(taxReceivable));
		tempArray.push(MyDouble.getBalanceAmountWithDecimal(taxPayable));
		tempArray.push(MyDouble.getBalanceAmountWithDecimal(netAmount));
		totalNetAmount += MyDouble.convertStringToDouble(netAmount);
		rowObj.push(tempArray);
	}
	rowObj.push([]);
	rowObj.push(['Total', '', '', '', '', '', '', '', '', MyDouble.getBalanceAmountWithDecimal(totalNetAmount)]);
	return rowObj;
};

function printExcel() {
	var XLSX = require('xlsx');
	var dataArray = prepareObjectForExcel(listOfTransactions);
	var worksheet = XLSX.utils.aoa_to_sheet(dataArray);

	var workbook = {
		'SheetNames': [],
		'Sheets': {}
	};

	var wsName = 'Item Wise Profit And Loss';
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet;
	var wscolWidth = [{ wch: 40 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 15 }, { wch: 15 }];
	worksheet['!cols'] = wscolWidth;
	console.log(workbook);
	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

populateTable();

$('#showInactiveItems').on('change', function () {
	populateTable();
});