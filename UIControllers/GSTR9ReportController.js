var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var startDate;
var endDate;
var DateFormat = require('./../Constants/DateFormat.js');
var MyString = require('./../Utilities/MyString.js');
var NameCache = require('./../Cache/NameCache.js');
var nameCache = new NameCache();
var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();
var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var TaxCodeConstants = require('./../Constants/TaxCodeConstants.js');
var GSTRReportHelper = require('./../UIControllers/GSTRReportHelper.js');
var CompositeSchemeTaxRate = require('./../Constants/CompositeSchemeTaxRate.js');
var CompositeUserType = require('./../Constants/CompositeUserType.js');
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var FirmCache = require('./../Cache/FirmCache.js');
var firmCache = new FirmCache();

var firmId;
var gstin;
var fyVal;
var GSTR9ReportHelper = require('./../UIControllers/GSTR9ReportHelper.js');
var gSTR9ReportHelper = new GSTR9ReportHelper();
var firmName;
var startDateString;
var endDateString;

$('.monthYearPicker').datepicker({
	changeMonth: true,
	changeYear: true,
	showButtonPanel: true,
	dateFormat: 'mm/yy'
}).focus(function () {
	var thisCalendar = $(this);
	$('.ui-datepicker-calendar').detach();
	$('.ui-datepicker-close').click(function () {
		var month = $('#ui-datepicker-div .ui-datepicker-month :selected').val();
		var year = $('#ui-datepicker-div .ui-datepicker-year :selected').val();
		thisCalendar.datepicker('setDate', new Date(year, month));
		populateTable();
	});
});

var fyDateMap = new _map2.default();
fyDateMap.set(1, ['01/04/2017', '31/03/2018']);
fyDateMap.set(2, ['01/04/2018', '31/03/2019']);
fyDateMap.set(3, ['01/04/2019', '31/03/2020']);
fyDateMap.set(4, ['01/04/2020', '31/03/2021']);

var fyDateMapUI = new _map2.default();
fyDateMapUI.set(1, ['2017', '2018']);
fyDateMapUI.set(2, ['2018', '2019']);
fyDateMapUI.set(3, ['2019', '2020']);
fyDateMapUI.set(4, ['2020', '2021']);

var populateTable = function populateTable() {
	nameCache = new NameCache();
	settingCache = new SettingCache();

	fyVal = Number($('#fyPicker').val());

	startDateString = fyDateMap.get(fyVal)[0];
	endDateString = fyDateMap.get(fyVal)[1];;
	var startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
	var endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');

	var considerNonTaxAsExemptedCheck = $('#considerNonTaxAsExempted').prop('checked');
	firmId = Number($('#firmFilterOptions').val());
	var firm = void 0;
	if (firmId == 0) {
		firm = firmCache.getFirmById(settingCache.getDefaultFirmId());
	} else {
		firm = firmCache.getFirmById(firmId);
	}
	firmName = firm.getFirmName();
	gstin = firmCache.getFirmGSTINByFirmName(firmName);

	CommonUtility.showLoader(function () {
		gSTR9ReportHelper.setGSTR9DataListBasedOnDate(startDate, endDate, firmId, considerNonTaxAsExemptedCheck);
		displayTransactionList();
	});
};

var date = MyDate.getDate('m/y');
$('#startDate').val(date);
$('#endDate').val(date);

if (settingCache.getMultipleFirmEnabled()) {
	var optionTemp = document.createElement('option');
	var firmList = firmCache.getFirmList();
	for (i in firmList) {
		var option = optionTemp.cloneNode();
		option.text = firmList[i].getFirmName();
		option.value = firmList[i].getFirmId();
		$('#firmFilterOptions').append(option);
	}
	$('#firmChooser').show();
} else {
	$('#firmChooser').hide();
}

$('#firmFilterOptions').on('selectmenuchange', function (event) {
	populateTable();
});

$('#fyPicker').change(function () {
	populateTable();
});

$('#considerNonTaxAsExempted').change(function () {
	populateTable();
});

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	// $('#frameDiv').load('');
	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}

	populateTable();
};

var displayTransactionList = function displayTransactionList() {
	var subTotalHtaxable_value = gSTR9ReportHelper.sale_purchase_without_reverse_charge_unregistered_taxable_value + gSTR9ReportHelper.sale_purchase_without_reverse_charge_registered_taxable_value + gSTR9ReportHelper.purchase_reverse_charge_taxable_value;
	var subtotalH_cgst = gSTR9ReportHelper.sale_purchase_without_reverse_charge_unregistered_cgst + gSTR9ReportHelper.sale_purchase_without_reverse_charge_registered_cgst + gSTR9ReportHelper.purchase_reverse_charge_cgst;
	var subtotalH_sgst = gSTR9ReportHelper.sale_purchase_without_reverse_charge_unregistered_sgst + gSTR9ReportHelper.sale_purchase_without_reverse_charge_registered_sgst + gSTR9ReportHelper.purchase_reverse_charge_sgst;
	var subtotalH_igst = gSTR9ReportHelper.sale_purchase_without_reverse_charge_unregistered_igst + gSTR9ReportHelper.sale_purchase_without_reverse_charge_registered_igst + gSTR9ReportHelper.purchase_reverse_charge_igst;
	var subtotalH_cess = gSTR9ReportHelper.sale_purchase_without_reverse_charge_unregistered_cess + gSTR9ReportHelper.sale_purchase_without_reverse_charge_registered_cess + gSTR9ReportHelper.purchase_reverse_charge_cess;

	var subtotalM_taxable_value = -gSTR9ReportHelper.sale_return_registered_taxable_value;
	var subtotalM_cgst = -gSTR9ReportHelper.sale_return_registered_cgst;
	var subtotalM_sgst = -gSTR9ReportHelper.sale_return_registered_sgst;
	var subtotalM_igst = -gSTR9ReportHelper.sale_return_registered_igst;
	var subtotalM_cess = -gSTR9ReportHelper.sale_return_registered_cess;

	var subtotalN_taxable_value = subTotalHtaxable_value + subtotalM_taxable_value;
	var subtotalN_cgst = subtotalH_cgst + subtotalM_cgst;
	var subtotalN_sgst = subtotalH_sgst + subtotalM_sgst;
	var subtotalN_igst = subtotalH_igst + subtotalM_igst;
	var subtotalN_cess = subtotalH_cess + subtotalM_cess;

	var row = '<table class="tabTxnTable width100" ><thead>\n        <tr style=\'background-color: darkgray;color:#4a4141;\'><th class=\'tableCellTextAlignCenter width5\'>Pt. I</th><th class=\'tableCellTextAlignCenter width95\' colspan="2">Basic Details</th></tr>\n        </thead><tbody>\n        <tr><td class="noBorderBottom tableCellTextAlignCenter width5">1</td><td class="width30">Financial Year</td><td class="width65">' + fyDateMapUI.get(fyVal)[0] + '-' + fyDateMapUI.get(fyVal)[1] + '</td></tr>\n        <tr><td class="noBorderBottom tableCellTextAlignCenter width5">2</td><td class="width30">GSTIN</td><td class="width65">' + gstin + '</td></tr>\n        <tr><td class="noBorderBottom tableCellTextAlignCenter width5">3A</td><td class="width30">Legal Name</td><td class="width65">' + firmName + '</td></tr>\n        <tr><td class="noBorderBottom tableCellTextAlignCenter width5">3B</td><td class="width30">Trade Name (if any)</td><td class="width65"></td></tr>\n        </tbody></table>';

	row += '<table class="tabTxnTable width100" style="margin-top: 5%;"><thead>\n        <tr style=\'background-color: darkgray;color:#4a4141;\'><th class=\'tableCellTextAlignCenter width5\'>Pt. II</th><th class=\'tableCellTextAlignCenter width95\' colspan="6">Details of Outward and inward supplies declared during the financial year</th></tr>\n        </thead><tbody>\n        <tr><td rowspan="3" class="width5"></td><td class="width60" colspan="2"></td><td class="tableCellTextAlignCenter width35" colspan="4">(Amount in Rupees in All Tables)</td></tr>\n        <td class="tableCellTextAlignCenter width40">Nature of Supplies</td><td class="tableCellTextAlignCenter width20">Taxable Value</td><td class="tableCellTextAlignCenter width9">Central Tax</td><td class="tableCellTextAlignCenter width9">State Tax/ UT Tax</td><td class="tableCellTextAlignCenter width9">Integrated Tax</td><td class="tableCellTextAlignCenter width8">Cess</td></tr>\n        <td class="tableCellTextAlignCenter">1</td><td class="tableCellTextAlignCenter">2</td><td class="tableCellTextAlignCenter">3</td><td class="tableCellTextAlignCenter">4</td><td class="tableCellTextAlignCenter">5</td><td class="tableCellTextAlignCenter">6</td></tr>';

	row += '<tr style=\'background-color: lightgray;\'><td class="tableCellTextAlignCenter">4</td><td colspan="6" class="tableCellTextAlignCenter">Details of advances, inward and outward supplies on which tax is payable as declared in returns filed during the financial year</td></tr> \n        <tr><td class="tableCellTextAlignCenter">A</td><td>Supplies made to un-registered persons(B2C)</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_purchase_without_reverse_charge_unregistered_taxable_value) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_purchase_without_reverse_charge_unregistered_cgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_purchase_without_reverse_charge_unregistered_sgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_purchase_without_reverse_charge_unregistered_igst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_purchase_without_reverse_charge_unregistered_cess) + '</td></tr>\n        <tr><td class="tableCellTextAlignCenter">B</td><td>Supplies made to registered persons(B2B)</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_purchase_without_reverse_charge_registered_taxable_value) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_purchase_without_reverse_charge_registered_cgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_purchase_without_reverse_charge_registered_sgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_purchase_without_reverse_charge_registered_igst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_purchase_without_reverse_charge_registered_cess) + '</td></tr>\n        <tr><td class="tableCellTextAlignCenter">C</td><td>Zero rated supply(Export) on payment of tax (except supplies to SEZs)</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">D</td><td>Supplies to SEZs on payment of tax</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">E</td><td>Deemed Exports</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">F</td><td>Advances on which tax has been paid but invoice has not been issued (not cover under (A) to (E) above)</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">G</td><td>Inward supplies on which tax is to be paid on reverse charge basis</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_reverse_charge_taxable_value) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_reverse_charge_cgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_reverse_charge_sgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_reverse_charge_igst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_reverse_charge_cess) + '</td></tr>\n        <tr><td class="tableCellTextAlignCenter">H</td><td>Sub-total (A to G above)</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(subTotalHtaxable_value) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(subtotalH_cgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(subtotalH_sgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(subtotalH_igst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(subtotalH_cess) + '</td></tr>\n        <tr><td class="tableCellTextAlignCenter">I</td><td>Credit Notes issued in respect of transactions specified in (B) to (E) above (-)</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_return_registered_taxable_value) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_return_registered_cgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_return_registered_sgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_return_registered_igst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_return_registered_cess) + '</td></tr>\n        <tr><td class="tableCellTextAlignCenter">J</td><td>Debit Notes issued in respect of  transactions specified in (B) to (E) above (+)</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">K</td><td>Supplies/tax declared through Amendments(+)</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">L</td><td>Supplies/tax reduced through Amendments(+)</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">M</td><td>Sub-total (I to L above)</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(subtotalM_taxable_value) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(subtotalM_cgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(subtotalM_sgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(subtotalM_igst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(subtotalM_cess) + '</td></tr>\n        <tr><td class="tableCellTextAlignCenter">N</td><td>Supplies and advances on which tax is to be paid (H+M) above</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(subtotalN_taxable_value) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(subtotalN_cgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(subtotalN_sgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(subtotalN_igst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(subtotalN_cess) + '</td></tr>';

	row += '<tr class="noBorderBottom" style="background-color:#ffffff"><td></td></tr>';

	row += '<tr style=\'background-color: lightgray;\'><td class="tableCellTextAlignCenter">5</td><td colspan="6" class="tableCellTextAlignCenter">Details of Outward supplies on which tax is not payable as declared in returns filed during the financial year</td></tr> \n        <tr><td class="tableCellTextAlignCenter">A</td><td>Zero rated supply (Export) without payment of tax</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">B</td><td>Supply to SEZs without payment of tax</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">C</td><td>Supplies on which tax is to be paid by the recipient on reverse charge basis</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">D</td><td>Exempted</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_exempted_taxable_value) + '</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">E</td><td>Nil Rated</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_nil_rated_taxable_value) + '</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">F</td><td>Non-GST supply</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">G</td><td>Sub-total (A to F above)</td><td class="tableCellTextAlignRight">' + (MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_exempted_taxable_value) + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_nil_rated_taxable_value)) + '</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">H</td><td>Credit Notes issued in respect of transactions specified in A to F above (-)</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_return_exempted_nil_rated_taxable_value) + '</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">I</td><td>Debit Notes issued in respect of transactions specified in A to F above (+)</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">J</td><td>Supplies declared through Amendments(+)</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">K</td><td>Supplies reduced through Amendments(+)</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">L</td><td>Sub-Total (H to K above)</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_return_exempted_nil_rated_taxable_value) * -1 + '</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">M</td><td>Turnover on which tax is not to be paid (G + L above)</td><td class="tableCellTextAlignRight">' + (MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_exempted_taxable_value) + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_nil_rated_taxable_value) - MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_return_exempted_nil_rated_taxable_value)) + '</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">N</td><td>Total Turnover (including advances) (4N + 5M - 4G above)</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(MyDouble.getAmountDecimalForGSTRReport(subtotalN_taxable_value) + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_exempted_taxable_value) + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_nil_rated_taxable_value) - MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_return_exempted_nil_rated_taxable_value) - MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_reverse_charge_taxable_value)) + '</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>';

	row += '</tbody></table>';

	row += '<table class="tabTxnTable width100" style="margin-top: 5%;"><thead>\n        <tr style=\'background-color: darkgray;color:#4a4141;\'><th class=\'tableCellTextAlignCenter width5\'>Pt. III</th><th class=\'tableCellTextAlignCenter width95\' colspan="6">Details of ITC as declared in returns filed during the financial year</th></tr>\n        </thead><tbody>\n        <tr><td rowspan="2" class="width5"></td><td class="tableCellTextAlignCenter width40">Description</td><td class="tableCellTextAlignCenter width20">Type</td><td class="tableCellTextAlignCenter width9">Central Tax</td><td class="tableCellTextAlignCenter width9">State Tax/ UT Tax</td><td class="tableCellTextAlignCenter width9">Integrated Tax</td><td class="tableCellTextAlignCenter width8">Cess</td></tr></tr>\n        <td class="tableCellTextAlignCenter">1</td><td class="tableCellTextAlignCenter">2</td><td class="tableCellTextAlignCenter">3</td><td class="tableCellTextAlignCenter">4</td><td class="tableCellTextAlignCenter">5</td><td class="tableCellTextAlignCenter">6</td></tr>';

	var subtotalCGSTForI = gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_inputs_cgst + gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_capital_goods_cgst + gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_input_services_cgst + gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_cgst + gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_cgst + gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_cgst + gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_inputs_cgst + gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_cgst + gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_input_services_cgst;
	var subtotalSGSTForI = gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_inputs_sgst + gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_capital_goods_sgst + gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_input_services_sgst + gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_sgst + gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_sgst + gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_sgst + gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_inputs_sgst + gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_sgst + gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_input_services_sgst;
	var subtotalIGSTForI = gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_inputs_igst + gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_capital_goods_igst + gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_input_services_igst + gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_igst + gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_igst + gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_igst + gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_inputs_igst + gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_igst + gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_input_services_igst;
	var subtotalCESSForI = gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_inputs_cess + gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_capital_goods_cess + gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_input_services_cess + gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_cess + gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_cess + gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_cess + gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_inputs_cess + gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_cess + gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_input_services_cess;

	row += '<tr style=\'background-color: lightgray;\'><td class="tableCellTextAlignCenter">6</td><td colspan="6" class="tableCellTextAlignCenter">Details of ITC availed as declared in returns filed during the financial year</td></tr> \n        <tr><td class="tableCellTextAlignCenter">A</td><td colspan="2">Total amount of input tax credit availed through FORM GSTR-3B (sum total of Table 4A of FORM GSTR-3B)</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter" rowspan="3">B</td><td rowspan="3">Inward supplies (other than imports  and inward supplies liable to reverse charge but includes service received from SEZs)</td><td class="tableCellTextAlignLeft">Inputs</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_inputs_cgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_inputs_sgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_inputs_igst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_inputs_cess) + '</td></tr>\n        <tr><td class="tableCellTextAlignLeft">Capital Goods</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_capital_goods_cgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_capital_goods_sgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_capital_goods_igst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_capital_goods_cess) + '</td></tr>\n        <tr><td class="tableCellTextAlignLeft">Input Services</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_input_services_cgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_input_services_sgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_input_services_igst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_input_services_cess) + '</td></tr>        \n        <tr><td class="tableCellTextAlignCenter" rowspan="3">C</td><td rowspan="3">Inward supplies  received from unregistered persons liable to reverse charge (other than B above) on which tax is paid & ITC availed</td><td class="tableCellTextAlignLeft">Inputs</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_cgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_sgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_igst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_cess) + '</td></tr>\n        <tr><td class="tableCellTextAlignLeft">Capital Goods</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_cgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_sgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_igst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_cess) + '</td></tr>\n        <tr><td class="tableCellTextAlignLeft">Input Services</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_cgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_sgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_igst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_cess) + '</td></tr>\n        <tr><td class="tableCellTextAlignCenter" rowspan="3">D</td><td rowspan="3">Inward supplies received from registered persons liable to reverse charge (other than B above) on which tax is paid  and ITC availed</td><td class="tableCellTextAlignLeft">Inputs</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_inputs_cgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_inputs_sgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_inputs_igst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_inputs_cess) + '</td></tr>\n        <tr><td class="tableCellTextAlignLeft">Capital Goods</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_cgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_sgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_igst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_cess) + '</td></tr>\n        <tr><td class="tableCellTextAlignLeft">Input Services</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_input_services_cgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_input_services_sgst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_input_services_igst) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_input_services_cess) + '</td></tr>        \n        <tr><td class="tableCellTextAlignCenter" rowspan="2">E</td><td rowspan="2">Import of goods (including suppliesfrom SEZs)</td><td class="tableCellTextAlignLeft">Inputs</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignLeft">Capital Goods</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">F</td><td colspan="2">Import of services (excluding inward supplies from SEZs)</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">G</td><td colspan="2">Input Tax  credit received from ISD</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">H</td><td colspan="2">Amount of ITC reclaimed (other than B above) under the provisons of Act</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n\n        <tr><td class="tableCellTextAlignCenter">I</td><td colspan="2">Sub-total (B to H above)</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(subtotalCGSTForI) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(subtotalSGSTForI) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(subtotalIGSTForI) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(subtotalCESSForI) + '</td></tr>\n        <tr><td class="tableCellTextAlignCenter">J</td><td colspan="2">Difference (I - A above)</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">K</td><td colspan="2">Transition credit through TRAN-I (including revisions if any)</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">L</td><td colspan="2">Transition credit through TRAN-II</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">M</td><td colspan="2">Any other ITC availed but not specified above</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">N</td><td colspan="2">Sub-total (K to M above)</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">O</td><td colspan="2">Total ITC availed (I + N above)</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(subtotalCGSTForI) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(subtotalSGSTForI) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(subtotalIGSTForI) + '</td><td class="tableCellTextAlignRight">' + MyDouble.getAmountDecimalForGSTRReport(subtotalCESSForI) + '</td></tr>';

	row += '<tr class="noBorderBottom" style="background-color:#ffffff"><td></td></tr>';

	row += '<tr style=\'background-color: lightgray;\'><td class="tableCellTextAlignCenter">7</td><td colspan="6" class="tableCellTextAlignCenter">Details of ITC Reversed and Ineligible ITC as declared in rerurns filed during the financial year</td></tr> \n        <tr><td class="tableCellTextAlignCenter">A</td><td colspan="2">As per Rule 37</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">B</td><td colspan="2">As per Rule 39</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">C</td><td colspan="2">As per Rule 42</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">D</td><td colspan="2">As per Rule 43</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">E</td><td colspan="2">As per section 17(5)</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">F</td><td colspan="2">Reversal of TRAN-I credit</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">G</td><td colspan="2">Reversal of TRAN-II credit</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">H</td><td colspan="2">Other reversals (pl. specify)</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">I</td><td colspan="2">Total ITC reversed (A to H above)</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">J</td><td colspan="2">Net ITC Available for Utilization (6O -7I)</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>';

	row += '<tr class="noBorderBottom" style="background-color:#ffffff"><td></td></tr>';

	row += '<tr style=\'background-color: lightgray;\'><td class="tableCellTextAlignCenter">8</td><td colspan="6" class="tableCellTextAlignCenter">Other ITC related information</td></tr> \n        <tr><td class="tableCellTextAlignCenter">A</td><td colspan="2">ITC  as per GSTR-2A (Table 3 & 5 thereof)</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">B</td><td colspan="2">ITC as per sum total of 6(B) and 6(H) above</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">C</td><td colspan="2">ITC on inward supplies (other than imports and inward supplies liable to reverse charge but includes services received from SEZs) received during 2017-18 but availed during April to September, 2018</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">D</td><td colspan="2">Difference [A-(B+C)]</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">E</td><td colspan="2">ITC available but  not availed (out of D)</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">F</td><td colspan="2">ITC available but ineligible (out of D)</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">G</td><td colspan="2">IGST paid on import  of goods (as per 6(E) above)</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">H</td><td colspan="2">IGST credit  availed  on import of goods (as per 6(E) above)</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">I</td><td colspan="2">Difference (G -H)</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">J</td><td colspan="2">ITC available  but not availed on import of goods (Equal to I)</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">K</td><td colspan="2">Total ITC to be lapsed in current  financial year (E+F+J)</td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td><td class="tableCellTextAlignRight"></td></tr>';

	row += '</tbody></table>';

	row += '<table class="tabTxnTable width100" style="margin-top: 5%;"><thead>\n        <tr style=\'background-color: darkgray;color:#4a4141;\'><th class=\'tableCellTextAlignCenter width5\'>Pt. IV</th><th class=\'tableCellTextAlignCenter width95\' colspan="7">Details of tax paid as declared in returns filed during the financial year</th></tr>\n        </thead><tbody>\n        <tr><td rowspan="2" class="width5">9</td><td class="tableCellTextAlignCenter width20" rowspan="2">Description</td><td class="tableCellTextAlignCenter width20" rowspan="2">Tax Payable</td><td class="tableCellTextAlignCenter width20" rowspan="2">Paid through cash</td><td class="tableCellTextAlignCenter width35" colspan="4">Paid through ITC</td></tr>\n        <tr><td class="tableCellTextAlignCenter width9">Central Tax</td><td class="tableCellTextAlignCenter width9">State Tax/ UT Tax</td><td class="tableCellTextAlignCenter width9">Integrated Tax</td><td class="tableCellTextAlignCenter width8">Cess</td></tr>\n        <tr><td rowspan="9"></td><td class="tableCellTextAlignCenter">1</td><td class="tableCellTextAlignCenter">2</td><td class="tableCellTextAlignCenter">3</td><td class="tableCellTextAlignCenter">4</td><td class="tableCellTextAlignCenter">5</td><td class="tableCellTextAlignCenter">6</td><td class="tableCellTextAlignCenter">7</td></tr>\n        <tr><td class="tableCellTextAlignLeft">Integrated Tax</td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td></tr>\n        <tr></td><td class="tableCellTextAlignLeft">Central Tax</td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td></tr>\n        <tr></td><td class="tableCellTextAlignLeft">State/ UT tax</td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td></tr>\n        <tr><td class="tableCellTextAlignLeft">Cess</td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td></tr>\n        <tr><td class="tableCellTextAlignLeft">Interest</td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td></tr>\n        <tr><td class="tableCellTextAlignLeft">Late fee</td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td></tr>\n        <tr><td class="tableCellTextAlignLeft">Penalty</td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td></tr>\n        <tr><td class="tableCellTextAlignLeft">Other</td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td></tr>';

	row += '<tr class="noBorderBottom" style="background-color:#ffffff"><td></td></tr>';

	row += '<tr style=\'background-color: darkgray;color:#4a4141;\'><th class=\'tableCellTextAlignCenter width5\'>Pt. V</th><th class=\'tableCellTextAlignCenter width95\' colspan="7">Particulars of the transactions for the previous FY declared in returns of April to September of current FY or upto date of filing  of annual  return  of previous  FY whichever is earlier</th></tr>\n        </thead><tbody>\n        <tr><td rowspan="2" class="width5"></td><td class="tableCellTextAlignCenter width40" colspan="2">Description</td><td class="tableCellTextAlignCenter width20">Tax Payable</td><td class="tableCellTextAlignCenter width9">Central Tax</td><td class="tableCellTextAlignCenter width9">State Tax/ UT Tax</td><td class="tableCellTextAlignCenter width9">Integrated Tax</td><td class="tableCellTextAlignCenter width8">Cess</td></tr>\n        <tr><td class="tableCellTextAlignCenter" colspan="2">1</td><td class="tableCellTextAlignCenter">2</td><td class="tableCellTextAlignCenter">3</td><td class="tableCellTextAlignCenter">4</td><td class="tableCellTextAlignCenter">5</td><td class="tableCellTextAlignCenter">6</td></tr>\n        <tr><td class="tableCellTextAlignCenter">10</td><td class="tableCellTextAlignLeft" colspan="2">Supplies/tax declared through Amendments (+) (net of debit notes)</td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">11</td></td><td class="tableCellTextAlignLeft" colspan="2">Supplies/tax reduced through Amendments (+) (net of credit notes)</td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">12</td></td><td class="tableCellTextAlignLeft" colspan="2">Reversal  of ITC availed during previous financial year</td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">13</td><td class="tableCellTextAlignLeft" colspan="2">ITC availed for the previous financial year</td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td></tr>';

	row += '<tr class="noBorderBottom" style="background-color:#ffffff"><td></td></tr>';

	row += '<tr style=\'background-color: lightgray;\'><th class=\'tableCellTextAlignCenter width5\'>14</th><th class=\'tableCellTextAlignCenter width95\' colspan="7">Differential tax paid on account of declaration in 10 & 11 above</th></tr>\n        </thead><tbody>\n        <tr><td rowspan="2" class="width5"></td><td class="tableCellTextAlignCenter width60" colspan="3">Description</td><td class="tableCellTextAlignCenter width18" colspan="2">Payable</td><td class="tableCellTextAlignCenter width17" colspan="2">Paid</td></tr>\n        <tr><td class="tableCellTextAlignCenter" colspan="3">1</td><td class="tableCellTextAlignCenter" colspan="2">2</td><td class="tableCellTextAlignCenter" colspan="2">3</td></tr>\n        <tr><td rowspan="5"></td><td class="tableCellTextAlignLeft" colspan="3">Integrated Tax</td><td class="tableCellTextAlignCenter" colspan="2"></td><td class="tableCellTextAlignCenter" colspan="2"></td></tr>\n        <tr><td class="tableCellTextAlignLeft" colspan="3">Central Tax</td><td class="tableCellTextAlignCenter" colspan="2"></td><td class="tableCellTextAlignCenter" colspan="2"></td></tr>\n        <tr><td class="tableCellTextAlignLeft" colspan="3">State/ UT Tax</td><td class="tableCellTextAlignCenter" colspan="2"></td><td class="tableCellTextAlignCenter" colspan="2"></td></tr>\n        <tr><td class="tableCellTextAlignLeft" colspan="3">Cess</td><td class="tableCellTextAlignCenter" colspan="2"></td><td class="tableCellTextAlignCenter" colspan="2"></td></tr>\n        <tr><td class="tableCellTextAlignLeft" colspan="3">Interest</td><td class="tableCellTextAlignCenter" colspan="2"></td><td class="tableCellTextAlignCenter" colspan="2"></td></tr>';

	row += '</tbody></table>';

	row += '<table class="tabTxnTable width100" style="margin-top:5%;"><thead>\n        <tr style=\'background-color: darkgray;\'><th class=\'tableCellTextAlignCenter width10\'>Pt. VI</th><th class=\'tableCellTextAlignCenter width90\' colspan="8">Other Information</th></tr>\n        </thead><tbody>\n        <tr><td class="width10">15</td><td class="tableCellTextAlignCenter width90" colspan="8">Particulars of Demands and Refunds</td></tr>\n        <tr><td rowspan="2"></td><td class="tableCellTextAlignCenter width20">Details</td><td class="tableCellTextAlignCenter width10">Central Tax</td><td class="tableCellTextAlignCenter width10">State Tax/ UT Tax</td><td class="tableCellTextAlignCenter width10">Integrated Tax</td><td class="tableCellTextAlignCenter width10">Cess</td><td class="tableCellTextAlignCenter width10">Interest</td><td class="tableCellTextAlignCenter width10">Penalty</td><td class="tableCellTextAlignCenter width10">Late Fee/ Others</td></tr>\n        <tr><td class="tableCellTextAlignCenter">1</td><td class="tableCellTextAlignCenter">2</td><td class="tableCellTextAlignCenter">3</td><td class="tableCellTextAlignCenter">4</td><td class="tableCellTextAlignCenter">5</td><td class="tableCellTextAlignCenter">6</td><td class="tableCellTextAlignCenter">7</td><td class="tableCellTextAlignCenter">8</td></tr>\n        <tr><td class="tableCellTextAlignCenter">A</td><td class="tableCellTextAlignLeft">Total Refund claimed</td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">B</td><td class="tableCellTextAlignLeft">Total Refund sanctioned</td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">C</td><td class="tableCellTextAlignLeft">Total Refund rejected</td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">D</td><td class="tableCellTextAlignLeft">Total Refund pending</td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">E</td><td class="tableCellTextAlignLeft">Total demand of taxes</td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">F</td><td class="tableCellTextAlignLeft">Total taxes paid in respect of E above</td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">G</td><td class="tableCellTextAlignLeft">Total demands pending out of E above</td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td></tr>';

	row += '<tr class="noBorderBottom" style="background-color:#ffffff"><td></td></tr>';

	row += '<tr style=\'background-color: lightgray;\'><th class=\'tableCellTextAlignCenter width10\'>16</th><th class=\'tableCellTextAlignCenter width90\' colspan="8">Information on supplies received from composition taxpayers, deemed supply under section 143 and goods sent on approval basis</th></tr>\n        </thead><tbody>\n        <tr><td class="width10" rowspan="2"></td><td class="tableCellTextAlignCenter width40" colspan="3">Details</td><td class="tableCellTextAlignCenter width10">Taxable Value</td><td class="tableCellTextAlignCenter width10">Central Tax</td><td class="tableCellTextAlignCenter width10">State/ UT Tax</td><td class="tableCellTextAlignCenter width10">Integrated Tax</td><td class="tableCellTextAlignCenter width10">Cess</td></tr>\n        <tr><td class="tableCellTextAlignCenter" colspan="3">1</td><td class="tableCellTextAlignCenter">2</td><td class="tableCellTextAlignCenter">3</td><td class="tableCellTextAlignCenter">4</td><td class="tableCellTextAlignCenter">5</td><td class="tableCellTextAlignCenter">6</td></tr>\n        <tr><td class="tableCellTextAlignCenter">A</td><td class="tableCellTextAlignLeft" colspan="3">Supplies received from composition taxpayers</td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">B</td><td class="tableCellTextAlignLeft" colspan="3">Deemed supply under section 143</td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td></tr>\n        <tr><td class="tableCellTextAlignCenter">C</td><td class="tableCellTextAlignLeft" colspan="3">Goods sent on approval basis but not returned</td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td><td class="tableCellTextAlignCenter"></td></tr>';

	row += '<tr class="noBorderBottom" style="background-color:#ffffff"><td></td></tr>';

	row += '<tr style=\'background-color: lightgray;\'><th class=\'tableCellTextAlignCenter width10\'>17</th><th class=\'tableCellTextAlignCenter width90\' colspan="8">HSN Wise Summary of Outward supplies</th></tr>\n        </thead><tbody>\n        <tr><td class="width10">HSN Code</td><td class="tableCellTextAlignCenter width20">UQC</td><td class="tableCellTextAlignCenter width10">Total Quantity</td><td class="tableCellTextAlignCenter width10">Taxable Value</td><td class="tableCellTextAlignCenter width10">Rate of Tax</td><td class="tableCellTextAlignCenter width10">Central Tax</td><td class="tableCellTextAlignCenter width10">State/ UT Tax</td><td class="tableCellTextAlignCenter width10">Integrated Tax</td><td class="tableCellTextAlignCenter width10">Cess</td></tr>\n        <tr><td class="tableCellTextAlignCenter">1</td><td class="tableCellTextAlignCenter">2</td><td class="tableCellTextAlignCenter">3</td><td class="tableCellTextAlignCenter">4</td><td class="tableCellTextAlignCenter">5</td><td class="tableCellTextAlignCenter">6</td><td class="tableCellTextAlignCenter">7</td><td class="tableCellTextAlignCenter">8</td><td class="tableCellTextAlignCenter">9</td></tr>';

	var hsn_summary_sale_list = gSTR9ReportHelper.hsn_summary_sale;
	var hsn_summary_purchase_list = gSTR9ReportHelper.hsn_summary_purchase;

	for (var k = 0; k < hsn_summary_sale_list.length; k++) {
		var hsnData = hsn_summary_sale_list[k];
		if (hsnData.getRateOfTax() != -1) {
			row += '<tr><td class="tableCellTextAlignCenter">' + hsnData.getItemHSN() + '</td>\n                <td class="tableCellTextAlignCenter">' + hsnData.getItemUnitShort() + '</td>\n                <td class="tableCellTextAlignCenter">' + MyDouble.getQuantityDecimalForGSTRReport(hsnData.getItemQuantity() + hsnData.getItemFreeQuantity()) + '</td>\n                <td class="tableCellTextAlignCenter">' + MyDouble.getAmountDecimalForGSTRReport(hsnData.getItemTaxableValue()) + '</td>\n                <td class="tableCellTextAlignCenter">' + hsnData.getRateOfTax() + '</td>\n                <td class="tableCellTextAlignCenter">' + MyDouble.getAmountDecimalForGSTRReport(hsnData.getCGSTAmt()) + '</td>\n                <td class="tableCellTextAlignCenter">' + MyDouble.getAmountDecimalForGSTRReport(hsnData.getSGSTAmt()) + '</td>\n                <td class="tableCellTextAlignCenter">' + MyDouble.getAmountDecimalForGSTRReport(hsnData.getIGSTAmt()) + '</td>\n                <td class="tableCellTextAlignCenter">' + (MyDouble.getAmountDecimalForGSTRReport(hsnData.getCESSAmt()) + MyDouble.getAmountDecimalForGSTRReport(hsnData.getAdditionalCessAmt())) + '</td>\n                </tr>';
		}
	}

	row += '<tr class="noBorderBottom" style="background-color:#ffffff"><td></td></tr>';

	row += '<tr style=\'background-color: lightgray;\'><th class=\'tableCellTextAlignCenter width10\'>18</th><th class=\'tableCellTextAlignCenter width90\' colspan="8">HSN Wise Summary of Inward supplies</th></tr>\n        </thead><tbody>\n        <tr><td class="width10">HSN Code</td><td class="tableCellTextAlignCenter width20">UQC</td><td class="tableCellTextAlignCenter width10">Total Quantity</td><td class="tableCellTextAlignCenter width10">Taxable Value</td><td class="tableCellTextAlignCenter width10">Rate of Tax</td><td class="tableCellTextAlignCenter width10">Central Tax</td><td class="tableCellTextAlignCenter width10">State/ UT Tax</td><td class="tableCellTextAlignCenter width10">Integrated Tax</td><td class="tableCellTextAlignCenter width10">Cess</td></tr>\n        <tr><td class="tableCellTextAlignCenter">1</td><td class="tableCellTextAlignCenter">2</td><td class="tableCellTextAlignCenter">3</td><td class="tableCellTextAlignCenter">4</td><td class="tableCellTextAlignCenter">5</td><td class="tableCellTextAlignCenter">6</td><td class="tableCellTextAlignCenter">7</td><td class="tableCellTextAlignCenter">8</td><td class="tableCellTextAlignCenter">9</td></tr>';

	// hsn wise data purchase

	for (var k = 0; k < hsn_summary_purchase_list.length; k++) {
		var _hsnData = hsn_summary_purchase_list[k];
		if (_hsnData.getRateOfTax() != -1) {
			row += '<tr><td class="tableCellTextAlignCenter">' + _hsnData.getItemHSN() + '</td>\n                <td class="tableCellTextAlignCenter">' + _hsnData.getItemUnitShort() + '</td>\n                <td class="tableCellTextAlignCenter">' + MyDouble.getQuantityDecimalForGSTRReport(_hsnData.getItemQuantity() + _hsnData.getItemFreeQuantity()) + '</td>\n                <td class="tableCellTextAlignCenter">' + MyDouble.getAmountDecimalForGSTRReport(_hsnData.getItemTaxableValue()) + '</td>\n                <td class="tableCellTextAlignCenter">' + _hsnData.getRateOfTax() + '</td>\n                <td class="tableCellTextAlignCenter">' + MyDouble.getAmountDecimalForGSTRReport(_hsnData.getCGSTAmt()) + '</td>\n                <td class="tableCellTextAlignCenter">' + MyDouble.getAmountDecimalForGSTRReport(_hsnData.getSGSTAmt()) + '</td>\n                <td class="tableCellTextAlignCenter">' + MyDouble.getAmountDecimalForGSTRReport(_hsnData.getIGSTAmt()) + '</td>\n                <td class="tableCellTextAlignCenter">' + (MyDouble.getAmountDecimalForGSTRReport(_hsnData.getCESSAmt()) + MyDouble.getAmountDecimalForGSTRReport(_hsnData.getAdditionalCessAmt())) + '</td>\n                </tr>';
		}
	}

	row += '<tr class="noBorderBottom" style="background-color:#ffffff"><td></td></tr>';

	row += '<tr style=\'background-color: lightgray;\'><th class=\'tableCellTextAlignCenter width10\'>19</th><th class=\'tableCellTextAlignCenter width90\' colspan="8">Late fee payable and paid</th></tr>\n        </thead><tbody>\n        <tr><td class="width10" rowspan="2"></td><td class="tableCellTextAlignCenter width50" colspan="4">Description</td><td class="tableCellTextAlignCenter width20" colspan="2">Payable</td><td class="tableCellTextAlignCenter width20" colspan="2">Paid</td></tr>\n        <tr><td class="tableCellTextAlignCenter" colspan="4">1</td><td class="tableCellTextAlignCenter" colspan="2">2</td><td class="tableCellTextAlignCenter" colspan="2">3</td></tr>\n        <tr><td class="tableCellTextAlignCenter">A</td><td class="tableCellTextAlignLeft" colspan="4">Central Tax</td><td class="tableCellTextAlignRight" colspan="2"></td><td class="tableCellTextAlignRight" colspan="2"></td></tr>\n         <tr><td class="tableCellTextAlignCenter">B</td><td class="tableCellTextAlignLeft" colspan="4">State Tax</td><td class="tableCellTextAlignRight" colspan="2"></td><td class="tableCellTextAlignRight" colspan="2"></td></tr>';

	row += '</tbody></table>';

	$('#divGSTR9').html(row);
};

var getHTMLTextForReport = function getHTMLTextForReport() {
	var GSTR9ReportHTMLGenerator = require('./../ReportHTMLGenerator/GSTR9ReportHTMLGenerator.js');
	var gSTR9ReportHTMLGenerator = new GSTR9ReportHTMLGenerator();
	var htmlText = gSTR9ReportHTMLGenerator.getHTMLText(gSTR9ReportHelper, firmId, fyVal);
	return htmlText;
};

/// ///////////////////////////PDF/////////////////////////////////////
var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.GSTR9_REPORT, fromDate: startDateString, toDate: endDateString });
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.GSTR9_REPORT, fromDate: startDateString, toDate: endDateString });
	excelHelper.saveExcel(fileName);
};

var getObjectFor5 = function getObjectFor5() {
	var subTotalG_taxable_value = gSTR9ReportHelper.sale_exempted_taxable_value + gSTR9ReportHelper.sale_nil_rated_taxable_value;
	var subtotalL_taxable_value = gSTR9ReportHelper.sale_return_exempted_nil_rated_taxable_value;
	var subTotalHtaxable_value = gSTR9ReportHelper.sale_purchase_without_reverse_charge_unregistered_taxable_value + gSTR9ReportHelper.sale_purchase_without_reverse_charge_registered_taxable_value + gSTR9ReportHelper.purchase_reverse_charge_taxable_value;
	var subtotalM_taxable_value = -gSTR9ReportHelper.sale_return_registered_taxable_value;

	var subtotalN_taxable_value = subTotalHtaxable_value + subtotalM_taxable_value;

	var rowObj = [];
	rowObj.push(['', 'Nature Of supplies', 'Taxable Values', 'Central Tax', 'State/ UT Tax', 'Integrated Tax', 'Cess']);
	rowObj.push([]);
	rowObj.push(['A', 'Zero rated supply (Export) without payment of tax', '', '', '', '', '']);
	rowObj.push(['B', 'Supply to SEZs without payment of tax', '', '', '', '']);
	rowObj.push(['C', 'Supplies on which tax is to be paid by the recipient on reverse charge basis', '', '', '', '']);
	rowObj.push(['D', 'Exempted', MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_exempted_taxable_value), '', '', '']);
	rowObj.push(['E', 'Nil Rated', MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_nil_rated_taxable_value), '', '', '']);
	rowObj.push(['F', 'Non-GST supply', '', '', '', '', '']);
	rowObj.push(['G', 'Sub-total (A to F above)', MyDouble.getAmountDecimalForGSTRReport(subTotalG_taxable_value), '', '', '', '']);
	rowObj.push(['H', 'Credit Notes issued in respect of transactions specified in A to F above (-)', MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_return_exempted_nil_rated_taxable_value), '', '', '', '']);
	rowObj.push(['I', 'Debit Notes issued in respect of transactions specified in A to F above (+)', '', '', '', '', '']);
	rowObj.push(['J', 'Supplies declared through Amendments(+)', '', '', '', '', '']);
	rowObj.push(['K', 'Supplies reduced through Amendments(+)', '', '', '', '', '']);
	rowObj.push(['L', 'Sub-Total (H to K above)', MyDouble.getAmountDecimalForGSTRReport(subtotalL_taxable_value) * -1, '', '', '', '']);
	rowObj.push(['M', 'Turnover on which tax is not to be paid (G + L above)', MyDouble.getAmountDecimalForGSTRReport(subTotalG_taxable_value - subtotalL_taxable_value), '', '', '', '']);
	rowObj.push(['N', 'Total Turnover (including advances) (4N + 5M - 4G above)', MyDouble.getAmountDecimalForGSTRReport(MyDouble.getAmountDecimalForGSTRReport(subtotalN_taxable_value) + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_exempted_taxable_value) + MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_nil_rated_taxable_value) - MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_return_exempted_nil_rated_taxable_value) - MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_reverse_charge_taxable_value)), '', '', '', '']);

	return rowObj;
};

var getObjectFor4 = function getObjectFor4() {
	var subTotalHtaxable_value = gSTR9ReportHelper.sale_purchase_without_reverse_charge_unregistered_taxable_value + gSTR9ReportHelper.sale_purchase_without_reverse_charge_registered_taxable_value + gSTR9ReportHelper.purchase_reverse_charge_taxable_value;
	var subtotalH_cgst = gSTR9ReportHelper.sale_purchase_without_reverse_charge_unregistered_cgst + gSTR9ReportHelper.sale_purchase_without_reverse_charge_registered_cgst + gSTR9ReportHelper.purchase_reverse_charge_cgst;
	var subtotalH_sgst = gSTR9ReportHelper.sale_purchase_without_reverse_charge_unregistered_sgst + gSTR9ReportHelper.sale_purchase_without_reverse_charge_registered_sgst + gSTR9ReportHelper.purchase_reverse_charge_sgst;
	var subtotalH_igst = gSTR9ReportHelper.sale_purchase_without_reverse_charge_unregistered_igst + gSTR9ReportHelper.sale_purchase_without_reverse_charge_registered_igst + gSTR9ReportHelper.purchase_reverse_charge_igst;
	var subtotalH_cess = gSTR9ReportHelper.sale_purchase_without_reverse_charge_unregistered_cess + gSTR9ReportHelper.sale_purchase_without_reverse_charge_registered_cess + gSTR9ReportHelper.purchase_reverse_charge_cess;

	var subtotalM_taxable_value = -gSTR9ReportHelper.sale_return_registered_taxable_value;
	var subtotalM_cgst = -gSTR9ReportHelper.sale_return_registered_cgst;
	var subtotalM_sgst = -gSTR9ReportHelper.sale_return_registered_sgst;
	var subtotalM_igst = -gSTR9ReportHelper.sale_return_registered_igst;
	var subtotalM_cess = -gSTR9ReportHelper.sale_return_registered_cess;

	var subtotalN_taxable_value = subTotalHtaxable_value + subtotalM_taxable_value;
	var subtotalN_cgst = subtotalH_cgst + subtotalM_cgst;
	var subtotalN_sgst = subtotalH_sgst + subtotalM_sgst;
	var subtotalN_igst = subtotalH_igst + subtotalM_igst;
	var subtotalN_cess = subtotalH_cess + subtotalM_cess;

	var rowObj = [];
	rowObj.push(['1', 'Financial Year', fyDateMapUI.get(fyVal)[0] + '-' + fyDateMapUI.get(fyVal)[1]]);
	rowObj.push(['2', 'GSTIN', gstin]);
	rowObj.push(['3', 'Legal Name', firmName]);
	rowObj.push(['4', '	Trade Name (if any)', '']);
	rowObj.push([]);
	rowObj.push([]);
	rowObj.push(['', 'Nature Of supplies', 'Taxable Values', 'Central Tax', 'State/ UT Tax', 'Integrated Tax', 'Cess']);
	rowObj.push([]);
	rowObj.push(['A', 'Supplies made to un-registered persons(B2C)', MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_purchase_without_reverse_charge_unregistered_taxable_value), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_purchase_without_reverse_charge_unregistered_cgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_purchase_without_reverse_charge_unregistered_sgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_purchase_without_reverse_charge_unregistered_igst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_purchase_without_reverse_charge_unregistered_cess)]);
	rowObj.push(['B', '	Supplies made to registered persons(B2B)', MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_purchase_without_reverse_charge_registered_taxable_value), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_purchase_without_reverse_charge_registered_cgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_purchase_without_reverse_charge_registered_sgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_purchase_without_reverse_charge_registered_igst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_purchase_without_reverse_charge_registered_cess)]);
	rowObj.push(['C', 'Zero rated supply(Export) on payment of tax (except supplies to SEZs)', '', '', '', '']);
	rowObj.push(['D', 'Supplies to SEZs on payment of tax', '', '', '', '']);
	rowObj.push(['E', 'Deemed Exports', '', '', '', '']);
	rowObj.push(['F', 'Advances on which tax has been paid but invoice has not been issued (not cover under (A) to (E) above)', '', '', '', '', '']);
	rowObj.push(['G', 'Inward supplies on which tax is to be paid on reverse charge basis', MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_reverse_charge_taxable_value), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_reverse_charge_cgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_reverse_charge_sgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_reverse_charge_igst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_reverse_charge_cess)]);
	rowObj.push(['H', 'Sub-total (A to G above)', MyDouble.getAmountDecimalForGSTRReport(subTotalHtaxable_value), MyDouble.getAmountDecimalForGSTRReport(subtotalH_cgst), MyDouble.getAmountDecimalForGSTRReport(subtotalH_sgst), MyDouble.getAmountDecimalForGSTRReport(subtotalH_igst), MyDouble.getAmountDecimalForGSTRReport(subtotalH_cess)]);
	rowObj.push(['I', 'Credit Notes issued in respect of transactions specified in (B) to (E) above (-)', MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_return_registered_taxable_value), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_return_registered_cgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_return_registered_sgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_return_registered_igst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.sale_return_registered_cess)]);
	rowObj.push(['J', 'Debit Notes issued in respect of transactions specified in (B) to (E) above (+)', '', '', '', '', '']);
	rowObj.push(['K', 'Supplies/tax declared through Amendments(+)', '', '', '', '', '']);
	rowObj.push(['L', 'Supplies/tax reduced through Amendments(+)', '', '', '', '', '']);
	rowObj.push(['M', '	Sub-total (I to L above)', MyDouble.getAmountDecimalForGSTRReport(subtotalM_taxable_value), MyDouble.getAmountDecimalForGSTRReport(subtotalM_cgst), MyDouble.getAmountDecimalForGSTRReport(subtotalM_sgst), MyDouble.getAmountDecimalForGSTRReport(subtotalM_igst), MyDouble.getAmountDecimalForGSTRReport(subtotalM_cess)]);
	rowObj.push(['N', '	Supplies and advances on which tax is to be paid (H+M) above', MyDouble.getAmountDecimalForGSTRReport(subtotalN_taxable_value), MyDouble.getAmountDecimalForGSTRReport(subtotalN_cgst), MyDouble.getAmountDecimalForGSTRReport(subtotalN_sgst), MyDouble.getAmountDecimalForGSTRReport(subtotalN_igst), MyDouble.getAmountDecimalForGSTRReport(subtotalN_cess)]);

	return rowObj;
};

var getObjectFor6 = function getObjectFor6() {
	var subTotalI_cgst = gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_inputs_cgst + gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_capital_goods_cgst + gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_input_services_cgst + gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_cgst + gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_cgst + gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_cgst + gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_inputs_cgst + gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_cgst + gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_input_services_cgst;
	var subTotalI_sgst = gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_inputs_sgst + gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_capital_goods_sgst + gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_input_services_sgst + gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_sgst + gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_sgst + gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_sgst + gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_inputs_sgst + gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_sgst + gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_input_services_sgst;
	var subTotalI_igst = gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_inputs_igst + gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_capital_goods_igst + gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_input_services_igst + gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_igst + gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_igst + gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_igst + gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_inputs_igst + gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_igst + gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_input_services_igst;
	var subTotalI_cess = gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_inputs_cess + gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_capital_goods_cess + gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_input_services_cess + gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_cess + gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_cess + gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_cess + gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_inputs_cess + gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_cess + gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_input_services_cess;

	var rowObj = [];
	rowObj.push(['', 'Description', 'Type', 'Central Tax', 'State/ UT Tax', 'Integrated Tax', 'Cess']);
	rowObj.push(['A', 'Total amount of input tax credit availed through FORM GSTR-3B (sum total of Table 4A of FORM GSTR-3B)']);
	rowObj.push(['B', '	Inward supplies (other than imports and inward supplies liable to reverse charge but includes service received from SEZs)', 'Inputs', MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_inputs_cgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_inputs_sgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_inputs_igst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_inputs_cess)]);
	rowObj.push(['', '', 'Capital Goods', MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_capital_goods_cgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_capital_goods_sgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_capital_goods_igst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_capital_goods_cess)]);
	rowObj.push(['', '', 'Input services', MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_input_services_cgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_input_services_sgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_input_services_igst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_without_reverse_charge_itc_input_services_cess)]);
	rowObj.push(['C', '		Inward supplies received from unregistered persons liable to reverse charge (other than B above) on which tax is paid & ITC availed', 'Inputs', MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_cgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_sgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_igst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_cess)]);
	rowObj.push(['', '', 'Capital Goods', MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_cgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_sgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_igst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_cess)]);
	rowObj.push(['', '', 'Input services', MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_cgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_sgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_igst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_cess)]);
	rowObj.push(['D', '	Inward supplies received from registered persons liable to reverse charge (other than B above) on which tax is paid and ITC availed', 'Inputs', MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_inputs_cgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_inputs_sgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_inputs_igst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_inputs_cess)]);
	rowObj.push(['', '', 'Capital Goods', MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_cgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_sgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_igst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_cess)]);
	rowObj.push(['', '', 'Input services', MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_input_services_cgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_input_services_sgst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_input_services_igst), MyDouble.getAmountDecimalForGSTRReport(gSTR9ReportHelper.purchase_purchase_return_registered_reverse_charge_itc_input_services_cess)]);
	rowObj.push(['E', '	Import of goods (including suppliesfrom SEZs)', 'Inputs', '', '', '', '']);
	rowObj.push(['', '', 'Capital Goods', '', '', '', '']);
	rowObj.push(['F', 'Import of services (excluding inward supplies from SEZs)', '', '', '', '', '']);
	rowObj.push(['G', '	Input Tax credit received from ISD', '', '', '', '', '']);
	rowObj.push(['H', '	Amount of ITC reclaimed (other than B above) under the provisons of Act', '', '', '', '', '']);
	rowObj.push(['I', 'Sub-total (B to H above)', '', MyDouble.getAmountDecimalForGSTRReport(subTotalI_cgst), MyDouble.getAmountDecimalForGSTRReport(subTotalI_sgst), MyDouble.getAmountDecimalForGSTRReport(subTotalI_igst), MyDouble.getAmountDecimalForGSTRReport(subTotalI_cess)]);
	rowObj.push(['J', 'Difference (I - A above)', '', '', '', '', '']);
	rowObj.push(['K', 'Transition credit through TRAN-I (including revisions if any)', '', '', '', '', '']);
	rowObj.push(['L', 'Transition credit through TRAN-II', '', '', '', '', '']);
	rowObj.push(['M', '	Any other ITC availed but not specified above', '', '', '', '', '']);
	rowObj.push(['N', 'Sub-total (K to M above)', '', '', '', '', '']);
	rowObj.push(['O', 'Total ITC availed (I + N above)', '', MyDouble.getAmountDecimalForGSTRReport(subTotalI_cgst), MyDouble.getAmountDecimalForGSTRReport(subTotalI_sgst), MyDouble.getAmountDecimalForGSTRReport(subTotalI_igst), MyDouble.getAmountDecimalForGSTRReport(subTotalI_cess)]);

	return rowObj;
};

var getObjectFor8 = function getObjectFor8() {
	var rowObj = [];
	rowObj.push(['', 'Description', 'Central Tax', 'State/ UT Tax', 'Integrated Tax', 'Cess']);
	rowObj.push(['A', 'AITC as per GSTR-2A (Table 3 & 5 thereof)', '', '', '', '']);
	rowObj.push(['B', '	ITC as per sum total of 6(B) and 6(H) above', '', '', '', '']);
	rowObj.push(['C', '	ITC on inward supplies (other than imports and inward supplies liable to reverse charge but includes services received from SEZs) received during 2017-18 but availed during April to September, 2018', '', '', '', '']);
	rowObj.push(['D', '	Difference [A-(B+C)]', '', '', '', '']);
	rowObj.push(['E', '	ITC available but not availed (out of D)', '', '', '', '']);
	rowObj.push(['F', 'ITC available but ineligible (out of D)', '', '', '', '']);
	rowObj.push(['G', 'IGST paid on import of goods (as per 6(E) above)', '', '', '', '']);
	rowObj.push(['H', 'IGST credit availed on import of goods (as per 6(E) above)', '', '', '', '']);
	rowObj.push(['I', 'Difference (G -H)', '', '', '', '']);
	rowObj.push(['J', 'ITC available but not availed on import of goods (Equal to I)', '', '', '', '']);
	rowObj.push(['K', 'Total ITC to be lapsed in current financial year (E+F+J)', '', '', '', '']);

	return rowObj;
};

var getObjectFor9 = function getObjectFor9() {
	var rowObj = [];
	rowObj.push(['9', 'Description', 'Tax Payable', 'Paid through cash', 'Central Tax', 'State/ UT Tax', 'Integrated Tax', 'Cess']);
	rowObj.push(['A', 'Integrated Tax', '', '', '', '', '', '']);
	rowObj.push(['B', 'Central Tax', '', '', '', '', '', '']);
	rowObj.push(['C', 'State/ UT Tax', '', '', '', '', '', '']);
	rowObj.push(['D', 'Cess', '', '', '', '', '', '']);
	rowObj.push(['E', 'Interest', '', '', '', '', '', '']);
	rowObj.push(['F', 'Late fee', '', '', '', '', '', '']);
	rowObj.push(['G', 'penalty', '', '', '', '', '', '']);
	rowObj.push(['H', 'Other', '', '', '', '', '', '']);

	return rowObj;
};

var getObjectFor10111213 = function getObjectFor10111213() {
	var rowObj = [];
	rowObj.push(['', 'Description', 'Tax Payable', 'Central Tax', 'State/ UT Tax', 'Integrated Tax', 'Cess']);
	rowObj.push(['10', 'Supplies/tax declared through Amendments (+) (net of debit notes)', '', '', '', '', '', '']);
	rowObj.push(['11', 'Supplies/tax reduced through Amendments (+) (net of credit notes)', '', '', '', '', '', '']);
	rowObj.push(['12', 'Reversal of ITC availed during previous financial year', '', '', '', '', '', '']);
	rowObj.push(['13', 'ITC availed for the previous financial year', '', '', '', '', '', '']);

	return rowObj;
};

var getObjectFor14 = function getObjectFor14() {
	var rowObj = [];
	rowObj.push(['', 'Description', 'Payable', 'Paid']);
	rowObj.push(['', 'Integrated Tax', '', '']);
	rowObj.push(['', 'Central Tax', '', '']);
	rowObj.push(['', 'State/ UT Tax', '', '']);
	rowObj.push(['', 'Cess', '', '']);
	rowObj.push(['', 'Interset', '', '']);

	return rowObj;
};

var getObjectFor15 = function getObjectFor15() {
	var rowObj = [];
	rowObj.push(['', 'Details', 'Central Tax', 'State/ UT Tax', 'Integrated Tax', 'Cess', 'Interset', 'Penalty', 'Late fee/ Penalty']);
	rowObj.push(['A', 'Total Refund claimed', '', '', '', '', '', '', '']);
	rowObj.push(['B', 'Total Refund sanctioned', '', '', '', '', '', '', '']);
	rowObj.push(['C', 'Total Refund rejected', '', '', '', '', '', '', '']);
	rowObj.push(['D', 'Total Refund pending', '', '', '', '', '', '', '']);
	rowObj.push(['E', 'Total demand of taxes', '', '', '', '', '', '', '']);
	rowObj.push(['F', 'Total taxes paid in respect of E above', '', '', '', '', '', '', '']);
	rowObj.push(['G', 'Total demands pending out of E above', '', '', '', '', '', '', '']);

	return rowObj;
};

var getObjectFor16 = function getObjectFor16() {
	var rowObj = [];
	rowObj.push(['', 'Details', 'Taxable value', 'Central Tax', 'State/ UT Tax', 'Integrated Tax', 'Cess']);
	rowObj.push(['A', 'Supplies received from composition taxpayers', '', '', '', '', '', '', '']);
	rowObj.push(['B', 'Deemed supply under section 143', '', '', '', '', '', '', '']);
	rowObj.push(['C', '	Goods sent on approval basis but not returned', '', '', '', '', '', '', '']);

	return rowObj;
};

var getObjectFor17 = function getObjectFor17() {
	var rowObj = [];
	rowObj.push(['HSN Code', 'UQC', 'Total Quantity', 'Taxable Value', 'Rate of Tax', 'Central Tax', 'State/ UT Tax', 'Integrated Tax', 'Cess']);
	rowObj.push([]);
	for (var i = 0; i < gSTR9ReportHelper.hsn_summary_sale.length; i++) {
		var hsnData = gSTR9ReportHelper.hsn_summary_sale[i];
		if (hsnData.getRateOfTax() != -1) {
			var tempArr = [];
			tempArr.push(hsnData.getItemHSN());
			tempArr.push(hsnData.getItemUnitShort());
			tempArr.push(MyDouble.getQuantityDecimalForGSTRReport(hsnData.getItemQuantity() + hsnData.getItemFreeQuantity()));
			tempArr.push(MyDouble.getAmountDecimalForGSTRReport(hsnData.getItemTaxableValue()));
			tempArr.push(hsnData.getRateOfTax());
			tempArr.push(MyDouble.getAmountDecimalForGSTRReport(hsnData.getCGSTAmt()));
			tempArr.push(MyDouble.getAmountDecimalForGSTRReport(hsnData.getSGSTAmt()));
			tempArr.push(MyDouble.getAmountDecimalForGSTRReport(hsnData.getIGSTAmt()));
			tempArr.push(MyDouble.getAmountDecimalForGSTRReport(hsnData.getCESSAmt()) + MyDouble.getAmountDecimalForGSTRReport(hsnData.getAdditionalCessAmt()));
			rowObj.push(tempArr);
		}
	}

	return rowObj;
};

var getObjectFor18 = function getObjectFor18() {
	var rowObj = [];
	rowObj.push(['HSN Code', 'UQC', 'Total Quantity', 'Taxable Value', 'Rate of Tax', 'Central Tax', 'State/ UT Tax', 'Integrated Tax', 'Cess']);
	rowObj.push([]);
	for (var i = 0; i < gSTR9ReportHelper.hsn_summary_purchase.length; i++) {
		var hsnData = gSTR9ReportHelper.hsn_summary_purchase[i];
		if (hsnData.getRateOfTax() != -1) {
			var tempArr = [];
			tempArr.push(hsnData.getItemHSN());
			tempArr.push(hsnData.getItemUnitShort());
			tempArr.push(MyDouble.getQuantityDecimalForGSTRReport(hsnData.getItemQuantity() + hsnData.getItemFreeQuantity()));
			tempArr.push(MyDouble.getAmountDecimalForGSTRReport(hsnData.getItemTaxableValue()));
			tempArr.push(hsnData.getRateOfTax());
			tempArr.push(MyDouble.getAmountDecimalForGSTRReport(hsnData.getCGSTAmt()));
			tempArr.push(MyDouble.getAmountDecimalForGSTRReport(hsnData.getSGSTAmt()));
			tempArr.push(MyDouble.getAmountDecimalForGSTRReport(hsnData.getIGSTAmt()));
			tempArr.push(MyDouble.getAmountDecimalForGSTRReport(hsnData.getCESSAmt()) + MyDouble.getAmountDecimalForGSTRReport(hsnData.getAdditionalCessAmt()));
			rowObj.push(tempArr);
		}
	}

	return rowObj;
};

var getObjectFor19 = function getObjectFor19() {
	var rowObj = [];
	rowObj.push(['', 'Description', 'Payable', 'Paid']);
	rowObj.push(['A', 'Central Tax', '', '', '', '']);
	rowObj.push(['B', 'State Tax', '', '', '', '']);

	return rowObj;
};

var getObjectFor7 = function getObjectFor7() {
	var rowObj = [];
	rowObj.push(['', 'Description', 'Central Tax', 'State/ UT Tax', 'Integrated Tax', 'Cess']);
	rowObj.push(['A', 'As per Rule 37', '', '', '', '']);
	rowObj.push(['B', 'As per Rule 39', '', '', '', '']);
	rowObj.push(['C', 'As per Rule 42', '', '', '', '']);
	rowObj.push(['D', 'As per Rule 43', '', '', '', '']);
	rowObj.push(['E', 'As per section 17(5)', '', '', '', '']);
	rowObj.push(['F', 'Reversal of TRAN-I credit', '', '', '', '']);
	rowObj.push(['G', '	Reversal of TRAN-II credit', '', '', '', '']);
	rowObj.push(['H', 'Other reversals (pl. specify)', '', '', '', '']);
	rowObj.push(['I', 'Total ITC reversed (A to H above)', '', '', '', '']);
	rowObj.push(['J', 'Net ITC Available for Utilization (6O -7I)', '', '', '', '']);

	return rowObj;
};

function prepareExcel() {
	var XLSX = require('xlsx');

	workbook = {
		'SheetNames': [],
		'Sheets': {}
	};

	/// /////////////////////////////////5A1///////////////////////////////////////

	var wsNameItem = '4';
	workbook.SheetNames[0] = wsNameItem;
	var data5A1bArray = getObjectFor4();
	var data5A1Worksheet = XLSX.utils.aoa_to_sheet(data5A1bArray);
	workbook.Sheets[wsNameItem] = data5A1Worksheet;
	var wsItemcolWidth = [{ wch: 10 }, { wch: 60 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	data5A1Worksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////5A2///////////////////////////////////////

	var wsNameItem = '5';
	workbook.SheetNames[1] = wsNameItem;
	var data5A2bArray = getObjectFor5();
	var data5A2Worksheet = XLSX.utils.aoa_to_sheet(data5A2bArray);
	workbook.Sheets[wsNameItem] = data5A2Worksheet;
	var wsItemcolWidth = [{ wch: 10 }, { wch: 60 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	data5A2Worksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////5A3///////////////////////////////////////

	var wsNameItem = '6';
	workbook.SheetNames[2] = wsNameItem;
	var data5A3bArray = getObjectFor6();
	var data5A3Worksheet = XLSX.utils.aoa_to_sheet(data5A3bArray);
	workbook.Sheets[wsNameItem] = data5A3Worksheet;
	var wsItemcolWidth = [{ wch: 10 }, { wch: 60 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	data5A3Worksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////5A4///////////////////////////////////////

	var wsNameItem = '7';
	workbook.SheetNames[3] = wsNameItem;
	var data5A4bArray = getObjectFor7();
	var data5A4Worksheet = XLSX.utils.aoa_to_sheet(data5A4bArray);
	workbook.Sheets[wsNameItem] = data5A4Worksheet;
	var wsItemcolWidth = [{ wch: 10 }, { wch: 60 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	data5A4Worksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////5B1///////////////////////////////////////

	var wsNameItem = '8';
	workbook.SheetNames[4] = wsNameItem;
	var data5B1Array = getObjectFor8();
	var data5B1Worksheet = XLSX.utils.aoa_to_sheet(data5B1Array);
	workbook.Sheets[wsNameItem] = data5B1Worksheet;
	var wsItemcolWidth = [{ wch: 10 }, { wch: 60 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	data5B1Worksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////5B2///////////////////////////////////////

	var wsNameItem = '9';
	workbook.SheetNames[5] = wsNameItem;
	var data5B2Array = getObjectFor9();
	var data5B2Worksheet = XLSX.utils.aoa_to_sheet(data5B2Array);
	workbook.Sheets[wsNameItem] = data5B2Worksheet;
	var wsItemcolWidth = [{ wch: 10 }, { wch: 60 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	data5B2Worksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////5C///////////////////////////////////////

	var wsNameItem = '10_11_12_13';
	workbook.SheetNames[6] = wsNameItem;
	var data5CArray = getObjectFor10111213();
	var data5CWorksheet = XLSX.utils.aoa_to_sheet(data5CArray);
	workbook.Sheets[wsNameItem] = data5CWorksheet;
	var wsItemcolWidth = [{ wch: 10 }, { wch: 60 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	data5CWorksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////6A///////////////////////////////////////

	var wsNameItem = '14';
	workbook.SheetNames[7] = wsNameItem;
	var data6AArray = getObjectFor14();
	var data6AWorksheet = XLSX.utils.aoa_to_sheet(data6AArray);
	workbook.Sheets[wsNameItem] = data6AWorksheet;
	var wsItemcolWidth = [{ wch: 10 }, { wch: 60 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	data6AWorksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////6B///////////////////////////////////////

	var wsNameItem = '15';
	workbook.SheetNames[8] = wsNameItem;
	var data6BArray = getObjectFor15();
	var data6BWorksheet = XLSX.utils.aoa_to_sheet(data6BArray);
	workbook.Sheets[wsNameItem] = data6BWorksheet;
	var wsItemcolWidth = [{ wch: 10 }, { wch: 60 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	data6BWorksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////7A///////////////////////////////////////

	var wsNameItem = '16';
	workbook.SheetNames[9] = wsNameItem;
	var data7AArray = getObjectFor16();
	var data7AWorksheet = XLSX.utils.aoa_to_sheet(data7AArray);
	workbook.Sheets[wsNameItem] = data7AWorksheet;
	var wsItemcolWidth = [{ wch: 10 }, { wch: 60 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	data7AWorksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////7B///////////////////////////////////////

	var wsNameItem = '17';
	workbook.SheetNames[10] = wsNameItem;
	var data7BArray = getObjectFor17();
	var data7BWorksheet = XLSX.utils.aoa_to_sheet(data7BArray);
	workbook.Sheets[wsNameItem] = data7BWorksheet;
	var wsItemcolWidth = [{ wch: 10 }, { wch: 60 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	data7BWorksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////7C///////////////////////////////////////

	var wsNameItem = '18';
	workbook.SheetNames[11] = wsNameItem;
	var data7CArray = getObjectFor18();
	var data7CWorksheet = XLSX.utils.aoa_to_sheet(data7CArray);
	workbook.Sheets[wsNameItem] = data7CWorksheet;
	var wsItemcolWidth = [{ wch: 10 }, { wch: 60 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	data7CWorksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////7D///////////////////////////////////////

	var wsNameItem = '19';
	workbook.SheetNames[12] = wsNameItem;
	var data7DArray = getObjectFor19();
	var data7DWorksheet = XLSX.utils.aoa_to_sheet(data7DArray);
	workbook.Sheets[wsNameItem] = data7DWorksheet;
	var wsItemcolWidth = [{ wch: 10 }, { wch: 60 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	data7DWorksheet['!cols'] = wsItemcolWidth;
}

function printExcel() {
	prepareExcel();
	var XLSX = require('xlsx');
	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

populateTable();