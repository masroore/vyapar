var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

var _typeof2 = require('babel-runtime/helpers/typeof');

var _typeof3 = _interopRequireDefault(_typeof2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var NameGroupCache = require('./../Cache/PartyGroupCache.js');
var nameGroupCache = new NameGroupCache();
var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();

var udfValueModel = require('./../Models/udfValueModel.js');

var udfConstants = require('./../Constants/UDFFieldsConstant.js');

var UDFCache = require('./../Cache/UDFCache.js');
var uDFCache = new UDFCache();

var NameCache = require('./../Cache/NameCache.js');
var nameCache = new NameCache();
// nameCache.reloadNameCache();

var TxnPaymentStatusConstants = require('./../Constants/TxnPaymentStatusConstants.js');

if (settingCache.getPartyShippingAddressEnabled()) {
	$('#shippingAddress').closest('tr').removeClass('hide');
}

if (settingCache.isPartyGroupEnabled()) {
	$('#partyGroupParent').css('display', 'block');
}

$('#tinNumberPartyLabel').html(settingCache.getTINText());

$('#curBal').keyup(function () {
	if ($('#curBal').val() != 0) {
		$('#openingBalanceCheckBox').removeClass('hide');
	} else {
		$('#openingBalanceCheckBox').addClass('hide');
	}
});

function udfKeyDown(e) {
	if (e.keyCode === 8) {} else {
		return false;
	}
}

setUdfFields();

function setUdfFields() {
	var UDFCache = require('./../Cache/UDFCache.js');
	var uDFCache = new UDFCache();
	var partyFieldMap = uDFCache.getPartyFieldsMap();

	var htmlString = '';

	var dateFormat = 0;
	var atleastOneUdfExists = 0;
	partyFieldMap.forEach(function (value, key) {
		var fieldModel = value;
		if (fieldModel && fieldModel.getUdfFieldStatus() == 1) {
			atleastOneUdfExists = 1;
			var fieldSeq = fieldModel.getUdfFieldNumber();
			if (fieldSeq == udfConstants.FIELD_FOR_DATE) {
				htmlString += '<div class="width50 floatLeft margintop">\n\t\t\t\t\t<div class="inputLabel" id="extraPartyField' + fieldSeq + 'Div" data-value=' + fieldModel.getUdfFieldId() + '>\n\t\t\t\t\t\t<div class="labelText" id="extraPartyField' + fieldSeq + 'Header">' + fieldModel.getUdfFieldName() + '</div>\n\t\t\t\t\t\t<input maxlength="30" type="text" id="extraPartyField' + fieldSeq + '" data-label="' + fieldModel.getUdfFieldId() + '" class="margintop datePicker" onkeydown="return udfKeyDown(event)" placeholder=""  tabindex="219" oninput="DecimalInputFilter.inputTextFilterForDate(this)"/>\n\t\t\t\t\t</div>\n\t\t\t\t</div>';
				dateFormat = fieldModel.getUdfFieldDataFormat();
			} else {
				htmlString += '<div class="width50 floatLeft margintop">\n\t\t\t\t\t\t<div class="inputLabel" id="extraPartyField' + fieldSeq + 'Div" data-value=' + fieldModel.getUdfFieldId() + '>\n\t\t\t\t\t\t\t<div class="labelText" id="extraPartyField' + fieldSeq + 'Header">' + fieldModel.getUdfFieldName() + '</div>\n\t\t\t\t\t\t\t<input type="text" maxlength="30" id="extraPartyField' + fieldSeq + '" data-label="' + fieldModel.getUdfFieldId() + '" class="margintop"  placeholder="" tabindex="219" oninput="DecimalInputFilter.inputTextFilterForLength(this,45)"/>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>';
			}
		}
	});
	if (atleastOneUdfExists) htmlString = '<td colspan="2"><p><span class="udfLabelAdditionalFields">Additional Fields</span></p>' + htmlString + '</td">';
	$('#extraFields').html(htmlString);
	if (dateFormat == StringConstant.monthYear) {
		$('#extraPartyField4').monthpicker({ changeYear: true, dateFormat: 'mm/yy', minDate: '-100 Y', maxDate: '+1000 Y' });
	} else if (dateFormat == StringConstant.dateMonthYear) {
		$('#extraPartyField4').datepicker({ changeYear: true, dateFormat: 'dd/mm/yy', minDate: '-1000 Y', maxDate: '+100 Y' });
	}
}

function getUdfFields() {
	var UDFCache = require('./../Cache/UDFCache.js');
	var uDFCache = new UDFCache();
	var partyFieldMap = uDFCache.getPartyFieldsMap();

	var htmlString = '';
	var atleastOneUdfExists = 0;
	partyFieldMap.forEach(function (value, key) {
		var fieldModel = value;
		if (fieldModel && fieldModel.getUdfFieldStatus() == 1) {
			atleastOneUdfExists = 1;
			var fieldSeq = fieldModel.getUdfFieldNumber();
			if (fieldSeq == udfConstants.FIELD_FOR_DATE) {
				htmlString += '<div class="width50 floatLeft margintop">\n\t\t\t\t\t<div class="inputLabel" id="extraPartyField' + fieldSeq + 'Div" data-value=' + fieldModel.getUdfFieldId() + '>\n\t\t\t\t\t\t<div class="labelText" id="extraPartyField' + fieldSeq + 'Header">' + fieldModel.getUdfFieldName() + '</div>\n\t\t\t\t\t\t<input maxlength="30" type="text" id="extraPartyField' + fieldSeq + '" data-label="' + fieldModel.getUdfFieldId() + '" class="margintop datePicker" onkeydown="return udfKeyDown(event)" placeholder="" tabindex="219" oninput="DecimalInputFilter.inputTextFilterForDate(this)"/>\n\t\t\t\t\t</div>\n\t\t\t\t</div>';
				dateFormat = fieldModel.getUdfFieldDataFormat();
			} else {
				htmlString += '<div class="width50 floatLeft margintop">\n\t\t\t\t\t\t<div class="inputLabel" id="extraPartyField' + fieldSeq + 'Div" data-value=' + fieldModel.getUdfFieldId() + '>\n\t\t\t\t\t\t\t<div class="labelText" id="extraPartyField' + fieldSeq + 'Header">' + fieldModel.getUdfFieldName() + '</div>\n\t\t\t\t\t\t\t<input type="text" maxlength="30" id="extraPartyField' + fieldSeq + '" data-label="' + fieldModel.getUdfFieldId() + '" class="margintop"  placeholder="" tabindex="219" oninput="DecimalInputFilter.inputTextFilterForLength(this,45)"/>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>';
			}
		}
	});
	var extraStringIncluded = false;
	if (atleastOneUdfExists) {
		extraStringIncluded = true;htmlString = '<td colspan="2"><p><span class="udfLabelAdditionalFields">Additional Fields</span></p>' + htmlString + '</td">';
	};
	return { 'htmlString': htmlString, 'atleastOneUdfExists': atleastOneUdfExists, 'extraStringIncluded': extraStringIncluded };
}

if (settingCache.isTINNumberEnabled()) {
	if (settingCache.getGSTEnabled()) {
		$('#tinNumberParent').addClass('hide');
		$('#gstinNumberParent').removeClass('hide');
		$('#contactStateParent').removeClass('hide');
	} else {
		$('#showGSTField').css('display', 'block');
		$('#tinNumberParent').removeClass('hide');
		$('#gstinNumberParent').addClass('hide');
		$('#contactStateParent').addClass('hide');
	}
}

if (!settingCache.getGSTEnabled()) {
	$('#customerType').closest('td').hide();
}

$('#contactGSTinNumber').on('keyup', function () {
	var value = $('#contactGSTinNumber').val();
	if (value) {
		var stateNo = Number(value.substr(0, 2));
	} else {
		stateNo = 0;
	}
	if (settingCache.getGSTEnabled() && stateNo > 0) {
		var state = StateCode.getStateName(stateNo);
		if (!state) {
			$('#contactState').val('');
		} else {
			$('#contactState').val(state);
		}
	} else {
		try {
			$('#contactState').val('');
		} catch (err) {}
	}
});

var fillState = function fillState() {
	var listOfStates = StateCode.getStateList();
	$('<option>').val('').text('NONE').appendTo('#contactState');
	$.each(listOfStates, function (key, value) {
		$('<option>').val(value).text(value).appendTo('#contactState');
	});
};

fillState();

var displayGroups = function displayGroups() {
	var listOfGroups = nameGroupCache.getNameGroupList();
	$('#groupNameSelect').text('');
	// var MyDate = require('./../Utilities/MyDate.js');
	$.each(listOfGroups, function (key, value) {
		// categoryName
		var grpId = value['groupId'];
		var grpName = value['groupName'];
		$('<option>').val(grpId).text(grpName).appendTo('#groupNameSelect');
	});
};

displayGroups();

MyAnalytics.pushScreen('New Contact');

if ((typeof module === 'undefined' ? 'undefined' : (0, _typeof3.default)(module)) === 'object') {
	window.module = module;module = undefined;
}

$(function () {
	$('#contactName').focus();
	if ($('#curBal').val() != 0) {
		$('#openingBalanceCheckBox').removeClass('hide');
	} else {
		$('#openingBalanceCheckBox').addClass('hide');
	}
	var DateFormat = require('./../Constants/DateFormat.js');
	$('#datepickerContact').datepicker({
		dateFormat: DateFormat.format,
		buttonImage: '../../inlineSVG/item_adj.svg',
		onSelect: function onSelect() {
			this.focus();
		},
		onClose: function onClose() {
			this.focus();
		}
	});
});

$(function () {
	$('input[name=isReceivable]').checkboxradio();
});

var nospecial = /^[^*|\"$^:<>[\]{}`\\()';@&$]+$/;

$('#contactGSTinNumber').on('keyup', function () {
	if (!$('#contactGSTinNumber').val().match(nospecial)) {
		var val = $('#contactTinNumber').val().slice(0, -1);
		$('#contactTinNumber').val(val);
	}
});

$('#newGroup').on('focus', function () {
	$(this).addClass('focusForButton');
}).on('focusout', function () {
	$(this).removeClass('focusForButton');
});

// $('#newGroupDialogNewContact').dialog({
// 	autoOpen: false,
// 	resizable: false,
// 	height: 'auto',
// 	width: 600,
// 	modal: true,
// 	appendTo: '#frameDiv'
// 	// title:'Add New Group',
// });

$('#newGroup').on('click', function () {
	$('#newGroupDialogNewContact').dialog({
		autoOpen: true,
		resizable: false,
		height: 'auto',
		width: 600,
		modal: true,
		appendTo: 'body',
		close: function close(event, ui) {
			$(this).dialog('destroy');
		}
		// title:'Add New Group',
	});
});

var handleSaveNewGroupInParties = function handleSaveNewGroupInParties() {
	var NewPartyGroupLogic = require('./../BizLogic/PartyGroupLogic.js');
	var newPartyGroupLogic = new NewPartyGroupLogic();
	var name = $('#newGroupName').val();
	var statusCode = newPartyGroupLogic.saveNewGroup(name);

	if (statusCode == ErrorCode.ERROR_PARTYGROUP_SAVE_SUCCESS) {
		ToastHelper.success(statusCode);
		$('#newGroupDialogNewContact').find('input:text').val('');
		$('#newGroupDialogNewContact').dialog('destroy');
		$('#newGroupDialogNewContact').hide();
		displayGroups();
		var groupId = nameGroupCache.getPartyGroupId(name);
		$('#groupNameSelect option[value="' + groupId + '"]').prop('selected', true);
	} else {
		ToastHelper.error(statusCode);
	}
};
$('.terminalButtonGroup').on('click', handleSaveNewGroupInParties);
function auto_grow(element) {
	element.style.height = '5px';
	element.style.height = element.scrollHeight + 'px';
}
//    var GetDate = require('./../BizLogic/GetDate.js');
//    var getDate = new GetDate();
var date = MyDate.getDate('d/m/y');
if (typeof oldDate != 'undefined' && oldDate) {
	date = oldDate;
}
$('#datepickerContact').val(date);
var userName = userNameGlobal;
// var userName = angular.element(document.getElementById('body')).scope().userName;
if (userName) {
	var NameCache = require('./../Cache/NameCache.js');
	var namecache = new NameCache();
	var userObj = namecache.findNameModelByName(userName);
	$('#heading').text('EDIT PARTY');
	MyAnalytics.pushEvent('Edit Party Open');

	if (!userObj) {
		$('#heading').html('EDIT EXPENSE');
		userObj = nameCache.findExpenseModelByName(userName);
	}

	$('#groupNameSelect').val(userObj['groupId']);
	var NameLogic = require('./../BizLogic/nameLogic.js');
	var namelogic = new NameLogic();
	var openingBalanceTransactionObject = namelogic.getTransactionObjectOnNameId(userObj.nameId);

	if (openingBalanceTransactionObject) {
		var date = openingBalanceTransactionObject['txnDate'];
		var newDate = MyDate.getDate('d/m/y', date);
	}

	$('#submitNew').css('display', 'none');
	var customerType = userObj.getCustomerType();
	$('#customerType').val(customerType);
	if (MyDouble.convertStringToDouble(customerType)) {
		$('#showGSTField').css('display', 'block');
	}
	$('#contactName').val(userObj.fullName);
	$('#contactPhoneNo').val(userObj.phoneNumber);
	$('#contactAddress').val(userObj.address);
	$('#contactEmailID').val(userObj.email);
	$('#datepickerContact').val(newDate || MyDate.getDate('d/m/y'));
	$('#shippingAddress').val(userObj.getShippingAddress());
	$('#contactGSTinNumber').val(userObj.getGstinNumber() && userObj.getGstinNumber() != 'undefined' ? userObj.getGstinNumber() : '');
	$('#contactState').val(userObj.getContactState());
	$('#contactTinNumber').val(userObj['tinNumber'] || '');

	// fill extra fields
	var MyString = require('./../Utilities/MyString.js');
	var extraFieldObjectArray = userObj.getUdfObjectArray();
	var udfFieldsString = getUdfFields();
	var htmlString = udfFieldsString['htmlString'];
	var atleastOneUdfExists = udfFieldsString['atleastOneUdfExists'];
	var extraStringIncluded = udfFieldsString['extraStringIncluded'];
	var dateFormat = 0;
	if (extraFieldObjectArray && extraFieldObjectArray.length) {
		for (var i = 0; i < extraFieldObjectArray.length; i++) {
			var fieldModel = extraFieldObjectArray[i];
			var udfModel = uDFCache.getUdfPartyModelByFieldId(fieldModel.getUdfFieldId());
			var fieldSeq = udfModel.getUdfFieldNumber();
			if (udfModel.getUdfFieldDataType() == udfConstants.DATA_TYPE_DATE) {
				dateFormat = udfModel.getUdfFieldDataFormat();
				if ($('#extraPartyField' + fieldSeq + 'Div').length == 0) {
					atleastOneUdfExists = 1;
					htmlString += '<div class="width50 floatLeft margintop">\n\t\t\t\t\t\t<div class="inputLabel" id="extraPartyField' + fieldSeq + 'Div" data-value=' + fieldModel.getUdfFieldId() + '>\n\t\t\t\t\t\t\t<div class="labelText" id="extraPartyField' + fieldSeq + 'Header">' + udfModel.getUdfFieldName() + '</div>\n\t\t\t\t\t\t\t<input type="text" maxlength="30" value="" id="extraPartyField' + fieldSeq + '" onkeydown="return udfKeyDown(event)" class="margintop"  placeholder="" tabindex="219" oninput="DecimalInputFilter.inputTextFilterForLength(this,45)"/>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>';
				}
			} else {
				if ($('#extraPartyField' + fieldSeq + 'Div').length == 0) {
					atleastOneUdfExists = 1;
					htmlString += '<div class="width50 floatLeft margintop">\n\t\t\t\t\t\t<div class="inputLabel" id="extraPartyField' + fieldSeq + 'Div" data-value=' + fieldModel.getUdfFieldId() + '>\n\t\t\t\t\t\t\t<div class="labelText" id="extraPartyField' + fieldSeq + 'Header">' + udfModel.getUdfFieldName() + '</div>\n\t\t\t\t\t\t\t<input type="text" value="" maxlength="30" id="extraPartyField' + fieldSeq + '" class="margintop"  placeholder="" tabindex="219" oninput="DecimalInputFilter.inputTextFilterForLength(this,45)"/>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>';
				}
			}
		}
		if (atleastOneUdfExists && !extraStringIncluded) htmlString = '<td colspan="2"><p><span class="udfLabelAdditionalFields">Additional Fields</span></p>' + htmlString + '</td">';
		$('#extraFields').html(htmlString);

		for (i = 0; i < extraFieldObjectArray.length; i++) {
			var _fieldModel = extraFieldObjectArray[i];
			var _udfModel = uDFCache.getUdfPartyModelByFieldId(_fieldModel.getUdfFieldId());
			var _fieldSeq = _udfModel.getUdfFieldNumber();
			var displayValue = _fieldModel.getDisplayValue(_udfModel);
			$('#extraPartyField' + _fieldSeq).val(displayValue);
		}
		if (dateFormat == StringConstant.monthYear) {
			$('#extraPartyField4').monthpicker({ changeYear: true, dateFormat: 'mm/yy' });
		} else if (dateFormat == StringConstant.dateMonthYear) {
			$('#extraPartyField4').datepicker({ changeYear: true, dateFormat: 'dd/mm/yy' });
		}
	}
	if (openingBalanceTransactionObject) {
		var TransactionLinksObject = require('./../BizLogic/TransactionLinksObject.js');
		var transactionLinksObject = new TransactionLinksObject();

		var statusCode = transactionLinksObject.isTransactionLinked(openingBalanceTransactionObject.getTxnId());

		if (statusCode == ErrorCode.ERROR_TXN_LINK_EXIST) {
			$('#showLinkedTransactions').removeClass('hide');
			$('#curBal').css('width', '90%');
			$('#curBal').attr('disabled', true);
			$('#isReceivable').attr('disabled', true);
			$('#isPayable').attr('disabled', true);
		}

		var curBalHere = openingBalanceTransactionObject.getBalanceAmount();

		document.getElementById('curBal').value = MyDouble.getAmountWithDecimal(curBalHere);

		var txnType = Number(openingBalanceTransactionObject.getTxnType());

		switch (txnType) {
			case TxnTypeConstant.TXN_TYPE_ROPENBALANCE:
				document.getElementById('isReceivable').checked = true;
				break;
			case TxnTypeConstant.TXN_TYPE_POPENBALANCE:
				document.getElementById('isPayable').checked = true;
				break;
		}
	} else {
		document.getElementById('curBal').value = MyDouble.convertStringToDouble(0);
	}

	$('.terminalButtonContact').on('click', function () {
		uDFCache = new UDFCache();
		var ErrorCode = require('./../Constants/ErrorCode.js');

		var customerType = Number($('#customerType').val());
		var gSTIN = $('#contactGSTinNumber').val();

		var regex = new RegExp(StringConstant.GSTINregex);
		if (!(regex.test(gSTIN) || gSTIN.trim() == '')) {
			ToastHelper.error(ErrorCode.ERROR_GST_INVALID);
			return;
		}
		var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
		var newnamelogic = new NameLogic();
		var NameType = require('./../Constants/NameType.js');
		var name = document.getElementById('contactName').value;
		var phone = document.getElementById('contactPhoneNo').value;
		var addresss = document.getElementById('contactAddress').value;
		var email = document.getElementById('contactEmailID').value;
		var shippingAddresss = document.getElementById('shippingAddress').value;
		var bal = MyDouble.convertStringToDouble(document.getElementById('curBal').value);
		var state = $('#contactState').val() ? $('#contactState').val() : '';
		var date = document.getElementById('datepickerContact').value;
		var groupId = $('#groupNameSelect').val();
		date = MyDate.getDateObj(date, 'dd/mm/yyyy', '/');
		var isReceivable = document.getElementById('isReceivable').checked ? TxnTypeConstant['TXN_TYPE_ROPENBALANCE'] : TxnTypeConstant['TXN_TYPE_POPENBALANCE'];

		var extraFieldValueArray = [];

		for (var i = 1; i <= udfConstants.TOTAL_EXTRA_FIELDS; i++) {
			var fieldSequence = i;
			if ($('#extraPartyField' + fieldSequence).length) {
				var udfValueObj = new udfValueModel();
				if (fieldSequence == udfConstants.FIELD_FOR_DATE) {
					var val = MyDate.getDateByMonthYearString($('#extraPartyField' + fieldSequence).val());
					if (val && val != '') {
						val = MyDate.getDate('y-m-d H:M:S', val);
					}
				} else {
					val = $('#extraPartyField' + fieldSequence).val();
				}
				udfValueObj.setUdfFieldValue(val);
				udfValueObj.setUdfFieldId($('#extraPartyField' + fieldSequence + 'Div').data('value'));
				udfValueObj.setUdfFieldType(udfConstants.FIELD_TYPE_PARTY);
				extraFieldValueArray.push(udfValueObj);
			}
		}
		var obj = {
			'name': name.trim(),
			'phone_number': phone,
			'addressStr': addresss,
			'email': email,
			'opening_balance': bal,
			'shippingAddress': shippingAddresss.trim(),
			'opening_balance_date': date,
			'isReceivable': isReceivable,
			'nameType': NameType.NAME_TYPE_PARTY,
			'groupId': groupId,
			'gstinNumber': $('#contactGSTinNumber').val().trim() ? $('#contactGSTinNumber').val().trim() : '',
			'tinNumber': $('#contactTinNumber').val().trim() ? $('#contactTinNumber').val().trim() : '',
			'contactState': state,
			'nameId': userObj.nameId,
			'customerType': customerType || 0,
			'extraFieldValueArray': extraFieldValueArray
		};

		namecache = new NameCache();
		userObj = namecache.findNameModelByName(userNameGlobal);

		if (!userObj) {
			userObj = nameCache.findExpenseModelByName(userNameGlobal);
		}

		openingBalanceTransactionObject = namelogic.getTransactionObjectOnNameId(userObj.nameId);
		var statusCode = userObj.updateContactName(openingBalanceTransactionObject, obj);

		if (statusCode == ErrorCode.ERROR_NAME_UPDATE_SUCCESS) {
			document.dispatchEvent(new CustomEvent('saveTxnEvent'));
			$('.terminalButtonGroup').unbind('click', handleSaveNewGroupInParties);
			ToastHelper.success(statusCode);
			MyAnalytics.pushEvent('Edit Party Save');
			emptyForm();
			try {
				$('#frameDiv').empty();
				$('#defaultPage').show();
				$('#modelContainer').hide();
				window.userNameGlobal = '';
				onResume(true, obj['name'].trim().toLowerCase());
			} catch (err) {
				console.log(err);
			}
		} else {
			ToastHelper.error(statusCode);
		}
	});

	$('#balanceDiv').html('Opening Balance');
	$('#curBal').attr('placeholder', 'Opening Balance');

	var showLinkedTxnDialog = function showLinkedTxnDialog(that) {
		var DataLoader = require('./../DBManager/DataLoader.js');
		var dataLoader = new DataLoader();
		var txnId = openingBalanceTransactionObject.getTxnId();
		var txnLinksList = dataLoader.loadAllTransactionLinksByTxnId(txnId);

		var linkedTxnMap = new _map2.default();

		for (var i = 0; i < txnLinksList.length; i++) {
			var linkTxn = txnLinksList[i];
			var txnLink1Id = MyDouble.convertStringToDouble(linkTxn.getTxnLinksTxn1Id());
			var txnLink2Id = MyDouble.convertStringToDouble(linkTxn.getTxnLinksTxn2Id());
			var txnLinkAmount = MyDouble.convertStringToDouble(linkTxn.getTxnLinksAmount());

			if (Number(txnId) == txnLink1Id) {
				var selectedTxnId = txnLink2Id;
			} else if (Number(txnId) == txnLink2Id) {
				var selectedTxnId = txnLink1Id;
			}

			var linkTxnObject = dataLoader.LoadTransactionFromId(selectedTxnId);

			linkedTxnMap.set(selectedTxnId, [linkTxnObject, txnLinkAmount]);
		}

		var TransactionFactory = require('./../BizLogic/TransactionFactory.js');
		var TxnPaymentStatusConstants = require('./../Constants/TxnPaymentStatusConstants.js');
		var transactionFactory = new TransactionFactory();

		var htmlForB2BSelectedList = "<thead class='tablesorter tabTxnTable'><tr class='textLeft' style='border-collapse:collapse'>" + "<th width='20%'>Party Name</th>" + "<th width='12%'>Ref No.</th>" + "<th width='12%'>Transaction Type</th>" + "<th width='12%'>Payment Status</th>" + "<th width='12%'>Total Amount</th>" + "<th width='12%'>Current Balance</th>";

		htmlForB2BSelectedList += "<th width='20%'>Linked Amount</th>";
		htmlForB2BSelectedList += '</tr></thead><tbody>';

		linkedTxnMap.forEach(function (value, key) {
			var selectedTxnId = key;
			var selectedTxn = value[0];
			var selectedTxnAmount = MyDouble.convertStringToDouble(value[1]);

			var partyName = selectedTxn.getNameRef().getFullName();
			var invoiceNo = selectedTxn.getFullTxnRefNumber();
			var transactionTypeString = transactionFactory.getTransTypeString(selectedTxn.getTxnType());
			var paymentStatus = selectedTxn.getTxnPaymentStatus();
			var paymentStatusString = paymentStatus == TxnPaymentStatusConstants.UNPAID ? 'Unpaid' : 'Partial';
			var txnTotalAmount = MyDouble.convertStringToDouble(selectedTxn.getCashAmount()) + MyDouble.convertStringToDouble(selectedTxn.getBalanceAmount());
			var txnCurrentBalanceAmount = MyDouble.convertStringToDouble(selectedTxn.getTxnCurrentBalanceAmount());

			htmlForB2BSelectedList += "<tr class='currentRow' style='height: 35px;'>" + "<td class='tdSpacing'>" + partyName + '</td>' + "<td class='tdSpacing'>" + invoiceNo + '</td>' + "<td class='tdSpacing'>" + transactionTypeString + '</td>' + "<td class='tdSpacing'>" + paymentStatusString + '</td>' + "<td class='tdSpacing'>" + txnTotalAmount + '</td>' + "<td class='tdSpacing'>" + txnCurrentBalanceAmount + '</td>';
			htmlForB2BSelectedList += "<td class='tdSpacing'>" + selectedTxnAmount + '</td>';
			htmlForB2BSelectedList += '</tr>';
		});

		htmlForB2BSelectedList += '</tbody>';

		$('#linkedTransactionTable').html(htmlForB2BSelectedList);

		$('#linkedTransactionDialog').dialog({
			'title': 'Linked Transactions',
			'width': '1000',
			'closeOnEscape': false,
			appendTo: '#frameDiv',
			modal: true
		});
	};
	userName = '';
} else {
	var saveNewContact = function saveNewContact() {
		var ErrorCode = require('./../Constants/ErrorCode.js');
		var customerType = Number($('#customerType').val());
		var gSTIN = $('#contactGSTinNumber').val();
		var regex = new RegExp(StringConstant.GSTINregex);
		if (!(regex.test(gSTIN) || gSTIN.trim() == '')) {
			ToastHelper.error(ErrorCode.ERROR_GST_INVALID);
			return;
		}

		var NameType = require('./../Constants/NameType.js');
		var nameLogic = require('./../BizLogic/nameLogic.js');
		var namelogic = new nameLogic();
		var name = document.getElementById('contactName').value;
		var phone = document.getElementById('contactPhoneNo').value;
		var addresss = document.getElementById('contactAddress').value;
		var shippingAddresss = document.getElementById('shippingAddress').value;
		var email = document.getElementById('contactEmailID').value;
		var bal = MyDouble.convertStringToDouble(document.getElementById('curBal').value);
		var date = document.getElementById('datepickerContact').value;
		date = MyDate.getDateObj(date, 'dd/mm/yyyy', '/');
		var state = $('#contactState').val() ? $('#contactState').val() : '';
		var isReceivable = document.getElementById('isReceivable').checked ? 1 : 0;
		var extraFieldValueArray = [];
		for (var i = 1; i <= udfConstants.TOTAL_EXTRA_FIELDS; i++) {
			var fieldSequence = i;
			if ($('#extraPartyField' + fieldSequence).length) {
				udfValueObj = new udfValueModel();
				if (fieldSequence == udfConstants.FIELD_FOR_DATE) {
					var val = MyDate.getDateByMonthYearString($('#extraPartyField' + fieldSequence).val());
					if (val && val != '') {
						val = MyDate.getDate('y-m-d H:M:S', val);
					}
				} else {
					var val = $('#extraPartyField' + fieldSequence).val();
				}
				udfValueObj.setUdfFieldValue(val);
				udfValueObj.setUdfFieldId($('#extraPartyField' + fieldSequence + 'Div').data('value'));
				udfValueObj.setUdfFieldType(udfConstants.FIELD_TYPE_PARTY);
				extraFieldValueArray.push(udfValueObj);
			}
		} // );

		var obj = {
			'name': name.trim(),
			'phone_number': phone.trim(),
			'addressStr': addresss.trim(),
			'shippingAddress': shippingAddresss.trim(),
			'email': email.trim(),
			'opening_balance': bal,
			'opening_balance_date': date,
			'groupId': $('#groupNameSelect').val(),
			'tinNumber': $('#contactTinNumber').val().trim() ? $('#contactTinNumber').val().trim() : '',
			'gstinNumber': $('#contactGSTinNumber').val().trim() ? $('#contactGSTinNumber').val().trim() : '',
			'isReceivable': isReceivable,
			'contactState': state,
			'nameType': NameType.NAME_TYPE_PARTY,
			'customerType': customerType || 0,
			'extraFieldValueArray': extraFieldValueArray
		};

		var that = this;
		$('#loading').show();
		nameCache.reloadNameCache();

		var statusCode = namelogic.saveNewName(obj, false);
		$('#loading').hide();

		if (statusCode == ErrorCode.ERROR_NAME_SAVE_SUCCESS) {
			document.dispatchEvent(new CustomEvent('saveTxnEvent'));
			$('.terminalButtonGroup').off('click', handleSaveNewGroupInParties);
			ToastHelper.success(statusCode);
			if (that.id == 'submit') {
				MyAnalytics.pushEvent('Add Party Save - new');

				if ($('#addNewNameDialog').is(':ui-dialog') && $('#addNewNameDialog').dialog('isOpen')) {
					$('#addNewNameDialog').dialog('close');
					return;
				}
				emptyForm();
				$('#addParty').html('');
				oldDate = null;

				$('#defaultPage').show();
				// if (!$('#modelContainer').css('display')=='none') {

				$('#modelContainer').css('display', 'none');
				// }
				$('.hideDefaultPage').css('display', 'block');
				try {
					onResume(true, obj['name'].trim().toLowerCase());
				} catch (err) {
					console.log(err);
				}
			} else {
				MyAnalytics.pushEvent('Add Party Save - new');
				oldDate = $('#datepickerContact').val();
				userNameGlobal = '';
				$('#frameDiv').load('NewContact.html');
			}
		} else {
			ToastHelper.error(statusCode);
		}

		// console.log(ErrorCode[statusCode);
	};

	MyAnalytics.pushEvent('Add Party Open');
	$('.terminalButtonContact').on('click', saveNewContact);

	;
	// require('./../UIControllers/NewContactController.js');
}

var emptyForm = function emptyForm() {
	var inputs = $('.form input');
	for (var i = 0; i < inputs.length; i++) {
		inputs[i].value = '';
	}
};

if (window.module) module = window.module;

// code to show/hide gst field based on customer type

$('#customerType').on('change', function () {
	var selectedVal = Number(this.value);
	if (!selectedVal) {
		$('#showGSTField').css('display', 'none');
		$('#contactGSTinNumber').val('');
		$('#contactTinNumber').val('');
	} else {
		$('#showGSTField').css('display', 'block');
	}
});