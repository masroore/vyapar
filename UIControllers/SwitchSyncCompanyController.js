
function joinCompany() {
	var ulJn = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'ulCompany';
	var showLoader = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

	var Domain = require('./../Constants/Domain');
	var domain = Domain.thisDomain;
	var host = Domain.thisHost;
	var fs = require('fs');
	var http = require('http');

	var SyncHelper = require('./../Utilities/SyncHelper');
	var TokenForSync = require('./../Utilities/TokenForSync');

	if (TokenForSync.ifExistToken()) {
		var tokenSync = TokenForSync.readToken();
		var VYAPAR_AUTH_TOKEN = tokenSync.auth_token;
		fetchListOfCompanies(VYAPAR_AUTH_TOKEN);
	} else {
		SyncHelper.loginToGetAuthToken(function () {
			var tokenSync = TokenForSync.readToken();
			var VYAPAR_AUTH_TOKEN = tokenSync.auth_token;
			fetchListOfCompanies(VYAPAR_AUTH_TOKEN);
			if (TokenForSync.ifExistToken()) {
				$('#joinComp').addClass('hide');
				$('#joinComp').css('display', 'none');
			}
		});
	}

	function fetchListOfCompanies(VYAPAR_AUTH_TOKEN) {
		function ajaxForFetchingCompanies() {
			$.ajax({
				async: true,
				type: 'GET',
				url: domain + '/api/sync/company/list',
				headers: {
					'Accept': 'application/json',
					'Authorization': 'Bearer ' + VYAPAR_AUTH_TOKEN
				},
				data: {},
				success: function success(data) {
					// var listToAppend = $('#'+ulJn);
					listToAppend.addClass('floatLeft');
					var CompanyListUtility = require('./../Utilities/CompanyListUtility.js');
					var fileNameObj = CompanyListUtility.getCompaniesFileNameObject();
					var fs = require('fs');
					data.forEach(function (companies, index, arr) {
						var flagToAppendJoinCompanyList = 0;
						if (companies.company) {
							try {
								var company_name = companies.company.unique_name;

								$.each(fileNameObj, function (compName) {
									if (fileNameObj[compName].company_global_id && fileNameObj[compName].company_global_id == company_name) {
										flagToAppendJoinCompanyList = 1;
										return false;
									}
								});
							} catch (e) {}

							if (flagToAppendJoinCompanyList == 0 && companies.company) {
								var db_version = companies.db_version ? companies.db_version : 0;
								listToAppend.append("<div style='font-family:roboto'><li data-company_id='" + companies.company_id + "' class='x halfDiv'>" + "<div id='syncsymbol' class='floatLeft'><svg fill=\"#097aa8\" height=\"24\" viewBox=\"0 0 24 24\" width=\"24\" xmlns=\"http://www.w3.org/2000/svg\">\n" + '    <path d="M12 4V1L8 5l4 4V6c3.31 0 6 2.69 6 6 0 1.01-.25 1.97-.7 2.8l1.46 1.46C19.54 15.03 20 13.57 20 12c0-4.42-3.58-8-8-8zm0 14c-3.31 0-6-2.69-6-6 0-1.01.25-1.97.7-2.8L5.24 7.74C4.46 8.97 4 10.43 4 12c0 4.42 3.58 8 8 8v3l4-4-4-4v3z"/>\n' + '    <path d="M0 0h24v24H0z" fill="none"/>\n' + '</svg></div>' + "<span class='businessKey' style='font-size:14px;'>" + "<div id='joinCompName'>" + companies.company.name + '</div>' + "</span><svg class='closeSync' fill='#9e9e9e' height='20' viewBox='0 0 24 24' width='20' xmlns='http://www.w3.org/2000/svg'><path d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z'/><path d='M0 0h24v24H0z' fill='none'/></svg></span><br><div id='joinCompPath'><span style='font-size:12px;' class='path' >" + companies.company.unique_name + "</span></div><div id='joinbut' class=''><button id='joinbut2' db_version=" + db_version + " class='floatRight terminalButton joinButton'>Join</button> </div></li></div>");

								// $("#ulJoinCompany li").remove();
							}
						}
					});
					$('#loading').hide();
					// $('#joinComp').hide();
					$('#loadId').hide();
				},
				error: function error(xhr, ajaxOptions, thrownError) {
					if (xhr.status == 401) {
						var SyncHelper = require('./../Utilities/SyncHelper');
						SyncHelper.loginToGetAuthToken();
					}

					$('#loadId').hide();
					$('#loading').hide();
					// $('#joinComp').hide();
				}

			});
		}

		var listToAppend = $('#' + ulJn);

		if (showLoader == false) {
			// $('#loading').hide();
			listToAppend.append("<div id='loadId' style='font-family:roboto;'><li class='blink_me2'  style='background: none;box-shadow: none;text-align: center'>loading....</li></div>");
			ajaxForFetchingCompanies();
		} else {
			$('#loading').show(function () {
				ajaxForFetchingCompanies();
			});
		}
	}

	var download = function download(url, dest, cb) {
		debugger;
		var ErrorCode = require('./../Constants/ErrorCode.js');
		var tokenSync = TokenForSync.readToken();
		var VYAPAR_AUTH_TOKEN = tokenSync.auth_token;
		var file = fs.createWriteStream(dest);
		var request = http.get({
			host: host,
			path: url,
			headers: { 'Authorization': 'Bearer ' + VYAPAR_AUTH_TOKEN, 'Accept': 'application/json' }
		}, function (response) {
			var len = parseInt(response.headers['content-length'], 10);

			var cur = 0;

			// let rawData = '';

			// response.on('data', (chunk) => { rawData += chunk; });

			response.on('data', function (chunk) {
				if (response.statusCode == 200) {
					cur += chunk.length;
					var percent = (100.0 * cur / len).toFixed(2);
					if (percent == 100) {
						percent = 99;
					}

					var downloadtimeout = setTimeout(function () {
						$('.loaderWrapper2').show(function () {
							$('.loadingText2').html(percent + '%');
							$('.loadingText3').html('Downloading Company');
						});
					}, 0);
				}
			});

			response.on('end', function () {
				debugger;
			});
			var statusCode = response.statusCode;


			if (statusCode == 500) {
				response.resume();
				if (typeof downloadtimeout != 'undefined') {
					clearInterval(downloadtimeout);
				}
				$('.loaderWrapper2').hide();
				alert(ErrorCode.ERROR_WENT_WRONG);
				return;
			} else if (statusCode != 200 && statusCode != 500) {
				response.resume();
				if (typeof downloadtimeout != 'undefined') {
					clearInterval(downloadtimeout);
				}
				$('.loaderWrapper2').hide();
				alert(ErrorCode.ERROR_WENT_WRONG);
				return;
			}

			response.pipe(file);
			debugger;
			file.on('finish', function () {
				file.close(cb); // close() is async, call cb after close completes.
				file.end();
			});
		}).on('error', function (err) {
			debugger;
			console.log(err);

			// alert('download error');// Handle errors
			fs.unlink(dest, function (err) {}); // Delete the file async. (But we don't check the result)
			if (cb) cb();
		});
	};

	function openThisDBFunctionJoin() {
		var thisDb = Number($(this).attr('db_version'));

		if (APP_DB_VERSION < thisDb) {
			alert('Please update you app to current version to join this company');
			return;
		}

		var fs = require('fs');
		var ErrorCode = require('./../Constants/ErrorCode.js');
		var company_id = $($(this).closest('li')[0]).attr('data-company_id');
		console.log(company_id);
		var company_name = $($(this).closest('li')[0]).find('.businessKey').text();
		var company_global_id = $($(this).closest('li')[0]).find('.path').text();

		var app = require('electron').remote.app;

		var appPath = app.getPath('userData');
		var pathVar = appPath + '/BusinessNames/' + company_global_id;

		var CompanyListUtility = require('./../Utilities/CompanyListUtility.js');
		var fileNameObj = CompanyListUtility.getCompaniesFileNameObject();
		debugger;
		if (fs.existsSync(pathVar) && fileNameObj[company_name] && fileNameObj[company_name].path) {
			var statusCode = ErrorCode.ERROR_DB_ALREADY_EXISTS;
			alert(statusCode);
			return;
		}

		if (typeof company_id != 'undefined') {
			download('/api/sync/company/download/' + company_id, pathVar, function (err) {
				if (err) {
					alert('Error while downloading file. Please try again.');
					return;
				}
				var CompanyListUtility = require('./../Utilities/CompanyListUtility.js');
				var fileNameObj = CompanyListUtility.getCompaniesFileNameObject();

				company_name = company_name.replace(/(.*)\.vyp$/ig, '$1');
				var lastNotificationSentAt = new Date().getTime();
				fileNameObj[company_name] = {
					'path': pathVar,
					'company_creation_type': 1,
					'company_global_id': company_global_id,
					'lastNotificationSentAt': lastNotificationSentAt
				};

				CompanyListUtility.updateCompanyListFile(fileNameObj);

				var _require = require('electron'),
				    ipcRenderer = _require.ipcRenderer;

				if (fs.existsSync(pathVar)) {
					var op = fs.writeFileSync(appPath + '/BusinessNames/currentDB.txt', pathVar);

					ipcRenderer.send('changeURL', 'Index1.html');
				} else if (fs.existsSync(__dirname + pathVar)) {
					var op = fs.writeFileSync(appPath + '/BusinessNames/currentDB.txt', __dirname + pathVar);
					ipcRenderer.send('changeURL', 'Index1.html');
				} else {
					$(this).closest('.hasmenu').remove();
					alert('Sorry the file name does not exist. Please select another file');
				}
				// alert("done");
			});
		} else {
			alert('Error while Joining the Company.');
		}
	}

	$('#refreshBut2').click(function () {
		$('#listOfJoinCompany ul').empty();
		joinCompany('listOfJoinCompany ul');
	});

	function searchCompaniesJoin() {
		var input, filter, ul, li, i;
		input = document.getElementById('searchCompanyInput3');
		filter = input.value.toUpperCase();
		ul = $('ul'); // [0];
		$('#ulJoinCompany li').remove();

		for (var item = 1; item < ul.length; item++) {
			ul1 = ul[item];
			li = ul1.getElementsByTagName('li');
			for (i = 0; i < li.length; i++) {
				if (li[i].innerHTML.toUpperCase().indexOf(filter) < 0) {
					$(li[i]).css('display', 'none');
				} else {
					$(li[i]).css('display', 'block');
				}
			}
		}

		assignContextMenu();
	}

	try {
		$('#' + ulJn).on('click', '.joinButton', openThisDBFunctionJoin);

		$('#searchCompanyInput3').on('keyup', searchCompaniesJoin);

		// $('#ulCompany').on('click', '.joinButton', openThisDBFunctionJoin);
	} catch (e) {}
}

// joinCompany();