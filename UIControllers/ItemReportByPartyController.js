var startDate;
var endDate;
var DateFormat = require('./../Constants/DateFormat.js');
$('#startDate, #endDate').datepicker({ dateFormat: DateFormat.format });
var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();
var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var NameCache = require('./../Cache/NameCache.js');
var nameCache = new NameCache();
var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');
var printForSharePDF = false;
var listOfTransactions = [];

var populateTable = function populateTable() {
	nameCache = new NameCache();
	var DataLoader = require('./../DBManager/DataLoader.js');
	var dataLoader = new DataLoader();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var nameString = $('#nameForReport').val();
	var nameId = 0;
	//
	if (nameString) {
		var nameModel = nameCache.findNameModelByName(nameString);
		if (nameModel) {
			nameId = nameModel.getNameId();
		} else {
			nameId = 0;
		}
	}

	startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
	endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');
	var firmId = Number($('#firmFilterOptions').val());
	CommonUtility.showLoader(function () {
		listOfTransactions = dataLoader.getItemReportByPartyList(nameId, startDate, endDate, firmId);
		displayTransactionList(listOfTransactions);
	});
};

var date = MyDate.getDate('d/m/y');
$('#startDate').val(MyDate.getFirstDateOfCurrentMonthString());
$('#endDate').val(date);

if (settingCache.getMultipleFirmEnabled()) {
	var optionTemp = document.createElement('option');
	var FirmCache = require('./../Cache/FirmCache.js');
	var firmCache = new FirmCache();
	var firmList = firmCache.getFirmList();
	for (i in firmList) {
		var option = optionTemp.cloneNode();
		option.text = firmList[i].getFirmName();
		option.value = firmList[i].getFirmId();
		$('#firmFilterOptions').append(option);
	}
	$('#firmChooser').show();
} else {
	$('#firmChooser').hide();
}

$('#firmFilterOptions').on('selectmenuchange', function (event) {
	populateTable();
});

$('#startDate').change(function () {
	populateTable();
});

$('#endDate').change(function () {
	populateTable();
});

$(function () {
	$('#nameForReport').autocomplete($.extend({}, UIHelper.getAutocompleteDefaultOptions(nameCache.getListOfNames()), {
		select: function select(event, ui) {
			$('#nameForReport').val(ui.item.value);
			$('#nameForReport').blur();
		}
	}));
});

var optionTemp = document.createElement('option');
var transactionListArray = ['Sale', 'Purchase', 'Payment-In', 'Payment-Out', 'Credit Note', 'Debit Note', 'Sale Order', 'Purchase Order', 'Estimate', 'Delivery Challan'];
var transactionListId = [TxnTypeConstant.TXN_TYPE_SALE, TxnTypeConstant.TXN_TYPE_PURCHASE, TxnTypeConstant.TXN_TYPE_CASHIN, TxnTypeConstant.TXN_TYPE_CASHOUT, TxnTypeConstant.TXN_TYPE_SALE_RETURN, TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN, TxnTypeConstant.TXN_TYPE_SALE_ORDER, TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER, TxnTypeConstant.TXN_TYPE_ESTIMATE, TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN];
// var len = transactionsList.length;
for (i in transactionListArray) {
	var option = optionTemp.cloneNode();
	option.text = transactionListArray[i];
	option.value = transactionListId[i];
	$('#transactionFilterOptions').append(option);
}

$('#nameForReport').on('change', function (event) {
	populateTable();
});

$('#transactionFilterOptions').on('selectmenuchange', function () {
	populateTable();
});

var itemReportByPartyClusterize;

var displayTransactionList = function displayTransactionList(listOfTransactions) {
	var len = listOfTransactions.length;
	var rowData = [];
	var dynamicRow = '';

	dynamicRow += "<thead><tr><th width='32%'>Item Name</th><th width='17%' class='tableCellTextAlignRight'>Sale Quantity</th><th width='17%' class='tableCellTextAlignRight'>Sale Amount</th><th width='17%' class='tableCellTextAlignRight'>Purchase Quantity</th><th width='17%' class='tableCellTextAlignRight'>Purchase Amount</th></tr></thead>";

	var totalSaleQuantity = 0;
	var totalPurchaseQuantity = 0;
	var totalSaleFreeQuantity = 0;
	var totalPurchaseFreeQuantity = 0;
	var totalSaleAmount = 0;
	var totalPurchaseAmount = 0;
	for (var _i = 0; _i < len; _i++) {
		var row = '';
		var txn = listOfTransactions[_i];
		var itemName = txn['itemName'];
		var saleQuantity = txn['saleQuantity'];
		var purchaseQuantity = txn['purchaseQuantity'];
		var saleFreeQuantity = txn['saleFreeQuantity'];
		var purchaseFreeQuantity = txn['purchaseFreeQuantity'];
		var saleAmount = txn['saleAmount'];
		var purchaseAmount = txn['purchaseAmount'];
		row += "<tr class='currentRow'><td width='32%'>" + itemName + "</td><td width='17%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(saleQuantity) + MyDouble.quantityDoubleToStringWithSignExplicitly(saleFreeQuantity, true) + "</td><td width='17%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(saleAmount) + "</td><td width='17%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(purchaseQuantity) + MyDouble.quantityDoubleToStringWithSignExplicitly(purchaseFreeQuantity, true) + "</td><td width='17%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(purchaseAmount) + '</td></tr>';
		rowData.push(row);
		totalSaleQuantity += saleQuantity;
		totalPurchaseQuantity += purchaseQuantity;
		totalSaleFreeQuantity += saleFreeQuantity;
		totalPurchaseFreeQuantity += purchaseFreeQuantity;
		totalSaleAmount += saleAmount;
		totalPurchaseAmount += purchaseAmount;
	}
	var data = rowData;

	var totalRowHTML = "<tr><td width='32%'>Total</td><td width='17%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(totalSaleQuantity) + MyDouble.quantityDoubleToStringWithSignExplicitly(totalSaleFreeQuantity, true) + "</td><td width='17%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalSaleAmount) + "</td><td width='17%' class='tableCellTextAlignRight'>" + MyDouble.getQuantityWithDecimalWithoutColor(totalPurchaseQuantity) + MyDouble.quantityDoubleToStringWithSignExplicitly(totalPurchaseFreeQuantity, true) + "</td><td width='17%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalPurchaseAmount) + '</td></tr>';
	if ($('#itemReportByPartyContainer').length === 0) {
		return;
	}
	if (itemReportByPartyClusterize && itemReportByPartyClusterize.destroy) {
		itemReportByPartyClusterize.clear();
		itemReportByPartyClusterize.destroy();
	}

	itemReportByPartyClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollArea',
		contentId: 'contentArea',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});

	$('#tableHead').html(dynamicRow);
	$('#totalTable').html(totalRowHTML);
};

var openTransaction = function openTransaction(txnId, txnType) {
	var ViewTransaction = require('./../BizLogic/ViewTransaction.js');
	var viewTransaction = new ViewTransaction();
	viewTransaction.viewTransactionFile(txnId + ':' + TxnTypeConstant.getTxnType(txnType));
};

if (settingCache.getItemEnabled()) {
	$('#itemDetailPrintDiv').show();
} else {
	$('#itemDetailPrintDiv').hide();
}

function openPrintOptions(sharePDF) {
	printForSharePDF = sharePDF;
	printReport();
}

function closePrintOptions() {
	$('#customPrintDialog').addClass('hide');
	$('#customPrintDialog').dialog('close').dialog('destroy');
}

function printReport() {
	if (printForSharePDF) {
		pdfHandler.sharePdf(getHTMLTextForReport(), 'Report', false);
	} else {
		pdfHandler.printPdf(getHTMLTextForReport(), 'Report', false);
	}
	// closePrintOptions();
}

var getHTMLTextForReport = function getHTMLTextForReport() {
	var ItemReportByPartyHTMLGenerator = require('./../ReportHTMLGenerator/ItemReportByPartyHTMLGenerator.js');
	var itemReportByPartyHTMLGenerator = new ItemReportByPartyHTMLGenerator();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var firmId = Number($('#firmFilterOptions').val());
	var nameString = $('#nameForReport').val();
	if (!nameString) {
		nameString = 'All parties';
	}
	var htmlText = itemReportByPartyHTMLGenerator.getHTMLText(listOfTransactions, startDateString, endDateString, firmId, nameString);
	return htmlText;
};

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	// $('#frameDiv').load('');
	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable();
};

/// ///////////////////////////PDF/////////////////////////////////////
var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.ITEM_REPORT_BY_PARTY, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

if (settingCache.getItemEnabled()) {
	$('#itemDetailsExcel').show();
} else {
	$('#itemDetailsExcel').hide();
}

function openExcelOptions() {
	$('#ExcelDialog').removeClass('hide');
	$('#ExcelDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closeExcelOptions() {
	$('#ExcelDialog').addClass('hide');
	$('#ExcelDialog').dialog('close').dialog('destroy');
}

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.ITEM_REPORT_BY_PARTY, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
	excelHelper.saveExcel(fileName);
};

var prepareObjectForExcel = function prepareObjectForExcel(listOfTransactions) {
	var len = listOfTransactions.length;
	var rowObj = [];
	var tableHeadArray = [];
	var totalArray = [];
	tableHeadArray.push('Item Name');
	tableHeadArray.push('Sale Quantity');
	tableHeadArray.push('Sale Amount');
	tableHeadArray.push('Purchase Quantity');
	tableHeadArray.push('Purchase Amount');
	rowObj.push(tableHeadArray);
	rowObj.push([]);

	var totalSaleQuantity = 0;
	var totalPurchaseQuantity = 0;
	var totalSaleFreeQuantity = 0;
	var totalPurchaseFreeQuantity = 0;
	var totalSaleAmount = 0;
	var totalPurchaseAmount = 0;
	for (var j = 0; j < len; j++) {
		var tempArray = [];
		var txn = listOfTransactions[j];
		var itemName = txn['itemName'];
		var saleQuantity = txn['saleQuantity'];
		var purchaseQuantity = txn['purchaseQuantity'];
		var saleFreeQuantity = txn['saleFreeQuantity'];
		var purchaseFreeQuantity = txn['purchaseFreeQuantity'];
		var saleAmount = txn['saleAmount'];
		var purchaseAmount = txn['purchaseAmount'];
		tempArray.push(itemName);
		tempArray.push(MyDouble.getQuantityWithDecimalWithoutColor(saleQuantity));
		tempArray.push(MyDouble.getAmountWithDecimal(saleAmount));
		tempArray.push(MyDouble.getQuantityWithDecimalWithoutColor(purchaseQuantity));
		tempArray.push(MyDouble.getAmountWithDecimal(purchaseAmount));
		totalSaleQuantity += saleQuantity;
		totalPurchaseQuantity += purchaseQuantity;
		totalSaleFreeQuantity += saleFreeQuantity;
		totalPurchaseFreeQuantity += purchaseFreeQuantity;
		totalSaleAmount += saleAmount;
		totalPurchaseAmount += purchaseAmount;
		rowObj.push(tempArray);
	}
	rowObj.push([]);
	totalArray.push('Total');
	totalArray.push(MyDouble.getQuantityWithDecimalWithoutColor(totalSaleQuantity) + MyDouble.quantityDoubleToStringWithSignExplicitly(totalSaleFreeQuantity, true));
	totalArray.push(MyDouble.getAmountWithDecimal(totalSaleAmount));
	totalArray.push(MyDouble.getQuantityWithDecimalWithoutColor(totalPurchaseQuantity) + MyDouble.quantityDoubleToStringWithSignExplicitly(totalPurchaseFreeQuantity, true));
	totalArray.push(MyDouble.getAmountWithDecimal(totalPurchaseAmount));
	rowObj.push(totalArray);
	return rowObj;
};

function printExcel() {
	var XLSX = require('xlsx');
	var dataArray = prepareObjectForExcel(listOfTransactions);
	var worksheet = XLSX.utils.aoa_to_sheet(dataArray);

	var workbook = {
		'SheetNames': [],
		'Sheets': {}
	};

	var wsName = 'Item Report By Party';
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet;
	var wscolWidth = [{ wch: 40 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 15 }, { wch: 15 }];
	worksheet['!cols'] = wscolWidth;
	console.log(workbook);
	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

populateTable();