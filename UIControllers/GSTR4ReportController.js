var startDate;
var endDate;
var DateFormat = require('./../Constants/DateFormat.js');
var MyString = require('./../Utilities/MyString.js');
var NameCache = require('./../Cache/NameCache.js');
var nameCache = new NameCache();
var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();
var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var TaxCodeConstants = require('./../Constants/TaxCodeConstants.js');
var GSTRReportHelper = require('./../UIControllers/GSTRReportHelper.js');
var CompositeSchemeTaxRate = require('./../Constants/CompositeSchemeTaxRate.js');
var CompositeUserType = require('./../Constants/CompositeUserType.js');
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');
var FirmCache = require('./../Cache/FirmCache.js');
var firmCache = new FirmCache();

var listOfTransactions = [];
var workbook;
var turnOverValueArr = [];
var turnOverValue = 0;
var turnOverValueBeforeRateChange = 0;

var GSTR_4_totals = {
	IGSTtotal4Ab2b: 0,
	CGSTtotal4Ab2b: 0,
	SGSTtotal4Ab2b: 0,
	CESStotal4Ab2b: 0,
	TaxableValuetotal4Ab2b: 0,
	IGSTtotal4Bb2b: 0,
	CGSTtotal4Bb2b: 0,
	SGSTtotal4Bb2b: 0,
	CESStotal4Bb2b: 0,
	TaxableValuetotal4Bb2b: 0,
	IGSTtotal4Cb2bur: 0,
	CGSTtotal4Cb2bur: 0,
	SGSTtotal4Cb2bur: 0,
	CESStotal4Cb2bur: 0,
	TaxableValuetotal4Cb2bur: 0,
	IGSTtotal5Bcdnr: 0,
	CGSTtotal5Bcdnr: 0,
	SGSTtotal5Bcdnr: 0,
	CESStotal5Bcdnr: 0,
	TaxableValuetotal5Bcdnr: 0,
	IGSTtotal5Bcdnur: 0,
	CGSTtotal5Bcdnur: 0,
	SGSTtotal5Bcdnur: 0,
	CESStotal5Bcdnur: 0,
	TaxableValuetotal5Bcdnur: 0,
	RateOfTax6txos: 0,
	CGSTtotal6txos: 0,
	SGSTtotal6txos: 0,
	Turnover6txos: 0,
	Turnover6txosFormanufacturer: 0,
	CGSTManufacturer: 0,
	SGSTManufacturer: 0
};

$('.monthYearPicker').datepicker({
	changeMonth: true,
	changeYear: true,
	showButtonPanel: true,
	dateFormat: 'mm/yy'
}).focus(function () {
	var thisCalendar = $(this);
	$('.ui-datepicker-calendar').detach();
	$('.ui-datepicker-close').click(function () {
		var month = $('#ui-datepicker-div .ui-datepicker-month :selected').val();
		var year = $('#ui-datepicker-div .ui-datepicker-year :selected').val();
		thisCalendar.datepicker('setDate', new Date(year, month));
		populateTable();
	});
});

var populateTable = function populateTable() {
	nameCache = new NameCache();
	settingCache = new SettingCache();

	var startDateString = $('#startDate').val();
	startDateString = '01/' + startDateString;
	var endDateString = $('#endDate').val();
	endDateString = '01/' + endDateString;
	startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
	endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');
	endDate = new Date(endDate.getFullYear(), endDate.getMonth() + 1, 0);
	var considerNonTaxAsExemptedCheck = $('#considerNonTaxAsExempted').prop('checked');
	var firmId = Number($('#firmFilterOptions').val());
	var GSTR4ReportHelper = require('./../UIControllers/GSTR4ReportHelper.js');
	var gSTR4ReportHelper = new GSTR4ReportHelper();

	CommonUtility.showLoader(function () {
		GSTR_4_totals.IGSTtotal4Ab2b = 0;
		GSTR_4_totals.CGSTtotal4Ab2b = 0;
		GSTR_4_totals.SGSTtotal4Ab2b = 0;
		GSTR_4_totals.CESStotal4Ab2b = 0;
		GSTR_4_totals.TaxableValuetotal4Ab2b = 0;
		GSTR_4_totals.IGSTtotal4Bb2b = 0;
		GSTR_4_totals.CGSTtotal4Bb2b = 0;
		GSTR_4_totals.SGSTtotal4Bb2b = 0;
		GSTR_4_totals.CESStotal4Bb2b = 0;
		GSTR_4_totals.TaxableValuetotal4Bb2b = 0;
		GSTR_4_totals.IGSTtotal4Cb2bur = 0;
		GSTR_4_totals.CGSTtotal4Cb2bur = 0;
		GSTR_4_totals.SGSTtotal4Cb2bur = 0;
		GSTR_4_totals.CESStotal4Cb2bur = 0;
		GSTR_4_totals.TaxableValuetotal4Cb2bur = 0;
		GSTR_4_totals.IGSTtotal5Bcdnr = 0;
		GSTR_4_totals.CGSTtotal5Bcdnr = 0;
		GSTR_4_totals.SGSTtotal5Bcdnr = 0;
		GSTR_4_totals.CESStotal5Bcdnr = 0;
		GSTR_4_totals.TaxableValuetotal5Bcdnr = 0;
		GSTR_4_totals.IGSTtotal5Bcdnur = 0;
		GSTR_4_totals.CGSTtotal5Bcdnur = 0;
		GSTR_4_totals.SGSTtotal5Bcdnur = 0;
		GSTR_4_totals.CESStotal5Bcdnur = 0;
		GSTR_4_totals.TaxableValuetotal5Bcdnur = 0;
		GSTR_4_totals.RateOfTax6txos = 0;
		GSTR_4_totals.CGSTtotal6txos = 0;
		GSTR_4_totals.SGSTtotal6txos = 0;
		GSTR_4_totals.Turnover6txos = 0;
		GSTR_4_totals.Turnover6txosFormanufacturer = 0;
		GSTR_4_totals.CGSTManufacturer = 0;
		GSTR_4_totals.SGSTManufacturer = 0;

		listOfTransactions = gSTR4ReportHelper.getTxnListBasedOnDate(startDate, endDate, firmId, considerNonTaxAsExemptedCheck);
		turnOverValueArr = gSTR4ReportHelper.getTurnOverForGSTR4(startDate, endDate, firmId);
		turnOverValue = turnOverValueArr[0];
		turnOverValueBeforeRateChange = turnOverValueArr[1];
		prepareExcel();
		displayTransactionList();
	});
};

var date = MyDate.getDate('m/y');
$('#startDate').val(date);
$('#endDate').val(date);

if (settingCache.getMultipleFirmEnabled()) {
	var optionTemp = document.createElement('option');
	var firmList = firmCache.getFirmList();
	for (i in firmList) {
		var option = optionTemp.cloneNode();
		option.text = firmList[i].getFirmName();
		option.value = firmList[i].getFirmId();
		$('#firmFilterOptions').append(option);
	}
	$('#firmChooser').show();
} else {
	$('#firmChooser').hide();
}

$('#firmFilterOptions').on('selectmenuchange', function (event) {
	populateTable();
});

$('#startDate').change(function () {
	populateTable();
});

$('#considerNonTaxAsExempted').change(function () {
	populateTable();
});

$('#endDate').change(function () {
	populateTable();
});

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	// $('#frameDiv').load('');
	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}

	populateTable();
};

var displayTransactionList = function displayTransactionList() {
	var row = '';
	row += "<thead><tr style='background-color: lightgrey;'><th width='35%'>Description</th><th width='13%' class='tableCellTextAlignRight'>IGST Amount</th><th width='13%' class='tableCellTextAlignRight'>CGST Amount</th><th width='13%' class='tableCellTextAlignRight'>SGST Amount</th><th width='13%' class='tableCellTextAlignRight'>Cess Amount</th><th width='13%' class='tableCellTextAlignRight'>Total taxable value</th></tr></thead>";
	row += '<tbody>';
	row += "<tr><td style='font-size: 10px;font-weight: bold;'>4A(B2B) Inward supplies received from registered supplier(excluding reverse charge)</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.IGSTtotal4Ab2b) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.CGSTtotal4Ab2b) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.SGSTtotal4Ab2b) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.CESStotal4Ab2b) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.TaxableValuetotal4Ab2b) + '</td></tr>';
	row += "<tr><td style='font-size: 10px;font-weight: bold;'>4B(B2B) Inward supplies received from registered supplier(attracting reverse charge)</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.IGSTtotal4Bb2b) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.CGSTtotal4Bb2b) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.SGSTtotal4Bb2b) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.CESStotal4Bb2b) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.TaxableValuetotal4Bb2b) + '</td></tr>';
	row += "<tr><td style='font-size: 10px;font-weight: bold;'>4C(B2BUR) Inward supplies received from an unregistered supplier</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.IGSTtotal4Cb2bur) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.CGSTtotal4Cb2bur) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.SGSTtotal4Cb2bur) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.CESStotal4Cb2bur) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.TaxableValuetotal4Cb2bur) + '</td></tr>';
	row += "<tr><td style='font-size: 10px;font-weight: bold;'>5B(CDNR) Debit notes/Credit notes (for registered)</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.IGSTtotal5Bcdnr) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.CGSTtotal5Bcdnr) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.SGSTtotal5Bcdnr) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.CESStotal5Bcdnr) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.TaxableValuetotal5Bcdnr) + '</td></tr>';
	row += "<tr><td style='font-size: 10px;font-weight: bold;'>5B(CDNUR) Debit notes/Credit notes (for unregistered)</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.IGSTtotal5Bcdnur) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.CGSTtotal5Bcdnur) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.SGSTtotal5Bcdnur) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.CESStotal5Bcdnur) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.TaxableValuetotal5Bcdnur) + '</td></tr>';
	row += '</tbody>';

	var row1 = '';
	row1 += "<thead><tr style='background-color: lightgrey;'><th rowspan='2' width='25%'>Rate of tax</th><th rowspan='2' width='25%' class='tableCellTextAlignRight'>Turnover</th><th colspan='2' width='50%' style='text-align: center;'>Composition tax amount</th></tr>";
	row1 += "<tr style='background-color: lightgrey;'></th><th class='tableCellTextAlignRight' width='25%'>Central Tax</th><th class='tableCellTextAlignRight' width='25%'>State/UT Tax</th></tr></thead>";
	row1 += '<tbody>';
	row1 += "<tr><td class='tableCellTextAlignLeft'>" + GSTR_4_totals.RateOfTax6txos + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.Turnover6txos) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.CGSTtotal6txos) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.SGSTtotal6txos) + '</td></tr>';
	if (turnOverValueBeforeRateChange) {
		row1 += "<tr><td class='tableCellTextAlignLeft'>2</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.Turnover6txosFormanufacturer) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.CGSTManufacturer) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.SGSTManufacturer) + '</td></tr>';
	}
	row1 += '</tbody>';

	$('#table4').html(row);
	$('#table4ratedescription').html('6(TXOS) Tax on outward supplies');
	$('#table4rate').html(row1);
};

var getHTMLTextForReport = function getHTMLTextForReport() {
	var GSTR1ReportHTMLGenerator = require('./../ReportHTMLGenerator/GSTR1ReportHTMLGenerator.js');
	var gSTR1ReportHTMLGenerator = new GSTR1ReportHTMLGenerator();
	var startDateString = $('#startDate').val();
	var firmId = Number($('#firmFilterOptions').val());
	var htmlText = gSTR1ReportHTMLGenerator.getHTMLText(listOfTransactions, startDateString, firmId, showOTHER, showAdditionalCess);
	return htmlText;
};

$('#partyFilterOptions').on('selectmenuchange', function () {
	displayTransactionList(listOfTransactions);
});

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.GSTR4_REPORT, fromDate: '01/' + $('#startDate').val(), format: 'MM_YY' });
	excelHelper.saveExcel(fileName);
};

var getDateFormatForGSTR1Report = function getDateFormatForGSTR1Report(date) {
	var monthArray = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	var monthIndex = Number(MyDate.getDate('m', date));
	var dateField = MyDate.getDate('d', date);
	var monthField = monthArray[monthIndex - 1];
	var yearField = MyDate.getDate('y', date);
	var formatedDate = dateField + '-' + monthField + '-' + yearField;
	return formatedDate;
};

var getObjectFor4Cb2bur = function getObjectFor4Cb2bur(listOfTransactions) {
	var rowObj = [];
	rowObj.push(['Invoice Number', 'Invoice Date', 'Invoice Value', 'Place Of Supply', 'Supply Type', 'Rate', 'Taxable value', 'Integrated Tax', 'Central Tax', 'State/UT Tax', 'Cess']);
	var len = listOfTransactions.length;
	for (var i = 0; i < len; i++) {
		var txn = listOfTransactions[i];
		var gstinNo = txn.getGstinNo();

		if ((gstinNo == '' || gstinNo == null) && txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_PURCHASE) {
			var gstinNo = txn.getGstinNo();
			var invoiceNo = txn.getInvoiceNo();
			var invoiceDate = txn.getInvoiceDate();
			var invoiceValue = txn.getInvoiceValue();
			var rate = txn.getRate() - txn.getCessRate();
			var taxableValue = txn.getInvoiceTaxableValue();
			var igstAmt = txn.getIGSTAmt();
			var cgstAmt = txn.getCGSTAmt();
			var sgstAmt = txn.getSGSTAmt();
			var cessAmt = txn.getCESSAmt() + txn.getAdditionalCessAmt();
			var POS = txn.getPlaceOfSupply() ? StateCode.getStateCode(txn.getPlaceOfSupply()) + '-' + txn.getPlaceOfSupply() : '';
			var partyState = nameCache.findNameObjectByNameId(txn.getNameId()).getContactState();

			if (partyState != '' && partyState != txn.getPlaceOfSupply()) {
				var supplyType = 'Inter State';
			} else {
				var supplyType = 'Intra State';
			}

			var tempArray = [];
			tempArray.push(invoiceNo);
			tempArray.push(GSTRReportHelper.getDateFormatForGSTR4Report(invoiceDate));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(invoiceValue));
			tempArray.push(POS);
			tempArray.push(supplyType);
			tempArray.push(MyDouble.getPercentageWithDecimal(rate));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(taxableValue));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(igstAmt));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(cgstAmt));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(sgstAmt));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(cessAmt));
			rowObj.push(tempArray);
			GSTR_4_totals.IGSTtotal4Cb2bur += igstAmt;
			GSTR_4_totals.CGSTtotal4Cb2bur += cgstAmt;
			GSTR_4_totals.SGSTtotal4Cb2bur += sgstAmt;
			GSTR_4_totals.CESStotal4Cb2bur += cessAmt;
			GSTR_4_totals.TaxableValuetotal4Cb2bur += taxableValue;
		}
	}

	return rowObj;
};

var getObjectFor4Bb2b = function getObjectFor4Bb2b(listOfTransactions) {
	var rowObj = [];
	rowObj.push(['GSTIN of supplier', 'Invoice No.', 'Invoice Date', 'Invoice Value', 'Place of supply', 'Reverse Charge', 'Invoice Type', 'Rate', 'Taxable value', 'Integrated Tax', 'Central Tax', 'State/UT Tax', 'Cess']);
	var len = listOfTransactions.length;
	for (var i = 0; i < len; i++) {
		var txn = listOfTransactions[i];
		var gstinNo = txn.getGstinNo();

		if (gstinNo != '' && gstinNo != null && txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_PURCHASE) {
			var gstinNo = txn.getGstinNo();
			var invoiceNo = txn.getInvoiceNo();
			var invoiceDate = txn.getInvoiceDate();
			var invoiceValue = txn.getInvoiceValue();
			var rate = txn.getRate() - txn.getCessRate();
			var taxableValue = txn.getInvoiceTaxableValue();
			var igstAmt = txn.getIGSTAmt();
			var cgstAmt = txn.getCGSTAmt();
			var sgstAmt = txn.getSGSTAmt();
			var cessAmt = txn.getCESSAmt() + txn.getAdditionalCessAmt();
			var POS = txn.getPlaceOfSupply() ? StateCode.getStateCode(txn.getPlaceOfSupply()) + '-' + txn.getPlaceOfSupply() : '';
			if (txn.getReverseCharge() == 1) {
				var reverseCharge = 'Yes';
			} else {
				var reverseCharge = 'No';
			}
			var InvoiceType = 'Regular';

			var tempArray = [];
			tempArray.push(gstinNo);
			tempArray.push(invoiceNo);
			tempArray.push(GSTRReportHelper.getDateFormatForGSTR4Report(invoiceDate));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(invoiceValue));
			tempArray.push(POS);
			tempArray.push(reverseCharge);
			tempArray.push(InvoiceType);
			tempArray.push(MyDouble.getPercentageWithDecimal(rate));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(taxableValue));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(igstAmt));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(cgstAmt));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(sgstAmt));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(cessAmt));
			rowObj.push(tempArray);

			if (txn.getReverseCharge() == 1) {
				GSTR_4_totals.IGSTtotal4Bb2b += igstAmt;
				GSTR_4_totals.CGSTtotal4Bb2b += cgstAmt;
				GSTR_4_totals.SGSTtotal4Bb2b += sgstAmt;
				GSTR_4_totals.CESStotal4Bb2b += cessAmt;
				GSTR_4_totals.TaxableValuetotal4Bb2b += taxableValue;
			} else {
				GSTR_4_totals.IGSTtotal4Ab2b += igstAmt;
				GSTR_4_totals.CGSTtotal4Ab2b += cgstAmt;
				GSTR_4_totals.SGSTtotal4Ab2b += sgstAmt;
				GSTR_4_totals.CESStotal4Ab2b += cessAmt;
				GSTR_4_totals.TaxableValuetotal4Ab2b += taxableValue;
			}
		}
	}

	return rowObj;
};

var getObjectFor5Bcdnr = function getObjectFor5Bcdnr(listOfTransactions) {
	var rowObj = [];
	rowObj.push(['GSTIN of supplier', 'Note/ Refund Voucher Number', 'Note/ Refund Voucher date', 'Invoice/ Payment Voucher Number', 'Invoice/ Payment Voucher Date', 'Pre GST', 'Document Type', 'Supply Type', 'Reverse Charge', 'Note/ Refund Voucher value', 'Rate', 'Taxable value', 'Integrated Tax', 'Central Tax', 'State/UT Tax', 'Cess']);
	var len = listOfTransactions.length;
	for (var i = 0; i < len; i++) {
		var txn = listOfTransactions[i];
		var gstinNo = txn.getGstinNo();

		if (gstinNo != '' && gstinNo != null && txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
			var gstinNo = txn.getGstinNo();
			var invoiceNo = txn.getInvoiceNo();
			var invoiceDate = txn.getInvoiceDate();
			var invoiceValue = txn.getInvoiceValue();
			var rate = txn.getRate() - txn.getCessRate();
			var taxableValue = txn.getInvoiceTaxableValue();
			var igstAmt = txn.getIGSTAmt();
			var cgstAmt = txn.getCGSTAmt();
			var sgstAmt = txn.getSGSTAmt();
			var cessAmt = txn.getCESSAmt() + txn.getAdditionalCessAmt();
			var POS = txn.getPlaceOfSupply();
			var mainInvoiceNo = txn.getTxnReturnRefNumber();
			var mainInvoiceDate = txn.getTxnReturnDate();
			var preGST = 'No';
			var documentType = 'Debit Note';
			var partyState = nameCache.findNameObjectByNameId(txn.getNameId()).getContactState();
			if (txn.getReverseCharge() == 1) {
				var reverseCharge = 'Yes';
			} else {
				var reverseCharge = 'No';
			}

			if (partyState != '' && partyState != txn.getPlaceOfSupply()) {
				var supplyType = 'Inter State';
			} else {
				var supplyType = 'Intra State';
			}

			var tempArray = [];
			tempArray.push(gstinNo);
			tempArray.push(invoiceNo);
			tempArray.push(GSTRReportHelper.getDateFormatForGSTR4Report(invoiceDate));
			tempArray.push(mainInvoiceNo);
			tempArray.push(mainInvoiceDate ? GSTRReportHelper.getDateFormatForGSTR4Report(mainInvoiceDate) : '');
			tempArray.push(preGST);
			tempArray.push(documentType);
			tempArray.push(supplyType);
			tempArray.push(reverseCharge);
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(invoiceValue));
			tempArray.push(MyDouble.getPercentageWithDecimal(rate));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(taxableValue));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(igstAmt));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(cgstAmt));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(sgstAmt));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(cessAmt));
			rowObj.push(tempArray);
			GSTR_4_totals.IGSTtotal5Bcdnr += igstAmt;
			GSTR_4_totals.CGSTtotal5Bcdnr += cgstAmt;
			GSTR_4_totals.SGSTtotal5Bcdnr += sgstAmt;
			GSTR_4_totals.CESStotal5Bcdnr += cessAmt;
			GSTR_4_totals.TaxableValuetotal5Bcdnr += taxableValue;
		}
	}

	return rowObj;
};

var getObjectFor5Bcdnur = function getObjectFor5Bcdnur(listOfTransactions) {
	var rowObj = [];
	rowObj.push(['Note/ Refund Voucher Number', 'Note/ Refund Voucher date', 'Invoice/ Advance Payment Voucher Number', 'Invoice/ Advance Payment Voucher Date', 'Pre GST', 'Document Type', 'Supply Type', 'Inward Supply Type', 'Note/ Refund voucher value', 'Rate', 'Taxable value', 'Integrated Tax Amount', 'Central Tax Amount', 'State/UT Tax Amount', 'CESS Amount']);
	var len = listOfTransactions.length;
	for (var i = 0; i < len; i++) {
		var txn = listOfTransactions[i];
		var gstinNo = txn.getGstinNo();

		if ((gstinNo == '' || gstinNo == null) && txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
			var gstinNo = txn.getGstinNo();
			var invoiceNo = txn.getInvoiceNo();
			var invoiceDate = txn.getInvoiceDate();

			var invoiceValue = txn.getInvoiceValue();
			var rate = txn.getRate() - txn.getCessRate();
			var taxableValue = txn.getInvoiceTaxableValue();
			var igstAmt = txn.getIGSTAmt();
			var cgstAmt = txn.getCGSTAmt();
			var sgstAmt = txn.getSGSTAmt();
			var cessAmt = txn.getCESSAmt() + txn.getAdditionalCessAmt();
			var POS = txn.getPlaceOfSupply();
			var mainInvoiceNo = txn.getTxnReturnRefNumber();
			var mainInvoiceDate = txn.getTxnReturnDate();
			var preGST = 'No';
			var documentType = 'Debit Note';
			var partyState = nameCache.findNameObjectByNameId(txn.getNameId()).getContactState();

			if (partyState != '' && partyState != txn.getPlaceOfSupply()) {
				var supplyType = 'Inter State';
			} else {
				var supplyType = 'Intra State';
			}
			var inwardSupplyType = 'B2BUR';

			var tempArray = [];
			tempArray.push(invoiceNo);
			tempArray.push(GSTRReportHelper.getDateFormatForGSTR4Report(invoiceDate));
			tempArray.push(mainInvoiceNo);
			tempArray.push(mainInvoiceDate ? GSTRReportHelper.getDateFormatForGSTR4Report(mainInvoiceDate) : '');
			tempArray.push(preGST);
			tempArray.push(documentType);
			tempArray.push(supplyType);
			tempArray.push(inwardSupplyType);
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(invoiceValue));
			tempArray.push(MyDouble.getPercentageWithDecimal(rate));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(taxableValue));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(igstAmt));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(cgstAmt));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(sgstAmt));
			tempArray.push(MyDouble.getAmountDecimalForGSTRReport(cessAmt));
			rowObj.push(tempArray);
			GSTR_4_totals.IGSTtotal5Bcdnur += igstAmt;
			GSTR_4_totals.CGSTtotal5Bcdnur += cgstAmt;
			GSTR_4_totals.SGSTtotal5Bcdnur += sgstAmt;
			GSTR_4_totals.CESStotal5Bcdnur += cessAmt;
			GSTR_4_totals.TaxableValuetotal5Bcdnur += taxableValue;
		}
	}

	return rowObj;
};

var getObjectFor6txos = function getObjectFor6txos(turnOverValue) {
	var rowObj = [];
	rowObj.push(['Rate of Tax', 'Turnover', 'Composition Central Tax Amount', 'Composition State/UT Tax Amount']);
	var compositeUserType = settingCache.getCompositeUserType();
	if (compositeUserType == CompositeUserType.MANUFACTURER) {
		var rateOfTax = CompositeSchemeTaxRate.TAXRATE_MANUFACTURER;
	} else if (compositeUserType == CompositeUserType.TRADER) {
		var rateOfTax = CompositeSchemeTaxRate.TAXRATE_TRADER;
	} else if (compositeUserType == CompositeUserType.RESTAURANT) {
		var rateOfTax = CompositeSchemeTaxRate.TAXRATE_RESTAURANT;
	}

	var compositionCentralTaxAmount = turnOverValue * rateOfTax / 100 / 2;
	var compositionStateUTTaxAmount = turnOverValue * rateOfTax / 100 / 2;
	rowObj.push([rateOfTax, MyDouble.getAmountDecimalForGSTRReport(turnOverValue), MyDouble.getAmountDecimalForGSTRReport(compositionCentralTaxAmount), MyDouble.getAmountDecimalForGSTRReport(compositionStateUTTaxAmount)]);

	GSTR_4_totals.RateOfTax6txos = rateOfTax;
	GSTR_4_totals.CGSTtotal6txos += compositionCentralTaxAmount;
	GSTR_4_totals.SGSTtotal6txos += compositionStateUTTaxAmount;
	GSTR_4_totals.Turnover6txos += turnOverValue;
	if (turnOverValueBeforeRateChange) {
		GSTR_4_totals.Turnover6txosFormanufacturer = turnOverValueBeforeRateChange;
		GSTR_4_totals.CGSTManufacturer = turnOverValueBeforeRateChange * 2 / 100 / 2;
		GSTR_4_totals.SGSTManufacturer = turnOverValueBeforeRateChange * 2 / 100 / 2;
		rowObj.push([2, MyDouble.getAmountDecimalForGSTRReport(turnOverValueBeforeRateChange), MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.CGSTManufacturer), MyDouble.getAmountDecimalForGSTRReport(GSTR_4_totals.SGSTManufacturer)]);
	}

	return rowObj;
};

var getObjectFor4Dimps = function getObjectFor4Dimps(listOfTransactions) {
	var rowObj = [];
	rowObj.push(['Invoice Number', 'Invoice Date', 'Invoice Value', 'Place Of Supply', 'Rate', 'Taxable Value', 'Integrated Tax', 'cess']);
	return rowObj;
};

var getObjectFor8Aat = function getObjectFor8Aat(listOfTransactions) {
	var rowObj = [];
	rowObj.push(['Place Of Supply', 'Supply Type', 'Rate', 'Gross Advance Paid', 'Integrated Tax', 'Central Tax', 'State/UT Tax', 'Cess']);
	return rowObj;
};

var getObjectFor8Aatadj = function getObjectFor8Aatadj(listOfTransactions) {
	var rowObj = [];
	rowObj.push(['Place Of Supply', 'Supply Type', 'Rate', 'Gross Advance Paid', 'Integrated Tax', 'Central Tax', 'State/UT Tax', 'Cess']);
	return rowObj;
};

function prepareExcel() {
	var XLSX = require('xlsx');

	workbook = {
		'SheetNames': [],
		'Sheets': {}
	};

	/// /////////////////////////////////4B(B2B)///////////////////////////////////////

	var wsNameItem = '4A&B(B2B)';
	workbook.SheetNames[0] = wsNameItem;
	var data4Bb2bArray = getObjectFor4Bb2b(listOfTransactions);
	var data4b2bBWorksheet = XLSX.utils.aoa_to_sheet(data4Bb2bArray);
	workbook.Sheets[wsNameItem] = data4b2bBWorksheet;
	var wsItemcolWidth = [{ wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	data4b2bBWorksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////4C(B2BUR)///////////////////////////////////////

	var wsNameItem = '4C(B2BUR)';
	workbook.SheetNames[1] = wsNameItem;
	var data4Cb2burArray = getObjectFor4Cb2bur(listOfTransactions);
	var data4Cb2burWorksheet = XLSX.utils.aoa_to_sheet(data4Cb2burArray);
	workbook.Sheets[wsNameItem] = data4Cb2burWorksheet;
	var wsItemcolWidth = [{ wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	data4Cb2burWorksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////4D(IMPS)///////////////////////////////////////

	var wsNameItem = '4D(IMPS)';
	workbook.SheetNames[2] = wsNameItem;
	var data4DimpsArray = getObjectFor4Dimps(listOfTransactions);
	var data4DimpsWorksheet = XLSX.utils.aoa_to_sheet(data4DimpsArray);
	workbook.Sheets[wsNameItem] = data4DimpsWorksheet;
	var wsItemcolWidth = [{ wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	data4DimpsWorksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////5B(CDNR)///////////////////////////////////////

	var wsNameItem = '5B(CDNR)';
	workbook.SheetNames[3] = wsNameItem;
	var data5BcdnrArray = getObjectFor5Bcdnr(listOfTransactions);
	var data5BcdnrWorksheet = XLSX.utils.aoa_to_sheet(data5BcdnrArray);
	workbook.Sheets[wsNameItem] = data5BcdnrWorksheet;
	var wsItemcolWidth = [{ wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	data5BcdnrWorksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////5B(CDNUR)///////////////////////////////////////

	var wsNameItem = '5B(CDNUR)';
	workbook.SheetNames[4] = wsNameItem;
	var data5BcdnurArray = getObjectFor5Bcdnur(listOfTransactions);
	var data5BcdnurWorksheet = XLSX.utils.aoa_to_sheet(data5BcdnurArray);
	workbook.Sheets[wsNameItem] = data5BcdnurWorksheet;
	var wsItemcolWidth = [{ wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	data5BcdnurWorksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////6(TXOS)///////////////////////////////////////

	var wsNameItem = '6(TXOS)';
	workbook.SheetNames[5] = wsNameItem;
	var data6txosArray = getObjectFor6txos(turnOverValue);
	var data6txosWorksheet = XLSX.utils.aoa_to_sheet(data6txosArray);
	workbook.Sheets[wsNameItem] = data6txosWorksheet;
	var wsItemcolWidth = [{ wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	data6txosWorksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////8A(AT)///////////////////////////////////////

	var wsNameItem = '8A(AT)';
	workbook.SheetNames[6] = wsNameItem;
	var data8AatArray = getObjectFor8Aat(listOfTransactions);
	var data8AatWorksheet = XLSX.utils.aoa_to_sheet(data8AatArray);
	workbook.Sheets[wsNameItem] = data8AatWorksheet;
	var wsItemcolWidth = [{ wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	data8AatWorksheet['!cols'] = wsItemcolWidth;

	/// /////////////////////////////////8A(ATADJ)///////////////////////////////////////

	var wsNameItem = '8A(ATADJ)';
	workbook.SheetNames[7] = wsNameItem;
	var data8AatadjArray = getObjectFor8Aatadj(listOfTransactions);
	var data8AatadjWorksheet = XLSX.utils.aoa_to_sheet(data8AatadjArray);
	workbook.Sheets[wsNameItem] = data8AatadjWorksheet;
	var wsItemcolWidth = [{ wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	data8AatadjWorksheet['!cols'] = wsItemcolWidth;
}

function printExcel() {
	var XLSX = require('xlsx');
	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

populateTable();