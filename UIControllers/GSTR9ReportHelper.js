var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TaxCodeCache = require('./../Cache/TaxCodeCache.js');
var ItemCache = require('./../Cache/ItemCache.js');
var itemCache = new ItemCache();
var taxCodeCache = new TaxCodeCache();
var TaxCodeConstants = require('./../Constants/TaxCodeConstants.js');
var GSTRReportHelper = require('./../UIControllers/GSTRReportHelper.js');
var DataLoader = require('./../DBManager/DataLoader.js');

var GSTR9ReportHelper = function GSTR9ReportHelper() {
	this.transactionList = [];
	this.considerNonTaxAsExemptedCheck = false;
	this.sale_purchase_without_reverse_charge_unregistered_taxable_value = 0; // for 4(A) taxable value
	this.sale_purchase_without_reverse_charge_unregistered_cgst = 0; // for 4(A) cgst
	this.sale_purchase_without_reverse_charge_unregistered_sgst = 0; // for 4(A) sgst
	this.sale_purchase_without_reverse_charge_unregistered_igst = 0; // for 4(A) igst
	this.sale_purchase_without_reverse_charge_unregistered_cess = 0; // for 4(A) cess
	this.sale_purchase_without_reverse_charge_registered_taxable_value = 0; // for 4(B) taxable value
	this.sale_purchase_without_reverse_charge_registered_cgst = 0; // for 4(B) cgst
	this.sale_purchase_without_reverse_charge_registered_sgst = 0; // for 4(B) sgst
	this.sale_purchase_without_reverse_charge_registered_igst = 0; // for 4(B) igst
	this.sale_purchase_without_reverse_charge_registered_cess = 0; // for 4(B) cess
	this.purchase_reverse_charge_taxable_value = 0; // for 4(G) taxable value
	this.purchase_reverse_charge_cgst = 0; // for 4(G) cgst
	this.purchase_reverse_charge_sgst = 0; // for 4(G) sgst
	this.purchase_reverse_charge_igst = 0; // for 4(G) igst
	this.purchase_reverse_charge_cess = 0; // for 4(G) cess
	this.sale_return_registered_taxable_value = 0; // for 4(I) taxable value
	this.sale_return_registered_cgst = 0; // for 4(I) cgst
	this.sale_return_registered_sgst = 0; // for 4(I) sgst
	this.sale_return_registered_igst = 0; // for 4(I) igst
	this.sale_return_registered_cess = 0; // for 4(I) cess
	this.sale_exempted_taxable_value = 0; // for 5(D)
	this.sale_nil_rated_taxable_value = 0; // for 5(E)
	this.sale_return_exempted_nil_rated_taxable_value = 0; // for 5(H)

	this.purchase_purchase_return_without_reverse_charge_itc_inputs_cgst = 0; // for 6(B)
	this.purchase_purchase_return_without_reverse_charge_itc_inputs_sgst = 0; // for 6(B)
	this.purchase_purchase_return_without_reverse_charge_itc_inputs_igst = 0; // for 6(B)
	this.purchase_purchase_return_without_reverse_charge_itc_inputs_cess = 0; // for 6(B)

	this.purchase_purchase_return_without_reverse_charge_itc_capital_goods_cgst = 0; // for 6(B)
	this.purchase_purchase_return_without_reverse_charge_itc_capital_goods_sgst = 0; // for 6(B)
	this.purchase_purchase_return_without_reverse_charge_itc_capital_goods_igst = 0; // for 6(B)
	this.purchase_purchase_return_without_reverse_charge_itc_capital_goods_cess = 0; // for 6(B)

	this.purchase_purchase_return_without_reverse_charge_itc_input_services_cgst = 0; // for 6(B)
	this.purchase_purchase_return_without_reverse_charge_itc_input_services_sgst = 0; // for 6(B)
	this.purchase_purchase_return_without_reverse_charge_itc_input_services_igst = 0; // for 6(B)
	this.purchase_purchase_return_without_reverse_charge_itc_input_services_cess = 0; // for 6(B)

	this.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_cgst = 0; // for 6(C)
	this.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_sgst = 0; // for 6(C)
	this.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_igst = 0; // for 6(C)
	this.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_cess = 0; // for 6(C)

	this.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_cgst = 0; // for 6(C)
	this.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_sgst = 0; // for 6(C)
	this.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_igst = 0; // for 6(C)
	this.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_cess = 0; // for 6(C)

	this.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_cgst = 0; // for 6(C)
	this.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_sgst = 0; // for 6(C)
	this.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_igst = 0; // for 6(C)
	this.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_cess = 0; // for 6(C)

	this.purchase_purchase_return_registered_reverse_charge_itc_inputs_cgst = 0; // for 6(D)
	this.purchase_purchase_return_registered_reverse_charge_itc_inputs_sgst = 0; // for 6(D)
	this.purchase_purchase_return_registered_reverse_charge_itc_inputs_igst = 0; // for 6(D)
	this.purchase_purchase_return_registered_reverse_charge_itc_inputs_cess = 0; // for 6(D)

	this.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_cgst = 0; // for 6(D)
	this.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_sgst = 0; // for 6(D)
	this.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_igst = 0; // for 6(D)
	this.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_cess = 0; // for 6(D)

	this.purchase_purchase_return_registered_reverse_charge_itc_input_services_cgst = 0; // for 6(D)
	this.purchase_purchase_return_registered_reverse_charge_itc_input_services_sgst = 0; // for 6(D)
	this.purchase_purchase_return_registered_reverse_charge_itc_input_services_igst = 0; // for 6(D)
	this.purchase_purchase_return_registered_reverse_charge_itc_input_services_cess = 0; // for 6(D)

	this.hsn_summary_sale = []; // for 17
	this.hsn_summary_purchase = []; // for 18

	this.initializeValues = function () {
		this.transactionList = [];
		this.considerNonTaxAsExemptedCheck = false;
		this.sale_purchase_without_reverse_charge_unregistered_taxable_value = 0;
		this.sale_purchase_without_reverse_charge_unregistered_cgst = 0;
		this.sale_purchase_without_reverse_charge_unregistered_sgst = 0;
		this.sale_purchase_without_reverse_charge_unregistered_igst = 0;
		this.sale_purchase_without_reverse_charge_unregistered_cess = 0;
		this.sale_purchase_without_reverse_charge_registered_taxable_value = 0;
		this.sale_purchase_without_reverse_charge_registered_cgst = 0;
		this.sale_purchase_without_reverse_charge_registered_sgst = 0;
		this.sale_purchase_without_reverse_charge_registered_igst = 0;
		this.sale_purchase_without_reverse_charge_registered_cess = 0;
		this.purchase_reverse_charge_taxable_value = 0;
		this.purchase_reverse_charge_cgst = 0;
		this.purchase_reverse_charge_sgst = 0;
		this.purchase_reverse_charge_igst = 0;
		this.purchase_reverse_charge_cess = 0;
		this.sale_return_registered_taxable_value = 0;
		this.sale_return_registered_cgst = 0;
		this.sale_return_registered_sgst = 0;
		this.sale_return_registered_igst = 0;
		this.sale_return_registered_cess = 0;
		this.sale_exempted_taxable_value = 0;
		this.sale_nil_rated_taxable_value = 0;
		this.sale_return_exempted_nil_rated_taxable_value = 0;

		this.purchase_purchase_return_without_reverse_charge_itc_inputs_cgst = 0;
		this.purchase_purchase_return_without_reverse_charge_itc_inputs_sgst = 0;
		this.purchase_purchase_return_without_reverse_charge_itc_inputs_igst = 0;
		this.purchase_purchase_return_without_reverse_charge_itc_inputs_cess = 0;

		this.purchase_purchase_return_without_reverse_charge_itc_capital_goods_cgst = 0;
		this.purchase_purchase_return_without_reverse_charge_itc_capital_goods_sgst = 0;
		this.purchase_purchase_return_without_reverse_charge_itc_capital_goods_igst = 0;
		this.purchase_purchase_return_without_reverse_charge_itc_capital_goods_cess = 0;

		this.purchase_purchase_return_without_reverse_charge_itc_input_services_cgst = 0;
		this.purchase_purchase_return_without_reverse_charge_itc_input_services_sgst = 0;
		this.purchase_purchase_return_without_reverse_charge_itc_input_services_igst = 0;
		this.purchase_purchase_return_without_reverse_charge_itc_input_services_cess = 0;

		this.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_cgst = 0;
		this.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_sgst = 0;
		this.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_igst = 0;
		this.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_cess = 0;

		this.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_cgst = 0;
		this.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_sgst = 0;
		this.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_igst = 0;
		this.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_cess = 0;

		this.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_cgst = 0;
		this.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_sgst = 0;
		this.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_igst = 0;
		this.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_cess = 0;

		this.purchase_purchase_return_registered_reverse_charge_itc_inputs_cgst = 0;
		this.purchase_purchase_return_registered_reverse_charge_itc_inputs_sgst = 0;
		this.purchase_purchase_return_registered_reverse_charge_itc_inputs_igst = 0;
		this.purchase_purchase_return_registered_reverse_charge_itc_inputs_cess = 0;

		this.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_cgst = 0;
		this.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_sgst = 0;
		this.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_igst = 0;
		this.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_cess = 0;

		this.purchase_purchase_return_registered_reverse_charge_itc_input_services_cgst = 0;
		this.purchase_purchase_return_registered_reverse_charge_itc_input_services_sgst = 0;
		this.purchase_purchase_return_registered_reverse_charge_itc_input_services_igst = 0;
		this.purchase_purchase_return_registered_reverse_charge_itc_input_services_cess = 0;

		this.hsn_summary_sale = [];
		this.hsn_summary_purchase = [];
	};

	// get key for (map key combination) based on transaction type
	this.getTransactionKey = function (txnType) {
		var txnKey = '';
		switch (txnType) {
			case TxnTypeConstant.TXN_TYPE_SALE:
				txnKey = 'outward';
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
				txnKey = 'outward';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE:
				txnKey = 'inward';
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
				txnKey = 'inward';
				break;
		}
		return txnKey;
	};

	// // get key for (map key combination) based on tax rate
	this.getTaxRateForHsn = function (taxCode) {
		var taxRate = -1;
		if (taxCode && taxCode.getTaxRateType() == TaxCodeConstants.Exempted || !taxCode && this.considerNonTaxAsExemptedCheck) {
			taxRate = 'Exempted';
		} else if (taxCode) {
			taxRate = taxCode.getTaxRate();
		}
		return taxRate;
	};

	// returns hsn report object based on hsn + transaction type + taxrate
	this.getHsnReportObject = function (hsnObjectMap, lineItem, txnType, taxCode) {
		var GSTR9HsnReportObject = require('./../BizLogic/GSTR9HsnReportObject.js');
		var hsnReportObject = void 0;
		var itemObj = itemCache.getItemById(lineItem.getItemId());
		var hsnCode = itemObj.getItemHSNCode() ? itemObj.getItemHSNCode() : '';
		var txnKey = this.getTransactionKey(txnType);
		var taxRate = this.getTaxRateForHsn(taxCode);
		hsnCode = hsnCode || '';
		// create map key by tansaction type + hsn code + taxrate for hsn report
		var mapKey = hsnCode + '_' + txnKey + '_' + taxRate;

		if (!hsnObjectMap.has(mapKey)) {
			hsnReportObject = new GSTR9HsnReportObject();
			hsnObjectMap.set(mapKey, hsnReportObject);
			if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
				this.hsn_summary_sale.push(hsnReportObject);
			} else if (txnType == TxnTypeConstant.TXN_TYPE_PURCHASE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
				this.hsn_summary_purchase.push(hsnReportObject);
			}
		} else {
			hsnReportObject = hsnObjectMap.get(mapKey);
		}
		hsnReportObject.setItemName(hsnReportObject.getItemName() + lineItem.getItemName());
		hsnReportObject.setItemHSN(hsnCode);
		hsnReportObject.setRateOfTax(taxRate);
		return hsnReportObject;
	};

	// set tax amount for hsn table
	this.setTaxAmountForHSN = function (taxCode, hsnReportObject, taxableAmt, txnType) {
		if (taxCode.getTaxType() == TaxCodeConstants.taxGroup) {
			for (var p = 0; p < taxCode.getTaxCodeMap().length; p++) {
				var taxId = taxCode.getTaxCodeMap()[p];
				var code = taxCodeCache.getTaxCodeObjectById(taxId);
				if (code != null) {
					this.setTaxEntriesForHSN(hsnReportObject, code, taxableAmt, txnType);
				}
			}
		} else {
			this.setTaxEntriesForHSN(hsnReportObject, taxCode, taxableAmt, txnType);
		}
	};

	// based on transaction type set quantity in hs object
	this.setLineItemHSNQuantity = function (hsnReportObject, lineItem, txnType) {
		if (txnType == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
			hsnReportObject.setItemQuantity(Number(hsnReportObject.getItemQuantity()) - Number(lineItem.getItemQuantity()));
			hsnReportObject.setItemFreeQuantity(Number(hsnReportObject.getItemFreeQuantity()) - Number(lineItem.getItemFreeQuantity()));
			hsnReportObject.setAdditionalCessAmt(Number(hsnReportObject.getAdditionalCessAmt()) - Number(lineItem.getLineItemAdditionalCESS()));
		} else if (txnType == TxnTypeConstant.TXN_TYPE_SALE || txnType == TxnTypeConstant.TXN_TYPE_PURCHASE) {
			hsnReportObject.setItemQuantity(Number(hsnReportObject.getItemQuantity()) + Number(lineItem.getItemQuantity()));
			hsnReportObject.setItemFreeQuantity(Number(hsnReportObject.getItemFreeQuantity()) + Number(lineItem.getItemFreeQuantity()));
			hsnReportObject.setAdditionalCessAmt(Number(hsnReportObject.getAdditionalCessAmt()) + Number(lineItem.getLineItemAdditionalCESS()));
		}
	};

	// set hsn summary data
	this.setHSNWiseSummary = function () {
		var hsnObjectMap = new _map2.default();
		var transactionList = this.transactionList;

		if (transactionList != null) {
			var transactionLength = transactionList.length;
			for (var i = 0; i < transactionLength; i++) {
				var transaction = transactionList[i];
				var txnType = transaction.getTxnType();
				var transcationTaxId = Number(transaction.getTransactionTaxId());
				var transactionSubTotalAmount = Number(transaction.getSubTotalAmount());
				var transactionDiscountAmount = Number(transaction.getDiscountAmount());
				var transactionDiscountPercentage = transactionDiscountAmount / transactionSubTotalAmount * 100;
				var txnTaxCodeObject = taxCodeCache.getTaxCodeObjectById(transcationTaxId);

				for (var j = 0; j < transaction.getLineItems().length; j++) {
					var lineItem = transaction.getLineItems()[j];
					var lineTaxId = Number(lineItem.getLineItemTaxId());
					var lineItemTotal = Number(lineItem.getLineItemTotal());
					var txnTaxableAmount = lineItemTotal - lineItemTotal * transactionDiscountPercentage / 100;
					var lineItemtaxCodeObject = taxCodeCache.getTaxCodeObjectById(lineTaxId);
					var hsnReportObject = void 0;

					// tax use case
					var taxOnLineItem = Number(lineTaxId) != 0 && !GSTRReportHelper.isTaxOfTypeOthers(lineTaxId);
					var taxOnTransaction = Number(transcationTaxId) != 0 && !GSTRReportHelper.isTaxOfTypeOthers(transcationTaxId);
					var noTaxCondition = !taxOnLineItem && !taxOnTransaction && this.considerNonTaxAsExemptedCheck;

					// tax on line item
					if (taxOnLineItem) {
						hsnReportObject = this.getHsnReportObject(hsnObjectMap, lineItem, txnType, lineItemtaxCodeObject);
						var lineItemtaxableAmt = Number(lineItem.getItemQuantity()) * Number(lineItem.getItemUnitPrice()) - Number(lineItem.getLineItemDiscountAmount());
						this.setTaxableAmountForHSN(hsnReportObject, lineItemtaxableAmt, txnType);
						this.setTaxAmountForHSN(lineItemtaxCodeObject, hsnReportObject, lineItemtaxableAmt, txnType, lineItem);
						this.setLineItemHSNQuantity(hsnReportObject, lineItem, txnType);
					}

					// tax on transaction
					if (taxOnTransaction) {
						if (lineTaxId != transcationTaxId) {
							// get new hsnObject if txn tax not same as lineitem tax
							hsnReportObject = this.getHsnReportObject(hsnObjectMap, lineItem, txnType, txnTaxCodeObject);
						}
						this.setTaxableAmountForHSN(hsnReportObject, txnTaxableAmount, txnType);
						this.setTaxAmountForHSN(txnTaxCodeObject, hsnReportObject, txnTaxableAmount, txnType);
						this.setLineItemHSNQuantity(hsnReportObject, lineItem, txnType);
					}

					// This is a specific case when tax is applied nowhere
					if (noTaxCondition) {
						hsnReportObject = this.getHsnReportObject(hsnObjectMap, lineItem, txnType, txnTaxCodeObject);
						this.setTaxableAmountForHSN(hsnReportObject, txnTaxableAmount, txnType);
						this.setLineItemHSNQuantity(hsnReportObject, lineItem, txnType);
					}
				}
			}
		}
	};

	// set taxable amount for hsn wise data based on transaction type
	this.setTaxableAmountForHSN = function (reportObject, taxableAmt, txnType) {
		// taxable amount is considered zero for regular registered user
		if (reportObject.getRateOfTax() == 'Exempted') {
			// taxableAmt = 0;
		}
		switch (txnType) {
			case TxnTypeConstant.TXN_TYPE_SALE:
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
				taxableAmt = taxableAmt * -1;
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE:
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
				taxableAmt = taxableAmt * -1;
		}
		reportObject.setItemTaxableValue(Number(reportObject.getItemTaxableValue()) + taxableAmt);
	};

	// set tax amount based on transaction type and tax type for hsn wise table
	this.setTaxEntriesForHSN = function (reportObject, taxCode, taxableAmt, txnType) {
		var taxAmount = taxableAmt * taxCode.getTaxRate() / 100;

		switch (txnType) {
			case TxnTypeConstant.TXN_TYPE_SALE:
				break;
			case TxnTypeConstant.TXN_TYPE_SALE_RETURN:
				taxAmount = taxAmount * -1;
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE:
				break;
			case TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN:
				taxAmount = taxAmount * -1;
		}

		switch (taxCode.getTaxRateType()) {
			case TaxCodeConstants.IGST:
				reportObject.setIGSTAmt(reportObject.getIGSTAmt() + taxAmount);
				break;
			case TaxCodeConstants.SGST:
				reportObject.setSGSTAmt(reportObject.getSGSTAmt() + taxAmount);
				break;
			case TaxCodeConstants.CGST:
				reportObject.setCGSTAmt(reportObject.getCGSTAmt() + taxAmount);
				break;
			case TaxCodeConstants.CESS:
				reportObject.setCESSAmt(reportObject.getCESSAmt() + taxAmount);
				break;
		}
	};

	this.setGSTR9DataListBasedOnDate = function (fromDate, toDate, firmId, considerNonTaxAsExemptedCheck) {
		// initialize all values to zero
		this.initializeValues();
		this.considerNonTaxAsExemptedCheck = considerNonTaxAsExemptedCheck;
		var taxCode = void 0;
		var taxableAmount = void 0;
		var dataLoader = new DataLoader();
		// load transactions (sale, sale return, purchase, purchase return)
		var transactionList = dataLoader.loadTransactionsForGSTR9Report(fromDate, toDate, firmId);
		this.transactionList = transactionList;
		// set hsn summary based on transaction type + hsn + tax rate
		this.setHSNWiseSummary();

		if (transactionList != null) {
			var transactionLength = transactionList.length;
			for (var i = 0; i < transactionLength; i++) {
				var transaction = transactionList[i];
				var transactionType = transaction.getTxnType();
				var lineItemsList = transaction.getLineItems();
				var lineItemsLength = lineItemsList.length;
				var transcationTaxId = Number(transaction.getTransactionTaxId());
				var isReverseChargeInTxn = Number(transaction.getReverseCharge());
				var nameObj = transaction.getNameRef();
				var partyType = nameObj.getCustomerType();
				var transactionSubTotalAmount = Number(transaction.getSubTotalAmount());
				var transactionDiscountAmount = Number(transaction.getDiscountAmount());
				var transactionDiscountPercentage = transactionDiscountAmount / transactionSubTotalAmount * 100;

				for (var j = 0; j < lineItemsLength; j++) {
					var lineItem = lineItemsList[j];
					var itemObj = itemCache.getItemById(lineItem.getItemId());
					var lineItemITCType = lineItem.getLineItemITCValue();
					lineItemITCType = GSTRReportHelper.getITCTypeForGSTR2Report(lineItemITCType, itemObj);

					var lineTaxId = Number(lineItem.getLineItemTaxId());
					var lineItemTotal = Number(lineItem.getLineItemTotal());

					// tax use case
					var taxOnLineItem = Number(lineTaxId) != 0 && !GSTRReportHelper.isTaxOfTypeOthers(lineTaxId);
					var taxOnTransaction = Number(transcationTaxId) != 0 && !GSTRReportHelper.isTaxOfTypeOthers(transcationTaxId);
					var noTaxCondition = !taxOnLineItem && !taxOnTransaction && this.considerNonTaxAsExemptedCheck;

					// tax on lineitem
					if (taxOnLineItem) {
						taxCode = lineTaxId ? taxCodeCache.getTaxCodeObjectById(lineTaxId) : null;
						taxableAmount = Number(lineItem.getItemQuantity()) * Number(lineItem.getItemUnitPrice()) - Number(lineItem.getLineItemDiscountAmount());
						this.setTaxableAmount(taxCode, taxableAmount, transaction);
						if (taxCode) {
							this.setTaxAmount(taxCode, taxableAmount, transaction, lineItemITCType, lineItem);
						}
					}

					// tax on transaction
					if (taxOnTransaction) {
						taxCode = taxCodeCache.getTaxCodeObjectById(transcationTaxId);
						taxableAmount = lineItemTotal - lineItemTotal * transactionDiscountPercentage / 100;
						if (taxCode && taxCode.taxCodeName == 'Exempted') {
							taxableAmount = 0;
						}
						this.setTaxableAmount(taxCode, taxableAmount, transaction);
						this.setTaxAmount(taxCode, taxableAmount, transaction, lineItemITCType);
					}

					// This is a specific case when tax is not apllied anywhere
					if (noTaxCondition) {
						taxCode = null;
						taxableAmount = lineItemTotal - lineItemTotal * transactionDiscountPercentage / 100;
						this.setTaxableAmount(taxCode, taxableAmount, transaction);
					}
				}
			}
		}
	};

	// set tax amount based on tax (group or single)
	this.setTaxAmount = function (taxCode, taxableAmount, transaction, lineItemITCType) {
		var lineItem = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;

		if (taxCode.getTaxType() == TaxCodeConstants.taxGroup) {
			for (var p = 0; p < taxCode.getTaxCodeMap().length; p++) {
				var taxId = taxCode.getTaxCodeMap()[p];
				var code = taxCodeCache.getTaxCodeObjectById(taxId);
				if (code != null) {
					this.setTaxEntries(code, taxableAmount, transaction, lineItemITCType, lineItem);
				}
			}
		} else {
			this.setTaxEntries(taxCode, taxableAmount, transaction, lineItemITCType, lineItem);
		}
	};

	// set taxable amount based on various conditions
	this.setTaxableAmount = function (taxCode, taxableAmt, transaction) {
		var NameCustomerType = require('./../Constants/NameCustomerType.js');
		var transactionType = transaction.getTxnType();
		var nameObj = transaction.getNameRef();
		var partyType = nameObj.getCustomerType();
		var isReverseChargeInTxn = Number(transaction.getReverseCharge());
		// exempted entry condition
		var exemptedEntry = taxCode && taxCode.getTaxRateType() == TaxCodeConstants.Exempted || !taxCode && this.considerNonTaxAsExemptedCheck;

		if (transactionType == TxnTypeConstant.TXN_TYPE_SALE) {
			// for sale
			if (exemptedEntry) {
				// exempted
				this.sale_exempted_taxable_value += taxableAmt;
			} else if (taxCode && taxCode.getTaxRate() == 0) {
				// nil rated sale
				this.sale_nil_rated_taxable_value += taxableAmt;
			} else {
				if (partyType == NameCustomerType.UNREGISTERED) {
					// unregistered sale
					this.sale_purchase_without_reverse_charge_unregistered_taxable_value += taxableAmt;
				} else {
					// registered and composite
					this.sale_purchase_without_reverse_charge_registered_taxable_value += taxableAmt;
				}
			}
		} else if (transactionType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
			// for sale return
			if (taxCode && taxCode.getTaxRate() == 0 || exemptedEntry) {
				// nil rated or exempted sale return
				this.sale_return_exempted_nil_rated_taxable_value += taxableAmt;
			} else {
				if (partyType == NameCustomerType.UNREGISTERED) {
					// no data
				} else {
					// registered and composite
					if (!(taxCode && taxCode.getTaxRate() == 0) && !exemptedEntry) {
						// gst tax apart from exempted and nil rated sale return
						this.sale_return_registered_taxable_value += taxableAmt;
					}
				}
			}
		} else if (transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE) {
			// for purchase
			if (isReverseChargeInTxn) {
				// reverse charge purchase
				if (!(taxCode && taxCode.getTaxRate() == 0) && !exemptedEntry) {
					this.purchase_reverse_charge_taxable_value += taxableAmt;
				}
			}
		} else if (transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
			// for purchase return
			if (isReverseChargeInTxn) {
				if (!(taxCode && taxCode.getTaxRate() == 0) && !exemptedEntry) {
					this.purchase_reverse_charge_taxable_value -= taxableAmt;
				}
			}
		}
	};

	// set tax entries based on transaction type + tax rate type + party type + ITC type
	this.setTaxEntries = function (taxCode, taxableAmt, transaction, lineItemITCType, lineItem) {
		var NameCustomerType = require('./../Constants/NameCustomerType.js');
		var ITCTypeConstantsGSTR = require('./../Constants/ITCTypeConstantsGSTR2.js');
		var transactionType = transaction.getTxnType();
		var nameObj = transaction.getNameRef();
		var partyType = nameObj.getCustomerType();
		var isReverseChargeInTxn = Number(transaction.getReverseCharge());
		var taxAmount = taxableAmt * taxCode.getTaxRate() / 100;
		var lineitemAdditionalCessAmount = lineItem ? Number(lineItem.getLineItemAdditionalCESS()) : 0;

		if (transactionType == TxnTypeConstant.TXN_TYPE_SALE) {
			if (partyType == NameCustomerType.UNREGISTERED) {
				if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
					this.sale_purchase_without_reverse_charge_unregistered_cgst += taxAmount;
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
					this.sale_purchase_without_reverse_charge_unregistered_sgst += taxAmount;
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
					this.sale_purchase_without_reverse_charge_unregistered_igst += taxAmount;
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
					this.sale_purchase_without_reverse_charge_unregistered_cess += taxAmount + lineitemAdditionalCessAmount;
				}
			} else {
				if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
					this.sale_purchase_without_reverse_charge_registered_cgst += taxAmount;
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
					this.sale_purchase_without_reverse_charge_registered_sgst += taxAmount;
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
					this.sale_purchase_without_reverse_charge_registered_igst += taxAmount;
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
					this.sale_purchase_without_reverse_charge_registered_cess += taxAmount + lineitemAdditionalCessAmount;
				}
			}
		} else if (transactionType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
			if (partyType == NameCustomerType.UNREGISTERED) {} else {
				if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
					this.sale_return_registered_cgst += taxAmount;
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
					this.sale_return_registered_sgst += taxAmount;
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
					this.sale_return_registered_igst += taxAmount;
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
					this.sale_return_registered_cess += taxAmount + lineitemAdditionalCessAmount;
				}
			}
		} else if (transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE) {
			if (isReverseChargeInTxn) {
				if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
					this.purchase_reverse_charge_cgst += taxAmount;
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
					this.purchase_reverse_charge_sgst += taxAmount;
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
					this.purchase_reverse_charge_igst += taxAmount;
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
					this.purchase_reverse_charge_cess += taxAmount + lineitemAdditionalCessAmount;
				}
				if (partyType == NameCustomerType.UNREGISTERED) {
					if (lineItemITCType == ITCTypeConstantsGSTR.INPUTS) {
						if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
							this.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_cgst += taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
							this.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_sgst += taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
							this.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_igst += taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
							this.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_cess += taxAmount + lineitemAdditionalCessAmount;
						}
					} else if (lineItemITCType == ITCTypeConstantsGSTR.CAPITAL_GOODS) {
						if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
							this.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_sgst += taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
							this.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_cgst += taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
							this.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_igst += taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
							this.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_cess += taxAmount + lineitemAdditionalCessAmount;
						}
					} else if (lineItemITCType == ITCTypeConstantsGSTR.INPUT_SERVICES) {
						if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
							this.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_cgst += taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
							this.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_sgst += taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
							this.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_igst += taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
							this.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_cess += taxAmount + lineitemAdditionalCessAmount;
						}
					}
				} else {
					if (lineItemITCType == ITCTypeConstantsGSTR.INPUTS) {
						if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
							this.purchase_purchase_return_registered_reverse_charge_itc_inputs_cgst += taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
							this.purchase_purchase_return_registered_reverse_charge_itc_inputs_sgst += taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
							this.purchase_purchase_return_registered_reverse_charge_itc_inputs_igst += taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
							this.purchase_purchase_return_registered_reverse_charge_itc_inputs_cess += taxAmount + lineitemAdditionalCessAmount;
						}
					} else if (lineItemITCType == ITCTypeConstantsGSTR.CAPITAL_GOODS) {
						if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
							this.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_cgst += taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
							this.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_sgst += taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
							this.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_igst += taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
							this.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_cess += taxAmount + lineitemAdditionalCessAmount;
						}
					} else if (lineItemITCType == ITCTypeConstantsGSTR.INPUT_SERVICES) {
						if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
							this.purchase_purchase_return_registered_reverse_charge_itc_input_services_cgst += taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
							this.purchase_purchase_return_registered_reverse_charge_itc_input_services_sgst += taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
							this.purchase_purchase_return_registered_reverse_charge_itc_input_services_igst += taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
							this.purchase_purchase_return_registered_reverse_charge_itc_input_services_cess += taxAmount + lineitemAdditionalCessAmount;
						}
					}
				}
			} else {
				if (lineItemITCType == ITCTypeConstantsGSTR.INPUTS) {
					if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
						this.purchase_purchase_return_without_reverse_charge_itc_inputs_cgst += taxAmount;
					} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
						this.purchase_purchase_return_without_reverse_charge_itc_inputs_sgst += taxAmount;
					} else if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
						this.purchase_purchase_return_without_reverse_charge_itc_inputs_igst += taxAmount;
					} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
						this.purchase_purchase_return_without_reverse_charge_itc_inputs_cess += taxAmount + lineitemAdditionalCessAmount;
					}
				} else if (lineItemITCType == ITCTypeConstantsGSTR.CAPITAL_GOODS) {
					if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
						this.purchase_purchase_return_without_reverse_charge_itc_capital_goods_cgst += taxAmount;
					} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
						this.purchase_purchase_return_without_reverse_charge_itc_capital_goods_sgst += taxAmount;
					} else if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
						this.purchase_purchase_return_without_reverse_charge_itc_capital_goods_igst += taxAmount;
					} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
						this.purchase_purchase_return_without_reverse_charge_itc_capital_goods_cess += taxAmount + lineitemAdditionalCessAmount;
					}
				} else if (lineItemITCType == ITCTypeConstantsGSTR.INPUT_SERVICES) {
					if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
						this.purchase_purchase_return_without_reverse_charge_itc_input_services_cgst += taxAmount;
					} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
						this.purchase_purchase_return_without_reverse_charge_itc_input_services_sgst += taxAmount;
					} else if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
						this.purchase_purchase_return_without_reverse_charge_itc_input_services_igst += taxAmount;
					} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
						this.purchase_purchase_return_without_reverse_charge_itc_input_services_cess += taxAmount + lineitemAdditionalCessAmount;
					}
				}
			}
		} else if (transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
			if (isReverseChargeInTxn) {
				if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
					this.purchase_reverse_charge_cgst -= taxAmount;
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
					this.purchase_reverse_charge_sgst -= taxAmount;
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
					this.purchase_reverse_charge_igst -= taxAmount;
				} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
					this.purchase_reverse_charge_cess -= taxAmount + lineitemAdditionalCessAmount;
				}
				if (partyType == NameCustomerType.UNREGISTERED) {
					if (lineItemITCType == ITCTypeConstantsGSTR.INPUTS) {
						if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
							this.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_cgst -= taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
							this.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_sgst -= taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
							this.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_igst -= taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
							this.purchase_purchase_return_unregistered_reverse_charge_itc_inputs_cess -= taxAmount + lineitemAdditionalCessAmount;
						}
					} else if (lineItemITCType == ITCTypeConstantsGSTR.CAPITAL_GOODS) {
						if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
							this.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_cgst -= taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
							this.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_sgst -= taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
							this.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_igst -= taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
							this.purchase_purchase_return_unregistered_reverse_charge_itc_capital_goods_cess -= taxAmount + lineitemAdditionalCessAmount;
						}
					} else if (lineItemITCType == ITCTypeConstantsGSTR.INPUT_SERVICES) {
						if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
							this.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_cgst -= taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
							this.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_sgst -= taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
							this.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_igst -= taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
							this.purchase_purchase_return_unregistered_reverse_charge_itc_input_services_cess -= taxAmount + lineitemAdditionalCessAmount;
						}
					}
				} else {
					if (lineItemITCType == ITCTypeConstantsGSTR.INPUTS) {
						if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
							this.purchase_purchase_return_registered_reverse_charge_itc_inputs_cgst -= taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
							this.purchase_purchase_return_registered_reverse_charge_itc_inputs_sgst -= taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
							this.purchase_purchase_return_registered_reverse_charge_itc_inputs_igst -= taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
							this.purchase_purchase_return_registered_reverse_charge_itc_inputs_cess -= taxAmount + lineitemAdditionalCessAmount;
						}
					} else if (lineItemITCType == ITCTypeConstantsGSTR.CAPITAL_GOODS) {
						if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
							this.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_cgst -= taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
							this.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_sgst -= taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
							this.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_igst -= taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
							this.purchase_purchase_return_registered_reverse_charge_itc_capital_goods_cess -= taxAmount + lineitemAdditionalCessAmount;
						}
					} else if (lineItemITCType == ITCTypeConstantsGSTR.INPUT_SERVICES) {
						if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
							this.purchase_purchase_return_registered_reverse_charge_itc_input_services_cgst -= taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
							this.purchase_purchase_return_registered_reverse_charge_itc_input_services_sgst -= taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
							this.purchase_purchase_return_registered_reverse_charge_itc_input_services_igst -= taxAmount;
						} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
							this.purchase_purchase_return_registered_reverse_charge_itc_input_services_cess -= taxAmount + lineitemAdditionalCessAmount;
						}
					}
				}
			} else {
				if (lineItemITCType == ITCTypeConstantsGSTR.INPUTS) {
					if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
						this.purchase_purchase_return_without_reverse_charge_itc_inputs_cgst -= taxAmount;
					} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
						this.purchase_purchase_return_without_reverse_charge_itc_inputs_sgst -= taxAmount;
					} else if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
						this.purchase_purchase_return_without_reverse_charge_itc_inputs_igst -= taxAmount;
					} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
						this.purchase_purchase_return_without_reverse_charge_itc_inputs_cess -= taxAmount + lineitemAdditionalCessAmount;
					}
				} else if (lineItemITCType == ITCTypeConstantsGSTR.CAPITAL_GOODS) {
					if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
						this.purchase_purchase_return_without_reverse_charge_itc_capital_goods_cgst -= taxAmount;
					} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
						this.purchase_purchase_return_without_reverse_charge_itc_capital_goods_sgst -= taxAmount;
					} else if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
						this.purchase_purchase_return_without_reverse_charge_itc_capital_goods_igst -= taxAmount;
					} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
						this.purchase_purchase_return_without_reverse_charge_itc_capital_goods_cess -= taxAmount + lineitemAdditionalCessAmount;
					}
				} else if (lineItemITCType == ITCTypeConstantsGSTR.INPUT_SERVICES) {
					if (taxCode.getTaxRateType() == TaxCodeConstants.CGST) {
						this.purchase_purchase_return_without_reverse_charge_itc_input_services_cgst -= taxAmount;
					} else if (taxCode.getTaxRateType() == TaxCodeConstants.SGST) {
						this.purchase_purchase_return_without_reverse_charge_itc_input_services_sgst -= taxAmount;
					} else if (taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
						this.purchase_purchase_return_without_reverse_charge_itc_input_services_igst -= taxAmount;
					} else if (taxCode.getTaxRateType() == TaxCodeConstants.CESS) {
						this.purchase_purchase_return_without_reverse_charge_itc_input_services_cess -= taxAmount + lineitemAdditionalCessAmount;
					}
				}
			}
		}
	};
};

module.exports = GSTR9ReportHelper;