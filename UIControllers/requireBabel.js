(function requireBabel() {
	var isDev = require('electron-is-dev');
	if (isDev) {
		require('babel-register');
		require('babel-polyfill');
	} else {
		process.env.NODE_ENV = 'production';
	}
})();