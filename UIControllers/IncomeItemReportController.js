var startDate;
var endDate;
var DateFormat = require('./../Constants/DateFormat.js');
$('#startDate, #endDate').datepicker({ dateFormat: DateFormat.format });
var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();
var NameCache = require('./../Cache/NameCache.js');
var nameCache = new NameCache();
var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');
var printForSharePDF = false;

var getCurrentDataForTable = function getCurrentDataForTable() {
	settingCache = new SettingCache();
	var DataLoader = require('./../DBManager/DataLoader.js');
	var dataLoader = new DataLoader();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var firmId = Number($('#firmFilterOptions').val());
	startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
	endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');
	var listOfTransactions = dataLoader.getIncomeItemReportList(startDate, endDate, firmId);
	return listOfTransactions;
};

var populateTable = function populateTable() {
	CommonUtility.showLoader(function () {
		var listOfTransactions = getCurrentDataForTable();
		displayTransactionList(listOfTransactions);
	});
};

var date = MyDate.getDate('d/m/y');
$('#startDate').val(MyDate.getFirstDateOfCurrentMonthString());
$('#endDate').val(date);

if (settingCache.getMultipleFirmEnabled()) {
	var optionTemp = document.createElement('option');
	var FirmCache = require('./../Cache/FirmCache.js');
	var firmCache = new FirmCache();
	var firmList = firmCache.getFirmList();
	for (i in firmList) {
		var option = optionTemp.cloneNode();
		option.text = firmList[i].getFirmName();
		option.value = firmList[i].getFirmId();
		$('#firmFilterOptions').append(option);
	}
	$('#firmChooser').show();
} else {
	$('#firmChooser').hide();
}

$('#firmFilterOptions').on('selectmenuchange', function (event) {
	populateTable();
});

$('#startDate').change(function () {
	populateTable();
});

$('#endDate').change(function () {
	populateTable();
});

var incomeItemReportClusterize;

var displayTransactionList = function displayTransactionList(listOfTransactions) {
	var len = listOfTransactions.length;
	var dynamicRow = '';
	var rowData = [];
	var totalAmount = 0;
	var totalQuantity = 0;

	dynamicRow += "<thead><tr><th width='50%'>Income Item</th><th width='20%'>Quantity</th><th width='30%' class='tableCellTextAlignRight'>Amount</th></tr></thead>";
	for (var i = 0; i < len; i++) {
		var txn = listOfTransactions[i];

		var row = "<tr class='currentRow'><td width='50%'>" + txn.getItemName() + '</td>';
		row += "<td width='20%'>" + txn.getQty() + "</td><td width='30%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(txn.getAmount()) + '</td></tr>';
		totalAmount += Number(txn.getAmount());
		totalQuantity += Number(txn.getQty());
		rowData.push(row);
	}
	var data = rowData;

	if ($('#incomeItemReportContainer').length === 0) {
		return;
	}
	if (incomeItemReportClusterize && incomeItemReportClusterize.destroy) {
		incomeItemReportClusterize.clear();
		incomeItemReportClusterize.destroy();
	}

	incomeItemReportClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollArea',
		contentId: 'contentArea',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});
	$('#tableHead').html(dynamicRow);
	$('#totalAmount').html(MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalAmount));
	$('#totalQuantity').html(totalQuantity);
};

var openTransaction = function openTransaction(txnId, txnType) {
	var ViewTransaction = require('./../BizLogic/ViewTransaction.js');
	var viewTransaction = new ViewTransaction();
	viewTransaction.viewTransactionFile(txnId + ':' + TxnTypeConstant.getTxnType(txnType));
};

if (settingCache.getItemEnabled()) {
	$('#itemDetailPrintDiv').show();
} else {
	$('#itemDetailPrintDiv').hide();
}

var getHTMLTextForReport = function getHTMLTextForReport() {
	var IncomeItemReportHTMLGenerator = require('./../ReportHTMLGenerator/IncomeItemReportHTMLGenerator.js');
	var incomeItemReportHTMLGenerator = new IncomeItemReportHTMLGenerator();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var firmId = Number($('#firmFilterOptions').val());
	var listOfTransactions = getCurrentDataForTable();
	var htmlText = incomeItemReportHTMLGenerator.getHTMLText(listOfTransactions, startDateString, endDateString, firmId);
	return htmlText;
};

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	// $('#frameDiv').load('');
	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable();
};

/// ///////////////////////////PDF/////////////////////////////////////
var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.OTHER_INCOME_ITEM_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

function openExcelOptions() {
	$('#ExcelDialog').removeClass('hide');
	$('#ExcelDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closeExcelOptions() {
	$('#ExcelDialog').addClass('hide');
	$('#ExcelDialog').dialog('close').dialog('destroy');
}

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.OTHER_INCOME_ITEM_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
	excelHelper.saveExcel(fileName);
};

var prepareObjectForExcel = function prepareObjectForExcel(listOfTransactions) {
	var len = listOfTransactions.length;
	var rowObj = [];
	var tableHeadArray = [];
	var totalArray = [];
	tableHeadArray.push('Other Income Item');
	tableHeadArray.push('Quantity');
	tableHeadArray.push('Amount');
	rowObj.push(tableHeadArray);
	rowObj.push([]);

	var totalAmount = 0;
	var totalQuantity = 0;

	for (var i = 0; i < len; i++) {
		var txn = listOfTransactions[i];
		var tempArray = [];
		var name = txn.getItemName();
		var quantity = txn.getQty();
		var amount = txn.getAmount();
		tempArray.push(name);
		tempArray.push(quantity);
		tempArray.push(MyDouble.getAmountWithDecimal(amount));
		totalQuantity += Number(quantity);
		totalAmount += Number(amount);
		rowObj.push(tempArray);
	}
	rowObj.push([]);
	totalArray.push('Total');
	totalArray.push(totalQuantity);
	totalArray.push(MyDouble.getAmountWithDecimal(totalAmount));
	rowObj.push(totalArray);
	return rowObj;
};

function printExcel() {
	closeExcelOptions();
	var listOfTransactions = getCurrentDataForTable();
	console.log(listOfTransactions);
	var XLSX = require('xlsx');
	var dataArray = prepareObjectForExcel(listOfTransactions);
	var worksheet = XLSX.utils.aoa_to_sheet(dataArray);

	var workbook = {
		'SheetNames': [],
		'Sheets': {}
	};

	var wsName = 'Other Income Report';
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet;
	var wscolWidth = [{ wch: 30 }, { wch: 30 }, { wch: 30 }];
	worksheet['!cols'] = wscolWidth;
	console.log(workbook);
	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

populateTable();