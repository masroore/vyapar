var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var TransactionStatus = require('./../Constants/TransactionStatus.js');
var startDate;
var endDate;
var DateFormat = require('./../Constants/DateFormat.js');
$('#startDate, #endDate').datepicker({ dateFormat: DateFormat.format });
var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var NameCache = require('./../Cache/NameCache.js');
var nameCache = new NameCache();
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');

var printForSharePDF = false;

var listOfTransactions = [];

var populateTable = function populateTable() {
	settingCache = new SettingCache();
	var DataLoader = require('./../DBManager/DataLoader.js');
	var dataLoader = new DataLoader();

	startDateString = $('#startDate').val();
	endDateString = $('#endDate').val();
	nameString = $('#nameForReport').val();
	var selectedTransaction = Number($('#transactionFilterOptions').val());
	var orderType = Number($('#transactionTypeFilterOptions').val());
	var nameId = -1;

	if (nameString) {
		var nameModel = nameCache.findNameModelByName(nameString);
		if (nameModel) {
			nameId = nameModel.getNameId();
		} else {
			nameId = 0;
		}
	}

	startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
	endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');
	var firmId = Number($('#firmFilterOptions').val());
	CommonUtility.showLoader(function () {
		listOfTransactions = dataLoader.LoadAllOrders(startDate, endDate, nameId, firmId, true, orderType, selectedTransaction);
		displayTransactionList(listOfTransactions);
	});
};

var date = MyDate.getDate('d/m/y');
$('#startDate').val(MyDate.getFirstDateOfCurrentMonthString());
$('#endDate').val(date);

if (settingCache.getMultipleFirmEnabled()) {
	var optionTemp = document.createElement('option');
	var FirmCache = require('./../Cache/FirmCache.js');
	var firmCache = new FirmCache();
	var firmList = firmCache.getFirmList();
	for (i in firmList) {
		var option = optionTemp.cloneNode();
		option.text = firmList[i].getFirmName();
		option.value = firmList[i].getFirmId();
		$('#firmFilterOptions').append(option);
	}
	$('#firmChooser').show();
} else {
	$('#firmChooser').hide();
}

var optionTemp = document.createElement('option');
var transactionListArray = ['Open Orders', 'Close Orders'];
var transactionListId = [TransactionStatus.TXN_ORDER_OPEN, TransactionStatus.TXN_ORDER_COMPLETE];
for (i in transactionListArray) {
	//        console.log(i);
	var option = optionTemp.cloneNode();
	option.text = transactionListArray[i];
	option.value = transactionListId[i];
	$('#transactionFilterOptions').append(option);
}

var optionTemp = document.createElement('option');
var transactionTypeListArray = ['Sale Order', 'Purchase Order'];
var transactionTypeListId = [TxnTypeConstant.TXN_TYPE_SALE_ORDER, TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER];
for (i in transactionTypeListArray) {
	var option = optionTemp.cloneNode();
	option.text = transactionTypeListArray[i];
	option.value = transactionTypeListId[i];
	$('#transactionTypeFilterOptions').append(option);
}

try {
	$('#transactionTypeFilterOptions').selectmenu('refresh', true);
} catch (err) {}

$('#firmFilterOptions').on('selectmenuchange', function (event) {
	populateTable();
});

$('#startDate').change(function () {
	populateTable();
});

$('#endDate').change(function () {
	populateTable();
});

$(function () {
	$('#nameForReport').autocomplete(UIHelper.getAutocompleteDefaultOptions(nameCache.getListOfNames()));
});

$('#nameForReport').on('change', function (event) {
	populateTable();
});

$('#transactionFilterOptions').on('selectmenuchange', function () {
	populateTable();
});

$('#transactionTypeFilterOptions').on('selectmenuchange', function () {
	populateTable();
});

var orderTxnReportClusterize;

var displayTransactionList = function displayTransactionList(listOfTransactions) {
	debugger;

	var len = listOfTransactions.length;
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var dynamicRow = '';
	var rowData = [];
	var isTransactionRefNumberEnabled = settingCache.getTxnRefNoEnabled();
	var extraColumnWidth = isTransactionRefNumberEnabled ? 0 : 1.5;

	var className = '';
	if (!$('#sortIcon').attr('class')) {
		className = 'ui-selectmenu-icon ui-icon ui-icon-triangle-1-s';
	} else {
		className = $('#sortIcon').attr('class');
	}

	dynamicRow += "<thead><tr><th width='" + (11 + extraColumnWidth) + "%'>DATE</th>";
	if (isTransactionRefNumberEnabled) {
		dynamicRow += '<th width=\'10.5%\' id=\'invoiceColId\' style=\'cursor:pointer;\'>Order No.<span id=\'sortIcon\' class=\'' + className + '\'></span></th>';
	}
	dynamicRow += "<th width='" + (13 + extraColumnWidth) + "%'>NAME</th><th width='" + (10.5 + extraColumnWidth) + "%'>Due date</th>" + "<th width='" + (11 + extraColumnWidth) + "%'>Status</th><th width='" + (8 + extraColumnWidth) + "%'>TYPE</th><th width='" + (12 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>TOTAL</th><th width='" + (12 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>ADVANCE</th><th width='" + (12 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>BALANCE</th></tr></thead>";
	var finalTotalAmount = 0;
	for (var _i = 0; _i < len; _i++) {
		var txn = listOfTransactions[_i];
		var typeTxn = TxnTypeConstant.getTxnType(txn.getTxnType());
		var txnTypeString = TxnTypeConstant.getTxnTypeForUI(txn.getTxnType());
		var name = txn.getNameRef().getFullName();
		var date = MyDate.getDate('d/m/y', txn.getTxnDate());
		var dueDate = MyDate.getDate('d/m/y', txn.getTxnDueDate());
		var receivedAmt = txn.getCashAmount() ? txn.getCashAmount() : 0;
		var balanceAmt = txn.getBalanceAmount() ? txn.getBalanceAmount() : 0;
		var txnStatus = txn.getStatus();
		var refNo = txn.getTxnRefNumber() ? txn.getFullTxnRefNumber() : '';
		var totalAmt = Number(receivedAmt) + Number(balanceAmt);
		finalTotalAmount += totalAmt;

		var row = '<tr id=' + txn.getTxnId() + ':' + typeTxn + " ondblclick='openTransaction(" + txn.getTxnId() + ',' + txn.getTxnType() + ")' class='currentRow'><td width='" + (11 + extraColumnWidth) + "%'>" + date + '</td>';
		if (isTransactionRefNumberEnabled) {
			row += "<td width='10.5%'>" + refNo + '</td>';
		}
		row += "<td width='" + (15 + extraColumnWidth) + "%'>" + name + "</td><td width='" + (10.5 + extraColumnWidth) + "%'>" + dueDate + '</td>' + "<td width='" + (11 + extraColumnWidth) + "%'>" + (txnStatus == TransactionStatus.TXN_ORDER_COMPLETE ? StringConstant.orderCompletedString : StringConstant.orderOpen) + "</td><td width='" + (8 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>" + txnTypeString + "</td><td width='" + (14 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalAmt) + "</td><td width='" + (14 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(receivedAmt) + '</td>';
		row += "<td width='" + (14 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(balanceAmt) + '</td></tr>';

		rowData.push(row);
	}
	var data = rowData;

	if ($('#orderTxnReportContainer').length === 0) {
		return;
	}
	if (orderTxnReportClusterize && orderTxnReportClusterize.destroy) {
		orderTxnReportClusterize.clear();
		orderTxnReportClusterize.destroy();
	}
	orderTxnReportClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollArea',
		contentId: 'contentArea',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});
	$('#tableHead').html(dynamicRow);
	$('#totalAmount').html(MyDouble.getAmountWithDecimalAndCurrencyWithSign(finalTotalAmount));
	$('#total').css('display', 'block');
	$('#totalAmount').css('color', '#1faf9d');
};

$('#tableHead').on('click', '#invoiceColId', function (e) {
	var SortHelper = require('./../Utilities/SortHelper.js');
	var sortHelper = new SortHelper();
	listOfTransactions = sortHelper.getSortedListByInvoice(listOfTransactions, 'txnRefNumber');
	displayTransactionList(listOfTransactions);
});

var openTransaction = function openTransaction(txnId, txnType) {
	var ViewTransaction = require('./../BizLogic/ViewTransaction.js');
	var viewTransaction = new ViewTransaction();
	viewTransaction.viewTransactionFile(txnId + ':' + TxnTypeConstant.getTxnType(txnType));
};

if (settingCache.getItemEnabled()) {
	$('#itemDetailPrintDiv').show();
} else {
	$('#itemDetailPrintDiv').hide();
}

var getHTMLTextForReport = function getHTMLTextForReport() {
	var OrderTxnReportHTMLGenerator = require('./../ReportHTMLGenerator/OrderTxnReportHTMLGenerator.js');
	var orderTxnReportHTMLGenerator = new OrderTxnReportHTMLGenerator();

	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var firmId = Number($('#firmFilterOptions').val());
	var nameString = $('#nameForReport').val();
	if (!nameString) {
		nameString = 'All parties';
	}
	var printItemDetails = $('#printItems').is(':checked');
	var printDescription = $('#printDescription').is(':checked');
	var htmlText = orderTxnReportHTMLGenerator.getHTMLText(listOfTransactions, startDateString, endDateString, firmId, nameString, printItemDetails, printDescription);
	return htmlText;
};

var printTransaction = function printTransaction(that) {
	var settingCache = new SettingCache();
	var ThermalPrinterConstant = require('./../Constants/ThermalPrinterConstant.js');
	if (settingCache.getDefaultPrinterType() == ThermalPrinterConstant.THERMAL_PRINTER) {
		var txnId = that.id.split(':')[0];
		var PrintUtil = require('./../Utilities/PrintUtil.js');
		PrintUtil.printTransactionUsingThermalPrinter(txnId);
	} else {
		var html = transactionPDFHandler.transactionToHTML(that.id);
		if (html) {
			pdfHandler.print(html);
		}
	}
};

$('#currentAccountTransactionDetails').contextmenu({
	delegate: '.currentRow',
	addClass: 'width10',
	menu: [{ title: 'Open PDF', cmd: 'openpdf' }, { title: '----' }, { title: 'Print', cmd: 'print' }, { title: '----' }, { title: 'Delete', cmd: 'delete' }],
	select: function select(event, ui) {
		var id = ui.target[0].parentNode.id;
		if (ui.cmd == 'print') {
			printTransaction(ui.target[0].parentNode);
		} else if (ui.cmd == 'openpdf') {
			openPdfFromRightClickMenu(ui.target[0].parentNode);
		} else if (ui.cmd == 'delete') {
			var DeleteTransaction = require('./../BizLogic/DeleteTransaction.js');
			var deleteTransaction = new DeleteTransaction();
			deleteTransaction.RemoveTransaction(id);
		}
	}
});

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	// $('#frameDiv').load('');
	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ display: 'none' });
		$('.viewItems').css('display', 'block');
	}

	populateTable();
};

/// ///////////////////////////PDF/////////////////////////////////////

function openPrintOptions() {
	$('#customPrintDialog').removeClass('hide');
	$('#customPrintDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closePrintOptions() {
	$('#customPrintDialog').addClass('hide');
	$('#customPrintDialog').dialog('close').dialog('destroy');
}

var openPdfFromRightClickMenu = function openPdfFromRightClickMenu(that) {
	$('#loading').show(function () {
		var html = transactionPDFHandler.transactionToHTML(that.id);
		if (html) {
			pdfHandler.openPDF(html, false);
		}
	});
};

var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.ORDER_TXN_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#customPrintDialog').addClass('hide');
	$('#customPrintDialog').dialog('close').dialog('destroy');
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

function openExcelOptions() {
	$('#ExcelDialog').removeClass('hide');
	$('#ExcelDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

if (settingCache.getItemEnabled()) {
	$('#itemDetailsExcel').show();
} else {
	$('#itemDetailsExcel').hide();
}

function closeExcelOptions() {
	$('#ExcelDialog').addClass('hide');
	$('#ExcelDialog').dialog('close').dialog('destroy');
}

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.ORDER_TXN_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
	excelHelper.saveExcel(fileName);
};

var prepareObjectForExcel = function prepareObjectForExcel(listOfTransactions) {
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var shouldPrintDescription = $('#printDescription').is(':checked');

	var rowObj = [];
	var len = listOfTransactions.length;
	var isTransactionRefNumberEnabled = settingCache.getTxnRefNoEnabled();
	// report title
	var reportTitle = ['', '', 'Order Transaction Report'];
	rowObj.push(reportTitle);

	// date for report
	startDateString = $('#startDate').val();
	endDateString = $('#endDate').val();
	rowObj.push(['', '', startDateString + ' to ' + endDateString]);

	// add headers
	var headers = [];
	headers.push('Date');
	if (isTransactionRefNumberEnabled) {
		headers.push('Order No.');
	}
	headers.push.apply(headers, ['Party Name', 'Due date', 'Status', 'Type', 'Total', 'Advance', 'Balance']);
	if (printDescription) {
		headers.push('Description');
	}

	rowObj.push(headers);
	rowObj.push([]);
	var tempArray = [];
	var finalTotalAmount = 0;
	for (var _i2 = 0; _i2 < len; _i2++) {
		var _tempArray;

		var printDescription = false;
		printDescription = shouldPrintDescription && txn.getDescription();

		var txn = listOfTransactions[_i2];
		var typeTxn = TxnTypeConstant.getTxnType(txn.getTxnType());
		var txnTypeString = TxnTypeConstant.getTxnTypeForUI(txn.getTxnType());
		var name = txn.getNameRef().getFullName();
		var date = MyDate.getDate('d/m/y', txn.getTxnDate());
		var dueDate = MyDate.getDate('d/m/y', txn.getTxnDueDate());
		var receivedAmt = txn.getCashAmount() ? txn.getCashAmount() : 0;
		var balanceAmt = txn.getBalanceAmount() ? txn.getBalanceAmount() : 0;
		var txnStatus = txn.getStatus();
		var refNo = txn.getTxnRefNumber() ? txn.getFullTxnRefNumber() : '';
		var totalAmt = Number(receivedAmt) + Number(balanceAmt);
		finalTotalAmount += totalAmt;

		tempArray.push(date);
		if (isTransactionRefNumberEnabled) {
			tempArray.push(refNo);
		}
		(_tempArray = tempArray).push.apply(_tempArray, [name, dueDate, txnStatus == TransactionStatus.TXN_ORDER_COMPLETE ? StringConstant.orderCompletedString : StringConstant.orderOpen, txnTypeString, totalAmt, receivedAmt, balanceAmt]);
		if (printDescription) {
			tempArray.push(txn.getDescription());
		}

		rowObj.push(tempArray);
		tempArray = [];
	}
	rowObj.push([]);

	// add total ammount
	var totalAmoutRow = Array(9).fill('');
	totalAmoutRow[7] = 'TotalAmount';
	totalAmoutRow[9] = finalTotalAmount;
	rowObj.push(totalAmoutRow);

	rowObj.push([]);
	return rowObj;
};

function printExcel() {
	closeExcelOptions();
	var XLSX = require('xlsx');
	var dataArray = prepareObjectForExcel(listOfTransactions);
	var worksheet = XLSX.utils.aoa_to_sheet(dataArray);
	var ExcelExportHelper = require('./../UIControllers/ExcelExportHelper.js');
	var excelExportHelper = new ExcelExportHelper();
	var ExcelItemDetails = $('#ExcelExpenseItemDetails').is(':checked');

	var workbook = {
		SheetNames: [],
		Sheets: {}
	};

	var wsName = 'Order Transaction Report';
	var lenCols = 10;
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet;
	var wscolWidth = new Array(lenCols).fill({ wch: 30 });
	worksheet['!cols'] = wscolWidth;

	if (ExcelItemDetails) {
		var wsNameItem = 'Item Details';
		workbook.SheetNames[1] = wsNameItem;
		var itemArray = excelExportHelper.prepareItemObjectForExcel(listOfTransactions);
		var itemWorksheet = XLSX.utils.aoa_to_sheet(itemArray);
		workbook.Sheets[wsNameItem] = itemWorksheet;
		var wsItemcolWidth = [{ wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }];
		itemWorksheet['!cols'] = wsItemcolWidth;
	}

	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

/// //////////////////////////////////////////////////////////////

populateTable();