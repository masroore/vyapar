var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function Outbox($) {
	var SettingCache = require('./../Cache/SettingCache');
	var TokenForSync = require('./../Utilities/TokenForSync');
	var SyncHelper = require('./../Utilities/SyncHelper');
	var OutboxLogic = require('../BizLogic/MessageDraftLogic');

	var allMessages = [];
	var settingCache = void 0;
	var outboxController = {
		defaults: {
			$mountPoint: '#outboxContainer',
			messages: []
		},
		init: function init() {
			var defaults = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

			settingCache = new SettingCache();
			this.readToken();
			this.defaults = $.extend(this.defaults, defaults);
			this.defaults.$mountPoint = $(this.defaults.$mountPoint);
			this.defaults.$dialog = this.defaults.$mountPoint.find('#outboxDialog');
			var $mountPoint = this.defaults.$mountPoint;

			if (settingCache.getTransactionMessageEnabled() || settingCache.isOwnerTxnMsgEnabled()) {
				var outboxLogic = new OutboxLogic();
				outboxLogic.getAll().then(function (messages) {
					var total = messages.length;
					if (total) {
						$mountPoint.removeClass('hide');
					}
				});
			}
			// this.render();
			this.updateOutboxCounter();
			this.attachEvents();
		},
		render: function render() {
			var _this = this;

			return (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
				var $dialog, html;
				return _regenerator2.default.wrap(function _callee$(_context) {
					while (1) {
						switch (_context.prev = _context.next) {
							case 0:
								$dialog = _this.defaults.$dialog;
								_context.next = 3;
								return _this.getMainTemplate();

							case 3:
								html = _context.sent;

								$dialog.html(html);

							case 5:
							case 'end':
								return _context.stop();
						}
					}
				}, _callee, _this);
			}))();
		},
		attachEvents: function attachEvents() {
			var _this2 = this;

			this.defaults.$mountPoint.on('click', '#outboxIcon', this.openDialog.bind(this));

			this.defaults.$dialog.on('click', '#checkUncheckAll', this.toggleAll).on('click', '#btnDeleteSMS', this.deleteSMS.bind(this)).on('click', '#btnSendSMS', this.sendSMS.bind(this));

			$(document).on('outboxCounter:updated', function (e, total) {
				var _defaults = _this2.defaults,
				    $mountPoint = _defaults.$mountPoint,
				    $dialog = _defaults.$dialog;

				if (total == 0) {
					$mountPoint.addClass('hide');
					$dialog.dialog('close');
				} else {
					$mountPoint.removeClass('hide');
					$mountPoint.find('.messageCount').html(total);
				}
			});
			$(document).on('outboxCounter:refresh', function (e) {
				_this2.updateOutboxCounter();
			});
		},
		deleteSMS: function deleteSMS(e) {
			var _this3 = this;

			var checked = $('.messageRow :checked');
			if (!checked.length) {
				return false;
			}
			var outboxLogic = new OutboxLogic();
			$.each(checked, function () {
				var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(key, el) {
					var $el, messageKey;
					return _regenerator2.default.wrap(function _callee2$(_context2) {
						while (1) {
							switch (_context2.prev = _context2.next) {
								case 0:
									$el = $(el);
									messageKey = $el.data('message-key');
									_context2.next = 4;
									return outboxLogic.deleteById(messageKey);

								case 4:
									$('#message-row-' + messageKey).remove();

								case 5:
								case 'end':
									return _context2.stop();
							}
						}
					}, _callee2, _this3);
				}));

				return function (_x2, _x3) {
					return _ref.apply(this, arguments);
				};
			}());
		},
		sendSMS: function sendSMS(e) {
			var _this4 = this;

			return (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3() {
				var checked, send;
				return _regenerator2.default.wrap(function _callee3$(_context3) {
					while (1) {
						switch (_context3.prev = _context3.next) {
							case 0:
								checked = $('.messageRow :checked');

								if (checked.length) {
									_context3.next = 3;
									break;
								}

								return _context3.abrupt('return', false);

							case 3:
								send = function send() {
									var outboxLogic = new OutboxLogic();
									var total = checked.length;
									var totalSent = 0;
									$('#loader').show();
									$.each(checked, function (key, el) {
										var $el = $(el);
										var messageKey = $el.data('message-key');
										var msg = allMessages.find(function (message) {
											return message.id == $el.data('txn-id');
										});
										outboxLogic.sendSMS({
											id: msg.id,
											type: msg.type,
											msg: msg,
											callback: function callback() {
												var err = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
												var status = arguments[1];

												if (status) {
													$('#message-row-' + messageKey).remove();
													totalSent++;
												}
												total--;
												if (total <= 0) {
													$('#loader').hide();
													if (totalSent > 0) {
														ToastHelper.success(totalSent + ' SMS' + (totalSent > 1 ? 's have' : ' has') + ' been sent successfully!');
													}
												}
											}
										}, false, true);
									});
								};

								send();

							case 5:
							case 'end':
								return _context3.stop();
						}
					}
				}, _callee3, _this4);
			}))();
		},
		toggleAll: function toggleAll(e) {
			$('.messageRow [type="checkbox"]').prop('checked', e.target.checked);
		},
		openDialog: function openDialog() {
			this.render();
			var $dialog = this.defaults.$dialog;

			var width = $('#centreDiv').width() - $('#shortcuts').width() - 100 || 800;
			if (width < 800) {
				width = 800;
			}
			$dialog.dialog({
				width: width,
				appendTo: '#dynamicDiv',
				height: 645,
				modal: true,
				close: function close(e) {
					e.stopPropagation();
					$dialog.html('');
					$dialog.dialog('destroy');
					// delete require.cache[require.resolve("../UIControllers/SMSController")];
				}
			});
		},
		readToken: function readToken() {
			return TokenForSync.ifExistToken();
		},
		login: function login(success, fail) {
			SyncHelper.loginToGetAuthToken(success, fail);
		},
		updateOutboxCounter: function updateOutboxCounter() {
			var _this5 = this;

			var outboxLogic = new OutboxLogic();
			outboxLogic.getAll().then(function (messages) {
				var $mountPoint = _this5.defaults.$mountPoint;

				$mountPoint.find('.messageCount').html(messages.length);
				if (messages.length && (settingCache.getTransactionMessageEnabled() || settingCache.isOwnerTxnMsgEnabled())) {
					$mountPoint.removeClass('hide');
				} else {
					$mountPoint.addClass('hide');
				}
			});
		},
		getMainTemplate: function getMainTemplate() {
			var _this6 = this;

			return (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4() {
				return _regenerator2.default.wrap(function _callee4$(_context4) {
					while (1) {
						switch (_context4.prev = _context4.next) {
							case 0:
								_context4.next = 2;
								return _this6.getMessageRowHTML();

							case 2:
								_context4.t0 = _context4.sent;
								_context4.t1 = '\n        <div class="messagesContainer">\n            <div class="container">\n                <div class="row messageHeaderRow">\n                    <div class="col-2">\n                      <div class="md-checkbox">\n                        <input id="checkUncheckAll" type="checkbox">\n                        <label for="checkUncheckAll">&nbsp;</label>\n                      </div>\n                    </div>\n                    <div class="col-2">Party</div>\n                    <div class="col-5">Message</div>\n                </div>\n                ' + _context4.t0;
								return _context4.abrupt('return', _context4.t1 + '\n          </div>\n      </div>\n      <div class="container btnSendSeletedSMSContainer">\n        <div class="row">\n            <div class="col-4 legend-container">\n                <div class="legend-item green">Owner SMS</div>\n                <div class="legend-item blue">Party SMS</div>\n            </div>\n            <div class="col-8 textRight">\n                <button id="btnDeleteSMS" class="terminalButton">Delete</button>\n                <button id="btnSendSMS" class="terminalButton">Resend</button>\n            </div>\n        </div>\n      </div>\n      ');

							case 5:
							case 'end':
								return _context4.stop();
						}
					}
				}, _callee4, _this6);
			}))();
		},
		getMessageRowHTML: function getMessageRowHTML() {
			var _this7 = this;

			return (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee5() {
				var outboxLogic, messages, html, i, message, id, type, partyName, body, messageContent;
				return _regenerator2.default.wrap(function _callee5$(_context5) {
					while (1) {
						switch (_context5.prev = _context5.next) {
							case 0:
								outboxLogic = new OutboxLogic();
								_context5.next = 3;
								return outboxLogic.getAll();

							case 3:
								messages = _context5.sent;

								console.log(messages, 'all messages');
								allMessages = messages;
								html = '';

								for (i = 0; i < messages.length; i++) {
									message = messages[i];
									id = message.id, type = message.type, partyName = message.party_name;
									body = message.message;

									if (type === 'owner') {
										body = '\nName: ' + message.party_name + '\n' + message.message;
									}
									messageContent = message.header + ' ' + body + ' ' + message.footer;


									html += '\n            <div class="row messageRow" id="message-row-' + id + '">\n                <div class="col-2">\n                  <span class="messageLabel ' + (type === 'owner' ? 'green' : 'blue') + '" />\n                  <div class="md-checkbox">\n                    <input\n                        id="checkbox-' + id + '"\n                        data-txn-id="' + id + '"\n                        data-message-key="' + id + '"\n                        data-message-for="' + type + '"\n                        data-message="' + messageContent + '"\n                        type="checkbox">\n                    <label for="checkbox-' + id + '">&nbsp;</label>\n                  </div>\n                </div>\n                <div class="col-2">' + (type === 'owner' ? 'Owner' : partyName) + '</div>\n                <div class="col-8 message-col" title="Message To ' + (type === 'owner' ? 'Owner' : 'Party') + ': \n\n ' + messageContent + '">' + messageContent + '</div>\n            </div>\n\n            ';
								}
								return _context5.abrupt('return', html);

							case 9:
							case 'end':
								return _context5.stop();
						}
					}
				}, _callee5, _this7);
			}))();
		}
	};

	return outboxController.init.bind(outboxController);
}(jQuery);