var TaxCodeCache = require('./../Cache/TaxCodeCache.js');
var taxCodeCache = new TaxCodeCache();
var TaxCodeConstants = require('./../Constants/TaxCodeConstants.js');
var ITCTypeConstantsGSTR2 = require('./../Constants/ITCTypeConstantsGSTR2.js');
var TxnITCConstants = require('./../Constants/TxnITCConstants.js');
var ItemType = require('./../Constants/ItemType.js');
var NameCustomerType = require('./../Constants/NameCustomerType.js');

var GSTRReportHelper = {

	getPlaceOfSupply: function getPlaceOfSupply(name, transaction) {
		if (transaction && transaction.getPlaceOfSupply() && transaction.getPlaceOfSupply().trim()) {
			return transaction.getPlaceOfSupply();
		} else if (transaction.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || transaction.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
			var firmId = transaction.getFirmId();
			var firmState = firmCache.getFirmById(transaction.getFirmId()).getFirmState();
			return firmState;
		} else if (name) {
			return name.getContactState();
		} else {
			return '';
		}
	},

	isTaxOfTypeOthers: function isTaxOfTypeOthers(taxId) {
		taxId = Number(taxId);
		var taxCode = taxCodeCache.getTaxCodeObjectById(taxId);
		if (taxCode) {
			if (taxCode.getTaxType() == TaxCodeConstants.taxGroup) {
				return false;
			}
			if (taxCode.getTaxRateType() == TaxCodeConstants.OTHER) {
				return true;
			}
		}
		return false;
	},

	getITCTypeForGSTR2Report: function getITCTypeForGSTR2Report(itcType, item) {
		if (itcType == TxnITCConstants.ITC_INELIGIBLE_17_5 || itcType == TxnITCConstants.ITC_INELIGIBLE_OTHERS) {
			return ITCTypeConstantsGSTR2.INELIGIBLE;
		} else if (itcType == TxnITCConstants.ITC_ELIGIBLE_GOODS) {
			return ITCTypeConstantsGSTR2.CAPITAL_GOODS;
		} else if (item != null && item.getItemType() == ItemType.ITEM_TYPE_SERVICE) {
			return ITCTypeConstantsGSTR2.INPUT_SERVICES;
		} else {
			return ITCTypeConstantsGSTR2.INPUTS;
		}
	},

	getDateFormatForGSTRReport: function getDateFormatForGSTRReport(date) {
		var monthArray = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		var monthIndex = Number(MyDate.getDate('m', date));
		var dateField = MyDate.getDate('d', date);
		var monthField = monthArray[monthIndex - 1];
		var yearField = MyDate.getDate('y', date);
		var formatedDate = dateField + '-' + monthField + '-' + yearField;
		return formatedDate;
	},

	getDateFormatForGSTR4Report: function getDateFormatForGSTR4Report(date) {
		var monthArray = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
		var monthIndex = Number(MyDate.getDate('m', date));
		var dateField = MyDate.getDate('d', date);
		var monthField = monthArray[monthIndex - 1];
		var yearField = MyDate.getDate('y', date);
		var formatedDate = dateField + '-' + monthField + '-' + yearField;
		return formatedDate;
	},

	getDateFormatForGSTR9AReport: function getDateFormatForGSTR9AReport(date) {
		var monthArray = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
		var monthIndex = Number(MyDate.getDate('m', date));
		var dateField = MyDate.getDate('d', date);
		var monthField = monthArray[monthIndex - 1];
		var yearField = MyDate.getDate('y', date);
		var formatedDate = dateField + '-' + monthField + '-' + yearField;
		return formatedDate;
	},

	validateFlagForGSTR: function validateFlagForGSTR(transactionObj) {
		var customerType = transactionObj.getNameRef().getCustomerType();
		if ((transactionObj.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || transactionObj.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) && customerType == NameCustomerType.REGISTERED_COMPOSITE || (transactionObj.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || transactionObj.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN) && settingCache.isCompositeSchemeEnabled()) {
			return true;
		} else {
			return false;
		}
	}

};

module.exports = GSTRReportHelper;