/**
 * Created by Ashish on 10/11/2017.
 */
var TaxCodeCache = require('./../Cache/TaxCodeCache.js');
var taxCodeCache = new TaxCodeCache();
var TaxCodeConstants = require('./../Constants/TaxCodeConstants.js');
var GSTRReportHelper = require('./../UIControllers/GSTRReportHelper.js');

var GSTR3ReportHelper = function GSTR3ReportHelper() {
	this.isEntryIncorrect = false;
	this.isTaxAppliedOnTxn = true;
	this.isTaxUsedInAllLineItems = true;
	this.isTaxUsedInAnyLineItems = false;

	this.getTxnListBasedOnDate = function (fromDate, toDate, firmId, considerNonTaxAsExemptedCheck) {
		var GSTR3ReportObject = require('./../BizLogic/GSTR3ReportObject.js');
		var DataLoader = require('./../DBManager/DataLoader.js');
		var dataLoader = new DataLoader();
		var transactionList = dataLoader.loadTransactionsForGSTR3Report(fromDate, toDate, firmId);
		var gstr3ReportObjectList = [];
		var objectSparseArray = [];
		var taxableAmt;
		var name;

		if (transactionList != null) {
			for (var i = 0; i < transactionList.length; i++) {
				this.isTaxUsedInLineItem = false;
				this.isTaxUsedInTransaction = false;
				this.isTaxUsedInAllLineItems = true;
				this.isTaxUsedInAnyLineItems = false;

				var InvoiceValue = 0;
				var transaction = transactionList[i];
				name = transaction.getNameRef();
				objectSparseArray = [];

				var NonTaxFlag = considerNonTaxAsExemptedCheck;
				NonTaxFlag = GSTRReportHelper.validateFlagForGSTR(transaction) || NonTaxFlag; // Include NONE tax transactions for Composite User

				this.isTaxAppliedOnTxn = Number(transaction.getTransactionTaxId()) != 0 && !GSTRReportHelper.isTaxOfTypeOthers(transaction.getTransactionTaxId());

				for (var k = 0; k < transaction.getLineItems().length; k++) {
					var _lineItem = transaction.getLineItems()[k];
					var _lineTaxId = Number(_lineItem.getLineItemTaxId());
					this.isTaxUsedInAllLineItems = this.isTaxUsedInAllLineItems && (_lineTaxId != 0 && !GSTRReportHelper.isTaxOfTypeOthers(_lineTaxId) || Number(_lineItem.getLineItemAdditionalCESS()) != 0);
					this.isTaxUsedInAnyLineItems = this.isTaxUsedInAnyLineItems || _lineTaxId != 0 && !GSTRReportHelper.isTaxOfTypeOthers(_lineTaxId) || Number(_lineItem.getLineItemAdditionalCESS()) != 0;
				}

				for (var j = 0; j < transaction.getLineItems().length; j++) {
					var lineItem = transaction.getLineItems()[j];
					var lineTaxId = Number(lineItem.getLineItemTaxId());
					if (lineTaxId != 0 && !GSTRReportHelper.isTaxOfTypeOthers(lineTaxId) || lineItem.getLineItemAdditionalCESS() > 0 || !this.isTaxUsedInAllLineItems && this.isTaxUsedInAnyLineItems && NonTaxFlag && !GSTRReportHelper.isTaxOfTypeOthers(lineTaxId)) {
						InvoiceValue = transaction.getBalanceAmount() + transaction.getCashAmount() + transaction.getReverseChargeAmount();
						var reportObject;
						var itemObjectKey = lineTaxId + '' + '_' + lineItem.getLineItemITCValue();
						if (!objectSparseArray[itemObjectKey]) {
							reportObject = new GSTR3ReportObject();
							objectSparseArray[itemObjectKey] = reportObject;
							gstr3ReportObjectList.push(reportObject);
							if (name != null) {
								reportObject.setNameId(name.getNameId());
								reportObject.setGstinNo(name.getGstinNumber());
								reportObject.setPartyName(name.getFullName());
								reportObject.setCustomerType(name.getCustomerType());
							}
							reportObject.setTxnRefNumber(transaction.getTxnRefNumber());
							reportObject.setFirmId(transaction.getFirmId());
							reportObject.setInvoiceNo(transaction.getFullTxnRefNumber());
							reportObject.setInvoiceDate(transaction.getTxnDate());
							reportObject.setInvoiceValue(InvoiceValue);
							reportObject.setTransactionId(transaction.getTxnId());
							reportObject.setTransactionType(transaction.getTxnType());
							reportObject.setReverseCharge(Number(transaction.getReverseCharge()));
							reportObject.setTransactionDescription(transaction.getDescription());
							reportObject.setPlaceOfSupply(GSTRReportHelper.getPlaceOfSupply(name, transaction));
						} else {
							reportObject = objectSparseArray[itemObjectKey];
						}
						taxableAmt = lineItem.getItemQuantity() * lineItem.getItemUnitPrice() - lineItem.getLineItemDiscountAmount();
						reportObject.setInvoiceTaxableValue(taxableAmt + Number(reportObject.getInvoiceTaxableValue()));
						reportObject.setAdditionalCessAmt(reportObject.getAdditionalCessAmt() + Number(lineItem.getLineItemAdditionalCESS()));
						reportObject.setItcApplicable(lineItem.getLineItemITCValue());
						var lineItemTaxableAmount = 0;
						if (Number(lineItem.getLineItemAdditionalCESS()) > 0) {
							lineItemTaxableAmount = taxableAmt;
						}
						var taxCode = taxCodeCache.getTaxCodeObjectById(lineTaxId);
						if (lineTaxId != 0 && taxCode != null && !GSTRReportHelper.isTaxOfTypeOthers(lineTaxId)) {
							reportObject.setTaxRateId(taxCode.getTaxCodeId());
							lineItemTaxableAmount = taxableAmt;
							if (taxCode.getTaxType() == TaxCodeConstants.taxGroup) {
								reportObject.setRate(taxCode.getTaxRate());
								for (var p = 0; p < taxCode.getTaxCodeMap().length; p++) {
									var taxId = taxCode.getTaxCodeMap()[p];
									var code;
									if ((code = taxCodeCache.getTaxCodeObjectById(taxId)) != null) {
										this.setTaxEntries(reportObject, code, taxableAmt);
									}
								}
							} else {
								reportObject.setRate(taxCode.getTaxRate());
								this.setTaxEntries(reportObject, taxCode, taxableAmt);
							}
						}
						this.totalTaxableValue += lineItemTaxableAmount;
					}
				}
				var transcationTaxId = Number(transaction.getTransactionTaxId());
				if (transcationTaxId != 0 && !GSTRReportHelper.isTaxOfTypeOthers(transcationTaxId) || NonTaxFlag && !this.isTaxAppliedOnTxn && !GSTRReportHelper.isTaxOfTypeOthers(transcationTaxId) && !this.isTaxUsedInAnyLineItems) {
					InvoiceValue = transaction.getBalanceAmount() + transaction.getCashAmount() + transaction.getReverseChargeAmount();
					var object;
					var txnObjectKey = transcationTaxId + '' + '_' + transaction.getTxnITCApplicable();
					if (!objectSparseArray[txnObjectKey]) {
						object = new GSTR3ReportObject();
						objectSparseArray[txnObjectKey] = object;
						gstr3ReportObjectList.push(object);
						if (name != null) {
							object.setNameId(name.getNameId());
							object.setGstinNo(name.getGstinNumber());
							object.setPartyName(name.getFullName());
							object.setCustomerType(name.getCustomerType());
						}
						object.setTxnRefNumber(transaction.getTxnRefNumber());
						object.setFirmId(transaction.getFirmId());
						object.setInvoiceNo(transaction.getFullTxnRefNumber());
						object.setInvoiceDate(transaction.getTxnDate());
						object.setInvoiceValue(InvoiceValue);
						object.setTransactionId(transaction.getTxnId());
						object.setTransactionType(transaction.getTxnType());
						object.setReverseCharge(Number(transaction.getReverseCharge()));
						object.setTransactionDescription(transaction.getDescription());
						object.setPlaceOfSupply(GSTRReportHelper.getPlaceOfSupply(name, transaction));
					} else {
						object = objectSparseArray[txnObjectKey];
					}
					taxableAmt = transaction.getSubTotalAmount() - transaction.getDiscountAmount();
					object.setInvoiceTaxableValue(taxableAmt + Number(object.getInvoiceTaxableValue()));
					object.setItcApplicable(transaction.getTxnITCApplicable());
					var _taxCode = taxCodeCache.getTaxCodeObjectById(transcationTaxId);
					if (transcationTaxId != 0 && transcationTaxId != null && _taxCode != null) {
						this.isTaxUsedInTransaction = true;
						object.setTaxRateId(_taxCode.getTaxCodeId());
						this.totalTaxableValue += taxableAmt;
						if (_taxCode.getTaxType() == TaxCodeConstants.taxGroup) {
							object.setRate(_taxCode.getTaxRate());
							for (var p = 0; p < _taxCode.getTaxCodeMap().length; p++) {
								var taxId = _taxCode.getTaxCodeMap()[p];
								var code;
								if ((code = taxCodeCache.getTaxCodeObjectById(taxId)) != null) {
									this.setTaxEntries(object, code, taxableAmt);
								}
							}
						} else {
							object.setRate(_taxCode.getTaxRate());
							this.setTaxEntries(object, _taxCode, taxableAmt);
						}
					}
					object.setEntryIncorrect(this.isTaxUsedInLineItem && this.isTaxUsedInTransaction);
				}
			}
		}
		return gstr3ReportObjectList;
	};

	this.setTaxEntries = function (reportObject, taxCode, taxableAmt) {
		switch (taxCode.getTaxRateType()) {
			case TaxCodeConstants.IGST:
				reportObject.setIGSTAmt(taxableAmt * taxCode.getTaxRate() / 100 + reportObject.getIGSTAmt());
				break;
			case TaxCodeConstants.SGST:
				reportObject.setSGSTAmt(taxableAmt * taxCode.getTaxRate() / 100 + reportObject.getSGSTAmt());
				reportObject.setSGSTRate(taxCode.getTaxRate() + reportObject.getSGSTRate());
				break;
			case TaxCodeConstants.CGST:
				reportObject.setCGSTAmt(taxableAmt * taxCode.getTaxRate() / 100 + reportObject.getCGSTAmt());
				break;
			case TaxCodeConstants.CESS:
				reportObject.setCESSAmt(taxableAmt * taxCode.getTaxRate() / 100 + reportObject.getCESSAmt());
				reportObject.setCessRate(taxCode.getTaxRate());
				break;
			case TaxCodeConstants.STATE_SPECIFIC_CESS:
				reportObject.setStateSpecificCESSAmt(taxableAmt * taxCode.getTaxRate() / 100 + reportObject.getStateSpecificCESSAmt());
				reportObject.setStateSpecificCESSRate(taxCode.getTaxRate() + reportObject.getStateSpecificCESSRate());
				break;
			case TaxCodeConstants.OTHER:
				reportObject.setOTHERAmt(taxableAmt * taxCode.getTaxRate() / 100 + reportObject.getOTHERAmt());
				this.showOTHER = true;
				break;
		}
	};
};

module.exports = GSTR3ReportHelper;