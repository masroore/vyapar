Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.getBalanceSheetData = getBalanceSheetData;
var ProfitAndLossReportObject = require('./../BizLogic/ProfitAndLossReportObject.js');
var DataLoader = require('./../DBManager/DataLoader.js');

function getBalanceSheetData(startDate, endDate) {
	var dataloader = new DataLoader();
	var profitAndLossReportObject = new ProfitAndLossReportObject();
	var beginingDate = new Date();
	beginingDate.setFullYear(1900);
	// cash in hand amount
	var cashInHand = dataloader.getCashInHandAmount(endDate);
	// bank balance list
	var bankAccountBalanceList = dataloader.getBankBalanceForAllBanks(endDate);
	// bank balance list
	var loanAccountBalanceList = dataloader.getLoanAccountBalances({
		tillDate: endDate
	});

	// undeposited/ unwithdrawn cheque amount
	var listOfOpenChequesDetails = dataloader.getOpenChequeStatus(endDate);
	var undepositedChequeAmount = Number(listOfOpenChequesDetails[0][1]);
	var unwithdrawnChequeAmount = Number(listOfOpenChequesDetails[1][1]);

	// getclosed cheque data
	var closedChequeAmount = dataloader.getClosedCheques(endDate);

	// total receivable payable amount
	var nameBalanceMap = dataloader.getExpectedNameBalance(endDate);
	var payableAmount = 0;
	var receivableAmount = 0;
	nameBalanceMap.forEach(function (value, key) {
		if (Number(value) < 0) {
			payableAmount += Math.abs(Number(value));
		} else {
			receivableAmount += Number(value);
		}
	});
	// open order details
	var orderValues = dataloader.getAdvanceAmountForOpenOrder(endDate);
	var advanceForSaleOrder = orderValues.saleOrderAmount;
	var advanceForPurchaseOrder = orderValues.purchaseOrderAmount;

	// opening stock value and closing stock value
	var profitLosObj = profitAndLossReportObject.getProfitAndLossReportObject(beginingDate, endDate);
	var inventoryClosingStock = profitLosObj.getClosingStockValue();
	var inventoryOpeningStock = profitLosObj.getOpeningStockValue();

	// opening balance equity (opening bank + opening cash in + opening party balance + opening stock value)
	var openingBalnanceEquityObj = dataloader.getOpeningBalancePartyCashAndBank(null, endDate);

	var openingCashInHandAmount = openingBalnanceEquityObj.openingCashInHandAmount;
	var openingBankBalance = openingBalnanceEquityObj.openingBankBalance;
	var openingPartyBalance = openingBalnanceEquityObj.openingPartyBalance;
	var openingLoanBalanceEquity = Number(dataloader.getOpeningLoanBalanceAfterCloseBook(null, endDate)) || 0;

	var openingBalnanceEquity = Number(openingCashInHandAmount) + Number(openingBankBalance) + Number(openingPartyBalance) + Number(inventoryOpeningStock) + Number(closedChequeAmount) - Number(openingLoanBalanceEquity);

	// retained earnings
	var retainedEarningEndDate = new Date(startDate);
	retainedEarningEndDate.setDate(retainedEarningEndDate.getDate() - 1);

	var retainedEarningObj = profitAndLossReportObject.getProfitAndLossReportObject(beginingDate, retainedEarningEndDate);
	var retainedEarningAmount = retainedEarningObj.getNetProfitAndLossAmount();
	// tax paybale
	var taxReceivable = profitLosObj.getReceivableTax();
	var taxPayable = profitLosObj.getPayableTax();

	// net profit
	var profitAmount = Number(profitLosObj.getNetProfitAndLossAmount()) - Number(retainedEarningAmount);
	// owners equity (increase cash , decrease cash, bank adjustments)
	var ownersEquityObj = dataloader.getAddReduceAmountForCashAndBank(null, endDate);

	var addCashAmount = ownersEquityObj.addCashAmount;
	var reduceCashAmount = ownersEquityObj.reduceCashAmount;
	var addBankAmount = ownersEquityObj.addBankAmount;
	var reduceBankAmount = ownersEquityObj.reduceBankAmount;

	var ownersEquity = Number(addCashAmount) + Number(reduceCashAmount) + Number(addBankAmount) + Number(reduceBankAmount);

	return {
		cashInHand: cashInHand,
		bankAccountBalanceList: bankAccountBalanceList,
		loanAccountBalanceList: loanAccountBalanceList,
		undepositedChequeAmount: undepositedChequeAmount,
		receivableAmount: receivableAmount,
		payableAmount: payableAmount,
		profitAmount: profitAmount,
		inventoryClosingStock: inventoryClosingStock,
		ownersEquity: ownersEquity,
		openingBalnanceEquity: openingBalnanceEquity,
		retainedEarningAmount: retainedEarningAmount,
		taxReceivable: taxReceivable,
		taxPayable: taxPayable,
		unwithdrawnChequeAmount: unwithdrawnChequeAmount,
		advanceForSaleOrder: advanceForSaleOrder,
		advanceForPurchaseOrder: advanceForPurchaseOrder,
		openingCashInHandAmount: openingCashInHandAmount,
		openingBankBalance: openingBankBalance,
		openingPartyBalance: openingPartyBalance,
		inventoryOpeningStock: inventoryOpeningStock,
		closedChequeAmount: closedChequeAmount,
		openingLoanBalanceEquity: openingLoanBalanceEquity,
		addCashAmount: addCashAmount,
		reduceCashAmount: reduceCashAmount,
		addBankAmount: addBankAmount,
		reduceBankAmount: reduceBankAmount
	};
}