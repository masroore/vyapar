var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var fs = require('fs');
var listToAppend = $('ul#ulCompany');
var path = require('path');

var app = require('electron').remote.app;

var appPath = app.getPath('userData');

var isCompanyListView = true;
var DbVersionConstant = require('../Constants/DBVersionConstant');
var APP_DB_VERSION = DbVersionConstant.DB_VERSION;

require('ui-contextmenu');

var CompanyListUtility = require('./../Utilities/CompanyListUtility.js');
var companyObjectList = CompanyListUtility.getCompaniesFileNameObject();

if (companyObjectList) {
	listToAppend.addClass('floatLeft');
	for (var companyDisplayName in companyObjectList) {
		var companyType = companyObjectList[companyDisplayName].company_creation_type;

		if (companyType == 0) {
			listToAppend.append("<li  style='font-family:roboto;' class='x halfDiv hasmenu openCompany' >" + "<div id='' class='companynotsync floatLeft'><svg  fill=\"#808080\" height=\"24\" viewBox=\"0 0 24 24\" width=\"24\" xmlns=\"http://www.w3.org/2000/svg\">\n" + '    <path d="M0 0h24v24H0zm0 0h24v24H0z" fill="none"/>\n' + '    <path d="M10 6.35V4.26c-.8.21-1.55.54-2.23.96l1.46 1.46c.25-.12.5-.24.77-.33zm-7.14-.94l2.36 2.36C4.45 8.99 4 10.44 4 12c0 2.21.91 4.2 2.36 5.64L4 20h6v-6l-2.24 2.24C6.68 15.15 6 13.66 6 12c0-1 .25-1.94.68-2.77l8.08 8.08c-.25.13-.5.25-.77.34v2.09c.8-.21 1.55-.54 2.23-.96l2.36 2.36 1.27-1.27L4.14 4.14 2.86 5.41zM20 4h-6v6l2.24-2.24C17.32 8.85 18 10.34 18 12c0 1-.25 1.94-.68 2.77l1.46 1.46C19.55 15.01 20 13.56 20 12c0-2.21-.91-4.2-2.36-5.64L20 4z"/>\n' + '</svg></div>' + "<span class='businessKey' style='font-size:14px;'>" + companyDisplayName + "</span><span style='font-size:12px;' class='deleteCompany floatRight'><svg fill='#777' height='18' viewBox='0 0 24 24' width='18' xmlns='http://www.w3.org/2000/svg'><path d='M0 0h24v24H0z' fill='none'/><path d='M12 8c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0 2c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z'/></svg></span><br><span class='path'>" + companyObjectList[companyDisplayName].path + '</span></li>');
		} else {
			listToAppend.append("<li style='font-family:roboto' class='x halfDiv hasmenu openCompany' >" + "<div id='' class='companynotsync floatLeft'><svg fill=\"#097aa8\" height=\"24\" viewBox=\"0 0 24 24\" width=\"24\" xmlns=\"http://www.w3.org/2000/svg\">\n" + '    <path d="M12 4V1L8 5l4 4V6c3.31 0 6 2.69 6 6 0 1.01-.25 1.97-.7 2.8l1.46 1.46C19.54 15.03 20 13.57 20 12c0-4.42-3.58-8-8-8zm0 14c-3.31 0-6-2.69-6-6 0-1.01.25-1.97.7-2.8L5.24 7.74C4.46 8.97 4 10.43 4 12c0 4.42 3.58 8 8 8v3l4-4-4-4v3z"/>\n' + '    <path d="M0 0h24v24H0z" fill="none"/>\n' + '</svg></div>' + "<span class='businessKey' style='font-size:14px;'>" + companyDisplayName + "</span><span style='font-size:12px;' class='deleteCompany floatRight'><svg fill='#777' height='18' viewBox='0 0 24 24' width='18' xmlns='http://www.w3.org/2000/svg'><path d='M0 0h24v24H0z' fill='none'/><path d='M12 8c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0 2c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z'/></svg></span><br><span class='path'>" + companyObjectList[companyDisplayName].path + '</span></li>');
		}
	}
}

$('.listOfCompany').append(listToAppend);

var _require = require('electron'),
    ipcRenderer = _require.ipcRenderer;
// function for browse button


function browseDBFileFunction() {
	var ChangeDB = require('./../BizLogic/ChangeDB.js');
	var changeDB = new ChangeDB();
	changeDB.changeDatabase();
	ipcRenderer.send('changeURL', 'Index1.html');
}
var MyAnalytics = require('../Utilities/analyticsHelper.js');

$('#browseCompany').on('click', browseDBFileFunction);
// end of function for browse buton

// function for adding new company
function addNewCompanyFunc() {
	MyAnalytics.pushEvent('Create Company Open');
	ipcRenderer.send('changeURL', 'UserLogIn.html');
}

$('#joinComp').on('click', function () {
	joinCompany('listOfJoinCompany ul');
});

var validatePassword = function validatePassword(that) {
	if (that.value == '9931') {
		$('#enableJoinCheck').closest('td').show().focus();
	} else {
		$('#enableJoinCheck').closest('td').hide();
	}
};

function showJoinButton(that) {
	debugger;
	if ($(that).prop('checked')) {
		$('#joinComp').removeClass('hide');
		$('#joinComp').css('display', 'block');
		localStorage.isSyncSettingVisible = true;
	} else {
		$('#joinComp').addClass('hide');
		$('#joinComp').css('display', 'none');
		localStorage.isSyncSettingVisible = false;
	}
}

if (localStorage.isSyncSettingVisible == 'true') {
	var TokenForSync = require('./../Utilities/TokenForSync');

	if (!TokenForSync.ifExistToken()) {
		$('#enableJoinCheck').prop('checked', true);
		$('#joinComp').removeClass('hide');
		$('#joinComp').css('display', 'block');
	}
} else {
	$('#enableJoinCheck').prop('checked', false);
	$('#joinComp').addClass('hide');
	$('#joinComp').css('display', 'none');
}

function closeEnableJoinCompanyDialog() {
	$('#enableJoinCompanyDialog').css('display', 'none');
	$('#enableJoinCompanyDialog').dialog('close').dialog('destroy');
}

function openEnableJoinCompanyDialog() {
	$('#enableJoinCompanyDialog').css('display', 'block');
	$('#enableJoinCompanyDialog').dialog({
		'width': 500,
		'height': 220,
		'title': 'Enable Sync & Join Company',
		appendTo: '#switchCompanyMainDiv',
		modal: true
	});
}

var isOnline = require('is-online');

isOnline().then(function (online) {
	if (online) {
		var TokenForSync = require('./../Utilities/TokenForSync');

		if (TokenForSync.ifExistToken()) {
			$('#joinComp').addClass('hide');

			joinCompany('listOfCompany ul', false);
		} else {
			// Todo:For now we are keeping Join company button hidden. Will be shown in later release.
			// $('#joinComp').removeClass("hide");
		}
	} else {
			// $('#joinComp').hide();

		}
});

$('#newCompany').on('click', addNewCompanyFunc);
// end of funtion to add new company

// function to restore db
function restoreDBFunction() {
	var ImportDB = require('./../BizLogic/ImportDB.js');
	var importDB = new ImportDB();
	importDB.ImportDatabase();
}

$('#restoreDB').on('click', restoreDBFunction);

// switching company while clicking on list
function openThisDBFunction(that) {
	debugger;
	var pathVar = $(that).find('.path').text().trim();
	var fs = require('fs');
	if (fs.existsSync(pathVar)) {
		var op = fs.writeFileSync(appPath + '/BusinessNames/currentDB.txt', pathVar);
		ipcRenderer.send('changeURL', 'Index1.html');
	} else if (fs.existsSync(__dirname + pathVar)) {
		var op = fs.writeFileSync(appPath + '/BusinessNames/currentDB.txt', __dirname + pathVar);
		ipcRenderer.send('changeURL', 'Index1.html');
	} else {
		$(this).closest('.hasmenu').remove();
		alert('Sorry the file name does not exist. Please select another file');
	}
}

function deleteCompanyFunc(that) {
	$('#deleteModal').css('display', 'block');
	$('#deleteModal').dialog({
		'width': 500,
		'title': 'Delete Company',
		appendTo: '#listOfCompany',
		modal: true
	});

	var deleteComapanyPromise = new _promise2.default(function (resolve, reject) {
		$('#deleteModal button').on('click', function () {
			if (this.id == 'confirmDeleteCompany') {
				resolve(true);
			} else if (this.id == 'cancelDeleteCompany') {
				resolve(false);
			}
			$('#deleteModal').dialog('close');
		});
	});

	deleteComapanyPromise.then(function (confirmValue) {
		if (confirmValue) {
			var pathVar = $(that).find('.path').text().trim();
			var businessKey = $(that).find('.businessKey').text().trim();

			if (fs.existsSync(pathVar)) {
				var pathToDel;
				if (pathVar.startsWith('./..')) {
					pathToDel = path.normalize(__dirname + pathVar);
				} else {
					pathToDel = path.normalize(pathVar);
				}
				fs.unlinkSync(pathToDel);
			}

			var currentDBFile = appPath + '/BusinessNames/currentDB.txt';
			if (fs.existsSync(currentDBFile)) {
				var currentDB = fs.readFileSync(currentDBFile);
				// var dbToDeletedbToDelete = pathVar.split('/');
				var dbToDelete = businessKey;
				if (currentDB.indexOf(dbToDelete) >= 0) {
					fs.writeFileSync(currentDBFile, '');
				}
			}

			var fileNameObj = CompanyListUtility.getCompaniesFileNameObject();

			if (typeof fileNameObj[businessKey] == 'undefined') {
				businessKey = businessKey + ' ';
			}

			delete fileNameObj[businessKey];
			CompanyListUtility.updateCompanyListFile(fileNameObj);
			$(that).closest('.hasmenu').remove();
			require('idb').deleteDB(businessKey).then(function () {
				return console.log('draft messages deleted for ', businessKey);
			});
			return false;
		} else {
			return false;
		}
	}).catch(function (error) {});
}

$('#listOfCompany').on('click', '.openCompany', function () {
	openThisDBFunction(this);
});

$('#listOfCompany').on('click', '.deleteCompany', function () {
	$(this).trigger('contextmenu');
	return false;
});

function assignContextMenu() {
	$('#listOfCompany').contextmenu({
		delegate: '.hasmenu',
		addClass: 'width10 contextMenuZIndex',
		menu: [{ title: 'Open Company', cmd: 'open' }, { title: '----' }, { title: 'Delete Company', cmd: 'delete' }],
		select: function select(event, ui) {
			var currentComp = ui.target[0].closest('li');
			if (ui.cmd == 'delete') {
				deleteCompanyFunc(currentComp);
			} else if (ui.cmd == 'open') {
				openThisDBFunction(currentComp);
			}
		}
	});
}

assignContextMenu();

function searchCompanies() {
	var input, filter, ul, li, i;
	input = document.getElementById('searchCompanyInput2');
	filter = input.value.toUpperCase();
	ul = $('ul'); // [0];

	for (var item = 1; item < ul.length; item++) {
		ul1 = ul[item];
		li = ul1.getElementsByTagName('li');
		for (i = 0; i < li.length; i++) {
			if (li[i].innerHTML.toUpperCase().indexOf(filter) < 0) {
				$(li[i]).css('display', 'none');
			} else {
				$(li[i]).css('display', 'block');
			}
		}
	}

	assignContextMenu();
}

$('#searchCompany2').on('click', function () {
	$('#searchCompany2').css('transform', 'rotateY(180deg)');
	$('#searchCompany2').css('left', '15px');
	if ($('#searchCompanyInput2').css('display') == 'none') {
		$('#searchCompanyInput2').removeClass('hide');
	} else {
		$('#searchCompany2').css('transform', 'rotateY(0deg)');
		$('#searchCompany2').css('left', '250px');
		$('#searchCompanyInput2').addClass('hide');
	}
});
$('#searchCompanyInput2').on('keyup', searchCompanies);

$('#refreshBut').click(function () {
	var _require2 = require('electron'),
	    ipcRenderer = _require2.ipcRenderer;

	ipcRenderer.send('changeURL', 'SwitchCompany.html');
});