var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by Ashish on 10/11/2017.
 */
var startDate;
var endDate;
var DateFormat = require('./../Constants/DateFormat.js');
var NameCache = require('./../Cache/NameCache.js');
var nameCache = new NameCache();
var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();
var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var NameCustomerType = require('./../Constants/NameCustomerType.js');
var TxnITCConstants = require('./../Constants/TxnITCConstants.js');
var GSTR3ReportHelper = require('./../UIControllers/GSTR3ReportHelper.js');
var gSTR3ReportHelper = new GSTR3ReportHelper();
var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var TaxCodeCache = require('./../Cache/TaxCodeCache.js');
var taxCodeCache = new TaxCodeCache();
var TaxCodeConstants = require('./../Constants/TaxCodeConstants.js');
var FirmCache = require('./../Cache/FirmCache.js');
var firmCache = new FirmCache();
var GSTRReportHelper = require('./../UIControllers/GSTRReportHelper.js');
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');

var listOfTransactions = [];

// for 3.1

var GSTR_3_B_data = {
	totalTaxableValueOutwardTaxableSuppliesotherThanZNE: 0,
	IGSTOutwardTaxableSuppliesotherThanZNE: 0,
	CGSTOutwardTaxableSuppliesotherThanZNE: 0,
	SGSTOutwardTaxableSuppliesotherThanZNE: 0,
	CESSOutwardTaxableSuppliesotherThanZNE: 0,
	totalTaxableValueOtherOutwardSuppliesNE: 0,
	IGSTOtherOutwardSuppliesNE: 0,
	CGSTOtherOutwardSuppliesNE: 0,
	SGSTOtherOutwardSuppliesNE: 0,
	CESSOtherOutwardSuppliesNE: 0,
	totalTaxableValueInwardSuppliesRC: 0,
	IGSTInwardSuppliesRC: 0,
	CGSTInwardSuppliesRC: 0,
	SGSTInwardSuppliesRC: 0,
	CESSInwardSuppliesRC: 0,

	// for 3.2
	interStateSupplyMap: new _map2.default(),

	// for 3
	IGSTInwardSuppliesRCITC: 0,
	CGSTInwardSuppliesRCITC: 0,
	SGSTInwardSuppliesRCITC: 0,
	CESSInwardSuppliesRCITC: 0,
	IGSTinwardSuppliesAllOtherITC: 0,
	CGSTinwardSuppliesAllOtherITC: 0,
	SGSTinwardSuppliesAllOtherITC: 0,
	CESSinwardSuppliesAllOtherITC: 0,
	IGSTineligibleITC175: 0,
	CGSTineligibleITC175: 0,
	SGSTineligibleITC175: 0,
	CESSineligibleITC175: 0,
	IGSTineligibleITCothers: 0,
	CGSTineligibleITCothers: 0,
	SGSTineligibleITCothers: 0,
	CESSineligibleITCothers: 0,

	// for 4
	interStateCompositiontaxableValue: 0,
	intraStateCompositiontaxableValue: 0,
	start_date: new Date()
};

$('.monthYearPicker').datepicker({
	changeMonth: true,
	changeYear: true,
	showButtonPanel: true,
	dateFormat: 'mm/yy'
}).focus(function () {
	var thisCalendar = $(this);
	$('.ui-datepicker-calendar').detach();
	$('.ui-datepicker-close').click(function () {
		var month = $('#ui-datepicker-div .ui-datepicker-month :selected').val();
		var year = $('#ui-datepicker-div .ui-datepicker-year :selected').val();
		thisCalendar.datepicker('setDate', new Date(year, month));
		populateTable();
	});
});

var populateTable = function populateTable() {
	nameCache = new NameCache();
	taxCodeCache = new TaxCodeCache();
	settingCache = new SettingCache();
	// for 3.1

	var startDateString = $('#startDate').val();
	startDateString = '01/' + startDateString;
	startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
	endDate = new Date(startDate.getFullYear(), startDate.getMonth() + 1, 0);
	var considerNonTaxAsExemptedCheck = $('#considerNonTaxAsExempted').prop('checked');
	var firmId = Number($('#firmFilterOptions').val());
	var partyFilterId = Number($('#transactionFilterOptions').val());
	CommonUtility.showLoader(function () {
		GSTR_3_B_data.totalTaxableValueOutwardTaxableSuppliesotherThanZNE = 0;
		GSTR_3_B_data.IGSTOutwardTaxableSuppliesotherThanZNE = 0;
		GSTR_3_B_data.CGSTOutwardTaxableSuppliesotherThanZNE = 0;
		GSTR_3_B_data.SGSTOutwardTaxableSuppliesotherThanZNE = 0;
		GSTR_3_B_data.CESSOutwardTaxableSuppliesotherThanZNE = 0;
		GSTR_3_B_data.totalTaxableValueOtherOutwardSuppliesNE = 0;
		GSTR_3_B_data.IGSTOtherOutwardSuppliesNE = 0;
		GSTR_3_B_data.CGSTOtherOutwardSuppliesNE = 0;
		GSTR_3_B_data.SGSTOtherOutwardSuppliesNE = 0;
		GSTR_3_B_data.CESSOtherOutwardSuppliesNE = 0;
		GSTR_3_B_data.totalTaxableValueInwardSuppliesRC = 0;
		GSTR_3_B_data.IGSTInwardSuppliesRC = 0;
		GSTR_3_B_data.CGSTInwardSuppliesRC = 0;
		GSTR_3_B_data.SGSTInwardSuppliesRC = 0;
		GSTR_3_B_data.CESSInwardSuppliesRC = 0;

		// for 3.2
		GSTR_3_B_data.interStateSupplyMap = new _map2.default();

		GSTR_3_B_data.stateSpecificCESSMap = new _map2.default();

		// for 3
		GSTR_3_B_data.IGSTInwardSuppliesRCITC = 0;
		GSTR_3_B_data.CGSTInwardSuppliesRCITC = 0;
		GSTR_3_B_data.SGSTInwardSuppliesRCITC = 0;
		GSTR_3_B_data.CESSInwardSuppliesRCITC = 0;
		GSTR_3_B_data.IGSTinwardSuppliesAllOtherITC = 0;
		GSTR_3_B_data.CGSTinwardSuppliesAllOtherITC = 0;
		GSTR_3_B_data.SGSTinwardSuppliesAllOtherITC = 0;
		GSTR_3_B_data.CESSinwardSuppliesAllOtherITC = 0;
		GSTR_3_B_data.IGSTineligibleITC175 = 0;
		GSTR_3_B_data.CGSTineligibleITC175 = 0;
		GSTR_3_B_data.SGSTineligibleITC175 = 0;
		GSTR_3_B_data.CESSineligibleITC175 = 0;
		GSTR_3_B_data.IGSTineligibleITCothers = 0;
		GSTR_3_B_data.CGSTineligibleITCothers = 0;
		GSTR_3_B_data.SGSTineligibleITCothers = 0;
		GSTR_3_B_data.CESSineligibleITCothers = 0;

		// for 4
		GSTR_3_B_data.interStateCompositiontaxableValue = 0;
		GSTR_3_B_data.intraStateCompositiontaxableValue = 0;

		GSTR_3_B_data.start_date = startDate;

		listOfTransactions = gSTR3ReportHelper.getTxnListBasedOnDate(startDate, endDate, firmId, considerNonTaxAsExemptedCheck);
		displayTransactionList(listOfTransactions);
	});
};

var date = MyDate.getDate('m/y');
$('#startDate').val(date);
$('#endDate').val(date);

if (settingCache.getMultipleFirmEnabled()) {
	var optionTemp = document.createElement('option');
	var firmList = firmCache.getFirmList();
	for (i in firmList) {
		var option = optionTemp.cloneNode();
		option.text = firmList[i].getFirmName();
		option.value = firmList[i].getFirmId();
		$('#firmFilterOptions').append(option);
	}
	$('#firmChooser').show();
} else {
	$('#firmChooser').hide();
}

$('#firmFilterOptions').on('selectmenuchange', function (event) {
	populateTable();
});

$('#startDate').change(function () {
	populateTable();
});

$('#endDate').change(function () {
	populateTable();
});

$('#considerNonTaxAsExempted').change(function () {
	populateTable();
});

var displayTransactionList = function displayTransactionList(listOfTransactions) {
	for (var i = 0; i < listOfTransactions.length; i++) {
		var txn = listOfTransactions[i];

		var taxRateId = MyDouble.convertStringToDouble(txn.getTaxRateId(), true);
		var taxCode = taxRateId ? taxCodeCache.getTaxCodeObjectById(taxRateId) : null;
		// 3.1
		if (taxRateId && taxCode.getTaxRate() != 0 && taxCode.getTaxRateType() != TaxCodeConstants.Exempted) {
			if (txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_SALE) {
				GSTR_3_B_data.totalTaxableValueOutwardTaxableSuppliesotherThanZNE += txn.getInvoiceTaxableValue();
				GSTR_3_B_data.IGSTOutwardTaxableSuppliesotherThanZNE += txn.getIGSTAmt();
				GSTR_3_B_data.CGSTOutwardTaxableSuppliesotherThanZNE += txn.getCGSTAmt();
				GSTR_3_B_data.SGSTOutwardTaxableSuppliesotherThanZNE += txn.getSGSTAmt();
				GSTR_3_B_data.CESSOutwardTaxableSuppliesotherThanZNE += txn.getCESSAmt() + txn.getAdditionalCessAmt();
			} else if (txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
				GSTR_3_B_data.totalTaxableValueOutwardTaxableSuppliesotherThanZNE -= txn.getInvoiceTaxableValue();
				GSTR_3_B_data.IGSTOutwardTaxableSuppliesotherThanZNE -= txn.getIGSTAmt();
				GSTR_3_B_data.CGSTOutwardTaxableSuppliesotherThanZNE -= txn.getCGSTAmt();
				GSTR_3_B_data.SGSTOutwardTaxableSuppliesotherThanZNE -= txn.getSGSTAmt();
				GSTR_3_B_data.CESSOutwardTaxableSuppliesotherThanZNE -= txn.getCESSAmt() + txn.getAdditionalCessAmt();
			}
		} else if (!txn.getTaxRateId() || taxRateId && (taxCode.getTaxRate() == 0 || taxCode.getTaxRateType() == TaxCodeConstants.Exempted)) {
			// !txn.getTaxRateId() this check for considering NONE as EXEMPTED

			if (txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_SALE) {
				GSTR_3_B_data.totalTaxableValueOtherOutwardSuppliesNE += txn.getInvoiceTaxableValue();
				GSTR_3_B_data.IGSTOtherOutwardSuppliesNE += txn.getIGSTAmt();
				GSTR_3_B_data.CGSTOtherOutwardSuppliesNE += txn.getCGSTAmt();
				GSTR_3_B_data.SGSTOtherOutwardSuppliesNE += txn.getSGSTAmt();
				GSTR_3_B_data.CESSOtherOutwardSuppliesNE += txn.getCESSAmt() + txn.getAdditionalCessAmt();
			} else if (txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
				GSTR_3_B_data.totalTaxableValueOtherOutwardSuppliesNE -= txn.getInvoiceTaxableValue();
				GSTR_3_B_data.IGSTOtherOutwardSuppliesNE -= txn.getIGSTAmt();
				GSTR_3_B_data.CGSTOtherOutwardSuppliesNE -= txn.getCGSTAmt();
				GSTR_3_B_data.SGSTOtherOutwardSuppliesNE -= txn.getSGSTAmt();
				GSTR_3_B_data.CESSOtherOutwardSuppliesNE -= txn.getCESSAmt() + txn.getAdditionalCessAmt();
			}
		}

		if (txn.getReverseCharge() == 1) {
			if (txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_PURCHASE) {
				GSTR_3_B_data.totalTaxableValueInwardSuppliesRC += txn.getInvoiceTaxableValue();
				GSTR_3_B_data.IGSTInwardSuppliesRC += txn.getIGSTAmt();
				GSTR_3_B_data.CGSTInwardSuppliesRC += txn.getCGSTAmt();
				GSTR_3_B_data.SGSTInwardSuppliesRC += txn.getSGSTAmt();
				GSTR_3_B_data.CESSInwardSuppliesRC += txn.getCESSAmt() + txn.getAdditionalCessAmt();
			} else if (txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
				GSTR_3_B_data.totalTaxableValueInwardSuppliesRC -= txn.getInvoiceTaxableValue();
				GSTR_3_B_data.IGSTInwardSuppliesRC -= txn.getIGSTAmt();
				GSTR_3_B_data.CGSTInwardSuppliesRC -= txn.getCGSTAmt();
				GSTR_3_B_data.SGSTInwardSuppliesRC -= txn.getSGSTAmt();
				GSTR_3_B_data.CESSInwardSuppliesRC -= txn.getCESSAmt() + txn.getAdditionalCessAmt();
			}
		}

		// 3.2
		var firmId = txn.getFirmId();
		var firm = firmCache.getFirmById(firmId);
		var firmState = firm.getFirmState() ? firm.getFirmState().trim() : '';
		var txnPlaceOfSupply = txn.getPlaceOfSupply() ? txn.getPlaceOfSupply().trim() : '';

		if (firmState && txnPlaceOfSupply && firmState != txnPlaceOfSupply && taxRateId && taxCode.getTaxRateType() == TaxCodeConstants.IGST) {
			var totalTaxableValueUnregistered;
			var totalIGSTtaxAmountUnregistered;
			var totalTaxableValueComposite;
			var totalIGSTtaxAmountComposite;
			if (txn.getGstinNo() == null || txn.getGstinNo().trim() == '') {
				if (txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_SALE) {
					if (!GSTR_3_B_data.interStateSupplyMap.has(txnPlaceOfSupply)) {
						totalTaxableValueUnregistered = txn.getInvoiceTaxableValue();
						totalIGSTtaxAmountUnregistered = txn.getIGSTAmt();
						GSTR_3_B_data.interStateSupplyMap.set(txnPlaceOfSupply, { 'unregistered': [totalTaxableValueUnregistered, totalIGSTtaxAmountUnregistered], 'composite': [0, 0] });
					} else {
						totalTaxableValueUnregistered = txn.getInvoiceTaxableValue() + GSTR_3_B_data.interStateSupplyMap.get(txnPlaceOfSupply)['unregistered'][0];
						totalIGSTtaxAmountUnregistered = txn.getIGSTAmt() + GSTR_3_B_data.interStateSupplyMap.get(txnPlaceOfSupply)['unregistered'][1];
						totalTaxableValueComposite = GSTR_3_B_data.interStateSupplyMap.get(txnPlaceOfSupply)['composite'][0];
						totalIGSTtaxAmountComposite = GSTR_3_B_data.interStateSupplyMap.get(txnPlaceOfSupply)['composite'][1];
						GSTR_3_B_data.interStateSupplyMap.set(txnPlaceOfSupply, { 'unregistered': [totalTaxableValueUnregistered, totalIGSTtaxAmountUnregistered], 'composite': [totalTaxableValueComposite, totalIGSTtaxAmountComposite] });
					}
				} else if (txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
					if (!GSTR_3_B_data.interStateSupplyMap.has(txnPlaceOfSupply)) {
						totalTaxableValueUnregistered = 0 - txn.getInvoiceTaxableValue();
						totalIGSTtaxAmountUnregistered = 0 - txn.getIGSTAmt();
						GSTR_3_B_data.interStateSupplyMap.set(txnPlaceOfSupply, { 'unregistered': [totalTaxableValueUnregistered, totalIGSTtaxAmountUnregistered], 'composite': [0, 0] });
					} else {
						totalTaxableValueUnregistered = GSTR_3_B_data.interStateSupplyMap.get(txnPlaceOfSupply)['unregistered'][0] - txn.getInvoiceTaxableValue();
						totalIGSTtaxAmountUnregistered = GSTR_3_B_data.interStateSupplyMap.get(txnPlaceOfSupply)['unregistered'][1] - txn.getIGSTAmt();
						totalTaxableValueComposite = GSTR_3_B_data.interStateSupplyMap.get(txnPlaceOfSupply)['composite'][0];
						totalIGSTtaxAmountComposite = GSTR_3_B_data.interStateSupplyMap.get(txnPlaceOfSupply)['composite'][1];
						GSTR_3_B_data.interStateSupplyMap.set(txnPlaceOfSupply, { 'unregistered': [totalTaxableValueUnregistered, totalIGSTtaxAmountUnregistered], 'composite': [totalTaxableValueComposite, totalIGSTtaxAmountComposite] });
					}
				}
			} else if (txn.getCustomerType() == NameCustomerType.REGISTERED_COMPOSITE) {
				if (txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_SALE) {
					if (!GSTR_3_B_data.interStateSupplyMap.has(txnPlaceOfSupply)) {
						totalTaxableValueComposite = txn.getInvoiceTaxableValue();
						totalIGSTtaxAmountComposite = txn.getIGSTAmt();
						GSTR_3_B_data.interStateSupplyMap.set(txnPlaceOfSupply, { 'unregistered': [0, 0], 'composite': [totalTaxableValueComposite, totalIGSTtaxAmountComposite] });
					} else {
						totalTaxableValueComposite = txn.getInvoiceTaxableValue() + GSTR_3_B_data.interStateSupplyMap.get(txnPlaceOfSupply)['composite'][0];
						totalIGSTtaxAmountComposite = txn.getIGSTAmt() + GSTR_3_B_data.interStateSupplyMap.get(txnPlaceOfSupply)['composite'][1];
						totalTaxableValueUnregistered = GSTR_3_B_data.interStateSupplyMap.get(txnPlaceOfSupply)['unregistered'][0];
						totalIGSTtaxAmountUnregistered = GSTR_3_B_data.interStateSupplyMap.get(txnPlaceOfSupply)['unregistered'][1];
						GSTR_3_B_data.interStateSupplyMap.set(txnPlaceOfSupply, { 'unregistered': [totalTaxableValueUnregistered, totalIGSTtaxAmountUnregistered], 'composite': [totalTaxableValueComposite, totalIGSTtaxAmountComposite] });
					}
				} else if (txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
					if (!GSTR_3_B_data.interStateSupplyMap.has(txnPlaceOfSupply)) {
						totalTaxableValueComposite = 0 - txn.getInvoiceTaxableValue();
						totalIGSTtaxAmountComposite = 0 - txn.getIGSTAmt();
						GSTR_3_B_data.interStateSupplyMap.set(txnPlaceOfSupply, { 'unregistered': [0, 0], 'composite': [totalTaxableValueComposite, totalIGSTtaxAmountComposite] });
					} else {
						totalTaxableValueComposite = GSTR_3_B_data.interStateSupplyMap.get(txnPlaceOfSupply)['composite'][0] - txn.getInvoiceTaxableValue();
						totalIGSTtaxAmountComposite = GSTR_3_B_data.interStateSupplyMap.get(txnPlaceOfSupply)['composite'][1] - txn.getIGSTAmt();
						totalTaxableValueUnregistered = GSTR_3_B_data.interStateSupplyMap.get(txnPlaceOfSupply)['unregistered'][0];
						totalIGSTtaxAmountUnregistered = GSTR_3_B_data.interStateSupplyMap.get(txnPlaceOfSupply)['unregistered'][1];
						GSTR_3_B_data.interStateSupplyMap.set(txnPlaceOfSupply, { 'unregistered': [totalTaxableValueUnregistered, totalIGSTtaxAmountUnregistered], 'composite': [totalTaxableValueComposite, totalIGSTtaxAmountComposite] });
					}
				}
			}
		}

		// 4
		var partyObject = nameCache.findNameObjectByNameId(txn.getNameId());
		var partyState = partyObject.getContactState();
		var customerType = partyObject.getCustomerType();

		if (txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_PURCHASE) {
			if (partyState && txnPlaceOfSupply && partyState != txnPlaceOfSupply) {
				// inter state
				if (customerType == NameCustomerType.REGISTERED_COMPOSITE) {
					GSTR_3_B_data.interStateCompositiontaxableValue += txn.getTotalAmount();
				} else if (taxCode == null || taxCode.getTaxRateType() == TaxCodeConstants.Exempted) {
					GSTR_3_B_data.interStateCompositiontaxableValue += txn.getInvoiceTaxableValue();
				} else if (taxCode != null && taxCode.getTaxRate() == 0) {
					GSTR_3_B_data.interStateCompositiontaxableValue += txn.getInvoiceTaxableValue();
				}
			} else {
				if (customerType == NameCustomerType.REGISTERED_COMPOSITE) {
					/// /intra state
					GSTR_3_B_data.intraStateCompositiontaxableValue += txn.getTotalAmount();
				} else if (taxCode == null || taxCode.getTaxRateType() == TaxCodeConstants.Exempted) {
					// exempted
					GSTR_3_B_data.intraStateCompositiontaxableValue += txn.getInvoiceTaxableValue();
				} else if (taxCode != null && taxCode.getTaxRate() == 0) {
					// nil rated
					GSTR_3_B_data.intraStateCompositiontaxableValue += txn.getInvoiceTaxableValue();
				}
			}
		} else if (txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
			if (partyState && txnPlaceOfSupply && partyState != txnPlaceOfSupply) {
				if (customerType == NameCustomerType.REGISTERED_COMPOSITE) {
					/// /inter
					GSTR_3_B_data.interStateCompositiontaxableValue -= txn.getTotalAmount();
				} else if (taxCode == null || taxCode.getTaxRateType() == TaxCodeConstants.Exempted) {
					GSTR_3_B_data.interStateCompositiontaxableValue -= txn.getInvoiceTaxableValue();
				} else if (taxCode != null && taxCode.getTaxRate() == 0) {
					GSTR_3_B_data.interStateCompositiontaxableValue -= txn.getInvoiceTaxableValue();
				}
			} else {
				if (customerType == NameCustomerType.REGISTERED_COMPOSITE) {
					/// /intra state
					GSTR_3_B_data.intraStateCompositiontaxableValue -= txn.getTotalAmount();
				} else if (taxCode == null || taxCode.getTaxRateType() == TaxCodeConstants.Exempted) {
					// exempted
					GSTR_3_B_data.intraStateCompositiontaxableValue -= txn.getInvoiceTaxableValue();
				} else if (taxCode != null && taxCode.getTaxRate() == 0) {
					// nil rated
					GSTR_3_B_data.intraStateCompositiontaxableValue -= txn.getInvoiceTaxableValue();
				}
			}
		}

		// 3
		if (txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_PURCHASE) {
			if (taxRateId && taxCode.getTaxRateType() != TaxCodeConstants.Exempted) {
				if (txn.getItcApplicable() == TxnITCConstants.ITC_ELIGIBLE || txn.getItcApplicable() == TxnITCConstants.ITC_ELIGIBLE_GOODS) {
					if (txn.getReverseCharge() == 1) {
						GSTR_3_B_data.IGSTInwardSuppliesRCITC += txn.getIGSTAmt();
						GSTR_3_B_data.CGSTInwardSuppliesRCITC += txn.getCGSTAmt();
						GSTR_3_B_data.SGSTInwardSuppliesRCITC += txn.getSGSTAmt();
						GSTR_3_B_data.CESSInwardSuppliesRCITC += txn.getCESSAmt() + txn.getAdditionalCessAmt();
					} else {
						GSTR_3_B_data.IGSTinwardSuppliesAllOtherITC += txn.getIGSTAmt();
						GSTR_3_B_data.CGSTinwardSuppliesAllOtherITC += txn.getCGSTAmt();
						GSTR_3_B_data.SGSTinwardSuppliesAllOtherITC += txn.getSGSTAmt();
						GSTR_3_B_data.CESSinwardSuppliesAllOtherITC += txn.getCESSAmt() + txn.getAdditionalCessAmt();
					}
				} else if (txn.getItcApplicable() == TxnITCConstants.ITC_INELIGIBLE_17_5) {
					GSTR_3_B_data.IGSTineligibleITC175 += txn.getIGSTAmt();
					GSTR_3_B_data.CGSTineligibleITC175 += txn.getCGSTAmt();
					GSTR_3_B_data.SGSTineligibleITC175 += txn.getSGSTAmt();
					GSTR_3_B_data.CESSineligibleITC175 += txn.getCESSAmt() + txn.getAdditionalCessAmt();
				} else if (txn.getItcApplicable() == TxnITCConstants.ITC_INELIGIBLE_OTHERS) {
					GSTR_3_B_data.IGSTineligibleITCothers += txn.getIGSTAmt();
					GSTR_3_B_data.CGSTineligibleITCothers += txn.getCGSTAmt();
					GSTR_3_B_data.SGSTineligibleITCothers += txn.getSGSTAmt();
					GSTR_3_B_data.CESSineligibleITCothers += txn.getCESSAmt() + txn.getAdditionalCessAmt();
				}
			}
		} else if (txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
			if (taxRateId && taxCode.getTaxRateType() != TaxCodeConstants.Exempted) {
				if (txn.getItcApplicable() == TxnITCConstants.ITC_ELIGIBLE || txn.getItcApplicable() == TxnITCConstants.ITC_ELIGIBLE_GOODS) {
					if (txn.getReverseCharge() == 1) {
						GSTR_3_B_data.IGSTInwardSuppliesRCITC -= txn.getIGSTAmt();
						GSTR_3_B_data.CGSTInwardSuppliesRCITC -= txn.getCGSTAmt();
						GSTR_3_B_data.SGSTInwardSuppliesRCITC -= txn.getSGSTAmt();
						GSTR_3_B_data.CESSInwardSuppliesRCITC -= txn.getCESSAmt() + txn.getAdditionalCessAmt();
					} else {
						GSTR_3_B_data.IGSTinwardSuppliesAllOtherITC -= txn.getIGSTAmt();
						GSTR_3_B_data.CGSTinwardSuppliesAllOtherITC -= txn.getCGSTAmt();
						GSTR_3_B_data.SGSTinwardSuppliesAllOtherITC -= txn.getSGSTAmt();
						GSTR_3_B_data.CESSinwardSuppliesAllOtherITC -= txn.getCESSAmt() + txn.getAdditionalCessAmt();
					}
				} else if (txn.getItcApplicable() == TxnITCConstants.ITC_INELIGIBLE_17_5) {
					GSTR_3_B_data.IGSTineligibleITC175 -= txn.getIGSTAmt();
					GSTR_3_B_data.CGSTineligibleITC175 -= txn.getCGSTAmt();
					GSTR_3_B_data.SGSTineligibleITC175 -= txn.getSGSTAmt();
					GSTR_3_B_data.CESSineligibleITC175 -= txn.getCESSAmt() + txn.getAdditionalCessAmt();
				} else if (txn.getItcApplicable() == TxnITCConstants.ITC_INELIGIBLE_OTHERS) {
					GSTR_3_B_data.IGSTineligibleITCothers -= txn.getIGSTAmt();
					GSTR_3_B_data.CGSTineligibleITCothers -= txn.getCGSTAmt();
					GSTR_3_B_data.SGSTineligibleITCothers -= txn.getSGSTAmt();
					GSTR_3_B_data.CESSineligibleITCothers -= txn.getCESSAmt() + txn.getAdditionalCessAmt();
				}
			}
		}

		// For Kerala flood cess or state specific CESS
		if (taxRateId && txn.getStateSpecificCESSAmt() > 0 && (txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN)) {
			// Intra state supply
			if (!firmState || !txnPlaceOfSupply || firmState == txnPlaceOfSupply) {
				var stateSpecificCESSRate = txn.getStateSpecificCESSRate();
				var stateCESSMap = GSTR_3_B_data.stateSpecificCESSMap.get(stateSpecificCESSRate);
				if (!stateCESSMap) {
					stateCESSMap = new _map2.default();
					GSTR_3_B_data.stateSpecificCESSMap.set(stateSpecificCESSRate, stateCESSMap);
				}
				var sgstRate = txn.getSGSTRate();
				var sgstArrayForStateCESS = stateCESSMap.get(sgstRate);
				if (!sgstArrayForStateCESS) {
					sgstArrayForStateCESS = [0, 0, 0];
					stateCESSMap.set(sgstRate, sgstArrayForStateCESS);
				}
				if (txn.getTransactionType() == TxnTypeConstant.TXN_TYPE_SALE) {
					sgstArrayForStateCESS[2] += txn.getStateSpecificCESSAmt();
					// For taxable parties
					if (customerType == NameCustomerType.REGISTERED_COMPOSITE || customerType == NameCustomerType.REGISTERED_NORMAL) {
						sgstArrayForStateCESS[0] += txn.getInvoiceTaxableValue();
					} else {
						// For unregistered parties
						sgstArrayForStateCESS[1] += txn.getInvoiceTaxableValue();
					}
				} else {
					sgstArrayForStateCESS[2] -= txn.getStateSpecificCESSAmt();
					// For taxable parties
					if (customerType == NameCustomerType.REGISTERED_COMPOSITE || customerType == NameCustomerType.REGISTERED_NORMAL) {
						sgstArrayForStateCESS[0] -= txn.getInvoiceTaxableValue();
					} else {
						// For unregistered parties
						sgstArrayForStateCESS[1] -= txn.getInvoiceTaxableValue();
					}
				}
			}
		}
	}

	var row3_1desc = '1. Details of outward supplies and inward supplies liable to reverse charge';
	var row3_1 = '';
	row3_1 += "<thead><tr style='background-color: lightgrey;'><th width='40%'>Nature of supplies</th><th width='10%' class='tableCellTextAlignRight'>Total taxable value</th><th width='12%' class='tableCellTextAlignRight'>Integrated Tax</th><th width='12%' class='tableCellTextAlignRight'>Central Tax</th><th width='12%' class='tableCellTextAlignRight'>State/UT Tax</th><th width='12%' class='tableCellTextAlignRight'>Cess</th></tr></thead>";
	row3_1 += '<tbody>';
	row3_1 += "<tr><td>Outward taxable supplies (other than zero rated, nil rated and exempted)</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.totalTaxableValueOutwardTaxableSuppliesotherThanZNE) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.IGSTOutwardTaxableSuppliesotherThanZNE) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CGSTOutwardTaxableSuppliesotherThanZNE) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.SGSTOutwardTaxableSuppliesotherThanZNE) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CESSOutwardTaxableSuppliesotherThanZNE) + '</td></tr>';
	row3_1 += "<tr><td>Outward taxable supplies (zero rated)</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td></tr>";
	row3_1 += "<tr><td> Other outward supplies (nil rated, exempted)</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.totalTaxableValueOtherOutwardSuppliesNE) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.IGSTOtherOutwardSuppliesNE) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CGSTOtherOutwardSuppliesNE) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.SGSTOtherOutwardSuppliesNE) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CESSOtherOutwardSuppliesNE) + '</td></tr>';
	row3_1 += "<tr><td>Inward supplies (liable to reverse charge)</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.totalTaxableValueInwardSuppliesRC) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.IGSTInwardSuppliesRC) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CGSTInwardSuppliesRC) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.SGSTInwardSuppliesRC) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CESSInwardSuppliesRC) + '</td></tr>';
	row3_1 += "<tr><td>Non-GST outward supplies</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td></tr>";
	row3_1 += '</tbody>';

	var row3_2desc = '2. Details of inter-State supplies made to unregistered persons, composition dealer and UIN holders';
	var row3_2 = '';
	row3_2 += "<thead><tr style='background-color: lightgrey;'><th width='20%' rowspan='2'>Place of supply (State/UT)</th><th colspan='2' width='30%' class='tableCellTextAlignRight'>Supplies made to unregistered persons</th><th colspan='2' width='30%' class='tableCellTextAlignRight'>Supplies made to composition taxable persons</th><th colspan='2' width='20%' class='tableCellTextAlignRight'>Supplies made to UIN holders</th></tr>";
	row3_2 += "<tr style='background-color: lightgrey;'><th width='15%' class='tableCellTextAlignRight'>Total taxable value</th><th width='15%' class='tableCellTextAlignRight'>Amount of integrated tax</th><th width='15%' class='tableCellTextAlignRight'>Total taxable value</th><th width='15%' class='tableCellTextAlignRight'>Amount of integrated tax</th><th width='10%' class='tableCellTextAlignRight'>Total taxable value</th><th width='10%' class='tableCellTextAlignRight'>Amount of integrated tax</th></tr></thead>";
	row3_2 += '<tbody>';
	if (GSTR_3_B_data.interStateSupplyMap.size == 0) {
		row3_2 += "<tr><td></td><td class='tableCellTextAlignRight'></td><td class='tableCellTextAlignRight'></td><td class='tableCellTextAlignRight'></td><td class='tableCellTextAlignRight'></td><td class='tableCellTextAlignRight'></td><td class='tableCellTextAlignRight'></td></tr>";
	} else {
		var _iteratorNormalCompletion = true;
		var _didIteratorError = false;
		var _iteratorError = undefined;

		try {
			for (var _iterator = (0, _getIterator3.default)(GSTR_3_B_data.interStateSupplyMap.entries()), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
				var value = _step.value;

				var POS = value[0];
				var taxableValueUnregistered = value[1]['unregistered'][0];
				var IGSTAmountUnregistered = value[1]['unregistered'][1];
				var taxableValueComposite = value[1]['composite'][0];
				var IGSTAmountComposite = value[1]['composite'][1];
				row3_2 += '<tr><td>' + POS + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(taxableValueUnregistered) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(IGSTAmountUnregistered) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(taxableValueComposite) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(IGSTAmountComposite) + "</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td></tr>";
			}
		} catch (err) {
			_didIteratorError = true;
			_iteratorError = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion && _iterator.return) {
					_iterator.return();
				}
			} finally {
				if (_didIteratorError) {
					throw _iteratorError;
				}
			}
		}
	}

	row3_2 += '</tbody>';

	var row3desc = '3. Details of eligible Input Tax Credit';
	var row3 = '';
	row3 += "<thead><tr style='background-color: lightgrey;'><th width='40%'>Details</th><th width='15%' class='tableCellTextAlignRight'>Integrated Tax</th><th width='15%' class='tableCellTextAlignRight'>Central Tax</th><th width='15%' class='tableCellTextAlignRight'>State/UT Tax</th><th width='15%' class='tableCellTextAlignRight'>Cess</th></tr></thead>";
	row3 += '<tbody>';
	row3 += "<tr><td style='font-weight: bold;'>(A) ITC Available (whether in full or part)</td></tr>";
	row3 += "<tr><td style='padding-left: 20px;'>(1) Import of goods</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td></tr>";
	row3 += "<tr><td style='padding-left: 20px;'>(2) Import of services</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td></tr>";
	row3 += "<tr><td style='padding-left: 20px;'>(3) Inward supplies liable to reverse charge (other than 1 & 2 above)</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.IGSTInwardSuppliesRCITC) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CGSTInwardSuppliesRCITC) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.SGSTInwardSuppliesRCITC) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CESSInwardSuppliesRCITC) + '</td></tr>';
	row3 += "<tr><td style='padding-left: 20px;'>(4) Inward supplies from ISD</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td></tr>";
	row3 += "<tr><td style='padding-left: 20px;'>(5) All other ITC</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.IGSTinwardSuppliesAllOtherITC) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CGSTinwardSuppliesAllOtherITC) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.SGSTinwardSuppliesAllOtherITC) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CESSinwardSuppliesAllOtherITC) + '</td></tr>';
	row3 += "<tr><td style='font-weight: bold;'>(D) Ineleigible ITC</td></tr>";
	row3 += "<tr><td style='padding-left: 20px;'>(1) As per section 17(5)</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.IGSTineligibleITC175) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CGSTineligibleITC175) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.SGSTineligibleITC175) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CESSineligibleITC175) + '</td></tr>';
	row3 += "<tr><td style='padding-left: 20px;'>(2) Others</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.IGSTineligibleITCothers) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CGSTineligibleITCothers) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.SGSTineligibleITCothers) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CESSineligibleITCothers) + '</td></tr>';
	row3 += '</tbody>';

	var row4desc = '4. Details of exempt, nil-rated and non-GST inward supplies';
	var row4 = '';
	row4 += "<thead><tr style='background-color: lightgrey;'><th width='50%'>Nature of supplies</th><th width='25%' class='tableCellTextAlignRight'>Inter-State supplies</th><th width='25%' class='tableCellTextAlignRight'>Intra-State supplies</th></tr></thead>";
	row4 += '<tbody>';
	row4 += "<tr><td>From a supplier under composition scheme, Exempt and Nil rated supply </td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.interStateCompositiontaxableValue) + "</td><td class='tableCellTextAlignRight'>" + MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.intraStateCompositiontaxableValue) + '</td></tr>';
	row4 += "<tr><td>Non GST supply</td><td class='tableCellTextAlignRight'>0</td><td class='tableCellTextAlignRight'>0</td></tr>";
	row4 += '</tbody>';

	if (GSTR_3_B_data.stateSpecificCESSMap && GSTR_3_B_data.stateSpecificCESSMap.size > 0) {
		$('#stateSpecificCESSElement').show();
		var row5desc = 'PARTICULARS OF Flood CESS PAYABLE';
		var row5 = '';
		var columnStyle = 'borderBottomForTxn';
		row5 += "<thead><tr style='background-color: lightgrey;'> <th rowspan='2' class='" + columnStyle + "' align='center' width='7%'>SI No.</th>" + "<th rowspan='2' class='" + columnStyle + "' align='center' width='20%'>Category of supply</th>" + "<th colspan='3' class='" + columnStyle + "' width='45%' align='center'>Value of intra-state supply</th>" + "<th rowspan='2' class='" + columnStyle + "' align='center' width='18%'>Rate of Flood CESS on value of supply</th>" + "<th rowspan='2' class='" + columnStyle + "' width='30%' align='center'>Flood CESS Payable</th></tr>" + "<tr style='background-color: lightgrey'>" + "<th  class='" + columnStyle + "' width='15%' align='center'>To taxable person having GST registration in the State, not in furtherance of business</th>" + "<th  class='" + columnStyle + "' width='15%' align='center'>To Unregistered person</th>" + "<th  class='" + columnStyle + "' width='15%' align='center'>Total</th></tr></thead><tbody>";

		var siNo = 0;
		var _iteratorNormalCompletion2 = true;
		var _didIteratorError2 = false;
		var _iteratorError2 = undefined;

		try {
			for (var _iterator2 = (0, _getIterator3.default)(GSTR_3_B_data.stateSpecificCESSMap.entries()), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
				var stateCESSRateEntry = _step2.value;

				var stateCESSRate = stateCESSRateEntry[0];
				var sgstMap = stateCESSRateEntry[1];
				var _iteratorNormalCompletion3 = true;
				var _didIteratorError3 = false;
				var _iteratorError3 = undefined;

				try {
					for (var _iterator3 = (0, _getIterator3.default)(sgstMap.entries()), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var sgstRateEntry = _step3.value;

						siNo++;
						var _sgstRate = sgstRateEntry[0];
						var stateCESSValues = sgstRateEntry[1];
						row5 += "<tr><td class='" + columnStyle + "'> " + siNo + '</td>' + "<td class='" + columnStyle + "'> Taxable supply at the rate of " + MyDouble.getAmountDecimalForGSTRReport(_sgstRate) + '% SGST</td>' + "<td class='" + columnStyle + "' align='right'>" + MyDouble.getAmountDecimalForGSTRReport(stateCESSValues[0]) + '</td>' + "<td class='" + columnStyle + "' align='right'>" + MyDouble.getAmountDecimalForGSTRReport(stateCESSValues[1]) + '</td>' + "<td class='" + columnStyle + "' align='right'>" + MyDouble.getAmountDecimalForGSTRReport(stateCESSValues[0] + stateCESSValues[1]) + '</td>' + "<td class='" + columnStyle + "' align='right'>" + MyDouble.getAmountDecimalForGSTRReport(stateCESSRate) + '</td>' + "<td class='" + columnStyle + "' align='right'>" + MyDouble.getAmountDecimalForGSTRReport(stateCESSValues[2]) + '</td></tr>';
					}
				} catch (err) {
					_didIteratorError3 = true;
					_iteratorError3 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError3) {
							throw _iteratorError3;
						}
					}
				}
			}
		} catch (err) {
			_didIteratorError2 = true;
			_iteratorError2 = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion2 && _iterator2.return) {
					_iterator2.return();
				}
			} finally {
				if (_didIteratorError2) {
					throw _iteratorError2;
				}
			}
		}

		row5 += '</tbody>';
		$('#table5description').html(row5desc);
		$('#table5').html(row5);
	} else {
		$('#stateSpecificCESSElement').hide();
	}

	$('#table3_1description').html(row3_1desc);
	$('#table3_1').html(row3_1);
	$('#table3_2description').html(row3_2desc);
	$('#table3_2').html(row3_2);
	$('#table3description').html(row3desc);
	$('#table3').html(row3);
	$('#table4description').html(row4desc);
	$('#table4').html(row4);
};

var getHTMLTextForReport = function getHTMLTextForReport() {
	var GSTR3BReportHTMLGenerator = require('./../ReportHTMLGenerator/GSTR3BReportHTMLGenerator.js');
	var gSTR3BReportHTMLGenerator = new GSTR3BReportHTMLGenerator();
	var htmlText = gSTR3BReportHTMLGenerator.getHTMLText(GSTR_3_B_data);
	return htmlText;
};

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	// $('#frameDiv').load('');
	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable();
};

/// ///////////////////////////PDF/////////////////////////////////////
var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.GSTR3_REPORT, fromDate: '01/' + $('#startDate').val(), format: 'MM_YY' });
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.GSTR3_REPORT, fromDate: '01/' + $('#startDate').val(), format: 'MM_YY' });
	excelHelper.saveExcel(fileName);
};

var getReportDataFor3 = function getReportDataFor3() {
	var rowObj = [];

	rowObj.push(['Details', 'Integrated Tax', 'Central Tax', 'State/UT Tax', 'Cess']);
	rowObj.push(['', '', '', '', '']);
	rowObj.push(['(A) ITC Available (Whether in full or part)', '', '', '', '']);
	rowObj.push(['(1) Import of goods', 0, 0, 0, 0]);
	rowObj.push(['(2) Import of services', 0, 0, 0, 0]);
	rowObj.push(['(3) Inward supplies liable to reverse charge( other than 1 & 2 above)', MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.IGSTInwardSuppliesRCITC), MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CGSTInwardSuppliesRCITC), MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.SGSTInwardSuppliesRCITC), MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CESSInwardSuppliesRCITC)]);
	rowObj.push(['(4) Inward supplies from ISD', 0, 0, 0, 0]);
	rowObj.push(['(5) All other ITC', MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.IGSTinwardSuppliesAllOtherITC), MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CGSTinwardSuppliesAllOtherITC), MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.SGSTinwardSuppliesAllOtherITC), MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CESSinwardSuppliesAllOtherITC)]);
	rowObj.push(['(D) Ineligible ITC', '', '', '', '']);
	rowObj.push(['(1) As per section 17(5)', MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.IGSTineligibleITC175), MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CGSTineligibleITC175), MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.SGSTineligibleITC175), MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CESSineligibleITC175)]);
	rowObj.push(['(2) Others', MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.IGSTineligibleITCothers), MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CGSTineligibleITCothers), MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.SGSTineligibleITCothers), MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CESSineligibleITCothers)]);
	return rowObj;
};

var getReportDataFor4 = function getReportDataFor4() {
	var rowObj = [];
	rowObj.push(['Nature of Supplies', 'Inter-State supplies', 'Intra-State supplies']);
	rowObj.push(['', '', '']);
	rowObj.push(['From a supplier under composition scheme, Exempt and Nil rated supply', MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.interStateCompositiontaxableValue), MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.intraStateCompositiontaxableValue)]);
	rowObj.push(['Non GST supply', 0, 0]);
	return rowObj;
};

var getReportDataFor3_2 = function getReportDataFor3_2() {
	var rowObj = [];
	rowObj.push(['Place of Supply (State/UT)', 'Supplies Made To Unregistered Persons', '', 'Supplies Made To Composition Taxable Persons', '', 'Supplies Made To UIN Holders', '']);
	rowObj.push(['', '', '', '', '', '']);
	rowObj.push(['', 'Total taxable value', 'Amount of integrated tax', 'Total taxable value', 'Amount of integrated tax', 'Total taxable value', 'Amount of integrated tax']);

	var _iteratorNormalCompletion4 = true;
	var _didIteratorError4 = false;
	var _iteratorError4 = undefined;

	try {
		for (var _iterator4 = (0, _getIterator3.default)(GSTR_3_B_data.interStateSupplyMap.entries()), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
			var value = _step4.value;

			var POS = value[0];
			var taxableValueUnregistered = value[1]['unregistered'][0];
			var IGSTAmountUnregistered = value[1]['unregistered'][1];
			var taxableValueComposite = value[1]['composite'][0];
			var IGSTAmountComposite = value[1]['composite'][1];
			rowObj.push([POS, taxableValueUnregistered, IGSTAmountUnregistered, taxableValueComposite, IGSTAmountComposite, 0, 0]);
		}
	} catch (err) {
		_didIteratorError4 = true;
		_iteratorError4 = err;
	} finally {
		try {
			if (!_iteratorNormalCompletion4 && _iterator4.return) {
				_iterator4.return();
			}
		} finally {
			if (_didIteratorError4) {
				throw _iteratorError4;
			}
		}
	}

	return rowObj;
};

var getReportDataForStateSpecificCESS = function getReportDataForStateSpecificCESS() {
	if (GSTR_3_B_data.stateSpecificCESSMap && GSTR_3_B_data.stateSpecificCESSMap.size > 0) {
		var rowObj = [];
		rowObj.push(['SI No.', 'Category of supply', 'Value of intra-state supply', '', '', 'Rate of Flood CESS on value of supply', 'Flood CESS Payable']);
		rowObj.push(['', '', 'To taxable person having GST registration in the State, not in furtherance of business', 'To Unregistered person', 'Total', '', '']);
		var siNo = 0;
		var _iteratorNormalCompletion5 = true;
		var _didIteratorError5 = false;
		var _iteratorError5 = undefined;

		try {
			for (var _iterator5 = (0, _getIterator3.default)(GSTR_3_B_data.stateSpecificCESSMap.entries()), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
				var stateCESSRateEntry = _step5.value;

				var stateCESSRate = stateCESSRateEntry[0];
				var sgstMap = stateCESSRateEntry[1];
				var _iteratorNormalCompletion6 = true;
				var _didIteratorError6 = false;
				var _iteratorError6 = undefined;

				try {
					for (var _iterator6 = (0, _getIterator3.default)(sgstMap.entries()), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
						var sgstRateEntry = _step6.value;

						siNo++;
						var sgstRate = sgstRateEntry[0];
						var stateCESSValues = sgstRateEntry[1];
						rowObj.push([siNo, 'Taxable supply at the rate of ' + MyDouble.getAmountDecimalForGSTRReport(sgstRate) + '% SGST', MyDouble.getAmountDecimalForGSTRReport(stateCESSValues[0]), MyDouble.getAmountDecimalForGSTRReport(stateCESSValues[1]), MyDouble.getAmountDecimalForGSTRReport(stateCESSValues[0] + stateCESSValues[1]), MyDouble.getAmountDecimalForGSTRReport(stateCESSRate), MyDouble.getAmountDecimalForGSTRReport(stateCESSValues[2])]);
					}
				} catch (err) {
					_didIteratorError6 = true;
					_iteratorError6 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion6 && _iterator6.return) {
							_iterator6.return();
						}
					} finally {
						if (_didIteratorError6) {
							throw _iteratorError6;
						}
					}
				}
			}
		} catch (err) {
			_didIteratorError5 = true;
			_iteratorError5 = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion5 && _iterator5.return) {
					_iterator5.return();
				}
			} finally {
				if (_didIteratorError5) {
					throw _iteratorError5;
				}
			}
		}

		return rowObj;
	}
	return null;
};

var getReportDataFor3_1 = function getReportDataFor3_1() {
	var dateObj = GSTR_3_B_data.start_date;
	var month = MyDate.getMonthName(dateObj);
	var year = dateObj.getFullYear();
	var rowObj = [];
	rowObj.push(['Year', year]);
	rowObj.push(['Month', month]);
	rowObj.push(['Nature of Supplies', 'Total Taxable Value', 'Integrated Tax', 'Central Tax', 'State/UT Tax', 'Cess']);
	rowObj.push(['', '', '', '', '', '']);
	rowObj.push(['(a) Outward taxable supplies (other than zero rated, nil rated and exempted', MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.totalTaxableValueOutwardTaxableSuppliesotherThanZNE), MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.IGSTOutwardTaxableSuppliesotherThanZNE), MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CGSTOutwardTaxableSuppliesotherThanZNE), MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.SGSTOutwardTaxableSuppliesotherThanZNE), MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CESSOutwardTaxableSuppliesotherThanZNE)]);
	rowObj.push(['(b) Outward taxable supplies (zero rated)', 0.0, 0.0, 0.0, 0.0, 0.0]);
	rowObj.push(['(c) Other outward supplies (nil rated, exempted)', MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.totalTaxableValueOtherOutwardSuppliesNE), MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.IGSTOtherOutwardSuppliesNE), MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CGSTOtherOutwardSuppliesNE), MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.SGSTOtherOutwardSuppliesNE), MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CESSOtherOutwardSuppliesNE)]);
	rowObj.push(['(d) Inward supplies (liable to reverse charge)', MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.totalTaxableValueInwardSuppliesRC), MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.IGSTInwardSuppliesRC), MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CGSTInwardSuppliesRC), MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.SGSTInwardSuppliesRC), MyDouble.getAmountDecimalForGSTRReport(GSTR_3_B_data.CESSInwardSuppliesRC)]);
	rowObj.push(['(e) Non-GST outward supplies', 0.0, 0.0, 0.0, 0.0, 0.0]);
	return rowObj;
};

function printExcel() {
	var XLSX = require('xlsx');
	var dataArray = getReportDataFor3_1();
	var worksheet3_1 = XLSX.utils.aoa_to_sheet(dataArray);

	var workbook = {
		'SheetNames': [],
		'Sheets': {}
	};

	var wsName = '3.1 Report';
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet3_1;
	var wscolWidth = [{ wch: 40 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];
	worksheet3_1['!cols'] = wscolWidth;

	var dataArray3_2 = getReportDataFor3_2();
	var worksheet3_2 = XLSX.utils.aoa_to_sheet(dataArray3_2);
	var wsName = '3.2 Report';
	workbook.SheetNames[1] = wsName;
	workbook.Sheets[wsName] = worksheet3_2;
	var wscolWidth = [{ wch: 40 }, { wch: 40 }, { wch: 40 }, { wch: 40 }, { wch: 40 }, { wch: 40 }, { wch: 40 }, { wch: 40 }, { wch: 40 }, { wch: 40 }, { wch: 40 }];
	worksheet3_2['!cols'] = wscolWidth;

	var dataArray5 = getReportDataFor4();
	var worksheet5 = XLSX.utils.aoa_to_sheet(dataArray5);
	var wsName = '4 Report';
	workbook.SheetNames[3] = wsName;
	workbook.Sheets[wsName] = worksheet5;
	var wscolWidth = [{ wch: 50 }, { wch: 40 }, { wch: 40 }];
	worksheet5['!cols'] = wscolWidth;

	var dataArray4 = getReportDataFor3();
	var worksheet4 = XLSX.utils.aoa_to_sheet(dataArray4);
	var wsName = '3 Report';
	workbook.SheetNames[2] = wsName;
	workbook.Sheets[wsName] = worksheet4;
	var wscolWidth = [{ wch: 50 }, { wch: 40 }, { wch: 40 }, { wch: 40 }, { wch: 40 }];
	worksheet4['!cols'] = wscolWidth;

	var dataArrayForStateSpecificCESS = getReportDataForStateSpecificCESS();
	if (dataArrayForStateSpecificCESS) {
		var stateCESSSheet = XLSX.utils.aoa_to_sheet(dataArrayForStateSpecificCESS);
		wsName = 'Flood CESS';
		workbook.SheetNames[4] = wsName;
		workbook.Sheets[wsName] = stateCESSSheet;
		wscolWidth = [{ wch: 40 }, { wch: 40 }, { wch: 40 }, { wch: 40 }, { wch: 40 }, { wch: 40 }, { wch: 40 }];
		stateCESSSheet['!cols'] = wscolWidth;
		stateCESSSheet['!merges'] = [{ s: { r: 0, c: 0 }, e: { r: 1, c: 0 } }, { s: { r: 0, c: 1 }, e: { r: 1, c: 1 } }, { s: { r: 0, c: 2 }, e: { r: 0, c: 4 } }, { s: { r: 0, c: 5 }, e: { r: 1, c: 5 } }, { s: { r: 0, c: 6 }, e: { r: 1, c: 6 } }];
	}

	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

populateTable();