var PDFHandler = require('./../Utilities/PDFHandler.js');
var MyDate = require('./../Utilities/MyDate');
var MyDouble = require('./../Utilities/MyDouble');
var StringConstant = require('./../Constants/StringConstants');
var pdfHandler = new PDFHandler();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var startDate;
var endDate;
var DateFormat = require('./../Constants/DateFormat.js');
$('#startDate, #endDate').datepicker({ dateFormat: DateFormat.format });
var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();

var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var TxnPaymentStatusConstants = require('./../Constants/TxnPaymentStatusConstants.js');
var NameCache = require('./../Cache/NameCache.js');
var nameCache = new NameCache();
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');
var printForSharePDF = false;

var listOfTransactions = [];
var transactionCount = 0;
var showContextMenuColumn = false;

function mountComponent() {
	var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

	var FirstScreen = require('./jsx/UIControls/FirstScreen').default;
	var MountComponent = require('./jsx/MountComponent').default;
	MountComponent(FirstScreen, document.querySelector('#firstTransaction'), data);
}
function loadTransactions(filteredTransactions, showFirstScreen) {
	var selectedTransaction = Number($('#transactionFilterOptions').val());
	if (transactionCount === 0 && showFirstScreen && selectedTransaction == TxnTypeConstant.TXN_TYPE_CASHIN) {
		$('#firstTransactionContainer').show();
		$('#customReportContainer').hide();
		mountComponent({
			image: '../inlineSVG/first-screens/payment.svg',
			text: 'Record payment received from parties & easily link them to your current invoices.',
			buttonLabel: 'Add Your First Payment In',
			onClick: function onClick() {
				var _require = require('../Utilities/TransactionHelper'),
				    viewTransaction = _require.viewTransaction;

				viewTransaction('', TxnTypeConstant.getTxnType(TxnTypeConstant.TXN_TYPE_CASHIN));
			}
		});
	} else if (transactionCount === 0 && showFirstScreen && selectedTransaction == TxnTypeConstant.TXN_TYPE_CASHOUT) {
		$('#firstTransactionContainer').show();
		$('#customReportContainer').hide();
		mountComponent({
			image: '../inlineSVG/first-screens/payment-out.svg',
			text: 'Record payment given to your parties & easily link them to purchase bills.',
			buttonLabel: 'Add Your First Payment Out',
			onClick: function onClick() {
				var _require2 = require('../Utilities/TransactionHelper'),
				    viewTransaction = _require2.viewTransaction;

				viewTransaction('', TxnTypeConstant.getTxnType(TxnTypeConstant.TXN_TYPE_CASHOUT));
			}
		});
	} else if (transactionCount === 0 && showFirstScreen && selectedTransaction == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
		$('#firstTransactionContainer').show();
		$('#customReportContainer').hide();
		mountComponent({
			image: '../inlineSVG/first-screens/sale-return.svg',
			text: 'Make sale return (credit note) for items returned by your customers & keep a track of your inventory.',
			buttonLabel: 'Add Your First Cr. Note/Sale Return',
			onClick: function onClick() {
				var _require3 = require('../Utilities/TransactionHelper'),
				    viewTransaction = _require3.viewTransaction;

				viewTransaction('', TxnTypeConstant.getTxnType(TxnTypeConstant.TXN_TYPE_SALE_RETURN));
			}
		});
	} else if (transactionCount === 0 && showFirstScreen && selectedTransaction == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
		$('#firstTransactionContainer').show();
		$('#customReportContainer').hide();
		mountComponent({
			image: '../inlineSVG/first-screens/sale-return.svg',
			text: 'Make purchase return (debit note) for items returned to your suppliers & keep a track of your inventory.',
			buttonLabel: 'Add Your First Purchase Return',
			onClick: function onClick() {
				var _require4 = require('../Utilities/TransactionHelper'),
				    viewTransaction = _require4.viewTransaction;

				viewTransaction('', TxnTypeConstant.getTxnType(TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN));
			}
		});
	} else {
		$('#firstTransactionContainer').hide();
		$('#customReportContainer').show();
		displayTransactionList(filteredTransactions || listOfTransactions);
	}
}

var populateTable = function populateTable() {
	var showFirstScreen = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	settingCache = new SettingCache();
	var DataLoader = require('./../DBManager/DataLoader.js');
	var dataLoader = new DataLoader();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var nameString = $('#nameForReport').val();
	var selectedTransaction = Number($('#transactionFilterOptions').val());
	var nameId = -1;

	if (nameString) {
		var nameCache = new NameCache();
		var nameModel = nameCache.findNameModelByName(nameString);
		if (nameModel) {
			nameId = nameModel.getNameId();
		} else {
			nameId = 0;
		}
	}

	startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
	endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');
	var firmId = Number($('#firmFilterOptions').val());
	CommonUtility.showLoader(function () {
		if (selectedTransaction) {
			transactionCount = dataLoader.getTransactionCountByTxnType(selectedTransaction);
			// transactionCount = 0;
		}
		listOfTransactions = dataLoader.loadTransactionsForCustomReport(startDate, endDate, nameId, selectedTransaction, firmId);
		loadTransactions(listOfTransactions, showFirstScreen);
	});
};

var date = MyDate.getDate('d/m/y');
$('#startDate').val(MyDate.getFirstDateOfCurrentMonthString());
$('#endDate').val(date);

if (settingCache.getMultipleFirmEnabled()) {
	var optionTemp = document.createElement('option');
	var FirmCache = require('./../Cache/FirmCache.js');
	var firmCache = new FirmCache();
	var firmList = firmCache.getFirmList();
	for (var i in firmList) {
		var option = optionTemp.cloneNode();
		option.text = firmList[i].getFirmName();
		option.value = firmList[i].getFirmId();
		$('#firmFilterOptions').append(option);
	}
	$('#firmChooser').show();
} else {
	$('#firmChooser').hide();
}

if (settingCache.isBillToBillEnabled()) {
	var paymentStatusArray = [StringConstant.paymentStatusUnPaid + '/ ' + StringConstant.paymentStatusUnUsed, StringConstant.paymentStatusPartial, StringConstant.paymentStatusPaid + '/ ' + StringConstant.paymentStatusUsed];
	var paymentListId = [TxnPaymentStatusConstants.UNPAID, TxnPaymentStatusConstants.PARTIAL, TxnPaymentStatusConstants.PAID];
	var _optionTemp = document.createElement('option');
	for (var _i in paymentStatusArray) {
		var _option = _optionTemp.cloneNode();
		_option.clssName = 'optionClass';
		_option.text = paymentStatusArray[_i];
		_option.value = paymentListId[_i];
		$('#paymentStatusFilterOptions').append(_option);
	}

	if (settingCache.isPaymentTermEnabled()) {
		var _option2 = _optionTemp.cloneNode();
		_option2.clssName = 'optionClass';
		_option2.text = StringConstant.paymentStatusOverDue;
		_option2.value = TxnPaymentStatusConstants.OVERDUE;
		$('#paymentStatusFilterOptions').append(_option2);
	}

	$('#paymentStatusFilterDiv').removeClass('hide');

	$('#paymentStatusPrintDiv').removeClass('hide');

	$('#paymentStatusExcel').removeClass('hide');
} else {
	$('#paymentStatusFilterDiv').addClass('hide');

	$('#paymentStatusPrintDiv').addClass('hide');

	$('#paymentStatusExcel').addClass('hide');
}

$('#paymentStatusFilterOptions').on('selectmenuchange', function (event) {
	displayTransactionList(listOfTransactions);
});

$('#firmFilterOptions').on('selectmenuchange', function (event) {
	populateTable(false);
});

$('#startDate').change(function () {
	populateTable(false);
});

$('#endDate').change(function () {
	populateTable(false);
});

$(function () {
	$('#nameForReport').autocomplete(UIHelper.getAutocompleteDefaultOptions(nameCache.getListOfNames()));
});

var transactionListArray = ['Sale', 'Purchase', 'Payment-In', 'Payment-Out', 'Credit Note', 'Debit Note', 'Sale Order', 'Purchase Order', 'Estimate', 'Delivery Challan'];
var transactionListId = [TxnTypeConstant.TXN_TYPE_SALE, TxnTypeConstant.TXN_TYPE_PURCHASE, TxnTypeConstant.TXN_TYPE_CASHIN, TxnTypeConstant.TXN_TYPE_CASHOUT, TxnTypeConstant.TXN_TYPE_SALE_RETURN, TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN, TxnTypeConstant.TXN_TYPE_SALE_ORDER, TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER, TxnTypeConstant.TXN_TYPE_ESTIMATE, TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN];
// var len = transactionsList.length;
for (var _i2 in transactionListArray) {
	var _option3 = $('<option />').text(transactionListArray[_i2]).val(transactionListId[_i2]);
	$('#transactionFilterOptions').append(_option3);
}
try {
	var transactionFilterOptions = $('#transactionFilterOptions');

	if (window.filterTxnTypeFromLeftNav && transactionFilterOptions.find('option[value=' + window.filterTxnTypeFromLeftNav + ']').length > 0) {
		transactionFilterOptions.val(window.filterTxnTypeFromLeftNav).selectmenu('refresh');
		showContextMenuColumn = true; // Context Menu should be shown if coming from leftNav and not for reports
		window.filterTxnTypeFromLeftNav = '';
	}
} catch (e) {}

$('#nameForReport').on('change', function (event) {
	populateTable(false);
});

$('#transactionFilterOptions').on('selectmenuchange', function () {
	populateTable(false);
});

var customReportClusterize;

var ShowPaymentStatusForTheseTransactions = [TxnTypeConstant.TXN_TYPE_SALE, TxnTypeConstant.TXN_TYPE_PURCHASE, TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN, TxnTypeConstant.TXN_TYPE_SALE_RETURN, TxnTypeConstant.TXN_TYPE_CASHIN, TxnTypeConstant.TXN_TYPE_CASHOUT, TxnTypeConstant.TXN_TYPE_ROPENBALANCE, TxnTypeConstant.TXN_TYPE_POPENBALANCE];

var displayTransactionList = function displayTransactionList(listOfTransactions) {
	var len = listOfTransactions.length;
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var dynamicRow = '';
	var rowData = [];
	var isTransactionRefNumberEnabled = settingCache.getTxnRefNoEnabled();
	var isBillToBillEnabled = settingCache.isBillToBillEnabled();
	var isPaymentTermEnabled = settingCache.isPaymentTermEnabled();
	var selectedTransaction = Number($('#transactionFilterOptions').val());
	var dateColumnWidthRatio = 9;
	var refNumberColumnRatio = isTransactionRefNumberEnabled ? 11 : 0;
	var nameColumnRatio = 19;
	var txnTypeColumnRatio = 10;
	var totalAmountColumnRatio = 14;
	var cashAmountColumnRatio = 14;
	var balanceAmountColumnRatio = 14;
	var paymentStatusColumnRatio = isBillToBillEnabled && !isPaymentTermEnabled ? 18 : 0;
	var dueDateColumnRatio = isPaymentTermEnabled ? 10 : 0;
	var statusColumnRatio = isPaymentTermEnabled ? 8 : 0;
	var contextMenuDotsRatio = showContextMenuColumn ? 2 : 0;

	var totalColumnWidth = dateColumnWidthRatio + refNumberColumnRatio + nameColumnRatio + txnTypeColumnRatio + totalAmountColumnRatio + cashAmountColumnRatio + balanceAmountColumnRatio + paymentStatusColumnRatio + dueDateColumnRatio + statusColumnRatio + contextMenuDotsRatio;

	var className = '';
	if (!$('#sortIcon').attr('class')) {
		className = 'ui-selectmenu-icon ui-icon ui-icon-triangle-1-s';
	} else {
		className = $('#sortIcon').attr('class');
	}

	dynamicRow += "<thead><tr><th width='" + dateColumnWidthRatio * 100 / totalColumnWidth + "%'>DATE</th>";
	if (isTransactionRefNumberEnabled) {
		dynamicRow += '<th width=\'' + refNumberColumnRatio * 100 / totalColumnWidth + '%\' id=\'invoiceColId\' style=\'cursor:pointer;\'>REF NO.<span id=\'sortIcon\' class=\'' + className + '\'></span></th>';
	}
	dynamicRow += "<th width='" + nameColumnRatio * 100 / totalColumnWidth + "%'>NAME</th><th width='" + txnTypeColumnRatio * 100 / totalColumnWidth + "%'>TYPE</th><th width='" + totalAmountColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>TOTAL</th><th width='" + cashAmountColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>RECEIVED / PAID</th>";

	dynamicRow += "<th width='" + balanceAmountColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>BALANCE</th>";

	if (isBillToBillEnabled) {
		if (isPaymentTermEnabled) {
			dynamicRow += "<th width='" + dueDateColumnRatio * 100 / totalColumnWidth + "%'>DUE DATE</th>";
			dynamicRow += "<th width='" + statusColumnRatio * 100 / totalColumnWidth + "%'>STATUS</th>";
		} else {
			dynamicRow += "<th width='" + paymentStatusColumnRatio * 100 / totalColumnWidth + "%'>PAYMENT STATUS</th>";
		}
	}
	if (showContextMenuColumn) {
		dynamicRow += '<td width="' + contextMenuDotsRatio * 100 / totalColumnWidth + '%"></td>';
	}
	dynamicRow += '</tr></thead>';
	var paymentStatVal = Number($('#paymentStatusFilterOptions').val());

	var finalTotalAmount = 0;
	for (var _i3 = 0; _i3 < len; _i3++) {
		var txn = listOfTransactions[_i3];
		var typeString = TxnTypeConstant.getTxnTypeForUI(txn.getTxnType());
		var name = txn.getNameRef().getFullName();
		var date = MyDate.getDate('d/m/y', txn.getTxnDate());
		var receivedAmt = txn.getCashAmount() ? txn.getCashAmount() : 0;
		var balanceAmt = txn.getBalanceAmount() ? txn.getBalanceAmount() : 0;
		var txnCurrentBalanceAmt = txn.getTxnCurrentBalanceAmount() ? txn.getTxnCurrentBalanceAmount() : 0;
		var totalAmt = 0;
		var discountAmt = txn.getDiscountAmount() ? txn.getDiscountAmount() : 0;
		var typeTxn = TxnTypeConstant.getTxnType(txn.getTxnType());
		var refNo = txn.getTxnRefNumber() === '' ? '' : txn.getFullTxnRefNumber();
		if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHIN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHOUT) {
			totalAmt = Number(receivedAmt) + Number(discountAmt);
		} else {
			totalAmt = Number(receivedAmt) + Number(balanceAmt);
		}

		var txnTotalReceivedAmount = totalAmt - txnCurrentBalanceAmt;

		var paymentStatus = txn.getTxnPaymentStatus();
		var paymentStatusString = TxnPaymentStatusConstants.getPaymentStatusForUI(txn.getTxnType(), paymentStatus);
		var dueDate = MyDate.convertDateToStringForUI(txn.getTxnDueDate());
		var dueByDays = MyDouble.getDueDays(txn.getTxnDueDate());

		if (!ShowPaymentStatusForTheseTransactions.includes(txn.getTxnType())) {
			paymentStatusString = '';
		}

		var row = '<tr id=' + txn.getTxnId() + ':' + typeTxn + " ondblclick='openTransaction(" + txn.getTxnId() + ',' + txn.getTxnType() + ")' class='currentRow'><td width='" + dateColumnWidthRatio * 100 / totalColumnWidth + "%'>" + date + '</td>';
		if (isTransactionRefNumberEnabled) {
			row += "<td width='" + refNumberColumnRatio * 100 / totalColumnWidth + "%'>" + refNo + '</td>';
		}
		row += "<td width='" + nameColumnRatio * 100 / totalColumnWidth + "%'>" + name + "</td><td width='" + txnTypeColumnRatio * 100 / totalColumnWidth + "%'>" + typeString + "</td><td width='" + totalAmountColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalAmt) + '</td>';

		if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHIN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHOUT) {
			row += "<td width='" + cashAmountColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(Number(receivedAmt)) + '</td>';
		} else if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN) {
			row += "<td width='" + cashAmountColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'></td>";
		} else {
			row += "<td width='" + cashAmountColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(txnTotalReceivedAmount) + '</td>';
		}

		if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHIN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHOUT || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN) {
			row += "<td width='" + balanceAmountColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'></td>";
		} else {
			row += "<td width='" + balanceAmountColumnRatio * 100 / totalColumnWidth + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(txnCurrentBalanceAmt) + '</td>';
		}

		if (isBillToBillEnabled) {
			if (isPaymentTermEnabled) {
				if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE) {
					row += "<td width='" + dueDateColumnRatio * 100 / totalColumnWidth + "%'>" + dueDate + '</td>';
					if (dueByDays > 0 && paymentStatus != TxnPaymentStatusConstants.PAID) {
						paymentStatusString = 'Overdue';
						row += "<td width='" + statusColumnRatio * 100 / totalColumnWidth + "%'>";
						if (paymentStatusString) {
							row += '<div>' + paymentStatusString + '</div>';
							row += '<div>' + dueByDays + ' Days</div>';
						}
					} else {
						row += "<td width='" + statusColumnRatio * 100 / totalColumnWidth + "%'>";
						row += '<div>' + paymentStatusString + '</div>';
					}
					row += '</td>';
				} else {
					row += "<td width='" + dueDateColumnRatio * 100 / totalColumnWidth + "%'></td>";
					row += "<td width='" + statusColumnRatio * 100 / totalColumnWidth + "%'>" + paymentStatusString + '</td>';
				}
			} else {
				row += "<td width='" + paymentStatusColumnRatio * 100 / totalColumnWidth + "%'>" + paymentStatusString + '</td>';
			}
		}

		if (showContextMenuColumn) {
			row += '<td class="contextMenuDots" width="' + contextMenuDotsRatio * 100 / totalColumnWidth + '%"></td>';
		}
		row += '</tr>';

		var paymentStatFilter = void 0;

		if (settingCache.isPaymentTermEnabled() && paymentStatVal == TxnPaymentStatusConstants.OVERDUE) {
			paymentStatFilter = dueByDays > 0 && paymentStatus != TxnPaymentStatusConstants.PAID;
		} else {
			paymentStatFilter = paymentStatVal ? paymentStatVal == paymentStatus : true;
		}

		paymentStatFilter = settingCache.isBillToBillEnabled() ? paymentStatFilter : true;

		if (paymentStatFilter) {
			finalTotalAmount += totalAmt;
			rowData.push(row);
		}
	}
	var data = rowData;
	if ($('#customReportContainer').length === 0) {
		return;
	}
	if (customReportClusterize && customReportClusterize.destroy) {
		customReportClusterize.clear();
		customReportClusterize.destroy();
	}
	customReportClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollArea',
		contentId: 'contentArea',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});
	$('#tableHead').html(dynamicRow);
	if (selectedTransaction > 0) {
		$('#totalAmountCustomReport').html(MyDouble.getAmountWithDecimalAndCurrencyWithSign(finalTotalAmount));
		$('#total').css('display', 'block');
		if (finalTotalAmount < 0) {
			$('#totalAmountCustomReport').css('color', '#e35959');
		} else {
			$('#totalAmountCustomReport').css('color', '#1faf9d');
		}
	} else {
		$('#total').css('display', 'none');
	}
};

$('#tableHead').on('click', '#invoiceColId', function (e) {
	var SortHelper = require('./../Utilities/SortHelper.js');
	var sortHelper = new SortHelper();
	listOfTransactions = sortHelper.getSortedListByInvoice(listOfTransactions, 'txnRefNumber');
	displayTransactionList(listOfTransactions);
});

var openTransaction = function openTransaction(txnId, txnType) {
	var ViewTransaction = require('./../BizLogic/ViewTransaction.js');
	var viewTransaction = new ViewTransaction();
	viewTransaction.viewTransactionFile(txnId + ':' + TxnTypeConstant.getTxnType(txnType));
};

if (settingCache.getItemEnabled()) {
	$('#itemDetailPrintDiv').show();
} else {
	$('#itemDetailPrintDiv').hide();
}

var getHTMLTextForReport = function getHTMLTextForReport() {
	var CustomReportHTMLGenerator = require('./../ReportHTMLGenerator/CustomReportHTMLGenerator.js');
	var customReportHTMLGenerator = new CustomReportHTMLGenerator();

	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var firmId = Number($('#firmFilterOptions').val());
	var nameString = $('#nameForReport').val();
	if (!nameString) {
		nameString = 'All parties';
	}
	var selectedTransaction = Number($('#transactionFilterOptions').val());
	var selectedTransactionIndex = transactionListId.indexOf(selectedTransaction);
	var selectedTransactionString = 'All transactions';
	if (selectedTransactionIndex >= 0) {
		selectedTransactionString = transactionListArray[selectedTransactionIndex];
	}
	var printItemDetails = $('#printItems').is(':checked');
	var printDescription = $('#printDescription').is(':checked');
	var printPaymentStatus = $('#printPaymentStatus').is(':checked');
	var paymentStatVal = Number($('#paymentStatusFilterOptions').val());
	var htmlText = customReportHTMLGenerator.getHTMLText(listOfTransactions, startDateString, endDateString, firmId, nameString, selectedTransactionString, printItemDetails, printDescription, printPaymentStatus, paymentStatVal);
	return htmlText;
};

var printTransaction = function printTransaction(that) {
	var settingCache = new SettingCache();
	var ThermalPrinterConstant = require('./../Constants/ThermalPrinterConstant.js');
	if (settingCache.getDefaultPrinterType() == ThermalPrinterConstant.THERMAL_PRINTER) {
		var txnId = that.id.split(':')[0];
		var PrintUtil = require('./../Utilities/PrintUtil.js');
		PrintUtil.printTransactionUsingThermalPrinter(txnId);
	} else {
		var html = transactionPDFHandler.transactionToHTML(that.id);
		if (html) {
			pdfHandler.print(html);
		}
	}
};

$('#currentAccountTransactionDetails').contextmenu({
	delegate: '.currentRow',
	addClass: 'width10',
	menu: [{ title: 'Open PDF', cmd: 'openpdf' }, { title: '----' }, { title: 'Print', cmd: 'print' }, { title: '----' }, { title: 'Delete', cmd: 'delete' }],
	select: function select(event, ui) {
		var id = ui.target[0].parentNode.id;
		if (ui.cmd == 'print') {
			printTransaction(ui.target[0].parentNode);
		} else if (ui.cmd == 'openpdf') {
			openPdfFromRightClickMenu(ui.target[0].parentNode);
		} else if (ui.cmd == 'delete') {
			var DeleteTransaction = require('./../BizLogic/DeleteTransaction.js');
			var deleteTransaction = new DeleteTransaction();
			deleteTransaction.RemoveTransaction(id);
		}
	}
}).on('click', '.contextMenuDots', function () {
	$(this).trigger('contextmenu');
});

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	// $('#frameDiv').load('');
	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ display: 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable(false);
};

/// ///////////////////////////PDF/////////////////////////////////////

function openPrintOptions() {
	$('#customPrintDialog').removeClass('hide');
	$('#customPrintDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closePrintOptions() {
	$('#customPrintDialog').addClass('hide');
	$('#customPrintDialog').dialog('close').dialog('destroy');
}

var openPdfFromRightClickMenu = function openPdfFromRightClickMenu(that) {
	$('#loading').show(function () {
		var html = transactionPDFHandler.transactionToHTML(that.id);
		if (html) {
			pdfHandler.openPDF(html, false);
		}
	});
};

var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({
		type: PDFReportConstant.CUSTOM_REPORT,
		fromDate: $('#startDate').val(),
		toDate: $('#endDate').val()
	});
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#customPrintDialog').addClass('hide');
	$('#customPrintDialog').dialog('close').dialog('destroy');
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

if (settingCache.getItemEnabled()) {
	$('#itemDetailsExcel').show();
} else {
	$('#itemDetailsExcel').hide();
}

function openExcelOptions() {
	$('#ExcelDialog').removeClass('hide');
	$('#ExcelDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closeExcelOptions() {
	$('#ExcelDialog').addClass('hide');
	$('#ExcelDialog').dialog('close').dialog('destroy');
}

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({
		type: PDFReportConstant.CUSTOM_REPORT,
		fromDate: $('#startDate').val(),
		toDate: $('#endDate').val()
	});
	excelHelper.saveExcel(fileName);
};

var prepareObjectForExcel = function prepareObjectForExcel(listOfTransactions) {
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var isTransactionRefNumberEnabled = settingCache.getTxnRefNoEnabled();
	var len = listOfTransactions.length;
	var rowObj = [];
	var tableHeadArray = [];
	var totalArray = [];
	var ExcelDescription = $('#ExcelSaleDescription').is(':checked');
	var ExcelPaymentStatus = $('#ExcelPaymentStatus').is(':checked');
	tableHeadArray.push('Date');
	if (isTransactionRefNumberEnabled) {
		tableHeadArray.push('Reference No');
		totalArray.push('');
	}
	tableHeadArray.push('Name');
	tableHeadArray.push('Type');
	tableHeadArray.push('Total');
	tableHeadArray.push('Paid');
	tableHeadArray.push('Received');

	tableHeadArray.push('Balance');
	if (settingCache.isBillToBillEnabled() && ExcelPaymentStatus) {
		if (settingCache.isPaymentTermEnabled()) {
			tableHeadArray.push('Due Date');
			tableHeadArray.push('Status');
		} else {
			tableHeadArray.push('Payment Status');
		}
	}
	if (ExcelDescription) {
		tableHeadArray.push('Description');
	}
	totalArray.push('');
	totalArray.push('');
	rowObj.push(tableHeadArray);
	rowObj.push([]);

	var totalAmount = 0;
	var totalReceivedAmount = 0;
	var totalPaidAmount = 0;

	var paymentStatVal = Number($('#paymentStatusFilterOptions').val());

	for (var j = 0; j < len; j++) {
		var tempArray = [];
		var txn = listOfTransactions[j];
		var txnType = txn.getTxnType();
		var typeString = TxnTypeConstant.getTxnTypeForUI(txn.getTxnType());
		var name = txn.getNameRef().getFullName();
		var date = MyDate.getDate('d/m/y', txn.getTxnDate());
		var receivedAmt = txn.getCashAmount() ? txn.getCashAmount() : 0;
		var balanceAmt = txn.getBalanceAmount() ? txn.getBalanceAmount() : 0;
		var txnCurrentBalanceAmount = txn.getTxnCurrentBalanceAmount() ? Number(txn.getTxnCurrentBalanceAmount()) : 0;
		var totalAmt = 0;
		var receivedAmount = 0;
		var paidAmount = 0;
		var discountAmt = txn.getDiscountAmount() ? txn.getDiscountAmount() : 0;
		if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHIN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHOUT) {
			totalAmt = Number(receivedAmt) + Number(discountAmt);
		} else {
			totalAmt = Number(receivedAmt) + Number(balanceAmt);
		}

		var paymentStatus = txn.getTxnPaymentStatus();
		var paymentStatusString = TxnPaymentStatusConstants.getPaymentStatusForUI(txn.getTxnType(), paymentStatus);

		if (!ShowPaymentStatusForTheseTransactions.includes(txn.getTxnType())) {
			paymentStatusString = '';
		}

		var dueDate = MyDate.convertDateToStringForUI(txn.getTxnDueDate());
		var dueByDays = MyDouble.getDueDays(txn.getTxnDueDate());

		var paymentStatFilter = void 0;

		if (settingCache.isPaymentTermEnabled() && paymentStatVal == TxnPaymentStatusConstants.OVERDUE) {
			paymentStatFilter = dueByDays > 0 && paymentStatus != TxnPaymentStatusConstants.PAID;
		} else {
			paymentStatFilter = paymentStatVal ? paymentStatVal == paymentStatus : true;
		}

		paymentStatFilter = settingCache.isBillToBillEnabled() ? paymentStatFilter : true;

		tempArray.push(date);
		if (isTransactionRefNumberEnabled) {
			var refNo = txn.getTxnRefNumber() === '' ? '' : txn.getFullTxnRefNumber();
			tempArray.push(refNo);
		}
		tempArray.push(name);
		tempArray.push(typeString);
		tempArray.push(MyDouble.getAmountWithDecimal(totalAmt));

		if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHIN) {
			receivedAmount = receivedAmt;
			tempArray.push('');
			tempArray.push(MyDouble.getAmountWithDecimal(receivedAmount));
		} else if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHOUT || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_EXPENSE) {
			paidAmount = receivedAmt;
			tempArray.push(MyDouble.getAmountWithDecimal(paidAmount));
			tempArray.push('');
		} else if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_OTHER_INCOME) {
			receivedAmount = totalAmt - txnCurrentBalanceAmount;
			tempArray.push('');
			tempArray.push(MyDouble.getAmountWithDecimal(receivedAmount));
		} else if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
			paidAmount = totalAmt - txnCurrentBalanceAmount;
			tempArray.push(MyDouble.getAmountWithDecimal(paidAmount));
			tempArray.push('');
		} else {
			tempArray.push('');
			tempArray.push('');
		}

		if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHIN || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_CASHOUT || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_EXPENSE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_OTHER_INCOME || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_ESTIMATE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN) {
			tempArray.push('');
		} else {
			tempArray.push(MyDouble.getAmountWithDecimal(txnCurrentBalanceAmount));
		}

		if (settingCache.isBillToBillEnabled() && ExcelPaymentStatus) {
			if (settingCache.isPaymentTermEnabled()) {
				if (txn.getTxnType() == TxnTypeConstant.TXN_TYPE_SALE || txn.getTxnType() == TxnTypeConstant.TXN_TYPE_PURCHASE) {
					tempArray.push(dueDate);
					if (dueByDays > 0 && paymentStatus != TxnPaymentStatusConstants.PAID) {
						tempArray.push('Overdue ' + dueByDays + ' Days');
					} else {
						tempArray.push(paymentStatusString);
					}
				} else {
					tempArray.push('');
					tempArray.push(paymentStatusString);
				}
			} else {
				tempArray.push(paymentStatusString);
			}
		}

		if (ExcelDescription) {
			tempArray.push(txn.getDescription());
		}

		if (paymentStatFilter) {
			totalReceivedAmount += receivedAmount;
			totalPaidAmount += paidAmount;
			totalAmount += totalAmt;
			rowObj.push(tempArray);
		}
	}
	rowObj.push([]);
	var selectedTransaction = Number($('#transactionFilterOptions').val());
	if (selectedTransaction > 0) {
		totalArray.push('Total');
		totalArray.push(MyDouble.getAmountWithDecimal(totalAmount));
		totalArray.push(MyDouble.getAmountWithDecimal(totalPaidAmount));
		totalArray.push(MyDouble.getAmountWithDecimal(totalReceivedAmount));
		rowObj.push(totalArray);
	}
	return rowObj;
};

function printExcel() {
	closeExcelOptions();
	var XLSX = require('xlsx');
	var dataArray = prepareObjectForExcel(listOfTransactions);
	var worksheet = XLSX.utils.aoa_to_sheet(dataArray);
	var ExcelItemDetails = $('#ExcelCustomItemDetails').is(':checked');
	var ExcelExportHelper = require('./../UIControllers/ExcelExportHelper.js');
	var excelExportHelper = new ExcelExportHelper();

	var workbook = {
		SheetNames: [],
		Sheets: {}
	};

	var wsName = 'Custom Report';
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet;
	var wscolWidth = [{ wch: 15 }, { wch: 15 }, { wch: 30 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 25 }, { wch: 25 }, { wch: 30 }, { wch: 30 }];
	worksheet['!cols'] = wscolWidth;

	if (ExcelItemDetails) {
		var wsNameItem = 'Item Details';
		workbook.SheetNames[1] = wsNameItem;
		var itemArray = excelExportHelper.prepareItemObjectForExcel(listOfTransactions);
		var itemWorksheet = XLSX.utils.aoa_to_sheet(itemArray);
		workbook.Sheets[wsNameItem] = itemWorksheet;
		var wsItemcolWidth = [{ wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }];
		itemWorksheet['!cols'] = wsItemcolWidth;
	}
	console.log(workbook);
	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

populateTable();

(function () {
	var TxnTypeConstant = require('../Constants/TxnTypeConstant.js');
	var MountComponent = require('./jsx/MountComponent').default;
	var NewTransactionButton = require('./jsx/NewTransactionButton').default;
	function changeButton() {
		var txnType = $('#transactionFilterOptions').val();
		var target = document.querySelector('#NEW-TRANSACTION-BUTTON');
		if (txnType != 0) {
			MountComponent(NewTransactionButton, target, {
				label: '+ Add ' + TxnTypeConstant.getTxnTypeForUI(txnType),
				txnType: TxnTypeConstant.getTxnType(txnType)
			});
		} else {
			window.unmountReactComponent(target);
		}
	}
	changeButton();
	$('#transactionFilterOptions').on('selectmenuchange', changeButton);
})();