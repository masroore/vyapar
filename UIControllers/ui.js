var searchInput = $('.searchBox input ');
var closeIcon = $('.closeIcon');
var searchIcon = $('.searchIcon');

searchInput.on('focus', function () {
	//    console.log(this.parentNode);
	parent = this.parentNode;
	var searchIcon = $('.searchIcon');
	searchIcon.css({ fill: '#444444' });
	$('.searchBox').css({ boxShadow: 'inset 1px 1px 2px rgba(0,0,0,0.3)' });
	$('#searchText').css({ boxShadow: 'none' });

	$(parent).css({ boxShadow: '2px 2px 4px rgba(0,0,0,0.3)' });
});
searchInput.on('focusout', function () {
	parent = this.parentNode;
	var searchIcon = $('.searchIcon');
	searchIcon.css({ fill: '#9e9e9e' });
	$('.searchBox').css({ boxShadow: 'inset 1px 1px 2px rgba(0,0,0,0.3)' });
	$('#searchText').css({ boxShadow: 'inset 1px 1px 2px rgba(0,0,0,0.3)' });

	//    $(this).next().css({display:"none"});
	//    $(parent).css({boxShadow: "2px 2px 4px rgba(0,0,0,0.3)"});
});
searchInput.keyup(function () {
	$(this).next().css({ display: 'block' });
	closeIcon.css({ fill: '444444' });
});

$('.svgIconRight').on('click', function () {
	$(this.parentNode.children[1]).val('');
	$(this).css({ display: 'none' });
	searchIcon.css({ fill: '#9e9e9e' });
	console.log('inside');
	$(this).parent().find('input').trigger('keyup');
});