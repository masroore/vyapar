var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _create = require('babel-runtime/core-js/object/create');

var _create2 = _interopRequireDefault(_create);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DBWindow = require('../Utilities/DBWindow');
var IPCActions = require('../Constants/IPCActions');
var TxnTypeConstant = require('../Constants/TxnTypeConstant');
var MyDate = require('../Utilities/MyDate');
var LocalStorageHelper = require('../Utilities/LocalStorageHelper');
var DATE_RANGES = {
	THIS_MONTH: 1,
	LAST_MONTH: 2,
	THIS_QUARTER: 3,
	THIS_YEAR: 4,
	IN_7_DAYS: 5,
	IN_15_DAYS: 6,
	IN_1_MONTH: 7,
	labels: ['None', 'This Month', 'Last Month', 'This Quarter', 'This year', 'In 7 days', 'In 15 days', 'In 1 Month']
};
var GRAPH_FILTER_OPTIONS = {
	DAILY: 'daily',
	WEEKLY: 'weekly',
	MONTHLY: 'monthly',
	labels: {
		'daily': 'Daily',
		'weekly': 'Weekly',
		'monthly': 'Monthly'
	}
};

var cards = [{
	key: 'saleInvoices',
	type: 'sale',
	title: 'Sale :',
	graphEnabled: true,
	filterEnabled: true,
	displayCard: false
}, {
	key: 'toReceive',
	type: 'sale',
	title: 'To Receive :',
	graphEnabled: false,
	filterEnabled: false,
	displayCard: false
}, {
	key: 'saleOrders',
	type: 'sale',
	title: 'Sale Orders :',
	graphEnabled: false,
	filterEnabled: false,
	displayCard: false
}, {
	key: 'deliveryChallans',
	type: 'sale',
	title: 'Delivery Challans :',
	graphEnabled: false,
	filterEnabled: false,
	displayCard: false
}, {
	key: 'purchaseBills',
	type: 'purchase',
	title: 'Purchase :',
	graphEnabled: false,
	filterEnabled: true,
	displayCard: false
}, {
	key: 'toPay',
	type: 'purchase',
	title: 'To Pay :',
	graphEnabled: false,
	filterEnabled: false,
	displayCard: false
}, {
	key: 'purchaseOrders',
	type: 'purchase',
	title: 'Purchase Orders :',
	graphEnabled: false,
	filterEnabled: false,
	displayCard: false
}, {
	key: 'expenses',
	type: 'purchase',
	title: 'Expenses :',
	graphEnabled: true,
	filterEnabled: true,
	displayCard: false
}, {
	key: 'stockValue',
	type: 'stockInventory',
	title: 'Stock Value :',
	graphEnabled: false,
	filterEnabled: false,
	displayCard: false
}, {
	key: 'lowStocks',
	type: 'stockInventory',
	title: 'Low Stocks :',
	graphEnabled: false,
	filterEnabled: false,
	displayCard: false
}, {
	key: 'expiringStocks',
	type: 'stockInventory',
	title: 'Expiring Stocks :',
	graphEnabled: false,
	filterEnabled: true,
	displayCard: false
}, {
	key: 'otherIncome',
	type: 'stockInventory',
	title: 'Other Income :',
	graphEnabled: false,
	filterEnabled: true,
	displayCard: false
}, {
	key: 'cashInHand',
	type: 'cashAndBank',
	title: 'Cash In hand :',
	graphEnabled: false,
	filterEnabled: false,
	displayCard: false
}, {
	key: 'bankAccounts',
	type: 'cashAndBank',
	title: 'Bank Accounts :',
	graphEnabled: false,
	filterEnabled: false,
	displayCard: false
}, {
	key: 'openCheques',
	type: 'cashAndBank',
	title: 'Open Cheques :',
	graphEnabled: false,
	filterEnabled: false,
	displayCard: false
}, {
	key: 'loanAccounts',
	type: 'cashAndBank',
	title: 'Loan Accounts :',
	graphEnabled: false,
	filterEnabled: false,
	displayCard: false
}];

var waitPromise = function waitPromise() {
	var ms = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;

	return new _promise2.default(function (resolve) {
		setTimeout(resolve, ms);
	});
};

var getEnabledDashboardCards = function () {
	var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
		var cardsDisplayedByDefault, cardsToCheck, otherCards;
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						cardsDisplayedByDefault = ['saleInvoices', 'purchaseBills', 'toReceive', 'expenses', 'cashInHand', 'stockValue'];
						cardsToCheck = ['lowStocks', 'expiringStocks', 'saleOrders', 'purchaseOrders', 'deliveryChallans', 'otherIncome'];
						otherCards = ['loanAccounts', 'toPay', 'openCheques', 'bankAccounts'];
						return _context.abrupt('return', DBWindow.send(IPCActions.GET_CARDS_ENABLED_IN_SETTINGS, { cardsToCheck: cardsToCheck }).then(function (_ref2) {
							var cardsEnabledInSettings = _ref2.cardsEnabledInSettings;

							var enabledCardsMap = (0, _create2.default)(null);
							var previouslyDisplayedCards = getPreviouslyDisplayedCards();
							cards.forEach(function (card) {
								if (cardsDisplayedByDefault.includes(card.key) || cardsEnabledInSettings.includes(card.key) || otherCards.includes(card.key)) {
									if (previouslyDisplayedCards.includes(card.key)) {
										card.displayCard = true;
									}
									if (enabledCardsMap[card.type]) {
										enabledCardsMap[card.type].push((0, _extends3.default)({}, card));
									} else {
										enabledCardsMap[card.type] = [(0, _extends3.default)({}, card)];
									}
								}
							});
							return enabledCardsMap;
						}));

					case 4:
					case 'end':
						return _context.stop();
				}
			}
		}, _callee, this);
	}));

	return function getEnabledDashboardCards() {
		return _ref.apply(this, arguments);
	};
}();

function getCashInHandDetails() {
	var amount = 0;
	return DBWindow.send(IPCActions.GET_CASH_IN_HAND_AMOUNT, {
		endDate: null
	}).then(function (data) {
		if (data) {
			amount = data.cashInHand || 0;
		}
		return {
			amount: MyDouble.getBalanceAmountWithDecimalAndCurrency(amount, true),
			hasData: !!amount
		};
	});
}
function getStockValueDetails() {
	return waitPromise(500).then(function () {
		var ItemCache = require('../Cache/ItemCache');
		var itemCache = new ItemCache();
		var totalStockValue = itemCache.getTotalStockValue();
		var amount = MyDouble.getBalanceAmountWithDecimalAndCurrency(totalStockValue, true);
		return { amount: amount, hasData: !!totalStockValue };
	});
}
function getBankAccountDetails() {
	return waitPromise(500).then(function () {
		var PaymentCache = require('./../Cache/PaymentInfoCache.js');
		var paymentCache = new PaymentCache();
		paymentCache.refreshPaymentInfoCache();
		var details = {
			amount: 0,
			list: [],
			hasData: false
		};
		var totalBankBalance = 0;
		var bankList = [];
		var banksMap = paymentCache.getBankList();
		(0, _keys2.default)(banksMap).forEach(function (bankName) {
			totalBankBalance += Number(banksMap[bankName]);
			bankList.push(getRow({
				name: bankName,
				value: MyDouble.getBalanceAmountWithDecimalAndCurrency(banksMap[bankName], true)
			}));
		});
		details.amount = MyDouble.getBalanceAmountWithDecimalAndCurrency(totalBankBalance, true);
		details.list = bankList;
		details.hasData = bankList.length || !!totalBankBalance;
		return details;
	});
}
function getOpenOrderDetails(cardKey) {
	var txnTypes = [];
	if (cardKey == 'saleOrders') {
		txnTypes.push(TxnTypeConstant.TXN_TYPE_SALE_ORDER);
	} else if (cardKey == 'purchaseOrders') {
		txnTypes.push(TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER);
	} else if (cardKey == 'deliveryChallans') {
		txnTypes.push(TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN);
	}
	return DBWindow.send(IPCActions.GET_OPEN_ORDER_DETAILS, {
		txnTypes: txnTypes
	}).then(function (orderDetails) {
		var amount = '';
		var list = [];
		var hasData = !!orderDetails.count;
		if (cardKey == 'saleOrders') {
			list.push(getRow({ name: 'No. Of Open Orders', value: orderDetails.count }));
			list.push(getRow({ name: 'Open Sale Orders Amount', value: orderDetails.amount }));
		} else if (cardKey == 'purchaseOrders') {
			list.push(getRow({ name: 'No. Of  Purchase Orders', value: orderDetails.count }));
			list.push(getRow({ name: 'Purchase Orders Amount', value: orderDetails.amount }));
		} else if (cardKey == 'deliveryChallans') {
			list.push(getRow({ name: 'No. Of  Open Challans', value: orderDetails.count }));
			list.push(getRow({ name: 'Delivery Challan Amount', value: orderDetails.amount }));
		}
		return {
			amount: amount,
			list: list,
			hasData: hasData
		};
	});
}
function getOpenChequeDetails() {
	return DBWindow.send(IPCActions.GET_OPEN_CHEQUE_DETAILS, {
		endDate: null
	}).then(function (_ref3) {
		var openChequeDetails = _ref3.openChequeDetails;

		if (openChequeDetails) {
			var _openChequeDetails = (0, _slicedToArray3.default)(openChequeDetails, 2),
			    _openChequeDetails$ = (0, _slicedToArray3.default)(_openChequeDetails[0], 2),
			    receivedCount = _openChequeDetails$[0],
			    receivedAmount = _openChequeDetails$[1],
			    _openChequeDetails$2 = (0, _slicedToArray3.default)(_openChequeDetails[1], 2),
			    paidCount = _openChequeDetails$2[0],
			    paidAmount = _openChequeDetails$2[1];

			var list = [];
			var hasData = !!receivedCount || !!paidCount;
			list.push(getRow({ name: 'Received (' + receivedCount + ')', value: MyDouble.getBalanceAmountWithDecimalAndCurrency(receivedAmount) }));
			list.push(getRow({ name: 'Paid (' + paidCount + ')', value: MyDouble.getBalanceAmountWithDecimalAndCurrency(paidAmount) }));
			return {
				amount: '',
				list: list,
				hasData: hasData
			};
		}
	});
}

function getRow(entry) {
	return '<tr className=\'listItem\'>\n\t\t\t\t<td align=\'left\'>' + entry.name + '</td>\n\t\t\t\t<td align=\'right\'>' + entry.value + '</td>\n\t\t\t</tr>';
}

function getLoanDetails() {
	return waitPromise(500).then(function () {
		var LoanAccountCache = require('../Cache/LoanAccountCache');
		var loanAccountCache = new LoanAccountCache();
		var loanAccounts = loanAccountCache.getAllLoanAccounts();
		var details = {
			amount: 0,
			list: [],
			hasData: false
		};
		var totalLoanAmount = 0;
		var loanAccountsList = [];
		loanAccounts.forEach(function (loanAccount) {
			totalLoanAmount += Number(loanAccount.currentBalance || 0);
			loanAccountsList.push(getRow({
				name: loanAccount.accountName,
				value: '<span style="color:#e35959">' + MyDouble.getAmountWithDecimalAndCurrency(loanAccount.currentBalance) + '</span>' // MyDouble.getBalanceAmountWithDecimalAndCurrency(loanAccount.currentBalance, true)
			}));
		});
		details.amount = '<span style="color:#e35959">' + MyDouble.getAmountWithDecimalAndCurrency(totalLoanAmount) + '</span>'; // MyDouble.getBalanceAmountWithDecimalAndCurrency(totalLoanAmount, true);
		details.list = loanAccountsList;
		details.hasData = loanAccountsList.length || !!totalLoanAmount;
		return details;
	});
}

function getLowStockSummary() {
	return DBWindow.send(IPCActions.GET_CAN_DISPLAY_LOW_STOCKS).then(function (canDisplayLowStocks) {
		var details = {
			amount: '',
			list: [],
			hasData: false
		};
		return waitPromise(500).then(function () {
			var ItemCache = require('../Cache/ItemCache');
			var itemCache = new ItemCache();
			var stockStatusList = itemCache.getLowStockItemList();
			details.list = stockStatusList.map(function (stockDetail) {
				return getRow({
					name: stockDetail['itemName'],
					value: MyDouble.getQuantityWithDecimal(stockDetail['itemStockQuantity'])
				});
			});
			details.hasData = canDisplayLowStocks && details.list.length > 0;
			return details;
		});
	});
}

function getExpiringStocks(options) {
	var dateFilter = options.dateFilter;

	if (Number(dateFilter) < DATE_RANGES.IN_7_DAYS) {
		dateFilter = DATE_RANGES.IN_7_DAYS;
	}

	var _getDateRangeFromFilt = getDateRangeFromFilterOption(dateFilter),
	    toDate = _getDateRangeFromFilt.toDate;

	return DBWindow.send(IPCActions.GET_EXPIRING_STOCK_DETAILS, {
		endDate: toDate
	}).then(function (_ref4) {
		var stockList = _ref4.stockList;

		var list = stockList.map(function (_ref5) {
			var name = _ref5.name,
			    quantity = _ref5.quantity;

			return getRow({
				name: name,
				value: '<span style=\'color:#e35959\'>' + MyDouble.getQuantityWithDecimalWithoutColor(quantity) + ' Qty</span>'
			});
		});
		return {
			amount: '',
			list: list,
			hasData: list.length > 0
		};
	});
}

function getSaleDetails(options) {
	var dateFilter = options.dateFilter;

	var _getDateRangeFromFilt2 = getDateRangeFromFilterOption(dateFilter),
	    fromDate = _getDateRangeFromFilt2.fromDate,
	    toDate = _getDateRangeFromFilt2.toDate;

	return DBWindow.send(IPCActions.GET_SALE_DETAILS, {
		fromDate: fromDate,
		toDate: toDate
	}).then(function (_ref6) {
		var _ref6$totalAmount = _ref6.totalAmount,
		    totalAmount = _ref6$totalAmount === undefined ? 0 : _ref6$totalAmount,
		    graphData = _ref6.graphData;

		return {
			amount: '<span style="color:#1faf9d">' + MyDouble.getAmountWithDecimalAndCurrency(totalAmount) + '</span>',
			graphData: graphData,
			hasData: !!totalAmount
		};
	});
}

function getExpenseDetails(options) {
	var dateFilter = options.dateFilter;

	var _getDateRangeFromFilt3 = getDateRangeFromFilterOption(dateFilter),
	    fromDate = _getDateRangeFromFilt3.fromDate,
	    toDate = _getDateRangeFromFilt3.toDate;

	return DBWindow.send(IPCActions.GET_EXPENSE_DETAILS, {
		fromDate: fromDate,
		toDate: toDate
	}).then(function (_ref7) {
		var _ref7$totalAmount = _ref7.totalAmount,
		    totalAmount = _ref7$totalAmount === undefined ? 0 : _ref7$totalAmount,
		    graphData = _ref7.graphData;

		var hasData = !!totalAmount;
		return {
			amount: MyDouble.getBalanceAmountWithDecimalAndCurrency(totalAmount),
			graphData: graphData,
			hasData: hasData
		};
	});
}

function getTxnTotalAmount(cardKey, options) {
	var dateFilter = options.dateFilter;

	var _getDateRangeFromFilt4 = getDateRangeFromFilterOption(dateFilter),
	    fromDate = _getDateRangeFromFilt4.fromDate,
	    toDate = _getDateRangeFromFilt4.toDate;

	var ipcAction = '';
	if (cardKey == 'otherIncome') {
		ipcAction = IPCActions.GET_OTHER_INCOME_AMOUNT;
	} else if (cardKey == 'purchaseBills') {
		ipcAction = IPCActions.GET_PURCHASE_AMOUNT;
	}
	return DBWindow.send(ipcAction, {
		fromDate: fromDate,
		toDate: toDate
	}).then(function (_ref8) {
		var _ref8$totalAmount = _ref8.totalAmount,
		    totalAmount = _ref8$totalAmount === undefined ? 0 : _ref8$totalAmount;

		var amount = '';
		var hasData = !!totalAmount;
		if (cardKey == 'otherIncome') {
			amount = MyDouble.getBalanceAmountWithDecimalAndCurrency(totalAmount, true);
		} else if (cardKey == 'purchaseBills') {
			amount = '<span style="color:#e35959">' + MyDouble.getAmountWithDecimalAndCurrency(totalAmount) + '</span>';
		}
		return {
			amount: amount,
			hasData: hasData
		};
	});
}
function getToPayDetails() {
	return DBWindow.send(IPCActions.GET_PAYABLE_AMOUNT).then(function (_ref9) {
		var totalAmount = _ref9.totalAmount;

		return {
			amount: MyDouble.getBalanceAmountWithDecimalAndCurrency(totalAmount),
			hasData: !!totalAmount
		};
	});
}
function getToReceiveDetails() {
	return DBWindow.send(IPCActions.GET_RECEIVABLE_AMOUNT).then(function (_ref10) {
		var totalAmount = _ref10.totalAmount;

		return {
			amount: MyDouble.getBalanceAmountWithDecimalAndCurrency(totalAmount),
			hasData: !!totalAmount
		};
	});
}
function getPreviouslyDisplayedCards() {
	var displayedDashboardCards = LocalStorageHelper.getValue('displayedDashboardCards');
	if (!displayedDashboardCards) {
		displayedDashboardCards = ['saleInvoices', 'purchaseBills', 'toReceive', 'expenses', 'cashInHand', 'stockValue'];
		LocalStorageHelper.setValue('displayedDashboardCards', (0, _stringify2.default)(displayedDashboardCards));
	} else {
		displayedDashboardCards = JSON.parse(displayedDashboardCards);
	}
	return displayedDashboardCards;
}
function getDateRangeFromFilterOption() {
	var dateFilter = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;

	dateFilter = Number(dateFilter);
	var fromDate = new Date();
	var toDate = new Date();
	switch (dateFilter) {
		case DATE_RANGES.THIS_MONTH:
			var _MyDate$getThisMonthF = MyDate.getThisMonthFromToDate();

			var _MyDate$getThisMonthF2 = (0, _slicedToArray3.default)(_MyDate$getThisMonthF, 2);

			fromDate = _MyDate$getThisMonthF2[0];
			toDate = _MyDate$getThisMonthF2[1];

			break;
		case DATE_RANGES.LAST_MONTH:
			var _MyDate$getLastMonthF = MyDate.getLastMonthFromToDate();

			var _MyDate$getLastMonthF2 = (0, _slicedToArray3.default)(_MyDate$getLastMonthF, 2);

			fromDate = _MyDate$getLastMonthF2[0];
			toDate = _MyDate$getLastMonthF2[1];

			break;
		case DATE_RANGES.THIS_QUARTER:
			var _MyDate$getThisQuarte = MyDate.getThisQuarterFromToDate();

			var _MyDate$getThisQuarte2 = (0, _slicedToArray3.default)(_MyDate$getThisQuarte, 2);

			fromDate = _MyDate$getThisQuarte2[0];
			toDate = _MyDate$getThisQuarte2[1];

			break;
		case DATE_RANGES.THIS_YEAR:
			var _MyDate$getThisYearFr = MyDate.getThisYearFromToDate();

			var _MyDate$getThisYearFr2 = (0, _slicedToArray3.default)(_MyDate$getThisYearFr, 2);

			fromDate = _MyDate$getThisYearFr2[0];
			toDate = _MyDate$getThisYearFr2[1];

			break;
		case DATE_RANGES.IN_7_DAYS:
			fromDate.setHours(0, 0, 0, 0);
			toDate.setDate(toDate.getDate() + 7);
			toDate.setHours(0, 0, 0, 0);
			break;
		case DATE_RANGES.IN_15_DAYS:
			fromDate.setHours(0, 0, 0, 0);
			toDate.setDate(toDate.getDate() + 15);
			toDate.setHours(0, 0, 0, 0);
			break;
		case DATE_RANGES.IN_1_MONTH:
			fromDate.setHours(0, 0, 0, 0);
			toDate.setDate(toDate.getDate() + 30);
			toDate.setHours(0, 0, 0, 0);
			break;

		default:
			break;
	}
	return {
		fromDate: fromDate,
		toDate: toDate
	};
}

function handleCardClick(cardKey, _ref11) {
	var _ref11$isGraph = _ref11.isGraph,
	    isGraph = _ref11$isGraph === undefined ? false : _ref11$isGraph,
	    _ref11$selectedDateFi = _ref11.selectedDateFilter,
	    selectedDateFilter = _ref11$selectedDateFi === undefined ? null : _ref11$selectedDateFi;

	var mount = require('../UIComponent/jsx/MountComponent').default;
	var CommonUtility = require('./../Utilities/CommonUtility');
	var StringConstants = require('../Constants/StringConstants');
	var component = null;
	switch (cardKey) {
		case 'saleInvoices':
			if (isGraph) {
				$('#defaultPage').load('saleGraphReport.html');
			} else {
				component = require('../UIComponent/jsx/SalePurchaseReportContainer').default;
				mount(component, document.getElementById('defaultPage'), {
					txnType: TxnTypeConstant.TXN_TYPE_SALE,
					includeReturnTransaction: false
				});
			}
			break;
		case 'toReceive':
			component = require('../UIComponent/jsx/PartiesContainer').default;
			mount(component, document.querySelector('#defaultPage'), {
				selectedFilter: 'To Receive'
			});
			break;
		case 'saleOrders':
			component = require('../UIComponent/jsx/OrdersContainer').default;
			mount(component, document.querySelector('#defaultPage'), {
				selectedTxnType: TxnTypeConstant.TXN_TYPE_SALE_ORDER,
				selectedStatusFilter: [StringConstants.orderOpen, StringConstants.orderOverdueString]
			});
			break;
		case 'deliveryChallans':
			component = require('../UIComponent/jsx/DeliveryChallanContainer').default;
			mount(component, document.querySelector('#defaultPage'), {
				selectedStatusFilter: StringConstants.deliveryChallanOpen
			});
			break;
		case 'purchaseBills':
			component = require('../UIComponent/jsx/SalePurchaseReportContainer').default;
			mount(component, document.querySelector('#defaultPage'), {
				txnType: TxnTypeConstant.TXN_TYPE_PURCHASE,
				includeReturnTransaction: false
			});
			break;
		case 'toPay':
			component = require('../UIComponent/jsx/PartiesContainer').default;
			mount(component, document.querySelector('#defaultPage'), {
				selectedFilter: 'To Pay'
			});
			break;
		case 'purchaseOrders':
			component = require('../UIComponent/jsx/OrdersContainer').default;
			mount(component, document.querySelector('#defaultPage'), {
				selectedTxnType: TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER,
				selectedStatusFilter: [StringConstants.orderOpen, StringConstants.orderOverdueString]
			});
			break;
		case 'expenses':
			if (isGraph) {
				$('#defaultPage').load('ExpenseGraphReport.html');
			} else {
				component = require('../UIComponent/jsx/ExpenseContainer').default;
				mount(component, document.querySelector('#defaultPage'));
			}
			break;
		case 'stockValue':
			component = require('../UIComponent/jsx/ItemsContainer').default;
			mount(component, document.querySelector('#defaultPage'));
			break;
		case 'lowStocks':
			CommonUtility.loadDefaultPage('ViewReports.html', function () {
				window.changeReportPage('LowStockSummaryReport.html');
			});
			break;
		case 'expiringStocks':
			CommonUtility.loadDefaultPage('ViewReports.html', function () {
				var callback = void 0;
				if (selectedDateFilter) {
					if (Number(selectedDateFilter) < DATE_RANGES.IN_7_DAYS) {
						selectedDateFilter = DATE_RANGES.IN_7_DAYS;
					}

					var _getDateRangeFromFilt5 = getDateRangeFromFilterOption(selectedDateFilter),
					    toDate = _getDateRangeFromFilt5.toDate;

					if (toDate) {
						callback = function callback() {
							var SettingCache = require('../Cache/SettingCache');
							var settingCache = new SettingCache();
							if (settingCache.getExpiryDateFormat() == StringConstants.monthYear) {
								$('#endDateExp').val(MyDate.getDate('m/y', toDate));
							} else {
								$('#endDateExp').val(MyDate.getDate('d/m/y', toDate));
							}
						};
					}
				}
				window.changeReportPage('ItemStockTrackingReport.html', callback);
			});
			break;
		case 'otherIncome':
			component = require('../UIComponent/jsx/OtherIncomeContainer').default;
			mount(component, document.querySelector('#defaultPage'));
			break;
		case 'cashInHand':
			component = require('../UIComponent/jsx/CashInHandContainer').default;
			mount(component, document.querySelector('#defaultPage'));
			break;
		case 'bankAccounts':
			component = require('../UIComponent/jsx/BankAccountsContainer').default;
			mount(component, document.querySelector('#defaultPage'));
			break;
		case 'openCheques':
			component = require('../UIComponent/jsx/ChequesContainer').default;
			var ChequeStatus = require('../Constants/ChequeStatus');
			mount(component, document.querySelector('#defaultPage'), {
				selectedStatusFilter: ChequeStatus.OPEN
			});
			break;
		case 'loanAccounts':
			component = require('../UIComponent/jsx/LoanAccountsContainer').default;
			mount(component, document.querySelector('#defaultPage'));
			break;
		default:
			break;
	}
}
function getIsPrivacyEnabled() {
	return DBWindow.send(IPCActions.GET_IS_PRIVACY_ENABLED).then(function (isPrivacyEnabled) {
		return isPrivacyEnabled;
	});
}
module.exports = {
	getEnabledDashboardCards: getEnabledDashboardCards,
	getCardDetails: function getCardDetails(cardKey, options) {
		switch (cardKey) {
			case 'saleInvoices':
				return getSaleDetails(options);
			case 'toReceive':
				return getToReceiveDetails();
			case 'saleOrders':
			case 'purchaseOrders':
			case 'deliveryChallans':
				return getOpenOrderDetails(cardKey, options);
			case 'otherIncome':
			case 'purchaseBills':
				return getTxnTotalAmount(cardKey, options);
			case 'toPay':
				return getToPayDetails();
			case 'expenses':
				return getExpenseDetails(options);
			case 'stockValue':
				return getStockValueDetails();
			case 'lowStocks':
				return getLowStockSummary();
			case 'expiringStocks':
				return getExpiringStocks(options);
			case 'cashInHand':
				return getCashInHandDetails();
			case 'bankAccounts':
				return getBankAccountDetails();
			case 'openCheques':
				return getOpenChequeDetails();
			case 'loanAccounts':
				return getLoanDetails();
			default:
				break;
		}
	},
	getFilterOptions: function getFilterOptions(cardKey) {
		var ranges = [DATE_RANGES.THIS_MONTH, DATE_RANGES.LAST_MONTH, DATE_RANGES.THIS_QUARTER, DATE_RANGES.THIS_YEAR];
		if (cardKey == 'expiringStocks') {
			ranges = [DATE_RANGES.IN_7_DAYS, DATE_RANGES.IN_15_DAYS, DATE_RANGES.IN_1_MONTH];
		}
		return ranges.map(function (range) {
			return {
				value: range,
				text: DATE_RANGES.labels[range]
			};
		});
	},
	getFilterOptionsForGraph: function getFilterOptionsForGraph(cardKey) {
		return [{ value: GRAPH_FILTER_OPTIONS.DAILY, text: GRAPH_FILTER_OPTIONS.labels[GRAPH_FILTER_OPTIONS.DAILY] }, { value: GRAPH_FILTER_OPTIONS.WEEKLY, text: GRAPH_FILTER_OPTIONS.labels[GRAPH_FILTER_OPTIONS.WEEKLY] }, { value: GRAPH_FILTER_OPTIONS.MONTHLY, text: GRAPH_FILTER_OPTIONS.labels[GRAPH_FILTER_OPTIONS.MONTHLY] }];
	},
	addToDisplayedCards: function addToDisplayedCards(cardKey) {
		var displayedDashboardCards = LocalStorageHelper.getValue('displayedDashboardCards');
		if (!displayedDashboardCards) {
			displayedDashboardCards = ['saleInvoices', 'purchaseBills', 'toReceive', 'expenses', 'cashInHand', 'stockValue'];
			LocalStorageHelper.setValue('displayedDashboardCards', (0, _stringify2.default)(displayedDashboardCards));
		} else {
			displayedDashboardCards = JSON.parse(displayedDashboardCards);
		}
		if (!displayedDashboardCards.includes(cardKey)) {
			displayedDashboardCards.push(cardKey);
			LocalStorageHelper.setValue('displayedDashboardCards', (0, _stringify2.default)(displayedDashboardCards));
		}
	},
	getIsPrivacyEnabled: getIsPrivacyEnabled,
	handleCardClick: handleCardClick
};