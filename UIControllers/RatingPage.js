var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
	var LocalStorageHelper = require('../Utilities/LocalStorageHelper');
	var clevertapID = LocalStorageHelper.getValue('clevertapID');
	var deviceId = LocalStorageHelper.getValue('device_id');
	var loginPhoneNumber = LocalStorageHelper.getValue('loginPhoneNumber');
	var isRatingSavedInServer = LocalStorageHelper.getValue('isRatingSavedInServer');
	var toBeHiddenItem = document.querySelector('#textBeforeRating');
	var toBeSHownItem = document.querySelector('#textAfterRating');
	var ratingData = LocalStorageHelper.getValue('ratingData');
	var Domain = require('./../Constants/Domain');
	var domain = Domain.thisDomain;

	var _require = require('electron'),
	    shell = _require.shell;

	var userReviews = [{ userName: 'Rhajiv Marmeto', review: 'Perfect Invoice & Billing App! I just want to thank the whole Vyapar team for making this possible. The app and desktop version works perfectly. It is very user friendly, easy to understand and navigate. Every feature is so useful that it can make life easier and organized. Wonderful, Wonderful!' }, { userName: 'Vipul K V', review: 'Used it for about 3 days now after being a premium lifetime member. probably the best decision. Makes everything so easier and UI is the simplest among all other similar apps. The staff who are not well versed with computers or phones also find it user friendly.' }, { userName: 'Srishti Purohit', review: 'Super App. This is so simple and easy to use. Thank you to the Vyapar team. I am a freelancer and I mostly use it to create estimates and tax invoices. This app helps me keep track of all my bills and expenses.' }, { userName: 'Kaushalaya Purohit', review: 'Nice app for small businesses. I tried other billing and accounting applications in the market, but they were either very difficult to use for a beginner or very costly. Vyapar fits the bill and is both easy to operate and has all the basic features that one would need in a business.' }, { userName: 'Hefraz Hezbullah', review: "I'm not very proficient in accounting and have been struggling with inventory and sales for my small business. Tested quite a few apps and deleted them within a short period. Then Vyapar came along and it was a game changer. The features are simply amazing, everything has been thought of and the app." }, { userName: 'Prosenjit Chakraborty', review: "It's a very user friendly and very useful app. You can maintain your total billing process and it can be accessed for anywhere. I would especially like to thank the developers for such applications." }, { userName: 'Chantel Bernal', review: "Pleasantly surprised by this app..it does everything I need! I'm a Color Street Stylist and was looking for an app to keep track of inventory and color Cash prizes. I tried several different apps but this one does everything I need and helps make my small business look so professional." }, { userName: 'Ayushi Joshi', review: 'Best software accounting application, The UI is amazing hence users can be easily guided by the application and tutorials are available for further references. Also the data is highly secured. Amazing application, it helped a lot.' }, { userName: 'Vinod Saini', review: 'This is a fantastic aap. You have like a Software so you can manage every data. I use this app. So easy to work. Technical team is very sportive.if you search for the same app, definitely try it. I manage the whole data with this app very easily.' }, { userName: 'Panchanan Panda', review: "Very good App This app is user friendly and fantastic feature output. All input and output reports are suitable for businessmen. Presently I think it's good for all business entrepreneurs." }, { userName: 'Arutun Frieda Daisy', review: 'This app just simplified my life and accounting. Keeps my business on check, I like the payment reminders, d total sales of the week, month etc.' }, { userName: 'Eswar Jagu', review: "It's a great app for small business invoice and estimate apps and highly recommended to all my friends. And appreciate the developer." }, { userName: 'Jesmon Joy', review: "This app is absolutely awesome for editing and work....The Billing structure and sections can be edited personally and the field can be defined as we wish... It's really doing a great job... Makes work easy. All safe in 1 hand." }, { userName: 'Navdeep', review: 'This app is generally for all types of #Vyaparis GST / Non GST The best thing is that this app had no charges which is best.' }, { userName: 'Ram Singh', review: 'Perfect for small to medium businesses, your complete inventory system on your mobile, excellent job on this app.' }, { userName: 'Jaidev Milan', review: 'Very simple to use, with a very effective outcome. It has all the necessary options to write up a professionally formatted invoice, applicable to most independent / freelancing services out there.' }];
	var randomUserIndex = Math.ceil(Math.random() * (userReviews.length - 1));
	var selectedRatingvalue = null;
	if (randomUserIndex === 0) {
		randomUserIndex++;
	}
	var firstUserName = userReviews[randomUserIndex].userName;
	var firstUserReview = userReviews[randomUserIndex].review;
	var secondUserName = userReviews[randomUserIndex - 1].userName;
	var secondUserReview = userReviews[randomUserIndex - 1].review;
	document.querySelector('.userFirst').innerHTML = firstUserName;
	document.querySelector('.paraFirst').innerHTML = firstUserReview;
	document.querySelector('.userSecond').innerHTML = secondUserName;
	document.querySelector('.paraSecond').innerHTML = secondUserReview;
	var request = require('request');
	var isOnline = require('is-online');
	var options = {
		method: 'POST',
		url: domain + '/api/app-feedback',
		headers: {
			'Content-Type': 'application/json',
			Accept: 'application/json'
		}
	};

	if (ratingData && isRatingSavedInServer == 'true') {
		toBeHiddenItem.classList.add('hide');
		toBeSHownItem.classList.remove('hide');
	} else if (ratingData && isRatingSavedInServer != 'true') {
		toBeHiddenItem.classList.add('hide');
		toBeSHownItem.classList.remove('hide');
		options.json = (0, _extends3.default)({}, JSON.parse(ratingData));
		isOnline().then(function (online) {
			if (online) {
				request(options, function (error, response) {
					if (error) return;
					if (response.statusCode == 200) {
						LocalStorageHelper.setValue(isRatingSavedInServer, true);
					}
				});
			}
		});
	}
	function setStarsActive(value) {
		document.querySelectorAll('.ratingValue').forEach(function (item) {
			if (item.value <= value) {
				item.checked = true;
			} else {
				item.checked = false;
			}
		});
	}
	function handleStarsOnHover(event) {
		var ratingValue = event.target.dataset.id;
		setStarsActive(ratingValue);
	}
	function handleStarsMouseOut() {
		setStarsActive(selectedRatingvalue);
	}
	function saveDataToLocalStorage(serverStatus, feedBackData) {
		LocalStorageHelper.setValue('isRatingSavedInServer', serverStatus);
		LocalStorageHelper.setValue('ratingData', (0, _stringify2.default)(feedBackData));
		ToastHelper.success('Thanks for rating us.');
	}

	function saveFeeedBackToServer(data, feedBackData) {
		isOnline().then(function (online) {
			if (!online) {
				saveDataToLocalStorage(false, feedBackData);
			} else {
				request(data, function (error, response) {
					if (error) return;
					if (response.statusCode == 200) {
						saveDataToLocalStorage(true, feedBackData);
					}
				});
			}
		});
	}

	function handleRating(event) {
		{
			var ratingValue = event.currentTarget.value;
			selectedRatingvalue = ratingValue;
			var _ratingData = {
				clevertapID: clevertapID,
				deviceId: deviceId,
				loginPhoneNumber: loginPhoneNumber,
				platform: 2,
				ratingValue: ratingValue
			};
			var lowerHalfDiv = document.querySelector('#lowerHalf');
			if (ratingValue < 5) {
				lowerHalfDiv.removeAttribute('class', 'hide');
				setStarsActive(ratingValue);
				var reasonForLessRating = [];
				var submitButton = document.querySelector('.submitButton');
				submitButton.addEventListener('click', function (event) {
					document.querySelectorAll('.defaultReasonsCheckBox').forEach(function (item) {
						if (item.checked == true) {
							reasonForLessRating.push(item.value);
						}
					});
					if (document.querySelector('.descriptionCheckBox').checked == true) {
						reasonForLessRating.push(document.querySelector('#descriptionTextBox').value);
					}
					_ratingData.comments = [].concat(reasonForLessRating);
					toBeHiddenItem.classList.add('hide');
					lowerHalfDiv.classList.add('hide');
					toBeSHownItem.classList.remove('hide');
					options.json = (0, _extends3.default)({}, _ratingData);
					saveFeeedBackToServer(options, _ratingData);
				});
			} else if (ratingValue == 5) {
				document.querySelectorAll('.ratingValue').forEach(function (item) {
					item.checked = true;
				});
				lowerHalfDiv.classList.add('hide');
				_ratingData.comments = [];
				toBeHiddenItem.classList.add('hide');
				toBeSHownItem.classList.remove('hide');
				options.json = (0, _extends3.default)({}, _ratingData);
				saveFeeedBackToServer(options, _ratingData);
			}
		}
	}
	$(document).off('mouseover', '.starIcon').on('mouseover', '.starIcon', handleStarsOnHover);
	$(document).off('mouseout', '.starIcon').on('mouseout', '.starIcon', handleStarsMouseOut);
	$(document).off('click', '.ratingValue').on('click', '.ratingValue', handleRating);
	$(document).off('click', '#moreReviewButton').on('click', '#moreReviewButton', function (event) {
		shell.openItem('https://play.google.com/store/apps/details?id=in.android.vyapar&hl=en_IN');
	});
	$(document).off('change', '.descriptionCheckBox').on('change', '.descriptionCheckBox', function (event) {
		var inputBox = document.querySelector('#descriptionTextBox');
		if (document.querySelector('.descriptionCheckBox').checked == true) {
			inputBox.classList.remove('hide');
		} else {
			inputBox.classList.add('hide');
		}
	});
})();