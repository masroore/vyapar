var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* global jQuery */
module.exports = function UserDefinedFieldsController1($) {
	var ErrorCode = require('./../Constants/ErrorCode.js');
	var SettingCache = require('./../Cache/SettingCache');
	var UDFCache = require('./../Cache/UDFCache.js');
	var uDFCache = new UDFCache();
	var Queries = require('./../Constants/Queries');
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant');
	var UDFConstants = require('./../Constants/UDFFieldsConstant.js');
	var FirmCache = require('./../Cache/FirmCache');
	var firmCache = new FirmCache();
	var udfCurrentFirmId = 1;
	var settingCache = void 0;
	var theme = 1;
	var UDFModel = require('./../BizLogic/UDFModel.js');
	var udfTxnTypeMap = new _map2.default();
	var udfModelMap = new _map2.default();
	var udfValueArray = [];
	var TransactionHTMLGenerator = require('./../ReportHTMLGenerator/TransactionHTMLGenerator.js');
	var transactionHTMLGenerator = new TransactionHTMLGenerator();
	var TransactionFactory = require('./../BizLogic/TransactionFactory');
	var transactionFactory = new TransactionFactory();
	var InvoiceTheme = require('./../Constants/InvoiceTheme.js');
	var txn = transactionFactory.getSampleTxn(TxnTypeConstant.TXN_TYPE_SALE);
	var htmlForThemePreview;
	var that;
	var StringConstant = require('./../Constants/StringConstants.js');
	var UserDefinedFieldsController2 = {

		defaults: {
			mountPoint: '',
			$el: ''
		},

		init: function init() {
			var defaults = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

			settingCache = new SettingCache();
			this.defaults = $.extend(this.defaults, defaults);
			udfCurrentFirmId = settingCache.getDefaultFirmId();
			this.render();
			this.defaults.$el = this.defaults.mountPoint.find('#extraFieldsContainer');
			htmlForThemePreview = transactionHTMLGenerator.getTransactionHTMLLayout(txn, InvoiceTheme.THEME_1, '#a3a3a3');
			$('#themePreviewDiv').html(htmlForThemePreview);
			udfTxnTypeMap = new _map2.default(uDFCache.getUdfActiveTransactionTypes());
			$('#htmlView').html(''); // closing preview leaves this html
			this.showFirmDropDownInExtraFieldsSetup();
			this.attachEvents();
			$('#headerFirmName').html(firmCache.getFirmNameById(Number(udfCurrentFirmId)));
			this.addHeaderPdfValues();
			this.addTxnPdfValues();
			udfValueArray = [];
			that = this;
			window.onResume = this.onResume.bind(this);
		},
		onResume: function onResume() {
			var notFromAutoSync = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

			if (!notFromAutoSync) {
				uDFCache.refreshUDF();
				udfCurrentFirmId = $('#selectFirmudf').val();
				udfModelMap = new _map2.default();
				udfValueArray = [];
				$('#firmUDF').html(this.getFirmExtraFields());
				$('#txnUDF').html(this.getTransactionExtraFields());
				$('#activeTxn').html(this.getTransactionCheckBox());
				$('#selectThemeudf').val(InvoiceTheme.THEME_1);
				this.changeThemePreviewForExtraFields(InvoiceTheme.THEME_1);
				this.fillInputs();
				this.addTxnPdfValues();
				this.addHeaderPdfValues();
				$('#headerFirmName').html(firmCache.getFirmNameById(Number(udfCurrentFirmId)));
			}
		},
		render: function render() {
			var html = this.getMainTemplate();
			var $mountPoint = this.defaults.mountPoint;
			$mountPoint.html(html);
			$mountPoint.dialog({
				width: 900,
				modal: true,
				close: function (e) {
					e.stopPropagation();
					this.defaults.$el.off('click', '**');
					$mountPoint.html('');
					$mountPoint.dialog('destroy');
				}.bind(this)
			});
			this.fillInputs();
		},
		addFieldForTheme9: function addFieldForTheme9() {
			var udfCache = new UDFCache();
			var txnMap = udfCache.getTxnExtraFields(Number(udfCurrentFirmId));
			var txnHeaderData = [];
			if (txnMap) {
				txnMap.forEach(function (value, key) {
					if ($('#enableBodyExtraField' + key).prop('checked') && $('#bodyShowInPrint' + key).prop('checked')) {
						txnHeaderData.push([$('#bodyF' + key + 'Name').val(), 'Value', key]);
					}
				});
			}
			var txnHeaderString = '';
			var extraCellNeeded;
			for (var i = 0; i < UDFConstants.TOTAL_EXTRA_FIELDS; i++) {
				$('#extraRow' + i).remove();
			}
			for (var _i = 0; _i < txnHeaderData.length; _i++) {
				var txnHeader = txnHeaderData[_i];
				var borderStyle = ' borderBottomForTxn ';
				var isEvenCell = _i % 2 == 0;
				if (isEvenCell) {
					borderStyle += ' borderRightForTxn ';
					txnHeaderString += '<tr id="extraRow' + _i + '">';
					extraCellNeeded = true;
				} else {
					extraCellNeeded = false;
				}
				txnHeaderString += "<td width='50%' class='" + borderStyle + "'><span id='tableFieldName" + txnHeader[2] + "' >" + txnHeader[0] + '</span><br/><b>' + txnHeader[1] + '</b></td>';
				if (!isEvenCell) {
					txnHeaderString += '</tr>';
				}
			}

			if (extraCellNeeded) {
				txnHeaderString += "<td width='50%' class='borderBottomForTxn'></td></tr>";
			}
			$('#txnHeader').append(txnHeaderString);
		},
		addTxnPdfValues: function addTxnPdfValues() {
			var themeColor = '#a3a3a3';
			var refTextColor = '#000000';
			if (theme == InvoiceTheme.THEME_2 || theme == InvoiceTheme.THEME_3) {
				refTextColor = themeColor;
				if (themeColor == '#ffffff') {
					refTextColor = 'black';
				}
			}

			var udfCache = new UDFCache();
			var txnMap = udfCache.getTxnExtraFields(Number(udfCurrentFirmId));
			var dateAndRefNoHTML = '';
			if (theme == InvoiceTheme.THEME_9) {
				this.addFieldForTheme9();
			} else {
				$('#txnPDFUDF').html('');
				var divMap = [];
				if (txnMap) {
					txnMap.forEach(function (value, key) {
						if ($('#enableBodyExtraField' + key).prop('checked') && $('#bodyShowInPrint' + key).prop('checked')) {
							dateAndRefNoHTML += "<p id='extra" + key + "' class='extra' align='right'  style='color:" + refTextColor + "'><span id='extraName" + key + "'>" + $('#bodyF' + key + 'Name').val() + ": </span><span id='extraValue" + key + "'>Value</p>";
						} else {
							dateAndRefNoHTML += "<p id='extra" + key + "' class='extra' align='right'  style='color:" + refTextColor + "'><span id='extraName" + key + "'></span><span id='extraValue" + key + "'></p>";
						}
					});
				}
				$('#txnPDFUDF').html(dateAndRefNoHTML);
			}
		},
		addHeaderPdfValues: function addHeaderPdfValues() {
			var firmModel = firmCache.getFirmById(udfCurrentFirmId);
			var alignment = 'center';
			var imageBlob = null;
			if (settingCache.isPrintLogoEnabled()) {
				imageBlob = firmModel.getFirmImagePath();
			}
			if (imageBlob && theme != InvoiceTheme.THEME_4) {
				alignment = 'right';
			}
			$('#uDFHeaderFields').html('');
			var udfCache = new UDFCache();
			var udfArray = firmModel.getUdfObjectArray();
			var headerDetails = '';
			var divMap = [];
			for (var i = 0; i < udfArray.length; i++) {
				divMap.push(i + 1);
				var key = i + 1;
				var udfValue = udfArray[i];
				var udfModel = udfCache.getUdfFirmModelByFirmAndFieldId(udfCurrentFirmId, udfValue.getUdfFieldId());
				var fieldNumber = udfModel.getUdfFieldNumber();
				if ($('#headerShowInPrint' + fieldNumber).prop('checked') && $('#enableHeaderExtraField' + fieldNumber).prop('checked')) {
					if (theme == InvoiceTheme.THEME_9) {
						headerDetails += "<p class='bigTextSize id='extraHeader" + udfModel.getUdfFieldNumber() + "'><span id='extraHeaderName" + udfModel.getUdfFieldNumber() + "'>" + $('#headerF' + udfModel.getUdfFieldNumber() + 'Name').val() + ": </span> <span id='extraHeaderValue" + udfModel.getUdfFieldNumber() + "'>" + $('#headerF' + udfModel.getUdfFieldNumber() + 'Value').val() + '</span></p>';
					} else {
						headerDetails += '<p align=' + alignment + " class='bigTextSize ' id='extraHeader" + udfModel.getUdfFieldNumber() + "'><span id='extraHeaderName" + udfModel.getUdfFieldNumber() + "'>" + $('#headerF' + udfModel.getUdfFieldNumber() + 'Name').val() + ": </span> <span id='extraHeaderValue" + udfModel.getUdfFieldNumber() + "'>" + $('#headerF' + udfModel.getUdfFieldNumber() + 'Value').val() + '</span></p>';
					}
				} else {
					if (theme == InvoiceTheme.THEME_9) {
						headerDetails += "<p class='bigTextSize' id='extraHeader" + udfModel.getUdfFieldNumber() + "'><span id='extraHeaderName" + udfModel.getUdfFieldNumber() + "'></span> <span id='extraHeaderValue" + udfModel.getUdfFieldNumber() + "'></span></p>";
					} else {
						headerDetails += '<p align=' + alignment + " class='bigTextSize ' id='extraHeader" + udfModel.getUdfFieldNumber() + "'><span id='extraHeaderName" + udfModel.getUdfFieldNumber() + "'></span> <span id='extraHeaderValue" + udfModel.getUdfFieldNumber() + "'></span></p>";
					}
				}
			}
			for (i = 1; i <= UDFConstants.TOTAL_FIRM_EXTRA_FIELDS; i++) {
				if (divMap.indexOf(i) == -1) {
					if ($('#enableHeaderExtraField' + i).prop('checked') && $('#headerShowInPrint' + i).prop('checked')) {
						headerDetails += '<p align=' + alignment + " class='bigTextSize ' id='extraHeader" + i + "'><span id='extraHeaderName" + i + "'>" + $('#headerF' + i + 'Name').val() + "</span> <span id='extraHeaderValue" + i + "'>" + $('#headerF' + i + 'Value').val() + '</span></p>';
					} else {
						headerDetails += '<p align=' + alignment + " class='bigTextSize ' id='extraHeader" + i + "'><span id='extraHeaderName" + i + "'></span> <span id='extraHeaderValue" + i + "'></span></p>";
					}
				}
			}
			$('#uDFHeaderFields').html(headerDetails);
		},
		showFirmDropDownInExtraFieldsSetup: function showFirmDropDownInExtraFieldsSetup() {
			$('#selectFirmudf').html('');
			if (settingCache.getMultipleFirmEnabled()) {
				var firmList = firmCache.getFirmList();
				var optionTemp = document.createElement('option');
				for (var i in firmList) {
					if (firmList.hasOwnProperty(i)) {
						var option = optionTemp.cloneNode();
						option.text = firmList[i].getFirmName();
						option.value = firmList[i].getFirmId();
						$('#selectFirmudf').append(option);
					}
				}
			} else {
				var _optionTemp = document.createElement('option');

				var firm = firmCache.getFirmById(settingCache.getDefaultFirmId());
				var _option = _optionTemp.cloneNode();
				_option.text = firm.getFirmName();
				_option.value = firm.getFirmId();
				$('#selectFirmudf').append(_option);
			}
			$('#selectFirmudf').val(settingCache.getDefaultFirmId());
		},
		fillInputs: function fillInputs() {
			var firmObj = firmCache.getFirmById(udfCurrentFirmId);
			var extraFieldObjectArray = firmObj.getUdfObjectArray();
			for (var j = 0; j < extraFieldObjectArray.length; j++) {
				var fieldModel = extraFieldObjectArray[j];
				var udfModel = uDFCache.getUdfFirmModelByFirmAndFieldId(firmObj.getFirmId(), fieldModel.getUdfFieldId());
				var i = udfModel.getUdfFieldNumber();
				$('#headerF' + i + 'Name').val(udfModel.getUdfFieldName());
				$('#headerF' + i + 'Value').val(fieldModel.getUdfFieldValue());
				if (udfModel.getUdfFieldPrintOnInvoice() == 1) {
					$('#extraHeaderName' + i).html(udfModel.getUdfFieldName() + ': ');
					$('#extraHeaderValue' + i).html(fieldModel.getUdfFieldValue());
				}
			}
			var udfTxnFieldsMap = new _map2.default(uDFCache.getTxnExtraFields(udfCurrentFirmId));
			udfTxnFieldsMap.forEach(function (value, key) {
				var txnfieldModel = value;
				var i = key;
				var bodyName = '#bodyF' + i + 'Name';
				$(bodyName).val(txnfieldModel.getUdfFieldName());
				if (i == UDFConstants.FIELD_FOR_DATE && txnfieldModel.getUdfFieldDataFormat() == StringConstant.monthYear) {
					$('#dateF1Value option[value="2"]').attr('selected', true);
				}
				if (txnfieldModel.getUdfFieldPrintOnInvoice() == 1) {
					$('#extraName' + i).html(txnfieldModel.getUdfFieldName() + ': ');
					$('#extraValue' + i).html('Value');
				}
			});
		},
		attachEvents: function attachEvents() {
			this.defaults.$el.on('change', '#selectFirmudf', this.changeHTML.bind(this));
			this.defaults.$el.on('change', '#selectThemeudf', this.changeTheme.bind(this));
			this.defaults.$el.on('change', '.enableHeaderExtraField', this.changeOnFirmCheckBox);
			this.defaults.$el.on('keydown', '.extraHeaderName', this.changePDFHeader);
			this.defaults.$el.on('keydown', '.extraHeaderValue', this.changePDFHeaderValue);
			this.defaults.$el.on('keydown', '.extraFieldName', this.changePDFFields);
			this.defaults.$el.on('change', '.headerShowInPrint', this.changePDFHeaderShowInPrint);
			this.defaults.$el.on('change', '.bodyShowInPrint', this.changePDFFieldShowInPrint);
			this.defaults.$el.on('change', '.enableExtraField', this.changeOnTxnCheckBox);
			this.defaults.$el.on('change', '.enableTxnType', this.changeOnTxn);
			this.defaults.$el.on('click', '#doneForUDFFields', this.saveData.bind(this));
		},
		changeTheme: function changeTheme() {
			theme = $('#selectThemeudf').val();
			this.changeThemePreviewForExtraFields(theme);
			$('#headerFirmName').html(firmCache.getFirmNameById(Number(udfCurrentFirmId)));
			this.addTxnPdfValues();
			this.addHeaderPdfValues();
		},
		changeOnTxn: function changeOnTxn() {
			var labelCheck = $(this).siblings().first();
			var activeTxnTypeMap = new _map2.default();
			if (udfTxnTypeMap.has(Number(udfCurrentFirmId))) {
				activeTxnTypeMap = udfTxnTypeMap.get(Number(udfCurrentFirmId));
			}

			if ($(this).prop('checked')) {
				labelCheck.removeClass('opacity6');
				activeTxnTypeMap.set($(this).data('txntype'), 1);
			} else {
				activeTxnTypeMap.set($(this).data('txntype'), 0);
				labelCheck.addClass('opacity6');
			}
			udfTxnTypeMap.set(Number(udfCurrentFirmId), activeTxnTypeMap);
		},
		changeOnTxnCheckBox: function changeOnTxnCheckBox() {
			var labelCheck = $(this).siblings().first();
			if ($(this).prop('checked')) {
				$('#bodyExFieldDiv' + $(this).data('value')).removeClass('hide');
				labelCheck.removeClass('opacity6');
				if ($('#bodyShowInPrint' + $(this).data('value')).prop('checked')) {
					$('#extraName' + $(this).data('value')).html($('#bodyF' + $(this).data('value') + 'Name').val() + ': ');
					$('#extraValue' + $(this).data('value')).html('Value');
				}
			} else {
				$('#bodyExFieldDiv' + $(this).data('value')).addClass('hide');
				labelCheck.addClass('opacity6');
				$('#extraName' + $(this).data('value')).html('');
				$('#extraValue' + $(this).data('value')).html('');
			}
			if (theme == InvoiceTheme.THEME_9) {
				that.addFieldForTheme9();
			}
		},
		changeOnFirmCheckBox: function changeOnFirmCheckBox() {
			var labelCheck = $(this).siblings().first();
			if ($(this).prop('checked')) {
				$('#headerExFieldDiv' + $(this).data('value')).removeClass('hide');
				labelCheck.removeClass('opacity6');
				if ($('#headerShowInPrint' + $(this).data('value')).prop('checked')) {
					$('#extraHeaderName' + $(this).data('value')).html($('#headerF' + $(this).data('value') + 'Name').val() + ': ');
					$('#extraHeaderValue' + $(this).data('value')).html($('#headerF' + $(this).data('value') + 'Value').val());
				}
			} else {
				$('#headerExFieldDiv' + $(this).data('value')).addClass('hide');
				labelCheck.addClass('opacity6');
				$('#extraHeaderName' + $(this).data('value')).html('');
				$('#extraHeaderValue' + $(this).data('value')).html('');
			}
		},
		changePDFFieldShowInPrint: function changePDFFieldShowInPrint() {
			var divNumber = $(this).data('value');
			if ($(this).prop('checked') && $('#bodyF' + divNumber + 'Name').val()) {
				$('#extraName' + divNumber).html($('#bodyF' + divNumber + 'Name').val() + ': ');
				$('#extraValue' + divNumber).html('Value');
			} else {
				$('#extraName' + divNumber).html('');
				$('#extraValue' + divNumber).html('');
			}
			if (theme == InvoiceTheme.THEME_9) {
				that.addFieldForTheme9();
			}
		},
		changePDFHeaderShowInPrint: function changePDFHeaderShowInPrint() {
			var divNumber = $(this).data('value');
			if ($(this).prop('checked') && $('#headerF' + divNumber + 'Name').val()) {
				$('#extraHeaderName' + divNumber).html($('#headerF' + divNumber + 'Name').val() + ': ');
				$('#extraHeaderValue' + divNumber).html($('#headerF' + divNumber + 'Value').val());
			} else {
				$('#extraHeaderName' + divNumber).html('');
				$('#extraHeaderValue' + divNumber).html('');
			}
		},
		changePDFFields: function changePDFFields() {
			var divNumber = $(this).data('value');
			if ($('#bodyShowInPrint' + divNumber).prop('checked')) {
				delay(function () {
					if (theme == InvoiceTheme.THEME_9) {
						$('#tableFieldName' + divNumber).html($('#bodyF' + divNumber + 'Name').val() + ': ');
					}
					$('#extraName' + divNumber).html($('#bodyF' + divNumber + 'Name').val() + ': ');
					$('#extraValue' + divNumber).html('Value');
				}, 500);
			}
		},
		changePDFHeaderValue: function changePDFHeaderValue() {
			var divNumber = $(this).data('value');
			if ($('#headerShowInPrint' + divNumber).prop('checked')) {
				delay(function () {
					$('#extraHeaderValue' + divNumber).html($('#headerF' + divNumber + 'Value').val());
				}, 500);
			}
		},
		changePDFHeader: function changePDFHeader() {
			var divNumber = $(this).data('value');
			if ($('#headerShowInPrint' + divNumber).prop('checked')) {
				delay(function () {
					$('#extraHeaderName' + divNumber).html($('#headerF' + divNumber + 'Name').val() + ': ');
				}, 500);
			}
		},
		changeHTML: function changeHTML() {
			udfCurrentFirmId = $('#selectFirmudf').val();
			udfModelMap = new _map2.default();
			udfValueArray = [];
			$('#firmUDF').html(this.getFirmExtraFields());
			$('#txnUDF').html(this.getTransactionExtraFields());
			$('#activeTxn').html(this.getTransactionCheckBox());
			$('#selectThemeudf').val(InvoiceTheme.THEME_1);
			this.changeThemePreviewForExtraFields(InvoiceTheme.THEME_1);
			this.fillInputs();
			this.addTxnPdfValues();
			this.addHeaderPdfValues();
			$('#headerFirmName').html(firmCache.getFirmNameById(Number(udfCurrentFirmId)));
		},
		saveData: function saveData() {
			this.createOnBasisOfFirmCheckBox();

			var allOkWithHeaderName = this.createOnBasisOfFirmName();

			this.createOnBasisOfFirmPrint();

			this.createOnBasisOfExtraFieldCheckBox();

			var allOkWithFieldName = this.createOnBasisOfBodyOfExtraField();

			if (!allOkWithFieldName) {
				return;
			}

			this.createOnBasisOfExtraFieldPrint();
			var isAnyTxnExtraFieldEnabled = $('.enableExtraField').prop('checked');
			var allOkWithTxnType = this.createUdfModelsBasedOnTxnType(isAnyTxnExtraFieldEnabled);
			var isOnlyHeaderExtraFieldEnabled = allOkWithHeaderName && !isAnyTxnExtraFieldEnabled && $('.enableHeaderExtraField').prop('checked');
			var isHeaderAndTxnExtraFieldEnabled = allOkWithHeaderName && allOkWithTxnType && allOkWithFieldName;

			if (isOnlyHeaderExtraFieldEnabled || isHeaderAndTxnExtraFieldEnabled) {
				var DataInserter = require('./../DBManager/DataInserter.js');
				var dataInserter = new DataInserter();
				var statusCode = dataInserter.insertUdfData(udfModelMap);
				if (statusCode == ErrorCode.ERROR_SETTING_SAVE_SUCCESS) {
					this.createOnBasisOfFirmValue();
					statusCode = dataInserter.insertUdfValue(udfValueArray, udfCurrentFirmId);
					if (statusCode == ErrorCode.ERROR_SETTING_SAVE_SUCCESS) {
						ToastHelper.success(statusCode);
					} else {
						ToastHelper.error(statusCode);
					}
				} else {
					ToastHelper.error(statusCode);
				}
				firmCache.refreshFirmCache();
				$('#extraFieldsSetupDialog').dialog('destroy');
				$('#loading').hide();
			}
		},
		changeThemePreviewForExtraFields: function changeThemePreviewForExtraFields(theme) {
			var themeColor = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '#a3a3a3';

			var summary = '';
			txn.setFirmId(udfCurrentFirmId);
			summary = transactionHTMLGenerator.getTransactionHTMLLayout(txn, theme, themeColor);
			// summary = summary.replace(/padding: 30px 45px;}/g, '}');
			$('#themePreviewDiv').html(summary);
			this.addTxnPdfValues();
			this.addHeaderPdfValues();
		},
		createOnBasisOfFirmCheckBox: function createOnBasisOfFirmCheckBox() {
			for (var i = 1; i <= UDFConstants.TOTAL_FIRM_EXTRA_FIELDS; i++) {
				var key = 'firm_' + udfCurrentFirmId.toString() + 'field_' + i;

				if (udfModelMap.has(key)) {
					var uDFModel = udfModelMap.get(key);
				} else {
					uDFModel = new UDFModel();
				}

				if ($('#enableHeaderExtraField' + i).prop('checked')) {
					uDFModel.setUdfFieldStatus(1);
				} else {
					uDFModel.setUdfFieldStatus(0);
				}
				uDFModel.setUdfFieldType(UDFConstants.FIELD_TYPE_FIRM);
				uDFModel.setUdfFirmId(udfCurrentFirmId);
				uDFModel.setUdfFieldNuber(i);
				udfModelMap.set(key, uDFModel);
			}
		},
		createOnBasisOfFirmName: function createOnBasisOfFirmName() {
			for (var i = 1; i <= UDFConstants.TOTAL_FIRM_EXTRA_FIELDS; i++) {
				if ($('#enableHeaderExtraField' + i).prop('checked')) {
					if ($('#headerF' + i + 'Name').val() == '' || !$('#headerF' + i + 'Name').val().trim().length) {
						ToastHelper.error('Field Name is mandatory for selected additional fields.');
						$('#loading').hide();
						return 0;
					}
				}
			}
			for (i = 1; i <= UDFConstants.TOTAL_FIRM_EXTRA_FIELDS; i++) {
				var key = 'firm_' + udfCurrentFirmId.toString() + 'field_' + i;
				if (udfModelMap.has(key)) {
					var uDFModel = udfModelMap.get(key);
				} else {
					uDFModel = new UDFModel();
				}
				var fieldName = $('#headerF' + i + 'Name').val();
				uDFModel.setUdfFieldName(fieldName);
				uDFModel.setUdfFieldType(UDFConstants.FIELD_TYPE_FIRM);
				uDFModel.setUdfFirmId(udfCurrentFirmId);
				uDFModel.setUdfFieldNuber(i);
				udfModelMap.set(key, uDFModel);
			}
			return 1;
		},
		createOnBasisOfFirmPrint: function createOnBasisOfFirmPrint() {
			for (var i = 1; i <= UDFConstants.TOTAL_FIRM_EXTRA_FIELDS; i++) {
				var key = 'firm_' + udfCurrentFirmId.toString() + 'field_' + i;

				if (udfModelMap.has(key)) {
					var uDFModel = udfModelMap.get(key);
				} else {
					uDFModel = new UDFModel();
				}

				if ($('#headerShowInPrint' + i).prop('checked')) {
					uDFModel.setUdfFieldPrintOnInvoice(1);
				} else {
					uDFModel.setUdfFieldPrintOnInvoice(0);
				}
				uDFModel.setUdfFieldType(UDFConstants.FIELD_TYPE_FIRM);
				uDFModel.setUdfFirmId(udfCurrentFirmId);
				uDFModel.setUdfFieldNuber(i);
				udfModelMap.set(key, uDFModel);
			}
		},
		createOnBasisOfFirmValue: function createOnBasisOfFirmValue() {
			var UDFValueModel = require('./../Models/udfValueModel.js');
			var firmMap = uDFCache.getFirmFieldsMap();
			var specificFirmMap = firmMap.get(Number(udfCurrentFirmId));
			specificFirmMap.forEach(function (value, key) {
				var fieldModel = value;
				if (fieldModel && fieldModel.getUdfFieldStatus() == 1) {
					var fieldSequence = fieldModel.getUdfFieldNumber();
					var udfValueObj = new UDFValueModel();
					var val = $('#headerF' + fieldSequence + 'Value').val();
					udfValueObj.setUdfFieldValue(val);
					udfValueObj.setUdfFieldId(fieldModel.getUdfFieldId());
					udfValueObj.setUdfRefId(fieldModel.getUdfFirmId());
					udfValueObj.setUdfFieldType(UDFConstants.FIELD_TYPE_FIRM);
					udfValueArray.push(udfValueObj);
				}
			});
			firmCache.refreshFirmCache();
		},
		createOnBasisOfExtraFieldCheckBox: function createOnBasisOfExtraFieldCheckBox() {
			for (var x = 1; x <= UDFConstants.TOTAL_EXTRA_FIELDS; x++) {
				var key = 'txn_' + udfCurrentFirmId.toString() + 'field_' + x;
				if (udfModelMap.has(key)) {
					var uDFModel = udfModelMap.get(key);
				} else {
					uDFModel = new UDFModel();
				}
				if ($('#enableBodyExtraField' + x).prop('checked')) {
					uDFModel.setUdfFieldStatus(1);
				} else {
					uDFModel.setUdfFieldStatus(0);
				}
				uDFModel.setUdfFieldType(UDFConstants.FIELD_TYPE_TRANSACTION);
				uDFModel.setUdfFirmId(udfCurrentFirmId);
				uDFModel.setUdfFieldNuber(x);
				udfModelMap.set(key, uDFModel);
			}
		},
		createOnBasisOfBodyOfExtraField: function createOnBasisOfBodyOfExtraField() {
			for (var i = 1; i <= UDFConstants.TOTAL_EXTRA_FIELDS; i++) {
				if ($('#enableBodyExtraField' + i).prop('checked')) {
					if ($('#bodyF' + i + 'Name').val() == '' || !$('#bodyF' + i + 'Name').val().trim().length) {
						ToastHelper.error('Field Name is mandatory for selected additional fields.');
						$('#loading').hide();
						return 0;
					}
				}
			}
			for (var x = 1; x <= UDFConstants.TOTAL_EXTRA_FIELDS; x++) {
				var key = 'txn_' + udfCurrentFirmId.toString() + 'field_' + x;
				if (udfModelMap.has(key)) {
					var uDFModel = udfModelMap.get(key);
				} else {
					uDFModel = new UDFModel();
				}
				var fieldValue = $('#bodyF' + x + 'Name').val();
				uDFModel.setUdfFieldName(fieldValue);
				uDFModel.setUdfFieldType(UDFConstants.FIELD_TYPE_TRANSACTION);
				uDFModel.setUdfFirmId(udfCurrentFirmId);
				uDFModel.setUdfFieldNuber(x);
				if (x == UDFConstants.FIELD_FOR_DATE) {
					uDFModel.setUdfFieldDataType(UDFConstants.DATA_TYPE_DATE);
					uDFModel.setUdfFieldDataFormat($('#dateF1Value').val());
				}
				udfModelMap.set(key, uDFModel);
			}
			return 1;
		},
		createOnBasisOfExtraFieldPrint: function createOnBasisOfExtraFieldPrint() {
			for (var x = 1; x <= UDFConstants.TOTAL_EXTRA_FIELDS; x++) {
				var key = 'txn_' + udfCurrentFirmId.toString() + 'field_' + x;

				if (udfModelMap.has(key)) {
					var uDFModel = udfModelMap.get(key);
				} else {
					uDFModel = new UDFModel();
				}

				if ($('#bodyShowInPrint' + x).prop('checked')) {
					uDFModel.setUdfFieldPrintOnInvoice(1);
				} else {
					uDFModel.setUdfFieldPrintOnInvoice(0);
				}

				uDFModel.setUdfFieldType(UDFConstants.FIELD_TYPE_TRANSACTION);
				uDFModel.setUdfFirmId(udfCurrentFirmId);
				uDFModel.setUdfFieldNuber(x);
				udfModelMap.set(key, uDFModel);
			}
		},
		createUdfModelsBasedOnTxnType: function createUdfModelsBasedOnTxnType() {
			var canShowError = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

			var returnVal = 0;
			var mkey = [];
			for (var i = 1; i <= UDFConstants.TOTAL_EXTRA_FIELDS; i++) {
				mkey[i] = 'txn_' + udfCurrentFirmId.toString() + 'field_' + i;
			}
			var activeTxnTypeMap = new _map2.default();
			if (udfTxnTypeMap.has(Number(udfCurrentFirmId))) {
				activeTxnTypeMap = udfTxnTypeMap.get(Number(udfCurrentFirmId));
			}
			if (activeTxnTypeMap.size == 0) {
				if ($('#txnUDF input:checkbox:checked').length > 0) {
					if (canShowError) {
						ToastHelper.error('Select atleast one transaction type to display additional fields.');
					}
					$('#loading').hide();
					return 0;
				}
			}
			if (activeTxnTypeMap && activeTxnTypeMap.size > 0) {
				activeTxnTypeMap.forEach(function (value, key) {
					if (value == 1) {
						returnVal = 1;
					}
				});
				activeTxnTypeMap.forEach(function (value, key) {
					var newKey = [];
					for (var i = 1; i <= UDFConstants.TOTAL_EXTRA_FIELDS; i++) {
						newKey[i] = mkey[i] + '_txntype_' + key;
						if (udfModelMap.has(mkey[i])) {
							var mModel = udfModelMap.get(mkey[i]);
							var udfModelSale = new UDFModel({
								udfFieldName: mModel.getUdfFieldName(),
								udfFieldType: mModel.getUdfFieldType(),
								udfFieldDataType: mModel.getUdfFieldDataType(),
								udfFieldDataFormat: mModel.getUdfFieldDataFormat(),
								udfPrintOnInvoice: mModel.getUdfFieldPrintOnInvoice(),
								udfTxnType: mModel.getUdfTxnType(),
								udfFirmId: mModel.getUdfFirmId(),
								udfFieldNumber: mModel.getUdfFieldNumber(),
								udfFieldStatus: mModel.getUdfFieldStatus()
							});
						} else {
							udfModelSale = null;
						}
						if (udfModelSale) {
							if (value == 0) {
								udfModelSale.setUdfFieldStatus(0);
							}
							udfModelSale.setUdfTxnType(key);
							udfModelMap.set(newKey[i], udfModelSale);
						}
					}
				});
				for (i = 1; i <= UDFConstants.TOTAL_EXTRA_FIELDS; i++) {
					udfModelMap.delete(mkey[i]);
				}
			}
			if (returnVal == 0) {
				returnVal = 1;
				for (i = 1; i <= UDFConstants.TOTAL_EXTRA_FIELDS; i++) {
					if ($('#enableBodyExtraField' + i).prop('checked')) {
						returnVal = 0;
					}
				}
				if (returnVal == 0) {
					ToastHelper.error('Please choose atleast one transaction type');
					$('#loading').hide();
				}
			}
			return returnVal;
		},
		getMainTemplate: function getMainTemplate() {
			return '\n\t\t\t<div class="row width100 udfFields" id="extraFieldsContainer">\n\t\t\t<div class="col-4">\n\t\t\t\t<div id="selectFirmParentDiv" class="col-12">\n\t\t\t\t\t<label for="selectFirmudf" id="labelForSelectFirmudf">Select Firm</label>\n\t\t\t\t\t<select id="selectFirmudf"></select>\n\t\t\t\t</div>\n\t\t\t\t<div id="headerFieldsDiv" class="col-12"> \n\t\t\t\t\t<span id="addFieldHeader" class="addField">Firm additional fields</span> \n\t\t\t\t\t<span title="Add extra fields to your firm details. These can be printed on transaction PDFs below firm name." class="udfToolTipImg"> &#63 </span>\n\t\t\t\t\t<div id="firmUDF"> \n\t\t\t\t\t' + this.getFirmExtraFields() + '\n\t\t\t\t\t</div>\n\t\t\t\t\t<span id="addFieldBody" class="addField">Transaction additional fields</span><span class="udfToolTipImg" title="Add extra fields to track more information for transaction. These can be printed on transaction PDFs."> &#63 </span>\n\t\t\t\t\t<div id="txnUDF"> \n\t\t\t\t\t' + this.getTransactionExtraFields() + '\n\t\t\t\t\t</div>\n\t\t\t\t\t<span id="addFieldTxnType" class="addField">Show fields on</span>\n\t\t\t\t\t<div id="activeTxn"> \n\t\t\t\t\t' + this.getTransactionCheckBox() + '\n\t\t\t\t\t</div>\n\t\t\t\t\t\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class="col-8">\n\t\t\t  <div id="selectThemeParentDiv" class="col-12">\n\t\t\t\t<label for="selectThemeudf" id="labelForSelectThemeudf"></label>\n\t\t\t\t<select id="selectThemeudf">\n\t\t\t\t  <option value="1">Theme 1</option>\n\t\t\t\t  <option value="2">Theme 2</option>\n\t\t\t\t  <option value="3">Theme 3</option>\n\t\t\t\t  <option value="4">Theme 4</option>\n\t\t\t\t  <option value="5">GST Theme 1</option>\n\t\t\t\t  <option value="6">GST Theme 2</option>\n\t\t\t\t  <option value="7">GST Theme 3</option>\n\t\t\t\t  <option value="8">GST Theme 4</option>\n\t\t\t\t  <option value="9">GST Theme 5</option>\n\t\t\t\t</select>\n\t\t\t  </div>\n\t\t\t  <div id="themePreviewDiv" class="col-12 themeDiv">\n  \n\t\t\t  </div>\n\t\t\t</div>\n\t\t\t<div class="dialogButton floatRight">\n\t\t<button id="doneForUDFFields" class="terminalButton">Done</button>\n\t  </div>\n\t\t</div>\t  \n\t\t\t';
		},
		getTransactionExtraFields: function getTransactionExtraFields() {
			var udfTxnFieldsMap = new _map2.default(uDFCache.getTxnExtraFields(udfCurrentFirmId));
			var htmlString = '';
			udfTxnFieldsMap.forEach(function (value, key) {
				var txnfieldModel = value;
				var i = key;
				htmlString += '<div class="md-checkbox">';
				if (txnfieldModel.getUdfFieldStatus() == 1) {
					htmlString += '<input id="enableBodyExtraField' + i + '" type="checkbox" class="enableExtraField" data-value="' + i + '" checked>\n\t\t\t\t\t<label tabIndex="0" for="enableBodyExtraField' + i + '" class="udfexFieldCheckBoxLabel">Additional Field ' + i + '</label>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div id="bodyExFieldDiv' + i + '" class="extFieldsDiv">\n\t\t\t\t<label for="bodyF' + i + 'Name" class="headerFieldLabel">Field Name<span class="fontColorRed"> *</span></label>\n\t\t\t\t<input type="text" id="bodyF' + i + 'Name" data-value="' + i + '" maxlength="30" class="headerFields extraFieldName" placeholder="Enter Field Name">';
				} else {
					htmlString += '<input id="enableBodyExtraField' + i + '" type="checkbox" class="enableExtraField" data-value="' + i + '">';
					if (i == UDFConstants.FIELD_FOR_DATE) {
						htmlString += '<label tabIndex="0" for="enableBodyExtraField' + i + '" class="udfexFieldCheckBoxLabel opacity6">Date Field</label>';
					} else {
						htmlString += '<label tabIndex="0" for="enableBodyExtraField' + i + '" class="udfexFieldCheckBoxLabel opacity6">Additional Field ' + i + '</label>';
					}
					htmlString += '</div>\n\t\t\t<div id="bodyExFieldDiv' + i + '" class="extFieldsDiv hide">\n\t\t\t\t<label for="bodyF' + i + 'Name" class="headerFieldLabel">Field Name<span class="fontColorRed"> *</span></label>\n\t\t\t\t<input type="text" id="bodyF' + i + 'Name" data-value="' + i + '" maxlength="30" class="headerFields extraFieldName" placeholder="Enter Field Name">';
				}
				if (i == UDFConstants.FIELD_FOR_DATE) {
					htmlString += '<label for="dateF1Value" class="headerFieldLabel">Date Format</label>\n\t\t\t\t\t<select id="dateF1Value" ' + (settingCache.isCurrentCountryNepal() ? 'disabled' : '') + '>\n\t\t\t\t\t\t<option value=\'1\'>dd/mm/yy</option>\n\t\t\t\t\t\t<option value=\'2\'>mm/yyyy</option>\n\t\t\t\t\t</select>';
				}
				htmlString += '<div class="udfShowInPrintDiv">\n\t\t\t\t\t<span class="udfShowInPrintSpan">Show in print</span>\n\t\t\t\t\t<label class="UDFswitch UDFtoggleSwitch">';
				if (txnfieldModel.getUdfFieldPrintOnInvoice() == 1) {
					htmlString += '<input type="checkbox" id="bodyShowInPrint' + i + '" class="bodyShowInPrint" data-value="' + i + '" checked>';
				} else {
					htmlString += '<input type="checkbox" id="bodyShowInPrint' + i + '" class="bodyShowInPrint" data-value="' + i + '">';
				}
				htmlString += '<span class="UDFslider round"></span>\n\t\t\t\t\t\t</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>';
			});
			return htmlString;
		},
		getTransactionCheckBox: function getTransactionCheckBox() {
			var udfTxnFieldsMap = new _map2.default(uDFCache.getActiveTxnType(udfCurrentFirmId));
			var htmlString = '';
			var txnTypes = [{
				type: TxnTypeConstant.TXN_TYPE_SALE,
				id: Queries.SETTING_TXN_MESSAGE_ENABLED_SALE,
				label: 'Sales'
			}, {
				type: TxnTypeConstant.TXN_TYPE_PURCHASE,
				id: Queries.SETTING_TXN_MESSAGE_ENABLED_PURCHASE,
				label: 'Purchase'
			}, {
				type: TxnTypeConstant.TXN_TYPE_CASHIN,
				id: Queries.SETTING_TXN_MESSAGE_ENABLED_CASHIN,
				label: 'Payment In'
			}, {
				type: TxnTypeConstant.TXN_TYPE_CASHOUT,
				id: Queries.SETTING_TXN_MESSAGE_ENABLED_CASHOUT,
				label: 'Payment Out'
			}, {
				type: TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN,
				id: Queries.SETTING_TXN_MESSAGE_ENABLED_PURCHASERETURN,
				label: 'Debit Note'
			}, {
				type: TxnTypeConstant.TXN_TYPE_SALE_RETURN,
				id: Queries.SETTING_TXN_MESSAGE_ENABLED_SALERETURN,
				label: 'Credit Note'
			}];

			if (settingCache.isDeliveryChallanEnabled()) {
				txnTypes.push({
					type: TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN,
					id: Queries.SETTING_TXN_MESSAGE_ENABLED_DELIVERY_CHALLAN,
					label: 'Delivery Challan'
				});
			}
			if (settingCache.isEstimateEnabled()) {
				txnTypes.push({
					type: TxnTypeConstant.TXN_TYPE_ESTIMATE,
					id: Queries.SETTING_TXN_MESSAGE_ENABLED_ESTIMATE_FORM,
					label: 'Estimate'
				});
			}
			if (settingCache.isOrderFormEnabled()) {
				txnTypes.push({
					type: TxnTypeConstant.TXN_TYPE_SALE_ORDER,
					id: Queries.SETTING_TXN_MESSAGE_ENABLED_SALE_ORDER,
					label: 'Sale Order'
				});
				txnTypes.push({
					type: TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER,
					id: Queries.SETTING_TXN_MESSAGE_ENABLED_PURCHASE_ORDER,
					label: 'Purchase Order'
				});
			}
			for (var i = 0; i < txnTypes.length; i++) {
				if (udfTxnFieldsMap.get(Number(txnTypes[i].type)) == 1) {
					htmlString += '<div class="md-checkbox">\n\t\t\t\t\t<input id="enableTxnType' + txnTypes[i].type + '" class="enableTxnType" data-txnType="' + txnTypes[i].type + '" type="checkbox" checked>\n\t\t\t\t\t<label tabIndex="0" for="enableTxnType' + txnTypes[i].type + '" class="udfexFieldCheckBoxLabel ">' + txnTypes[i].label + '</label>\n\t\t\t\t</div>';
				} else {
					htmlString += '<div class="md-checkbox">\n\t\t\t\t\t<input id="enableTxnType' + txnTypes[i].type + '" class="enableTxnType" data-txnType="' + txnTypes[i].type + '" type="checkbox">\n\t\t\t\t\t<label tabIndex="0" for="enableTxnType' + txnTypes[i].type + '" class="udfexFieldCheckBoxLabel opacity6">' + txnTypes[i].label + '</label>\n\t\t\t\t</div>';
				}
			}
			return htmlString;
		},
		getFirmExtraFields: function getFirmExtraFields() {
			var udfFirmFieldsMap = new _map2.default(uDFCache.getFirmMap(udfCurrentFirmId));
			var htmlString = '';
			udfFirmFieldsMap.forEach(function (value, key) {
				var firmModel = value;
				var i = firmModel.getUdfFieldNumber();
				htmlString += '<div class="md-checkbox">';
				if (firmModel.getUdfFieldStatus() == 1) {
					htmlString += '<input id="enableHeaderExtraField' + i + '" type="checkbox" class="enableHeaderExtraField" data-value="' + i + '" checked>\n\t\t\t\t\t<label tabIndex="0" for="enableHeaderExtraField' + i + '" class="udfexFieldCheckBoxLabel">Additional Field ' + i + '</label>\n\t\t\t\t</div>\n\t\t\t\t<div id="headerExFieldDiv' + i + '" class="extFieldsDiv">';
				} else {
					htmlString += '<input id="enableHeaderExtraField' + i + '" type="checkbox" class="enableHeaderExtraField" data-value="' + i + '">\n\t\t\t\t\t<label tabIndex="0" for="enableHeaderExtraField' + i + '" class="udfexFieldCheckBoxLabel opacity6">Additional Field ' + i + '</label>\n\t\t\t\t</div>\n\t\t\t\t<div id="headerExFieldDiv' + i + '" class="extFieldsDiv hide">';
				}
				htmlString += '<label for="headerF' + i + 'Name" class="headerFieldLabel">Field Name<span class="fontColorRed"> *</span></label>\n\t\t\t\t<input type="text" id="headerF' + i + 'Name" class="headerFields extraHeaderName" maxlength="30" placeholder="Enter Field Name" data-value="' + i + '">\n\t\t\t\t<label for="headerF' + i + 'Value" class="headerFieldLabel">Value</label>\n\t\t\t\t<input type="text" id="headerF' + i + 'Value" maxlength="30" class="headerFields extraHeaderValue" data-value="' + i + '">\n\t\t\t\t<div class="udfShowInPrintDiv">\n\t\t\t\t  <span class="udfShowInPrintSpan">Show in print</span>\n\t\t\t\t  <label class="UDFswitch UDFtoggleSwitch">';
				if (firmModel.getUdfFieldPrintOnInvoice() == 1) {
					htmlString += '<input type="checkbox" id="headerShowInPrint' + i + '" class="headerShowInPrint" data-value="' + i + '" checked>';
				} else {
					htmlString += '<input type="checkbox" id="headerShowInPrint' + i + '" class="headerShowInPrint" data-value="' + i + '">';
				}
				htmlString += '<span class="UDFslider round"></span>\n\t\t\t\t\t\t</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>';
			});
			return htmlString;
		}
	};
	return UserDefinedFieldsController2.init.bind(UserDefinedFieldsController2);
}(jQuery);