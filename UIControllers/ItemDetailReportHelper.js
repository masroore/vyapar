var DataLoader = require('./../DBManager/DataLoader.js');
var dataLoader = new DataLoader();

var ItemDetailReportHelper = function ItemDetailReportHelper() {
	function sortItemDetailReportData(txnList) {
		if (txnList) {
			txnList.sort(function (lhs, rhs) {
				var firstDate = MyDate.getDateObj(lhs.getDate(), 'yyyy-mm-dd', '-');
				var secondDate = MyDate.getDateObj(rhs.getDate(), 'yyyy-mm-dd', '-');
				var timeDiff = firstDate.getTime() - secondDate.getTime();
				return timeDiff;
			});
		}
	}

	this.getItemDetailReportObjectList = function (itemNameString, fromDate, endDate, showOnlyActiveRows) {
		var ItemCache = require('./../Cache/ItemCache.js');
		var ItemDetailReportObject = require('./../BizLogic/ItemDetailReportObject.js');
		var itemCache = new ItemCache();
		var itemLogic = itemCache.findItemByName(itemNameString);
		var itemId = itemLogic && itemLogic.getItemId() || 0;
		if (itemId < 1) {
			return [];
		} else {
			var txnList = dataLoader.getItemDetailReportData(itemId);
			sortItemDetailReportData(txnList);
			var currentPositionInList = 0;
			var listSize = txnList.length;
			var itemDataList = [];
			var beginningQty = 0;

			// For beginning stock
			while (listSize > currentPositionInList) {
				var itemDetailReportObject = txnList[currentPositionInList];
				if (MyDate.compareOnlyDatePart(MyDate.getDateObj(itemDetailReportObject.getDate(), 'yyyy-mm-dd', '-'), fromDate) < 0) {
					beginningQty = beginningQty - (itemDetailReportObject.getSaleQuantity() + itemDetailReportObject.getSaleFreeQuantity()) + (itemDetailReportObject.getPurchaseQuantity() + itemDetailReportObject.getPurchaseFreeQuantity()) + itemDetailReportObject.getAdjustmentQuantity();
				} else {
					break;
				}
				currentPositionInList++;
			}

			// If current position is greater than 0 then it means there was some transaction before from date
			if (currentPositionInList > 0) {
				var beginningStock = new ItemDetailReportObject();
				beginningStock.setDate(fromDate);
				beginningStock.setSaleQuantity(0);
				beginningStock.setPurchaseQuantity(0);
				beginningStock.setSaleFreeQuantity(0);
				beginningStock.setPurchaseFreeQuantity(0);
				beginningStock.setAdjustmentQuantity(0);
				beginningStock.setClosingQuantity(beginningQty);
				beginningStock.setForwardedStock(true);
				itemDataList.push(beginningStock);
			}

			var closingStockQty = beginningQty;

			for (var startDate = new Date(fromDate.getTime()); MyDate.compareOnlyDatePart(startDate, endDate) <= 0; startDate.setTime(startDate.getTime() + MyDate.numberOfMilliSecondsInDay)) {
				var shouldAddInactieRow = !showOnlyActiveRows;
				if (listSize > currentPositionInList) {
					var itemDetailReportObject = txnList[currentPositionInList];
					if (MyDate.isSameDate(MyDate.getDateObj(itemDetailReportObject.getDate(), 'yyyy-mm-dd', '-'), startDate)) {
						closingStockQty = closingStockQty - (itemDetailReportObject.getSaleQuantity() + itemDetailReportObject.getSaleFreeQuantity()) + (itemDetailReportObject.getPurchaseQuantity() + itemDetailReportObject.getPurchaseFreeQuantity()) + itemDetailReportObject.getAdjustmentQuantity();

						var itemEntry = new ItemDetailReportObject();
						itemEntry.setDate(new Date(startDate));
						itemEntry.setSaleQuantity(itemDetailReportObject.getSaleQuantity());
						itemEntry.setPurchaseQuantity(itemDetailReportObject.getPurchaseQuantity());
						itemEntry.setSaleFreeQuantity(itemDetailReportObject.getSaleFreeQuantity());
						itemEntry.setPurchaseFreeQuantity(itemDetailReportObject.getPurchaseFreeQuantity());
						itemEntry.setAdjustmentQuantity(itemDetailReportObject.getAdjustmentQuantity());
						itemEntry.setClosingQuantity(closingStockQty);
						itemEntry.setForwardedStock(false);
						itemDataList.push(itemEntry);
						currentPositionInList++;
						shouldAddInactieRow = false;
					}
				}
				if (shouldAddInactieRow) {
					// Non active rows
					var itemEntry = new ItemDetailReportObject();
					itemEntry.setDate(new Date(startDate));
					itemEntry.setSaleQuantity(0);
					itemEntry.setPurchaseQuantity(0);
					itemEntry.setAdjustmentQuantity(0);
					itemEntry.setClosingQuantity(closingStockQty);
					itemEntry.setForwardedStock(false);
					itemDataList.push(itemEntry);
				}
			}
			return itemDataList;
		}
	};
};

module.exports = ItemDetailReportHelper;