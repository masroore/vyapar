var TaxCodeCache = require('./../Cache/TaxCodeCache.js');
var taxCodeCache = new TaxCodeCache();
var TaxCodeConstants = require('./../Constants/TaxCodeConstants.js');
var ItemCache = require('./../Cache/ItemCache.js');
var GSTRReportHelper = require('./../UIControllers/GSTRReportHelper.js');

var GSTR4ReportHelper = function GSTR4ReportHelper() {
	this.showOTHER = false;
	this.showAdditionalCess = false;
	this.isEntryIncorrect = false;
	this.isTaxUsedInLineItem = false;
	this.isTaxUsedInTransaction = false;
	this.isTaxAppliedOnTxn = true;
	this.isTaxUsedInAllLineItems = true;
	this.isTaxUsedInAnyLineItems = false;

	var compositionRateChangeDate = MyDate.getDateObj('01/01/2018', 'dd/MM/yyyy', '/');

	this.getTxnListBasedOnDate = function (fromDate, toDate, firmId, considerNonTaxAsExemptedCheck) {
		var GSTR4ReportObject = require('./../BizLogic/GSTR4ReportObject.js');
		var DataLoader = require('./../DBManager/DataLoader.js');
		var dataLoader = new DataLoader();
		var transactionList = dataLoader.loadTransactionsForGSTR4Report(fromDate, toDate, firmId);
		var gstr4ReportObjectList = [];
		var objectSparseArray = [];
		var taxableAmt;
		var name;

		if (transactionList != null) {
			var lenn = transactionList.length;
			for (var i = 0; i < lenn; i++) {
				this.isTaxUsedInLineItem = false;
				this.isTaxUsedInTransaction = false;
				this.isTaxUsedInAllLineItems = true;
				this.isTaxUsedInAnyLineItems = false;

				var InvoiceValue = 0;
				var transaction = transactionList[i];
				var gstinNumber = transaction.getNameRef().getGstinNumber();
				name = transaction.getNameRef();
				objectSparseArray = [];

				var NonTaxFlag = considerNonTaxAsExemptedCheck;
				NonTaxFlag = GSTRReportHelper.validateFlagForGSTR(transaction) || NonTaxFlag; // Include NONE tax transactions for Composite User

				this.isTaxAppliedOnTxn = Number(transaction.getTransactionTaxId()) != 0 && !GSTRReportHelper.isTaxOfTypeOthers(transaction.getTransactionTaxId());

				for (var k = 0; k < transaction.getLineItems().length; k++) {
					var lineItem = transaction.getLineItems()[k];
					var lineTaxId = Number(lineItem.getLineItemTaxId());
					this.isTaxUsedInAllLineItems = this.isTaxUsedInAllLineItems && (lineTaxId != 0 && !GSTRReportHelper.isTaxOfTypeOthers(lineTaxId) || Number(lineItem.getLineItemAdditionalCESS()) != 0);
					this.isTaxUsedInAnyLineItems = this.isTaxUsedInAnyLineItems || lineTaxId != 0 && !GSTRReportHelper.isTaxOfTypeOthers(lineTaxId) || Number(lineItem.getLineItemAdditionalCESS()) != 0;
				}

				var transactionLineItems = transaction.getLineItems();
				var transactionLineItemsLenth = transaction.getLineItems().length;
				for (var j = 0; j < transactionLineItemsLenth; j++) {
					var lineItem = transactionLineItems[j];
					var lineTaxId = Number(lineItem.getLineItemTaxId());

					if (lineTaxId != 0 && !GSTRReportHelper.isTaxOfTypeOthers(lineTaxId) || lineItem.getLineItemAdditionalCESS() > 0 || !this.isTaxUsedInAllLineItems && this.isTaxUsedInAnyLineItems && NonTaxFlag && !GSTRReportHelper.isTaxOfTypeOthers(lineTaxId)) {
						InvoiceValue = transaction.getBalanceAmount() + transaction.getCashAmount() + transaction.getReverseChargeAmount();
						var reportObject;
						if (!objectSparseArray[lineTaxId]) {
							reportObject = new GSTR4ReportObject();
							objectSparseArray[lineTaxId] = reportObject;
							gstr4ReportObjectList.push(reportObject);
							if (name != null) {
								reportObject.setNameId(name.getNameId());
								reportObject.setGstinNo(name.getGstinNumber());
								reportObject.setPartyName(name.getFullName());
							}
							reportObject.setTxnRefNumber(transaction.getTxnRefNumber());
							reportObject.setTxnReturnRefNumber(transaction.getTxnReturnRefNumber());
							reportObject.setTxnReturnDate(transaction.getTxnReturnDate());
							reportObject.setFirmId(transaction.getFirmId());
							reportObject.setInvoiceNo(transaction.getFullTxnRefNumber());
							reportObject.setInvoiceDate(transaction.getTxnDate());
							reportObject.setInvoiceValue(InvoiceValue);
							reportObject.setTransactionId(transaction.getTxnId());
							reportObject.setTransactionType(transaction.getTxnType());
							reportObject.setReverseCharge(Number(transaction.getReverseCharge()));
							reportObject.setTransactionDescription(transaction.getDescription());
							reportObject.setPlaceOfSupply(GSTRReportHelper.getPlaceOfSupply(name, transaction));
						} else {
							reportObject = objectSparseArray[lineTaxId];
						}
						taxableAmt = lineItem.getItemQuantity() * lineItem.getItemUnitPrice() - lineItem.getLineItemDiscountAmount();
						reportObject.setInvoiceTaxableValue(taxableAmt + Number(reportObject.getInvoiceTaxableValue()));
						reportObject.setAdditionalCessAmt(reportObject.getAdditionalCessAmt() + Number(lineItem.getLineItemAdditionalCESS()));
						var lineItemTaxableAmount = 0;
						if (Number(lineItem.getLineItemAdditionalCESS()) > 0) {
							this.isTaxUsedInLineItem = true;
							this.showAdditionalCess = true;
							lineItemTaxableAmount = taxableAmt;
						}
						var taxCode = taxCodeCache.getTaxCodeObjectById(lineTaxId);
						if (lineTaxId != 0 && lineTaxId != null && taxCode != null && !GSTRReportHelper.isTaxOfTypeOthers(lineTaxId)) {
							this.isTaxUsedInLineItem = true;
							reportObject.setTaxRateId(taxCode.getTaxCodeId());
							lineItemTaxableAmount = taxableAmt;
							if (taxCode.getTaxType() == TaxCodeConstants.taxGroup) {
								reportObject.setRate(taxCode.getTaxRate());
								for (var p = 0; p < taxCode.getTaxCodeMap().length; p++) {
									var taxId = taxCode.getTaxCodeMap()[p];
									var code;
									if ((code = taxCodeCache.getTaxCodeObjectById(taxId)) != null) {
										this.setTaxEntries(reportObject, code, taxableAmt);
									}
								}
							} else {
								reportObject.setRate(taxCode.getTaxRate());
								this.setTaxEntries(reportObject, taxCode, taxableAmt);
							}
						}
					}
				}
				var transcationTaxId = Number(transaction.getTransactionTaxId());
				if (transcationTaxId != 0 && !GSTRReportHelper.isTaxOfTypeOthers(transcationTaxId) || NonTaxFlag && !this.isTaxAppliedOnTxn && !GSTRReportHelper.isTaxOfTypeOthers(transcationTaxId) && !this.isTaxUsedInAnyLineItems) {
					InvoiceValue = transaction.getBalanceAmount() + transaction.getCashAmount() + transaction.getReverseChargeAmount();
					var object;
					if (!objectSparseArray[transcationTaxId]) {
						object = new GSTR4ReportObject();
						objectSparseArray[transcationTaxId] = object;
						gstr4ReportObjectList.push(object);
						if (name != null) {
							object.setNameId(name.getNameId());
							object.setGstinNo(name.getGstinNumber());
							object.setPartyName(name.getFullName());
						}
						object.setTxnRefNumber(transaction.getTxnRefNumber());
						object.setTxnReturnRefNumber(transaction.getTxnReturnRefNumber());
						object.setTxnReturnDate(transaction.getTxnReturnDate());
						object.setFirmId(transaction.getFirmId());
						object.setInvoiceNo(transaction.getFullTxnRefNumber());
						object.setInvoiceDate(transaction.getTxnDate());
						object.setInvoiceValue(InvoiceValue);
						object.setTransactionId(transaction.getTxnId());
						object.setTransactionType(transaction.getTxnType());
						object.setReverseCharge(Number(transaction.getReverseCharge()));
						object.setTransactionDescription(transaction.getDescription());
						object.setPlaceOfSupply(GSTRReportHelper.getPlaceOfSupply(name, transaction));
					} else {
						object = objectSparseArray[transcationTaxId];
					}
					taxableAmt = transaction.getSubTotalAmount() - transaction.getDiscountAmount();
					object.setInvoiceTaxableValue(taxableAmt + Number(object.getInvoiceTaxableValue()));

					var taxCode = taxCodeCache.getTaxCodeObjectById(transcationTaxId);
					if (transcationTaxId != 0 && transcationTaxId != null && taxCode != null) {
						this.isTaxUsedInTransaction = true;
						object.setTaxRateId(taxCode.getTaxCodeId());
						if (taxCode.getTaxType() == TaxCodeConstants.taxGroup) {
							object.setRate(taxCode.getTaxRate());
							for (var p = 0; p < taxCode.getTaxCodeMap().length; p++) {
								var taxId = taxCode.getTaxCodeMap()[p];
								var code;
								if ((code = taxCodeCache.getTaxCodeObjectById(taxId)) != null) {
									this.setTaxEntries(object, code, taxableAmt);
								}
							}
						} else {
							object.setRate(taxCode.getTaxRate());
							this.setTaxEntries(object, taxCode, taxableAmt);
						}
					}
					object.setEntryIncorrect(this.isTaxUsedInLineItem && this.isTaxUsedInTransaction);
				}
			}
		}
		return gstr4ReportObjectList;
	};

	this.getTurnOverForGSTR4 = function (fromDate, toDate, firmId) {
		var DataLoader = require('./../DBManager/DataLoader.js');
		var CompositeUserType = require('./../Constants/CompositeUserType.js');
		var dataLoader = new DataLoader();
		var compositionRateChangeDateDecember = MyDate.getDateObj('31/12/2017', 'dd/MM/yyyy', '/');
		var turnOverValue = 0;
		var turnOverValueForManufacturerBeforeRateChange = 0;
		if (settingCache.getCompositeUserType() == CompositeUserType.MANUFACTURER) {
			if (fromDate < compositionRateChangeDate) {
				// compositionRateChangeDate is 1st january 2018
				if (toDate < compositionRateChangeDate) {
					turnOverValueForManufacturerBeforeRateChange = dataLoader.loadTurnOverForGSTR4Report(fromDate, toDate, firmId);
				} else {
					turnOverValue = dataLoader.loadTurnOverForGSTR4Report(compositionRateChangeDate, toDate, firmId);
					turnOverValueForManufacturerBeforeRateChange = dataLoader.loadTurnOverForGSTR4Report(fromDate, compositionRateChangeDateDecember, firmId); // compositionRateChangeDateDecember is 31st december 2017
				}
			} else {
				turnOverValue = dataLoader.loadTurnOverForGSTR4Report(fromDate, toDate, firmId);
			}
		} else {
			turnOverValue = dataLoader.loadTurnOverForGSTR4Report(fromDate, toDate, firmId);
		}
		return [turnOverValue, turnOverValueForManufacturerBeforeRateChange];
	};

	this.setTaxEntries = function (reportObject, taxCode, taxableAmt) {
		switch (taxCode.getTaxRateType()) {
			case TaxCodeConstants.IGST:
				reportObject.setIGSTAmt(taxableAmt * taxCode.getTaxRate() / 100 + reportObject.getIGSTAmt());
				break;
			case TaxCodeConstants.SGST:
				reportObject.setSGSTAmt(taxableAmt * taxCode.getTaxRate() / 100 + reportObject.getSGSTAmt());
				break;
			case TaxCodeConstants.CGST:
				reportObject.setCGSTAmt(taxableAmt * taxCode.getTaxRate() / 100 + reportObject.getCGSTAmt());
				break;
			case TaxCodeConstants.CESS:
				reportObject.setCESSAmt(taxableAmt * taxCode.getTaxRate() / 100 + reportObject.getCESSAmt());
				reportObject.setCessRate(taxCode.getTaxRate());
				break;
			case TaxCodeConstants.OTHER:
				reportObject.setOTHERAmt(taxableAmt * taxCode.getTaxRate() / 100 + reportObject.getOTHERAmt());
				this.showOTHER = true;
				break;
		}
	};
};

module.exports = GSTR4ReportHelper;