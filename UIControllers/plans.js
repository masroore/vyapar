/**
 * Created by rahi on 3/24/2017.
 * Edited by ishwar on 8/11/2017
 * Edited by Anuj on 9/13/2019
 * Edited by Sagar on 26/5/2019
 */

(function () {
	var planKeyForLocalStorage = 'lastPlansData';
	var oldPlansData = {
		plans: []
	};
	try {
		var localStorageValueForPlans = localStorage.getItem(planKeyForLocalStorage);
		if (localStorageValueForPlans) {
			oldPlansData = JSON.parse(localStorageValueForPlans);
		}
	} catch (e) {}
	var UsageTypeConstants = require('./../Constants/UsageTypeConstants.js');
	var LicenseInfoConstant = require('../Constants/LicenseInfoConstant.js');
	var LicenseUtility = require('../Utilities/LicenseUtility');
	var licenseInfoDetails = LicenseUtility.getLicenseInfo();
	var usageType = licenseInfoDetails[LicenseInfoConstant.licenseUsageType];
	var DeviceHelper = require('../Utilities/deviceHelper');
	var LocalStorageHelper = require('../Utilities/LocalStorageHelper');

	if (usageType == UsageTypeConstants.VALID_LICENSE || usageType == UsageTypeConstants.EXPIRED_LICENSE) {
		$('#discountBanner').removeClass('hide');
		$('#planDescription').addClass('hide');
	} else {
		$('#discountBanner').addClass('hide');
		$('#planDescription').removeClass('hide');
	}

	if (usageType != UsageTypeConstants.VALID_LICENSE) {
		$('#haveALicense').html('').html("<div style='padding-top: 10px;display:table;margin:auto;font-size:12px;'><a href='#' id='alreadyHaveALicense'>Already have a License ?</a></div>");
	}

	window.onResume = function () {
		// Do nothing
	};

	function showLicenseDetails() {
		LicenseUtility.checkLicenseStatus();
		var remainingTrialPeriod = licenseInfoDetails[LicenseInfoConstant.licenseValidityDays];
		var licenseHtml = '';
		var licenseInfoToolTip = '<table class="license-detail-table">';
		if (usageType == UsageTypeConstants.VALID_LICENSE || usageType == UsageTypeConstants.EXPIRED_LICENSE) {
			licenseInfoToolTip += '<tr><td class="license-detail-col">License Number</td> <td class="license-detail-col"> : </td> <td class="license-detail-col boldFont">' + licenseInfoDetails['license_code'] + '</td></tr>';
		}
		licenseInfoToolTip += '<tr><td class="license-detail-col">Device Id</td> <td class="license-detail-col"> : </td> <td class="license-detail-col boldFont">' + licenseInfoDetails.device_id + '</td></tr>';
		var SettingCache = require('./../Cache/SettingCache');
		var settingCache = new SettingCache();
		var companyGlobalId = settingCache.getCompanyGlobalId();
		if (companyGlobalId) {
			licenseInfoToolTip += '<tr><td class="license-detail-col">Sync Id</td> <td class="license-detail-col"> : </td> <td class="license-detail-col boldFont wrapLines">' + companyGlobalId + '</td></tr>';
		}
		licenseInfoToolTip += '</table>';
		licenseHtml += "<div class='licenseDetailContainer'>";
		licenseHtml += "<div class='licenseDetailHeader'><p class='licenseDetailContainerHeader d-inline'>License Details</p> <div class='d-inline floatRight tooltip'><img src='./../inlineSVG/infoLicense.svg'></img><span class='tooltiptext'>" + licenseInfoToolTip + '</span></div></div>';
		licenseHtml += "<div style='padding-top: 10px;'>";
		licenseHtml += "<table class='licenseTable' style='width: 100%;'>";
		if (usageType == UsageTypeConstants.VALID_LICENSE || usageType == UsageTypeConstants.EXPIRED_LICENSE) {
			licenseHtml += "<tr><td style='padding-bottom: 8px;'>License Status</td><td style='padding-bottom: 8px;'>:&nbsp;&nbsp;</td>";
			if (usageType == UsageTypeConstants.EXPIRED_LICENSE) {
				licenseHtml += "<td style='padding-bottom: 8px;'>Expired (Expired on " + MyDate.getDateObj(licenseInfoDetails['expiry_date'], 'yyyy-mm-dd', '-').toDateString() + ')</td>';
			} else if (usageType == UsageTypeConstants.VALID_LICENSE) {
				licenseHtml += "<td style='padding-bottom: 8px;'>Active (Expires on " + MyDate.getDateObj(licenseInfoDetails['expiry_date'], 'yyyy-mm-dd', '-').toDateString() + ')</td>';
			}
			licenseHtml += '</tr>';
			licenseHtml += "<tr><td style='padding-bottom: 8px;'>Plan</td><td style='padding-bottom: 8px;'>:&nbsp;&nbsp;</td><td style='padding-bottom: 8px;'>" + licenseInfoDetails['plan_name'] + '</td></tr>';
		}

		if (usageType == UsageTypeConstants.BLOCKED) {
			licenseHtml += "<tr><td style='padding-bottom: 8px;'>Status</td><td style='padding-bottom: 8px;'>:&nbsp;&nbsp;</td><td style='padding-bottom: 8px;'>Trial Period Expired</td></tr>";
		} else if (usageType == UsageTypeConstants.EXPIRED_LICENSE) {
			licenseHtml += "<tr><td style='padding-bottom: 8px;'>Status</td><td style='padding-bottom: 8px;'>:&nbsp;&nbsp;</td><td style='padding-bottom: 8px;'>License Expired</td></tr>";
		} else if (usageType == UsageTypeConstants.TRIAL_PERIOD) {
			licenseHtml += "<tr><td style='padding-bottom: 8px;'>Trial </td><td style='padding-bottom: 8px;'>:&nbsp;&nbsp;</td><td style='padding-bottom: 8px;'>" + remainingTrialPeriod + ' day(s) left.</td></tr>';
		} else if (usageType == UsageTypeConstants.DEVICE_TIME_MIS_CONFIGURED) {
			licenseHtml += "<tr><td style='padding-bottom: 8px;'>Status</td><td style='padding-bottom: 8px;'>:&nbsp;&nbsp;</td><td style='padding-bottom: 8px;'>Usage is blocked due to misconfigured device time.</td></tr>";
		}

		licenseHtml += '</table>';
		licenseHtml += '</div>';

		licenseHtml += '</div>';
		$('#licenseDetail').html(licenseHtml);
	}

	function showPlans(plansData) {
		var errorMsg = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';

		oldPlansData = plansData;

		var SettingCache = require('./../Cache/SettingCache');
		var settingCache = new SettingCache();

		var isIndianUser = settingCache.isCurrentCountryIndia();
		var planshtml = '';
		var anotherDevicePlanHtml = '';
		planshtml += '<p class="planDetailsHeader">Plans</p>';

		anotherDevicePlanHtml += '<p class="planDetailsHeader">Plans</p>';
		planshtml += "<div id='outerBox' class='d-flex'>";
		anotherDevicePlanHtml += "<div id='anotherOuterBox' class='d-flex'>";
		planshtml += '<div style=\'align-self:center\'> \n\t\t\t\t\t\t\t<span id=\'previous\' class=\'hidden\'><i class=\'arrow left\'></i></span>\n\t\t\t\t\t  </div>';
		anotherDevicePlanHtml += '<div style=\'align-self:center\'>\n\t\t\t\t\t\t\t\t\t<span id=\'anotherPrevious\' class=\'hidden\'><i class=\'arrow left\'></i></span>\n\t\t\t\t\t\t\t\t  </div>';
		planshtml += "<div id='container' style='overflow-x:hidden;width:100%'>";
		planshtml += "<div id='plansRow' class='d-flex' style='flex-wrap: nowrap;'>";
		anotherDevicePlanHtml += "<div id='anotherDeviceContainer' style='overflow-x:hidden;width:100%'>";
		anotherDevicePlanHtml += "<div id='plansRow' class='d-flex' style='flex-wrap: nowrap;'>";
		var buyOrRenew = void 0;
		if (usageType == UsageTypeConstants.VALID_LICENSE || usageType == UsageTypeConstants.EXPIRED_LICENSE) {
			buyOrRenew = 'Renew';
		} else {
			buyOrRenew = 'Buy Now';
		}

		if (plansData && plansData.plans && plansData.plans.length > 0) {
			var plans = plansData['plans'];
			var plansLength = plans.length;
			for (var i = 0; i < plansLength; i++) {
				var plan = plans[i];
				var planCost = isIndianUser ? plan.inrPrice : plan.usdPrice;
				var planOriginalCost = isIndianUser ? plan.originalInrPrice : plan.originalUsdPrice;
				var specialPlan = plan.show_tag;
				if (specialPlan) {
					planshtml += "<div id='planCard' class='specialCard planCard'>";
					anotherDevicePlanHtml += "<div id='planCard' class='specialCard planCard'>";
					planshtml += '<div class=\'bannerBox\'>\n\t\t\t\t\t\t\t\t\t <img src=\'../Images/banner_green.png\'/>\n\t\t\t\t\t\t\t\t\t <div class=\'bannerText\'>SPECIAL PLAN</div>\n\t\t\t\t\t\t\t\t  </div>';
					anotherDevicePlanHtml += '<div class=\'bannerBox\'>\n\t\t\t\t\t\t\t\t\t\t\t\t<img src=\'../Images/banner_green.png\'/>\n\t\t\t\t\t\t\t\t\t\t\t\t<div class=\'bannerText\'>SPECIAL PLAN</div>\n\t\t\t\t\t\t\t\t\t\t\t </div>';
				} else {
					planshtml += "<div id='planCard' class='planCard'>";
					anotherDevicePlanHtml += "<div id='planCard' class='planCard'>";
				}
				// planshtml += '<div>';
				planshtml += "<p class='planName'>" + plan.planName + '</p>';
				anotherDevicePlanHtml += "<p class='planName'>" + plan.planName + '</p>';
				planshtml += "<p class='planOriginalCost'>" + planOriginalCost + '</p>';
				anotherDevicePlanHtml += "<p class='planOriginalCost'>" + planOriginalCost + '</p>';
				if (usageType == UsageTypeConstants.VALID_LICENSE || usageType == UsageTypeConstants.EXPIRED_LICENSE) {
					var planDiscounted = isIndianUser ? plan.inrRenewalPrice : plan.usdRenewalPrice;
					if (!planDiscounted) {
						planDiscounted = planCost;
					}
					planDiscounted = planDiscounted.split('/')[0].split(' ');
					var symbolCurrency = planDiscounted[0];
					planDiscounted = Number(planDiscounted[1]);
					planshtml += "<p class='plancost'>" + symbolCurrency + ' ' + Math.floor(planDiscounted) + '</p>';
					planshtml += "<p class='planTime'>" + planCost.split('/')[1] + '</p>';
					if (plansData.renew_discount_message) {
						$('#discountBanner .percentage-discount').text(plansData.renew_discount_message);
					}
				} else {
					planshtml += "<p class='plancost'>" + planCost.split('/')[0] + '</p>';
					planshtml += "<p class='planTime'>" + planCost.split('/')[1] + '</p>';
				}
				anotherDevicePlanHtml += "<p class='plancost'>" + planCost.split('/')[0] + '</p>';
				anotherDevicePlanHtml += "<p class='planTime'>" + planCost.split('/')[1] + '</p>';
				if (planOriginalCost) {
					planshtml += "<p class='timeLimitedContent'> Limited Time Offer </p>";
					anotherDevicePlanHtml += "<p class='timeLimitedContent'> Limited Time Offer </p>";
				}

				planshtml += "<div style='padding:16px 0;text-align: center;'>";
				if (usageType == UsageTypeConstants.VALID_LICENSE || usageType == UsageTypeConstants.EXPIRED_LICENSE) {
					planshtml += '<a class=' + (specialPlan ? 'specialTerminalButton' : 'plansTerminalButton') + ' href=\'#\' id =\'extendLicense\' data-plan-id=' + plan.planId + '>' + buyOrRenew + '</a>';
				} else {
					planshtml += '<a class=' + (specialPlan ? 'specialTerminalButton' : 'plansTerminalButton') + ' href=\'#\' id =\'buyLicense\' data-plan-id=' + plan.planId + '>' + buyOrRenew + '</a>';
				}
				planshtml += '</div></div>';
				anotherDevicePlanHtml += "<div style='padding:16px 0;text-align: center;'>";
				anotherDevicePlanHtml += '<a class=' + (specialPlan ? 'specialTerminalButton' : 'plansTerminalButton') + ' href=\'#\' id =\'buyLicense\' data-plan-id=' + plan.planId + '> Buy Now </a>';
				anotherDevicePlanHtml += '</div></div>';
			}
		} else {
			planshtml += '<div class="center" style="margin-top: 5%; color:white">' + (errorMsg || 'Error while fetching plans. ') + ' <div> <a href="#" class="refreshPlansList">Refresh</a></div></div>';
			errorMsg = '';
		}

		planshtml += '</div></div>';
		anotherDevicePlanHtml += '</div></div>';
		planshtml += '<div style=\'align-self:center\'>\n\t\t\t\t\t\t\t<span id="next" class=' + (plansData['plans'].length > 4 ? 'visible' : 'hidden') + '><i class=\'arrow right\'></i></span>\n\t\t\t\t\t  </div>';
		anotherDevicePlanHtml += '<div style=\'align-self:center\'>\n\t\t\t\t\t\t\t\t\t\t<span id="anotherNext" class=' + (plansData['plans'].length > 4 ? 'visible' : 'hidden') + '><i class=\'arrow right\'></i></span>\n\t\t\t\t\t\t\t\t  </div>';
		planshtml += '</div>';
		anotherDevicePlanHtml += '</div>';
		if (errorMsg) {
			planshtml += '<div class="center" style="margin-top: 2%; color: white">' + errorMsg + '</div>';
		}
		// planshtml += '</div>';

		$('#planDetailsList').html('').html(planshtml);
		$('#anotherDevicePlanDetailsList').html('').html(anotherDevicePlanHtml);
	}

	function showPlansDropDown() {
		if (usageType == UsageTypeConstants.VALID_LICENSE) {
			MyAnalytics.pushEvent('Buy License for Another Device Start');
		}
		if ($('#plansRow').hasClass('hide')) {
			$('#plansRow').show(500);
			$('#plansRow').removeClass('hide');
		} else {
			$('#plansRow').hide(500);
			$('#plansRow').addClass('hide');
		}
	}

	function fetchPlans() {
		var isOnline = require('is-online');
		isOnline().then(function (online) {
			if (online) {
				var PlansUtility = require('../Utilities/PlansUtility');
				var loadTextLicense = $('#loadTextLicense');
				var ldsEllipsis = $('.lds-ellipsis');
				loadTextLicense.html('Fetching plans. Please wait.');
				ldsEllipsis.show();
				var successCallback = function successCallback(data) {
					showPlans(data);
					loadTextLicense.html('');
					ldsEllipsis.hide();
				};
				var errorCallback = function errorCallback() {
					showPlans(oldPlansData);
					loadTextLicense.html('');
					ldsEllipsis.hide();
				};
				PlansUtility.getPlanDetails(successCallback, errorCallback);
			} else {
				showPlans(oldPlansData, 'No internet connection found. Please Connect to Internet for purchasing license.');
			}
		});
	};

	function getURLForPayment(workflow, plan_id, license_code) {
		var deviceInfo = DeviceHelper.getDeviceInfo();
		var settings = require('./../Cache/SettingCache');
		var user = new settings();
		var url = domain + '/register?client_type=2';
		var remainingTrialPeriod = licenseInfoDetails[LicenseInfoConstant.licenseValidityDays];

		if (plan_id) {
			url += '&plan_id=' + plan_id;
		}
		url += '&workflow=' + workflow + '&device_id=' + encodeURIComponent(deviceInfo.deviceId) + '&os_version=' + encodeURIComponent(deviceInfo.osPlatform) + encodeURIComponent(deviceInfo.osRelease) + '&platform=2&email_id_=' + encodeURIComponent(user.getLoginUserEmailId()) + '&phone_number_=' + encodeURIComponent(user.getLoginUserNumber()) + '&business_name_=' + encodeURIComponent(user.getLoginUserName());

		if (usageType == UsageTypeConstants.TRIAL_PERIOD && remainingTrialPeriod < 62) {
			url += '&remaining_trial_period=' + remainingTrialPeriod;
		}

		if (workflow == 3) {
			url += '&license_number=' + license_code;
		}
		url += '&country_code=' + encodeURIComponent(user.getCountryCode());
		if (oldPlansData && oldPlansData.couponCode) {
			url += '&couponCode=' + oldPlansData.couponCode;
		}

		var referrerCode = LocalStorageHelper.getValue(LicenseInfoConstant.referralCode);
		if (referrerCode) {
			url += '&referrer_code=' + referrerCode;
		}
		return url;
	}

	function buyLicense(e) {
		var id = e.data('planId');
		MyAnalytics.pushEvent('Buy License Start');
		var url = getURLForPayment(1, id, null);
		openPaymentWindow(url);
	};

	function alreadyHaveALicense(e) {
		e.preventDefault();
		MyAnalytics.pushEvent('Already Have A License');
		var url = getURLForPayment(2, null, null);
		openPaymentWindow(url);
	}

	function extendLicense(e) {
		var id = e.data('planId');
		MyAnalytics.pushEvent('Extend License Start');
		var url = getURLForPayment(3, id, licenseInfoDetails[LicenseInfoConstant.licenseCode]);
		openPaymentWindow(url);
	}

	function listenMessage(msg, paymentWindow) {
		if (msg.status == 'paymentSuccess' || msg.status == 'deviceChangedSuccess') {
			if (msg.status == 'paymentSuccess') {
				MyAnalytics.pushEvent('License Bought');
			} else {
				MyAnalytics.pushEvent('Device changed');
			}
			delete msg.status;
			var deviceId = LocalStorageHelper.getValue(LicenseInfoConstant.DEVICE_ID, true);
			if (deviceId == msg.mac_address) {
				LocalStorageHelper.setValue(LicenseInfoConstant.expiryDate, msg.expiry_date, true);
				LocalStorageHelper.setValue(LicenseInfoConstant.licenseCode, msg.license_code);
			}
			setTimeout(function () {
				if (paymentWindow) {
					paymentWindow.close();
				}
			}, 4000);
		}
	}

	function openPaymentWindow(url) {
		var rem = require('electron');
		var _rem$remote = rem.remote,
		    BrowserWindow = _rem$remote.BrowserWindow,
		    dialog = _rem$remote.dialog,
		    shell = _rem$remote.shell;

		var paymentWindow = new BrowserWindow({
			show: false,
			minimizable: false,
			alwaysOnTop: true,
			skipTaskbar: true,
			autoHideMenuBar: true,
			webPreferences: {
				nodeIntegration: false
			}
		});
		paymentWindow.on('closed', function () {
			paymentWindow = null;
		});
		paymentWindow.webContents.session.clearStorageData({
			origin: domain,
			storages: ['cookies']
		});
		paymentWindow.loadURL(url);
		paymentWindow.show();

		paymentWindow.on('page-title-updated', function () {
			var title = paymentWindow.getTitle();
			try {
				var jsonData = JSON.parse(title);
				listenMessage(jsonData, paymentWindow);
			} catch (e) {}
		});

		paymentWindow.on('close', function () {
			showPlans(oldPlansData);
			showLicenseDetails();
		});
	}

	$(function () {
		if (oldPlansData && oldPlansData.plans && oldPlansData.plans.length > 0) {
			showPlans(oldPlansData);
		}
		fetchPlans();
		showLicenseDetails();

		// left and right slider arrow for multiple plan (more than 4)
		var sliderCard = document.querySelectorAll('#planCard');
		var rowWidth = document.querySelector('#plansRow').offsetWidth;
		var counter = 0;
		var defaultClicks = 5;
		var cardWidth = sliderCard[0].offsetWidth;
		var width = cardWidth + 0.03 * rowWidth;
		$(document).off('click', '#previous').on('click', '#previous', function (e) {
			if (counter <= 1) {
				$('#previous').removeClass('visible').addClass('hidden');
			} else {
				$('#previous').removeClass('hidden').addClass('visible');
			};
			$('#next').removeClass('hidden').addClass('visible');
			$('#plansRow').addClass('transition');
			counter--;
			$('#plansRow').css('transform', 'translateX(' + -width * counter + 'px)');
		});

		$(document).off('click', '#next').on('click', '#next', function (e) {
			if (counter >= sliderCard.length / 2 - defaultClicks) {
				$('#next').removeClass('visible').addClass('hidden');
			} else {
				$('#next').removeClass('hidden').addClass('visible');
			};

			$('#previous').removeClass('hidden').addClass('visible');
			$('#plansRow').addClass('transition');
			counter++;
			$('#plansRow').css('transform', 'translateX(' + -width * counter + 'px)');
		});

		var otherCounter = 0;
		$(document).off('click', '#anotherPrevious').on('click', '#anotherPrevious', function (e) {
			if (otherCounter <= 1) {
				$('#anotherPrevious').removeClass('visible').addClass('hidden');
			} else {
				$('#anotherPrevious').removeClass('hidden').addClass('visible');
			};
			$('#anotherNext').removeClass('hidden').addClass('visible');
			$('#anotherDeviceContainer #plansRow').addClass('transition');
			otherCounter--;
			$('#anotherDeviceContainer #plansRow').css('transform', 'translateX(' + -width * otherCounter + 'px)');
		});

		$(document).off('click', '#anotherNext').on('click', '#anotherNext', function (e) {
			if (otherCounter >= sliderCard.length / 2 - defaultClicks) {
				$('#anotherNext').removeClass('visible').addClass('hidden');
			} else {
				$('#anotherNext').removeClass('hidden').addClass('visible');
			};
			$('#anotherPrevious').removeClass('hidden').addClass('visible');
			$('#anotherDeviceContainer #plansRow').addClass('transition');
			otherCounter++;
			$('#anotherDeviceContainer #plansRow').css('transform', 'translateX(' + -width * otherCounter + 'px)');
		});
		// ====================================================================================
		$(document).off('click', '.planCard').on('click', '.planCard', function () {
			var el = $(this).find('a');
			if (el.attr('id') == 'extendLicense') {
				extendLicense(el);
			} else if (el.attr('id') == 'buyLicense') {
				buyLicense(el);
			}
		});

		// $(document).off('click', '#extendLicense').on('click', '#extendLicense', extendLicense);
		$(document).off('click', '#alreadyHaveALicense').on('click', '#alreadyHaveALicense', alreadyHaveALicense);
		$(document).off('click', '#showPlansDropDown').on('click', '#showPlansDropDown', showPlansDropDown);
		// $(document).off('click', '#buyLicense').on('click', '#buyLicense', buyLicense);
		$(document).off('click', '.refreshPlansList').on('click', '.refreshPlansList', fetchPlans);
		$('#anotherDeviceLicenseLink').off('click').on('click', function () {
			$('#licenseForAnotherDeviceDialog').dialog({
				// appendTo: 'body',
				height: 550,
				width: 900,
				title: 'Purchase for other device',
				resizable: false,
				modal: true,
				close: function close(event, ui) {
					$('#licenseForAnotherDeviceDialog').dialog('destroy');
				}
			});
		});
	});
})();