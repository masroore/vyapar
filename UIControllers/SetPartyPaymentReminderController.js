var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _create = require('babel-runtime/core-js/object/create');

var _create2 = _interopRequireDefault(_create);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* global jQuery */
var DataLoader = require('./../DBManager/DataLoader.js');
var ErrorCode = require('./../Constants/ErrorCode.js');
var NameCache = require('./../Cache/NameCache.js');
var MyDouble = require('./../Utilities/MyDouble');
var MyDate = require('./../Utilities/MyDate');
var PaymentReminderObject = require('./../BizLogic/PaymentReminderObject.js');
var StringConstants = require('../Constants/StringConstants');
var numberRegex = '/[^0-9]/';

module.exports = function PartyReminderController($) {
	var PartyPaymentReminderController = {
		defaults: {
			mountPoint: '',
			$el: '',
			partyId: ''
		},

		init: function init() {
			var defaults = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

			this.defaults = $.extend(this.defaults, defaults);
			this.render();
			this.defaults.$el = this.defaults.mountPoint.find('#reminderContainer');
			this.attachEvents();
		},
		render: function render() {
			var html = this.getMainTemplate();
			var $mountPoint = this.defaults.mountPoint;
			$mountPoint.html(html);
			$mountPoint.dialog({
				width: 390,
				height: 350,
				modal: true,
				close: function (e) {
					e.stopPropagation();
					this.defaults.$el.off('click', '**');
					$mountPoint.html('');
					$mountPoint.dialog('destroy');
				}.bind(this)
			});
			this.fillInputs();
			$('.paymentReminderDate').datepicker({ changeYear: true, dateFormat: 'dd/mm/yy', minDate: '-1000 Y', maxDate: '+100 Y' });
		},
		fillInputs: function fillInputs() {
			var dataloader = new DataLoader();
			var nameCache = new NameCache();
			var reminderModel = dataloader.getPaymentReminderStatus(this.defaults.partyId);
			var nameModel = nameCache.findNameObjectByNameId(this.defaults.partyId);
			$('#setReminderPartyName').html(nameModel.getFullName());
			$('#setReminderPartyAmount').html(MyDouble.getBalanceAmountWithDecimalAndCurrency(nameModel.getAmount()));
			var remindDate = reminderModel.getRemindOnDate() && reminderModel.getRemindOnDate() != 'Invalid Date' ? MyDate.getDate('d/m/y', reminderModel.getRemindOnDate()) : '';
			var sendSMSDate = reminderModel.getSendSMSOnDate() && reminderModel.getSendSMSOnDate() != 'Invalid Date' ? MyDate.getDate('d/m/y', reminderModel.getSendSMSOnDate()) : '';
			if (remindDate) {
				$('input[data-value="remind"]').prop('checked', true);
				$('#remindDate').prop('disabled', false);
				$('#setReminderMessageBody').html(StringConstants.REMINDER_NOTIFICATION_TEXT);
			}
			if (sendSMSDate) {
				$('input[data-value="sendSMS"]').prop('checked', true);
				$('#sendSMSDate').prop('disabled', false);
				$('#setReminderMessageBody').html(StringConstants.PARTY_SMS_TXT);
			}
			if (remindDate && sendSMSDate) {
				$('#setReminderMessageBody').html(StringConstants.PARTY_SMS_AND_REMINDER_NOTIFICATION_TEXT);
			}
			$('#remindDate').val(remindDate);
			$('#sendSMSDate').val(sendSMSDate);
		},
		attachEvents: function attachEvents() {
			this.defaults.$el.on('click', '#setAlerts', this.saveData.bind(this));
			this.defaults.$el.on('change', '.setReminderDiv input:checkbox', this.handleCheckboxChange.bind(this));
			this.defaults.$el.on('click', '#setReminderReset', this.reset.bind(this));
		},
		reset: function reset() {
			var partyId = this.defaults.partyId;
			var allDateInputs = $('input[data-action]');
			$('.setReminderDiv input:checkbox').each(function (index, element) {
				element.checked = false;
			});
			for (var i = 0; i < allDateInputs.length; i++) {
				$(allDateInputs[i]).val('');
				$(allDateInputs[i]).prop('disabled', true);
			}
			var paymentReminderObject = new PaymentReminderObject();
			paymentReminderObject.setNameId(partyId);
			var statusCode = paymentReminderObject.updateNoneDate();
			if (statusCode == ErrorCode.ERROR_PAYMENT_ALERT_DATE_SAVE_SUCCESS) {
				var SettingCache = require('../Cache/SettingCache');
				var settingcache = new SettingCache();
				var isCurrentCountryIndia = settingcache.isCurrentCountryIndia();
				ToastHelper.success('Dates have been reset');
				$('#setReminderMessageBody').html(isCurrentCountryIndia ? StringConstants.DEFAULT_REMINDER_TEXT : StringConstants.DEFAULT_REMINDER_TEXT_OUTSIDE_INDIA);
			} else {
				ToastHelper.error(statusCode);
			}
		},
		handleCheckboxChange: function handleCheckboxChange(event) {
			var action = event.currentTarget.dataset.value || '';
			var isChecked = event.currentTarget.checked;
			var dateInput = $('input[data-action=' + action + ']');
			if (isChecked) {
				if (action == 'sendSMS') {
					var _NameCache = require('../Cache/NameCache');
					var nameCache = new _NameCache();
					var partyObject = nameCache.findNameObjectByNameId(this.defaults.partyId);
					var phoneNumber = partyObject.getPhoneNumber();
					phoneNumber = (phoneNumber || '').replace(numberRegex, '').slice(-12);
					if (!phoneNumber) {
						ToastHelper.error('Recipient number required.');
					} else if (phoneNumber.length < 10) {
						ToastHelper.error('Recipient number is invalid.');
					}
				}
				dateInput.prop('disabled', false);
				dateInput.datepicker('show');
			} else {
				dateInput.prop('disabled', true);
			}
			var isSendSMSChecked = $('input[data-value="sendSMS"]').prop('checked');
			var isRemindChecked = $('input[data-value="remind"]').prop('checked');
			if (isSendSMSChecked && isRemindChecked) {
				$('#setReminderMessageBody').html(StringConstants.PARTY_SMS_AND_REMINDER_NOTIFICATION_TEXT);
			} else if (isSendSMSChecked) {
				$('#setReminderMessageBody').html(StringConstants.PARTY_SMS_TXT);
			} else if (isRemindChecked) {
				$('#setReminderMessageBody').html(StringConstants.REMINDER_NOTIFICATION_TEXT);
			} else {
				var SettingCache = require('../Cache/SettingCache');
				var settingCache = new SettingCache();
				var isCurrentCountryIndia = settingCache.isCurrentCountryIndia();
				$('#setReminderMessageBody').html(isCurrentCountryIndia ? StringConstants.DEFAULT_REMINDER_TEXT : StringConstants.DEFAULT_REMINDER_TEXT_OUTSIDE_INDIA);
			}
		},
		saveData: function saveData() {
			var nameCache = new NameCache();
			var partyId = this.defaults.partyId;
			var checkedFields = $('.setReminderDiv input:checkbox');
			var obj = (0, _create2.default)(null);
			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;

			try {
				for (var _iterator = (0, _getIterator3.default)(checkedFields), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					var field = _step.value;

					var action = field.dataset.value;
					var checked = field.checked;
					var alertDate = checked ? $('.setReminderDiv input[data-action=' + action + ']').val() : null;
					alertDate = alertDate ? MyDate.getDateObj(alertDate, 'dd/MM/yyyy', '/') : null;
					obj[action] = alertDate;
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}

			var paymentReminderObject = new PaymentReminderObject();
			var statusCodes = [];
			for (var action in obj) {
				var alertDate = obj[action];
				alertDate = alertDate && MyDate.getDate('y-m-d H:M:S', alertDate);
				paymentReminderObject.setNameId(partyId);
				if (action) {
					switch (action) {
						case 'sendSMS':
							var partyObject = nameCache.findNameObjectByNameId(partyId);
							var phoneNumber = partyObject.getPhoneNumber();
							phoneNumber = (phoneNumber || '').replace(numberRegex, '').slice(-12);
							if (alertDate) {
								if (!phoneNumber) {
									ToastHelper.error('Recipient number required.');
									return;
								}
								if (phoneNumber.length < 10) {
									ToastHelper.error('Recipient number is invalid.');
									return;
								}
							}
							var statusCode = paymentReminderObject.updateSendSMSOnDate(alertDate);
							statusCodes.push(statusCode);
							break;
						case 'remind':
							statusCode = paymentReminderObject.updateRemindOnDate(alertDate);
							statusCodes.push(statusCode);
							break;
					}
				}
			}
			var errorStatus = statusCodes.find(function (statusCode) {
				return statusCode != ErrorCode.ERROR_PAYMENT_ALERT_DATE_SAVE_SUCCESS;
			});
			if (errorStatus) {
				ToastHelper.error(errorStatus);
			} else {
				ToastHelper.success(ErrorCode.ERROR_PAYMENT_ALERT_DATE_SAVE_SUCCESS);
			}
			$('#setPaymentReminderDialog').dialog('close');
		},
		getMainTemplate: function getMainTemplate() {
			var SettingCache = require('../Cache/SettingCache');
			var settingCache = new SettingCache();
			var isCurrentCountryIndia = settingCache.isCurrentCountryIndia();
			return '\n\t\t\t\t<div id="reminderContainer">\n\t\t\t\t\t<div id="setReminderPartyDetails" class="setReminderPartyDetails">\n\t\t\t\t\t\t<span id="setReminderPartyName" class="setReminderPartyName">Rajat</span>\n\t\t\t\t\t\t<span id="setReminderPartyAmount" class="setReminderPartyAmount">$5000</span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div id="setReminderBody" class="setReminderBody">\n\t\t\t\t\t\t<div class="setReminderDiv">\n\t\t\t\t\t\t\t<label for=\'remindOn\' class=\'input-checkbox\'>\n\t\t\t\t\t\t\t\t<input type=\'checkbox\' id=\'remindOn\' data-value="remind">\n\t\t\t\t\t\t\t\t<svg width=\'18\' height=\'18\'><path class=\'checked\'  d=\'M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M7,14L2,9l1.4-1.4L7,11.2l7.6-7.6L16,5L7,14z\'/><path class=\'indeterminate\' d=\'M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M14,10H4V8h10V10z\'/><path class=\'unchecked\' fill=\'#097AA8\' d=\'M16,2v14H2V2H16 M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z\'/></svg>\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t<span>Remind me on</span>\n\t\t\t\t\t\t\t<input maxLength="30" data-action="remind" type="text" id="remindDate" class="paymentReminderDate" disabled readonly/>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t' + (isCurrentCountryIndia ? '\n\t\t\t\t\t\t<div class="setReminderDiv">\n\t\t\t\t\t\t\t<label for=\'sendSMSOn\' class=\'input-checkbox\'>\n\t\t\t\t\t\t\t\t<input type=\'checkbox\' id=\'sendSMSOn\' data-value="sendSMS">\n\t\t\t\t\t\t\t\t<svg width=\'18\' height=\'18\'><path class=\'checked\'  d=\'M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M7,14L2,9l1.4-1.4L7,11.2l7.6-7.6L16,5L7,14z\'/><path class=\'indeterminate\' d=\'M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z M14,10H4V8h10V10z\'/><path class=\'unchecked\' fill=\'#097AA8\' d=\'M16,2v14H2V2H16 M16,0H2C0.9,0,0,0.9,0,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0,16,0z\'/></svg>\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t<span>Remind Party on</span>\n\t\t\t\t\t\t\t<input maxLength="30" data-action="sendSMS" id="sendSMSDate" type="text" class="paymentReminderDate" disabled readonly/>\n\t\t\t\t\t\t</div>' : '') + '\n\t\t\t\t\t</div>\n\t\t\t\t\t<div id="setReminderMessage" class="setReminderMessage"><span id= "setReminderNote" class="setReminderNote">Note: </span><span id="setReminderMessageBody">' + (isCurrentCountryIndia ? StringConstants.DEFAULT_REMINDER_TEXT : StringConstants.DEFAULT_REMINDER_TEXT_OUTSIDE_INDIA) + '</span></div>\n\t\t\t\t\t<div id="setReminderFooter" class="setReminderFooter">\n\t\t\t\t\t\t<button class="paymentReminderButton setReminderReset floatLeft whiteBtn" id="setReminderReset">Delete Reminder</button>\n\t\t\t\t\t\t<button class="paymentReminderButton floatRight" id="setAlerts">Done</button>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t';
		}
	};
	return PartyPaymentReminderController.init.bind(PartyPaymentReminderController);
}(jQuery);