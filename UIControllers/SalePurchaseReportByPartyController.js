var startDate;
var endDate;
var DateFormat = require('./../Constants/DateFormat.js');
$('#startDate, #endDate').datepicker({ dateFormat: DateFormat.format });
var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();
var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');
var printForSharePDF = false;

var listOfTransactions = [];

var populateTable = function populateTable() {
	var DataLoader = require('./../DBManager/DataLoader.js');
	var dataLoader = new DataLoader();
	startDateString = $('#startDate').val();
	endDateString = $('#endDate').val();
	startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
	endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');
	var firmId = Number($('#firmFilterOptions').val());
	CommonUtility.showLoader(function () {
		listOfTransactions = dataLoader.getSalePurchaseReportByParty(startDate, endDate, firmId);
		displayTransactionList(listOfTransactions);
	});
};

$('#startDate').val(MyDate.getFirstDateOfCurrentMonthString());
var eDateForReport = new Date();
var lastDay = MyDate.getDateStringForUI(new Date(eDateForReport.getFullYear(), eDateForReport.getMonth() + 1, 0));
$('#endDate').val(lastDay);

if (settingCache.getMultipleFirmEnabled()) {
	var optionTemp = document.createElement('option');
	var FirmCache = require('./../Cache/FirmCache.js');
	var firmCache = new FirmCache();
	var firmList = firmCache.getFirmList();
	for (i in firmList) {
		var option = optionTemp.cloneNode();
		option.text = firmList[i].getFirmName();
		option.value = firmList[i].getFirmId();
		$('#firmFilterOptions').append(option);
	}
	$('#firmChooser').show();
} else {
	$('#firmChooser').hide();
}

$('#firmFilterOptions').on('selectmenuchange', function (event) {
	populateTable();
});

$('#startDate').change(function () {
	populateTable();
});

$('#endDate').change(function () {
	populateTable();
});

var optionTemp = document.createElement('option');
var transactionListArray = ['Sale', 'Purchase', 'Payment-In', 'Payment-Out', 'Credit Note', 'Debit Note', 'Sale Order', 'Purchase Order', 'Estimate', 'Delivery Challan'];
var transactionListId = [TxnTypeConstant.TXN_TYPE_SALE, TxnTypeConstant.TXN_TYPE_PURCHASE, TxnTypeConstant.TXN_TYPE_CASHIN, TxnTypeConstant.TXN_TYPE_CASHOUT, TxnTypeConstant.TXN_TYPE_SALE_RETURN, TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN, TxnTypeConstant.TXN_TYPE_SALE_ORDER, TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER, TxnTypeConstant.TXN_TYPE_ESTIMATE, TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN];
// var len = transactionsList.length;
for (i in transactionListArray) {
	var option = optionTemp.cloneNode();
	option.text = transactionListArray[i];
	option.value = transactionListId[i];
	$('#transactionFilterOptions').append(option);
}

$('#name').on('change', function (event) {
	populateTable();
});

$('#transactionFilterOptions').on('selectmenuchange', function () {
	populateTable();
});

var partyGroupSalePurchaseReportClusterize;

var displayTransactionList = function displayTransactionList(listOfTransactions) {
	var len = listOfTransactions.length;
	var rowData = [];
	var dynamicRow = '';
	dynamicRow += "<thead><tr><th width='40%'>Party Name</th><th width='30%' class='tableCellTextAlignRight'>Sale Amount</th><th width='30%' class='tableCellTextAlignRight'>Purchase Amount</th></tr></thead>";
	//
	var totalSaleAmount = 0;
	var totalPurchaseAmount = 0;
	for (var _i = 0; _i < len; _i++) {
		var row = '';
		var txn = listOfTransactions[_i];
		var partyName = txn.get('partyName');
		var saleAmount = txn.get('saleAmount');
		var purchaseAmount = txn.get('purchaseAmount');
		row += "<tr ondblclick='openPartyDetails(" + txn.get('nameId') + ")' class='currentRow'><td width='40%'>" + partyName + "</td><td width='30%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(saleAmount) + "</td><td width='30%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(purchaseAmount) + '</td></tr>';
		rowData.push(row);
		totalSaleAmount += saleAmount;
		totalPurchaseAmount += purchaseAmount;
	}
	var data = rowData;

	if ($('#salePurchaseReportByPartyContainer').length === 0) {
		return;
	}
	if (partyGroupSalePurchaseReportClusterize && partyGroupSalePurchaseReportClusterize.destroy) {
		partyGroupSalePurchaseReportClusterize.clear();
		partyGroupSalePurchaseReportClusterize.destroy();
	}

	partyGroupSalePurchaseReportClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollArea',
		contentId: 'contentArea',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});

	$('#tableHead').html(dynamicRow);
	$('#totalSaleQuantity').html(MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalSaleAmount));
	$('#totalPurchaseQuantity').html(MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalPurchaseAmount));
};

var openPartyDetails = function openPartyDetails(nameId) {
	var PartiesContainer = require('../UIComponent/jsx/PartiesContainer').default;
	var MountComponent = require('../UIComponent/jsx/MountComponent').default;
	MountComponent(PartiesContainer, document.querySelector('#defaultPage'), {
		partyId: Number(nameId)
	});
};

var openTransaction = function openTransaction(txnId, txnType) {
	var ViewTransaction = require('./../BizLogic/ViewTransaction.js');
	var viewTransaction = new ViewTransaction();
	viewTransaction.viewTransactionFile(txnId + ':' + TxnTypeConstant.getTxnType(txnType));
};

if (settingCache.getItemEnabled()) {
	$('#itemDetailPrintDiv').show();
} else {
	$('#itemDetailPrintDiv').hide();
}

function openPrintOptions(sharePDF) {
	printForSharePDF = sharePDF;
	printReport();
}

function closePrintOptions() {
	$('#customPrintDialog').addClass('hide');
	$('#customPrintDialog').dialog('close').dialog('destroy');
}

function printReport() {
	if (printForSharePDF) {
		pdfHandler.sharePdf(getHTMLTextForReport(), 'Report', false);
	} else {
		pdfHandler.printPdf(getHTMLTextForReport(), 'Report', false);
	}
	// closePrintOptions();
}

var getHTMLTextForReport = function getHTMLTextForReport() {
	var SalePurchaseReportByPartyHTMLGenerator = require('./../ReportHTMLGenerator/SalePurchaseReportByPartyHTMLGenerator.js');
	var salePurchaseReportByPartyHTMLGenerator = new SalePurchaseReportByPartyHTMLGenerator();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var firmId = Number($('#firmFilterOptions').val());
	var nameString = $('#name').val();
	if (!nameString) {
		nameString = 'All parties';
	}
	var htmlText = salePurchaseReportByPartyHTMLGenerator.getHTMLText(listOfTransactions, startDateString, endDateString, firmId, nameString);
	return htmlText;
};

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	// $('#frameDiv').load('');
	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable();
};

/// ///////////////////////////PDF/////////////////////////////////////
var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.PARTY_SALE_PURCHASE_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

if (settingCache.getItemEnabled()) {
	$('#itemDetailsExcel').show();
} else {
	$('#itemDetailsExcel').hide();
}

function openExcelOptions() {
	$('#ExcelDialog').removeClass('hide');
	$('#ExcelDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closeExcelOptions() {
	$('#ExcelDialog').addClass('hide');
	$('#ExcelDialog').dialog('close').dialog('destroy');
}

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.PARTY_SALE_PURCHASE_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
	excelHelper.saveExcel(fileName);
};

var prepareObjectForExcel = function prepareObjectForExcel(listOfTransactions) {
	var len = listOfTransactions.length;
	var rowObj = [];
	var tableHeadArray = [];
	var totalArray = [];
	tableHeadArray.push('Party Name');
	tableHeadArray.push('Sale Amount');
	tableHeadArray.push('Purchase Amount');
	rowObj.push(tableHeadArray);
	rowObj.push([]);

	var totalSaleAmount = 0;
	var totalPurchaseAmount = 0;
	for (var j = 0; j < len; j++) {
		var tempArray = [];
		var txn = listOfTransactions[j];
		var partyName = txn.get('partyName');
		var saleAmount = txn.get('saleAmount');
		var purchaseAmount = txn.get('purchaseAmount');
		tempArray.push(partyName);
		tempArray.push(MyDouble.getQuantityWithDecimalWithoutColor(saleAmount));
		tempArray.push(MyDouble.getQuantityWithDecimalWithoutColor(purchaseAmount));
		totalSaleAmount += saleAmount;
		totalPurchaseAmount += purchaseAmount;
		rowObj.push(tempArray);
	}
	rowObj.push([]);
	totalArray.push('Total');
	totalArray.push(MyDouble.getQuantityWithDecimalWithoutColor(totalSaleAmount));
	totalArray.push(MyDouble.getQuantityWithDecimalWithoutColor(totalPurchaseAmount));
	rowObj.push(totalArray);
	return rowObj;
};

function printExcel() {
	var XLSX = require('xlsx');
	var dataArray = prepareObjectForExcel(listOfTransactions);
	var worksheet = XLSX.utils.aoa_to_sheet(dataArray);

	var workbook = {
		'SheetNames': [],
		'Sheets': {}
	};

	var wsName = 'Sale Purchase By Party';
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet;
	var wscolWidth = [{ wch: 40 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 15 }, { wch: 15 }];
	worksheet['!cols'] = wscolWidth;
	console.log(workbook);
	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

populateTable();