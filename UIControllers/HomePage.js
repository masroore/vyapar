var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var fs = require('fs');
var Domain = require('./../Constants/Domain');
var domain = Domain.thisDomain;

var _require = require('electron'),
    ipcRenderer = _require.ipcRenderer;

var globalErrorMessage;
var isCompanyListView = false;
var MyAnalytics = require('../Utilities/analyticsHelper.js');
var LicenseInfoConstant = require('./../Constants/LicenseInfoConstant.js');
var SendLeadsInfo = require('../Utilities/SendLeadsInfo');
// global socket variable
var socket;

var MyDate = require('./../Utilities/MyDate.js');
var MyDouble = require('./../Utilities/MyDouble.js');
var Messages = require('./../Constants/Messages.js');
var StringConstant = require('./../Constants/StringConstants.js');
var StateCode = require('./../Constants/StateCode.js');
var isProduction = true;
var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();
var DecimalInputFilter = require('./../Utilities/DecimalInputFilter.js');
var CompanyListUtility = require('./../Utilities/CompanyListUtility.js');
var UIHelper = require('./../UIControllers/UIHelper.js');
var ToastHelper = require('./../UIControllers/ToastHelper');
var ErrorCode = require('./../Constants/ErrorCode.js');
var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var MyNotification = require('./../Utilities/MyNotification');
var PaymentReminderHelper = require('./../Utilities/paymentReminderHelper');
var CommonUtility = require('./../Utilities/CommonUtility');
var SyncHandler = require('./../Utilities/SyncHandler.js');

var app = require('electron').remote.app;

var appPath = app.getPath('userData'); // This is in use in whole application

if (isProduction) {
	console.log = function () {
		// Overriding it so that it does not print anything in production
	};
}
var logger = require('./../Utilities/logger');
var AnalyticsExceptionTracker = require('./../Utilities/AnalyticsExceptionTracker.js');

window.onerror = function (message, source, lineno, colno, error) {
	try {
		AnalyticsExceptionTracker.logErrorToAnalytics(error.toString() + ' :: ' + source + ' :: ' + lineno + error.stack, false);
	} catch (e) {}
};
try {
	MyAnalytics.pushEvent('App Opened - 1');
	MyAnalytics.pushScreen('Home Page');
} catch (ex) {}

var delay = function () {
	var timer = 0;
	return function (callback, ms) {
		clearTimeout(timer);
		timer = setTimeout(callback, ms);
	};
}();

$(document).off('click', '.hasDatepicker').on('click', '.hasDatepicker', function () {
	$(this).trigger('focus');
});

var FirmCache = require('./../Cache/FirmCache.js');
var firmCache = new FirmCache();

var licenseCheck = function licenseCheck() {
	var LicenseUtility = require('../Utilities/LicenseUtility');
	var licenseInfoDetails = LicenseUtility.getLicenseInfo();
	MyAnalytics.pushProfile({
		'License Type': licenseInfoDetails[LicenseInfoConstant.licenseUsageType]
	});
	LicenseUtility.licenseTab();
};
licenseCheck();

var Queries = require('./../Constants/Queries.js');

var SettingsModel = require('./../Models/SettingsModel.js');
var settingsModel = new SettingsModel();

SendLeadsInfo.pushLeadsToServer();

var Country = require('./../Constants/Country.js');
var TaxSetup = require('./../CompanySetup/TaxSetup.js');
var selectedCountryCode = settingCache.getCountryCode();
if (!selectedCountryCode || !selectedCountryCode.trim()) {
	var listOfCountries = Country.getCountryList();
	var listOfCountriesToAppend = '';

	for (var i = 0; i < listOfCountries.length; i++) {
		listOfCountriesToAppend += '<option value="' + listOfCountries[i].countryCode + '">' + listOfCountries[i].countryName + '</option>';
	}

	$('#userCountry').append(listOfCountriesToAppend);
	$('#userCountry').val(Country.Countries.INDIA.countryCode);
	$('#selectCountry').removeClass('hide').dialog({
		title: 'Select Country',
		closeOnEscape: false,
		closeText: 'hide',
		width: 'auto',
		modal: true,
		open: function open(event, ui) {
			$('.ui-dialog-titlebar-close').hide();
		}
	});
} else if (!settingCache.isTaxSetupCompleted()) {
	var selectedCountry = Country.getCountryFromCountryCode(selectedCountryCode);
	var taxSetupStatus = TaxSetup.setupTax(selectedCountry);
	if (taxSetupStatus != ErrorCode.SUCCESS) {
		ToastHelper.error(taxSetupStatus);
	}
}

$('#terminalButtonSelectCountry').on('click', function () {
	var selectedCountryCode = $('#userCountry').val();
	var selectedCountry = Country.getCountryFromCountryCode(selectedCountryCode);
	if (selectedCountry) {
		settingsModel.setSettingKey(Queries.SETTING_USER_COUNTRY);
		settingsModel.UpdateSetting(selectedCountry.countryCode);
		if (!settingCache.isTaxSetupCompleted()) {
			var _taxSetupStatus = TaxSetup.setupTax(selectedCountry);
			if (_taxSetupStatus != ErrorCode.SUCCESS) {
				ToastHelper.error(_taxSetupStatus);
			}
		}
		var currenciesForSelectedCountry = selectedCountry.currencySymbols;
		if (currenciesForSelectedCountry && currenciesForSelectedCountry.length && currenciesForSelectedCountry.length > 0) {
			var defaultCurrencySymbol = currenciesForSelectedCountry[0];
			settingsModel.setSettingKey(Queries.SETTING_CURRENCY_SYMBOL);
			settingsModel.UpdateSetting(defaultCurrencySymbol);
		}

		var InvoiceTheme = require('./../Constants/InvoiceTheme.js');

		if (selectedCountry == Country.Countries.INDIA) {
			settingsModel.setSettingKey(Queries.SETTING_GST_ENABLED);
			settingsModel.UpdateSetting('1');
			settingsModel.setSettingKey(Queries.SETTING_HSN_SAC_ENABLED);
			settingsModel.UpdateSetting('1');
			settingsModel.setSettingKey(Queries.SETTING_ITEMWISE_TAX_ENABLED);
			settingsModel.UpdateSetting('1');
			settingsModel.setSettingKey(Queries.SETTING_ITEMWISE_DISCOUNT_ENABLED);
			settingsModel.UpdateSetting('1');
			settingsModel.setSettingKey(Queries.SETTING_TIN_NUMBER_ENABLED);
			settingsModel.UpdateSetting('1');
			settingsModel.setSettingKey(Queries.SETTING_ENABLE_PLACE_OF_SUPPLY);
			settingsModel.UpdateSetting('1');
			settingsModel.setSettingKey(Queries.SETTING_TAX_ENABLED);
			settingsModel.UpdateSetting('0');
			settingsModel.setSettingKey(Queries.SETTING_PRINT_TINNUMBER);
			settingsModel.UpdateSetting('1');
			settingsModel.setSettingKey(Queries.SETTING_CUSTOM_NAME_FOR_SALE);
			settingsModel.UpdateSetting('Tax Invoice');
		} else if (Country.isGulfCountryByCountry(selectedCountry)) {
			settingsModel.setSettingKey(Queries.SETTING_ITEMWISE_TAX_ENABLED);
			settingsModel.UpdateSetting('1');
			settingsModel.setSettingKey(Queries.SETTING_ITEMWISE_DISCOUNT_ENABLED);
			settingsModel.UpdateSetting('1');
			settingsModel.setSettingKey(Queries.SETTING_TIN_NUMBER_ENABLED);
			settingsModel.UpdateSetting('1');
			settingsModel.setSettingKey(Queries.SETTING_PRINT_TINNUMBER);
			settingsModel.UpdateSetting('1');
			settingsModel.setSettingKey(Queries.SETTING_THERMAL_PRINTER_THEME);
			settingsModel.UpdateSetting(InvoiceTheme.THERMAL_THEME_2 + '');
		} else {
			if (selectedCountry == Country.Countries.NEPAL) {
				settingsModel.setSettingKey(Queries.SETTING_EXPIRY_DATE_TYPE);
				settingsModel.UpdateSetting(StringConstant.dateMonthYear);
			}
			settingsModel.setSettingKey(Queries.SETTING_TAX_ENABLED);
			settingsModel.UpdateSetting('1');
			settingsModel.setSettingKey(Queries.SETTING_DISCOUNT_ENABLED);
			settingsModel.UpdateSetting('1');
		}
		$('#selectCountry').addClass('hide').dialog('destroy');
		if (settingCache.isItemwiseTaxEnabled() || settingCache.getTaxEnabled()) {
			$('#viewTax').removeClass('hide'); // .attr('accessKey','4');
		}
	} else {
		ToastHelper.error('Selected country is not present in our country list.');
	}
});

// endregion
var toggleLeftNav = $('#toggleLeftNav');
var sideNav = $('#sideNav');
var headingNav = $('#headingNav');
var centreDiv = $('#centreDiv');
$(toggleLeftNav).on('click', toggleMenu);

function toggleMenu() {
	setTimeout(function () {
		document.dispatchEvent(new CustomEvent('resize-grid'));
	}, 100);
	if ($(sideNav).css('width') == '50px') {
		// if sidenav is closed
		sideNav.css('width', '15%');
		centreDiv.css('width', '85%');
		headingNav.css('width', '85%');
		$('.companyImage img').css('width', '46px');
		if ($('#shortcuts').css('width') == '30px') {
			var dynamicDivWidth = parseInt(centreDiv.css('width').replace(/px/, '')) - parseInt(sideNav.css('width').replace(/px/, '')) - 105 - 30 + 'px';
			$('#dynamicDiv').css('width', dynamicDivWidth);
		}
		setTimeout(function () {
			$('.textInsideMenu').css('display', 'block');
			$('.company').css('display', 'block');
		}, 200);
	} else {
		// if sidenav is open
		$('.companyImage img').css('width', '24px');
		var sideNavWidth = $(sideNav).css('width');
		var headerNavWidth = $(headingNav).css('width');
		var centreDivWidth = $(centreDiv).css('width');
		var totalWidthHeader = parseInt(sideNavWidth.replace(/px/, '')) + parseInt(headerNavWidth.replace(/px/, '')) - 50 + 'px';
		var totalWidthCentre = parseInt(sideNavWidth.replace(/px/, '')) + parseInt(centreDivWidth.replace(/px/, '')) - 50 + 'px';
		$('.textInsideMenu').css('display', 'none');
		$('.company').css('display', 'none');
		$(sideNav).css('width', '50px');
		$(headingNav).css('width', totalWidthHeader);
		$(centreDiv).css('width', totalWidthCentre);
		if ($('#shortcuts').css('width') == '30px') {
			var dynamicDivWidth = parseInt(totalWidthCentre.replace(/px/, '')) - 30 + 'px';
			console.log(totalWidthCentre);
			console.log(dynamicDivWidth);
			$('#dynamicDiv').css('width', dynamicDivWidth);
			console.log($('#dynamicDiv').css('width'));
		}
	}
}

var showPaymentHistory = function showPaymentHistory(that) {
	var selectedTransactionMap = new _map2.default();
	var txnId = that.split(':')[0];

	var DataLoader = require('./../DBManager/DataLoader.js');
	var dataLoader = new DataLoader();

	var txnObj = dataLoader.LoadTransactionFromId(txnId);

	var cashReceivedAmount = txnObj.getCashAmount();
	var discountAmountInHistory = MyDouble.convertStringToDouble(txnObj.getDiscountAmount(), true);

	var txnLinksList = dataLoader.loadAllTransactionLinksByTxnId(txnId);

	if (txnLinksList) {
		for (var i = 0; i < txnLinksList.length; i++) {
			var linkTxn = txnLinksList[i];
			var txnLink1Id = MyDouble.convertStringToDouble(linkTxn.getTxnLinksTxn1Id());
			var txnLink2Id = MyDouble.convertStringToDouble(linkTxn.getTxnLinksTxn2Id());
			var txnLinkAmount = MyDouble.convertStringToDouble(linkTxn.getTxnLinksAmount());
			var closedTxnId = MyDouble.convertStringToDouble(linkTxn.getTxnLinkClosedTxnRefId());

			if (Number(txnId) == txnLink1Id) {
				var selectedTxnId = txnLink2Id;
			} else if (Number(txnId) == txnLink2Id) {
				var selectedTxnId = txnLink1Id;
			}

			if (selectedTxnId > 0) {
				var linkTxnObject = dataLoader.LoadTransactionFromId(selectedTxnId);

				var linkTxnObjectTxnCurrentBalance = linkTxnObject.getTxnCurrentBalanceAmount();
			} else {
				// for uneditable closed linked transactions
				var linkTxnObject = dataLoader.loadClosedTransactionDataById(closedTxnId);

				selectedTxnId = i + 'closed';
			}

			selectedTransactionMap.set(selectedTxnId, [linkTxnObject, txnLinkAmount]);
		}
	}

	var MountComponent = require('../UIComponent/jsx/MountComponent').default;
	var Component = require('../UIComponent/jsx/PaymentHistoryModal').default;
	MountComponent(Component, document.querySelector('#paymentHistoryTable'), {
		txnType: txnObj.getTxnType(),
		totalReceivedAmount: cashReceivedAmount,
		receivedLater: 0,
		selectedTransactionMap: selectedTransactionMap,
		discountAmountInHistory: discountAmountInHistory,
		isOpen: true,
		onClose: function onClose() {
			unmountReactComponent(document.querySelector('#paymentHistoryTable'));
		}
	});
};

if (settingCache.isCurrentCountryIndia()) {
	$('#referAndEarnTab').show();
}

setTimeout(function () {
	if (!settingCache.isSettingPushedToCleverTap() && settingCache.isTaxSetupCompleted()) {
		var SettingsModel = require('./../Models/SettingsModel.js');
		var settingModel = new SettingsModel();
		var Queries = require('./../Constants/Queries.js');

		var allSettings = settingCache.LoadAllSettings();

		MyAnalytics.logSetting(allSettings);

		var windowHeight = window.screen.availHeight;
		var windowWidth = window.screen.availWidth;

		MyAnalytics.pushProfile({
			Resolution: windowWidth + ' X ' + windowHeight
		});

		settingModel.setSettingKey(Queries.SETTING_PUSH_SETTING_TO_CLEVERTAP_DESKTOP);
		settingModel.UpdateSetting('1', { nonSyncableSetting: true });
	}
}, 3000);

function submitReferralCodeFunction() {
	var refCode = $('#referralCodeValue').val();
	if (refCode && refCode.trim && refCode.trim()) {
		$('#referralCodeDialog').dialog('close');
		var LocalStorageHelper = require('./../Utilities/LocalStorageHelper');
		LocalStorageHelper.setValue(LicenseInfoConstant.referralCode, refCode.trim());
		settingsModel.setSettingKey(Queries.SETTING_LEADS_INFO_SENT);
		settingsModel.UpdateSetting('0', { nonSyncableSetting: true });
		licenseCheck();
		SendLeadsInfo.pushLeadsToServer();
		$('.referralCode').addClass('hide');
	}
}

if (SyncHandler.isSyncEnabled()) {
	$('#syncul').show();
} else {
	$('#syncul').hide();
}

function functionToEnterReferralCode() {
	$('#referralCodeDialog').dialog({
		close: function close() {
			$(this).dialog('close');
		}
	});
}

referralCodeShowHide();
function referralCodeShowHide() {
	var LocalStorageHelper = require('../Utilities/LocalStorageHelper');
	var referralCode = LocalStorageHelper.getValue(LicenseInfoConstant.referralCode);
	if (referralCode) {
		$('.referralCode').addClass('hide');
	} else {
		$('.referralCode').removeClass('hide').on('click', functionToEnterReferralCode);
	}
}

if (settingCache.isItemwiseTaxEnabled() || settingCache.getTaxEnabled()) {
	$('#viewTax').removeClass('hide'); // .attr('accessKey','4');
}

// expandable code
$(function () {
	$('#dialogForLicenseExpire').dialog({
		autoOpen: false,
		resizable: false,
		height: 'auto',
		width: 800,
		modal: true,
		buttons: {
			'Buy License': function BuyLicense() {
				$(this).dialog('close');
				document.getElementById('trialText').click();
			},
			Cancel: function Cancel() {
				$('#defaultPage').show();
				$('.viewItems').show();
				$('#frameDiv').empty();
				// $('#defaultPage').load('BusinessStatus.html');
				$('#modelContainer').css({
					display: 'none'
				});
				$(this).dialog('close');
				$(this).dialog('close');
			}
		},
		close: function close(event, ui) {
			window.onResume = null;
			$('#defaultPage').show();
			$('.viewItems').show();
			$('#frameDiv').empty();
			//	$('#defaultPage').load('BusinessStatus.html');
			$('#modelContainer').css({
				display: 'none'
			});
			$(this).dialog('close');
			$('#close').click();
			// $('#closeShortcut').click();
			$(this).dialog('close');
		},
		create: function create() {
			$(this).closest('.ui-dialog').find('.ui-button').eq(1).addClass('terminalButton margin-right'); // the first button
		}
	});
});

$(function () {
	$('#dialogForUserOffline').dialog({
		autoOpen: false,
		resizable: false,
		height: 'auto',
		width: 820,
		modal: true,

		buttons: [{
			text: 'Ok',
			id: 'offlineOk',
			class: 'terminalButton',
			click: function click() {
				var CommonUtility = require('../Utilities/CommonUtility');
				var Dashboard = require('../UIComponent/jsx/Dashboard/Dashboard').default;
				CommonUtility.MountDefaultPage(Dashboard);
				$(this).dialog('close');
			}
		}],

		close: function close() {
			var CommonUtility = require('../Utilities/CommonUtility');
			var Dashboard = require('../UIComponent/jsx/Dashboard/Dashboard').default;
			CommonUtility.MountDefaultPage(Dashboard);
			$(this).dialog('close');
		}
	});

	$('#dialogForLockedEntity').dialog({
		autoOpen: false,
		resizable: false,
		height: 'auto',
		width: 820,
		modal: true,

		buttons: [{
			text: 'Ok',
			id: 'lockedOk',
			class: 'terminalButton',
			click: function click() {
				$(this).dialog('close');
			}
		}],

		close: function close() {
			$('#defaultPage').show();
			$('#frameDiv').empty();
			if ($('#SalePurchaseFormDialogRef').length) {
				unmountReactComponent(document.querySelector('#salePurchaseContainer'));
			}
			$('#modelContainer').css({
				display: 'none'
			});
			$(this).dialog('close');
		}
	});
});

(function ($) {
	$.event.special.destroyed = {
		remove: function remove(o) {
			if (o.handler) {
				o.handler();
			}
		}
	};
})(jQuery);

require('ui-contextmenu');
var navMenu = $('#scrollableMenu ul');

var saveBusinessName = function saveBusinessName() {
	var latestFirmValue = $('.enterBusinessName').val();
	latestFirmValue = latestFirmValue && latestFirmValue.trim();
	if (!latestFirmValue) {
		$('.enterBusinessName').val('');
		ToastHelper.error('Please provide valid business name');
		return;
	}
	var FirmCache = require('./../Cache/FirmCache.js');
	var firmCache = new FirmCache();
	var oldFirmObj = firmCache.getDefaultFirm();
	oldFirmObj.setFirmName(latestFirmValue);
	var statusCode = oldFirmObj.updateFirmWithoutObject();
	if (statusCode == ErrorCode.ERROR_FIRM_UPDATE_SUCCESS) {
		updateCompanyInfo();
		ToastHelper.success(statusCode);
		$('.searchField').removeClass('hide');
		$('.businessNameDiv').addClass('hide');
	} else {
		ToastHelper.error(statusCode);
	}
};

function updateCompanyInfo() {
	var FirmCache = require('./../Cache/FirmCache.js');
	var firmCache = new FirmCache();
	firmCache.refreshFirmCache();
	var settingCache = new SettingCache();
	var defaultFirm = settingCache.getDefaultFirmId();
	var company = firmCache.getFirmById(defaultFirm);

	if (company) {
		var companyName = company.getFirmName(); // Todo:Ishwar. company is coming as null
		var companyLogo = company.getFirmImagePath();
		$('.company').html(companyName);
		if (companyLogo) {
			$('.companyImage').attr('src', 'data:image/gif;base64,' + companyLogo);
			$('.companyImage, .companyImageContainer').removeClass('addLogo');
		} else {
			$('.companyImage').attr('src', '../inlineSVG/Add_logo_for_compnay.svg');
			$('.companyImage, .companyImageContainer').addClass('addLogo');
		}
		if (company.getFirmName() == StringConstant.MY_COMPANY) {
			$('.searchField').addClass('hide');
			$('.businessNameDiv').removeClass('hide');
			$('.welcomeDiv').addClass('hide');
			$('.enterBusinessName').on('focus', function () {
				$('.saveBusinessName').removeClass('hide');
				$('.businessNameDiv>.dot').addClass('hide');
				$('.enterBusinessName').removeClass('noBorderBottom');
			});
			$('.saveBusinessName').on('mousedown', saveBusinessName);
			$('.enterBusinessName').on('keyup', function (event) {
				if (event.keyCode === 13) {
					saveBusinessName();
				}
			});
			$('.enterBusinessName').on('blur', function () {
				$('.enterBusinessName').val('');
				$('.enterBusinessName').addClass('noBorderBottom');
				$('.businessNameDiv>.dot').removeClass('hide');
				$('.saveBusinessName').addClass('hide');
			});
		} else {
			$('.searchField').removeClass('hide');
			$('.businessNameDiv').addClass('hide');
			$('.welcomeDiv').removeClass('hide');
		}
	}
}

setTimeout(updateCompanyInfo, 1);

$('#scrollableMenu .navMenu li.expandable-menu').keypress(function (e) {
	if (e.keyCode === 13) {
		$(this).find('.expandHeader').click();
	}
});
$('.expandHeader').on('click', function () {
	var that = $(this);
	var isSelfClicked = that.find('.rotate180').hasClass('rotate180');
	$('.expandable-open').slideUp();
	$('.expand-icon').find('[class="rotate180"]').toggleClass('rotate180');
	$('.expandHeader .textInsideMenu').removeClass('white-text');
	if (!isSelfClicked) {
		var expandableOpen = $(this).closest('li').next('.expandable-open');
		expandableOpen.slideToggle(300, 'easeOutQuad');
		$(this).parent().find('.expand-icon img').toggleClass('rotate180');
		$(this).parent().find('.expandHeader .textInsideMenu').toggleClass('white-text');
	}
});

$('.exportItem').off('click').on('click', function () {
	var ItemCache = require('./../Cache/ItemCache.js');
	var itemCache = new ItemCache();
	MyAnalytics.pushEvent('Export Item Start');
	if (exportItemClusterize && exportItemClusterize.destroy) {
		exportItemClusterize.clear();
		exportItemClusterize.destroy();
	}
	var exportItemList = itemCache.getListOfItemsObject();
	$('#contentAreaExportItem').html('');
	$('#exportItemButton').off('click').on('click', exportItemList, exportItems);
	$('#exportItemDialog').removeClass('hide').dialog({
		width: 1000,
		height: 500,
		appendTo: '#dynamicDiv',
		classes: {
			'ui-dialog': 'exportItemZIndex'
		}
	});

	var SettingCache = require('./../Cache/SettingCache.js');
	var TaxCodeCache = require('./../Cache/TaxCodeCache.js');
	var ItemCategoryCache = require('../Cache/ItemCategoryCache');
	var ItemUnitCache = require('../Cache/ItemUnitCache');
	var ItemUnitMappingCache = require('../Cache/ItemUnitMappingCache');
	var taxCodeCache = new TaxCodeCache();
	var itemCategoryCache = new ItemCategoryCache();
	var itemUnitCache = new ItemUnitCache();
	var itemUnitMappingCache = new ItemUnitMappingCache();
	var settingCache = new SettingCache();
	var isItemCategoryEnabled = settingCache.isItemCategoryEnabled();
	var isItemUnitEnabled = settingCache.getItemUnitEnabled();
	var isStockEnabled = settingCache.getStockEnabled();
	var isItemDescriptionEnabled = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_DESCRIPTION);

	var header = '<tr style=\'text-align:left;background-color: #fbfbfb;\'>\n\t\t<th style=\'width:9%\'>Item Name</th>\n\t\t<th style=\'width:5%;  padding: 12px 6px\'>Item Code</th>\n\t\t' + (isItemDescriptionEnabled ? '<th style=\'width:9%;  padding: 12px 6px\'>Description</th>' : '') + '\n\t\t' + (isItemCategoryEnabled ? '<th style=\'width:8%;  padding: 12px 6px\'>Category</th>' : '') + '\n\t\t<th style=\'width:5%\'>HSN</th>\n\t\t<th style=\'width:5%\'>Sale Price</th>\n\t\t<th style=\'width:6%\'>Purchase Price</th>\n\t\t<th style=\'width:6%\'>Current Stock</th>\n\t\t' + (isStockEnabled ? '<th style=\'width:5%\'>Min Stock</th>\n\t\t\t<th style=\'width:8%\'>Item Location</th>\n\t\t\t<th style=\'width:4%\'>Tax Rate</th>' : '') + '\n\t\t<th style=\'width:8%\'> Tax Type</th>\n\t\t' + (isItemUnitEnabled ? '<th style=\'width:8%\'> Base Unit</th>\n\t\t\t<th style=\'width:8%\'> Secondary Unit</th>\n\t\t\t<th style=\'width:5%\'> Conv. Rate</th>' : '') + '\n\t\t</tr>';
	$('#tableHeadexportItemList').html(header);

	var exportItemClusterize;
	var rowData = [];
	$.each(exportItemList, function (key, value) {
		var taxCode = value.getItemTaxId() ? taxCodeCache.getTaxCodeNameById(value.getItemTaxId()) : 'none';
		var category = itemCategoryCache.getItemCategoryObjectById(value.getItemCategoryId());
		var categoryName = category ? category.getCategoryName() : '';
		var baseUnit = itemUnitCache.getItemUnitNameById(value.getBaseUnitId()) || '';
		var secondaryUnit = itemUnitCache.getItemUnitNameById(value.getSecondaryUnitId()) || '';
		var mapping = itemUnitMappingCache.getItemUnitMapping(value.getUnitMappingId());
		var conversionRate = mapping && mapping.getConversionRate() || 0;

		var row = '\n\t\t<tr>\n\t\t\t<td style=\'width:9%; padding-left:6px;\'>' + value.getItemName() + '</td>\n\t\t\t<td style=\'width:5%;\'>' + (value.getItemCode() || '') + '</td>\n\t\t\t' + (isItemDescriptionEnabled ? '<td style=\'width:9%;\'>' + (value.getItemDescription() || '') + '</td>' : '') + '\n\t\t\t' + (isItemCategoryEnabled ? '<td style=\'width:8%;\'>' + categoryName + '</td>' : '') + '\n\t\t\t<td style=\'width:5%;\'>' + (value.getItemHSNCode() || '') + '</td>\n\t\t\t<td style=\'width:5%;\'>' + MyDouble.getAmountWithDecimal(value.getItemSaleUnitPrice()) + '</td>\n\t\t\t<td style=\'width:6%;\'>' + MyDouble.getAmountWithDecimal(value.getItemPurchaseUnitPrice()) + '</td>\n\t\t\t' + (isStockEnabled ? '<td style=\'width:6%;\'>' + MyDouble.getQuantityWithDecimalWithoutColor(value.getItemStockQuantity()) + '</td>\n\t\t\t\t<td style=\'width:5%;\'>' + MyDouble.getQuantityWithDecimalWithoutColor(value.getItemMinStockQuantity()) + '</td>\n\t\t\t\t<td style=\'width:8%;\'>' + (value.getItemLocation() || '') + '</td>' : '') + '\n\t\t\t<td style=\'width:4%;\'>' + taxCode + '</td>\n\t\t\t<td style=\'width:8%;\'>' + (value.getItemTaxTypeSale() == 1 ? 'inclusive' : 'exclusive') + '</td>\n\t\t\t' + (isItemUnitEnabled ? '<td style=\'width:8%;\'>' + baseUnit + '</td>\n\t\t\t\t<td style=\'width:8%;\'>' + secondaryUnit + '</td>\n\t\t\t\t<td style=\'width:5%;\'>' + (MyDouble.getAmountWithDecimal(conversionRate) || '') + '</td>' : '') + '\n\t\t\t</tr>\n\t\t';
		rowData.push(row);
	});

	var data = rowData;
	if (exportItemClusterize && exportItemClusterize.destroy) {
		exportItemClusterize.clear();
		exportItemClusterize.destroy();
	}
	exportItemClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollAreaExportItem',
		contentId: 'contentAreaExportItem',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});
});

var downloadExcelFileForExportItem = function downloadExcelFileForExportItem() {
	var ExcelHelper = require('../Utilities/ExcelHelper');
	var excelHelper = new ExcelHelper();
	excelHelper.saveExcel('Export Items');
	MyAnalytics.pushEvent('Export Item Successful');
};

var prepareObjectForExcelForExportItem = function prepareObjectForExcelForExportItem(listOfTransactions) {
	var SettingCache = require('./../Cache/SettingCache.js');
	var TaxCodeCache = require('./../Cache/TaxCodeCache.js');
	var ItemCategoryCache = require('../Cache/ItemCategoryCache');
	var ItemUnitCache = require('../Cache/ItemUnitCache');
	var ItemUnitMappingCache = require('../Cache/ItemUnitMappingCache');

	var taxCodeCache = new TaxCodeCache();
	var itemCategoryCache = new ItemCategoryCache();
	var itemUnitCache = new ItemUnitCache();
	var itemUnitMappingCache = new ItemUnitMappingCache();
	var settingCache = new SettingCache();
	var isItemCategoryEnabled = settingCache.isItemCategoryEnabled();
	var isItemUnitEnabled = settingCache.getItemUnitEnabled();
	var isStockEnabled = settingCache.getStockEnabled();
	var isItemDescriptionEnabled = settingCache.isAdditionalItemDetailsEnabled(Queries.SETTING_ENABLE_ITEM_DESCRIPTION);

	var rowObj = [];
	var tableHeadArray = [];
	tableHeadArray.push('Item name*');
	tableHeadArray.push('Item code');
	isItemDescriptionEnabled && tableHeadArray.push('Description');
	isItemCategoryEnabled && tableHeadArray.push('Category');
	tableHeadArray.push('HSN');
	tableHeadArray.push('Sale price');
	tableHeadArray.push('Purchase price');
	if (isStockEnabled) {
		tableHeadArray.push('Current stock quantity');
		tableHeadArray.push('Minimum stock quantity');
		tableHeadArray.push('Item Location');
	}
	tableHeadArray.push('Tax Rate');
	tableHeadArray.push('Inclusive Of Tax');
	if (isItemUnitEnabled) {
		tableHeadArray.push('Base Unit (x)');
		tableHeadArray.push('Secondary Unit (y)');
		tableHeadArray.push('Conversion Rate (n) (x = ny)');
	}
	rowObj.push(tableHeadArray);
	$.each(listOfTransactions, function (key, value) {
		var tempArray = [];
		var taxCode = value.getItemTaxId() ? taxCodeCache.getTaxCodeNameById(value.getItemTaxId()) : '';

		tempArray.push(value.getItemName());
		tempArray.push(value.getItemCode());
		isItemDescriptionEnabled && tempArray.push(value.getItemDescription() || '');
		if (isItemCategoryEnabled) {
			var category = itemCategoryCache.getItemCategoryObjectById(value.getItemCategoryId());
			var categoryName = category ? category.getCategoryName() : '';
			tempArray.push(categoryName);
		}
		tempArray.push(value.getItemHSNCode() || '');
		tempArray.push(MyDouble.getAmountWithDecimal(value.getItemSaleUnitPrice()));
		tempArray.push(MyDouble.getAmountWithDecimal(value.getItemPurchaseUnitPrice()));
		if (isStockEnabled) {
			tempArray.push(MyDouble.getQuantityWithDecimalWithoutColor(value.getItemStockQuantity()));
			tempArray.push(MyDouble.getQuantityWithDecimalWithoutColor(value.getItemMinStockQuantity()));
			tempArray.push(value.getItemLocation());
		}
		tempArray.push(taxCode);
		if (value.getItemTaxId()) {
			tempArray.push(value.getItemTaxTypeSale() == 1 ? 'Y' : 'N');
		} else {
			tempArray.push('');
		}
		if (isItemUnitEnabled) {
			var baseUnit = itemUnitCache.getItemUnitNameById(value.getBaseUnitId()) || '';
			var secondaryUnit = itemUnitCache.getItemUnitNameById(value.getSecondaryUnitId()) || '';
			var mapping = itemUnitMappingCache.getItemUnitMapping(value.getUnitMappingId());
			var conversionRate = mapping && mapping.getConversionRate() || '';
			tempArray.push(baseUnit);
			tempArray.push(secondaryUnit);
			tempArray.push(conversionRate);
		}
		rowObj.push(tempArray);
	});
	return rowObj;
};

function exportItems(evt) {
	var XLSX = require('xlsx');
	var dataArray = prepareObjectForExcelForExportItem(evt.data);
	var worksheet = XLSX.utils.aoa_to_sheet(dataArray);

	var workbook = {
		SheetNames: [],
		Sheets: {}
	};

	var wsName = 'Export Items';
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet;
	var wscolWidth = [{
		wch: 20
	}, {
		wch: 20
	}, {
		wch: 20
	}, {
		wch: 20
	}, {
		wch: 20
	}];
	worksheet['!cols'] = wscolWidth;
	console.log(workbook);
	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFileForExportItem();
}

function onCloseClick() {
	$('#defaultPage').show();
	document.dispatchEvent(new CustomEvent('resize-grid'));

	var container = document.querySelector('#selectUnitButtonContainer');
	var frameDiv = document.querySelector('#frameDiv');
	try {
		container && unmountReactComponent(container);
		frameDiv && unmountReactComponent(frameDiv);
	} catch (err) {}
	$('#frameDiv').empty();
	$('#modelContainer').css({
		display: 'none'
	});
	oldDate = null;
	try {
		try {
			if (tabVisibilityControl) {
				tabVisibilityControl();
			}
		} catch (err) {}
		if (typeof onResume === 'function') {
			onResume();
		} else {
			logger.error(new Error('onResume is not a function'));
		}
	} catch (err) {}
}
$('#close').off('click').on('click', onCloseClick);

function driveBackup() {
	MyAnalytics.pushEvent('Backup to Google Drive');

	if (SyncHandler.isAdminUser()) {
		var BackupDB = require('./../BizLogic/BackupDB.js');
		var backUp = new BackupDB();
		var alertUser = true;
		backUp.backup(alertUser);
	} else {
		ToastHelper.info(ErrorCode.ERROR_SYNC_AUTO_BACKUP_ADMIN2);
	}
}

function emailBackup() {
	MyAnalytics.pushEvent('Backup to Email', {
		Action: 'Click'
	});

	if (SyncHandler.isAdminUser()) {
		var BackupDB = require('./../BizLogic/BackupDB.js');
		var backUp = new BackupDB();
		var alertUser = true;
		backUp.backupToEmail(alertUser);
	} else {
		ToastHelper.info(ErrorCode.ERROR_SYNC_AUTO_BACKUP_ADMIN2);
	}
}

function restoreBackup() {
	MyAnalytics.pushEvent('Restore Backup', {
		Action: 'Click'
	});
	var ImportDB = require('./../BizLogic/ImportDB.js');
	var importDB = new ImportDB();
	importDB.ImportDatabase();
}

var chkStatus = function chkStatus() {
	$('#sy').removeClass('styleWhenNavChanges');
	$('#stat').removeClass('styleWhenNavChanges');
	$('#syncmail').removeClass('styleWhenNavChanges');
	var SettingCache = require('./../Cache/SettingCache.js');
	var settingCache = new SettingCache();
	var TokenForSync = require('./../Utilities/TokenForSync');

	if (TokenForSync.ifExistToken()) {
		$('#loginbutton2').hide();
		$('#logoutbutton2').show();
		var jsonData = TokenForSync.readToken();

		$('#syncmail').text('Welcome ' + jsonData.email);
	} else {
		$('#logoutbutton2').hide();
		$('#loginbutton2').show();
		$('#syncmail').text('');
	}
};

function onLogout() {
	$('#logoutbutton2').removeClass('styleWhenNavChanges');
	$('#loginbutton2').removeClass('styleWhenNavChanges');
	$('#stat').removeClass('styleWhenNavChanges');
	$('#syncmail').removeClass('styleWhenNavChanges');
	$('#syncstat').removeClass('styleWhenNavChanges');

	var TokenForSync = require('./../Utilities/TokenForSync');

	if (TokenForSync.ifExistToken()) {
		TokenForSync.deleteToken();
	}

	if (SyncHandler.isSyncEnabled()) {
		alert(ErrorCode.ERROR_USER_NOT_AUTHORISED_AFTER_LOGOUT);
		ipcRenderer.send('changeURL', 'SwitchCompany.html');
	} else {
		$('#logoutbutton2').hide();
		$('#loginbutton2').show();
		$('#syncmail').text('');
	}
}

function onLogin() {
	$('#stat').removeClass('styleWhenNavChanges');
	$('#syncmail').removeClass('styleWhenNavChanges');
	$('#loginbutton2').removeClass('styleWhenNavChanges');
	$('#logoutbutton2').removeClass('styleWhenNavChanges');
	$('#syncstat  li').removeClass('styleWhenNavChanges');
	var SyncHelper = require('./../Utilities/SyncHelper');

	SyncHelper.loginToGetAuthToken(function () {
		$('#loginbutton2').hide();
		$('#logoutbutton2').show();

		var TokenForSync = require('./../Utilities/TokenForSync');

		if (TokenForSync.ifExistToken()) {
			var jsonData = TokenForSync.readToken();

			$('#syncmail').text('Welcome ' + jsonData.email);
		}
	});
}

function localbackup() {
	MyAnalytics.pushEvent('Backup to Local Drive', {
		Action: 'Click'
	});

	if (SyncHandler.isAdminUser()) {
		var BackupDB = require('./../BizLogic/BackupDB.js');
		var backUp = new BackupDB();
		backUp.localBackup();
	} else {
		ToastHelper.info(ErrorCode.ERROR_SYNC_AUTO_BACKUP_ADMIN2);
	}
}

$(document).off('click', '.changeDefaultPageSP').on('click', '.changeDefaultPageSP', function () {
	var _this = this;

	try {
		if (!canCloseDialogue || canCloseDialogue()) {
			$('#defaultPage').hide();
			txnIdReqGlobal = '';
			valueGlobal = this.id;
			MyAnalytics.pushEvent(this.dataset.eventname + ' Open');
			// $('#modelContainer').css('display', 'block');
			if (this.id == 'NewBankAccount.html') {
				$('#defaultPage').hide();
				txnIdReqGlobal = '';
				valueGlobal = this.id;
				var _CommonUtility = require('./../Utilities/CommonUtility.js');
				_CommonUtility.loadFrameDiv('NewBankAccount.html');
			} else if (this.id === 'cashAdjustmentAddForm') {
				$('#frameDiv').empty();
				var _CommonUtility2 = require('./../Utilities/CommonUtility.js');
				var Component = require('../UIComponent/jsx/CashInHandContainer').default;
				_CommonUtility2.MountDefaultPage(Component, {
					showAdjustCashInHand: true
				});
			} else if (this.id === 'newLoanAccountForm') {
				var _CommonUtility3 = require('./../Utilities/CommonUtility.js');
				var Component = require('../UIComponent/jsx/LoanAccountsContainer').default;
				_CommonUtility3.MountDefaultPage(Component, {
					showAddLoanModal: true
				});
			} else if (this.id === 'extraIncomeLink') {
				var MountComponent = require('../UIComponent/jsx/MountComponent').default;
				var _Component = require('../UIComponent/jsx/SalePurchaseContainer').default;
				MountComponent(_Component, document.querySelector('#salePurchaseContainer'), { txnType: TxnTypeConstant.TXN_TYPE_OTHER_INCOME });
			} else {
				setTimeout(function () {
					var MountComponent = require('../UIComponent/jsx/MountComponent').default;
					var txnType = TxnTypeConstant[_this.id];
					if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
						var _Component2 = require('../UIComponent/jsx/SalePurchaseContainer/PaymentTransaction').default;
						MountComponent(_Component2, document.querySelector('#salePurchaseContainer'), { txnType: txnType });
					} else {
						var _Component3 = require('../UIComponent/jsx/SalePurchaseContainer').default;
						MountComponent(_Component3, document.querySelector('#salePurchaseContainer'), { txnType: txnType });
					}
				});
			}
		}
	} catch (err) {
		console.log(err);

		if (this.id == 'NewBankAccount.html') {
			$('#defaultPage').hide();
			txnIdReqGlobal = '';
			valueGlobal = this.id;
			$('#defaultPage').css('display', 'none');
			$('#frameDiv').empty();
			$('#frameDiv').load('NewBankAccount.html');
			$('#modelContainer').css('display', 'block');
		} else {
			var _MountComponent = require('../UIComponent/jsx/MountComponent').default;
			var txnType = TxnTypeConstant[this.id];
			if (txnType == TxnTypeConstant.TXN_TYPE_CASHIN || txnType == TxnTypeConstant.TXN_TYPE_CASHOUT) {
				var _Component4 = require('../UIComponent/jsx/SalePurchaseContainer/PaymentTransaction').default;
				_MountComponent(_Component4, document.querySelector('#salePurchaseContainer'), { txnType: txnType });
			} else {
				var _Component5 = require('../UIComponent/jsx/SalePurchaseContainer').default;
				_MountComponent(_Component5, document.querySelector('#salePurchaseContainer'), { txnType: txnType });
			}
		}
	}
});

$('.changeDefaultPageOther').on('click', function () {
	try {
		if (!canCloseDialogue || canCloseDialogue()) {
			$('#defaultPage').hide();
			accountNameGlobal = '';
			userNameGlobal = '';
			itemNameGlobal = '';
			$('#modelContainer').css('display', 'block');
			var url = this.id;
			MyAnalytics.pushEvent(url, {
				Action: 'Click'
			});
			if (url.trim() != '') {
				$('#frameDiv').empty();
				$('#frameDiv').load(url);
				$('#modelContainer').css('display', 'block');
			} else {
				$('#defaultPage').show();
				$('#modelContainer').css('display', 'none');
			}
		}
	} catch (err) {
		console.log(err);
		$('#defaultPage').hide();
		accountNameGlobal = '';
		userNameGlobal = '';
		itemNameGlobal = '';
		$('#modelContainer').css('display', 'block');
		MyAnalytics.pushEvent(url, {
			Action: 'Click'
		});
		var url = this.id;
		MyAnalytics.pushEvent(url, {
			Action: 'Click'
		});
		if (url.trim() != '') {
			$('#frameDiv').empty();
			$('#frameDiv').load(url);
			$('#modelContainer').css('display', 'block');
		}
	}
});

navMenu.on('focus', 'li', function () {
	$this = $(this);
	navMenu.find('li').removeClass('styleWhenNavChanges');
	$this.addClass('styleWhenNavChanges');
}).on('keydown', 'li', function (e) {
	$this = $(this);
	if (e.keyCode == 40) {
		$this.next().focus();
		return false;
	} else if (e.keyCode == 38) {
		$this.prev().focus();
		return false;
	}
}).find('li').first().focus();

$('#calculator').on('click', function () {
	MyAnalytics.pushEvent('Open Calculator', {
		Action: 'Click'
	});

	var _require2 = require('electron'),
	    shell = _require2.shell;

	shell.openItem('calc');
});

$(document).keydown(function (e) {
	if (e.keyCode === 27) {
		if (!$('.vyaparFormToValidate').hasClass('salePurchaseDialog')) {
			if (!canCloseDialogue || canCloseDialogue()) {
				$('#close').click();
				//   $('#closeShortcut').click();// esc
				if (typeof onResume === 'function') {
					onResume();
				} else {
					logger.error(new Error('onResume is not a function'));
				}
			}
		}
	}
});

// providing ripple effect for buttons
$(document).off('click', '.ripple').on('click', '.ripple', function (event) {
	event.preventDefault();
	var $div = $('<div/>');

	var btnOffset = $(this).offset();

	var xPos = event.pageX - btnOffset.left;

	var yPos = event.pageY - btnOffset.top;

	$div.addClass('ripple-effect');
	var $ripple = $('.ripple-effect');
	$div.css({
		top: yPos - $(this).outerHeight() / 2,
		left: xPos - $(this).outerWidth() / 2,
		background: $(this).data('ripple-color')
	}).appendTo($(this));

	window.setTimeout(function () {
		$div.remove();
	}, 500);
});

if (window.module) module = window.module;

$('.updateCurrentFirm').on('click', function (event) {
	var component = require('../UIComponent/jsx/AddEditFirmDialog').default;
	var MountComponent = require('../UIComponent/jsx/MountComponent').default;
	var SettingCache = require('../Cache/SettingCache');
	var settingCache = new SettingCache();
	var firmId = settingCache.getDefaultFirmId();
	MountComponent(component, document.getElementById('addEditFirmDialog'), {
		firmId: firmId
	});
});

$('.companyImageContainer').on('click', function (event) {
	var companyImageSrc = $('.companyImage').attr('src');
	if (companyImageSrc && companyImageSrc.indexOf('Add_logo_for_compnay.svg') != -1) {
		event.stopPropagation();
		var updateLogo = function updateLogo(imageName, selectedFile) {
			var FirmCache = require('./../Cache/FirmCache.js');
			var firmCache = new FirmCache();
			var oldFirmObj = firmCache.getDefaultFirm();
			oldFirmObj.setFirmImagePath(imageName);
			var firmSignImagePath = oldFirmObj.getFirmSignImagePath();
			firmSignImagePath = firmSignImagePath ? 'data:image/png;base64,' + firmSignImagePath : '';
			oldFirmObj.setFirmSignImagePath(firmSignImagePath);
			var statusCode = oldFirmObj.updateFirmWithoutObject(false);
			if (statusCode == ErrorCode.ERROR_FIRM_UPDATE_SUCCESS) {
				ToastHelper.success(statusCode);
				$('.companyImage').attr('src', selectedFile);
				$('.companyImageContainer, .companyImage').removeClass('addLogo');
			} else {
				ToastHelper.error(statusCode);
			}
		};

		var _require3 = require('../Utilities/ImageHelper'),
		    openSaveImageDialog = _require3.openSaveImageDialog;

		openSaveImageDialog({ title: 'Select Firm Logo' }, updateLogo);
	}
});

function checkOrderFormEnabled() {
	var SettingCache = require('./../Cache/SettingCache.js');
	var settingCache = new SettingCache();
	if (settingCache.isOrderFormEnabled()) {
		$('#orderFormTab').show();
		$('.shortcutsList #TXN_TYPE_SALE_ORDER').show(); // .attr('accessKey','f');
		$('.shortcutsList #TXN_TYPE_PURCHASE_ORDER').show(); // .attr('accessKey','g');
		$('#subMenuItem_TXN_TYPE_SALE_ORDER').show();
		$('#subMenuItem_TXN_TYPE_PURCHASE_ORDER').show();
		$('#SaleOrderRowFirm').removeClass('hide');
		$('#PurchaseOrderRowFirm').removeClass('hide');
	} else {
		$('#orderFormTab').hide();
		$('.shortcutsList #TXN_TYPE_SALE_ORDER').hide(); // .attr('accessKey','');
		$('.shortcutsList #TXN_TYPE_PURCHASE_ORDER').hide(); // .attr('accessKey','');
		$('#subMenuItem_TXN_TYPE_SALE_ORDER').hide();
		$('#subMenuItem_TXN_TYPE_PURCHASE_ORDER').hide();
		$('#SaleOrderRowFirm').addClass('hide');
		$('#PurchaseOrderRowFirm').addClass('hide');
	}
}

checkOrderFormEnabled();

function checkIncomeEnabled() {
	var SettingCache = require('./../Cache/SettingCache.js');
	var settingCache = new SettingCache();
	if (settingCache.isExtraIncomeEnabled()) {
		$('#extraIncomeLink').show();
		$('#extraIncomeReportLink').show();
		$('#TXN_TYPE_OTHER_INCOME').show(); // .attr('accessKey','q');
		$('#shortcuts #TXN_TYPE_OTHER_INCOME').show();
	} else {
		$('#extraIncomeLink').hide();
		$('#extraIncomeReportLink').hide();
		$('#TXN_TYPE_OTHER_INCOME').hide(); // .attr('accessKey','');
		$('#shortcuts #TXN_TYPE_OTHER_INCOME').hide();
	}
}

checkIncomeEnabled();

function checkDeliveryChallanEnabled() {
	var SettingCache = require('./../Cache/SettingCache.js');
	var settingCache = new SettingCache();
	if (settingCache.isDeliveryChallanEnabled()) {
		$('#deliveryChallanRowFirm').removeClass('hide');
		$('#VYAPAR\\.DELIVERYCHALLANRETURNENABLED').removeClass('hide');
		$('#deliveryChallanTab').show();
		$('.shortcutsList #TXN_TYPE_DELIVERY_CHALLAN').show(); // .attr('accessKey','d');
		$('#subMenuItem_deliveryChallanTab').show();
	} else {
		$('#deliveryChallanRowFirm').addClass('hide');
		$('#deliveryChallanTab').hide();
		$('#VYAPAR\\.DELIVERYCHALLANRETURNENABLED').addClass('hide');
		$('.shortcutsList #TXN_TYPE_DELIVERY_CHALLAN').hide(); // .attr('accessKey','');
		$('#subMenuItem_deliveryChallanTab').hide();
	}
}

function checkEstimateEnabled() {
	var SettingCache = require('./../Cache/SettingCache.js');
	var settingCache = new SettingCache();
	if (settingCache.isEstimateEnabled()) {
		$('#estimateRow').removeClass('hide');
		$('#estimateTab').show();
		$('.shortcutsList #TXN_TYPE_ESTIMATE').show(); // .attr('accessKey','m');
		$('#subMenuItem_TXN_TYPE_ESTIMATE').show();
	} else {
		$('#estimateRow').addClass('hide');
		$('#estimateTab').hide();
		$('.shortcutsList #TXN_TYPE_ESTIMATE').hide(); // .attr('accessKey','');
		$('#subMenuItem_TXN_TYPE_ESTIMATE').hide();
	}
}

function checkTaxInvoiceEnabled() {
	var SettingCache = require('./../Cache/SettingCache.js');
	var settingCache = new SettingCache();
	if (settingCache.getTaxInvoiceEnabled()) {
		$('#taxInvoiceRow').removeClass('hide');
	}
}

checkEstimateEnabled();
checkDeliveryChallanEnabled();
checkTaxInvoiceEnabled();

function showHideAddItemButtons() {
	if (settingCache.getItemEnabled()) {
		$('#sideNav #viewItems').show(); // .attr('accessKey','3');
		$('.addItemHiddenForItemSetting').show(); // .attr('accessKey','A');
	} else {
		$('#sideNav #viewItems').hide(); // .attr('accessKey','3');
		$('.addItemHiddenForItemSetting').hide(); // .attr('accessKey','A');
	}
}
showHideAddItemButtons();
function auto_grow(element) {
	element.style.height = '5px';
	element.style.height = element.scrollHeight + 'px';
}

var verifyData = function verifyData() {
	MyAnalytics.pushEvent('Verify Data Start');

	if (SyncHandler.isAdminUser()) {
		var VerifyMyData = require('./../Utilities/VerifyMyData.js');
		var _verifyData = new VerifyMyData();
		var listOfFaultyItemAndParty = _verifyData.verifyData();
		var faultyItems = listOfFaultyItemAndParty[0] || [];
		var faultyParties = listOfFaultyItemAndParty[1] || [];
		MyAnalytics.pushEvent('Verify Data', {
			Action: 'Click'
		});
		if (faultyItems.length == 0 && faultyParties.length == 0) {
			$('#customAlertDialog #customAlertDialogBody').text('No issues were found while checking your data.');
			MyAnalytics.pushEvent('Verify Data Successful');
			$('#customAlertDialog').show().dialog({
				title: 'No Issue',
				closeX: false,
				closeOnEscape: false,
				closeText: 'hide',
				modal: true,
				open: function open(event, ui) {
					$('.ui-dialog-titlebar-close').hide();
				}
			});
			$('#customAlertDialog #confirm').text('Ok').show();
			$('#customAlertDialog #cancel').hide();
			$('#customAlertDialog button').off('click').on('click', function () {
				$('#customAlertDialog').dialog('close').dialog('destroy').hide();
			});
		} else {
			var ItemCache = require('./../Cache/ItemCache.js');
			var itemCache = new ItemCache();
			var NameCache = require('./../Cache/NameCache.js');
			var nameCache = new NameCache();
			var divToAppend = '<div>';
			if (faultyItems.length > 0) {
				divToAppend += '<div><span style="font-weight: bold; margin-top: 10px; display:block">Item List</span><div class="tableInsideDialogForFixMyData"><table width="100%" ><tr><th align="left">Item Name</th><th align="left">Current Value</th><th align="left">Expected Value</th></tr>';
				$.each(faultyItems, function (index, value) {
					var itemName = itemCache.findItemNameById(value.getId());
					divToAppend += '<tr><td>' + itemName + '</td><td>' + value.getCurrentValue() + '</td><td>' + value.getExpectedValue() + '</td></tr>';
				});
				divToAppend += '</table></div></div>';
			}
			if (faultyParties.length > 0) {
				divToAppend += '<div><span style="font-weight: bold; margin-top: 10px; display:block">Party List</span><div class="tableInsideDialogForFixMyData"><table width="100%"><tr><th align="left">Party Name</th><th align="left">Current Amount</th><th align="left">Expected Amount</th></tr>';
				$.each(faultyParties, function (index, value) {
					var partyName = nameCache.findNameByNameId(value.getId());
					divToAppend += '<tr><td>' + partyName + '</td><td>' + value.getCurrentValue() + '</td><td>' + value.getExpectedValue() + '</td></tr>';
				});
				divToAppend += '</table></div></div>';
			}
			divToAppend += '</div>';

			$('#customAlertDialog #customAlertDialogBody').html('').html('Following issues were found while checking your data. ' + divToAppend);
			$('#customAlertDialog').show().dialog({
				title: 'Issues Found',
				closeX: false,
				closeOnEscape: false,
				closeText: 'hide',
				width: 500,
				modal: true,
				open: function open(event, ui) {
					$('.ui-dialog-titlebar-close').hide();
				}
			});
			$('#customAlertDialog #confirm').text('Fix').show();
			$('#customAlertDialog #cancel').text("Don't Fix").show();
			$('#customAlertDialog button').off('click').on('click', function () {
				if (this.id == 'confirm' && $(this).text() == 'Fix') {
					$('#customAlertDialog').dialog('close').dialog('destroy');
					$('#loading').show(function () {
						_verifyData.fixFaultyDataInDB();
					});
				} else {
					$('#customAlertDialog').hide().dialog('close').dialog('destroy');
				}
			});
		}
	} else {
		ToastHelper.info('Only Admin can Verify the data. Please contact your admin.');
	}
};

$('.verifyData').on('click', verifyData);

ipcRenderer.on('CheckingForUpdate', function () {
	ToastHelper.info('We are checking for update.');
});

ipcRenderer.on('ShowLoader', function () {
	$('#loading').show();
});

ipcRenderer.on('HideLoader', function () {
	$('#loading').hide();
});

ipcRenderer.on('ShowUpdatingMessage', function () {
	try {
		new window.Notification('Downloading Update', {
			body: 'We are downloading the latest update of Vyapar app. You can continue with your usual work. We will inform you once the download is over.',
			icon: './inlineSVG/vyapar.ico'
		});
	} catch (e) {}
});

function restartApp() {
	ipcRenderer.send('changeURL', 'Index1.html');
}

function toastSqliteBusy() {
	ToastHelper.info('Database is getting updated. Please wait for sometime.');
}

if (SyncHandler.isSyncEnabled()) {
	var SyncController = require('./../UIControllers/SyncController.js');
	var syncController = new SyncController();
	syncController.turnOnSocketEventsForSync();
}

ipcRenderer.on('ShowUpdateComapnyName', function () {
	var oldDisplayName = CompanyListUtility.getCurrentCompanyKey();

	if (oldDisplayName) {
		$('#compName').val(oldDisplayName);

		$('#changeCompanyDiaplayNameDialog').removeClass('hide').dialog({
			title: 'Update company display name',
			closeX: false,
			closeOnEscape: false,
			closeText: 'hide',
			modal: true,
			width: 400,
			appendTo: '#dynamicDiv'
		});

		$('#saveCompanyDisplayName').on('click', function () {
			var newCompName = $('#compName').val();

			if (newCompName.trim() == '') {
				ToastHelper.error('Company name cannot be empty');
				return;
			}

			var stat = CompanyListUtility.updateJSONFileForCompanies(oldDisplayName, newCompName);

			if (stat) {
				$('#changeCompanyDiaplayNameDialog').dialog('close');
			}
		});
	} else {
		ToastHelper.error('Company not selected');
	}
});

function canCloseDialogue() {
	var canCloseFormDialogue = true;
	try {
		var ignoreFormClassList = ['hasDatepicker', 'ignoreForDiscardChanges'];
		var ignoreFormTypeList = ['radio', 'hidden'];
		var inputs = $('.vyaparFormToValidate:visible input');

		var _loop = function _loop(_i2) {
			var currentInput = inputs[_i2];
			var hasIgnoreClass = false;
			ignoreFormClassList.forEach(function (c) {
				if (currentInput.classList.toString().indexOf(c) >= 0) {
					hasIgnoreClass = true;
				}
			});
			if (currentInput && currentInput.value && ignoreFormTypeList.indexOf(currentInput.type) === -1 && !hasIgnoreClass) {
				var msg = Messages.discardChanges;
				var conf = confirm(msg);
				if (conf) {
					for (_i2 = 0; _i2 < inputs.length; _i2++) {
						inputs[_i2].value = '';
					}
				}
				canCloseFormDialogue = conf;
				return 'break';
			}
			_i = _i2;
		};

		for (var _i = 0; _i < inputs.length; _i++) {
			var _ret = _loop(_i);

			if (_ret === 'break') break;
		}
	} catch (e) {}
	return canCloseFormDialogue;
}

setTimeout(function () {
	require('../UIControllers/shortcutController')({});
}, 1);

setTimeout(function () {
	try {
		if (document && document.visibilityState === 'visible' && settingCache.getAutoBackUpEnabled()) {
			var backup = require('./../BizLogic/BackupDB');
			var Backup = new backup();
			Backup.autoBackup();
		}
	} catch (ex) {
		logger.error('Error occurred in autoback ' + ex);
	}
}, 3000);

if (settingCache.isCurrentCountryIndia()) {
	setTimeout(function () {
		require('../UIControllers/OutboxController')();
	}, 1);
}