var startDate;
var endDate;
var DateFormat = require('./../Constants/DateFormat.js');
var ExcelHelper = require('./../Utilities/ExcelHelper.js');

$('#startDate, #endDate').datepicker({ dateFormat: DateFormat.format });
var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();
var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var excelHelper = new ExcelHelper();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');
// var printForSharePDF = false;

var listOfTransactions = [];

var populateTable = function populateTable() {
	var DataLoader = require('./../DBManager/DataLoader.js');
	// var dataLoader = new DataLoader();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
	endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');
	var ProfitAndLossReportObject = require('./../BizLogic/ProfitAndLossReportObject.js');
	var profitAndLossReportObject = new ProfitAndLossReportObject();
	CommonUtility.showLoader(function () {
		listOfTransactions = profitAndLossReportObject.getProfitAndLossReportObject(startDate, endDate);
		displayTransactionList(listOfTransactions);
	});
};

var date = MyDate.getDate('d/m/y');
$('#startDate').val(MyDate.getFirstDateOfCurrentMonthString());
$('#endDate').val(date);

$('#startDate').change(function () {
	populateTable();
});

$('#endDate').change(function () {
	populateTable();
});

var displayTransactionList = function displayTransactionList(listOfTransactions) {
	var dynamicRow = '';
	dynamicRow += "<thead><tr><th width='50%'>Particulars</th><th width='50%' class='tableCellTextAlignRight'>Amount</th></tr></thead>";
	var saleAmount = listOfTransactions.getSaleAmount();
	var saleReturnAmount = listOfTransactions.getSaleReturnAmount();
	var purchaseAmount = listOfTransactions.getPurchaseAmount();
	var purchaseReturnAmount = listOfTransactions.getPurchaseReturnAmount();
	var paymentInDiscountAmount = listOfTransactions.getPaymentInDiscountAmount();
	var otherExpenseAmount = listOfTransactions.getOtherExpenseAmount();
	var loanInterestExpense = listOfTransactions.getLoanInterestExpense();
	var loanProcessingFeeExpense = listOfTransactions.getLoanProcessingFeeExpense();
	var paymentOutDiscountAmount = listOfTransactions.getPaymentOutDiscountAmount();
	var otherIncomeAmount = listOfTransactions.getOtherIncomeAmount();
	var grossProfitAndLossAmount = listOfTransactions.getGrossProfitAndLossAmount();
	var openingStockValue = listOfTransactions.getOpeningStockValue();
	var closingStockValue = listOfTransactions.getClosingStockValue();
	var netProfitAndLossAmount = listOfTransactions.getNetProfitAndLossAmount();
	var taxReceivable = listOfTransactions.getReceivableTax();
	var taxPayable = listOfTransactions.getPayableTax();
	//
	dynamicRow += '<tbody>';
	dynamicRow += "<tr class='currentRow'><td width='50%'>Sale (+) </td><td width='50%' class='tableCellTextAlignRight'  style='color:#1faf9d'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(saleAmount) + '</td></tr>';
	dynamicRow += "<tr class='currentRow'><td width='50%'>Credit Note (-) </td><td width='50%' class='tableCellTextAlignRight' style='color:#e35959'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(saleReturnAmount) + '</td></tr>';
	dynamicRow += "<tr class='currentRow'><td width='50%'>Purchase (-) </td><td width='50%' class='tableCellTextAlignRight' style='color:#e35959'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(purchaseAmount) + '</td></tr>';
	dynamicRow += "<tr class='currentRow'><td width='50%'>Debit Note (+) </td><td width='50%' class='tableCellTextAlignRight'  style='color:#1faf9d'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(purchaseReturnAmount) + '</td></tr>';

	// income
	dynamicRow += Number(paymentOutDiscountAmount) != 0 ? "<tr class='currentRow'><td class='' width='50%'>Payment-out Discount (+)</td><td width='50%' class='tableCellTextAlignRight'  style='color:#1faf9d'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(paymentOutDiscountAmount) + '</td></tr>' : '';

	// expense
	dynamicRow += Number(paymentInDiscountAmount) != 0 ? "<tr class='currentRow'><td class='' width='50%'>Payment-in Discount (-)</td><td width='50%' class='tableCellTextAlignRight' style='color:#e35959'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(paymentInDiscountAmount) + '</td></tr>' : '';

	// Tax
	dynamicRow += "<tr class='currentRow'><td colspan='2' width='100%'>Tax </td></tr>";
	dynamicRow += "<tr class='currentRow greybg'><td class='pl-20' width='50%'>Tax Payable(-)</td><td width='50%' class='tableCellTextAlignRight' style='color:#e35959'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(taxPayable) + '</td></tr>';
	dynamicRow += "<tr class='currentRow greybg'><td class='pl-20' width='50%'>Tax Receivable(+)</td><td width='50%' class='tableCellTextAlignRight' style='color:#1faf9d'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(taxReceivable) + '</td></tr>';

	dynamicRow += "<tr class='currentRow wMyDoublehitebg'><td width='50%'>Opening Stock (-) </td><td width='50%' class='tableCellTextAlignRight' style='color:#e35959'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(openingStockValue) + '</td></tr>';
	dynamicRow += "<tr class='currentRow whitebg'><td width='50%'>Closing Stock (+) </td><td width='50%' class='tableCellTextAlignRight' style='color:#1faf9d'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(closingStockValue) + '</td></tr>';
	dynamicRow += "<tr class='currentRow'><td width='50%' class='grossCell'>";
	if (grossProfitAndLossAmount >= 0) {
		dynamicRow += 'Gross Profit';
	} else {
		dynamicRow += 'Gross Loss';
	}
	dynamicRow += "</td><td width='50%' class='grossCell tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimalAndCurrency(grossProfitAndLossAmount) + '</td></tr>';
	dynamicRow += "<tr class='currentRow greybg'><td width='50%'>Other Income (+)</td><td width='50%' class='tableCellTextAlignRight' style='color:#1faf9d'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(otherIncomeAmount) + '</td></tr>';

	// Indirect Expenses
	dynamicRow += "<tr class='currentRow whitebg'><td colspan='2' width='100%'>Indirect Expenses(-) </td></tr>";
	dynamicRow += "<tr class='currentRow greybg'><td class='pl-20' width='50%'>Other Expense</td><td width='50%' class='tableCellTextAlignRight' style='color:#e35959'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(otherExpenseAmount) + '</td></tr>';
	dynamicRow += "<tr class='currentRow greybg'><td class='pl-20' width='50%'>Loan Interest Expense</td><td width='50%' class='tableCellTextAlignRight' style='color:#e35959'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(loanInterestExpense) + '</td></tr>';
	dynamicRow += "<tr class='currentRow greybg'><td class='pl-20' width='50%'>Loan Processing Fee Expense</td><td width='50%' class='tableCellTextAlignRight' style='color:#e35959'>" + MyDouble.getBalanceAmountWithDecimalAndCurrencyWithoutColor(loanProcessingFeeExpense) + '</td></tr>';
	dynamicRow += "<tr class='currentRow'><td width='50%' class='netCell'>";
	if (netProfitAndLossAmount >= 0) {
		dynamicRow += 'Net Profit';
	} else {
		dynamicRow += 'Net Loss';
	}
	dynamicRow += "</td><td width='50%' class='netCell tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimalAndCurrency(netProfitAndLossAmount) + '</td></tr>';
	dynamicRow += '</tbody>';

	$('#tableHead').html(dynamicRow);
};

var openTransaction = function openTransaction(txnId, txnType) {
	var ViewTransaction = require('./../BizLogic/ViewTransaction.js');
	var viewTransaction = new ViewTransaction();
	viewTransaction.viewTransactionFile(txnId + ':' + TxnTypeConstant.getTxnType(txnType));
};

if (settingCache.getItemEnabled()) {
	$('#itemDetailPrintDiv').show();
} else {
	$('#itemDetailPrintDiv').hide();
}

var getHTMLTextForReport = function getHTMLTextForReport() {
	var ProfitAndLossReportHTMLGenerator = require('./../ReportHTMLGenerator/ProfitAndLossReportHTMLGenerator.js');
	var profitAndLossReportHTMLGenerator = new ProfitAndLossReportHTMLGenerator();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	// var firmId = Number($('#firmFilterOptions').val());
	debugger;
	var htmlText = profitAndLossReportHTMLGenerator.getHTMLText(listOfTransactions, startDateString, endDateString);
	return htmlText;
};

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	// $('#frameDiv').load('');
	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ display: 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable();
};

/// ///////////////////////////PDF/////////////////////////////////////
var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.PROFIT_AND_LOSS_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

function openExcelOptions() {
	$('#ExcelDialog').removeClass('hide');
	$('#ExcelDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

if (settingCache.getItemEnabled()) {
	$('#itemDetailsExcel').show();
} else {
	$('#itemDetailsExcel').hide();
}

function closeExcelOptions() {
	$('#ExcelDialog').addClass('hide');
	$('#ExcelDialog').dialog('close').dialog('destroy');
}

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.PROFIT_AND_LOSS_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
	excelHelper.saveExcel(fileName);
};

var prepareObjectForExcel = function prepareObjectForExcel(listOfTransactions) {
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var rowObj = [];

	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var reportTitle = PDFReportConstant.PROFIT_AND_LOSS_REPORT;

	rowObj.push(['', reportTitle, '']);
	rowObj.push(['', startDateString + ' to ' + endDateString, '']);

	rowObj.push([]);

	var txn = listOfTransactions;
	var tempArray = [];
	var saleAmount = txn.getSaleAmount() || 0;
	var saleReturnAmount = txn.getSaleReturnAmount() || 0;
	var purchaseAmount = txn.getPurchaseAmount() || 0;
	var purchaseReturnAmount = txn.getPurchaseReturnAmount() || 0;
	var paymentInDiscountAmount = txn.getPaymentInDiscountAmount() || 0;
	var otherExpenseAmount = txn.getOtherExpenseAmount() || 0;
	var loanInterestExpense = txn.getLoanInterestExpense() || 0;
	var loanProcessingFeeExpense = txn.getLoanProcessingFeeExpense() || 0;
	var paymentOutDiscountAmount = txn.getPaymentOutDiscountAmount() || 0;
	var otherIncomeAmount = txn.getOtherIncomeAmount() || 0;
	var grossProfitAndLossAmount = txn.getGrossProfitAndLossAmount() || 0;
	var openingStockValue = txn.getOpeningStockValue() || 0;
	var closingStockValue = txn.getClosingStockValue() || 0;
	var netProfitAndLossAmount = txn.getNetProfitAndLossAmount() || 0;
	var taxReceivable = txn.getReceivableTax() || 0;
	var taxPayable = txn.getPayableTax() || 0;

	tempArray.push('Sale (+)');
	tempArray.push('');
	tempArray.push(saleAmount);
	rowObj.push(tempArray);
	tempArray = [];
	tempArray.push('Credit Note (-) ');
	tempArray.push('');
	tempArray.push(saleReturnAmount);
	rowObj.push(tempArray);
	tempArray = [];
	tempArray.push('Purchase (-) ');
	tempArray.push('');
	tempArray.push(purchaseAmount);
	rowObj.push(tempArray);
	tempArray = [];
	tempArray.push('Debit Note (+)');
	tempArray.push('');
	tempArray.push(purchaseReturnAmount);
	rowObj.push(tempArray);
	tempArray = [];
	tempArray.push('Payment Out Discount Amount');
	tempArray.push('');
	tempArray.push(paymentOutDiscountAmount);
	rowObj.push(tempArray);
	tempArray = [];
	tempArray.push('Payment In Discount Amount');
	tempArray.push('');
	tempArray.push(paymentInDiscountAmount);
	rowObj.push(tempArray);
	tempArray = [];
	tempArray = ['Tax', '', ''];
	rowObj.push(tempArray);
	tempArray = [];
	tempArray.push('Tax Payable(-)');
	tempArray.push('');
	tempArray.push(taxPayable);
	rowObj.push(tempArray);
	tempArray = [];
	tempArray.push('Tax Receivable(+)');
	tempArray.push('');
	tempArray.push(taxReceivable);
	rowObj.push(tempArray);
	tempArray = [];
	tempArray.push('Opening Stock (-)');
	tempArray.push('');
	tempArray.push(openingStockValue);
	rowObj.push(tempArray);
	tempArray = [];
	tempArray.push('Closing Stock (+)');
	tempArray.push('');
	tempArray.push(closingStockValue);
	rowObj.push(tempArray);
	tempArray = [];
	if (grossProfitAndLossAmount >= 0) {
		// first col should be filled otherwise empty
		tempArray.push('Gross Profit');
	} else {
		tempArray.push('Gross Loss');
	}
	tempArray.push('');
	tempArray.push(grossProfitAndLossAmount);
	rowObj.push(tempArray);
	tempArray = [];
	tempArray.push('Other Income (+)');
	tempArray.push('');
	tempArray.push(otherIncomeAmount);
	rowObj.push(tempArray);
	tempArray = ['Indirect Expenses(-)', '', ''];
	rowObj.push(tempArray);
	tempArray = [];
	tempArray.push('Other Expense');
	tempArray.push('');
	tempArray.push(otherExpenseAmount);
	rowObj.push(tempArray);
	tempArray = [];
	tempArray.push('Loan Interest Expense');
	tempArray.push('');
	tempArray.push(loanInterestExpense);
	rowObj.push(tempArray);
	tempArray = [];
	tempArray.push('Loan Processing Fee Expense');
	tempArray.push('');
	tempArray.push(loanProcessingFeeExpense);
	rowObj.push(tempArray);
	tempArray = [];
	if (netProfitAndLossAmount >= 0) {
		tempArray.push('Net Profit');
	} else {
		tempArray.push('Net Loss');
	}
	tempArray.push('');
	tempArray.push(netProfitAndLossAmount);
	rowObj.push(tempArray);
	tempArray.push('');
	tempArray = [];
	rowObj.push(tempArray);
	rowObj.push([]);
	return rowObj;
};

function printExcel() {
	closeExcelOptions();
	var XLSX = require('xlsx');
	var dataArray = prepareObjectForExcel(listOfTransactions);
	var worksheet = XLSX.utils.aoa_to_sheet(dataArray);
	var ExcelExportHelper = require('./../UIControllers/ExcelExportHelper.js');
	var excelExportHelper = new ExcelExportHelper();

	var workbook = {
		SheetNames: [],
		Sheets: {}
	};

	var wsName = 'Profit and Loss Report';
	var lenCols = 3;
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet;
	var wscolWidth = new Array(lenCols).fill({ wch: 30 });
	worksheet['!cols'] = wscolWidth;

	// var wsNameItem = 'Profit and loss report';
	// workbook.SheetNames[1] = wsNameItem;
	// var itemArray = excelExportHelper.prepareItemObjectForExcel(listOfTransactions);
	// var itemWorksheet = XLSX.utils.aoa_to_sheet(itemArray);
	// workbook.Sheets[wsNameItem] = itemWorksheet;
	// var wsItemcolWidth = new Array(lenCols).fill({ wch: 30 });
	// itemWorksheet['!cols'] = wsItemcolWidth;

	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

/// //////////////////////////////////////////////////////////////
populateTable();