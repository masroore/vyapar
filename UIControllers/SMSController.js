var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* global jQuery */
module.exports = function smsSettings($) {
	var SettingCache = require('./../Cache/SettingCache');
	var Queries = require('./../Constants/Queries');
	var SettingsModel = require('./../Models/SettingsModel');
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant');
	var MessageConfig = require('./../Cache/MessageCache');
	var MessageModel = require('./../Models/TxnMessageConfigModel');
	var ErrorCode = require('./../Constants/ErrorCode.js');
	var MessageFormatter = require('./../BizLogic/TxnMessageFormatter');
	var TokenForSync = require('./../Utilities/TokenForSync');
	var SyncHelper = require('./../Utilities/SyncHelper');
	var Domain = require('./../Constants/Domain');
	var domain = Domain.thisDomain;

	var BrowserWindow = require('electron').remote.BrowserWindow;

	var FirmCache = require('./../Cache/FirmCache');
	var messageFieldConstants = require('../Constants/TxnMessageFieldConstants');

	// const DataLoader = require('./../DBManager/DataLoader');
	// const Domain = require('./../Constants/Domain');
	// const ErrorCode = require('./../Constants/ErrorCode');
	// const TransactionMessageConfig = require('./../BizLogic/TransactionMessageConfig');
	// const domain = Domain.thisDomain;

	var settingCache = void 0;
	var settingsModel = void 0;
	var messageConfig = void 0;
	var messageFormatter = void 0;
	// const theme = InvoiceTheme.THEME_1;

	var isLoggedIn = false;
	var authToken = '';
	var loggedInAs = '';

	var WORKFLOW = 5;
	var regSenderId = /^[A-Z]{6}$/;

	var lang = {
		'Enable Transaction Settings': 'Enable Transaction Settings',
		'Enable owner messages': 'Enable owner messages',
		'Please enter phone number': 'Please enter phone number'
	};
	var SMSSettings = {
		defaults: {
			messageSettings: [],
			mountPoint: '',
			$el: '',
			txnTypes: [{
				type: TxnTypeConstant.TXN_TYPE_SALE,
				id: Queries.SETTING_TXN_MESSAGE_ENABLED_SALE,
				label: 'Sales'
			}, {
				type: TxnTypeConstant.TXN_TYPE_PURCHASE,
				id: Queries.SETTING_TXN_MESSAGE_ENABLED_PURCHASE,
				label: 'Purchase'
			}, {
				type: TxnTypeConstant.TXN_TYPE_SALE_RETURN,
				id: Queries.SETTING_TXN_MESSAGE_ENABLED_SALERETURN,
				label: 'Sales Return'
			}, {
				type: TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN,
				id: Queries.SETTING_TXN_MESSAGE_ENABLED_PURCHASERETURN,
				label: 'Purchase Return'
			}, {
				type: TxnTypeConstant.TXN_TYPE_CASHIN,
				id: Queries.SETTING_TXN_MESSAGE_ENABLED_CASHIN,
				label: 'Payment In'
			}, {
				type: TxnTypeConstant.TXN_TYPE_CASHOUT,
				id: Queries.SETTING_TXN_MESSAGE_ENABLED_CASHOUT,
				label: 'Payment Out'
			}, {
				type: TxnTypeConstant.TXN_TYPE_SALE_ORDER,
				id: Queries.SETTING_TXN_MESSAGE_ENABLED_SALE_ORDER,
				label: 'Sale Order'
			}, {
				type: TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER,
				id: Queries.SETTING_TXN_MESSAGE_ENABLED_PURCHASE_ORDER,
				label: 'Purchase Order'
			}, {
				type: TxnTypeConstant.TXN_TYPE_ESTIMATE,
				id: Queries.SETTING_TXN_MESSAGE_ENABLED_ESTIMATE_FORM,
				label: 'Estimate'
			}, {
				type: TxnTypeConstant.TXN_TYPE_DELIVERY_CHALLAN,
				id: Queries.SETTING_TXN_MESSAGE_ENABLED_DELIVERY_CHALLAN,
				label: 'Delivery Challan'
			}]
		},
		init: function init() {
			var defaults = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

			settingCache = new SettingCache();
			settingsModel = new SettingsModel();
			messageConfig = new MessageConfig();
			messageFormatter = new MessageFormatter();

			this.readToken();
			this.defaults = $.extend(this.defaults, defaults);
			this.render();
			this.defaults.$el = this.defaults.mountPoint.find('#smsSettingContainer');
			this.attachEvents();
			this.displayFirst();
			this.showSettings(this.defaults.$el.find('#enableTransactionMessageSettings').is(':checked'));
			this.showActiveTab();
			this.getSettings();
			if (isLoggedIn) {
				this.defaults.$el.find('#logout').removeClass('hide').siblings().addClass('hide');
				this.defaults.$el.find('#txtSignInAs').val(loggedInAs);
			} else {
				this.defaults.$el.find('#login').removeClass('hide').siblings().addClass('hide');
			}
		},
		render: function render() {
			var html = this.getMainTemplate();
			var $mountPoint = this.defaults.mountPoint;
			$mountPoint.html(html);
			$mountPoint.dialog({
				width: 800,
				height: 645,
				modal: true,
				close: function (e) {
					e.stopPropagation();
					this.defaults.$el.off('click', '**');
					$mountPoint.html('');
					$mountPoint.dialog('destroy');
					// delete require.cache[require.resolve("../UIControllers/SMSController")];
				}.bind(this)
			});
		},
		attachEvents: function attachEvents() {
			this.defaults.$el.on('click', '.tab', this.changeTab.bind(this)).on('change', '#enableTransactionMessageSettings', this.toggleMessageSettingKey.bind(this)).on('click', '.txn-list-item', this.toggleMessageSetting.bind(this)).on('click', '.sms-field', this.toggleFieldSetting.bind(this)).on('blur', '.sms-input.small', this.saveHeaderFooter.bind(this)).on('click', '#enableOwnerMessageSettings', this.toggleMessageFromOwner.bind(this)).on('click', '#btnPhoneNumber', this.savePhoneNumber.bind(this)).on('change', '#drdSenderId', this.changeSenderId.bind(this)).on('click', '#btnSignIn', this.changeAccount.bind(this)).on('click', '#btnLogout', this.logout.bind(this)).on('click', '#addCredits', this.openMoreSettingsWindow.bind(this, domain + '/sms?workflow=' + WORKFLOW + '&credit=true&client_type=2')).on('click', '#btnMoreSettings', this.openMoreSettingsWindow.bind(this, domain + '/sms?workflow=' + WORKFLOW + '&client_type=2')).on('click', '#btnSenderId', function (e) {
				$(e.target).next().toggleClass('hide');
			}).on('click', '#btnCreateSenderId', this.createSenderId.bind(this));

			this.defaults.$el.find('.inline-tooltip').tooltipster({
				maxWidth: 300,
				trigger: 'click'
			});

			// const $drdSenderId = $('#drdSenderId')
			//
			// let senderIds = ['Add New Sender Id']
		},
		displayFirst: function displayFirst() {
			var firstTxn = this.defaults.$el.find('.txn-list-item').eq(0);
			if (firstTxn.length) {
				firstTxn.addClass('active');
				var checked = firstTxn.find('input').first().is(':checked');
				this.createMessage(firstTxn.data('txn-type'), checked);
			}
		},
		changeTab: function changeTab(e) {
			this.showActiveTab($(e.target));
		},
		showActiveTab: function showActiveTab($target) {
			$target = $target || this.defaults.$el.find('.tab.active').first();
			$target.addClass('active');
			$target.siblings().each(function (index, el) {
				var $el = $(el);
				$el.removeClass('active');
				$($el.data('target')).hide();
			});
			$($target.data('target')).show();
		},
		getSettings: function getSettings() {
			if (!authToken) {
				return false;
			}

			this.getSenderIds();
			this.getSMSBalance();
		},
		getSenderIds: function getSenderIds() {
			var _this = this;

			var selected = false;
			var ENDPOINTS = {
				alias: domain + '/api/sms/alias',
				defaultHeader: {
					'Accept': 'application/json',
					'Authorization': 'Bearer ' + authToken,
					'Content-Type': 'application/json',
					'Cache-Control': 'no-cache'
				}
			};
			$.get({
				url: ENDPOINTS.alias,
				headers: (0, _extends3.default)({}, ENDPOINTS.defaultHeader),
				success: function success(data) {
					var approvedSenderIds = data.map(function (sender) {
						var label = '';
						if (sender.approval_status == 'Pending') {
							label = 'Pending';
						} else if (sender.approval_status == 'Approved') {
							label = 'Approved';
						} else {
							label = 'Rejected';
						}
						var senderId = sender.sender_id;
						if (settingCache.getTxnMsgSenderId() == senderId) {
							selected = true;
						}
						return $('<option />').html(senderId + ' (' + label + ')').data('status', label).val(senderId).prop('selected', settingCache.getTxnMsgSenderId() == senderId);
					});
					approvedSenderIds.unshift($('<option value="-1"> + Add Sender Id </option>'));
					approvedSenderIds.unshift($('<option hidden ' + (selected ? 'selected' : '') + ' value="0"> Select Sender Id </option>'));
					var $drdSenderId = _this.defaults.$el.find('#drdSenderId');
					$drdSenderId.html(approvedSenderIds);
				},
				error: function error(xhr) {
					if (xhr.status == 0) {
						ToastHelper.error('Failed to get SMS Settings. Please check your internet connection.');
					} else if (xhr.status == 401) {
						ToastHelper.error('Failed to get SMS Settings. Invalid User. Please login.');
					} else {
						ToastHelper.error('Failed to get SMS Settings. Something went wrong. Please try again later.');
					}
				}
			});
		},
		getSMSBalance: function getSMSBalance() {
			var _this2 = this;

			var ENDPOINTS = {
				credits: domain + '/api/sms/credits',
				defaultHeader: {
					'Accept': 'application/json',
					'Authorization': 'Bearer ' + authToken,
					'Content-Type': 'application/json',
					'Cache-Control': 'no-cache'
				}
			};
			$.get({
				url: ENDPOINTS.credits,
				headers: (0, _extends3.default)({}, ENDPOINTS.defaultHeader),
				success: function success(data) {
					_this2.defaults.$el.find('#balanceAmount').html(data.credits || 0);
				},
				error: function error(xhr) {
					if (xhr.status == 0) {
						ToastHelper.error('Failed to get SMS Settings. Please check your internet connection.');
					} else if (xhr.status == 401) {
						ToastHelper.error('Failed to get SMS Settings. Invalid User. Please login.');
					} else {
						ToastHelper.error('Failed to get SMS Settings. Something went wrong. Please try again later.');
					}
				}
			});
		},
		toggleFieldSetting: function toggleFieldSetting(e) {
			var _this3 = this;

			var $target = $(e.target);
			var txnType = $target.data('txn-id');
			var field = messageConfig.getField(txnType, $target.data('field-id'));
			var checked = Number($target.is(':checked'));
			if (!field) {
				e.preventDefault();
				ToastHelper.error('Something went wrong.');
				return false;
			}

			var model = new MessageModel(field.txnType, field.txnFieldId, field.txnFieldName, checked);

			model.save(false).then(function (status) {
				field.txnFieldValue = Number(checked);
				_this3.messageGenerator(txnType);
				ToastHelper.success(status);
			}).catch(function (error) {
				e.preventDefault();
				ToastHelper.error(ErrorCode.ERROR_MESSAGE_CONFIG_SAVE_FAILED);
				if (logger !== 'undefined' && logger && logger.error) {
					logger.error(error);
				}
			});
		},
		saveHeaderFooter: function saveHeaderFooter(e) {
			var _this4 = this;

			var $target = $(e.target);
			var txnType = $target.data('txn-id');
			var html = $target.val();
			var field = messageConfig.getField(txnType, $target.data('field-id'));
			if (!field) {
				e.preventDefault();
				ToastHelper.error('Something went wrong.');
				return false;
			}
			var model = new MessageModel(field.txnType, field.txnFieldId, field.txnFieldName, html);

			model.save(false).then(function (status) {
				field.txnFieldValue = html;
				_this4.messageGenerator(txnType);
				ToastHelper.success(status);
			}).catch(function (error) {
				e.preventDefault();
				ToastHelper.error(ErrorCode.ERROR_MESSAGE_CONFIG_SAVE_FAILED);
				if (logger !== 'undefined' && logger && logger.error) {
					logger.error(error);
				}
			});
		},
		readToken: function readToken() {
			if (TokenForSync.ifExistToken()) {
				var token = TokenForSync.readToken();
				authToken = token.auth_token;
				loggedInAs = token.email;
				isLoggedIn = true;
				return true;
			} else {
				isLoggedIn = false;
				return false;
			}
		},
		isLoggedIn: function isLoggedIn(success, fail) {
			if (this.readToken()) {
				var token = TokenForSync.readToken();
				success(token);
			} else {
				this.login(success, fail);
			}
		},
		login: function login(success, fail) {
			SyncHelper.loginToGetAuthToken(success, fail, WORKFLOW);
		},
		changeAccount: function changeAccount() {
			this.login(this.afterLogin.bind(this));
		},
		afterLogin: function afterLogin(token) {
			var $el = this.defaults.$el;
			$el.find('#login').addClass('hide').siblings().removeClass('hide');
			authToken = token.auth_token;
			loggedInAs = token.email;
			isLoggedIn = true;

			$('#stat').removeClass('styleWhenNavChanges');
			$('#syncmail').removeClass('styleWhenNavChanges');
			$('#loginbutton2').removeClass('styleWhenNavChanges');
			$('#logoutbutton2').removeClass('styleWhenNavChanges');
			$('#syncstat  li').removeClass('styleWhenNavChanges');

			$el.find('#txtSignInAs').val(loggedInAs);
			$('#syncmail').text('Welcome ' + loggedInAs);
			$('#loginbutton2, #logoutbutton2').toggle();
			this.getSenderIds();
			this.getSMSBalance();
		},
		logout: function logout() {
			var $el = this.defaults.$el;
			$('#stat').removeClass('styleWhenNavChanges');
			$('#syncmail').removeClass('styleWhenNavChanges');
			$('#loginbutton2').removeClass('styleWhenNavChanges');
			$('#logoutbutton2').removeClass('styleWhenNavChanges');
			$('#syncstat  li').removeClass('styleWhenNavChanges');

			TokenForSync.deleteToken();
			authToken = '';
			loggedInAs = '';
			isLoggedIn = false;

			$('#loginbutton2, #logoutbutton2').toggle();
			$('#syncmail').text('');

			$el.find('#logout').addClass('hide').siblings().removeClass('hide');
		},
		toggleMessageSetting: function toggleMessageSetting(e) {
			var $target = $(e.target).closest('li');
			var $input = $target.find('input').first();
			var txnType = $target.data('txn-type');
			var checked = $input.is(':checked');
			if ($(e.target).hasClass('txn-type-input')) {
				var status = this.saveSettings($target.data('settings-key'), Number(checked));
				if (status === ErrorCode.ERROR_SETTING_SAVE_FAILED) {
					ToastHelper.error(status);
					e.preventDefault();
					return;
				} else {
					ToastHelper.success(status);
				}
			}
			$target.addClass('active').siblings().removeClass('active');
			this.createMessage(txnType, checked);
		},
		createMessage: function createMessage(txnType, enabled) {
			var fields = messageConfig.getByTxnType(txnType);
			var html = this.smsFieldsGenerator(fields, enabled);
			if (enabled) {
				$('#sms-fields').html(html);
				this.messageGenerator(txnType);
				$('.show-on-transaction-disable').hide();
				$('.hide-on-transaction-disable').show();
			} else {
				$('.show-on-transaction-disable').show();
				$('.hide-on-transaction-disable').hide();
			}
		},
		smsFieldsGenerator: function smsFieldsGenerator(fields, enabled) {
			var _this5 = this;

			var disabled = enabled ? '' : 'disabled="disabled"';
			return fields.map(function (field) {
				if (field.txnFieldName == 'Header') {
					$('#sms-header').data('field-id', field.txnFieldId).data('txn-id', field.txnType);
					return '';
				} else if (field.txnFieldName == 'Footer') {
					$('#sms-footer').data('field-id', field.txnFieldId).data('txn-id', field.txnType);
					return '';
				}
				return '<div class="col-4">\n                  <div class="md-checkbox sms-field-checkbox">\n                    <input\n                        ' + disabled + '\n                        id="sms-field-' + field.txnFieldId + '"\n                        data-field-id="' + field.txnFieldId + '"\n                        data-txn-id="' + field.txnType + '"\n                        class="sms-field"\n                        ' + (field.txnFieldValue == '1' ? 'checked=checked' : '') + '\n                        type="checkbox">\n                    <label for="sms-field-' + field.txnFieldId + '">' + _this5.generateFieldLabel(field) + '</label>\n                  </div>\n                </div>';
			});
		},
		generateFieldLabel: function generateFieldLabel(field) {
			var label = this.getI18N(field.txnFieldName);
			if (field.txnFieldId == messageFieldConstants.TXN_MESSAGE_TRANSPORTATION_DETAIL) {
				var words = label.split(' ');
				if (words.length >= 1) {
					label = words[0] + ' ';
					for (var i = 1; i < words.length; i++) {
						label += '<span class="md-sub-label">' + words[i] + '</span>';
					}
				}
			}
			return label;
		},
		messageGenerator: function messageGenerator(txnType) {
			var sms = messageFormatter.getDefaultSampleMessage(txnType);
			$('#sms-header').val(sms.header);
			$('#sms-body').val(sms.message);
			$('#sms-footer').val(sms.footer);
		},
		toggleMessageSettingKey: function toggleMessageSettingKey(e) {
			var $target = $(e.target);
			var checked = $target.is(':checked');
			$(document).trigger('outboxCounter:refresh');

			var status = this.saveSettings($target.data('settings-key'), Number(checked));
			if (status === ErrorCode.ERROR_SETTING_SAVE_FAILED) {
				$target.prop('checked', !checked);
				ToastHelper.error(status);
				return;
			}
			ToastHelper.success(status);
			this.showSettings(checked);
		},
		toggleMessageFromOwner: function toggleMessageFromOwner(e) {
			$(document).trigger('outboxCounter:refresh');
			var status = this.saveSettings(Queries.SETTING_TXN_MSG_TO_OWNER, Number($(e.target).is(':checked')));
			if (status === ErrorCode.ERROR_SETTING_SAVE_FAILED) {
				ToastHelper.error(status);
				e.preventDefault();
				return;
			}
			ToastHelper.success(status);
			this.defaults.$el.find('#ownerNumberContainer').toggleClass('hide');
		},
		changeSenderId: function changeSenderId(e) {
			var $target = $(e.target);
			if ($target.val() === '-1') {
				e.preventDefault();
				$target.val(settingCache.getTxnMsgSenderId());
				this.openSenderIdDialog();
				return false;
			}
			var $selectedOption = $target.find('option:selected');
			if ($selectedOption.data('status') == 'Pending' || $selectedOption.data('status') == 'Rejected') {
				e.preventDefault();
				$target.val(settingCache.getTxnMsgSenderId() || 0);
				ToastHelper.error('Please select approved sender id.');
				return false;
			}
			var status = this.saveSettings(Queries.SETTING_TXN_MSG_SENDER_ID, $target.val());
			if (status === ErrorCode.ERROR_SETTING_SAVE_FAILED) {
				ToastHelper.error(status);
				e.preventDefault();
				return false;
			}
			ToastHelper.success(status);
		},
		openSenderIdDialog: function openSenderIdDialog() {
			$('#newSenderIdContainer').dialog({
				appendTo: this.defaults.$el.find('#tab-settings'),
				modal: true,
				close: function close(e) {
					e.stopPropagation();
				}
			});
		},
		createSenderId: function createSenderId(e) {
			var _this6 = this;

			var txtSenderId = this.defaults.$el.find('#txtSenderId');
			var newSenderId = txtSenderId.val().toUpperCase();
			if (!regSenderId.test(newSenderId)) {
				ToastHelper.error('Invalid SenderId: Sender id should be 6 character long and between A to Z');
				return false;
			}
			var ENDPOINTS = {
				alias: domain + '/api/sms/alias',
				defaultHeader: {
					'Accept': 'application/json',
					'Authorization': 'Bearer ' + authToken,
					'Content-Type': 'application/json',
					'Cache-Control': 'no-cache'
				}
			};
			$.post({
				url: ENDPOINTS.alias,
				data: { sender_id: newSenderId },
				headers: (0, _extends3.default)({}, ENDPOINTS.defaultHeader, {
					'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
				}),
				success: function success() {
					var option = $('<option />').data('status', 'Pending').val(newSenderId).html(newSenderId + ' (Pending)');
					_this6.defaults.$el.find('#drdSenderId').append(option);
					txtSenderId.val('');
					$('#newSenderIdContainer').dialog('close');
					ToastHelper.success('Sender Id is created. Please wait for Vyapar team to approve it and you will be able to set it. We will inform you once we approve it by SMS/Email.');
				},
				error: function error(xhr) {
					console.log(xhr.status);
					if (xhr.status == 422) {
						ToastHelper.error('The sender id must be 6 characters.');
					} else if (xhr.status == 409) {
						ToastHelper.error('The sender already exists. Please choose different sender id with 6 characters.');
					} else if (xhr.status == 401) {
						ToastHelper.error('Sender id creation failed. Invalid User. Please login and try again.');
					} else if (xhr.status == 0) {
						ToastHelper.error('Sender id creation failed. Please check internet connection.');
					} else {
						ToastHelper.error('Something went wrong. Please try again.');
					}
				}
			});
		},
		savePhoneNumber: function savePhoneNumber() {
			var regExPhone = /^\+?[0-9]{10,12}$/;
			var number = this.defaults.$el.find('#txtPhoneNumber').val();
			if (!regExPhone.test(number)) {
				ToastHelper.error('Please enter valid phone number without STD Code. Number should contain only 0 to 9 digits. Minimum length must be 10 and Maximum length must be 12.');
				return false;
			}
			var status = this.saveSettings(Queries.SETTING_TXN_MSG_OWNER_NUMBER, number);
			if (status === ErrorCode.ERROR_SETTING_SAVE_FAILED) {
				ToastHelper.error(status);
			} else {
				ToastHelper.success(status);
			}
		},
		showSettings: function showSettings(yes) {
			if (yes) {
				$('.hide-on-disable').css('visibility', 'visible');
			} else {
				$('.hide-on-disable').css('visibility', 'hidden');
			}
		},
		txnTypesHtml: function txnTypesHtml(txnTypes) {
			var _this7 = this;

			var html = '<ul id="txnTypesContainer">';
			txnTypes.forEach(function (txn) {
				html += '\n              <li\n                data-txn-type="' + txn.type + '"\n                data-settings-key="' + txn.id + '"\n                class="txn-list-item">\n                  <div class="md-checkbox">\n                    <input\n                        id="' + txn.id + '"\n                        class="txn-type-input"\n                        ' + (settingCache.isTxnMessageEnabledForTxn(txn.type) ? 'checked=checked' : '') + '\n                        type="checkbox">\n                        <label for="' + txn.id + '">&nbsp;</label>\n                  </div>\n                  <label class="txn-type-label">' + _this7.getI18N(txn.label) + '</label>\n              </li>\n             ';
			});
			html += '</ul>';
			return html;
		},
		getI18N: function getI18N(label) {
			return lang[label] || label;
		},
		saveSettings: function saveSettings(key, value) {
			settingsModel.setSettingKey(key);
			return settingsModel.UpdateSetting(value, { nonSyncableSetting: true });
		},
		openMoreSettingsWindow: function openMoreSettingsWindow() {
			var _this8 = this;

			var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : domain + '/sms';

			var moreSettingsWindow = new BrowserWindow({
				show: true,
				minimizable: false,
				alwaysOnTop: true,
				skipTaskbar: true,
				minWidth: 1000,
				width: 1000,
				autoHideMenuBar: true,
				webPreferences: { nodeIntegration: false }
			});

			// moreSettingsWindow.maximize();

			moreSettingsWindow.on('page-title-updated', function () {
				var title = moreSettingsWindow.getTitle();
				try {
					var data = JSON.parse(title);
					if (data.auth_token !== undefined) {
						TokenForSync.writeToken((0, _stringify2.default)(data));
						_this8.afterLogin(data);
						return false;
					}
					if (data.status != 'paymentSuccess') {
						return false;
					}
					ToastHelper.success('Payment Successful.');
					moreSettingsWindow.close();
				} catch (e) {}
			});
			moreSettingsWindow.on('closed', function (e) {
				moreSettingsWindow = null;
				if (authToken) {
					_this8.getSMSBalance();
				}
			});
			var headers = void 0;
			var emailforlogin = '';

			var firmCache = new FirmCache();
			if (firmCache.getDefaultFirm() == null) {
				emailforlogin = '';
			} else {
				emailforlogin = firmCache.getDefaultFirm().firmEmail;
			}

			if (emailforlogin) {
				url += '&email_id_=' + emailforlogin;
			}

			moreSettingsWindow.webContents.session.clearStorageData({ origin: domain, storages: ['cookies'] });
			moreSettingsWindow.loadURL(url, {
				extraHeaders: headers
			});
			moreSettingsWindow.show();
		},
		getMainTemplate: function getMainTemplate() {
			return '\n      <div id="smsSettingContainer">\n        <div class="container">\n          ' + this.getTabsHTML() + '\n          <div id="tab-transaction" class="hide"> ' + this.getTransactionTabHTML() + ' </div>\n          <div id="tab-owner" class="hide"> ' + this.getOwnerTabHTML() + ' </div>\n          <div id="tab-settings" class="hide"> ' + this.getSettingsTabHTML() + ' </div>\n        </div>\n      </div>\n            ';
		},
		getTabsHTML: function getTabsHTML() {
			return '\n          <div class="row">\n            <div class="tabs-wrapper col-12">\n              <ul class="tabs">\n                <li data-target="#tab-transaction" class="tab active">' + this.getI18N('Transaction SMS') + '</li>\n                <li data-target="#tab-owner" class="tab">' + this.getI18N('Owner SMS') + '</li>\n                <li data-target="#tab-settings" class="tab">' + this.getI18N('SMS Settings') + '</li>\n              </ul>\n            </div>\n          </div>\n        ';
		},
		getTransactionTabHTML: function getTransactionTabHTML() {
			return '\n              <div class="row">\n                <div class="col-4 left-col">\n                  <div class="row toggle-transaction-container">\n                    <div class="col-12">\n                      <div class="md-checkbox">\n                        <input\n                        id="enableTransactionMessageSettings"\n                        data-settings-key="' + Queries.SETTING_TRANSACTION_MESSAGE_ENABLED + '"\n                        ' + (settingCache.getTransactionMessageEnabled() ? 'checked=checked' : '') + '\n                        type="checkbox">\n                        <label tabIndex="0" for="enableTransactionMessageSettings">' + this.getI18N('Enable Transaction SMS') + '</label>\n                      </div>\n                    </div>\n                  </div>\n                  <div class="hide-on-disable">\n                    <div class=\'heading\'>' + this.getI18N('Select transactions for automatic SMS.') + ' </div>\n                    ' + this.txnTypesHtml(this.defaults.txnTypes) + '\n                  </div>\n                </div>\n                <div class="col-8 right-col">\n                  <div class="hide-on-disable">\n                    <div class="show-on-transaction-disable hide"> Please enable transaction to change setting. </div>\n                    <div class="hide-on-transaction-disable hide">\n                    <div class=\'heading\'>' + this.getI18N('Select fields to be added in Message Body.') + '</div>\n                    <div class="row fields" id="sms-fields"> </div>\n                    <div class="heading message-preview">' + this.getI18N('Message Preview') + '</div>\n                    <div class="row sms-text-settings">\n                      <div class="col-12">\n                        <label for="sms-header"> ' + this.getI18N('Message Header') + ' </label>\n                        <textarea class="col-12 sms-input small" type="text" id="sms-header"></textarea>\n                      </div>\n                      <div class="col-12">\n                        <label for="sms-body"> ' + this.getI18N('Message Body') + ' </label>\n                        <textarea class="col-12 sms-input" type="text" id="sms-body" readonly></textarea>\n                      </div>\n                      <div class="col-12">\n                        <label for="sms-footer"> ' + this.getI18N('Message Footer') + ' </label>\n                        <textarea class="col-12 sms-input small" type="text" id="sms-footer"></textarea>\n                      </div>\n                    </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n      ';
		},
		getOwnerTabHTML: function getOwnerTabHTML() {
			return '\n            <div class="row">\n              <div class="col-12 owner-message-setting-container">\n                <div class="md-checkbox">\n                  <input\n                  id="enableOwnerMessageSettings"\n                  data-settings-key="' + Queries.SETTING_TRANSACTION_MESSAGE_ENABLED + '"\n                  ' + (settingCache.isOwnerTxnMsgEnabled() ? 'checked=checked' : '') + '\n                  type="checkbox">\n                  <label tabIndex="0" for="enableOwnerMessageSettings">' + this.getI18N('Enable SMS For Owner') + ' </label>\n                  <span class="inline-tooltip" title="Vyapar will send an SMS copy to owner along with the Party on enabling this feature. SMS Credits are required to send messages to the Owner.">&nbsp;</span>\n                </div>\n              </div>\n              <div id="ownerNumberContainer" class="col-12 ' + (settingCache.isOwnerTxnMsgEnabled() ? '' : 'hide') + '">\n                  <label tabIndex="0" for="txtPhoneNumber">' + this.getI18N('Enter Mobile Number of Company Owner') + '</label>\n                  <input\n                  id="txtPhoneNumber"\n                  value="' + settingCache.getOwnerTxnMsgPhoneNumber() + '"\n                  ' + (settingCache.isOwnerTxnMsgEnabled() ? 'checked=checked' : '') + '\n                  maxlength="12"\n                  minlength="10"\n                  type="text">\n                  <button id="btnPhoneNumber" class="btn btn-small">Save</button>\n              </div>\n            </div>\n      ';
		},
		getSettingsTabHTML: function getSettingsTabHTML() {
			return '\n            <div class="row">\n              <div class="col-6">\n                <div class="row">\n\n                  <div id="signInAsContainer" class="col-12">\n                    <div id="login" class="hide">\n                      <label>' + this.getI18N('Signed in to send SMS') + '</label>\n                      <button id="btnSignIn" class="btn btn-big">Sign In</button>\n                    </div>\n                    <div id="logout" class="hide">\n                      <label for="txtSignedInAs">' + this.getI18N('Signed in as') + '</label>\n                      <input readonly type="text" id="txtSignInAs" />\n                      <a href="#" id="btnLogout">Logout</a>\n                    </div>\n                  </div>\n\n                  <div id="senderIdContainer" class="col-12">\n                      <label for="drdSenderId">' + this.getI18N('Sender ID') + ' <span class="inline-tooltip" title="SMS will be sent by this Id. You may enter any 6 character Id and your parties will receive SMS from this Id."></span></label>\n                      <select id="drdSenderId"> </select>\n                  </div>\n\n                  <div id="newSenderIdContainer" title="Create New Sender Id" class="col-12 hide">\n                      <div class="new-sender-id-container">\n                          <input id="txtSenderId" maxlength="6" type="text" placeholder="' + this.getI18N('Enter new sender id') + '" />\n                          <button id="btnCreateSenderId" class="btn btn-small">Create</button>\n                      </div>\n                  </div>\n\n\n                  <div class="col-12">\n                      <button id="btnMoreSettings" class="btn btn-small">Show More Settings</button>\n                  </div>\n                </div>\n              </div>\n              <div class="col-6">\n                <div class="balance-container material-card" id="balanceContainer">\n                    <div class="balance-label">' + this.getI18N('SMS Balance') + ' <span class="inline-tooltip" title="Number of messages that can be sent without Vyapar branding."></span> </div>\n                    <div class="balance-amount" id="balanceAmount">0</div>\n                    <div class="balance-add-credits" id="addCredits">\n                        <span class="icon-plus">+</span>\n                        <span class="label">Add SMS</span>\n                    </div>\n                </div>\n              </div>\n            </div>\n          ';
		}
	};

	return SMSSettings.init.bind(SMSSettings);
}(jQuery);