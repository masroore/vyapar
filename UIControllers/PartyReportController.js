var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var NameCache = require('./../Cache/NameCache.js');
var nameCache = new NameCache();
var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();
var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');
var DateFormat = require('./../Constants/DateFormat.js');
$('#partyReportEndDate').datepicker({ dateFormat: DateFormat.format });

var printForSharePDF = false;

var nameListToBeShown = [];

var populateTable = function populateTable() {
	CommonUtility.showLoader(function () {
		nameListToBeShown = [];
		settingCache = new SettingCache();
		nameCache = new NameCache();
		var endDateString = $('#partyReportEndDate').val();
		var endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');
		var DataLoader = require('./../DBManager/DataLoader.js');
		var dataLoader = new DataLoader();
		var isDateFilterOn = $('#dateFilter').prop('checked');
		if (!isDateFilterOn) {
			var nameList = nameCache.getListOfNamesObject();
		} else {
			var nameList = dataLoader.getPartyReportList(endDate);
		}
		var partyFilter = $('#partyFilterOptions').val();
		var groupFilter = Number($('#groupFilterOptions').val());
		var zeroBalanceToShown = $('#showZeroBalanceParties').is(':checked');
		for (var i in nameList) {
			var nameObject = nameList[i];
			if (!zeroBalanceToShown && nameObject.getAmount() == 0) {
				continue;
			}
			if (partyFilter == '1' && nameObject.getAmount() < 0) {
				continue;
			}
			if (partyFilter == '2' && nameObject.getAmount() >= 0) {
				continue;
			}
			if (groupFilter > 0 && nameObject.getGroupId() != groupFilter) {
				continue;
			}
			nameListToBeShown.push(nameObject);
		}
		sortNameList(nameListToBeShown);
		displayNameList(nameListToBeShown);
	});
};

$('#dateFilter').change(function () {
	populateTable();
	var isDateFilterEnabled = $('#dateFilter').prop('checked');
	if (isDateFilterEnabled) {
		$('#partyReportEndDate').prop('disabled', false);
	} else {
		$('#partyReportEndDate').prop('disabled', true);
	}
});

if (settingCache.isPartyGroupEnabled()) {
	var PartyGroupCache = require('./../Cache/PartyGroupCache.js');
	var partyGroupCache = new PartyGroupCache();
	var nameGroupList = partyGroupCache.getNameGroupList();

	var optionTemp = document.createElement('option');
	var _iteratorNormalCompletion = true;
	var _didIteratorError = false;
	var _iteratorError = undefined;

	try {
		for (var _iterator = (0, _getIterator3.default)(nameGroupList), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
			var nameGroupObj = _step.value;

			var option = optionTemp.cloneNode();
			option.text = nameGroupObj.getGroupName();
			option.value = nameGroupObj.getGroupId();
			$('#groupFilterOptions').append(option);
		}
	} catch (err) {
		_didIteratorError = true;
		_iteratorError = err;
	} finally {
		try {
			if (!_iteratorNormalCompletion && _iterator.return) {
				_iterator.return();
			}
		} finally {
			if (_didIteratorError) {
				throw _iteratorError;
			}
		}
	}

	$('#groupFilterChooser').show();
} else {
	$('#groupFilterChooser').hide();
}

$('#groupFilterOptions').on('selectmenuchange', function (event) {
	populateTable();
});

$('#partyFilterOptions').on('selectmenuchange', function () {
	populateTable();
});

$('#showZeroBalanceParties').on('change', function () {
	populateTable();
});

var date = MyDate.getDate('d/m/y');
$('#partyReportEndDate').val(date);

$('#partyReportEndDate').change(function () {
	populateTable();
});

var partyReportClusterize;

var displayNameList = function displayNameList(nameListToBeShown) {
	var len = nameListToBeShown.length;

	var dynamicRow = '';
	var rowData = [];
	var dynamicFooter = '';
	var totalReceivableAmount = 0;
	var totalPayableAmount = 0;
	dynamicRow += "<thead><tr><th width='5%'></th><th width='20%'>Name</th><th width='20%'>Email</th><th width='19%'>Phone No.</th><th width='18%' class='tableCellTextAlignRight'>Receivable Balance</th><th width='18%' class='tableCellTextAlignRight'>Payable Balance</th></tr></thead>";
	dynamicFooter += "<thead><tr><th width='5%'></th><th width='20%'>Total</th><th width='20%'></th><th width='19%'></th>";
	for (var i = 0; i < len; i++) {
		var nameModel = nameListToBeShown[i];
		var row = '';
		row = "<tr ondblclick='openPartyDetails(" + nameModel.getNameId() + ")' class='currentRow'><td style='width:5%'>" + (i + 1) + "</td><td width='20%'>" + nameModel.getFullName() + "</td><td width='20%'>" + nameModel.getEmail() + "</td><td width='20%'>" + nameModel.getPhoneNumber() + '</td>';

		if (nameModel.getAmount() < 0) {
			totalPayableAmount += nameModel.getAmount();
			row += "<td width='18%'></td><td width='18%' class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimalAndCurrency(nameModel.getAmount()) + '</td>';
		} else {
			totalReceivableAmount += nameModel.getAmount();
			row += "<td width='18%' class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimalAndCurrency(nameModel.getAmount()) + "</td><td width='18%'></td>";
		}
		row += '</tr>';
		rowData.push(row);
	}
	var data = rowData;

	if ($('#partyReportContainer').length === 0) {
		return;
	}
	if (partyReportClusterize && partyReportClusterize.destroy) {
		partyReportClusterize.clear();
		partyReportClusterize.destroy();
	}

	partyReportClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollArea',
		contentId: 'contentArea',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});
	$('#tableHead').html(dynamicRow);
	dynamicFooter += "<th width='18%' class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimalAndCurrency(totalReceivableAmount) + "</th><th width='18%' class='tableCellTextAlignRight'>" + MyDouble.getBalanceAmountWithDecimalAndCurrency(totalPayableAmount) + '</th></tr></thead>';
	$('#total').html(dynamicFooter);
};

var sortNameList = function sortNameList(nameListToBeShown) {
	if (nameListToBeShown) {
		nameListToBeShown.sort(function (lhs, rhs) {
			if (rhs.getFullName().toLowerCase() < lhs.getFullName().toLowerCase()) {
				return 1;
			} else if (rhs.getFullName().toLowerCase() > lhs.getFullName().toLowerCase()) {
				return -1;
			}
			return 0;
		});
	}
};

var openPartyDetails = function openPartyDetails(nameId) {
	var PartiesContainer = require('../UIComponent/jsx/PartiesContainer').default;
	var MountComponent = require('../UIComponent/jsx/MountComponent').default;
	MountComponent(PartiesContainer, document.querySelector('#defaultPage'), {
		partyId: Number(nameId)
	});
};

var getHTMLTextForReport = function getHTMLTextForReport() {
	var PartyReportHTMLGenerator = require('./../ReportHTMLGenerator/PartyReportHTMLGenerator.js');
	var partyReportHTMLGenerator = new PartyReportHTMLGenerator();

	var printPhoneNumber = $('#printPhone').is(':checked');
	var printEmail = $('#printEmail').is(':checked');
	var printBalance = $('#printBalance').is(':checked');
	var printAddress = $('#printAddress').is(':checked');
	var printTIN = $('#printTIN').is(':checked');
	var htmlText = partyReportHTMLGenerator.getHTMLText(nameListToBeShown, printPhoneNumber, printEmail, printBalance, printAddress, printTIN);
	return htmlText;
};

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	// $('#frameDiv').load('');
	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable();
};

/// ///////////////////////////PDF/////////////////////////////////////

function openPrintOptions() {
	if (settingCache.isTINNumberEnabled()) {
		$('#printTINDiv').show();
		if (settingCache.getGSTEnabled()) {
			$('#printTINText').html('GSTIN');
		} else {
			$('#printTINText').html(settingCache.getTINText());
		}
	} else {
		$('#printTINDiv').hide();
		$('#printTIN').prop('checked', false);
	}
	$('#partyPrintDialog').removeClass('hide');
	$('#partyPrintDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closePrintOptions() {
	$('#partyPrintDialog').addClass('hide');
	$('#partyPrintDialog').dialog('close').dialog('destroy');
}

var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.PARTY_REPORT });
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
		$('#partyPrintDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

function openExcelOptions() {
	if (settingCache.isTINNumberEnabled()) {
		$('#excelTINDiv').show();
		if (settingCache.getGSTEnabled()) {
			$('#excelTINText').html('GSTIN');
		} else {
			$('#excelTINText').html(settingCache.getTINText());
		}
	} else {
		$('#excelTINDiv').hide();
		$('#ExcelTIN').prop('checked', false);
	}
	$('#ExcelDialog').removeClass('hide');
	$('#ExcelDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closeExcelOptions() {
	$('#ExcelDialog').addClass('hide');
	$('#ExcelDialog').dialog('close').dialog('destroy');
}

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.PARTY_REPORT });
	excelHelper.saveExcel(fileName);
};

var prepareObjectForExcel = function prepareObjectForExcel(nameListToBeShown) {
	var len = nameListToBeShown.length;
	var rowObj = [];
	var tableHeadArray = [];
	var ExcelPhone = $('#ExcelPhone').is(':checked');
	var ExcelEmail = $('#ExcelEmail').is(':checked');
	var ExcelBalance = $('#ExcelBalance').is(':checked');
	var ExcelAddress = $('#ExcelAddress').is(':checked');
	var ExcelTIN = $('#ExcelTIN').is(':checked');
	tableHeadArray.push('Name');
	if (ExcelEmail) {
		tableHeadArray.push('Email');
	}
	if (ExcelPhone) {
		tableHeadArray.push('Phone No.');
	}
	if (ExcelAddress) {
		tableHeadArray.push('Address');
	}
	if (ExcelTIN) {
		if (settingCache.getGSTEnabled()) {
			tableHeadArray.push('GSTIN');
		} else {
			tableHeadArray.push(settingCache.getTINText());
		}
	}
	if (ExcelBalance) {
		tableHeadArray.push('Receivable Balance');tableHeadArray.push('Payable Balance');
	}
	rowObj.push(tableHeadArray);
	rowObj.push([]);
	var totalreceivableAmount = 0;
	var totalpayableAmount = 0;
	for (var j = 0; j < len; j++) {
		var tempArray = [];
		var nameModel = nameListToBeShown[j];
		var receivableAmount = '';
		var payableAmount = '';
		tempArray.push(nameModel.getFullName());
		if (ExcelEmail) {
			tempArray.push(nameModel.getEmail());
		}
		if (ExcelPhone) {
			tempArray.push(nameModel.getPhoneNumber());
		}
		if (ExcelAddress) {
			tempArray.push(nameModel.getAddress() ? nameModel.getAddress() : '');
		}
		if (ExcelTIN) {
			if (settingCache.getGSTEnabled()) {
				tempArray.push(nameModel.getGstinNumber() ? nameModel.getGstinNumber() : '');
			} else {
				tempArray.push(nameModel.getTinNumber() ? nameModel.getTinNumber() : '');
			}
		}
		if (ExcelBalance) {
			if (nameModel.getAmount() < 0) {
				payableAmount = Math.abs(nameModel.getAmount());
				totalpayableAmount += payableAmount;
			} else {
				receivableAmount = Math.abs(nameModel.getAmount());
				totalreceivableAmount += receivableAmount;
			}
			tempArray.push(MyDouble.getAmountWithDecimal(receivableAmount));
			tempArray.push(MyDouble.getAmountWithDecimal(payableAmount));
		}
		rowObj.push(tempArray);
	}
	rowObj.push([]);
	var totalArray = [];
	if (ExcelEmail) {
		totalArray.push('');
	}
	if (ExcelPhone) {
		totalArray.push('');
	}
	if (ExcelAddress) {
		totalArray.push('');
	}
	if (ExcelTIN) {
		totalArray.push('');
	}
	if (ExcelBalance) {
		totalArray.push('Total');
		totalArray.push(MyDouble.getAmountWithDecimal(totalreceivableAmount));
		totalArray.push(MyDouble.getAmountWithDecimal(totalpayableAmount));
	}
	rowObj.push(totalArray);
	return rowObj;
};

function printExcel() {
	closeExcelOptions();
	var XLSX = require('xlsx');
	var dataArray = prepareObjectForExcel(nameListToBeShown);
	var worksheet = XLSX.utils.aoa_to_sheet(dataArray);

	var workbook = {
		'SheetNames': [],
		'Sheets': {}
	};

	var wsName = 'Party Report';
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet;
	var wscolWidth = [{ wch: 30 }, { wch: 25 }, { wch: 25 }, { wch: 25 }, { wch: 25 }, { wch: 25 }];
	worksheet['!cols'] = wscolWidth;
	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

populateTable();