var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var startDate;
var endDate;
var firmId;
var DateFormat = require('./../Constants/DateFormat.js');
$('#startDate, #endDate').datepicker({ dateFormat: DateFormat.format });
var SettingCache = require('./../Cache/SettingCache.js');
var settingCache = new SettingCache();
var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');

var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
var paymentInfoCache = new PaymentInfoCache();

var printForSharePDF = false;

var listOfTransactions = [];
var openingCashInHand = 0;
var closingCashInHand = 0;
var populateTable = function populateTable() {
	settingCache = new SettingCache();
	paymentInfoCache = new PaymentInfoCache();
	var DataLoader = require('./../DBManager/DataLoader.js');
	var dataLoader = new DataLoader();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	firmId = Number($('#firmFilterOptions').val());
	startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
	endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');
	var showZeroCashAmountTransaction = $('#showZeroCashAmountTransaction').is(':checked');
	CommonUtility.showLoader(function () {
		listOfTransactions = dataLoader.loadTransactionsForCashFlowReport(startDate, endDate, firmId, showZeroCashAmountTransaction);
		if (firmId == 0) {
			var prevDate = MyDate.addDays(startDate, -1);
			openingCashInHand = dataLoader.getCashInHandAmount(prevDate);
			closingCashInHand = dataLoader.getCashInHandAmount(endDate);
		} else {
			openingCashInHand = 0;
			closingCashInHand = 0;
		}

		displayTransactionList(listOfTransactions, openingCashInHand, closingCashInHand, firmId);
	});
};

var date = MyDate.getDate('d/m/y');
$('#startDate').val(MyDate.getFirstDateOfCurrentMonthString());
$('#endDate').val(date);

if (settingCache.getMultipleFirmEnabled()) {
	var optionTemp = document.createElement('option');
	var FirmCache = require('./../Cache/FirmCache.js');
	var firmCache = new FirmCache();
	var firmList = firmCache.getFirmList();
	for (i in firmList) {
		var option = optionTemp.cloneNode();
		option.text = firmList[i].getFirmName();
		option.value = firmList[i].getFirmId();
		$('#firmFilterOptions').append(option);
	}
	$('#firmChooser').show();
} else {
	$('#firmChooser').hide();
}

$('#firmFilterOptions').on('selectmenuchange', function (event) {
	populateTable();
});

$('#startDate').change(function () {
	populateTable();
});

$('#endDate').change(function () {
	populateTable();
});

$('#showZeroCashAmountTransaction').on('change', function () {
	populateTable();
});

var cashFlowReportClusterize;

var displayTransactionList = function displayTransactionList(listOfTransactions, openingCashInHand, closingCashInHand, firmId) {
	var len = listOfTransactions.length;
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var dynamicRow = '';
	var rowData = [];
	var isTransactionRefNumberEnabled = settingCache.getTxnRefNoEnabled();
	var extraColumnWidth = isTransactionRefNumberEnabled ? 0 : 2;
	var cashInHand = openingCashInHand;

	$('#open_cash_in_hand').html(MyDouble.getBalanceAmountWithDecimalAndCurrency(openingCashInHand, true));
	$('#close_cash_in_hand').html(MyDouble.getBalanceAmountWithDecimalAndCurrency(closingCashInHand, true));

	if (firmId == 0) {
		$('.total_cash').css('display', 'block');
	} else {
		$('.total_cash').css('display', 'none');
	}

	var className = '';
	if (!$('#sortIcon').attr('class')) {
		className = 'ui-selectmenu-icon ui-icon ui-icon-triangle-1-s';
	} else {
		className = $('#sortIcon').attr('class');
	}

	dynamicRow += "<thead><tr><th width='" + (13 + extraColumnWidth) + "%'>DATE</th>";
	if (isTransactionRefNumberEnabled) {
		dynamicRow += '<th width=\'12%\' id=\'invoiceColId\' style=\'cursor:pointer;\'>Ref No.<span id=\'sortIcon\' class=\'' + className + '\'></span></th>';
	}
	dynamicRow += "<th width='" + (17 + extraColumnWidth) + "%'>NAME</th><th width='" + (13 + extraColumnWidth) + "%'>TYPE</th><th width='" + (15 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>Cash in</th><th width='" + (15 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>Cash out</th>";
	if (firmId == 0) {
		dynamicRow += "<th width='" + (15 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>Running Cash In Hand</th>";
	}
	dynamicRow += '</tr></thead>';
	var totalCashIn = 0;
	var totalCashOut = 0;
	for (var _i = 0; _i < len; _i++) {
		var txn = listOfTransactions[_i];
		var txnType = txn.getTxnType();
		var typeString = TxnTypeConstant.getTxnTypeForUI(txnType, txn.getTxnSubType());
		var name = '';
		var cashAmount = Number(txn.getCashAmount());
		var typeTxn = TxnTypeConstant.getTxnType(txn.getTxnType());
		var refNo = txn.getTxnRefNumber() === '' ? '' : txn.getFullTxnRefNumber();

		if (txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE || txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD) {
			name = paymentInfoCache.getPaymentBankName(txn.getBankId());
		} else if (txnType == TxnTypeConstant.TXN_TYPE_CASH_ADJ_REDUCE || txnType == TxnTypeConstant.TXN_TYPE_CASH_ADJ_ADD) {
			name = 'Cash adjustment';
		} else if (txnType == TxnTypeConstant.TXN_TYPE_CASH_OPENINGBALANCE) {
			name = 'Opening Cash in Hand';
		} else if (TxnTypeConstant.isLoanTxnType(txnType)) {
			name = txn.getDescription();
		} else {
			if (txn.getNameRef && txn.getNameRef()) {
				name = txn.getNameRef().getFullName();
			}
		}
		var date = MyDate.getDate('d/m/y', txn.getTxnDate());
		var row = '<tr id=' + txn.getTxnId() + ':' + typeTxn + " ondblclick='openTransaction(" + txn.getTxnId() + ',' + txn.getTxnType() + ")' class='currentRow'><td width='" + (13 + extraColumnWidth) + "%'>" + date + '</td>';
		if (isTransactionRefNumberEnabled) {
			row += "<td width='12%'>" + refNo + '</td>';
		}
		row += "<td width='" + (17 + extraColumnWidth) + "%'>" + name + "</td><td width='" + (13 + extraColumnWidth) + "%'>" + typeString + '</td>';

		var txnTypeToBeUsed = txnType == TxnTypeConstant.TXN_TYPE_CHEQUE_TRANSFER ? txn.getTxnSubType() : txnType;
		if (txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_SALE || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_CASHIN || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_CASH_ADJ_ADD || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_OTHER_INCOME) {
			totalCashIn += cashAmount;
			cashInHand += cashAmount;
			row += "<td width='" + (15 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(cashAmount) + "</td><td width='" + (15 + extraColumnWidth) + "%' class='tableCellTextAlignRight'></td>";
		} else if (txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_PURCHASE || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_CASHOUT || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_EXPENSE || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_CASH_ADJ_REDUCE || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
			totalCashOut += cashAmount;
			cashInHand -= cashAmount;
			row += "<td width='" + (15 + extraColumnWidth) + "%' class='tableCellTextAlignRight'></td><td class='tableCellTextAlignRight' width='" + (15 + extraColumnWidth) + "%'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(cashAmount) + '</td>';
		} else if (txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_CASH_OPENINGBALANCE) {
			if (cashAmount >= 0) {
				totalCashIn += cashAmount;
				row += "<td width='" + (15 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(cashAmount) + "</td><td width='" + (15 + extraColumnWidth) + "%' class='tableCellTextAlignRight'></td>";
			} else {
				totalCashOut += Math.abs(cashAmount);
				row += "<td width='" + (15 + extraColumnWidth) + "%' class='tableCellTextAlignRight'></td><td width='" + (15 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(cashAmount) + '</td>';
			}
			cashInHand += cashAmount;
		} else if (TxnTypeConstant.isLoanTxnType(txnTypeToBeUsed)) {
			if (txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_LOAN_EMI_PAYMENT || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_LOAN_PROCESSING_FEE) {
				totalCashOut += cashAmount;
				cashInHand -= cashAmount;
				row += "<td width='" + (15 + extraColumnWidth) + "%' class='tableCellTextAlignRight'></td><td class='tableCellTextAlignRight' width='" + (15 + extraColumnWidth) + "%'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(cashAmount) + '</td>';
			} else {
				totalCashIn += cashAmount;
				cashInHand += cashAmount;
				row += "<td width='" + (15 + extraColumnWidth) + "%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(cashAmount) + "</td><td width='" + (15 + extraColumnWidth) + "%' class='tableCellTextAlignRight'></td>";
			}
		}
		if (firmId == 0) {
			row += "<td class='tableCellTextAlignRight' width='" + (15 + extraColumnWidth) + "%'>" + MyDouble.getBalanceAmountWithDecimalAndCurrency(cashInHand, true) + '</td>';
		}
		row += '</tr>';
		rowData.push(row);
	}
	var data = rowData;

	if ($('#cashFlowReportContainer').length === 0) {
		return;
	}
	if (cashFlowReportClusterize && cashFlowReportClusterize.destroy) {
		cashFlowReportClusterize.clear();
		cashFlowReportClusterize.destroy();
	}

	cashFlowReportClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollArea',
		contentId: 'contentArea',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});
	$('#tableHead').html(dynamicRow);
	$('#totalCashInAmount').html(MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalCashIn));
	$('#totalCashOutAmount').html(MyDouble.getAmountWithDecimalAndCurrencyWithoutSign(totalCashOut));
};

$('#tableHead').on('click', '#invoiceColId', function (e) {
	var SortHelper = require('./../Utilities/SortHelper.js');
	var sortHelper = new SortHelper();
	listOfTransactions = sortHelper.getSortedListByInvoice(listOfTransactions, 'txnRefNumber');
	displayTransactionList(listOfTransactions);
});
var openTransaction = function openTransaction(txnId, txnType) {
	var ViewTransaction = require('./../BizLogic/ViewTransaction.js');
	var viewTransaction = new ViewTransaction();
	viewTransaction.viewTransactionFile(txnId + ':' + TxnTypeConstant.getTxnType(txnType));
};

if (settingCache.getItemEnabled()) {
	$('#itemDetailPrintDiv').show();
} else {
	$('#itemDetailPrintDiv').hide();
}

var getHTMLTextForReport = function getHTMLTextForReport() {
	var CashFlowReportHTMLGenerator = require('./../ReportHTMLGenerator/CashFlowReportHTMLGenerator.js');
	var cashFlowReportHTMLGenerator = new CashFlowReportHTMLGenerator();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var firmId = Number($('#firmFilterOptions').val());
	var printItemDetails = $('#printItems').is(':checked');
	var printDescription = $('#printDescription').is(':checked');
	var htmlText = cashFlowReportHTMLGenerator.getHTMLText(listOfTransactions, openingCashInHand, closingCashInHand, startDateString, endDateString, firmId, printItemDetails, printDescription);
	return htmlText;
};

var printTransaction = function printTransaction(that) {
	var settingCache = new SettingCache();
	var ThermalPrinterConstant = require('./../Constants/ThermalPrinterConstant.js');
	if (settingCache.getDefaultPrinterType() == ThermalPrinterConstant.THERMAL_PRINTER) {
		var txnId = that.id.split(':')[0];
		var PrintUtil = require('./../Utilities/PrintUtil.js');
		PrintUtil.printTransactionUsingThermalPrinter(txnId);
	} else {
		var html = transactionPDFHandler.transactionToHTML(that.id);
		if (html) {
			pdfHandler.print(html);
		}
	}
};

$('#currentAccountTransactionDetails').contextmenu({
	delegate: '.currentRow',
	addClass: 'width10',
	menu: [{ title: 'Open PDF', cmd: 'openpdf' }, { title: '----' }, { title: 'Print', cmd: 'print' }, { title: '----' }, { title: 'Delete', cmd: 'delete' }],
	select: function select(event, ui) {
		var id = ui.target[0].parentNode.id;
		if (ui.cmd == 'print') {
			printTransaction(ui.target[0].parentNode);
		} else if (ui.cmd == 'openpdf') {
			openPdfFromRightClickMenu(ui.target[0].parentNode);
		} else if (ui.cmd == 'delete') {
			var DeleteTransaction = require('./../BizLogic/DeleteTransaction.js');
			var deleteTransaction = new DeleteTransaction();
			deleteTransaction.RemoveTransaction(id);
		}
	}
});

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	// $('#frameDiv').load('');
	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable();
};

/// ///////////////////////////PDF/////////////////////////////////////

function openPrintOptions() {
	$('#cashFlowPrintDialog').removeClass('hide');
	$('#cashFlowPrintDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closePrintOptions() {
	$('#cashFlowPrintDialog').addClass('hide');
	$('#cashFlowPrintDialog').dialog('close').dialog('destroy');
}

var openPdfFromRightClickMenu = function openPdfFromRightClickMenu(that) {
	$('#loading').show(function () {
		var html = transactionPDFHandler.transactionToHTML(that.id);
		if (html) {
			pdfHandler.openPDF(html, false);
		}
	});
};

var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.CASHFLOW_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#cashFlowPrintDialog').addClass('hide');
	$('#cashFlowPrintDialog').dialog('close').dialog('destroy');
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

if (settingCache.getItemEnabled()) {
	$('#itemDetailsExcel').show();
} else {
	$('#itemDetailsExcel').hide();
}

function openExcelOptions() {
	$('#ExcelDialog').removeClass('hide');
	$('#ExcelDialog').dialog({
		closeOnEscape: true,
		draggable: true,
		modal: true
	});
}

function closeExcelOptions() {
	$('#ExcelDialog').addClass('hide');
	$('#ExcelDialog').dialog('close').dialog('destroy');
}

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.CASHFLOW_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
	excelHelper.saveExcel(fileName);
};

var prepareObjectForExcel = function prepareObjectForExcel(listOfTransactions, openingCashInHand, closingCashInHand, firmId) {
	var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
	var isTransactionRefNumberEnabled = settingCache.getTxnRefNoEnabled();
	var ExcelDescription = $('#ExcelCashDescription').is(':checked');
	var len = listOfTransactions.length;
	var rowObj = [];
	var tableHeadArray = [];
	var opening_cash = [];
	var closing_cash = [];
	var totalArray = [];
	var cashInHand = openingCashInHand;
	tableHeadArray.push('Date');
	if (isTransactionRefNumberEnabled) {
		tableHeadArray.push('Reference No');
		totalArray.push('');
		if (firmId == 0) {
			opening_cash.push('');
			closing_cash.push('');
		}
	}
	tableHeadArray.push('Name');
	tableHeadArray.push('Type');
	tableHeadArray.push('Cash In Amount');
	tableHeadArray.push('Cash Out Amount');
	if (firmId == 0) {
		tableHeadArray.push('Running hand in cash');
	}
	if (ExcelDescription) {
		tableHeadArray.push('Description');
	}

	rowObj.push(tableHeadArray);
	rowObj.push([]);
	if (firmId == 0) {
		opening_cash.push('');
		opening_cash.push('');
		opening_cash.push('Opening Cash');
		opening_cash.push('');
		opening_cash.push('');
		opening_cash.push(openingCashInHand);
		closing_cash.push('');
		closing_cash.push('');
		closing_cash.push('Closing Cash');
		closing_cash.push('');
		closing_cash.push('');
		closing_cash.push(closingCashInHand);
		rowObj.push(opening_cash);
		rowObj.push([]);
	}

	totalArray.push('');
	totalArray.push('');

	var totalCashInAmount = 0;
	var totalCashOutAmount = 0;
	for (var j = 0; j < len; j++) {
		var tempArray = [];
		var txn = listOfTransactions[j];
		var txnType = txn.getTxnType();
		var typeString = TxnTypeConstant.getTxnTypeForUI(txnType, txn.getTxnSubType());
		var name = '';
		var cashAmount = Number(txn.getCashAmount());
		var cashInAmount = '';
		var cashOutAmount = '';

		if (txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE || txnType == TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD) {
			name = paymentInfoCache.getPaymentBankName(txn.getBankId());
		} else if (txnType == TxnTypeConstant.TXN_TYPE_CASH_ADJ_REDUCE || txnType == TxnTypeConstant.TXN_TYPE_CASH_ADJ_ADD) {
			name = 'Cash adjustment';
		} else if (txnType == TxnTypeConstant.TXN_TYPE_CASH_OPENINGBALANCE) {
			name = 'Opening Cash in Hand';
		} else if (TxnTypeConstant.isLoanTxnType(txnType)) {
			name = txn.getDescription();
		} else {
			if (txn.getNameRef && txn.getNameRef()) {
				name = txn.getNameRef().getFullName();
			}
		}
		var date = MyDate.getDate('d/m/y', txn.getTxnDate());
		tempArray.push(date);
		if (isTransactionRefNumberEnabled) {
			var refNo = txn.getTxnRefNumber() === '' ? '' : txn.getFullTxnRefNumber();
			tempArray.push(refNo);
		}
		tempArray.push(name);
		tempArray.push(typeString);
		var txnTypeToBeUsed = txnType == TxnTypeConstant.TXN_TYPE_CHEQUE_TRANSFER ? txn.getTxnSubType() : txnType;
		if (txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_SALE || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_CASHIN || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_SALE_ORDER || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_BANK_ADJ_REDUCE || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_CASH_ADJ_ADD || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_OTHER_INCOME) {
			cashInAmount = cashAmount;
			totalCashInAmount += cashAmount;
			cashInHand += cashAmount;
		} else if (txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_SALE_RETURN || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_PURCHASE || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_CASHOUT || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_EXPENSE || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_BANK_ADJ_ADD || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_CASH_ADJ_REDUCE || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_PURCHASE_ORDER) {
			cashOutAmount = cashAmount;
			totalCashOutAmount += cashAmount;
			cashInHand -= cashAmount;
		} else if (txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_CASH_OPENINGBALANCE) {
			if (cashAmount >= 0) {
				cashInAmount = cashAmount;
				totalCashInAmount += cashAmount;
			} else {
				cashOutAmount = Math.abs(cashAmount);
				totalCashOutAmount += Math.abs(cashAmount);
			}
			cashInHand += cashAmount;
		} else if (TxnTypeConstant.isLoanTxnType(txnTypeToBeUsed)) {
			if (txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_LOAN_EMI_PAYMENT || txnTypeToBeUsed == TxnTypeConstant.TXN_TYPE_LOAN_PROCESSING_FEE) {
				cashOutAmount = Math.abs(cashAmount);
				totalCashOutAmount += Math.abs(cashAmount);
			} else {
				cashInAmount = cashAmount;
				totalCashInAmount += cashAmount;
			}
		}
		tempArray.push(MyDouble.getAmountWithDecimal(cashInAmount));
		tempArray.push(MyDouble.getAmountWithDecimal(cashOutAmount));
		if (firmId == 0) {
			tempArray.push(MyDouble.getAmountWithDecimal(cashInHand));
		}
		if (ExcelDescription) {
			tempArray.push(!TxnTypeConstant.isLoanTxnType(txnType) ? txn.getDescription() : '');
		}
		rowObj.push(tempArray);
	}
	rowObj.push([]);
	totalArray.push('Total');
	totalArray.push(MyDouble.getAmountWithDecimal(totalCashInAmount));
	totalArray.push(MyDouble.getAmountWithDecimal(totalCashOutAmount));
	rowObj.push(totalArray);
	if (firmId == 0) {
		rowObj.push([]);
		rowObj.push(closing_cash);
	}

	return rowObj;
};

function printExcel() {
	closeExcelOptions();
	var XLSX = require('xlsx');
	var dataArray = prepareObjectForExcel(listOfTransactions, openingCashInHand, closingCashInHand, firmId);
	var worksheet = XLSX.utils.aoa_to_sheet(dataArray);
	var ExcelItemDetails = $('#ExcelCashItemDetails').is(':checked');
	var ExcelExportHelper = require('./../UIControllers/ExcelExportHelper.js');
	var excelExportHelper = new ExcelExportHelper();

	var workbook = {
		'SheetNames': [],
		'Sheets': {}
	};

	var wsName = 'Cash Flow Report';
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet;
	var wscolWidth = [{ wch: 15 }, { wch: 15 }, { wch: 30 }, { wch: 20 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }];
	worksheet['!cols'] = wscolWidth;

	if (ExcelItemDetails) {
		var wsNameItem = 'Item Details';
		workbook.SheetNames[1] = wsNameItem;
		var itemArray = excelExportHelper.prepareItemObjectForExcel(listOfTransactions);
		var itemWorksheet = XLSX.utils.aoa_to_sheet(itemArray);
		workbook.Sheets[wsNameItem] = itemWorksheet;
		var wsItemcolWidth = [{ wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 15 }, { wch: 20 }, { wch: 20 }];
		itemWorksheet['!cols'] = wsItemcolWidth;
	}
	console.log(workbook);
	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}
populateTable();