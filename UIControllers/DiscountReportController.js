var startDate;
var endDate;
var DateFormat = require('./../Constants/DateFormat.js');
$('#startDate, #endDate').datepicker({ dateFormat: DateFormat.format });
var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var excelHelper = new ExcelHelper();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');
var printForSharePDF = false;

var listOfTransactions = [];

var populateTable = function populateTable() {
	//
	var DataLoader = require('./../DBManager/DataLoader.js');
	var dataLoader = new DataLoader();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	startDate = MyDate.getDateObj(startDateString, 'dd/MM/yyyy', '/');
	endDate = MyDate.getDateObj(endDateString, 'dd/MM/yyyy', '/');
	var firmId = Number($('#firmFilterOptions').val());
	CommonUtility.showLoader(function () {
		listOfTransactions = dataLoader.getListForDiscountReport(startDate, endDate, firmId);
		displayTransactionList(listOfTransactions);
	});
};

var date = MyDate.getDate('d/m/y');
$('#startDate').val(MyDate.getFirstDateOfCurrentMonthString());
$('#endDate').val(date);

if (settingCache.getMultipleFirmEnabled()) {
	var optionTemp = document.createElement('option');
	var FirmCache = require('./../Cache/FirmCache.js');
	var firmCache = new FirmCache();
	var firmList = firmCache.getFirmList();
	for (i in firmList) {
		var option = optionTemp.cloneNode();
		option.text = firmList[i].getFirmName();
		option.value = firmList[i].getFirmId();
		$('#firmFilterOptions').append(option);
	}
	$('#firmChooser').show();
} else {
	$('#firmChooser').hide();
}

$('#firmFilterOptions').on('selectmenuchange', function (event) {
	populateTable();
});

$('#startDate').change(function () {
	populateTable();
});

$('#endDate').change(function () {
	populateTable();
});

var discountReportClusterize;

var displayTransactionList = function displayTransactionList(listOfTransactions) {
	var len = listOfTransactions.length;
	var rowData = [];
	var dynamicRow = '';
	dynamicRow += "<thead><tr><th width='33%'>Party Name</th><th width='33%' class='tableCellTextAlignRight'>Sale Discount</th><th width='33%' class='tableCellTextAlignRight'>Purchase Discount</th></tr></thead>";
	var totalSaleDiscount = 0;
	var totalPurchaseDiscount = 0;
	for (var _i = 0; _i < len; _i++) {
		var row = '';
		var txn = listOfTransactions[_i];
		var partyName = txn.getPartyName();
		var saleDiscount = txn.getSaleDiscount();
		var purchaseDiscount = txn.getPurchaseDiscount();

		row += "<tr><td width='33%'>" + partyName + '</td>';

		row += "<td width='33%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(saleDiscount) + '</td>';
		row += "<td width='33%' class='tableCellTextAlignRight'>" + MyDouble.getAmountWithDecimalAndCurrencyWithSign(purchaseDiscount) + '</td>';
		row += '</tr>';
		totalSaleDiscount += saleDiscount;
		totalPurchaseDiscount += purchaseDiscount;
		rowData.push(row);
	}
	var data = rowData;

	if ($('#discountReportContainer').length === 0) {
		return;
	}
	if (discountReportClusterize && discountReportClusterize.destroy) {
		discountReportClusterize.clear();
		discountReportClusterize.destroy();
	}

	discountReportClusterize = new Clusterize({
		rows: data,
		scrollId: 'scrollArea',
		contentId: 'contentArea',
		rows_in_block: StringConstant.clusterizeRow,
		blocks_in_cluster: StringConstant.clusterizeBlock
	});

	$('#tableHead').html(dynamicRow);
	$('#totalSaleDiscount').html(MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalSaleDiscount));
	$('#totalPurchaseDiscount').html(MyDouble.getAmountWithDecimalAndCurrencyWithSign(totalPurchaseDiscount));
};

var getHTMLTextForReport = function getHTMLTextForReport() {
	var DiscountReportHTMLGenerator = require('./../ReportHTMLGenerator/DiscountReportHTMLGenerator.js');
	var discountReportHTMLGenerator = new DiscountReportHTMLGenerator();
	var startDateString = $('#startDate').val();
	var endDateString = $('#endDate').val();
	var firmId = Number($('#firmFilterOptions').val());
	var htmlText = discountReportHTMLGenerator.getHTMLText(listOfTransactions, startDateString, endDateString, firmId);
	return htmlText;
};

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable();
};

/// ///////////////////////////PDF/////////////////////////////////////
var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.DISCOUNT_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.DISCOUNT_REPORT, fromDate: $('#startDate').val(), toDate: $('#endDate').val() });
	excelHelper.saveExcel(fileName);
};

var prepareObjectForExcel = function prepareObjectForExcel(listOfTransactions) {
	//
	var len = listOfTransactions.length;
	var rowObj = [];
	var tableHeadArray = [];
	tableHeadArray.push('Party Name');
	tableHeadArray.push('Sale Discount');
	tableHeadArray.push('Purchase Discount');
	rowObj.push(tableHeadArray);
	rowObj.push([]);

	var totalSaleDiscount = 0;
	var totalPurchaseDiscount = 0;
	for (var _i2 = 0; _i2 < len; _i2++) {
		var tempArray = [];
		var txn = listOfTransactions[_i2];
		var partyName = txn.getPartyName();
		var saleDiscount = txn.getSaleDiscount();
		var purchaseDiscount = txn.getPurchaseDiscount();
		tempArray.push(partyName);

		tempArray.push(MyDouble.getAmountWithDecimal(saleDiscount));

		tempArray.push(MyDouble.getAmountWithDecimal(purchaseDiscount));

		totalSaleDiscount += saleDiscount;
		totalPurchaseDiscount += purchaseDiscount;
		rowObj.push(tempArray);
	}
	rowObj.push([]);
	var totalArray = ['Total', MyDouble.getAmountWithDecimal(totalSaleDiscount), MyDouble.getAmountWithDecimal(totalPurchaseDiscount)];
	rowObj.push(totalArray);
	return rowObj;
};

function printExcel() {
	var XLSX = require('xlsx');
	var dataArray = prepareObjectForExcel(listOfTransactions);
	var worksheet = XLSX.utils.aoa_to_sheet(dataArray);

	var workbook = {
		'SheetNames': [],
		'Sheets': {}
	};

	var wsName = 'Discount Report';
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet;
	var wscolWidth = [{ wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }];
	worksheet['!cols'] = wscolWidth;
	console.log(workbook);
	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

populateTable();