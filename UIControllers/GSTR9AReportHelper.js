var TaxCodeCache = require('./../Cache/TaxCodeCache.js');
var taxCodeCache = new TaxCodeCache();
var TaxCodeConstants = require('./../Constants/TaxCodeConstants.js');
var TxnTypeConstant = require('./../Constants/TxnTypeConstant.js');
var CompositeUserType = require('./../Constants/CompositeUserType.js');
var ItemCache = require('./../Cache/ItemCache.js');
var SettingCache = require('./../Cache/SettingCache.js');
var GSTRReportHelper = require('./../UIControllers/GSTRReportHelper.js');

var GSTR9AReportHelper = function GSTR9AReportHelper() {
	this.taxableTurnOver = 0;
	this.taxableTurnOverBerfore = 0;
	this.nonTaxableTurnOver = 0;

	var compositionRateChangeDate = MyDate.getDateObj('01/01/2018', 'dd/MM/yyyy', '/');

	this.getTxnListBasedOnDate = function (fromDate, toDate, firmId, considerNonTaxAsExemptedCheck) {
		var GSTR9AReportObject = require('./../BizLogic/GSTR9AReportObject.js');
		var DataLoader = require('./../DBManager/DataLoader.js');
		var itemCache = new ItemCache();
		var settingCache = new SettingCache();
		var dataLoader = new DataLoader();
		var transactionList = dataLoader.loadTransactionsForGSTR9AReport(fromDate, toDate, firmId);
		var gstr9AReportObjectList = [];
		var objectSparseArray = {};
		var name = void 0;

		if (transactionList != null) {
			var lenn = transactionList.length;
			for (var i = 0; i < lenn; i++) {
				var transaction = transactionList[i];
				var transactionType = transaction.getTxnType();
				var transcationTaxId = Number(transaction.getTransactionTaxId());
				var transactionSubTotalAmount = Number(transaction.getSubTotalAmount());
				var transactionDiscountAmount = Number(transaction.getDiscountAmount());
				var transactionDiscountPercentage = Number(transactionDiscountAmount / transactionSubTotalAmount * 100);
				var transactionDate = transaction.getTxnDate();

				name = transaction.getNameRef();
				objectSparseArray = {};
				var NonTaxFlag = GSTRReportHelper.validateFlagForGSTR(transaction) || considerNonTaxAsExemptedCheck; // Include NONE tax transactions for Composite User

				var transactionLineItems = transaction.getLineItems();
				var transactionLineItemsLenth = transaction.getLineItems().length;
				for (var j = 0; j < transactionLineItemsLenth; j++) {
					var lineItem = transactionLineItems[j];
					var lineTaxId = Number(lineItem.getLineItemTaxId());
					var lineItemTotal = Number(lineItem.getLineItemTotal());
					var taxableAmtLineItem = lineItem.getItemQuantity() * lineItem.getItemUnitPrice() - lineItem.getLineItemDiscountAmount();
					var taxableAmountTransaction = Number(lineItemTotal - lineItemTotal * transactionDiscountPercentage / 100);
					var itemObj = itemCache.getItemById(lineItem.getItemId());
					var itemDefaultTaxCodeId = itemObj.getItemTaxId() || 0;
					var itemDefaultTaxObject = itemDefaultTaxCodeId ? taxCodeCache.getTaxCodeObjectById(itemDefaultTaxCodeId) : null;
					var lineitemAdditionalCess = void 0;

					if (transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE || transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
						// tax on lineitem
						if (lineTaxId != 0 && !GSTRReportHelper.isTaxOfTypeOthers(lineTaxId) || lineItem.getLineItemAdditionalCESS() > 0 || NonTaxFlag && !GSTRReportHelper.isTaxOfTypeOthers(lineTaxId)) {
							var reportObject = void 0;
							if (!objectSparseArray[lineTaxId]) {
								reportObject = new GSTR9AReportObject();
								objectSparseArray[lineTaxId] = reportObject;
								gstr9AReportObjectList.push(reportObject);
								if (name != null) {
									reportObject.setNameId(name.getNameId());
								}
								reportObject.setTransactionType(transaction.getTxnType());
								reportObject.setReverseCharge(Number(transaction.getReverseCharge()));
							} else {
								reportObject = objectSparseArray[lineTaxId];
							}

							lineitemAdditionalCess = Number(lineItem.getLineItemAdditionalCESS());

							if (transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
								taxableAmtLineItem = taxableAmtLineItem * -1;
								lineitemAdditionalCess = lineitemAdditionalCess * -1;
							}
							reportObject.setInvoiceTaxableValue(Number(reportObject.getInvoiceTaxableValue()) + taxableAmtLineItem);
							reportObject.setAdditionalCessAmt(reportObject.getAdditionalCessAmt() + Number(lineItem.getLineItemAdditionalCESS()));

							var taxCode = lineTaxId ? taxCodeCache.getTaxCodeObjectById(lineTaxId) : null;
							if (lineTaxId && taxCode && !GSTRReportHelper.isTaxOfTypeOthers(lineTaxId)) {
								reportObject.setRate(taxCode.getTaxRate());
								if (taxCode.getTaxType() == TaxCodeConstants.taxGroup) {
									for (var p = 0; p < taxCode.getTaxCodeMap().length; p++) {
										var taxId = taxCode.getTaxCodeMap()[p];
										var code = void 0;
										if ((code = taxCodeCache.getTaxCodeObjectById(taxId)) != null) {
											this.setTaxEntries(reportObject, code, taxableAmtLineItem);
										}
									}
								} else {
									this.setTaxEntries(reportObject, taxCode, taxableAmtLineItem);
								}
							}
						}

						// tax on transaction
						if (transcationTaxId && !GSTRReportHelper.isTaxOfTypeOthers(transcationTaxId)) {
							var _reportObject = void 0;
							if (!objectSparseArray[transcationTaxId]) {
								_reportObject = new GSTR9AReportObject();
								objectSparseArray[transcationTaxId] = _reportObject;
								gstr9AReportObjectList.push(_reportObject);
								if (name != null) {
									_reportObject.setNameId(name.getNameId());
								}
								_reportObject.setTransactionType(transaction.getTxnType());
								_reportObject.setReverseCharge(Number(transaction.getReverseCharge()));
							} else {
								_reportObject = objectSparseArray[transcationTaxId];
							}

							if (transactionType == TxnTypeConstant.TXN_TYPE_PURCHASE_RETURN) {
								taxableAmountTransaction = taxableAmountTransaction * -1;
							}

							_reportObject.setInvoiceTaxableValue(Number(_reportObject.getInvoiceTaxableValue()) + taxableAmountTransaction);

							var _taxCode = taxCodeCache.getTaxCodeObjectById(transcationTaxId);
							if (transcationTaxId && _taxCode) {
								_reportObject.setRate(_taxCode.getTaxRate());
								if (_taxCode.getTaxType() == TaxCodeConstants.taxGroup) {
									for (var _p = 0; _p < _taxCode.getTaxCodeMap().length; _p++) {
										var _taxId = _taxCode.getTaxCodeMap()[_p];
										var _code = void 0;
										if ((_code = taxCodeCache.getTaxCodeObjectById(_taxId)) != null) {
											this.setTaxEntries(_reportObject, _code, taxableAmountTransaction);
										}
									}
								} else {
									this.setTaxEntries(_reportObject, _taxCode, taxableAmountTransaction);
								}
							}
						}
					} else if (transactionType == TxnTypeConstant.TXN_TYPE_SALE || transactionType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
						// outward supplies
						if (transactionType == TxnTypeConstant.TXN_TYPE_SALE_RETURN) {
							taxableAmountTransaction = taxableAmountTransaction * -1;
						}
						// exempted and nil rated
						var nilratedFilter = itemDefaultTaxObject && (itemDefaultTaxObject.getTaxRateType() == TaxCodeConstants.Exempted || Number(itemDefaultTaxObject.getTaxRate()) == 0) || !itemDefaultTaxObject && considerNonTaxAsExemptedCheck;
						var dateFilterForManufacturer = settingCache.getCompositeUserType() == CompositeUserType.MANUFACTURER && transactionDate < compositionRateChangeDate;

						if (nilratedFilter) {
							this.nonTaxableTurnOver += taxableAmountTransaction;
						} else if (itemDefaultTaxObject || considerNonTaxAsExemptedCheck) {
							if (dateFilterForManufacturer) {
								this.taxableTurnOverBerfore += taxableAmountTransaction;
							} else {
								this.taxableTurnOver += taxableAmountTransaction;
							}
						}
					}
				}
			}
		}
		return gstr9AReportObjectList;
	};

	this.getTurnOverForGSTR9A = function () {
		var turnOverObject = {
			taxable_turn_over: this.taxableTurnOver,
			taxable_turn_over_before: this.taxableTurnOverBerfore,
			non_taxable_turn_over: this.nonTaxableTurnOver
		};
		return turnOverObject;
	};

	this.setTaxEntries = function (reportObject, taxCode, taxableAmt) {
		switch (taxCode.getTaxRateType()) {
			case TaxCodeConstants.IGST:
				reportObject.setIGSTAmt(taxableAmt * taxCode.getTaxRate() / 100 + reportObject.getIGSTAmt());
				break;
			case TaxCodeConstants.SGST:
				reportObject.setSGSTAmt(taxableAmt * taxCode.getTaxRate() / 100 + reportObject.getSGSTAmt());
				break;
			case TaxCodeConstants.CGST:
				reportObject.setCGSTAmt(taxableAmt * taxCode.getTaxRate() / 100 + reportObject.getCGSTAmt());
				break;
			case TaxCodeConstants.CESS:
				reportObject.setCESSAmt(taxableAmt * taxCode.getTaxRate() / 100 + reportObject.getCESSAmt());
				reportObject.setCessRate(taxCode.getTaxRate());
				break;
			case TaxCodeConstants.OTHER:
				reportObject.setOTHERAmt(taxableAmt * taxCode.getTaxRate() / 100 + reportObject.getOTHERAmt());
				this.showOTHER = true;
				break;
		}
	};
};

module.exports = GSTR9AReportHelper;