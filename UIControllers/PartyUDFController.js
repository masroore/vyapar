var _map = require('babel-runtime/core-js/map');

var _map2 = _interopRequireDefault(_map);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* global jQuery */
module.exports = function partySetting($) {
	var ErrorCode = require('./../Constants/ErrorCode.js');
	var UDFCache = require('./../Cache/UDFCache.js');
	var uDFCache = new UDFCache();
	var SettingCache = require('./../Cache/SettingCache.js');
	var settingCache = new SettingCache();
	var UDFModel = require('./../Models/UDFModel.js');
	var UDFFieldsConstant = require('./../Constants/UDFFieldsConstant.js');
	var udfPartyModelMap = new _map2.default();
	var StringConstant = require('./../Constants/StringConstants.js');

	var partyUDFSetting = {

		defaults: {
			mountPoint: '',
			$el: ''
		},

		init: function init() {
			var defaults = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

			this.defaults = $.extend(this.defaults, defaults);
			this.render();
			this.defaults.$el = this.defaults.mountPoint.find('#extraFieldsPartyContainer');
			this.attachEvents();
			window.onResume = this.onResume.bind(this);
		},
		onResume: function onResume(notFromAutoSync) {
			if (!notFromAutoSync) {
				uDFCache.refreshUDF();
				$('#headerPartyFieldsDiv').html(this.getPartyHTML());
				this.fillInputs();
			}
		},
		render: function render() {
			var html = this.getMainTemplate();
			var $mountPoint = this.defaults.mountPoint;
			$mountPoint.html(html);
			$mountPoint.dialog({
				width: 350,
				modal: true,
				close: function (e) {
					e.stopPropagation();
					this.defaults.$el.off('click', '**');
					$mountPoint.html('');
					$mountPoint.dialog('destroy');
				}.bind(this)
			});
			this.fillInputs();
		},
		fillInputs: function fillInputs() {
			var udfPartyFieldsMap = new _map2.default(uDFCache.getPartyFieldsMap());
			udfPartyFieldsMap.forEach(function (value, key) {
				var txnfieldModel = value;
				var i = txnfieldModel.getUdfFieldNumber();
				var bodyName = '#bodyPartyF' + i + 'Name';
				$(bodyName).val(txnfieldModel.getUdfFieldName());
				if (i == UDFFieldsConstant.FIELD_FOR_DATE && txnfieldModel.getUdfFieldDataFormat() == StringConstant.monthYear) {
					$('#datePartyF1Value option[value="2"]').attr('selected', true);
					if (settingCache.isCurrentCountryNepal()) {
						$('#datePartyF1Value').attr('disabled', true);
					}
				}
			});
		},
		attachEvents: function attachEvents() {
			this.defaults.$el.on('change', '.enableExtraPartyField', this.changeOnPartyCheckBox);
			this.defaults.$el.on('click', '#doneForUDFPartyFields', this.saveData.bind(this));
		},
		changeOnPartyCheckBox: function changeOnPartyCheckBox() {
			var labelCheck = $(this).siblings().first();

			if ($(this).prop('checked')) {
				$('#bodyExPartyFieldDiv' + $(this).data('value')).removeClass('hide');
				labelCheck.removeClass('opacity6');
			} else {
				$('#bodyExPartyFieldDiv' + $(this).data('value')).addClass('hide');
				labelCheck.addClass('opacity6');
			}
		},
		saveData: function saveData() {
			this.createOnBasisOfPartyExtraFieldCheckBox();

			var allOkWithPartyName = this.createOnBasisOfBodyOfExtraPartyField();

			this.createOnBasisOfPartyExtraFieldPrint();

			if (allOkWithPartyName) {
				var DataInserter = require('./../DBManager/DataInserter.js');
				var dataInserter = new DataInserter();
				var statusCode = dataInserter.insertUdfData(udfPartyModelMap);
				if (statusCode == ErrorCode.ERROR_SETTING_SAVE_SUCCESS) {
					ToastHelper.success(statusCode);
				} else {
					ToastHelper.error(statusCode);
				}
				$('#extraPartyFieldsSetupDialog').dialog('destroy');
			}
		},
		createOnBasisOfPartyExtraFieldCheckBox: function createOnBasisOfPartyExtraFieldCheckBox() {
			for (var x = 1; x <= UDFFieldsConstant.TOTAL_EXTRA_FIELDS; x++) {
				var key = 'party_field_' + x;

				if (udfPartyModelMap.has(key)) {
					var uDFModel = udfPartyModelMap.get(key);
				} else {
					uDFModel = new UDFModel();
				}
				if ($('#enableBodyExtraPartyField' + x).prop('checked')) {
					uDFModel.setUdfFieldStatus(1);
				} else {
					uDFModel.setUdfFieldStatus(0);
				}

				uDFModel.setUdfFieldType(UDFFieldsConstant.FIELD_TYPE_PARTY);
				uDFModel.setUdfFirmId(1);
				uDFModel.setUdfFieldNuber(x);
				udfPartyModelMap.set(key, uDFModel);
			}
		},
		createOnBasisOfBodyOfExtraPartyField: function createOnBasisOfBodyOfExtraPartyField() {
			for (var i = 1; i <= UDFFieldsConstant.TOTAL_EXTRA_FIELDS; i++) {
				if ($('#enableBodyExtraPartyField' + i).prop('checked')) {
					if ($('#bodyPartyF' + i + 'Name').val() == '') {
						ToastHelper.error('Field Name is mandatory for selected additional fields.');
						$('#loading').hide();
						return 0;
					}
				}
			}
			for (var x = 1; x <= UDFFieldsConstant.TOTAL_EXTRA_FIELDS; x++) {
				var key = 'party_field_' + x;
				if (udfPartyModelMap.has(key)) {
					var uDFModel = udfPartyModelMap.get(key);
				} else {
					uDFModel = new UDFModel();
				}
				var fieldValue = $('#bodyPartyF' + x + 'Name').val();
				uDFModel.setUdfFieldName(fieldValue);
				uDFModel.setUdfFieldType(UDFFieldsConstant.FIELD_TYPE_PARTY);
				uDFModel.setUdfFieldNuber(x);
				if (x == UDFFieldsConstant.FIELD_FOR_DATE) {
					uDFModel.setUdfFieldDataType(UDFFieldsConstant.DATA_TYPE_DATE);
					uDFModel.setUdfFieldDataFormat($('#datePartyF1Value').val());
				}
				udfPartyModelMap.set(key, uDFModel);
			}
			return 1;
		},
		createOnBasisOfPartyExtraFieldPrint: function createOnBasisOfPartyExtraFieldPrint() {
			for (var x = 1; x <= UDFFieldsConstant.TOTAL_EXTRA_FIELDS; x++) {
				var key = 'party_field_' + x;

				if (udfPartyModelMap.has(key)) {
					var uDFModel = udfPartyModelMap.get(key);
				} else {
					uDFModel = new UDFModel();
				}

				if ($('#partyShowInPrint' + x).prop('checked')) {
					uDFModel.setUdfFieldPrintOnInvoice(1);
				} else {
					uDFModel.setUdfFieldPrintOnInvoice(0);
				}

				uDFModel.setUdfFieldType(UDFFieldsConstant.FIELD_TYPE_PARTY);
				uDFModel.setUdfFieldNuber(x);
				udfPartyModelMap.set(key, uDFModel);
			}
		},
		getMainTemplate: function getMainTemplate() {
			return '\n\t\t\t<div class="row udfFields"  class="width100" id="extraFieldsPartyContainer">\n        <div class="col-12">\n            <div id="headerPartyFieldsDiv" class="col-12">\n                \n\t\t\t\t\n\t\t\t\t' + this.getPartyHTML() + '\n               \n            </div>\n\t\t</div>\n\t\t<div class="dialogButton floatRight">\n    <button id="doneForUDFPartyFields" class="terminalButton">Done</button>\n  </div>\n    </div>\n\t\t\t';
		},
		getPartyHTML: function getPartyHTML() {
			var partyFieldsMap = new _map2.default(uDFCache.getPartyFieldsMap());
			var htmlString = '';
			partyFieldsMap.forEach(function (value, key) {
				var partyFieldModel = value;
				var i = partyFieldModel.getUdfFieldNumber();
				htmlString += '<div class="md-checkbox">';
				if (partyFieldModel.getUdfFieldStatus() == 1) {
					htmlString += '\n                    <input id="enableBodyExtraPartyField' + i + '" type="checkbox" class = "enableExtraPartyField " data-value="' + i + '" checked>\n                    <label tabIndex="0" for="enableBodyExtraPartyField' + i + '" class="udfexFieldCheckBoxLabel">Additional Field ' + i + '</label>\n                </div>\n                <div id="bodyExPartyFieldDiv' + i + '" class="extFieldsDiv">';
				} else {
					htmlString += '\n\t\t\t\t\t<input id="enableBodyExtraPartyField' + i + '" type="checkbox" class = "enableExtraPartyField " data-value="' + i + '">';
					if (i == UDFFieldsConstant.FIELD_FOR_DATE) {
						htmlString += '<label tabIndex="0" for="enableBodyExtraPartyField' + i + '" class="udfexFieldCheckBoxLabel opacity6">Date Field</label>';
					} else {
						htmlString += '<label tabIndex="0" for="enableBodyExtraPartyField' + i + '" class="udfexFieldCheckBoxLabel opacity6">Additional Field ' + i + '</label>';
					}
					htmlString += '</div>\n                <div id="bodyExPartyFieldDiv' + i + '" class="extFieldsDiv hide">';
				}

				htmlString += '<label for="bodyPartyF' + i + 'Name" class="headerFieldLabel">Field Name<span class="fontColorRed"> *</span></label>\n                    <input type="text" id="bodyPartyF' + i + 'Name" data-value="' + i + '" maxlength="30" class="headerFields extraPartyFieldName" placeholder="Enter Field Name">';
				if (i == UDFFieldsConstant.FIELD_FOR_DATE) {
					htmlString += '<label for="datePartyF1Value" class="headerFieldLabel">Date Format</label>\n                      <select id="datePartyF1Value" ' + (settingCache.isCurrentCountryNepal() ? 'disabled' : '') + '>\n                        <option value=\'1\'>dd/mm/yy</option>\n                        <option value=\'2\'>mm/yyyy</option>\n\t\t\t\t\t  </select>';
				}
				htmlString += '<div class="udfShowInPrintDiv">\n\t\t\t\t\t\t<span class="udfShowInPrintSpan">Show in print</span>\n\t\t\t\t\t\t<label class="UDFswitch UDFtoggleSwitch">';
				if (partyFieldModel.getUdfFieldPrintOnInvoice() == 1) {
					htmlString += '<input type="checkbox" id="partyShowInPrint' + i + '" class="partyShowInPrint" data-value="' + i + '" checked>';
				} else {
					htmlString += '<input type="checkbox" id="partyShowInPrint' + i + '" class="partyShowInPrint" data-value="' + i + '">';
				}

				htmlString += '\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t<span class="UDFslider round"></span>\n\t\t\t\t\t\t</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>';
			});
			return htmlString;
		}
	};
	return partyUDFSetting.init.bind(partyUDFSetting);
}(jQuery);