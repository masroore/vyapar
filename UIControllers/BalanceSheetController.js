var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DateFormat = require('./../Constants/DateFormat.js');
var PDFHandler = require('./../Utilities/PDFHandler.js');
var pdfHandler = new PDFHandler();
var ExcelHelper = require('./../Utilities/ExcelHelper.js');
var SettingCache = require('./../Cache/SettingCache.js');
var PaymentInfoCache = require('./../Cache/PaymentInfoCache.js');
var LoanAccountCache = require('./../Cache/LoanAccountCache.js');
var paymentInfoCache = new PaymentInfoCache();
var loanAccountCache = new LoanAccountCache();
var settingCache = new SettingCache();
var excelHelper = new ExcelHelper();
var TransactionPDFHandler = require('./../UIControllers/TransactionPDFHandler.js');
var transactionPDFHandler = new TransactionPDFHandler();
var PDFReportConstant = require('./../Constants/PDFReportConstant.js');
var DateFormat = require('./../Constants/DateFormat.js');

var _require = require('./../UIControllers/BalanceSheetHelper.js'),
    getBalanceSheetData = _require.getBalanceSheetData;

$('#balanceSheetFilterEndDate').datepicker({ dateFormat: DateFormat.format });
$('#balanceSheetFilterStartDate').datepicker({ dateFormat: DateFormat.format });
var balanceSheetData;
var assetsTotal;
var liabilitiesTotal;
var endDate;
var bankAccountTotal;
var loanAccountTotal;
var isBankAcountExpanded = false;
var isLoanAcountExpanded = false;
var isOpeningBalanceEquityExpanded = false;
var isOwnersEquityExpanded = false;

function isActiveRow(value) {
	return Number(value) != 0;
	// return true;
}

var populateTable = function populateTable() {
	debugger;
	endDate = $('#balanceSheetFilterEndDate').val();
	endDate = MyDate.getDateObj(endDate, 'dd/MM/yyyy', '/');

	var startDate = $('#balanceSheetFilterStartDate').val();
	startDate = MyDate.getDateObj(startDate, 'dd/MM/yyyy', '/');
	$('#dateUptoBalanceSheet').html(endDate.toDateString().slice(4));

	CommonUtility.showLoader(function () {
		balanceSheetData = getBalanceSheetData(startDate, endDate);

		bankAccountTotal = 0;
		var _iteratorNormalCompletion = true;
		var _didIteratorError = false;
		var _iteratorError = undefined;

		try {
			for (var _iterator = (0, _getIterator3.default)(balanceSheetData.bankAccountBalanceList), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
				var _ref = _step.value;

				var _ref2 = (0, _slicedToArray3.default)(_ref, 2);

				var key = _ref2[0];
				var value = _ref2[1];

				bankAccountTotal += Number(value);
			}
		} catch (err) {
			_didIteratorError = true;
			_iteratorError = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion && _iterator.return) {
					_iterator.return();
				}
			} finally {
				if (_didIteratorError) {
					throw _iteratorError;
				}
			}
		}

		loanAccountTotal = 0;
		var _iteratorNormalCompletion2 = true;
		var _didIteratorError2 = false;
		var _iteratorError2 = undefined;

		try {
			for (var _iterator2 = (0, _getIterator3.default)(balanceSheetData.loanAccountBalanceList), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
				var _ref3 = _step2.value;

				var _ref4 = (0, _slicedToArray3.default)(_ref3, 2);

				var _key = _ref4[0];
				var _value = _ref4[1];

				loanAccountTotal += Number(_value);
			}
		} catch (err) {
			_didIteratorError2 = true;
			_iteratorError2 = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion2 && _iterator2.return) {
					_iterator2.return();
				}
			} finally {
				if (_didIteratorError2) {
					throw _iteratorError2;
				}
			}
		}

		assetsTotal = Number(balanceSheetData.cashInHand) + Number(bankAccountTotal) + Number(balanceSheetData.undepositedChequeAmount) + Number(balanceSheetData.receivableAmount) + Number(balanceSheetData.inventoryClosingStock) + Number(balanceSheetData.taxReceivable) + Number(balanceSheetData.advanceForPurchaseOrder);
		liabilitiesTotal = Number(balanceSheetData.profitAmount) + Number(balanceSheetData.payableAmount) + Number(balanceSheetData.ownersEquity) + Number(balanceSheetData.openingBalnanceEquity) + Number(balanceSheetData.retainedEarningAmount) + Number(balanceSheetData.taxPayable) + Number(balanceSheetData.unwithdrawnChequeAmount) + Number(balanceSheetData.advanceForSaleOrder) + Number(loanAccountTotal);

		var diff = Math.abs(assetsTotal - liabilitiesTotal);

		// if difference is in between 0.01 and 0.1, then make them same bcoz this diff is bcoz of roundoff
		if (diff <= 0.10 && diff >= 0.01) {
			liabilitiesTotal = assetsTotal;
		}

		if (diff > 1) {
			var DeviceHelper = require('../Utilities/deviceHelper');
			var deviceInfo = DeviceHelper.getDeviceInfo();
			MyAnalytics.pushEvent('Balance Sheet Mismatch', {
				'assetsTotal': Number(assetsTotal),
				'liabilitiesTotal': Number(liabilitiesTotal),
				'device_id': deviceInfo.deviceId
			});
			// balanceSheetData.ownersEquity += Number(liabilitiesTotal) - Number(assetsTotal) // commented for testing
		}

		function getRows() {
			var assetsCol = [];
			var liabilitiesCol = [];
			var dropDownArrow = '<img src="../inlineSVG/outline-keyboard_arrow_right-grey.svg"></img>';
			var isBankLIstArrowVisible = balanceSheetData.bankAccountBalanceList.size > 0;
			var isLoanLIstArrowVisible = balanceSheetData.loanAccountBalanceList.size > 0;
			var isOpeningBalanceEquityArrowVisible = isActiveRow(balanceSheetData.inventoryOpeningStock) || isActiveRow(balanceSheetData.openingPartyBalance) || isActiveRow(balanceSheetData.openingBankBalance) || isActiveRow(balanceSheetData.openingCashInHandAmount) || isActiveRow(balanceSheetData.closedChequeAmount) || isActiveRow(balanceSheetData.openingLoanBalanceEquity);
			var isOwnersEquityArrowVisible = isActiveRow(balanceSheetData.addCashAmount) || isActiveRow(balanceSheetData.reduceCashAmount) || isActiveRow(balanceSheetData.addBankAmount) || isActiveRow(balanceSheetData.reduceBankAmount);

			// assetsCol.push([`<li class='balancesheetTableHead bottomBorderLight '>Assets<span class='bsAmountSpan'>Amount</span</li>`]);
			assetsCol.push(['<li style=\'background: #F7F7F7;font-weight: bold;\' class=\'\'>Current Assets</li>']);
			assetsCol.push(['<li class=\'\'>Cash in hand <span class=\'bsAmountSpan\'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.cashInHand) + '</span></li>']);
			assetsCol.push(['<li id=\'bankHeadBS\' class=\'\' style=\'color: #553555;font-weight: bold;\'>\n\t\t\t' + (isBankLIstArrowVisible ? dropDownArrow : '') + '\n\t\t\tBank Accounts<span class=\'bsAmountSpan\'>' + MyDouble.getBalanceAmountWithDecimal(bankAccountTotal) + '</span></li>']);
			var bankListDiv = '<div style=\'max-height: 150px; overflow-y: auto;\'>';
			var bankListDivEnd = '</div>';
			var index = 0;
			var _iteratorNormalCompletion3 = true;
			var _didIteratorError3 = false;
			var _iteratorError3 = undefined;

			try {
				for (var _iterator3 = (0, _getIterator3.default)(balanceSheetData.bankAccountBalanceList), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var _ref5 = _step3.value;

					var _ref6 = (0, _slicedToArray3.default)(_ref5, 2);

					var _key2 = _ref6[0];
					var _value2 = _ref6[1];

					assetsCol.push([(index == 0 ? bankListDiv : '') + '<li class=\'bankList ' + (isBankAcountExpanded ? '' : 'hide') + ' padLeft5 padRight15 \'>' + paymentInfoCache.getPaymentBankName(_key2) + ' <span class=\'floatRight\'>' + MyDouble.getBalanceAmountWithDecimal(_value2) + '</span></li>' + (index == balanceSheetData.bankAccountBalanceList.size - 1 ? bankListDivEnd : '')]);
					index++;
				}
			} catch (err) {
				_didIteratorError3 = true;
				_iteratorError3 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError3) {
						throw _iteratorError3;
					}
				}
			}

			isActiveRow(balanceSheetData.undepositedChequeAmount) && assetsCol.push(['<li class=\'\'>Undeposited cheques <span class=\'bsAmountSpan\'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.undepositedChequeAmount) + '</span</li>']);
			assetsCol.push(['<li class=\'\'>Accounts receivable/ Sundry Debtors <span class=\'bsAmountSpan\'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.receivableAmount) + '</span></li>']);
			isActiveRow(balanceSheetData.advanceForPurchaseOrder) && assetsCol.push(['<li class=\'\'>Advance for purchase order <span class=\'bsAmountSpan\'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.advanceForPurchaseOrder) + '</span></li>']);
			assetsCol.push(['<li class=\'\'>Inventory on hand/ Closing stock <span class=\'bsAmountSpan\'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.inventoryClosingStock) + '</span></li>']);
			assetsCol.push(['<li class=\'\'>Tax Receivable <span class=\'bsAmountSpan\'>' + MyDouble.getBalanceAmountWithDecimal(Number(balanceSheetData.taxReceivable)) + '</span></li> ']);

			// liabilitiesCol.push([`<li class='balancesheetTableHead bottomBorderLight'>Liabilities<span class='bsAmountSpan'>Amount</span</li>`]);
			liabilitiesCol.push(['<div style=\'\'><li style=\'background: #F7F7F7;font-weight: bold;\'>Current Liabilities</li>']);
			liabilitiesCol.push(['<li>Accounts Payable / Sundry Creditors <span class=\'bsAmountSpan\'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.payableAmount) + '</span></li>']);
			liabilitiesCol.push(['<li id=\'loanHeadBS\' class=\'\' style=\'color: #553555;font-weight: bold;\'>\n\t\t\t' + (isLoanLIstArrowVisible ? dropDownArrow : '') + '\n\t\t\tLoan Accounts<span class=\'bsAmountSpan\'>' + MyDouble.getBalanceAmountWithDecimal(loanAccountTotal) + '</span></li>']);
			var loanListDiv = '<div style=\'max-height: 150px; overflow-y: auto;\'>';
			var loanListDivEnd = '</div>';
			index = 0;
			var _iteratorNormalCompletion4 = true;
			var _didIteratorError4 = false;
			var _iteratorError4 = undefined;

			try {
				for (var _iterator4 = (0, _getIterator3.default)(balanceSheetData.loanAccountBalanceList), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
					var _ref7 = _step4.value;

					var _ref8 = (0, _slicedToArray3.default)(_ref7, 2);

					var _key3 = _ref8[0];
					var _value3 = _ref8[1];

					var loanAccount = loanAccountCache.getLoanAccountById(_key3);
					var accountName = loanAccount ? loanAccount.getLoanAccName() : '';
					liabilitiesCol.push([(index == 0 ? loanListDiv : '') + '<li class=\'loanList ' + (isLoanAcountExpanded ? '' : 'hide') + ' padLeft5 padRight15 \'>' + accountName + ' <span class=\'floatRight\'>' + MyDouble.getBalanceAmountWithDecimal(_value3) + '</span></li>' + (index == balanceSheetData.loanAccountBalanceList.size - 1 ? loanListDivEnd : '')]);
					index++;
				}
			} catch (err) {
				_didIteratorError4 = true;
				_iteratorError4 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion4 && _iterator4.return) {
						_iterator4.return();
					}
				} finally {
					if (_didIteratorError4) {
						throw _iteratorError4;
					}
				}
			}

			liabilitiesCol.push(['<li>Tax payable <span class=\'bsAmountSpan\'>' + MyDouble.getBalanceAmountWithDecimal(Number(balanceSheetData.taxPayable)) + '</span></li>']);
			isActiveRow(balanceSheetData.unwithdrawnChequeAmount) && liabilitiesCol.push(['<li>Unwithdrawn cheques <span class=\'bsAmountSpan\'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.unwithdrawnChequeAmount) + '</span></li>']);
			isActiveRow(balanceSheetData.advanceForSaleOrder) && liabilitiesCol.push(['<li>Advance for sale order <span class=\'bsAmountSpan\'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.advanceForSaleOrder) + '</span></li>']);

			liabilitiesCol.push(['<li style=\'font-weight: bold; background: #F7F7F7;\'>Equity/Capital</li></div>']);
			liabilitiesCol.push(['<div style=\'display:flex; overflow-y: auto; flex: 1; flex-direction:column\'><li id=\'openingBalEquityBS\' class=\'\' style=\'color: #553555;font-weight: bold;\'>' + (isOpeningBalanceEquityArrowVisible ? dropDownArrow : '') + 'Opening balance equity<span class=\'bsAmountSpan\'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.openingBalnanceEquity) + '</span></li>']);
			isActiveRow(balanceSheetData.inventoryOpeningStock) && liabilitiesCol.push(['<li class=\'openingbalList  padRight15 ' + (isOpeningBalanceEquityExpanded ? '' : 'hide') + ' padLeft5\'>Opening stock <span class=\'floatRight\'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.inventoryOpeningStock) + '</span></li>']);
			isActiveRow(balanceSheetData.openingPartyBalance) && liabilitiesCol.push(['<li class=\'openingbalList padRight15 ' + (isOpeningBalanceEquityExpanded ? '' : 'hide') + ' padLeft5\'>Opening party balance <span class=\'floatRight\'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.openingPartyBalance) + '</span></li>']);
			isActiveRow(balanceSheetData.openingBankBalance) && liabilitiesCol.push(['<li class=\'openingbalList  padRight15 ' + (isOpeningBalanceEquityExpanded ? '' : 'hide') + ' padLeft5\'>Opening bank balance <span class=\'floatRight\'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.openingBankBalance) + '</span></li>']);
			isActiveRow(balanceSheetData.openingCashInHandAmount) && liabilitiesCol.push(['<li class=\'openingbalList padRight15 ' + (isOpeningBalanceEquityExpanded ? '' : 'hide') + ' padLeft5\'>Opening cash in hand <span class=\'floatRight\'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.openingCashInHandAmount) + '</span></li>']);
			isActiveRow(balanceSheetData.closedChequeAmount) && liabilitiesCol.push(['<li class=\'openingbalList padRight15 ' + (isOpeningBalanceEquityExpanded ? '' : 'hide') + ' padLeft5\'>Closed transaction cheque <span class=\'floatRight\'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.closedChequeAmount) + '</span></li>']);
			isActiveRow(balanceSheetData.openingLoanBalanceEquity) && liabilitiesCol.push(['<li class=\'openingbalList padRight15 ' + (isOpeningBalanceEquityExpanded ? '' : 'hide') + ' padLeft5\'>Opening Loan Balance(-) <span class=\'floatRight\'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.openingLoanBalanceEquity) + '</span></li>']);

			liabilitiesCol.push(['<li id=\'ownersEquityHead\' style=\'color: #553555;font-weight: bold;\'>' + (isOwnersEquityArrowVisible ? dropDownArrow : '') + '\n\t\t\tOwner\'s equity <span class=\'bsAmountSpan\'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.ownersEquity) + '</span></li>']);
			isActiveRow(balanceSheetData.addCashAmount) && liabilitiesCol.push(['<li class=\'ownersList padRight15 ' + (isOwnersEquityExpanded ? '' : 'hide') + ' padLeft5\'>Add cash <span class=\'floatRight\'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.addCashAmount) + '</span></li>']);
			isActiveRow(balanceSheetData.reduceCashAmount) && liabilitiesCol.push(['<li class=\'ownersList  padRight15 ' + (isOwnersEquityExpanded ? '' : 'hide') + ' padLeft5\'>Reduce cash <span class=\'floatRight\'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.reduceCashAmount) + '</span></li>']);
			isActiveRow(balanceSheetData.addBankAmount) && liabilitiesCol.push(['<li class=\'ownersList padRight15 ' + (isOwnersEquityExpanded ? '' : 'hide') + ' padLeft5\'>Increase bank balance <span class=\'floatRight\'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.addBankAmount) + '</span></li>']);
			isActiveRow(balanceSheetData.reduceBankAmount) && liabilitiesCol.push(['<li class=\'ownersList padRight15 ' + (isOwnersEquityExpanded ? '' : 'hide') + ' padLeft5\'>Decrease bank balance <span class=\'floatRight\'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.reduceBankAmount) + '</span></li>']);

			liabilitiesCol.push(['<li>Retained Earnings <span class=\'bsAmountSpan\'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.retainedEarningAmount) + '</span></li>']);
			liabilitiesCol.push(['<li>Net Income (profit) <span class=\'bsAmountSpan\'>' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.profitAmount) + '</span></li></div>']);

			var maxLength = Math.max(liabilitiesCol.length, assetsCol.length);
			var assetrowHtml = '<div style=\'display: flex;flex: 1;border-right: 1px solid #bcbcbc;\'><ul style=\'\'>';
			var liabilitiesRowHtml = '<div style=\'display: flex;flex: 1;\'><ul style=\'display:flex; flex-direction: column\'>';
			for (var _index = 0; _index < maxLength; _index++) {
				var assetsRow = assetsCol[_index] || ['<li class=\'\'></li>'];
				var liabilitiesRow = liabilitiesCol[_index] || ['<li></li>'];
				assetrowHtml += '' + assetsRow.join('');
				liabilitiesRowHtml += '' + liabilitiesRow.join('');
			}

			// assetrowHtml += `<li></li><li class='balancesheetTableHead' style='position: sticky; bottom: 0; width:inherit'>Assets Total<span class='bsAmountSpan'>${MyDouble.getBalanceAmountWithDecimal(assetsTotal)}</span</li></ul>`;
			// liabilitiesRowHtml += `<li></li><li class='balancesheetTableHead' style='position: sticky; bottom: 0; width:inherit'>Liabilities Total<span class='bsAmountSpan'>${MyDouble.getBalanceAmountWithDecimal(liabilitiesTotal)}</span</li></ul>`;
			assetrowHtml += '</ul></div>';
			liabilitiesRowHtml += '</ul></div>';

			return assetrowHtml + liabilitiesRowHtml;
		}

		var htmlText = getRows();

		$('#bsList').html(htmlText);
		$('#bsHeader').html('<div class=\'balancesheetTableHead bsAmountSpan bottomBorderLight\' style=\'font-size:12px; flex:1; padding:8px; border-right: 1px solid #bcbcbc;\'>Assets<span style=\'float:right;\'>Amount</span></div><div class=\'balancesheetTableHead bsAmountSpan bottomBorderLight\' style=\'font-size:12px; flex:1; padding:8px\'>Liabilities<span style=\'float:right\'>Amount</span></div>');
		$('#bsFooter').html('<div class=\'balancesheetTableHead bsAmountSpan \' style=\'font-size:12px; flex:1; padding:8px; border-right: 1px solid #bcbcbc;\'>Assets Total<span style=\'float:right;\'>' + MyDouble.getBalanceAmountWithDecimal(assetsTotal) + '</span></div><div class=\'balancesheetTableHead bsAmountSpan\' style=\'font-size:12px; flex:1; padding:8px\'>Liabilities Total<span style=\'float:right\'>' + MyDouble.getBalanceAmountWithDecimal(liabilitiesTotal) + '</span></div>');

		$('#bankHeadBS').on('click', function () {
			$(this).find('img').toggleClass('rotate90');
			if ($('.bankList').is(':visible')) {
				$('.bankList').hide(500);
				isBankAcountExpanded = false;
			} else {
				$('.bankList').show(500);
				isBankAcountExpanded = true;
			}
		});
		$('#loanHeadBS').on('click', function () {
			$(this).find('img').toggleClass('rotate90');
			if ($('.loanList').is(':visible')) {
				$('.loanList').hide(500);
				isLoanAcountExpanded = false;
			} else {
				$('.loanList').show(500);
				isLoanAcountExpanded = true;
			}
		});
		$('#openingBalEquityBS').on('click', function () {
			$(this).find('img').toggleClass('rotate90');
			if ($('.openingbalList').is(':visible')) {
				$('.openingbalList').hide(500);
				isOpeningBalanceEquityExpanded = false;
			} else {
				$('.openingbalList').show(500);
				isOpeningBalanceEquityExpanded = true;
			}
		});

		$('#ownersEquityHead').on('click', function () {
			$(this).find('img').toggleClass('rotate90');
			if ($('.ownersList').is(':visible')) {
				$('.ownersList').hide(500);
				isOwnersEquityExpanded = false;
			} else {
				$('.ownersList').show(500);
				isOwnersEquityExpanded = true;
			}
		});
	});
};

var fromDate = new Date();
var endDate = new Date();
if (settingCache.isCurrentCountryIndia()) {
	if (fromDate.getMonth() < 3) {
		fromDate.setFullYear(fromDate.getFullYear() - 1);
	}
	fromDate.setDate(1);
	fromDate.setMonth(3);
} else {
	fromDate.setDate(1);
	fromDate.setMonth(0);
}

$('#balanceSheetFilterEndDate').val(MyDate.getDate('d/m/y', endDate));
$('#balanceSheetFilterStartDate').val(MyDate.getDate('d/m/y', fromDate));

$('#balanceSheetFilterEndDate').change(function () {
	populateTable();
});

$('#showZeroAmountFieldsBS').change(function () {
	populateTable();
});

$('#balanceSheetFilterStartDate').change(function () {
	populateTable();
});

var getHTMLTextForReport = function getHTMLTextForReport() {
	var BalanceSheetHTMLGenerator = require('./../ReportHTMLGenerator/BalanceSheetHTMLGenerator.js');
	var balanceSheetHTMLGenerator = new BalanceSheetHTMLGenerator();
	var htmlText = balanceSheetHTMLGenerator.getHTMLText(balanceSheetData, endDate);
	return htmlText;
};

var onResume = function onResume() {
	var notFromAutoSyncFlow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	// $('#frameDiv').load('');
	if (notFromAutoSyncFlow) {
		$('#modelContainer').css({ 'display': 'none' });
		$('.viewItems').css('display', 'block');
	}
	populateTable();
};

/// ///////////////////////////PDF/////////////////////////////////////
var openPDF = function openPDF() {
	var html = $('#htmlView').html();
	$('#loading').show(function () {
		pdfHandler.openPDF(html, true);
	});
};

function printPDF() {
	var html = $('#htmlView').html();
	pdfHandler.print(html);
}

function savePDF() {
	pdfHandler.savePDF({ type: PDFReportConstant.BALANCE_SHEET, fromDate: $('#balanceSheetFilterStartDate').val(), toDate: $('#balanceSheetFilterEndDate').val() });
}

function sharePDF() {
	$('#loading').show(function () {
		pdfHandler.sharePDF();
	});
}

var closePreview = function closePreview() {
	try {
		$('#pdfViewDialog').addClass('hide').dialog('close').dialog('destroy');
	} catch (err) {}
};

var openPreview = function openPreview() {
	$('#loading').show(function () {
		var html = getHTMLTextForReport();
		if (html) {
			pdfHandler.generateHiddenBrowserWindowForPDF(html);
			transactionPDFHandler.openPreviewDialog(html);
		}
	});
};

/// ////////////////////////////////////////////////////////////////////

var downloadExcelFile = function downloadExcelFile() {
	var fileUtil = require('./../Utilities/FileUtil.js');
	var fileName = fileUtil.getFileName({ type: PDFReportConstant.BALANCE_SHEET, fromDate: $('#balanceSheetFilterStartDate').val(), toDate: $('#balanceSheetFilterEndDate').val() });
	excelHelper.saveExcel(fileName);
};

var getbalnceSheetData = function getbalnceSheetData() {
	var assetsCol = [];
	var liabilitiesCol = [];
	var endDate = MyDate.getDateObj($('#balanceSheetFilterEndDate').val(), 'dd/MM/yyyy', '/');

	assetsCol.push(['Balance Sheet as on ' + endDate.toDateString().slice(4)]);
	assetsCol.push([]);
	assetsCol.push(['Assets', 'Amount']);
	assetsCol.push(['Current Assets', '']);
	assetsCol.push(['Cash in hand', MyDouble.getBalanceAmountWithDecimal(balanceSheetData.cashInHand)]);
	assetsCol.push(['', '']);
	assetsCol.push(['Bank Accounts (' + MyDouble.getBalanceAmountWithDecimal(bankAccountTotal) + ')', '']);
	var _iteratorNormalCompletion5 = true;
	var _didIteratorError5 = false;
	var _iteratorError5 = undefined;

	try {
		for (var _iterator5 = (0, _getIterator3.default)(balanceSheetData.bankAccountBalanceList), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
			var _ref9 = _step5.value;

			var _ref10 = (0, _slicedToArray3.default)(_ref9, 2);

			var key = _ref10[0];
			var value = _ref10[1];

			assetsCol.push([paymentInfoCache.getPaymentBankName(key), MyDouble.getBalanceAmountWithDecimal(value)]);
		}
	} catch (err) {
		_didIteratorError5 = true;
		_iteratorError5 = err;
	} finally {
		try {
			if (!_iteratorNormalCompletion5 && _iterator5.return) {
				_iterator5.return();
			}
		} finally {
			if (_didIteratorError5) {
				throw _iteratorError5;
			}
		}
	}

	assetsCol.push(['', '']);
	isActiveRow(balanceSheetData.undepositedChequeAmount) && assetsCol.push(['Undeposited cheques', MyDouble.getBalanceAmountWithDecimal(balanceSheetData.undepositedChequeAmount)]);
	assetsCol.push(['Accounts receivable/Sundry Debtors', MyDouble.getBalanceAmountWithDecimal(balanceSheetData.receivableAmount)]);
	assetsCol.push(['Inventory on hand/Closing stock', MyDouble.getBalanceAmountWithDecimal(balanceSheetData.inventoryClosingStock)]);
	isActiveRow(balanceSheetData.advanceForPurchaseOrder) && assetsCol.push(['Advance for purchase order', MyDouble.getBalanceAmountWithDecimal(balanceSheetData.advanceForPurchaseOrder)]);
	assetsCol.push(['Tax Receivable', MyDouble.getBalanceAmountWithDecimal(balanceSheetData.taxReceivable)]);

	liabilitiesCol.push([]);
	liabilitiesCol.push([]);
	liabilitiesCol.push(['Liabilities', 'Amount']);
	liabilitiesCol.push(['Current Liabilities', '']);
	liabilitiesCol.push(['Accounts Payable/Sundry Creditors', MyDouble.getBalanceAmountWithDecimal(balanceSheetData.payableAmount)]);
	liabilitiesCol.push(['', '']);
	liabilitiesCol.push(['Loan Accounts (' + MyDouble.getBalanceAmountWithDecimal(loanAccountTotal) + ')', '']);
	var _iteratorNormalCompletion6 = true;
	var _didIteratorError6 = false;
	var _iteratorError6 = undefined;

	try {
		for (var _iterator6 = (0, _getIterator3.default)(balanceSheetData.loanAccountBalanceList), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
			var _ref11 = _step6.value;

			var _ref12 = (0, _slicedToArray3.default)(_ref11, 2);

			var _key4 = _ref12[0];
			var _value4 = _ref12[1];

			var loanAccount = loanAccountCache.getLoanAccountById(_key4);
			var accountName = loanAccount ? loanAccount.getLoanAccName() : '';
			liabilitiesCol.push([accountName, MyDouble.getBalanceAmountWithDecimal(_value4)]);
		}
	} catch (err) {
		_didIteratorError6 = true;
		_iteratorError6 = err;
	} finally {
		try {
			if (!_iteratorNormalCompletion6 && _iterator6.return) {
				_iterator6.return();
			}
		} finally {
			if (_didIteratorError6) {
				throw _iteratorError6;
			}
		}
	}

	liabilitiesCol.push(['', '']);
	liabilitiesCol.push(['Tax Payable', MyDouble.getBalanceAmountWithDecimal(balanceSheetData.taxPayable)]);
	isActiveRow(balanceSheetData.unwithdrawnChequeAmount) && liabilitiesCol.push(['Unwithdrawn cheques', MyDouble.getBalanceAmountWithDecimal(balanceSheetData.unwithdrawnChequeAmount)]);
	isActiveRow(balanceSheetData.advanceForSaleOrder) && liabilitiesCol.push(['Advance for sale order', MyDouble.getBalanceAmountWithDecimal(balanceSheetData.advanceForSaleOrder)]);

	liabilitiesCol.push([]);
	liabilitiesCol.push(['Equity/Capital', '']);
	liabilitiesCol.push([]);
	liabilitiesCol.push(['Opening balance equity (' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.openingBalnanceEquity) + ')']);
	isActiveRow(balanceSheetData.openingCashInHandAmount) && liabilitiesCol.push(['Opening cash in hand', MyDouble.getBalanceAmountWithDecimal(balanceSheetData.openingCashInHandAmount)]);
	isActiveRow(balanceSheetData.openingBankBalance) && liabilitiesCol.push(['Opening bank balance', MyDouble.getBalanceAmountWithDecimal(balanceSheetData.openingBankBalance)]);
	isActiveRow(balanceSheetData.openingPartyBalance) && liabilitiesCol.push(['Opening party balance', MyDouble.getBalanceAmountWithDecimal(balanceSheetData.openingPartyBalance)]);
	isActiveRow(balanceSheetData.inventoryOpeningStock) && liabilitiesCol.push(['Opening stock', MyDouble.getBalanceAmountWithDecimal(balanceSheetData.inventoryOpeningStock)]);
	isActiveRow(balanceSheetData.closedChequeAmount) && liabilitiesCol.push(['Closed transaction cheque', MyDouble.getBalanceAmountWithDecimal(balanceSheetData.closedChequeAmount)]);
	isActiveRow(balanceSheetData.openingLoanBalanceEquity) && liabilitiesCol.push(['Opening Loan Balance(-)', MyDouble.getBalanceAmountWithDecimal(balanceSheetData.openingLoanBalanceEquity)]);
	liabilitiesCol.push([]);
	liabilitiesCol.push(['Owner\'s equity (' + MyDouble.getBalanceAmountWithDecimal(balanceSheetData.ownersEquity) + ')']);
	isActiveRow(balanceSheetData.addCashAmount) && liabilitiesCol.push(['Add cash', MyDouble.getBalanceAmountWithDecimal(balanceSheetData.addCashAmount)]);
	isActiveRow(balanceSheetData.reduceCashAmount) && liabilitiesCol.push(['Reduce cash', MyDouble.getBalanceAmountWithDecimal(balanceSheetData.reduceCashAmount)]);
	isActiveRow(balanceSheetData.addBankAmount) && liabilitiesCol.push(['Increase bank balance', MyDouble.getBalanceAmountWithDecimal(balanceSheetData.addBankAmount)]);
	isActiveRow(balanceSheetData.reduceBankAmount) && liabilitiesCol.push(['Decrease bank balance', MyDouble.getBalanceAmountWithDecimal(balanceSheetData.reduceBankAmount)]);
	liabilitiesCol.push([]);
	liabilitiesCol.push(['Retained Earnings', MyDouble.getBalanceAmountWithDecimal(balanceSheetData.retainedEarningAmount)]);
	liabilitiesCol.push(['Net Income(profit)', MyDouble.getBalanceAmountWithDecimal(balanceSheetData.profitAmount)]);

	var maxLength = Math.max(liabilitiesCol.length, assetsCol.length);
	var rowObj = [];
	rowObj.push([]);
	for (var index = 0; index < maxLength; index++) {
		var row = [];
		var assetsRow = assetsCol[index] || ['', ''];
		var liabilitiesRow = liabilitiesCol[index] || [];
		row.push.apply(row, (0, _toConsumableArray3.default)(assetsRow).concat((0, _toConsumableArray3.default)(liabilitiesRow)));
		rowObj[index] = row;
	}
	rowObj.push([]);
	rowObj.push(['', MyDouble.getBalanceAmountWithDecimal(assetsTotal), '', MyDouble.getBalanceAmountWithDecimal(liabilitiesTotal)]);
	return rowObj;
};

function printExcel() {
	var XLSX = require('xlsx');
	var dataArray = getbalnceSheetData();
	var worksheet3_1 = XLSX.utils.aoa_to_sheet(dataArray);

	var workbook = {
		'SheetNames': [],
		'Sheets': {}
	};

	var wsName = 'Balance sheet';
	workbook.SheetNames[0] = wsName;
	workbook.Sheets[wsName] = worksheet3_1;
	worksheet3_1['!cols'] = [{ wch: 40 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 30 }];;
	worksheet3_1['!merges'] = [{ s: { r: 1, c: 0 }, e: { r: 1, c: 4 } }];

	XLSX.writeFile(workbook, appPath + '/report.xlsx');
	downloadExcelFile();
}

populateTable();