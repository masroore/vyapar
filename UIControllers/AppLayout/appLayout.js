var valueGlobal = '';
var txnIdReqGlobal = '';
var itemNameGlobal = '';
var userNameGlobal = '';
var accountNameGlobal = '';
var itemAdjustmentNameGlobal = '';
var itemAdjustmentIdGlobal = '';
var accountAdjustmentNameGlobal = '';
var accountAdjustmentIdGlobal = '';
var TransferChequeIdGlobal = '';
var unitNameGlobal = '';
var firmNameGlobal = '';
var selectedPartyNameFromOutstandingsGlobal = ''; // for selecting particular name when double clicked in outstandings
var selectedItemNameFromReportsGlobal = '';
function canOpenSearch(e, fileName) {
	if (e.keyCode != 9) {
		try {
			if (!canCloseDialogue || canCloseDialogue()) {
				$('#defaultPage').load(fileName);
				$('#titleText').innerHTML = $('#search-anything').val();
			}
		} catch (err) {
			$('#defaultPage').load(fileName);
		}
	}
}