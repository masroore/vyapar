var SqliteDBHelper = require('./../DBManager/SqliteDBHelper.js');
var sqliteDBHelper = new SqliteDBHelper();
var SettingCache = require('./../Cache/SettingCache.js');
var syncToken = null;
var SyncResponseType = require('./../Constants/SyncResponseType.js');
var ErrorCode = require('./../Constants/ErrorCode.js');
var SyncHandler = require('./../Utilities/SyncHandler.js');
var Domain = require('./../Constants/Domain');
var domain = Domain.thisDomain;
var DBWindow = require('../Utilities/DBWindow');

var _require = require('../Constants/IPCActions'),
    CLOSE_DB_CONNECTION = _require.CLOSE_DB_CONNECTION;

var DeviceHelper = require('../Utilities/deviceHelper');
var socket;

SyncHandler.setAdminUserValue(false);

var TokenForSync = require('./../Utilities/TokenForSync');

getSyncToken();

function getSyncToken() {
	if (TokenForSync.ifExistToken()) {
		var tokenFile = TokenForSync.readToken();

		if (tokenFile) {
			syncToken = tokenFile.auth_token;
		}
	}
}

var SyncController = function SyncController() {
	function turnOnSocket() {
		socket = io(domain + ':3001');
	}

	function handleRevokeAccess() {
		$('#revokedialog').dialog({
			title: false,
			closeOnEscape: false,
			resizable: false,
			height: 170,
			minHeight: 170,
			maxHeight: 170,
			width: 500,
			appendTo: '#dynamicDiv',
			modal: true,
			maxWidth: 500,
			close: function close() {
				DBWindow.send(CLOSE_DB_CONNECTION).then(onRevokeAccess);
				try {
					$(this).dialog('close');
					$(this).dialog('destroy');
				} catch (err) {}
			},
			buttons: [{
				text: 'Ok',
				class: 'terminalButton',
				click: function click() {
					try {
						DBWindow.send(CLOSE_DB_CONNECTION).then(onRevokeAccess);

						$(this).dialog('close');
						$(this).dialog('destroy');
					} catch (err) {}
				}
			}]
		});
	};

	function onResumeForSync() {
		var settingCache = new SettingCache();
		try {
			onResume(false);
		} catch (err) {
			// dont bother to do anything since i am caling reload of UI. There might not be this function present in some ui, so fot that this catch block
		}
		try {
			viewUnitsPage();
		} catch (e) {}
		try {
			viewCategoriesPage();
		} catch (e) {}
		try {
			displayBusinessStatus();
		} catch (e) {}
		try {
			updateCompanyInfo();
		} catch (e) {}
		try {
			onRes();
		} catch (e) {}
		try {
			taxListRefresh();
		} catch (e) {}
		try {
			checkDeliveryChallanEnabled();
		} catch (e) {}
		try {
			checkEstimateEnabled();
		} catch (e) {}
		try {
			checkOrderFormEnabled();
		} catch (e) {}
		try {
			checkIncomeEnabled();
		} catch (e) {}
		try {
			if (settingCache.getItemEnabled()) {
				$('#sideNav #viewItems').removeClass('hide'); // .attr('accessKey','3');
				$('.addItemHiddenForItemSetting').removeClass('hide'); // .attr('accessKey','A');
			} else {
				$('#sideNav #viewItems').addClass('hide');
				$('.addItemHiddenForItemSetting').addClass('hide');
			}
		} catch (e) {}
	};

	function taxListRefresh() {
		var settingCache = new SettingCache();
		settingCache.reloadSettingCache();
		if (settingCache.isItemwiseTaxEnabled() || settingCache.getTaxEnabled()) {
			$('#viewTax').removeClass('hide'); // .attr('accessKey','4');
		} else {
			$('#viewTax').addClass('hide'); // .attr('accessKey','');
		}
	}

	function handleSync(message, syncResponseType) {
		var lastChangeLogNumberFromDB = sqliteDBHelper.fetchLastChangeLogNumber();
		var changeLog = message.changeLog;
		var changeLogNumber = 0;

		if (syncResponseType == SyncResponseType.CHANGE_LOG) {
			changeLogNumber = message.changeLogId;
			var referenceChangeLogNumber = message.referenceChangeLogNumber;
			if (referenceChangeLogNumber != lastChangeLogNumberFromDB) {
				emitSubscribeToCompany();
				return;
			}
		} else if (syncResponseType == SyncResponseType.GET_IN_SYNC) {
			changeLogNumber = message.lastChangeLogNumber;
		}

		if (lastChangeLogNumberFromDB >= changeLogNumber || !SyncHandler.isSyncEnabled()) {
			return;
		}

		var interval = setInterval(function () {
			if (!sqliteDBHelper.checkIfSqliteIsBusy()) {
				clearInterval(interval);
				sqliteDBHelper.getInSync(changeLog, changeLogNumber, syncResponseType);
				var RefreshCacheTracker = require('../BizLogic/RefreshCacheTracker');
				var refreshCacheTracker = new RefreshCacheTracker();
				refreshCacheTracker.refreshForAllCache(true);
				onResumeForSync();
			}
		}, 300);
	};

	DBWindow.subscribe(CLOSE_DB_CONNECTION, onRevokeAccess);

	function onRevokeAccess() {
		try {
			DBWindow.unsubscribe(CLOSE_DB_CONNECTION, onRevokeAccess);
			var fs = require('fs');

			sqliteDBHelper.closeConnection();

			var companyNameToBeDeleted = CompanyListUtility.getCurrentCompanyKey();

			if (companyNameToBeDeleted) {
				try {
					var fileNameObj = CompanyListUtility.getCompaniesFileNameObject();
					fs.unlinkSync(fileNameObj[companyNameToBeDeleted].path);
					delete fileNameObj[companyNameToBeDeleted];
					CompanyListUtility.updateCompanyListFile(fileNameObj);

					var _require2 = require('electron'),
					    ipcRenderer = _require2.ipcRenderer;

					ipcRenderer.send('changeURL', 'SwitchCompany.html');
				} catch (err) {}
			}
		} catch (e) {}
	};

	function emitSubscribeToCompany() {
		var deviceInfo = DeviceHelper.getDeviceInfo();
		if (sqliteDBHelper.fetchCompanyGlobalId() && syncToken) {
			socket.emit('subscribe-to-company', {
				company: sqliteDBHelper.fetchCompanyGlobalId(),
				token: syncToken,
				last_change_log_number: sqliteDBHelper.fetchLastChangeLogNumber(),
				prev_socket_id: sqliteDBHelper.getOldSocketId() ? sqliteDBHelper.getOldSocketId() : '',
				platform: 0,
				device_id: deviceInfo.deviceId
			}, function (data) {
				SyncHandler.setAdminUserValue(false);
				if (data.socketId) {
					sqliteDBHelper.setSocketId(data.socketId);
				}

				if (data.message == 'Authorized') {
					SyncHandler.setAdminUserValue(data.isAdmin);
				} else if (data.message == 'UnAuthorized') {
					if (data.revoked) {
						handleRevokeAccess();
					} else {
						alert(ErrorCode.ERROR_USER_NOT_AUTHORISED);

						var _require3 = require('electron'),
						    ipcRenderer = _require3.ipcRenderer;

						ipcRenderer.send('changeURL', 'SwitchCompany.html');
					}
				} else if (data.message == 'Unauthenticated') {
					alert(ErrorCode.ERROR_USER_NOT_AUTHENTICATED);
					var SyncHelper = require('./../Utilities/SyncHelper');

					SyncHelper.loginToGetAuthToken(null, function () {
						var TokenForSync = require('./../Utilities/TokenForSync');

						if (!TokenForSync.ifExistToken()) {
							var _require4 = require('electron'),
							    _ipcRenderer = _require4.ipcRenderer;

							_ipcRenderer.send('changeURL', 'SwitchCompany.html');
						}
					});
				}
			});
		}
	};

	function turnOnSocketConnectEvent() {
		socket.on('connect', function (connectData) {
			emitSubscribeToCompany();
		});
	};

	function turnOnSocketChangeLogEvent() {
		socket.on('Change Log', function (message) {
			var syncResponseType = SyncResponseType.CHANGE_LOG;

			handleSync(message, syncResponseType);
		});
	};

	function turnOnSocketGetInSyncEvent() {
		socket.on('get-in-sync', function (message) {
			var syncResponseType = SyncResponseType.GET_IN_SYNC;
			handleSync(message, syncResponseType);
		});
	};

	function turnOnSocketRevokeAccessEvent() {
		socket.on('revoke_access', function (message) {
			var companyGlobalIdFromServer = message.company_global_id;

			var companyGlobalIdFromDB = sqliteDBHelper.fetchCompanyGlobalId();

			if (companyGlobalIdFromServer == companyGlobalIdFromDB) {
				handleRevokeAccess();
			}
		});
	};

	function turnOnSocketDisconnectEvent() {
		socket.on('disconnect', function () {});
	};

	function turnOffSocket() {
		if (socket) {
			socket = null;
		}
	}

	this.turnOnSocketEventsForSync = function () {
		getSyncToken();
		turnOnSocket();
		turnOnSocketConnectEvent();
		turnOnSocketChangeLogEvent();
		turnOnSocketGetInSyncEvent();
		turnOnSocketRevokeAccessEvent();
		turnOnSocketDisconnectEvent();
	};

	this.turnOffSocketEventsForSync = function () {
		turnOffSocket();
	};
};

module.exports = SyncController;