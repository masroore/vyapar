document.getElementById('submit').addEventListener('click', SaveNewItem);
var ItemType;
var ErrorCode = require('../Constants/ErrorCode');

function SaveNewItem() {
	if (!ItemType) {
		ItemType = require('./../Constants/ItemType.js');
	}

	var obj = {
		'itemName': document.getElementById('itemName').value,
		'salePricePerUnit': MyDouble.convertStringToDouble(document.getElementById('salePrice').value),
		'purchasePricePerUnit': MyDouble.convertStringToDouble(document.getElementById('purchasePrice').value),
		'stockQuantity': MyDouble.convertStringToDouble(document.getElementById('openingStock').value),
		'minStockQuantity': MyDouble.convertStringToDouble(document.getElementById('minimumStock').value),
		'itemLocation': document.getElementById('itemLocation').value ? document.getElementById('itemLocation').value : '',
		'itemType': ItemType['ITEM_TYPE_INVENTORY']
	};
	var ItemLogic = require('./../BizLogic/ItemLogic.js');
	itemlogic = new ItemLogic();
	console.log(obj);
	var statusCode = itemlogic.SaveNewItem(obj);
	if (statusCode == ErrorCode.ERROR_ITEM_SAVE_SUCCESS) {
		ToastHelper.success(statusCode);
	} else {
		ToastHelper.error(statusCode);
	}
}